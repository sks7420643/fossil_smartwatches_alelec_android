package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.fossil.np1;
import com.fossil.rp1;
import com.fossil.ys1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rs1 implements ur1, ys1 {
    @DexIgnore
    public /* final */ vs1 a;
    @DexIgnore
    public /* final */ zs1 b;
    @DexIgnore
    public /* final */ zs1 c;
    @DexIgnore
    public /* final */ vr1 d;

    @DexIgnore
    public interface b<T, U> {
        @DexIgnore
        U apply(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    public rs1(zs1 zs1, zs1 zs12, vr1 vr1, vs1 vs1) {
        this.a = vs1;
        this.b = zs1;
        this.c = zs12;
        this.d = vr1;
    }

    @DexIgnore
    public static /* synthetic */ SQLiteDatabase b(Throwable th) {
        throw new xs1("Timed out while trying to open db.", th);
    }

    @DexIgnore
    public static String c(Iterable<zr1> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<zr1> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().b());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    @DexIgnore
    public final SQLiteDatabase a() {
        vs1 vs1 = this.a;
        vs1.getClass();
        return (SQLiteDatabase) a(js1.a(vs1), ls1.a());
    }

    @DexIgnore
    public int cleanUp() {
        return ((Integer) a(ds1.a(this.b.a() - this.d.b()))).intValue();
    }

    @DexIgnore
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public final boolean d() {
        return b() * c() >= this.d.d();
    }

    @DexIgnore
    public Iterable<rp1> w() {
        return (Iterable) a(cs1.a());
    }

    @DexIgnore
    public zr1 a(rp1 rp1, np1 np1) {
        mq1.a("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", rp1.c(), np1.f(), rp1.a());
        long longValue = ((Long) a(ms1.a(this, rp1, np1))).longValue();
        if (longValue < 1) {
            return null;
        }
        return zr1.a(longValue, rp1, np1);
    }

    @DexIgnore
    public final Long b(SQLiteDatabase sQLiteDatabase, rp1 rp1) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(new String[]{rp1.a(), String.valueOf(ft1.a(rp1.c()))}));
        if (rp1.b() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(rp1.b(), 0));
        }
        return (Long) a(sQLiteDatabase.query("transport_contexts", new String[]{"_id"}, sb.toString(), (String[]) arrayList.toArray(new String[0]), (String) null, (String) null, (String) null), ns1.a());
    }

    @DexIgnore
    public static /* synthetic */ Long a(rs1 rs1, rp1 rp1, np1 np1, SQLiteDatabase sQLiteDatabase) {
        if (rs1.d()) {
            return -1L;
        }
        long a2 = rs1.a(sQLiteDatabase, rp1);
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", Long.valueOf(a2));
        contentValues.put("transport_name", np1.f());
        contentValues.put("timestamp_ms", Long.valueOf(np1.c()));
        contentValues.put("uptime_ms", Long.valueOf(np1.g()));
        contentValues.put("payload", np1.e());
        contentValues.put("code", np1.b());
        contentValues.put("num_attempts", 0);
        long insert = sQLiteDatabase.insert("events", (String) null, contentValues);
        for (Map.Entry next : np1.d().entrySet()) {
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("event_id", Long.valueOf(insert));
            contentValues2.put("name", (String) next.getKey());
            contentValues2.put("value", (String) next.getValue());
            sQLiteDatabase.insert("event_metadata", (String) null, contentValues2);
        }
        return Long.valueOf(insert);
    }

    @DexIgnore
    public boolean c(rp1 rp1) {
        return ((Boolean) a(qs1.a(this, rp1))).booleanValue();
    }

    @DexIgnore
    public static /* synthetic */ List c(SQLiteDatabase sQLiteDatabase) {
        return (List) a(sQLiteDatabase.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), is1.a());
    }

    @DexIgnore
    public static /* synthetic */ List c(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            rp1.a d2 = rp1.d();
            d2.a(cursor.getString(1));
            d2.a(ft1.a(cursor.getInt(2)));
            d2.a(a(cursor.getString(3)));
            arrayList.add(d2.a());
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ Long b(Cursor cursor) {
        if (!cursor.moveToNext()) {
            return null;
        }
        return Long.valueOf(cursor.getLong(0));
    }

    @DexIgnore
    public void b(Iterable<zr1> iterable) {
        if (iterable.iterator().hasNext()) {
            a(os1.a("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in " + c(iterable)));
        }
    }

    @DexIgnore
    public final List<zr1> c(SQLiteDatabase sQLiteDatabase, rp1 rp1) {
        ArrayList arrayList = new ArrayList();
        Long b2 = b(sQLiteDatabase, rp1);
        if (b2 == null) {
            return arrayList;
        }
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        a(sQLiteDatabase2.query("events", new String[]{"_id", "transport_name", "timestamp_ms", "uptime_ms", "payload", "code"}, "context_id = ?", new String[]{b2.toString()}, (String) null, (String) null, (String) null, String.valueOf(this.d.c())), es1.a(arrayList, rp1));
        return arrayList;
    }

    @DexIgnore
    public long b(rp1 rp1) {
        return ((Long) a(a().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{rp1.a(), String.valueOf(ft1.a(rp1.c()))}), ps1.a())).longValue();
    }

    @DexIgnore
    public static /* synthetic */ List b(rs1 rs1, rp1 rp1, SQLiteDatabase sQLiteDatabase) {
        List<zr1> c2 = rs1.c(sQLiteDatabase, rp1);
        rs1.a(c2, rs1.a(sQLiteDatabase, c2));
        return c2;
    }

    @DexIgnore
    public final long c() {
        return a().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }

    @DexIgnore
    public final long a(SQLiteDatabase sQLiteDatabase, rp1 rp1) {
        Long b2 = b(sQLiteDatabase, rp1);
        if (b2 != null) {
            return b2.longValue();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", rp1.a());
        contentValues.put("priority", Integer.valueOf(ft1.a(rp1.c())));
        contentValues.put("next_request_ms", 0);
        if (rp1.b() != null) {
            contentValues.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(rp1.b(), 0));
        }
        return sQLiteDatabase.insert("transport_contexts", (String) null, contentValues);
    }

    @DexIgnore
    public final long b() {
        return a().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }

    @DexIgnore
    public static /* synthetic */ Object a(String str, SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.compileStatement(str).execute();
        sQLiteDatabase.compileStatement("DELETE FROM events WHERE num_attempts >= 10").execute();
        return null;
    }

    @DexIgnore
    public void a(Iterable<zr1> iterable) {
        if (iterable.iterator().hasNext()) {
            a().compileStatement("DELETE FROM events WHERE _id in " + c(iterable)).execute();
        }
    }

    @DexIgnore
    public static /* synthetic */ Long a(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return 0L;
    }

    @DexIgnore
    public static /* synthetic */ Boolean a(rs1 rs1, rp1 rp1, SQLiteDatabase sQLiteDatabase) {
        Long b2 = rs1.b(sQLiteDatabase, rp1);
        if (b2 == null) {
            return false;
        }
        return (Boolean) a(rs1.a().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{b2.toString()}), ks1.a());
    }

    @DexIgnore
    public void a(rp1 rp1, long j) {
        a(as1.a(j, rp1));
    }

    @DexIgnore
    public static /* synthetic */ Object a(long j, rp1 rp1, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("next_request_ms", Long.valueOf(j));
        if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{rp1.a(), String.valueOf(ft1.a(rp1.c()))}) < 1) {
            contentValues.put("backend_name", rp1.a());
            contentValues.put("priority", Integer.valueOf(ft1.a(rp1.c())));
            sQLiteDatabase.insert("transport_contexts", (String) null, contentValues);
        }
        return null;
    }

    @DexIgnore
    public Iterable<zr1> a(rp1 rp1) {
        return (Iterable) a(bs1.a(this, rp1));
    }

    @DexIgnore
    public static byte[] a(String str) {
        if (str == null) {
            return null;
        }
        return Base64.decode(str, 0);
    }

    @DexIgnore
    public static /* synthetic */ Object a(List list, rp1 rp1, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            np1.a i = np1.i();
            i.a(cursor.getString(1));
            i.a(cursor.getLong(2));
            i.b(cursor.getLong(3));
            i.a(cursor.getBlob(4));
            if (!cursor.isNull(5)) {
                i.a(Integer.valueOf(cursor.getInt(5)));
            }
            list.add(zr1.a(j, rp1, i.a()));
        }
        return null;
    }

    @DexIgnore
    public final Map<Long, Set<c>> a(SQLiteDatabase sQLiteDatabase, List<zr1> list) {
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).b());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        a(sQLiteDatabase.query("event_metadata", new String[]{"event_id", "name", "value"}, sb.toString(), (String[]) null, (String) null, (String) null, (String) null), fs1.a(hashMap));
        return hashMap;
    }

    @DexIgnore
    public static /* synthetic */ Object a(Map map, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            Set set = (Set) map.get(Long.valueOf(j));
            if (set == null) {
                set = new HashSet();
                map.put(Long.valueOf(j), set);
            }
            set.add(new c(cursor.getString(1), cursor.getString(2)));
        }
        return null;
    }

    @DexIgnore
    public final List<zr1> a(List<zr1> list, Map<Long, Set<c>> map) {
        ListIterator<zr1> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            zr1 next = listIterator.next();
            if (map.containsKey(Long.valueOf(next.b()))) {
                np1.a h = next.a().h();
                for (c cVar : map.get(Long.valueOf(next.b()))) {
                    h.a(cVar.a, cVar.b);
                }
                listIterator.set(zr1.a(next.b(), next.c(), h.a()));
            }
        }
        return list;
    }

    @DexIgnore
    public final <T> T a(d<T> dVar, b<Throwable, T> bVar) {
        long a2 = this.c.a();
        while (true) {
            try {
                return dVar.a();
            } catch (SQLiteDatabaseLockedException e) {
                if (this.c.a() >= ((long) this.d.a()) + a2) {
                    return bVar.apply(e);
                }
                SystemClock.sleep(50);
            }
        }
    }

    @DexIgnore
    public final void a(SQLiteDatabase sQLiteDatabase) {
        a(gs1.a(sQLiteDatabase), hs1.a());
    }

    @DexIgnore
    public static /* synthetic */ Object a(Throwable th) {
        throw new xs1("Timed out while trying to acquire the lock.", th);
    }

    @DexIgnore
    public <T> T a(ys1.a<T> aVar) {
        SQLiteDatabase a2 = a();
        a(a2);
        try {
            T s = aVar.s();
            a2.setTransactionSuccessful();
            return s;
        } finally {
            a2.endTransaction();
        }
    }

    @DexIgnore
    public final <T> T a(b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase a2 = a();
        a2.beginTransaction();
        try {
            T apply = bVar.apply(a2);
            a2.setTransactionSuccessful();
            return apply;
        } finally {
            a2.endTransaction();
        }
    }

    @DexIgnore
    public static <T> T a(Cursor cursor, b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }
}
