package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fh1 extends if1 {
    @DexIgnore
    public /* final */ zd0[] B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fh1(ue1 ue1, q41 q41, zd0[] zd0Arr, String str, int i) {
        super(ue1, q41, eh1.SET_REPLY_MESSAGE, (i & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
        this.B = zd0Arr;
    }

    @DexIgnore
    public void h() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        boolean z = false;
        for (zd0 replyMessageGroup : this.B) {
            for (y70 messageIcon : replyMessageGroup.getReplyMessageGroup().getReplyMessages()) {
                copyOnWriteArrayList.addIfAbsent(messageIcon.getMessageIcon());
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new NotificationIcon[0]);
        if (array != null) {
            NotificationIcon[] notificationIconArr = (NotificationIcon[]) array;
            if (notificationIconArr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    co0 co0 = co0.d;
                    w40 w40 = this.x.a().i().get(Short.valueOf(w31.NOTIFICATION.a));
                    if (w40 == null) {
                        w40 = mi0.A.g();
                    }
                    if1.a((if1) this, (if1) new p21(this.w, this.x, eh1.PUT_REPLY_MESSAGE_ICON, true, 1795, co0.a(notificationIconArr, 1795, w40), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), (hg6) new a81(this), (hg6) new x91(this), (ig6) new sb1(this), (hg6) null, (hg6) null, 48, (Object) null);
                } catch (sw0 e) {
                    qs0.h.a(e);
                    a(km1.a(this.v, (eh1) null, sk1.INCOMPATIBLE_FIRMWARE, (bn0) null, 5));
                }
            } else {
                m();
            }
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.REPLY_MESSAGE_GROUP, (Object) cw0.a((p40[]) this.B));
    }

    @DexIgnore
    public final void m() {
        short b = lk1.b.b(this.w.t, w31.REPLY_MESSAGES_FILE);
        try {
            of0 of0 = of0.d;
            w40 w40 = this.x.a().i().get(Short.valueOf(w31.REPLY_MESSAGES_FILE.a));
            if (w40 == null) {
                w40 = new w40((byte) 2, (byte) 0);
            }
            byte[] a = of0.a(b, w40, this.B);
            if1.a((if1) this, (if1) new p21(this.w, this.x, eh1.PUT_REPLY_MESSAGE_DATA, true, b, a, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), (hg6) new nd1(this), (hg6) new jf1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
        } catch (sw0 e) {
            qs0.h.a(e);
            a(km1.a(this.v, (eh1) null, sk1.UNSUPPORTED_FORMAT, (bn0) null, 5));
        }
    }
}
