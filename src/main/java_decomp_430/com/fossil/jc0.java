package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc0 extends ic0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public jc0 createFromParcel(Parcel parcel) {
            return new jc0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new jc0[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m30createFromParcel(Parcel parcel) {
            return new jc0(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public /* synthetic */ jc0(Parcel parcel, qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    /* renamed from: isPercentageCircleEnable */
    public boolean b() {
        return super.b();
    }

    @DexIgnore
    public jc0(boolean z) {
        super(pz0.PERCENTAGE_CIRCLE, z);
    }
}
