package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ e73 e;

    @DexIgnore
    public h73(e73 e73, String str, String str2, Object obj, long j) {
        this.e = e73;
        this.a = str;
        this.b = str2;
        this.c = obj;
        this.d = j;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.a, this.b, this.c, this.d);
    }
}
