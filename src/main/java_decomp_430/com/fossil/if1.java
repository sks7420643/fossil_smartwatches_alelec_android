package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class if1 {
    @DexIgnore
    public static /* final */ hf1 A; // = new hf1((qg6) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public qv0 b;
    @DexIgnore
    public /* final */ j71 c;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<if1, cd6>> d;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<if1, cd6>> e;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<if1, cd6>> f;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<if1, cd6>> g;
    @DexIgnore
    public CopyOnWriteArrayList<ig6<if1, Float, cd6>> h;
    @DexIgnore
    public /* final */ ArrayList<hl1> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public y11 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public if1 n;
    @DexIgnore
    public /* final */ Handler o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ dr0 q;
    @DexIgnore
    public /* final */ wy0 r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public km1 v;
    @DexIgnore
    public /* final */ ue1 w;
    @DexIgnore
    public /* final */ q41 x;
    @DexIgnore
    public /* final */ eh1 y;
    @DexIgnore
    public String z;

    @DexIgnore
    public if1(ue1 ue1, q41 q41, eh1 eh1, String str) {
        this.w = ue1;
        this.x = q41;
        this.y = eh1;
        this.z = str;
        this.a = cw0.a((Enum<?>) this.y);
        this.c = new qt0(this);
        this.d = new CopyOnWriteArrayList<>();
        this.e = new CopyOnWriteArrayList<>();
        this.f = new CopyOnWriteArrayList<>();
        this.g = new CopyOnWriteArrayList<>();
        this.h = new CopyOnWriteArrayList<>();
        this.i = qd6.a(new hl1[]{hl1.DEVICE_INFORMATION});
        ld1 ld1 = ld1.NOT_AUTHENTICATED;
        this.k = y11.NORMAL;
        this.l = System.currentTimeMillis();
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.o = new Handler(myLooper);
            this.p = 30000;
            this.q = new dr0(new q01(this));
            this.r = new wy0(this);
            qs0.h.a(new nn0(cw0.a((Enum<?>) this.y), og0.PHASE_INIT, this.w.t, cw0.a((Enum<?>) this.y), this.z, true, (String) null, (r40) null, (bw0) null, (JSONObject) null, 960));
            this.v = new km1(this.y, sk1.NOT_START, (bn0) null, 4);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public qv0 a(lx0 lx0) {
        return null;
    }

    @DexIgnore
    public final void a(dr0 dr0) {
        if (!dr0.a) {
            dr0.a = true;
            this.o.removeCallbacksAndMessages(dr0);
        }
    }

    @DexIgnore
    public boolean a(qv0 qv0) {
        return false;
    }

    @DexIgnore
    public boolean b() {
        return this.j;
    }

    @DexIgnore
    public final boolean c() {
        return b() && this.x.a().e().compareTo(mi0.A.n()) >= 0;
    }

    @DexIgnore
    public Object d() {
        return cd6.a;
    }

    @DexIgnore
    public y11 e() {
        return this.k;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.i;
    }

    @DexIgnore
    public final long g() {
        return this.l;
    }

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public JSONObject i() {
        return new JSONObject();
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public JSONObject k() {
        return new JSONObject();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        r0 = com.fossil.cc0.DEBUG;
        r2 = new java.lang.Object[]{r5.z, f().toString()};
        r2 = com.fossil.cc0.DEBUG;
        r0 = new java.lang.Object[]{r5.z, java.lang.Long.valueOf(r5.p)};
        r0 = r5.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
        if (r0 <= 0) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r5.o.postDelayed(r5.q, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        r0 = r5.x.b();
        r1 = r5.r;
        r0.a.b(r5);
        r0.c.put(r5, r1);
        r0.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005b, code lost:
        return;
     */
    @DexIgnore
    public final void l() {
        synchronized (Boolean.valueOf(this.s)) {
            if (!this.s) {
                this.s = true;
                cd6 cd6 = cd6.a;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return cw0.a((Enum<?>) this.y) + '(' + this.z + ')';
    }

    @DexIgnore
    public final boolean b(if1 if1) {
        if (wg6.a(this.n, if1)) {
            return true;
        }
        if1 if12 = this.n;
        return if12 != null && if12.b(if1);
    }

    @DexIgnore
    public final if1 b(hg6<? super if1, cd6> hg6) {
        if (!this.t) {
            this.f.add(hg6);
        } else if (this.v.b != sk1.SUCCESS) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final if1 c(hg6<? super if1, cd6> hg6) {
        if (!this.t) {
            this.e.add(hg6);
        } else if (this.v.b == sk1.SUCCESS) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public void a(y11 y11) {
        this.k = y11;
    }

    @DexIgnore
    public boolean a(if1 if1) {
        return b(if1) || this.y != if1.y;
    }

    @DexIgnore
    public static /* synthetic */ void a(if1 if1, qv0 qv0, hg6 hg6, hg6 hg62, xf0 xf0, oh0 oh0, hj0 hj0, int i2, Object obj) {
        boolean z2;
        if (obj == null) {
            if ((i2 & 8) != 0) {
                xf0 = xf0.a;
            }
            if ((i2 & 16) != 0) {
                oh0 = oh0.a;
            }
            if ((i2 & 32) != 0) {
                hj0 = hj0.a;
            }
            if (if1.t || (z2 && (!(z2 = if1.u) || !if1.a(qv0)))) {
                if1.a(if1.v);
                return;
            }
            qv0.d = if1.z;
            nn0 nn0 = qv0.f;
            if (nn0 != null) {
                nn0.h = qv0.d;
            }
            qv0.c = cw0.a((Enum<?>) if1.y);
            nn0 nn02 = qv0.f;
            if (nn02 != null) {
                nn02.g = qv0.c;
            }
            if1.b = qv0;
            qv0 qv02 = if1.b;
            if (qv02 != null) {
                qv02.b((hg6<? super qv0, cd6>) hg6);
                qv02.a((hg6<? super qv0, cd6>) new bl0(if1, hj0, hg62));
                if (!qv02.t) {
                    qv02.o.add(xf0);
                }
                if (!qv02.t) {
                    qv02.n.add(oh0);
                } else {
                    oh0.invoke(qv02);
                }
                if (!qv02.t) {
                    cc0 cc0 = cc0.DEBUG;
                    new Object[1][0] = qv02.h().toString(2);
                    long currentTimeMillis = System.currentTimeMillis();
                    nn0 nn03 = qv02.f;
                    if (nn03 != null) {
                        nn03.a = currentTimeMillis;
                    }
                    nn0 nn04 = qv02.f;
                    if (nn04 != null) {
                        nn04.m = cw0.a(nn04.m, cw0.a(cw0.a(cw0.a(qv02.h(), bm0.CREATED_AT, (Object) Double.valueOf(cw0.a(qv02.e))), bm0.STARTED_AT, (Object) Double.valueOf(cw0.a(currentTimeMillis))), bm0.REQUEST_TIMEOUT_IN_MS, (Object) Long.valueOf(qv02.e())));
                    }
                    qv02.a(qv02.p);
                    ue1 ue1 = qv02.y;
                    es0 es0 = qv02.q;
                    if (!ue1.f.contains(es0)) {
                        ue1.f.add(es0);
                    }
                    ue1 ue12 = qv02.y;
                    xt0 xt0 = qv02.r;
                    if (!ue12.g.contains(xt0)) {
                        ue12.g.add(xt0);
                    }
                    qv02.b();
                    return;
                }
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeRequest");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ if1(ue1 ue1, q41 q41, eh1 eh1, String str, int i2) {
        this(ue1, q41, eh1, (i2 & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public final void a(bn0 bn0) {
        this.v = km1.a(this.v, (eh1) null, sk1.G.a(bn0), bn0, 1);
        a();
    }

    @DexIgnore
    public final void a(km1 km1) {
        this.v = km1.a(km1, this.y, (sk1) null, (bn0) null, 6);
        a();
    }

    @DexIgnore
    public final void a() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            this.w.h.remove(this.c);
            uk0 b2 = this.x.b();
            b2.e.b(this);
            b2.a.remove(this);
            b2.b.remove(this);
            b2.c.remove(this);
            JSONObject a2 = cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.MESSAGE, (Object) A.a(this.v)), bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.v.b)), k());
            if (this.v.c.c != il0.SUCCESS) {
                cw0.a(a2, bm0.ERROR_DETAIL, (Object) cw0.a(cw0.a(new JSONObject(), bm0.REQUEST_ID, (Object) this.v.c.a), bm0.REQUEST_UUID, (Object) this.v.c.b));
            }
            nn0 nn0 = new nn0(cw0.a((Enum<?>) this.y), og0.PHASE_END, this.w.t, cw0.a((Enum<?>) this.y), this.z, this.v.b == sk1.SUCCESS, (String) null, (r40) null, (bw0) null, a2, 448);
            qs0.h.a(nn0);
            if (this instanceof hx0) {
                zj0.h.a(nn0);
            } else if ((this instanceof r91) || (this instanceof u71)) {
                fi0.h.a(nn0);
            }
            km1 km1 = this.v;
            if (km1.b == sk1.SUCCESS) {
                cc0 cc0 = cc0.DEBUG;
                Object[] objArr = {this.z, p40.a(km1, 0, 1, (Object) null), k().toString(2)};
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    ((hg6) it.next()).invoke(this);
                }
            } else {
                cc0 cc02 = cc0.ERROR;
                Object[] objArr2 = {this.z, p40.a(km1, 0, 1, (Object) null)};
                Iterator<T> it2 = this.f.iterator();
                while (it2.hasNext()) {
                    ((hg6) it2.next()).invoke(this);
                }
            }
            Iterator<T> it3 = this.g.iterator();
            while (it3.hasNext()) {
                ((hg6) it3.next()).invoke(this);
            }
        }
    }

    @DexIgnore
    public final void a(float f2) {
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            ((ig6) it.next()).invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void a(h91 h91) {
        if (ao1.a[h91.ordinal()] == 1) {
            a(sk1.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(if1 if1, lx0 lx0, lx0 lx02, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                lx02 = lx0;
            }
            if1.a(lx0, lx02);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryExecuteRequest");
    }

    @DexIgnore
    public final void a(lx0 lx0, lx0 lx02) {
        a(lx0, (gg6<cd6>) new k21(this, lx02));
    }

    @DexIgnore
    public final void a(lx0 lx0, gg6<cd6> gg6) {
        qv0 a2 = a(lx0);
        if (a2 == null) {
            a(km1.a(this.v, (eh1) null, sk1.FLOW_BROKEN, (bn0) null, 5));
        } else if (this.m < a2.z) {
            a(this, a2, (hg6) g41.a, (hg6) new w91(this, gg6), (ig6) null, (hg6) new rb1(this), (hg6) md1.a, 8, (Object) null);
        } else {
            a(this.v);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(if1 if1, if1 if12, hg6 hg6, hg6 hg62, um0 um0, no0 no0, fq0 fq0, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                um0 = um0.a;
            }
            if ((i2 & 16) != 0) {
                no0 = no0.a;
            }
            if ((i2 & 32) != 0) {
                fq0 = fq0.a;
            }
            if (!if1.t) {
                if1.n = if12;
                if1 if13 = if1.n;
                if (if13 != null) {
                    if13.c(hg6);
                    if13.b((hg6<? super if1, cd6>) new xr0(if1, fq0, hg62));
                    if (!if13.t) {
                        if13.h.add(um0);
                    }
                    if13.a((hg6<? super if1, cd6>) no0);
                    if13.l();
                    return;
                }
                return;
            }
            if1.a(if1.v);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeSubPhase");
    }

    @DexIgnore
    public void a(sk1 sk1) {
        if (!this.t && !this.u) {
            cc0 cc0 = cc0.DEBUG;
            Object[] objArr = {this.z, cw0.a((Enum<?>) sk1)};
            this.o.removeCallbacksAndMessages((Object) null);
            this.u = true;
            this.v = km1.a(this.v, (eh1) null, sk1, (bn0) null, 5);
            qv0 qv0 = this.b;
            if (qv0 == null || qv0.t) {
                if1 if1 = this.n;
                if (if1 == null || if1.t) {
                    a(this.v);
                } else if (if1 != null) {
                    if1.a(sk1);
                }
            } else if (qv0 != null) {
                qv0.a(il0.INTERRUPTED);
            }
        }
    }

    @DexIgnore
    public final if1 a(hg6<? super if1, cd6> hg6) {
        if (!this.t) {
            this.g.add(hg6);
        } else {
            hg6.invoke(this);
        }
        return this;
    }
}
