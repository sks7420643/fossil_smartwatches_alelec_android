package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y94 extends x94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public long I;

    /*
    static {
        K.put(2131362082, 1);
        K.put(2131362542, 2);
        K.put(2131363218, 3);
        K.put(2131362056, 4);
        K.put(2131362543, 5);
        K.put(2131362321, 6);
        K.put(2131362657, 7);
        K.put(2131362320, 8);
        K.put(2131362653, 9);
        K.put(2131362318, 10);
        K.put(2131362316, 11);
        K.put(2131362609, 12);
        K.put(2131362122, 13);
        K.put(2131362022, 14);
        K.put(2131362058, 15);
        K.put(2131362780, 16);
        K.put(2131362407, 17);
        K.put(2131362357, 18);
        K.put(2131362149, 19);
        K.put(2131363264, 20);
        K.put(2131362062, 21);
        K.put(2131362063, 22);
        K.put(2131362649, 23);
        K.put(2131361881, 24);
        K.put(2131363188, 25);
        K.put(2131362887, 26);
    }
    */

    @DexIgnore
    public y94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 27, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public y94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[24], objArr[14], objArr[4], objArr[15], objArr[21], objArr[22], objArr[1], objArr[13], objArr[19], objArr[11], objArr[10], objArr[8], objArr[6], objArr[18], objArr[17], objArr[2], objArr[5], objArr[12], objArr[23], objArr[9], objArr[7], objArr[16], objArr[0], objArr[26], objArr[25], objArr[3], objArr[20]);
        this.I = -1;
        this.F.setTag((Object) null);
        a(view);
        f();
    }
}
