package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os3 {
    @DexIgnore
    public static File e(Context context, String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString(str.getBytes("UTF-8"), 11);
                StringBuilder sb = new StringBuilder(String.valueOf(encodeToString).length() + 33);
                sb.append("com.google.InstanceId_");
                sb.append(encodeToString);
                sb.append(".properties");
                str2 = sb.toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        return new File(b(context), str2);
    }

    @DexIgnore
    public final qs3 a(Context context, String str) throws rs3 {
        qs3 c = c(context, str);
        if (c != null) {
            return c;
        }
        return b(context, str);
    }

    @DexIgnore
    public final qs3 b(Context context, String str) {
        qs3 qs3 = new qs3(dr3.a(vr3.a().getPublic()), System.currentTimeMillis());
        qs3 a = a(context, str, qs3, true);
        if (a == null || a.equals(qs3)) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Generated new key");
            }
            a(context, str, qs3);
            return qs3;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Loaded key after generating new one, using loaded one");
        }
        return a;
    }

    @DexIgnore
    public final qs3 c(Context context, String str) throws rs3 {
        try {
            qs3 d = d(context, str);
            if (d != null) {
                a(context, str, d);
                return d;
            }
            e = null;
            try {
                qs3 a = a(context.getSharedPreferences("com.google.android.gms.appid", 0), str);
                if (a != null) {
                    a(context, str, a, false);
                    return a;
                }
            } catch (rs3 e) {
                e = e;
            }
            if (e == null) {
                return null;
            }
            throw e;
        } catch (rs3 e2) {
            e = e2;
        }
    }

    @DexIgnore
    public final qs3 d(Context context, String str) throws rs3 {
        File e = e(context, str);
        if (!e.exists()) {
            return null;
        }
        try {
            return a(e);
        } catch (rs3 | IOException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
                sb.append("Failed to read ID from file, retrying: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            try {
                return a(e);
            } catch (IOException e3) {
                String valueOf2 = String.valueOf(e3);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("IID file exists, but failed to read from it: ");
                sb2.append(valueOf2);
                Log.w("FirebaseInstanceId", sb2.toString());
                throw new rs3((Exception) e3);
            }
        }
    }

    @DexIgnore
    public static void a(Context context) {
        for (File file : b(context).listFiles()) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public static PublicKey a(String str) throws rs3 {
        try {
            try {
                return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 8)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 19);
                sb.append("Invalid key stored ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                throw new rs3((Exception) e);
            }
        } catch (IllegalArgumentException e2) {
            throw new rs3((Exception) e2);
        }
    }

    @DexIgnore
    public static File b(Context context) {
        File c = w6.c(context);
        if (c != null && c.isDirectory()) {
            return c;
        }
        Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
        return context.getFilesDir();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009b, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009c, code lost:
        if (r9 != null) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        a(r11, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a1, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a4, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        a(r9, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a8, code lost:
        throw r11;
     */
    @DexIgnore
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0095=Splitter:B:31:0x0095, B:19:0x0057=Splitter:B:19:0x0057} */
    public final qs3 a(Context context, String str, qs3 qs3, boolean z) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing ID to properties file");
        }
        Properties properties = new Properties();
        properties.setProperty("id", qs3.a());
        properties.setProperty("cre", String.valueOf(qs3.b));
        File e = e(context, str);
        try {
            e.createNewFile();
            RandomAccessFile randomAccessFile = new RandomAccessFile(e, "rw");
            FileChannel channel = randomAccessFile.getChannel();
            channel.lock();
            if (z && channel.size() > 0) {
                try {
                    channel.position(0);
                    qs3 a = a(channel);
                    if (channel != null) {
                        a((Throwable) null, channel);
                    }
                    a((Throwable) null, randomAccessFile);
                    return a;
                } catch (rs3 | IOException e2) {
                    if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        String valueOf = String.valueOf(e2);
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 58);
                        sb.append("Tried reading ID before writing new one, but failed with: ");
                        sb.append(valueOf);
                        Log.d("FirebaseInstanceId", sb.toString());
                    }
                }
            }
            channel.truncate(0);
            properties.store(Channels.newOutputStream(channel), (String) null);
            if (channel != null) {
                a((Throwable) null, channel);
            }
            a((Throwable) null, randomAccessFile);
            return qs3;
        } catch (IOException e3) {
            String valueOf2 = String.valueOf(e3);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 21);
            sb2.append("Failed to write key: ");
            sb2.append(valueOf2);
            Log.w("FirebaseInstanceId", sb2.toString());
            return null;
        }
    }

    @DexIgnore
    public static long b(SharedPreferences sharedPreferences, String str) {
        String string = sharedPreferences.getString(or3.a(str, "cre"), (String) null);
        if (string == null) {
            return 0;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        if (r8 != null) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        a(r1, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002f, code lost:
        a(r8, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0032, code lost:
        throw r1;
     */
    @DexIgnore
    public final qs3 a(File file) throws rs3, IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel channel = fileInputStream.getChannel();
        channel.lock(0, RecyclerView.FOREVER_NS, true);
        qs3 a = a(channel);
        if (channel != null) {
            a((Throwable) null, channel);
        }
        a((Throwable) null, fileInputStream);
        return a;
    }

    @DexIgnore
    public static qs3 a(FileChannel fileChannel) throws rs3, IOException {
        Properties properties = new Properties();
        properties.load(Channels.newInputStream(fileChannel));
        try {
            long parseLong = Long.parseLong(properties.getProperty("cre"));
            String property = properties.getProperty("id");
            if (property == null) {
                String property2 = properties.getProperty("pub");
                if (property2 != null) {
                    property = dr3.a(a(property2));
                } else {
                    throw new rs3("Invalid properties file");
                }
            }
            return new qs3(property, parseLong);
        } catch (NumberFormatException e) {
            throw new rs3((Exception) e);
        }
    }

    @DexIgnore
    public static qs3 a(SharedPreferences sharedPreferences, String str) throws rs3 {
        long b = b(sharedPreferences, str);
        String string = sharedPreferences.getString(or3.a(str, "id"), (String) null);
        if (string == null) {
            String string2 = sharedPreferences.getString(or3.a(str, "|P|"), (String) null);
            if (string2 == null) {
                return null;
            }
            string = dr3.a(a(string2));
        }
        return new qs3(string, b);
    }

    @DexIgnore
    public final void a(Context context, String str, qs3 qs3) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (qs3.equals(a(sharedPreferences, str))) {
                return;
            }
        } catch (rs3 unused) {
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing key to shared preferences");
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(or3.a(str, "id"), qs3.a());
        edit.putString(or3.a(str, "cre"), String.valueOf(qs3.b));
        edit.commit();
    }

    @DexIgnore
    public static /* synthetic */ void a(Throwable th, FileChannel fileChannel) {
        if (th != null) {
            try {
                fileChannel.close();
            } catch (Throwable th2) {
                rb2.a(th, th2);
            }
        } else {
            fileChannel.close();
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(Throwable th, RandomAccessFile randomAccessFile) {
        if (th != null) {
            try {
                randomAccessFile.close();
            } catch (Throwable th2) {
                rb2.a(th, th2);
            }
        } else {
            randomAccessFile.close();
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(Throwable th, FileInputStream fileInputStream) {
        if (th != null) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                rb2.a(th, th2);
            }
        } else {
            fileInputStream.close();
        }
    }
}
