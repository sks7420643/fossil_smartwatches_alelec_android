package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.google.android.material.button.MaterialButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rg3 {
    @DexIgnore
    public static /* final */ boolean s; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ MaterialButton a;
    @DexIgnore
    public hj3 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public PorterDuff.Mode i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public LayerDrawable r;

    @DexIgnore
    public rg3(MaterialButton materialButton, hj3 hj3) {
        this.a = materialButton;
        this.b = hj3;
    }

    @DexIgnore
    public void a(TypedArray typedArray) {
        this.c = typedArray.getDimensionPixelOffset(xf3.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(xf3.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(xf3.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(xf3.MaterialButton_android_insetBottom, 0);
        if (typedArray.hasValue(xf3.MaterialButton_cornerRadius)) {
            this.g = typedArray.getDimensionPixelSize(xf3.MaterialButton_cornerRadius, -1);
            a(this.b.a((float) this.g));
            this.p = true;
        }
        this.h = typedArray.getDimensionPixelSize(xf3.MaterialButton_strokeWidth, 0);
        this.i = li3.a(typedArray.getInt(xf3.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = pi3.a(this.a.getContext(), typedArray, xf3.MaterialButton_backgroundTint);
        this.k = pi3.a(this.a.getContext(), typedArray, xf3.MaterialButton_strokeColor);
        this.l = pi3.a(this.a.getContext(), typedArray, xf3.MaterialButton_rippleColor);
        this.q = typedArray.getBoolean(xf3.MaterialButton_android_checkable, false);
        int dimensionPixelSize = typedArray.getDimensionPixelSize(xf3.MaterialButton_elevation, 0);
        int t = x9.t(this.a);
        int paddingTop = this.a.getPaddingTop();
        int s2 = x9.s(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        this.a.setInternalBackground(a());
        dj3 d2 = d();
        if (d2 != null) {
            d2.b((float) dimensionPixelSize);
        }
        x9.b(this.a, t + this.c, paddingTop + this.e, s2 + this.d, paddingBottom + this.f);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            o();
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (d() != null) {
                o7.a((Drawable) d(), this.j);
            }
        }
    }

    @DexIgnore
    public dj3 d() {
        return a(false);
    }

    @DexIgnore
    public ColorStateList e() {
        return this.l;
    }

    @DexIgnore
    public hj3 f() {
        return this.b;
    }

    @DexIgnore
    public ColorStateList g() {
        return this.k;
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public ColorStateList i() {
        return this.j;
    }

    @DexIgnore
    public PorterDuff.Mode j() {
        return this.i;
    }

    @DexIgnore
    public final dj3 k() {
        return a(true);
    }

    @DexIgnore
    public boolean l() {
        return this.o;
    }

    @DexIgnore
    public boolean m() {
        return this.q;
    }

    @DexIgnore
    public void n() {
        this.o = true;
        this.a.setSupportBackgroundTintList(this.j);
        this.a.setSupportBackgroundTintMode(this.i);
    }

    @DexIgnore
    public final void o() {
        dj3 d2 = d();
        dj3 k2 = k();
        if (d2 != null) {
            d2.a((float) this.h, this.k);
            if (k2 != null) {
                k2.a((float) this.h, this.n ? yg3.a((View) this.a, nf3.colorSurface) : 0);
            }
        }
    }

    @DexIgnore
    public void b(int i2) {
        if (!this.p || this.g != i2) {
            this.g = i2;
            this.p = true;
            a(this.b.a((float) i2));
        }
    }

    @DexIgnore
    public void c(boolean z) {
        this.n = z;
        o();
    }

    @DexIgnore
    public void c(int i2) {
        if (this.h != i2) {
            this.h = i2;
            o();
        }
    }

    @DexIgnore
    public int b() {
        return this.g;
    }

    @DexIgnore
    public void b(boolean z) {
        this.q = z;
    }

    @DexIgnore
    public final void b(hj3 hj3) {
        if (d() != null) {
            d().setShapeAppearanceModel(hj3);
        }
        if (k() != null) {
            k().setShapeAppearanceModel(hj3);
        }
        if (c() != null) {
            c().setShapeAppearanceModel(hj3);
        }
    }

    @DexIgnore
    public kj3 c() {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        if (this.r.getNumberOfLayers() > 2) {
            return (kj3) this.r.getDrawable(2);
        }
        return (kj3) this.r.getDrawable(1);
    }

    @DexIgnore
    public final InsetDrawable a(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (d() != null && this.i != null) {
                o7.a((Drawable) d(), this.i);
            }
        }
    }

    @DexIgnore
    public final Drawable a() {
        dj3 dj3 = new dj3(this.b);
        dj3.a(this.a.getContext());
        o7.a((Drawable) dj3, this.j);
        PorterDuff.Mode mode = this.i;
        if (mode != null) {
            o7.a((Drawable) dj3, mode);
        }
        dj3.a((float) this.h, this.k);
        dj3 dj32 = new dj3(this.b);
        dj32.setTint(0);
        dj32.a((float) this.h, this.n ? yg3.a((View) this.a, nf3.colorSurface) : 0);
        if (s) {
            this.m = new dj3(this.b);
            o7.b(this.m, -1);
            this.r = new RippleDrawable(ui3.b(this.l), a((Drawable) new LayerDrawable(new Drawable[]{dj32, dj3})), this.m);
            return this.r;
        }
        this.m = new ti3(this.b);
        o7.a(this.m, ui3.b(this.l));
        this.r = new LayerDrawable(new Drawable[]{dj32, dj3, this.m});
        return a((Drawable) this.r);
    }

    @DexIgnore
    public void a(int i2, int i3) {
        Drawable drawable = this.m;
        if (drawable != null) {
            drawable.setBounds(this.c, this.e, i3 - this.d, i2 - this.f);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (d() != null) {
            d().setTint(i2);
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            if (s && (this.a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.a.getBackground()).setColor(ui3.b(colorStateList));
            } else if (!s && (this.a.getBackground() instanceof ti3)) {
                ((ti3) this.a.getBackground()).setTintList(ui3.b(colorStateList));
            }
        }
    }

    @DexIgnore
    public final dj3 a(boolean z) {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        if (s) {
            return (dj3) ((LayerDrawable) ((InsetDrawable) this.r.getDrawable(0)).getDrawable()).getDrawable(z ^ true ? 1 : 0);
        }
        return (dj3) this.r.getDrawable(z ^ true ? 1 : 0);
    }

    @DexIgnore
    public void a(hj3 hj3) {
        this.b = hj3;
        b(hj3);
    }
}
