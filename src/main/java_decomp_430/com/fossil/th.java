package com.fossil;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class th implements ji {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ ji e;
    @DexIgnore
    public fh f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public th(Context context, String str, File file, int i, ji jiVar) {
        this.a = context;
        this.b = str;
        this.c = file;
        this.d = i;
        this.e = jiVar;
    }

    @DexIgnore
    public void a(boolean z) {
        this.e.a(z);
    }

    @DexIgnore
    public final void b() {
        String databaseName = getDatabaseName();
        File databasePath = this.a.getDatabasePath(databaseName);
        fh fhVar = this.f;
        zh zhVar = new zh(databaseName, this.a.getFilesDir(), fhVar == null || fhVar.j);
        try {
            zhVar.a();
            if (!databasePath.exists()) {
                a(databasePath);
                zhVar.b();
            } else if (this.f == null) {
                zhVar.b();
            } else {
                try {
                    int a2 = bi.a(databasePath);
                    if (a2 == this.d) {
                        zhVar.b();
                    } else if (this.f.a(a2, this.d)) {
                        zhVar.b();
                    } else {
                        if (this.a.deleteDatabase(databaseName)) {
                            try {
                                a(databasePath);
                            } catch (IOException e2) {
                                Log.w("ROOM", "Unable to copy database file.", e2);
                            }
                        } else {
                            Log.w("ROOM", "Failed to delete database file (" + databaseName + ") for a copy destructive migration.");
                        }
                        zhVar.b();
                    }
                } catch (IOException e3) {
                    Log.w("ROOM", "Unable to read database version.", e3);
                    zhVar.b();
                }
            }
        } catch (IOException e4) {
            throw new RuntimeException("Unable to copy database file.", e4);
        } catch (Throwable th) {
            zhVar.b();
            throw th;
        }
    }

    @DexIgnore
    public synchronized void close() {
        this.e.close();
        this.g = false;
    }

    @DexIgnore
    public String getDatabaseName() {
        return this.e.getDatabaseName();
    }

    @DexIgnore
    public synchronized ii a() {
        if (!this.g) {
            b();
            this.g = true;
        }
        return this.e.a();
    }

    @DexIgnore
    public void a(fh fhVar) {
        this.f = fhVar;
    }

    @DexIgnore
    public final void a(File file) throws IOException {
        ReadableByteChannel readableByteChannel;
        if (this.b != null) {
            readableByteChannel = Channels.newChannel(this.a.getAssets().open(this.b));
        } else {
            File file2 = this.c;
            if (file2 != null) {
                readableByteChannel = new FileInputStream(file2).getChannel();
            } else {
                throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
            }
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.a.getCacheDir());
        createTempFile.deleteOnExit();
        ci.a(readableByteChannel, new FileOutputStream(createTempFile).getChannel());
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Failed to create directories for " + file.getAbsolutePath());
        } else if (!createTempFile.renameTo(file)) {
            throw new IOException("Failed to move intermediate file (" + createTempFile.getAbsolutePath() + ") to destination (" + file.getAbsolutePath() + ").");
        }
    }
}
