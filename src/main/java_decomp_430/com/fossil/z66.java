package com.fossil;

import android.os.Build;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z66 {
    @DexIgnore
    public static /* final */ String a; // = "z66";
    @DexIgnore
    public static /* final */ List<String> b; // = Arrays.asList(new String[]{"he", "yi", "id"});

    @DexIgnore
    public static String a(Locale locale) {
        if (locale == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage());
        if (a76.b(locale.getCountry())) {
            sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
            sb.append(locale.getCountry().toLowerCase(Locale.US));
        }
        return sb.toString();
    }

    @DexIgnore
    public static Locale b(String str, String str2) {
        try {
            Method declaredMethod = Locale.class.getDeclaredMethod("createConstant", new Class[]{String.class, String.class});
            declaredMethod.setAccessible(true);
            return (Locale) declaredMethod.invoke((Object) null, new Object[]{str, str2});
        } catch (Exception e) {
            t66.b(a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    @DexIgnore
    public static Locale a(String str) {
        Locale locale;
        t66.a(a, "Assuming Locale.getDefault()", new Object[0]);
        Locale locale2 = Locale.getDefault();
        if (!a76.b(str)) {
            return locale2;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        int countTokens = stringTokenizer.countTokens();
        int i = 2;
        if (countTokens == 1 || countTokens == 2) {
            if (countTokens != 1) {
                i = 5;
            }
            if (i != str.length()) {
                t66.a(a, "number of tokens is correct but the length of the locale string does not match the expected length", new Object[0]);
                return locale2;
            }
            String nextToken = stringTokenizer.nextToken();
            String upperCase = (stringTokenizer.hasMoreTokens() ? stringTokenizer.nextToken() : "").toUpperCase(Locale.US);
            if (b.contains(nextToken)) {
                t66.a(a, "New ISO-6390-Alpha3 locale detected trying to create new locale per reflection", new Object[0]);
                locale = b(nextToken, upperCase);
                if (locale == null) {
                    locale = a(nextToken, upperCase);
                }
                if (locale == null) {
                    locale = new Locale(nextToken, upperCase);
                }
            } else {
                locale = new Locale(nextToken, upperCase);
            }
            return locale;
        }
        t66.d(a, "Unexpected number of tokens, must be at least one and at most two", new Object[0]);
        return locale2;
    }

    @DexIgnore
    public static Locale a(String str, String str2) {
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                Constructor<Locale> declaredConstructor = Locale.class.getDeclaredConstructor(new Class[]{Boolean.TYPE, String.class, String.class});
                declaredConstructor.setAccessible(true);
                return declaredConstructor.newInstance(new Object[]{true, str, str2});
            }
            Constructor<Locale> declaredConstructor2 = Locale.class.getDeclaredConstructor(new Class[0]);
            declaredConstructor2.setAccessible(true);
            Locale newInstance = declaredConstructor2.newInstance(new Object[0]);
            Class<?> cls = newInstance.getClass();
            Field declaredField = cls.getDeclaredField("languageCode");
            declaredField.setAccessible(true);
            declaredField.set(newInstance, str);
            Field declaredField2 = cls.getDeclaredField("countryCode");
            declaredField2.setAccessible(true);
            declaredField2.set(newInstance, str2);
            return newInstance;
        } catch (Exception e) {
            t66.b(a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }
}
