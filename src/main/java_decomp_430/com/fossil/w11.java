package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w11 extends qg1 {
    @DexIgnore
    public byte[] l; // = new byte[0];

    @DexIgnore
    public w11(rg1 rg1, at0 at0) {
        super(hm0.READ_CHARACTERISTIC, rg1, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.a(this.k);
        this.j = true;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return (p51 instanceof m71) && ((m71) p51).b == this.k;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.d;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.l = ((m71) p51).c;
    }
}
