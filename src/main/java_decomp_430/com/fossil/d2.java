package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d2 extends z1 implements SubMenu {
    @DexIgnore
    public /* final */ w7 e;

    @DexIgnore
    public d2(Context context, w7 w7Var) {
        super(context, w7Var);
        this.e = w7Var;
    }

    @DexIgnore
    public void clearHeader() {
        this.e.clearHeader();
    }

    @DexIgnore
    public MenuItem getItem() {
        return a(this.e.getItem());
    }

    @DexIgnore
    public SubMenu setHeaderIcon(int i) {
        this.e.setHeaderIcon(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(int i) {
        this.e.setHeaderTitle(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        this.e.setHeaderView(view);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(int i) {
        this.e.setIcon(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderIcon(Drawable drawable) {
        this.e.setHeaderIcon(drawable);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        this.e.setHeaderTitle(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(Drawable drawable) {
        this.e.setIcon(drawable);
        return this;
    }
}
