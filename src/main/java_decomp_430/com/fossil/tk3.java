package com.fossil;

import com.fossil.dn3;
import com.fossil.en3;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tk3<E> extends wk3<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -2250766705698539974L;
    @DexIgnore
    public transient Map<E, fl3> c;
    @DexIgnore
    public transient long d; // = ((long) super.size());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Iterator<dn3.a<E>> {
        @DexIgnore
        public Map.Entry<E, fl3> a;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tk3$a$a")
        /* renamed from: com.fossil.tk3$a$a  reason: collision with other inner class name */
        public class C0046a extends en3.b<E> {
            @DexIgnore
            public /* final */ /* synthetic */ Map.Entry a;

            @DexIgnore
            public C0046a(Map.Entry entry) {
                this.a = entry;
            }

            @DexIgnore
            public int getCount() {
                fl3 fl3;
                fl3 fl32 = (fl3) this.a.getValue();
                if ((fl32 == null || fl32.get() == 0) && (fl3 = (fl3) tk3.this.c.get(getElement())) != null) {
                    return fl3.get();
                }
                if (fl32 == null) {
                    return 0;
                }
                return fl32.get();
            }

            @DexIgnore
            public E getElement() {
                return this.a.getKey();
            }
        }

        @DexIgnore
        public a(Iterator it) {
            this.b = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        public void remove() {
            bl3.a(this.a != null);
            tk3.access$122(tk3.this, (long) this.a.getValue().getAndSet(0));
            this.b.remove();
            this.a = null;
        }

        @DexIgnore
        public dn3.a<E> next() {
            Map.Entry<E, fl3> entry = (Map.Entry) this.b.next();
            this.a = entry;
            return new C0046a(entry);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<E, fl3>> a;
        @DexIgnore
        public Map.Entry<E, fl3> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b() {
            this.a = tk3.this.c.entrySet().iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c > 0 || this.a.hasNext();
        }

        @DexIgnore
        public E next() {
            if (this.c == 0) {
                this.b = this.a.next();
                this.c = this.b.getValue().get();
            }
            this.c--;
            this.d = true;
            return this.b.getKey();
        }

        @DexIgnore
        public void remove() {
            bl3.a(this.d);
            if (this.b.getValue().get() > 0) {
                if (this.b.getValue().addAndGet(-1) == 0) {
                    this.a.remove();
                }
                tk3.access$110(tk3.this);
                this.d = false;
                return;
            }
            throw new ConcurrentModificationException();
        }
    }

    @DexIgnore
    public tk3(Map<E, fl3> map) {
        jk3.a(map);
        this.c = map;
    }

    @DexIgnore
    public static int a(fl3 fl3, int i) {
        if (fl3 == null) {
            return 0;
        }
        return fl3.getAndSet(i);
    }

    @DexIgnore
    public static /* synthetic */ long access$110(tk3 tk3) {
        long j = tk3.d;
        tk3.d = j - 1;
        return j;
    }

    @DexIgnore
    public static /* synthetic */ long access$122(tk3 tk3, long j) {
        long j2 = tk3.d - j;
        tk3.d = j2;
        return j2;
    }

    @DexIgnore
    public int add(E e, int i) {
        int i2;
        if (i == 0) {
            return count(e);
        }
        boolean z = true;
        jk3.a(i > 0, "occurrences cannot be negative: %s", i);
        fl3 fl3 = this.c.get(e);
        if (fl3 == null) {
            this.c.put(e, new fl3(i));
            i2 = 0;
        } else {
            i2 = fl3.get();
            long j = ((long) i2) + ((long) i);
            if (j > 2147483647L) {
                z = false;
            }
            jk3.a(z, "too many occurrences: %s", j);
            fl3.add(i);
        }
        this.d += (long) i;
        return i2;
    }

    @DexIgnore
    public void clear() {
        for (fl3 fl3 : this.c.values()) {
            fl3.set(0);
        }
        this.c.clear();
        this.d = 0;
    }

    @DexIgnore
    public int count(Object obj) {
        fl3 fl3 = (fl3) ym3.e(this.c, obj);
        if (fl3 == null) {
            return 0;
        }
        return fl3.get();
    }

    @DexIgnore
    public int distinctElements() {
        return this.c.size();
    }

    @DexIgnore
    public Iterator<dn3.a<E>> entryIterator() {
        return new a(this.c.entrySet().iterator());
    }

    @DexIgnore
    public Set<dn3.a<E>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    public int remove(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        jk3.a(i > 0, "occurrences cannot be negative: %s", i);
        fl3 fl3 = this.c.get(obj);
        if (fl3 == null) {
            return 0;
        }
        int i2 = fl3.get();
        if (i2 <= i) {
            this.c.remove(obj);
            i = i2;
        }
        fl3.add(-i);
        this.d -= (long) i;
        return i2;
    }

    @DexIgnore
    public void setBackingMap(Map<E, fl3> map) {
        this.c = map;
    }

    @DexIgnore
    public int setCount(E e, int i) {
        int i2;
        bl3.a(i, "count");
        if (i == 0) {
            i2 = a(this.c.remove(e), i);
        } else {
            fl3 fl3 = this.c.get(e);
            int a2 = a(fl3, i);
            if (fl3 == null) {
                this.c.put(e, new fl3(i));
            }
            i2 = a2;
        }
        this.d += (long) (i - i2);
        return i2;
    }

    @DexIgnore
    public int size() {
        return xo3.a(this.d);
    }
}
