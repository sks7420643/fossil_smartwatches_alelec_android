package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class up3 implements zp3 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public up3(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static zp3 a(Object obj) {
        return new up3(obj);
    }

    @DexIgnore
    public Object a(xp3 xp3) {
        Object obj = this.a;
        wp3.b(obj, xp3);
        return obj;
    }
}
