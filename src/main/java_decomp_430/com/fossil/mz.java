package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mz<R> {
    @DexIgnore
    boolean a(mt mtVar, Object obj, yz<R> yzVar, boolean z);

    @DexIgnore
    boolean a(R r, Object obj, yz<R> yzVar, pr prVar, boolean z);
}
