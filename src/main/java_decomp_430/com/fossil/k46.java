package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k46 {
    @DexIgnore
    public static volatile long f;
    @DexIgnore
    public v36 a;
    @DexIgnore
    public o36 b; // = null;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public Context d; // = null;
    @DexIgnore
    public long e; // = System.currentTimeMillis();

    @DexIgnore
    public k46(v36 v36) {
        this.a = v36;
        this.b = n36.o();
        this.c = v36.e();
        this.d = v36.d();
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            if (n36.J > 0 && this.e >= f) {
                q36.f(this.d);
                f = this.e + n36.K;
                if (n36.q()) {
                    b56 f2 = q36.m;
                    f2.e("nextFlushTime=" + f);
                }
            }
            if (b46.a(this.d).f()) {
                if (n36.q()) {
                    b56 f3 = q36.m;
                    f3.e("sendFailedCount=" + q36.p);
                }
                if (!q36.a()) {
                    b();
                    return;
                }
                o46.b(this.d).a(this.a, (x56) null, this.c, false);
                if (this.e - q36.q > 1800000) {
                    q36.d(this.d);
                    return;
                }
                return;
            }
            o46.b(this.d).a(this.a, (x56) null, this.c, false);
        }
    }

    @DexIgnore
    public final void a(x56 x56) {
        y56.b(q36.r).a(this.a, x56);
    }

    @DexIgnore
    public final void b() {
        if (this.a.c() != null && this.a.c().e()) {
            this.b = o36.INSTANT;
        }
        if (n36.z && b46.a(q36.r).e()) {
            this.b = o36.INSTANT;
        }
        if (n36.q()) {
            b56 f2 = q36.m;
            f2.e("strategy=" + this.b.name());
        }
        switch (e46.a[this.b.ordinal()]) {
            case 1:
                c();
                return;
            case 2:
                o46.b(this.d).a(this.a, (x56) null, this.c, false);
                if (n36.q()) {
                    b56 f3 = q36.m;
                    f3.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + q36.s + ",difftime=" + (q36.s - this.e));
                }
                if (q36.s == 0) {
                    q36.s = q56.a(this.d, "last_period_ts", 0);
                    if (this.e > q36.s) {
                        q36.e(this.d);
                    }
                    long l = this.e + ((long) (n36.l() * 60 * 1000));
                    if (q36.s > l) {
                        q36.s = l;
                    }
                    t56.a(this.d).a();
                }
                if (n36.q()) {
                    b56 f4 = q36.m;
                    f4.e("PERIOD currTime=" + this.e + ",nextPeriodSendTs=" + q36.s + ",difftime=" + (q36.s - this.e));
                }
                if (this.e > q36.s) {
                    q36.e(this.d);
                    return;
                }
                return;
            case 3:
            case 4:
                o46.b(this.d).a(this.a, (x56) null, this.c, false);
                return;
            case 5:
                o46.b(this.d).a(this.a, (x56) new l46(this), this.c, true);
                return;
            case 6:
                if (b46.a(q36.r).c() == 1) {
                    c();
                    return;
                } else {
                    o46.b(this.d).a(this.a, (x56) null, this.c, false);
                    return;
                }
            case 7:
                if (m56.h(this.d)) {
                    a((x56) new m46(this));
                    return;
                }
                return;
            default:
                b56 f5 = q36.m;
                f5.d("Invalid stat strategy:" + n36.o());
                return;
        }
    }

    @DexIgnore
    public final void c() {
        if (o46.i().f <= 0 || !n36.I) {
            a((x56) new n46(this));
            return;
        }
        o46.i().a(this.a, (x56) null, this.c, true);
        o46.i().a(-1);
    }

    @DexIgnore
    public final boolean d() {
        if (n36.w <= 0) {
            return false;
        }
        if (this.e > q36.d) {
            q36.c.clear();
            long unused = q36.d = this.e + n36.x;
            if (n36.q()) {
                b56 f2 = q36.m;
                f2.e("clear methodsCalledLimitMap, nextLimitCallClearTime=" + q36.d);
            }
        }
        Integer valueOf = Integer.valueOf(this.a.a().a());
        Integer num = (Integer) q36.c.get(valueOf);
        if (num != null) {
            q36.c.put(valueOf, Integer.valueOf(num.intValue() + 1));
            if (num.intValue() <= n36.w) {
                return false;
            }
            if (n36.q()) {
                b56 f3 = q36.m;
                f3.c("event " + this.a.f() + " was discard, cause of called limit, current:" + num + ", limit:" + n36.w + ", period:" + n36.x + " ms");
            }
            return true;
        }
        q36.c.put(valueOf, 1);
        return false;
    }
}
