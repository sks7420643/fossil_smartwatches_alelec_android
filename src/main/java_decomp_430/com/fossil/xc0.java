package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ w40 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(aa0.class.getClassLoader());
            if (readParcelable != null) {
                wg6.a(readParcelable, "parcel.readParcelable<Ri\u2026class.java.classLoader)!!");
                aa0 aa0 = (aa0) readParcelable;
                uc0 uc0 = (uc0) parcel.readParcelable(uc0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(w40.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new xc0(aa0, uc0, (w40) readParcelable2);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new xc0[i];
        }
    }

    @DexIgnore
    public xc0(aa0 aa0, w40 w40) {
        super(aa0, (uc0) null);
        this.c = w40;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        try {
            kk1 kk1 = kk1.d;
            x90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return kk1.a(s, w40, new z81(((z90) deviceRequest).d(), new w40(this.c.getMajor(), this.c.getMinor())).getData());
            }
            throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sw0 e) {
            qs0.h.a(e);
            return new byte[0];
        }
    }

    @DexIgnore
    public JSONObject b() {
        return cw0.a(super.b(), bm0.MICRO_APP_VERSION, (Object) this.c.toString());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(xc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            return !(wg6.a(this.c, ((xc0) obj).c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData");
    }

    @DexIgnore
    public final w40 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public xc0(aa0 aa0, uc0 uc0, w40 w40) {
        super(aa0, uc0);
        this.c = w40;
    }
}
