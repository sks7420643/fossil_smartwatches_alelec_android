package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.security.KeyPair;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw5 extends m24<b, d, c> {
    @DexIgnore
    public /* final */ an4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "aliasName");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            wg6.b(str, "errorMessage");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wg6.b(str, "decryptedValue");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public kw5(an4 an4) {
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = an4;
    }

    @DexIgnore
    public String c() {
        return "DecryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v0, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.kw5] */
    public Object a(b bVar, xe6<Object> xe6) {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                an4 an4 = this.d;
                if (bVar != null) {
                    String b2 = an4.b(bVar.a());
                    String c2 = this.d.c(bVar.a());
                    KeyStore.SecretKeyEntry e = qx5.b.e(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(2, e.getSecretKey(), new GCMParameterSpec(Logger.DEFAULT_FULL_MESSAGE_LENGTH, Base64.decode(b2, 0)));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DecryptValueKeyStoreUseCase", "Start decrypt with iv " + b2 + " encryptedValue " + c2);
                    byte[] doFinal = instance.doFinal(Base64.decode(c2, 0));
                    wg6.a((Object) doFinal, "decryptedByteArray");
                    String str = new String(doFinal, ej6.a);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str);
                    a(new d(str));
                    return new Object();
                }
                wg6.a();
                throw null;
            }
            an4 an42 = this.d;
            if (bVar != null) {
                String c3 = an42.c(bVar.a());
                KeyPair c4 = qx5.b.c(bVar.a());
                if (c4 == null) {
                    c4 = qx5.b.a(bVar.a());
                }
                Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance2.init(2, c4.getPrivate());
                byte[] doFinal2 = instance2.doFinal(Base64.decode(c3, 0));
                wg6.a((Object) doFinal2, "decryptedByte");
                String str2 = new String(doFinal2, ej6.a);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DecryptValueKeyStoreUseCase", "Decrypted success " + str2);
                a(new d(str2));
                return new Object();
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("DecryptValueKeyStoreUseCase", "Exception when decrypt value " + e2);
            a(new c(600, ""));
        }
    }
}
