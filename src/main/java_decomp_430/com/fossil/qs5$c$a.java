package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$deviceName$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
public final class qs5$c$a extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qs5$c$a(PairingPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qs5$c$a qs5_c_a = new qs5$c$a(this.this$0, xe6);
        qs5_c_a.p$ = (il6) obj;
        return qs5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qs5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            DeviceRepository c = this.this$0.this$0.u;
            String serial = this.this$0.$shineDevice.getSerial();
            wg6.a((Object) serial, "shineDevice.serial");
            return c.getDeviceNameBySerial(serial);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
