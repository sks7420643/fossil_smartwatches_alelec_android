package com.fossil;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.kv4;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lv4<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, kv4.a {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public boolean a;
    @DexIgnore
    public Cursor b;
    @DexIgnore
    public lv4<VH>.a c;
    @DexIgnore
    public DataSetObserver d;
    @DexIgnore
    public kv4 e;
    @DexIgnore
    public FilterQueryProvider f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            lv4.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends DataSetObserver {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onChanged() {
            lv4.this.a = true;
            lv4.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            lv4.this.a = false;
            lv4 lv4 = lv4.this;
            lv4.notifyItemRangeRemoved(0, lv4.getItemCount());
        }
    }

    /*
    static {
        new b((qg6) null);
        String simpleName = lv4.class.getSimpleName();
        wg6.a((Object) simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public lv4(Cursor cursor) {
        boolean z = cursor != null;
        this.b = cursor;
        this.a = z;
        this.c = new a();
        this.d = new c();
        if (z) {
            lv4<VH>.a aVar = this.c;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.d;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public abstract void a(VH vh, Cursor cursor, int i);

    @DexIgnore
    public Cursor b() {
        return this.b;
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (wg6.a((Object) cursor, (Object) this.b)) {
            return null;
        }
        Cursor cursor2 = this.b;
        if (cursor2 != null) {
            lv4<VH>.a aVar = this.c;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.d;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.b = cursor;
        if (cursor != null) {
            lv4<VH>.a aVar2 = this.c;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.d;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.a = true;
            notifyDataSetChanged();
        } else {
            this.a = false;
            notifyItemRangeRemoved(0, getItemCount());
        }
        return cursor2;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.e == null) {
            this.e = new kv4(this);
        }
        kv4 kv4 = this.e;
        if (kv4 != null) {
            return kv4;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public int getItemCount() {
        Cursor cursor;
        if (!this.a || (cursor = this.b) == null) {
            return 0;
        }
        if (cursor != null) {
            return cursor.getCount();
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void onBindViewHolder(VH vh, int i) {
        wg6.b(vh, "holder");
        if (!this.a) {
            FLogger.INSTANCE.getLocal().d(g, ".Inside onBindViewHolder the cursor is invalid");
        }
        a(vh, this.b, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r1 = r1.toString();
     */
    @DexIgnore
    public CharSequence b(Cursor cursor) {
        String obj;
        return (cursor == null || obj == null) ? "" : obj;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        wg6.b(cursor, "cursor");
        Cursor c2 = c(cursor);
        if (c2 != null) {
            c2.close();
        }
    }

    @DexIgnore
    public Cursor a(CharSequence charSequence) {
        wg6.b(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.f;
        if (filterQueryProvider == null) {
            return this.b;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(FilterQueryProvider filterQueryProvider) {
        wg6.b(filterQueryProvider, "filterQueryProvider");
        this.f = filterQueryProvider;
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(g, ".Inside onContentChanged");
    }
}
