package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv2 extends sh2 implements eu2 {
    @DexIgnore
    public fv2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeLong(j);
        b(23, q);
    }

    @DexIgnore
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (Parcelable) bundle);
        b(9, q);
    }

    @DexIgnore
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeLong(j);
        b(24, q);
    }

    @DexIgnore
    public final void generateEventId(ev2 ev2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) ev2);
        b(22, q);
    }

    @DexIgnore
    public final void getCachedAppInstanceId(ev2 ev2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) ev2);
        b(19, q);
    }

    @DexIgnore
    public final void getConditionalUserProperties(String str, String str2, ev2 ev2) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (IInterface) ev2);
        b(10, q);
    }

    @DexIgnore
    public final void getCurrentScreenClass(ev2 ev2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) ev2);
        b(17, q);
    }

    @DexIgnore
    public final void getCurrentScreenName(ev2 ev2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) ev2);
        b(16, q);
    }

    @DexIgnore
    public final void getGmpAppId(ev2 ev2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) ev2);
        b(21, q);
    }

    @DexIgnore
    public final void getMaxUserProperties(String str, ev2 ev2) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        li2.a(q, (IInterface) ev2);
        b(6, q);
    }

    @DexIgnore
    public final void getUserProperties(String str, String str2, boolean z, ev2 ev2) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, z);
        li2.a(q, (IInterface) ev2);
        b(5, q);
    }

    @DexIgnore
    public final void initialize(x52 x52, mv2 mv2, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        li2.a(q, (Parcelable) mv2);
        q.writeLong(j);
        b(1, q);
    }

    @DexIgnore
    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (Parcelable) bundle);
        li2.a(q, z);
        li2.a(q, z2);
        q.writeLong(j);
        b(2, q);
    }

    @DexIgnore
    public final void logHealthData(int i, String str, x52 x52, x52 x522, x52 x523) throws RemoteException {
        Parcel q = q();
        q.writeInt(i);
        q.writeString(str);
        li2.a(q, (IInterface) x52);
        li2.a(q, (IInterface) x522);
        li2.a(q, (IInterface) x523);
        b(33, q);
    }

    @DexIgnore
    public final void onActivityCreated(x52 x52, Bundle bundle, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        li2.a(q, (Parcelable) bundle);
        q.writeLong(j);
        b(27, q);
    }

    @DexIgnore
    public final void onActivityDestroyed(x52 x52, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeLong(j);
        b(28, q);
    }

    @DexIgnore
    public final void onActivityPaused(x52 x52, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeLong(j);
        b(29, q);
    }

    @DexIgnore
    public final void onActivityResumed(x52 x52, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeLong(j);
        b(30, q);
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(x52 x52, ev2 ev2, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        li2.a(q, (IInterface) ev2);
        q.writeLong(j);
        b(31, q);
    }

    @DexIgnore
    public final void onActivityStarted(x52 x52, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeLong(j);
        b(25, q);
    }

    @DexIgnore
    public final void onActivityStopped(x52 x52, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeLong(j);
        b(26, q);
    }

    @DexIgnore
    public final void registerOnMeasurementEventListener(jv2 jv2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) jv2);
        b(35, q);
    }

    @DexIgnore
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) bundle);
        q.writeLong(j);
        b(8, q);
    }

    @DexIgnore
    public final void setCurrentScreen(x52 x52, String str, String str2, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, (IInterface) x52);
        q.writeString(str);
        q.writeString(str2);
        q.writeLong(j);
        b(15, q);
    }

    @DexIgnore
    public final void setDataCollectionEnabled(boolean z) throws RemoteException {
        Parcel q = q();
        li2.a(q, z);
        b(39, q);
    }

    @DexIgnore
    public final void setMeasurementEnabled(boolean z, long j) throws RemoteException {
        Parcel q = q();
        li2.a(q, z);
        q.writeLong(j);
        b(11, q);
    }

    @DexIgnore
    public final void setUserId(String str, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeLong(j);
        b(7, q);
    }

    @DexIgnore
    public final void setUserProperty(String str, String str2, x52 x52, boolean z, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (IInterface) x52);
        li2.a(q, z);
        q.writeLong(j);
        b(4, q);
    }
}
