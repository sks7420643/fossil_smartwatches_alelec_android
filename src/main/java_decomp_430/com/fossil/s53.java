package com.fossil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.fj2;
import com.fossil.gj2;
import com.fossil.vi2;
import com.fossil.wi2;
import com.fossil.xi2;
import com.fossil.zi2;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s53 extends aa3 implements eb3 {
    @DexIgnore
    public static int j; // = 65535;
    @DexIgnore
    public static int k; // = 2;
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> d; // = new p4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> e; // = new p4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> f; // = new p4();
    @DexIgnore
    public /* final */ Map<String, gj2> g; // = new p4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Integer>> h; // = new p4();
    @DexIgnore
    public /* final */ Map<String, String> i; // = new p4();

    @DexIgnore
    public s53(ea3 ea3) {
        super(ea3);
    }

    @DexIgnore
    public final gj2 a(String str) {
        r();
        g();
        w12.b(str);
        i(str);
        return this.g.get(str);
    }

    @DexIgnore
    public final String b(String str) {
        g();
        return this.i.get(str);
    }

    @DexIgnore
    public final void c(String str) {
        g();
        this.i.put(str, (Object) null);
    }

    @DexIgnore
    public final void d(String str) {
        g();
        this.g.remove(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        g();
        gj2 a = a(str);
        if (a == null) {
            return false;
        }
        return a.v();
    }

    @DexIgnore
    public final long f(String str) {
        String a = a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a)) {
            return 0;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e2) {
            b().w().a("Unable to parse timezone offset. appId", t43.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean g(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    @DexIgnore
    public final boolean h(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    @DexIgnore
    public final void i(String str) {
        r();
        g();
        w12.b(str);
        if (this.g.get(str) == null) {
            byte[] d2 = o().d(str);
            if (d2 == null) {
                this.d.put(str, (Object) null);
                this.e.put(str, (Object) null);
                this.f.put(str, (Object) null);
                this.g.put(str, (Object) null);
                this.i.put(str, (Object) null);
                this.h.put(str, (Object) null);
                return;
            }
            gj2.a aVar = (gj2.a) a(str, d2).j();
            a(str, aVar);
            this.d.put(str, a((gj2) aVar.i()));
            this.g.put(str, (gj2) aVar.i());
            this.i.put(str, (Object) null);
        }
    }

    @DexIgnore
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        Boolean bool;
        g();
        i(str);
        if (g(str) && ma3.f(str2)) {
            return true;
        }
        if (h(str) && ma3.e(str2)) {
            return true;
        }
        Map map = this.e.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final boolean c(String str, String str2) {
        Boolean bool;
        g();
        i(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final int d(String str, String str2) {
        Integer num;
        g();
        i(str);
        Map map = this.h.get(str);
        if (map == null || (num = (Integer) map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    @DexIgnore
    public final String a(String str, String str2) {
        g();
        i(str);
        Map map = this.d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public static Map<String, String> a(gj2 gj2) {
        p4 p4Var = new p4();
        if (gj2 != null) {
            for (hj2 next : gj2.r()) {
                p4Var.put(next.n(), next.o());
            }
        }
        return p4Var;
    }

    @DexIgnore
    public final void a(String str, gj2.a aVar) {
        p4 p4Var = new p4();
        p4 p4Var2 = new p4();
        p4 p4Var3 = new p4();
        if (aVar != null) {
            for (int i2 = 0; i2 < aVar.j(); i2++) {
                fj2.a aVar2 = (fj2.a) aVar.a(i2).j();
                if (TextUtils.isEmpty(aVar2.j())) {
                    b().w().a("EventConfig contained null event name");
                } else {
                    String a = x63.a(aVar2.j());
                    if (!TextUtils.isEmpty(a)) {
                        aVar2.a(a);
                        aVar.a(i2, aVar2);
                    }
                    p4Var.put(aVar2.j(), Boolean.valueOf(aVar2.k()));
                    p4Var2.put(aVar2.j(), Boolean.valueOf(aVar2.l()));
                    if (aVar2.m()) {
                        if (aVar2.n() < k || aVar2.n() > j) {
                            b().w().a("Invalid sampling rate. Event name, sample rate", aVar2.j(), Integer.valueOf(aVar2.n()));
                        } else {
                            p4Var3.put(aVar2.j(), Integer.valueOf(aVar2.n()));
                        }
                    }
                }
            }
        }
        this.e.put(str, p4Var);
        this.f.put(str, p4Var2);
        this.h.put(str, p4Var3);
    }

    @DexIgnore
    public final boolean a(String str, byte[] bArr, String str2) {
        byte[] bArr2;
        boolean z;
        String str3 = str;
        r();
        g();
        w12.b(str);
        gj2.a aVar = (gj2.a) a(str, bArr).j();
        if (aVar == null) {
            return false;
        }
        a(str3, aVar);
        this.g.put(str3, (gj2) aVar.i());
        this.i.put(str3, str2);
        this.d.put(str3, a((gj2) aVar.i()));
        sa3 m = m();
        ArrayList arrayList = new ArrayList(aVar.k());
        w12.a(arrayList);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            vi2.a aVar2 = (vi2.a) ((vi2) arrayList.get(i2)).j();
            if (aVar2.k() != 0) {
                for (int i3 = 0; i3 < aVar2.k(); i3++) {
                    wi2.a aVar3 = (wi2.a) aVar2.b(i3).j();
                    wi2.a aVar4 = (wi2.a) aVar3.clone();
                    String a = x63.a(aVar3.j());
                    if (a != null) {
                        aVar4.a(a);
                        z = true;
                    } else {
                        z = false;
                    }
                    boolean z2 = z;
                    for (int i4 = 0; i4 < aVar3.k(); i4++) {
                        xi2 a2 = aVar3.a(i4);
                        String a3 = w63.a(a2.t());
                        if (a3 != null) {
                            xi2.a aVar5 = (xi2.a) a2.j();
                            aVar5.a(a3);
                            aVar4.a(i4, (xi2) aVar5.i());
                            z2 = true;
                        }
                    }
                    if (z2) {
                        aVar2.a(i3, aVar4);
                        arrayList.set(i2, (vi2) aVar2.i());
                    }
                }
            }
            if (aVar2.j() != 0) {
                for (int i5 = 0; i5 < aVar2.j(); i5++) {
                    zi2 a4 = aVar2.a(i5);
                    String a5 = z63.a(a4.p());
                    if (a5 != null) {
                        zi2.a aVar6 = (zi2.a) a4.j();
                        aVar6.a(a5);
                        aVar2.a(i5, aVar6);
                        arrayList.set(i2, (vi2) aVar2.i());
                    }
                }
            }
        }
        m.o().a(str3, (List<vi2>) arrayList);
        try {
            aVar.l();
            bArr2 = ((gj2) ((fn2) aVar.i())).f();
        } catch (RuntimeException e2) {
            b().w().a("Unable to serialize reduced-size config. Storing full config instead. appId", t43.a(str), e2);
            bArr2 = bArr;
        }
        yz2 o = o();
        w12.b(str);
        o.g();
        o.r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr2);
        try {
            if (((long) o.v().update("apps", contentValues, "app_id = ?", new String[]{str3})) == 0) {
                o.b().t().a("Failed to update remote config (got 0). appId", t43.a(str));
            }
        } catch (SQLiteException e3) {
            o.b().t().a("Error storing remote config. appId", t43.a(str), e3);
        }
        this.g.put(str3, (gj2) aVar.i());
        return true;
    }

    @DexIgnore
    public final gj2 a(String str, byte[] bArr) {
        if (bArr == null) {
            return gj2.y();
        }
        try {
            gj2.a x = gj2.x();
            ia3.a(x, bArr);
            gj2 gj2 = (gj2) ((fn2) x.i());
            v43 B = b().B();
            String str2 = null;
            Long valueOf = gj2.n() ? Long.valueOf(gj2.o()) : null;
            if (gj2.p()) {
                str2 = gj2.q();
            }
            B.a("Parsed config. version, gmp_app_id", valueOf, str2);
            return gj2;
        } catch (qn2 e2) {
            b().w().a("Unable to merge remote config. appId", t43.a(str), e2);
            return gj2.y();
        } catch (RuntimeException e3) {
            b().w().a("Unable to merge remote config. appId", t43.a(str), e3);
            return gj2.y();
        }
    }
}
