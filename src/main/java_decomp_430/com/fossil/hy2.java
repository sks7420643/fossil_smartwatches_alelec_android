package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.x52;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy2 extends jh2 implements vx2 {
    @DexIgnore
    public hy2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaFragmentDelegate");
    }

    @DexIgnore
    public final void a(x52 x52, StreetViewPanoramaOptions streetViewPanoramaOptions, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        lh2.a(zza, (Parcelable) streetViewPanoramaOptions);
        lh2.a(zza, (Parcelable) bundle);
        b(2, zza);
    }

    @DexIgnore
    public final void b(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) bundle);
        b(3, zza);
    }

    @DexIgnore
    public final void c() throws RemoteException {
        b(14, zza());
    }

    @DexIgnore
    public final void d() throws RemoteException {
        b(5, zza());
    }

    @DexIgnore
    public final void e() throws RemoteException {
        b(7, zza());
    }

    @DexIgnore
    public final void onLowMemory() throws RemoteException {
        b(9, zza());
    }

    @DexIgnore
    public final void onPause() throws RemoteException {
        b(6, zza());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        b(8, zza());
    }

    @DexIgnore
    public final x52 a(x52 x52, x52 x522, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        lh2.a(zza, (IInterface) x522);
        lh2.a(zza, (Parcelable) bundle);
        Parcel a = a(4, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) bundle);
        Parcel a = a(10, zza);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    public final void a(ey2 ey2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) ey2);
        b(12, zza);
    }

    @DexIgnore
    public final void a() throws RemoteException {
        b(13, zza());
    }
}
