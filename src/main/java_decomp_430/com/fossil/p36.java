package com.fossil;

import android.content.Context;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p36 {
    @DexIgnore
    public static void a(Context context) {
        q36.b(context, (r36) null);
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties) {
        q36.a(context, str, properties, (r36) null);
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2) {
        return q36.a(context, str, str2, (r36) null);
    }

    @DexIgnore
    public static void b(Context context) {
        q36.c(context, (r36) null);
    }
}
