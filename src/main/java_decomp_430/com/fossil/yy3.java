package com.fossil;

import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yy3 {
    @DexIgnore
    public static yy3[] i; // = {new yy3(false, 3, 5, 8, 8, 1), new yy3(false, 5, 7, 10, 10, 1), new yy3(true, 5, 7, 16, 6, 1), new yy3(false, 8, 10, 12, 12, 1), new yy3(true, 10, 11, 14, 6, 2), new yy3(false, 12, 12, 14, 14, 1), new yy3(true, 16, 14, 24, 10, 1), new yy3(false, 18, 14, 16, 16, 1), new yy3(false, 22, 18, 18, 18, 1), new yy3(true, 22, 18, 16, 10, 2), new yy3(false, 30, 20, 20, 20, 1), new yy3(true, 32, 24, 16, 14, 2), new yy3(false, 36, 24, 22, 22, 1), new yy3(false, 44, 28, 24, 24, 1), new yy3(true, 49, 28, 22, 14, 2), new yy3(false, 62, 36, 14, 14, 4), new yy3(false, 86, 42, 16, 16, 4), new yy3(false, 114, 48, 18, 18, 4), new yy3(false, 144, 56, 20, 20, 4), new yy3(false, 174, 68, 22, 22, 4), new yy3(false, 204, 84, 24, 24, 4, 102, 42), new yy3(false, 280, 112, 14, 14, 16, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 56), new yy3(false, 368, 144, 16, 16, 16, 92, 36), new yy3(false, 456, 192, 18, 18, 16, 114, 48), new yy3(false, 576, 224, 20, 20, 16, 144, 56), new yy3(false, 696, 272, 22, 22, 16, 174, 68), new yy3(false, 816, 336, 24, 24, 16, 136, 56), new yy3(false, 1050, 408, 18, 18, 36, 175, 68), new yy3(false, 1304, 496, 20, 20, 36, 163, 62), new ry3()};
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore
    public yy3(boolean z, int i2, int i3, int i4, int i5, int i6) {
        this(z, i2, i3, i4, i5, i6, i2, i3);
    }

    @DexIgnore
    public static yy3 a(int i2, zy3 zy3, rx3 rx3, rx3 rx32, boolean z) {
        for (yy3 yy3 : i) {
            if ((zy3 != zy3.FORCE_SQUARE || !yy3.a) && ((zy3 != zy3.FORCE_RECTANGLE || yy3.a) && ((rx3 == null || (yy3.h() >= rx3.b() && yy3.g() >= rx3.a())) && ((rx32 == null || (yy3.h() <= rx32.b() && yy3.g() <= rx32.a())) && i2 <= yy3.b)))) {
                return yy3;
            }
        }
        if (!z) {
            return null;
        }
        throw new IllegalArgumentException("Can't find a symbol arrangement that matches the message. Data codewords: " + i2);
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        int i2 = this.f;
        int i3 = 1;
        if (i2 != 1) {
            i3 = 2;
            if (!(i2 == 2 || i2 == 4)) {
                if (i2 == 16) {
                    return 4;
                }
                if (i2 == 36) {
                    return 6;
                }
                throw new IllegalStateException("Cannot handle this number of data regions");
            }
        }
        return i3;
    }

    @DexIgnore
    public int d() {
        return this.b / this.g;
    }

    @DexIgnore
    public final int e() {
        return i() * this.e;
    }

    @DexIgnore
    public final int f() {
        return c() * this.d;
    }

    @DexIgnore
    public final int g() {
        return e() + (i() << 1);
    }

    @DexIgnore
    public final int h() {
        return f() + (c() << 1);
    }

    @DexIgnore
    public final int i() {
        int i2 = this.f;
        if (i2 == 1 || i2 == 2) {
            return 1;
        }
        if (i2 == 4) {
            return 2;
        }
        if (i2 == 16) {
            return 4;
        }
        if (i2 == 36) {
            return 6;
        }
        throw new IllegalStateException("Cannot handle this number of data regions");
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a ? "Rectangular Symbol:" : "Square Symbol:");
        sb.append(" data region ");
        sb.append(this.d);
        sb.append('x');
        sb.append(this.e);
        sb.append(", symbol size ");
        sb.append(h());
        sb.append('x');
        sb.append(g());
        sb.append(", symbol data size ");
        sb.append(f());
        sb.append('x');
        sb.append(e());
        sb.append(", codewords ");
        sb.append(this.b);
        sb.append('+');
        sb.append(this.c);
        return sb.toString();
    }

    @DexIgnore
    public yy3(boolean z, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.a = z;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
        this.g = i7;
        this.h = i8;
    }

    @DexIgnore
    public final int b(int i2) {
        return this.h;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public int a(int i2) {
        return this.g;
    }
}
