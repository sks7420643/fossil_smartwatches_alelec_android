package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o84 extends n84 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131363098, 1);
        x.put(2131363192, 2);
        x.put(2131362868, 3);
        x.put(2131362884, 4);
    }
    */

    @DexIgnore
    public o84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 5, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public o84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[3], objArr[4], objArr[1], objArr[2]);
        this.v = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
