package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qg3 extends o0 {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> c;
    @DexIgnore
    public FrameLayout d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BottomSheetBehavior.e i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            qg3 qg3 = qg3.this;
            if (qg3.f && qg3.isShowing() && qg3.this.f()) {
                qg3.this.cancel();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnTouchListener {
        @DexIgnore
        public c(qg3 qg3) {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends BottomSheetBehavior.e {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(View view, float f) {
        }

        @DexIgnore
        public void a(View view, int i) {
            if (i == 5) {
                qg3.this.cancel();
            }
        }
    }

    @DexIgnore
    public qg3(Context context) {
        this(context, 0);
    }

    @DexIgnore
    public final View a(int i2, View view, ViewGroup.LayoutParams layoutParams) {
        b();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) this.d.findViewById(rf3.coordinator);
        if (i2 != 0 && view == null) {
            view = getLayoutInflater().inflate(i2, coordinatorLayout, false);
        }
        FrameLayout frameLayout = (FrameLayout) this.d.findViewById(rf3.design_bottom_sheet);
        if (layoutParams == null) {
            frameLayout.addView(view);
        } else {
            frameLayout.addView(view, layoutParams);
        }
        coordinatorLayout.findViewById(rf3.touch_outside).setOnClickListener(new a());
        x9.a((View) frameLayout, (a9) new b());
        frameLayout.setOnTouchListener(new c(this));
        return this.d;
    }

    @DexIgnore
    public final FrameLayout b() {
        if (this.d == null) {
            this.d = (FrameLayout) View.inflate(getContext(), tf3.design_bottom_sheet_dialog, (ViewGroup) null);
            this.c = BottomSheetBehavior.b((FrameLayout) this.d.findViewById(rf3.design_bottom_sheet));
            this.c.a(this.i);
            this.c.b(this.f);
        }
        return this.d;
    }

    @DexIgnore
    public BottomSheetBehavior<FrameLayout> c() {
        if (this.c == null) {
            b();
        }
        return this.c;
    }

    @DexIgnore
    public void cancel() {
        BottomSheetBehavior<FrameLayout> c2 = c();
        if (!this.e || c2.e() == 5) {
            super.cancel();
        } else {
            c2.e(5);
        }
    }

    @DexIgnore
    public boolean d() {
        return this.e;
    }

    @DexIgnore
    public void e() {
        this.c.b(this.i);
    }

    @DexIgnore
    public boolean f() {
        if (!this.h) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{16843611});
            this.g = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
            this.h = true;
        }
        return this.g;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
                window.addFlags(RecyclerView.UNDEFINED_DURATION);
            }
            window.setLayout(-1, -1);
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.c;
        if (bottomSheetBehavior != null && bottomSheetBehavior.e() == 5) {
            this.c.e(4);
        }
    }

    @DexIgnore
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.f != z) {
            this.f = z;
            BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.c;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.b(z);
            }
        }
    }

    @DexIgnore
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.f) {
            this.f = true;
        }
        this.g = z;
        this.h = true;
    }

    @DexIgnore
    public void setContentView(int i2) {
        super.setContentView(a(i2, (View) null, (ViewGroup.LayoutParams) null));
    }

    @DexIgnore
    public qg3(Context context, int i2) {
        super(context, a(context, i2));
        this.f = true;
        this.g = true;
        this.i = new d();
        a(1);
    }

    @DexIgnore
    public void setContentView(View view) {
        super.setContentView(a(0, view, (ViewGroup.LayoutParams) null));
    }

    @DexIgnore
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(a(0, view, layoutParams));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends a9 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(View view, ia iaVar) {
            super.a(view, iaVar);
            if (qg3.this.f) {
                iaVar.a(1048576);
                iaVar.g(true);
                return;
            }
            iaVar.g(false);
        }

        @DexIgnore
        public boolean a(View view, int i, Bundle bundle) {
            if (i == 1048576) {
                qg3 qg3 = qg3.this;
                if (qg3.f) {
                    qg3.cancel();
                    return true;
                }
            }
            return super.a(view, i, bundle);
        }
    }

    @DexIgnore
    public static int a(Context context, int i2) {
        if (i2 != 0) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(nf3.bottomSheetDialogTheme, typedValue, true)) {
            return typedValue.resourceId;
        }
        return wf3.Theme_Design_Light_BottomSheetDialog;
    }
}
