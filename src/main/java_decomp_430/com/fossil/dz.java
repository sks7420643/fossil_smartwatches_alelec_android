package com.fossil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dz {
    @DexIgnore
    public /* final */ AtomicReference<p00> a; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ p4<p00, List<Class<?>>> b; // = new p4<>();

    @DexIgnore
    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        List<Class<?>> list;
        p00 andSet = this.a.getAndSet((Object) null);
        if (andSet == null) {
            andSet = new p00(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
        }
        synchronized (this.b) {
            list = this.b.get(andSet);
        }
        this.a.set(andSet);
        return list;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new p00(cls, cls2, cls3), list);
        }
    }
}
