package com.fossil;

import android.os.SystemClock;
import com.fossil.ip;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fq implements op {
    @DexIgnore
    public static /* final */ boolean c; // = cq.b;
    @DexIgnore
    public /* final */ eq a;
    @DexIgnore
    public /* final */ gq b;

    @DexIgnore
    @Deprecated
    public fq(lq lqVar) {
        this(lqVar, new gq(4096));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005e, code lost:
        r15 = null;
        r19 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b3, code lost:
        r1 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b5, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b6, code lost:
        r19 = r1;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ba, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00bb, code lost:
        r19 = r1;
        r12 = null;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c1, code lost:
        r0 = r12.d();
        com.fossil.cq.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r29.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00da, code lost:
        if (r15 != null) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00dc, code lost:
        r13 = new com.fossil.rp(r0, r15, false, android.os.SystemClock.elapsedRealtime() - r9, r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ed, code lost:
        if (r0 == 401) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        if (r0 < 400) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0102, code lost:
        throw new com.fossil.kp(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0105, code lost:
        if (r0 < 500) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x010f, code lost:
        if (r29.shouldRetryServerErrors() != false) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0111, code lost:
        a("server", r8, new com.fossil.zp(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0122, code lost:
        throw new com.fossil.zp(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0128, code lost:
        throw new com.fossil.zp(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0129, code lost:
        a("auth", r8, new com.fossil.hp(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0135, code lost:
        a("network", r8, new com.fossil.qp());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0146, code lost:
        throw new com.fossil.sp(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0147, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0162, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r29.getUrl(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0163, code lost:
        a("socket", r8, new com.fossil.aq());
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0147 A[ExcHandler: MalformedURLException (r0v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0141 A[SYNTHETIC] */
    public rp a(up<?> upVar) throws bq {
        kq kqVar;
        List<np> c2;
        byte[] a2;
        List<np> list;
        up<?> upVar2 = upVar;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            List<np> emptyList = Collections.emptyList();
            try {
                kqVar = this.a.b(upVar2, a(upVar.getCacheEntry()));
                int d = kqVar.d();
                c2 = kqVar.c();
                if (d == 304) {
                    ip.a cacheEntry = upVar.getCacheEntry();
                    if (cacheEntry == null) {
                        return new rp(304, (byte[]) null, true, SystemClock.elapsedRealtime() - elapsedRealtime, c2);
                    }
                    return new rp(304, cacheEntry.a, true, SystemClock.elapsedRealtime() - elapsedRealtime, a(c2, cacheEntry));
                }
                InputStream a3 = kqVar.a();
                a2 = a3 != null ? a(a3, kqVar.b()) : new byte[0];
                a(SystemClock.elapsedRealtime() - elapsedRealtime, upVar, a2, d);
                if (d < 200 || d > 299) {
                    List<np> list2 = c2;
                } else {
                    list = c2;
                    rp rpVar = new rp(d, a2, false, SystemClock.elapsedRealtime() - elapsedRealtime, list);
                    return rpVar;
                }
            } catch (SocketTimeoutException unused) {
            } catch (MalformedURLException e) {
            } catch (IOException e2) {
                e = e2;
                list = c2;
                List<np> list3 = list;
                byte[] bArr = a2;
                if (kqVar == null) {
                }
            }
        }
        List<np> list22 = c2;
        throw new IOException();
    }

    @DexIgnore
    @Deprecated
    public fq(lq lqVar, gq gqVar) {
        this.a = new dq(lqVar);
        this.b = gqVar;
    }

    @DexIgnore
    public fq(eq eqVar) {
        this(eqVar, new gq(4096));
    }

    @DexIgnore
    public fq(eq eqVar, gq gqVar) {
        this.a = eqVar;
        this.b = gqVar;
    }

    @DexIgnore
    public final void a(long j, up<?> upVar, byte[] bArr, int i) {
        if (c || j > 3000) {
            Object[] objArr = new Object[5];
            objArr[0] = upVar;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(i);
            objArr[4] = Integer.valueOf(upVar.getRetryPolicy().b());
            cq.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    @DexIgnore
    public static void a(String str, up<?> upVar, bq bqVar) throws bq {
        yp retryPolicy = upVar.getRetryPolicy();
        int timeoutMs = upVar.getTimeoutMs();
        try {
            retryPolicy.a(bqVar);
            upVar.addMarker(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(timeoutMs)}));
        } catch (bq e) {
            upVar.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(timeoutMs)}));
            throw e;
        }
    }

    @DexIgnore
    public final Map<String, String> a(ip.a aVar) {
        if (aVar == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap();
        String str = aVar.b;
        if (str != null) {
            hashMap.put("If-None-Match", str);
        }
        long j = aVar.d;
        if (j > 0) {
            hashMap.put("If-Modified-Since", jq.a(j));
        }
        return hashMap;
    }

    @DexIgnore
    public final byte[] a(InputStream inputStream, int i) throws IOException, zp {
        rq rqVar = new rq(this.b, i);
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                bArr = this.b.a(1024);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    rqVar.write(bArr, 0, read);
                }
                return rqVar.toByteArray();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                        cq.d("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.b.a(bArr);
                rqVar.close();
            }
        } else {
            throw new zp();
        }
    }

    @DexIgnore
    public static List<np> a(List<np> list, ip.a aVar) {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        if (!list.isEmpty()) {
            for (np a2 : list) {
                treeSet.add(a2.a());
            }
        }
        ArrayList arrayList = new ArrayList(list);
        List<np> list2 = aVar.h;
        if (list2 != null) {
            if (!list2.isEmpty()) {
                for (np next : aVar.h) {
                    if (!treeSet.contains(next.a())) {
                        arrayList.add(next);
                    }
                }
            }
        } else if (!aVar.g.isEmpty()) {
            for (Map.Entry next2 : aVar.g.entrySet()) {
                if (!treeSet.contains(next2.getKey())) {
                    arrayList.add(new np((String) next2.getKey(), (String) next2.getValue()));
                }
            }
        }
        return arrayList;
    }
}
