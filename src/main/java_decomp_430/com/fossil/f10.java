package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f10 {
    @DexIgnore
    public /* final */ cb6 a;

    @DexIgnore
    public f10(cb6 cb6) {
        this.a = cb6;
    }

    @DexIgnore
    public static f10 a(Context context) {
        return new f10(new db6(context, "settings"));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public void b() {
        cb6 cb6 = this.a;
        cb6.a(cb6.edit().putBoolean("analytics_launched", true));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a() {
        return this.a.get().getBoolean("analytics_launched", false);
    }
}
