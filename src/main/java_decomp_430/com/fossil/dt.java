package com.fossil;

import android.os.Build;
import android.util.Log;
import com.fossil.bt;
import com.fossil.dr;
import com.fossil.et;
import com.fossil.s00;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dt<R> implements bt.a, Runnable, Comparable<dt<?>>, s00.f {
    @DexIgnore
    public Thread A;
    @DexIgnore
    public vr B;
    @DexIgnore
    public vr C;
    @DexIgnore
    public Object D;
    @DexIgnore
    public pr E;
    @DexIgnore
    public fs<?> F;
    @DexIgnore
    public volatile bt G;
    @DexIgnore
    public volatile boolean H;
    @DexIgnore
    public volatile boolean I;
    @DexIgnore
    public /* final */ ct<R> a; // = new ct<>();
    @DexIgnore
    public /* final */ List<Throwable> b; // = new ArrayList();
    @DexIgnore
    public /* final */ u00 c; // = u00.b();
    @DexIgnore
    public /* final */ e d;
    @DexIgnore
    public /* final */ v8<dt<?>> e;
    @DexIgnore
    public /* final */ d<?> f; // = new d<>();
    @DexIgnore
    public /* final */ f g; // = new f();
    @DexIgnore
    public yq h;
    @DexIgnore
    public vr i;
    @DexIgnore
    public br j;
    @DexIgnore
    public jt o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public ft r;
    @DexIgnore
    public xr s;
    @DexIgnore
    public b<R> t;
    @DexIgnore
    public int u;
    @DexIgnore
    public h v;
    @DexIgnore
    public g w;
    @DexIgnore
    public long x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public Object z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[g.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[h.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c; // = new int[rr.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        /*
        static {
            try {
                c[rr.SOURCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[rr.TRANSFORMED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            b[h.RESOURCE_CACHE.ordinal()] = 1;
            b[h.DATA_CACHE.ordinal()] = 2;
            b[h.SOURCE.ordinal()] = 3;
            b[h.FINISHED.ordinal()] = 4;
            b[h.INITIALIZE.ordinal()] = 5;
            a[g.INITIALIZE.ordinal()] = 1;
            a[g.SWITCH_TO_SOURCE_SERVICE.ordinal()] = 2;
            try {
                a[g.DECODE_DATA.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
        */
    }

    @DexIgnore
    public interface b<R> {
        @DexIgnore
        void a(dt<?> dtVar);

        @DexIgnore
        void a(mt mtVar);

        @DexIgnore
        void a(rt<R> rtVar, pr prVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c<Z> implements et.a<Z> {
        @DexIgnore
        public /* final */ pr a;

        @DexIgnore
        public c(pr prVar) {
            this.a = prVar;
        }

        @DexIgnore
        public rt<Z> a(rt<Z> rtVar) {
            return dt.this.a(this.a, rtVar);
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        ku a();
    }

    @DexIgnore
    public enum g {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    @DexIgnore
    public enum h {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    @DexIgnore
    public dt(e eVar, v8<dt<?>> v8Var) {
        this.d = eVar;
        this.e = v8Var;
    }

    @DexIgnore
    public dt<R> a(yq yqVar, Object obj, jt jtVar, vr vrVar, int i2, int i3, Class<?> cls, Class<R> cls2, br brVar, ft ftVar, Map<Class<?>, bs<?>> map, boolean z2, boolean z3, boolean z4, xr xrVar, b<R> bVar, int i4) {
        this.a.a(yqVar, obj, vrVar, i2, i3, ftVar, cls, cls2, brVar, xrVar, map, z2, z3, this.d);
        this.h = yqVar;
        this.i = vrVar;
        this.j = brVar;
        this.o = jtVar;
        this.p = i2;
        this.q = i3;
        this.r = ftVar;
        this.y = z4;
        this.s = xrVar;
        this.t = bVar;
        this.u = i4;
        this.w = g.INITIALIZE;
        this.z = obj;
        return this;
    }

    @DexIgnore
    public void b() {
        this.w = g.SWITCH_TO_SOURCE_SERVICE;
        this.t.a((dt<?>) this);
    }

    @DexIgnore
    public final void c() {
        if (Log.isLoggable("DecodeJob", 2)) {
            long j2 = this.x;
            a("Retrieved data", j2, "data: " + this.D + ", cache key: " + this.B + ", fetcher: " + this.F);
        }
        rt<R> rtVar = null;
        try {
            rtVar = a(this.F, this.D, this.E);
        } catch (mt e2) {
            e2.setLoggingDetails(this.C, this.E);
            this.b.add(e2);
        }
        if (rtVar != null) {
            b(rtVar, this.E);
        } else {
            j();
        }
    }

    @DexIgnore
    public u00 d() {
        return this.c;
    }

    @DexIgnore
    public final bt e() {
        int i2 = a.b[this.v.ordinal()];
        if (i2 == 1) {
            return new st(this.a, this);
        }
        if (i2 == 2) {
            return new ys(this.a, this);
        }
        if (i2 == 3) {
            return new vt(this.a, this);
        }
        if (i2 == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.v);
    }

    @DexIgnore
    public final void f() {
        l();
        this.t.a(new mt("Failed to load resource", (List<Throwable>) new ArrayList(this.b)));
        h();
    }

    @DexIgnore
    public final void g() {
        if (this.g.a()) {
            i();
        }
    }

    @DexIgnore
    public final int getPriority() {
        return this.j.ordinal();
    }

    @DexIgnore
    public final void h() {
        if (this.g.b()) {
            i();
        }
    }

    @DexIgnore
    public final void i() {
        this.g.c();
        this.f.a();
        this.a.a();
        this.H = false;
        this.h = null;
        this.i = null;
        this.s = null;
        this.j = null;
        this.o = null;
        this.t = null;
        this.v = null;
        this.G = null;
        this.A = null;
        this.B = null;
        this.D = null;
        this.E = null;
        this.F = null;
        this.x = 0;
        this.I = false;
        this.z = null;
        this.b.clear();
        this.e.a(this);
    }

    @DexIgnore
    public final void j() {
        this.A = Thread.currentThread();
        this.x = m00.a();
        boolean z2 = false;
        while (!this.I && this.G != null && !(z2 = this.G.a())) {
            this.v = a(this.v);
            this.G = e();
            if (this.v == h.SOURCE) {
                b();
                return;
            }
        }
        if ((this.v == h.FINISHED || this.I) && !z2) {
            f();
        }
    }

    @DexIgnore
    public final void k() {
        int i2 = a.a[this.w.ordinal()];
        if (i2 == 1) {
            this.v = a(h.INITIALIZE);
            this.G = e();
            j();
        } else if (i2 == 2) {
            j();
        } else if (i2 == 3) {
            c();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.w);
        }
    }

    @DexIgnore
    public final void l() {
        Throwable th;
        this.c.a();
        if (this.H) {
            if (this.b.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.b;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.H = true;
    }

    @DexIgnore
    public boolean m() {
        h a2 = a(h.INITIALIZE);
        return a2 == h.RESOURCE_CACHE || a2 == h.DATA_CACHE;
    }

    @DexIgnore
    public void run() {
        t00.a("DecodeJob#run(model=%s)", this.z);
        fs<?> fsVar = this.F;
        try {
            if (this.I) {
                f();
                if (fsVar != null) {
                    fsVar.a();
                }
                t00.a();
                return;
            }
            k();
            if (fsVar != null) {
                fsVar.a();
            }
            t00.a();
        } catch (xs e2) {
            throw e2;
        } catch (Throwable th) {
            if (fsVar != null) {
                fsVar.a();
            }
            t00.a();
            throw th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public synchronized boolean a() {
            this.b = true;
            return a(false);
        }

        @DexIgnore
        public synchronized boolean b(boolean z) {
            this.a = true;
            return a(z);
        }

        @DexIgnore
        public synchronized void c() {
            this.b = false;
            this.a = false;
            this.c = false;
        }

        @DexIgnore
        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.a;
        }

        @DexIgnore
        public synchronized boolean b() {
            this.c = true;
            return a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Z> {
        @DexIgnore
        public vr a;
        @DexIgnore
        public as<Z> b;
        @DexIgnore
        public qt<Z> c;

        @DexIgnore
        public <X> void a(vr vrVar, as<X> asVar, qt<X> qtVar) {
            this.a = vrVar;
            this.b = asVar;
            this.c = qtVar;
        }

        @DexIgnore
        public boolean b() {
            return this.c != null;
        }

        @DexIgnore
        public void a(e eVar, xr xrVar) {
            t00.a("DecodeJob.encode");
            try {
                eVar.a().a(this.a, new at(this.b, this.c, xrVar));
            } finally {
                this.c.f();
                t00.a();
            }
        }

        @DexIgnore
        public void a() {
            this.a = null;
            this.b = null;
            this.c = null;
        }
    }

    @DexIgnore
    public final void b(rt<R> rtVar, pr prVar) {
        if (rtVar instanceof nt) {
            ((nt) rtVar).d();
        }
        qt<R> qtVar = null;
        qt<R> qtVar2 = rtVar;
        if (this.f.b()) {
            qt<R> b2 = qt.b(rtVar);
            qtVar = b2;
            qtVar2 = b2;
        }
        a(qtVar2, prVar);
        this.v = h.ENCODE;
        try {
            if (this.f.b()) {
                this.f.a(this.d, this.s);
            }
            g();
        } finally {
            if (qtVar != null) {
                qtVar.f();
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.g.b(z2)) {
            i();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(dt<?> dtVar) {
        int priority = getPriority() - dtVar.getPriority();
        return priority == 0 ? this.u - dtVar.u : priority;
    }

    @DexIgnore
    public void a() {
        this.I = true;
        bt btVar = this.G;
        if (btVar != null) {
            btVar.cancel();
        }
    }

    @DexIgnore
    public final void a(rt<R> rtVar, pr prVar) {
        l();
        this.t.a(rtVar, prVar);
    }

    @DexIgnore
    public final h a(h hVar) {
        int i2 = a.b[hVar.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                return this.y ? h.FINISHED : h.SOURCE;
            }
            if (i2 == 3 || i2 == 4) {
                return h.FINISHED;
            }
            if (i2 != 5) {
                throw new IllegalArgumentException("Unrecognized stage: " + hVar);
            } else if (this.r.b()) {
                return h.RESOURCE_CACHE;
            } else {
                return a(h.RESOURCE_CACHE);
            }
        } else if (this.r.a()) {
            return h.DATA_CACHE;
        } else {
            return a(h.DATA_CACHE);
        }
    }

    @DexIgnore
    public void a(vr vrVar, Object obj, fs<?> fsVar, pr prVar, vr vrVar2) {
        this.B = vrVar;
        this.D = obj;
        this.F = fsVar;
        this.E = prVar;
        this.C = vrVar2;
        if (Thread.currentThread() != this.A) {
            this.w = g.DECODE_DATA;
            this.t.a((dt<?>) this);
            return;
        }
        t00.a("DecodeJob.decodeFromRetrievedData");
        try {
            c();
        } finally {
            t00.a();
        }
    }

    @DexIgnore
    public void a(vr vrVar, Exception exc, fs<?> fsVar, pr prVar) {
        fsVar.a();
        mt mtVar = new mt("Fetching data failed", (Throwable) exc);
        mtVar.setLoggingDetails(vrVar, prVar, fsVar.getDataClass());
        this.b.add(mtVar);
        if (Thread.currentThread() != this.A) {
            this.w = g.SWITCH_TO_SOURCE_SERVICE;
            this.t.a((dt<?>) this);
            return;
        }
        j();
    }

    @DexIgnore
    public final <Data> rt<R> a(fs<?> fsVar, Data data, pr prVar) throws mt {
        if (data == null) {
            fsVar.a();
            return null;
        }
        try {
            long a2 = m00.a();
            rt<R> a3 = a(data, prVar);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Decoded result " + a3, a2);
            }
            return a3;
        } finally {
            fsVar.a();
        }
    }

    @DexIgnore
    public final <Data> rt<R> a(Data data, pr prVar) throws mt {
        return a(data, prVar, this.a.a(data.getClass()));
    }

    @DexIgnore
    public final xr a(pr prVar) {
        xr xrVar = this.s;
        if (Build.VERSION.SDK_INT < 26) {
            return xrVar;
        }
        boolean z2 = prVar == pr.RESOURCE_DISK_CACHE || this.a.o();
        Boolean bool = (Boolean) xrVar.a(pw.i);
        if (bool != null && (!bool.booleanValue() || z2)) {
            return xrVar;
        }
        xr xrVar2 = new xr();
        xrVar2.a(this.s);
        xrVar2.a(pw.i, Boolean.valueOf(z2));
        return xrVar2;
    }

    @DexIgnore
    public final <Data, ResourceType> rt<R> a(Data data, pr prVar, pt<Data, ResourceType, R> ptVar) throws mt {
        xr a2 = a(prVar);
        gs b2 = this.h.f().b(data);
        try {
            return ptVar.a(b2, a2, this.p, this.q, new c(prVar));
        } finally {
            b2.a();
        }
    }

    @DexIgnore
    public final void a(String str, long j2) {
        a(str, j2, (String) null);
    }

    @DexIgnore
    public final void a(String str, long j2, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(m00.a(j2));
        sb.append(", load key: ");
        sb.append(this.o);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v6, resolved type: com.fossil.zs} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v8, resolved type: com.fossil.tt} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: com.fossil.tt} */
    /* JADX WARNING: type inference failed for: r12v5, types: [com.fossil.vr] */
    /* JADX WARNING: Multi-variable type inference failed */
    public <Z> rt<Z> a(pr prVar, rt<Z> rtVar) {
        bs<Z> bsVar;
        rt<Z> rtVar2;
        rr rrVar;
        tt ttVar;
        Class<?> cls = rtVar.get().getClass();
        as<Z> asVar = null;
        if (prVar != pr.RESOURCE_DISK_CACHE) {
            bs<Z> b2 = this.a.b(cls);
            bsVar = b2;
            rtVar2 = b2.a(this.h, rtVar, this.p, this.q);
        } else {
            rtVar2 = rtVar;
            bsVar = null;
        }
        if (!rtVar.equals(rtVar2)) {
            rtVar.a();
        }
        if (this.a.b((rt<?>) rtVar2)) {
            asVar = this.a.a(rtVar2);
            rrVar = asVar.a(this.s);
        } else {
            rrVar = rr.NONE;
        }
        as<Z> asVar2 = asVar;
        if (!this.r.a(!this.a.a(this.B), prVar, rrVar)) {
            return rtVar2;
        }
        if (asVar2 != null) {
            int i2 = a.c[rrVar.ordinal()];
            if (i2 == 1) {
                ttVar = new zs(this.B, this.i);
            } else if (i2 == 2) {
                ttVar = new tt(this.a.b(), this.B, this.i, this.p, this.q, bsVar, cls, this.s);
            } else {
                throw new IllegalArgumentException("Unknown strategy: " + rrVar);
            }
            qt<Z> b3 = qt.b(rtVar2);
            this.f.a(ttVar, asVar2, b3);
            return b3;
        }
        throw new dr.d(rtVar2.get().getClass());
    }
}
