package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import androidx.collection.SparseArrayCompat;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y2 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static y2 i;
    @DexIgnore
    public static /* final */ c j; // = new c(6);
    @DexIgnore
    public WeakHashMap<Context, SparseArrayCompat<ColorStateList>> a;
    @DexIgnore
    public p4<String, d> b;
    @DexIgnore
    public SparseArrayCompat<String> c;
    @DexIgnore
    public /* final */ WeakHashMap<Context, s4<WeakReference<Drawable.ConstantState>>> d; // = new WeakHashMap<>(0);
    @DexIgnore
    public TypedValue e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public e g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return v0.e(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return rk.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends t4<Integer, PorterDuffColorFilter> {
        @DexIgnore
        public c(int i) {
            super(i);
        }

        @DexIgnore
        public static int b(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) b(Integer.valueOf(b(i, mode)));
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        ColorStateList a(Context context, int i);

        @DexIgnore
        PorterDuff.Mode a(int i);

        @DexIgnore
        Drawable a(y2 y2Var, Context context, int i);

        @DexIgnore
        boolean a(Context context, int i, Drawable drawable);

        @DexIgnore
        boolean b(Context context, int i, Drawable drawable);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements d {
        @DexIgnore
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return xk.createFromXmlInner(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    @DexIgnore
    public static synchronized y2 a() {
        y2 y2Var;
        synchronized (y2.class) {
            if (i == null) {
                i = new y2();
                a(i);
            }
            y2Var = i;
        }
        return y2Var;
    }

    @DexIgnore
    public synchronized Drawable b(Context context, int i2) {
        return a(context, i2, false);
    }

    @DexIgnore
    public synchronized ColorStateList c(Context context, int i2) {
        ColorStateList d2;
        d2 = d(context, i2);
        if (d2 == null) {
            d2 = this.g == null ? null : this.g.a(context, i2);
            if (d2 != null) {
                a(context, i2, d2);
            }
        }
        return d2;
    }

    @DexIgnore
    public final ColorStateList d(Context context, int i2) {
        SparseArrayCompat sparseArrayCompat;
        WeakHashMap<Context, SparseArrayCompat<ColorStateList>> weakHashMap = this.a;
        if (weakHashMap == null || (sparseArrayCompat = weakHashMap.get(context)) == null) {
            return null;
        }
        return (ColorStateList) sparseArrayCompat.a(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073 A[Catch:{ Exception -> 0x00a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[Catch:{ Exception -> 0x00a2 }] */
    public final Drawable e(Context context, int i2) {
        int next;
        p4<String, d> p4Var = this.b;
        if (p4Var == null || p4Var.isEmpty()) {
            return null;
        }
        SparseArrayCompat<String> sparseArrayCompat = this.c;
        if (sparseArrayCompat != null) {
            String a2 = sparseArrayCompat.a(i2);
            if ("appcompat_skip_skip".equals(a2) || (a2 != null && this.b.get(a2) == null)) {
                return null;
            }
        } else {
            this.c = new SparseArrayCompat<>();
        }
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        Resources resources = context.getResources();
        resources.getValue(i2, typedValue, true);
        long a3 = a(typedValue);
        Drawable a4 = a(context, a3);
        if (a4 != null) {
            return a4;
        }
        CharSequence charSequence = typedValue.string;
        if (charSequence != null && charSequence.toString().endsWith(".xml")) {
            try {
                XmlResourceParser xml = resources.getXml(i2);
                AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
                while (true) {
                    next = xml.next();
                    if (next == 2 || next == 1) {
                        if (next != 2) {
                            String name = xml.getName();
                            this.c.a(i2, name);
                            d dVar = this.b.get(name);
                            if (dVar != null) {
                                a4 = dVar.a(context, xml, asAttributeSet, context.getTheme());
                            }
                            if (a4 != null) {
                                a4.setChangingConfigurations(typedValue.changingConfigurations);
                                a(context, a3, a4);
                            }
                        } else {
                            throw new XmlPullParserException("No start tag found");
                        }
                    }
                }
                if (next != 2) {
                }
            } catch (Exception e2) {
                Log.e("ResourceManagerInternal", "Exception while inflating drawable", e2);
            }
        }
        if (a4 == null) {
            this.c.a(i2, "appcompat_skip_skip");
        }
        return a4;
    }

    @DexIgnore
    public synchronized void b(Context context) {
        s4 s4Var = this.d.get(context);
        if (s4Var != null) {
            s4Var.a();
        }
    }

    @DexIgnore
    public static void a(y2 y2Var) {
        if (Build.VERSION.SDK_INT < 24) {
            y2Var.a("vector", (d) new f());
            y2Var.a("animated-vector", (d) new b());
            y2Var.a("animated-selector", (d) new a());
        }
    }

    @DexIgnore
    public synchronized void a(e eVar) {
        this.g = eVar;
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i2, boolean z) {
        Drawable e2;
        a(context);
        e2 = e(context, i2);
        if (e2 == null) {
            e2 = a(context, i2);
        }
        if (e2 == null) {
            e2 = w6.c(context, i2);
        }
        if (e2 != null) {
            e2 = a(context, i2, z, e2);
        }
        if (e2 != null) {
            s2.b(e2);
        }
        return e2;
    }

    @DexIgnore
    public static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    @DexIgnore
    public final Drawable a(Context context, int i2) {
        Drawable drawable;
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 != null) {
            return a3;
        }
        e eVar = this.g;
        if (eVar == null) {
            drawable = null;
        } else {
            drawable = eVar.a(this, context, i2);
        }
        if (drawable != null) {
            drawable.setChangingConfigurations(typedValue.changingConfigurations);
            a(context, a2, drawable);
        }
        return drawable;
    }

    @DexIgnore
    public final Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList c2 = c(context, i2);
        if (c2 != null) {
            if (s2.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable i3 = o7.i(drawable);
            o7.a(i3, c2);
            PorterDuff.Mode a2 = a(i2);
            if (a2 == null) {
                return i3;
            }
            o7.a(i3, a2);
            return i3;
        }
        e eVar = this.g;
        if ((eVar == null || !eVar.b(context, i2, drawable)) && !a(context, i2, drawable) && z) {
            return null;
        }
        return drawable;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        return null;
     */
    @DexIgnore
    public final synchronized Drawable a(Context context, long j2) {
        s4 s4Var = this.d.get(context);
        if (s4Var == null) {
            return null;
        }
        WeakReference weakReference = (WeakReference) s4Var.b(j2);
        if (weakReference != null) {
            Drawable.ConstantState constantState = (Drawable.ConstantState) weakReference.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            s4Var.a(j2);
        }
    }

    @DexIgnore
    public final synchronized boolean a(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        s4 s4Var = this.d.get(context);
        if (s4Var == null) {
            s4Var = new s4();
            this.d.put(context, s4Var);
        }
        s4Var.c(j2, new WeakReference(constantState));
        return true;
    }

    @DexIgnore
    public synchronized Drawable a(Context context, n3 n3Var, int i2) {
        Drawable e2 = e(context, i2);
        if (e2 == null) {
            e2 = n3Var.a(i2);
        }
        if (e2 == null) {
            return null;
        }
        return a(context, i2, false, e2);
    }

    @DexIgnore
    public boolean a(Context context, int i2, Drawable drawable) {
        e eVar = this.g;
        return eVar != null && eVar.a(context, i2, drawable);
    }

    @DexIgnore
    public final void a(String str, d dVar) {
        if (this.b == null) {
            this.b = new p4<>();
        }
        this.b.put(str, dVar);
    }

    @DexIgnore
    public PorterDuff.Mode a(int i2) {
        e eVar = this.g;
        if (eVar == null) {
            return null;
        }
        return eVar.a(i2);
    }

    @DexIgnore
    public final void a(Context context, int i2, ColorStateList colorStateList) {
        if (this.a == null) {
            this.a = new WeakHashMap<>();
        }
        SparseArrayCompat sparseArrayCompat = this.a.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new SparseArrayCompat();
            this.a.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.a(i2, colorStateList);
    }

    @DexIgnore
    public static void a(Drawable drawable, g3 g3Var, int[] iArr) {
        if (!s2.a(drawable) || drawable.mutate() == drawable) {
            if (g3Var.d || g3Var.c) {
                drawable.setColorFilter(a(g3Var.d ? g3Var.a : null, g3Var.c ? g3Var.b : h, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("ResourceManagerInternal", "Mutated drawable is not the same instance as the input.");
    }

    @DexIgnore
    public static PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    @DexIgnore
    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (y2.class) {
            a2 = j.a(i2, mode);
            if (a2 == null) {
                a2 = new PorterDuffColorFilter(i2, mode);
                j.a(i2, mode, a2);
            }
        }
        return a2;
    }

    @DexIgnore
    public final void a(Context context) {
        if (!this.f) {
            this.f = true;
            Drawable b2 = b(context, z0.abc_vector_test);
            if (b2 == null || !a(b2)) {
                this.f = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    @DexIgnore
    public static boolean a(Drawable drawable) {
        return (drawable instanceof xk) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }
}
