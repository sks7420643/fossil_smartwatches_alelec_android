package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl0 implements Parcelable.Creator<sn0> {
    @DexIgnore
    public /* synthetic */ zl0(qg6 qg6) {
    }

    @DexIgnore
    public sn0 createFromParcel(Parcel parcel) {
        return new sn0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new sn0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m75createFromParcel(Parcel parcel) {
        return new sn0(parcel, (qg6) null);
    }
}
