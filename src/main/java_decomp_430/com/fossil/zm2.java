package com.fossil;

import com.fossil.zm2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zm2<T extends zm2<T>> extends Comparable<T> {
    @DexIgnore
    nq2 zzb();

    @DexIgnore
    qq2 zzc();

    @DexIgnore
    boolean zzd();
}
