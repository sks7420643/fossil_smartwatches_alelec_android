package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kh {
    @DexIgnore
    public /* final */ Set<LiveData> a; // = Collections.newSetFromMap(new IdentityHashMap());
    @DexIgnore
    public /* final */ oh b;

    @DexIgnore
    public kh(oh ohVar) {
        this.b = ohVar;
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new sh(this.b, this, z, callable, strArr);
    }

    @DexIgnore
    public void b(LiveData liveData) {
        this.a.remove(liveData);
    }

    @DexIgnore
    public void a(LiveData liveData) {
        this.a.add(liveData);
    }
}
