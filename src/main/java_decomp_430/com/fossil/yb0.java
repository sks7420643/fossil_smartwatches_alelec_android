package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yb0<V> extends zb0<V> {
    @DexIgnore
    public yb0<V> f(hg6<? super cd6, cd6> hg6) {
        if (a()) {
            try {
                hg6.invoke(cd6.a);
            } catch (Exception e) {
                qs0.h.a(e);
            }
        } else {
            this.f.add(hg6);
        }
        return this;
    }

    @DexIgnore
    public yb0<V> g(hg6<? super Float, cd6> hg6) {
        if (!a()) {
            this.g.add(hg6);
        }
        return this;
    }

    @DexIgnore
    public yb0<V> d(hg6<? super ac0, cd6> hg6) {
        zb0 d = super.b(hg6);
        if (d != null) {
            return (yb0) d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public yb0<V> e(hg6<? super V, cd6> hg6) {
        zb0 e = super.c(hg6);
        if (e != null) {
            return (yb0) e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public yb0<V> a(hg6<? super ac0, cd6> hg6) {
        zb0 a = super.a((hg6) hg6);
        if (a != null) {
            return (yb0) a;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }
}
