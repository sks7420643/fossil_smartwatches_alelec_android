package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sm implements em {
    @DexIgnore
    public static /* final */ String o; // = tl.a("SystemAlarmDispatcher");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ to b;
    @DexIgnore
    public /* final */ um c;
    @DexIgnore
    public /* final */ gm d;
    @DexIgnore
    public /* final */ lm e;
    @DexIgnore
    public /* final */ pm f;
    @DexIgnore
    public /* final */ Handler g;
    @DexIgnore
    public /* final */ List<Intent> h;
    @DexIgnore
    public Intent i;
    @DexIgnore
    public c j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            d dVar;
            sm smVar;
            synchronized (sm.this.h) {
                sm.this.i = sm.this.h.get(0);
            }
            Intent intent = sm.this.i;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = sm.this.i.getIntExtra("KEY_START_ID", 0);
                tl.a().a(sm.o, String.format("Processing command %s, %s", new Object[]{sm.this.i, Integer.valueOf(intExtra)}), new Throwable[0]);
                PowerManager.WakeLock a2 = oo.a(sm.this.a, String.format("%s (%s)", new Object[]{action, Integer.valueOf(intExtra)}));
                try {
                    tl.a().a(sm.o, String.format("Acquiring operation wake lock (%s) %s", new Object[]{action, a2}), new Throwable[0]);
                    a2.acquire();
                    sm.this.f.g(sm.this.i, intExtra, sm.this);
                    tl.a().a(sm.o, String.format("Releasing operation wake lock (%s) %s", new Object[]{action, a2}), new Throwable[0]);
                    a2.release();
                    smVar = sm.this;
                    dVar = new d(smVar);
                } catch (Throwable th) {
                    tl.a().a(sm.o, String.format("Releasing operation wake lock (%s) %s", new Object[]{action, a2}), new Throwable[0]);
                    a2.release();
                    sm smVar2 = sm.this;
                    smVar2.a((Runnable) new d(smVar2));
                    throw th;
                }
                smVar.a((Runnable) dVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ sm a;
        @DexIgnore
        public /* final */ Intent b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(sm smVar, Intent intent, int i) {
            this.a = smVar;
            this.b = intent;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ sm a;

        @DexIgnore
        public d(sm smVar) {
            this.a = smVar;
        }

        @DexIgnore
        public void run() {
            this.a.b();
        }
    }

    @DexIgnore
    public sm(Context context) {
        this(context, (gm) null, (lm) null);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        a((Runnable) new b(this, pm.a(this.a, str, z), 0));
    }

    @DexIgnore
    public void b() {
        tl.a().a(o, "Checking if commands are complete.", new Throwable[0]);
        a();
        synchronized (this.h) {
            if (this.i != null) {
                tl.a().a(o, String.format("Removing command %s", new Object[]{this.i}), new Throwable[0]);
                if (this.h.remove(0).equals(this.i)) {
                    this.i = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            lo b2 = this.b.b();
            if (!this.f.a() && this.h.isEmpty() && !b2.a()) {
                tl.a().a(o, "No more commands & intents.", new Throwable[0]);
                if (this.j != null) {
                    this.j.a();
                }
            } else if (!this.h.isEmpty()) {
                h();
            }
        }
    }

    @DexIgnore
    public gm c() {
        return this.d;
    }

    @DexIgnore
    public to d() {
        return this.b;
    }

    @DexIgnore
    public lm e() {
        return this.e;
    }

    @DexIgnore
    public um f() {
        return this.c;
    }

    @DexIgnore
    public void g() {
        tl.a().a(o, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.d.b((em) this);
        this.c.a();
        this.j = null;
    }

    @DexIgnore
    public final void h() {
        a();
        PowerManager.WakeLock a2 = oo.a(this.a, "ProcessCommand");
        try {
            a2.acquire();
            this.e.h().a(new a());
        } finally {
            a2.release();
        }
    }

    @DexIgnore
    public sm(Context context, gm gmVar, lm lmVar) {
        this.a = context.getApplicationContext();
        this.f = new pm(this.a);
        this.c = new um();
        this.e = lmVar == null ? lm.a(context) : lmVar;
        this.d = gmVar == null ? this.e.e() : gmVar;
        this.b = this.e.h();
        this.d.a((em) this);
        this.h = new ArrayList();
        this.i = null;
        this.g = new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public boolean a(Intent intent, int i2) {
        boolean z = false;
        tl.a().a(o, String.format("Adding command %s (%s)", new Object[]{intent, Integer.valueOf(i2)}), new Throwable[0]);
        a();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            tl.a().e(o, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && a("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.h) {
                if (!this.h.isEmpty()) {
                    z = true;
                }
                this.h.add(intent);
                if (!z) {
                    h();
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        if (this.j != null) {
            tl.a().b(o, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.j = cVar;
        }
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.g.post(runnable);
    }

    @DexIgnore
    public final boolean a(String str) {
        a();
        synchronized (this.h) {
            for (Intent action : this.h) {
                if (str.equals(action.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public final void a() {
        if (this.g.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }
}
