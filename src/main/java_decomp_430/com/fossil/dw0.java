package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw0 extends uj1 {
    @DexIgnore
    public static /* final */ lu0 CREATOR; // = new lu0((qg6) null);
    @DexIgnore
    public ge1[] b; // = new ge1[0];
    @DexIgnore
    public /* final */ ml1 c; // = ml1.SIMPLE_MOVEMENT;

    @DexIgnore
    public dw0(ge1[] ge1Arr) {
        super(ck0.ANIMATION);
        this.b = ge1Arr;
    }

    @DexIgnore
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (ge1 a : this.b) {
            jSONArray.put(a.a());
        }
        return cw0.a(super.a(), bm0.ANIMATIONS, (Object) jSONArray);
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate((this.b.length * 3) + 2).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c.a);
        byte b2 = (byte) 0;
        order.put(b2);
        byte b3 = b2;
        for (ge1 ge1 : this.b) {
            b3 = (byte) (b3 | ge1.a.a);
            ByteBuffer order2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
            wg6.a(order2, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
            order2.put((byte) (((byte) (((byte) (((byte) (ge1.b.a << 6)) | ((byte) (ge1.c.a << 5)))) | 0)) | ge1.d.a));
            order2.putShort(ge1.e);
            byte[] array = order2.array();
            wg6.a(array, "dataBuffer.array()");
            order.put(array);
        }
        order.put(1, b3);
        byte[] array2 = order.array();
        wg6.a(array2, "byteBuffer.array()");
        return array2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(dw0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((dw0) obj).b);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.AnimationInstr");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }

    @DexIgnore
    public /* synthetic */ dw0(Parcel parcel, qg6 qg6) {
        super(parcel);
        Object[] readArray = parcel.readArray(ge1.class.getClassLoader());
        if (readArray != null) {
            this.b = (ge1[]) readArray;
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.microapp.animation.HandAnimation>");
    }
}
