package com.fossil;

import android.os.SystemClock;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o96 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;

    @DexIgnore
    public o96(String str, String str2) {
        this.a = str;
        this.b = str2;
        this.c = !Log.isLoggable(str2, 2);
    }

    @DexIgnore
    public final void a() {
        String str = this.b;
        Log.v(str, this.a + ": " + this.e + "ms");
    }

    @DexIgnore
    public synchronized void b() {
        if (!this.c) {
            this.d = SystemClock.elapsedRealtime();
            this.e = 0;
        }
    }

    @DexIgnore
    public synchronized void c() {
        if (!this.c) {
            if (this.e == 0) {
                this.e = SystemClock.elapsedRealtime() - this.d;
                a();
            }
        }
    }
}
