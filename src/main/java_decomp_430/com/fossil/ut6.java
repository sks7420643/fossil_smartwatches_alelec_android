package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut6 implements lt6 {
    @DexIgnore
    public /* final */ jt6 a; // = new jt6();
    @DexIgnore
    public /* final */ zt6 b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public ut6(zt6 zt6) {
        if (zt6 != null) {
            this.b = zt6;
            return;
        }
        throw new NullPointerException("source == null");
    }

    @DexIgnore
    public jt6 a() {
        return this.a;
    }

    @DexIgnore
    public long b(jt6 jt6, long j) throws IOException {
        if (jt6 == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.c) {
            jt6 jt62 = this.a;
            if (jt62.b == 0 && this.b.b(jt62, 8192) == -1) {
                return -1;
            }
            return this.a.b(jt6, Math.min(j, this.a.b));
        } else {
            throw new IllegalStateException("closed");
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.c) {
            this.c = true;
            this.b.close();
            this.a.k();
        }
    }

    @DexIgnore
    public mt6 e(long j) throws IOException {
        i(j);
        return this.a.e(j);
    }

    @DexIgnore
    public boolean f() throws IOException {
        if (!this.c) {
            return this.a.f() && this.b.b(this.a, 8192) == -1;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public boolean g(long j) throws IOException {
        jt6 jt6;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (!this.c) {
            do {
                jt6 = this.a;
                if (jt6.b >= j) {
                    return true;
                }
            } while (this.b.b(jt6, 8192) != -1);
            return false;
        } else {
            throw new IllegalStateException("closed");
        }
    }

    @DexIgnore
    public byte[] h(long j) throws IOException {
        i(j);
        return this.a.h(j);
    }

    @DexIgnore
    public void i(long j) throws IOException {
        if (!g(j)) {
            throw new EOFException();
        }
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.c;
    }

    @DexIgnore
    public short j() throws IOException {
        i(2);
        return this.a.j();
    }

    @DexIgnore
    public long q() throws IOException {
        i(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!g((long) i2)) {
                break;
            }
            byte a2 = this.a.a((long) i);
            if ((a2 >= 48 && a2 <= 57) || ((a2 >= 97 && a2 <= 102) || (a2 >= 65 && a2 <= 70))) {
                i = i2;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(a2)}));
            }
        }
        return this.a.q();
    }

    @DexIgnore
    public InputStream r() {
        return new a();
    }

    @DexIgnore
    public int read(ByteBuffer byteBuffer) throws IOException {
        jt6 jt6 = this.a;
        if (jt6.b == 0 && this.b.b(jt6, 8192) == -1) {
            return -1;
        }
        return this.a.read(byteBuffer);
    }

    @DexIgnore
    public byte readByte() throws IOException {
        i(1);
        return this.a.readByte();
    }

    @DexIgnore
    public void readFully(byte[] bArr) throws IOException {
        try {
            i((long) bArr.length);
            this.a.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (true) {
                jt6 jt6 = this.a;
                long j = jt6.b;
                if (j > 0) {
                    int a2 = jt6.a(bArr, i, (int) j);
                    if (a2 != -1) {
                        i += a2;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw e;
                }
            }
        }
    }

    @DexIgnore
    public int readInt() throws IOException {
        i(4);
        return this.a.readInt();
    }

    @DexIgnore
    public short readShort() throws IOException {
        i(2);
        return this.a.readShort();
    }

    @DexIgnore
    public void skip(long j) throws IOException {
        if (!this.c) {
            while (j > 0) {
                jt6 jt6 = this.a;
                if (jt6.b == 0 && this.b.b(jt6, 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.a.p());
                this.a.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.b + ")";
    }

    @DexIgnore
    public long a(yt6 yt6) throws IOException {
        if (yt6 != null) {
            long j = 0;
            while (this.b.b(this.a, 8192) != -1) {
                long l = this.a.l();
                if (l > 0) {
                    j += l;
                    yt6.a(this.a, l);
                }
            }
            if (this.a.p() <= 0) {
                return j;
            }
            long p = j + this.a.p();
            jt6 jt6 = this.a;
            yt6.a(jt6, jt6.p());
            return p;
        }
        throw new IllegalArgumentException("sink == null");
    }

    @DexIgnore
    public int i() throws IOException {
        i(4);
        return this.a.i();
    }

    @DexIgnore
    public byte[] e() throws IOException {
        this.a.a(this.b);
        return this.a.e();
    }

    @DexIgnore
    public String h() throws IOException {
        return f(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public String f(long j) throws IOException {
        if (j >= 0) {
            long j2 = j == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? Long.MAX_VALUE : j + 1;
            long a2 = a((byte) 10, 0, j2);
            if (a2 != -1) {
                return this.a.j(a2);
            }
            if (j2 < ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD && g(j2) && this.a.a(j2 - 1) == 13 && g(1 + j2) && this.a.a(j2) == 10) {
                return this.a.j(j2);
            }
            jt6 jt6 = new jt6();
            jt6 jt62 = this.a;
            jt62.a(jt6, 0, Math.min(32, jt62.p()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.a.p(), j) + " content=" + jt6.m().hex() + 8230);
        }
        throw new IllegalArgumentException("limit < 0: " + j);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends InputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public int available() throws IOException {
            ut6 ut6 = ut6.this;
            if (!ut6.c) {
                return (int) Math.min(ut6.a.b, 2147483647L);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public void close() throws IOException {
            ut6.this.close();
        }

        @DexIgnore
        public int read() throws IOException {
            ut6 ut6 = ut6.this;
            if (!ut6.c) {
                jt6 jt6 = ut6.a;
                if (jt6.b == 0 && ut6.b.b(jt6, 8192) == -1) {
                    return -1;
                }
                return ut6.this.a.readByte() & 255;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public String toString() {
            return ut6.this + ".inputStream()";
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws IOException {
            if (!ut6.this.c) {
                bu6.a((long) bArr.length, (long) i, (long) i2);
                ut6 ut6 = ut6.this;
                jt6 jt6 = ut6.a;
                if (jt6.b == 0 && ut6.b.b(jt6, 8192) == -1) {
                    return -1;
                }
                return ut6.this.a.a(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    public long g() throws IOException {
        i(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!g((long) i2)) {
                break;
            }
            byte a2 = this.a.a((long) i);
            if ((a2 >= 48 && a2 <= 57) || (i == 0 && a2 == 45)) {
                i = i2;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9] or '-' character but was %#x", new Object[]{Byte.valueOf(a2)}));
            }
        }
        if (i == 0) {
        }
        return this.a.g();
    }

    @DexIgnore
    public String a(Charset charset) throws IOException {
        if (charset != null) {
            this.a.a(this.b);
            return this.a.a(charset);
        }
        throw new IllegalArgumentException("charset == null");
    }

    @DexIgnore
    public au6 b() {
        return this.b.b();
    }

    @DexIgnore
    public long a(byte b2) throws IOException {
        return a(b2, 0, ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
    }

    @DexIgnore
    public long a(byte b2, long j, long j2) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2)}));
        } else {
            while (j < j2) {
                long a2 = this.a.a(b2, j, j2);
                if (a2 == -1) {
                    jt6 jt6 = this.a;
                    long j3 = jt6.b;
                    if (j3 >= j2 || this.b.b(jt6, 8192) == -1) {
                        break;
                    }
                    j = Math.max(j, j3);
                } else {
                    return a2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public boolean a(long j, mt6 mt6) throws IOException {
        return a(j, mt6, 0, mt6.size());
    }

    @DexIgnore
    public boolean a(long j, mt6 mt6, int i, int i2) throws IOException {
        if (this.c) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || mt6.size() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!g(1 + j2) || this.a.a(j2) != mt6.getByte(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }
}
