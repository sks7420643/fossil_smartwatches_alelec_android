package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr0 extends xg6 implements hg6<if1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ if1 a;
    @DexIgnore
    public /* final */ /* synthetic */ hg6 b;
    @DexIgnore
    public /* final */ /* synthetic */ hg6 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xr0(if1 if1, hg6 hg6, hg6 hg62) {
        super(1);
        this.a = if1;
        this.b = hg6;
        this.c = hg62;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        if1 if1 = (if1) obj;
        if (((Boolean) this.b.invoke(if1.v)).booleanValue()) {
            if1 if12 = this.a;
            if12.a(km1.a(if1.v, if12.y, (sk1) null, (bn0) null, 6));
        } else {
            this.c.invoke(if1);
        }
        return cd6.a;
    }
}
