package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class az5 {
    @DexIgnore
    public static /* final */ ExecutorService f; // = Executors.newCachedThreadPool();
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ zy5 c;
    @DexIgnore
    public /* final */ Bitmap d;
    @DexIgnore
    public /* final */ b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.az5$a$a")
        /* renamed from: com.fossil.az5$a$a  reason: collision with other inner class name */
        public class C0005a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ BitmapDrawable a;

            @DexIgnore
            public C0005a(BitmapDrawable bitmapDrawable) {
                this.a = bitmapDrawable;
            }

            @DexIgnore
            public void run() {
                az5.this.e.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            az5 az5 = az5.this;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(az5.a, vy5.a((Context) az5.this.b.get(), az5.d, az5.c));
            if (az5.this.e != null) {
                new Handler(Looper.getMainLooper()).post(new C0005a(bitmapDrawable));
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(BitmapDrawable bitmapDrawable);
    }

    @DexIgnore
    public az5(Context context, Bitmap bitmap, zy5 zy5, b bVar) {
        this.a = context.getResources();
        this.c = zy5;
        this.e = bVar;
        this.b = new WeakReference<>(context);
        this.d = bitmap;
    }

    @DexIgnore
    public void a() {
        f.execute(new a());
    }
}
