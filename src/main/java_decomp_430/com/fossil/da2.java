package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface da2 extends IInterface {
    @DexIgnore
    boolean a(boolean z) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    boolean zzc() throws RemoteException;
}
