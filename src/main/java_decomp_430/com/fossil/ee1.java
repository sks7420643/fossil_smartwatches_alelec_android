package com.fossil;

import java.io.File;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee1<T> implements Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        return ue6.a(Long.valueOf(((File) t2).lastModified()), Long.valueOf(((File) t).lastModified()));
    }
}
