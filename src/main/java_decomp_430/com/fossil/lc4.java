package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat C;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat D;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat E;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ View I;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ LinearLayout y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lc4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ConstraintLayout constraintLayout6, ScrollView scrollView, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleSwitchCompat flexibleSwitchCompat2, FlexibleSwitchCompat flexibleSwitchCompat3, FlexibleSwitchCompat flexibleSwitchCompat4, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = constraintLayout2;
        this.r = constraintLayout4;
        this.s = flexibleButton;
        this.t = flexibleTextView5;
        this.u = flexibleTextView7;
        this.v = flexibleTextView12;
        this.w = flexibleTextView15;
        this.x = rTLImageView;
        this.y = linearLayout;
        this.z = linearLayout2;
        this.A = linearLayout3;
        this.B = constraintLayout6;
        this.C = flexibleSwitchCompat;
        this.D = flexibleSwitchCompat2;
        this.E = flexibleSwitchCompat3;
        this.F = flexibleSwitchCompat4;
        this.G = view2;
        this.H = view3;
        this.I = view4;
    }
}
