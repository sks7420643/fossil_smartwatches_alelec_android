package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw2 implements Parcelable.Creator<lw2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = "";
        String str2 = str;
        String str3 = str2;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                str2 = f22.e(parcel, a);
            } else if (a2 == 2) {
                str3 = f22.e(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                str = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new lw2(str, str2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new lw2[i];
    }
}
