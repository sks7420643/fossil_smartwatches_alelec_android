package com.fossil;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gi5 extends k24<fi5> {
    @DexIgnore
    void a(ut4 ut4, ArrayList<String> arrayList);

    @DexIgnore
    void a(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c(cf<GoalTrackingData> cfVar);

    @DexIgnore
    void f();
}
