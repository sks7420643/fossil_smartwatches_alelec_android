package com.fossil;

import com.fossil.DianaCustomizeEditPresenter;
import com.fossil.m24;
import com.fossil.u85;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d45$b$c implements m24.e<u85.d, u85.b> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeEditPresenter.b a;
    @DexIgnore
    public /* final */ /* synthetic */ jl4 b;

    @DexIgnore
    public d45$b$c(DianaCustomizeEditPresenter.b bVar, jl4 jl4) {
        this.a = bVar;
        this.b = jl4;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetDianaPresetToWatchUseCase.d dVar) {
        wg6.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "setToWatch success");
        this.b.a("");
        this.a.this$0.l();
        this.a.this$0.m.n();
        this.a.this$0.m.f(true);
    }

    @DexIgnore
    public void a(SetDianaPresetToWatchUseCase.b bVar) {
        wg6.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "setToWatch onError");
        this.a.this$0.l();
        this.a.this$0.m.n();
        int b2 = bVar.b();
        if (b2 != 1101) {
            if (b2 == 8888) {
                this.a.this$0.m.c();
            } else if (!(b2 == 1112 || b2 == 1113)) {
                this.a.this$0.m.q();
            }
            String arrayList = bVar.a().toString();
            jl4 jl4 = this.b;
            wg6.a((Object) arrayList, "errorCode");
            jl4.a(arrayList);
            return;
        }
        List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
        wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
        z35 k = this.a.this$0.m;
        Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
        if (array != null) {
            uh4[] uh4Arr = (uh4[]) array;
            k.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
            String arrayList2 = bVar.a().toString();
            jl4 jl42 = this.b;
            wg6.a((Object) arrayList2, "errorCode");
            jl42.a(arrayList2);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
