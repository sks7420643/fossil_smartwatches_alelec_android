package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rh2 extends jh2 implements ph2 {
    @DexIgnore
    public rh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    @DexIgnore
    public final boolean a(ph2 ph2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) ph2);
        Parcel a = a(16, zza);
        boolean a2 = lh2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int j() throws RemoteException {
        Parcel a = a(17, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
