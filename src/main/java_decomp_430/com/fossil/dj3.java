package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hj3;
import com.fossil.ij3;
import com.fossil.jj3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dj3 extends Drawable implements p7, kj3 {
    @DexIgnore
    public static /* final */ Paint z; // = new Paint(1);
    @DexIgnore
    public c a;
    @DexIgnore
    public /* final */ jj3.g[] b;
    @DexIgnore
    public /* final */ jj3.g[] c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Matrix e;
    @DexIgnore
    public /* final */ Path f;
    @DexIgnore
    public /* final */ Path g;
    @DexIgnore
    public /* final */ RectF h;
    @DexIgnore
    public /* final */ RectF i;
    @DexIgnore
    public /* final */ Region j;
    @DexIgnore
    public /* final */ Region o;
    @DexIgnore
    public hj3 p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ vi3 s;
    @DexIgnore
    public /* final */ ij3.a t;
    @DexIgnore
    public /* final */ ij3 u;
    @DexIgnore
    public PorterDuffColorFilter v;
    @DexIgnore
    public PorterDuffColorFilter w;
    @DexIgnore
    public Rect x;
    @DexIgnore
    public /* final */ RectF y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ij3.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(jj3 jj3, Matrix matrix, int i) {
            dj3.this.b[i] = jj3.a(matrix);
        }

        @DexIgnore
        public void b(jj3 jj3, Matrix matrix, int i) {
            dj3.this.c[i] = jj3.a(matrix);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements hj3.c {
        @DexIgnore
        public /* final */ /* synthetic */ float a;

        @DexIgnore
        public b(dj3 dj3, float f) {
            this.a = f;
        }

        @DexIgnore
        public zi3 a(zi3 zi3) {
            return zi3 instanceof fj3 ? zi3 : new yi3(this.a, zi3);
        }
    }

    @DexIgnore
    public /* synthetic */ dj3(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public static int a(int i2, int i3) {
        return (i2 * (i3 + (i3 >>> 7))) >>> 8;
    }

    @DexIgnore
    public final boolean A() {
        return Build.VERSION.SDK_INT < 21 || (!z() && !this.f.isConvex());
    }

    @DexIgnore
    public final boolean B() {
        PorterDuffColorFilter porterDuffColorFilter = this.v;
        PorterDuffColorFilter porterDuffColorFilter2 = this.w;
        c cVar = this.a;
        this.v = a(cVar.g, cVar.h, this.q, true);
        c cVar2 = this.a;
        this.w = a(cVar2.f, cVar2.h, this.r, false);
        c cVar3 = this.a;
        if (cVar3.u) {
            this.s.a(cVar3.g.getColorForState(getState(), 0));
        }
        if (!t8.a(porterDuffColorFilter, this.v) || !t8.a(porterDuffColorFilter2, this.w)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void C() {
        float t2 = t();
        this.a.r = (int) Math.ceil((double) (0.75f * t2));
        this.a.s = (int) Math.ceil((double) (t2 * 0.25f));
        B();
        x();
    }

    @DexIgnore
    public void c(float f2) {
        c cVar = this.a;
        if (cVar.k != f2) {
            cVar.k = f2;
            this.d = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void d(int i2) {
        c cVar = this.a;
        if (cVar.q != i2) {
            cVar.q = i2;
            x();
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.q.setColorFilter(this.v);
        int alpha = this.q.getAlpha();
        this.q.setAlpha(a(alpha, this.a.m));
        this.r.setColorFilter(this.w);
        this.r.setStrokeWidth(this.a.l);
        int alpha2 = this.r.getAlpha();
        this.r.setAlpha(a(alpha2, this.a.m));
        if (this.d) {
            b();
            a(e(), this.f);
            this.d = false;
        }
        if (u()) {
            canvas.save();
            d(canvas);
            int width = (int) (this.y.width() - ((float) getBounds().width()));
            int height = (int) (this.y.height() - ((float) getBounds().height()));
            Bitmap createBitmap = Bitmap.createBitmap(((int) this.y.width()) + (this.a.r * 2) + width, ((int) this.y.height()) + (this.a.r * 2) + height, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap);
            float f2 = (float) ((getBounds().left - this.a.r) - width);
            float f3 = (float) ((getBounds().top - this.a.r) - height);
            canvas2.translate(-f2, -f3);
            a(canvas2);
            canvas.drawBitmap(createBitmap, f2, f3, (Paint) null);
            createBitmap.recycle();
            canvas.restore();
        }
        if (v()) {
            b(canvas);
        }
        if (w()) {
            c(canvas);
        }
        this.q.setAlpha(alpha);
        this.r.setAlpha(alpha2);
    }

    @DexIgnore
    public void e(float f2) {
        this.a.l = f2;
        invalidateSelf();
    }

    @DexIgnore
    public final RectF f() {
        RectF e2 = e();
        float o2 = o();
        this.i.set(e2.left + o2, e2.top + o2, e2.right - o2, e2.bottom - o2);
        return this.i;
    }

    @DexIgnore
    public float g() {
        return this.a.o;
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.a.q != 2) {
            if (z()) {
                outline.setRoundRect(getBounds(), q());
                return;
            }
            a(e(), this.f);
            if (this.f.isConvex()) {
                outline.setConvexPath(this.f);
            }
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        Rect rect2 = this.x;
        if (rect2 == null) {
            return super.getPadding(rect);
        }
        rect.set(rect2);
        return true;
    }

    @DexIgnore
    public Region getTransparentRegion() {
        this.j.set(getBounds());
        a(e(), this.f);
        this.o.setPath(this.f, this.j);
        this.j.op(this.o, Region.Op.DIFFERENCE);
        return this.j;
    }

    @DexIgnore
    public ColorStateList h() {
        return this.a.d;
    }

    @DexIgnore
    public float i() {
        return this.a.k;
    }

    @DexIgnore
    public void invalidateSelf() {
        this.d = true;
        super.invalidateSelf();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        r0 = r1.a.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r0 = r1.a.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = r1.a.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        r0 = r1.a.f;
     */
    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        return super.isStateful() || (colorStateList != null && colorStateList.isStateful()) || ((colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList3 != null && colorStateList3.isStateful()) || (colorStateList4 != null && colorStateList4.isStateful())));
    }

    @DexIgnore
    public float j() {
        return this.a.n;
    }

    @DexIgnore
    public int k() {
        c cVar = this.a;
        return (int) (((double) cVar.s) * Math.sin(Math.toRadians((double) cVar.t)));
    }

    @DexIgnore
    public int l() {
        c cVar = this.a;
        return (int) (((double) cVar.s) * Math.cos(Math.toRadians((double) cVar.t)));
    }

    @DexIgnore
    public int m() {
        return this.a.r;
    }

    @DexIgnore
    public Drawable mutate() {
        this.a = new c(this.a);
        return this;
    }

    @DexIgnore
    public hj3 n() {
        return this.a.a;
    }

    @DexIgnore
    public final float o() {
        return w() ? this.r.getStrokeWidth() / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.d = true;
        super.onBoundsChange(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean z2 = a(iArr) || B();
        if (z2) {
            invalidateSelf();
        }
        return z2;
    }

    @DexIgnore
    public ColorStateList p() {
        return this.a.g;
    }

    @DexIgnore
    public float q() {
        return this.a.a.j().a(e());
    }

    @DexIgnore
    public float r() {
        return this.a.a.l().a(e());
    }

    @DexIgnore
    public float s() {
        return this.a.p;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        c cVar = this.a;
        if (cVar.m != i2) {
            cVar.m = i2;
            x();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.c = colorFilter;
        x();
    }

    @DexIgnore
    public void setShapeAppearanceModel(hj3 hj3) {
        this.a.a = hj3;
        invalidateSelf();
    }

    @DexIgnore
    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        this.a.g = colorStateList;
        B();
        x();
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.a;
        if (cVar.h != mode) {
            cVar.h = mode;
            B();
            x();
        }
    }

    @DexIgnore
    public float t() {
        return g() + s();
    }

    @DexIgnore
    public final boolean u() {
        c cVar = this.a;
        int i2 = cVar.q;
        if (i2 == 1 || cVar.r <= 0 || (i2 != 2 && !A())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean v() {
        Paint.Style style = this.a.v;
        return style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.FILL;
    }

    @DexIgnore
    public final boolean w() {
        Paint.Style style = this.a.v;
        return (style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.STROKE) && this.r.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void x() {
        super.invalidateSelf();
    }

    @DexIgnore
    public boolean y() {
        rh3 rh3 = this.a.b;
        return rh3 != null && rh3.a();
    }

    @DexIgnore
    public boolean z() {
        return this.a.a.a(e());
    }

    @DexIgnore
    public dj3() {
        this(new hj3());
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.e != colorStateList) {
            cVar.e = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public dj3(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(hj3.a(context, attributeSet, i2, i3).a());
    }

    @DexIgnore
    public static dj3 a(Context context, float f2) {
        int a2 = yg3.a(context, nf3.colorSurface, dj3.class.getSimpleName());
        dj3 dj3 = new dj3();
        dj3.a(context);
        dj3.a(ColorStateList.valueOf(a2));
        dj3.b(f2);
        return dj3;
    }

    @DexIgnore
    public RectF e() {
        Rect bounds = getBounds();
        this.h.set((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        return this.h;
    }

    @DexIgnore
    public dj3(hj3 hj3) {
        this(new c(hj3, (rh3) null));
    }

    @DexIgnore
    public void d(float f2) {
        c cVar = this.a;
        if (cVar.n != f2) {
            cVar.n = f2;
            C();
        }
    }

    @DexIgnore
    public dj3(c cVar) {
        this.b = new jj3.g[4];
        this.c = new jj3.g[4];
        this.e = new Matrix();
        this.f = new Path();
        this.g = new Path();
        this.h = new RectF();
        this.i = new RectF();
        this.j = new Region();
        this.o = new Region();
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new vi3();
        this.u = new ij3();
        this.y = new RectF();
        this.a = cVar;
        this.r.setStyle(Paint.Style.STROKE);
        this.q.setStyle(Paint.Style.FILL);
        z.setColor(-1);
        z.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        B();
        a(getState());
        this.t = new a();
    }

    @DexIgnore
    public void b(float f2) {
        c cVar = this.a;
        if (cVar.o != f2) {
            cVar.o = f2;
            C();
        }
    }

    @DexIgnore
    public void c(int i2) {
        c cVar = this.a;
        if (cVar.t != i2) {
            cVar.t = i2;
            x();
        }
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        int k = k();
        int l = l();
        if (Build.VERSION.SDK_INT < 21) {
            Rect clipBounds = canvas.getClipBounds();
            int i2 = this.a.r;
            clipBounds.inset(-i2, -i2);
            clipBounds.offset(k, l);
            canvas.clipRect(clipBounds, Region.Op.REPLACE);
        }
        canvas.translate((float) k, (float) l);
    }

    @DexIgnore
    public void b(int i2) {
        this.s.a(i2);
        this.a.u = false;
        x();
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        a(canvas, this.r, this.g, this.p, f());
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.d != colorStateList) {
            cVar.d = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        a(canvas, this.q, this.f, this.a.a, e());
    }

    @DexIgnore
    public float c() {
        return this.a.a.c().a(e());
    }

    @DexIgnore
    public final void b(RectF rectF, Path path) {
        ij3 ij3 = this.u;
        c cVar = this.a;
        hj3 hj3 = cVar.a;
        float f2 = cVar.k;
        ij3.a(hj3, f2, rectF, this.t, path);
    }

    @DexIgnore
    public void a(float f2, int i2) {
        e(f2);
        b(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public final void b() {
        this.p = n().a((hj3.c) new b(this, -o()));
        this.u.a(this.p, this.a.k, f(), this.g);
    }

    @DexIgnore
    public void a(float f2, ColorStateList colorStateList) {
        e(f2);
        b(colorStateList);
    }

    @DexIgnore
    public float d() {
        return this.a.a.e().a(e());
    }

    @DexIgnore
    public void a(float f2) {
        setShapeAppearanceModel(this.a.a.a(f2));
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        c cVar = this.a;
        if (cVar.i == null) {
            cVar.i = new Rect();
        }
        this.a.i.set(i2, i3, i4, i5);
        this.x = this.a.i;
        invalidateSelf();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Drawable.ConstantState {
        @DexIgnore
        public hj3 a;
        @DexIgnore
        public rh3 b;
        @DexIgnore
        public ColorFilter c;
        @DexIgnore
        public ColorStateList d; // = null;
        @DexIgnore
        public ColorStateList e; // = null;
        @DexIgnore
        public ColorStateList f; // = null;
        @DexIgnore
        public ColorStateList g; // = null;
        @DexIgnore
        public PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
        @DexIgnore
        public Rect i; // = null;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = 1.0f;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m; // = 255;
        @DexIgnore
        public float n; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float o; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float p; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public int q; // = 0;
        @DexIgnore
        public int r; // = 0;
        @DexIgnore
        public int s; // = 0;
        @DexIgnore
        public int t; // = 0;
        @DexIgnore
        public boolean u; // = false;
        @DexIgnore
        public Paint.Style v; // = Paint.Style.FILL_AND_STROKE;

        @DexIgnore
        public c(hj3 hj3, rh3 rh3) {
            this.a = hj3;
            this.b = rh3;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            dj3 dj3 = new dj3(this, (a) null);
            boolean unused = dj3.d = true;
            return dj3;
        }

        @DexIgnore
        public c(c cVar) {
            this.a = cVar.a;
            this.b = cVar.b;
            this.l = cVar.l;
            this.c = cVar.c;
            this.d = cVar.d;
            this.e = cVar.e;
            this.h = cVar.h;
            this.g = cVar.g;
            this.m = cVar.m;
            this.j = cVar.j;
            this.s = cVar.s;
            this.q = cVar.q;
            this.u = cVar.u;
            this.k = cVar.k;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
            this.r = cVar.r;
            this.t = cVar.t;
            this.f = cVar.f;
            this.v = cVar.v;
            Rect rect = cVar.i;
            if (rect != null) {
                this.i = new Rect(rect);
            }
        }
    }

    @DexIgnore
    public void a(Context context) {
        this.a.b = new rh3(context);
        C();
    }

    @DexIgnore
    public final int a(int i2) {
        float t2 = t() + j();
        rh3 rh3 = this.a.b;
        return rh3 != null ? rh3.b(i2, t2) : i2;
    }

    @DexIgnore
    public void a(Paint.Style style) {
        this.a.v = style;
        x();
    }

    @DexIgnore
    public void a(Canvas canvas, Paint paint, Path path, RectF rectF) {
        a(canvas, paint, path, this.a.a, rectF);
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, Path path, hj3 hj3, RectF rectF) {
        if (hj3.a(rectF)) {
            float a2 = hj3.l().a(rectF);
            canvas.drawRoundRect(rectF, a2, a2, paint);
            return;
        }
        canvas.drawPath(path, paint);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        if (this.a.s != 0) {
            canvas.drawPath(this.f, this.s.a());
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.b[i2].a(this.s, this.a.r, canvas);
            this.c[i2].a(this.s, this.a.r, canvas);
        }
        int k = k();
        int l = l();
        canvas.translate((float) (-k), (float) (-l));
        canvas.drawPath(this.f, z);
        canvas.translate((float) k, (float) l);
    }

    @DexIgnore
    public final void a(RectF rectF, Path path) {
        b(rectF, path);
        if (this.a.j != 1.0f) {
            this.e.reset();
            Matrix matrix = this.e;
            float f2 = this.a.j;
            matrix.setScale(f2, f2, rectF.width() / 2.0f, rectF.height() / 2.0f);
            path.transform(this.e);
        }
        path.computeBounds(this.y, true);
    }

    @DexIgnore
    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, Paint paint, boolean z2) {
        if (colorStateList == null || mode == null) {
            return a(paint, z2);
        }
        return a(colorStateList, mode, z2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r2 = r2.getColor();
     */
    @DexIgnore
    public final PorterDuffColorFilter a(Paint paint, boolean z2) {
        int color;
        int a2;
        if (!z2 || (a2 = a(color)) == color) {
            return null;
        }
        return new PorterDuffColorFilter(a2, PorterDuff.Mode.SRC_IN);
    }

    @DexIgnore
    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, boolean z2) {
        int colorForState = colorStateList.getColorForState(getState(), 0);
        if (z2) {
            colorForState = a(colorForState);
        }
        return new PorterDuffColorFilter(colorForState, mode);
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        boolean z2;
        int color;
        int colorForState;
        int color2;
        int colorForState2;
        if (this.a.d == null || (color2 = this.q.getColor()) == (colorForState2 = this.a.d.getColorForState(iArr, color2))) {
            z2 = false;
        } else {
            this.q.setColor(colorForState2);
            z2 = true;
        }
        if (this.a.e == null || (color = this.r.getColor()) == (colorForState = this.a.e.getColorForState(iArr, color))) {
            return z2;
        }
        this.r.setColor(colorForState);
        return true;
    }
}
