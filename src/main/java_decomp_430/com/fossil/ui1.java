package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ui1 extends x51 {
    @DexIgnore
    public ui1(ue1 ue1, q41 q41, eh1 eh1, HashMap<io0, Object> hashMap, String str) {
        super(ue1, q41, eh1, lk1.b.a(ue1.t, w31.DATA_COLLECTION_FILE), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
    }

    @DexIgnore
    public JSONObject k() {
        JSONArray jSONArray = new JSONArray();
        Iterator<ie1> it = this.G.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().a(false));
        }
        JSONObject put = super.k().put(cw0.a((Enum<?>) io0.SKIP_ERASE), this.O);
        wg6.a(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        return cw0.a(put, bm0.FILES, (Object) jSONArray);
    }

    @DexIgnore
    public byte[][] d() {
        ArrayList<ie1> arrayList = this.G;
        ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
        for (ie1 ie1 : arrayList) {
            arrayList2.add(ie1.e);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
