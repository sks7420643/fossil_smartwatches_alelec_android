package com.fossil;

import com.fossil.d15;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy4$h$c implements m24.e<d15.a, m24.a> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.h a;

    @DexIgnore
    public oy4$h$c(NotificationCallsAndMessagesPresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(d15.a aVar) {
        wg6.b(aVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetApps onSuccess");
        this.a.this$0.p().clear();
        this.a.this$0.p().addAll(aVar.a());
    }

    @DexIgnore
    public void a(CoroutineUseCase.a aVar) {
        wg6.b(aVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetApps onError");
    }
}
