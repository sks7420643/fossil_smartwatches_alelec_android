package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rg1 {
    UNKNOWN("unknown", r2),
    MODEL_NUMBER("model_number", r2),
    SERIAL_NUMBER("serial_number", r2),
    FIRMWARE_VERSION("firmware_version", r2),
    SOFTWARE_REVISION("software_revision", r2),
    DC("device_config", r2),
    FTC("file_transfer_control", r2),
    FTD("file_transfer_data", (byte) 0),
    ASYNC("async", r2),
    FTD_1("file_transfer_data_1", (byte) 1),
    AUTHENTICATION("authentication", r2),
    HEART_RATE("heart_rate", r2);
    
    @DexIgnore
    public static /* final */ ad1 p; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        p = new ad1((qg6) null);
    }
    */

    @DexIgnore
    public rg1(String str, byte b2) {
        this.a = str;
        this.b = b2;
    }

    @DexIgnore
    public final hl1 a() {
        switch (ve1.a[ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                return hl1.DEVICE_INFORMATION;
            case 5:
                return hl1.DEVICE_CONFIG;
            case 6:
                return hl1.FILE_CONFIG;
            case 7:
            case 8:
                return hl1.TRANSFER_DATA;
            case 9:
                return hl1.AUTHENTICATION;
            case 10:
                return hl1.ASYNC;
            default:
                return hl1.UNKNOWN;
        }
    }
}
