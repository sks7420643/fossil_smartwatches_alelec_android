package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn6 extends jk6 {
    @DexIgnore
    public /* final */ jo6 a;

    @DexIgnore
    public hn6(jo6 jo6) {
        wg6.b(jo6, "node");
        this.a = jo6;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.a.j();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "RemoveOnCancel[" + this.a + ']';
    }
}
