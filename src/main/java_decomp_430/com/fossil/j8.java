package com.fossil;

import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import androidx.collection.SimpleArrayMap;
import com.fossil.d7;
import com.fossil.k8;
import com.fossil.wearables.fsl.fitness.FitnessProviderImpl;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j8 {
    @DexIgnore
    public static /* final */ t4<String, Typeface> a; // = new t4<>(16);
    @DexIgnore
    public static /* final */ k8 b; // = new k8("fonts", 10, FitnessProviderImpl.DEFAULT_DAILY_STEP_GOAL);
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ SimpleArrayMap<String, ArrayList<k8.d<g>>> d; // = new SimpleArrayMap<>();
    @DexIgnore
    public static /* final */ Comparator<byte[]> e; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Callable<g> {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ i8 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;

        @DexIgnore
        public a(Context context, i8 i8Var, int i, String str) {
            this.a = context;
            this.b = i8Var;
            this.c = i;
            this.d = str;
        }

        @DexIgnore
        public g call() throws Exception {
            g a2 = j8.a(this.a, this.b, this.c);
            Typeface typeface = a2.a;
            if (typeface != null) {
                j8.a.a(this.d, typeface);
            }
            return a2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements k8.d<g> {
        @DexIgnore
        public /* final */ /* synthetic */ d7.a a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;

        @DexIgnore
        public b(d7.a aVar, Handler handler) {
            this.a = aVar;
            this.b = handler;
        }

        @DexIgnore
        public void a(g gVar) {
            if (gVar == null) {
                this.a.a(1, this.b);
                return;
            }
            int i = gVar.b;
            if (i == 0) {
                this.a.a(gVar.a, this.b);
            } else {
                this.a.a(i, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements k8.d<g> {
        @DexIgnore
        public /* final */ /* synthetic */ String a;

        @DexIgnore
        public c(String str) {
            this.a = str;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
            if (r0 >= r1.size()) goto L_0x002c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
            ((com.fossil.k8.d) r1.get(r0)).a(r5);
            r0 = r0 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
            r0 = 0;
         */
        @DexIgnore
        public void a(g gVar) {
            synchronized (j8.c) {
                ArrayList arrayList = j8.d.get(this.a);
                if (arrayList != null) {
                    j8.d.remove(this.a);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Comparator<byte[]> {
        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            int i;
            int i2;
            if (bArr.length != bArr2.length) {
                i2 = bArr.length;
                i = bArr2.length;
            } else {
                int i3 = 0;
                while (i3 < bArr.length) {
                    if (bArr[i3] != bArr2[i3]) {
                        i2 = bArr[i3];
                        i = bArr2[i3];
                    } else {
                        i3++;
                    }
                }
                return 0;
            }
            return i2 - i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ f[] b;

        @DexIgnore
        public e(int i, f[] fVarArr) {
            this.a = i;
            this.b = fVarArr;
        }

        @DexIgnore
        public f[] a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        public f(Uri uri, int i, int i2, boolean z, int i3) {
            y8.a(uri);
            this.a = uri;
            this.b = i;
            this.c = i2;
            this.d = z;
            this.e = i3;
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public Uri c() {
            return this.a;
        }

        @DexIgnore
        public int d() {
            return this.c;
        }

        @DexIgnore
        public boolean e() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public g(Typeface typeface, int i) {
            this.a = typeface;
            this.b = i;
        }
    }

    @DexIgnore
    public static g a(Context context, i8 i8Var, int i) {
        try {
            e a2 = a(context, (CancellationSignal) null, i8Var);
            int i2 = -3;
            if (a2.b() == 0) {
                Typeface a3 = h7.a(context, (CancellationSignal) null, a2.a(), i);
                if (a3 != null) {
                    i2 = 0;
                }
                return new g(a3, i2);
            }
            if (a2.b() == 1) {
                i2 = -2;
            }
            return new g((Typeface) null, i2);
        } catch (PackageManager.NameNotFoundException unused) {
            return new g((Typeface) null, -1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0072, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
        b.a(r1, new com.fossil.j8.c(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008d, code lost:
        return null;
     */
    @DexIgnore
    public static Typeface a(Context context, i8 i8Var, d7.a aVar, Handler handler, boolean z, int i, int i2) {
        b bVar;
        String str = i8Var.c() + "-" + i2;
        Typeface b2 = a.b(str);
        if (b2 != null) {
            if (aVar != null) {
                aVar.a(b2);
            }
            return b2;
        } else if (!z || i != -1) {
            a aVar2 = new a(context, i8Var, i2, str);
            if (z) {
                try {
                    return ((g) b.a(aVar2, i)).a;
                } catch (InterruptedException unused) {
                    return null;
                }
            } else {
                if (aVar == null) {
                    bVar = null;
                } else {
                    bVar = new b(aVar, handler);
                }
                synchronized (c) {
                    ArrayList arrayList = d.get(str);
                    if (arrayList != null) {
                        if (bVar != null) {
                            arrayList.add(bVar);
                        }
                    } else if (bVar != null) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(bVar);
                        d.put(str, arrayList2);
                    }
                }
            }
        } else {
            g a2 = a(context, i8Var, i2);
            if (aVar != null) {
                int i3 = a2.b;
                if (i3 == 0) {
                    aVar.a(a2.a, handler);
                } else {
                    aVar.a(i3, handler);
                }
            }
            return a2.a;
        }
    }

    @DexIgnore
    public static Map<Uri, ByteBuffer> a(Context context, f[] fVarArr, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (f fVar : fVarArr) {
            if (fVar.a() == 0) {
                Uri c2 = fVar.c();
                if (!hashMap.containsKey(c2)) {
                    hashMap.put(c2, n7.a(context, cancellationSignal, c2));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    @DexIgnore
    public static e a(Context context, CancellationSignal cancellationSignal, i8 i8Var) throws PackageManager.NameNotFoundException {
        ProviderInfo a2 = a(context.getPackageManager(), i8Var, context.getResources());
        if (a2 == null) {
            return new e(1, (f[]) null);
        }
        return new e(0, a(context, i8Var, a2.authority, cancellationSignal));
    }

    @DexIgnore
    public static ProviderInfo a(PackageManager packageManager, i8 i8Var, Resources resources) throws PackageManager.NameNotFoundException {
        String d2 = i8Var.d();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(d2, 0);
        if (resolveContentProvider == null) {
            throw new PackageManager.NameNotFoundException("No package found for authority: " + d2);
        } else if (resolveContentProvider.packageName.equals(i8Var.e())) {
            List<byte[]> a2 = a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort(a2, e);
            List<List<byte[]>> a3 = a(i8Var, resources);
            for (int i = 0; i < a3.size(); i++) {
                ArrayList arrayList = new ArrayList(a3.get(i));
                Collections.sort(arrayList, e);
                if (a(a2, (List<byte[]>) arrayList)) {
                    return resolveContentProvider;
                }
            }
            return null;
        } else {
            throw new PackageManager.NameNotFoundException("Found content provider " + d2 + ", but package was not " + i8Var.e());
        }
    }

    @DexIgnore
    public static List<List<byte[]>> a(i8 i8Var, Resources resources) {
        if (i8Var.a() != null) {
            return i8Var.a();
        }
        return a7.a(resources, i8Var.b());
    }

    @DexIgnore
    public static boolean a(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static List<byte[]> a(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature byteArray : signatureArr) {
            arrayList.add(byteArray.toByteArray());
        }
        return arrayList;
    }

    @DexIgnore
    public static f[] a(Context context, i8 i8Var, String str, CancellationSignal cancellationSignal) {
        Uri uri;
        Cursor query;
        String str2 = str;
        ArrayList arrayList = new ArrayList();
        Uri build = new Uri.Builder().scheme("content").authority(str2).build();
        Uri build2 = new Uri.Builder().scheme("content").authority(str2).appendPath("file").build();
        Cursor cursor = null;
        try {
            if (Build.VERSION.SDK_INT > 16) {
                query = context.getContentResolver().query(build, new String[]{"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new String[]{i8Var.f()}, (String) null, cancellationSignal);
            } else {
                query = context.getContentResolver().query(build, new String[]{"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new String[]{i8Var.f()}, (String) null);
            }
            if (cursor != null && cursor.getCount() > 0) {
                int columnIndex = cursor.getColumnIndex("result_code");
                ArrayList arrayList2 = new ArrayList();
                int columnIndex2 = cursor.getColumnIndex("_id");
                int columnIndex3 = cursor.getColumnIndex("file_id");
                int columnIndex4 = cursor.getColumnIndex("font_ttc_index");
                int columnIndex5 = cursor.getColumnIndex("font_weight");
                int columnIndex6 = cursor.getColumnIndex("font_italic");
                while (cursor.moveToNext()) {
                    int i = columnIndex != -1 ? cursor.getInt(columnIndex) : 0;
                    int i2 = columnIndex4 != -1 ? cursor.getInt(columnIndex4) : 0;
                    if (columnIndex3 == -1) {
                        uri = ContentUris.withAppendedId(build, cursor.getLong(columnIndex2));
                    } else {
                        uri = ContentUris.withAppendedId(build2, cursor.getLong(columnIndex3));
                    }
                    arrayList2.add(new f(uri, i2, columnIndex5 != -1 ? cursor.getInt(columnIndex5) : 400, columnIndex6 != -1 && cursor.getInt(columnIndex6) == 1, i));
                }
                arrayList = arrayList2;
            }
            return (f[]) arrayList.toArray(new f[0]);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
