package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kd4 extends jd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public long I;

    /*
    static {
        K.put(2131363218, 1);
        K.put(2131362542, 2);
        K.put(2131363275, 3);
        K.put(2131362980, 4);
        K.put(2131361971, 5);
        K.put(2131363280, 6);
        K.put(2131362479, 7);
        K.put(2131361987, 8);
        K.put(2131361988, 9);
        K.put(2131361985, 10);
        K.put(2131361986, 11);
        K.put(2131362355, 12);
        K.put(2131362675, 13);
        K.put(2131362236, 14);
        K.put(2131362358, 15);
        K.put(2131362689, 16);
        K.put(2131362243, 17);
        K.put(2131362429, 18);
        K.put(2131362244, 19);
        K.put(2131362430, 20);
        K.put(2131361907, 21);
        K.put(2131362323, 22);
        K.put(2131363276, 23);
    }
    */

    @DexIgnore
    public kd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 24, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public kd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[21], objArr[5], objArr[10], objArr[11], objArr[8], objArr[9], objArr[14], objArr[17], objArr[19], objArr[22], objArr[12], objArr[15], objArr[18], objArr[20], objArr[7], objArr[2], objArr[13], objArr[16], objArr[0], objArr[4], objArr[1], objArr[3], objArr[23], objArr[6]);
        this.I = -1;
        this.G.setTag((Object) null);
        a(view);
        f();
    }
}
