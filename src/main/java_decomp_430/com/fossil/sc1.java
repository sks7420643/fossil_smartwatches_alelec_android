package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc1 {
    @DexIgnore
    public static /* final */ sc1 a; // = new sc1();

    /* JADX WARNING: Can't wrap try/catch for region: R(2:15|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        com.fossil.tj6.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        if (r2 == null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003c, code lost:
        r1 = r2;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x002e */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003f A[SYNTHETIC, Splitter:B:26:0x003f] */
    public final String a(File file) {
        InputStreamReader inputStreamReader;
        InputStreamReader inputStreamReader2 = null;
        if (!file.exists()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try {
            inputStreamReader = new InputStreamReader(new FileInputStream(file));
            char[] cArr = new char[1024];
            for (int read = inputStreamReader.read(cArr); read != -1; read = inputStreamReader.read(cArr)) {
                sb.append(cArr, 0, read);
            }
        } catch (IOException unused) {
            inputStreamReader = null;
        } catch (Throwable th) {
            th = th;
            if (inputStreamReader2 != null) {
                try {
                    inputStreamReader2.close();
                } catch (IOException unused2) {
                }
            }
            throw th;
        }
        try {
            inputStreamReader.close();
        } catch (IOException unused3) {
        }
        return sb.toString();
    }
}
