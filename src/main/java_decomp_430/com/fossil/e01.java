package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class e01 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[s60.values().length];

    /*
    static {
        a[s60.BIOMETRIC_PROFILE.ordinal()] = 1;
        a[s60.DAILY_STEP.ordinal()] = 2;
        a[s60.DAILY_STEP_GOAL.ordinal()] = 3;
        a[s60.DAILY_CALORIE.ordinal()] = 4;
        a[s60.DAILY_CALORIE_GOAL.ordinal()] = 5;
        a[s60.DAILY_TOTAL_ACTIVE_MINUTE.ordinal()] = 6;
        a[s60.DAILY_ACTIVE_MINUTE_GOAL.ordinal()] = 7;
        a[s60.DAILY_DISTANCE.ordinal()] = 8;
        a[s60.INACTIVE_NUDGE.ordinal()] = 9;
        a[s60.VIBE_STRENGTH.ordinal()] = 10;
        a[s60.DO_NOT_DISTURB_SCHEDULE.ordinal()] = 11;
        a[s60.TIME.ordinal()] = 12;
        a[s60.BATTERY.ordinal()] = 13;
        a[s60.HEART_RATE_MODE.ordinal()] = 14;
        a[s60.DAILY_SLEEP.ordinal()] = 15;
        a[s60.DISPLAY_UNIT.ordinal()] = 16;
        a[s60.SECOND_TIMEZONE_OFFSET.ordinal()] = 17;
        a[s60.CURRENT_HEART_RATE.ordinal()] = 18;
        a[s60.HELLAS_BATTERY.ordinal()] = 19;
    }
    */
}
