package com.fossil;

import com.portfolio.platform.uirenew.home.HomeFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw4 implements MembersInjector<HomeFragment> {
    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeDashboardPresenter homeDashboardPresenter) {
        homeFragment.g = homeDashboardPresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
        homeFragment.h = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
        homeFragment.i = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeProfilePresenter homeProfilePresenter) {
        homeFragment.j = homeProfilePresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeAlertsPresenter homeAlertsPresenter) {
        homeFragment.o = homeAlertsPresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        homeFragment.p = homeAlertsHybridPresenter;
    }

    @DexIgnore
    public static void a(HomeFragment homeFragment, jj5 jj5) {
        homeFragment.q = jj5;
    }
}
