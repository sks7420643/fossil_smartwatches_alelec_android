package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.HorizontalScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public final class xz5 extends HorizontalScrollView {
    @DexIgnore
    public long a; // = -1;
    @DexIgnore
    public b b;
    @DexIgnore
    public Runnable c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            xz5 xz5 = xz5.this;
            if (currentTimeMillis - xz5.a > 100) {
                xz5.a = -1;
                xz5.b.a();
                return;
            }
            xz5.postDelayed(this, 100);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void onScrollChanged();
    }

    @DexIgnore
    public xz5(Context context, b bVar) {
        super(context);
        this.b = bVar;
    }

    @DexIgnore
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        b bVar = this.b;
        if (bVar != null) {
            bVar.onScrollChanged();
            if (this.a == -1) {
                postDelayed(this.c, 100);
            }
            this.a = System.currentTimeMillis();
        }
    }
}
