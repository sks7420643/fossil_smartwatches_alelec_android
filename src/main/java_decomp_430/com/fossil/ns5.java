package com.fossil;

import com.misfit.frameworks.buttonservice.model.ShineDevice;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ns5 extends xt4<ks5> {
    @DexIgnore
    void B();

    @DexIgnore
    void S();

    @DexIgnore
    boolean U();

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i, String str, String str2);

    @DexIgnore
    void a(String str, boolean z);

    @DexIgnore
    void a(String str, boolean z, boolean z2);

    @DexIgnore
    void b();

    @DexIgnore
    void b(String str, boolean z);

    @DexIgnore
    void c(int i, String str);

    @DexIgnore
    void g(List<lc6<ShineDevice, String>> list);

    @DexIgnore
    void h();

    @DexIgnore
    void k(List<String> list);

    @DexIgnore
    void l(String str);

    @DexIgnore
    void l(List<lc6<ShineDevice, String>> list);

    @DexIgnore
    void n(String str);

    @DexIgnore
    void o();

    @DexIgnore
    void o(String str);

    @DexIgnore
    void q(boolean z);

    @DexIgnore
    void s(boolean z);

    @DexIgnore
    void t();

    @DexIgnore
    void v(String str);

    @DexIgnore
    void y();
}
