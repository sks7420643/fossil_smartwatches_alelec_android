package com.fossil;

import com.facebook.share.internal.VideoUploader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum cd0 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop");
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final cd0 a(String str) {
            for (cd0 cd0 : cd0.values()) {
                if (wg6.a(cd0.a(), str)) {
                    return cd0;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public cd0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }
}
