package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v02 {
    @DexIgnore
    public /* final */ DataHolder a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public v02(DataHolder dataHolder, int i) {
        w12.a(dataHolder);
        this.a = dataHolder;
        a(i);
    }

    @DexIgnore
    public final void a(int i) {
        w12.b(i >= 0 && i < this.a.getCount());
        this.b = i;
        this.c = this.a.c(this.b);
    }

    @DexIgnore
    public int b(String str) {
        return this.a.b(str, this.b, this.c);
    }

    @DexIgnore
    public String c(String str) {
        return this.a.c(str, this.b, this.c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof v02) {
            v02 v02 = (v02) obj;
            if (!u12.a(Integer.valueOf(v02.b), Integer.valueOf(this.b)) || !u12.a(Integer.valueOf(v02.c), Integer.valueOf(this.c)) || v02.a != this.a) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }

    @DexIgnore
    public byte[] a(String str) {
        return this.a.a(str, this.b, this.c);
    }
}
