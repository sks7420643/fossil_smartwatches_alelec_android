package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw2 implements Parcelable.Creator<qw2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        long j = -1;
        long j2 = -1;
        int i = 1;
        int i2 = 1;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                i2 = f22.q(parcel, a);
            } else if (a2 == 3) {
                j = f22.s(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                j2 = f22.s(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new qw2(i, i2, j, j2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new qw2[i];
    }
}
