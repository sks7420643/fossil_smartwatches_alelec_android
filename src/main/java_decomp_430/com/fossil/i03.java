package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i03 extends e22 implements Iterable<String> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<i03> CREATOR; // = new k03();
    @DexIgnore
    public /* final */ Bundle a;

    @DexIgnore
    public i03(Bundle bundle) {
        this.a = bundle;
    }

    @DexIgnore
    public final Object e(String str) {
        return this.a.get(str);
    }

    @DexIgnore
    public final Long f(String str) {
        return Long.valueOf(this.a.getLong(str));
    }

    @DexIgnore
    public final Double g(String str) {
        return Double.valueOf(this.a.getDouble(str));
    }

    @DexIgnore
    public final String h(String str) {
        return this.a.getString(str);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new h03(this);
    }

    @DexIgnore
    public final String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, zzb(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public final int zza() {
        return this.a.size();
    }

    @DexIgnore
    public final Bundle zzb() {
        return new Bundle(this.a);
    }
}
