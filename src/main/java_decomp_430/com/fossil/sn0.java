package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn0 extends uj1 {
    @DexIgnore
    public static /* final */ zl0 CREATOR; // = new zl0((qg6) null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public /* synthetic */ sn0(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.STREAM_DATA, (Object) Byte.valueOf(this.b));
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(sn0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((sn0) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.StreamingInstr");
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }
}
