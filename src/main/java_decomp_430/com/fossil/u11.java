package com.fossil;

import android.bluetooth.BluetoothDevice;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u11 {
    @DexIgnore
    public static /* final */ Hashtable<String, ue1> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ u11 b; // = new u11();

    @DexIgnore
    public final ue1 a(BluetoothDevice bluetoothDevice) {
        ue1 ue1;
        oa1 oa1 = oa1.a;
        new Object[1][0] = bluetoothDevice.getAddress();
        synchronized (a) {
            ue1 = a.get(bluetoothDevice.getAddress());
            if (ue1 == null) {
                ue1 = new ue1(bluetoothDevice, (qg6) null);
                a.put(ue1.w.getAddress(), ue1);
            }
        }
        return ue1;
    }
}
