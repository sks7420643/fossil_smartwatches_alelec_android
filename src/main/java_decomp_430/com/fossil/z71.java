package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z71 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ w91 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z71(w91 w91) {
        super(1);
        this.a = w91;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        qv0 qv0 = (qv0) obj;
        if1 if1 = this.a.a;
        km1 km1 = if1.v;
        if (km1.b == sk1.INTERRUPTED) {
            if1.a(km1);
        } else {
            if1.a(qv0.v);
        }
        return cd6.a;
    }
}
