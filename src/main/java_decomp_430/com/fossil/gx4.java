package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx4 implements Factory<ox4> {
    @DexIgnore
    public static ox4 a(ex4 ex4) {
        ox4 b = ex4.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
