package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ha6 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ da6 b;
    @DexIgnore
    public /* final */ ga6 c;

    @DexIgnore
    public ha6(da6 da6, ga6 ga6) {
        this(0, da6, ga6);
    }

    @DexIgnore
    public long a() {
        return this.b.a(this.a);
    }

    @DexIgnore
    public ha6 b() {
        return new ha6(this.b, this.c);
    }

    @DexIgnore
    public ha6 c() {
        return new ha6(this.a + 1, this.b, this.c);
    }

    @DexIgnore
    public ha6(int i, da6 da6, ga6 ga6) {
        this.a = i;
        this.b = da6;
        this.c = ga6;
    }
}
