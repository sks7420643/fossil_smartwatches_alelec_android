package com.fossil;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.fragment.app.Fragment;
import com.fossil.pw6;
import java.util.Arrays;
import pub.devrel.easypermissions.RationaleDialogFragmentCompat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sw6 implements DialogInterface.OnClickListener {
    @DexIgnore
    public Object a;
    @DexIgnore
    public tw6 b;
    @DexIgnore
    public pw6.a c;
    @DexIgnore
    public pw6.b d;

    @DexIgnore
    public sw6(RationaleDialogFragmentCompat rationaleDialogFragmentCompat, tw6 tw6, pw6.a aVar, pw6.b bVar) {
        Fragment fragment;
        if (rationaleDialogFragmentCompat.getParentFragment() != null) {
            fragment = rationaleDialogFragmentCompat.getParentFragment();
        } else {
            fragment = rationaleDialogFragmentCompat.getActivity();
        }
        this.a = fragment;
        this.b = tw6;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    public final void a() {
        pw6.a aVar = this.c;
        if (aVar != null) {
            tw6 tw6 = this.b;
            aVar.a(tw6.d, Arrays.asList(tw6.f));
        }
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        tw6 tw6 = this.b;
        int i2 = tw6.d;
        if (i == -1) {
            String[] strArr = tw6.f;
            pw6.b bVar = this.d;
            if (bVar != null) {
                bVar.a(i2);
            }
            Object obj = this.a;
            if (obj instanceof Fragment) {
                zw6.a((Fragment) obj).a(i2, strArr);
            } else if (obj instanceof Activity) {
                zw6.a((Activity) obj).a(i2, strArr);
            } else {
                throw new RuntimeException("Host must be an Activity or Fragment!");
            }
        } else {
            pw6.b bVar2 = this.d;
            if (bVar2 != null) {
                bVar2.b(i2);
            }
            a();
        }
    }

    @DexIgnore
    public sw6(uw6 uw6, tw6 tw6, pw6.a aVar, pw6.b bVar) {
        this.a = uw6.getActivity();
        this.b = tw6;
        this.c = aVar;
        this.d = bVar;
    }
}
