package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lk extends rj {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    public void a(tj tjVar) {
        View view = tjVar.b;
        Integer num = (Integer) tjVar.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        tjVar.a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        tjVar.a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    public int b(tj tjVar) {
        Integer num;
        if (tjVar == null || (num = (Integer) tjVar.a.get("android:visibilityPropagation:visibility")) == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int c(tj tjVar) {
        return a(tjVar, 0);
    }

    @DexIgnore
    public int d(tj tjVar) {
        return a(tjVar, 1);
    }

    @DexIgnore
    public String[] a() {
        return a;
    }

    @DexIgnore
    public static int a(tj tjVar, int i) {
        int[] iArr;
        if (tjVar == null || (iArr = (int[]) tjVar.a.get("android:visibilityPropagation:center")) == null) {
            return -1;
        }
        return iArr[i];
    }
}
