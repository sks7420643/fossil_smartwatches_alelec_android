package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l45$h$b<T> implements ld<List<? extends CustomizeRealData>> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.h a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ l45$h$b this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.l45$h$b$a$a")
        /* renamed from: com.fossil.l45$h$b$a$a  reason: collision with other inner class name */
        public static final class C0023a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0023a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0023a aVar = new C0023a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0023a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    return this.this$0.this$0.a.this$0.B.getCurrentUser();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends sf6 implements ig6<il6, xe6<? super List<? extends m35>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                b bVar = new b(this.this$0, xe6);
                bVar.p$ = (il6) obj;
                return bVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                xp6 xp6;
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    il6 il6 = this.p$;
                    xp6 m = this.this$0.this$0.a.this$0.p;
                    this.L$0 = il6;
                    this.L$1 = m;
                    this.label = 1;
                    if (m.a((Object) null, this) == a) {
                        return a;
                    }
                    xp6 = m;
                } else if (i == 1) {
                    xp6 = (xp6) this.L$1;
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                try {
                    CopyOnWriteArrayList<DianaPreset> n = this.this$0.this$0.a.this$0.i;
                    ArrayList arrayList = new ArrayList(rd6.a(n, 10));
                    for (DianaPreset dianaPreset : n) {
                        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0.this$0.a.this$0;
                        wg6.a((Object) dianaPreset, "it");
                        arrayList.add(homeDianaCustomizePresenter.a(dianaPreset));
                    }
                    return arrayList;
                } finally {
                    xp6.a((Object) null);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends sf6 implements ig6<il6, xe6<? super DianaComplicationRingStyle>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                c cVar = new c(this.this$0, xe6);
                cVar.p$ = (il6) obj;
                return cVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    RingStyleRepository p = this.this$0.this$0.a.this$0.w;
                    Object a = this.this$0.this$0.a.this$0.k.a();
                    if (a != null) {
                        wg6.a(a, "mSerialLiveData.value!!");
                        return p.getRingStylesBySerial((String) a);
                    }
                    wg6.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(List list, xe6 xe6, l45$h$b l45_h_b) {
            super(2, xe6);
            this.$it = list;
            this.this$0 = l45_h_b;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.$it, xe6, this.this$0);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0123 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0124  */
        public final Object invokeSuspend(Object obj) {
            List list;
            HomeDianaCustomizePresenter homeDianaCustomizePresenter;
            MFUser mFUser;
            il6 il6;
            Object a;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                dl6 c2 = this.this$0.a.this$0.b();
                C0023a aVar = new C0023a(this, (xe6) null);
                this.L$0 = il62;
                this.label = 1;
                Object a3 = gk6.a(c2, aVar, this);
                if (a3 == a2) {
                    return a2;
                }
                Object obj2 = a3;
                il6 = il62;
                obj = obj2;
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                nc6.a(obj);
                il6 il63 = (il6) this.L$0;
                mFUser = (MFUser) this.L$1;
                il6 = il63;
                List list2 = (List) obj;
                HomeDianaCustomizePresenter homeDianaCustomizePresenter2 = this.this$0.a.this$0;
                dl6 d = homeDianaCustomizePresenter2.c();
                c cVar = new c(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = mFUser;
                this.L$2 = list2;
                this.L$3 = homeDianaCustomizePresenter2;
                this.label = 3;
                a = gk6.a(d, cVar, this);
                if (a != a2) {
                    return a2;
                }
                homeDianaCustomizePresenter = homeDianaCustomizePresenter2;
                Object obj3 = a;
                list = list2;
                obj = obj3;
                homeDianaCustomizePresenter.q = (DianaComplicationRingStyle) obj;
                this.this$0.a.this$0.t.a(list, this.this$0.a.this$0.q);
                return cd6.a;
            } else if (i == 3) {
                homeDianaCustomizePresenter = (HomeDianaCustomizePresenter) this.L$3;
                list = (List) this.L$2;
                MFUser mFUser2 = (MFUser) this.L$1;
                il6 il64 = (il6) this.L$0;
                nc6.a(obj);
                homeDianaCustomizePresenter.q = (DianaComplicationRingStyle) obj;
                this.this$0.a.this$0.t.a(list, this.this$0.a.this$0.q);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser3 = (MFUser) obj;
            if ((!wg6.a((Object) this.this$0.a.this$0.m, (Object) this.$it)) || (!wg6.a((Object) this.this$0.a.this$0.o, (Object) mFUser3))) {
                this.this$0.a.this$0.o = mFUser3;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDianaCustomizePresenter", "on real data change " + this.$it);
                this.this$0.a.this$0.m.clear();
                this.this$0.a.this$0.m.addAll(this.$it);
                if (true ^ this.this$0.a.this$0.i.isEmpty()) {
                    dl6 c3 = this.this$0.a.this$0.b();
                    b bVar = new b(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = mFUser3;
                    this.label = 2;
                    Object a4 = gk6.a(c3, bVar, this);
                    if (a4 == a2) {
                        return a2;
                    }
                    Object obj4 = a4;
                    mFUser = mFUser3;
                    obj = obj4;
                    List list22 = (List) obj;
                    HomeDianaCustomizePresenter homeDianaCustomizePresenter22 = this.this$0.a.this$0;
                    dl6 d2 = homeDianaCustomizePresenter22.c();
                    c cVar2 = new c(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = mFUser;
                    this.L$2 = list22;
                    this.L$3 = homeDianaCustomizePresenter22;
                    this.label = 3;
                    a = gk6.a(d2, cVar2, this);
                    if (a != a2) {
                    }
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public l45$h$b(HomeDianaCustomizePresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(List<CustomizeRealData> list) {
        if (list != null) {
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(list, (xe6) null, this), 3, (Object) null);
        }
    }
}
