package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn5 implements Factory<rn5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public sn5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static sn5 a(Provider<ThemeRepository> provider) {
        return new sn5(provider);
    }

    @DexIgnore
    public static rn5 b(Provider<ThemeRepository> provider) {
        return new CustomizeGoalTrackingChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeGoalTrackingChartViewModel get() {
        return b(this.a);
    }
}
