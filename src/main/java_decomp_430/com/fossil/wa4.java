package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wa4 extends va4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j E; // = new ViewDataBinding.j(26);
    @DexIgnore
    public static /* final */ SparseIntArray F; // = new SparseIntArray();
    @DexIgnore
    public long D;

    /*
    static {
        E.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558805});
        F.put(2131362293, 3);
        F.put(2131362292, 4);
        F.put(2131362649, 5);
        F.put(2131361955, 6);
        F.put(2131362865, 7);
        F.put(2131362402, 8);
        F.put(2131362669, 9);
        F.put(2131362300, 10);
        F.put(2131363267, 11);
        F.put(2131362333, 12);
        F.put(2131362419, 13);
        F.put(2131362989, 14);
        F.put(2131363268, 15);
        F.put(2131362069, 16);
        F.put(2131362422, 17);
        F.put(2131362687, 18);
        F.put(2131362423, 19);
        F.put(2131361909, 20);
        F.put(2131362420, 21);
        F.put(2131362686, 22);
        F.put(2131362421, 23);
        F.put(2131361910, 24);
        F.put(2131362395, 25);
    }
    */

    @DexIgnore
    public wa4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 26, E, F));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
        ViewDataBinding.d(this.w);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.w.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.D = 2;
        }
        this.w.f();
        g();
    }

    @DexIgnore
    public wa4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 1, objArr[20], objArr[24], objArr[6], objArr[1], objArr[16], objArr[4], objArr[3], objArr[10], objArr[12], objArr[25], objArr[8], objArr[13], objArr[21], objArr[23], objArr[17], objArr[19], objArr[2], objArr[5], objArr[9], objArr[22], objArr[18], objArr[0], objArr[7], objArr[14], objArr[11], objArr[15]);
        this.D = -1;
        this.t.setTag((Object) null);
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
