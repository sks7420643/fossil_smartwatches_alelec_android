package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.jv;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ov<Data> implements jv<Integer, Data> {
    @DexIgnore
    public /* final */ jv<Uri, Data> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements kv<Integer, AssetFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public a(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public jv<Integer, AssetFileDescriptor> a(nv nvVar) {
            return new ov(this.a, nvVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements kv<Integer, ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public b(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public jv<Integer, ParcelFileDescriptor> a(nv nvVar) {
            return new ov(this.a, nvVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements kv<Integer, InputStream> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public c(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public jv<Integer, InputStream> a(nv nvVar) {
            return new ov(this.a, nvVar.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements kv<Integer, Uri> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public d(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public jv<Integer, Uri> a(nv nvVar) {
            return new ov(this.a, rv.a());
        }
    }

    @DexIgnore
    public ov(Resources resources, jv<Uri, Data> jvVar) {
        this.b = resources;
        this.a = jvVar;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean a(Integer num) {
        return true;
    }

    @DexIgnore
    public jv.a<Data> a(Integer num, int i, int i2, xr xrVar) {
        Uri a2 = a(num);
        if (a2 == null) {
            return null;
        }
        return this.a.a(a2, i, i2, xrVar);
    }

    @DexIgnore
    public final Uri a(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }
}
