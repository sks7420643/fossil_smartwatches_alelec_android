package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s14 implements Factory<s24> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetDao> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<DeviceDao> f;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> g;

    @DexIgnore
    public s14(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
    }

    @DexIgnore
    public static s14 a(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return new s14(b14, provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static s24 b(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return a(b14, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public static s24 a(b14 b14, an4 an4, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        s24 a2 = b14.a(an4, userRepository, hybridPresetDao, notificationsRepository, deviceDao, portfolioApp);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public s24 get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
    }
}
