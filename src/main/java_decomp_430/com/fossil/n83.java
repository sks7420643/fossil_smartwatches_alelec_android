package com.fossil;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 b;
    @DexIgnore
    public /* final */ /* synthetic */ l83 c;

    @DexIgnore
    public n83(l83 l83, AtomicReference atomicReference, ra3 ra3) {
        this.c = l83;
        this.a = atomicReference;
        this.b = ra3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                l43 d = this.c.d;
                if (d == null) {
                    this.c.b().t().a("Failed to get app instance id");
                    this.a.notify();
                    return;
                }
                this.a.set(d.c(this.b));
                String str = (String) this.a.get();
                if (str != null) {
                    this.c.o().a(str);
                    this.c.k().l.a(str);
                }
                this.c.I();
                this.a.notify();
            } catch (RemoteException e) {
                try {
                    this.c.b().t().a("Failed to get app instance id", e);
                    this.a.notify();
                } catch (Throwable th) {
                    this.a.notify();
                    throw th;
                }
            }
        }
    }
}
