package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.cf;
import com.fossil.ue;
import com.fossil.zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class df<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ ue<T> a;
    @DexIgnore
    public /* final */ ue.e<T> b; // = new a();
    @DexIgnore
    public /* final */ cf.j c; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ue.e<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(cf<T> cfVar, cf<T> cfVar2) {
            df.this.a(cfVar2);
            df.this.a(cfVar, cfVar2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements cf.j {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(cf.l lVar, cf.i iVar, Throwable th) {
            df.this.a(lVar, iVar, th);
        }
    }

    @DexIgnore
    public df(zf.d<T> dVar) {
        this.a = new ue<>(this, dVar);
        this.a.a(this.b);
        this.a.a(this.c);
    }

    @DexIgnore
    public void a(cf.l lVar, cf.i iVar, Throwable th) {
    }

    @DexIgnore
    @Deprecated
    public void a(cf<T> cfVar) {
    }

    @DexIgnore
    public void a(cf<T> cfVar, cf<T> cfVar2) {
    }

    @DexIgnore
    public void b(cf<T> cfVar) {
        this.a.a(cfVar);
    }

    @DexIgnore
    public T getItem(int i) {
        return this.a.a(i);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.a();
    }
}
