package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l12 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public l12(String str, String str2) {
        w12.a(str, (Object) "log tag cannot be null");
        w12.a(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        this.a = str;
        if (str2 == null || str2.length() <= 0) {
            this.b = null;
        } else {
            this.b = str2;
        }
    }

    @DexIgnore
    public final boolean a(int i) {
        return Log.isLoggable(this.a, i);
    }

    @DexIgnore
    public final void b(String str, String str2) {
        if (a(6)) {
            Log.e(str, a(str2));
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        if (a(2)) {
            Log.v(str, a(str2));
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (a(3)) {
            Log.d(str, a(str2));
        }
    }

    @DexIgnore
    public final void a(String str, String str2, Throwable th) {
        if (a(6)) {
            Log.e(str, a(str2), th);
        }
    }

    @DexIgnore
    public final String a(String str) {
        String str2 = this.b;
        if (str2 == null) {
            return str;
        }
        return str2.concat(str);
    }

    @DexIgnore
    public l12(String str) {
        this(str, (String) null);
    }
}
