package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oe implements me {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public oe(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof oe)) {
            return false;
        }
        oe oeVar = (oe) obj;
        if (TextUtils.equals(this.a, oeVar.a) && this.b == oeVar.b && this.c == oeVar.c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return t8.a(this.a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
