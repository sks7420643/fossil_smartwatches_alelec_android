package com.fossil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co0 extends jb1<jd0[], byte[]> {
    @DexIgnore
    public static /* final */ u31<jd0[]>[] b; // = {new fh0(), new xi0()};
    @DexIgnore
    public static /* final */ ed1<byte[]>[] c; // = {new rk0(w31.ASSET), new km0(w31.ASSET)};
    @DexIgnore
    public static /* final */ co0 d; // = new co0();

    @DexIgnore
    public u31<jd0[]>[] a() {
        return b;
    }

    @DexIgnore
    public ed1<byte[]>[] b() {
        return c;
    }

    @DexIgnore
    public final byte[] a(jd0[] jd0Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ArrayList arrayList = new ArrayList();
        for (jd0 b2 : jd0Arr) {
            byte[] b3 = b2.b();
            if (b3 != null) {
                arrayList.add(b3);
            }
        }
        for (byte[] write : yd6.c(arrayList)) {
            byteArrayOutputStream.write(write);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final byte[] a(NotificationIcon[] notificationIconArr, short s, w40 w40) {
        if (notificationIconArr.length == 0) {
            return new byte[0];
        }
        return a(s, w40, notificationIconArr);
    }
}
