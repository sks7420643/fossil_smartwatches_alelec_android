package com.fossil;

import com.fossil.dt4;
import com.fossil.ft4;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo5 extends ap5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ bp5 f;
    @DexIgnore
    public /* final */ UpdateUser g;
    @DexIgnore
    public /* final */ dt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xo5.i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ xo5 a;
        @DexIgnore
        public /* final */ /* synthetic */ zh4 b;

        @DexIgnore
        public b(xo5 xo5, zh4 zh4) {
            this.a = xo5;
            this.b = zh4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            UserDisplayUnit a2;
            wg6.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.e(this.b);
            MFUser h = this.a.h();
            if (h != null && (a2 = xi4.a(h)) != null) {
                PortfolioApp.get.instance().a(a2, PortfolioApp.get.instance().e());
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ xo5 a;
        @DexIgnore
        public /* final */ /* synthetic */ zh4 b;

        @DexIgnore
        public c(xo5 xo5, zh4 zh4) {
            this.a = xo5;
            this.b = zh4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.c(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ xo5 a;
        @DexIgnore
        public /* final */ /* synthetic */ zh4 b;

        @DexIgnore
        public d(xo5 xo5, zh4 zh4) {
            this.a = xo5;
            this.b = zh4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            UserDisplayUnit a2;
            wg6.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.a(this.b);
            MFUser h = this.a.h();
            if (h != null && (a2 = xi4.a(h)) != null) {
                PortfolioApp.get.instance().a(a2, PortfolioApp.get.instance().e());
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ xo5 a;
        @DexIgnore
        public /* final */ /* synthetic */ zh4 b;

        @DexIgnore
        public e(xo5 xo5, zh4 zh4) {
            this.a = xo5;
            this.b = zh4;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.d(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements m24.e<dt4.a, m24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ xo5 a;

        @DexIgnore
        public f(xo5 xo5) {
            this.a = xo5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dt4.a aVar) {
            wg6.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(xo5.j.a(), "GetUser information onSuccess");
            this.a.f.a();
            this.a.a(aVar.a());
            if (this.a.f.isActive() && this.a.h() != null) {
                bp5 a2 = this.a.f;
                MFUser h = this.a.h();
                if (h != null) {
                    a2.c(h.getHeightUnit());
                    bp5 a3 = this.a.f;
                    MFUser h2 = this.a.h();
                    if (h2 != null) {
                        a3.d(h2.getWeightUnit());
                        bp5 a4 = this.a.f;
                        MFUser h3 = this.a.h();
                        if (h3 != null) {
                            a4.e(h3.getDistanceUnit());
                            bp5 a5 = this.a.f;
                            MFUser h4 = this.a.h();
                            if (h4 != null) {
                                a5.a(h4.getTemperatureUnit());
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wg6.b(aVar, "errorValue");
            this.a.f.a();
            FLogger.INSTANCE.getLocal().d(xo5.j.a(), "GetUser information onError");
        }
    }

    /*
    static {
        String simpleName = xo5.class.getSimpleName();
        wg6.a((Object) simpleName, "PreferredUnitPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public xo5(bp5 bp5, UpdateUser updateUser, dt4 dt4) {
        wg6.b(bp5, "mView");
        wg6.b(updateUser, "mUpdateUser");
        wg6.b(dt4, "mGetUser");
        this.f = bp5;
        this.g = updateUser;
        this.h = dt4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.xo5$c, com.portfolio.platform.CoroutineUseCase$e] */
    public void b(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setHeightUnit() called with: unit = [" + zh4 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save height with null user");
        } else if (mFUser != null) {
            mFUser.setHeightUnit(zh4.getValue());
            this.f.b();
            Object r0 = this.g;
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                r0.a(new UpdateUser.b(mFUser2), new c(this, zh4));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.fossil.xo5$d, com.portfolio.platform.CoroutineUseCase$e] */
    public void c(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setTemperatureUnit() called with: unit = [" + zh4 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setTemperatureUnit: unit = " + zh4);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            mFUser.setTemperatureUnit(zh4.getValue());
            this.f.b();
            Object r0 = this.g;
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                r0.a(new UpdateUser.b(mFUser2), new d(this, zh4));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.xo5$e, com.portfolio.platform.CoroutineUseCase$e] */
    public void d(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setWeightUnit() called with: unit = [" + zh4 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save weight with null user");
        } else if (mFUser != null) {
            mFUser.setWeightUnit(zh4.getValue());
            this.f.b();
            Object r0 = this.g;
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                r0.a(new UpdateUser.b(mFUser2), new e(this, zh4));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.dt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.xo5$f] */
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "presenter starts: Get user information");
        this.f.b();
        this.h.a(null, new f(this));
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    public final MFUser h() {
        return this.e;
    }

    @DexIgnore
    public void i() {
        this.f.a(this);
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.e = mFUser;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.xo5$b] */
    public void a(zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setDistanceUnit() called with: unit = [" + zh4 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setDistanceUnit: unit = " + zh4);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            mFUser.setDistanceUnit(zh4.getValue());
            this.f.b();
            Object r0 = this.g;
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                r0.a(new UpdateUser.b(mFUser2), new b(this, zh4));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
