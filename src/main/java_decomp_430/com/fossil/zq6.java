package com.fossil;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zq6 implements Closeable {
    @DexIgnore
    public Reader reader;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends zq6 {
        @DexIgnore
        public /* final */ /* synthetic */ uq6 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;
        @DexIgnore
        public /* final */ /* synthetic */ lt6 c;

        @DexIgnore
        public a(uq6 uq6, long j, lt6 lt6) {
            this.a = uq6;
            this.b = j;
            this.c = lt6;
        }

        @DexIgnore
        public long contentLength() {
            return this.b;
        }

        @DexIgnore
        public uq6 contentType() {
            return this.a;
        }

        @DexIgnore
        public lt6 source() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Reader {
        @DexIgnore
        public /* final */ lt6 a;
        @DexIgnore
        public /* final */ Charset b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public Reader d;

        @DexIgnore
        public b(lt6 lt6, Charset charset) {
            this.a = lt6;
            this.b = charset;
        }

        @DexIgnore
        public void close() throws IOException {
            this.c = true;
            Reader reader = this.d;
            if (reader != null) {
                reader.close();
            } else {
                this.a.close();
            }
        }

        @DexIgnore
        public int read(char[] cArr, int i, int i2) throws IOException {
            if (!this.c) {
                Reader reader = this.d;
                if (reader == null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(this.a.r(), fr6.a(this.a, this.b));
                    this.d = inputStreamReader;
                    reader = inputStreamReader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    @DexIgnore
    private Charset charset() {
        uq6 contentType = contentType();
        return contentType != null ? contentType.a(fr6.i) : fr6.i;
    }

    @DexIgnore
    public static zq6 create(uq6 uq6, String str) {
        Charset charset = fr6.i;
        if (uq6 != null && (charset = uq6.a()) == null) {
            charset = fr6.i;
            uq6 = uq6.b(uq6 + "; charset=utf-8");
        }
        jt6 jt6 = new jt6();
        jt6.a(str, charset);
        return create(uq6, jt6.p(), jt6);
    }

    @DexIgnore
    public final InputStream byteStream() {
        return source().r();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength <= 2147483647L) {
            lt6 source = source();
            try {
                byte[] e = source.e();
                fr6.a((Closeable) source);
                if (contentLength == -1 || contentLength == ((long) e.length)) {
                    return e;
                }
                throw new IOException("Content-Length (" + contentLength + ") and stream length (" + e.length + ") disagree");
            } catch (Throwable th) {
                fr6.a((Closeable) source);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + contentLength);
        }
    }

    @DexIgnore
    public final Reader charStream() {
        Reader reader2 = this.reader;
        if (reader2 != null) {
            return reader2;
        }
        b bVar = new b(source(), charset());
        this.reader = bVar;
        return bVar;
    }

    @DexIgnore
    public void close() {
        fr6.a((Closeable) source());
    }

    @DexIgnore
    public abstract long contentLength();

    @DexIgnore
    public abstract uq6 contentType();

    @DexIgnore
    public abstract lt6 source();

    @DexIgnore
    public final String string() throws IOException {
        lt6 source = source();
        try {
            return source.a(fr6.a(source, charset()));
        } finally {
            fr6.a((Closeable) source);
        }
    }

    @DexIgnore
    public static zq6 create(uq6 uq6, byte[] bArr) {
        jt6 jt6 = new jt6();
        jt6.write(bArr);
        return create(uq6, (long) bArr.length, jt6);
    }

    @DexIgnore
    public static zq6 create(uq6 uq6, mt6 mt6) {
        jt6 jt6 = new jt6();
        jt6.a(mt6);
        return create(uq6, (long) mt6.size(), jt6);
    }

    @DexIgnore
    public static zq6 create(uq6 uq6, long j, lt6 lt6) {
        if (lt6 != null) {
            return new a(uq6, j, lt6);
        }
        throw new NullPointerException("source == null");
    }
}
