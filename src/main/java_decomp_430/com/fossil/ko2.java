package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ko2 {
    @DexIgnore
    io2<?, ?> a(Object obj);

    @DexIgnore
    Object b(Object obj);

    @DexIgnore
    int zza(int i, Object obj, Object obj2);

    @DexIgnore
    Object zza(Object obj, Object obj2);

    @DexIgnore
    Map<?, ?> zza(Object obj);

    @DexIgnore
    Map<?, ?> zzb(Object obj);

    @DexIgnore
    boolean zzc(Object obj);

    @DexIgnore
    Object zzd(Object obj);
}
