package com.fossil;

import android.content.Context;
import com.fossil.n16;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x06 extends n16 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public x06(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(l16 l16) {
        return "content".equals(l16.d.getScheme());
    }

    @DexIgnore
    public InputStream c(l16 l16) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(l16.d);
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        return new n16.a(c(l16), Picasso.LoadedFrom.DISK);
    }
}
