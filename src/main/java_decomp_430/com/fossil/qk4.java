package com.fossil;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qk4 {
    @DexIgnore
    public static qk4 b;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public qk4() {
        WindowManager windowManager = (WindowManager) PortfolioApp.T.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.a = point.x;
            return;
        }
        this.a = 0;
    }

    @DexIgnore
    public static qk4 b() {
        if (b == null) {
            b = new qk4();
        }
        return b;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }
}
