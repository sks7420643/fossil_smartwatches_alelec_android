package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jf4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ hf4 r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public jf4(Object obj, View view, int i, ConstraintLayout constraintLayout, hf4 hf4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = hf4;
        a(this.r);
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
    }

    @DexIgnore
    public static jf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, kb.a());
    }

    @DexIgnore
    @Deprecated
    public static jf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return ViewDataBinding.a(layoutInflater, 2131558634, viewGroup, z, obj);
    }
}
