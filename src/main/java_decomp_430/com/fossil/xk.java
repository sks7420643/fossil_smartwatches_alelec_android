package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.g7;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xk extends wk {
    @DexIgnore
    public static /* final */ PorterDuff.Mode j; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public h b;
    @DexIgnore
    public PorterDuffColorFilter c;
    @DexIgnore
    public ColorFilter d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ float[] g;
    @DexIgnore
    public /* final */ Matrix h;
    @DexIgnore
    public /* final */ Rect i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (e7.a(xmlPullParser, "pathData")) {
                TypedArray a = e7.a(resources, theme, attributeSet, pk.d);
                a(a, xmlPullParser);
                a.recycle();
            }
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public b(b bVar) {
            super(bVar);
        }

        @DexIgnore
        public final void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.b = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.a = g7.a(string2);
            }
            this.c = e7.b(typedArray, xmlPullParser, "fillType", 2, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public g b;
        @DexIgnore
        public ColorStateList c;
        @DexIgnore
        public PorterDuff.Mode d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public ColorStateList g;
        @DexIgnore
        public PorterDuff.Mode h;
        @DexIgnore
        public int i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public Paint l;

        @DexIgnore
        public h(h hVar) {
            this.c = null;
            this.d = xk.j;
            if (hVar != null) {
                this.a = hVar.a;
                this.b = new g(hVar.b);
                Paint paint = hVar.b.e;
                if (paint != null) {
                    this.b.e = new Paint(paint);
                }
                Paint paint2 = hVar.b.d;
                if (paint2 != null) {
                    this.b.d = new Paint(paint2);
                }
                this.c = hVar.c;
                this.d = hVar.d;
                this.e = hVar.e;
            }
        }

        @DexIgnore
        public void a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f, (Rect) null, rect, a(colorFilter));
        }

        @DexIgnore
        public boolean b() {
            return this.b.getRootAlpha() < 255;
        }

        @DexIgnore
        public void c(int i2, int i3) {
            this.f.eraseColor(0);
            this.b.a(new Canvas(this.f), i2, i3, (ColorFilter) null);
        }

        @DexIgnore
        public void d() {
            this.g = this.c;
            this.h = this.d;
            this.i = this.b.getRootAlpha();
            this.j = this.e;
            this.k = false;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new xk(this);
        }

        @DexIgnore
        public void b(int i2, int i3) {
            if (this.f == null || !a(i2, i3)) {
                this.f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.k = true;
            }
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new xk(this);
        }

        @DexIgnore
        public Paint a(ColorFilter colorFilter) {
            if (!b() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                this.l = new Paint();
                this.l.setFilterBitmap(true);
            }
            this.l.setAlpha(this.b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        @DexIgnore
        public boolean c() {
            return this.b.a();
        }

        @DexIgnore
        public boolean a(int i2, int i3) {
            return i2 == this.f.getWidth() && i3 == this.f.getHeight();
        }

        @DexIgnore
        public boolean a() {
            return !this.k && this.g == this.c && this.h == this.d && this.j == this.e && this.i == this.b.getRootAlpha();
        }

        @DexIgnore
        public h() {
            this.c = null;
            this.d = xk.j;
            this.b = new g();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            boolean a2 = this.b.a(iArr);
            this.k |= a2;
            return a2;
        }
    }

    @DexIgnore
    public xk() {
        this.f = true;
        this.g = new float[9];
        this.h = new Matrix();
        this.i = new Rect();
        this.b = new h();
    }

    @DexIgnore
    public static xk createFromXmlInner(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        xk xkVar = new xk();
        xkVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return xkVar;
    }

    @DexIgnore
    public Object a(String str) {
        return this.b.b.p.get(str);
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.a;
        if (drawable == null) {
            return false;
        }
        o7.a(drawable);
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.i);
        if (this.i.width() > 0 && this.i.height() > 0) {
            ColorFilter colorFilter = this.d;
            if (colorFilter == null) {
                colorFilter = this.c;
            }
            canvas.getMatrix(this.h);
            this.h.getValues(this.g);
            float abs = Math.abs(this.g[0]);
            float abs2 = Math.abs(this.g[4]);
            float abs3 = Math.abs(this.g[1]);
            float abs4 = Math.abs(this.g[3]);
            if (!(abs3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && abs4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min(2048, (int) (((float) this.i.width()) * abs));
            int min2 = Math.min(2048, (int) (((float) this.i.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                Rect rect = this.i;
                canvas.translate((float) rect.left, (float) rect.top);
                if (a()) {
                    canvas.translate((float) this.i.width(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.i.offsetTo(0, 0);
                this.b.b(min, min2);
                if (!this.f) {
                    this.b.c(min, min2);
                } else if (!this.b.a()) {
                    this.b.c(min, min2);
                    this.b.d();
                }
                this.b.a(canvas, colorFilter, this.i);
                canvas.restoreToCount(save);
            }
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.c(drawable);
        }
        return this.b.b.getRootAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.getChangingConfigurations();
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.d(drawable);
        }
        return this.d;
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.a;
        if (drawable != null && Build.VERSION.SDK_INT >= 24) {
            return new i(drawable.getConstantState());
        }
        this.b.a = getChangingConfigurations();
        return this.b;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.b.b.j;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.b.b.i;
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
        }
    }

    @DexIgnore
    public void invalidateSelf() {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.a;
        if (drawable != null) {
            return o7.f(drawable);
        }
        return this.b.e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        r0 = r1.b.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
        r0 = r1.b;
     */
    @DexIgnore
    public boolean isStateful() {
        h hVar;
        ColorStateList colorStateList;
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return super.isStateful() || (hVar != null && (hVar.c() || (colorStateList != null && colorStateList.isStateful())));
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.e && super.mutate() == this) {
            this.b = new h(this.b);
            this.e = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        h hVar = this.b;
        ColorStateList colorStateList = hVar.c;
        if (!(colorStateList == null || (mode = hVar.d) == null)) {
            this.c = a(this.c, colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        if (!hVar.c() || !hVar.a(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.b.b.getRootAlpha() != i2) {
            this.b.b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, z);
        } else {
            this.b.e = z;
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.d = colorFilter;
        invalidateSelf();
    }

    @DexIgnore
    public void setTint(int i2) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.b(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, colorStateList);
            return;
        }
        h hVar = this.b;
        if (hVar.c != colorStateList) {
            hVar.c = colorStateList;
            this.c = a(this.c, colorStateList, hVar.d);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, mode);
            return;
        }
        h hVar = this.b;
        if (hVar.d != mode) {
            hVar.d = mode;
            this.c = a(this.c, hVar.c, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.a;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Drawable.ConstantState a;

        @DexIgnore
        public i(Drawable.ConstantState constantState) {
            this.a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            xk xkVar = new xk();
            xkVar.a = (VectorDrawable) this.a.newDrawable();
            return xkVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            xk xkVar = new xk();
            xkVar.a = (VectorDrawable) this.a.newDrawable(resources);
            return xkVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            xk xkVar = new xk();
            xkVar.a = (VectorDrawable) this.a.newDrawable(resources, theme);
            return xkVar;
        }
    }

    @DexIgnore
    public PorterDuffColorFilter a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f {
        @DexIgnore
        public int[] e;
        @DexIgnore
        public z6 f;
        @DexIgnore
        public float g; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public z6 h;
        @DexIgnore
        public float i; // = 1.0f;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float l; // = 1.0f;
        @DexIgnore
        public float m; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public Paint.Cap n; // = Paint.Cap.BUTT;
        @DexIgnore
        public Paint.Join o; // = Paint.Join.MITER;
        @DexIgnore
        public float p; // = 4.0f;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final Paint.Cap a(int i2, Paint.Cap cap) {
            if (i2 == 0) {
                return Paint.Cap.BUTT;
            }
            if (i2 != 1) {
                return i2 != 2 ? cap : Paint.Cap.SQUARE;
            }
            return Paint.Cap.ROUND;
        }

        @DexIgnore
        public float getFillAlpha() {
            return this.j;
        }

        @DexIgnore
        public int getFillColor() {
            return this.h.a();
        }

        @DexIgnore
        public float getStrokeAlpha() {
            return this.i;
        }

        @DexIgnore
        public int getStrokeColor() {
            return this.f.a();
        }

        @DexIgnore
        public float getStrokeWidth() {
            return this.g;
        }

        @DexIgnore
        public float getTrimPathEnd() {
            return this.l;
        }

        @DexIgnore
        public float getTrimPathOffset() {
            return this.m;
        }

        @DexIgnore
        public float getTrimPathStart() {
            return this.k;
        }

        @DexIgnore
        public void setFillAlpha(float f2) {
            this.j = f2;
        }

        @DexIgnore
        public void setFillColor(int i2) {
            this.h.a(i2);
        }

        @DexIgnore
        public void setStrokeAlpha(float f2) {
            this.i = f2;
        }

        @DexIgnore
        public void setStrokeColor(int i2) {
            this.f.a(i2);
        }

        @DexIgnore
        public void setStrokeWidth(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public void setTrimPathEnd(float f2) {
            this.l = f2;
        }

        @DexIgnore
        public void setTrimPathOffset(float f2) {
            this.m = f2;
        }

        @DexIgnore
        public void setTrimPathStart(float f2) {
            this.k = f2;
        }

        @DexIgnore
        public final Paint.Join a(int i2, Paint.Join join) {
            if (i2 == 0) {
                return Paint.Join.MITER;
            }
            if (i2 != 1) {
                return i2 != 2 ? join : Paint.Join.BEVEL;
            }
            return Paint.Join.ROUND;
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a = e7.a(resources, theme, attributeSet, pk.c);
            a(a, xmlPullParser, theme);
            a.recycle();
        }

        @DexIgnore
        public final void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) {
            this.e = null;
            if (e7.a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.b = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.a = g7.a(string2);
                }
                Resources.Theme theme2 = theme;
                this.h = e7.a(typedArray, xmlPullParser, theme2, "fillColor", 1, 0);
                this.j = e7.a(typedArray, xmlPullParser, "fillAlpha", 12, this.j);
                this.n = a(e7.b(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
                this.o = a(e7.b(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
                this.p = e7.a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
                this.f = e7.a(typedArray, xmlPullParser, theme2, "strokeColor", 3, 0);
                this.i = e7.a(typedArray, xmlPullParser, "strokeAlpha", 11, this.i);
                this.g = e7.a(typedArray, xmlPullParser, "strokeWidth", 4, this.g);
                this.l = e7.a(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
                this.m = e7.a(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
                this.k = e7.a(typedArray, xmlPullParser, "trimPathStart", 5, this.k);
                this.c = e7.b(typedArray, xmlPullParser, "fillType", 13, this.c);
            }
        }

        @DexIgnore
        public c(c cVar) {
            super(cVar);
            this.e = cVar.e;
            this.f = cVar.f;
            this.g = cVar.g;
            this.i = cVar.i;
            this.h = cVar.h;
            this.c = cVar.c;
            this.j = cVar.j;
            this.k = cVar.k;
            this.l = cVar.l;
            this.m = cVar.m;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
        }

        @DexIgnore
        public boolean a() {
            return this.h.d() || this.f.d();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return this.f.a(iArr) | this.h.a(iArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends e {
        @DexIgnore
        public /* final */ Matrix a; // = new Matrix();
        @DexIgnore
        public /* final */ ArrayList<e> b; // = new ArrayList<>();
        @DexIgnore
        public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float d; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float e; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float f; // = 1.0f;
        @DexIgnore
        public float g; // = 1.0f;
        @DexIgnore
        public float h; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float i; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public /* final */ Matrix j; // = new Matrix();
        @DexIgnore
        public int k;
        @DexIgnore
        public int[] l;
        @DexIgnore
        public String m; // = null;

        @DexIgnore
        public d(d dVar, p4<String, Object> p4Var) {
            super();
            f fVar;
            this.c = dVar.c;
            this.d = dVar.d;
            this.e = dVar.e;
            this.f = dVar.f;
            this.g = dVar.g;
            this.h = dVar.h;
            this.i = dVar.i;
            this.l = dVar.l;
            this.m = dVar.m;
            this.k = dVar.k;
            String str = this.m;
            if (str != null) {
                p4Var.put(str, this);
            }
            this.j.set(dVar.j);
            ArrayList<e> arrayList = dVar.b;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                e eVar = arrayList.get(i2);
                if (eVar instanceof d) {
                    this.b.add(new d((d) eVar, p4Var));
                } else {
                    if (eVar instanceof c) {
                        fVar = new c((c) eVar);
                    } else if (eVar instanceof b) {
                        fVar = new b((b) eVar);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.b.add(fVar);
                    String str2 = fVar.b;
                    if (str2 != null) {
                        p4Var.put(str2, fVar);
                    }
                }
            }
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = e7.a(resources, theme, attributeSet, pk.b);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        @DexIgnore
        public final void b() {
            this.j.reset();
            this.j.postTranslate(-this.d, -this.e);
            this.j.postScale(this.f, this.g);
            this.j.postRotate(this.c, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.j.postTranslate(this.h + this.d, this.i + this.e);
        }

        @DexIgnore
        public String getGroupName() {
            return this.m;
        }

        @DexIgnore
        public Matrix getLocalMatrix() {
            return this.j;
        }

        @DexIgnore
        public float getPivotX() {
            return this.d;
        }

        @DexIgnore
        public float getPivotY() {
            return this.e;
        }

        @DexIgnore
        public float getRotation() {
            return this.c;
        }

        @DexIgnore
        public float getScaleX() {
            return this.f;
        }

        @DexIgnore
        public float getScaleY() {
            return this.g;
        }

        @DexIgnore
        public float getTranslateX() {
            return this.h;
        }

        @DexIgnore
        public float getTranslateY() {
            return this.i;
        }

        @DexIgnore
        public void setPivotX(float f2) {
            if (f2 != this.d) {
                this.d = f2;
                b();
            }
        }

        @DexIgnore
        public void setPivotY(float f2) {
            if (f2 != this.e) {
                this.e = f2;
                b();
            }
        }

        @DexIgnore
        public void setRotation(float f2) {
            if (f2 != this.c) {
                this.c = f2;
                b();
            }
        }

        @DexIgnore
        public void setScaleX(float f2) {
            if (f2 != this.f) {
                this.f = f2;
                b();
            }
        }

        @DexIgnore
        public void setScaleY(float f2) {
            if (f2 != this.g) {
                this.g = f2;
                b();
            }
        }

        @DexIgnore
        public void setTranslateX(float f2) {
            if (f2 != this.h) {
                this.h = f2;
                b();
            }
        }

        @DexIgnore
        public void setTranslateY(float f2) {
            if (f2 != this.i) {
                this.i = f2;
                b();
            }
        }

        @DexIgnore
        public final void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.c = e7.a(typedArray, xmlPullParser, "rotation", 5, this.c);
            this.d = typedArray.getFloat(1, this.d);
            this.e = typedArray.getFloat(2, this.e);
            this.f = e7.a(typedArray, xmlPullParser, "scaleX", 3, this.f);
            this.g = e7.a(typedArray, xmlPullParser, "scaleY", 4, this.g);
            this.h = e7.a(typedArray, xmlPullParser, "translateX", 6, this.h);
            this.i = e7.a(typedArray, xmlPullParser, "translateY", 7, this.i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            b();
        }

        @DexIgnore
        public boolean a() {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            boolean z = false;
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                z |= this.b.get(i2).a(iArr);
            }
            return z;
        }

        @DexIgnore
        public d() {
            super();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f extends e {
        @DexIgnore
        public g7.b[] a; // = null;
        @DexIgnore
        public String b;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public int d;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        public void a(Path path) {
            path.reset();
            g7.b[] bVarArr = this.a;
            if (bVarArr != null) {
                g7.b.a(bVarArr, path);
            }
        }

        @DexIgnore
        public boolean b() {
            return false;
        }

        @DexIgnore
        public g7.b[] getPathData() {
            return this.a;
        }

        @DexIgnore
        public String getPathName() {
            return this.b;
        }

        @DexIgnore
        public void setPathData(g7.b[] bVarArr) {
            if (!g7.a(this.a, bVarArr)) {
                this.a = g7.a(bVarArr);
            } else {
                g7.b(this.a, bVarArr);
            }
        }

        @DexIgnore
        public f(f fVar) {
            super();
            this.b = fVar.b;
            this.d = fVar.d;
            this.a = g7.a(fVar.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    public static xk a(Resources resources, int i2, Resources.Theme theme) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            xk xkVar = new xk();
            xkVar.a = d7.a(resources, i2, theme);
            new i(xkVar.a.getConstantState());
            return xkVar;
        }
        try {
            XmlResourceParser xml = resources.getXml(i2);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            while (true) {
                next = xml.next();
                if (next == 2 || next == 1) {
                    if (next != 2) {
                        return createFromXmlInner(resources, xml, asAttributeSet, theme);
                    }
                    throw new XmlPullParserException("No start tag found");
                }
            }
            if (next != 2) {
            }
        } catch (XmlPullParserException e2) {
            Log.e("VectorDrawableCompat", "parser error", e2);
            return null;
        } catch (IOException e3) {
            Log.e("VectorDrawableCompat", "parser error", e3);
            return null;
        }
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.a;
        if (drawable != null) {
            o7.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        h hVar = this.b;
        hVar.b = new g();
        TypedArray a2 = e7.a(resources, theme, attributeSet, pk.a);
        a(a2, xmlPullParser, theme);
        a2.recycle();
        hVar.a = getChangingConfigurations();
        hVar.k = true;
        a(resources, xmlPullParser, attributeSet, theme);
        this.c = a(this.c, hVar.c, hVar.d);
    }

    @DexIgnore
    public xk(h hVar) {
        this.f = true;
        this.g = new float[9];
        this.h = new Matrix();
        this.i = new Rect();
        this.b = hVar;
        this.c = a(this.c, hVar.c, hVar.d);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public static /* final */ Matrix q; // = new Matrix();
        @DexIgnore
        public /* final */ Path a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ Matrix c;
        @DexIgnore
        public Paint d;
        @DexIgnore
        public Paint e;
        @DexIgnore
        public PathMeasure f;
        @DexIgnore
        public int g;
        @DexIgnore
        public /* final */ d h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public float k;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m;
        @DexIgnore
        public String n;
        @DexIgnore
        public Boolean o;
        @DexIgnore
        public /* final */ p4<String, Object> p;

        @DexIgnore
        public g() {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new p4<>();
            this.h = new d();
            this.a = new Path();
            this.b = new Path();
        }

        @DexIgnore
        public static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        @DexIgnore
        public final void a(d dVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            dVar.a.set(matrix);
            dVar.a.preConcat(dVar.j);
            canvas.save();
            for (int i4 = 0; i4 < dVar.b.size(); i4++) {
                e eVar = dVar.b.get(i4);
                if (eVar instanceof d) {
                    a((d) eVar, dVar.a, canvas, i2, i3, colorFilter);
                } else if (eVar instanceof f) {
                    a(dVar, (f) eVar, canvas, i2, i3, colorFilter);
                }
            }
            canvas.restore();
        }

        @DexIgnore
        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        @DexIgnore
        public int getRootAlpha() {
            return this.m;
        }

        @DexIgnore
        public void setAlpha(float f2) {
            setRootAlpha((int) (f2 * 255.0f));
        }

        @DexIgnore
        public void setRootAlpha(int i2) {
            this.m = i2;
        }

        @DexIgnore
        public void a(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            a(this.h, q, canvas, i2, i3, colorFilter);
        }

        @DexIgnore
        public g(g gVar) {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new p4<>();
            this.h = new d(gVar.h, this.p);
            this.a = new Path(gVar.a);
            this.b = new Path(gVar.b);
            this.i = gVar.i;
            this.j = gVar.j;
            this.k = gVar.k;
            this.l = gVar.l;
            this.g = gVar.g;
            this.m = gVar.m;
            this.n = gVar.n;
            String str = gVar.n;
            if (str != null) {
                this.p.put(str, this);
            }
            this.o = gVar.o;
        }

        @DexIgnore
        public final void a(d dVar, f fVar, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.k;
            float f3 = ((float) i3) / this.l;
            float min = Math.min(f2, f3);
            Matrix matrix = dVar.a;
            this.c.set(matrix);
            this.c.postScale(f2, f3);
            float a2 = a(matrix);
            if (a2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                fVar.a(this.a);
                Path path = this.a;
                this.b.reset();
                if (fVar.b()) {
                    this.b.setFillType(fVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    this.b.addPath(path, this.c);
                    canvas.clipPath(this.b);
                    return;
                }
                c cVar = (c) fVar;
                if (!(cVar.k == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && cVar.l == 1.0f)) {
                    float f4 = cVar.k;
                    float f5 = cVar.m;
                    float f6 = (f4 + f5) % 1.0f;
                    float f7 = (cVar.l + f5) % 1.0f;
                    if (this.f == null) {
                        this.f = new PathMeasure();
                    }
                    this.f.setPath(this.a, false);
                    float length = this.f.getLength();
                    float f8 = f6 * length;
                    float f9 = f7 * length;
                    path.reset();
                    if (f8 > f9) {
                        this.f.getSegment(f8, length, path, true);
                        this.f.getSegment(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, path, true);
                    } else {
                        this.f.getSegment(f8, f9, path, true);
                    }
                    path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                this.b.addPath(path, this.c);
                if (cVar.h.e()) {
                    z6 z6Var = cVar.h;
                    if (this.e == null) {
                        this.e = new Paint(1);
                        this.e.setStyle(Paint.Style.FILL);
                    }
                    Paint paint = this.e;
                    if (z6Var.c()) {
                        Shader b2 = z6Var.b();
                        b2.setLocalMatrix(this.c);
                        paint.setShader(b2);
                        paint.setAlpha(Math.round(cVar.j * 255.0f));
                    } else {
                        paint.setShader((Shader) null);
                        paint.setAlpha(255);
                        paint.setColor(xk.a(z6Var.a(), cVar.j));
                    }
                    paint.setColorFilter(colorFilter);
                    this.b.setFillType(cVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.b, paint);
                }
                if (cVar.f.e()) {
                    z6 z6Var2 = cVar.f;
                    if (this.d == null) {
                        this.d = new Paint(1);
                        this.d.setStyle(Paint.Style.STROKE);
                    }
                    Paint paint2 = this.d;
                    Paint.Join join = cVar.o;
                    if (join != null) {
                        paint2.setStrokeJoin(join);
                    }
                    Paint.Cap cap = cVar.n;
                    if (cap != null) {
                        paint2.setStrokeCap(cap);
                    }
                    paint2.setStrokeMiter(cVar.p);
                    if (z6Var2.c()) {
                        Shader b3 = z6Var2.b();
                        b3.setLocalMatrix(this.c);
                        paint2.setShader(b3);
                        paint2.setAlpha(Math.round(cVar.i * 255.0f));
                    } else {
                        paint2.setShader((Shader) null);
                        paint2.setAlpha(255);
                        paint2.setColor(xk.a(z6Var2.a(), cVar.i));
                    }
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(cVar.g * min * a2);
                    canvas.drawPath(this.b, paint2);
                }
            }
        }

        @DexIgnore
        public final float a(Matrix matrix) {
            float[] fArr = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
            matrix.mapVectors(fArr);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return Math.abs(a2) / max;
            }
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }

        @DexIgnore
        public boolean a() {
            if (this.o == null) {
                this.o = Boolean.valueOf(this.h.a());
            }
            return this.o.booleanValue();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return this.h.a(iArr);
        }
    }

    @DexIgnore
    public static int a(int i2, float f2) {
        return (i2 & 16777215) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }

    @DexIgnore
    public static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public final void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException {
        h hVar = this.b;
        g gVar = hVar.b;
        hVar.d = a(e7.b(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList a2 = e7.a(typedArray, xmlPullParser, theme, "tint", 1);
        if (a2 != null) {
            hVar.c = a2;
        }
        hVar.e = e7.a(typedArray, xmlPullParser, "autoMirrored", 5, hVar.e);
        gVar.k = e7.a(typedArray, xmlPullParser, "viewportWidth", 7, gVar.k);
        gVar.l = e7.a(typedArray, xmlPullParser, "viewportHeight", 8, gVar.l);
        if (gVar.k <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (gVar.l > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            gVar.i = typedArray.getDimension(3, gVar.i);
            gVar.j = typedArray.getDimension(2, gVar.j);
            if (gVar.i <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (gVar.j > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                gVar.setAlpha(e7.a(typedArray, xmlPullParser, "alpha", 4, gVar.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    gVar.n = string;
                    gVar.p.put(string, gVar);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    @DexIgnore
    public final void a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        h hVar = this.b;
        g gVar = hVar.b;
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(gVar.h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                d dVar = (d) arrayDeque.peek();
                if ("path".equals(name)) {
                    c cVar = new c();
                    cVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(cVar);
                    if (cVar.getPathName() != null) {
                        gVar.p.put(cVar.getPathName(), cVar);
                    }
                    z = false;
                    hVar.a = cVar.d | hVar.a;
                } else if ("clip-path".equals(name)) {
                    b bVar = new b();
                    bVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(bVar);
                    if (bVar.getPathName() != null) {
                        gVar.p.put(bVar.getPathName(), bVar);
                    }
                    hVar.a = bVar.d | hVar.a;
                } else if ("group".equals(name)) {
                    d dVar2 = new d();
                    dVar2.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(dVar2);
                    arrayDeque.push(dVar2);
                    if (dVar2.getGroupName() != null) {
                        gVar.p.put(dVar2.getGroupName(), dVar2);
                    }
                    hVar.a = dVar2.k | hVar.a;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                arrayDeque.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            throw new XmlPullParserException("no path defined");
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public final boolean a() {
        if (Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || o7.e(this) != 1) {
            return false;
        }
        return true;
    }
}
