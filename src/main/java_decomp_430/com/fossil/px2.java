package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px2 {
    @DexIgnore
    public /* final */ xx2 a;

    @DexIgnore
    public px2(xx2 xx2) {
        this.a = xx2;
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.d(z);
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }
}
