package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm6 implements mm6 {
    @DexIgnore
    public /* final */ dn6 a;

    @DexIgnore
    public lm6(dn6 dn6) {
        wg6.b(dn6, "list");
        this.a = dn6;
    }

    @DexIgnore
    public dn6 a() {
        return this.a;
    }

    @DexIgnore
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return nl6.c() ? a().a("New") : super.toString();
    }
}
