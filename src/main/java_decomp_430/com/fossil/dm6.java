package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm6 implements mm6 {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public dm6(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public dn6 a() {
        return null;
    }

    @DexIgnore
    public boolean isActive() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(isActive() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
