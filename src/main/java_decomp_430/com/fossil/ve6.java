package com.fossil;

import com.fossil.af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ve6 implements af6.b {
    @DexIgnore
    public /* final */ af6.c<?> key;

    @DexIgnore
    public ve6(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        this.key = cVar;
    }

    @DexIgnore
    public <R> R fold(R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
        wg6.b(ig6, "operation");
        return af6.b.a.a(this, r, ig6);
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        return af6.b.a.a((af6.b) this, cVar);
    }

    @DexIgnore
    public af6.c<?> getKey() {
        return this.key;
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        return af6.b.a.b(this, cVar);
    }

    @DexIgnore
    public af6 plus(af6 af6) {
        wg6.b(af6, "context");
        return af6.b.a.a((af6.b) this, af6);
    }
}
