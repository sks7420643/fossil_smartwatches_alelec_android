package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm1 extends vf1 {
    @DexIgnore
    public long K;
    @DexIgnore
    public long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ xm1(short s, ue1 ue1, int i, int i2) {
        super(dl1.GET_SIZE_WRITTEN, s, lx0.GET_FILE_SIZE_WRITTEN, ue1, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.K = cw0.b(order.getInt(0));
            this.L = cw0.b(order.getInt(4));
            cw0.a(cw0.a(jSONObject, bm0.WRITTEN_SIZE, (Object) Long.valueOf(this.K)), bm0.WRITTEN_DATA_CRC, (Object) Long.valueOf(this.L));
        }
        return jSONObject;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.WRITTEN_SIZE, (Object) Long.valueOf(this.K)), bm0.WRITTEN_DATA_CRC, (Object) Long.valueOf(this.L));
    }
}
