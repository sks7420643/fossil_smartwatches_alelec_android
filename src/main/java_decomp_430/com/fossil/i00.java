package com.fossil;

import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i00<K, V> extends p4<K, V> {
    @DexIgnore
    public int i;

    @DexIgnore
    public V a(int i2, V v) {
        this.i = 0;
        return super.a(i2, v);
    }

    @DexIgnore
    public void clear() {
        this.i = 0;
        super.clear();
    }

    @DexIgnore
    public V d(int i2) {
        this.i = 0;
        return super.d(i2);
    }

    @DexIgnore
    public int hashCode() {
        if (this.i == 0) {
            this.i = super.hashCode();
        }
        return this.i;
    }

    @DexIgnore
    public V put(K k, V v) {
        this.i = 0;
        return super.put(k, v);
    }

    @DexIgnore
    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.i = 0;
        super.a(simpleArrayMap);
    }
}
