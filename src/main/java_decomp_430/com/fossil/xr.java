package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr implements vr {
    @DexIgnore
    public /* final */ p4<wr<?>, Object> b; // = new i00();

    @DexIgnore
    public void a(xr xrVar) {
        this.b.a(xrVar.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof xr) {
            return this.b.equals(((xr) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    @DexIgnore
    public <T> xr a(wr<T> wrVar, T t) {
        this.b.put(wrVar, t);
        return this;
    }

    @DexIgnore
    public <T> T a(wr<T> wrVar) {
        return this.b.containsKey(wrVar) ? this.b.get(wrVar) : wrVar.a();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            a(this.b.c(i), this.b.e(i), messageDigest);
        }
    }

    @DexIgnore
    public static <T> void a(wr<T> wrVar, Object obj, MessageDigest messageDigest) {
        wrVar.a(obj, messageDigest);
    }
}
