package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gv6 {
    @DexIgnore
    public static /* final */ ByteArrayBuffer e; // = a(iv6.a, ": ");
    @DexIgnore
    public static /* final */ ByteArrayBuffer f; // = a(iv6.a, "\r\n");
    @DexIgnore
    public static /* final */ ByteArrayBuffer g; // = a(iv6.a, "--");
    @DexIgnore
    public /* final */ Charset a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ List<ev6> c;
    @DexIgnore
    public /* final */ hv6 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[hv6.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[hv6.STRICT.ordinal()] = 1;
            a[hv6.BROWSER_COMPATIBLE.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public gv6(String str, Charset charset, String str2, hv6 hv6) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 != null) {
            this.a = charset == null ? iv6.a : charset;
            this.b = str2;
            this.c = new ArrayList();
            this.d = hv6;
        } else {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        }
    }

    @DexIgnore
    public static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public long c() {
        long j = 0;
        for (ev6 a2 : this.c) {
            long contentLength = a2.a().getContentLength();
            if (contentLength < 0) {
                return -1;
            }
            j += contentLength;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.d, (OutputStream) byteArrayOutputStream, false);
            return j + ((long) byteArrayOutputStream.toByteArray().length);
        } catch (IOException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    @DexIgnore
    public static void a(String str, Charset charset, OutputStream outputStream) throws IOException {
        a(a(charset, str), outputStream);
    }

    @DexIgnore
    public static void a(String str, OutputStream outputStream) throws IOException {
        a(a(iv6.a, str), outputStream);
    }

    @DexIgnore
    public static void a(jv6 jv6, OutputStream outputStream) throws IOException {
        a(jv6.b(), outputStream);
        a(e, outputStream);
        a(jv6.a(), outputStream);
        a(f, outputStream);
    }

    @DexIgnore
    public static void a(jv6 jv6, Charset charset, OutputStream outputStream) throws IOException {
        a(jv6.b(), charset, outputStream);
        a(e, outputStream);
        a(jv6.a(), charset, outputStream);
        a(f, outputStream);
    }

    @DexIgnore
    public List<ev6> a() {
        return this.c;
    }

    @DexIgnore
    public void a(ev6 ev6) {
        if (ev6 != null) {
            this.c.add(ev6);
        }
    }

    @DexIgnore
    public final void a(hv6 hv6, OutputStream outputStream, boolean z) throws IOException {
        ByteArrayBuffer a2 = a(this.a, b());
        for (ev6 next : this.c) {
            a(g, outputStream);
            a(a2, outputStream);
            a(f, outputStream);
            fv6 b2 = next.b();
            int i = a.a[hv6.ordinal()];
            if (i == 1) {
                Iterator<jv6> it = b2.iterator();
                while (it.hasNext()) {
                    a(it.next(), outputStream);
                }
            } else if (i == 2) {
                a(next.b().a("Content-Disposition"), this.a, outputStream);
                if (next.a().d() != null) {
                    a(next.b().a("Content-Type"), this.a, outputStream);
                }
            }
            a(f, outputStream);
            if (z) {
                next.a().writeTo(outputStream);
            }
            a(f, outputStream);
        }
        a(g, outputStream);
        a(a2, outputStream);
        a(g, outputStream);
        a(f, outputStream);
    }

    @DexIgnore
    public void a(OutputStream outputStream) throws IOException {
        a(this.d, outputStream, true);
    }
}
