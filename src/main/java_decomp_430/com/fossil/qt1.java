package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.fossil.v12;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qt1 extends vv1<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements v12.a<st1, GoogleSignInAccount> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final /* synthetic */ Object a(ew1 ew1) {
            return ((st1) ew1).a();
        }

        @DexIgnore
        public /* synthetic */ a(wu1 wu1) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class b {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {a, b, c, d};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    /*
    static {
        new a((wu1) null);
    }
    */

    @DexIgnore
    public qt1(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, jt1.e, googleSignInOptions, (zw1) new kw1());
    }

    @DexIgnore
    public Intent i() {
        Context f = f();
        int i = wu1.a[k() - 1];
        if (i == 1) {
            return cu1.b(f, (GoogleSignInOptions) e());
        }
        if (i != 2) {
            return cu1.c(f, (GoogleSignInOptions) e());
        }
        return cu1.a(f, (GoogleSignInOptions) e());
    }

    @DexIgnore
    public qc3<Void> j() {
        return v12.a(cu1.a(b(), f(), k() == b.c));
    }

    @DexIgnore
    public final synchronized int k() {
        if (j == b.a) {
            Context f = f();
            jv1 a2 = jv1.a();
            int a3 = a2.a(f, (int) nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE);
            if (a3 == 0) {
                j = b.d;
            } else if (a2.a(f, a3, (String) null) != null || DynamiteModule.a(f, "com.google.android.gms.auth.api.fallback") == 0) {
                j = b.b;
            } else {
                j = b.c;
            }
        }
        return j;
    }

    @DexIgnore
    public qt1(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, jt1.e, googleSignInOptions, (zw1) new kw1());
    }
}
