package com.fossil;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.ol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vm {
    @DexIgnore
    public static /* final */ String b; // = tl.a("SystemJobInfoConverter");
    @DexIgnore
    public /* final */ ComponentName a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ul.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[ul.NOT_REQUIRED.ordinal()] = 1;
            a[ul.CONNECTED.ordinal()] = 2;
            a[ul.UNMETERED.ordinal()] = 3;
            a[ul.NOT_ROAMING.ordinal()] = 4;
            a[ul.METERED.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    public vm(Context context) {
        this.a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    @DexIgnore
    public JobInfo a(zn znVar, int i) {
        nl nlVar = znVar.j;
        int a2 = a(nlVar.b());
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", znVar.a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", znVar.d());
        JobInfo.Builder extras = new JobInfo.Builder(i, this.a).setRequiredNetworkType(a2).setRequiresCharging(nlVar.g()).setRequiresDeviceIdle(nlVar.h()).setExtras(persistableBundle);
        if (!nlVar.h()) {
            extras.setBackoffCriteria(znVar.m, znVar.l == ll.LINEAR ? 0 : 1);
        }
        long max = Math.max(znVar.a() - System.currentTimeMillis(), 0);
        if (Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (Build.VERSION.SDK_INT >= 24 && nlVar.e()) {
            for (ol.a a3 : nlVar.a().a()) {
                extras.addTriggerContentUri(a(a3));
            }
            extras.setTriggerContentUpdateDelay(nlVar.c());
            extras.setTriggerContentMaxDelay(nlVar.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(nlVar.f());
            extras.setRequiresStorageNotLow(nlVar.i());
        }
        return extras.build();
    }

    @DexIgnore
    public static JobInfo.TriggerContentUri a(ol.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    @DexIgnore
    public static int a(ul ulVar) {
        int i = a.a[ulVar.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i != 4) {
            if (i == 5 && Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        tl.a().a(b, String.format("API version too low. Cannot convert network type value %s", new Object[]{ulVar}), new Throwable[0]);
        return 1;
    }
}
