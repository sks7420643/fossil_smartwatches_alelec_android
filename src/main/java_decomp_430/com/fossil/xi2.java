package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi2 extends fn2<xi2, a> implements to2 {
    @DexIgnore
    public static /* final */ xi2 zzh;
    @DexIgnore
    public static volatile yo2<xi2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public aj2 zzd;
    @DexIgnore
    public yi2 zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public String zzg; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<xi2, a> implements to2 {
        @DexIgnore
        public a() {
            super(xi2.zzh);
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((xi2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(bj2 bj2) {
            this();
        }
    }

    /*
    static {
        xi2 xi2 = new xi2();
        zzh = xi2;
        fn2.a(xi2.class, xi2);
    }
    */

    @DexIgnore
    public static xi2 v() {
        return zzh;
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 8;
            this.zzg = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final aj2 o() {
        aj2 aj2 = this.zzd;
        return aj2 == null ? aj2.w() : aj2;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final yi2 q() {
        yi2 yi2 = this.zze;
        return yi2 == null ? yi2.y() : yi2;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final boolean s() {
        return this.zzf;
    }

    @DexIgnore
    public final String t() {
        return this.zzg;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new xi2();
            case 2:
                return new a((bj2) null);
            case 3:
                return fn2.a((ro2) zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\u0007\u0002\u0004\b\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                yo2<xi2> yo2 = zzi;
                if (yo2 == null) {
                    synchronized (xi2.class) {
                        yo2 = zzi;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzh);
                            zzi = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
