package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr1 implements Factory<vr1> {
    @DexIgnore
    public static /* final */ yr1 a; // = new yr1();

    @DexIgnore
    public static yr1 a() {
        return a;
    }

    @DexIgnore
    public static vr1 b() {
        vr1 b = wr1.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    public vr1 get() {
        return b();
    }
}
