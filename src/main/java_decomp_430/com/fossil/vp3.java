package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class vp3 implements zp3 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public vp3(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static zp3 a(Object obj) {
        return new vp3(obj);
    }

    @DexIgnore
    public Object a(xp3 xp3) {
        Object obj = this.a;
        wp3.a(obj, xp3);
        return obj;
    }
}
