package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn0 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ vj0 b;
    @DexIgnore
    public /* final */ /* synthetic */ fu0 c;

    @DexIgnore
    public jn0(fu0 fu0) {
        this.c = fu0;
        this.a = fu0.h;
        this.b = fu0.f;
    }

    @DexIgnore
    public Context a() {
        return this.a;
    }

    @DexIgnore
    public void a(q81 q81) {
        fu0 fu0 = this.c;
        if (fu0.a != null) {
            fu0.d.a(q81);
        }
    }
}
