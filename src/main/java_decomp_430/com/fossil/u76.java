package com.fossil;

import dagger.internal.Factory;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u76<K, V, V2> implements Factory<Map<K, V2>> {
    @DexIgnore
    public /* final */ Map<K, Provider<V>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<K, V, V2> {
        @DexIgnore
        public /* final */ LinkedHashMap<K, Provider<V>> a;

        @DexIgnore
        public a(int i) {
            this.a = v76.b(i);
        }

        @DexIgnore
        public a<K, V, V2> a(K k, Provider<V> provider) {
            LinkedHashMap<K, Provider<V>> linkedHashMap = this.a;
            z76.a(k, "key");
            z76.a(provider, "provider");
            linkedHashMap.put(k, provider);
            return this;
        }
    }

    @DexIgnore
    public u76(Map<K, Provider<V>> map) {
        this.a = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public final Map<K, Provider<V>> a() {
        return this.a;
    }
}
