package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm2 extends km2 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public mm2(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.a = i2 + i;
        this.c = i;
        this.d = this.c;
    }

    @DexIgnore
    public final int a() {
        return this.c - this.d;
    }

    @DexIgnore
    public final int b(int i) throws qn2 {
        if (i >= 0) {
            int a2 = i + a();
            int i2 = this.e;
            if (a2 <= i2) {
                this.e = a2;
                b();
                return i2;
            }
            throw qn2.zza();
        }
        throw qn2.zzb();
    }

    @DexIgnore
    public final void b() {
        this.a += this.b;
        int i = this.a;
        int i2 = i - this.d;
        int i3 = this.e;
        if (i2 > i3) {
            this.b = i2 - i3;
            this.a = i - this.b;
            return;
        }
        this.b = 0;
    }
}
