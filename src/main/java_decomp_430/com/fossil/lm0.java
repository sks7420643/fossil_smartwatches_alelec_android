package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm0 extends jb1<r60[], HashMap<s60, r60>> {
    @DexIgnore
    public static /* final */ up0[] b; // = {new up0(s60.BIOMETRIC_PROFILE, new r51(h60.CREATOR)), new up0(s60.DAILY_STEP, new o71(o60.CREATOR)), new up0(s60.DAILY_STEP_GOAL, new m91(p60.CREATOR)), new up0(s60.DAILY_CALORIE, new ib1(k60.CREATOR)), new up0(s60.DAILY_CALORIE_GOAL, new dd1(l60.CREATOR)), new up0(s60.DAILY_TOTAL_ACTIVE_MINUTE, new ye1(q60.CREATOR)), new up0(s60.DAILY_ACTIVE_MINUTE_GOAL, new ug1(j60.CREATOR)), new up0(s60.DAILY_DISTANCE, new pi1(m60.CREATOR)), new up0(s60.INACTIVE_NUDGE, new jk1(x60.CREATOR)), new up0(s60.VIBE_STRENGTH, new mr0(a70.CREATOR)), new up0(s60.DO_NOT_DISTURB_SCHEDULE, new ft0(u60.CREATOR)), new up0(s60.TIME, new xu0(z60.CREATOR)), new up0(s60.BATTERY, new rw0(g60.CREATOR)), new up0(s60.HEART_RATE_MODE, new ly0(v60.CREATOR)), new up0(s60.DAILY_SLEEP, new f01(n60.CREATOR)), new up0(s60.DISPLAY_UNIT, new z11(t60.CREATOR)), new up0(s60.SECOND_TIMEZONE_OFFSET, new v31(y60.CREATOR))};
    @DexIgnore
    public static /* final */ up0[] c; // = ((up0[]) md6.b(b, new up0[]{new up0(s60.CURRENT_HEART_RATE, new cm1(i60.CREATOR)), new up0(s60.HELLAS_BATTERY, new sn1(w60.CREATOR))}));
    @DexIgnore
    public static /* final */ u31<r60[]>[] d; // = {new pf0(), new gh0()};
    @DexIgnore
    public static /* final */ ed1<HashMap<s60, r60>>[] e; // = {new yi0(w31.DEVICE_CONFIG), new sk0(w31.DEVICE_CONFIG)};
    @DexIgnore
    public static /* final */ lm0 f; // = new lm0();

    @DexIgnore
    public ed1<HashMap<s60, r60>>[] b() {
        return e;
    }

    @DexIgnore
    public u31<r60[]>[] a() {
        return d;
    }

    @DexIgnore
    public final byte[] a(r60[] r60Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (r60 b2 : r60Arr) {
            byteArrayOutputStream.write(b2.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "entryData.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: com.fossil.r60} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final HashMap<s60, r60> a(byte[] bArr, up0[] up0Arr) {
        r60 r60;
        up0 up0;
        int length = (bArr.length - 12) - 4;
        byte[] a = md6.a(bArr, 12, length + 12);
        ByteBuffer order = ByteBuffer.wrap(a).order(ByteOrder.LITTLE_ENDIAN);
        HashMap<s60, r60> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i + 3;
            if (i2 > length) {
                return hashMap;
            }
            short s = order.getShort(i);
            int i3 = order.get(i + 2);
            s60 a2 = s60.d.a(s);
            try {
                byte[] a3 = md6.a(a, i2, i2 + i3);
                int length2 = up0Arr.length;
                int i4 = 0;
                while (true) {
                    r60 = null;
                    if (i4 >= length2) {
                        up0 = null;
                        break;
                    }
                    up0 = up0Arr[i4];
                    if (up0.a == a2) {
                        break;
                    }
                    i4++;
                }
                if (up0 != null) {
                    r60 = up0.b.invoke(a3);
                }
                if (!(a2 == null || r60 == null)) {
                    hashMap.put(a2, r60);
                }
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
            i += i3 + 3;
        }
    }
}
