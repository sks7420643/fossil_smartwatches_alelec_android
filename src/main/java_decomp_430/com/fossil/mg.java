package com.fossil;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg extends qg {
    @DexIgnore
    public lg d;
    @DexIgnore
    public lg e;

    @DexIgnore
    public int[] a(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.a()) {
            iArr[0] = a(mVar, view, d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.b()) {
            iArr[1] = a(mVar, view, f(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    public gg b(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new a(this.a.getContext());
    }

    @DexIgnore
    public View c(RecyclerView.m mVar) {
        if (mVar.b()) {
            return a(mVar, f(mVar));
        }
        if (mVar.a()) {
            return a(mVar, d(mVar));
        }
        return null;
    }

    @DexIgnore
    public final lg d(RecyclerView.m mVar) {
        lg lgVar = this.e;
        if (lgVar == null || lgVar.a != mVar) {
            this.e = lg.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final lg e(RecyclerView.m mVar) {
        if (mVar.b()) {
            return f(mVar);
        }
        if (mVar.a()) {
            return d(mVar);
        }
        return null;
    }

    @DexIgnore
    public final lg f(RecyclerView.m mVar) {
        lg lgVar = this.d;
        if (lgVar == null || lgVar.a != mVar) {
            this.d = lg.b(mVar);
        }
        return this.d;
    }

    @DexIgnore
    public final boolean g(RecyclerView.m mVar) {
        PointF a2;
        int j = mVar.j();
        if (!(mVar instanceof RecyclerView.v.b) || (a2 = ((RecyclerView.v.b) mVar).a(j - 1)) == null) {
            return false;
        }
        if (a2.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || a2.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return true;
        }
        return false;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gg {
        @DexIgnore
        public a(Context context) {
            super(context);
        }

        @DexIgnore
        public void a(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            mg mgVar = mg.this;
            int[] a = mgVar.a(mgVar.a.getLayoutManager(), view);
            int i = a[0];
            int i2 = a[1];
            int d = d(Math.max(Math.abs(i), Math.abs(i2)));
            if (d > 0) {
                aVar.a(i, i2, d, this.j);
            }
        }

        @DexIgnore
        public int e(int i) {
            return Math.min(100, super.e(i));
        }

        @DexIgnore
        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public final boolean c(RecyclerView.m mVar, int i, int i2) {
        return mVar.a() ? i > 0 : i2 > 0;
    }

    @DexIgnore
    public int a(RecyclerView.m mVar, int i, int i2) {
        lg e2;
        int j = mVar.j();
        if (j == 0 || (e2 = e(mVar)) == null) {
            return -1;
        }
        int e3 = mVar.e();
        View view = null;
        View view2 = null;
        int i3 = RecyclerView.UNDEFINED_DURATION;
        int i4 = Integer.MAX_VALUE;
        for (int i5 = 0; i5 < e3; i5++) {
            View d2 = mVar.d(i5);
            if (d2 != null) {
                int a2 = a(mVar, d2, e2);
                if (a2 <= 0 && a2 > i3) {
                    view2 = d2;
                    i3 = a2;
                }
                if (a2 >= 0 && a2 < i4) {
                    view = d2;
                    i4 = a2;
                }
            }
        }
        boolean c = c(mVar, i, i2);
        if (c && view != null) {
            return mVar.l(view);
        }
        if (!c && view2 != null) {
            return mVar.l(view2);
        }
        if (!c) {
            view2 = view;
        }
        if (view2 == null) {
            return -1;
        }
        int l = mVar.l(view2) + (g(mVar) == c ? -1 : 1);
        if (l < 0 || l >= j) {
            return -1;
        }
        return l;
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, View view, lg lgVar) {
        return (lgVar.d(view) + (lgVar.b(view) / 2)) - (lgVar.f() + (lgVar.g() / 2));
    }

    @DexIgnore
    public final View a(RecyclerView.m mVar, lg lgVar) {
        int e2 = mVar.e();
        View view = null;
        if (e2 == 0) {
            return null;
        }
        int f = lgVar.f() + (lgVar.g() / 2);
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < e2; i2++) {
            View d2 = mVar.d(i2);
            int abs = Math.abs((lgVar.d(d2) + (lgVar.b(d2) / 2)) - f);
            if (abs < i) {
                view = d2;
                i = abs;
            }
        }
        return view;
    }
}
