package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zh6 extends xh6 implements th6<Long> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        new zh6(1, 0);
    }
    */

    @DexIgnore
    public zh6(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean b(long j) {
        return a() <= j && j <= b();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof zh6) {
            if (!isEmpty() || !((zh6) obj).isEmpty()) {
                zh6 zh6 = (zh6) obj;
                if (!(a() == zh6.a() && b() == zh6.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (a() ^ (a() >>> 32))) + (b() ^ (b() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
