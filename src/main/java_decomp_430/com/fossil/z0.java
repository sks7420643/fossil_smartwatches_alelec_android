package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z0 {
    @DexIgnore
    public static /* final */ int abc_vector_test; // = 2131230811;
    @DexIgnore
    public static /* final */ int notification_action_background; // = 2131231279;
    @DexIgnore
    public static /* final */ int notification_bg; // = 2131231280;
    @DexIgnore
    public static /* final */ int notification_bg_low; // = 2131231281;
    @DexIgnore
    public static /* final */ int notification_bg_low_normal; // = 2131231282;
    @DexIgnore
    public static /* final */ int notification_bg_low_pressed; // = 2131231283;
    @DexIgnore
    public static /* final */ int notification_bg_normal; // = 2131231284;
    @DexIgnore
    public static /* final */ int notification_bg_normal_pressed; // = 2131231285;
    @DexIgnore
    public static /* final */ int notification_icon_background; // = 2131231287;
    @DexIgnore
    public static /* final */ int notification_template_icon_bg; // = 2131231293;
    @DexIgnore
    public static /* final */ int notification_template_icon_low_bg; // = 2131231294;
    @DexIgnore
    public static /* final */ int notification_tile_bg; // = 2131231295;
    @DexIgnore
    public static /* final */ int notify_panel_notification_icon_bg; // = 2131231296;
}
