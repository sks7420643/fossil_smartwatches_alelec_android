package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum o36 {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    
    @DexIgnore
    public int a;

    @DexIgnore
    public o36(int i) {
        this.a = i;
    }

    @DexIgnore
    public static o36 getStatReportStrategy(int i) {
        for (o36 o36 : values()) {
            if (i == o36.a()) {
                return o36;
            }
        }
        return null;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
