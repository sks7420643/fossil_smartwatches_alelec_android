package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr1 extends zr1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ rp1 b;
    @DexIgnore
    public /* final */ np1 c;

    @DexIgnore
    public tr1(long j, rp1 rp1, np1 np1) {
        this.a = j;
        if (rp1 != null) {
            this.b = rp1;
            if (np1 != null) {
                this.c = np1;
                return;
            }
            throw new NullPointerException("Null event");
        }
        throw new NullPointerException("Null transportContext");
    }

    @DexIgnore
    public np1 a() {
        return this.c;
    }

    @DexIgnore
    public long b() {
        return this.a;
    }

    @DexIgnore
    public rp1 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zr1)) {
            return false;
        }
        zr1 zr1 = (zr1) obj;
        if (this.a != zr1.b() || !this.b.equals(zr1.c()) || !this.c.equals(zr1.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "PersistedEvent{id=" + this.a + ", transportContext=" + this.b + ", event=" + this.c + "}";
    }
}
