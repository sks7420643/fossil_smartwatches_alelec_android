package com.fossil;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yx implements cy<Bitmap, byte[]> {
    @DexIgnore
    public /* final */ Bitmap.CompressFormat a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public yx() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    public rt<byte[]> a(rt<Bitmap> rtVar, xr xrVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        rtVar.get().compress(this.a, this.b, byteArrayOutputStream);
        rtVar.a();
        return new gx(byteArrayOutputStream.toByteArray());
    }

    @DexIgnore
    public yx(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }
}
