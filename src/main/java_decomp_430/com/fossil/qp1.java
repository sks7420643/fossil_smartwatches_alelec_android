package com.fossil;

import com.fossil.jp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qp1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(fo1<?> fo1);

        @DexIgnore
        public abstract a a(ho1<?, byte[]> ho1);

        @DexIgnore
        public abstract a a(rp1 rp1);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract qp1 a();
    }

    @DexIgnore
    public static a f() {
        return new jp1.b();
    }

    @DexIgnore
    public abstract fo1<?> a();

    @DexIgnore
    public byte[] b() {
        return c().apply(a().b());
    }

    @DexIgnore
    public abstract ho1<?, byte[]> c();

    @DexIgnore
    public abstract rp1 d();

    @DexIgnore
    public abstract String e();
}
