package com.fossil;

import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1", f = "SetReplyMessageMappingUseCase.kt", l = {38, 39}, m = "invokeSuspend")
public final class ur4$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetReplyMessageMappingUseCase.e this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase$SetReplyMessageReceiver$receive$1$1", f = "SetReplyMessageMappingUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ur4$e$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ur4$e$a ur4_e_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ur4_e_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return SetReplyMessageMappingUseCase.this.a(new SetReplyMessageMappingUseCase.d());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ur4$e$a(SetReplyMessageMappingUseCase.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ur4$e$a ur4_e_a = new ur4$e$a(this.this$0, xe6);
        ur4_e_a.p$ = (il6) obj;
        return ur4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ur4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            QuickResponseRepository b = SetReplyMessageMappingUseCase.this.g;
            List a3 = SetReplyMessageMappingUseCase.this.f;
            this.L$0 = il6;
            this.label = 1;
            if (b.updateQR((List<QuickResponseMessage>) a3, (xe6<? super cd6>) this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        cn6 c = zl6.c();
        a aVar = new a(this, (xe6) null);
        this.L$0 = il6;
        this.label = 2;
        if (gk6.a(c, aVar, this) == a2) {
            return a2;
        }
        return cd6.a;
    }
}
