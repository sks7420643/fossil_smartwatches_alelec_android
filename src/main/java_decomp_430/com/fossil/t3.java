package com.fossil;

import com.fossil.u3;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t3<K, V> extends u3<K, V> {
    @DexIgnore
    public HashMap<K, u3.c<K, V>> e; // = new HashMap<>();

    @DexIgnore
    public u3.c<K, V> a(K k) {
        return this.e.get(k);
    }

    @DexIgnore
    public V b(K k, V v) {
        u3.c a = a(k);
        if (a != null) {
            return a.b;
        }
        this.e.put(k, a(k, v));
        return null;
    }

    @DexIgnore
    public boolean contains(K k) {
        return this.e.containsKey(k);
    }

    @DexIgnore
    public V remove(K k) {
        V remove = super.remove(k);
        this.e.remove(k);
        return remove;
    }

    @DexIgnore
    public Map.Entry<K, V> b(K k) {
        if (contains(k)) {
            return this.e.get(k).d;
        }
        return null;
    }
}
