package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg6 {
    @DexIgnore
    public static final <T> Class<T> a(fi6<T> fi6) {
        wg6.b(fi6, "$this$javaObjectType");
        Class a = ((ng6) fi6).a();
        if (a.isPrimitive()) {
            String name = a.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals("double")) {
                            a = Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals("int")) {
                            a = Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals("byte")) {
                            a = Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals("char")) {
                            a = Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals("long")) {
                            a = Long.class;
                            break;
                        }
                        break;
                    case 3625364:
                        if (name.equals("void")) {
                            a = Void.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            a = Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals("float")) {
                            a = Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals("short")) {
                            a = Short.class;
                            break;
                        }
                        break;
                }
            }
            if (a != null) {
                return a;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.Class<T>");
        } else if (a != null) {
            return a;
        } else {
            throw new rc6("null cannot be cast to non-null type java.lang.Class<T>");
        }
    }

    @DexIgnore
    public static final <T> fi6<T> a(Class<T> cls) {
        wg6.b(cls, "$this$kotlin");
        return kh6.a((Class) cls);
    }

    @DexIgnore
    public static final <T> Class<T> a(T t) {
        wg6.b(t, "$this$javaClass");
        Class<?> cls = t.getClass();
        if (cls != null) {
            return cls;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.Class<T>");
    }
}
