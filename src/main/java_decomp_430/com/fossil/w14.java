package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w14 implements Factory<s04> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public w14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static w14 a(b14 b14) {
        return new w14(b14);
    }

    @DexIgnore
    public static s04 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static s04 c(b14 b14) {
        s04 l = b14.l();
        z76.a(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    public s04 get() {
        return b(this.a);
    }
}
