package com.fossil;

import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm1 {
    @DexIgnore
    public /* synthetic */ pm1(qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        wg6.a(jSONObject2, "jsonFileContent.toString()");
        Charset f = mi0.A.f();
        if (jSONObject2 != null) {
            byte[] bytes = jSONObject2.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }
}
