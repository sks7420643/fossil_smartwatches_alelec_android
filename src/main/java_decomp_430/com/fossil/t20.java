package com.fossil;

import android.os.Looper;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t20 {
    @DexIgnore
    public /* final */ ExecutorService a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable a;

        @DexIgnore
        public a(t20 t20, Runnable runnable) {
            this.a = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.a.run();
            } catch (Exception e) {
                c86.g().e("CrashlyticsCore", "Failed to execute task.", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Callable a;

        @DexIgnore
        public b(t20 t20, Callable callable) {
            this.a = callable;
        }

        @DexIgnore
        public T call() throws Exception {
            try {
                return this.a.call();
            } catch (Exception e) {
                c86.g().e("CrashlyticsCore", "Failed to execute task.", e);
                return null;
            }
        }
    }

    @DexIgnore
    public t20(ExecutorService executorService) {
        this.a = executorService;
    }

    @DexIgnore
    public Future<?> a(Runnable runnable) {
        try {
            return this.a.submit(new a(this, runnable));
        } catch (RejectedExecutionException unused) {
            c86.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }

    @DexIgnore
    public <T> T b(Callable<T> callable) {
        try {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                return this.a.submit(callable).get(4, TimeUnit.SECONDS);
            }
            return this.a.submit(callable).get();
        } catch (RejectedExecutionException unused) {
            c86.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        } catch (Exception e) {
            c86.g().e("CrashlyticsCore", "Failed to execute task.", e);
            return null;
        }
    }

    @DexIgnore
    public <T> Future<T> a(Callable<T> callable) {
        try {
            return this.a.submit(new b(this, callable));
        } catch (RejectedExecutionException unused) {
            c86.g().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }
}
