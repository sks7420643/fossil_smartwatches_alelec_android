package com.fossil;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.fossil.k40;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j91 extends qg1 {
    @DexIgnore
    public byte[] l; // = new byte[0];
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore
    public j91(rg1 rg1, boolean z, at0 at0) {
        super(hm0.SUBSCRIBE_CHARACTERISTIC, rg1, at0);
        this.m = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0028, code lost:
        r0 = r0.a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0162  */
    public void a(ue1 ue1) {
        pn1[] pn1Arr;
        byte[] bArr;
        t31 t31;
        t31 t312;
        BluetoothGattCharacteristic bluetoothGattCharacteristic;
        if (ue1.a(this.k, this.m)) {
            rg1 rg1 = this.k;
            if (k40.l.d() != k40.c.ENABLED || ue1.b == null) {
                pn1Arr = new pn1[0];
            } else {
                gk1 gk1 = ue1.i.get(rg1);
                Integer valueOf = (gk1 == null || bluetoothGattCharacteristic == null) ? null : Integer.valueOf(bluetoothGattCharacteristic.getProperties());
                pn1Arr = valueOf == null ? new pn1[0] : pn1.e.a(valueOf.intValue());
            }
            if (true == (!this.m)) {
                bArr = pk0.d.a();
            } else if (true == nd6.a(pn1Arr, pn1.PROPERTY_NOTIFY)) {
                bArr = pk0.d.c();
            } else {
                bArr = true == nd6.a(pn1Arr, pn1.PROPERTY_INDICATE) ? pk0.d.b() : new byte[0];
            }
            this.l = bArr;
            if (this.l.length == 0) {
                ue1.a(this.k, !this.m);
                this.d = ch0.a(this.d, (hm0) null, lf0.UNSUPPORTED, (t31) null, 5);
                a();
                return;
            }
            rg1 rg12 = this.k;
            vi0 vi0 = vi0.CLIENT_CHARACTERISTIC_CONFIGURATION;
            byte[] bArr2 = this.l;
            if (k40.l.d() != k40.c.ENABLED) {
                cc0 cc0 = cc0.DEBUG;
                ue1.a.post(new zc1(ue1, new t31(x11.BLUETOOTH_OFF, 0, 2), rg12, vi0, new byte[0]));
            } else {
                t31 t313 = new t31(x11.SUCCESS, 0, 2);
                if (ue1.b == null) {
                    t312 = new t31(x11.GATT_NULL, 0, 2);
                } else {
                    gk1 gk12 = ue1.i.get(rg12);
                    BluetoothGattCharacteristic bluetoothGattCharacteristic2 = gk12 != null ? gk12.a : null;
                    if (bluetoothGattCharacteristic2 == null) {
                        t312 = new t31(x11.CHARACTERISTIC_NOT_FOUND, 0, 2);
                    } else {
                        BluetoothGattDescriptor descriptor = bluetoothGattCharacteristic2.getDescriptor(vi0.a());
                        if (descriptor != null) {
                            descriptor.setValue(bArr2);
                        }
                        if (descriptor == null) {
                            t312 = new t31(x11.DESCRIPTOR_NOT_FOUND, 0, 2);
                        } else {
                            BluetoothGatt bluetoothGatt = ue1.b;
                            if (bluetoothGatt == null || true != bluetoothGatt.writeDescriptor(descriptor)) {
                                cc0 cc02 = cc0.DEBUG;
                                BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
                                wg6.a(characteristic, "descriptor.characteristic");
                                BluetoothGattService service = characteristic.getService();
                                wg6.a(service, "descriptor.characteristic.service");
                                BluetoothGattCharacteristic characteristic2 = descriptor.getCharacteristic();
                                wg6.a(characteristic2, "descriptor.characteristic");
                                byte[] value = descriptor.getValue();
                                wg6.a(value, "descriptor.value");
                                Object[] objArr = {service.getUuid(), characteristic2.getUuid(), descriptor.getUuid(), cw0.a(value, (String) null, 1)};
                                t312 = new t31(x11.START_FAIL, 0, 2);
                            } else {
                                t31 = t313;
                                if (t31.a != x11.SUCCESS) {
                                    ue1.a.post(new zc1(ue1, t31, rg12, vi0, new byte[0]));
                                }
                            }
                        }
                    }
                }
                t31 = t312;
                if (t31.a != x11.SUCCESS) {
                }
            }
            this.j = true;
            return;
        }
        this.d = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        a();
    }

    @DexIgnore
    public boolean b(p51 p51) {
        if (p51 instanceof wu0) {
            wu0 wu0 = (wu0) p51;
            return wu0.b == this.k && wu0.c == vi0.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.b;
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        lf0 lf0;
        this.j = false;
        t31 t31 = p51.a;
        if (t31.a == x11.SUCCESS) {
            if (Arrays.equals(this.l, ((wu0) p51).d)) {
                lf0 = lf0.SUCCESS;
            } else {
                lf0 = lf0.UNEXPECTED_RESULT;
            }
            ch0 = ch0.a(this.d, (hm0) null, lf0, p51.a, 1);
        } else {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        }
        this.d = ch0;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return cw0.a(super.a(z), bm0.ENABLE, (Object) Boolean.valueOf(this.m));
    }
}
