package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleCheckBox;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu4 extends RecyclerView.g<a> {
    @DexIgnore
    public List<WeatherLocationWrapper> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public FlexibleCheckBox b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ tu4 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tu4$a$a")
        /* renamed from: com.fossil.tu4$a$a  reason: collision with other inner class name */
        public static final class C0040a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0040a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2;
                if (this.a.d.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && !((WeatherLocationWrapper) this.a.d.a.get(this.a.getAdapterPosition())).isUseCurrentLocation() && (a2 = this.a.d.b) != null) {
                    a2.a(this.a.getAdapterPosition() - 1, !((WeatherLocationWrapper) this.a.d.a.get(this.a.getAdapterPosition())).isEnableLocation());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(tu4 tu4, View view) {
            super(view);
            wg6.b(view, "view");
            this.d = tu4;
            View findViewById = view.findViewById(2131362026);
            if (findViewById != null) {
                this.c = findViewById;
                View findViewById2 = view.findViewById(2131361979);
                if (findViewById2 != null) {
                    this.b = (FlexibleCheckBox) findViewById2;
                    View findViewById3 = view.findViewById(2131363217);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                        View view2 = this.c;
                        if (view2 != null) {
                            view2.setOnClickListener(new C0040a(this));
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
        /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(WeatherLocationWrapper weatherLocationWrapper) {
            FlexibleCheckBox flexibleCheckBox;
            wg6.b(weatherLocationWrapper, "locationWrapper");
            TextView textView = this.a;
            if (textView != null) {
                textView.setText(weatherLocationWrapper.getFullName());
            }
            Object r0 = this.b;
            if (r0 != 0) {
                r0.setChecked(weatherLocationWrapper.isEnableLocation());
            }
            Object r02 = this.b;
            if (r02 != 0) {
                r02.setEnabled(!weatherLocationWrapper.isUseCurrentLocation());
            }
            TextView textView2 = this.a;
            if (wg6.a((Object) textView2 != null ? textView2.getText() : null, (Object) jm4.a((Context) PortfolioApp.get.instance(), 2131886228)) && (flexibleCheckBox = this.b) != null) {
                flexibleCheckBox.setEnableColor("nonBrandDisableCalendarDay");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            aVar.a(this.a.get(i));
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558692, viewGroup, false);
        wg6.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(List<WeatherLocationWrapper> list) {
        wg6.b(list, "locationSearchList");
        this.a.clear();
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886228);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886228);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        this.a.add(0, new WeatherLocationWrapper("", 0.0d, 0.0d, a2, a3, true, true, 6, (qg6) null));
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
