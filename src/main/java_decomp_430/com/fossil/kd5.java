package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd5 implements MembersInjector<CaloriesOverviewFragment> {
    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
        caloriesOverviewFragment.g = caloriesOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
        caloriesOverviewFragment.h = caloriesOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        caloriesOverviewFragment.i = caloriesOverviewMonthPresenter;
    }
}
