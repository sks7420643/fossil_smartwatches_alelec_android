package com.fossil;

import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt5 implements Factory<ut5> {
    @DexIgnore
    public static SignUpPresenter a(qt5 qt5, BaseActivity baseActivity) {
        return new SignUpPresenter(qt5, baseActivity);
    }
}
