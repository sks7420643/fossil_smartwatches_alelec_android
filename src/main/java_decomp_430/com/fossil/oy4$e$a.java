package com.fossil;

import com.fossil.m24;
import com.fossil.ur4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy4$e$a implements m24.e<ur4.d, ur4.c> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.e a;

    @DexIgnore
    public oy4$e$a(NotificationCallsAndMessagesPresenter.e eVar) {
        this.a = eVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetReplyMessageMappingUseCase.d dVar) {
        wg6.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "Set reply message to device success");
        this.a.this$0.t.a();
        this.a.this$0.t.l(true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(SetReplyMessageMappingUseCase.c cVar) {
        wg6.b(cVar, "errorValue");
        FLogger.INSTANCE.getLocal().e(NotificationCallsAndMessagesPresenter.E.a(), "Set reply message to device fail");
        this.a.this$0.t.a();
        if (!xm4.d.a(PortfolioApp.get.instance().getApplicationContext(), 1)) {
            this.a.this$0.t.b(qd6.a((T[]) new Integer[]{1}));
        } else {
            this.a.this$0.t.L();
        }
        this.a.this$0.t.l(false);
    }
}
