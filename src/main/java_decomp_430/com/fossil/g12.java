package com.fossil;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g12 implements DialogInterface.OnClickListener {
    @DexIgnore
    public static g12 a(Activity activity, Intent intent, int i) {
        return new x22(intent, activity, i);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            a();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static g12 a(Fragment fragment, Intent intent, int i) {
        return new w22(intent, fragment, i);
    }

    @DexIgnore
    public static g12 a(tw1 tw1, Intent intent, int i) {
        return new y22(intent, tw1, i);
    }
}
