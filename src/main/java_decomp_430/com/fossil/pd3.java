package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ od3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Callable b;

    @DexIgnore
    public pd3(od3 od3, Callable callable) {
        this.a = od3;
        this.b = callable;
    }

    @DexIgnore
    public final void run() {
        try {
            this.a.a(this.b.call());
        } catch (Exception e) {
            this.a.a(e);
        }
    }
}
