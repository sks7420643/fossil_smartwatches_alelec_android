package com.fossil;

import kotlinx.coroutines.DeferredCoroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ik6 {
    @DexIgnore
    public static /* synthetic */ rl6 a(il6 il6, af6 af6, ll6 ll6, ig6 ig6, int i, Object obj) {
        if ((i & 1) != 0) {
            af6 = bf6.INSTANCE;
        }
        if ((i & 2) != 0) {
            ll6 = ll6.DEFAULT;
        }
        return gk6.a(il6, af6, ll6, ig6);
    }

    @DexIgnore
    public static /* synthetic */ rm6 b(il6 il6, af6 af6, ll6 ll6, ig6 ig6, int i, Object obj) {
        if ((i & 1) != 0) {
            af6 = bf6.INSTANCE;
        }
        if ((i & 2) != 0) {
            ll6 = ll6.DEFAULT;
        }
        return gk6.b(il6, af6, ll6, ig6);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: kotlinx.coroutines.DeferredCoroutine} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: com.fossil.an6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: kotlinx.coroutines.DeferredCoroutine} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: kotlinx.coroutines.DeferredCoroutine} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static final <T> rl6<T> a(il6 il6, af6 af6, ll6 ll6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6) {
        DeferredCoroutine deferredCoroutine;
        wg6.b(il6, "$this$async");
        wg6.b(af6, "context");
        wg6.b(ll6, "start");
        wg6.b(ig6, "block");
        af6 a = cl6.a(il6, af6);
        if (ll6.isLazy()) {
            deferredCoroutine = new an6(a, ig6);
        } else {
            deferredCoroutine = new DeferredCoroutine(a, true);
        }
        deferredCoroutine.a(ll6, deferredCoroutine, ig6);
        return deferredCoroutine;
    }

    @DexIgnore
    public static final rm6 b(il6 il6, af6 af6, ll6 ll6, ig6<? super il6, ? super xe6<? super cd6>, ? extends Object> ig6) {
        ck6 ck6;
        wg6.b(il6, "$this$launch");
        wg6.b(af6, "context");
        wg6.b(ll6, "start");
        wg6.b(ig6, "block");
        af6 a = cl6.a(il6, af6);
        if (ll6.isLazy()) {
            ck6 = new bn6(a, ig6);
        } else {
            ck6 = new kn6(a, true);
        }
        ck6.a(ll6, ck6, ig6);
        return ck6;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> Object a(af6 af6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6, xe6<? super T> xe6) {
        Object obj;
        af6 context = xe6.getContext();
        af6 plus = context.plus(af6);
        wn6.a(plus);
        if (plus == context) {
            ro6 ro6 = new ro6(plus, xe6);
            obj = dp6.a(ro6, ro6, ig6);
        } else if (wg6.a((Object) (ye6) plus.get(ye6.l), (Object) (ye6) context.get(ye6.l))) {
            vn6 vn6 = new vn6(plus, xe6);
            Object b = yo6.b(plus, (Object) null);
            try {
                Object a = dp6.a(vn6, vn6, ig6);
                yo6.a(plus, b);
                obj = a;
            } catch (Throwable th) {
                yo6.a(plus, b);
                throw th;
            }
        } else {
            wl6 wl6 = new wl6(plus, xe6);
            wl6.n();
            cp6.a(ig6, wl6, wl6);
            obj = wl6.p();
        }
        if (obj == ff6.a()) {
            nf6.c(xe6);
        }
        return obj;
    }
}
