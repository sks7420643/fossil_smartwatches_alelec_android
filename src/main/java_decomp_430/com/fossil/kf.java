package com.fossil;

import com.fossil.af;
import com.fossil.xe;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kf<K, A, B> extends af<K, B> {
    @DexIgnore
    public /* final */ af<K, A> a;
    @DexIgnore
    public /* final */ v3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends af.c<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ af.c a;

        @DexIgnore
        public a(af.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list, K k, K k2) {
            this.a.a(xe.convert(kf.this.b, list), k, k2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends af.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ af.a a;

        @DexIgnore
        public b(af.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(xe.convert(kf.this.b, list), k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends af.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ af.a a;

        @DexIgnore
        public c(af.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(xe.convert(kf.this.b, list), k);
        }
    }

    @DexIgnore
    public kf(af<K, A> afVar, v3<List<A>, List<B>> v3Var) {
        this.a = afVar;
        this.b = v3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(xe.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(af.f<K> fVar, af.a<K, B> aVar) {
        this.a.loadAfter(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadBefore(af.f<K> fVar, af.a<K, B> aVar) {
        this.a.loadBefore(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadInitial(af.e<K> eVar, af.c<K, B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(xe.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
