package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nc6 {
    @DexIgnore
    public static final Object a(Throwable th) {
        wg6.b(th, "exception");
        return new mc6.b(th);
    }

    @DexIgnore
    public static final void a(Object obj) {
        if (obj instanceof mc6.b) {
            throw ((mc6.b) obj).exception;
        }
    }
}
