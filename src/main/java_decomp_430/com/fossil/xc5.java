package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc5 implements Factory<wc5> {
    @DexIgnore
    public static ActivityOverviewWeekPresenter a(vc5 vc5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new ActivityOverviewWeekPresenter(vc5, userRepository, summariesRepository, portfolioApp);
    }
}
