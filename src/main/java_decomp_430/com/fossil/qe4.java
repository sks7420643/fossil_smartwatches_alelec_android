package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qe4 extends pe4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F; // = new SparseIntArray();
    @DexIgnore
    public long D;

    /*
    static {
        F.put(2131362820, 1);
        F.put(2131362086, 2);
        F.put(2131362447, 3);
        F.put(2131362825, 4);
        F.put(2131362313, 5);
        F.put(2131362448, 6);
        F.put(2131362896, 7);
        F.put(2131362511, 8);
        F.put(2131362210, 9);
        F.put(2131362085, 10);
        F.put(2131362444, 11);
        F.put(2131362590, 12);
        F.put(2131362345, 13);
        F.put(2131362363, 14);
        F.put(2131362228, 15);
    }
    */

    @DexIgnore
    public qe4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 16, E, F));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public qe4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[10], objArr[2], objArr[9], objArr[15], objArr[5], objArr[13], objArr[14], objArr[11], objArr[3], objArr[6], objArr[8], objArr[12], objArr[1], objArr[4], objArr[0], objArr[7]);
        this.D = -1;
        this.B.setTag((Object) null);
        a(view);
        f();
    }
}
