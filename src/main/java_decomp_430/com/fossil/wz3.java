package com.fossil;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz3 implements xx3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        int i3;
        int i4;
        if (qx3 == qx3.PDF_417) {
            b04 b04 = new b04();
            int i5 = 30;
            int i6 = 2;
            if (map != null) {
                if (map.containsKey(sx3.PDF417_COMPACT)) {
                    b04.a(Boolean.valueOf(map.get(sx3.PDF417_COMPACT).toString()).booleanValue());
                }
                if (map.containsKey(sx3.PDF417_COMPACTION)) {
                    b04.a(zz3.valueOf(map.get(sx3.PDF417_COMPACTION).toString()));
                }
                if (map.containsKey(sx3.PDF417_DIMENSIONS)) {
                    a04 a04 = (a04) map.get(sx3.PDF417_DIMENSIONS);
                    b04.a(a04.a(), a04.c(), a04.b(), a04.d());
                }
                if (map.containsKey(sx3.MARGIN)) {
                    i5 = Integer.parseInt(map.get(sx3.MARGIN).toString());
                }
                if (map.containsKey(sx3.ERROR_CORRECTION)) {
                    i6 = Integer.parseInt(map.get(sx3.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(sx3.CHARACTER_SET)) {
                    b04.a(Charset.forName(map.get(sx3.CHARACTER_SET).toString()));
                }
                i3 = i5;
                i4 = i6;
            } else {
                i4 = 2;
                i3 = 30;
            }
            return a(b04, str, i4, i, i2, i3);
        }
        throw new IllegalArgumentException("Can only encode PDF_417, but got " + qx3);
    }

    @DexIgnore
    public static iy3 a(b04 b04, String str, int i, int i2, int i3, int i4) throws yx3 {
        boolean z;
        b04.a(str, i);
        byte[][] a = b04.a().a(1, 4);
        if ((i3 > i2) ^ (a[0].length < a.length)) {
            a = a(a);
            z = true;
        } else {
            z = false;
        }
        int length = i2 / a[0].length;
        int length2 = i3 / a.length;
        if (length >= length2) {
            length = length2;
        }
        if (length <= 1) {
            return a(a, i4);
        }
        byte[][] a2 = b04.a().a(length, length << 2);
        if (z) {
            a2 = a(a2);
        }
        return a(a2, i4);
    }

    @DexIgnore
    public static iy3 a(byte[][] bArr, int i) {
        int i2 = i * 2;
        iy3 iy3 = new iy3(bArr[0].length + i2, bArr.length + i2);
        iy3.a();
        int b = (iy3.b() - i) - 1;
        int i3 = 0;
        while (i3 < bArr.length) {
            for (int i4 = 0; i4 < bArr[0].length; i4++) {
                if (bArr[i3][i4] == 1) {
                    iy3.b(i4 + i, b);
                }
            }
            i3++;
            b--;
        }
        return iy3;
    }

    @DexIgnore
    public static byte[][] a(byte[][] bArr) {
        byte[][] bArr2 = (byte[][]) Array.newInstance(byte.class, new int[]{bArr[0].length, bArr.length});
        for (int i = 0; i < bArr.length; i++) {
            int length = (bArr.length - i) - 1;
            for (int i2 = 0; i2 < bArr[0].length; i2++) {
                bArr2[i2][length] = bArr[i][i2];
            }
        }
        return bArr2;
    }
}
