package com.fossil;

import android.app.Activity;
import android.content.Context;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yw6<T> extends zw6<T> {
    @DexIgnore
    public yw6(T t) {
        super(t);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        throw new IllegalStateException("Should never be requesting permissions on API < 23!");
    }

    @DexIgnore
    public void b(String str, String str2, String str3, int i, int i2, String... strArr) {
        throw new IllegalStateException("Should never be requesting permissions on API < 23!");
    }

    @DexIgnore
    public boolean b(String str) {
        return false;
    }

    @DexIgnore
    public Context a() {
        if (b() instanceof Activity) {
            return (Context) b();
        }
        if (b() instanceof Fragment) {
            return ((Fragment) b()).getContext();
        }
        throw new IllegalStateException("Unknown host: " + b());
    }
}
