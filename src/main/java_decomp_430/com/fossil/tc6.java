package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc6 implements Collection<sc6>, ph6 {
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends oe6 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ byte[] b;

        @DexIgnore
        public a(byte[] bArr) {
            wg6.b(bArr, "array");
            this.b = bArr;
        }

        @DexIgnore
        public byte a() {
            int i = this.a;
            byte[] bArr = this.b;
            if (i < bArr.length) {
                this.a = i + 1;
                byte b2 = bArr[i];
                sc6.c(b2);
                return b2;
            }
            throw new NoSuchElementException(String.valueOf(i));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(byte[] bArr, Object obj) {
        return (obj instanceof tc6) && wg6.a((Object) bArr, (Object) ((tc6) obj).b());
    }

    @DexIgnore
    public static int b(byte[] bArr) {
        if (bArr != null) {
            return Arrays.hashCode(bArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(byte[] bArr) {
        return bArr.length == 0;
    }

    @DexIgnore
    public static oe6 d(byte[] bArr) {
        return new a(bArr);
    }

    @DexIgnore
    public static String e(byte[] bArr) {
        return "UByteArray(storage=" + Arrays.toString(bArr) + ")";
    }

    @DexIgnore
    public boolean a(byte b) {
        return a(this.a, b);
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends sc6> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ byte[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof sc6) {
            return a(((sc6) obj).a());
        }
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<sc6>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    public oe6 iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public static boolean a(byte[] bArr, byte b) {
        return nd6.b(bArr, b);
    }

    @DexIgnore
    public static boolean a(byte[] bArr, Collection<sc6> collection) {
        boolean z;
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        for (T next : collection) {
            if (!(next instanceof sc6) || !nd6.b(bArr, ((sc6) next).a())) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
