package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<vc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new vc0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new vc0[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ vc0(Parcel parcel, qg6 qg6) {
        super(r4, (uc0) r3);
        Parcelable readParcelable = parcel.readParcelable(y90.class.getClassLoader());
        if (readParcelable != null) {
            y90 y90 = (y90) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(uc0.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("iftttApp._.config.query_res_msg", getDeviceMessage());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        wg6.a(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = mi0.A.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }
}
