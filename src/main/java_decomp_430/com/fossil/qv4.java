package com.fossil;

import android.util.SparseIntArray;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qv4 extends xt4<pv4> {
    @DexIgnore
    String C();

    @DexIgnore
    void N();

    @DexIgnore
    void X();

    @DexIgnore
    void a();

    @DexIgnore
    void a(SparseIntArray sparseIntArray);

    @DexIgnore
    void a(Alarm alarm, boolean z);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void b();

    @DexIgnore
    void b(int i);

    @DexIgnore
    void c();

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    String getTitle();

    @DexIgnore
    void k(boolean z);

    @DexIgnore
    void m(String str);

    @DexIgnore
    void t(boolean z);

    @DexIgnore
    void x();
}
