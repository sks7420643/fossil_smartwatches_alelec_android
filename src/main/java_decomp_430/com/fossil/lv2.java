package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv2 extends sh2 implements jv2 {
    @DexIgnore
    public lv2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, long j) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (Parcelable) bundle);
        q.writeLong(j);
        b(1, q);
    }

    @DexIgnore
    public final int zza() throws RemoteException {
        Parcel a = a(2, q());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
