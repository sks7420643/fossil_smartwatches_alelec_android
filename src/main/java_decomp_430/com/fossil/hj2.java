package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj2 extends fn2<hj2, a> implements to2 {
    @DexIgnore
    public static /* final */ hj2 zzf;
    @DexIgnore
    public static volatile yo2<hj2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<hj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(hj2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(jj2 jj2) {
            this();
        }
    }

    /*
    static {
        hj2 hj2 = new hj2();
        zzf = hj2;
        fn2.a(hj2.class, hj2);
    }
    */

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (jj2.a[i - 1]) {
            case 1:
                return new hj2();
            case 2:
                return new a((jj2) null);
            case 3:
                return fn2.a((ro2) zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                yo2<hj2> yo2 = zzg;
                if (yo2 == null) {
                    synchronized (hj2.class) {
                        yo2 = zzg;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzf);
                            zzg = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final String n() {
        return this.zzd;
    }

    @DexIgnore
    public final String o() {
        return this.zze;
    }
}
