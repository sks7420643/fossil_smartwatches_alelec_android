package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ bd0 c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<j90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new j90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new j90[i];
        }
    }

    @DexIgnore
    public j90(byte b, bd0 bd0, byte b2) {
        super(e90.BATTERY, b);
        this.c = bd0;
        this.d = b2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.STATE, (Object) cw0.a((Enum<?>) this.c)), bm0.PERCENTAGE, (Object) Byte.valueOf(this.d));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(j90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            j90 j90 = (j90) obj;
            return this.c == j90.c && this.d == j90.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.BatteryNotification");
    }

    @DexIgnore
    public final byte getBatteryPercentage() {
        return this.d;
    }

    @DexIgnore
    public final bd0 getBatteryState() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return Byte.valueOf(this.d).hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ j90(Parcel parcel, qg6 qg6) {
        super(parcel);
        bd0 a2 = bd0.c.a(parcel.readByte());
        this.c = a2 == null ? bd0.LOW : a2;
        this.d = parcel.readByte();
    }
}
