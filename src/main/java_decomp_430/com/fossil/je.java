package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.ie;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class je {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ie.b {
        @DexIgnore
        public a(Context context, b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((b) this.a).a(str, new ie.c(result));
        }
    }

    @DexIgnore
    public interface b extends ie.d {
        @DexIgnore
        void a(String str, ie.c<Parcel> cVar);
    }

    @DexIgnore
    public static Object a(Context context, b bVar) {
        return new a(context, bVar);
    }
}
