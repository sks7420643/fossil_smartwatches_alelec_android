package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1$bitmap$1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
public final class mk5$d$a extends sf6 implements ig6<il6, xe6<? super Bitmap>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Uri $imageUri;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mk5$d$a(Uri uri, xe6 xe6) {
        super(2, xe6);
        this.$imageUri = uri;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        mk5$d$a mk5_d_a = new mk5$d$a(this.$imageUri, xe6);
        mk5_d_a.p$ = (il6) obj;
        return mk5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((mk5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return jj4.a((Context) PortfolioApp.get.instance()).e().a(this.$imageUri).a(new nz().a(ft.a).a(true).a(new xj4())).c(MFNetworkReturnCode.RESPONSE_OK, MFNetworkReturnCode.RESPONSE_OK).get();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
