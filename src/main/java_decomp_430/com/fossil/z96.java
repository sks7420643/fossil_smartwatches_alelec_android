package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z96 implements r96<ba6>, y96, ba6 {
    @DexIgnore
    public /* final */ List<ba6> a; // = new ArrayList();
    @DexIgnore
    public /* final */ AtomicBoolean b; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicReference<Throwable> c; // = new AtomicReference<>((Object) null);

    @DexIgnore
    public boolean b() {
        for (ba6 a2 : c()) {
            if (!a2.a()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public synchronized Collection<ba6> c() {
        return Collections.unmodifiableCollection(this.a);
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return u96.compareTo(this, obj);
    }

    @DexIgnore
    public u96 getPriority() {
        return u96.NORMAL;
    }

    @DexIgnore
    public synchronized void a(ba6 ba6) {
        this.a.add(ba6);
    }

    @DexIgnore
    public static boolean b(Object obj) {
        try {
            r96 r96 = (r96) obj;
            ba6 ba6 = (ba6) obj;
            y96 y96 = (y96) obj;
            if (r96 == null || ba6 == null || y96 == null) {
                return false;
            }
            return true;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public synchronized void a(boolean z) {
        this.b.set(z);
    }

    @DexIgnore
    public boolean a() {
        return this.b.get();
    }

    @DexIgnore
    public void a(Throwable th) {
        this.c.set(th);
    }
}
