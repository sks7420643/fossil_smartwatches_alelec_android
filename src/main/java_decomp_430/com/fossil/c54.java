package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class c54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon17 a;
    @DexIgnore
    private /* final */ /* synthetic */ List b;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback c;

    @DexIgnore
    public /* synthetic */ c54(PresetRepository.Anon17 anon17, List list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        this.a = anon17;
        this.b = list;
        this.c = deleteMappingSetCallback;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
