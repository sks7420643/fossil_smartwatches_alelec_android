package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q53 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ x53 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ Context d;
    @DexIgnore
    public /* final */ /* synthetic */ t43 e;
    @DexIgnore
    public /* final */ /* synthetic */ BroadcastReceiver.PendingResult f;

    @DexIgnore
    public q53(o53 o53, x53 x53, long j, Bundle bundle, Context context, t43 t43, BroadcastReceiver.PendingResult pendingResult) {
        this.a = x53;
        this.b = j;
        this.c = bundle;
        this.d = context;
        this.e = t43;
        this.f = pendingResult;
    }

    @DexIgnore
    public final void run() {
        long a2 = this.a.q().j.a();
        long j = this.b;
        if (a2 > 0 && (j >= a2 || j <= 0)) {
            j = a2 - 1;
        }
        if (j > 0) {
            this.c.putLong("click_timestamp", j);
        }
        this.c.putString("_cis", "referrer broadcast");
        x53.a(this.d, (mv2) null).v().a("auto", "_cmp", this.c);
        this.e.B().a("Install campaign recorded");
        BroadcastReceiver.PendingResult pendingResult = this.f;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
