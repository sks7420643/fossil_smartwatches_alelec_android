package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu4 extends df<SleepSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ PortfolioApp l;
    @DexIgnore
    public /* final */ bv4 m;
    @DexIgnore
    public /* final */ FragmentManager n;
    @DexIgnore
    public /* final */ BaseFragment o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public int f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4) {
            wg6.b(str, "mDayOfWeek");
            wg6.b(str2, "mDayOfMonth");
            wg6.b(str3, "mDailyUnit");
            wg6.b(str4, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = i;
            this.g = str3;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void c(String str) {
            wg6.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4, int i2, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) != 0 ? false : z2, (r0 & 8) != 0 ? r6 : str, (r0 & 16) != 0 ? r6 : str2, (r0 & 32) == 0 ? i : 0, (r0 & 64) != 0 ? r6 : str3, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0 ? str4 : r6);
            int i3 = i2;
            Date date2 = (i3 & 1) != 0 ? null : date;
            String str5 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final int c() {
            return this.f;
        }

        @DexIgnore
        public final void d(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.f = i;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ bh4 b;
        @DexIgnore
        public /* final */ /* synthetic */ zu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.c.m.b(a2);
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zu4 zu4, bh4 bh4, View view) {
            super(view);
            wg6.b(bh4, "binding");
            wg6.b(view, "root");
            this.c = zu4;
            this.b = bh4;
            this.b.d().setOnClickListener(new a(this));
            this.b.s.setTextColor(zu4.g);
            this.b.v.setTextColor(zu4.g);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v50, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v52, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v54, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v56, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v60, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v62, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v64, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v68, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(SleepSummary sleepSummary) {
            b a2 = this.c.a(sleepSummary);
            this.a = a2.d();
            Object r0 = this.b.u;
            wg6.a((Object) r0, "binding.ftvDayOfWeek");
            r0.setText(a2.f());
            Object r02 = this.b.t;
            wg6.a((Object) r02, "binding.ftvDayOfMonth");
            r02.setText(a2.e());
            if (a2.c() <= 0) {
                ConstraintLayout constraintLayout = this.b.q;
                wg6.a((Object) constraintLayout, "binding.clDailyValue");
                constraintLayout.setVisibility(8);
                Object r03 = this.b.s;
                wg6.a((Object) r03, "binding.ftvDailyUnit");
                r03.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                wg6.a((Object) constraintLayout2, "binding.clDailyValue");
                constraintLayout2.setVisibility(0);
                Object r04 = this.b.x;
                wg6.a((Object) r04, "binding.tvMin");
                r04.setText(String.valueOf(al4.b(a2.c())));
                Object r05 = this.b.w;
                wg6.a((Object) r05, "binding.tvHour");
                r05.setText(String.valueOf(al4.a(a2.c())));
                Object r06 = this.b.s;
                wg6.a((Object) r06, "binding.ftvDailyUnit");
                r06.setVisibility(8);
            }
            Object r07 = this.b.s;
            wg6.a((Object) r07, "binding.ftvDailyUnit");
            r07.setText(a2.b());
            Object r08 = this.b.v;
            wg6.a((Object) r08, "binding.ftvEst");
            r08.setText(a2.a());
            if (a2.g()) {
                this.b.s.setTextColor(w6.a(PortfolioApp.get.instance(), 2131099966));
                Object r09 = this.b.s;
                wg6.a((Object) r09, "binding.ftvDailyUnit");
                r09.setAllCaps(true);
            } else {
                this.b.s.setTextColor(w6.a(PortfolioApp.get.instance(), 2131099968));
                Object r010 = this.b.s;
                wg6.a((Object) r010, "binding.ftvDailyUnit");
                r010.setAllCaps(false);
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            wg6.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(true ^ a2.g());
            Object r011 = this.b.u;
            wg6.a((Object) r011, "binding.ftvDayOfWeek");
            r011.setSelected(a2.h());
            Object r012 = this.b.t;
            wg6.a((Object) r012, "binding.ftvDayOfMonth");
            r012.setSelected(a2.h());
            if (a2.h()) {
                this.b.r.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.h);
                this.b.t.setBackgroundColor(this.c.h);
                this.b.u.setTextColor(this.c.k);
                this.b.t.setTextColor(this.c.k);
            } else if (a2.g()) {
                this.b.r.setBackgroundColor(this.c.f);
                this.b.u.setBackgroundColor(this.c.f);
                this.b.t.setBackgroundColor(this.c.f);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            } else {
                this.b.r.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.j);
                this.b.t.setBackgroundColor(this.c.j);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wg6.b(str, "mWeekly");
            wg6.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ dh4 f;
        @DexIgnore
        public /* final */ /* synthetic */ zu4 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.d != null && this.a.e != null) {
                    bv4 e = this.a.g.m;
                    Date b = this.a.d;
                    if (b != null) {
                        Date a2 = this.a.e;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(zu4 zu4, dh4 dh4) {
            super(zu4, r0, r1);
            wg6.b(dh4, "binding");
            this.g = zu4;
            bh4 bh4 = dh4.r;
            if (bh4 != null) {
                wg6.a((Object) bh4, "binding.dailyItem!!");
                View d2 = dh4.d();
                wg6.a((Object) d2, "binding.root");
                this.f = dh4;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(SleepSummary sleepSummary) {
            d b = this.g.b(sleepSummary);
            this.e = b.a();
            this.d = b.b();
            Object r1 = this.f.s;
            wg6.a((Object) r1, "binding.ftvWeekly");
            r1.setText(b.c());
            Object r12 = this.f.t;
            wg6.a((Object) r12, "binding.ftvWeeklyValue");
            r12.setText(b.d());
            super.a(sleepSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ zu4 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(zu4 zu4, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = zu4;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wg6.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.o.getId() + ", isAdded=" + this.a.o.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.n.b(this.a.o.h1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                hc b3 = this.a.n.b();
                b3.a(view.getId(), this.a.o, this.a.o.h1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                hc b4 = this.a.n.b();
                b4.d(b2);
                b4.d();
                hc b5 = this.a.n.b();
                b5.a(view.getId(), this.a.o, this.a.o.h1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.o.getId() + ", isAdded2=" + this.a.o.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wg6.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zu4(cv4 cv4, PortfolioApp portfolioApp, bv4 bv4, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(cv4);
        wg6.b(cv4, "sleepDifference");
        wg6.b(portfolioApp, "mApp");
        wg6.b(bv4, "mOnItemClick");
        wg6.b(fragmentManager, "mFragmentManager");
        wg6.b(baseFragment, "mFragment");
        this.l = portfolioApp;
        this.m = bv4;
        this.n = fragmentManager;
        this.o = baseFragment;
        String b2 = ThemeManager.l.a().b("primaryText");
        this.e = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("nonBrandSurface");
        this.f = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.g = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("dianaSleepTab");
        this.h = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("secondaryText");
        this.i = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.j = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
        String b8 = ThemeManager.l.a().b("onDianaSleepTab");
        this.k = Color.parseColor(b8 == null ? "#FFFFFF" : b8);
    }

    @DexIgnore
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return zu4.super.getItemId(i2);
        }
        if (this.o.getId() == 0) {
            return 1010101;
        }
        return (long) this.o.getId();
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        SleepSummary sleepSummary = (SleepSummary) getItem(i2);
        if (sleepSummary == null) {
            return 1;
        }
        Calendar calendar = this.d;
        wg6.a((Object) calendar, "mCalendar");
        calendar.setTime(sleepSummary.getDate());
        Calendar calendar2 = this.d;
        wg6.a((Object) calendar2, "mCalendar");
        Boolean t = bk4.t(calendar2.getTime());
        wg6.a((Object) t, "DateHelper.isToday(mCalendar.time)");
        if (t.booleanValue() || this.d.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepsAdapter", "onBindViewHolder - position=" + i2 + ", viewType=" + getItemViewType(i2));
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wg6.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wg6.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardSleepsAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wg6.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((SleepSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((SleepSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((SleepSummary) getItem(i2));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            bh4 a2 = bh4.a(from, viewGroup, false);
            wg6.a((Object) a2, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wg6.a((Object) d2, "itemSleepDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            bh4 a3 = bh4.a(from, viewGroup, false);
            wg6.a((Object) a3, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wg6.a((Object) d3, "itemSleepDayBinding.root");
            return new c(this, a3, d3);
        } else {
            dh4 a4 = dh4.a(from, viewGroup, false);
            wg6.a((Object) a4, "ItemSleepWeekBinding.inf\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(cf<SleepSummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList pagedList=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardSleepsAdapter", sb.toString());
        zu4.super.b(cfVar);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r15v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final b a(SleepSummary sleepSummary) {
        MFSleepDay sleepDay;
        b bVar = new b((Date) null, false, false, (String) null, (String) null, 0, (String) null, (String) null, 255, (qg6) null);
        if (!(sleepSummary == null || (sleepDay = sleepSummary.getSleepDay()) == null)) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(sleepDay.getDate());
            int i2 = instance.get(7);
            Boolean t = bk4.t(instance.getTime());
            wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
            if (t.booleanValue()) {
                String a2 = jm4.a((Context) this.l, 2131886446);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026n_SleepToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(yk4.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            int sleepMinutes = sleepDay.getSleepMinutes();
            boolean z = true;
            if (sleepMinutes > 0) {
                bVar.a(sleepMinutes);
                bVar.b("");
                List<MFSleepSession> sleepSessions = sleepSummary.getSleepSessions();
                if (sleepSessions != null && (!sleepSessions.isEmpty())) {
                    MFSleepSession mFSleepSession = sleepSessions.get(0);
                    int startTime = mFSleepSession.getStartTime();
                    int endTime = mFSleepSession.getEndTime();
                    yk4 yk4 = yk4.b;
                    String a3 = bk4.a(((long) startTime) * 1000, mFSleepSession.getTimezoneOffset());
                    wg6.a((Object) a3, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    Object b2 = yk4.b(a3);
                    yk4 yk42 = yk4.b;
                    String a4 = bk4.a(((long) endTime) * 1000, mFSleepSession.getTimezoneOffset());
                    wg6.a((Object) a4, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    Object b3 = yk42.b(a4);
                    nh6 nh6 = nh6.a;
                    String a5 = jm4.a((Context) this.l, 2131887321);
                    wg6.a((Object) a5, "LanguageHelper.getString\u2026ing.sleep_start_end_time)");
                    Object[] objArr = {b2, b3};
                    String format = String.format(a5, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    int size = sleepSessions.size();
                    if (size > 1) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(format);
                        sb.append("\n");
                        nh6 nh62 = nh6.a;
                        String a6 = jm4.a((Context) this.l, 2131886436);
                        wg6.a((Object) a6, "LanguageHelper.getString\u2026ltiple_Label__NumberMore)");
                        Object[] objArr2 = {Integer.valueOf(size - 1)};
                        String format2 = String.format(a6, Arrays.copyOf(objArr2, objArr2.length));
                        wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                        sb.append(format2);
                        bVar.a(sb.toString());
                    } else {
                        bVar.a(format);
                    }
                }
                if (sleepDay.getGoalMinutes() > 0) {
                    if (sleepMinutes < sleepDay.getGoalMinutes()) {
                        z = false;
                    }
                    bVar.b(z);
                } else {
                    bVar.b(false);
                }
            } else {
                String a7 = jm4.a((Context) this.l, 2131886444);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026leepToday_Text__NoRecord)");
                bVar.b(a7);
                bVar.a(true);
            }
        }
        return bVar;
    }

    /* JADX WARNING: type inference failed for: r14v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00f3, code lost:
        r14 = r14.getAverageSleepOfWeek();
     */
    @DexIgnore
    public final d b(SleepSummary sleepSummary) {
        String str;
        Double averageSleepOfWeek;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (qg6) null);
        if (sleepSummary != null) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(sleepSummary.getDate());
            Boolean t = bk4.t(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String b2 = bk4.b(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String b3 = bk4.b(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            wg6.a((Object) t, "isToday");
            if (t.booleanValue()) {
                str = jm4.a((Context) this.l, 2131886448);
                wg6.a((Object) str, "LanguageHelper.getString\u2026eepToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = b3 + ' ' + i5 + " - " + b3 + ' ' + i2;
            } else if (i7 == i4) {
                str = b3 + ' ' + i5 + " - " + b2 + ' ' + i2;
            } else {
                str = b3 + ' ' + i5 + ", " + i7 + " - " + b2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            double doubleValue = (sleepDay == null || averageSleepOfWeek == null) ? 0.0d : averageSleepOfWeek.doubleValue();
            if (doubleValue <= 0.0d) {
                dVar.b("");
            } else {
                double d2 = (double) 60;
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) this.l, 2131886445);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026xt__NumberHrNumberMinAvg)");
                Object[] objArr = {Integer.valueOf((int) (doubleValue / d2)), Integer.valueOf((int) (doubleValue % d2))};
                String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                dVar.b(format);
            }
        }
        return dVar;
    }
}
