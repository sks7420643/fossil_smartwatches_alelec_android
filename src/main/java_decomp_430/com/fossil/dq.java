package com.fossil;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dq extends eq {
    @DexIgnore
    public /* final */ lq a;

    @DexIgnore
    public dq(lq lqVar) {
        this.a = lqVar;
    }

    @DexIgnore
    public kq b(up<?> upVar, Map<String, String> map) throws IOException, hp {
        try {
            HttpResponse a2 = this.a.a(upVar, map);
            int statusCode = a2.getStatusLine().getStatusCode();
            Header[] allHeaders = a2.getAllHeaders();
            ArrayList arrayList = new ArrayList(allHeaders.length);
            for (Header header : allHeaders) {
                arrayList.add(new np(header.getName(), header.getValue()));
            }
            if (a2.getEntity() == null) {
                return new kq(statusCode, arrayList);
            }
            long contentLength = a2.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new kq(statusCode, arrayList, (int) a2.getEntity().getContentLength(), a2.getEntity().getContent());
            }
            throw new IOException("Response too large: " + contentLength);
        } catch (ConnectTimeoutException e) {
            throw new SocketTimeoutException(e.getMessage());
        }
    }
}
