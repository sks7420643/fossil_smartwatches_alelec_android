package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements v {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v$a$a")
        /* renamed from: com.fossil.v$a$a  reason: collision with other inner class name */
        public static class C0048a implements v {
            @DexIgnore
            public IBinder a;

            @DexIgnore
            public C0048a(IBinder iBinder) {
                this.a = iBinder;
            }

            @DexIgnore
            public void a(int i, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.os.IResultReceiver");
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.a.transact(1, obtain, (Parcel) null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.a;
            }
        }

        @DexIgnore
        public a() {
            attachInterface(this, "android.support.v4.os.IResultReceiver");
        }

        @DexIgnore
        public static v a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof v)) {
                return new C0048a(iBinder);
            }
            return (v) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
                a(parcel.readInt(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
        }
    }

    @DexIgnore
    void a(int i, Bundle bundle) throws RemoteException;
}
