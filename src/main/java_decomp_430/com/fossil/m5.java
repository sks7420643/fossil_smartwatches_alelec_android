package com.fossil;

import com.fossil.i5;
import com.fossil.j5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m5 extends j5 {
    @DexIgnore
    public float k0; // = -1.0f;
    @DexIgnore
    public int l0; // = -1;
    @DexIgnore
    public int m0; // = -1;
    @DexIgnore
    public i5 n0; // = this.t;
    @DexIgnore
    public int o0;
    @DexIgnore
    public boolean p0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[i5.d.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[i5.d.LEFT.ordinal()] = 1;
            a[i5.d.RIGHT.ordinal()] = 2;
            a[i5.d.TOP.ordinal()] = 3;
            a[i5.d.BOTTOM.ordinal()] = 4;
            a[i5.d.BASELINE.ordinal()] = 5;
            a[i5.d.CENTER.ordinal()] = 6;
            a[i5.d.CENTER_X.ordinal()] = 7;
            a[i5.d.CENTER_Y.ordinal()] = 8;
            a[i5.d.NONE.ordinal()] = 9;
        }
        */
    }

    @DexIgnore
    public m5() {
        this.o0 = 0;
        this.p0 = false;
        new p5();
        this.B.clear();
        this.B.add(this.n0);
        int length = this.A.length;
        for (int i = 0; i < length; i++) {
            this.A[i] = this.n0;
        }
    }

    @DexIgnore
    public int K() {
        return this.o0;
    }

    @DexIgnore
    public i5 a(i5.d dVar) {
        switch (a.a[dVar.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(dVar.name());
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public ArrayList<i5> c() {
        return this.B;
    }

    @DexIgnore
    public void e(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void u(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void v(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    @DexIgnore
    public void w(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            this.B.clear();
            if (this.o0 == 1) {
                this.n0 = this.s;
            } else {
                this.n0 = this.t;
            }
            this.B.add(this.n0);
            int length = this.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.A[i2] = this.n0;
            }
        }
    }

    @DexIgnore
    public void c(z4 z4Var) {
        if (l() != null) {
            int b = z4Var.b((Object) this.n0);
            if (this.o0 == 1) {
                s(b);
                t(0);
                h(l().j());
                p(0);
                return;
            }
            s(0);
            t(b);
            p(l().t());
            h(0);
        }
    }

    @DexIgnore
    public void a(int i) {
        j5 l = l();
        if (l != null) {
            if (K() == 1) {
                this.t.d().a(1, l.t.d(), 0);
                this.v.d().a(1, l.t.d(), 0);
                if (this.l0 != -1) {
                    this.s.d().a(1, l.s.d(), this.l0);
                    this.u.d().a(1, l.s.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.s.d().a(1, l.u.d(), -this.m0);
                    this.u.d().a(1, l.u.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.k() == j5.b.FIXED) {
                    int i2 = (int) (((float) l.E) * this.k0);
                    this.s.d().a(1, l.s.d(), i2);
                    this.u.d().a(1, l.s.d(), i2);
                }
            } else {
                this.s.d().a(1, l.s.d(), 0);
                this.u.d().a(1, l.s.d(), 0);
                if (this.l0 != -1) {
                    this.t.d().a(1, l.t.d(), this.l0);
                    this.v.d().a(1, l.t.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.t.d().a(1, l.v.d(), -this.m0);
                    this.v.d().a(1, l.v.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.r() == j5.b.FIXED) {
                    int i3 = (int) (((float) l.F) * this.k0);
                    this.t.d().a(1, l.t.d(), i3);
                    this.v.d().a(1, l.t.d(), i3);
                }
            }
        }
    }

    @DexIgnore
    public void a(z4 z4Var) {
        k5 k5Var = (k5) l();
        if (k5Var != null) {
            i5 a2 = k5Var.a(i5.d.LEFT);
            i5 a3 = k5Var.a(i5.d.RIGHT);
            j5 j5Var = this.D;
            boolean z = j5Var != null && j5Var.C[0] == j5.b.WRAP_CONTENT;
            if (this.o0 == 0) {
                a2 = k5Var.a(i5.d.TOP);
                a3 = k5Var.a(i5.d.BOTTOM);
                j5 j5Var2 = this.D;
                z = j5Var2 != null && j5Var2.C[1] == j5.b.WRAP_CONTENT;
            }
            if (this.l0 != -1) {
                d5 a4 = z4Var.a((Object) this.n0);
                z4Var.a(a4, z4Var.a((Object) a2), this.l0, 6);
                if (z) {
                    z4Var.b(z4Var.a((Object) a3), a4, 0, 5);
                }
            } else if (this.m0 != -1) {
                d5 a5 = z4Var.a((Object) this.n0);
                d5 a6 = z4Var.a((Object) a3);
                z4Var.a(a5, a6, -this.m0, 6);
                if (z) {
                    z4Var.b(a5, z4Var.a((Object) a2), 0, 5);
                    z4Var.b(a6, a5, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                z4Var.a(z4.a(z4Var, z4Var.a((Object) this.n0), z4Var.a((Object) a2), z4Var.a((Object) a3), this.k0, this.p0));
            }
        }
    }
}
