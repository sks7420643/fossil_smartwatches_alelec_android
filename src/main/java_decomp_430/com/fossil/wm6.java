package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wm6 {
    @DexIgnore
    public static final uk6 a(rm6 rm6) {
        return new um6(rm6);
    }

    @DexIgnore
    public static final am6 a(rm6 rm6, am6 am6) {
        wg6.b(rm6, "$this$disposeOnCompletion");
        wg6.b(am6, "handle");
        return rm6.a((hg6<? super Throwable, cd6>) new cm6(rm6, am6));
    }

    @DexIgnore
    public static /* synthetic */ void a(af6 af6, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        vm6.a(af6, cancellationException);
    }

    @DexIgnore
    public static final void a(af6 af6, CancellationException cancellationException) {
        wg6.b(af6, "$this$cancel");
        rm6 rm6 = (rm6) af6.get(rm6.n);
        if (rm6 != null) {
            rm6.a(cancellationException);
        }
    }
}
