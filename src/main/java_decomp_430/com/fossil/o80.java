package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o80 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ q80 a;
    @DexIgnore
    public ce0 b;
    @DexIgnore
    public /* final */ ee0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<o80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                q80 valueOf = q80.valueOf(readString);
                parcel.setDataPosition(0);
                switch (ht0.a[valueOf.ordinal()]) {
                    case 1:
                        return i80.CREATOR.createFromParcel(parcel);
                    case 2:
                        return s80.CREATOR.createFromParcel(parcel);
                    case 3:
                        return t80.CREATOR.createFromParcel(parcel);
                    case 4:
                        return k80.CREATOR.createFromParcel(parcel);
                    case 5:
                        return l80.CREATOR.createFromParcel(parcel);
                    case 6:
                        return j80.CREATOR.createFromParcel(parcel);
                    case 7:
                        return m80.CREATOR.createFromParcel(parcel);
                    case 8:
                        return f80.CREATOR.createFromParcel(parcel);
                    case 9:
                        return n80.CREATOR.createFromParcel(parcel);
                    case 10:
                        return r80.CREATOR.createFromParcel(parcel);
                    case 11:
                        return h80.CREATOR.createFromParcel(parcel);
                    case 12:
                        return g80.CREATOR.createFromParcel(parcel);
                    default:
                        throw new kc6();
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new o80[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o80(q80 q80, ce0 ce0, ee0 ee0, int i) {
        this(q80, (i & 2) != 0 ? ce0.TOP_SHORT_PRESS_RELEASE : ce0, (i & 4) != 0 ? new ci1() : ee0);
    }

    @DexIgnore
    public final void a(ce0 ce0) {
        this.b = ce0;
    }

    @DexIgnore
    public JSONObject b() {
        return new JSONObject();
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            o80 o80 = (o80) obj;
            return this.a == o80.a && this.b == o80.b && !(wg6.a(this.c, o80.c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchApp");
    }

    @DexIgnore
    public final ce0 getButtonEvent() {
        return this.b;
    }

    @DexIgnore
    public final q80 getId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (this.a.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.a.a()).put("button_evt", cw0.a((Enum<?>) this.b));
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public o80(q80 q80, ce0 ce0, ee0 ee0) {
        this.a = q80;
        this.b = ce0;
        this.c = ee0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public o80(Parcel parcel) {
        this(r0, r2, (ee0) r5);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            q80 valueOf = q80.valueOf(readString);
            ce0 ce0 = ce0.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(ee0.class.getClassLoader());
            if (readParcelable != null) {
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
