package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z33 extends a53 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public z33(x53 x53) {
        super(x53);
        this.a.a(this);
    }

    @DexIgnore
    public void u() {
    }

    @DexIgnore
    public final boolean v() {
        return this.b;
    }

    @DexIgnore
    public final void w() {
        if (!v()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void x() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!z()) {
            this.a.k();
            this.b = true;
        }
    }

    @DexIgnore
    public final void y() {
        if (!this.b) {
            u();
            this.a.k();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean z();
}
