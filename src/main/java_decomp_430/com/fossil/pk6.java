package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk6 extends tm6<rm6> {
    @DexIgnore
    public /* final */ mk6<?> e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pk6(rm6 rm6, mk6<?> mk6) {
        super(rm6);
        wg6.b(rm6, "parent");
        wg6.b(mk6, "child");
        this.e = mk6;
    }

    @DexIgnore
    public void b(Throwable th) {
        mk6<?> mk6 = this.e;
        mk6.a(mk6.a((rm6) this.d));
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildContinuation[" + this.e + ']';
    }
}
