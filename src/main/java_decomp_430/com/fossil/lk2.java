package com.fossil;

import android.net.Uri;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk2 {
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> a;

    @DexIgnore
    public lk2(Map<String, Map<String, String>> map) {
        this.a = map;
    }

    @DexIgnore
    public static lk2 a() {
        return new lk2((Map<String, Map<String, String>>) null);
    }

    @DexIgnore
    public final String a(Uri uri, String str, String str2, String str3) {
        if (this.a == null) {
            return null;
        }
        if (uri != null) {
            str = uri.toString();
        } else if (str == null) {
            return null;
        }
        Map map = this.a.get(str);
        if (map == null) {
            return null;
        }
        if (str2 != null) {
            String valueOf = String.valueOf(str2);
            String valueOf2 = String.valueOf(str3);
            str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        }
        return (String) map.get(str3);
    }
}
