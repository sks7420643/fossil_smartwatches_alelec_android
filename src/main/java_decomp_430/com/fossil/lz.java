package com.fossil;

import android.graphics.drawable.Drawable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lz<R> implements iz<R>, mz<R> {
    @DexIgnore
    public static /* final */ a o; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public R e;
    @DexIgnore
    public jz f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public mt j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public void a(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }

        @DexIgnore
        public void a(Object obj) {
            obj.notifyAll();
        }
    }

    @DexIgnore
    public lz(int i2, int i3) {
        this(i2, i3, true, o);
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public synchronized void a(jz jzVar) {
        this.f = jzVar;
    }

    @DexIgnore
    public void a(xz xzVar) {
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Drawable drawable) {
    }

    @DexIgnore
    public void b(xz xzVar) {
        xzVar.a(this.a, this.b);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void c(Drawable drawable) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r3 == null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        r3.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        return true;
     */
    @DexIgnore
    public boolean cancel(boolean z) {
        jz jzVar;
        synchronized (this) {
            if (isDone()) {
                return false;
            }
            this.g = true;
            this.d.a(this);
            if (z) {
                jzVar = this.f;
                this.f = null;
            } else {
                jzVar = null;
            }
        }
    }

    @DexIgnore
    public synchronized jz d() {
        return this.f;
    }

    @DexIgnore
    public R get() throws InterruptedException, ExecutionException {
        try {
            return a((Long) null);
        } catch (TimeoutException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public synchronized boolean isCancelled() {
        return this.g;
    }

    @DexIgnore
    public synchronized boolean isDone() {
        return this.g || this.h || this.i;
    }

    @DexIgnore
    public lz(int i2, int i3, boolean z, a aVar) {
        this.a = i2;
        this.b = i3;
        this.c = z;
        this.d = aVar;
    }

    @DexIgnore
    public synchronized void a(Drawable drawable) {
    }

    @DexIgnore
    public R get(long j2, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return a(Long.valueOf(timeUnit.toMillis(j2)));
    }

    @DexIgnore
    public synchronized void a(R r, b00<? super R> b00) {
    }

    @DexIgnore
    public final synchronized R a(Long l) throws ExecutionException, InterruptedException, TimeoutException {
        if (this.c && !isDone()) {
            r00.a();
        }
        if (this.g) {
            throw new CancellationException();
        } else if (this.i) {
            throw new ExecutionException(this.j);
        } else if (this.h) {
            return this.e;
        } else {
            if (l == null) {
                this.d.a(this, 0);
            } else if (l.longValue() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                long longValue = l.longValue() + currentTimeMillis;
                while (!isDone() && currentTimeMillis < longValue) {
                    this.d.a(this, longValue - currentTimeMillis);
                    currentTimeMillis = System.currentTimeMillis();
                }
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            } else if (this.i) {
                throw new ExecutionException(this.j);
            } else if (this.g) {
                throw new CancellationException();
            } else if (this.h) {
                return this.e;
            } else {
                throw new TimeoutException();
            }
        }
    }

    @DexIgnore
    public synchronized boolean a(mt mtVar, Object obj, yz<R> yzVar, boolean z) {
        this.i = true;
        this.j = mtVar;
        this.d.a(this);
        return false;
    }

    @DexIgnore
    public synchronized boolean a(R r, Object obj, yz<R> yzVar, pr prVar, boolean z) {
        this.h = true;
        this.e = r;
        this.d.a(this);
        return false;
    }
}
