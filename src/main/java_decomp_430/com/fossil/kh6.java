package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kh6 {
    @DexIgnore
    public static /* final */ lh6 a;

    /*
    static {
        lh6 lh6 = null;
        try {
            lh6 = (lh6) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (lh6 == null) {
            lh6 = new lh6();
        }
        a = lh6;
    }
    */

    @DexIgnore
    public static hi6 a(Class cls, String str) {
        return a.a(cls, str);
    }

    @DexIgnore
    public static fi6 a(Class cls) {
        return a.a(cls);
    }

    @DexIgnore
    public static String a(xg6 xg6) {
        return a.a(xg6);
    }

    @DexIgnore
    public static String a(sg6 sg6) {
        return a.a(sg6);
    }

    @DexIgnore
    public static ii6 a(tg6 tg6) {
        a.a(tg6);
        return tg6;
    }

    @DexIgnore
    public static mi6 a(bh6 bh6) {
        a.a(bh6);
        return bh6;
    }

    @DexIgnore
    public static ki6 a(yg6 yg6) {
        a.a(yg6);
        return yg6;
    }

    @DexIgnore
    public static ni6 a(ch6 ch6) {
        a.a(ch6);
        return ch6;
    }
}
