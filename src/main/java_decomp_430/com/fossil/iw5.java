package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iw5 extends m24<b, d, c> {
    @DexIgnore
    public /* final */ DianaPresetRepository d;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public iw5(DianaPresetRepository dianaPresetRepository, CustomizeRealDataRepository customizeRealDataRepository) {
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(customizeRealDataRepository, "mRealDataRepository");
        this.d = dianaPresetRepository;
        this.e = customizeRealDataRepository;
    }

    @DexIgnore
    public String c() {
        return "DSTChangeUseCase";
    }

    @DexIgnore
    public Object a(b bVar, xe6<Object> xe6) {
        DianaPreset activePresetBySerial;
        String str;
        String e2 = PortfolioApp.get.instance().e();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(e2);
        if (!(deviceBySerial == null || jw5.a[deviceBySerial.ordinal()] != 1 || (activePresetBySerial = this.d.getActivePresetBySerial(e2)) == null)) {
            ComplicationAppMappingSettings a2 = zi4.a(activePresetBySerial.getComplications(), new Gson());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DSTChangeUseCase", "DST change, reset complications " + a2);
            PortfolioApp.get.instance().a(a2, e2);
            for (DianaPresetComplicationSetting dianaPresetComplicationSetting : activePresetBySerial.getComplications()) {
                if (wg6.a((Object) dianaPresetComplicationSetting.getId(), (Object) "second-timezone")) {
                    try {
                        SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().a(dianaPresetComplicationSetting.getSettings(), SecondTimezoneSetting.class);
                        if (secondTimezoneSetting != null) {
                            double timezoneRawOffsetById = (((double) ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())) / 60.0d) - ((double) (((float) zk4.a()) / ((float) DateTimeConstants.SECONDS_PER_HOUR)));
                            if (timezoneRawOffsetById > ((double) 0)) {
                                StringBuilder sb = new StringBuilder();
                                sb.append('+');
                                sb.append(timezoneRawOffsetById);
                                sb.append('h');
                                str = sb.toString();
                            } else {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(timezoneRawOffsetById);
                                sb2.append('h');
                                str = sb2.toString();
                            }
                            this.e.upsertCustomizeRealData(new CustomizeRealData("second-timezone", str));
                        }
                    } catch (Exception e3) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("DSTChangeUseCase", "exception " + e3);
                    }
                }
            }
        }
        return new d();
    }
}
