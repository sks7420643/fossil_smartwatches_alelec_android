package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt5 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, SignUpPresenter signUpPresenter) {
        signUpActivity.B = signUpPresenter;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, hn4 hn4) {
        signUpActivity.C = hn4;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, in4 in4) {
        signUpActivity.D = in4;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, kn4 kn4) {
        signUpActivity.E = kn4;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, MFLoginWechatManager mFLoginWechatManager) {
        signUpActivity.F = mFLoginWechatManager;
    }
}
