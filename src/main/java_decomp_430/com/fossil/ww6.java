package com.fossil;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ww6 extends xw6<AppCompatActivity> {
    @DexIgnore
    public ww6(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        h6.a((Activity) b(), strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return h6.a((Activity) b(), str);
    }

    @DexIgnore
    public FragmentManager c() {
        return ((AppCompatActivity) b()).getSupportFragmentManager();
    }

    @DexIgnore
    public Context a() {
        return (Context) b();
    }
}
