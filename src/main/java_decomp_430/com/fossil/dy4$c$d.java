package com.fossil;

import com.fossil.NotificationAppsPresenter;
import com.fossil.m24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1", f = "NotificationAppsPresenter.kt", l = {188}, m = "invokeSuspend")
public final class dy4$c$d extends sf6 implements ig6<il6, xe6<? super m24.c>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy4$c$d(NotificationAppsPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        dy4$c$d dy4_c_d = new dy4$c$d(this.this$0, xe6);
        dy4_c_d.p$ = (il6) obj;
        return dy4_c_d;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((dy4$c$d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            Object c = this.this$0.this$0.n;
            this.L$0 = il6;
            this.label = 1;
            obj = n24.a(c, null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
