package com.fossil;

import android.os.Bundle;
import com.fossil.w52;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c62 implements a62<T> {
    @DexIgnore
    public /* final */ /* synthetic */ w52 a;

    @DexIgnore
    public c62(w52 w52) {
        this.a = w52;
    }

    @DexIgnore
    public final void a(T t) {
        y52 unused = this.a.a = t;
        Iterator it = this.a.c.iterator();
        while (it.hasNext()) {
            ((w52.a) it.next()).a(this.a.a);
        }
        this.a.c.clear();
        Bundle unused2 = this.a.b = null;
    }
}
