package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m74 extends l74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131361965, 1);
        y.put(2131362196, 2);
        y.put(2131361961, 3);
        y.put(2131361956, 4);
        y.put(2131362888, 5);
        y.put(2131363188, 6);
    }
    */

    @DexIgnore
    public m74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public m74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[3], objArr[1], objArr[2], objArr[0], objArr[5], objArr[6]);
        this.w = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
