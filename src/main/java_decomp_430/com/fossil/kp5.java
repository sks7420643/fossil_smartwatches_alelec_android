package com.fossil;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp5 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ jp5 b;

    @DexIgnore
    public kp5(BaseActivity baseActivity, jp5 jp5) {
        wg6.b(baseActivity, "baseActivity");
        wg6.b(jp5, "mView");
        this.a = baseActivity;
        this.b = jp5;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final jp5 b() {
        return this.b;
    }
}
