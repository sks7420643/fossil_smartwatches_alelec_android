package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lz3 extends tz3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (qx3 == qx3.EAN_13) {
            return super.a(str, qx3, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got " + qx3);
    }

    @DexIgnore
    public boolean[] a(String str) {
        if (str.length() == 13) {
            try {
                if (sz3.a(str)) {
                    int i = kz3.f[Integer.parseInt(str.substring(0, 1))];
                    boolean[] zArr = new boolean[95];
                    int a = qz3.a(zArr, 0, sz3.a, true) + 0;
                    int i2 = 1;
                    while (i2 <= 6) {
                        int i3 = i2 + 1;
                        int parseInt = Integer.parseInt(str.substring(i2, i3));
                        if (((i >> (6 - i2)) & 1) == 1) {
                            parseInt += 10;
                        }
                        a += qz3.a(zArr, a, sz3.e[parseInt], false);
                        i2 = i3;
                    }
                    int a2 = a + qz3.a(zArr, a, sz3.b, false);
                    int i4 = 7;
                    while (i4 <= 12) {
                        int i5 = i4 + 1;
                        a2 += qz3.a(zArr, a2, sz3.d[Integer.parseInt(str.substring(i4, i5))], true);
                        i4 = i5;
                    }
                    qz3.a(zArr, a2, sz3.a, true);
                    return zArr;
                }
                throw new IllegalArgumentException("Contents do not pass checksum");
            } catch (tx3 unused) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 13 digits long, but got " + str.length());
        }
    }
}
