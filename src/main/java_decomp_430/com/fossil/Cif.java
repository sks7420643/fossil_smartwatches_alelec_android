package com.fossil;

import com.fossil.bf;
import com.fossil.cf;
import com.fossil.ef;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* renamed from: com.fossil.if  reason: invalid class name */
public class Cif<T> extends cf<T> implements ef.a {
    @DexIgnore
    public /* final */ gf<T> u;
    @DexIgnore
    public bf.a<T> v; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.if$b")
    /* renamed from: com.fossil.if$b */
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public b(int i) {
            this.a = i;
        }

        @DexIgnore
        public void run() {
            if (!Cif.this.h()) {
                Cif ifVar = Cif.this;
                int i = ifVar.d.a;
                if (ifVar.u.isInvalid()) {
                    Cif.this.c();
                    return;
                }
                int i2 = this.a * i;
                int min = Math.min(i, Cif.this.e.size() - i2);
                Cif ifVar2 = Cif.this;
                ifVar2.u.dispatchLoadRange(3, i2, min, ifVar2.a, ifVar2.v);
            }
        }
    }

    @DexIgnore
    public Cif(gf<T> gfVar, Executor executor, Executor executor2, cf.e<T> eVar, cf.h hVar, int i) {
        super(new ef(), executor, executor2, eVar, hVar);
        this.u = gfVar;
        int i2 = this.d.a;
        this.f = i;
        if (this.u.isInvalid()) {
            c();
            return;
        }
        int max = Math.max(this.d.e / i2, 2) * i2;
        this.u.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.a, this.v);
    }

    @DexIgnore
    public void a(cf<T> cfVar, cf.g gVar) {
        ef<T> efVar = cfVar.e;
        if (efVar.isEmpty() || this.e.size() != efVar.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i = this.d.a;
        int e = this.e.e() / i;
        int i2 = this.e.i();
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + e;
            int i5 = 0;
            while (i5 < this.e.i()) {
                int i6 = i4 + i5;
                if (!this.e.b(i, i6) || efVar.b(i, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                gVar.a(i4 * i, i * i5);
                i3 += i5 - 1;
            }
            i3++;
        }
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void c(int i) {
        this.b.execute(new b(i));
    }

    @DexIgnore
    public xe<?, T> d() {
        return this.u;
    }

    @DexIgnore
    public Object e() {
        return Integer.valueOf(this.f);
    }

    @DexIgnore
    public boolean g() {
        return false;
    }

    @DexIgnore
    public void b() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void c(int i, int i2) {
        d(i, i2);
    }

    @DexIgnore
    public void e(int i) {
        ef<T> efVar = this.e;
        cf.h hVar = this.d;
        efVar.a(i, hVar.b, hVar.a, (ef.a) this);
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public void a(int i) {
        e(0, i);
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.if$a")
    /* renamed from: com.fossil.if$a */
    public class a extends bf.a<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, bf<T> bfVar) {
            if (bfVar.a()) {
                Cif.this.c();
            } else if (!Cif.this.h()) {
                if (i == 0 || i == 3) {
                    List<T> list = bfVar.a;
                    if (Cif.this.e.i() == 0) {
                        Cif ifVar = Cif.this;
                        ifVar.e.a(bfVar.b, list, bfVar.c, bfVar.d, ifVar.d.a, ifVar);
                    } else {
                        Cif ifVar2 = Cif.this;
                        ifVar2.e.b(bfVar.d, list, ifVar2.f, ifVar2.d.d, ifVar2.h, ifVar2);
                    }
                    Cif ifVar3 = Cif.this;
                    if (ifVar3.c != null) {
                        boolean z = true;
                        boolean z2 = ifVar3.e.size() == 0;
                        boolean z3 = !z2 && bfVar.b == 0 && bfVar.d == 0;
                        int size = Cif.this.size();
                        if (z2 || (!(i == 0 && bfVar.c == 0) && (i != 3 || bfVar.d + Cif.this.d.a < size))) {
                            z = false;
                        }
                        Cif.this.a(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }

        @DexIgnore
        public void a(int i, Throwable th, boolean z) {
            throw new IllegalStateException("Tiled error handling not yet implemented");
        }
    }

    @DexIgnore
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }
}
