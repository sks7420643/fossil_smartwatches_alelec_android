package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im6 {
    @DexIgnore
    public static /* final */ uo6 a; // = new uo6("REMOVED_TASK");
    @DexIgnore
    public static /* final */ uo6 b; // = new uo6("CLOSED_EMPTY");

    @DexIgnore
    public static final long a(long j) {
        if (j <= 0) {
            return 0;
        }
        return j >= 9223372036854L ? ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD : 1000000 * j;
    }
}
