package com.fossil;

import android.os.Bundle;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.widget.ShareDialog;
import com.fossil.a20;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q10 {
    @DexIgnore
    public static /* final */ Set<String> a; // = new HashSet(Arrays.asList(new String[]{"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "screen_view", "firebase_extra_parameter"}));

    @DexIgnore
    public p10 a(a20 a20) {
        Bundle bundle;
        String str;
        boolean z = true;
        boolean z2 = a20.c.CUSTOM.equals(a20.c) && a20.e != null;
        boolean z3 = a20.c.PREDEFINED.equals(a20.c) && a20.g != null;
        if (!z2 && !z3) {
            return null;
        }
        if (z3) {
            bundle = b(a20);
        } else {
            bundle = new Bundle();
            Map<String, Object> map = a20.f;
            if (map != null) {
                a(bundle, map);
            }
        }
        if (z3) {
            String str2 = (String) a20.h.get("success");
            if (str2 == null || Boolean.parseBoolean(str2)) {
                z = false;
            }
            str = a(a20.g, z);
        } else {
            str = c(a20.e);
        }
        c86.g().d("Answers", "Logging event into firebase...");
        return new p10(str, bundle);
    }

    @DexIgnore
    public final Bundle b(a20 a20) {
        Bundle bundle = new Bundle();
        if ("purchase".equals(a20.g)) {
            a(bundle, "item_id", (String) a20.h.get("itemId"));
            a(bundle, "item_name", (String) a20.h.get("itemName"));
            a(bundle, "item_category", (String) a20.h.get("itemType"));
            a(bundle, "value", b(a20.h.get("itemPrice")));
            a(bundle, "currency", (String) a20.h.get("currency"));
        } else if ("addToCart".equals(a20.g)) {
            a(bundle, "item_id", (String) a20.h.get("itemId"));
            a(bundle, "item_name", (String) a20.h.get("itemName"));
            a(bundle, "item_category", (String) a20.h.get("itemType"));
            a(bundle, "price", b(a20.h.get("itemPrice")));
            a(bundle, "value", b(a20.h.get("itemPrice")));
            a(bundle, "currency", (String) a20.h.get("currency"));
            bundle.putLong("quantity", 1);
        } else if ("startCheckout".equals(a20.g)) {
            a(bundle, "quantity", Long.valueOf((long) ((Integer) a20.h.get("itemCount")).intValue()));
            a(bundle, "value", b(a20.h.get("totalPrice")));
            a(bundle, "currency", (String) a20.h.get("currency"));
        } else if ("contentView".equals(a20.g)) {
            a(bundle, "content_type", (String) a20.h.get("contentType"));
            a(bundle, "item_id", (String) a20.h.get("contentId"));
            a(bundle, "item_name", (String) a20.h.get("contentName"));
        } else if ("search".equals(a20.g)) {
            a(bundle, "search_term", (String) a20.h.get("query"));
        } else if (ShareDialog.WEB_SHARE_DIALOG.equals(a20.g)) {
            a(bundle, "method", (String) a20.h.get("method"));
            a(bundle, "content_type", (String) a20.h.get("contentType"));
            a(bundle, "item_id", (String) a20.h.get("contentId"));
            a(bundle, "item_name", (String) a20.h.get("contentName"));
        } else if ("rating".equals(a20.g)) {
            a(bundle, "rating", String.valueOf(a20.h.get("rating")));
            a(bundle, "content_type", (String) a20.h.get("contentType"));
            a(bundle, "item_id", (String) a20.h.get("contentId"));
            a(bundle, "item_name", (String) a20.h.get("contentName"));
        } else if ("signUp".equals(a20.g)) {
            a(bundle, "method", (String) a20.h.get("method"));
        } else if ("login".equals(a20.g)) {
            a(bundle, "method", (String) a20.h.get("method"));
        } else if ("invite".equals(a20.g)) {
            a(bundle, "method", (String) a20.h.get("method"));
        } else if ("levelStart".equals(a20.g)) {
            a(bundle, "level_name", (String) a20.h.get("levelName"));
        } else if ("levelEnd".equals(a20.g)) {
            a(bundle, "score", a(a20.h.get("score")));
            a(bundle, "level_name", (String) a20.h.get("levelName"));
            a(bundle, "success", b((String) a20.h.get("success")));
        }
        a(bundle, a20.f);
        return bundle;
    }

    @DexIgnore
    public final String c(String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_event";
        }
        if (a.contains(str)) {
            return "fabric_" + str;
        }
        String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    @DexIgnore
    public final String a(String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_parameter";
        }
        String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r11.equals("purchase") != false) goto L_0x00ca;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[RETURN] */
    public final String a(String str, boolean z) {
        char c;
        char c2 = 0;
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != -902468296) {
                if (hashCode != 103149417) {
                    if (hashCode == 1743324417 && str.equals("purchase")) {
                        c = 0;
                        if (c != 0) {
                            return "failed_ecommerce_purchase";
                        }
                        if (c == 1) {
                            return "failed_sign_up";
                        }
                        if (c == 2) {
                            return "failed_login";
                        }
                    }
                } else if (str.equals("login")) {
                    c = 2;
                    if (c != 0) {
                    }
                }
            } else if (str.equals("signUp")) {
                c = 1;
                if (c != 0) {
                }
            }
            c = 65535;
            if (c != 0) {
            }
        }
        switch (str.hashCode()) {
            case -2131650889:
                if (str.equals("levelEnd")) {
                    c2 = 11;
                    break;
                }
            case -1183699191:
                if (str.equals("invite")) {
                    c2 = 9;
                    break;
                }
            case -938102371:
                if (str.equals("rating")) {
                    c2 = 6;
                    break;
                }
            case -906336856:
                if (str.equals("search")) {
                    c2 = 4;
                    break;
                }
            case -902468296:
                if (str.equals("signUp")) {
                    c2 = 7;
                    break;
                }
            case -389087554:
                if (str.equals("contentView")) {
                    c2 = 3;
                    break;
                }
            case 23457852:
                if (str.equals("addToCart")) {
                    c2 = 1;
                    break;
                }
            case 103149417:
                if (str.equals("login")) {
                    c2 = 8;
                    break;
                }
            case 109400031:
                if (str.equals(ShareDialog.WEB_SHARE_DIALOG)) {
                    c2 = 5;
                    break;
                }
            case 196004670:
                if (str.equals("levelStart")) {
                    c2 = 10;
                    break;
                }
            case 1664021448:
                if (str.equals("startCheckout")) {
                    c2 = 2;
                    break;
                }
            case 1743324417:
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return "ecommerce_purchase";
            case 1:
                return "add_to_cart";
            case 2:
                return "begin_checkout";
            case 3:
                return "select_content";
            case 4:
                return "search";
            case 5:
                return ShareDialog.WEB_SHARE_DIALOG;
            case 6:
                return "rate_content";
            case 7:
                return "sign_up";
            case 8:
                return "login";
            case 9:
                return "invite";
            case 10:
                return "level_start";
            case 11:
                return "level_end";
            default:
                return c(str);
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Long l) {
        if (l != null) {
            bundle.putLong(str, l.longValue());
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Integer num) {
        if (num != null) {
            bundle.putInt(str, num.intValue());
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, String str2) {
        if (str2 != null) {
            bundle.putString(str, str2);
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Double d) {
        Double a2 = a((Object) d);
        if (a2 != null) {
            bundle.putDouble(str, a2.doubleValue());
        }
    }

    @DexIgnore
    public final Double a(Object obj) {
        String valueOf = String.valueOf(obj);
        if (valueOf == null) {
            return null;
        }
        return Double.valueOf(valueOf);
    }

    @DexIgnore
    public final void a(Bundle bundle, Map<String, Object> map) {
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            String a2 = a((String) next.getKey());
            if (value instanceof String) {
                bundle.putString(a2, next.getValue().toString());
            } else if (value instanceof Double) {
                bundle.putDouble(a2, ((Double) next.getValue()).doubleValue());
            } else if (value instanceof Long) {
                bundle.putLong(a2, ((Long) next.getValue()).longValue());
            } else if (value instanceof Integer) {
                bundle.putInt(a2, ((Integer) next.getValue()).intValue());
            }
        }
    }

    @DexIgnore
    public final Integer b(String str) {
        if (str == null) {
            return null;
        }
        return Integer.valueOf(str.equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE) ? 1 : 0);
    }

    @DexIgnore
    public final Double b(Object obj) {
        Long l = (Long) obj;
        if (l == null) {
            return null;
        }
        return Double.valueOf(new BigDecimal(l.longValue()).divide(x00.c).doubleValue());
    }
}
