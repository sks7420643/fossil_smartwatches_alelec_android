package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy4 {
    @DexIgnore
    public /* final */ uy4 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public vy4(uy4 uy4, LoaderManager loaderManager) {
        wg6.b(uy4, "mView");
        wg6.b(loaderManager, "mLoaderManager");
        this.a = uy4;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final uy4 b() {
        return this.a;
    }
}
