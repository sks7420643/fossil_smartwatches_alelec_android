package com.fossil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm3<E> extends tk3<E> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    public sm3() {
        super(new LinkedHashMap());
    }

    @DexIgnore
    public static <E> sm3<E> create() {
        return new sm3<>();
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int a = wn3.a(objectInputStream);
        setBackingMap(new LinkedHashMap());
        wn3.a(this, objectInputStream, a);
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        wn3.a(this, objectOutputStream);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int add(Object obj, int i) {
        return super.add(obj, i);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean addAll(Collection collection) {
        return super.addAll(collection);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean contains(Object obj) {
        return super.contains(obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int count(Object obj) {
        return super.count(obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Set elementSet() {
        return super.elementSet();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Set entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int remove(Object obj, int i) {
        return super.remove(obj, i);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int setCount(Object obj, int i) {
        return super.setCount(obj, i);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int size() {
        return super.size();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    public sm3(int i) {
        super(ym3.b(i));
    }

    @DexIgnore
    public static <E> sm3<E> create(int i) {
        return new sm3<>(i);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ boolean setCount(Object obj, int i, int i2) {
        return super.setCount(obj, i, i2);
    }

    @DexIgnore
    public static <E> sm3<E> create(Iterable<? extends E> iterable) {
        sm3<E> create = create(en3.b(iterable));
        pm3.a(create, iterable);
        return create;
    }
}
