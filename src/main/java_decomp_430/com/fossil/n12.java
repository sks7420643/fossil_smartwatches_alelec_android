package com.fossil;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n12 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends db2 implements n12 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n12$a$a")
        /* renamed from: com.fossil.n12$a$a  reason: collision with other inner class name */
        public static class C0031a extends cb2 implements n12 {
            @DexIgnore
            public C0031a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            public final Account g() throws RemoteException {
                Parcel a = a(2, zza());
                Account account = (Account) eb2.a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        @DexIgnore
        public static n12 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof n12) {
                return (n12) queryLocalInterface;
            }
            return new C0031a(iBinder);
        }
    }

    @DexIgnore
    Account g() throws RemoteException;
}
