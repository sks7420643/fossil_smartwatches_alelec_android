package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm2 {
    @DexIgnore
    public /* final */ pm2 a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public gm2(int i) {
        this.b = new byte[i];
        this.a = pm2.a(this.b);
    }

    @DexIgnore
    public final yl2 a() {
        this.a.b();
        return new im2(this.b);
    }

    @DexIgnore
    public final pm2 b() {
        return this.a;
    }

    @DexIgnore
    public /* synthetic */ gm2(int i, bm2 bm2) {
        this(i);
    }
}
