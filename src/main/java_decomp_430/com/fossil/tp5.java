package com.fossil;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.is4;
import com.fossil.ls4;
import com.fossil.m24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp5 extends td {
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public Location b;
    @DexIgnore
    public /* final */ Geocoder c; // = new Geocoder(this.d);
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GetUserLocation e;
    @DexIgnore
    public /* final */ GetAddress f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Double b;
        @DexIgnore
        public Double c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public b(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
            this.a = num;
            this.b = d2;
            this.c = d3;
            this.d = str;
            this.e = bool;
            this.f = bool2;
            this.g = bool3;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final Boolean b() {
            return this.e;
        }

        @DexIgnore
        public final Integer c() {
            return this.a;
        }

        @DexIgnore
        public final Double d() {
            return this.b;
        }

        @DexIgnore
        public final Double e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && wg6.a((Object) this.b, (Object) bVar.b) && wg6.a((Object) this.c, (Object) bVar.c) && wg6.a((Object) this.d, (Object) bVar.d) && wg6.a((Object) this.e, (Object) bVar.e) && wg6.a((Object) this.f, (Object) bVar.f) && wg6.a((Object) this.g, (Object) bVar.g);
        }

        @DexIgnore
        public final Boolean f() {
            return this.g;
        }

        @DexIgnore
        public final Boolean g() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.a;
            int i = 0;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            Double d2 = this.b;
            int hashCode2 = (hashCode + (d2 != null ? d2.hashCode() : 0)) * 31;
            Double d3 = this.c;
            int hashCode3 = (hashCode2 + (d3 != null ? d3.hashCode() : 0)) * 31;
            String str = this.d;
            int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
            Boolean bool = this.e;
            int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.f;
            int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.g;
            if (bool3 != null) {
                i = bool3.hashCode();
            }
            return hashCode6 + i;
        }

        @DexIgnore
        public String toString() {
            return "UiDataWrapper(errorNetwork=" + this.a + ", latitude=" + this.b + ", longitude=" + this.c + ", address=" + this.d + ", enableBtnComfirm=" + this.e + ", isShowDialogLoading=" + this.f + ", isPermissionGianted=" + this.g + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<is4.d, is4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ tp5 a;
        @DexIgnore
        public /* final */ /* synthetic */ double b;
        @DexIgnore
        public /* final */ /* synthetic */ double c;

        @DexIgnore
        public c(tp5 tp5, double d, double d2) {
            this.a = tp5;
            this.b = d;
            this.c = d2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetAddress.d dVar) {
            wg6.b(dVar, "responseValue");
            String a2 = dVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "GetCityName onSuccess - address: " + a2 + " lat " + this.b + " long " + this.c);
            this.a.b = new Location("gps");
            Location a3 = this.a.b;
            if (a3 != null) {
                a3.setLatitude(this.b);
                Location a4 = this.a.b;
                if (a4 != null) {
                    a4.setLongitude(this.c);
                    tp5.a(this.a, (Integer) null, (Double) null, (Double) null, a2, true, false, (Boolean) null, 71, (Object) null);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r12v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(GetAddress.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetCityName onError");
            tp5.a(this.a, (Integer) null, (Double) null, (Double) null, jm4.a((Context) PortfolioApp.get.instance(), 2131886384), (Boolean) null, false, (Boolean) null, 87, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ls4.d, ls4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ tp5 a;

        @DexIgnore
        public d(tp5 tp5) {
            this.a = tp5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetUserLocation.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onSuccess");
            this.a.b(dVar.a(), dVar.b());
        }

        @DexIgnore
        public void a(GetUserLocation.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onError");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public tp5(PortfolioApp portfolioApp, GetUserLocation getUserLocation, GetAddress getAddress) {
        wg6.b(portfolioApp, "mApp");
        wg6.b(getUserLocation, "mGetLocation");
        wg6.b(getAddress, "mGetAddress");
        this.d = portfolioApp;
        this.e = getUserLocation;
        this.f = getAddress;
    }

    @DexIgnore
    public final LiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.fossil.tp5$d, com.portfolio.platform.CoroutineUseCase$e] */
    public final void c() {
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation");
        this.e.a(new GetUserLocation.b(), new d(this));
    }

    @DexIgnore
    public final Location d() {
        return this.b;
    }

    @DexIgnore
    public final void b(double d2, double d3) {
        double d4 = d2;
        double d5 = d3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "handleLocation latitude=" + d4 + ", longitude=" + d5);
        if (d4 != 0.0d && d5 != 0.0d) {
            a(this, (Integer) null, Double.valueOf(d2), Double.valueOf(d3), (String) null, (Boolean) null, (Boolean) null, (Boolean) null, 121, (Object) null);
            a(d2, d3);
        }
    }

    @DexIgnore
    public final void a(double d2, double d3, String str) {
        wg6.b(str, "address");
        if (d2 != 0.0d && d3 != 0.0d && !TextUtils.isEmpty(str)) {
            a(this, (Integer) null, Double.valueOf(d2), Double.valueOf(d3), str, (Boolean) null, false, (Boolean) null, 81, (Object) null);
        } else if (!TextUtils.isEmpty(str)) {
            a(str);
        } else {
            c();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r13v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean a() {
        if (!NetworkUtils.isNetworkAvailable(this.d)) {
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "No internet connection");
            a(this, 601, (Double) null, (Double) null, (String) null, (Boolean) null, (Boolean) null, (Boolean) null, 126, (Object) null);
            return false;
        } else if (xm4.a(xm4.d, this.d, "FIND_DEVICE", false, false, false, 28, (Object) null)) {
            return true;
        } else {
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "No permsssion gianted");
            return false;
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "address");
        try {
            List<Address> fromLocationName = this.c.getFromLocationName(str, 5);
            wg6.a((Object) fromLocationName, "location");
            if (!fromLocationName.isEmpty()) {
                Address address = fromLocationName.get(0);
                wg6.a((Object) address, "location[0]");
                Double valueOf = Double.valueOf(address.getLatitude());
                Address address2 = fromLocationName.get(0);
                wg6.a((Object) address2, "location[0]");
                a(this, (Integer) null, valueOf, Double.valueOf(address2.getLongitude()), str, (Boolean) null, (Boolean) null, (Boolean) null, 113, (Object) null);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "No lat lng found for address " + str);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MapPickerViewModel", "Exception when get lat lng from address " + str + ' ' + e2);
            c();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.device.locate.map.usecase.GetAddress] */
    public final void a(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "GetCityName at lat: " + d2 + ", lng: " + d3);
        this.f.a(new GetAddress.b(d2, d3), new c(this, d2, d3));
    }

    @DexIgnore
    public static /* synthetic */ void a(tp5 tp5, Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            d2 = null;
        }
        if ((i & 4) != 0) {
            d3 = null;
        }
        if ((i & 8) != 0) {
            str = null;
        }
        if ((i & 16) != 0) {
            bool = null;
        }
        if ((i & 32) != 0) {
            bool2 = null;
        }
        if ((i & 64) != 0) {
            bool3 = null;
        }
        tp5.a(num, d2, d3, str, bool, bool2, bool3);
    }

    @DexIgnore
    public final void a(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
        this.a.a(new b(num, d2, d3, str, bool, bool2, bool3));
    }
}
