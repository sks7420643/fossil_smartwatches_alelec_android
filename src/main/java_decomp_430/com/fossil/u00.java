package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u00 {
    @DexIgnore
    public static u00 b() {
        return new b();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends u00 {
        @DexIgnore
        public volatile boolean a;

        @DexIgnore
        public b() {
            super();
        }

        @DexIgnore
        public void a() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
        }
    }

    @DexIgnore
    public u00() {
    }
}
