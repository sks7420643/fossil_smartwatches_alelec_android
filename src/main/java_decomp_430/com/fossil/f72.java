package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f72 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<f72> CREATOR; // = new x72();
    @DexIgnore
    public static /* final */ int[] i; // = new int[0];
    @DexIgnore
    public /* final */ DataType a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ g72 d;
    @DexIgnore
    public /* final */ t72 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int[] g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public DataType a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public String c;
        @DexIgnore
        public g72 d;
        @DexIgnore
        public t72 e;
        @DexIgnore
        public String f; // = "";
        @DexIgnore
        public int[] g;

        @DexIgnore
        public final a a(DataType dataType) {
            this.a = dataType;
            return this;
        }

        @DexIgnore
        @Deprecated
        public final a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        public final a a(int i) {
            this.b = i;
            return this;
        }

        @DexIgnore
        public final a a(g72 g72) {
            this.d = g72;
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            this.e = t72.e(str);
            return this;
        }

        @DexIgnore
        public final a a(Context context) {
            a(context.getPackageName());
            return this;
        }

        @DexIgnore
        public final f72 a() {
            boolean z = true;
            w12.b(this.a != null, "Must set data type");
            if (this.b < 0) {
                z = false;
            }
            w12.b(z, "Must set data source type");
            return new f72(this);
        }
    }

    @DexIgnore
    public f72(DataType dataType, String str, int i2, g72 g72, t72 t72, String str2, int[] iArr) {
        this.a = dataType;
        this.c = i2;
        this.b = str;
        this.d = g72;
        this.e = t72;
        this.f = str2;
        this.h = K();
        this.g = iArr == null ? i : iArr;
    }

    @DexIgnore
    public DataType B() {
        return this.a;
    }

    @DexIgnore
    public g72 C() {
        return this.d;
    }

    @DexIgnore
    @Deprecated
    public String D() {
        return this.b;
    }

    @DexIgnore
    public String E() {
        return this.h;
    }

    @DexIgnore
    public String F() {
        return this.f;
    }

    @DexIgnore
    public int G() {
        return this.c;
    }

    @DexIgnore
    public final String H() {
        int i2 = this.c;
        if (i2 == 0) {
            return OrmLiteConfigUtil.RAW_DIR_NAME;
        }
        if (i2 != 1) {
        }
        return "derived";
    }

    @DexIgnore
    public final String I() {
        String str;
        String str2;
        int i2 = this.c;
        String str3 = i2 != 0 ? i2 != 1 ? "?" : "d" : "r";
        String E = this.a.E();
        t72 t72 = this.e;
        String str4 = "";
        if (t72 == null) {
            str = str4;
        } else if (t72.equals(t72.b)) {
            str = ":gms";
        } else {
            String valueOf = String.valueOf(this.e.p());
            str = valueOf.length() != 0 ? ":".concat(valueOf) : new String(":");
        }
        g72 g72 = this.d;
        if (g72 != null) {
            String B = g72.B();
            String E2 = this.d.E();
            StringBuilder sb = new StringBuilder(String.valueOf(B).length() + 2 + String.valueOf(E2).length());
            sb.append(":");
            sb.append(B);
            sb.append(":");
            sb.append(E2);
            str2 = sb.toString();
        } else {
            str2 = str4;
        }
        String str5 = this.f;
        if (str5 != null) {
            String valueOf2 = String.valueOf(str5);
            str4 = valueOf2.length() != 0 ? ":".concat(valueOf2) : new String(":");
        }
        StringBuilder sb2 = new StringBuilder(str3.length() + 1 + String.valueOf(E).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str4).length());
        sb2.append(str3);
        sb2.append(":");
        sb2.append(E);
        sb2.append(str);
        sb2.append(str2);
        sb2.append(str4);
        return sb2.toString();
    }

    @DexIgnore
    public final t72 J() {
        return this.e;
    }

    @DexIgnore
    public final String K() {
        StringBuilder sb = new StringBuilder();
        sb.append(H());
        sb.append(":");
        sb.append(this.a.B());
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e.p());
        }
        if (this.d != null) {
            sb.append(":");
            sb.append(this.d.C());
        }
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        return sb.toString();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f72)) {
            return false;
        }
        return this.h.equals(((f72) obj).h);
    }

    @DexIgnore
    public int hashCode() {
        return this.h.hashCode();
    }

    @DexIgnore
    @Deprecated
    public int[] p() {
        return this.g;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("DataSource{");
        sb.append(H());
        if (this.b != null) {
            sb.append(":");
            sb.append(this.b);
        }
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e);
        }
        if (this.d != null) {
            sb.append(":");
            sb.append(this.d);
        }
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        sb.append(":");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) B(), i2, false);
        g22.a(parcel, 2, D(), false);
        g22.a(parcel, 3, G());
        g22.a(parcel, 4, (Parcelable) C(), i2, false);
        g22.a(parcel, 5, (Parcelable) this.e, i2, false);
        g22.a(parcel, 6, F(), false);
        g22.a(parcel, 8, p(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public f72(a aVar) {
        this.a = aVar.a;
        this.c = aVar.b;
        this.b = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.h = K();
        this.g = aVar.g;
    }
}
