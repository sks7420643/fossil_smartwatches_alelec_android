package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ie5 implements Factory<he5> {
    @DexIgnore
    public static GoalTrackingOverviewDayPresenter a(ge5 ge5, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewDayPresenter(ge5, an4, goalTrackingRepository);
    }
}
