package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e15 implements Factory<d15> {
    @DexIgnore
    public static /* final */ e15 a; // = new e15();

    @DexIgnore
    public static e15 a() {
        return a;
    }

    @DexIgnore
    public static d15 b() {
        return new d15();
    }

    @DexIgnore
    public d15 get() {
        return b();
    }
}
