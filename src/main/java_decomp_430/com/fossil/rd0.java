package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum rd0 {
    TOP((byte) 16),
    MIDDLE((byte) 32),
    BOTTOM((byte) 48);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public rd0(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
