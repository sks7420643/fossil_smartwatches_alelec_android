package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z6 {
    @DexIgnore
    public /* final */ Shader a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public int c;

    @DexIgnore
    public z6(Shader shader, ColorStateList colorStateList, int i) {
        this.a = shader;
        this.b = colorStateList;
        this.c = i;
    }

    @DexIgnore
    public static z6 a(Shader shader) {
        return new z6(shader, (ColorStateList) null, 0);
    }

    @DexIgnore
    public static z6 b(int i) {
        return new z6((Shader) null, (ColorStateList) null, i);
    }

    @DexIgnore
    public boolean c() {
        return this.a != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.b;
     */
    @DexIgnore
    public boolean d() {
        ColorStateList colorStateList;
        return this.a == null && colorStateList != null && colorStateList.isStateful();
    }

    @DexIgnore
    public boolean e() {
        return c() || this.c != 0;
    }

    @DexIgnore
    public static z6 a(ColorStateList colorStateList) {
        return new z6((Shader) null, colorStateList, colorStateList.getDefaultColor());
    }

    @DexIgnore
    public Shader b() {
        return this.a;
    }

    @DexIgnore
    public static z6 b(Resources resources, int i, Resources.Theme theme) {
        try {
            return a(resources, i, theme);
        } catch (Exception e) {
            Log.e("ComplexColorCompat", "Failed to inflate ComplexColor.", e);
            return null;
        }
    }

    @DexIgnore
    public int a() {
        return this.c;
    }

    @DexIgnore
    public void a(int i) {
        this.c = i;
    }

    @DexIgnore
    public boolean a(int[] iArr) {
        if (d()) {
            ColorStateList colorStateList = this.b;
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (colorForState != this.c) {
                this.c = colorForState;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static z6 a(Resources resources, int i, Resources.Theme theme) throws IOException, XmlPullParserException {
        int next;
        XmlResourceParser xml = resources.getXml(i);
        AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
        do {
            next = xml.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            String name = xml.getName();
            char c2 = 65535;
            int hashCode = name.hashCode();
            if (hashCode != 89650992) {
                if (hashCode == 1191572447 && name.equals("selector")) {
                    c2 = 0;
                }
            } else if (name.equals("gradient")) {
                c2 = 1;
            }
            if (c2 == 0) {
                return a(y6.a(resources, (XmlPullParser) xml, asAttributeSet, theme));
            }
            if (c2 == 1) {
                return a(b7.a(resources, xml, asAttributeSet, theme));
            }
            throw new XmlPullParserException(xml.getPositionDescription() + ": unsupported complex color tag " + name);
        }
        throw new XmlPullParserException("No start tag found");
    }
}
