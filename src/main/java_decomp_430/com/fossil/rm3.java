package com.fossil;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm3<T> extends jn3<Iterable<T>> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Comparator<? super T> elementOrder;

    @DexIgnore
    public rm3(Comparator<? super T> comparator) {
        this.elementOrder = comparator;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof rm3) {
            return this.elementOrder.equals(((rm3) obj).elementOrder);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.elementOrder.hashCode() ^ 2075626741;
    }

    @DexIgnore
    public String toString() {
        return this.elementOrder + ".lexicographical()";
    }

    @DexIgnore
    public int compare(Iterable<T> iterable, Iterable<T> iterable2) {
        Iterator<T> it = iterable2.iterator();
        for (T compare : iterable) {
            if (!it.hasNext()) {
                return 1;
            }
            int compare2 = this.elementOrder.compare(compare, it.next());
            if (compare2 != 0) {
                return compare2;
            }
        }
        return it.hasNext() ? -1 : 0;
    }
}
