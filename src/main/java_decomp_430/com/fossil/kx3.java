package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements b {
        @DexIgnore
        public /* final */ /* synthetic */ sw3 a;

        @DexIgnore
        public a(sw3 sw3) {
            this.a = sw3;
        }

        @DexIgnore
        public byte a(int i) {
            return this.a.byteAt(i);
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        byte a(int i);

        @DexIgnore
        int size();
    }

    @DexIgnore
    public static String a(b bVar) {
        StringBuilder sb = new StringBuilder(bVar.size());
        for (int i = 0; i < bVar.size(); i++) {
            byte a2 = bVar.a(i);
            if (a2 == 34) {
                sb.append("\\\"");
            } else if (a2 == 39) {
                sb.append("\\'");
            } else if (a2 != 92) {
                switch (a2) {
                    case 7:
                        sb.append("\\a");
                        break;
                    case 8:
                        sb.append("\\b");
                        break;
                    case 9:
                        sb.append("\\t");
                        break;
                    case 10:
                        sb.append("\\n");
                        break;
                    case 11:
                        sb.append("\\v");
                        break;
                    case 12:
                        sb.append("\\f");
                        break;
                    case 13:
                        sb.append("\\r");
                        break;
                    default:
                        if (a2 >= 32 && a2 <= 126) {
                            sb.append((char) a2);
                            break;
                        } else {
                            sb.append('\\');
                            sb.append((char) (((a2 >>> 6) & 3) + 48));
                            sb.append((char) (((a2 >>> 3) & 7) + 48));
                            sb.append((char) ((a2 & 7) + 48));
                            break;
                        }
                        break;
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static String a(sw3 sw3) {
        return a((b) new a(sw3));
    }

    @DexIgnore
    public static String a(String str) {
        return a(sw3.copyFromUtf8(str));
    }
}
