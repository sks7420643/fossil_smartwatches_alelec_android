package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ua4 extends ta4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = new ViewDataBinding.j(28);
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public long G;

    /*
    static {
        H.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558805});
        I.put(2131362293, 3);
        I.put(2131362292, 4);
        I.put(2131362649, 5);
        I.put(2131361955, 6);
        I.put(2131362865, 7);
        I.put(2131362402, 8);
        I.put(2131362672, 9);
        I.put(2131363267, 10);
        I.put(2131362668, 11);
        I.put(2131362299, 12);
        I.put(2131363268, 13);
        I.put(2131362694, 14);
        I.put(2131362449, 15);
        I.put(2131363269, 16);
        I.put(2131362333, 17);
        I.put(2131362419, 18);
        I.put(2131362989, 19);
        I.put(2131362069, 20);
        I.put(2131362422, 21);
        I.put(2131362687, 22);
        I.put(2131362423, 23);
        I.put(2131362420, 24);
        I.put(2131362686, 25);
        I.put(2131362421, 26);
        I.put(2131362395, 27);
    }
    */

    @DexIgnore
    public ua4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 28, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
        ViewDataBinding.d(this.x);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.x.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 2;
        }
        this.x.f();
        g();
    }

    @DexIgnore
    public ua4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 1, objArr[6], objArr[1], objArr[20], objArr[4], objArr[3], objArr[12], objArr[17], objArr[27], objArr[8], objArr[18], objArr[24], objArr[26], objArr[21], objArr[23], objArr[15], objArr[2], objArr[5], objArr[11], objArr[9], objArr[25], objArr[22], objArr[14], objArr[0], objArr[7], objArr[19], objArr[10], objArr[13], objArr[16]);
        this.G = -1;
        this.r.setTag((Object) null);
        this.D.setTag((Object) null);
        a(view);
        f();
    }
}
