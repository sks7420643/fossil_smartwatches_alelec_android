package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class td4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RecyclerViewAlphabetIndex u;
    @DexIgnore
    public /* final */ FlexibleEditText v;
    @DexIgnore
    public /* final */ RecyclerView w;

    @DexIgnore
    public td4(Object obj, View view, int i, RTLImageView rTLImageView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2, ConstraintLayout constraintLayout, RecyclerViewAlphabetIndex recyclerViewAlphabetIndex, FlexibleEditText flexibleEditText, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = imageView;
        this.s = flexibleTextView2;
        this.t = constraintLayout;
        this.u = recyclerViewAlphabetIndex;
        this.v = flexibleEditText;
        this.w = recyclerView;
    }
}
