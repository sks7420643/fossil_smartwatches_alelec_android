package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg5 implements Factory<wg5> {
    @DexIgnore
    public static SleepOverviewWeekPresenter a(vg5 vg5, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        return new SleepOverviewWeekPresenter(vg5, userRepository, sleepSummariesRepository, portfolioApp);
    }
}
