package com.fossil;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class yr3 implements Runnable {
    @DexIgnore
    public /* final */ zr3 a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public yr3(zr3 zr3, Intent intent) {
        this.a = zr3;
        this.b = intent;
    }

    @DexIgnore
    public final void run() {
        zr3 zr3 = this.a;
        String action = this.b.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("FirebaseInstanceId", sb.toString());
        zr3.a();
    }
}
