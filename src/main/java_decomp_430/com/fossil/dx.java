package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx implements zr<Bitmap, Bitmap> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements rt<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap a;

        @DexIgnore
        public a(Bitmap bitmap) {
            this.a = bitmap;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public int b() {
            return r00.a(this.a);
        }

        @DexIgnore
        public Class<Bitmap> c() {
            return Bitmap.class;
        }

        @DexIgnore
        public Bitmap get() {
            return this.a;
        }
    }

    @DexIgnore
    public boolean a(Bitmap bitmap, xr xrVar) {
        return true;
    }

    @DexIgnore
    public rt<Bitmap> a(Bitmap bitmap, int i, int i2, xr xrVar) {
        return new a(bitmap);
    }
}
