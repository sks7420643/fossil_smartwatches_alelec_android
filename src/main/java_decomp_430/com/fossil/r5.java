package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r5 extends s5 {
    @DexIgnore
    public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    public void a(int i) {
        if (this.b == 0 || this.c != ((float) i)) {
            this.c = (float) i;
            if (this.b == 1) {
                b();
            }
            a();
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void f() {
        this.b = 2;
    }
}
