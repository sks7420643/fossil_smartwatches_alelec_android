package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr0 implements Parcelable.Creator<zs0> {
    @DexIgnore
    public /* synthetic */ gr0(qg6 qg6) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0059 A[LOOP:1: B:11:0x0048->B:13:0x0059, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005b A[EDGE_INSN: B:30:0x005b->B:14:0x005b ?: BREAK  , SYNTHETIC] */
    public final zs0 a(byte b, byte[] bArr) throws IllegalArgumentException {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < bArr.length) {
            short b2 = cw0.b(order.get(i));
            int i2 = i + 1;
            int i3 = i2 + b2;
            if (i3 <= bArr.length) {
                byte[] a = md6.a(bArr, i2, i3);
                if (a.length % 3 == 0) {
                    uh6 a2 = ci6.a(ci6.d(0, a.length), 3);
                    int a3 = a2.a();
                    int b3 = a2.b();
                    int c = a2.c();
                    if (c >= 0) {
                        if (a3 > b3) {
                        }
                        while (true) {
                            arrayList.add(lr0.CREATOR.a(md6.a(a, a3, a3 + 3)));
                            if (a3 != b3) {
                                break;
                            }
                            a3 += c;
                        }
                    } else {
                        if (a3 < b3) {
                        }
                        while (true) {
                            arrayList.add(lr0.CREATOR.a(md6.a(a, a3, a3 + 3)));
                            if (a3 != b3) {
                            }
                            a3 += c;
                        }
                    }
                    i += b2 + 1;
                } else {
                    throw new IllegalArgumentException(ze0.a(ze0.b("Chain size ("), a.length, ") is not ", "divide to 3."));
                }
            } else {
                throw new IllegalArgumentException("Invalid Chain length " + '(' + i3 + "), " + "remain " + bArr.length + '.');
            }
        }
        Object[] array = arrayList.toArray(new lr0[0]);
        if (array != null) {
            return new zs0(b, (lr0[]) array, bArr);
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public zs0 createFromParcel(Parcel parcel) {
        return new zs0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new zs0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m20createFromParcel(Parcel parcel) {
        return new zs0(parcel, (qg6) null);
    }
}
