package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lb5 {
    @DexIgnore
    public /* final */ gb5 a;
    @DexIgnore
    public /* final */ vb5 b;
    @DexIgnore
    public /* final */ qb5 c;

    @DexIgnore
    public lb5(gb5 gb5, vb5 vb5, qb5 qb5) {
        wg6.b(gb5, "mActiveTimeOverviewDayView");
        wg6.b(vb5, "mActiveTimeOverviewWeekView");
        wg6.b(qb5, "mActiveTimeOverviewMonthView");
        this.a = gb5;
        this.b = vb5;
        this.c = qb5;
    }

    @DexIgnore
    public final gb5 a() {
        return this.a;
    }

    @DexIgnore
    public final qb5 b() {
        return this.c;
    }

    @DexIgnore
    public final vb5 c() {
        return this.b;
    }
}
