package com.fossil;

import android.graphics.Bitmap;
import com.fossil.pw;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bx implements zr<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ pw a;
    @DexIgnore
    public /* final */ xt b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements pw.b {
        @DexIgnore
        public /* final */ zw a;
        @DexIgnore
        public /* final */ k00 b;

        @DexIgnore
        public a(zw zwVar, k00 k00) {
            this.a = zwVar;
            this.b = k00;
        }

        @DexIgnore
        public void a() {
            this.a.k();
        }

        @DexIgnore
        public void a(au auVar, Bitmap bitmap) throws IOException {
            IOException k = this.b.k();
            if (k != null) {
                if (bitmap != null) {
                    auVar.a(bitmap);
                }
                throw k;
            }
        }
    }

    @DexIgnore
    public bx(pw pwVar, xt xtVar) {
        this.a = pwVar;
        this.b = xtVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, xr xrVar) {
        return this.a.a(inputStream);
    }

    @DexIgnore
    public rt<Bitmap> a(InputStream inputStream, int i, int i2, xr xrVar) throws IOException {
        zw zwVar;
        boolean z;
        if (inputStream instanceof zw) {
            zwVar = (zw) inputStream;
            z = false;
        } else {
            zwVar = new zw(inputStream, this.b);
            z = true;
        }
        k00 b2 = k00.b(zwVar);
        try {
            return this.a.a((InputStream) new o00(b2), i, i2, xrVar, (pw.b) new a(zwVar, b2));
        } finally {
            b2.l();
            if (z) {
                zwVar.l();
            }
        }
    }
}
