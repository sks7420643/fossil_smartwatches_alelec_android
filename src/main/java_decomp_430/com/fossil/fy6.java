package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy6 implements fx6<zq6, Long> {
    @DexIgnore
    public static /* final */ fy6 a; // = new fy6();

    @DexIgnore
    public Long a(zq6 zq6) throws IOException {
        return Long.valueOf(zq6.string());
    }
}
