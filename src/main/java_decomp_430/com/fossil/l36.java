package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l36 extends Exception {
    @DexIgnore
    public l36() {
    }

    @DexIgnore
    public l36(String str) {
        super(str);
    }

    @DexIgnore
    public l36(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public l36(Throwable th) {
        super(th);
    }
}
