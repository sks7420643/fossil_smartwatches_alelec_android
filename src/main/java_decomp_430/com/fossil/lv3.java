package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv3 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer s; // = new a();
    @DexIgnore
    public static /* final */ nu3 t; // = new nu3("closed");
    @DexIgnore
    public /* final */ List<JsonElement> p; // = new ArrayList();
    @DexIgnore
    public String q;
    @DexIgnore
    public JsonElement r; // = ju3.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Writer {
        @DexIgnore
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public lv3() {
        super(s);
    }

    @DexIgnore
    public JsonWriter F() throws IOException {
        a((JsonElement) ju3.a);
        return this;
    }

    @DexIgnore
    public final JsonElement G() {
        List<JsonElement> list = this.p;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public JsonElement I() {
        if (this.p.isEmpty()) {
            return this.r;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.p);
    }

    @DexIgnore
    public final void a(JsonElement jsonElement) {
        if (this.q != null) {
            if (!jsonElement.h() || B()) {
                ((ku3) G()).a(this.q, jsonElement);
            }
            this.q = null;
        } else if (this.p.isEmpty()) {
            this.r = jsonElement;
        } else {
            JsonElement G = G();
            if (G instanceof fu3) {
                ((fu3) G).a(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void close() throws IOException {
        if (this.p.isEmpty()) {
            this.p.add(t);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    public JsonWriter d(boolean z) throws IOException {
        a((JsonElement) new nu3(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    public JsonWriter e(String str) throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (G() instanceof ku3) {
            this.q = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void flush() throws IOException {
    }

    @DexIgnore
    public JsonWriter h(String str) throws IOException {
        if (str == null) {
            F();
            return this;
        }
        a((JsonElement) new nu3(str));
        return this;
    }

    @DexIgnore
    public JsonWriter m() throws IOException {
        fu3 fu3 = new fu3();
        a((JsonElement) fu3);
        this.p.add(fu3);
        return this;
    }

    @DexIgnore
    public JsonWriter n() throws IOException {
        ku3 ku3 = new ku3();
        a((JsonElement) ku3);
        this.p.add(ku3);
        return this;
    }

    @DexIgnore
    public JsonWriter o() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (G() instanceof fu3) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter p() throws IOException {
        if (this.p.isEmpty() || this.q != null) {
            throw new IllegalStateException();
        } else if (G() instanceof ku3) {
            List<JsonElement> list = this.p;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public JsonWriter a(Boolean bool) throws IOException {
        if (bool == null) {
            F();
            return this;
        }
        a((JsonElement) new nu3(bool));
        return this;
    }

    @DexIgnore
    public JsonWriter a(long j) throws IOException {
        a((JsonElement) new nu3((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    public JsonWriter a(Number number) throws IOException {
        if (number == null) {
            F();
            return this;
        }
        if (!D()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a((JsonElement) new nu3(number));
        return this;
    }
}
