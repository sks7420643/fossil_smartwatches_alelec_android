package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qn2 extends IOException {
    @DexIgnore
    public ro2 zza; // = null;

    @DexIgnore
    public qn2(String str) {
        super(str);
    }

    @DexIgnore
    public static qn2 zza() {
        return new qn2("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    @DexIgnore
    public static qn2 zzb() {
        return new qn2("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static qn2 zzc() {
        return new qn2("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static qn2 zzd() {
        return new qn2("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static qn2 zze() {
        return new qn2("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static pn2 zzf() {
        return new pn2("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static qn2 zzg() {
        return new qn2("Failed to parse the message.");
    }

    @DexIgnore
    public static qn2 zzh() {
        return new qn2("Protocol message had invalid UTF-8.");
    }
}
