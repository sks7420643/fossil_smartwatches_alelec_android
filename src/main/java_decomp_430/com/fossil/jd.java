package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jd<T> extends MutableLiveData<T> {
    @DexIgnore
    public u3<LiveData<?>, a<?>> k; // = new u3<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<V> implements ld<V> {
        @DexIgnore
        public /* final */ LiveData<V> a;
        @DexIgnore
        public /* final */ ld<? super V> b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public a(LiveData<V> liveData, ld<? super V> ldVar) {
            this.a = liveData;
            this.b = ldVar;
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }

        @DexIgnore
        public void b() {
            this.a.b(this);
        }

        @DexIgnore
        public void onChanged(V v) {
            if (this.c != this.a.b()) {
                this.c = this.a.b();
                this.b.onChanged(v);
            }
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData, ld<? super S> ldVar) {
        a aVar = new a(liveData, ldVar);
        a b = this.k.b(liveData, aVar);
        if (b != null && b.b != ldVar) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        } else if (b == null && c()) {
            aVar.a();
        }
    }

    @DexIgnore
    public void d() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            ((a) it.next().getValue()).a();
        }
    }

    @DexIgnore
    public void e() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            ((a) it.next().getValue()).b();
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData) {
        a remove = this.k.remove(liveData);
        if (remove != null) {
            remove.b();
        }
    }
}
