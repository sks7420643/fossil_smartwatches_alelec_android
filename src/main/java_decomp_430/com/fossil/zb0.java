package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zb0<V> extends v40<V, ac0> {
    @DexIgnore
    /* renamed from: d */
    public zb0<V> b(hg6<? super ac0, cd6> hg6) {
        super.b(hg6);
        return this;
    }

    @DexIgnore
    public final void e() {
        super.a(new t40(u40.INTERRUPTED, (km1) null, 2));
    }

    @DexIgnore
    public zb0<V> a(hg6<? super ac0, cd6> hg6) {
        super.a(hg6);
        return this;
    }

    @DexIgnore
    /* renamed from: e */
    public zb0<V> c(hg6<? super V, cd6> hg6) {
        super.c(hg6);
        return this;
    }
}
