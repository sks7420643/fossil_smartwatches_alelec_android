package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum r50 {
    DISTANCE((byte) 1),
    POSITION((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public r50(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
