package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1", f = "WatchAppsPresenter.kt", l = {144, 145}, m = "invokeSuspend")
public final class p75$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ WatchApp $watchApp;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter.f this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$1$grantedPermissionList$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super String[]>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        public a(xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return xx5.a.a();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mPermissionOfSelectedWatchAppTransformations$1$1$1$requiredPermissionList$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super List<? extends String>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchApp $it;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchApp watchApp, xe6 xe6) {
            super(2, xe6);
            this.$it = watchApp;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$it, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return dl4.c.b(this.$it.getWatchappId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p75$f$a(WatchAppsPresenter.f fVar, WatchApp watchApp, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$watchApp = watchApp;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        p75$f$a p75_f_a = new p75$f$a(this.this$0, this.$watchApp, xe6);
        p75_f_a.p$ = (il6) obj;
        return p75_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((p75$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0129, code lost:
        if (r0 <= 0) goto L_0x012b;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f3  */
    public final Object invokeSuspend(Object obj) {
        WatchApp watchApp;
        List<String> list;
        ArrayList<lc6> arrayList;
        il6 il6;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            WatchApp watchApp2 = this.$watchApp;
            if (watchApp2 != null) {
                dl6 a3 = this.this$0.a.b();
                b bVar = new b(watchApp2, (xe6) null);
                this.L$0 = il6;
                this.L$1 = watchApp2;
                this.label = 1;
                Object a4 = gk6.a(a3, bVar, this);
                if (a4 == a2) {
                    return a2;
                }
                Object obj2 = a4;
                watchApp = watchApp2;
                obj = obj2;
            }
            return cd6.a;
        } else if (i == 1) {
            watchApp = (WatchApp) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            list = (List) this.L$2;
            watchApp = (WatchApp) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            String[] strArr = (String[]) obj;
            arrayList = new ArrayList<>();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + watchApp.getWatchappId() + ' ' + "grantedPermission " + strArr.length + " requiredPermission " + list.size());
            if (!list.isEmpty()) {
                for (String str : list) {
                    arrayList.add(new lc6(str, hf6.a(nd6.a((T[]) strArr, str))));
                }
            }
            this.this$0.a.i.a(arrayList);
            if (!arrayList.isEmpty()) {
                int i2 = 0;
                if (!arrayList.isEmpty()) {
                    for (lc6 second : arrayList) {
                        if (hf6.a(!((Boolean) second.getSecond()).booleanValue()).booleanValue() && (i2 = i2 + 1) < 0) {
                            qd6.b();
                            throw null;
                        }
                    }
                }
            }
            rm6 unused = this.this$0.a.b(watchApp.getWatchappId());
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list2 = (List) obj;
        dl6 a5 = this.this$0.a.b();
        a aVar = new a((xe6) null);
        this.L$0 = il6;
        this.L$1 = watchApp;
        this.L$2 = list2;
        this.label = 2;
        Object a6 = gk6.a(a5, aVar, this);
        if (a6 == a2) {
            return a2;
        }
        list = list2;
        obj = a6;
        String[] strArr2 = (String[]) obj;
        arrayList = new ArrayList<>();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("WatchAppsPresenter", "checkPermissionOf watchAppId=" + watchApp.getWatchappId() + ' ' + "grantedPermission " + strArr2.length + " requiredPermission " + list.size());
        if (!list.isEmpty()) {
        }
        this.this$0.a.i.a(arrayList);
        if (!arrayList.isEmpty()) {
        }
        rm6 unused2 = this.this$0.a.b(watchApp.getWatchappId());
        return cd6.a;
    }
}
