package com.fossil;

import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1", f = "PairingPresenter.kt", l = {280, 286}, m = "invokeSuspend")
public final class qs5$d$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $devicePairingSerial;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qs5$d$b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qs5$d$b qs5_d_b, xe6 xe6) {
            super(2, xe6);
            this.this$0 = qs5_d_b;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v10, types: [com.portfolio.platform.usecase.SetNotificationUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            String str;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                String deviceNameBySerial = this.this$0.this$0.a.u.getDeviceNameBySerial(this.this$0.$devicePairingSerial);
                MisfitDeviceProfile a = DeviceHelper.o.e().a(this.this$0.$devicePairingSerial);
                if (a == null || (str = a.getFirmwareVersion()) == null) {
                    str = "";
                }
                AnalyticsHelper.f.c().a(DeviceHelper.o.b(this.this$0.$devicePairingSerial), deviceNameBySerial, str);
                return this.this$0.this$0.a.w.a(new SetNotificationUseCase.b(this.this$0.$devicePairingSerial), (CoroutineUseCase.e) null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super HashMap<String, String>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qs5$d$b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qs5$d$b qs5_d_b, xe6 xe6) {
            super(2, xe6);
            this.this$0 = qs5_d_b;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                qs5$d$b qs5_d_b = this.this$0;
                return qs5_d_b.this$0.a.d(qs5_d_b.$devicePairingSerial);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qs5$d$b(PairingPresenter.d dVar, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$devicePairingSerial = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qs5$d$b qs5_d_b = new qs5$d$b(this.this$0, this.$devicePairingSerial, xe6);
        qs5_d_b.p$ = (il6) obj;
        return qs5_d_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qs5$d$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    public final Object invokeSuspend(Object obj) {
        jl4 p;
        il6 il6;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            dl6 a3 = this.this$0.a.b();
            b bVar = new b(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a3, bVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            HashMap hashMap = (HashMap) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            p = this.this$0.a.p();
            if (p != null) {
                p.a("");
            }
            AnalyticsHelper.f.e("setup_device_session");
            this.this$0.a.x();
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HashMap hashMap2 = (HashMap) obj;
        if (hashMap2.containsKey("Style_Number") && hashMap2.containsKey("Device_Name")) {
            this.this$0.a.a("pair_success", (Map<String, String>) hashMap2);
            this.this$0.a.a((Map<String, String>) hashMap2);
        }
        dl6 b2 = this.this$0.a.c();
        a aVar = new a(this, (xe6) null);
        this.L$0 = il6;
        this.L$1 = hashMap2;
        this.label = 2;
        if (gk6.a(b2, aVar, this) == a2) {
            return a2;
        }
        p = this.this$0.a.p();
        if (p != null) {
        }
        AnalyticsHelper.f.e("setup_device_session");
        this.this$0.a.x();
        return cd6.a;
    }
}
