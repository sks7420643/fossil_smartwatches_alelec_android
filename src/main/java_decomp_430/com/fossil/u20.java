package com.fossil;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.ServerProtocol;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.a96;
import com.fossil.b30;
import com.fossil.j96;
import com.fossil.k30;
import com.fossil.s20;
import com.fossil.z30;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u20 {
    @DexIgnore
    public static /* final */ FilenameFilter r; // = new k("BeginSession");
    @DexIgnore
    public static /* final */ FilenameFilter s; // = new r();
    @DexIgnore
    public static /* final */ FileFilter t; // = new s();
    @DexIgnore
    public static /* final */ Comparator<File> u; // = new t();
    @DexIgnore
    public static /* final */ Comparator<File> v; // = new u();
    @DexIgnore
    public static /* final */ Pattern w; // = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    @DexIgnore
    public static /* final */ Map<String, String> x; // = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    @DexIgnore
    public static /* final */ String[] y; // = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    @DexIgnore
    public /* final */ v20 a;
    @DexIgnore
    public /* final */ t20 b;
    @DexIgnore
    public /* final */ va6 c;
    @DexIgnore
    public /* final */ j96 d;
    @DexIgnore
    public /* final */ t30 e;
    @DexIgnore
    public /* final */ ab6 f;
    @DexIgnore
    public /* final */ k20 g;
    @DexIgnore
    public /* final */ e0 h;
    @DexIgnore
    public /* final */ k30 i;
    @DexIgnore
    public /* final */ z30.c j; // = new g0(this, (k) null);
    @DexIgnore
    public /* final */ z30.b k; // = new h0(this, (k) null);
    @DexIgnore
    public /* final */ g30 l;
    @DexIgnore
    public /* final */ e40 m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ l20 o;
    @DexIgnore
    public /* final */ n10 p;
    @DexIgnore
    public b30 q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public a(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        public Void call() throws Exception {
            new m30(u20.this.f()).a(u20.this.d(), new h40(this.a, this.b, this.c));
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 implements b30.b {
        @DexIgnore
        public a0() {
        }

        @DexIgnore
        public yb6 a() {
            return vb6.d().a();
        }

        @DexIgnore
        public /* synthetic */ a0(k kVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Map a;

        @DexIgnore
        public b(Map map) {
            this.a = map;
        }

        @DexIgnore
        public Void call() throws Exception {
            new m30(u20.this.f()).a(u20.this.d(), (Map<String, String>) this.a);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b0 implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b0(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Void> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public Void call() throws Exception {
            u20.this.b();
            return null;
        }
    }

    @DexIgnore
    public interface c0 {
        @DexIgnore
        void a(FileOutputStream fileOutputStream) throws Exception;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ub6 a;

        @DexIgnore
        public d(ub6 ub6) {
            this.a = ub6;
        }

        @DexIgnore
        public Boolean call() throws Exception {
            if (u20.this.j()) {
                c86.g().d("CrashlyticsCore", "Skipping session finalization because a crash has already occurred.");
                return Boolean.FALSE;
            }
            c86.g().d("CrashlyticsCore", "Finalizing previously open sessions.");
            u20.this.a(this.a, true);
            c86.g().d("CrashlyticsCore", "Closed all previously open sessions");
            return Boolean.TRUE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d0 implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return p20.d.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            u20 u20 = u20.this;
            u20.a(u20.a((FilenameFilter) new d0()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e0 implements k30.b {
        @DexIgnore
        public /* final */ ab6 a;

        @DexIgnore
        public e0(ab6 ab6) {
            this.a = ab6;
        }

        @DexIgnore
        public File a() {
            File file = new File(this.a.a(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements FilenameFilter {
        @DexIgnore
        public /* final */ /* synthetic */ Set a;

        @DexIgnore
        public f(u20 u20, Set set) {
            this.a = set;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (str.length() < 35) {
                return false;
            }
            return this.a.contains(str.substring(0, 35));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f0 implements z30.d {
        @DexIgnore
        public /* final */ i86 a;
        @DexIgnore
        public /* final */ t30 b;
        @DexIgnore
        public /* final */ tb6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements s20.d {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void a(boolean z) {
                f0.this.b.a(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ s20 a;

            @DexIgnore
            public b(f0 f0Var, s20 s20) {
                this.a = s20;
            }

            @DexIgnore
            public void run() {
                this.a.c();
            }
        }

        @DexIgnore
        public f0(i86 i86, t30 t30, tb6 tb6) {
            this.a = i86;
            this.b = t30;
            this.c = tb6;
        }

        @DexIgnore
        public boolean a() {
            Activity a2 = this.a.f().a();
            if (a2 == null || a2.isFinishing()) {
                return true;
            }
            s20 a3 = s20.a(a2, this.c, new a());
            a2.runOnUiThread(new b(this, a3));
            c86.g().d("CrashlyticsCore", "Waiting for user opt-in.");
            a3.a();
            return a3.b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ y20 a;

        @DexIgnore
        public g(y20 y20) {
            this.a = y20;
        }

        @DexIgnore
        public Boolean call() throws Exception {
            File first;
            TreeSet<File> treeSet = this.a.a;
            String c = u20.this.i();
            if (!(c == null || treeSet.isEmpty() || (first = treeSet.first()) == null)) {
                u20 u20 = u20.this;
                u20.a(u20.a.d(), first, c);
            }
            u20.this.a((Set<File>) treeSet);
            return Boolean.TRUE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements z30.c {
        @DexIgnore
        public g0() {
        }

        @DexIgnore
        public File[] a() {
            return u20.this.l();
        }

        @DexIgnore
        public File[] b() {
            return u20.this.g().listFiles();
        }

        @DexIgnore
        public File[] c() {
            return u20.this.k();
        }

        @DexIgnore
        public /* synthetic */ g0(u20 u20, k kVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements z {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        public h(u20 u20, String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        public void a(q20 q20) throws Exception {
            b40.a(q20, this.a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements z30.b {
        @DexIgnore
        public h0() {
        }

        @DexIgnore
        public boolean a() {
            return u20.this.j();
        }

        @DexIgnore
        public /* synthetic */ h0(u20 u20, k kVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements c0 {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends HashMap<String, Object> {
            @DexIgnore
            public a() {
                put("session_id", i.this.a);
                put("generator", i.this.b);
                put("started_at_seconds", Long.valueOf(i.this.c));
            }
        }

        @DexIgnore
        public i(u20 u20, String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i0 implements Runnable {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ y30 b;
        @DexIgnore
        public /* final */ z30 c;

        @DexIgnore
        public i0(Context context, y30 y30, z30 z30) {
            this.a = context;
            this.b = y30;
            this.c = z30;
        }

        @DexIgnore
        public void run() {
            if (z86.b(this.a)) {
                c86.g().d("CrashlyticsCore", "Attempting to send crash report at time of crash...");
                this.c.a(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements z {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public j(String str, String str2, String str3, String str4, int i) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
        }

        @DexIgnore
        public void a(q20 q20) throws Exception {
            b40.a(q20, this.a, u20.this.g.a, this.b, this.c, this.d, this.e, u20.this.n);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j0 implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public j0(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (!str.equals(this.a + ".cls") && str.contains(this.a) && !str.endsWith(".cls_temp")) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class k extends b0 {
        @DexIgnore
        public k(String str) {
            super(str);
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements c0 {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends HashMap<String, Object> {
            @DexIgnore
            public a() {
                put("app_identifier", l.this.a);
                put("api_key", u20.this.g.a);
                put("version_code", l.this.b);
                put("version_name", l.this.c);
                put("install_uuid", l.this.d);
                put("delivery_mechanism", Integer.valueOf(l.this.e));
                put("unity_version", TextUtils.isEmpty(u20.this.n) ? "" : u20.this.n);
            }
        }

        @DexIgnore
        public l(String str, String str2, String str3, String str4, int i) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
        }

        @DexIgnore
        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements z {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;

        @DexIgnore
        public m(u20 u20, boolean z) {
            this.a = z;
        }

        @DexIgnore
        public void a(q20 q20) throws Exception {
            b40.a(q20, Build.VERSION.RELEASE, Build.VERSION.CODENAME, this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements c0 {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends HashMap<String, Object> {
            @DexIgnore
            public a() {
                put("version", Build.VERSION.RELEASE);
                put("build_version", Build.VERSION.CODENAME);
                put("is_rooted", Boolean.valueOf(n.this.a));
            }
        }

        @DexIgnore
        public n(u20 u20, boolean z) {
            this.a = z;
        }

        @DexIgnore
        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o implements z {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ Map f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore
        public o(u20 u20, int i, int i2, long j, long j2, boolean z, Map map, int i3) {
            this.a = i;
            this.b = i2;
            this.c = j;
            this.d = j2;
            this.e = z;
            this.f = map;
            this.g = i3;
        }

        @DexIgnore
        public void a(q20 q20) throws Exception {
            b40.a(q20, this.a, Build.MODEL, this.b, this.c, this.d, this.e, (Map<j96.a, String>) this.f, this.g, Build.MANUFACTURER, Build.PRODUCT);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class p implements c0 {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ Map f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends HashMap<String, Object> {
            @DexIgnore
            public a() {
                put("arch", Integer.valueOf(p.this.a));
                put("build_model", Build.MODEL);
                put("available_processors", Integer.valueOf(p.this.b));
                put("total_ram", Long.valueOf(p.this.c));
                put("disk_space", Long.valueOf(p.this.d));
                put("is_emulator", Boolean.valueOf(p.this.e));
                put("ids", p.this.f);
                put(ServerProtocol.DIALOG_PARAM_STATE, Integer.valueOf(p.this.g));
                put("build_manufacturer", Build.MANUFACTURER);
                put("build_product", Build.PRODUCT);
            }
        }

        @DexIgnore
        public p(u20 u20, int i, int i2, long j, long j2, boolean z, Map map, int i3) {
            this.a = i;
            this.b = i2;
            this.c = j;
            this.d = j2;
            this.e = z;
            this.f = map;
            this.g = i3;
        }

        @DexIgnore
        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class q implements z {
        @DexIgnore
        public /* final */ /* synthetic */ h40 a;

        @DexIgnore
        public q(u20 u20, h40 h40) {
            this.a = h40;
        }

        @DexIgnore
        public void a(q20 q20) throws Exception {
            h40 h40 = this.a;
            b40.a(q20, h40.a, h40.b, h40.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class r implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class s implements FileFilter {
        @DexIgnore
        public boolean accept(File file) {
            return file.isDirectory() && file.getName().length() == 35;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class t implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class u implements Comparator<File> {
        @DexIgnore
        /* renamed from: a */
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class v implements b30.a {
        @DexIgnore
        public v() {
        }

        @DexIgnore
        public void a(b30.b bVar, Thread thread, Throwable th, boolean z) {
            u20.this.a(bVar, thread, th, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class w implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Date a;
        @DexIgnore
        public /* final */ /* synthetic */ Thread b;
        @DexIgnore
        public /* final */ /* synthetic */ Throwable c;
        @DexIgnore
        public /* final */ /* synthetic */ b30.b d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        public w(Date date, Thread thread, Throwable th, b30.b bVar, boolean z) {
            this.a = date;
            this.b = thread;
            this.c = th;
            this.d = bVar;
            this.e = z;
        }

        @DexIgnore
        public Void call() throws Exception {
            rb6 rb6;
            ub6 ub6;
            u20.this.a.o();
            u20.this.a(this.a, this.b, this.c);
            yb6 a2 = this.d.a();
            if (a2 != null) {
                ub6 = a2.b;
                rb6 = a2.d;
            } else {
                ub6 = null;
                rb6 = null;
            }
            boolean z = false;
            if ((rb6 == null || rb6.d) || this.e) {
                u20.this.a(this.a.getTime());
            }
            u20.this.a(ub6);
            u20.this.b();
            if (ub6 != null) {
                u20.this.b(ub6.b);
            }
            if (c96.a(u20.this.a.d()).a() && !u20.this.c(a2)) {
                z = true;
            }
            if (z) {
                u20.this.b(a2);
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class x implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ long a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public x(long j, String str) {
            this.a = j;
            this.b = str;
        }

        @DexIgnore
        public Void call() throws Exception {
            if (u20.this.j()) {
                return null;
            }
            u20.this.i.a(this.a, this.b);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class y implements FilenameFilter {
        @DexIgnore
        public y() {
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return !u20.s.accept(file, str) && u20.w.matcher(str).matches();
        }

        @DexIgnore
        public /* synthetic */ y(k kVar) {
            this();
        }
    }

    @DexIgnore
    public interface z {
        @DexIgnore
        void a(q20 q20) throws Exception;
    }

    @DexIgnore
    public u20(v20 v20, t20 t20, va6 va6, j96 j96, t30 t30, ab6 ab6, k20 k20, g40 g40, l20 l20, n10 n10) {
        new AtomicInteger(0);
        this.a = v20;
        this.b = t20;
        this.c = va6;
        this.d = j96;
        this.e = t30;
        this.f = ab6;
        this.g = k20;
        this.n = g40.a();
        this.o = l20;
        this.p = n10;
        Context d2 = v20.d();
        this.h = new e0(ab6);
        this.i = new k30(d2, this.h);
        this.l = new g30(d2);
        this.m = new n30(1024, new x30(10));
    }

    @DexIgnore
    public final File[] b(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    @DexIgnore
    public File h() {
        return new File(f(), "nonfatal-sessions");
    }

    @DexIgnore
    public final String i() {
        File[] n2 = n();
        if (n2.length > 1) {
            return c(n2[1]);
        }
        return null;
    }

    @DexIgnore
    public boolean j() {
        b30 b30 = this.q;
        return b30 != null && b30.a();
    }

    @DexIgnore
    public File[] k() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, a(e(), s));
        Collections.addAll(linkedList, a(h(), s));
        Collections.addAll(linkedList, a(f(), s));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    @DexIgnore
    public File[] l() {
        return a(t);
    }

    @DexIgnore
    public File[] m() {
        return a(r);
    }

    @DexIgnore
    public final File[] n() {
        File[] m2 = m();
        Arrays.sort(m2, u);
        return m2;
    }

    @DexIgnore
    public void o() {
        this.b.a(new c());
    }

    @DexIgnore
    public void p() {
        this.l.b();
    }

    @DexIgnore
    public final void q() {
        File g2 = g();
        if (g2.exists()) {
            File[] a2 = a(g2, (FilenameFilter) new d0());
            Arrays.sort(a2, Collections.reverseOrder());
            HashSet hashSet = new HashSet();
            for (int i2 = 0; i2 < a2.length && hashSet.size() < 4; i2++) {
                hashSet.add(c(a2[i2]));
            }
            a(a(g2), (Set<String>) hashSet);
        }
    }

    @DexIgnore
    public static String c(File file) {
        return file.getName().substring(0, 35);
    }

    @DexIgnore
    public final String d() {
        File[] n2 = n();
        if (n2.length > 0) {
            return c(n2[0]);
        }
        return null;
    }

    @DexIgnore
    public final void e(String str) throws Exception {
        String str2 = str;
        Context d2 = this.a.d();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int a2 = z86.a();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long b2 = z86.b();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean l2 = z86.l(d2);
        int i2 = a2;
        int i3 = availableProcessors;
        long j2 = b2;
        long j3 = blockCount;
        boolean z2 = l2;
        Map f2 = this.d.f();
        long j4 = b2;
        o oVar = r0;
        int f3 = z86.f(d2);
        o oVar2 = new o(this, i2, i3, j2, j3, z2, f2, f3);
        a(str2, "SessionDevice", (z) oVar);
        a(str2, "SessionDevice.json", (c0) new p(this, i2, i3, j4, j3, z2, f2, f3));
    }

    @DexIgnore
    public final void f(String str) throws Exception {
        boolean m2 = z86.m(this.a.d());
        a(str, "SessionOS", (z) new m(this, m2));
        a(str, "SessionOS.json", (c0) new n(this, m2));
    }

    @DexIgnore
    public final void g(String str) throws Exception {
        a(str, "SessionUser", (z) new q(this, b(str)));
    }

    @DexIgnore
    public boolean b(ub6 ub6) {
        return ((Boolean) this.b.b(new d(ub6))).booleanValue();
    }

    @DexIgnore
    public final File[] c(String str) {
        return a((FilenameFilter) new j0(str));
    }

    @DexIgnore
    public final void b() throws Exception {
        Date date = new Date();
        String o20 = new o20(this.d).toString();
        l86 g2 = c86.g();
        g2.d("CrashlyticsCore", "Opening a new session with ID " + o20);
        a(o20, date);
        d(o20);
        f(o20);
        e(o20);
        this.i.b(o20);
    }

    @DexIgnore
    public final boolean c(yb6 yb6) {
        if (yb6 != null && yb6.d.a && !this.e.a()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public File g() {
        return new File(f(), "invalidClsFiles");
    }

    @DexIgnore
    public final void d(String str) throws Exception {
        String d2 = this.d.d();
        k20 k20 = this.g;
        String str2 = k20.e;
        String str3 = k20.f;
        String str4 = d2;
        String str5 = str2;
        String str6 = str3;
        String e2 = this.d.e();
        int id = d96.determineFrom(this.g.c).getId();
        a(str, "SessionApp", (z) new j(str4, str5, str6, e2, id));
        a(str, "SessionApp.json", (c0) new l(str4, str5, str6, e2, id));
    }

    @DexIgnore
    public File f() {
        return this.f.a();
    }

    @DexIgnore
    public static void c(String str, String str2) {
        y00 a2 = c86.a(y00.class);
        if (a2 == null) {
            c86.g().d("CrashlyticsCore", "Answers is not available");
        } else {
            a2.a(new a96.a(str, str2));
        }
    }

    @DexIgnore
    public void a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, boolean z2) {
        o();
        this.q = new b30(new v(), new a0((k) null), z2, uncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(this.q);
    }

    @DexIgnore
    public final boolean c() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public final void b(File file) {
        if (file.isDirectory()) {
            for (File b2 : file.listFiles()) {
                b(b2);
            }
        }
        file.delete();
    }

    @DexIgnore
    public synchronized void a(b30.b bVar, Thread thread, Throwable th, boolean z2) {
        l86 g2 = c86.g();
        g2.d("CrashlyticsCore", "Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        this.l.a();
        this.b.b(new w(new Date(), thread, th, bVar, z2));
    }

    @DexIgnore
    public File e() {
        return new File(f(), "fatal-sessions");
    }

    @DexIgnore
    public void b(int i2) {
        int a2 = i2 - i40.a(e(), i2, v);
        i40.a(f(), s, a2 - i40.a(h(), a2, v), v);
    }

    @DexIgnore
    public final void b(byte[] bArr, File file) throws IOException {
        if (bArr != null && bArr.length > 0) {
            a(bArr, file);
        }
    }

    @DexIgnore
    public void a(float f2, yb6 yb6) {
        if (yb6 == null) {
            c86.g().w("CrashlyticsCore", "Could not send reports. Settings are not available.");
            return;
        }
        ib6 ib6 = yb6.a;
        new z30(this.g.a, a(ib6.c, ib6.d), this.j, this.k).a(f2, c(yb6) ? new f0(this.a, this.e, yb6.c) : new z30.a());
    }

    @DexIgnore
    public final byte[] b(String str, String str2) {
        File f2 = f();
        return q30.d(new File(f2, str + str2));
    }

    @DexIgnore
    public final h40 b(String str) {
        if (j()) {
            return new h40(this.a.x(), this.a.y(), this.a.v());
        }
        return new m30(f()).c(str);
    }

    @DexIgnore
    public void a(long j2, String str) {
        this.b.a(new x(j2, str));
    }

    @DexIgnore
    public final void b(yb6 yb6) {
        if (yb6 == null) {
            c86.g().w("CrashlyticsCore", "Cannot send reports. Settings are unavailable.");
            return;
        }
        Context d2 = this.a.d();
        ib6 ib6 = yb6.a;
        z30 z30 = new z30(this.g.a, a(ib6.c, ib6.d), this.j, this.k);
        for (File c40 : k()) {
            this.b.a((Runnable) new i0(d2, new c40(c40, x), z30));
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        this.b.a(new a(str, str2, str3));
    }

    @DexIgnore
    public void a(Map<String, String> map) {
        this.b.a(new b(map));
    }

    @DexIgnore
    public void a(ub6 ub6) throws Exception {
        a(ub6, false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(ub6 ub6, boolean r5) throws Exception {
        a(r5 + 8);
        File[] n2 = n();
        if (n2.length <= r5) {
            c86.g().d("CrashlyticsCore", "No open sessions to be closed.");
            return;
        }
        g(c(n2[r5]));
        if (ub6 == null) {
            c86.g().d("CrashlyticsCore", "Unable to close session. Settings are not loaded.");
        } else {
            a(n2, (int) r5, ub6.a);
        }
    }

    @DexIgnore
    public final void a(File[] fileArr, int i2, int i3) {
        c86.g().d("CrashlyticsCore", "Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String c2 = c(file);
            l86 g2 = c86.g();
            g2.d("CrashlyticsCore", "Closing session: " + c2);
            a(file, c2, i3);
            i2++;
        }
    }

    @DexIgnore
    public final void a(p20 p20) {
        if (p20 != null) {
            try {
                p20.k();
            } catch (IOException e2) {
                c86.g().e("CrashlyticsCore", "Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    @DexIgnore
    public final void a(Set<File> set) {
        for (File b2 : set) {
            b(b2);
        }
    }

    @DexIgnore
    public final void a(String str) {
        for (File delete : c(str)) {
            delete.delete();
        }
    }

    @DexIgnore
    public final File[] a(FileFilter fileFilter) {
        return b(f().listFiles(fileFilter));
    }

    @DexIgnore
    public final File[] a(FilenameFilter filenameFilter) {
        return a(f(), filenameFilter);
    }

    @DexIgnore
    public final File[] a(File file, FilenameFilter filenameFilter) {
        return b(file.listFiles(filenameFilter));
    }

    @DexIgnore
    public final File[] a(File file) {
        return b(file.listFiles());
    }

    @DexIgnore
    public final void a(String str, int i2) {
        File f2 = f();
        i40.a(f2, new b0(str + "SessionEvent"), i2, v);
    }

    @DexIgnore
    public final void a(int i2) {
        HashSet hashSet = new HashSet();
        File[] n2 = n();
        int min = Math.min(i2, n2.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(c(n2[i3]));
        }
        this.i.a((Set<String>) hashSet);
        a(a((FilenameFilter) new y((k) null)), (Set<String>) hashSet);
    }

    @DexIgnore
    public final void a(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = w.matcher(name);
            if (!matcher.matches()) {
                c86.g().d("CrashlyticsCore", "Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                c86.g().d("CrashlyticsCore", "Trimming session file: " + name);
                file.delete();
            }
        }
    }

    @DexIgnore
    public final File[] a(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        c86.g().d("CrashlyticsCore", String.format(Locale.US, "Trimming down to %d logged exceptions.", new Object[]{Integer.valueOf(i2)}));
        a(str, i2);
        return a((FilenameFilter) new b0(str + "SessionEvent"));
    }

    @DexIgnore
    public void a() {
        this.b.a((Runnable) new e());
    }

    @DexIgnore
    public void a(File[] fileArr) {
        HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            c86.g().d("CrashlyticsCore", "Found invalid session part file: " + file);
            hashSet.add(c(file));
        }
        if (!hashSet.isEmpty()) {
            File g2 = g();
            if (!g2.exists()) {
                g2.mkdir();
            }
            for (File file2 : a((FilenameFilter) new f(this, hashSet))) {
                c86.g().d("CrashlyticsCore", "Moving session file: " + file2);
                if (!file2.renameTo(new File(g2, file2.getName()))) {
                    c86.g().d("CrashlyticsCore", "Could not move session file. Deleting " + file2);
                    file2.delete();
                }
            }
            q();
        }
    }

    @DexIgnore
    public final void a(Context context, File file, String str) throws IOException {
        byte[] b2 = q30.b(file);
        byte[] a2 = q30.a(file);
        byte[] b3 = q30.b(file, context);
        if (b2 == null || b2.length == 0) {
            l86 g2 = c86.g();
            g2.w("CrashlyticsCore", "No minidump data found in directory " + file);
            return;
        }
        c(str, "<native-crash: minidump>");
        byte[] b4 = b(str, "BeginSession.json");
        byte[] b5 = b(str, "SessionApp.json");
        byte[] b6 = b(str, "SessionDevice.json");
        byte[] b7 = b(str, "SessionOS.json");
        byte[] d2 = q30.d(new m30(f()).b(str));
        k30 k30 = new k30(this.a.d(), this.h, str);
        byte[] c2 = k30.c();
        k30.a();
        byte[] d3 = q30.d(new m30(f()).a(str));
        File file2 = new File(this.f.a(), str);
        if (!file2.mkdir()) {
            c86.g().d("CrashlyticsCore", "Couldn't create native sessions directory");
            return;
        }
        b(b2, new File(file2, "minidump"));
        b(a2, new File(file2, "metadata"));
        b(b3, new File(file2, "binaryImages"));
        b(b4, new File(file2, "session"));
        b(b5, new File(file2, "app"));
        b(b6, new File(file2, DeviceRequestsHelper.DEVICE_INFO_DEVICE));
        b(b7, new File(file2, "os"));
        b(d2, new File(file2, "user"));
        b(c2, new File(file2, "logs"));
        b(d3, new File(file2, "keys"));
    }

    @DexIgnore
    public boolean a(y20 y20) {
        if (y20 == null) {
            return true;
        }
        return ((Boolean) this.b.b(new g(y20))).booleanValue();
    }

    @DexIgnore
    public final void a(byte[] bArr, File file) throws IOException {
        GZIPOutputStream gZIPOutputStream = null;
        try {
            GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(new FileOutputStream(file));
            try {
                gZIPOutputStream2.write(bArr);
                gZIPOutputStream2.finish();
                z86.a(gZIPOutputStream2);
            } catch (Throwable th) {
                th = th;
                gZIPOutputStream = gZIPOutputStream2;
                z86.a(gZIPOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            z86.a(gZIPOutputStream);
            throw th;
        }
    }

    @DexIgnore
    public final void a(Date date, Thread thread, Throwable th) {
        p20 p20;
        q20 q20 = null;
        try {
            String d2 = d();
            if (d2 == null) {
                c86.g().e("CrashlyticsCore", "Tried to write a fatal exception while no session was open.", (Throwable) null);
                z86.a((Flushable) null, "Failed to flush to session begin file.");
                z86.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            c(d2, th.getClass().getName());
            p20 = new p20(f(), d2 + "SessionCrash");
            try {
                q20 = q20.a((OutputStream) p20);
                a(q20, date, thread, th, CrashDumperPlugin.NAME, true);
            } catch (Exception e2) {
                e = e2;
            }
            z86.a(q20, "Failed to flush to session begin file.");
            z86.a(p20, "Failed to close fatal exception file output stream.");
        } catch (Exception e3) {
            e = e3;
            p20 = null;
            try {
                c86.g().e("CrashlyticsCore", "An error occurred in the fatal exception logger", e);
                z86.a(q20, "Failed to flush to session begin file.");
                z86.a(p20, "Failed to close fatal exception file output stream.");
            } catch (Throwable th2) {
                th = th2;
                z86.a(q20, "Failed to flush to session begin file.");
                z86.a(p20, "Failed to close fatal exception file output stream.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            p20 = null;
            z86.a(q20, "Failed to flush to session begin file.");
            z86.a(p20, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, z zVar) throws Exception {
        p20 p20;
        q20 q20 = null;
        try {
            p20 = new p20(f(), str + str2);
            try {
                q20 = q20.a((OutputStream) p20);
                zVar.a(q20);
                z86.a(q20, "Failed to flush to session " + str2 + " file.");
                z86.a(p20, "Failed to close session " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                z86.a(q20, "Failed to flush to session " + str2 + " file.");
                z86.a(p20, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            p20 = null;
            z86.a(q20, "Failed to flush to session " + str2 + " file.");
            z86.a(p20, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, c0 c0Var) throws Exception {
        FileOutputStream fileOutputStream = null;
        try {
            File f2 = f();
            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(f2, str + str2));
            try {
                c0Var.a(fileOutputStream2);
                z86.a(fileOutputStream2, "Failed to close " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                fileOutputStream = fileOutputStream2;
                z86.a(fileOutputStream, "Failed to close " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            z86.a(fileOutputStream, "Failed to close " + str2 + " file.");
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str, Date date) throws Exception {
        String str2 = str;
        String format = String.format(Locale.US, "Crashlytics Android SDK/%s", new Object[]{this.a.j()});
        long time = date.getTime() / 1000;
        a(str, "BeginSession", (z) new h(this, str2, format, time));
        a(str, "BeginSession.json", (c0) new i(this, str2, format, time));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: java.util.TreeMap} */
    /* JADX WARNING: type inference failed for: r6v2, types: [boolean] */
    /* JADX WARNING: type inference failed for: r6v4 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(q20 q20, Date date, Thread thread, Throwable th, String str, boolean z2) throws Exception {
        Thread[] threadArr;
        Object r6;
        Map map;
        Map map2;
        f40 f40 = new f40(th, this.m);
        Context d2 = this.a.d();
        long time = date.getTime() / 1000;
        Float e2 = z86.e(d2);
        int a2 = z86.a(d2, this.l.c());
        boolean g2 = z86.g(d2);
        int i2 = d2.getResources().getConfiguration().orientation;
        long b2 = z86.b() - z86.a(d2);
        long a3 = z86.a(Environment.getDataDirectory().getPath());
        ActivityManager.RunningAppProcessInfo a4 = z86.a(d2.getPackageName(), d2);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = f40.c;
        String str2 = this.g.b;
        String d3 = this.d.d();
        int i3 = 0;
        if (z2) {
            Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            Thread[] threadArr2 = new Thread[allStackTraces.size()];
            for (Map.Entry next : allStackTraces.entrySet()) {
                threadArr2[i3] = (Thread) next.getKey();
                linkedList.add(this.m.a((StackTraceElement[]) next.getValue()));
                i3++;
            }
            r6 = 1;
            threadArr = threadArr2;
        } else {
            r6 = 1;
            threadArr = new Thread[0];
        }
        if (!z86.a(d2, "com.crashlytics.CollectCustomKeys", r6)) {
            map2 = new TreeMap();
        } else {
            map2 = this.a.t();
            if (map2 != null && map2.size() > r6) {
                map = new TreeMap(map2);
                b40.a(q20, time, str, f40, thread, stackTraceElementArr, threadArr, (List<StackTraceElement[]>) linkedList, (Map<String, String>) map, this.i, a4, i2, d3, str2, e2, a2, g2, b2, a3);
            }
        }
        map = map2;
        b40.a(q20, time, str, f40, thread, stackTraceElementArr, threadArr, (List<StackTraceElement[]>) linkedList, (Map<String, String>) map, this.i, a4, i2, d3, str2, e2, a2, g2, b2, a3);
    }

    @DexIgnore
    public final void a(File file, String str, int i2) {
        l86 g2 = c86.g();
        g2.d("CrashlyticsCore", "Collecting session parts for ID " + str);
        File[] a2 = a((FilenameFilter) new b0(str + "SessionCrash"));
        boolean z2 = a2 != null && a2.length > 0;
        c86.g().d("CrashlyticsCore", String.format(Locale.US, "Session %s has fatal exception: %s", new Object[]{str, Boolean.valueOf(z2)}));
        File[] a3 = a((FilenameFilter) new b0(str + "SessionEvent"));
        boolean z3 = a3 != null && a3.length > 0;
        c86.g().d("CrashlyticsCore", String.format(Locale.US, "Session %s has non-fatal exceptions: %s", new Object[]{str, Boolean.valueOf(z3)}));
        if (z2 || z3) {
            a(file, str, a(str, a3, i2), z2 ? a2[0] : null);
        } else {
            l86 g3 = c86.g();
            g3.d("CrashlyticsCore", "No events present for session ID " + str);
        }
        l86 g4 = c86.g();
        g4.d("CrashlyticsCore", "Removing session part files for ID " + str);
        a(str);
    }

    @DexIgnore
    public final void a(File file, String str, File[] fileArr, File file2) {
        p20 p20;
        String str2 = str;
        File file3 = file2;
        boolean z2 = file3 != null;
        File e2 = z2 ? e() : h();
        if (!e2.exists()) {
            e2.mkdirs();
        }
        try {
            p20 = new p20(e2, str2);
            try {
                q20 a2 = q20.a((OutputStream) p20);
                c86.g().d("CrashlyticsCore", "Collecting SessionStart data for session ID " + str2);
                a(a2, file);
                a2.a(4, new Date().getTime() / 1000);
                a2.a(5, z2);
                a2.d(11, 1);
                a2.a(12, 3);
                a(a2, str2);
                a(a2, fileArr, str2);
                if (z2) {
                    a(a2, file3);
                }
                z86.a(a2, "Error flushing session file stream");
                z86.a(p20, "Failed to close CLS file");
            } catch (Exception e3) {
                e = e3;
                try {
                    c86.g().e("CrashlyticsCore", "Failed to write session file for session ID: " + str2, e);
                    z86.a((Flushable) null, "Error flushing session file stream");
                    a(p20);
                } catch (Throwable th) {
                    th = th;
                    z86.a((Flushable) null, "Error flushing session file stream");
                    z86.a(p20, "Failed to close CLS file");
                    throw th;
                }
            }
        } catch (Exception e4) {
            e = e4;
            p20 = null;
            c86.g().e("CrashlyticsCore", "Failed to write session file for session ID: " + str2, e);
            z86.a((Flushable) null, "Error flushing session file stream");
            a(p20);
        } catch (Throwable th2) {
            th = th2;
            p20 = null;
            z86.a((Flushable) null, "Error flushing session file stream");
            z86.a(p20, "Failed to close CLS file");
            throw th;
        }
    }

    @DexIgnore
    public static void a(q20 q20, File[] fileArr, String str) {
        Arrays.sort(fileArr, z86.d);
        for (File file : fileArr) {
            try {
                c86.g().d("CrashlyticsCore", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[]{str, file.getName()}));
                a(q20, file);
            } catch (Exception e2) {
                c86.g().e("CrashlyticsCore", "Error writting non-fatal to session.", e2);
            }
        }
    }

    @DexIgnore
    public final void a(q20 q20, String str) throws IOException {
        for (String str2 : y) {
            File[] a2 = a((FilenameFilter) new b0(str + str2 + ".cls"));
            if (a2.length == 0) {
                c86.g().e("CrashlyticsCore", "Can't find " + str2 + " data for session ID " + str, (Throwable) null);
            } else {
                c86.g().d("CrashlyticsCore", "Collecting " + str2 + " data for session ID " + str);
                a(q20, a2[0]);
            }
        }
    }

    @DexIgnore
    public static void a(q20 q20, File file) throws IOException {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            c86.g().e("CrashlyticsCore", "Tried to include a file that doesn't exist: " + file.getName(), (Throwable) null);
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                a((InputStream) fileInputStream, q20, (int) file.length());
                z86.a(fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                z86.a(fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            z86.a(fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    @DexIgnore
    public static void a(InputStream inputStream, q20 q20, int i2) throws IOException {
        int read;
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < bArr.length && (read = inputStream.read(bArr, i3, bArr.length - i3)) >= 0) {
            i3 += read;
        }
        q20.a(bArr);
    }

    @DexIgnore
    public void a(yb6 yb6) {
        if (yb6.d.d) {
            boolean register = this.o.register();
            l86 g2 = c86.g();
            g2.d("CrashlyticsCore", "Registered Firebase Analytics event listener for breadcrumbs: " + register);
        }
    }

    @DexIgnore
    public final d30 a(String str, String str2) {
        String b2 = z86.b(this.a.d(), "com.crashlytics.ApiEndpoint");
        return new r20(new f30(this.a, b2, str, this.c), new p30(this.a, b2, str2, this.c));
    }

    @DexIgnore
    public final void a(long j2) {
        if (c()) {
            c86.g().d("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        } else if (this.p != null) {
            c86.g().d("CrashlyticsCore", "Logging Crashlytics event to Firebase");
            Bundle bundle = new Bundle();
            bundle.putInt("_r", 1);
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", j2);
            this.p.a("clx", "_ae", bundle);
        } else {
            c86.g().d("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
        }
    }
}
