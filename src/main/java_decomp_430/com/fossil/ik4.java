package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik4 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((qg6) null);
    @DexIgnore
    public /* final */ an4 a;
    @DexIgnore
    public /* final */ ActivitySummaryDao b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(long j) {
            if (j < ((long) 70)) {
                return 0;
            }
            return j < ((long) 140) ? 1 : 2;
        }

        @DexIgnore
        public final int a(ActivitySummary activitySummary, sh4 sh4) {
            wg6.b(sh4, "goalType");
            int i = hk4.a[sh4.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return 5000;
                    }
                    if (activitySummary != null) {
                        return activitySummary.getCaloriesGoal();
                    }
                    return 140;
                } else if (activitySummary != null) {
                    return activitySummary.getActiveTimeGoal();
                } else {
                    return 30;
                }
            } else if (activitySummary != null) {
                return activitySummary.getStepGoal();
            } else {
                return 5000;
            }
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int a(MFSleepDay mFSleepDay) {
            if (mFSleepDay != null) {
                return mFSleepDay.getGoalMinutes();
            }
            return 480;
        }

        @DexIgnore
        public final int a(GoalTrackingSummary goalTrackingSummary) {
            if (goalTrackingSummary != null) {
                return goalTrackingSummary.getGoalTarget();
            }
            return 8;
        }

        @DexIgnore
        public final String a(String str, MFSleepSession mFSleepSession) {
            wg6.b(str, ButtonService.USER_ID);
            wg6.b(mFSleepSession, "sleepSession");
            String value = ph4.values()[mFSleepSession.getSource()].getValue();
            wg6.a((Object) value, "FitnessSourceType.values\u2026leepSession.source].value");
            if (value != null) {
                String lowerCase = value.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return str + ":" + lowerCase + ":" + mFSleepSession.getRealEndTime();
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
    }

    /*
    static {
        String simpleName = ik4.class.getSimpleName();
        wg6.a((Object) simpleName, "FitnessHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public ik4(an4 an4, ActivitySummaryDao activitySummaryDao) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(activitySummaryDao, "mActivitySummaryDao");
        this.a = an4;
        this.b = activitySummaryDao;
    }

    @DexIgnore
    public final List<ActivitySample> a(List<ActivitySample> list, String str) {
        List<ActivitySample> list2 = list;
        wg6.b(list2, "activitySamples");
        wg6.b(str, ButtonService.USER_ID);
        long a2 = a(new Date());
        long j = 0;
        long j2 = 0;
        for (ActivitySample next : list) {
            DateTime component4 = next.component4();
            j += (long) next.component5();
            j2 = component4.getMillis();
        }
        FLogger.INSTANCE.getLocal().d(c, "addRealTimeStepToActivitySample - steps=" + j + ", realTimeSteps=" + a2);
        if (j < a2) {
            try {
                ActivityIntensities activityIntensities = new ActivityIntensities();
                double d2 = (double) (a2 - j);
                activityIntensities.setIntensity(d2);
                long j3 = j2 + 1;
                Date date = new Date(j3);
                DateTime dateTime = new DateTime(j3);
                DateTime dateTime2 = new DateTime(j2 + ((long) 100));
                int a3 = bk4.a();
                String value = ph4.Device.getValue();
                wg6.a((Object) value, "FitnessSourceType.Device.value");
                long j4 = j3;
                list2.add(new ActivitySample(str, date, dateTime, dateTime2, d2, 0.0d, 0.0d, 0, activityIntensities, a3, value, j4, j4, j4));
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(c, "addRealTimeStepToActivitySample - e=" + e);
                e.printStackTrace();
            }
        }
        return list2;
    }

    @DexIgnore
    public final float a(Date date, boolean z) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        float f = 0.0f;
        if (zk4.a(date).before(PortfolioApp.get.instance().k())) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = c;
                local.d(str, "Inside " + c + ".getCurrentSteps, this date -" + date + "- before user signing up date, return null.");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0.0f;
        }
        ActivitySummary activitySummary = this.b.getActivitySummary(date);
        if (activitySummary != null) {
            f = (float) activitySummary.getSteps();
        }
        Boolean t = bk4.t(date);
        wg6.a((Object) t, "DateHelper.isToday(date)");
        if (!t.booleanValue() || !z) {
            return f;
        }
        long a2 = a(date);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local2.d(str2, "getCurrentSteps - steps=" + f + ", realTimeSteps=" + a2);
        return Math.max((float) a2, f);
    }

    @DexIgnore
    public final void a(Date date, long j) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "saveRealTimeStep - date=" + date + ", steps=" + j);
        if (j >= 0) {
            String w = this.a.w();
            if (!TextUtils.isEmpty(w)) {
                String[] a2 = bv6.a(w, "_");
                if (a2 != null && a2.length == 2) {
                    try {
                        long parseLong = Long.parseLong(a2[0]);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = c;
                        local2.d(str2, "saveRealTimeStep - lastSampleRawTimeStamp=" + date + ", savedRealTimeStepTimeStamp=" + new Date(parseLong));
                        if (bk4.d(new Date(parseLong), date)) {
                            an4 an4 = this.a;
                            an4.v(String.valueOf(date.getTime()) + "_" + j);
                            return;
                        }
                        FLogger.INSTANCE.getLocal().d(c, "saveRealTimeStep - Different date, clear realTimeStepStamp");
                        this.a.v("");
                    } catch (Exception e) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = c;
                        local3.e(str3, "saveRealTimeStep - e=" + e);
                        e.printStackTrace();
                    }
                }
            } else {
                an4 an42 = this.a;
                an42.v(String.valueOf(date.getTime()) + "_" + j);
            }
        }
    }

    @DexIgnore
    public final long a(Date date) {
        String[] a2;
        wg6.b(date, HardwareLog.COLUMN_DATE);
        String w = this.a.w();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "getRealTimeStep - data=" + w + ", date=" + bk4.i(date));
        if (!TextUtils.isEmpty(w) && (a2 = bv6.a(w, "_")) != null && a2.length == 2) {
            try {
                if (bk4.d(date, new Date(Long.parseLong(a2[0])))) {
                    return Long.parseLong(a2[1]);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = c;
                local2.e(str2, "getRealTimeStep - e=" + e);
                e.printStackTrace();
            }
        }
        return 0;
    }
}
