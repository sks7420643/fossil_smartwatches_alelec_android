package com.fossil;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr3 extends kb2 {
    @DexIgnore
    public /* final */ /* synthetic */ jr3 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mr3(jr3 jr3, Looper looper) {
        super(looper);
        this.a = jr3;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        this.a.a(message);
    }
}
