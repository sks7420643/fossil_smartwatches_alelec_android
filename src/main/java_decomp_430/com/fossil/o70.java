package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o70 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<o70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                byte readByte = parcel.readByte();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        wg6.a(readString3, "parcel.readString()!!");
                        String readString4 = parcel.readString();
                        if (readString4 != null) {
                            wg6.a(readString4, "parcel.readString()!!");
                            return new o70(readString, readByte, readString2, readString3, readString4);
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new o70[i];
        }
    }

    @DexIgnore
    public o70(String str, byte b2, String str2, String str3, String str4) {
        this.a = str;
        this.b = b2;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.APP_NAME, (Object) this.a), bm0.VOLUME, (Object) Byte.valueOf(this.b)), bm0.g, (Object) this.c), bm0.ARTIST, (Object) this.d), bm0.ALBUM, (Object) this.e);
    }

    @DexIgnore
    public final byte[] b() {
        ByteBuffer allocate = ByteBuffer.allocate(6);
        wg6.a(allocate, "ByteBuffer.allocate(HEADER_LENGTH.toInt())");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        String str = this.a;
        Charset f = mi0.A.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            byte min = (byte) Math.min(bytes.length + 1, 50);
            String str2 = this.c;
            Charset f2 = mi0.A.f();
            if (str2 != null) {
                byte[] bytes2 = str2.getBytes(f2);
                wg6.a(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte min2 = (byte) Math.min(bytes2.length + 1, 50);
                String str3 = this.d;
                Charset f3 = mi0.A.f();
                if (str3 != null) {
                    byte[] bytes3 = str3.getBytes(f3);
                    wg6.a(bytes3, "(this as java.lang.String).getBytes(charset)");
                    byte min3 = (byte) Math.min(bytes3.length + 1, 50);
                    String str4 = this.e;
                    Charset f4 = mi0.A.f();
                    if (str4 != null) {
                        byte[] bytes4 = str4.getBytes(f4);
                        wg6.a(bytes4, "(this as java.lang.String).getBytes(charset)");
                        byte min4 = (byte) Math.min(bytes4.length + 1, 50);
                        short s = (short) (min + 7 + min2 + min3 + min4);
                        allocate.putShort(s);
                        allocate.put(min);
                        allocate.put(min2);
                        allocate.put(min3);
                        allocate.put(min4);
                        ByteBuffer allocate2 = ByteBuffer.allocate(s);
                        wg6.a(allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                        allocate2.order(ByteOrder.LITTLE_ENDIAN);
                        allocate2.put(allocate.array());
                        ByteBuffer allocate3 = ByteBuffer.allocate(s - 6);
                        wg6.a(allocate3, "ByteBuffer.allocate(totalLen - HEADER_LENGTH)");
                        allocate3.order(ByteOrder.LITTLE_ENDIAN);
                        allocate3.put(this.b);
                        String str5 = this.a;
                        Charset f5 = mi0.A.f();
                        if (str5 != null) {
                            byte[] bytes5 = str5.getBytes(f5);
                            wg6.a(bytes5, "(this as java.lang.String).getBytes(charset)");
                            allocate3.put(Arrays.copyOfRange(bytes5, 0, min - 1)).put((byte) 0);
                            String str6 = this.c;
                            Charset f6 = mi0.A.f();
                            if (str6 != null) {
                                byte[] bytes6 = str6.getBytes(f6);
                                wg6.a(bytes6, "(this as java.lang.String).getBytes(charset)");
                                allocate3.put(Arrays.copyOfRange(bytes6, 0, min2 - 1)).put((byte) 0);
                                String str7 = this.d;
                                Charset f7 = mi0.A.f();
                                if (str7 != null) {
                                    byte[] bytes7 = str7.getBytes(f7);
                                    wg6.a(bytes7, "(this as java.lang.String).getBytes(charset)");
                                    allocate3.put(Arrays.copyOfRange(bytes7, 0, min3 - 1)).put((byte) 0);
                                    String str8 = this.e;
                                    Charset f8 = mi0.A.f();
                                    if (str8 != null) {
                                        byte[] bytes8 = str8.getBytes(f8);
                                        wg6.a(bytes8, "(this as java.lang.String).getBytes(charset)");
                                        allocate3.put(Arrays.copyOfRange(bytes8, 0, min4 - 1)).put((byte) 0);
                                        allocate2.put(allocate3.array());
                                        byte[] array = allocate2.array();
                                        wg6.a(array, "trackInfoData.array()");
                                        return array;
                                    }
                                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                                }
                                throw new rc6("null cannot be cast to non-null type java.lang.String");
                            }
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(o70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            o70 o70 = (o70) obj;
            return !(wg6.a(this.a, o70.a) ^ true) && this.b == o70.b && !(wg6.a(this.c, o70.c) ^ true) && !(wg6.a(this.d, o70.d) ^ true) && !(wg6.a(this.e, o70.e) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.TrackInfo");
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.e;
    }

    @DexIgnore
    public final String getAppName() {
        return this.a;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.d;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.c;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        return this.e.hashCode() + ((hashCode2 + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
    }
}
