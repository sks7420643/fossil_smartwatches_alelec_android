package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq3 {
    @DexIgnore
    public /* final */ Class<?> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public gq3(Class<?> cls, int i, int i2) {
        w12.a(cls, (Object) "Null dependency anInterface.");
        this.a = cls;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public static gq3 a(Class<?> cls) {
        return new gq3(cls, 1, 0);
    }

    @DexIgnore
    public static gq3 b(Class<?> cls) {
        return new gq3(cls, 2, 0);
    }

    @DexIgnore
    public boolean c() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean d() {
        return this.b == 2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof gq3)) {
            return false;
        }
        gq3 gq3 = (gq3) obj;
        if (this.a == gq3.a && this.b == gq3.b && this.c == gq3.c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        int i = this.b;
        boolean z = true;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", direct=");
        if (this.c != 0) {
            z = false;
        }
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return this.c == 0;
    }
}
