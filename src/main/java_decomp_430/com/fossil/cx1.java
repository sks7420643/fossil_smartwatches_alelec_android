package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cx1 {
    @DexIgnore
    public static void a(Status status, rc3<Void> rc3) {
        a(status, (Object) null, rc3);
    }

    @DexIgnore
    public static <TResult> void a(Status status, TResult tresult, rc3<TResult> rc3) {
        if (status.F()) {
            rc3.a(tresult);
        } else {
            rc3.a((Exception) new sv1(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static qc3<Void> a(qc3<Boolean> qc3) {
        return qc3.a((ic3<Boolean, TContinuationResult>) new hz1());
    }
}
