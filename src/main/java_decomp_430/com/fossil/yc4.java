package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yc4 extends xc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public long t;

    /*
    static {
        v.put(2131362561, 1);
        v.put(2131362820, 2);
        v.put(2131362442, 3);
        v.put(2131362567, 4);
        v.put(2131362323, 5);
    }
    */

    @DexIgnore
    public yc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public yc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[3], objArr[1], objArr[4], objArr[2], objArr[0]);
        this.t = -1;
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
