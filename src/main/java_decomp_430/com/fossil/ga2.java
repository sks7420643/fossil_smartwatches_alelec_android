package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ga2 implements IInterface {
    @DexIgnore
    public /* final */ IBinder a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ga2(IBinder iBinder, String str) {
        this.a = iBinder;
        this.b = str;
    }

    @DexIgnore
    public final void a(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.a;
    }

    @DexIgnore
    public final Parcel q() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.b);
        return obtain;
    }
}
