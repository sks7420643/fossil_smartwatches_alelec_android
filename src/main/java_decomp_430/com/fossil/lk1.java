package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk1 {
    @DexIgnore
    public static /* final */ HashMap<lc6<String, w31>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ lk1 b; // = new lk1();

    @DexIgnore
    public final short a(String str, w31 w31) {
        return w31.a;
    }

    @DexIgnore
    public final short b(String str, w31 w31) {
        Byte b2 = a.get(new lc6(str, w31));
        byte byteValue = b2 != null ? b2.byteValue() : 0;
        switch (ri1.b[w31.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return w31.a;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return w31.a((byte) 0);
            case 15:
                return w31.a((byte) 254);
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                a.put(new lc6(str, w31), Byte.valueOf((byte) ((cw0.b(byteValue) + 1) % cw0.b((byte) -1))));
                return w31.a(byteValue);
            default:
                throw new kc6();
        }
    }
}
