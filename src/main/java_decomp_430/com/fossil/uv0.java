package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv0 extends ml0 {
    @DexIgnore
    public fo0 J; // = new fo0(0, 0, 0);

    @DexIgnore
    public uv0(ue1 ue1) {
        super(tf1.GET_CONNECTION_PARAMETERS, lx0.GET_CONNECTION_PARAMS, ue1, 0, 8);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.J = new fo0(cw0.b(order.getShort(0)), cw0.b(order.getShort(2)), cw0.b(order.getShort(4)));
        return cw0.a(jSONObject, this.J.a());
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), this.J.a());
    }
}
