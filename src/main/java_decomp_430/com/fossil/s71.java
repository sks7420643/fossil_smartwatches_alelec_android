package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s71 extends if1 {
    @DexIgnore
    public /* final */ HashMap<qr0, Object> B;

    @DexIgnore
    public s71(ue1 ue1, q41 q41, HashMap<qr0, Object> hashMap) {
        super(ue1, q41, eh1.CONNECT_HID, (String) null, 8);
        this.B = hashMap;
    }

    @DexIgnore
    public final void b(km1 km1) {
        if (km1.b == sk1.SUCCESS) {
            a(km1);
            return;
        }
        this.v = km1;
        if1.a((if1) this, (qv0) new x01(this.w), (hg6) d21.a, (hg6) z31.a, (ig6) null, (hg6) new v51(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (if1) new cf1(this.w, this.x, this.z), (hg6) new py0(this), (hg6) new j01(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public static final /* synthetic */ void a(s71 s71) {
        ue1 ue1 = s71.w;
        Long l = (Long) s71.B.get(qr0.CONNECTION_TIME_OUT);
        if1.a((if1) s71, (qv0) new wt0(ue1, l != null ? l.longValue() : 60000), (hg6) jt0.a, (hg6) av0.a, (ig6) null, (hg6) new vw0(s71), (hg6) null, 40, (Object) null);
    }
}
