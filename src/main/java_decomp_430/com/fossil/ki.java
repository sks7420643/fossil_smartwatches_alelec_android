package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ki extends Closeable {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(int i, double d);

    @DexIgnore
    void a(int i, long j);

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(int i, byte[] bArr);
}
