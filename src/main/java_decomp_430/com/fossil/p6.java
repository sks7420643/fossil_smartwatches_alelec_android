package com.fossil;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Bundle a;
        @DexIgnore
        public /* final */ t6[] b;
        @DexIgnore
        public /* final */ t6[] c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public CharSequence h;
        @DexIgnore
        public PendingIntent i;

        @DexIgnore
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2, charSequence, pendingIntent, new Bundle(), (t6[]) null, (t6[]) null, true, 0, true);
        }

        @DexIgnore
        public PendingIntent a() {
            return this.i;
        }

        @DexIgnore
        public boolean b() {
            return this.d;
        }

        @DexIgnore
        public t6[] c() {
            return this.c;
        }

        @DexIgnore
        public Bundle d() {
            return this.a;
        }

        @DexIgnore
        public int e() {
            return this.g;
        }

        @DexIgnore
        public t6[] f() {
            return this.b;
        }

        @DexIgnore
        public int g() {
            return this.f;
        }

        @DexIgnore
        public boolean h() {
            return this.e;
        }

        @DexIgnore
        public CharSequence i() {
            return this.h;
        }

        @DexIgnore
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, t6[] t6VarArr, t6[] t6VarArr2, boolean z, int i3, boolean z2) {
            this.e = true;
            this.g = i2;
            this.h = d.d(charSequence);
            this.i = pendingIntent;
            this.a = bundle == null ? new Bundle() : bundle;
            this.b = t6VarArr;
            this.c = t6VarArr2;
            this.d = z;
            this.f = i3;
            this.e = z2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends e {
        @DexIgnore
        public CharSequence e;

        @DexIgnore
        public c a(CharSequence charSequence) {
            this.e = d.d(charSequence);
            return this;
        }

        @DexIgnore
        public void a(o6 o6Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(o6Var.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public String A;
        @DexIgnore
        public Bundle B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public Notification E;
        @DexIgnore
        public RemoteViews F;
        @DexIgnore
        public RemoteViews G;
        @DexIgnore
        public RemoteViews H;
        @DexIgnore
        public String I;
        @DexIgnore
        public int J;
        @DexIgnore
        public String K;
        @DexIgnore
        public long L;
        @DexIgnore
        public int M;
        @DexIgnore
        public Notification N;
        @DexIgnore
        @Deprecated
        public ArrayList<String> O;
        @DexIgnore
        public Context a;
        @DexIgnore
        public ArrayList<a> b;
        @DexIgnore
        public ArrayList<a> c;
        @DexIgnore
        public CharSequence d;
        @DexIgnore
        public CharSequence e;
        @DexIgnore
        public PendingIntent f;
        @DexIgnore
        public PendingIntent g;
        @DexIgnore
        public RemoteViews h;
        @DexIgnore
        public Bitmap i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public int k;
        @DexIgnore
        public int l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public e o;
        @DexIgnore
        public CharSequence p;
        @DexIgnore
        public CharSequence[] q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public String u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public String w;
        @DexIgnore
        public boolean x;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public boolean z;

        @DexIgnore
        public d(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            this.N = new Notification();
            this.a = context;
            this.I = str;
            this.N.when = System.currentTimeMillis();
            this.N.audioStreamType = -1;
            this.l = 0;
            this.O = new ArrayList<>();
        }

        @DexIgnore
        public d a(long j2) {
            this.N.when = j2;
            return this;
        }

        @DexIgnore
        public d b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        @DexIgnore
        public d c(int i2) {
            this.k = i2;
            return this;
        }

        @DexIgnore
        public d d(boolean z2) {
            this.m = z2;
            return this;
        }

        @DexIgnore
        public d e(int i2) {
            this.N.icon = i2;
            return this;
        }

        @DexIgnore
        public d f(int i2) {
            this.D = i2;
            return this;
        }

        @DexIgnore
        public d a(CharSequence charSequence) {
            this.e = d(charSequence);
            return this;
        }

        @DexIgnore
        public d b(PendingIntent pendingIntent) {
            this.N.deleteIntent = pendingIntent;
            return this;
        }

        @DexIgnore
        public d c(CharSequence charSequence) {
            this.N.tickerText = d(charSequence);
            return this;
        }

        @DexIgnore
        public d d(int i2) {
            this.l = i2;
            return this;
        }

        @DexIgnore
        public static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        @DexIgnore
        public d a(PendingIntent pendingIntent) {
            this.f = pendingIntent;
            return this;
        }

        @DexIgnore
        public d b(Bitmap bitmap) {
            this.i = a(bitmap);
            return this;
        }

        @DexIgnore
        public d c(boolean z2) {
            a(2, z2);
            return this;
        }

        @DexIgnore
        public final Bitmap a(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(e6.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(e6.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
        }

        @DexIgnore
        public d b(boolean z2) {
            this.x = z2;
            return this;
        }

        @DexIgnore
        public d b(int i2) {
            Notification notification = this.N;
            notification.defaults = i2;
            if ((i2 & 4) != 0) {
                notification.flags |= 1;
            }
            return this;
        }

        @DexIgnore
        public Bundle b() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        @DexIgnore
        public d b(String str) {
            this.I = str;
            return this;
        }

        @DexIgnore
        @Deprecated
        public d(Context context) {
            this(context, (String) null);
        }

        @DexIgnore
        public d a(Uri uri) {
            Notification notification = this.N;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        @DexIgnore
        public d a(long[] jArr) {
            this.N.vibrate = jArr;
            return this;
        }

        @DexIgnore
        public d a(int i2, int i3, int i4) {
            Notification notification = this.N;
            notification.ledARGB = i2;
            notification.ledOnMS = i3;
            notification.ledOffMS = i4;
            int i5 = (notification.ledOnMS == 0 || notification.ledOffMS == 0) ? 0 : 1;
            Notification notification2 = this.N;
            notification2.flags = i5 | (notification2.flags & -2);
            return this;
        }

        @DexIgnore
        public d a(boolean z2) {
            a(16, z2);
            return this;
        }

        @DexIgnore
        public d a(String str) {
            this.A = str;
            return this;
        }

        @DexIgnore
        public final void a(int i2, boolean z2) {
            if (z2) {
                Notification notification = this.N;
                notification.flags = i2 | notification.flags;
                return;
            }
            Notification notification2 = this.N;
            notification2.flags = (~i2) & notification2.flags;
        }

        @DexIgnore
        public d a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new a(i2, charSequence, pendingIntent));
            return this;
        }

        @DexIgnore
        public d a(a aVar) {
            this.b.add(aVar);
            return this;
        }

        @DexIgnore
        public d a(e eVar) {
            if (this.o != eVar) {
                this.o = eVar;
                e eVar2 = this.o;
                if (eVar2 != null) {
                    eVar2.a(this);
                }
            }
            return this;
        }

        @DexIgnore
        public d a(int i2) {
            this.C = i2;
            return this;
        }

        @DexIgnore
        public Notification a() {
            return new q6(this).b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public d a;
        @DexIgnore
        public CharSequence b;
        @DexIgnore
        public CharSequence c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public void a(Bundle bundle) {
        }

        @DexIgnore
        public abstract void a(o6 o6Var);

        @DexIgnore
        public void a(d dVar) {
            if (this.a != dVar) {
                this.a = dVar;
                d dVar2 = this.a;
                if (dVar2 != null) {
                    dVar2.a(this);
                }
            }
        }

        @DexIgnore
        public RemoteViews b(o6 o6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews c(o6 o6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews d(o6 o6Var) {
            return null;
        }
    }

    @DexIgnore
    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return r6.a(notification);
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends e {
        @DexIgnore
        public Bitmap e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public b a(Bitmap bitmap) {
            this.f = bitmap;
            this.g = true;
            return this;
        }

        @DexIgnore
        public b b(Bitmap bitmap) {
            this.e = bitmap;
            return this;
        }

        @DexIgnore
        public void a(o6 o6Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(o6Var.a()).setBigContentTitle(this.b).bigPicture(this.e);
                if (this.g) {
                    bigPicture.bigLargeIcon(this.f);
                }
                if (this.d) {
                    bigPicture.setSummaryText(this.c);
                }
            }
        }
    }
}
