package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f80 extends o80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<f80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public f80 createFromParcel(Parcel parcel) {
            return new f80(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new f80[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m14createFromParcel(Parcel parcel) {
            return new f80(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public f80() {
        super(q80.ASSISTANT, (ce0) null, (ee0) null, 6);
    }

    @DexIgnore
    public f80(ce0 ce0) {
        super(q80.ASSISTANT, ce0, (ee0) null, 4);
    }

    @DexIgnore
    public /* synthetic */ f80(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
