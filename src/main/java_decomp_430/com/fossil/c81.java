package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c81 extends xg6 implements hg6<em0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ub1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c81(ub1 ub1) {
        super(1);
        this.a = ub1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        em0 em0 = (em0) obj;
        hg6<? super em0, cd6> hg6 = this.a.B;
        if (hg6 != null) {
            cd6 cd6 = (cd6) hg6.invoke(em0);
        }
        return cd6.a;
    }
}
