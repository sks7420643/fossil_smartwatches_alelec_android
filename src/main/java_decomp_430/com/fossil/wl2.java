package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl2 extends sl2<Boolean> implements nn2<Boolean>, cp2, RandomAccess {
    @DexIgnore
    public boolean[] b;
    @DexIgnore
    public int c;

    /*
    static {
        new wl2(new boolean[0], 0).k();
    }
    */

    @DexIgnore
    public wl2() {
        this(new boolean[10], 0);
    }

    @DexIgnore
    public final void a(boolean z) {
        a();
        int i = this.c;
        boolean[] zArr = this.b;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[(((i * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.b = zArr2;
        }
        boolean[] zArr3 = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        zArr3[i2] = z;
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        a();
        if (i < 0 || i > (i2 = this.c)) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
        boolean[] zArr = this.b;
        if (i2 < zArr.length) {
            System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
        } else {
            boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.b, i, zArr2, i + 1, this.c - i);
            this.b = zArr2;
        }
        this.b[i] = booleanValue;
        this.c++;
        this.modCount++;
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Boolean> collection) {
        a();
        hn2.a(collection);
        if (!(collection instanceof wl2)) {
            return super.addAll(collection);
        }
        wl2 wl2 = (wl2) collection;
        int i = wl2.c;
        if (i == 0) {
            return false;
        }
        int i2 = this.c;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.b;
            if (i3 > zArr.length) {
                this.b = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(wl2.b, 0, this.b, this.c, wl2.c);
            this.c = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof wl2)) {
            return super.equals(obj);
        }
        wl2 wl2 = (wl2) obj;
        if (this.c != wl2.c) {
            return false;
        }
        boolean[] zArr = wl2.b;
        for (int i = 0; i < this.c; i++) {
            if (this.b[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        zzb(i);
        return Boolean.valueOf(this.b[i]);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + hn2.a(this.b[i2]);
        }
        return i;
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Boolean.valueOf(this.b[i]))) {
                boolean[] zArr = this.b;
                System.arraycopy(zArr, i + 1, zArr, i, (this.c - i) - 1);
                this.c--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            boolean[] zArr = this.b;
            System.arraycopy(zArr, i2, zArr, i, this.c - i2);
            this.c -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        a();
        zzb(i);
        boolean[] zArr = this.b;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final int size() {
        return this.c;
    }

    @DexIgnore
    public final /* synthetic */ nn2 zza(int i) {
        if (i >= this.c) {
            return new wl2(Arrays.copyOf(this.b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void zzb(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
    }

    @DexIgnore
    public final String zzc(int i) {
        int i2 = this.c;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public wl2(boolean[] zArr, int i) {
        this.b = zArr;
        this.c = i;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        zzb(i);
        boolean[] zArr = this.b;
        boolean z = zArr[i];
        int i2 = this.c;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.c--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final /* synthetic */ boolean add(Object obj) {
        a(((Boolean) obj).booleanValue());
        return true;
    }
}
