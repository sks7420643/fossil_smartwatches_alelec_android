package com.fossil;

import com.fossil.m24;
import com.fossil.sw5;
import com.fossil.vw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au5 extends yt5 {
    @DexIgnore
    public RequestEmailOtp e;
    @DexIgnore
    public VerifyEmailOtp f;
    @DexIgnore
    public SignUpEmailAuth g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ zt5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<sw5.d, sw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ au5 a;

        @DexIgnore
        public b(au5 au5) {
            this.a = au5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(RequestEmailOtp.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.i.i();
            this.a.i.O0();
        }

        @DexIgnore
        public void a(RequestEmailOtp.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.i.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "resendOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.i.d(cVar.a(), cVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<vw5.d, vw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ au5 a;

        @DexIgnore
        public c(au5 au5) {
            this.a = au5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(VerifyEmailOtp.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.i.i();
            zt5 a2 = this.a.i;
            SignUpEmailAuth l = this.a.l();
            if (l != null) {
                a2.a(l.getEmail(), 10, 20);
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public void a(VerifyEmailOtp.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.i.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "verifyOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.i.L(cVar.a() == 401);
            this.a.i.d(cVar.a(), cVar.b());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public au5(zt5 zt5) {
        wg6.b(zt5, "mView");
        this.i = zt5;
    }

    @DexIgnore
    public final void b(String[] strArr) {
        this.h = "";
        int length = strArr.length;
        int i2 = 0;
        while (i2 < length) {
            String str = strArr[i2];
            String str2 = this.h;
            if (str != null) {
                this.h = wg6.a(str2, (Object) yj6.d(str).toString());
                i2++;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }

    @DexIgnore
    public void f() {
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            this.i.w(signUpEmailAuth.getEmail());
        }
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.i.o0();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.usecase.RequestEmailOtp, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.au5$b, com.portfolio.platform.CoroutineUseCase$e] */
    public void i() {
        this.i.L(false);
        this.i.k();
        Object r0 = this.e;
        if (r0 != 0) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            String email = signUpEmailAuth != null ? signUpEmailAuth.getEmail() : null;
            if (email != null) {
                r0.a(new RequestEmailOtp.b(email), new b(this));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        zt5 zt5 = this.i;
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            zt5.a(signUpEmailAuth);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.usecase.VerifyEmailOtp, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.au5$c, com.portfolio.platform.CoroutineUseCase$e] */
    public void k() {
        this.i.L(false);
        this.i.k();
        Object r0 = this.f;
        if (r0 != 0) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            if (signUpEmailAuth != null) {
                String email = signUpEmailAuth.getEmail();
                String str = this.h;
                if (str != null) {
                    r0.a(new VerifyEmailOtp.b(email, str), new c(this));
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mVerifyEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public final SignUpEmailAuth l() {
        return this.g;
    }

    @DexIgnore
    public void m() {
        this.i.a(this);
    }

    @DexIgnore
    public void a(String[] strArr) {
        String str;
        wg6.b(strArr, "codes");
        b(strArr);
        String str2 = this.h;
        boolean z = false;
        if (!(str2 == null || xj6.a(str2)) && (str = this.h) != null && str.length() == 4) {
            z = true;
        }
        this.i.y(z);
    }

    @DexIgnore
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        wg6.b(signUpEmailAuth, "emailAuth");
        this.g = signUpEmailAuth;
    }
}
