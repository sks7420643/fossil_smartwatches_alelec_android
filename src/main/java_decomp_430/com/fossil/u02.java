package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u02<T> implements Iterator<T> {
    @DexIgnore
    public /* final */ t02<T> a;
    @DexIgnore
    public int b; // = -1;

    @DexIgnore
    public u02(t02<T> t02) {
        w12.a(t02);
        this.a = t02;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.b < this.a.getCount() - 1;
    }

    @DexIgnore
    public T next() {
        if (hasNext()) {
            t02<T> t02 = this.a;
            int i = this.b + 1;
            this.b = i;
            return t02.get(i);
        }
        int i2 = this.b;
        StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
