package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq1 implements Factory<sq1> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<dq1> b;
    @DexIgnore
    public /* final */ Provider<rr1> c;
    @DexIgnore
    public /* final */ Provider<ur1> d;
    @DexIgnore
    public /* final */ Provider<ys1> e;

    @DexIgnore
    public tq1(Provider<Executor> provider, Provider<dq1> provider2, Provider<rr1> provider3, Provider<ur1> provider4, Provider<ys1> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static tq1 a(Provider<Executor> provider, Provider<dq1> provider2, Provider<rr1> provider3, Provider<ur1> provider4, Provider<ys1> provider5) {
        return new tq1(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public sq1 get() {
        return new sq1((Executor) this.a.get(), (dq1) this.b.get(), (rr1) this.c.get(), (ur1) this.d.get(), (ys1) this.e.get());
    }
}
