package com.fossil;

import com.fossil.m24;
import com.fossil.rt4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr5$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ lr5$c$a a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lr5$c$a$a$a")
        /* renamed from: com.fossil.lr5$c$a$a$a  reason: collision with other inner class name */
        public static final class C0027a implements m24.e<rt4.c, rt4.b> {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0027a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(SignUpEmailUseCase.c cVar) {
                wg6.b(cVar, "responseValue");
                PortfolioApp.get.instance().g().a(this.a.a.this$0.this$0);
                ProfileSetupPresenter profileSetupPresenter = this.a.a.this$0.this$0;
                String lowerCase = "Email".toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                profileSetupPresenter.d(lowerCase);
            }

            @DexIgnore
            public void a(SignUpEmailUseCase.b bVar) {
                wg6.b(bVar, "errorValue");
                this.a.a.this$0.this$0.x.i();
                this.a.a.this$0.this$0.x.g(bVar.a(), bVar.b());
            }
        }

        @DexIgnore
        public a(lr5$c$a lr5_c_a) {
            this.a = lr5_c_a;
        }

        @DexIgnore
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String C = ProfileSetupPresenter.B;
            local.e(C, "getServerSettingList - onFailed. ErrorCode = " + i);
            this.a.this$0.this$0.x.i();
            this.a.this$0.this$0.x.g(i, "");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v9, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase] */
        /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.lr5$c$a$a$a] */
        public void onSuccess(ServerSettingList serverSettingList) {
            List<ServerSetting> serverSettings;
            FLogger.INSTANCE.getLocal().d(ProfileSetupPresenter.B, "getServerSettingList - onSuccess");
            if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                for (ServerSetting next : serverSettings) {
                    if (next != null) {
                        if (wg6.a((Object) next.getObjectId(), (Object) "dataLocationSharingPrivacyVersionLatest")) {
                            this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element = String.valueOf(next.getValue());
                        }
                        if (wg6.a((Object) next.getObjectId(), (Object) "privacyVersionLatest")) {
                            this.a.this$0.$privacyVersionLatest.element = String.valueOf(next.getValue());
                        }
                        if (wg6.a((Object) next.getObjectId(), (Object) "tosVersionLatest")) {
                            this.a.this$0.$termOfUseVersionLatest.element = String.valueOf(next.getValue());
                        }
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String C = ProfileSetupPresenter.B;
            local.d(C, "dataLocationSharingPrivacyVersionLatest=" + this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element + " privacyVersionLatest=" + this.a.this$0.$privacyVersionLatest.element + " termOfUseVersionLatest=" + this.a.this$0.$termOfUseVersionLatest.element);
            ArrayList arrayList = new ArrayList();
            if (this.a.this$0.this$0.t) {
                arrayList.add(this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
            }
            this.a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
            ArrayList arrayList2 = new ArrayList();
            if (this.a.this$0.this$0.s) {
                arrayList2.add(this.a.this$0.$privacyVersionLatest.element);
            }
            this.a.this$0.$it.setAcceptedPrivacies(arrayList2);
            ArrayList arrayList3 = new ArrayList();
            if (this.a.this$0.this$0.r) {
                arrayList3.add(this.a.this$0.$termOfUseVersionLatest.element);
            }
            this.a.this$0.$it.setAcceptedTermsOfService(arrayList3);
            this.a.this$0.this$0.p().a(new SignUpEmailUseCase.a(this.a.this$0.$it), new C0027a(this));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lr5$c$a(ProfileSetupPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        lr5$c$a lr5_c_a = new lr5$c$a(this.this$0, xe6);
        lr5_c_a.p$ = (il6) obj;
        return lr5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((lr5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.A.getServerSettingList(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
