package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import com.fossil.am;
import com.fossil.pl;
import com.fossil.wl;
import com.fossil.zn;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ho implements Runnable {
    @DexIgnore
    public static /* final */ String c; // = tl.a("EnqueueRunnable");
    @DexIgnore
    public /* final */ jm a;
    @DexIgnore
    public /* final */ fm b; // = new fm();

    @DexIgnore
    public ho(jm jmVar) {
        this.a = jmVar;
    }

    @DexIgnore
    public boolean a() {
        WorkDatabase g = this.a.g().g();
        g.beginTransaction();
        try {
            boolean b2 = b(this.a);
            g.setTransactionSuccessful();
            return b2;
        } finally {
            g.endTransaction();
        }
    }

    @DexIgnore
    public wl b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        lm g = this.a.g();
        im.a(g.c(), g.g(), g.f());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.a.h()) {
                if (a()) {
                    jo.a(this.a.g().b(), RescheduleReceiver.class, true);
                    c();
                }
                this.b.a(wl.a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", new Object[]{this.a}));
        } catch (Throwable th) {
            this.b.a(new wl.b.a(th));
        }
    }

    @DexIgnore
    public static boolean b(jm jmVar) {
        List<jm> e = jmVar.e();
        boolean z = false;
        if (e != null) {
            boolean z2 = false;
            for (jm next : e) {
                if (!next.i()) {
                    z2 |= b(next);
                } else {
                    tl.a().e(c, String.format("Already enqueued work ids (%s).", new Object[]{TextUtils.join(", ", next.c())}), new Throwable[0]);
                }
            }
            z = z2;
        }
        return a(jmVar) | z;
    }

    @DexIgnore
    public static boolean a(jm jmVar) {
        boolean a2 = a(jmVar.g(), jmVar.f(), (String[]) jm.a(jmVar).toArray(new String[0]), jmVar.d(), jmVar.b());
        jmVar.j();
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01a8 A[LOOP:6: B:107:0x01a2->B:109:0x01a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x015f  */
    public static boolean a(lm lmVar, List<? extends cm> list, String[] strArr, String str, ql qlVar) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j;
        int i;
        lm lmVar2 = lmVar;
        String[] strArr2 = strArr;
        String str2 = str;
        ql qlVar2 = qlVar;
        long currentTimeMillis = System.currentTimeMillis();
        WorkDatabase g = lmVar.g();
        boolean z5 = strArr2 != null && strArr2.length > 0;
        if (z5) {
            z3 = true;
            z2 = false;
            z = false;
            for (String str3 : strArr2) {
                zn e = g.d().e(str3);
                if (e == null) {
                    tl.a().b(c, String.format("Prerequisite %s doesn't exist; not enqueuing", new Object[]{str3}), new Throwable[0]);
                    return false;
                }
                am.a aVar = e.b;
                z3 &= aVar == am.a.SUCCEEDED;
                if (aVar == am.a.FAILED) {
                    z2 = true;
                } else if (aVar == am.a.CANCELLED) {
                    z = true;
                }
            }
        } else {
            z3 = true;
            z2 = false;
            z = false;
        }
        boolean z6 = !TextUtils.isEmpty(str);
        if (z6 && !z5) {
            List<zn.b> a2 = g.d().a(str2);
            if (!a2.isEmpty()) {
                if (qlVar2 == ql.APPEND) {
                    rn a3 = g.a();
                    ArrayList arrayList = new ArrayList();
                    for (zn.b next : a2) {
                        if (!a3.c(next.a)) {
                            boolean z7 = (next.b == am.a.SUCCEEDED) & z3;
                            am.a aVar2 = next.b;
                            if (aVar2 == am.a.FAILED) {
                                z2 = true;
                            } else if (aVar2 == am.a.CANCELLED) {
                                z = true;
                            }
                            arrayList.add(next.a);
                            z3 = z7;
                        }
                    }
                    strArr2 = (String[]) arrayList.toArray(strArr2);
                    z5 = strArr2.length > 0;
                } else {
                    if (qlVar2 == ql.KEEP) {
                        for (zn.b bVar : a2) {
                            am.a aVar3 = bVar.b;
                            if (aVar3 == am.a.ENQUEUED || aVar3 == am.a.RUNNING) {
                                return false;
                            }
                            while (r3.hasNext()) {
                            }
                        }
                    }
                    go.a(str2, lmVar2, false).run();
                    ao d = g.d();
                    for (zn.b bVar2 : a2) {
                        d.b(bVar2.a);
                    }
                    z4 = true;
                    for (cm cmVar : list) {
                        zn d2 = cmVar.d();
                        if (!z5 || z3) {
                            if (!d2.d()) {
                                d2.n = currentTimeMillis;
                            } else {
                                j = currentTimeMillis;
                                d2.n = 0;
                                i = Build.VERSION.SDK_INT;
                                if (i >= 23 && i <= 25) {
                                    a(d2);
                                } else if (Build.VERSION.SDK_INT <= 22 && a(lmVar2, "androidx.work.impl.background.gcm.GcmScheduler")) {
                                    a(d2);
                                }
                                if (d2.b == am.a.ENQUEUED) {
                                    z4 = true;
                                }
                                g.d().a(d2);
                                if (z5) {
                                    for (String qnVar : strArr2) {
                                        g.a().a(new qn(cmVar.b(), qnVar));
                                    }
                                }
                                for (String coVar : cmVar.c()) {
                                    g.e().a(new co(coVar, cmVar.b()));
                                }
                                if (!z6) {
                                    g.c().a(new wn(str2, cmVar.b()));
                                }
                                currentTimeMillis = j;
                            }
                        } else if (z2) {
                            d2.b = am.a.FAILED;
                        } else if (z) {
                            d2.b = am.a.CANCELLED;
                        } else {
                            d2.b = am.a.BLOCKED;
                        }
                        j = currentTimeMillis;
                        i = Build.VERSION.SDK_INT;
                        if (i >= 23 || i <= 25) {
                        }
                        if (d2.b == am.a.ENQUEUED) {
                        }
                        g.d().a(d2);
                        if (z5) {
                        }
                        while (r3.hasNext()) {
                        }
                        if (!z6) {
                        }
                        currentTimeMillis = j;
                    }
                    return z4;
                }
            }
        }
        z4 = false;
        while (r8.hasNext()) {
        }
        return z4;
    }

    @DexIgnore
    public static void a(zn znVar) {
        nl nlVar = znVar.j;
        if (nlVar.f() || nlVar.i()) {
            String str = znVar.c;
            pl.a aVar = new pl.a();
            aVar.a(znVar.e);
            aVar.a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            znVar.c = ConstraintTrackingWorker.class.getName();
            znVar.e = aVar.a();
        }
    }

    @DexIgnore
    public static boolean a(lm lmVar, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (hm hmVar : lmVar.f()) {
                if (cls.isAssignableFrom(hmVar.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }
}
