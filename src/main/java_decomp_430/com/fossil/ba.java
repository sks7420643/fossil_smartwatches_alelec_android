package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba {
    @DexIgnore
    public WeakReference<View> a;
    @DexIgnore
    public Runnable b; // = null;
    @DexIgnore
    public Runnable c; // = null;
    @DexIgnore
    public int d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ ca a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(ba baVar, ca caVar, View view) {
            this.a = caVar;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a.a(this.b);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b(this.b);
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.c(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ea a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public b(ba baVar, ea eaVar, View view) {
            this.a = eaVar;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements ca {
        @DexIgnore
        public ba a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(ba baVar) {
            this.a = baVar;
        }

        @DexIgnore
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            ca caVar = tag instanceof ca ? (ca) tag : null;
            if (caVar != null) {
                caVar.a(view);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.fossil.ca} */
        /* JADX WARNING: Multi-variable type inference failed */
        public void b(View view) {
            int i = this.a.d;
            ca caVar = null;
            if (i > -1) {
                view.setLayerType(i, (Paint) null);
                this.a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                ba baVar = this.a;
                Runnable runnable = baVar.c;
                if (runnable != null) {
                    baVar.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                if (tag instanceof ca) {
                    caVar = tag;
                }
                if (caVar != null) {
                    caVar.b(view);
                }
                this.b = true;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.ca} */
        /* JADX WARNING: Multi-variable type inference failed */
        public void c(View view) {
            this.b = false;
            ca caVar = null;
            if (this.a.d > -1) {
                view.setLayerType(2, (Paint) null);
            }
            ba baVar = this.a;
            Runnable runnable = baVar.b;
            if (runnable != null) {
                baVar.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            if (tag instanceof ca) {
                caVar = tag;
            }
            if (caVar != null) {
                caVar.c(view);
            }
        }
    }

    @DexIgnore
    public ba(View view) {
        this.a = new WeakReference<>(view);
    }

    @DexIgnore
    public ba a(long j) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    public ba b(float f) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    @DexIgnore
    public void c() {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    public ba a(float f) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    public long b() {
        View view = (View) this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    public ba a(Interpolator interpolator) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    public ba b(long j) {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    public void a() {
        View view = (View) this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    public ba a(ca caVar) {
        View view = (View) this.a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                a(view, caVar);
            } else {
                view.setTag(2113929216, caVar);
                a(view, new c(this));
            }
        }
        return this;
    }

    @DexIgnore
    public final void a(View view, ca caVar) {
        if (caVar != null) {
            view.animate().setListener(new a(this, caVar, view));
        } else {
            view.animate().setListener((Animator.AnimatorListener) null);
        }
    }

    @DexIgnore
    public ba a(ea eaVar) {
        View view = (View) this.a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            b bVar = null;
            if (eaVar != null) {
                bVar = new b(this, eaVar, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
