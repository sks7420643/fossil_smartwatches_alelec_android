package com.fossil;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gg3 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b; // = 300;
    @DexIgnore
    public TimeInterpolator c; // = null;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public int e; // = 1;

    @DexIgnore
    public gg3(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    @DexIgnore
    public void a(Animator animator) {
        animator.setStartDelay(a());
        animator.setDuration(b());
        animator.setInterpolator(c());
        if (animator instanceof ValueAnimator) {
            ValueAnimator valueAnimator = (ValueAnimator) animator;
            valueAnimator.setRepeatCount(d());
            valueAnimator.setRepeatMode(e());
        }
    }

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public TimeInterpolator c() {
        TimeInterpolator timeInterpolator = this.c;
        return timeInterpolator != null ? timeInterpolator : yf3.b;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof gg3)) {
            return false;
        }
        gg3 gg3 = (gg3) obj;
        if (a() == gg3.a() && b() == gg3.b() && d() == gg3.d() && e() == gg3.e()) {
            return c().getClass().equals(gg3.c().getClass());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((int) (a() ^ (a() >>> 32))) * 31) + ((int) (b() ^ (b() >>> 32)))) * 31) + c().getClass().hashCode()) * 31) + d()) * 31) + e();
    }

    @DexIgnore
    public String toString() {
        return 10 + gg3.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " delay: " + a() + " duration: " + b() + " interpolator: " + c().getClass() + " repeatCount: " + d() + " repeatMode: " + e() + "}\n";
    }

    @DexIgnore
    public static TimeInterpolator b(ValueAnimator valueAnimator) {
        TimeInterpolator interpolator = valueAnimator.getInterpolator();
        if ((interpolator instanceof AccelerateDecelerateInterpolator) || interpolator == null) {
            return yf3.b;
        }
        if (interpolator instanceof AccelerateInterpolator) {
            return yf3.c;
        }
        return interpolator instanceof DecelerateInterpolator ? yf3.d : interpolator;
    }

    @DexIgnore
    public long a() {
        return this.a;
    }

    @DexIgnore
    public static gg3 a(ValueAnimator valueAnimator) {
        gg3 gg3 = new gg3(valueAnimator.getStartDelay(), valueAnimator.getDuration(), b(valueAnimator));
        gg3.d = valueAnimator.getRepeatCount();
        gg3.e = valueAnimator.getRepeatMode();
        return gg3;
    }

    @DexIgnore
    public gg3(long j, long j2, TimeInterpolator timeInterpolator) {
        this.a = j;
        this.b = j2;
        this.c = timeInterpolator;
    }
}
