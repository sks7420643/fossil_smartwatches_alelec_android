package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<i90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new i90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new i90[i];
        }
    }

    @DexIgnore
    public i90(byte b) {
        super(e90.AUTHENTICATION_REQUEST, b);
    }

    @DexIgnore
    public /* synthetic */ i90(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
