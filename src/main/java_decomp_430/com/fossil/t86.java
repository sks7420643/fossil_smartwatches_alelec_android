package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t86 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ cb6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends y86 {
        @DexIgnore
        public /* final */ /* synthetic */ s86 a;

        @DexIgnore
        public a(s86 s86) {
            this.a = s86;
        }

        @DexIgnore
        public void a() {
            s86 a2 = t86.this.b();
            if (!this.a.equals(a2)) {
                c86.g().d("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                t86.this.c(a2);
            }
        }
    }

    @DexIgnore
    public t86(Context context) {
        this.a = context.getApplicationContext();
        this.b = new db6(context, "TwitterAdvertisingInfoPreferences");
    }

    @DexIgnore
    public final void b(s86 s86) {
        new Thread(new a(s86)).start();
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final void c(s86 s86) {
        if (a(s86)) {
            cb6 cb6 = this.b;
            cb6.a(cb6.edit().putString("advertising_id", s86.a).putBoolean("limit_ad_tracking_enabled", s86.b));
            return;
        }
        cb6 cb62 = this.b;
        cb62.a(cb62.edit().remove("advertising_id").remove("limit_ad_tracking_enabled"));
    }

    @DexIgnore
    public w86 d() {
        return new u86(this.a);
    }

    @DexIgnore
    public w86 e() {
        return new v86(this.a);
    }

    @DexIgnore
    public s86 a() {
        s86 c = c();
        if (a(c)) {
            c86.g().d("Fabric", "Using AdvertisingInfo from Preference Store");
            b(c);
            return c;
        }
        s86 b2 = b();
        c(b2);
        return b2;
    }

    @DexIgnore
    public final s86 b() {
        s86 a2 = d().a();
        if (!a(a2)) {
            a2 = e().a();
            if (!a(a2)) {
                c86.g().d("Fabric", "AdvertisingInfo not present");
            } else {
                c86.g().d("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            c86.g().d("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return a2;
    }

    @DexIgnore
    public final boolean a(s86 s86) {
        return s86 != null && !TextUtils.isEmpty(s86.a);
    }

    @DexIgnore
    public s86 c() {
        return new s86(this.b.get().getString("advertising_id", ""), this.b.get().getBoolean("limit_ad_tracking_enabled", false));
    }
}
