package com.fossil;

import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jk extends ik {
    @DexIgnore
    public void a(View view, float f) {
        view.setTransitionAlpha(f);
    }

    @DexIgnore
    public float b(View view) {
        return view.getTransitionAlpha();
    }

    @DexIgnore
    public void c(View view, Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }

    @DexIgnore
    public void a(View view, int i) {
        view.setTransitionVisibility(i);
    }

    @DexIgnore
    public void b(View view, Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }

    @DexIgnore
    public void a(View view, int i, int i2, int i3, int i4) {
        view.setLeftTopRightBottom(i, i2, i3, i4);
    }

    @DexIgnore
    public void a(View view, Matrix matrix) {
        view.setAnimationMatrix(matrix);
    }
}
