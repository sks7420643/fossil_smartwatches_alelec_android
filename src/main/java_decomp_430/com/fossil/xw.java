package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw implements rt<BitmapDrawable>, nt {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ rt<Bitmap> b;

    @DexIgnore
    public xw(Resources resources, rt<Bitmap> rtVar) {
        q00.a(resources);
        this.a = resources;
        q00.a(rtVar);
        this.b = rtVar;
    }

    @DexIgnore
    public static rt<BitmapDrawable> a(Resources resources, rt<Bitmap> rtVar) {
        if (rtVar == null) {
            return null;
        }
        return new xw(resources, rtVar);
    }

    @DexIgnore
    public int b() {
        return this.b.b();
    }

    @DexIgnore
    public Class<BitmapDrawable> c() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    public void d() {
        rt<Bitmap> rtVar = this.b;
        if (rtVar instanceof nt) {
            ((nt) rtVar).d();
        }
    }

    @DexIgnore
    public void a() {
        this.b.a();
    }

    @DexIgnore
    public BitmapDrawable get() {
        return new BitmapDrawable(this.a, this.b.get());
    }
}
