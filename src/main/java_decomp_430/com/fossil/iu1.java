package com.fossil;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu1 {
    @DexIgnore
    public static iu1 c;
    @DexIgnore
    public vt1 a;
    @DexIgnore
    public GoogleSignInAccount b; // = this.a.b();

    @DexIgnore
    public iu1(Context context) {
        this.a = vt1.a(context);
        this.a.c();
    }

    @DexIgnore
    public static synchronized iu1 a(Context context) {
        iu1 b2;
        synchronized (iu1.class) {
            b2 = b(context.getApplicationContext());
        }
        return b2;
    }

    @DexIgnore
    public static synchronized iu1 b(Context context) {
        iu1 iu1;
        synchronized (iu1.class) {
            if (c == null) {
                c = new iu1(context);
            }
            iu1 = c;
        }
        return iu1;
    }

    @DexIgnore
    public final synchronized void a() {
        this.a.a();
        this.b = null;
    }

    @DexIgnore
    public final synchronized GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final synchronized void a(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.a.a(googleSignInAccount, googleSignInOptions);
        this.b = googleSignInAccount;
    }
}
