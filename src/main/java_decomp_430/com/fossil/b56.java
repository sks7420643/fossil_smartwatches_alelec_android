package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b56 {
    @DexIgnore
    public String a; // = "default";
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public int c; // = 2;

    @DexIgnore
    public b56(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        int length = stackTrace.length;
        int i = 0;
        while (i < length) {
            StackTraceElement stackTraceElement = stackTrace[i];
            if (stackTraceElement.isNativeMethod() || stackTraceElement.getClassName().equals(Thread.class.getName()) || stackTraceElement.getClassName().equals(b56.class.getName())) {
                i++;
            } else {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(Object obj) {
        if (b()) {
            b(obj);
        }
    }

    @DexIgnore
    public final void a(Throwable th) {
        if (b()) {
            b(th);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void b(Object obj) {
        String str;
        if (this.c <= 3) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.d(this.a, str);
            w56 d = n36.d();
            if (d != null) {
                d.c(str);
            }
        }
    }

    @DexIgnore
    public final void b(Throwable th) {
        if (this.c <= 6) {
            Log.e(this.a, "", th);
            w56 d = n36.d();
            if (d != null) {
                d.d(th);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final void c(Object obj) {
        if (b()) {
            d(obj);
        }
    }

    @DexIgnore
    public final void d(Object obj) {
        String str;
        if (this.c <= 6) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.e(this.a, str);
            w56 d = n36.d();
            if (d != null) {
                d.d(str);
            }
        }
    }

    @DexIgnore
    public final void e(Object obj) {
        if (b()) {
            f(obj);
        }
    }

    @DexIgnore
    public final void f(Object obj) {
        String str;
        if (this.c <= 4) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.i(this.a, str);
            w56 d = n36.d();
            if (d != null) {
                d.a(str);
            }
        }
    }

    @DexIgnore
    public final void g(Object obj) {
        if (b()) {
            h(obj);
        }
    }

    @DexIgnore
    public final void h(Object obj) {
        String str;
        if (this.c <= 5) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.w(this.a, str);
            w56 d = n36.d();
            if (d != null) {
                d.b(str);
            }
        }
    }
}
