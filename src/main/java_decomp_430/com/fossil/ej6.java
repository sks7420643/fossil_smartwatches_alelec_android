package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej6 {
    @DexIgnore
    public static /* final */ Charset a;

    /*
    static {
        new ej6();
        Charset forName = Charset.forName("UTF-8");
        wg6.a((Object) forName, "Charset.forName(\"UTF-8\")");
        a = forName;
        wg6.a((Object) Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        wg6.a((Object) Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        wg6.a((Object) Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        wg6.a((Object) Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        wg6.a((Object) Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }
    */
}
