package com.fossil;

import android.database.Cursor;
import com.fossil.gf;
import com.fossil.lh;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yh<T> extends gf<T> {
    @DexIgnore
    public /* final */ String mCountQuery;
    @DexIgnore
    public /* final */ oh mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public /* final */ rh mSourceQuery;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lh.c {
        @DexIgnore
        public a(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            yh.this.invalidate();
        }
    }

    @DexIgnore
    public yh(oh ohVar, li liVar, boolean z, String... strArr) {
        this(ohVar, rh.a(liVar), z, strArr);
    }

    @DexIgnore
    private rh getSQLiteQuery(int i, int i2) {
        rh b = rh.b(this.mLimitOffsetQuery, this.mSourceQuery.a() + 2);
        b.a(this.mSourceQuery);
        b.a(b.a() - 1, (long) i2);
        b.a(b.a(), (long) i);
        return b;
    }

    @DexIgnore
    public abstract List<T> convertRows(Cursor cursor);

    @DexIgnore
    public int countItems() {
        rh b = rh.b(this.mCountQuery, this.mSourceQuery.a());
        b.a(this.mSourceQuery);
        Cursor query = this.mDb.query(b);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            query.close();
            b.c();
            return 0;
        } finally {
            query.close();
            b.c();
        }
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().c();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadInitial(gf.d dVar, gf.b<T> bVar) {
        rh rhVar;
        List list;
        int i;
        List emptyList = Collections.emptyList();
        this.mDb.beginTransaction();
        Cursor cursor = null;
        try {
            int countItems = countItems();
            if (countItems != 0) {
                i = gf.computeInitialLoadPosition(dVar, countItems);
                rhVar = getSQLiteQuery(i, gf.computeInitialLoadSize(dVar, i, countItems));
                try {
                    cursor = this.mDb.query(rhVar);
                    list = convertRows(cursor);
                    this.mDb.setTransactionSuccessful();
                } catch (Throwable th) {
                    th = th;
                }
            } else {
                list = emptyList;
                rhVar = null;
                i = 0;
            }
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (rhVar != null) {
                rhVar.c();
            }
            bVar.a(list, i, countItems);
        } catch (Throwable th2) {
            th = th2;
            rhVar = null;
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (rhVar != null) {
                rhVar.c();
            }
            throw th;
        }
    }

    @DexIgnore
    public void loadRange(gf.g gVar, gf.e<T> eVar) {
        eVar.a(loadRange(gVar.a, gVar.b));
    }

    @DexIgnore
    public yh(oh ohVar, rh rhVar, boolean z, String... strArr) {
        this.mDb = ohVar;
        this.mSourceQuery = rhVar;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new a(strArr);
        ohVar.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    public List<T> loadRange(int i, int i2) {
        rh sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                List<T> convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
                return convertRows;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.c();
            }
        } else {
            Cursor query = this.mDb.query(sQLiteQuery);
            try {
                return convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.c();
            }
        }
    }
}
