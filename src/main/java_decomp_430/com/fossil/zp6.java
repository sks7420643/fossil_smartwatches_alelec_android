package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp6 {
    @DexIgnore
    public static /* final */ uo6 a; // = new uo6("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ uo6 b; // = new uo6("LOCKED");
    @DexIgnore
    public static /* final */ uo6 c; // = new uo6("UNLOCKED");
    @DexIgnore
    public static /* final */ wp6 d; // = new wp6(b);
    @DexIgnore
    public static /* final */ wp6 e; // = new wp6(c);

    /*
    static {
        new uo6("LOCK_FAIL");
        new uo6("ENQUEUE_FAIL");
        new uo6("SELECT_SUCCESS");
    }
    */

    @DexIgnore
    public static /* synthetic */ xp6 a(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }

    @DexIgnore
    public static final xp6 a(boolean z) {
        return new yp6(z);
    }
}
