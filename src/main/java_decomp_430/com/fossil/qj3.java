package com.fossil;

import android.view.View;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qj3 extends tj3 {
    @DexIgnore
    public qj3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public void a() {
        this.a.setEndIconOnClickListener((View.OnClickListener) null);
        this.a.setEndIconOnLongClickListener((View.OnLongClickListener) null);
    }
}
