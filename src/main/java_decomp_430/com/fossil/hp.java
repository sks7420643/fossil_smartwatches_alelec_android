package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hp extends bq {
    @DexIgnore
    public Intent mResolutionIntent;

    @DexIgnore
    public hp() {
    }

    @DexIgnore
    public String getMessage() {
        if (this.mResolutionIntent != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }

    @DexIgnore
    public Intent getResolutionIntent() {
        return this.mResolutionIntent;
    }

    @DexIgnore
    public hp(Intent intent) {
        this.mResolutionIntent = intent;
    }

    @DexIgnore
    public hp(rp rpVar) {
        super(rpVar);
    }

    @DexIgnore
    public hp(String str) {
        super(str);
    }

    @DexIgnore
    public hp(String str, Exception exc) {
        super(str, exc);
    }
}
