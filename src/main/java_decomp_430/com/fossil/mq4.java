package com.fossil;

import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq4 implements Factory<lq4> {
    @DexIgnore
    public static /* final */ mq4 a; // = new mq4();

    @DexIgnore
    public static mq4 a() {
        return a;
    }

    @DexIgnore
    public static HybridMessageNotificationComponent b() {
        return new HybridMessageNotificationComponent();
    }

    @DexIgnore
    public HybridMessageNotificationComponent get() {
        return b();
    }
}
