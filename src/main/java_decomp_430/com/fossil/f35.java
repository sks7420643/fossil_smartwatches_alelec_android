package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f35 {
    @DexIgnore
    public /* final */ e35 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<wx4> c;

    @DexIgnore
    public f35(e35 e35, int i, ArrayList<wx4> arrayList) {
        wg6.b(e35, "mView");
        this.a = e35;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final ArrayList<wx4> a() {
        ArrayList<wx4> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final e35 c() {
        return this.a;
    }
}
