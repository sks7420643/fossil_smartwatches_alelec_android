package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d64 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ lf4 s;
    @DexIgnore
    public /* final */ lf4 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public d64(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, lf4 lf4, lf4 lf42, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = lf4;
        a(this.s);
        this.t = lf42;
        a(this.t);
        this.u = linearLayout;
    }
}
