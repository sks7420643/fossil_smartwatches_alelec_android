package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.helper.DeviceHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ck4 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[DeviceHelper.ImageStyle.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];

    /*
    static {
        a[DeviceHelper.ImageStyle.SMALL.ordinal()] = 1;
        a[DeviceHelper.ImageStyle.NORMAL.ordinal()] = 2;
        a[DeviceHelper.ImageStyle.LARGE.ordinal()] = 3;
        a[DeviceHelper.ImageStyle.WATCH_COMPLETED.ordinal()] = 4;
        a[DeviceHelper.ImageStyle.HYBRID_WATCH_HOUR.ordinal()] = 5;
        a[DeviceHelper.ImageStyle.HYBRID_WATCH_MINUTE.ordinal()] = 6;
        a[DeviceHelper.ImageStyle.HYBRID_WATCH_SUBEYE.ordinal()] = 7;
        a[DeviceHelper.ImageStyle.DIANA_WATCH_HOUR.ordinal()] = 8;
        a[DeviceHelper.ImageStyle.DIANA_WATCH_MINUTE.ordinal()] = 9;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
    }
    */
}
