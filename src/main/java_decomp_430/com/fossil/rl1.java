package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl1 {
    @DexIgnore
    public static /* final */ KeyStore a;
    @DexIgnore
    public static /* final */ rl1 b; // = new rl1();

    /*
    static {
        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
        instance.load((KeyStore.LoadStoreParameter) null);
        wg6.a(instance, "mKeyStore");
        a = instance;
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000b, code lost:
        r4 = a.getCertificate(r4);
     */
    @DexIgnore
    public final KeyPair a(String str) {
        Certificate certificate;
        PrivateKey privateKey = (PrivateKey) a.getKey(str, (char[]) null);
        PublicKey publicKey = (privateKey == null || certificate == null) ? null : certificate.getPublicKey();
        if (privateKey == null || publicKey == null) {
            return null;
        }
        return new KeyPair(publicKey, privateKey);
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey b(String str) {
        if (a.containsAlias(str)) {
            KeyStore.Entry entry = a.getEntry(str, (KeyStore.ProtectionParameter) null);
            if (entry != null) {
                SecretKey secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
                wg6.a(secretKey, "secretKeyEntry.secretKey");
                return secretKey;
            }
            throw new rc6("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
        }
        KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
        KeyGenParameterSpec build = new KeyGenParameterSpec.Builder(str, 3).setBlockModes(new String[]{"GCM"}).setEncryptionPaddings(new String[]{"NoPadding"}).build();
        wg6.a(build, "KeyGenParameterSpec\n    \u2026\n                .build()");
        instance.init(build);
        SecretKey generateKey = instance.generateKey();
        wg6.a(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final KeyPair a(Context context, String str) {
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        Calendar instance2 = Calendar.getInstance();
        Calendar instance3 = Calendar.getInstance();
        instance3.add(1, 20);
        KeyPairGeneratorSpec.Builder serialNumber = new KeyPairGeneratorSpec.Builder(context.getApplicationContext()).setAlias(str).setSerialNumber(BigInteger.ONE);
        KeyPairGeneratorSpec.Builder subject = serialNumber.setSubject(new X500Principal("CN=" + str + " CA Certificate"));
        wg6.a(instance2, GoalPhase.COLUMN_START_DATE);
        KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
        wg6.a(instance3, GoalPhase.COLUMN_END_DATE);
        KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
        wg6.a(endDate, "KeyPairGeneratorSpec.Bui\u2026.setEndDate(endDate.time)");
        instance.initialize(endDate.build());
        KeyPair generateKeyPair = instance.generateKeyPair();
        wg6.a(generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }
}
