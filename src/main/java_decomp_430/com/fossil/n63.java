package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n63 implements Callable<byte[]> {
    @DexIgnore
    public /* final */ /* synthetic */ j03 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ d63 c;

    @DexIgnore
    public n63(d63 d63, j03 j03, String str) {
        this.c = d63;
        this.a = j03;
        this.b = str;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.c.a.t();
        this.c.a.n().a(this.a, this.b);
        throw null;
    }
}
