package com.fossil;

import com.fossil.mo1;
import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo1 extends yw3<uo1, b> implements vo1 {
    @DexIgnore
    public static /* final */ uo1 f; // = new uo1();
    @DexIgnore
    public static volatile gx3<uo1> g;
    @DexIgnore
    public int d;
    @DexIgnore
    public mo1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<uo1, b> implements vo1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(c cVar) {
            d();
            ((uo1) this.b).a(cVar);
            return this;
        }

        @DexIgnore
        public b() {
            super(uo1.f);
        }

        @DexIgnore
        public b a(mo1 mo1) {
            d();
            uo1.a((uo1) this.b, mo1);
            return this;
        }
    }

    @DexIgnore
    public enum c implements zw3.a {
        UNKNOWN(0),
        ANDROID(4),
        UNRECOGNIZED(-1);
        
        @DexIgnore
        public /* final */ int zzd;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements zw3.b<c> {
        }

        /*
        static {
            UNKNOWN = new c("UNKNOWN", 0, 0);
            ANDROID = new c("ANDROID", 1, 4);
            UNRECOGNIZED = new c("UNRECOGNIZED", 2, -1);
            c[] cVarArr = {UNKNOWN, ANDROID, UNRECOGNIZED};
            new a();
        }
        */

        @DexIgnore
        public c(int i) {
            this.zzd = i;
        }

        @DexIgnore
        public static c zza(int i) {
            if (i == 0) {
                return UNKNOWN;
            }
            if (i != 4) {
                return null;
            }
            return ANDROID;
        }

        @DexIgnore
        public final int getNumber() {
            return this.zzd;
        }
    }

    /*
    static {
        f.g();
    }
    */

    @DexIgnore
    public static uo1 k() {
        return f;
    }

    @DexIgnore
    public static b l() {
        return (b) f.c();
    }

    @DexIgnore
    public static gx3<uo1> m() {
        return f.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new uo1();
            case 2:
                return f;
            case 3:
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                uo1 uo1 = (uo1) obj2;
                boolean z3 = this.d != 0;
                int i = this.d;
                if (uo1.d == 0) {
                    z = false;
                }
                this.d = kVar.a(z3, i, z, uo1.d);
                this.e = (mo1) kVar.a(this.e, uo1.e);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z2) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 8) {
                                this.d = tw3.c();
                            } else if (l == 18) {
                                mo1.b bVar = this.e != null ? (mo1.b) this.e.c() : null;
                                this.e = (mo1) tw3.a(mo1.m(), ww3);
                                if (bVar != null) {
                                    bVar.b(this.e);
                                    this.e = (mo1) bVar.c();
                                }
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z2 = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (g == null) {
                    synchronized (uo1.class) {
                        if (g == null) {
                            g = new yw3.c(f);
                        }
                    }
                }
                return g;
            default:
                throw new UnsupportedOperationException();
        }
        return f;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.d != c.UNKNOWN.getNumber()) {
            i2 = 0 + uw3.c(1, this.d);
        }
        mo1 mo1 = this.e;
        if (mo1 != null) {
            if (mo1 == null) {
                mo1 = mo1.k();
            }
            i2 += uw3.b(2, (dx3) mo1);
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        if (this.d != c.UNKNOWN.getNumber()) {
            uw3.a(1, this.d);
        }
        mo1 mo1 = this.e;
        if (mo1 != null) {
            if (mo1 == null) {
                mo1 = mo1.k();
            }
            uw3.a(2, (dx3) mo1);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        if (cVar != null) {
            this.d = cVar.getNumber();
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void a(uo1 uo1, mo1 mo1) {
        if (mo1 != null) {
            uo1.e = mo1;
            return;
        }
        throw new NullPointerException();
    }
}
