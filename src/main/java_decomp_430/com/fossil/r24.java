package com.fossil;

import com.fossil.zf;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r24 extends zf.d<GoalTrackingData> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        wg6.b(goalTrackingData, "oldItem");
        wg6.b(goalTrackingData2, "newItem");
        return wg6.a((Object) goalTrackingData, (Object) goalTrackingData2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        wg6.b(goalTrackingData, "oldItem");
        wg6.b(goalTrackingData2, "newItem");
        return wg6.a((Object) goalTrackingData.getId(), (Object) goalTrackingData2.getId());
    }
}
