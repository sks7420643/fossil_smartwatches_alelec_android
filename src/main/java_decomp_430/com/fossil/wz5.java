package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cg;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wz5 extends cg.i {
    @DexIgnore
    public wz5(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
        wg6.b(canvas, "c");
        wg6.b(recyclerView, "recyclerView");
        wg6.b(viewHolder, "viewHolder");
        wz5.super.a(canvas, recyclerView, viewHolder, f, f2, i, z);
        View view = viewHolder.itemView;
        wg6.a((Object) view, "viewHolder.itemView");
        ColorDrawable colorDrawable = new ColorDrawable(-65536);
        float f3 = (float) 0;
        if (f < f3) {
            colorDrawable.setBounds((view.getRight() + Math.round(f)) - 0, view.getTop(), view.getRight(), view.getBottom());
        } else {
            colorDrawable.setBounds(0, 0, 0, 0);
        }
        colorDrawable.draw(canvas);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((float) PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165719));
        float f4 = (float) 2;
        float bottom = ((float) (view.getBottom() + view.getTop())) / f4;
        String a = jm4.a((Context) PortfolioApp.get.instance(), 2131887118);
        wg6.a((Object) a, "LanguageHelper.getString\u2026ute_time_app_CTA__Delete)");
        if (a != null) {
            String upperCase = a.toUpperCase();
            wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            canvas.drawText(upperCase, ((((float) view.getRight()) + f) - f3) + ((float) PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165399)), bottom - ((paint.descent() + paint.ascent()) / f4), paint);
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        wg6.b(recyclerView, "recyclerView");
        wg6.b(viewHolder, "viewHolder");
        wg6.b(viewHolder2, RemoteFLogger.MESSAGE_TARGET_KEY);
        return false;
    }
}
