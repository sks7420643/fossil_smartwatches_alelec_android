package com.fossil;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class le6 extends ke6 {
    @DexIgnore
    public static final <T> Set<T> a() {
        return be6.INSTANCE;
    }

    @DexIgnore
    public static final <T> LinkedHashSet<T> a(T... tArr) {
        wg6.b(tArr, "elements");
        LinkedHashSet<T> linkedHashSet = new LinkedHashSet<>(he6.a(tArr.length));
        nd6.b(tArr, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> Set<T> a(Set<? extends T> set) {
        wg6.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return set;
        }
        return ke6.a(set.iterator().next());
    }
}
