package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o94 extends n94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public long G;

    /*
    static {
        I.put(2131362542, 1);
        I.put(2131363218, 2);
        I.put(2131362906, 3);
        I.put(2131362587, 4);
        I.put(2131362339, 5);
        I.put(2131362338, 6);
        I.put(2131362087, 7);
        I.put(2131362186, 8);
        I.put(2131362197, 9);
        I.put(2131362198, 10);
        I.put(2131362188, 11);
        I.put(2131362375, 12);
        I.put(2131362340, 13);
        I.put(2131362234, 14);
        I.put(2131361934, 15);
    }
    */

    @DexIgnore
    public o94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 16, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public o94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[15], objArr[7], objArr[8], objArr[11], objArr[9], objArr[10], objArr[14], objArr[6], objArr[5], objArr[13], objArr[12], objArr[1], objArr[4], objArr[0], objArr[3], objArr[2]);
        this.G = -1;
        this.D.setTag((Object) null);
        a(view);
        f();
    }
}
