package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wh6 extends uh6 implements th6<Integer> {
    @DexIgnore
    public static /* final */ wh6 e; // = new wh6(1, 0);
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wh6 a() {
            return wh6.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public wh6(int i, int i2) {
        super(i, i2, 1);
    }

    @DexIgnore
    public Integer d() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public Integer e() {
        return Integer.valueOf(a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof wh6) {
            if (!isEmpty() || !((wh6) obj).isEmpty()) {
                wh6 wh6 = (wh6) obj;
                if (!(a() == wh6.a() && b() == wh6.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
