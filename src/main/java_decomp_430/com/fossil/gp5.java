package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp5 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, hn4 hn4) {
        loginActivity.B = hn4;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, in4 in4) {
        loginActivity.C = in4;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, kn4 kn4) {
        loginActivity.D = kn4;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, MFLoginWechatManager mFLoginWechatManager) {
        loginActivity.E = mFLoginWechatManager;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, LoginPresenter loginPresenter) {
        loginActivity.F = loginPresenter;
    }
}
