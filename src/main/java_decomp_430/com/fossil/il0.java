package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum il0 {
    SUCCESS(0),
    NOT_START(1),
    COMMAND_ERROR(2),
    RESPONSE_ERROR(3),
    EOF_TIME_OUT(5),
    CONNECTION_DROPPED(6),
    MISS_PACKAGE(7),
    INVALID_DATA_LENGTH(8),
    RECEIVED_DATA_CRC_MISS_MATCH(9),
    INVALID_RESPONSE_LENGTH(10),
    INVALID_RESPONSE_DATA(11),
    REQUEST_UNSUPPORTED(12),
    UNSUPPORTED_FILE_HANDLE(13),
    BLUETOOTH_OFF(14),
    WRONG_AUTHENTICATION_KEY_TYPE(15),
    REQUEST_TIMEOUT(16),
    RESPONSE_TIMEOUT(17),
    EMPTY_SERVICES(18),
    INTERRUPTED(254),
    UNKNOWN_ERROR(255),
    HID_PROXY_NOT_CONNECTED(256),
    HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
    HID_INPUT_DEVICE_DISABLED(258),
    HID_UNKNOWN_ERROR(511);
    
    @DexIgnore
    public static /* final */ oj0 z; // = null;

    /*
    static {
        z = new oj0((qg6) null);
    }
    */

    @DexIgnore
    public il0(int i) {
    }
}
