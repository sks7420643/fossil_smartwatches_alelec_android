package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec0 extends p40 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ hc0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ec0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    Object[] createTypedArray = parcel.createTypedArray(hc0.CREATOR);
                    if (createTypedArray != null) {
                        wg6.a(createTypedArray, "parcel.createTypedArray(Player.CREATOR)!!");
                        return new ec0(readString, readString2, (hc0[]) createTypedArray);
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ec0[i];
        }
    }

    @DexIgnore
    public ec0(String str, String str2, hc0[] hc0Arr) {
        this.a = str;
        this.b = str2;
        this.c = hc0Arr;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.CHALLENGE_ID, (Object) this.a), bm0.NAME, (Object) this.b), bm0.PLAYER_NUM, (Object) Integer.valueOf(this.c.length));
        cw0.a(a2, bm0.USER, (Object) cw0.a((p40[]) this.c));
        return a2;
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject a2 = cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.CHALLENGE_ID, (Object) this.a), bm0.NAME, (Object) this.b), bm0.PLAYER_NUM, (Object) Integer.valueOf(this.c.length));
        cw0.a(a2, bm0.USER, (Object) cw0.a((p40[]) this.c));
        return a2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.a;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final hc0[] getPlayers() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeTypedArray(this.c, i);
    }
}
