package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.common.constants.Constants;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep6 implements Executor, Closeable {
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater i; // = AtomicLongFieldUpdater.newUpdater(ep6.class, "parkedWorkersStack");
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater j; // = AtomicLongFieldUpdater.newUpdater(ep6.class, "controlState");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater o; // = AtomicIntegerFieldUpdater.newUpdater(ep6.class, "_isTerminated");
    @DexIgnore
    public static /* final */ int p; // = xo6.a("kotlinx.coroutines.scheduler.spins", 1000, 1, 0, 8, (Object) null);
    @DexIgnore
    public static /* final */ int q; // = (p + xo6.a("kotlinx.coroutines.scheduler.yields", 0, 0, 0, 8, (Object) null));
    @DexIgnore
    public static /* final */ int r; // = ((int) TimeUnit.SECONDS.toNanos(1));
    @DexIgnore
    public static /* final */ int s; // = ((int) ci6.b(ci6.a(qp6.a / ((long) 4), 10), (long) r));
    @DexIgnore
    public static /* final */ uo6 t; // = new uo6("NOT_IN_STACK");
    @DexIgnore
    public volatile int _isTerminated;
    @DexIgnore
    public /* final */ ip6 a;
    @DexIgnore
    public /* final */ Semaphore b;
    @DexIgnore
    public /* final */ b[] c;
    @DexIgnore
    public volatile long controlState;
    @DexIgnore
    public /* final */ Random d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public volatile long parkedWorkersStack;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Thread {
        @DexIgnore
        public static /* final */ AtomicIntegerFieldUpdater h; // = AtomicIntegerFieldUpdater.newUpdater(b.class, "terminationState");
        @DexIgnore
        public /* final */ sp6 a;
        @DexIgnore
        public long b;
        @DexIgnore
        public long c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public volatile int indexInArray;
        @DexIgnore
        public volatile Object nextParkedWorker;
        @DexIgnore
        public volatile int spins;
        @DexIgnore
        public volatile c state;
        @DexIgnore
        public volatile int terminationState;

        @DexIgnore
        public b() {
            setDaemon(true);
            this.a = new sp6();
            this.state = c.RETIRING;
            this.terminationState = 0;
            this.nextParkedWorker = ep6.t;
            this.d = ep6.s;
            this.e = ep6.this.d.nextInt();
        }

        @DexIgnore
        public final void a(Object obj) {
            this.nextParkedWorker = obj;
        }

        @DexIgnore
        public final void b(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append(ep6.this.h);
            sb.append("-worker-");
            sb.append(i == 0 ? "TERMINATED" : String.valueOf(i));
            setName(sb.toString());
            this.indexInArray = i;
        }

        @DexIgnore
        public final void c() {
            int i = this.spins;
            if (i <= ep6.q) {
                this.spins = i + 1;
                if (i >= ep6.p) {
                    Thread.yield();
                    return;
                }
                return;
            }
            if (this.d < ep6.r) {
                this.d = ci6.b((this.d * 3) >>> 1, ep6.r);
            }
            a(c.PARKING);
            a((long) this.d);
        }

        @DexIgnore
        public final mp6 d() {
            if (m()) {
                return e();
            }
            mp6 b2 = this.a.b();
            return b2 != null ? b2 : ep6.this.a.a(pp6.PROBABLY_BLOCKING);
        }

        @DexIgnore
        public final mp6 e() {
            mp6 mp6;
            mp6 a2;
            boolean z = a(ep6.this.e * 2) == 0;
            if (z && (a2 = ep6.this.a.a(pp6.NON_BLOCKING)) != null) {
                return a2;
            }
            mp6 b2 = this.a.b();
            if (b2 != null) {
                return b2;
            }
            if (z || (mp6 = (mp6) ep6.this.a.c()) == null) {
                return o();
            }
            return mp6;
        }

        @DexIgnore
        public final int f() {
            return this.indexInArray;
        }

        @DexIgnore
        public final sp6 g() {
            return this.a;
        }

        @DexIgnore
        public final c getState() {
            return this.state;
        }

        @DexIgnore
        public final Object h() {
            return this.nextParkedWorker;
        }

        @DexIgnore
        public final ep6 i() {
            return ep6.this;
        }

        @DexIgnore
        public final void j() {
            this.d = ep6.s;
            this.spins = 0;
        }

        @DexIgnore
        public final boolean k() {
            return this.state == c.BLOCKING;
        }

        @DexIgnore
        public final boolean l() {
            return this.state == c.PARKING;
        }

        @DexIgnore
        public final boolean m() {
            if (this.state == c.CPU_ACQUIRED) {
                return true;
            }
            if (!ep6.this.b.tryAcquire()) {
                return false;
            }
            this.state = c.CPU_ACQUIRED;
            return true;
        }

        @DexIgnore
        public final boolean n() {
            int i = this.terminationState;
            if (i == 1 || i == -1) {
                return false;
            }
            if (i == 0) {
                return h.compareAndSet(this, 0, -1);
            }
            throw new IllegalStateException(("Invalid terminationState = " + i).toString());
        }

        @DexIgnore
        public final mp6 o() {
            int c2 = ep6.this.m();
            if (c2 < 2) {
                return null;
            }
            int i = this.f;
            if (i == 0) {
                i = a(c2);
            }
            int i2 = i + 1;
            if (i2 > c2) {
                i2 = 1;
            }
            this.f = i2;
            b bVar = ep6.this.c[i2];
            if (bVar == null || bVar == this || !this.a.a(bVar.a, ep6.this.a)) {
                return null;
            }
            return this.a.b();
        }

        @DexIgnore
        public final void p() {
            synchronized (ep6.this.c) {
                if (!ep6.this.isTerminated()) {
                    if (ep6.this.m() > ep6.this.e) {
                        if (a()) {
                            if (h.compareAndSet(this, 0, 1)) {
                                int i = this.indexInArray;
                                b(0);
                                ep6.this.a(this, i, 0);
                                int andDecrement = (int) (ep6.j.getAndDecrement(ep6.this) & 2097151);
                                if (andDecrement != i) {
                                    b bVar = ep6.this.c[andDecrement];
                                    if (bVar != null) {
                                        ep6.this.c[i] = bVar;
                                        bVar.b(i);
                                        ep6.this.a(bVar, andDecrement, i);
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                                ep6.this.c[andDecrement] = null;
                                cd6 cd6 = cd6.a;
                                this.state = c.TERMINATED;
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void run() {
            boolean z = false;
            while (!ep6.this.isTerminated() && this.state != c.TERMINATED) {
                mp6 d2 = d();
                if (d2 == null) {
                    if (this.state == c.CPU_ACQUIRED) {
                        c();
                    } else {
                        b();
                    }
                    z = true;
                } else {
                    pp6 a2 = d2.a();
                    if (z) {
                        b(a2);
                        z = false;
                    }
                    a(a2, d2.a);
                    ep6.this.a(d2);
                    a(a2);
                }
            }
            a(c.TERMINATED);
        }

        @DexIgnore
        public final boolean a(c cVar) {
            wg6.b(cVar, "newState");
            c cVar2 = this.state;
            boolean z = cVar2 == c.CPU_ACQUIRED;
            if (z) {
                ep6.this.b.release();
            }
            if (cVar2 != cVar) {
                this.state = cVar;
            }
            return z;
        }

        @DexIgnore
        public final void b() {
            a(c.PARKING);
            if (a()) {
                this.terminationState = 0;
                if (this.b == 0) {
                    this.b = System.nanoTime() + ep6.this.g;
                }
                if (a(ep6.this.g) && System.nanoTime() - this.b >= 0) {
                    this.b = 0;
                    p();
                }
            }
        }

        @DexIgnore
        public final void a(pp6 pp6, long j) {
            if (pp6 != pp6.NON_BLOCKING) {
                ep6.j.addAndGet(ep6.this, 2097152);
                if (a(c.BLOCKING)) {
                    ep6.this.o();
                }
            } else if (ep6.this.b.availablePermits() != 0) {
                long a2 = qp6.f.a();
                long j2 = qp6.a;
                if (a2 - j >= j2 && a2 - this.c >= j2 * ((long) 5)) {
                    this.c = a2;
                    ep6.this.o();
                }
            }
        }

        @DexIgnore
        public b(ep6 ep6, int i) {
            this();
            b(i);
        }

        @DexIgnore
        public final void b(pp6 pp6) {
            this.b = 0;
            this.f = 0;
            if (this.state == c.PARKING) {
                if (nl6.a()) {
                    if (!(pp6 == pp6.PROBABLY_BLOCKING)) {
                        throw new AssertionError();
                    }
                }
                this.state = c.BLOCKING;
                this.d = ep6.s;
            }
            this.spins = 0;
        }

        @DexIgnore
        public final void a(pp6 pp6) {
            if (pp6 != pp6.NON_BLOCKING) {
                ep6.j.addAndGet(ep6.this, -2097152);
                c cVar = this.state;
                if (cVar != c.TERMINATED) {
                    if (nl6.a()) {
                        if (!(cVar == c.BLOCKING)) {
                            throw new AssertionError();
                        }
                    }
                    this.state = c.RETIRING;
                }
            }
        }

        @DexIgnore
        public final int a(int i) {
            int i2 = this.e;
            this.e = i2 ^ (i2 << 13);
            int i3 = this.e;
            this.e = i3 ^ (i3 >> 17);
            int i4 = this.e;
            this.e = i4 ^ (i4 << 5);
            int i5 = i - 1;
            if ((i5 & i) == 0) {
                return this.e & i5;
            }
            return (this.e & Integer.MAX_VALUE) % i;
        }

        @DexIgnore
        public final boolean a(long j) {
            ep6.this.b(this);
            if (!a()) {
                return false;
            }
            LockSupport.parkNanos(j);
            return true;
        }

        @DexIgnore
        public final boolean a() {
            mp6 a2 = ep6.this.a.a(pp6.PROBABLY_BLOCKING);
            if (a2 == null) {
                return true;
            }
            this.a.a(a2, ep6.this.a);
            return false;
        }
    }

    @DexIgnore
    public enum c {
        CPU_ACQUIRED,
        BLOCKING,
        PARKING,
        RETIRING,
        TERMINATED
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ep6(int i2, int i3, long j2, String str) {
        wg6.b(str, "schedulerName");
        this.e = i2;
        this.f = i3;
        this.g = j2;
        this.h = str;
        if (this.e >= 1) {
            if (this.f >= this.e) {
                if (this.f <= 2097150) {
                    if (this.g > 0) {
                        this.a = new ip6();
                        this.b = new Semaphore(this.e, false);
                        this.parkedWorkersStack = 0;
                        this.c = new b[(this.f + 1)];
                        this.controlState = 0;
                        this.d = new Random();
                        this._isTerminated = 0;
                        return;
                    }
                    throw new IllegalArgumentException(("Idle worker keep alive time " + this.g + " must be positive").toString());
                }
                throw new IllegalArgumentException(("Max pool size " + this.f + " should not exceed maximal supported number of threads 2097150").toString());
            }
            throw new IllegalArgumentException(("Max pool size " + this.f + " should be greater than or equals to core pool size " + this.e).toString());
        }
        throw new IllegalArgumentException(("Core pool size " + this.e + " should be at least 1").toString());
    }

    @DexIgnore
    public void close() {
        a((long) ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        wg6.b(runnable, Constants.COMMAND);
        a(this, runnable, (np6) null, false, 6, (Object) null);
    }

    @DexIgnore
    public final boolean isTerminated() {
        return this._isTerminated != 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007c, code lost:
        return 0;
     */
    @DexIgnore
    public final int k() {
        synchronized (this.c) {
            if (isTerminated()) {
                return -1;
            }
            long j2 = this.controlState;
            int i2 = (int) (j2 & 2097151);
            int i3 = i2 - ((int) ((j2 & 4398044413952L) >> 21));
            boolean z = false;
            if (i3 >= this.e) {
                return 0;
            }
            if (i2 < this.f) {
                if (this.b.availablePermits() != 0) {
                    int i4 = ((int) (this.controlState & 2097151)) + 1;
                    if (i4 > 0 && this.c[i4] == null) {
                        b bVar = new b(this, i4);
                        bVar.start();
                        if (i4 == ((int) (2097151 & j.incrementAndGet(this)))) {
                            z = true;
                        }
                        if (z) {
                            this.c[i4] = bVar;
                            int i5 = i3 + 1;
                            return i5;
                        }
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            }
        }
    }

    @DexIgnore
    public final b l() {
        Thread currentThread = Thread.currentThread();
        if (!(currentThread instanceof b)) {
            currentThread = null;
        }
        b bVar = (b) currentThread;
        if (bVar == null || !wg6.a((Object) bVar.i(), (Object) this)) {
            return null;
        }
        return bVar;
    }

    @DexIgnore
    public final int m() {
        return (int) (this.controlState & 2097151);
    }

    @DexIgnore
    public final b n() {
        while (true) {
            long j2 = this.parkedWorkersStack;
            b bVar = this.c[(int) (2097151 & j2)];
            if (bVar == null) {
                return null;
            }
            long j3 = (2097152 + j2) & -2097152;
            int a2 = a(bVar);
            if (a2 >= 0) {
                if (i.compareAndSet(this, j2, ((long) a2) | j3)) {
                    bVar.a((Object) t);
                    return bVar;
                }
            }
        }
    }

    @DexIgnore
    public final void o() {
        if (this.b.availablePermits() == 0) {
            p();
        } else if (!p()) {
            long j2 = this.controlState;
            if (((int) (2097151 & j2)) - ((int) ((j2 & 4398044413952L) >> 21)) < this.e) {
                int k = k();
                if (k == 1 && this.e > 1) {
                    k();
                }
                if (k > 0) {
                    return;
                }
            }
            p();
        }
    }

    @DexIgnore
    public final boolean p() {
        while (true) {
            b n = n();
            if (n == null) {
                return false;
            }
            n.j();
            boolean l = n.l();
            LockSupport.unpark(n);
            if (l && n.n()) {
                return true;
            }
        }
    }

    @DexIgnore
    public String toString() {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        for (b bVar : this.c) {
            if (bVar != null) {
                int c2 = bVar.g().c();
                int i7 = fp6.a[bVar.getState().ordinal()];
                if (i7 == 1) {
                    i4++;
                } else if (i7 == 2) {
                    i3++;
                    arrayList.add(String.valueOf(c2) + "b");
                } else if (i7 == 3) {
                    i2++;
                    arrayList.add(String.valueOf(c2) + "c");
                } else if (i7 == 4) {
                    i5++;
                    if (c2 > 0) {
                        arrayList.add(String.valueOf(c2) + "r");
                    }
                } else if (i7 == 5) {
                    i6++;
                }
            }
        }
        long j2 = this.controlState;
        return this.h + '@' + ol6.b(this) + '[' + "Pool Size {" + "core = " + this.e + ", " + "max = " + this.f + "}, " + "Worker States {" + "CPU = " + i2 + ", " + "blocking = " + i3 + ", " + "parked = " + i4 + ", " + "retired = " + i5 + ", " + "terminated = " + i6 + "}, " + "running workers queues = " + arrayList + ", " + "global queue size = " + this.a.b() + ", " + "Control State Workers {" + "created = " + ((int) (2097151 & j2)) + ", " + "blocking = " + ((int) ((j2 & 4398044413952L) >> 21)) + '}' + "]";
    }

    @DexIgnore
    public final void b(b bVar) {
        long j2;
        long j3;
        int f2;
        if (bVar.h() == t) {
            do {
                j2 = this.parkedWorkersStack;
                int i2 = (int) (2097151 & j2);
                j3 = (2097152 + j2) & -2097152;
                f2 = bVar.f();
                if (nl6.a()) {
                    if (!(f2 != 0)) {
                        throw new AssertionError();
                    }
                }
                bVar.a((Object) this.c[i2]);
            } while (!i.compareAndSet(this, j2, ((long) f2) | j3));
        }
    }

    @DexIgnore
    public final int a(b bVar) {
        Object h2 = bVar.h();
        while (h2 != t) {
            if (h2 == null) {
                return 0;
            }
            b bVar2 = (b) h2;
            int f2 = bVar2.f();
            if (f2 != 0) {
                return f2;
            }
            h2 = bVar2.h();
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006a, code lost:
        if (r9 != null) goto L_0x0075;
     */
    @DexIgnore
    public final void a(long j2) {
        int i2;
        mp6 mp6;
        boolean z = false;
        if (o.compareAndSet(this, 0, 1)) {
            b l = l();
            synchronized (this.c) {
                i2 = (int) (this.controlState & 2097151);
            }
            if (1 <= i2) {
                int i3 = 1;
                while (true) {
                    b bVar = this.c[i3];
                    if (bVar != null) {
                        if (bVar != l) {
                            while (bVar.isAlive()) {
                                LockSupport.unpark(bVar);
                                bVar.join(j2);
                            }
                            c state = bVar.getState();
                            if (nl6.a()) {
                                if (!(state == c.TERMINATED)) {
                                    throw new AssertionError();
                                }
                            }
                            bVar.g().a(this.a);
                        }
                        if (i3 == i2) {
                            break;
                        }
                        i3++;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            this.a.a();
            while (true) {
                if (l != null) {
                    mp6 = l.d();
                }
                mp6 = (mp6) this.a.c();
                if (mp6 == null) {
                    break;
                }
                a(mp6);
            }
            if (l != null) {
                l.a(c.TERMINATED);
            }
            if (nl6.a()) {
                if (this.b.availablePermits() == this.e) {
                    z = true;
                }
                if (!z) {
                    throw new AssertionError();
                }
            }
            this.parkedWorkersStack = 0;
            this.controlState = 0;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(ep6 ep6, Runnable runnable, np6 np6, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            np6 = lp6.b;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        ep6.a(runnable, np6, z);
    }

    @DexIgnore
    public final void a(Runnable runnable, np6 np6, boolean z) {
        wg6.b(runnable, "block");
        wg6.b(np6, "taskContext");
        pn6 a2 = qn6.a();
        if (a2 != null) {
            a2.e();
        }
        mp6 a3 = a(runnable, np6);
        int a4 = a(a3, z);
        if (a4 == -1) {
            return;
        }
        if (a4 != 1) {
            o();
        } else if (this.a.a(a3)) {
            o();
        } else {
            throw new RejectedExecutionException(this.h + " was terminated");
        }
    }

    @DexIgnore
    public final mp6 a(Runnable runnable, np6 np6) {
        wg6.b(runnable, "block");
        wg6.b(np6, "taskContext");
        long a2 = qp6.f.a();
        if (!(runnable instanceof mp6)) {
            return new op6(runnable, a2, np6);
        }
        mp6 mp6 = (mp6) runnable;
        mp6.a = a2;
        mp6.b = np6;
        return mp6;
    }

    @DexIgnore
    public final int a(mp6 mp6, boolean z) {
        boolean z2;
        b l = l();
        if (l == null || l.getState() == c.TERMINATED) {
            return 1;
        }
        int i2 = -1;
        if (mp6.a() == pp6.NON_BLOCKING) {
            if (l.k()) {
                i2 = 0;
            } else if (!l.m()) {
                return 1;
            }
        }
        if (z) {
            z2 = l.g().b(mp6, this.a);
        } else {
            z2 = l.g().a(mp6, this.a);
        }
        if (!z2 || l.g().a() > qp6.b) {
            return 0;
        }
        return i2;
    }

    @DexIgnore
    public final void a(mp6 mp6) {
        pn6 a2;
        try {
            mp6.run();
            a2 = qn6.a();
            if (a2 == null) {
                return;
            }
        } catch (Throwable th) {
            pn6 a3 = qn6.a();
            if (a3 != null) {
                a3.c();
            }
            throw th;
        }
        a2.c();
    }

    @DexIgnore
    public final void a(b bVar, int i2, int i3) {
        while (true) {
            long j2 = this.parkedWorkersStack;
            int i4 = (int) (2097151 & j2);
            long j3 = (2097152 + j2) & -2097152;
            int a2 = i4 == i2 ? i3 == 0 ? a(bVar) : i3 : i4;
            if (a2 >= 0) {
                if (i.compareAndSet(this, j2, j3 | ((long) a2))) {
                    return;
                }
            }
        }
    }
}
