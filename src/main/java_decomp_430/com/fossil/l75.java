package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l75 extends j24 {
    @DexIgnore
    public abstract void a(Category category);

    @DexIgnore
    public abstract void a(DianaCustomizeViewModel dianaCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, Parcelable parcelable);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
