package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw implements zr<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ gw a; // = new gw();

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, xr xrVar) throws IOException {
        return true;
    }

    @DexIgnore
    public rt<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, xr xrVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(byteBuffer), i, i2, xrVar);
    }
}
