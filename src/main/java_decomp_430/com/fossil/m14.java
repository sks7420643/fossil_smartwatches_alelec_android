package com.fossil;

import com.portfolio.platform.manager.FileDownloadManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m14 implements Factory<qm4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public m14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static m14 a(b14 b14) {
        return new m14(b14);
    }

    @DexIgnore
    public static FileDownloadManager b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static FileDownloadManager c(b14 b14) {
        FileDownloadManager g = b14.g();
        z76.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    public FileDownloadManager get() {
        return b(this.a);
    }
}
