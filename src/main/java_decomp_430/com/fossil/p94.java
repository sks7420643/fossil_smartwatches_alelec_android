package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ TabLayout r;
    @DexIgnore
    public /* final */ DashBar s;
    @DexIgnore
    public /* final */ ViewPager2 t;

    @DexIgnore
    public p94(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, TabLayout tabLayout, DashBar dashBar, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = tabLayout;
        this.s = dashBar;
        this.t = viewPager2;
    }
}
