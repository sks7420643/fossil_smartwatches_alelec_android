package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q76 {
    @DexIgnore
    public static t76 a(Fragment fragment) {
        t76 t76 = fragment;
        do {
            t76 = t76.getParentFragment();
            if (t76 == null) {
                FragmentActivity activity = fragment.getActivity();
                if (activity instanceof t76) {
                    return (t76) activity;
                }
                if (activity.getApplication() instanceof t76) {
                    return (t76) activity.getApplication();
                }
                throw new IllegalArgumentException(String.format("No injector was found for %s", new Object[]{fragment.getClass().getCanonicalName()}));
            }
        } while (!(t76 instanceof t76));
        return t76;
    }

    @DexIgnore
    public static void b(Fragment fragment) {
        z76.a(fragment, "fragment");
        t76 a = a(fragment);
        if (Log.isLoggable("dagger.android.support", 3)) {
            Log.d("dagger.android.support", String.format("An injector for %s was found in %s", new Object[]{fragment.getClass().getCanonicalName(), a.getClass().getCanonicalName()}));
        }
        d76<Fragment> T = a.T();
        z76.a(T, "%s.supportFragmentInjector() returned null", a.getClass());
        T.a(fragment);
    }
}
