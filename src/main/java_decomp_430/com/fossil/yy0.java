package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy0 extends if1 {
    @DexIgnore
    public fo0 B;
    @DexIgnore
    public /* final */ ArrayList<hl1> C; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ it0 D;

    @DexIgnore
    public yy0(ue1 ue1, q41 q41, it0 it0, String str) {
        super(ue1, q41, eh1.SET_CONNECTION_PARAMS, str);
        this.D = it0;
    }

    @DexIgnore
    public Object d() {
        fo0 fo0 = this.B;
        return fo0 != null ? fo0 : new fo0(0, 0, 0);
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.C;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new uv0(this.w), (hg6) new zr0(this), (hg6) new st0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.REQUESTED_CONNECTION_PARAMS, (Object) this.D.a());
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.ACCEPTED_CONNECTION_PARAMS;
        fo0 fo0 = this.B;
        return cw0.a(k, bm0, (Object) fo0 != null ? fo0.a() : null);
    }

    @DexIgnore
    public final fo0 m() {
        return this.B;
    }
}
