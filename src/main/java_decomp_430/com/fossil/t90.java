package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t90 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<t90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new t90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new t90[i];
        }
    }

    @DexIgnore
    public t90(byte b, int i) {
        super(e90.CHANCE_OF_RAIN_COMPLICATION, b, i);
    }

    @DexIgnore
    public /* synthetic */ t90(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
