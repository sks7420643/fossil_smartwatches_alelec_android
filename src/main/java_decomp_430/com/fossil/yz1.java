package com.fossil;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yz1 extends uy1 {
    @DexIgnore
    public /* final */ /* synthetic */ Dialog a;
    @DexIgnore
    public /* final */ /* synthetic */ wz1 b;

    @DexIgnore
    public yz1(wz1 wz1, Dialog dialog) {
        this.b = wz1;
        this.a = dialog;
    }

    @DexIgnore
    public final void a() {
        this.b.b.g();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
