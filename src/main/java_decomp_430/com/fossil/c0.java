package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c0 {
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_dark; // = 2131099652;
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_light; // = 2131099653;
    @DexIgnore
    public static /* final */ int abc_btn_colored_borderless_text_material; // = 2131099654;
    @DexIgnore
    public static /* final */ int abc_btn_colored_text_material; // = 2131099655;
    @DexIgnore
    public static /* final */ int abc_color_highlight_material; // = 2131099656;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_dark; // = 2131099657;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_light; // = 2131099658;
    @DexIgnore
    public static /* final */ int abc_input_method_navigation_guard; // = 2131099659;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_dark; // = 2131099660;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_light; // = 2131099661;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_dark; // = 2131099662;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_light; // = 2131099663;
    @DexIgnore
    public static /* final */ int abc_search_url_text; // = 2131099664;
    @DexIgnore
    public static /* final */ int abc_search_url_text_normal; // = 2131099665;
    @DexIgnore
    public static /* final */ int abc_search_url_text_pressed; // = 2131099666;
    @DexIgnore
    public static /* final */ int abc_search_url_text_selected; // = 2131099667;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_dark; // = 2131099668;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_light; // = 2131099669;
    @DexIgnore
    public static /* final */ int abc_tint_btn_checkable; // = 2131099670;
    @DexIgnore
    public static /* final */ int abc_tint_default; // = 2131099671;
    @DexIgnore
    public static /* final */ int abc_tint_edittext; // = 2131099672;
    @DexIgnore
    public static /* final */ int abc_tint_seek_thumb; // = 2131099673;
    @DexIgnore
    public static /* final */ int abc_tint_spinner; // = 2131099674;
    @DexIgnore
    public static /* final */ int abc_tint_switch_track; // = 2131099675;
    @DexIgnore
    public static /* final */ int accent_material_dark; // = 2131099677;
    @DexIgnore
    public static /* final */ int accent_material_light; // = 2131099678;
    @DexIgnore
    public static /* final */ int background_floating_material_dark; // = 2131099692;
    @DexIgnore
    public static /* final */ int background_floating_material_light; // = 2131099693;
    @DexIgnore
    public static /* final */ int background_material_dark; // = 2131099694;
    @DexIgnore
    public static /* final */ int background_material_light; // = 2131099695;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_dark; // = 2131099704;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_light; // = 2131099705;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_dark; // = 2131099706;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_light; // = 2131099707;
    @DexIgnore
    public static /* final */ int bright_foreground_material_dark; // = 2131099708;
    @DexIgnore
    public static /* final */ int bright_foreground_material_light; // = 2131099709;
    @DexIgnore
    public static /* final */ int button_material_dark; // = 2131099715;
    @DexIgnore
    public static /* final */ int button_material_light; // = 2131099716;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_dark; // = 2131099817;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_light; // = 2131099818;
    @DexIgnore
    public static /* final */ int dim_foreground_material_dark; // = 2131099819;
    @DexIgnore
    public static /* final */ int dim_foreground_material_light; // = 2131099820;
    @DexIgnore
    public static /* final */ int error_color_material_dark; // = 2131099826;
    @DexIgnore
    public static /* final */ int error_color_material_light; // = 2131099827;
    @DexIgnore
    public static /* final */ int foreground_material_dark; // = 2131099829;
    @DexIgnore
    public static /* final */ int foreground_material_light; // = 2131099830;
    @DexIgnore
    public static /* final */ int highlighted_text_material_dark; // = 2131099875;
    @DexIgnore
    public static /* final */ int highlighted_text_material_light; // = 2131099876;
    @DexIgnore
    public static /* final */ int material_blue_grey_800; // = 2131099888;
    @DexIgnore
    public static /* final */ int material_blue_grey_900; // = 2131099889;
    @DexIgnore
    public static /* final */ int material_blue_grey_950; // = 2131099890;
    @DexIgnore
    public static /* final */ int material_deep_teal_200; // = 2131099891;
    @DexIgnore
    public static /* final */ int material_deep_teal_500; // = 2131099892;
    @DexIgnore
    public static /* final */ int material_grey_100; // = 2131099893;
    @DexIgnore
    public static /* final */ int material_grey_300; // = 2131099894;
    @DexIgnore
    public static /* final */ int material_grey_50; // = 2131099895;
    @DexIgnore
    public static /* final */ int material_grey_600; // = 2131099896;
    @DexIgnore
    public static /* final */ int material_grey_800; // = 2131099897;
    @DexIgnore
    public static /* final */ int material_grey_850; // = 2131099898;
    @DexIgnore
    public static /* final */ int material_grey_900; // = 2131099899;
    @DexIgnore
    public static /* final */ int notification_action_color_filter; // = 2131099972;
    @DexIgnore
    public static /* final */ int notification_icon_bg_color; // = 2131099973;
    @DexIgnore
    public static /* final */ int primary_dark_material_dark; // = 2131100012;
    @DexIgnore
    public static /* final */ int primary_dark_material_light; // = 2131100013;
    @DexIgnore
    public static /* final */ int primary_material_dark; // = 2131100014;
    @DexIgnore
    public static /* final */ int primary_material_light; // = 2131100015;
    @DexIgnore
    public static /* final */ int primary_text_default_material_dark; // = 2131100016;
    @DexIgnore
    public static /* final */ int primary_text_default_material_light; // = 2131100017;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_dark; // = 2131100018;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_light; // = 2131100019;
    @DexIgnore
    public static /* final */ int ripple_material_dark; // = 2131100372;
    @DexIgnore
    public static /* final */ int ripple_material_light; // = 2131100373;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_dark; // = 2131100375;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_light; // = 2131100376;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_dark; // = 2131100377;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_light; // = 2131100378;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_dark; // = 2131100391;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_light; // = 2131100392;
    @DexIgnore
    public static /* final */ int switch_thumb_material_dark; // = 2131100393;
    @DexIgnore
    public static /* final */ int switch_thumb_material_light; // = 2131100394;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_dark; // = 2131100395;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_light; // = 2131100396;
    @DexIgnore
    public static /* final */ int tooltip_background_dark; // = 2131100399;
    @DexIgnore
    public static /* final */ int tooltip_background_light; // = 2131100400;
}
