package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm2 extends dm2 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ int b; // = this.c.zza();
    @DexIgnore
    public /* final */ /* synthetic */ yl2 c;

    @DexIgnore
    public bm2(yl2 yl2) {
        this.c = yl2;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a < this.b;
    }

    @DexIgnore
    public final byte zza() {
        int i = this.a;
        if (i < this.b) {
            this.a = i + 1;
            return this.c.zzb(i);
        }
        throw new NoSuchElementException();
    }
}
