package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kv1 {
    @DexIgnore
    public static /* final */ int a; // = nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    @DexIgnore
    public static /* final */ kv1 b; // = new kv1();

    @DexIgnore
    public static kv1 a() {
        return b;
    }

    @DexIgnore
    public int b(Context context) {
        return nv1.getApkVersion(context);
    }

    @DexIgnore
    public int c(Context context) {
        return a(context, a);
    }

    @DexIgnore
    public int a(Context context, int i) {
        int isGooglePlayServicesAvailable = nv1.isGooglePlayServicesAvailable(context, i);
        if (nv1.isPlayServicesPossiblyUpdating(context, isGooglePlayServicesAvailable)) {
            return 18;
        }
        return isGooglePlayServicesAvailable;
    }

    @DexIgnore
    public boolean b(Context context, int i) {
        return nv1.isPlayServicesPossiblyUpdating(context, i);
    }

    @DexIgnore
    public boolean c(int i) {
        return nv1.isUserRecoverableError(i);
    }

    @DexIgnore
    public String b(int i) {
        return nv1.getErrorString(i);
    }

    @DexIgnore
    public static String b(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(a);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(g52.b(context).b(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Deprecated
    public Intent a(int i) {
        return a((Context) null, i, (String) null);
    }

    @DexIgnore
    public Intent a(Context context, int i, String str) {
        if (i == 1 || i == 2) {
            if (context == null || !o42.c(context)) {
                return o32.a("com.google.android.gms", b(context, str));
            }
            return o32.a();
        } else if (i != 3) {
            return null;
        } else {
            return o32.a("com.google.android.gms");
        }
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2) {
        return a(context, i, i2, (String) null);
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2, String str) {
        Intent a2 = a(context, i, str);
        if (a2 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, a2, 134217728);
    }

    @DexIgnore
    public void a(Context context) {
        nv1.cancelAvailabilityErrorNotifications(context);
    }

    @DexIgnore
    public boolean a(Context context, String str) {
        return nv1.isUninstalledAppPossiblyUpdating(context, str);
    }
}
