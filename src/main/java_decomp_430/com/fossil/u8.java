package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u8<F, S> {
    @DexIgnore
    public /* final */ F a;
    @DexIgnore
    public /* final */ S b;

    @DexIgnore
    public u8(F f, S s) {
        this.a = f;
        this.b = s;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof u8)) {
            return false;
        }
        u8 u8Var = (u8) obj;
        if (!t8.a(u8Var.a, this.a) || !t8.a(u8Var.b, this.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        F f = this.a;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Pair{" + String.valueOf(this.a) + " " + String.valueOf(this.b) + "}";
    }
}
