package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
public final class v95$b$a extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v95$b$a(MicroAppPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        v95$b$a v95_b_a = new v95$b$a(this.this$0, xe6);
        v95_b_a.p$ = (il6) obj;
        return v95_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((v95$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return MicroAppPresenter.f(this.this$0.this$0).c(this.this$0.$id);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
