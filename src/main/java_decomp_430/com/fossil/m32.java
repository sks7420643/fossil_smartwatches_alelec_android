package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.facebook.login.LoginStatusClient;
import com.fossil.k12;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m32 extends k12 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<k12.a, n32> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ b42 f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public m32(Context context) {
        this.d = context.getApplicationContext();
        this.e = new fb2(context.getMainLooper(), this);
        this.f = b42.a();
        this.g = LoginStatusClient.DEFAULT_TOAST_DURATION_MS;
        this.h = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    public final boolean a(k12.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d2;
        w12.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            n32 n32 = this.c.get(aVar);
            if (n32 == null) {
                n32 = new n32(this, aVar);
                n32.a(serviceConnection, str);
                n32.a(str);
                this.c.put(aVar, n32);
            } else {
                this.e.removeMessages(0, aVar);
                if (!n32.a(serviceConnection)) {
                    n32.a(serviceConnection, str);
                    int c2 = n32.c();
                    if (c2 == 1) {
                        serviceConnection.onServiceConnected(n32.b(), n32.a());
                    } else if (c2 == 2) {
                        n32.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = n32.d();
        }
        return d2;
    }

    @DexIgnore
    public final void b(k12.a aVar, ServiceConnection serviceConnection, String str) {
        w12.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            n32 n32 = this.c.get(aVar);
            if (n32 == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (n32.a(serviceConnection)) {
                n32.b(serviceConnection, str);
                if (n32.e()) {
                    this.e.sendMessageDelayed(this.e.obtainMessage(0, aVar), this.g);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.c) {
                k12.a aVar = (k12.a) message.obj;
                n32 n32 = this.c.get(aVar);
                if (n32 != null && n32.e()) {
                    if (n32.d()) {
                        n32.b("GmsClientSupervisor");
                    }
                    this.c.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.c) {
                k12.a aVar2 = (k12.a) message.obj;
                n32 n322 = this.c.get(aVar2);
                if (n322 != null && n322.c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = n322.b();
                    if (b == null) {
                        b = aVar2.a();
                    }
                    if (b == null) {
                        b = new ComponentName(aVar2.b(), "unknown");
                    }
                    n322.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
