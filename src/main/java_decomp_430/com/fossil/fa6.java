package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fa6 implements da6 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public fa6(long j, int i) {
        this.a = j;
        this.b = i;
    }

    @DexIgnore
    public long a(int i) {
        return (long) (((double) this.a) * Math.pow((double) this.b, (double) i));
    }
}
