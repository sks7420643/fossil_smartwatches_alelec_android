package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if0 implements Parcelable.Creator<zg0> {
    @DexIgnore
    public /* synthetic */ if0(qg6 qg6) {
    }

    @DexIgnore
    public zg0 createFromParcel(Parcel parcel) {
        return new zg0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new zg0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m26createFromParcel(Parcel parcel) {
        return new zg0(parcel, (qg6) null);
    }
}
