package com.fossil;

import android.app.Activity;
import android.view.View;
import com.fossil.xx5;
import com.portfolio.platform.service.ShakeFeedbackService;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up4$b$a implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService.b a;

    @DexIgnore
    public up4$b$a(ShakeFeedbackService.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void onClick(View view) {
        this.a.a.f = 1;
        xx5.a aVar = xx5.a;
        WeakReference b = this.a.a.a;
        if (b != null) {
            Object obj = b.get();
            if (obj == null) {
                throw new rc6("null cannot be cast to non-null type android.app.Activity");
            } else if (aVar.c((Activity) obj, 123)) {
                this.a.a.d();
                qg3 a2 = this.a.a.c;
                if (a2 != null) {
                    a2.dismiss();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
