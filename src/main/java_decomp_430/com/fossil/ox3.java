package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ox3 {
    @DexIgnore
    public static /* final */ a a; // = (d.a() ? new d() : new b());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract int a(int i, byte[] bArr, int i2, int i3);

        @DexIgnore
        public abstract int a(CharSequence charSequence, byte[] bArr, int i, int i2);

        @DexIgnore
        public final boolean a(byte[] bArr, int i, int i2) {
            return a(0, bArr, i, i2) == 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends IllegalArgumentException {
        @DexIgnore
        public c(int i, int i2) {
            super("Unpaired surrogate at index " + i + " of " + i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends a {
        @DexIgnore
        public static boolean a() {
            return nx3.d() && nx3.e();
        }

        @DexIgnore
        public static int b(byte[] bArr, long j, int i) {
            if (i < 16) {
                return 0;
            }
            int i2 = ((int) j) & 7;
            long j2 = j;
            int i3 = i2;
            while (i3 > 0) {
                long j3 = 1 + j2;
                if (nx3.a(bArr, j2) < 0) {
                    return i2 - i3;
                }
                i3--;
                j2 = j3;
            }
            int i4 = i - i2;
            while (i4 >= 8 && (nx3.b(bArr, j2) & -9187201950435737472L) == 0) {
                j2 += 8;
                i4 -= 8;
            }
            return i - i4;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x002f, code lost:
            if (com.fossil.nx3.a(r13, r2) > -65) goto L_0x0031;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0060, code lost:
            if (com.fossil.nx3.a(r13, r2) > -65) goto L_0x0062;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a2, code lost:
            if (com.fossil.nx3.a(r13, r2) > -65) goto L_0x00a4;
         */
        @DexIgnore
        public int a(int i, byte[] bArr, int i2, int i3) {
            long j;
            byte b = 0;
            if ((i2 | i3 | (bArr.length - i3)) >= 0) {
                long b2 = nx3.b() + ((long) i2);
                long b3 = nx3.b() + ((long) i3);
                if (i == 0) {
                    j = b2;
                } else if (b2 >= b3) {
                    return i;
                } else {
                    byte b4 = (byte) i;
                    if (b4 < -32) {
                        if (b4 >= -62) {
                            j = b2 + 1;
                        }
                        return -1;
                    } else if (b4 < -16) {
                        byte b5 = (byte) (~(i >> 8));
                        if (b5 == 0) {
                            long j2 = b2 + 1;
                            b5 = nx3.a(bArr, b2);
                            if (j2 >= b3) {
                                return ox3.b(b4, b5);
                            }
                            b2 = j2;
                        }
                        if (b5 <= -65 && ((b4 != -32 || b5 >= -96) && (b4 != -19 || b5 < -96))) {
                            j = b2 + 1;
                        }
                        return -1;
                    } else {
                        byte b6 = (byte) (~(i >> 8));
                        if (b6 == 0) {
                            long j3 = b2 + 1;
                            b6 = nx3.a(bArr, b2);
                            if (j3 >= b3) {
                                return ox3.b(b4, b6);
                            }
                            b2 = j3;
                        } else {
                            b = (byte) (i >> 16);
                        }
                        if (b == 0) {
                            long j4 = b2 + 1;
                            b = nx3.a(bArr, b2);
                            if (j4 >= b3) {
                                return ox3.b((int) b4, (int) b6, (int) b);
                            }
                            b2 = j4;
                        }
                        if (b6 <= -65 && (((b4 << 28) + (b6 + 112)) >> 30) == 0 && b <= -65) {
                            j = b2 + 1;
                        }
                        return -1;
                    }
                }
                return a(bArr, j, (int) (b3 - j));
            }
            throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
        }

        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x003d A[LOOP:1: B:14:0x003d->B:39:0x0105, LOOP_START, PHI: r2 r3 r4 r11 
  PHI: (r2v3 int) = (r2v2 int), (r2v5 int) binds: [B:10:0x0034, B:39:0x0105] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r3v2 char) = (r3v1 char), (r3v3 char) binds: [B:10:0x0034, B:39:0x0105] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r4v4 long) = (r4v3 long), (r4v6 long) binds: [B:10:0x0034, B:39:0x0105] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r11v3 long) = (r11v2 long), (r11v4 long) binds: [B:10:0x0034, B:39:0x0105] A[DONT_GENERATE, DONT_INLINE]] */
        @DexIgnore
        public int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
            long b;
            long j;
            long j2;
            int i3;
            char charAt;
            CharSequence charSequence2 = charSequence;
            byte[] bArr2 = bArr;
            int i4 = i;
            int i5 = i2;
            long b2 = nx3.b() + ((long) i4);
            long j3 = ((long) i5) + b2;
            int length = charSequence.length();
            if (length > i5 || bArr2.length - i5 < i4) {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charSequence2.charAt(length - 1) + " at index " + (i4 + i5));
            }
            int i6 = 0;
            while (true) {
                char c = 128;
                long j4 = 1;
                if (i6 < length && (charAt = charSequence2.charAt(i6)) < 128) {
                    nx3.a(bArr2, b2, (byte) charAt);
                    i6++;
                    b2 = 1 + b2;
                } else if (i6 != length) {
                    b = nx3.b();
                } else {
                    while (i6 < length) {
                        char charAt2 = charSequence2.charAt(i6);
                        if (charAt2 < c && b2 < j3) {
                            long j5 = b2 + j4;
                            nx3.a(bArr2, b2, (byte) charAt2);
                            j2 = j4;
                            j = j5;
                        } else if (charAt2 < 2048 && b2 <= j3 - 2) {
                            long j6 = b2 + j4;
                            nx3.a(bArr2, b2, (byte) ((charAt2 >>> 6) | 960));
                            nx3.a(bArr2, j6, (byte) ((charAt2 & '?') | 128));
                            j = j6 + j4;
                            j2 = j4;
                            i6++;
                            c = 128;
                            long j7 = j2;
                            b2 = j;
                            j4 = j7;
                        } else if ((charAt2 < 55296 || 57343 < charAt2) && b2 <= j3 - 3) {
                            long j8 = b2 + j4;
                            nx3.a(bArr2, b2, (byte) ((charAt2 >>> 12) | 480));
                            long j9 = j8 + j4;
                            nx3.a(bArr2, j8, (byte) (((charAt2 >>> 6) & 63) | 128));
                            nx3.a(bArr2, j9, (byte) ((charAt2 & '?') | 128));
                            j = j9 + 1;
                            j2 = 1;
                        } else if (b2 <= j3 - 4) {
                            int i7 = i6 + 1;
                            if (i7 != length) {
                                char charAt3 = charSequence2.charAt(i7);
                                if (Character.isSurrogatePair(charAt2, charAt3)) {
                                    int codePoint = Character.toCodePoint(charAt2, charAt3);
                                    long j10 = b2 + 1;
                                    nx3.a(bArr2, b2, (byte) ((codePoint >>> 18) | 240));
                                    long j11 = j10 + 1;
                                    nx3.a(bArr2, j10, (byte) (((codePoint >>> 12) & 63) | 128));
                                    long j12 = j11 + 1;
                                    nx3.a(bArr2, j11, (byte) (((codePoint >>> 6) & 63) | 128));
                                    j2 = 1;
                                    j = j12 + 1;
                                    nx3.a(bArr2, j12, (byte) ((codePoint & 63) | 128));
                                    i6 = i7;
                                    i6++;
                                    c = 128;
                                    long j72 = j2;
                                    b2 = j;
                                    j4 = j72;
                                } else {
                                    i6 = i7;
                                }
                            }
                            throw new c(i6 - 1, length);
                        } else if (55296 > charAt2 || charAt2 > 57343 || ((i3 = i6 + 1) != length && Character.isSurrogatePair(charAt2, charSequence2.charAt(i3)))) {
                            throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + b2);
                        } else {
                            throw new c(i6, length);
                        }
                        i6++;
                        c = 128;
                        long j722 = j2;
                        b2 = j;
                        j4 = j722;
                    }
                    b = nx3.b();
                }
            }
            if (i6 != length) {
            }
            return (int) (b2 - b);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
            return -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0063, code lost:
            return -1;
         */
        @DexIgnore
        public static int a(byte[] bArr, long j, int i) {
            long j2;
            int b = b(bArr, j, i);
            int i2 = i - b;
            long j3 = j + ((long) b);
            while (true) {
                byte b2 = 0;
                while (true) {
                    if (i2 <= 0) {
                        break;
                    }
                    long j4 = j3 + 1;
                    b2 = nx3.a(bArr, j3);
                    if (b2 < 0) {
                        j3 = j4;
                        break;
                    }
                    i2--;
                    j3 = j4;
                }
                if (i2 == 0) {
                    return 0;
                }
                int i3 = i2 - 1;
                if (b2 >= -32) {
                    if (b2 >= -16) {
                        if (i3 >= 3) {
                            i2 = i3 - 3;
                            long j5 = j3 + 1;
                            byte a = nx3.a(bArr, j3);
                            if (a > -65 || (((b2 << 28) + (a + 112)) >> 30) != 0) {
                                break;
                            }
                            long j6 = j5 + 1;
                            if (nx3.a(bArr, j5) > -65) {
                                break;
                            }
                            j2 = 1 + j6;
                            if (nx3.a(bArr, j6) > -65) {
                                break;
                            }
                        } else {
                            return a(bArr, (int) b2, j3, i3);
                        }
                    } else if (i3 >= 2) {
                        i2 = i3 - 2;
                        long j7 = j3 + 1;
                        byte a2 = nx3.a(bArr, j3);
                        if (a2 > -65 || ((b2 == -32 && a2 < -96) || (b2 == -19 && a2 >= -96))) {
                            break;
                        }
                        j2 = 1 + j7;
                        if (nx3.a(bArr, j7) > -65) {
                            break;
                        }
                    } else {
                        return a(bArr, (int) b2, j3, i3);
                    }
                } else if (i3 != 0) {
                    i2 = i3 - 1;
                    if (b2 < -62) {
                        break;
                    }
                    j2 = 1 + j3;
                    if (nx3.a(bArr, j3) > -65) {
                        break;
                    }
                } else {
                    return b2;
                }
                j3 = j2;
            }
            return -1;
        }

        @DexIgnore
        public static int a(byte[] bArr, int i, long j, int i2) {
            if (i2 == 0) {
                return ox3.b(i);
            }
            if (i2 == 1) {
                return ox3.b(i, nx3.a(bArr, j));
            }
            if (i2 == 2) {
                return ox3.b(i, (int) nx3.a(bArr, j), (int) nx3.a(bArr, j + 1));
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public static int b(int i) {
        if (i > -12) {
            return -1;
        }
        return i;
    }

    @DexIgnore
    public static int b(int i, int i2) {
        if (i > -12 || i2 > -65) {
            return -1;
        }
        return i ^ (i2 << 8);
    }

    @DexIgnore
    public static int b(int i, int i2, int i3) {
        if (i > -12 || i2 > -65 || i3 > -65) {
            return -1;
        }
        return (i ^ (i2 << 8)) ^ (i3 << 16);
    }

    @DexIgnore
    public static int b(byte[] bArr, int i, int i2) {
        byte b2 = bArr[i - 1];
        int i3 = i2 - i;
        if (i3 == 0) {
            return b(b2);
        }
        if (i3 == 1) {
            return b(b2, bArr[i]);
        }
        if (i3 == 2) {
            return b((int) b2, (int) bArr[i], (int) bArr[i + 1]);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static boolean c(byte[] bArr, int i, int i2) {
        return a.a(bArr, i, i2);
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3) {
        return a.a(i, bArr, i2, i3);
    }

    @DexIgnore
    public static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) < 128) {
            i++;
        }
        int i2 = length;
        while (true) {
            if (i < length) {
                char charAt = charSequence.charAt(i);
                if (charAt >= 2048) {
                    i2 += a(charSequence, i);
                    break;
                }
                i2 += (127 - charAt) >>> 31;
                i++;
            } else {
                break;
            }
        }
        if (i2 >= length) {
            return i2;
        }
        throw new IllegalArgumentException("UTF-8 length does not fit in int: " + (((long) i2) + 4294967296L));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a {
        @DexIgnore
        public static int b(byte[] bArr, int i, int i2) {
            while (i < i2 && bArr[i] >= 0) {
                i++;
            }
            if (i >= i2) {
                return 0;
            }
            return c(bArr, i, i2);
        }

        @DexIgnore
        public static int c(byte[] bArr, int i, int i2) {
            while (i < i2) {
                int i3 = i + 1;
                byte b = bArr[i];
                if (b < 0) {
                    if (b < -32) {
                        if (i3 >= i2) {
                            return b;
                        }
                        if (b >= -62) {
                            i = i3 + 1;
                            if (bArr[i3] > -65) {
                            }
                        }
                        return -1;
                    } else if (b < -16) {
                        if (i3 >= i2 - 1) {
                            return ox3.b(bArr, i3, i2);
                        }
                        int i4 = i3 + 1;
                        byte b2 = bArr[i3];
                        if (b2 <= -65 && ((b != -32 || b2 >= -96) && (b != -19 || b2 < -96))) {
                            i = i4 + 1;
                            if (bArr[i4] > -65) {
                            }
                        }
                        return -1;
                    } else if (i3 >= i2 - 2) {
                        return ox3.b(bArr, i3, i2);
                    } else {
                        int i5 = i3 + 1;
                        byte b3 = bArr[i3];
                        if (b3 <= -65 && (((b << 28) + (b3 + 112)) >> 30) == 0) {
                            int i6 = i5 + 1;
                            if (bArr[i5] <= -65) {
                                i3 = i6 + 1;
                                if (bArr[i6] > -65) {
                                }
                            }
                        }
                        return -1;
                    }
                }
                i = i3;
            }
            return 0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0042, code lost:
            if (r8[r9] > -65) goto L_0x0044;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x007a, code lost:
            if (r8[r7] > -65) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
            if (r8[r9] > -65) goto L_0x0017;
         */
        @DexIgnore
        public int a(int i, byte[] bArr, int i2, int i3) {
            int i4;
            int i5;
            if (i != 0) {
                if (i2 >= i3) {
                    return i;
                }
                byte b = (byte) i;
                if (b < -32) {
                    if (b >= -62) {
                        i4 = i2 + 1;
                    }
                    return -1;
                } else if (b < -16) {
                    byte b2 = (byte) (~(i >> 8));
                    if (b2 == 0) {
                        int i6 = i2 + 1;
                        byte b3 = bArr[i2];
                        if (i6 >= i3) {
                            return ox3.b(b, b3);
                        }
                        byte b4 = b3;
                        i2 = i6;
                        b2 = b4;
                    }
                    if (b2 <= -65 && ((b != -32 || b2 >= -96) && (b != -19 || b2 < -96))) {
                        i4 = i2 + 1;
                    }
                    return -1;
                } else {
                    byte b5 = (byte) (~(i >> 8));
                    byte b6 = 0;
                    if (b5 == 0) {
                        i5 = i2 + 1;
                        b5 = bArr[i2];
                        if (i5 >= i3) {
                            return ox3.b(b, b5);
                        }
                    } else {
                        b6 = (byte) (i >> 16);
                        i5 = i2;
                    }
                    if (b6 == 0) {
                        int i7 = i5 + 1;
                        b6 = bArr[i5];
                        if (i7 >= i3) {
                            return ox3.b((int) b, (int) b5, (int) b6);
                        }
                        i5 = i7;
                    }
                    if (b5 <= -65 && (((b << 28) + (b5 + 112)) >> 30) == 0 && b6 <= -65) {
                        i2 = i5 + 1;
                    }
                    return -1;
                }
                return b(bArr, i4, i3);
            }
            i4 = i2;
            return b(bArr, i4, i3);
        }

        @DexIgnore
        public int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
            int i3;
            int i4;
            int i5;
            char charAt;
            int length = charSequence.length();
            int i6 = i2 + i;
            int i7 = 0;
            while (i7 < length && (i5 = i7 + i) < i6 && (charAt = charSequence.charAt(i7)) < 128) {
                bArr[i5] = (byte) charAt;
                i7++;
            }
            if (i7 == length) {
                return i + length;
            }
            int i8 = i + i7;
            while (i7 < length) {
                char charAt2 = charSequence.charAt(i7);
                if (charAt2 < 128 && i8 < i6) {
                    i4 = i8 + 1;
                    bArr[i8] = (byte) charAt2;
                } else if (charAt2 < 2048 && i8 <= i6 - 2) {
                    int i9 = i8 + 1;
                    bArr[i8] = (byte) ((charAt2 >>> 6) | 960);
                    i8 = i9 + 1;
                    bArr[i9] = (byte) ((charAt2 & '?') | 128);
                    i7++;
                } else if ((charAt2 < 55296 || 57343 < charAt2) && i8 <= i6 - 3) {
                    int i10 = i8 + 1;
                    bArr[i8] = (byte) ((charAt2 >>> 12) | 480);
                    int i11 = i10 + 1;
                    bArr[i10] = (byte) (((charAt2 >>> 6) & 63) | 128);
                    i4 = i11 + 1;
                    bArr[i11] = (byte) ((charAt2 & '?') | 128);
                } else if (i8 <= i6 - 4) {
                    int i12 = i7 + 1;
                    if (i12 != charSequence.length()) {
                        char charAt3 = charSequence.charAt(i12);
                        if (Character.isSurrogatePair(charAt2, charAt3)) {
                            int codePoint = Character.toCodePoint(charAt2, charAt3);
                            int i13 = i8 + 1;
                            bArr[i8] = (byte) ((codePoint >>> 18) | 240);
                            int i14 = i13 + 1;
                            bArr[i13] = (byte) (((codePoint >>> 12) & 63) | 128);
                            int i15 = i14 + 1;
                            bArr[i14] = (byte) (((codePoint >>> 6) & 63) | 128);
                            i8 = i15 + 1;
                            bArr[i15] = (byte) ((codePoint & 63) | 128);
                            i7 = i12;
                            i7++;
                        } else {
                            i7 = i12;
                        }
                    }
                    throw new c(i7 - 1, length);
                } else if (55296 > charAt2 || charAt2 > 57343 || ((i3 = i7 + 1) != charSequence.length() && Character.isSurrogatePair(charAt2, charSequence.charAt(i3)))) {
                    throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + i8);
                } else {
                    throw new c(i7, length);
                }
                i8 = i4;
                i7++;
            }
            return i8;
        }
    }

    @DexIgnore
    public static int a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        int i2 = 0;
        while (i < length) {
            char charAt = charSequence.charAt(i);
            if (charAt < 2048) {
                i2 += (127 - charAt) >>> 31;
            } else {
                i2 += 2;
                if (55296 <= charAt && charAt <= 57343) {
                    if (Character.codePointAt(charSequence, i) >= 65536) {
                        i++;
                    } else {
                        throw new c(i, length);
                    }
                }
            }
            i++;
        }
        return i2;
    }

    @DexIgnore
    public static int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        return a.a(charSequence, bArr, i, i2);
    }
}
