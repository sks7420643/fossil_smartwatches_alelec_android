package com.fossil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m30 {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ File a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends JSONObject {
        @DexIgnore
        public /* final */ /* synthetic */ h40 a;

        @DexIgnore
        public a(h40 h40) throws JSONException {
            this.a = h40;
            put("userId", this.a.a);
            put("userName", this.a.b);
            put("userEmail", this.a.c);
        }
    }

    @DexIgnore
    public m30(File file) {
        this.a = file;
    }

    @DexIgnore
    public static h40 d(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        return new h40(a(jSONObject, "userId"), a(jSONObject, "userName"), a(jSONObject, "userEmail"));
    }

    @DexIgnore
    public void a(String str, h40 h40) {
        File b2 = b(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a2 = a(h40);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), b));
            try {
                bufferedWriter2.write(a2);
                bufferedWriter2.flush();
                z86.a(bufferedWriter2, "Failed to close user metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    c86.g().e("CrashlyticsCore", "Error serializing user metadata.", e);
                    z86.a(bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    z86.a(bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                z86.a(bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            c86.g().e("CrashlyticsCore", "Error serializing user metadata.", e);
            z86.a(bufferedWriter, "Failed to close user metadata file.");
        }
    }

    @DexIgnore
    public File b(String str) {
        File file = this.a;
        return new File(file, str + "user" + ".meta");
    }

    @DexIgnore
    public h40 c(String str) {
        File b2 = b(str);
        if (!b2.exists()) {
            return h40.d;
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(b2);
            try {
                h40 d = d(z86.b(fileInputStream2));
                z86.a(fileInputStream2, "Failed to close user metadata file.");
                return d;
            } catch (Exception e) {
                e = e;
                fileInputStream = fileInputStream2;
                try {
                    c86.g().e("CrashlyticsCore", "Error deserializing user metadata.", e);
                    z86.a(fileInputStream, "Failed to close user metadata file.");
                    return h40.d;
                } catch (Throwable th) {
                    th = th;
                    z86.a(fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                z86.a(fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            c86.g().e("CrashlyticsCore", "Error deserializing user metadata.", e);
            z86.a(fileInputStream, "Failed to close user metadata file.");
            return h40.d;
        }
    }

    @DexIgnore
    public void a(String str, Map<String, String> map) {
        File a2 = a(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a3 = a(map);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a2), b));
            try {
                bufferedWriter2.write(a3);
                bufferedWriter2.flush();
                z86.a(bufferedWriter2, "Failed to close key/value metadata file.");
            } catch (Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    c86.g().e("CrashlyticsCore", "Error serializing key/value metadata.", e);
                    z86.a(bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    z86.a(bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                z86.a(bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            c86.g().e("CrashlyticsCore", "Error serializing key/value metadata.", e);
            z86.a(bufferedWriter, "Failed to close key/value metadata file.");
        }
    }

    @DexIgnore
    public File a(String str) {
        File file = this.a;
        return new File(file, str + "keys" + ".meta");
    }

    @DexIgnore
    public static String a(h40 h40) throws JSONException {
        return new a(h40).toString();
    }

    @DexIgnore
    public static String a(Map<String, String> map) throws JSONException {
        return new JSONObject(map).toString();
    }

    @DexIgnore
    public static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, (String) null);
        }
        return null;
    }
}
