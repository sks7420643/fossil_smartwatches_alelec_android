package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w95 implements Factory<v95> {
    @DexIgnore
    public static MicroAppPresenter a(t95 t95, CategoryRepository categoryRepository) {
        return new MicroAppPresenter(t95, categoryRepository);
    }
}
