package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ly3 {
    @DexIgnore
    public /* final */ ky3 a;
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore
    public ly3(ky3 ky3, int[] iArr) {
        if (iArr.length != 0) {
            this.a = ky3;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.b = new int[]{0};
                return;
            }
            this.b = new int[(length - i)];
            int[] iArr2 = this.b;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int[] a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.b.length - 1;
    }

    @DexIgnore
    public boolean c() {
        return this.b[0] == 0;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(b() * 8);
        for (int b2 = b(); b2 >= 0; b2--) {
            int a2 = a(b2);
            if (a2 != 0) {
                if (a2 < 0) {
                    sb.append(" - ");
                    a2 = -a2;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (b2 == 0 || a2 != 1) {
                    int c = this.a.c(a2);
                    if (c == 0) {
                        sb.append('1');
                    } else if (c == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(c);
                    }
                }
                if (b2 != 0) {
                    if (b2 == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(b2);
                    }
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public int a(int i) {
        int[] iArr = this.b;
        return iArr[(iArr.length - 1) - i];
    }

    @DexIgnore
    public ly3[] b(ly3 ly3) {
        if (!this.a.equals(ly3.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!ly3.c()) {
            ly3 b2 = this.a.b();
            int b3 = this.a.b(ly3.a(ly3.b()));
            ly3 ly32 = b2;
            ly3 ly33 = this;
            while (ly33.b() >= ly3.b() && !ly33.c()) {
                int b4 = ly33.b() - ly3.b();
                int b5 = this.a.b(ly33.a(ly33.b()), b3);
                ly3 a2 = ly3.a(b4, b5);
                ly32 = ly32.a(this.a.a(b4, b5));
                ly33 = ly33.a(a2);
            }
            return new ly3[]{ly32, ly33};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    @DexIgnore
    public ly3 c(ly3 ly3) {
        if (!this.a.equals(ly3.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c() || ly3.c()) {
            return this.a.b();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = ly3.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = ky3.c(iArr3[i4], this.a.b(i2, iArr2[i3]));
                }
            }
            return new ly3(this.a, iArr3);
        }
    }

    @DexIgnore
    public ly3 a(ly3 ly3) {
        if (!this.a.equals(ly3.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c()) {
            return ly3;
        } else {
            if (ly3.c()) {
                return this;
            }
            int[] iArr = this.b;
            int[] iArr2 = ly3.b;
            if (iArr.length > iArr2.length) {
                int[] iArr3 = iArr;
                iArr = iArr2;
                iArr2 = iArr3;
            }
            int[] iArr4 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr4, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr4[i] = ky3.c(iArr[i - length], iArr2[i]);
            }
            return new ly3(this.a, iArr4);
        }
    }

    @DexIgnore
    public ly3 a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.a.b();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.a.b(this.b[i3], i2);
            }
            return new ly3(this.a, iArr);
        }
    }
}
