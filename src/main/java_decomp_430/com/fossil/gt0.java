package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gt0 {
    GET((byte) 0),
    PUT((byte) 1);

    @DexIgnore
    public gt0(byte b) {
    }
}
