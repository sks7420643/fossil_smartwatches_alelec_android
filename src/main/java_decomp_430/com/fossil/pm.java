package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import com.fossil.sm;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pm implements em {
    @DexIgnore
    public static /* final */ String d; // = tl.a("CommandHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Map<String, em> b; // = new HashMap();
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public pm(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent c(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public final void d(Intent intent, int i, sm smVar) {
        tl.a().a(d, String.format("Handling reschedule %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        smVar.e().j();
    }

    @DexIgnore
    public final void e(Intent intent, int i, sm smVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        tl.a().a(d, String.format("Handling schedule work for %s", new Object[]{string}), new Throwable[0]);
        WorkDatabase g = smVar.e().g();
        g.beginTransaction();
        try {
            zn e = g.d().e(string);
            if (e == null) {
                tl a2 = tl.a();
                String str = d;
                a2.e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (e.b.isFinished()) {
                tl a3 = tl.a();
                String str2 = d;
                a3.e(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                g.endTransaction();
            } else {
                long a4 = e.a();
                if (!e.b()) {
                    tl.a().a(d, String.format("Setting up Alarms for %s at %s", new Object[]{string, Long.valueOf(a4)}), new Throwable[0]);
                    om.a(this.a, smVar.e(), string, a4);
                } else {
                    tl.a().a(d, String.format("Opportunistically setting an alarm for %s at %s", new Object[]{string, Long.valueOf(a4)}), new Throwable[0]);
                    om.a(this.a, smVar.e(), string, a4);
                    smVar.a((Runnable) new sm.b(smVar, a(this.a), i));
                }
                g.setTransactionSuccessful();
                g.endTransaction();
            }
        } finally {
            g.endTransaction();
        }
    }

    @DexIgnore
    public final void f(Intent intent, int i, sm smVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        tl.a().a(d, String.format("Handing stopWork work for %s", new Object[]{string}), new Throwable[0]);
        smVar.e().b(string);
        om.a(this.a, smVar.e(), string);
        smVar.a(string, false);
    }

    @DexIgnore
    public void g(Intent intent, int i, sm smVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            a(intent, i, smVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            d(intent, i, smVar);
        } else if (!a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            tl.a().b(d, String.format("Invalid request for %s, requires %s.", new Object[]{action, "KEY_WORKSPEC_ID"}), new Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            e(intent, i, smVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            b(intent, i, smVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            f(intent, i, smVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            c(intent, i, smVar);
        } else {
            tl.a().e(d, String.format("Ignoring intent %s", new Object[]{intent}), new Throwable[0]);
        }
    }

    @DexIgnore
    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    public final void c(Intent intent, int i, sm smVar) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        tl.a().a(d, String.format("Handling onExecutionCompleted %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        a(string, z);
    }

    @DexIgnore
    public static Intent a(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    public final void b(Intent intent, int i, sm smVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.c) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            tl.a().a(d, String.format("Handing delay met for %s", new Object[]{string}), new Throwable[0]);
            if (!this.b.containsKey(string)) {
                rm rmVar = new rm(this.a, i, string, smVar);
                this.b.put(string, rmVar);
                rmVar.b();
            } else {
                tl.a().a(d, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", new Object[]{string}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.c) {
            em remove = this.b.remove(str);
            if (remove != null) {
                remove.a(str, z);
            }
        }
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.c) {
            z = !this.b.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public final void a(Intent intent, int i, sm smVar) {
        tl.a().a(d, String.format("Handling constraints changed %s", new Object[]{intent}), new Throwable[0]);
        new qm(this.a, i, smVar).a();
    }

    @DexIgnore
    public static boolean a(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
