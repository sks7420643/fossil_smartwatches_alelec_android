package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.fossil.a20;
import com.fossil.i10;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y10 implements i10.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ c10 b;
    @DexIgnore
    public /* final */ a86 c;
    @DexIgnore
    public /* final */ i10 d;
    @DexIgnore
    public /* final */ f10 e;

    @DexIgnore
    public y10(c10 c10, a86 a86, i10 i10, f10 f10, long j) {
        this.b = c10;
        this.c = a86;
        this.d = i10;
        this.e = f10;
        this.a = j;
    }

    @DexIgnore
    public static y10 a(i86 i86, Context context, j96 j96, String str, String str2, long j) {
        Context context2 = context;
        j96 j962 = j96;
        d20 d20 = new d20(context, j96, str, str2);
        d10 d10 = new d10(context, new bb6(i86));
        sa6 sa6 = new sa6(c86.g());
        a86 a86 = new a86(context);
        ScheduledExecutorService b2 = f96.b("Answers Events Handler");
        i10 i10 = new i10(b2);
        return new y10(new c10(i86, context, d10, d20, sa6, b2, new o10(context)), a86, i10, f10.a(context), j);
    }

    @DexIgnore
    public void b() {
        this.c.a();
        this.b.a();
    }

    @DexIgnore
    public void c() {
        this.b.b();
        this.c.a(new e10(this, this.d));
        this.d.a((i10.b) this);
        if (d()) {
            a(this.a);
            this.e.b();
        }
    }

    @DexIgnore
    public boolean d() {
        return !this.e.a();
    }

    @DexIgnore
    public void a(j10 j10) {
        l86 g = c86.g();
        g.d("Answers", "Logged custom event: " + j10);
        this.b.a(a20.a(j10));
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            c86.g().d("Answers", "Logged crash");
            this.b.c(a20.a(str, str2));
            return;
        }
        throw new IllegalStateException("onCrash called from main thread!!!");
    }

    @DexIgnore
    public void a(long j) {
        c86.g().d("Answers", "Logged install");
        this.b.b(a20.a(j));
    }

    @DexIgnore
    public void a(Activity activity, a20.c cVar) {
        l86 g = c86.g();
        g.d("Answers", "Logged lifecycle event: " + cVar.name());
        this.b.a(a20.a(cVar, activity));
    }

    @DexIgnore
    public void a() {
        c86.g().d("Answers", "Flush events when app is backgrounded");
        this.b.c();
    }

    @DexIgnore
    public void a(fb6 fb6, String str) {
        this.d.a(fb6.i);
        this.b.a(fb6, str);
    }
}
