package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu1 implements Parcelable.Creator<GoogleSignInOptions> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        ArrayList<Scope> arrayList = null;
        Account account = null;
        String str = null;
        String str2 = null;
        ArrayList<tt1> arrayList2 = null;
        String str3 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel, a);
                    break;
                case 2:
                    arrayList = f22.c(parcel, a, Scope.CREATOR);
                    break;
                case 3:
                    account = f22.a(parcel, a, Account.CREATOR);
                    break;
                case 4:
                    z = f22.i(parcel, a);
                    break;
                case 5:
                    z2 = f22.i(parcel, a);
                    break;
                case 6:
                    z3 = f22.i(parcel, a);
                    break;
                case 7:
                    str = f22.e(parcel, a);
                    break;
                case 8:
                    str2 = f22.e(parcel, a);
                    break;
                case 9:
                    arrayList2 = f22.c(parcel, a, tt1.CREATOR);
                    break;
                case 10:
                    str3 = f22.e(parcel, a);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new GoogleSignInOptions(i, arrayList, account, z, z2, z3, str, str2, arrayList2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
