package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$1$1", f = "CaloriesOverviewMonthPresenter.kt", l = {67}, m = "invokeSuspend")
public final class rd5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $data;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewMonthPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$1$1$1", f = "CaloriesOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super TreeMap<Long, Float>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ rd5$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rd5$d$a rd5_d_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = rd5_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter = this.this$0.this$0.a;
                Object a = caloriesOverviewMonthPresenter.f.a();
                if (a != null) {
                    wg6.a(a, "mDate.value!!");
                    return caloriesOverviewMonthPresenter.a((Date) a, (List<ActivitySummary>) this.this$0.$data);
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rd5$d$a(CaloriesOverviewMonthPresenter.d dVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$data = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        rd5$d$a rd5_d_a = new rd5$d$a(this.this$0, this.$data, xe6);
        rd5_d_a.p$ = (il6) obj;
        return rd5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((rd5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            this.this$0.a.l = this.$data;
            CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter2 = this.this$0.a;
            dl6 a3 = caloriesOverviewMonthPresenter2.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = caloriesOverviewMonthPresenter2;
            this.label = 1;
            obj = gk6.a(a3, aVar, this);
            if (obj == a2) {
                return a2;
            }
            caloriesOverviewMonthPresenter = caloriesOverviewMonthPresenter2;
        } else if (i == 1) {
            caloriesOverviewMonthPresenter = (CaloriesOverviewMonthPresenter) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        caloriesOverviewMonthPresenter.n = (TreeMap) obj;
        qd5 l = this.this$0.a.o;
        TreeMap d = this.this$0.a.n;
        if (d == null) {
            d = new TreeMap();
        }
        l.a(d);
        return cd6.a;
    }
}
