package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r63 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ d63 e;

    @DexIgnore
    public r63(d63 d63, String str, String str2, String str3, long j) {
        this.e = d63;
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
    }

    @DexIgnore
    public final void run() {
        String str = this.a;
        if (str == null) {
            this.e.a.v().E().a(this.b, (h83) null);
            return;
        }
        this.e.a.v().E().a(this.b, new h83(this.c, str, this.d));
    }
}
