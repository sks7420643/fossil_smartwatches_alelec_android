package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gg4 extends fg4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362602, 1);
        x.put(2131362442, 2);
        x.put(2131362312, 3);
        x.put(2131362575, 4);
        x.put(2131362653, 5);
    }
    */

    @DexIgnore
    public gg4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public gg4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[3], objArr[2], objArr[4], objArr[1], objArr[5]);
        this.v = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
