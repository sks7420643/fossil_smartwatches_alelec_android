package com.fossil;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d00 implements vr {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ vr c;

    @DexIgnore
    public d00(int i, vr vrVar) {
        this.b = i;
        this.c = vrVar;
    }

    @DexIgnore
    public static vr a(Context context) {
        return new d00(context.getResources().getConfiguration().uiMode & 48, e00.b(context));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof d00)) {
            return false;
        }
        d00 d00 = (d00) obj;
        if (this.b != d00.b || !this.c.equals(d00.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return r00.a((Object) this.c, this.b);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }
}
