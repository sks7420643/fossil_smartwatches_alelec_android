package com.fossil;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.fossil.pw6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uw6 extends DialogFragment {
    @DexIgnore
    public pw6.a a;
    @DexIgnore
    public pw6.b b;
    @DexIgnore
    public boolean c; // = false;

    @DexIgnore
    public static uw6 a(String str, String str2, String str3, int i, int i2, String[] strArr) {
        uw6 uw6 = new uw6();
        uw6.setArguments(new tw6(str, str2, str3, i, i2, strArr).a());
        return uw6;
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (Build.VERSION.SDK_INT >= 17 && getParentFragment() != null) {
            if (getParentFragment() instanceof pw6.a) {
                this.a = (pw6.a) getParentFragment();
            }
            if (getParentFragment() instanceof pw6.b) {
                this.b = (pw6.b) getParentFragment();
            }
        }
        if (context instanceof pw6.a) {
            this.a = (pw6.a) context;
        }
        if (context instanceof pw6.b) {
            this.b = (pw6.b) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        setCancelable(false);
        tw6 tw6 = new tw6(getArguments());
        return tw6.a(getActivity(), new sw6(this, tw6, this.a, this.b));
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.a = null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        this.c = true;
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void a(FragmentManager fragmentManager, String str) {
        if ((Build.VERSION.SDK_INT < 26 || !fragmentManager.isStateSaved()) && !this.c) {
            show(fragmentManager, str);
        }
    }
}
