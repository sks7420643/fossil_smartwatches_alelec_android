package com.fossil;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r65 extends k24<q65> {
    @DexIgnore
    void A(boolean z);

    @DexIgnore
    void J0();

    @DexIgnore
    void M0();

    @DexIgnore
    void P0();

    @DexIgnore
    void R0();

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, ai4 ai4);

    @DexIgnore
    void a(List<WatchFaceWrapper> list, ai4 ai4);

    @DexIgnore
    void b();

    @DexIgnore
    void b(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    void h0();

    @DexIgnore
    void t0();
}
