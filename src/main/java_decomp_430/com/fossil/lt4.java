package com.fossil;

import android.app.Activity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt4 extends m24<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ hn4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lt4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<wq4> a;

        @DexIgnore
        public b(WeakReference<wq4> weakReference) {
            wg6.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<wq4> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, gv1 gv1) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = lt4.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginFacebookUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public lt4(hn4 hn4) {
        wg6.b(hn4, "mLoginFacebookManager");
        this.d = hn4;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ln4 {
        @DexIgnore
        public /* final */ /* synthetic */ lt4 a;

        @DexIgnore
        public e(lt4 lt4) {
            this.a = lt4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.lt4] */
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lt4.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.lt4] */
        public void a(int i, gv1 gv1, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lt4.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + gv1);
            this.a.a(new c(i, gv1));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.lt4] */
    public Object a(b bVar, xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.get.instance().y()) {
                return a(new c(601, (gv1) null));
            }
            hn4 hn4 = this.d;
            WeakReference<wq4> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                Object obj = a2.get();
                if (obj != null) {
                    wg6.a(obj, "requestValues?.activityContext!!.get()!!");
                    hn4.a((Activity) obj, (ln4) new e(this));
                    return cd6.a;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, (gv1) null);
        }
    }
}
