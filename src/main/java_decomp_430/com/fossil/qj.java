package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qj {
    @DexIgnore
    public static Transition a; // = new AutoTransition();
    @DexIgnore
    public static ThreadLocal<WeakReference<p4<ViewGroup, ArrayList<Transition>>>> b; // = new ThreadLocal<>();
    @DexIgnore
    public static ArrayList<ViewGroup> c; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public Transition a;
        @DexIgnore
        public ViewGroup b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qj$a$a")
        /* renamed from: com.fossil.qj$a$a  reason: collision with other inner class name */
        public class C0041a extends pj {
            @DexIgnore
            public /* final */ /* synthetic */ p4 a;

            @DexIgnore
            public C0041a(p4 p4Var) {
                this.a = p4Var;
            }

            @DexIgnore
            public void c(Transition transition) {
                ((ArrayList) this.a.get(a.this.b)).remove(transition);
                transition.b((Transition.f) this);
            }
        }

        @DexIgnore
        public a(Transition transition, ViewGroup viewGroup) {
            this.a = transition;
            this.b = viewGroup;
        }

        @DexIgnore
        public final void a() {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.b.removeOnAttachStateChangeListener(this);
        }

        @DexIgnore
        public boolean onPreDraw() {
            a();
            if (!qj.c.remove(this.b)) {
                return true;
            }
            p4<ViewGroup, ArrayList<Transition>> a2 = qj.a();
            ArrayList arrayList = a2.get(this.b);
            ArrayList arrayList2 = null;
            if (arrayList == null) {
                arrayList = new ArrayList();
                a2.put(this.b, arrayList);
            } else if (arrayList.size() > 0) {
                arrayList2 = new ArrayList(arrayList);
            }
            arrayList.add(this.a);
            this.a.a((Transition.f) new C0041a(a2));
            this.a.a(this.b, false);
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).e(this.b);
                }
            }
            this.a.a(this.b);
            return true;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            a();
            qj.c.remove(this.b);
            ArrayList arrayList = qj.a().get(this.b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).e(this.b);
                }
            }
            this.a.a(true);
        }
    }

    @DexIgnore
    public static p4<ViewGroup, ArrayList<Transition>> a() {
        p4<ViewGroup, ArrayList<Transition>> p4Var;
        WeakReference weakReference = b.get();
        if (weakReference != null && (p4Var = (p4) weakReference.get()) != null) {
            return p4Var;
        }
        p4<ViewGroup, ArrayList<Transition>> p4Var2 = new p4<>();
        b.set(new WeakReference(p4Var2));
        return p4Var2;
    }

    @DexIgnore
    public static void b(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            a aVar = new a(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, Transition transition) {
        ArrayList arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((Transition) it.next()).c((View) viewGroup);
            }
        }
        if (transition != null) {
            transition.a(viewGroup, true);
        }
        mj a2 = mj.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, Transition transition) {
        if (!c.contains(viewGroup) && x9.E(viewGroup)) {
            c.add(viewGroup);
            if (transition == null) {
                transition = a;
            }
            Transition clone = transition.clone();
            c(viewGroup, clone);
            mj.a(viewGroup, (mj) null);
            b(viewGroup, clone);
        }
    }
}
