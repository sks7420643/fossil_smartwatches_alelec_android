package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.d5;
import com.fossil.i5;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z4 {
    @DexIgnore
    public static int p; // = 1000;
    @DexIgnore
    public static a5 q;
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public HashMap<String, d5> b; // = null;
    @DexIgnore
    public a c;
    @DexIgnore
    public int d; // = 32;
    @DexIgnore
    public int e;
    @DexIgnore
    public w4[] f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean[] h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ x4 l;
    @DexIgnore
    public d5[] m;
    @DexIgnore
    public int n;
    @DexIgnore
    public /* final */ a o;

    @DexIgnore
    public interface a {
        @DexIgnore
        d5 a(z4 z4Var, boolean[] zArr);

        @DexIgnore
        void a(d5 d5Var);

        @DexIgnore
        void a(a aVar);

        @DexIgnore
        void clear();

        @DexIgnore
        d5 getKey();
    }

    @DexIgnore
    public z4() {
        int i2 = this.d;
        this.e = i2;
        this.f = null;
        this.g = false;
        this.h = new boolean[i2];
        this.i = 1;
        this.j = 0;
        this.k = i2;
        this.m = new d5[p];
        this.n = 0;
        w4[] w4VarArr = new w4[i2];
        this.f = new w4[i2];
        h();
        this.l = new x4();
        this.c = new y4(this.l);
        this.o = new w4(this.l);
    }

    @DexIgnore
    public static a5 j() {
        return q;
    }

    @DexIgnore
    public d5 a(Object obj) {
        d5 d5Var = null;
        if (obj == null) {
            return null;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        if (obj instanceof i5) {
            i5 i5Var = (i5) obj;
            d5Var = i5Var.e();
            if (d5Var == null) {
                i5Var.a(this.l);
                d5Var = i5Var.e();
            }
            int i2 = d5Var.b;
            if (i2 == -1 || i2 > this.a || this.l.c[i2] == null) {
                if (d5Var.b != -1) {
                    d5Var.a();
                }
                this.a++;
                this.i++;
                int i3 = this.a;
                d5Var.b = i3;
                d5Var.g = d5.a.UNRESTRICTED;
                this.l.c[i3] = d5Var;
            }
        }
        return d5Var;
    }

    @DexIgnore
    public d5 b() {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.n++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        d5 a2 = a(d5.a.SLACK, (String) null);
        this.a++;
        this.i++;
        int i2 = this.a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public w4 c() {
        w4 a2 = this.l.a.a();
        if (a2 == null) {
            a2 = new w4(this.l);
        } else {
            a2.d();
        }
        d5.b();
        return a2;
    }

    @DexIgnore
    public d5 d() {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.m++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        d5 a2 = a(d5.a.SLACK, (String) null);
        this.a++;
        this.i++;
        int i2 = this.a;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public x4 e() {
        return this.l;
    }

    @DexIgnore
    public final void f() {
        this.d *= 2;
        this.f = (w4[]) Arrays.copyOf(this.f, this.d);
        x4 x4Var = this.l;
        x4Var.c = (d5[]) Arrays.copyOf(x4Var.c, this.d);
        int i2 = this.d;
        this.h = new boolean[i2];
        this.e = i2;
        this.k = i2;
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.d++;
            a5Var.o = Math.max(a5Var.o, (long) i2);
            a5 a5Var2 = q;
            a5Var2.A = a5Var2.o;
        }
    }

    @DexIgnore
    public void g() throws Exception {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.e++;
        }
        if (this.g) {
            a5 a5Var2 = q;
            if (a5Var2 != null) {
                a5Var2.q++;
            }
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 >= this.j) {
                    z = true;
                    break;
                } else if (!this.f[i2].e) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                b(this.c);
                return;
            }
            a5 a5Var3 = q;
            if (a5Var3 != null) {
                a5Var3.p++;
            }
            a();
            return;
        }
        b(this.c);
    }

    @DexIgnore
    public final void h() {
        int i2 = 0;
        while (true) {
            w4[] w4VarArr = this.f;
            if (i2 < w4VarArr.length) {
                w4 w4Var = w4VarArr[i2];
                if (w4Var != null) {
                    this.l.a.a(w4Var);
                }
                this.f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public void i() {
        x4 x4Var;
        int i2 = 0;
        while (true) {
            x4Var = this.l;
            d5[] d5VarArr = x4Var.c;
            if (i2 >= d5VarArr.length) {
                break;
            }
            d5 d5Var = d5VarArr[i2];
            if (d5Var != null) {
                d5Var.a();
            }
            i2++;
        }
        x4Var.b.a(this.m, this.n);
        this.n = 0;
        Arrays.fill(this.l.c, (Object) null);
        HashMap<String, d5> hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.a = 0;
        this.c.clear();
        this.i = 1;
        for (int i3 = 0; i3 < this.j; i3++) {
            this.f[i3].c = false;
        }
        h();
        this.j = 0;
    }

    @DexIgnore
    public final void c(w4 w4Var) {
        w4[] w4VarArr = this.f;
        int i2 = this.j;
        if (w4VarArr[i2] != null) {
            this.l.a.a(w4VarArr[i2]);
        }
        w4[] w4VarArr2 = this.f;
        int i3 = this.j;
        w4VarArr2[i3] = w4Var;
        d5 d5Var = w4Var.a;
        d5Var.c = i3;
        this.j = i3 + 1;
        d5Var.c(w4Var);
    }

    @DexIgnore
    public final void b(w4 w4Var) {
        w4Var.a(this, 0);
    }

    @DexIgnore
    public final void d(w4 w4Var) {
        if (this.j > 0) {
            w4Var.d.a(w4Var, this.f);
            if (w4Var.d.a == 0) {
                w4Var.e = true;
            }
        }
    }

    @DexIgnore
    public int b(Object obj) {
        d5 e2 = ((i5) obj).e();
        if (e2 != null) {
            return (int) (e2.e + 0.5f);
        }
        return 0;
    }

    @DexIgnore
    public void c(d5 d5Var, d5 d5Var2, int i2, int i3) {
        w4 c2 = c();
        d5 d2 = d();
        d2.d = 0;
        c2.b(d5Var, d5Var2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public void b(a aVar) throws Exception {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.s++;
            a5Var.t = Math.max(a5Var.t, (long) this.i);
            a5 a5Var2 = q;
            a5Var2.u = Math.max(a5Var2.u, (long) this.j);
        }
        d((w4) aVar);
        a(aVar);
        a(aVar, false);
        a();
    }

    @DexIgnore
    public void a(w4 w4Var, int i2, int i3) {
        w4Var.a(a(i3, (String) null), i2);
    }

    @DexIgnore
    public d5 a(int i2, String str) {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.l++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        d5 a2 = a(d5.a.ERROR, str);
        this.a++;
        this.i++;
        int i3 = this.a;
        a2.b = i3;
        a2.d = i2;
        this.l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    @DexIgnore
    public void b(d5 d5Var, d5 d5Var2, int i2, int i3) {
        w4 c2 = c();
        d5 d2 = d();
        d2.d = 0;
        c2.a(d5Var, d5Var2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public final d5 a(d5.a aVar, String str) {
        d5 a2 = this.l.b.a();
        if (a2 == null) {
            a2 = new d5(aVar, str);
            a2.a(aVar, str);
        } else {
            a2.a();
            a2.a(aVar, str);
        }
        int i2 = this.n;
        int i3 = p;
        if (i2 >= i3) {
            p = i3 * 2;
            this.m = (d5[]) Arrays.copyOf(this.m, p);
        }
        d5[] d5VarArr = this.m;
        int i4 = this.n;
        this.n = i4 + 1;
        d5VarArr[i4] = a2;
        return a2;
    }

    @DexIgnore
    public void b(d5 d5Var, d5 d5Var2, boolean z) {
        w4 c2 = c();
        d5 d2 = d();
        d2.d = 0;
        c2.b(d5Var, d5Var2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(w4 w4Var) {
        d5 c2;
        if (w4Var != null) {
            a5 a5Var = q;
            if (a5Var != null) {
                a5Var.f++;
                if (w4Var.e) {
                    a5Var.g++;
                }
            }
            if (this.j + 1 >= this.k || this.i + 1 >= this.e) {
                f();
            }
            boolean z = false;
            if (!w4Var.e) {
                d(w4Var);
                if (!w4Var.c()) {
                    w4Var.a();
                    if (w4Var.a(this)) {
                        d5 b2 = b();
                        w4Var.a = b2;
                        c(w4Var);
                        this.o.a((a) w4Var);
                        a(this.o, true);
                        if (b2.c == -1) {
                            if (w4Var.a == b2 && (c2 = w4Var.c(b2)) != null) {
                                a5 a5Var2 = q;
                                if (a5Var2 != null) {
                                    a5Var2.j++;
                                }
                                w4Var.d(c2);
                            }
                            if (!w4Var.e) {
                                w4Var.a.c(w4Var);
                            }
                            this.j--;
                        }
                        z = true;
                    }
                    if (!w4Var.b()) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!z) {
                c(w4Var);
            }
        }
    }

    @DexIgnore
    public final int a(a aVar, boolean z) {
        a5 a5Var = q;
        if (a5Var != null) {
            a5Var.h++;
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            a5 a5Var2 = q;
            if (a5Var2 != null) {
                a5Var2.i++;
            }
            i3++;
            if (i3 >= this.i * 2) {
                return i3;
            }
            if (aVar.getKey() != null) {
                this.h[aVar.getKey().b] = true;
            }
            d5 a2 = aVar.a(this, this.h);
            if (a2 != null) {
                boolean[] zArr = this.h;
                int i4 = a2.b;
                if (zArr[i4]) {
                    return i3;
                }
                zArr[i4] = true;
            }
            if (a2 != null) {
                int i5 = -1;
                float f2 = Float.MAX_VALUE;
                for (int i6 = 0; i6 < this.j; i6++) {
                    w4 w4Var = this.f[i6];
                    if (w4Var.a.g != d5.a.UNRESTRICTED && !w4Var.e && w4Var.b(a2)) {
                        float b2 = w4Var.d.b(a2);
                        if (b2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            float f3 = (-w4Var.b) / b2;
                            if (f3 < f2) {
                                i5 = i6;
                                f2 = f3;
                            }
                        }
                    }
                }
                if (i5 > -1) {
                    w4 w4Var2 = this.f[i5];
                    w4Var2.a.c = -1;
                    a5 a5Var3 = q;
                    if (a5Var3 != null) {
                        a5Var3.j++;
                    }
                    w4Var2.d(a2);
                    d5 d5Var = w4Var2.a;
                    d5Var.c = i5;
                    d5Var.c(w4Var2);
                }
            }
            z2 = true;
        }
        return i3;
    }

    @DexIgnore
    public final int a(a aVar) throws Exception {
        float f2;
        boolean z;
        int i2 = 0;
        while (true) {
            int i3 = this.j;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (i2 >= i3) {
                z = false;
                break;
            }
            w4[] w4VarArr = this.f;
            if (w4VarArr[i2].a.g != d5.a.UNRESTRICTED && w4VarArr[i2].b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
                break;
            }
            i2++;
        }
        if (!z) {
            return 0;
        }
        boolean z2 = false;
        int i4 = 0;
        while (!z2) {
            a5 a5Var = q;
            if (a5Var != null) {
                a5Var.k++;
            }
            i4++;
            int i5 = 0;
            int i6 = -1;
            int i7 = -1;
            float f3 = Float.MAX_VALUE;
            int i8 = 0;
            while (i5 < this.j) {
                w4 w4Var = this.f[i5];
                if (w4Var.a.g != d5.a.UNRESTRICTED && !w4Var.e && w4Var.b < f2) {
                    int i9 = 1;
                    while (i9 < this.i) {
                        d5 d5Var = this.l.c[i9];
                        float b2 = w4Var.d.b(d5Var);
                        if (b2 > f2) {
                            int i10 = i8;
                            float f4 = f3;
                            int i11 = i7;
                            int i12 = i6;
                            for (int i13 = 0; i13 < 7; i13++) {
                                float f5 = d5Var.f[i13] / b2;
                                if ((f5 < f4 && i13 == i10) || i13 > i10) {
                                    i11 = i9;
                                    i12 = i5;
                                    f4 = f5;
                                    i10 = i13;
                                }
                            }
                            i6 = i12;
                            i7 = i11;
                            f3 = f4;
                            i8 = i10;
                        }
                        i9++;
                        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                }
                i5++;
                f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            if (i6 != -1) {
                w4 w4Var2 = this.f[i6];
                w4Var2.a.c = -1;
                a5 a5Var2 = q;
                if (a5Var2 != null) {
                    a5Var2.j++;
                }
                w4Var2.d(this.l.c[i7]);
                d5 d5Var2 = w4Var2.a;
                d5Var2.c = i6;
                d5Var2.c(w4Var2);
            } else {
                z2 = true;
            }
            if (i4 > this.i / 2) {
                z2 = true;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return i4;
    }

    @DexIgnore
    public final void a() {
        for (int i2 = 0; i2 < this.j; i2++) {
            w4 w4Var = this.f[i2];
            w4Var.a.e = w4Var.b;
        }
    }

    @DexIgnore
    public void a(d5 d5Var, d5 d5Var2, boolean z) {
        w4 c2 = c();
        d5 d2 = d();
        d2.d = 0;
        c2.a(d5Var, d5Var2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(d5 d5Var, d5 d5Var2, int i2, float f2, d5 d5Var3, d5 d5Var4, int i3, int i4) {
        int i5 = i4;
        w4 c2 = c();
        c2.a(d5Var, d5Var2, i2, f2, d5Var3, d5Var4, i3);
        if (i5 != 6) {
            c2.a(this, i5);
        }
        a(c2);
    }

    @DexIgnore
    public void a(d5 d5Var, d5 d5Var2, d5 d5Var3, d5 d5Var4, float f2, int i2) {
        w4 c2 = c();
        c2.a(d5Var, d5Var2, d5Var3, d5Var4, f2);
        if (i2 != 6) {
            c2.a(this, i2);
        }
        a(c2);
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, int i2, int i3) {
        w4 c2 = c();
        c2.a(d5Var, d5Var2, i2);
        if (i3 != 6) {
            c2.a(this, i3);
        }
        a(c2);
        return c2;
    }

    @DexIgnore
    public void a(d5 d5Var, int i2) {
        int i3 = d5Var.c;
        if (i3 != -1) {
            w4 w4Var = this.f[i3];
            if (w4Var.e) {
                w4Var.b = (float) i2;
            } else if (w4Var.d.a == 0) {
                w4Var.e = true;
                w4Var.b = (float) i2;
            } else {
                w4 c2 = c();
                c2.c(d5Var, i2);
                a(c2);
            }
        } else {
            w4 c3 = c();
            c3.b(d5Var, i2);
            a(c3);
        }
    }

    @DexIgnore
    public static w4 a(z4 z4Var, d5 d5Var, d5 d5Var2, d5 d5Var3, float f2, boolean z) {
        w4 c2 = z4Var.c();
        if (z) {
            z4Var.b(c2);
        }
        c2.a(d5Var, d5Var2, d5Var3, f2);
        return c2;
    }

    @DexIgnore
    public void a(j5 j5Var, j5 j5Var2, float f2, int i2) {
        j5 j5Var3 = j5Var;
        j5 j5Var4 = j5Var2;
        d5 a2 = a((Object) j5Var3.a(i5.d.LEFT));
        d5 a3 = a((Object) j5Var3.a(i5.d.TOP));
        d5 a4 = a((Object) j5Var3.a(i5.d.RIGHT));
        d5 a5 = a((Object) j5Var3.a(i5.d.BOTTOM));
        d5 a6 = a((Object) j5Var4.a(i5.d.LEFT));
        d5 a7 = a((Object) j5Var4.a(i5.d.TOP));
        d5 a8 = a((Object) j5Var4.a(i5.d.RIGHT));
        d5 a9 = a((Object) j5Var4.a(i5.d.BOTTOM));
        w4 c2 = c();
        double d2 = (double) f2;
        d5 d5Var = a4;
        double d3 = (double) i2;
        c2.b(a3, a5, a7, a9, (float) (Math.sin(d2) * d3));
        a(c2);
        w4 c3 = c();
        c3.b(a2, d5Var, a6, a8, (float) (Math.cos(d2) * d3));
        a(c3);
    }
}
