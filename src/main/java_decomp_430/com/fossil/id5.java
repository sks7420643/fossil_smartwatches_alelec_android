package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class id5 implements Factory<hd5> {
    @DexIgnore
    public static CaloriesOverviewDayPresenter a(gd5 gd5, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        return new CaloriesOverviewDayPresenter(gd5, summariesRepository, activitiesRepository, workoutSessionRepository, portfolioApp);
    }
}
