package com.fossil;

import com.fossil.ba6;
import com.fossil.r96;
import com.fossil.y96;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s96<E extends r96 & ba6 & y96> extends PriorityBlockingQueue<E> {
    @DexIgnore
    public static /* final */ int PEEK; // = 1;
    @DexIgnore
    public static /* final */ int POLL; // = 2;
    @DexIgnore
    public static /* final */ int POLL_WITH_TIMEOUT; // = 3;
    @DexIgnore
    public static /* final */ int TAKE; // = 0;
    @DexIgnore
    public /* final */ Queue<E> blockedQueue; // = new LinkedList();
    @DexIgnore
    public /* final */ ReentrantLock lock; // = new ReentrantLock();

    @DexIgnore
    public boolean canProcess(E e) {
        return e.b();
    }

    @DexIgnore
    public void clear() {
        try {
            this.lock.lock();
            this.blockedQueue.clear();
            super.clear();
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public <T> T[] concatenate(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, tArr3, 0, length);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        return tArr3;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        try {
            this.lock.lock();
            return super.contains(obj) || this.blockedQueue.contains(obj);
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public int drainTo(Collection<? super E> collection) {
        try {
            this.lock.lock();
            int drainTo = super.drainTo(collection) + this.blockedQueue.size();
            while (!this.blockedQueue.isEmpty()) {
                collection.add(this.blockedQueue.poll());
            }
            return drainTo;
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public E get(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        E performOperation;
        while (true) {
            performOperation = performOperation(i, l, timeUnit);
            if (performOperation == null || canProcess(performOperation)) {
                return performOperation;
            }
            offerBlockedResult(i, performOperation);
        }
        return performOperation;
    }

    @DexIgnore
    public boolean offerBlockedResult(int i, E e) {
        try {
            this.lock.lock();
            if (i == 1) {
                super.remove(e);
            }
            return this.blockedQueue.offer(e);
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public E performOperation(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        if (i == 0) {
            return (r96) super.take();
        }
        if (i == 1) {
            return (r96) super.peek();
        }
        if (i == 2) {
            return (r96) super.poll();
        }
        if (i != 3) {
            return null;
        }
        return (r96) super.poll(l.longValue(), timeUnit);
    }

    @DexIgnore
    public void recycleBlockedQueue() {
        try {
            this.lock.lock();
            Iterator it = this.blockedQueue.iterator();
            while (it.hasNext()) {
                r96 r96 = (r96) it.next();
                if (canProcess(r96)) {
                    super.offer(r96);
                    it.remove();
                }
            }
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public boolean remove(Object obj) {
        try {
            this.lock.lock();
            return super.remove(obj) || this.blockedQueue.remove(obj);
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        try {
            this.lock.lock();
            return this.blockedQueue.removeAll(collection) | super.removeAll(collection);
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public int size() {
        try {
            this.lock.lock();
            return this.blockedQueue.size() + super.size();
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        try {
            this.lock.lock();
            return concatenate(super.toArray(tArr), this.blockedQueue.toArray(tArr));
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public E peek() {
        try {
            return get(1, (Long) null, (TimeUnit) null);
        } catch (InterruptedException unused) {
            return null;
        }
    }

    @DexIgnore
    public E take() throws InterruptedException {
        return get(0, (Long) null, (TimeUnit) null);
    }

    @DexIgnore
    public E poll(long j, TimeUnit timeUnit) throws InterruptedException {
        return get(3, Long.valueOf(j), timeUnit);
    }

    @DexIgnore
    public E poll() {
        try {
            return get(2, (Long) null, (TimeUnit) null);
        } catch (InterruptedException unused) {
            return null;
        }
    }

    @DexIgnore
    public Object[] toArray() {
        try {
            this.lock.lock();
            return concatenate(super.toArray(), this.blockedQueue.toArray());
        } finally {
            this.lock.unlock();
        }
    }

    @DexIgnore
    public int drainTo(Collection<? super E> collection, int i) {
        try {
            this.lock.lock();
            int drainTo = super.drainTo(collection, i);
            while (!this.blockedQueue.isEmpty() && drainTo <= i) {
                collection.add(this.blockedQueue.poll());
                drainTo++;
            }
            return drainTo;
        } finally {
            this.lock.unlock();
        }
    }
}
