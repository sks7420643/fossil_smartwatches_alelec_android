package com.fossil;

import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
public final class ea5$a$a extends sf6 implements ig6<il6, xe6<? super List<MicroApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchMicroAppPresenter.a this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ea5$a$a(SearchMicroAppPresenter.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ea5$a$a ea5_a_a = new ea5$a$a(this.this$0, xe6);
        ea5_a_a.p$ = (il6) obj;
        return ea5_a_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ea5$a$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            MicroAppRepository e = this.this$0.this$0.l;
            SearchMicroAppPresenter.a aVar = this.this$0;
            return yd6.d(e.queryMicroAppByName(aVar.$query, aVar.this$0.n.e()));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
