package com.fossil;

import com.fossil.mc6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf6<T> implements xe6<T>, kf6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<cf6<?>, Object> c; // = AtomicReferenceFieldUpdater.newUpdater(cf6.class, Object.class, "a");
    @DexIgnore
    public volatile Object a;
    @DexIgnore
    public /* final */ xe6<T> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public cf6(xe6<? super T> xe6, Object obj) {
        wg6.b(xe6, "delegate");
        this.b = xe6;
        this.a = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.a;
        df6 df6 = df6.UNDECIDED;
        if (obj == df6) {
            if (c.compareAndSet(this, df6, ff6.a())) {
                return ff6.a();
            }
            obj = this.a;
        }
        if (obj == df6.RESUMED) {
            return ff6.a();
        }
        if (!(obj instanceof mc6.b)) {
            return obj;
        }
        throw ((mc6.b) obj).exception;
    }

    @DexIgnore
    public kf6 getCallerFrame() {
        xe6<T> xe6 = this.b;
        if (!(xe6 instanceof kf6)) {
            xe6 = null;
        }
        return (kf6) xe6;
    }

    @DexIgnore
    public af6 getContext() {
        return this.b.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.a;
            df6 df6 = df6.UNDECIDED;
            if (obj2 == df6) {
                if (c.compareAndSet(this, df6, obj)) {
                    return;
                }
            } else if (obj2 != ff6.a()) {
                throw new IllegalStateException("Already resumed");
            } else if (c.compareAndSet(this, ff6.a(), df6.RESUMED)) {
                this.b.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.b;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public cf6(xe6<? super T> xe6) {
        this(xe6, df6.UNDECIDED);
        wg6.b(xe6, "delegate");
    }
}
