package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dq2 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> a; // = this.b.a.iterator();
    @DexIgnore
    public /* final */ /* synthetic */ bq2 b;

    @DexIgnore
    public dq2(bq2 bq2) {
        this.b = bq2;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.a.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
