package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import com.zendesk.belvedere.BelvedereCallback;
import com.zendesk.belvedere.BelvedereResult;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i66 {
    @DexIgnore
    public /* final */ h66 a;
    @DexIgnore
    public /* final */ n66 b;
    @DexIgnore
    public /* final */ Map<Integer, BelvedereResult> c; // = new HashMap();
    @DexIgnore
    public /* final */ k66 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[m66.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[m66.Gallery.ordinal()] = 1;
            a[m66.Camera.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public i66(h66 h66, n66 n66) {
        this.a = h66;
        this.b = n66;
        this.d = h66.b();
    }

    @DexIgnore
    public final boolean a(Context context) {
        if (e(context)) {
            if (!a(context, "android.permission.CAMERA")) {
                return true;
            }
            if (w6.a(context, "android.permission.CAMERA") == 0) {
                return true;
            }
            this.d.w("BelvedereImagePicker", "Found Camera permission declared in AndroidManifest.xml and the user hasn't granted that permission. Not doing any further efforts to acquire that permission.");
        }
        return false;
    }

    @DexIgnore
    public List<j66> b(Context context) {
        TreeSet<m66> c2 = this.a.c();
        ArrayList arrayList = new ArrayList();
        Iterator<m66> it = c2.iterator();
        while (it.hasNext()) {
            j66 j66 = null;
            int i = a.a[it.next().ordinal()];
            if (i == 1) {
                j66 = d(context);
            } else if (i == 2) {
                j66 = c(context);
            }
            if (j66 != null) {
                arrayList.add(j66);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final j66 c(Context context) {
        if (a(context)) {
            return h(context);
        }
        return null;
    }

    @DexIgnore
    public j66 d(Context context) {
        if (f(context)) {
            return new j66(a(), this.a.h(), m66.Gallery);
        }
        return null;
    }

    @DexIgnore
    public final boolean e(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = context.getPackageManager();
        boolean z = packageManager.hasSystemFeature("android.hardware.camera") || packageManager.hasSystemFeature("android.hardware.camera.front");
        boolean a2 = a(intent, context);
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera present: %b, Camera App present: %b", new Object[]{Boolean.valueOf(z), Boolean.valueOf(a2)}));
        if (!z || !a2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean f(Context context) {
        return a(a(), context);
    }

    @DexIgnore
    public boolean g(Context context) {
        for (m66 a2 : m66.values()) {
            if (a(a2, context)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final j66 h(Context context) {
        Set<Integer> keySet = this.c.keySet();
        int d2 = this.a.d();
        int e = this.a.e();
        while (true) {
            if (e >= this.a.d()) {
                break;
            } else if (!keySet.contains(Integer.valueOf(e))) {
                d2 = e;
                break;
            } else {
                e++;
            }
        }
        File a2 = this.b.a(context);
        if (a2 == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent. Image path is null. There's something wrong with the storage.");
            return null;
        }
        Uri a3 = this.b.a(context, a2);
        if (a3 == null) {
            this.d.w("BelvedereImagePicker", "Camera Intent: Uri to file is null. There's something wrong with the storage or FileProvider configuration.");
            return null;
        }
        this.c.put(Integer.valueOf(d2), new BelvedereResult(a2, a3));
        this.d.d("BelvedereImagePicker", String.format(Locale.US, "Camera Intent: Request Id: %s - File: %s - Uri: %s", new Object[]{Integer.valueOf(d2), a2, a3}));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", a3);
        this.b.a(context, intent, a3, 3);
        return new j66(intent, d2, m66.Camera);
    }

    @DexIgnore
    public void a(Context context, int i, int i2, Intent intent, BelvedereCallback<List<BelvedereResult>> belvedereCallback) {
        ArrayList arrayList = new ArrayList();
        if (i == this.a.h()) {
            k66 k66 = this.d;
            Locale locale = Locale.US;
            Object[] objArr = new Object[1];
            objArr[0] = Boolean.valueOf(i2 == -1);
            k66.d("BelvedereImagePicker", String.format(locale, "Parsing activity result - Gallery - Ok: %s", objArr));
            if (i2 == -1) {
                List<Uri> a2 = a(intent);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Number of items received from gallery: %s", new Object[]{Integer.valueOf(a2.size())}));
                new l66(context, this.d, this.b, belvedereCallback).execute(a2.toArray(new Uri[a2.size()]));
                return;
            }
        } else if (this.c.containsKey(Integer.valueOf(i))) {
            k66 k662 = this.d;
            Locale locale2 = Locale.US;
            Object[] objArr2 = new Object[1];
            objArr2[0] = Boolean.valueOf(i2 == -1);
            k662.d("BelvedereImagePicker", String.format(locale2, "Parsing activity result - Camera - Ok: %s", objArr2));
            BelvedereResult belvedereResult = this.c.get(Integer.valueOf(i));
            this.b.a(context, belvedereResult.b(), 3);
            if (i2 == -1) {
                arrayList.add(belvedereResult);
                this.d.d("BelvedereImagePicker", String.format(Locale.US, "Image from camera: %s", new Object[]{belvedereResult.a()}));
            }
            this.c.remove(Integer.valueOf(i));
        }
        if (belvedereCallback != null) {
            belvedereCallback.internalSuccess(arrayList);
        }
    }

    @DexIgnore
    public boolean a(m66 m66, Context context) {
        if (!this.a.c().contains(m66)) {
            return false;
        }
        int i = a.a[m66.ordinal()];
        if (i == 1) {
            return f(context);
        }
        if (i != 2) {
            return false;
        }
        return a(context);
    }

    @DexIgnore
    public final boolean a(Intent intent, Context context) {
        return intent.resolveActivity(context.getPackageManager()) != null;
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public final List<Uri> a(Intent intent) {
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
            ClipData clipData = intent.getClipData();
            int itemCount = clipData.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                ClipData.Item itemAt = clipData.getItemAt(i);
                if (itemAt.getUri() != null) {
                    arrayList.add(itemAt.getUri());
                }
            }
        } else if (intent.getData() != null) {
            arrayList.add(intent.getData());
        }
        return arrayList;
    }

    @DexIgnore
    @TargetApi(19)
    public final Intent a() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= 19) {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_OPEN_DOCUMENT'");
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
        } else {
            this.d.d("BelvedereImagePicker", "Gallery Intent, using 'ACTION_GET_CONTENT'");
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType(this.a.f());
        intent.addCategory("android.intent.category.OPENABLE");
        if (Build.VERSION.SDK_INT >= 18) {
            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", this.a.a());
        }
        return intent;
    }

    @DexIgnore
    public final boolean a(Context context, String str) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr != null && strArr.length > 0) {
                for (String equals : strArr) {
                    if (equals.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            this.d.e("BelvedereImagePicker", "Not able to find permissions in manifest", e);
        }
        return false;
    }
}
