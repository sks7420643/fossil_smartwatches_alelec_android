package com.fossil;

import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j65 extends k24<i65> {
    @DexIgnore
    void a(Complication complication);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(List<lc6<Complication, String>> list);

    @DexIgnore
    void d(List<lc6<Complication, String>> list);

    @DexIgnore
    void u();
}
