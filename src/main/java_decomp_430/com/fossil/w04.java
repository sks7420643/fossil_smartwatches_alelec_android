package com.fossil;

import androidx.lifecycle.ViewModelProvider;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w04 implements ViewModelProvider.Factory {
    @DexIgnore
    public /* final */ Map<Class<? extends td>, Provider<td>> a;

    @DexIgnore
    public w04(Map<Class<? extends td>, Provider<td>> map) {
        wg6.b(map, "creators");
        this.a = map;
    }

    @DexIgnore
    public <T extends td> T create(Class<T> cls) {
        T t;
        wg6.b(cls, "modelClass");
        Provider provider = this.a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (cls.isAssignableFrom((Class) ((Map.Entry) t).getKey())) {
                    break;
                }
            }
            Map.Entry entry = (Map.Entry) t;
            provider = entry != null ? (Provider) entry.getValue() : null;
        }
        if (provider != null) {
            try {
                T t2 = provider.get();
                if (t2 != null) {
                    return (td) t2;
                }
                throw new rc6("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
