package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum SpecialEntryID {
    ACTIVITY(0),
    GOAL_TRACKING(202),
    CONTINOUS_HEARTRATE(203),
    RESTING_HEARTRATE(204),
    STRESS(205),
    HEARTRATE_QUALITY(206),
    RESTING_QUALITY(207),
    ON_OFF_BODY_STATE(208),
    ON_OFF_CHANGER_STATE(209),
    GPS_DATA_POINT(210),
    TAGGED_ACTIVITY(221),
    AVG_HEARTRATE(222),
    MAX_HEARTRATE(223),
    WORKOUT_SUMMARY(224),
    INFOMATION_CODE(225),
    INFOMATION_CODE_WITH_TIMESTAMP(226),
    IGNORE_PREVIOUS_ENTRY(253),
    PADDING(254),
    DEBUGGING(255);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public SpecialEntryID(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
