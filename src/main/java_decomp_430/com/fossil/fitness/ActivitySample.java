package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySample implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ActivitySample> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ boolean mActive;
    @DexIgnore
    public /* final */ Integer mAvgHeartRate;
    @DexIgnore
    public /* final */ BodyState mBodyState;
    @DexIgnore
    public /* final */ byte mCalories;
    @DexIgnore
    public /* final */ ChargeState mChargeState;
    @DexIgnore
    public /* final */ ArrayList<DebugEntry> mDebugEntries;
    @DexIgnore
    public /* final */ ArrayList<GpsDataPoint> mGpsDataPoint;
    @DexIgnore
    public /* final */ Integer mHeartrate;
    @DexIgnore
    public /* final */ Integer mHeartrateFilterd;
    @DexIgnore
    public /* final */ Byte mHeartrateQuality;
    @DexIgnore
    public /* final */ int mId;
    @DexIgnore
    public /* final */ HashMap<InformationCode, InfomationCodeEntry> mInfomationCodes;
    @DexIgnore
    public /* final */ boolean mIsPaddingEntry;
    @DexIgnore
    public /* final */ Integer mMaxHeartRate;
    @DexIgnore
    public /* final */ int mMaxVariance;
    @DexIgnore
    public /* final */ double mNormalizedDistance;
    @DexIgnore
    public /* final */ short mPoint;
    @DexIgnore
    public /* final */ Integer mResting;
    @DexIgnore
    public /* final */ Byte mRestingQuality;
    @DexIgnore
    public /* final */ SkinProximity mSkinProximity;
    @DexIgnore
    public /* final */ SleepState mSleepState;
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ short mSteps;
    @DexIgnore
    public /* final */ ArrayList<Integer> mTaggedGoals;
    @DexIgnore
    public /* final */ ArrayList<TaggedWorkoutEntry> mTaggedWorkoutEntries;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ int mVariance;
    @DexIgnore
    public /* final */ WorkoutSummary mWorkoutSummary;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<ActivitySample> {
        @DexIgnore
        public ActivitySample createFromParcel(Parcel parcel) {
            return new ActivitySample(parcel);
        }

        @DexIgnore
        public ActivitySample[] newArray(int i) {
            return new ActivitySample[i];
        }
    }

    @DexIgnore
    public ActivitySample(int i, int i2, int i3, short s, short s2, byte b, int i4, int i5, boolean z, Integer num, Integer num2, Integer num3, Integer num4, double d, Integer num5, HashMap<InformationCode, InfomationCodeEntry> hashMap, ArrayList<TaggedWorkoutEntry> arrayList, ArrayList<Integer> arrayList2, SleepState sleepState, SkinProximity skinProximity, Byte b2, Byte b3, ArrayList<DebugEntry> arrayList3, boolean z2, BodyState bodyState, ChargeState chargeState, WorkoutSummary workoutSummary, ArrayList<GpsDataPoint> arrayList4) {
        this.mId = i;
        this.mStartTime = i2;
        this.mTimezoneOffsetInSecond = i3;
        this.mSteps = s;
        this.mPoint = s2;
        this.mCalories = b;
        this.mVariance = i4;
        this.mMaxVariance = i5;
        this.mActive = z;
        this.mHeartrate = num;
        this.mHeartrateFilterd = num2;
        this.mMaxHeartRate = num3;
        this.mAvgHeartRate = num4;
        this.mNormalizedDistance = d;
        this.mResting = num5;
        this.mInfomationCodes = hashMap;
        this.mTaggedWorkoutEntries = arrayList;
        this.mTaggedGoals = arrayList2;
        this.mSleepState = sleepState;
        this.mSkinProximity = skinProximity;
        this.mHeartrateQuality = b2;
        this.mRestingQuality = b3;
        this.mDebugEntries = arrayList3;
        this.mIsPaddingEntry = z2;
        this.mBodyState = bodyState;
        this.mChargeState = chargeState;
        this.mWorkoutSummary = workoutSummary;
        this.mGpsDataPoint = arrayList4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        WorkoutSummary workoutSummary;
        ChargeState chargeState;
        BodyState bodyState;
        Byte b;
        Byte b2;
        SkinProximity skinProximity;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        Integer num5;
        if (!(obj instanceof ActivitySample)) {
            return false;
        }
        ActivitySample activitySample = (ActivitySample) obj;
        if (this.mId != activitySample.mId || this.mStartTime != activitySample.mStartTime || this.mTimezoneOffsetInSecond != activitySample.mTimezoneOffsetInSecond || this.mSteps != activitySample.mSteps || this.mPoint != activitySample.mPoint || this.mCalories != activitySample.mCalories || this.mVariance != activitySample.mVariance || this.mMaxVariance != activitySample.mMaxVariance || this.mActive != activitySample.mActive) {
            return false;
        }
        if ((this.mHeartrate != null || activitySample.mHeartrate != null) && ((num5 = this.mHeartrate) == null || !num5.equals(activitySample.mHeartrate))) {
            return false;
        }
        if ((this.mHeartrateFilterd != null || activitySample.mHeartrateFilterd != null) && ((num4 = this.mHeartrateFilterd) == null || !num4.equals(activitySample.mHeartrateFilterd))) {
            return false;
        }
        if ((this.mMaxHeartRate != null || activitySample.mMaxHeartRate != null) && ((num3 = this.mMaxHeartRate) == null || !num3.equals(activitySample.mMaxHeartRate))) {
            return false;
        }
        if (((this.mAvgHeartRate != null || activitySample.mAvgHeartRate != null) && ((num2 = this.mAvgHeartRate) == null || !num2.equals(activitySample.mAvgHeartRate))) || this.mNormalizedDistance != activitySample.mNormalizedDistance) {
            return false;
        }
        if (((this.mResting != null || activitySample.mResting != null) && ((num = this.mResting) == null || !num.equals(activitySample.mResting))) || !this.mInfomationCodes.equals(activitySample.mInfomationCodes) || !this.mTaggedWorkoutEntries.equals(activitySample.mTaggedWorkoutEntries) || !this.mTaggedGoals.equals(activitySample.mTaggedGoals) || this.mSleepState != activitySample.mSleepState) {
            return false;
        }
        if ((this.mSkinProximity != null || activitySample.mSkinProximity != null) && ((skinProximity = this.mSkinProximity) == null || !skinProximity.equals(activitySample.mSkinProximity))) {
            return false;
        }
        if ((this.mHeartrateQuality != null || activitySample.mHeartrateQuality != null) && ((b2 = this.mHeartrateQuality) == null || !b2.equals(activitySample.mHeartrateQuality))) {
            return false;
        }
        if (((this.mRestingQuality != null || activitySample.mRestingQuality != null) && ((b = this.mRestingQuality) == null || !b.equals(activitySample.mRestingQuality))) || !this.mDebugEntries.equals(activitySample.mDebugEntries) || this.mIsPaddingEntry != activitySample.mIsPaddingEntry) {
            return false;
        }
        if ((this.mBodyState != null || activitySample.mBodyState != null) && ((bodyState = this.mBodyState) == null || !bodyState.equals(activitySample.mBodyState))) {
            return false;
        }
        if ((this.mChargeState != null || activitySample.mChargeState != null) && ((chargeState = this.mChargeState) == null || !chargeState.equals(activitySample.mChargeState))) {
            return false;
        }
        if (((this.mWorkoutSummary != null || activitySample.mWorkoutSummary != null) && ((workoutSummary = this.mWorkoutSummary) == null || !workoutSummary.equals(activitySample.mWorkoutSummary))) || !this.mGpsDataPoint.equals(activitySample.mGpsDataPoint)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean getActive() {
        return this.mActive;
    }

    @DexIgnore
    public Integer getAvgHeartRate() {
        return this.mAvgHeartRate;
    }

    @DexIgnore
    public BodyState getBodyState() {
        return this.mBodyState;
    }

    @DexIgnore
    public byte getCalories() {
        return this.mCalories;
    }

    @DexIgnore
    public ChargeState getChargeState() {
        return this.mChargeState;
    }

    @DexIgnore
    public ArrayList<DebugEntry> getDebugEntries() {
        return this.mDebugEntries;
    }

    @DexIgnore
    public ArrayList<GpsDataPoint> getGpsDataPoint() {
        return this.mGpsDataPoint;
    }

    @DexIgnore
    public Integer getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public Integer getHeartrateFilterd() {
        return this.mHeartrateFilterd;
    }

    @DexIgnore
    public Byte getHeartrateQuality() {
        return this.mHeartrateQuality;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public HashMap<InformationCode, InfomationCodeEntry> getInfomationCodes() {
        return this.mInfomationCodes;
    }

    @DexIgnore
    public boolean getIsPaddingEntry() {
        return this.mIsPaddingEntry;
    }

    @DexIgnore
    public Integer getMaxHeartRate() {
        return this.mMaxHeartRate;
    }

    @DexIgnore
    public int getMaxVariance() {
        return this.mMaxVariance;
    }

    @DexIgnore
    public double getNormalizedDistance() {
        return this.mNormalizedDistance;
    }

    @DexIgnore
    public short getPoint() {
        return this.mPoint;
    }

    @DexIgnore
    public Integer getResting() {
        return this.mResting;
    }

    @DexIgnore
    public Byte getRestingQuality() {
        return this.mRestingQuality;
    }

    @DexIgnore
    public SkinProximity getSkinProximity() {
        return this.mSkinProximity;
    }

    @DexIgnore
    public SleepState getSleepState() {
        return this.mSleepState;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public short getSteps() {
        return this.mSteps;
    }

    @DexIgnore
    public ArrayList<Integer> getTaggedGoals() {
        return this.mTaggedGoals;
    }

    @DexIgnore
    public ArrayList<TaggedWorkoutEntry> getTaggedWorkoutEntries() {
        return this.mTaggedWorkoutEntries;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int getVariance() {
        return this.mVariance;
    }

    @DexIgnore
    public WorkoutSummary getWorkoutSummary() {
        return this.mWorkoutSummary;
    }

    @DexIgnore
    public int hashCode() {
        int i = (((((((((((((((((527 + this.mId) * 31) + this.mStartTime) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mSteps) * 31) + this.mPoint) * 31) + this.mCalories) * 31) + this.mVariance) * 31) + this.mMaxVariance) * 31) + (this.mActive ? 1 : 0)) * 31;
        Integer num = this.mHeartrate;
        int i2 = 0;
        int hashCode = (i + (num == null ? 0 : num.hashCode())) * 31;
        Integer num2 = this.mHeartrateFilterd;
        int hashCode2 = (hashCode + (num2 == null ? 0 : num2.hashCode())) * 31;
        Integer num3 = this.mMaxHeartRate;
        int hashCode3 = (hashCode2 + (num3 == null ? 0 : num3.hashCode())) * 31;
        Integer num4 = this.mAvgHeartRate;
        int hashCode4 = (((hashCode3 + (num4 == null ? 0 : num4.hashCode())) * 31) + ((int) (Double.doubleToLongBits(this.mNormalizedDistance) ^ (Double.doubleToLongBits(this.mNormalizedDistance) >>> 32)))) * 31;
        Integer num5 = this.mResting;
        int hashCode5 = (((((((((hashCode4 + (num5 == null ? 0 : num5.hashCode())) * 31) + this.mInfomationCodes.hashCode()) * 31) + this.mTaggedWorkoutEntries.hashCode()) * 31) + this.mTaggedGoals.hashCode()) * 31) + this.mSleepState.hashCode()) * 31;
        SkinProximity skinProximity = this.mSkinProximity;
        int hashCode6 = (hashCode5 + (skinProximity == null ? 0 : skinProximity.hashCode())) * 31;
        Byte b = this.mHeartrateQuality;
        int hashCode7 = (hashCode6 + (b == null ? 0 : b.hashCode())) * 31;
        Byte b2 = this.mRestingQuality;
        int hashCode8 = (((((hashCode7 + (b2 == null ? 0 : b2.hashCode())) * 31) + this.mDebugEntries.hashCode()) * 31) + (this.mIsPaddingEntry ? 1 : 0)) * 31;
        BodyState bodyState = this.mBodyState;
        int hashCode9 = (hashCode8 + (bodyState == null ? 0 : bodyState.hashCode())) * 31;
        ChargeState chargeState = this.mChargeState;
        int hashCode10 = (hashCode9 + (chargeState == null ? 0 : chargeState.hashCode())) * 31;
        WorkoutSummary workoutSummary = this.mWorkoutSummary;
        if (workoutSummary != null) {
            i2 = workoutSummary.hashCode();
        }
        return ((hashCode10 + i2) * 31) + this.mGpsDataPoint.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ActivitySample{mId=" + this.mId + ",mStartTime=" + this.mStartTime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mSteps=" + this.mSteps + ",mPoint=" + this.mPoint + ",mCalories=" + this.mCalories + ",mVariance=" + this.mVariance + ",mMaxVariance=" + this.mMaxVariance + ",mActive=" + this.mActive + ",mHeartrate=" + this.mHeartrate + ",mHeartrateFilterd=" + this.mHeartrateFilterd + ",mMaxHeartRate=" + this.mMaxHeartRate + ",mAvgHeartRate=" + this.mAvgHeartRate + ",mNormalizedDistance=" + this.mNormalizedDistance + ",mResting=" + this.mResting + ",mInfomationCodes=" + this.mInfomationCodes + ",mTaggedWorkoutEntries=" + this.mTaggedWorkoutEntries + ",mTaggedGoals=" + this.mTaggedGoals + ",mSleepState=" + this.mSleepState + ",mSkinProximity=" + this.mSkinProximity + ",mHeartrateQuality=" + this.mHeartrateQuality + ",mRestingQuality=" + this.mRestingQuality + ",mDebugEntries=" + this.mDebugEntries + ",mIsPaddingEntry=" + this.mIsPaddingEntry + ",mBodyState=" + this.mBodyState + ",mChargeState=" + this.mChargeState + ",mWorkoutSummary=" + this.mWorkoutSummary + ",mGpsDataPoint=" + this.mGpsDataPoint + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeInt(this.mSteps);
        parcel.writeInt(this.mPoint);
        parcel.writeByte(this.mCalories);
        parcel.writeInt(this.mVariance);
        parcel.writeInt(this.mMaxVariance);
        parcel.writeByte(this.mActive ? (byte) 1 : 0);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mHeartrate.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mHeartrateFilterd != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mHeartrateFilterd.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaxHeartRate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mMaxHeartRate.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAvgHeartRate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAvgHeartRate.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeDouble(this.mNormalizedDistance);
        if (this.mResting != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mResting.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeMap(this.mInfomationCodes);
        parcel.writeList(this.mTaggedWorkoutEntries);
        parcel.writeList(this.mTaggedGoals);
        parcel.writeInt(this.mSleepState.ordinal());
        if (this.mSkinProximity != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mSkinProximity.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mHeartrateQuality != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mHeartrateQuality.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mRestingQuality != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mRestingQuality.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mDebugEntries);
        parcel.writeByte(this.mIsPaddingEntry ? (byte) 1 : 0);
        if (this.mBodyState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mBodyState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mChargeState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mChargeState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mWorkoutSummary != null) {
            parcel.writeByte((byte) 1);
            this.mWorkoutSummary.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mGpsDataPoint);
    }

    @DexIgnore
    public ActivitySample(Parcel parcel) {
        this.mId = parcel.readInt();
        this.mStartTime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mSteps = (short) parcel.readInt();
        this.mPoint = (short) parcel.readInt();
        this.mCalories = parcel.readByte();
        this.mVariance = parcel.readInt();
        this.mMaxVariance = parcel.readInt();
        boolean z = true;
        this.mActive = parcel.readByte() != 0;
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = Integer.valueOf(parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mHeartrateFilterd = null;
        } else {
            this.mHeartrateFilterd = Integer.valueOf(parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mMaxHeartRate = null;
        } else {
            this.mMaxHeartRate = Integer.valueOf(parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mAvgHeartRate = null;
        } else {
            this.mAvgHeartRate = Integer.valueOf(parcel.readInt());
        }
        this.mNormalizedDistance = parcel.readDouble();
        if (parcel.readByte() == 0) {
            this.mResting = null;
        } else {
            this.mResting = Integer.valueOf(parcel.readInt());
        }
        this.mInfomationCodes = new HashMap<>();
        parcel.readMap(this.mInfomationCodes, ActivitySample.class.getClassLoader());
        this.mTaggedWorkoutEntries = new ArrayList<>();
        parcel.readList(this.mTaggedWorkoutEntries, ActivitySample.class.getClassLoader());
        this.mTaggedGoals = new ArrayList<>();
        parcel.readList(this.mTaggedGoals, ActivitySample.class.getClassLoader());
        this.mSleepState = SleepState.values()[parcel.readInt()];
        if (parcel.readByte() == 0) {
            this.mSkinProximity = null;
        } else {
            this.mSkinProximity = SkinProximity.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mHeartrateQuality = null;
        } else {
            this.mHeartrateQuality = Byte.valueOf(parcel.readByte());
        }
        if (parcel.readByte() == 0) {
            this.mRestingQuality = null;
        } else {
            this.mRestingQuality = Byte.valueOf(parcel.readByte());
        }
        this.mDebugEntries = new ArrayList<>();
        parcel.readList(this.mDebugEntries, ActivitySample.class.getClassLoader());
        this.mIsPaddingEntry = parcel.readByte() == 0 ? false : z;
        if (parcel.readByte() == 0) {
            this.mBodyState = null;
        } else {
            this.mBodyState = BodyState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mChargeState = null;
        } else {
            this.mChargeState = ChargeState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mWorkoutSummary = null;
        } else {
            this.mWorkoutSummary = new WorkoutSummary(parcel);
        }
        this.mGpsDataPoint = new ArrayList<>();
        parcel.readList(this.mGpsDataPoint, ActivitySample.class.getClassLoader());
    }
}
