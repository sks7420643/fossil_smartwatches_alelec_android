package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSession implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<SleepSession> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mAwakeMinutes;
    @DexIgnore
    public /* final */ int mDeepSleepMinutes;
    @DexIgnore
    public /* final */ int mEndTime;
    @DexIgnore
    public /* final */ HeartRate mHeartrate;
    @DexIgnore
    public /* final */ int mLightSleepMinutes;
    @DexIgnore
    public /* final */ int mNormalizedSleepQuality;
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ ArrayList<SleepStateChange> mStateChanges;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<SleepSession> {
        @DexIgnore
        public SleepSession createFromParcel(Parcel parcel) {
            return new SleepSession(parcel);
        }

        @DexIgnore
        public SleepSession[] newArray(int i) {
            return new SleepSession[i];
        }
    }

    @DexIgnore
    public SleepSession(int i, int i2, int i3, int i4, int i5, int i6, ArrayList<SleepStateChange> arrayList, int i7, HeartRate heartRate) {
        this.mStartTime = i;
        this.mEndTime = i2;
        this.mAwakeMinutes = i3;
        this.mLightSleepMinutes = i4;
        this.mDeepSleepMinutes = i5;
        this.mNormalizedSleepQuality = i6;
        this.mStateChanges = arrayList;
        this.mTimezoneOffsetInSecond = i7;
        this.mHeartrate = heartRate;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        HeartRate heartRate;
        if (!(obj instanceof SleepSession)) {
            return false;
        }
        SleepSession sleepSession = (SleepSession) obj;
        if (this.mStartTime != sleepSession.mStartTime || this.mEndTime != sleepSession.mEndTime || this.mAwakeMinutes != sleepSession.mAwakeMinutes || this.mLightSleepMinutes != sleepSession.mLightSleepMinutes || this.mDeepSleepMinutes != sleepSession.mDeepSleepMinutes || this.mNormalizedSleepQuality != sleepSession.mNormalizedSleepQuality || !this.mStateChanges.equals(sleepSession.mStateChanges) || this.mTimezoneOffsetInSecond != sleepSession.mTimezoneOffsetInSecond) {
            return false;
        }
        if ((this.mHeartrate != null || sleepSession.mHeartrate != null) && ((heartRate = this.mHeartrate) == null || !heartRate.equals(sleepSession.mHeartrate))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int getAwakeMinutes() {
        return this.mAwakeMinutes;
    }

    @DexIgnore
    public int getDeepSleepMinutes() {
        return this.mDeepSleepMinutes;
    }

    @DexIgnore
    public int getEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public HeartRate getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public int getLightSleepMinutes() {
        return this.mLightSleepMinutes;
    }

    @DexIgnore
    public int getNormalizedSleepQuality() {
        return this.mNormalizedSleepQuality;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public ArrayList<SleepStateChange> getStateChanges() {
        return this.mStateChanges;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((((((527 + this.mStartTime) * 31) + this.mEndTime) * 31) + this.mAwakeMinutes) * 31) + this.mLightSleepMinutes) * 31) + this.mDeepSleepMinutes) * 31) + this.mNormalizedSleepQuality) * 31) + this.mStateChanges.hashCode()) * 31) + this.mTimezoneOffsetInSecond) * 31;
        HeartRate heartRate = this.mHeartrate;
        return hashCode + (heartRate == null ? 0 : heartRate.hashCode());
    }

    @DexIgnore
    public String toString() {
        return "SleepSession{mStartTime=" + this.mStartTime + ",mEndTime=" + this.mEndTime + ",mAwakeMinutes=" + this.mAwakeMinutes + ",mLightSleepMinutes=" + this.mLightSleepMinutes + ",mDeepSleepMinutes=" + this.mDeepSleepMinutes + ",mNormalizedSleepQuality=" + this.mNormalizedSleepQuality + ",mStateChanges=" + this.mStateChanges + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mHeartrate=" + this.mHeartrate + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mEndTime);
        parcel.writeInt(this.mAwakeMinutes);
        parcel.writeInt(this.mLightSleepMinutes);
        parcel.writeInt(this.mDeepSleepMinutes);
        parcel.writeInt(this.mNormalizedSleepQuality);
        parcel.writeList(this.mStateChanges);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            this.mHeartrate.writeToParcel(parcel, i);
            return;
        }
        parcel.writeByte((byte) 0);
    }

    @DexIgnore
    public SleepSession(Parcel parcel) {
        this.mStartTime = parcel.readInt();
        this.mEndTime = parcel.readInt();
        this.mAwakeMinutes = parcel.readInt();
        this.mLightSleepMinutes = parcel.readInt();
        this.mDeepSleepMinutes = parcel.readInt();
        this.mNormalizedSleepQuality = parcel.readInt();
        this.mStateChanges = new ArrayList<>();
        parcel.readList(this.mStateChanges, SleepSession.class.getClassLoader());
        this.mTimezoneOffsetInSecond = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = new HeartRate(parcel);
        }
    }
}
