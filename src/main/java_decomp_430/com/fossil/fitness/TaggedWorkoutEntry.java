package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TaggedWorkoutEntry implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<TaggedWorkoutEntry> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Integer mAceLatencyInMinute;
    @DexIgnore
    public /* final */ byte mCaloriesBeforeTagged;
    @DexIgnore
    public /* final */ short mHeartrate;
    @DexIgnore
    public /* final */ int mId;
    @DexIgnore
    public /* final */ boolean mIsRemainder;
    @DexIgnore
    public /* final */ byte mSecondsInMinute;
    @DexIgnore
    public /* final */ WorkoutState mState;
    @DexIgnore
    public /* final */ int mStateChangeIndexInSeconds;
    @DexIgnore
    public /* final */ short mStepsBeforeTagged;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<TaggedWorkoutEntry> {
        @DexIgnore
        public TaggedWorkoutEntry createFromParcel(Parcel parcel) {
            return new TaggedWorkoutEntry(parcel);
        }

        @DexIgnore
        public TaggedWorkoutEntry[] newArray(int i) {
            return new TaggedWorkoutEntry[i];
        }
    }

    @DexIgnore
    public TaggedWorkoutEntry(int i, byte b, short s, byte b2, short s2, WorkoutState workoutState, WorkoutType workoutType, int i2, Integer num, boolean z) {
        this.mId = i;
        this.mSecondsInMinute = b;
        this.mStepsBeforeTagged = s;
        this.mCaloriesBeforeTagged = b2;
        this.mHeartrate = s2;
        this.mState = workoutState;
        this.mType = workoutType;
        this.mStateChangeIndexInSeconds = i2;
        this.mAceLatencyInMinute = num;
        this.mIsRemainder = z;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        if (!(obj instanceof TaggedWorkoutEntry)) {
            return false;
        }
        TaggedWorkoutEntry taggedWorkoutEntry = (TaggedWorkoutEntry) obj;
        if (this.mId != taggedWorkoutEntry.mId || this.mSecondsInMinute != taggedWorkoutEntry.mSecondsInMinute || this.mStepsBeforeTagged != taggedWorkoutEntry.mStepsBeforeTagged || this.mCaloriesBeforeTagged != taggedWorkoutEntry.mCaloriesBeforeTagged || this.mHeartrate != taggedWorkoutEntry.mHeartrate || this.mState != taggedWorkoutEntry.mState || this.mType != taggedWorkoutEntry.mType || this.mStateChangeIndexInSeconds != taggedWorkoutEntry.mStateChangeIndexInSeconds) {
            return false;
        }
        if (((this.mAceLatencyInMinute != null || taggedWorkoutEntry.mAceLatencyInMinute != null) && ((num = this.mAceLatencyInMinute) == null || !num.equals(taggedWorkoutEntry.mAceLatencyInMinute))) || this.mIsRemainder != taggedWorkoutEntry.mIsRemainder) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public Integer getAceLatencyInMinute() {
        return this.mAceLatencyInMinute;
    }

    @DexIgnore
    public byte getCaloriesBeforeTagged() {
        return this.mCaloriesBeforeTagged;
    }

    @DexIgnore
    public short getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public boolean getIsRemainder() {
        return this.mIsRemainder;
    }

    @DexIgnore
    public byte getSecondsInMinute() {
        return this.mSecondsInMinute;
    }

    @DexIgnore
    public WorkoutState getState() {
        return this.mState;
    }

    @DexIgnore
    public int getStateChangeIndexInSeconds() {
        return this.mStateChangeIndexInSeconds;
    }

    @DexIgnore
    public short getStepsBeforeTagged() {
        return this.mStepsBeforeTagged;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((((((527 + this.mId) * 31) + this.mSecondsInMinute) * 31) + this.mStepsBeforeTagged) * 31) + this.mCaloriesBeforeTagged) * 31) + this.mHeartrate) * 31) + this.mState.hashCode()) * 31) + this.mType.hashCode()) * 31) + this.mStateChangeIndexInSeconds) * 31;
        Integer num = this.mAceLatencyInMinute;
        return ((hashCode + (num == null ? 0 : num.hashCode())) * 31) + (this.mIsRemainder ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return "TaggedWorkoutEntry{mId=" + this.mId + ",mSecondsInMinute=" + this.mSecondsInMinute + ",mStepsBeforeTagged=" + this.mStepsBeforeTagged + ",mCaloriesBeforeTagged=" + this.mCaloriesBeforeTagged + ",mHeartrate=" + this.mHeartrate + ",mState=" + this.mState + ",mType=" + this.mType + ",mStateChangeIndexInSeconds=" + this.mStateChangeIndexInSeconds + ",mAceLatencyInMinute=" + this.mAceLatencyInMinute + ",mIsRemainder=" + this.mIsRemainder + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
        parcel.writeByte(this.mSecondsInMinute);
        parcel.writeInt(this.mStepsBeforeTagged);
        parcel.writeByte(this.mCaloriesBeforeTagged);
        parcel.writeInt(this.mHeartrate);
        parcel.writeInt(this.mState.ordinal());
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mStateChangeIndexInSeconds);
        if (this.mAceLatencyInMinute != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAceLatencyInMinute.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeByte(this.mIsRemainder ? (byte) 1 : 0);
    }

    @DexIgnore
    public TaggedWorkoutEntry(Parcel parcel) {
        this.mId = parcel.readInt();
        this.mSecondsInMinute = parcel.readByte();
        this.mStepsBeforeTagged = (short) parcel.readInt();
        this.mCaloriesBeforeTagged = parcel.readByte();
        this.mHeartrate = (short) parcel.readInt();
        this.mState = WorkoutState.values()[parcel.readInt()];
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mStateChangeIndexInSeconds = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.mAceLatencyInMinute = null;
        } else {
            this.mAceLatencyInMinute = Integer.valueOf(parcel.readInt());
        }
        this.mIsRemainder = parcel.readByte() != 0;
    }
}
