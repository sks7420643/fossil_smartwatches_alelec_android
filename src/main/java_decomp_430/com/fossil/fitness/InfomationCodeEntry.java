package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InfomationCodeEntry implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<InfomationCodeEntry> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ InformationCode mCode;
    @DexIgnore
    public /* final */ short mMillisec;
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ short mTzOffsetInMinute;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<InfomationCodeEntry> {
        @DexIgnore
        public InfomationCodeEntry createFromParcel(Parcel parcel) {
            return new InfomationCodeEntry(parcel);
        }

        @DexIgnore
        public InfomationCodeEntry[] newArray(int i) {
            return new InfomationCodeEntry[i];
        }
    }

    @DexIgnore
    public InfomationCodeEntry(InformationCode informationCode, int i, short s, short s2) {
        this.mCode = informationCode;
        this.mTimestamp = i;
        this.mMillisec = s;
        this.mTzOffsetInMinute = s2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof InfomationCodeEntry)) {
            return false;
        }
        InfomationCodeEntry infomationCodeEntry = (InfomationCodeEntry) obj;
        if (this.mCode == infomationCodeEntry.mCode && this.mTimestamp == infomationCodeEntry.mTimestamp && this.mMillisec == infomationCodeEntry.mMillisec && this.mTzOffsetInMinute == infomationCodeEntry.mTzOffsetInMinute) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public InformationCode getCode() {
        return this.mCode;
    }

    @DexIgnore
    public short getMillisec() {
        return this.mMillisec;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public short getTzOffsetInMinute() {
        return this.mTzOffsetInMinute;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.mCode.hashCode()) * 31) + this.mTimestamp) * 31) + this.mMillisec) * 31) + this.mTzOffsetInMinute;
    }

    @DexIgnore
    public String toString() {
        return "InfomationCodeEntry{mCode=" + this.mCode + ",mTimestamp=" + this.mTimestamp + ",mMillisec=" + this.mMillisec + ",mTzOffsetInMinute=" + this.mTzOffsetInMinute + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mCode.ordinal());
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mMillisec);
        parcel.writeInt(this.mTzOffsetInMinute);
    }

    @DexIgnore
    public InfomationCodeEntry(Parcel parcel) {
        this.mCode = InformationCode.values()[parcel.readInt()];
        this.mTimestamp = parcel.readInt();
        this.mMillisec = (short) parcel.readInt();
        this.mTzOffsetInMinute = (short) parcel.readInt();
    }
}
