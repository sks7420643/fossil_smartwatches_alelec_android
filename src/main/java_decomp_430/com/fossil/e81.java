package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e81 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ xk1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e81(xk1 xk1) {
        super(1);
        this.a = xk1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        xk1 xk1 = this.a;
        xk1.K = ((bi0) obj).K;
        ue0.b.a(xk1.w.t).b(this.a.K);
        this.a.a(lx0.TRANSFER_DATA, lx0.GET_FILE_SIZE_WRITTEN);
        return cd6.a;
    }
}
