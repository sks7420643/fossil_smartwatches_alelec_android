package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.fossil.la;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ia {
    @DexIgnore
    public static int d;
    @DexIgnore
    public /* final */ AccessibilityNodeInfo a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a e; // = new a(1, (CharSequence) null);
        @DexIgnore
        public static /* final */ a f; // = new a(2, (CharSequence) null);
        @DexIgnore
        public static /* final */ a g; // = new a(16, (CharSequence) null);
        @DexIgnore
        public static /* final */ a h; // = new a(4096, (CharSequence) null);
        @DexIgnore
        public static /* final */ a i; // = new a(8192, (CharSequence) null);
        @DexIgnore
        public static /* final */ a j; // = new a(262144, (CharSequence) null);
        @DexIgnore
        public static /* final */ a k; // = new a(524288, (CharSequence) null);
        @DexIgnore
        public static /* final */ a l; // = new a(1048576, (CharSequence) null);
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Class<? extends la.a> c;
        @DexIgnore
        public /* final */ la d;

        /*
        static {
            Class<la.c> cls = la.c.class;
            Class<la.b> cls2 = la.b.class;
            AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new a(4, (CharSequence) null);
            new a(8, (CharSequence) null);
            new a(32, (CharSequence) null);
            new a(64, (CharSequence) null);
            new a(128, (CharSequence) null);
            new a(256, (CharSequence) null, cls2);
            new a(512, (CharSequence) null, cls2);
            new a(1024, (CharSequence) null, cls);
            new a(2048, (CharSequence) null, cls);
            new a(16384, (CharSequence) null);
            new a(32768, (CharSequence) null);
            new a(65536, (CharSequence) null);
            new a(131072, (CharSequence) null, la.g.class);
            new a(2097152, (CharSequence) null, la.h.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null, 16908342, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null, 16908343, (CharSequence) null, (la) null, la.e.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null, 16908344, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null, 16908345, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null, 16908346, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null, 16908347, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null, 16908348, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            new a(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null, 16908349, (CharSequence) null, (la) null, la.f.class);
            new a(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null, 16908354, (CharSequence) null, (la) null, la.d.class);
            new a(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null, 16908356, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
            if (Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new a(accessibilityAction, 16908357, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
        }
        */

        @DexIgnore
        public a(int i2, CharSequence charSequence) {
            this((Object) null, i2, charSequence, (la) null, (Class<? extends la.a>) null);
        }

        @DexIgnore
        public int a() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getId();
            }
            return 0;
        }

        @DexIgnore
        public CharSequence b() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getLabel();
            }
            return null;
        }

        @DexIgnore
        public a(Object obj) {
            this(obj, 0, (CharSequence) null, (la) null, (Class<? extends la.a>) null);
        }

        @DexIgnore
        public a(int i2, CharSequence charSequence, Class<? extends la.a> cls) {
            this((Object) null, i2, charSequence, (la) null, cls);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0028  */
        public boolean a(View view, Bundle bundle) {
            Class<? extends la.a> cls;
            String str;
            if (this.d == null) {
                return false;
            }
            la.a aVar = null;
            Class<? extends la.a> cls2 = this.c;
            if (cls2 != null) {
                try {
                    la.a aVar2 = (la.a) cls2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                    try {
                        aVar2.a(bundle);
                        aVar = aVar2;
                    } catch (Exception e2) {
                        e = e2;
                        aVar = aVar2;
                        cls = this.c;
                        if (cls != null) {
                        }
                        Log.e("A11yActionCompat", "Failed to execute command with argument class ViewCommandArgument: " + str, e);
                        return this.d.a(view, aVar);
                    }
                } catch (Exception e3) {
                    e = e3;
                    cls = this.c;
                    if (cls != null) {
                        str = "null";
                    } else {
                        str = cls.getName();
                    }
                    Log.e("A11yActionCompat", "Failed to execute command with argument class ViewCommandArgument: " + str, e);
                    return this.d.a(view, aVar);
                }
            }
            return this.d.a(view, aVar);
        }

        @DexIgnore
        public a(Object obj, int i2, CharSequence charSequence, la laVar, Class<? extends la.a> cls) {
            this.b = i2;
            this.d = laVar;
            if (Build.VERSION.SDK_INT < 21 || obj != null) {
                this.a = obj;
            } else {
                this.a = new AccessibilityNodeInfo.AccessibilityAction(i2, charSequence);
            }
            this.c = cls;
        }

        @DexIgnore
        public a a(CharSequence charSequence, la laVar) {
            return new a((Object) null, this.b, charSequence, laVar, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public c(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static c a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2));
            }
            if (i5 >= 19) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new c((Object) null);
        }
    }

    @DexIgnore
    public ia(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.a = accessibilityNodeInfo;
    }

    @DexIgnore
    public static ia B() {
        return a(AccessibilityNodeInfo.obtain());
    }

    @DexIgnore
    public static ia a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new ia(accessibilityNodeInfo);
    }

    @DexIgnore
    public static String d(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    @DexIgnore
    public static ia g(View view) {
        return a(AccessibilityNodeInfo.obtain(view));
    }

    @DexIgnore
    public AccessibilityNodeInfo A() {
        return this.a;
    }

    @DexIgnore
    public boolean b(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.a.removeAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
        return false;
    }

    @DexIgnore
    public void c(View view, int i) {
        this.c = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setSource(view, i);
        }
    }

    @DexIgnore
    public int d() {
        return this.a.getChildCount();
    }

    @DexIgnore
    public void e(View view) {
        this.b = -1;
        this.a.setParent(view);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ia.class != obj.getClass()) {
            return false;
        }
        ia iaVar = (ia) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            if (iaVar.a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(iaVar.a)) {
            return false;
        }
        return this.c == iaVar.c && this.b == iaVar.b;
    }

    @DexIgnore
    public void f(View view) {
        this.c = -1;
        this.a.setSource(view);
    }

    @DexIgnore
    public int h() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.getMovementGranularities();
        }
        return 0;
    }

    @DexIgnore
    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    @DexIgnore
    public void i(boolean z) {
        this.a.setFocusable(z);
    }

    @DexIgnore
    public void j(boolean z) {
        this.a.setFocused(z);
    }

    @DexIgnore
    public String k() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.a.getViewIdResourceName();
        }
        return null;
    }

    @DexIgnore
    public void l(boolean z) {
        this.a.setLongClickable(z);
    }

    @DexIgnore
    public boolean m() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isAccessibilityFocused();
        }
        return false;
    }

    @DexIgnore
    public boolean n() {
        return this.a.isCheckable();
    }

    @DexIgnore
    public boolean o() {
        return this.a.isChecked();
    }

    @DexIgnore
    public boolean p() {
        return this.a.isClickable();
    }

    @DexIgnore
    public void q(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setVisibleToUser(z);
        }
    }

    @DexIgnore
    public boolean r() {
        return this.a.isFocusable();
    }

    @DexIgnore
    public boolean s() {
        return this.a.isFocused();
    }

    @DexIgnore
    public boolean t() {
        return this.a.isLongClickable();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(i());
        sb.append("; className: ");
        sb.append(e());
        sb.append("; text: ");
        sb.append(j());
        sb.append("; contentDescription: ");
        sb.append(f());
        sb.append("; viewId: ");
        sb.append(k());
        sb.append("; checkable: ");
        sb.append(n());
        sb.append("; checked: ");
        sb.append(o());
        sb.append("; focusable: ");
        sb.append(r());
        sb.append("; focused: ");
        sb.append(s());
        sb.append("; selected: ");
        sb.append(w());
        sb.append("; clickable: ");
        sb.append(p());
        sb.append("; longClickable: ");
        sb.append(t());
        sb.append("; enabled: ");
        sb.append(q());
        sb.append("; password: ");
        sb.append(u());
        sb.append("; scrollable: " + v());
        sb.append("; [");
        int c2 = c();
        while (c2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(c2);
            c2 &= ~numberOfTrailingZeros;
            sb.append(d(numberOfTrailingZeros));
            if (c2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public boolean u() {
        return this.a.isPassword();
    }

    @DexIgnore
    public boolean v() {
        return this.a.isScrollable();
    }

    @DexIgnore
    public boolean w() {
        return this.a.isSelected();
    }

    @DexIgnore
    public boolean x() {
        if (Build.VERSION.SDK_INT >= 26) {
            return this.a.isShowingHintText();
        }
        return b(4);
    }

    @DexIgnore
    public boolean y() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isVisibleToUser();
        }
        return false;
    }

    @DexIgnore
    public void z() {
        this.a.recycle();
    }

    @DexIgnore
    public static ia a(ia iaVar) {
        return a(AccessibilityNodeInfo.obtain(iaVar.a));
    }

    @DexIgnore
    public void d(Rect rect) {
        this.a.setBoundsInScreen(rect);
    }

    @DexIgnore
    public void g(CharSequence charSequence) {
        this.a.setText(charSequence);
    }

    @DexIgnore
    public CharSequence i() {
        return this.a.getPackageName();
    }

    @DexIgnore
    public CharSequence j() {
        if (!l()) {
            return this.a.getText();
        }
        List<Integer> a2 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List<Integer> a3 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List<Integer> a4 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List<Integer> a5 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.a.getText(), 0, this.a.getText().length()));
        for (int i = 0; i < a2.size(); i++) {
            spannableString.setSpan(new ga(a5.get(i).intValue(), this, g().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), a2.get(i).intValue(), a3.get(i).intValue(), a4.get(i).intValue());
        }
        return spannableString;
    }

    @DexIgnore
    public final boolean l() {
        return !a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty();
    }

    @DexIgnore
    public void n(boolean z) {
        this.a.setScrollable(z);
    }

    @DexIgnore
    public void o(boolean z) {
        this.a.setSelected(z);
    }

    @DexIgnore
    public void p(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.a.setShowingHintText(z);
        } else {
            a(4, z);
        }
    }

    @DexIgnore
    public void a(View view) {
        this.a.addChild(view);
    }

    @DexIgnore
    public void b(View view, int i) {
        this.b = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setParent(view, i);
        }
    }

    @DexIgnore
    public void d(boolean z) {
        this.a.setChecked(z);
    }

    @DexIgnore
    public void e(boolean z) {
        this.a.setClickable(z);
    }

    @DexIgnore
    public CharSequence f() {
        return this.a.getContentDescription();
    }

    @DexIgnore
    public Bundle g() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.a.getExtras();
        }
        return new Bundle();
    }

    @DexIgnore
    public void h(boolean z) {
        this.a.setEnabled(z);
    }

    @DexIgnore
    public void k(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setHeading(z);
        } else {
            a(2, z);
        }
    }

    @DexIgnore
    public void m(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setScreenReaderFocusable(z);
        } else {
            a(1, z);
        }
    }

    @DexIgnore
    public boolean q() {
        return this.a.isEnabled();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static b a(int i, int i2, boolean z, int i3) {
            int i4 = Build.VERSION.SDK_INT;
            if (i4 >= 21) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3));
            }
            if (i4 >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new b((Object) null);
        }

        @DexIgnore
        public static b a(int i, int i2, boolean z) {
            if (Build.VERSION.SDK_INT >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new b((Object) null);
        }
    }

    @DexIgnore
    public static ClickableSpan[] h(CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
        }
        return null;
    }

    @DexIgnore
    public void a(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.addChild(view, i);
        }
    }

    @DexIgnore
    public int c() {
        return this.a.getActions();
    }

    @DexIgnore
    public final void d(View view) {
        SparseArray<WeakReference<ClickableSpan>> c2 = c(view);
        if (c2 != null) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < c2.size(); i++) {
                if (c2.valueAt(i).get() == null) {
                    arrayList.add(Integer.valueOf(i));
                }
            }
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                c2.remove(((Integer) arrayList.get(i2)).intValue());
            }
        }
    }

    @DexIgnore
    public void e(CharSequence charSequence) {
        this.a.setPackageName(charSequence);
    }

    @DexIgnore
    public void f(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setContentInvalid(z);
        }
    }

    @DexIgnore
    public void c(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setMovementGranularities(i);
        }
    }

    @DexIgnore
    public CharSequence e() {
        return this.a.getClassName();
    }

    @DexIgnore
    public void a(int i) {
        this.a.addAction(i);
    }

    @DexIgnore
    public void b(Rect rect) {
        this.a.getBoundsInScreen(rect);
    }

    @DexIgnore
    public void f(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.a.setPaneTitle(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    @DexIgnore
    public void g(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setDismissable(z);
        }
    }

    @DexIgnore
    public final List<Integer> a(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.a.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList arrayList = new ArrayList();
        this.a.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> b(View view) {
        SparseArray<WeakReference<ClickableSpan>> c2 = c(view);
        if (c2 != null) {
            return c2;
        }
        SparseArray<WeakReference<ClickableSpan>> sparseArray = new SparseArray<>();
        view.setTag(f6.tag_accessibility_clickable_spans, sparseArray);
        return sparseArray;
    }

    @DexIgnore
    public void c(Rect rect) {
        this.a.setBoundsInParent(rect);
    }

    @DexIgnore
    public void c(boolean z) {
        this.a.setCheckable(z);
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> c(View view) {
        return (SparseArray) view.getTag(f6.tag_accessibility_clickable_spans);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a.setContentDescription(charSequence);
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.setError(charSequence);
        }
    }

    @DexIgnore
    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionItemInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionItemInfo) ((c) obj).a);
        }
    }

    @DexIgnore
    public void d(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.a.setHintText(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    @DexIgnore
    public void a(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.addAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
    }

    @DexIgnore
    public List<a> b() {
        List<AccessibilityNodeInfo.AccessibilityAction> actionList = Build.VERSION.SDK_INT >= 21 ? this.a.getActionList() : null;
        if (actionList == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        int size = actionList.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(new a(actionList.get(i)));
        }
        return arrayList;
    }

    @DexIgnore
    public boolean a(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.performAction(i, bundle);
        }
        return false;
    }

    @DexIgnore
    public void a(Rect rect) {
        this.a.getBoundsInParent(rect);
    }

    @DexIgnore
    public void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setAccessibilityFocused(z);
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.a.setClassName(charSequence);
    }

    @DexIgnore
    public void b(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCanOpenPopup(z);
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence, View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19 && i < 26) {
            a();
            d(view);
            ClickableSpan[] h = h(charSequence);
            if (h != null && h.length > 0) {
                g().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", f6.accessibility_action_clickable_span);
                SparseArray<WeakReference<ClickableSpan>> b2 = b(view);
                int i2 = 0;
                while (h != null && i2 < h.length) {
                    int a2 = a(h[i2], b2);
                    b2.put(a2, new WeakReference(h[i2]));
                    a(h[i2], (Spanned) charSequence, a2);
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public final boolean b(int i) {
        Bundle g = g();
        if (g != null && (g.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & i) == i) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int a(ClickableSpan clickableSpan, SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); i++) {
                if (clickableSpan.equals((ClickableSpan) sparseArray.valueAt(i).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        int i2 = d;
        d = i2 + 1;
        return i2;
    }

    @DexIgnore
    public final void a() {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }

    @DexIgnore
    public final void a(ClickableSpan clickableSpan, Spanned spanned, int i) {
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionInfo) ((b) obj).a);
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        Bundle g = g();
        if (g != null) {
            int i2 = g.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (~i);
            if (!z) {
                i = 0;
            }
            g.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
