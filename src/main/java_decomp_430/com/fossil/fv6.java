package com.fossil;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fv6 implements Iterable<jv6> {
    @DexIgnore
    public /* final */ List<jv6> a; // = new LinkedList();
    @DexIgnore
    public /* final */ Map<String, List<jv6>> b; // = new HashMap();

    @DexIgnore
    public void a(jv6 jv6) {
        if (jv6 != null) {
            String lowerCase = jv6.b().toLowerCase(Locale.US);
            List list = this.b.get(lowerCase);
            if (list == null) {
                list = new LinkedList();
                this.b.put(lowerCase, list);
            }
            list.add(jv6);
            this.a.add(jv6);
        }
    }

    @DexIgnore
    public Iterator<jv6> iterator() {
        return Collections.unmodifiableList(this.a).iterator();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public jv6 a(String str) {
        List list;
        if (str == null || (list = this.b.get(str.toLowerCase(Locale.US))) == null || list.isEmpty()) {
            return null;
        }
        return (jv6) list.get(0);
    }
}
