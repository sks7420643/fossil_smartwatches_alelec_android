package com.fossil;

import android.text.TextUtils;
import com.fossil.a35;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {129}, m = "invokeSuspend")
public final class r15$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ a35.d $responseValue;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $contactWrapperList;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ r15$c$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(r15$c$a r15_c_a, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = r15_c_a;
            this.$contactWrapperList = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$contactWrapperList, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Contact contact;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                for (ContactGroup next : this.this$0.$responseValue.a()) {
                    for (Contact contact2 : next.getContacts()) {
                        if (next.getHour() == this.this$0.this$0.a.p) {
                            wx4 wx4 = new wx4(contact2, "");
                            wx4.setAdded(true);
                            wg6.a((Object) contact2, "contact");
                            ContactGroup contactGroup = contact2.getContactGroup();
                            wg6.a((Object) contactGroup, "contact.contactGroup");
                            wx4.setCurrentHandGroup(contactGroup.getHour());
                            Contact contact3 = wx4.getContact();
                            if (contact3 != null) {
                                contact3.setDbRowId(contact2.getDbRowId());
                                contact3.setUseSms(contact2.isUseSms());
                                contact3.setUseCall(contact2.isUseCall());
                            }
                            List phoneNumbers = contact2.getPhoneNumbers();
                            wg6.a((Object) phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                Object obj2 = contact2.getPhoneNumbers().get(0);
                                wg6.a(obj2, "contact.phoneNumbers[0]");
                                String number = ((PhoneNumber) obj2).getNumber();
                                if (!TextUtils.isEmpty(number)) {
                                    wx4.setHasPhoneNumber(true);
                                    wx4.setPhoneNumber(number);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a = NotificationContactsAndAppsAssignedPresenter.w.a();
                                    local.d(a, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a2 = NotificationContactsAndAppsAssignedPresenter.w.a();
                            StringBuilder sb = new StringBuilder();
                            sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                            ContactGroup contactGroup2 = contact2.getContactGroup();
                            wg6.a((Object) contactGroup2, "contact.contactGroup");
                            sb.append(contactGroup2.getHour());
                            sb.append(" ,rowId=");
                            sb.append(contact2.getDbRowId());
                            sb.append(" ,isUseText=");
                            sb.append(contact2.isUseSms());
                            sb.append(" ,isUseCall=");
                            sb.append(contact2.isUseCall());
                            local2.d(a2, sb.toString());
                            this.$contactWrapperList.add(wx4);
                            Contact contact4 = wx4.getContact();
                            if ((contact4 != null && contact4.getContactId() == -100) || ((contact = wx4.getContact()) != null && contact.getContactId() == -200)) {
                                this.this$0.this$0.a.r().add(wx4);
                            }
                        }
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r15$c$a(NotificationContactsAndAppsAssignedPresenter.c cVar, a35.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        r15$c$a r15_c_a = new r15$c$a(this.this$0, this.$responseValue, xe6);
        r15_c_a.p$ = (il6) obj;
        return r15_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((r15$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetAllHybridContactGroups onSuccess");
            ArrayList arrayList = new ArrayList();
            rl6 a3 = ik6.a(il6, this.this$0.a.b(), (ll6) null, new a(this, arrayList, (xe6) null), 2, (Object) null);
            this.L$0 = il6;
            this.L$1 = arrayList;
            this.L$2 = a3;
            this.label = 1;
            if (a3.a(this) == a2) {
                return a2;
            }
            list = arrayList;
        } else if (i == 1) {
            rl6 rl6 = (rl6) this.L$2;
            list = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.a.o().addAll(list);
        this.this$0.a.q().addAll(list);
        List<Object> p = this.this$0.a.p();
        Object[] array = list.toArray(new wx4[0]);
        if (array != null) {
            Serializable a4 = av6.a((Serializable) array);
            wg6.a((Object) a4, "SerializationUtils.clone\u2026apperList.toTypedArray())");
            vd6.a(p, (T[]) (Object[]) a4);
            this.this$0.a.t();
            return cd6.a;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
