package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l63 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ j03 a;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 b;
    @DexIgnore
    public /* final */ /* synthetic */ d63 c;

    @DexIgnore
    public l63(d63 d63, j03 j03, ra3 ra3) {
        this.c = d63;
        this.a = j03;
        this.b = ra3;
    }

    @DexIgnore
    public final void run() {
        j03 b2 = this.c.b(this.a, this.b);
        this.c.a.t();
        this.c.a.a(b2, this.b);
    }
}
