package com.fossil;

import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class iq implements lq {
    @DexIgnore
    public /* final */ HttpClient a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends HttpEntityEnclosingRequestBase {
        @DexIgnore
        public a(String str) {
            setURI(URI.create(str));
        }

        @DexIgnore
        public String getMethod() {
            return "PATCH";
        }
    }

    @DexIgnore
    public iq(HttpClient httpClient) {
        this.a = httpClient;
    }

    @DexIgnore
    public static void a(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    @DexIgnore
    public static HttpUriRequest b(up<?> upVar, Map<String, String> map) throws hp {
        switch (upVar.getMethod()) {
            case -1:
                byte[] postBody = upVar.getPostBody();
                if (postBody == null) {
                    return new HttpGet(upVar.getUrl());
                }
                HttpPost httpPost = new HttpPost(upVar.getUrl());
                httpPost.addHeader("Content-Type", upVar.getPostBodyContentType());
                httpPost.setEntity(new ByteArrayEntity(postBody));
                return httpPost;
            case 0:
                return new HttpGet(upVar.getUrl());
            case 1:
                HttpPost httpPost2 = new HttpPost(upVar.getUrl());
                httpPost2.addHeader("Content-Type", upVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPost2, upVar);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(upVar.getUrl());
                httpPut.addHeader("Content-Type", upVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPut, upVar);
                return httpPut;
            case 3:
                return new HttpDelete(upVar.getUrl());
            case 4:
                return new HttpHead(upVar.getUrl());
            case 5:
                return new HttpOptions(upVar.getUrl());
            case 6:
                return new HttpTrace(upVar.getUrl());
            case 7:
                a aVar = new a(upVar.getUrl());
                aVar.addHeader("Content-Type", upVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) aVar, upVar);
                return aVar;
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    @DexIgnore
    public void a(HttpUriRequest httpUriRequest) throws IOException {
    }

    @DexIgnore
    public HttpResponse a(up<?> upVar, Map<String, String> map) throws IOException, hp {
        HttpUriRequest b = b(upVar, map);
        a(b, map);
        a(b, upVar.getHeaders());
        a(b);
        HttpParams params = b.getParams();
        int timeoutMs = upVar.getTimeoutMs();
        HttpConnectionParams.setConnectionTimeout(params, VideoUploader.RETRY_DELAY_UNIT_MS);
        HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.a.execute(b);
    }

    @DexIgnore
    public static void a(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, up<?> upVar) throws hp {
        byte[] body = upVar.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(body));
        }
    }
}
