package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm6 extends tm6<rm6> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(pm6.class, "_invoked");
    @DexIgnore
    public volatile int _invoked; // = 0;
    @DexIgnore
    public /* final */ hg6<Throwable, cd6> e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pm6(rm6 rm6, hg6<? super Throwable, cd6> hg6) {
        super(rm6);
        wg6.b(rm6, "job");
        wg6.b(hg6, "handler");
        this.e = hg6;
    }

    @DexIgnore
    public void b(Throwable th) {
        if (f.compareAndSet(this, 0, 1)) {
            this.e.invoke(th);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancelling[" + ol6.a((Object) this) + '@' + ol6.b(this) + ']';
    }
}
