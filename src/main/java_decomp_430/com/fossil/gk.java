package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gk extends fk {
    @DexIgnore
    public static boolean g; // = true;
    @DexIgnore
    public static boolean h; // = true;
    @DexIgnore
    public static boolean i; // = true;

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void a(View view, Matrix matrix) {
        if (g) {
            try {
                view.setAnimationMatrix(matrix);
            } catch (NoSuchMethodError unused) {
                g = false;
            }
        }
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void b(View view, Matrix matrix) {
        if (h) {
            try {
                view.transformMatrixToGlobal(matrix);
            } catch (NoSuchMethodError unused) {
                h = false;
            }
        }
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void c(View view, Matrix matrix) {
        if (i) {
            try {
                view.transformMatrixToLocal(matrix);
            } catch (NoSuchMethodError unused) {
                i = false;
            }
        }
    }
}
