package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1$allCategory$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
public final class t45$j$a extends sf6 implements ig6<il6, xe6<? super List<? extends Category>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter.j this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t45$j$a(ComplicationsPresenter.j jVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = jVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        t45$j$a t45_j_a = new t45$j$a(this.this$0, xe6);
        t45_j_a.p$ = (il6) obj;
        return t45_j_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((t45$j$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.u.getAllCategories();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
