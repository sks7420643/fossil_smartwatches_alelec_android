package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import com.fossil.fitness.FitnessData;
import com.fossil.ja0;
import com.fossil.k40;
import com.fossil.q40;
import com.fossil.r40;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii1 implements q40, ib0, wa0, bb0, db0, ga0, fb0, kb0, ja0, ka0, la0, na0, mb0, oa0, pa0, ra0, ua0, va0, fd1, af1, wg1, qa0, nb0, ha0, ya0, za0, pb0, ab0, ub0, xb0, rb0, ta0, ma0, eb0, gb0, hb0, ob0, jb0, lb0, vb0, wb0, sb0, sa0, tb0, ia0, xa0, ea0, cb0, fa0, qb0 {
    @DexIgnore
    public static /* final */ iq0 CREATOR; // = new iq0((qg6) null);
    @DexIgnore
    public /* final */ q41 a; // = new q41(this);
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ ue1 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ uk0 f;
    @DexIgnore
    public /* final */ t51 g;
    @DexIgnore
    public /* final */ u21 h;
    @DexIgnore
    public /* final */ hg6<em0, cd6> i;
    @DexIgnore
    public /* final */ ig6<byte[], rg1, cd6> j;
    @DexIgnore
    public String o;
    @DexIgnore
    public /* final */ Object p;
    @DexIgnore
    public ja0.a q;
    @DexIgnore
    public r40 r;
    @DexIgnore
    public q40.c s;
    @DexIgnore
    public q40.b t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ BluetoothDevice v;
    @DexIgnore
    public /* final */ String w;

    @DexIgnore
    public /* synthetic */ ii1(BluetoothDevice bluetoothDevice, String str, qg6 qg6) {
        this.v = bluetoothDevice;
        this.w = str;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.c = u11.b.a(this.v);
            this.f = new uk0(new nj1(new Hashtable(), (qg6) null));
            this.g = new t51();
            this.h = new u21(this);
            this.i = new ng0(this);
            this.j = new we0(this);
            this.o = ze0.a("UUID.randomUUID().toString()");
            this.p = new Object();
            this.r = new r40(this.c.d(), this.c.t, this.w, "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
            this.s = q40.c.DISCONNECTED;
            ue1 ue1 = this.c;
            u21 u21 = this.h;
            if (!ue1.h.contains(u21)) {
                ue1.h.add(u21);
            }
            r40 r40 = this.r;
            xq0.e.a(r40.getMacAddress(), r40);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ void a(ii1 ii1, cc0 cc0, String str, String str2, Object... objArr) {
    }

    @DexIgnore
    public final void a(ac0 ac0) {
    }

    @DexIgnore
    public zb0<byte[]> c(byte[] bArr) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a(bArr, (String) null, 1);
        zp0 zp0 = new zp0(this.c, this.a, bArr);
        zp0.c(new bg0(this));
        synchronized (this.p) {
            u40 a2 = a((if1) zp0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                zp0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new jp0(this, zp0));
                br0 br0 = new br0(this, zp0);
                if (!zp0.t) {
                    zp0.d.add(br0);
                }
                mu0 mu0 = new mu0(yb0, this, zp0);
                if (!zp0.t) {
                    zp0.h.add(mu0);
                }
                zp0.a((hg6<? super if1, cd6>) new ke0(yb0, this, zp0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                zp0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> cleanUp() {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        mk1 mk1 = new mk1(this.c, this.a);
        synchronized (this.p) {
            u40 a2 = a((if1) mk1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                mk1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new un0(this, mk1));
                mp0 mp0 = new mp0(this, mk1);
                if (!mk1.t) {
                    mk1.d.add(mp0);
                }
                xs0 xs0 = new xs0(yb0, this, mk1);
                if (!mk1.t) {
                    mk1.h.add(xs0);
                }
                mk1.a((hg6<? super if1, cd6>) new o21(yb0, this, mk1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                mk1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public void d(byte[] bArr) {
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a(bArr, (String) null, 1);
        qs0.h.a(new nn0(cw0.a((Enum<?>) bs0.SET_SECRET_KEY), og0.REQUEST, this.c.t, cw0.a((Enum<?>) bs0.SET_SECRET_KEY), ze0.a("UUID.randomUUID().toString()"), true, this.o, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.SECRET_KEY_CRC, (Object) Long.valueOf(h51.a.a(bArr, q11.CRC32))), 384));
        ue0.b.a(this.c.t, bArr);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public zb0<cd6> e() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        zb0<cd6> zb0 = new zb0<>();
        cf1 cf1 = new cf1(this.c, this.a, ze0.a("UUID.randomUUID().toString()"));
        synchronized (this.p) {
            u40 a2 = a((if1) cf1);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                cf1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new me1(this, cf1));
                ig1 ig1 = new ig1(this, cf1);
                if (!cf1.t) {
                    cf1.d.add(ig1);
                }
                xj1 xj1 = new xj1(yb0, this, cf1);
                if (!cf1.t) {
                    cf1.h.add(xj1);
                }
                cf1.a((hg6<? super if1, cd6>) new au0(yb0, this, cf1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                cf1.l();
            }
        }
        yb0.e(new tv0(zb0)).d((hg6<? super ac0, cd6>) new ox0(zb0));
        return zb0;
    }

    @DexIgnore
    public byte[] f() {
        return ue0.b.a(this.c.t).a;
    }

    @DexIgnore
    public boolean g() {
        cc0 cc0 = cc0.DEBUG;
        if (!isActive()) {
            kj0.i.e(this);
        }
        qs0.h.a(new nn0(cw0.a((Enum<?>) bs0.ENABLE_MAINTAINING_CONNECTION), og0.REQUEST, this.c.t, cw0.a((Enum<?>) bs0.ENABLE_MAINTAINING_CONNECTION), ze0.a("UUID.randomUUID().toString()"), true, this.o, (r40) null, (bw0) null, (JSONObject) null, 896));
        c(true);
        return true;
    }

    @DexIgnore
    public q40.a getBondState() {
        return q40.a.b.a(this.c.getBondState());
    }

    @DexIgnore
    public q40.c getState() {
        return this.s;
    }

    @DexIgnore
    public zb0<Integer> h() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        yr0 yr0 = new yr0(this.c, this.a);
        synchronized (this.p) {
            u40 a2 = a((if1) yr0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                yr0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new lg1(this, yr0));
                gi1 gi1 = new gi1(this, yr0);
                if (!yr0.t) {
                    yr0.d.add(gi1);
                }
                tl1 tl1 = new tl1(yb0, this, yr0);
                if (!yr0.t) {
                    yr0.h.add(tl1);
                }
                yr0.a((hg6<? super if1, cd6>) new ij1(yb0, this, yr0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                yr0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> i() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        ue1 ue1 = this.c;
        wm0 wm0 = new wm0(ue1, this.a, eh1.RELEASE_HANDS, new pq0(ue1));
        synchronized (this.p) {
            u40 a2 = a((if1) wm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                wm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new yg0(this, wm0));
                qi0 qi0 = new qi0(this, wm0);
                if (!wm0.t) {
                    wm0.d.add(qi0);
                }
                dm0 dm0 = new dm0(yb0, this, wm0);
                if (!wm0.t) {
                    wm0.h.add(dm0);
                }
                wm0.a((hg6<? super if1, cd6>) new hg0(yb0, this, wm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                wm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public boolean isActive() {
        return kj0.i.b(this);
    }

    @DexIgnore
    public zb0<cd6> j() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        ue1 ue1 = this.c;
        wm0 wm0 = new wm0(ue1, this.a, eh1.REQUEST_HANDS, new is0(ue1));
        synchronized (this.p) {
            u40 a2 = a((if1) wm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                wm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new ab1(this, wm0));
                vc1 vc1 = new vc1(this, wm0);
                if (!wm0.t) {
                    wm0.d.add(vc1);
                }
                mg1 mg1 = new mg1(yb0, this, wm0);
                if (!wm0.t) {
                    wm0.h.add(mg1);
                }
                wm0.a((hg6<? super if1, cd6>) new en0(yb0, this, wm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                wm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> k() {
        cc0 cc0 = cc0.DEBUG;
        qs0.h.a(new nn0(cw0.a((Enum<?>) bs0.CLEAR_CACHE), og0.DEVICE_EVENT, this.c.t, "", "", true, (String) null, (r40) null, (bw0) null, (JSONObject) null, 960));
        qz0.a.a(this.c.t);
        return new zb0<>();
    }

    @DexIgnore
    public r40 l() {
        return this.r;
    }

    @DexIgnore
    public yb0<HashMap<s60, r60>> m() {
        yb0<HashMap<s60, r60>> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        ue1 ue1 = this.c;
        ok1 ok1 = new ok1(ue1, this.a, lk1.b.a(ue1.t, w31.DEVICE_CONFIG));
        synchronized (this.p) {
            u40 a2 = a((if1) ok1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                ok1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new d91(this, ok1));
                za1 za1 = new za1(this, ok1);
                if (!ok1.t) {
                    ok1.d.add(za1);
                }
                pe1 pe1 = new pe1(yb0, this, ok1);
                if (!ok1.t) {
                    ok1.h.add(pe1);
                }
                ok1.a((hg6<? super if1, cd6>) new w01(yb0, this, ok1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                ok1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<c90> n() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        df1 df1 = new df1(this.c, this.a);
        synchronized (this.p) {
            u40 a2 = a((if1) df1);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                df1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new k11(this, df1));
                f31 f31 = new f31(this, df1);
                if (!df1.t) {
                    df1.d.add(f31);
                }
                y61 y61 = new y61(yb0, this, df1);
                if (!df1.t) {
                    df1.h.add(y61);
                }
                df1.a((hg6<? super if1, cd6>) new f81(yb0, this, df1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                df1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> o() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        ue1 ue1 = this.c;
        wm0 wm0 = new wm0(ue1, this.a, eh1.SET_CALIBRATION_POSITION, new bu0(ue1));
        synchronized (this.p) {
            u40 a2 = a((if1) wm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                wm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new vn0(this, wm0));
                np0 np0 = new np0(this, wm0);
                if (!wm0.t) {
                    wm0.d.add(np0);
                }
                ys0 ys0 = new ys0(yb0, this, wm0);
                if (!wm0.t) {
                    wm0.h.add(ys0);
                }
                wm0.a((hg6<? super if1, cd6>) new n81(yb0, this, wm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                wm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final boolean p() {
        boolean z;
        synchronized (Boolean.valueOf(this.e)) {
            z = this.e;
        }
        return z;
    }

    @DexIgnore
    public final void q() {
        cc0 cc0 = cc0.DEBUG;
        this.f.a(sk1.INTERRUPTED, new eh1[]{eh1.STREAMING});
        if (this.s != q40.c.DISCONNECTED && !this.d) {
            gm1 gm1 = new gm1(this.c, this.a);
            gm1.a((hg6<? super if1, cd6>) new s21(this));
            gm1.l();
        }
    }

    @DexIgnore
    public final void r() {
        r40 r40 = r1;
        r40 r402 = new r40(this.r.getName(), this.r.getMacAddress(), this.r.getSerialNumber(), (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262136);
        this.r = r40;
    }

    @DexIgnore
    public final void s() {
        ub1 ub1 = new ub1(this.c, this.a);
        ub1.B = this.i;
        ub1.C = this.j;
        ub1.c(u61.a);
        ub1.b((hg6<? super if1, cd6>) new r81(this));
        ub1.l();
    }

    @DexIgnore
    public final zb0<cd6> t() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        jq0 jq0 = new jq0(this.c, this.a, (HashMap) null, 4);
        synchronized (this.p) {
            u40 a2 = a((if1) jq0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                jq0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new f91(this, jq0));
                i11 i11 = new i11(this, jq0);
                if (!jq0.t) {
                    jq0.d.add(i11);
                }
                ye0 ye0 = new ye0(yb0, this, jq0);
                if (!jq0.t) {
                    jq0.h.add(ye0);
                }
                jq0.a((hg6<? super if1, cd6>) new aw0(yb0, this, jq0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                jq0.l();
            }
        }
        yb0.f(vx0.a);
        return yb0;
    }

    @DexIgnore
    public boolean u() {
        boolean z;
        synchronized (Boolean.valueOf(this.u)) {
            z = this.u;
        }
        return z;
    }

    @DexIgnore
    public q40.b v() {
        return this.t;
    }

    @DexIgnore
    public q40.d w() {
        return q40.d.f.a(this.c.e());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.v, i2);
        }
        if (parcel != null) {
            parcel.writeString(this.w);
        }
    }

    @DexIgnore
    public <T> T a(Class<T> cls) {
        if (nd6.a(this.r.getDeviceType().a(), cls)) {
            cc0 cc0 = cc0.DEBUG;
            new Object[1][0] = cls.getSimpleName();
            return this;
        }
        cc0 cc02 = cc0.DEBUG;
        new Object[1][0] = cls.getSimpleName();
        return null;
    }

    @DexIgnore
    public zb0<cd6> b() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        zi1 zi1 = new zi1(this.c, this.a);
        synchronized (this.p) {
            u40 a2 = a((if1) zi1);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                zi1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new ff0(this, zi1));
                wg0 wg0 = new wg0(this, zi1);
                if (!zi1.t) {
                    zi1.d.add(wg0);
                }
                ik0 ik0 = new ik0(yb0, this, zi1);
                if (!zi1.t) {
                    zi1.h.add(ik0);
                }
                zi1.a((hg6<? super if1, cd6>) new bc1(yb0, this, zi1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                zi1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(nd0[] nd0Arr) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((p40[]) nd0Arr).toString(2);
        fm1 fm1 = new fm1(this.c, this.a, nd0Arr);
        synchronized (this.p) {
            u40 a2 = a((if1) fm1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                fm1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new vj1(this, fm1));
                pl1 pl1 = new pl1(this, fm1);
                if (!fm1.t) {
                    fm1.d.add(pl1);
                }
                df0 df0 = new df0(yb0, this, fm1);
                if (!fm1.t) {
                    fm1.h.add(df0);
                }
                fm1.a((hg6<? super if1, cd6>) new aa1(yb0, this, fm1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                fm1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> d() {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        t01 t01 = new t01(this.c, this.a, ze0.a("UUID.randomUUID().toString()"));
        synchronized (this.p) {
            u40 a2 = a((if1) t01);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                t01.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new v81(this, t01));
                ra1 ra1 = new ra1(this, t01);
                if (!t01.t) {
                    t01.d.add(ra1);
                }
                he1 he1 = new he1(yb0, this, t01);
                if (!t01.t) {
                    t01.h.add(he1);
                }
                t01.a((hg6<? super if1, cd6>) new zf1(yb0, this, t01));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                t01.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<Boolean> b(byte[] bArr) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = Long.valueOf(h51.a.a(bArr, q11.CRC32));
        so0 so0 = new so0(this.c, this.a, bArr);
        synchronized (this.p) {
            u40 a2 = a((if1) so0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                so0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new zx0(this, so0));
                sz0 sz0 = new sz0(this, so0);
                if (!so0.t) {
                    so0.d.add(sz0);
                }
                i31 i31 = new i31(yb0, this, so0);
                if (!so0.t) {
                    so0.h.add(i31);
                }
                so0.a((hg6<? super if1, cd6>) new ng1(yb0, this, so0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                so0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<r40> c() {
        yb0<r40> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        if (this.s == q40.c.CONNECTED) {
            yb0<r40> yb02 = new yb0<>();
            yb02.a(1.0f);
            yb02.c(this.r);
            return yb02;
        }
        e21 e21 = new e21(this.c, this.a, ze0.a("UUID.randomUUID().toString()"));
        synchronized (this.p) {
            u40 a2 = a((if1) e21);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                e21.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new f71(this, e21));
                tc1 tc1 = new tc1(this, e21);
                if (!e21.t) {
                    e21.d.add(tc1);
                }
                kg1 kg1 = new kg1(yb0, this, e21);
                if (!e21.t) {
                    e21.h.add(kg1);
                }
                e21.a((hg6<? super if1, cd6>) new zm0(yb0, this, e21));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                e21.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> a(long j2) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = Long.valueOf(j2);
        yp0 yp0 = new yp0(this.c, this.a, j2);
        synchronized (this.p) {
            u40 a2 = a((if1) yp0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                yp0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new k31(this, yp0));
                a91 a91 = new a91(this, yp0);
                if (!yp0.t) {
                    yp0.d.add(a91);
                }
                rc1 rc1 = new rc1(yb0, this, yp0);
                if (!yp0.t) {
                    yp0.h.add(rc1);
                }
                yp0.a((hg6<? super if1, cd6>) new ih1(yb0, this, yp0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                yp0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final v40<r40, km1> b(HashMap<d41, Object> hashMap) {
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = hashMap;
        jh6 jh6 = new jh6();
        jh6.element = null;
        v40<r40, km1> a2 = new v40().a(new ea1(jh6));
        if (this.s == q40.c.CONNECTED) {
            a2.c(this.r);
        } else if (!this.d) {
            this.o = ze0.a("UUID.randomUUID().toString()");
            xq0.e.a(this.r.getMacAddress(), this.o);
            jh6.element = new al0(this.c, this.a, hashMap, ze0.a("UUID.randomUUID().toString()"));
            if1 if1 = (if1) jh6.element;
            o41 o41 = new o41(this);
            if (!if1.t) {
                if1.d.add(o41);
            }
            if1.c(new k61(this, a2));
            if1.b((hg6<? super if1, cd6>) new h81(this, a2));
            ((if1) jh6.element).l();
        } else {
            cc0 cc02 = cc0.DEBUG;
            new Object[1][0] = true;
            a2.b(new km1(eh1.MAKE_DEVICE_READY, sk1.NOT_ALLOW_TO_START, (bn0) null, 4));
        }
        return a2;
    }

    @DexIgnore
    public boolean a() {
        cc0 cc0 = cc0.DEBUG;
        if (isActive()) {
            kj0.i.f(this);
        }
        qs0.h.a(new nn0(cw0.a((Enum<?>) bs0.APP_DISCONNECT), og0.REQUEST, this.c.t, cw0.a((Enum<?>) bs0.APP_DISCONNECT), ze0.a("UUID.randomUUID().toString()"), true, this.o, (r40) null, (bw0) null, (JSONObject) null, 896));
        q();
        u();
        c(false);
        return true;
    }

    @DexIgnore
    public void c(boolean z) {
        qs0.h.a(new nn0(cw0.a((Enum<?>) bs0.ENABLE_BACKGROUND_SYNC), og0.DEVICE_EVENT, this.c.t, "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.VALUE, (Object) Boolean.valueOf(z)), 448));
        synchronized (Boolean.valueOf(this.u)) {
            this.u = z;
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public zb0<cd6> a(r50 r50, o50[] o50Arr) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {r50, cw0.a((p40[]) o50Arr).toString(2)};
        ue1 ue1 = this.c;
        wm0 wm0 = new wm0(ue1, this.a, eh1.MOVE_HANDS, new xo0(ue1, r50, o50Arr));
        synchronized (this.p) {
            u40 a2 = a((if1) wm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                wm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new bk1(this, wm0));
                ul1 ul1 = new ul1(this, wm0);
                if (!wm0.t) {
                    wm0.d.add(ul1);
                }
                hf0 hf0 = new hf0(yb0, this, wm0);
                if (!wm0.t) {
                    wm0.h.add(hf0);
                }
                wm0.a((hg6<? super if1, cd6>) new mh1(yb0, this, wm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                wm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void b(boolean z) {
        synchronized (Boolean.valueOf(this.e)) {
            this.e = z;
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public static /* synthetic */ yb0 b(ii1 ii1, boolean z, boolean z2, y11 y11, int i2) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                y11 = y11.LOW;
            } else {
                y11 = y11.NORMAL;
            }
        }
        return ii1.b(z, z2, y11);
    }

    @DexIgnore
    public final yb0<byte[][]> b(boolean z, boolean z2, y11 y11) {
        r91 r91;
        yb0<byte[][]> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {Boolean.valueOf(z), y11, Boolean.valueOf(z2)};
        if (z2) {
            r91 = new u71(this.c, this.a, he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, false), qc6.a(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS, false)}));
        } else {
            r91 r912 = new r91(this.c, this.a, he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, Boolean.valueOf(z)), qc6.a(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))}), (String) null, 8);
            r912.a(y11);
            r91 = r912;
        }
        synchronized (this.p) {
            u40 a2 = a((if1) r91);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                r91.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new jn1(this, r91));
                qu0 qu0 = new qu0(this, r91);
                if (!r91.t) {
                    r91.d.add(qu0);
                }
                hi1 hi1 = new hi1(yb0, this, r91);
                if (!r91.t) {
                    r91.h.add(hi1);
                }
                r91.a((hg6<? super if1, cd6>) new me0(yb0, this, r91));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                r91.l();
            }
        }
        yb0.f(dg0.a);
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> a(x70 x70) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = x70.a(2);
        tm0 tm0 = new tm0(this.c, this.a, x70);
        synchronized (this.p) {
            u40 a2 = a((if1) tm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                tm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new gw0(this, tm0));
                by0 by0 = new by0(this, tm0);
                if (!tm0.t) {
                    tm0.d.add(by0);
                }
                p11 p11 = new p11(yb0, this, tm0);
                if (!tm0.t) {
                    tm0.h.add(p11);
                }
                tm0.a((hg6<? super if1, cd6>) new oe0(yb0, this, tm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                tm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> a(n70 n70) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = n70.a(2);
        mo0 mo0 = new mo0(this.c, this.a, n70);
        synchronized (this.p) {
            u40 a2 = a((if1) mo0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                mo0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new xh1(this, mo0));
                rj1 rj1 = new rj1(this, mo0);
                if (!mo0.t) {
                    mo0.d.add(rj1);
                }
                cn1 cn1 = new cn1(yb0, this, mo0);
                if (!mo0.t) {
                    mo0.h.add(cn1);
                }
                mo0.a((hg6<? super if1, cd6>) new cn0(yb0, this, mo0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                mo0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<String> a(byte[] bArr, String str) {
        yb0<String> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {Integer.valueOf(bArr.length), str};
        qb1 qb1 = new qb1(this.c, this.a, bArr, str);
        rv0 rv0 = new rv0(this);
        if (!qb1.t) {
            qb1.d.add(rv0);
        }
        qb1.c(new mx0(this));
        qb1.b((hg6<? super if1, cd6>) new gz0(this));
        synchronized (this.p) {
            u40 a2 = a((if1) qb1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                qb1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new fi1(this, qb1));
                zj1 zj1 = new zj1(this, qb1);
                if (!qb1.t) {
                    qb1.d.add(zj1);
                }
                in1 in1 = new in1(yb0, this, qb1);
                if (!qb1.t) {
                    qb1.h.add(in1);
                }
                qb1.a((hg6<? super if1, cd6>) new yt0(yb0, this, qb1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                qb1.l();
            }
        }
        yb0.f(a11.a);
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(p70 p70) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = p70.a(2);
        rt0 rt0 = new rt0(this.c, this.a, p70, 0, (String) null, 24);
        synchronized (this.p) {
            u40 a2 = a((if1) rt0);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                rt0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new oz0(this, rt0));
                z41 z41 = new z41(this, rt0);
                if (!rt0.t) {
                    rt0.d.add(z41);
                }
                t81 t81 = new t81(yb0, this, rt0);
                if (!rt0.t) {
                    rt0.h.add(t81);
                }
                rt0.a((hg6<? super if1, cd6>) new s41(yb0, this, rt0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                rt0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<cd6> a(byte[] bArr, d70 d70) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {cw0.a(bArr, (String) null, 1), d70};
        ue1 ue1 = this.c;
        wm0 wm0 = new wm0(ue1, this.a, eh1.SEND_CUSTOM_COMMAND, new dz0(ue1, bArr, d70.a()));
        synchronized (this.p) {
            u40 a2 = a((if1) wm0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                wm0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new gf0(this, wm0));
                xg0 xg0 = new xg0(this, wm0);
                if (!wm0.t) {
                    wm0.d.add(xg0);
                }
                jk0 jk0 = new jk0(yb0, this, wm0);
                if (!wm0.t) {
                    wm0.h.add(jk0);
                }
                wm0.a((hg6<? super if1, cd6>) new kj1(yb0, this, wm0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                wm0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(tc0 tc0) {
        w31 w31;
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = tc0.a(2);
        if ((tc0 instanceof pc0) || (tc0 instanceof zc0) || (tc0 instanceof yc0) || (tc0 instanceof ad0) || (tc0 instanceof sc0) || (tc0 instanceof nc0) || (tc0 instanceof oc0) || (tc0 instanceof vc0)) {
            w31 = w31.UI_SCRIPT;
        } else if (!(tc0 instanceof xc0) && !(tc0 instanceof rc0) && !(tc0 instanceof qc0) && !(tc0 instanceof wc0)) {
            yb0<cd6> yb02 = new yb0<>();
            yb02.b(new t40(u40.INVALID_PARAMETERS, (km1) null, 2));
            return yb02;
        } else {
            w31 = w31.MICRO_APP;
        }
        l21 l21 = new l21(this.c, this.a, tc0, w31, (String) null, 16);
        synchronized (this.p) {
            u40 a2 = a((if1) l21);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                l21.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new o31(this, l21));
                k51 k51 = new k51(this, l21);
                if (!l21.t) {
                    l21.d.add(k51);
                }
                e91 e91 = new e91(yb0, this, l21);
                if (!l21.t) {
                    l21.h.add(e91);
                }
                l21.a((hg6<? super if1, cd6>) new jg0(yb0, this, l21));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                l21.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(o70 o70) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = o70.a(2);
        h41 h41 = new h41(this.c, this.a, o70, 0, (String) null, 24);
        synchronized (this.p) {
            u40 a2 = a((if1) h41);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                h41.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new pa1(this, h41));
                kc1 kc1 = new kc1(this, h41);
                if (!h41.t) {
                    h41.d.add(kc1);
                }
                bg1 bg1 = new bg1(yb0, this, h41);
                if (!h41.t) {
                    h41.h.add(bg1);
                }
                h41.a((hg6<? super if1, cd6>) new gn0(yb0, this, h41));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                h41.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(z40[] z40Arr) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((p40[]) z40Arr).toString(2);
        d61 d61 = new d61(this.c, this.a, z40Arr, 0, (String) null, 24);
        synchronized (this.p) {
            u40 a2 = a((if1) d61);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                d61.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new ru0(this, d61));
                kw0 kw0 = new kw0(this, d61);
                if (!d61.t) {
                    d61.d.add(kw0);
                }
                yz0 yz0 = new yz0(yb0, this, d61);
                if (!d61.t) {
                    d61.h.add(yz0);
                }
                d61.a((hg6<? super if1, cd6>) new cu0(yb0, this, d61));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                d61.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<s60[]> a(r60[] r60Arr) {
        yb0<s60[]> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((p40[]) r60Arr).toString(2);
        ArrayList arrayList = new ArrayList();
        for (r60 r60 : r60Arr) {
            if (nd6.a(this.r.h(), r60.getKey())) {
                arrayList.add(r60);
            }
        }
        if (arrayList.isEmpty()) {
            yb0<s60[]> yb02 = new yb0<>();
            yb02.c(new s60[0]);
            return yb02;
        }
        ue1 ue1 = this.c;
        q41 q41 = this.a;
        Object[] array = arrayList.toArray(new r60[0]);
        if (array != null) {
            s01 s01 = new s01(ue1, q41, (r60[]) array, 0, (String) null, 24);
            synchronized (this.p) {
                u40 a2 = a((if1) s01);
                yb0 = new yb0<>();
                if (a2 != null) {
                    if (lv0.d[a2.ordinal()] != 1) {
                        sk1 = sk1.NOT_ALLOW_TO_START;
                    } else {
                        sk1 = sk1.BLUETOOTH_OFF;
                    }
                    s01.a(sk1);
                    yb0.b(new t40(a2, (km1) null, 2));
                } else {
                    yb0.a((hg6) new s11(this, s01));
                    n31 n31 = new n31(this, s01);
                    if (!s01.t) {
                        s01.d.add(n31);
                    }
                    g71 g71 = new g71(yb0, this, s01);
                    if (!s01.t) {
                        s01.h.add(g71);
                    }
                    s01.a((hg6<? super if1, cd6>) new ym1(yb0, this, s01));
                    if (this.s != q40.c.CONNECTED && !this.d) {
                        fm0.f.f();
                        b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                    }
                    s01.l();
                }
            }
            return yb0;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(q40.b bVar) {
        this.t = bVar;
    }

    @DexIgnore
    public yb0<cd6> a(boolean z) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = Boolean.valueOf(z);
        kf1 kf1 = new kf1(this.c, this.a, z, (String) null, 8);
        synchronized (this.p) {
            u40 a2 = a((if1) kf1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                kf1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new pg0(this, kf1));
                gi0 gi0 = new gi0(this, kf1);
                if (!kf1.t) {
                    kf1.d.add(gi0);
                }
                vl0 vl0 = new vl0(yb0, this, kf1);
                if (!kf1.t) {
                    kf1.h.add(vl0);
                }
                kf1.a((hg6<? super if1, cd6>) new sq0(yb0, this, kf1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                kf1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<String> a(kd0 kd0) {
        yb0<String> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = kd0.a(2);
        tk1 tk1 = new tk1(this.c, this.a, kd0);
        tk1.c(new lz0(this));
        synchronized (this.p) {
            u40 a2 = a((if1) tk1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                tk1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new e51(this, tk1));
                b71 b71 = new b71(this, tk1);
                if (!tk1.t) {
                    tk1.d.add(b71);
                }
                ua1 ua1 = new ua1(yb0, this, tk1);
                if (!tk1.t) {
                    tk1.h.add(ua1);
                }
                tk1.a((hg6<? super if1, cd6>) new rx0(yb0, this, tk1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                tk1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(z70[] z70Arr) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((p40[]) z70Arr).toString(2);
        he0 he0 = new he0(this.c, this.a, z70Arr, (String) null, 8);
        synchronized (this.p) {
            u40 a2 = a((if1) he0);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                he0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new on0(this, he0));
                gp0 gp0 = new gp0(this, he0);
                if (!he0.t) {
                    he0.d.add(gp0);
                }
                rs0 rs0 = new rs0(yb0, this, he0);
                if (!he0.t) {
                    he0.h.add(rs0);
                }
                he0.a((hg6<? super if1, cd6>) new s61(yb0, this, he0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                he0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(ae0 ae0) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = ae0.a(2);
        od1 od1 = new od1(this.c, this.a, ae0, (String) null, 8);
        synchronized (this.p) {
            u40 a2 = a((if1) od1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                od1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new pc1(this, od1));
                ke1 ke1 = new ke1(this, od1);
                if (!od1.t) {
                    od1.d.add(ke1);
                }
                bi1 bi1 = new bi1(yb0, this, od1);
                if (!od1.t) {
                    od1.h.add(bi1);
                }
                od1.a((hg6<? super if1, cd6>) new be1(yb0, this, od1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                od1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(zd0[] zd0Arr) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((p40[]) zd0Arr).toString(2);
        fh1 fh1 = new fh1(this.c, this.a, zd0Arr, (String) null, 8);
        synchronized (this.p) {
            u40 a2 = a((if1) fh1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                fh1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new wz0(this, fh1));
                r11 r11 = new r11(this, fh1);
                if (!fh1.t) {
                    fh1.d.add(r11);
                }
                i51 i51 = new i51(yb0, this, fh1);
                if (!fh1.t) {
                    fh1.h.add(i51);
                }
                fh1.a((hg6<? super if1, cd6>) new il1(yb0, this, fh1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                fh1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<cd6> a(md0 md0) {
        yb0<cd6> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = md0.a(2);
        lm1 lm1 = new lm1(this.c, this.a, md0);
        lm1.c(new uq0(this));
        synchronized (this.p) {
            u40 a2 = a((if1) lm1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                lm1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new ug0(this, lm1));
                li0 li0 = new li0(this, lm1);
                if (!lm1.t) {
                    lm1.d.add(li0);
                }
                am0 am0 = new am0(yb0, this, lm1);
                if (!lm1.t) {
                    lm1.h.add(am0);
                }
                lm1.a((hg6<? super if1, cd6>) new cp0(yb0, this, lm1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                lm1.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public zb0<byte[]> a(byte[] bArr) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a(bArr, (String) null, 1);
        tt0 tt0 = new tt0(this.c, this.a, nq0.PRE_SHARED_KEY, bArr);
        synchronized (this.p) {
            u40 a2 = a((if1) tt0);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                tt0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new ji0(this, tt0));
                dk0 dk0 = new dk0(this, tt0);
                if (!tt0.t) {
                    tt0.d.add(dk0);
                }
                rn0 rn0 = new rn0(yb0, this, tt0);
                if (!tt0.t) {
                    tt0.h.add(rn0);
                }
                tt0.a((hg6<? super if1, cd6>) new tx0(yb0, this, tt0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                tt0.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public yb0<FitnessData[]> a(h60 h60) {
        yb0<FitnessData[]> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = h60.a(2);
        ym0 ym0 = new ym0(this.c, this.a, h60, new HashMap());
        synchronized (this.p) {
            u40 a2 = a((if1) ym0);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                ym0.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new pu0(this, ym0));
                iw0 iw0 = new iw0(this, ym0);
                if (!ym0.t) {
                    ym0.d.add(iw0);
                }
                xz0 xz0 = new xz0(yb0, this, ym0);
                if (!ym0.t) {
                    ym0.h.add(xz0);
                }
                ym0.a((hg6<? super if1, cd6>) new mn0(yb0, this, ym0));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                ym0.l();
            }
        }
        yb0.f(ep0.a);
        return yb0;
    }

    @DexIgnore
    public final void a(q40.c cVar) {
        q40.c cVar2;
        q40.c cVar3 = cVar;
        synchronized (this.s) {
            cVar2 = this.s;
            this.s = cVar3;
            cd6 cd6 = cd6.a;
        }
        if (cVar2 != cVar3) {
            qs0 qs0 = qs0.h;
            nn0 nn0 = r4;
            nn0 nn02 = new nn0(cw0.a((Enum<?>) bs0.DEVICE_STATE_CHANGED), og0.DEVICE_EVENT, this.c.t, "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.PREV_STATE, (Object) cw0.a((Enum<?>) cVar2)), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) cVar)), 448);
            qs0.a(nn0);
            q40.c cVar4 = this.s;
            cc0 cc0 = cc0.DEBUG;
            Object[] objArr = {this.v, cVar2, cVar4};
            Intent intent = new Intent();
            intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE", cVar2);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE", cVar4);
            Context a2 = gk0.f.a();
            if (a2 != null) {
                ce.a(a2).a(intent);
            }
            Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                myLooper = Looper.getMainLooper();
            }
            if (myLooper != null) {
                new Handler(myLooper).post(new fg0(this, cVar2, cVar4));
                int i2 = lv0.c[cVar.ordinal()];
                if (i2 == 1) {
                    r();
                    this.f.b(sk1.CONNECTION_DROPPED, new eh1[]{eh1.STREAMING});
                    this.f.a(new nj1(new Hashtable(), (qg6) null));
                    this.d = false;
                } else if (i2 == 2) {
                    r();
                } else if (i2 == 3) {
                    r40 r40 = this.r;
                    xq0.e.a(r40.getMacAddress(), r40);
                    this.f.d();
                } else if (i2 != 4 && i2 == 5) {
                    r();
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(d90 d90) {
        q40.b bVar = this.t;
        if (bVar != null) {
            bVar.onEventReceived(this, d90);
        }
    }

    @DexIgnore
    public static final /* synthetic */ boolean a(ii1 ii1) {
        T t2;
        boolean z;
        if (ii1.c.s == h91.CONNECTED) {
            Iterator<T> it = ii1.f.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (((if1) t2).y == eh1.STREAMING) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final zb0<cd6> a(em0 em0) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = em0.a(2);
        r01 r01 = new r01(this.c, this.a, em0);
        synchronized (this.p) {
            u40 a2 = a((if1) r01);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                r01.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new hk0(this, r01));
                c91 c91 = new c91(this, r01);
                if (!r01.t) {
                    r01.d.add(c91);
                }
                cm0 cm0 = new cm0(yb0, this, r01);
                if (!r01.t) {
                    r01.h.add(cm0);
                }
                r01.a((hg6<? super if1, cd6>) new dc1(yb0, this, r01));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                r01.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public static /* synthetic */ yb0 a(ii1 ii1, boolean z, boolean z2, y11 y11, int i2) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                y11 = y11.LOW;
            } else {
                y11 = y11.NORMAL;
            }
        }
        return ii1.a(z, z2, y11);
    }

    @DexIgnore
    public final yb0<byte[][]> a(boolean z, boolean z2, y11 y11) {
        ui1 ui1;
        yb0<byte[][]> yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {Boolean.valueOf(z), y11, Boolean.valueOf(z2)};
        if (z2) {
            ui1 = new zg1(this.c, this.a, he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, false), qc6.a(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS, true)}));
        } else {
            ui1 ui12 = new ui1(this.c, this.a, eh1.GET_DATA_COLLECTION_FILE, he6.a(new lc6[]{qc6.a(io0.SKIP_ERASE, Boolean.valueOf(z)), qc6.a(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))}), ze0.a("UUID.randomUUID().toString()"));
            ui12.a(y11);
            ui1 = ui12;
        }
        synchronized (this.p) {
            u40 a2 = a((if1) ui1);
            yb0 = new yb0<>();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                ui1.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new dg1(this, ui1));
                bf0 bf0 = new bf0(this, ui1);
                if (!ui1.t) {
                    ui1.d.add(bf0);
                }
                ew0 ew0 = new ew0(yb0, this, ui1);
                if (!ui1.t) {
                    ui1.h.add(ew0);
                }
                ui1.a((hg6<? super if1, cd6>) new of1(yb0, this, ui1));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                ui1.l();
            }
        }
        yb0.f(kh1.a);
        return yb0;
    }

    @DexIgnore
    public final zb0<cd6> a(HashMap<qr0, Object> hashMap) {
        yb0 yb0;
        sk1 sk1;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = hashMap;
        s71 s71 = new s71(this.c, this.a, hashMap);
        synchronized (this.p) {
            u40 a2 = a((if1) s71);
            yb0 = new yb0();
            if (a2 != null) {
                if (lv0.d[a2.ordinal()] != 1) {
                    sk1 = sk1.NOT_ALLOW_TO_START;
                } else {
                    sk1 = sk1.BLUETOOTH_OFF;
                }
                s71.a(sk1);
                yb0.b(new t40(a2, (km1) null, 2));
            } else {
                yb0.a((hg6) new tn0(this, s71));
                g51 g51 = new g51(this, s71);
                if (!s71.t) {
                    s71.d.add(g51);
                }
                ni0 ni0 = new ni0(yb0, this, s71);
                if (!s71.t) {
                    s71.h.add(ni0);
                }
                s71.a((hg6<? super if1, cd6>) new y01(yb0, this, s71));
                if (this.s != q40.c.CONNECTED && !this.d) {
                    fm0.f.f();
                    b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)}));
                }
                s71.l();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void a(q40.d dVar, q40.d dVar2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE", dVar);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE", dVar2);
        Context a2 = gk0.f.a();
        if (a2 != null) {
            ce.a(a2).a(intent);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    public final void a(km1 km1) {
        boolean z;
        x11 x11;
        if (km1.b == sk1.REQUEST_ERROR) {
            bn0 bn0 = km1.c;
            if (bn0.c == il0.COMMAND_ERROR) {
                ch0 ch0 = bn0.d;
                if (ch0.b == lf0.GATT_ERROR && ((x11 = ch0.c.a) == x11.GATT_NULL || x11 == x11.START_FAIL)) {
                    z = true;
                    if (true != z) {
                        q();
                        return;
                    }
                    return;
                }
            }
        }
        z = false;
        if (true != z) {
        }
    }

    @DexIgnore
    public final u40 a(if1 if1) {
        T t2;
        boolean z;
        if (k40.l.d() != k40.c.ENABLED) {
            return u40.BLUETOOTH_OFF;
        }
        int i2 = lv0.e[if1.y.ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            return null;
        }
        if (i2 == 5) {
            ArrayList<if1> a2 = this.f.a();
            if (a2.isEmpty()) {
                return null;
            }
            Iterator<T> it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if1 if12 = (if1) t2;
                eh1 eh1 = if12.y;
                if (eh1 == eh1.STREAMING || eh1 == eh1.MAKE_DEVICE_READY || if12.e().compareTo(y11.LOW) <= 0) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return null;
            }
            return u40.DEVICE_BUSY;
        } else if (this.s != q40.c.UPGRADING_FIRMWARE) {
            return null;
        } else {
            return u40.DEVICE_BUSY;
        }
    }
}
