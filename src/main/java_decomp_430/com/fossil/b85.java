package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b85 implements Factory<a85> {
    @DexIgnore
    public /* final */ Provider<an4> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public b85(Provider<an4> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static b85 a(Provider<an4> provider, Provider<UserRepository> provider2) {
        return new b85(provider, provider2);
    }

    @DexIgnore
    public static a85 b(Provider<an4> provider, Provider<UserRepository> provider2) {
        return new CommuteTimeWatchAppSettingsViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel get() {
        return b(this.a, this.b);
    }
}
