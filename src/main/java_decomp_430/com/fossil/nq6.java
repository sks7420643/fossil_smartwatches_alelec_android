package com.fossil;

import com.fossil.xq6;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq6 {
    @DexIgnore
    public int a; // = 64;
    @DexIgnore
    public int b; // = 5;
    @DexIgnore
    public Runnable c;
    @DexIgnore
    public ExecutorService d;
    @DexIgnore
    public /* final */ Deque<xq6.b> e; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<xq6.b> f; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<xq6> g; // = new ArrayDeque();

    @DexIgnore
    public nq6(ExecutorService executorService) {
        this.d = executorService;
    }

    @DexIgnore
    public synchronized ExecutorService a() {
        if (this.d == null) {
            this.d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), fr6.a("OkHttp Dispatcher", false));
        }
        return this.d;
    }

    @DexIgnore
    public void b(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.b = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final int c(xq6.b bVar) {
        int i = 0;
        for (xq6.b next : this.f) {
            if (!next.c().f && next.d().equals(bVar.d())) {
                i++;
            }
        }
        return i;
    }

    @DexIgnore
    public synchronized int c() {
        return this.f.size() + this.g.size();
    }

    @DexIgnore
    public void a(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.a = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final boolean b() {
        int i;
        boolean z;
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            Iterator<xq6.b> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                xq6.b next = it.next();
                if (this.f.size() >= this.a) {
                    break;
                } else if (c(next) < this.b) {
                    it.remove();
                    arrayList.add(next);
                    this.f.add(next);
                }
            }
            z = c() > 0;
        }
        int size = arrayList.size();
        for (i = 0; i < size; i++) {
            ((xq6.b) arrayList.get(i)).a(a());
        }
        return z;
    }

    @DexIgnore
    public nq6() {
    }

    @DexIgnore
    public void a(xq6.b bVar) {
        synchronized (this) {
            this.e.add(bVar);
        }
        b();
    }

    @DexIgnore
    public synchronized void a(xq6 xq6) {
        this.g.add(xq6);
    }

    @DexIgnore
    public final <T> void a(Deque<T> deque, T t) {
        Runnable runnable;
        synchronized (this) {
            if (deque.remove(t)) {
                runnable = this.c;
            } else {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        if (!b() && runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void b(xq6.b bVar) {
        a(this.f, bVar);
    }

    @DexIgnore
    public void b(xq6 xq6) {
        a(this.g, xq6);
    }
}
