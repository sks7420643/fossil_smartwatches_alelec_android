package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h14 implements Factory<PortfolioApp> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public h14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static h14 a(b14 b14) {
        return new h14(b14);
    }

    @DexIgnore
    public static PortfolioApp b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static PortfolioApp c(b14 b14) {
        PortfolioApp b = b14.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    public PortfolioApp get() {
        return b(this.a);
    }
}
