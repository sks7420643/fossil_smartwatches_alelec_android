package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y35 extends j24 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(DianaCustomizeViewModel dianaCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(String str, String str2);

    @DexIgnore
    public abstract void c(String str, String str2);

    @DexIgnore
    public abstract void d(String str, String str2);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();
}
