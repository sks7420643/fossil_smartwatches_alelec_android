package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz3 extends qz3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (qx3 == qx3.CODE_39) {
            return super.a(str, qx3, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_39, but got " + qx3);
    }

    @DexIgnore
    public boolean[] a(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            int i = length + 25;
            int i2 = 0;
            while (i2 < length) {
                int indexOf = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i2));
                if (indexOf >= 0) {
                    a(gz3.a[indexOf], iArr);
                    int i3 = i;
                    for (int i4 = 0; i4 < 9; i4++) {
                        i3 += iArr[i4];
                    }
                    i2++;
                    i = i3;
                } else {
                    throw new IllegalArgumentException("Bad contents: " + str);
                }
            }
            boolean[] zArr = new boolean[i];
            a(gz3.b, iArr);
            int a = qz3.a(zArr, 0, iArr, true);
            int[] iArr2 = {1};
            int a2 = a + qz3.a(zArr, a, iArr2, false);
            for (int i5 = 0; i5 < length; i5++) {
                a(gz3.a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i5))], iArr);
                int a3 = a2 + qz3.a(zArr, a2, iArr, true);
                a2 = a3 + qz3.a(zArr, a3, iArr2, false);
            }
            a(gz3.b, iArr);
            qz3.a(zArr, a2, iArr, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }

    @DexIgnore
    public static void a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) != 0) {
                i3 = 2;
            }
            iArr[i2] = i3;
        }
    }
}
