package com.fossil;

import com.fossil.t60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z11 extends tg6 implements hg6<byte[], t60> {
    @DexIgnore
    public z11(t60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(t60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DisplayUnitConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((t60.a) this.receiver).a((byte[]) obj);
    }
}
