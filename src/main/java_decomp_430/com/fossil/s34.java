package com.fossil;

import android.content.Context;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s34 implements p34 {
    @DexIgnore
    public /* final */ List<String> a; // = qd6.d("Agree terms of use and privacy");

    @DexIgnore
    public boolean a(List<String> list) {
        wg6.b(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    public void a(BaseFragment baseFragment) {
        wg6.b(baseFragment, "fragment");
        HelpActivity.a aVar = HelpActivity.C;
        Context requireContext = baseFragment.requireContext();
        wg6.a((Object) requireContext, "fragment.requireContext()");
        aVar.a(requireContext);
    }
}
