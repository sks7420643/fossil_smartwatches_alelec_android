package com.fossil;

import com.fossil.dn3;
import com.fossil.yn3;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jn3<dn3.a<?>> {
        @DexIgnore
        /* renamed from: a */
        public int compare(dn3.a<?> aVar, dn3.a<?> aVar2) {
            return xo3.a(aVar2.getCount(), aVar.getCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> implements dn3.a<E> {
        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof dn3.a)) {
                return false;
            }
            dn3.a aVar = (dn3.a) obj;
            if (getCount() != aVar.getCount() || !gk3.a(getElement(), aVar.getElement())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            Object element = getElement();
            if (element == null) {
                i = 0;
            } else {
                i = element.hashCode();
            }
            return i ^ getCount();
        }

        @DexIgnore
        public String toString() {
            String valueOf = String.valueOf(getElement());
            int count = getCount();
            if (count == 1) {
                return valueOf;
            }
            return valueOf + " x " + count;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<E> extends yn3.a<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ho3<dn3.a<E>, E> {
            @DexIgnore
            public a(c cVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            public E a(dn3.a<E> aVar) {
                return aVar.getElement();
            }
        }

        @DexIgnore
        public abstract dn3<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().contains(obj);
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            return a().containsAll(collection);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        public Iterator<E> iterator() {
            return new a(this, a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return a().remove(obj, Integer.MAX_VALUE) > 0;
        }

        @DexIgnore
        public int size() {
            return a().entrySet().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<E> extends yn3.a<dn3.a<E>> {
        @DexIgnore
        public abstract dn3<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof dn3.a)) {
                return false;
            }
            dn3.a aVar = (dn3.a) obj;
            if (aVar.getCount() > 0 && a().count(aVar.getElement()) == aVar.getCount()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (obj instanceof dn3.a) {
                dn3.a aVar = (dn3.a) obj;
                Object element = aVar.getElement();
                int count = aVar.getCount();
                if (count != 0) {
                    return a().setCount(element, count, 0);
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<E> extends b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int count;
        @DexIgnore
        public /* final */ E element;

        @DexIgnore
        public e(E e, int i) {
            this.element = e;
            this.count = i;
            bl3.a(i, "count");
        }

        @DexIgnore
        public final int getCount() {
            return this.count;
        }

        @DexIgnore
        public final E getElement() {
            return this.element;
        }

        @DexIgnore
        public e<E> nextInBucket() {
            return null;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public static <E> dn3.a<E> a(E e2, int i) {
        return new e(e2, i);
    }

    @DexIgnore
    public static int b(Iterable<?> iterable) {
        if (iterable instanceof dn3) {
            return ((dn3) iterable).elementSet().size();
        }
        return 11;
    }

    @DexIgnore
    public static boolean c(dn3<?> dn3, Collection<?> collection) {
        jk3.a(collection);
        if (collection instanceof dn3) {
            collection = ((dn3) collection).elementSet();
        }
        return dn3.elementSet().retainAll(collection);
    }

    @DexIgnore
    public static boolean a(dn3<?> dn3, Object obj) {
        if (obj == dn3) {
            return true;
        }
        if (obj instanceof dn3) {
            dn3 dn32 = (dn3) obj;
            if (dn3.size() == dn32.size() && dn3.entrySet().size() == dn32.entrySet().size()) {
                for (dn3.a aVar : dn32.entrySet()) {
                    if (dn3.count(aVar.getElement()) != aVar.getCount()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean b(dn3<?> dn3, Collection<?> collection) {
        if (collection instanceof dn3) {
            collection = ((dn3) collection).elementSet();
        }
        return dn3.elementSet().removeAll(collection);
    }

    @DexIgnore
    public static <E> boolean a(dn3<E> dn3, Collection<? extends E> collection) {
        if (collection.isEmpty()) {
            return false;
        }
        if (collection instanceof dn3) {
            for (dn3.a next : a(collection).entrySet()) {
                dn3.add(next.getElement(), next.getCount());
            }
            return true;
        }
        qm3.a(dn3, collection.iterator());
        return true;
    }

    @DexIgnore
    public static <E> int a(dn3<E> dn3, E e2, int i) {
        bl3.a(i, "count");
        int count = dn3.count(e2);
        int i2 = i - count;
        if (i2 > 0) {
            dn3.add(e2, i2);
        } else if (i2 < 0) {
            dn3.remove(e2, -i2);
        }
        return count;
    }

    @DexIgnore
    public static <E> boolean a(dn3<E> dn3, E e2, int i, int i2) {
        bl3.a(i, "oldCount");
        bl3.a(i2, "newCount");
        if (dn3.count(e2) != i) {
            return false;
        }
        dn3.setCount(e2, i2);
        return true;
    }

    @DexIgnore
    public static int a(dn3<?> dn3) {
        long j = 0;
        for (dn3.a<?> count : dn3.entrySet()) {
            j += (long) count.getCount();
        }
        return xo3.a(j);
    }

    @DexIgnore
    public static <T> dn3<T> a(Iterable<T> iterable) {
        return (dn3) iterable;
    }
}
