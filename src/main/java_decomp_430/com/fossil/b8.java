package com.fossil;

import android.os.Build;
import android.os.LocaleList;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b8 {
    @DexIgnore
    public d8 a;

    /*
    static {
        a(new Locale[0]);
    }
    */

    @DexIgnore
    public b8(d8 d8Var) {
        this.a = d8Var;
    }

    @DexIgnore
    public static b8 a(LocaleList localeList) {
        return new b8(new e8(localeList));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof b8) && this.a.equals(((b8) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static b8 a(Locale... localeArr) {
        if (Build.VERSION.SDK_INT >= 24) {
            return a(new LocaleList(localeArr));
        }
        return new b8(new c8(localeArr));
    }

    @DexIgnore
    public Locale a(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public static Locale a(String str) {
        if (str.contains("-")) {
            String[] split = str.split("-", -1);
            if (split.length > 2) {
                return new Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new Locale(split[0]);
            }
        } else if (!str.contains("_")) {
            return new Locale(str);
        } else {
            String[] split2 = str.split("_", -1);
            if (split2.length > 2) {
                return new Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new Locale(split2[0]);
            }
        }
        throw new IllegalArgumentException("Can not parse language tag: [" + str + "]");
    }
}
