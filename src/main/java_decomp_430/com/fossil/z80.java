package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z80 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ h70 c;
    @DexIgnore
    public /* final */ w80 d;
    @DexIgnore
    public /* final */ y80[] e;
    @DexIgnore
    public /* final */ x80[] f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<z80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    h70 valueOf = h70.valueOf(readString2);
                    Parcelable readParcelable = parcel.readParcelable(w80.class.getClassLoader());
                    if (readParcelable != null) {
                        w80 w80 = (w80) readParcelable;
                        Object[] createTypedArray = parcel.createTypedArray(y80.CREATOR);
                        if (createTypedArray != null) {
                            wg6.a(createTypedArray, "parcel.createTypedArray(\u2026erHourForecast.CREATOR)!!");
                            y80[] y80Arr = (y80[]) createTypedArray;
                            Object[] createTypedArray2 = parcel.createTypedArray(x80.CREATOR);
                            if (createTypedArray2 != null) {
                                wg6.a(createTypedArray2, "parcel.createTypedArray(\u2026herDayForecast.CREATOR)!!");
                                return new z80(readLong, readString, valueOf, w80, y80Arr, (x80[]) createTypedArray2);
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new z80[i];
        }
    }

    @DexIgnore
    public z80(long j, String str, h70 h70, w80 w80, y80[] y80Arr, x80[] x80Arr) throws IllegalArgumentException {
        this.a = j;
        this.b = str;
        this.c = h70;
        this.d = w80;
        this.e = y80Arr;
        this.f = x80Arr;
        if (this.e.length < 3) {
            throw new IllegalArgumentException("hourForecast must have at least 3 elements.");
        } else if (this.f.length < 3) {
            throw new IllegalArgumentException("dayForecast must have at least 3 elements.");
        }
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.EXPIRED_TIMESTAMP_IN_SECOND, (Object) Long.valueOf(this.a)), bm0.LOCATION, (Object) this.b), bm0.TEMP_UNIT, (Object) cw0.a((Enum<?>) this.c)), bm0.CURRENT_WEATHER_INFO, (Object) this.d.a()), bm0.HOURLY_FORECAST, (Object) cw0.a((p40[]) this.e)), bm0.DAILY_FORECAST, (Object) cw0.a((p40[]) this.f));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            jSONArray.put(this.e[i].b());
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = 0; i2 < 3; i2++) {
            jSONArray2.put(this.f[i2].b());
        }
        JSONObject put = new JSONObject().put("alive", this.a).put("city", this.b).put("unit", cw0.a((Enum<?>) this.c));
        wg6.a(put, "JSONObject().put(UIScrip\u2026ratureUnit.lowerCaseName)");
        JSONObject put2 = cw0.a(put, this.d.b()).put("forecast_day", jSONArray).put("forecast_week", jSONArray2);
        wg6.a(put2, "JSONObject().put(UIScrip\u2026ST_WEEK, dayForecastJSON)");
        return put2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(z80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            z80 z80 = (z80) obj;
            return this.a == z80.a && this.c == z80.c && !(wg6.a(this.b, z80.b) ^ true) && !(wg6.a(this.d, z80.d) ^ true) && Arrays.equals(this.e, z80.e) && Arrays.equals(this.f, z80.f);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherInfo");
    }

    @DexIgnore
    public final w80 getCurrentWeatherInfo() {
        return this.d;
    }

    @DexIgnore
    public final x80[] getDayForecast() {
        return this.f;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public final y80[] getHourForecast() {
        return this.e;
    }

    @DexIgnore
    public final String getLocation() {
        return this.b;
    }

    @DexIgnore
    public final h70 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.b.hashCode();
        return ((((this.d.hashCode() + ((hashCode2 + ((hashCode + (Long.valueOf(this.a).hashCode() * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.e)) * 31) + Arrays.hashCode(this.f);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.e, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.f, i);
        }
    }
}
