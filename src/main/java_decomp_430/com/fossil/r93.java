package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r93 {
    @DexIgnore
    public /* final */ Runnable a; // = new q93(this);
    @DexIgnore
    public /* final */ /* synthetic */ m93 b;

    @DexIgnore
    public r93(m93 m93) {
        this.b = m93;
    }

    @DexIgnore
    public final void a(long j) {
        this.b.g();
        if (this.b.l().a(l03.L0)) {
            this.b.c.removeCallbacks(this.a);
        }
    }

    @DexIgnore
    public final void b(long j) {
        if (this.b.l().a(l03.L0)) {
            this.b.c.postDelayed(this.a, 2000);
        }
    }
}
