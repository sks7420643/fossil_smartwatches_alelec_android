package com.fossil;

import com.portfolio.platform.receiver.SmsMmsReceiver;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo4 implements Factory<vo4> {
    @DexIgnore
    public /* final */ Provider<iq4> a;
    @DexIgnore
    public /* final */ Provider<an4> b;

    @DexIgnore
    public wo4(Provider<iq4> provider, Provider<an4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static wo4 a(Provider<iq4> provider, Provider<an4> provider2) {
        return new wo4(provider, provider2);
    }

    @DexIgnore
    public static vo4 b(Provider<iq4> provider, Provider<an4> provider2) {
        SmsMmsReceiver smsMmsReceiver = new SmsMmsReceiver();
        xo4.a(smsMmsReceiver, provider.get());
        xo4.a(smsMmsReceiver, provider2.get());
        return smsMmsReceiver;
    }

    @DexIgnore
    public SmsMmsReceiver get() {
        return b(this.a, this.b);
    }
}
