package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.io.FileNotFoundException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv implements jv<Uri, File> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements kv<Uri, File> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public jv<Uri, File> a(nv nvVar) {
            return new gv(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements fs<File> {
        @DexIgnore
        public static /* final */ String[] c; // = {"_data"};
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Uri b;

        @DexIgnore
        public b(Context context, Uri uri) {
            this.a = context;
            this.b = uri;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super File> aVar) {
            Cursor query = this.a.getContentResolver().query(this.b, c, (String) null, (String[]) null, (String) null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.a((Exception) new FileNotFoundException("Failed to find file path for: " + this.b));
                return;
            }
            aVar.a(new File(str));
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<File> getDataClass() {
            return File.class;
        }
    }

    @DexIgnore
    public gv(Context context) {
        this.a = context;
    }

    @DexIgnore
    public jv.a<File> a(Uri uri, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(uri), new b(this.a, uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ss.b(uri);
    }
}
