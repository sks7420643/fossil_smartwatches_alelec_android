package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ gc0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<k90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new k90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new k90[i];
        }
    }

    @DexIgnore
    public k90(byte b, int i, gc0 gc0) {
        super(e90.BUDDY_CHALLENGE_UPDATE_INFO, b);
        this.c = i;
        this.d = gc0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.REQUEST_ID, (Object) Integer.valueOf(this.c)), bm0.FITNESS_DATA, (Object) this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(k90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((k90) obj).c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.BuddyChallengeUpdateInfoNotification");
    }

    @DexIgnore
    public final gc0 getFitnessData() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return Integer.valueOf(this.c).hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public /* synthetic */ k90(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt();
        Parcelable readParcelable = parcel.readParcelable(gc0.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (gc0) readParcelable;
        } else {
            wg6.a();
            throw null;
        }
    }
}
