package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r90 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<r90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new r90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new r90[i];
        }
    }

    @DexIgnore
    public r90(byte b, int i, String str) {
        super(e90.BUDDY_CHALLENGE_GET_INFO, b, i);
        this.d = str;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.CHALLENGE_ID, (Object) this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(r90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.d, ((r90) obj).d) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.BuddyChallengeGetInfoRequest");
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ r90(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
        } else {
            wg6.a();
            throw null;
        }
    }
}
