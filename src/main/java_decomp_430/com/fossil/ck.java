package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ck implements dk {
    @DexIgnore
    public /* final */ ViewOverlay a;

    @DexIgnore
    public ck(View view) {
        this.a = view.getOverlay();
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }
}
