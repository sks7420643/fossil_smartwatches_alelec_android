package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m12 {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public kv1 b;

    @DexIgnore
    public m12(kv1 kv1) {
        w12.a(kv1);
        this.b = kv1;
    }

    @DexIgnore
    public int a(Context context, rv1.f fVar) {
        w12.a(context);
        w12.a(fVar);
        if (!fVar.i()) {
            return 0;
        }
        int j = fVar.j();
        int i = this.a.get(j, -1);
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        while (true) {
            if (i2 < this.a.size()) {
                int keyAt = this.a.keyAt(i2);
                if (keyAt > j && this.a.get(keyAt) == 0) {
                    i = 0;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        if (i == -1) {
            i = this.b.a(context, j);
        }
        this.a.put(j, i);
        return i;
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }
}
