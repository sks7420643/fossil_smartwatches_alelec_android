package com.fossil;

import android.content.Context;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om4 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ PortfolioApp b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public om4(PortfolioApp portfolioApp) {
        wg6.b(portfolioApp, "mApp");
        this.b = portfolioApp;
    }

    @DexIgnore
    public final String a(MFUser mFUser, List<CustomizeRealData> list, String str, DianaPreset dianaPreset) {
        T t;
        wg6.b(list, "realDataList");
        wg6.b(str, "complicationId");
        wg6.b(dianaPreset, "preset");
        if (str.hashCode() == 134170930 && str.equals("second-timezone")) {
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) ((DianaPresetComplicationSetting) t).getId(), (Object) "second-timezone")) {
                    break;
                }
            }
            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
            if (dianaPresetComplicationSetting == null || vi4.a(dianaPresetComplicationSetting.getSettings())) {
                return "- -";
            }
            try {
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) this.a.a(dianaPresetComplicationSetting.getSettings(), SecondTimezoneSetting.class);
                if (secondTimezoneSetting == null) {
                    return "- -";
                }
                double timezoneRawOffsetById = (((double) ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())) / 60.0d) - ((double) (((float) zk4.a()) / ((float) DateTimeConstants.SECONDS_PER_HOUR)));
                if (timezoneRawOffsetById > ((double) 0)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append('+');
                    sb.append(timezoneRawOffsetById);
                    sb.append('h');
                    return sb.toString();
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(timezoneRawOffsetById);
                sb2.append('h');
                return sb2.toString();
            } catch (Exception unused) {
                FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse 2nd tz setting");
                return "- -";
            }
        } else {
            ArrayList arrayList = new ArrayList(rd6.a(list, 10));
            for (CustomizeRealData copy$default : list) {
                arrayList.add(CustomizeRealData.copy$default(copy$default, (String) null, (String) null, 3, (Object) null));
            }
            return a(new ArrayList(arrayList), str, mFUser);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v25, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(List<CustomizeRealData> list, String str, MFUser mFUser) {
        CustomizeRealData customizeRealData;
        CustomizeRealData customizeRealData2;
        String valueOf;
        CustomizeRealData customizeRealData3;
        zh4 zh4;
        Integer num = null;
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    return "34m";
                }
                return "- -";
            case -331239923:
                if (!str.equals(Constants.BATTERY)) {
                    return "- -";
                }
                MisfitDeviceProfile a2 = DeviceHelper.o.e().a(this.b.e());
                StringBuilder sb = new StringBuilder();
                if (a2 != null) {
                    num = Integer.valueOf(a2.getBatteryLevel());
                }
                sb.append(String.valueOf(num));
                sb.append("%");
                return sb.toString();
            case -168965370:
                if (str.equals(Constants.CALORIES)) {
                    return "1200";
                }
                return "- -";
            case -85386984:
                if (str.equals("active-minutes")) {
                    return "12m";
                }
                return "- -";
            case -48173007:
                if (!str.equals("chance-of-rain")) {
                    return "- -";
                }
                Iterator<T> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        customizeRealData = it.next();
                        if (wg6.a((Object) "chance_of_rain", (Object) customizeRealData.getId())) {
                        }
                    } else {
                        customizeRealData = null;
                    }
                }
                CustomizeRealData customizeRealData4 = (CustomizeRealData) customizeRealData;
                if (customizeRealData4 == null) {
                    return "- -";
                }
                return customizeRealData4.getValue() + '%';
            case 3076014:
                if (str.equals(HardwareLog.COLUMN_DATE)) {
                    Iterator<T> it2 = list.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            customizeRealData2 = it2.next();
                            if (wg6.a((Object) "day", (Object) customizeRealData2.getId())) {
                            }
                        } else {
                            customizeRealData2 = null;
                        }
                    }
                    CustomizeRealData customizeRealData5 = (CustomizeRealData) customizeRealData2;
                    if (customizeRealData5 == null || (valueOf = customizeRealData5.getValue()) == null) {
                        valueOf = String.valueOf(Calendar.getInstance().get(5));
                        break;
                    }
                } else {
                    return "- -";
                }
            case 96634189:
                if (!str.equals("empty")) {
                    return "- -";
                }
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886387);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026me_PresetSwiped_CTA__Set)");
                return a3;
            case 109761319:
                if (str.equals("steps")) {
                    return "2385";
                }
                return "- -";
            case 1223440372:
                if (!str.equals("weather")) {
                    return "- -";
                }
                Iterator<T> it3 = list.iterator();
                while (true) {
                    if (it3.hasNext()) {
                        customizeRealData3 = it3.next();
                        if (wg6.a((Object) "temperature", (Object) customizeRealData3.getId())) {
                        }
                    } else {
                        customizeRealData3 = null;
                    }
                }
                CustomizeRealData customizeRealData6 = (CustomizeRealData) customizeRealData3;
                if (mFUser == null || (zh4 = mFUser.getTemperatureUnit()) == null) {
                    zh4 = zh4.METRIC;
                }
                int i = pm4.a[zh4.ordinal()];
                float f = 0.0f;
                if (i != 1) {
                    if (i == 2 && customizeRealData6 != null) {
                        try {
                            f = Float.parseFloat(customizeRealData6.getValue());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse celsius temperature");
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(rh6.a(zj4.a(f)));
                        sb2.append(8457);
                        valueOf = sb2.toString();
                        break;
                    } else {
                        return "- -";
                    }
                } else if (customizeRealData6 != null) {
                    try {
                        f = Float.parseFloat(customizeRealData6.getValue());
                    } catch (Exception unused2) {
                        FLogger.INSTANCE.getLocal().d("CustomizeRealDataManager", "exception when parse celsius temperature");
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(rh6.a(f));
                    sb3.append(8451);
                    valueOf = sb3.toString();
                    break;
                } else {
                    return "- -";
                }
                break;
            case 1884273159:
                if (str.equals("heart-rate")) {
                    return "68";
                }
                return "- -";
            default:
                return "- -";
        }
        return valueOf;
    }
}
