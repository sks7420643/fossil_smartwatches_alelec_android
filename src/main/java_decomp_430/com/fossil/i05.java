package com.fossil;

import android.text.SpannableString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i05 extends k24<h05> {
    @DexIgnore
    void F(boolean z);

    @DexIgnore
    void G(boolean z);

    @DexIgnore
    void N(String str);

    @DexIgnore
    void c(SpannableString spannableString);

    @DexIgnore
    void close();

    @DexIgnore
    void d(SpannableString spannableString);

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void x(boolean z);

    @DexIgnore
    void z(boolean z);
}
