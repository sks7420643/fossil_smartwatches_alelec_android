package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OptInFragmentMapping extends OptInFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362133, 1);
        x.put(2131362542, 2);
        x.put(2131362078, 3);
        x.put(2131362433, 4);
        x.put(2131362905, 5);
        x.put(2131362329, 6);
        x.put(2131362072, 7);
        x.put(2131363035, 8);
        x.put(2131361888, 9);
    }
    */

    @DexIgnore
    public OptInFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 10, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OptInFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[9], objArr[7], objArr[3], objArr[1], objArr[6], objArr[4], objArr[2], objArr[0], objArr[5], objArr[8]);
        this.v = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
