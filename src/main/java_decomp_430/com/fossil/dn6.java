package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn6 extends ho6 implements mm6 {
    @DexIgnore
    public dn6 a() {
        return this;
    }

    @DexIgnore
    public final String a(String str) {
        wg6.b(str, Constants.STATE);
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object c = c();
        if (c != null) {
            boolean z = true;
            for (jo6 jo6 = (jo6) c; !wg6.a((Object) jo6, (Object) this); jo6 = jo6.d()) {
                if (jo6 instanceof xm6) {
                    xm6 xm6 = (xm6) jo6;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(xm6);
                }
            }
            sb.append("]");
            String sb2 = sb.toString();
            wg6.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    public String toString() {
        return nl6.c() ? a("Active") : super.toString();
    }
}
