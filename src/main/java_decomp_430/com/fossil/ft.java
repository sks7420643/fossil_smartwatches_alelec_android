package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ft {
    @DexIgnore
    public static /* final */ ft a; // = new b();
    @DexIgnore
    public static /* final */ ft b; // = new c();
    @DexIgnore
    public static /* final */ ft c; // = new d();
    @DexIgnore
    public static /* final */ ft d; // = new e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ft {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(pr prVar) {
            return prVar == pr.REMOTE;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public boolean a(boolean z, pr prVar, rr rrVar) {
            return (prVar == pr.RESOURCE_DISK_CACHE || prVar == pr.MEMORY_CACHE) ? false : true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ft {
        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(pr prVar) {
            return false;
        }

        @DexIgnore
        public boolean a(boolean z, pr prVar, rr rrVar) {
            return false;
        }

        @DexIgnore
        public boolean b() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ft {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(pr prVar) {
            return (prVar == pr.DATA_DISK_CACHE || prVar == pr.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        public boolean a(boolean z, pr prVar, rr rrVar) {
            return false;
        }

        @DexIgnore
        public boolean b() {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ft {
        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(pr prVar) {
            return false;
        }

        @DexIgnore
        public boolean a(boolean z, pr prVar, rr rrVar) {
            return (prVar == pr.RESOURCE_DISK_CACHE || prVar == pr.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ft {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(pr prVar) {
            return prVar == pr.REMOTE;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public boolean a(boolean z, pr prVar, rr rrVar) {
            return ((z && prVar == pr.DATA_DISK_CACHE) || prVar == pr.LOCAL) && rrVar == rr.TRANSFORMED;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract boolean a(pr prVar);

    @DexIgnore
    public abstract boolean a(boolean z, pr prVar, rr rrVar);

    @DexIgnore
    public abstract boolean b();
}
