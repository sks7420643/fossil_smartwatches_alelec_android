package com.fossil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ e73 e;

    @DexIgnore
    public l73(e73 e73, AtomicReference atomicReference, String str, String str2, String str3) {
        this.e = e73;
        this.a = atomicReference;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    @DexIgnore
    public final void run() {
        this.e.a.F().a((AtomicReference<List<ab3>>) this.a, this.b, this.c, this.d);
    }
}
