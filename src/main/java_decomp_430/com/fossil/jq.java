package com.fossil;

import com.fossil.ip;
import com.fossil.wearables.fsl.dial.ConfigItem;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jq {
    @DexIgnore
    public static ip.a a(rp rpVar) {
        boolean z;
        long j;
        long j2;
        long j3;
        long j4;
        rp rpVar2 = rpVar;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = rpVar2.c;
        String str = map.get(ConfigItem.KEY_DATE);
        long a = str != null ? a(str) : 0;
        String str2 = map.get("Cache-Control");
        int i = 0;
        if (str2 != null) {
            String[] split = str2.split(",", 0);
            j2 = 0;
            int i2 = 0;
            j = 0;
            while (i < split.length) {
                String trim = split[i].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j2 = Long.parseLong(trim.substring(8));
                    } catch (Exception unused) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j = Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    i2 = 1;
                }
                i++;
            }
            i = i2;
            z = true;
        } else {
            j2 = 0;
            j = 0;
            z = false;
        }
        String str3 = map.get("Expires");
        long a2 = str3 != null ? a(str3) : 0;
        String str4 = map.get("Last-Modified");
        long a3 = str4 != null ? a(str4) : 0;
        String str5 = map.get("ETag");
        if (z) {
            j4 = currentTimeMillis + (j2 * 1000);
            if (i == 0) {
                Long.signum(j);
                j3 = (j * 1000) + j4;
                ip.a aVar = new ip.a();
                aVar.a = rpVar2.b;
                aVar.b = str5;
                aVar.f = j4;
                aVar.e = j3;
                aVar.c = a;
                aVar.d = a3;
                aVar.g = map;
                aVar.h = rpVar2.d;
                return aVar;
            }
        } else if (a <= 0 || a2 < a) {
            j4 = 0;
        } else {
            j3 = (a2 - a) + currentTimeMillis;
            j4 = j3;
            ip.a aVar2 = new ip.a();
            aVar2.a = rpVar2.b;
            aVar2.b = str5;
            aVar2.f = j4;
            aVar2.e = j3;
            aVar2.c = a;
            aVar2.d = a3;
            aVar2.g = map;
            aVar2.h = rpVar2.d;
            return aVar2;
        }
        j3 = j4;
        ip.a aVar22 = new ip.a();
        aVar22.a = rpVar2.b;
        aVar22.b = str5;
        aVar22.f = j4;
        aVar22.e = j3;
        aVar22.c = a;
        aVar22.d = a3;
        aVar22.g = map;
        aVar22.h = rpVar2.d;
        return aVar22;
    }

    @DexIgnore
    public static long a(String str) {
        try {
            return a().parse(str).getTime();
        } catch (ParseException e) {
            cq.a(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    @DexIgnore
    public static String a(long j) {
        return a().format(new Date(j));
    }

    @DexIgnore
    public static SimpleDateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }

    @DexIgnore
    public static String a(Map<String, String> map, String str) {
        String str2 = map.get("Content-Type");
        if (str2 != null) {
            String[] split = str2.split(";", 0);
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].trim().split("=", 0);
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return str;
    }

    @DexIgnore
    public static Map<String, String> a(List<np> list) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (np next : list) {
            treeMap.put(next.a(), next.b());
        }
        return treeMap;
    }

    @DexIgnore
    public static List<np> a(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(new np((String) next.getKey(), (String) next.getValue()));
        }
        return arrayList;
    }
}
