package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph1 {
    @DexIgnore
    public /* synthetic */ ph1(qg6 qg6) {
    }

    @DexIgnore
    public final jj1 a(byte b) {
        jj1 jj1;
        jj1[] values = jj1.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                jj1 = null;
                break;
            }
            jj1 = values[i];
            if (jj1.b == b) {
                break;
            }
            i++;
        }
        return jj1 != null ? jj1 : jj1.UNKNOWN;
    }
}
