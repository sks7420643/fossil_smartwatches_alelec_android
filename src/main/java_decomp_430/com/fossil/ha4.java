package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ha4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ ImageView F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public /* final */ RecyclerView I;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ TodayHeartRateChart u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ha4(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, TodayHeartRateChart todayHeartRateChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, Guideline guideline, RTLImageView rTLImageView, ImageView imageView, ImageView imageView2, View view2, LinearLayout linearLayout, ConstraintLayout constraintLayout5, RecyclerView recyclerView, View view3, FlexibleTextView flexibleTextView9, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = todayHeartRateChart;
        this.v = flexibleTextView;
        this.w = flexibleTextView2;
        this.x = flexibleTextView3;
        this.y = flexibleTextView4;
        this.z = flexibleTextView5;
        this.A = flexibleTextView6;
        this.B = flexibleTextView7;
        this.C = flexibleTextView8;
        this.D = rTLImageView;
        this.E = imageView;
        this.F = imageView2;
        this.G = linearLayout;
        this.H = constraintLayout5;
        this.I = recyclerView;
    }
}
