package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<qw2> CREATOR; // = new rw2();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public qw2(int i, int i2, long j, long j2) {
        this.a = i;
        this.b = i2;
        this.c = j;
        this.d = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && qw2.class == obj.getClass()) {
            qw2 qw2 = (qw2) obj;
            return this.a == qw2.a && this.b == qw2.b && this.c == qw2.c && this.d == qw2.d;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Integer.valueOf(this.b), Integer.valueOf(this.a), Long.valueOf(this.d), Long.valueOf(this.c));
    }

    @DexIgnore
    public final String toString() {
        return "NetworkLocationStatus:" + " Wifi status: " + this.a + " Cell status: " + this.b + " elapsed time NS: " + this.d + " system time ms: " + this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d);
        g22.a(parcel, a2);
    }
}
