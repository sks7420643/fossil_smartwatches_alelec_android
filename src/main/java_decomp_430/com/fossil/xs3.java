package com.fossil;

import android.os.Handler;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xs3 implements Handler.Callback {
    @DexIgnore
    public /* final */ us3 a;

    @DexIgnore
    public xs3(us3 us3) {
        this.a = us3;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.a.a(message);
    }
}
