package com.fossil;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig2 extends cx2 {
    @DexIgnore
    public /* final */ uw1<yv2> a;

    @DexIgnore
    public ig2(uw1<yv2> uw1) {
        this.a = uw1;
    }

    @DexIgnore
    public final void a(LocationAvailability locationAvailability) {
        this.a.a(new kg2(this, locationAvailability));
    }

    @DexIgnore
    public final void a(LocationResult locationResult) {
        this.a.a(new jg2(this, locationResult));
    }

    @DexIgnore
    public final synchronized void q() {
        this.a.a();
    }
}
