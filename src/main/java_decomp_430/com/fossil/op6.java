package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class op6 extends mp6 {
    @DexIgnore
    public /* final */ Runnable c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public op6(Runnable runnable, long j, np6 np6) {
        super(j, np6);
        wg6.b(runnable, "block");
        wg6.b(np6, "taskContext");
        this.c = runnable;
    }

    @DexIgnore
    public void run() {
        try {
            this.c.run();
        } finally {
            this.b.m();
        }
    }

    @DexIgnore
    public String toString() {
        return "Task[" + ol6.a((Object) this.c) + '@' + ol6.b(this.c) + ", " + this.a + ", " + this.b + ']';
    }
}
