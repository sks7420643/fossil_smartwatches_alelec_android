package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw0 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;

    @DexIgnore
    public /* synthetic */ bw0(String str, String str2, String str3, int i, String str4, String str5, int i2) {
        str5 = (i2 & 32) != 0 ? "android" : str5;
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
        this.e = str4;
        this.f = str5;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bw0)) {
            return false;
        }
        bw0 bw0 = (bw0) obj;
        return wg6.a(this.a, bw0.a) && wg6.a(this.b, bw0.b) && wg6.a(this.c, bw0.c) && this.d == bw0.d && wg6.a(this.e, bw0.e) && wg6.a(this.f, bw0.f);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (((hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31) + this.d) * 31;
        String str4 = this.e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("SystemInformation(osVersion=");
        b2.append(this.a);
        b2.append(", phoneModel=");
        b2.append(this.b);
        b2.append(", userId=");
        b2.append(this.c);
        b2.append(", timezoneOffset=");
        b2.append(this.d);
        b2.append(", sdkVersion=");
        b2.append(this.e);
        b2.append(", osName=");
        b2.append(this.f);
        b2.append(")");
        return b2.toString();
    }
}
