package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt5 implements MembersInjector<ut5> {
    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, lt4 lt4) {
        signUpPresenter.e = lt4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, kn4 kn4) {
        signUpPresenter.f = kn4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, mt4 mt4) {
        signUpPresenter.g = mt4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, pt4 pt4) {
        signUpPresenter.h = pt4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, ot4 ot4) {
        signUpPresenter.i = ot4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, LoginSocialUseCase loginSocialUseCase) {
        signUpPresenter.j = loginSocialUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, UserRepository userRepository) {
        signUpPresenter.k = userRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, DeviceRepository deviceRepository) {
        signUpPresenter.l = deviceRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, z24 z24) {
        signUpPresenter.m = z24;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSleepSessions fetchSleepSessions) {
        signUpPresenter.n = fetchSleepSessions;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSleepSummaries fetchSleepSummaries) {
        signUpPresenter.o = fetchSleepSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchActivities fetchActivities) {
        signUpPresenter.p = fetchActivities;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchSummaries fetchSummaries) {
        signUpPresenter.q = fetchSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        signUpPresenter.r = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        signUpPresenter.s = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, AlarmsRepository alarmsRepository) {
        signUpPresenter.t = alarmsRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, rr4 rr4) {
        signUpPresenter.u = rr4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, cj4 cj4) {
        signUpPresenter.v = cj4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        signUpPresenter.w = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, an4 an4) {
        signUpPresenter.x = an4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, CheckAuthenticationEmailExisting checkAuthenticationEmailExisting) {
        signUpPresenter.y = checkAuthenticationEmailExisting;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        signUpPresenter.z = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, AnalyticsHelper analyticsHelper) {
        signUpPresenter.A = analyticsHelper;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, dt4 dt4) {
        signUpPresenter.B = dt4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, SummariesRepository summariesRepository) {
        signUpPresenter.C = summariesRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, SleepSummariesRepository sleepSummariesRepository) {
        signUpPresenter.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, GoalTrackingRepository goalTrackingRepository) {
        signUpPresenter.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        signUpPresenter.F = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, qs4 qs4) {
        signUpPresenter.G = qs4;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, RequestEmailOtp requestEmailOtp) {
        signUpPresenter.H = requestEmailOtp;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        signUpPresenter.I = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        signUpPresenter.J = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter) {
        signUpPresenter.D();
    }
}
