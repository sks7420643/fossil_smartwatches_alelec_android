package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ow;
import com.fossil.vw;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw {
    @DexIgnore
    public static /* final */ wr<qr> f; // = wr.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", qr.DEFAULT);
    @DexIgnore
    public static /* final */ wr<yr> g; // = wr.a("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace", yr.SRGB);
    @DexIgnore
    public static /* final */ wr<Boolean> h; // = wr.a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", false);
    @DexIgnore
    public static /* final */ wr<Boolean> i; // = wr.a("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", false);
    @DexIgnore
    public static /* final */ Set<String> j; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"image/vnd.wap.wbmp", "image/x-ico"})));
    @DexIgnore
    public static /* final */ b k; // = new a();
    @DexIgnore
    public static /* final */ Set<ImageHeaderParser.ImageType> l; // = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
    @DexIgnore
    public static /* final */ Queue<BitmapFactory.Options> m; // = r00.a(0);
    @DexIgnore
    public /* final */ au a;
    @DexIgnore
    public /* final */ DisplayMetrics b;
    @DexIgnore
    public /* final */ xt c;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> d;
    @DexIgnore
    public /* final */ uw e; // = uw.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(au auVar, Bitmap bitmap) {
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(au auVar, Bitmap bitmap) throws IOException;
    }

    /*
    static {
        wr<ow> wrVar = ow.f;
    }
    */

    @DexIgnore
    public pw(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, au auVar, xt xtVar) {
        this.d = list;
        q00.a(displayMetrics);
        this.b = displayMetrics;
        q00.a(auVar);
        this.a = auVar;
        q00.a(xtVar);
        this.c = xtVar;
    }

    @DexIgnore
    public static boolean a(int i2) {
        return i2 == 90 || i2 == 270;
    }

    @DexIgnore
    public static int b(double d2) {
        if (d2 > 1.0d) {
            d2 = 1.0d / d2;
        }
        return (int) Math.round(d2 * 2.147483647E9d);
    }

    @DexIgnore
    public static int c(double d2) {
        return (int) (d2 + 0.5d);
    }

    @DexIgnore
    public static void c(BitmapFactory.Options options) {
        d(options);
        synchronized (m) {
            m.offer(options);
        }
    }

    @DexIgnore
    public static void d(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    @DexIgnore
    public boolean a(ParcelFileDescriptor parcelFileDescriptor) {
        return os.c();
    }

    @DexIgnore
    public boolean a(InputStream inputStream) {
        return true;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer) {
        return true;
    }

    @DexIgnore
    public static int[] b(vw vwVar, BitmapFactory.Options options, b bVar, au auVar) throws IOException {
        options.inJustDecodeBounds = true;
        a(vwVar, options, bVar, auVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    @DexIgnore
    public rt<Bitmap> a(InputStream inputStream, int i2, int i3, xr xrVar) throws IOException {
        return a(inputStream, i2, i3, xrVar, k);
    }

    @DexIgnore
    public rt<Bitmap> a(InputStream inputStream, int i2, int i3, xr xrVar, b bVar) throws IOException {
        return a((vw) new vw.a(inputStream, this.d, this.c), i2, i3, xrVar, bVar);
    }

    @DexIgnore
    public rt<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i2, int i3, xr xrVar) throws IOException {
        return a((vw) new vw.b(parcelFileDescriptor, this.d, this.c), i2, i3, xrVar, k);
    }

    @DexIgnore
    public final rt<Bitmap> a(vw vwVar, int i2, int i3, xr xrVar, b bVar) throws IOException {
        xr xrVar2 = xrVar;
        byte[] bArr = (byte[]) this.c.b(65536, byte[].class);
        BitmapFactory.Options a2 = a();
        a2.inTempStorage = bArr;
        qr qrVar = (qr) xrVar2.a(f);
        yr yrVar = (yr) xrVar2.a(g);
        try {
            return hw.a(a(vwVar, a2, (ow) xrVar2.a(ow.f), qrVar, yrVar, xrVar2.a(i) != null && ((Boolean) xrVar2.a(i)).booleanValue(), i2, i3, ((Boolean) xrVar2.a(h)).booleanValue(), bVar), this.a);
        } finally {
            c(a2);
            this.c.put(bArr);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r1.inDensity;
     */
    @DexIgnore
    public static boolean b(BitmapFactory.Options options) {
        int i2;
        int i3 = options.inTargetDensity;
        return i3 > 0 && i2 > 0 && i3 != i2;
    }

    @DexIgnore
    public final Bitmap a(vw vwVar, BitmapFactory.Options options, ow owVar, qr qrVar, yr yrVar, boolean z, int i2, int i3, boolean z2, b bVar) throws IOException {
        int i4;
        int i5;
        pw pwVar;
        int i6;
        ColorSpace colorSpace;
        int i7;
        int i8;
        BitmapFactory.Options options2 = options;
        b bVar2 = bVar;
        long a2 = m00.a();
        int[] b2 = b(vwVar, options2, bVar2, this.a);
        boolean z3 = false;
        int i9 = b2[0];
        int i10 = b2[1];
        String str = options2.outMimeType;
        boolean z4 = (i9 == -1 || i10 == -1) ? false : z;
        int a3 = vwVar.a();
        int a4 = cx.a(a3);
        boolean b3 = cx.b(a3);
        int i11 = i2;
        if (i11 == Integer.MIN_VALUE) {
            i5 = i3;
            i4 = a(a4) ? i10 : i9;
        } else {
            i5 = i3;
            i4 = i11;
        }
        int i12 = i5 == Integer.MIN_VALUE ? a(a4) ? i9 : i10 : i5;
        ImageHeaderParser.ImageType c2 = vwVar.c();
        au auVar = this.a;
        ImageHeaderParser.ImageType imageType = c2;
        a(c2, vwVar, bVar, auVar, owVar, a4, i9, i10, i4, i12, options);
        int i13 = a3;
        String str2 = str;
        int i14 = i10;
        int i15 = i9;
        b bVar3 = bVar2;
        BitmapFactory.Options options3 = options2;
        a(vwVar, qrVar, z4, b3, options, i4, i12);
        boolean z5 = Build.VERSION.SDK_INT >= 19;
        if (options3.inSampleSize == 1 || z5) {
            pwVar = this;
            if (pwVar.a(imageType)) {
                if (i15 < 0 || i14 < 0 || !z2 || !z5) {
                    float f2 = b(options) ? ((float) options3.inTargetDensity) / ((float) options3.inDensity) : 1.0f;
                    int i16 = options3.inSampleSize;
                    float f3 = (float) i16;
                    i8 = Math.round(((float) ((int) Math.ceil((double) (((float) i15) / f3)))) * f2);
                    i7 = Math.round(((float) ((int) Math.ceil((double) (((float) i14) / f3)))) * f2);
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculated target [" + i8 + "x" + i7 + "] for source [" + i15 + "x" + i14 + "], sampleSize: " + i16 + ", targetDensity: " + options3.inTargetDensity + ", density: " + options3.inDensity + ", density multiplier: " + f2);
                    }
                } else {
                    i8 = i4;
                    i7 = i12;
                }
                if (i8 > 0 && i7 > 0) {
                    a(options3, pwVar.a, i8, i7);
                }
            }
        } else {
            pwVar = this;
        }
        int i17 = Build.VERSION.SDK_INT;
        if (i17 >= 28) {
            if (yrVar == yr.DISPLAY_P3 && (colorSpace = options3.outColorSpace) != null && colorSpace.isWideGamut()) {
                z3 = true;
            }
            options3.inPreferredColorSpace = ColorSpace.get(z3 ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB);
        } else if (i17 >= 26) {
            options3.inPreferredColorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
        }
        Bitmap a5 = a(vwVar, options3, bVar3, pwVar.a);
        bVar3.a(pwVar.a, a5);
        if (Log.isLoggable("Downsampler", 2)) {
            i6 = i13;
            a(i15, i14, str2, options, a5, i2, i3, a2);
        } else {
            i6 = i13;
        }
        Bitmap bitmap = null;
        if (a5 != null) {
            a5.setDensity(pwVar.b.densityDpi);
            bitmap = cx.a(pwVar.a, a5, i6);
            if (!a5.equals(bitmap)) {
                pwVar.a.a(a5);
            }
        }
        return bitmap;
    }

    @DexIgnore
    public static void a(ImageHeaderParser.ImageType imageType, vw vwVar, b bVar, au auVar, ow owVar, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) throws IOException {
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        double d2;
        ImageHeaderParser.ImageType imageType2 = imageType;
        ow owVar2 = owVar;
        int i13 = i3;
        int i14 = i4;
        int i15 = i5;
        int i16 = i6;
        BitmapFactory.Options options2 = options;
        if (i13 <= 0 || i14 <= 0) {
            String str = "x";
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Unable to determine dimensions for: " + imageType2 + " with target [" + i15 + str + i16 + "]");
                return;
            }
            return;
        }
        if (a(i2)) {
            i7 = i13;
            i8 = i14;
        } else {
            i8 = i13;
            i7 = i14;
        }
        float b2 = owVar2.b(i8, i7, i15, i16);
        if (b2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            ow.g a2 = owVar2.a(i8, i7, i15, i16);
            if (a2 != null) {
                float f2 = (float) i8;
                float f3 = (float) i7;
                int c2 = i8 / c((double) (b2 * f2));
                int c3 = i7 / c((double) (b2 * f3));
                if (a2 == ow.g.MEMORY) {
                    i9 = Math.max(c2, c3);
                } else {
                    i9 = Math.min(c2, c3);
                }
                String str2 = "x";
                if (Build.VERSION.SDK_INT > 23 || !j.contains(options2.outMimeType)) {
                    i10 = Math.max(1, Integer.highestOneBit(i9));
                    if (a2 == ow.g.MEMORY && ((float) i10) < 1.0f / b2) {
                        i10 <<= 1;
                    }
                } else {
                    i10 = 1;
                }
                options2.inSampleSize = i10;
                if (imageType2 == ImageHeaderParser.ImageType.JPEG) {
                    float min = (float) Math.min(i10, 8);
                    i11 = (int) Math.ceil((double) (f2 / min));
                    i12 = (int) Math.ceil((double) (f3 / min));
                    int i17 = i10 / 8;
                    if (i17 > 0) {
                        i11 /= i17;
                        i12 /= i17;
                    }
                } else {
                    if (imageType2 == ImageHeaderParser.ImageType.PNG || imageType2 == ImageHeaderParser.ImageType.PNG_A) {
                        float f4 = (float) i10;
                        i11 = (int) Math.floor((double) (f2 / f4));
                        d2 = Math.floor((double) (f3 / f4));
                    } else if (imageType2 == ImageHeaderParser.ImageType.WEBP || imageType2 == ImageHeaderParser.ImageType.WEBP_A) {
                        if (Build.VERSION.SDK_INT >= 24) {
                            float f5 = (float) i10;
                            i11 = Math.round(f2 / f5);
                            i12 = Math.round(f3 / f5);
                        } else {
                            float f6 = (float) i10;
                            i11 = (int) Math.floor((double) (f2 / f6));
                            d2 = Math.floor((double) (f3 / f6));
                        }
                    } else if (i8 % i10 == 0 && i7 % i10 == 0) {
                        i11 = i8 / i10;
                        i12 = i7 / i10;
                    } else {
                        int[] b3 = b(vwVar, options2, bVar, auVar);
                        i11 = b3[0];
                        i12 = b3[1];
                    }
                    i12 = (int) d2;
                }
                double b4 = (double) owVar2.b(i11, i12, i15, i16);
                if (Build.VERSION.SDK_INT >= 19) {
                    options2.inTargetDensity = a(b4);
                    options2.inDensity = b(b4);
                }
                if (b(options)) {
                    options2.inScaled = true;
                } else {
                    options2.inTargetDensity = 0;
                    options2.inDensity = 0;
                }
                if (Log.isLoggable("Downsampler", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Calculate scaling, source: [");
                    sb.append(i3);
                    String str3 = str2;
                    sb.append(str3);
                    sb.append(i4);
                    sb.append("], degreesToRotate: ");
                    sb.append(i2);
                    sb.append(", target: [");
                    sb.append(i15);
                    sb.append(str3);
                    sb.append(i16);
                    sb.append("], power of two scaled: [");
                    sb.append(i11);
                    sb.append(str3);
                    sb.append(i12);
                    sb.append("], exact scale factor: ");
                    sb.append(b2);
                    sb.append(", power of 2 sample size: ");
                    sb.append(i10);
                    sb.append(", adjusted scale factor: ");
                    sb.append(b4);
                    sb.append(", target density: ");
                    sb.append(options2.inTargetDensity);
                    sb.append(", density: ");
                    sb.append(options2.inDensity);
                    Log.v("Downsampler", sb.toString());
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("Cannot round with null rounding");
        }
        String str4 = "x";
        int i18 = i13;
        throw new IllegalArgumentException("Cannot scale with factor: " + b2 + " from: " + owVar2 + ", source: [" + i18 + str4 + i14 + "], target: [" + i15 + str4 + i16 + "]");
    }

    @DexIgnore
    public static int a(double d2) {
        int b2 = b(d2);
        int c2 = c(((double) b2) * d2);
        return c((d2 / ((double) (((float) c2) / ((float) b2)))) * ((double) c2));
    }

    @DexIgnore
    public final boolean a(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return l.contains(imageType);
    }

    @DexIgnore
    public final void a(vw vwVar, qr qrVar, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        if (!this.e.a(i2, i3, options, z, z2)) {
            if (qrVar == qr.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            boolean z3 = false;
            try {
                z3 = vwVar.c().hasAlpha();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + qrVar, e2);
                }
            }
            options.inPreferredConfig = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            if (options.inPreferredConfig == Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        throw r1;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0050 */
    public static Bitmap a(vw vwVar, BitmapFactory.Options options, b bVar, au auVar) throws IOException {
        if (!options.inJustDecodeBounds) {
            bVar.a();
            vwVar.b();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        String str = options.outMimeType;
        cx.a().lock();
        try {
            return vwVar.a(options);
        } catch (IllegalArgumentException e2) {
            IOException a2 = a(e2, i2, i3, str, options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", a2);
            }
            if (options.inBitmap != null) {
                auVar.a(options.inBitmap);
                options.inBitmap = null;
                return a(vwVar, options, bVar, auVar);
            }
            throw a2;
        } finally {
            cx.a().unlock();
        }
    }

    @DexIgnore
    public static void a(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        Log.v("Downsampler", "Decoded " + a(bitmap) + " from [" + i2 + "x" + i3 + "] " + str + " with inBitmap " + a(options) + " for [" + i4 + "x" + i5 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + m00.a(j2));
    }

    @DexIgnore
    public static String a(BitmapFactory.Options options) {
        return a(options.inBitmap);
    }

    @DexIgnore
    @TargetApi(19)
    public static String a(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    @DexIgnore
    public static IOException a(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + a(options), illegalArgumentException);
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(BitmapFactory.Options options, au auVar, int i2, int i3) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = auVar.b(i2, i3, config);
    }

    @DexIgnore
    public static synchronized BitmapFactory.Options a() {
        BitmapFactory.Options poll;
        synchronized (pw.class) {
            synchronized (m) {
                poll = m.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                d(poll);
            }
        }
        return poll;
    }
}
