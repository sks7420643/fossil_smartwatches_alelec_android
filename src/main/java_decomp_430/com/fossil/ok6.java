package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok6 extends vk6 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(ok6.class, "_resumed");
    @DexIgnore
    public volatile int _resumed;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ok6(xe6<?> xe6, Throwable th, boolean z) {
        super(th, z);
        wg6.b(xe6, "continuation");
        if (th == null) {
            th = new CancellationException("Continuation " + xe6 + " was cancelled normally");
        }
        this._resumed = 0;
    }

    @DexIgnore
    public final boolean c() {
        return c.compareAndSet(this, 0, 1);
    }
}
