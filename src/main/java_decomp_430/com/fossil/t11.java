package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t11 implements Parcelable.Creator<p31> {
    @DexIgnore
    public /* synthetic */ t11(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new p31(parcel);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new p31[i];
    }
}
