package com.fossil;

import com.fossil.NotificationAppHelper;
import com.fossil.mz4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1", f = "NotificationAppHelper.kt", l = {99}, m = "invokeSuspend")
public final class vx5$a$a extends sf6 implements ig6<il6, xe6<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $callSettingsType;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $messageSettingsType;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppHelper.a this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vx5$a$a(NotificationAppHelper.a aVar, hh6 hh6, hh6 hh62, xe6 xe6) {
        super(2, xe6);
        this.this$0 = aVar;
        this.$callSettingsType = hh6;
        this.$messageSettingsType = hh62;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        vx5$a$a vx5_a_a = new vx5$a$a(this.this$0, this.$callSettingsType, this.$messageSettingsType, xe6);
        vx5_a_a.p$ = (il6) obj;
        return vx5_a_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((vx5$a$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            List arrayList = new ArrayList();
            Object r3 = this.this$0.$getAllContactGroup;
            this.L$0 = il6;
            this.L$1 = arrayList;
            this.label = 1;
            obj = n24.a(r3, null, this);
            if (obj == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
        if (cVar instanceof mz4.d) {
            List<T> d = yd6.d(((mz4.d) cVar).a());
            int i2 = this.$callSettingsType.element;
            if (i2 == 0) {
                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                list.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
            } else if (i2 == 1) {
                int size = d.size();
                for (int i3 = 0; i3 < size; i3++) {
                    ContactGroup contactGroup = d.get(i3);
                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                    List contacts = contactGroup.getContacts();
                    wg6.a((Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        Object obj2 = contactGroup.getContacts().get(0);
                        wg6.a(obj2, "item.contacts[0]");
                        appNotificationFilter.setSender(((Contact) obj2).getDisplayName());
                        list.add(appNotificationFilter);
                    }
                }
            }
            int i4 = this.$messageSettingsType.element;
            if (i4 == 0) {
                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                list.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
            } else if (i4 == 1) {
                int size2 = d.size();
                for (int i5 = 0; i5 < size2; i5++) {
                    ContactGroup contactGroup2 = d.get(i5);
                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    FNotification fNotification = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                    List contacts2 = contactGroup2.getContacts();
                    wg6.a((Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification);
                        Object obj3 = contactGroup2.getContacts().get(0);
                        wg6.a(obj3, "item.contacts[0]");
                        appNotificationFilter2.setSender(((Contact) obj3).getDisplayName());
                        list.add(appNotificationFilter2);
                    }
                }
            }
        }
        return list;
    }
}
