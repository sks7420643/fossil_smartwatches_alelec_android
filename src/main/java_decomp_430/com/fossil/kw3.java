package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kw3 extends IOException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -1616151763072450476L;

    @DexIgnore
    public kw3(String str) {
        super(str);
    }

    @DexIgnore
    public static kw3 invalidEndTag() {
        return new kw3("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static kw3 invalidTag() {
        return new kw3("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static kw3 invalidWireType() {
        return new kw3("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static kw3 malformedVarint() {
        return new kw3("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static kw3 negativeSize() {
        return new kw3("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static kw3 recursionLimitExceeded() {
        return new kw3("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    @DexIgnore
    public static kw3 sizeLimitExceeded() {
        return new kw3("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }

    @DexIgnore
    public static kw3 truncatedMessage() {
        return new kw3("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }
}
