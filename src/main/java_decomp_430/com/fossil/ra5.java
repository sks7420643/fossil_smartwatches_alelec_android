package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ra5 implements Factory<ae5> {
    @DexIgnore
    public static ae5 a(na5 na5) {
        ae5 d = na5.d();
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
