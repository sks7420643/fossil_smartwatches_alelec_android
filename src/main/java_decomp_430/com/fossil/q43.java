package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.api.Status;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q43 extends z33 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public List<String> j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;

    @DexIgnore
    public q43(x53 x53, long j2) {
        super(x53);
        this.i = j2;
    }

    @DexIgnore
    public final String A() {
        w();
        return this.c;
    }

    @DexIgnore
    public final String B() {
        w();
        return this.l;
    }

    @DexIgnore
    public final String C() {
        w();
        return this.m;
    }

    @DexIgnore
    public final int D() {
        w();
        return this.e;
    }

    @DexIgnore
    public final int E() {
        w();
        return this.k;
    }

    @DexIgnore
    public final List<String> F() {
        return this.j;
    }

    @DexIgnore
    public final String G() {
        if (!tu2.a() || !l().a(l03.N0)) {
            try {
                Class<?> loadClass = c().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                if (loadClass == null) {
                    return null;
                }
                try {
                    Object invoke = loadClass.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke((Object) null, new Object[]{c()});
                    if (invoke == null) {
                        return null;
                    }
                    try {
                        return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                    } catch (Exception unused) {
                        b().y().a("Failed to retrieve Firebase Instance Id");
                        return null;
                    }
                } catch (Exception unused2) {
                    b().x().a("Failed to obtain Firebase Analytics instance");
                    return null;
                }
            } catch (ClassNotFoundException unused3) {
                return null;
            }
        } else {
            b().B().a("Disabled IID for tests.");
            return null;
        }
    }

    @DexIgnore
    public final ra3 a(String str) {
        String str2;
        String str3;
        Boolean b;
        g();
        e();
        String A = A();
        String B = B();
        w();
        String str4 = this.d;
        long D = (long) D();
        w();
        String str5 = this.f;
        long n2 = l().n();
        w();
        g();
        if (this.g == 0) {
            this.g = this.a.w().a(c(), c().getPackageName());
        }
        long j2 = this.g;
        boolean g2 = this.a.g();
        boolean z = !k().x;
        g();
        e();
        if (!this.a.g()) {
            str2 = null;
        } else {
            str2 = G();
        }
        w();
        boolean z2 = g2;
        long j3 = this.h;
        long h2 = this.a.h();
        int E = E();
        boolean booleanValue = l().r().booleanValue();
        cb3 l2 = l();
        l2.e();
        Boolean b2 = l2.b("google_analytics_ssaid_collection_enabled");
        boolean booleanValue2 = Boolean.valueOf(b2 == null || b2.booleanValue()).booleanValue();
        boolean y = k().y();
        String C = C();
        long j4 = j3;
        Boolean valueOf = (!l().e(A(), l03.i0) || (b = l().b("google_analytics_default_allow_ad_personalization_signals")) == null) ? null : Boolean.valueOf(!b.booleanValue());
        long j5 = this.i;
        List<String> list = l().e(A(), l03.t0) ? this.j : null;
        if (!bt2.a() || !l().e(A(), l03.K0)) {
            str3 = null;
        } else {
            w();
            str3 = this.n;
        }
        return new ra3(A, B, str4, D, str5, n2, j2, str, z2, z, str2, j4, h2, E, booleanValue, booleanValue2, y, C, valueOf, j5, list, str3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0197 A[Catch:{ IllegalStateException -> 0x0205 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0199 A[Catch:{ IllegalStateException -> 0x0205 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01a4 A[SYNTHETIC, Splitter:B:73:0x01a4] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01e2 A[Catch:{ IllegalStateException -> 0x0205 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01f3 A[Catch:{ IllegalStateException -> 0x0205 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0228  */
    public final void u() {
        boolean z;
        boolean z2;
        String a;
        String str;
        String packageName = c().getPackageName();
        PackageManager packageManager = c().getPackageManager();
        String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str3 = "";
        String str4 = "unknown";
        int i2 = RecyclerView.UNDEFINED_DURATION;
        if (packageManager == null) {
            b().t().a("PackageManager is null, app identity information might be inaccurate. appId", t43.a(packageName));
        } else {
            try {
                str4 = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException unused) {
                b().t().a("Error retrieving app installer package name. appId", t43.a(packageName));
            }
            if (str4 == null) {
                str4 = "manual_install";
            } else if ("com.android.vending".equals(str4)) {
                str4 = str3;
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(c().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    str = !TextUtils.isEmpty(applicationLabel) ? applicationLabel.toString() : str2;
                    try {
                        str2 = packageInfo.versionName;
                        i2 = packageInfo.versionCode;
                    } catch (PackageManager.NameNotFoundException unused2) {
                    }
                }
            } catch (PackageManager.NameNotFoundException unused3) {
                str = str2;
                b().t().a("Error retrieving package info. appId, appName", t43.a(packageName), str);
                this.c = packageName;
                this.f = str4;
                this.d = str2;
                this.e = i2;
                this.g = 0;
                d();
                Status a2 = rw1.a(c());
                boolean z3 = true;
                z = (a2 != null && a2.F()) | (!TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B()));
                if (!z) {
                }
                if (z) {
                }
                z2 = false;
                this.l = str3;
                this.m = str3;
                this.n = str3;
                this.h = 0;
                d();
                this.m = this.a.A();
                a = rw1.a();
                this.l = TextUtils.isEmpty(a) ? str3 : a;
                if (bt2.a()) {
                }
                if (!TextUtils.isEmpty(a)) {
                }
                if (z2) {
                }
                this.j = null;
                if (l().e(this.c, l03.t0)) {
                }
                if (Build.VERSION.SDK_INT >= 16) {
                }
            }
        }
        this.c = packageName;
        this.f = str4;
        this.d = str2;
        this.e = i2;
        this.g = 0;
        d();
        Status a22 = rw1.a(c());
        boolean z32 = true;
        z = (a22 != null && a22.F()) | (!TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B()));
        if (!z) {
            if (a22 == null) {
                b().t().a("GoogleService failed to initialize (no status)");
            } else {
                b().t().a("GoogleService failed to initialize, status", Integer.valueOf(a22.B()), a22.C());
            }
        }
        if (z) {
            Boolean q = l().q();
            if (l().p()) {
                if (this.a.z()) {
                    b().z().a("Collection disabled with firebase_analytics_collection_deactivated=1");
                }
            } else if (q == null || q.booleanValue()) {
                if (q != null || !rw1.b()) {
                    b().B().a("Collection enabled");
                    z2 = true;
                    this.l = str3;
                    this.m = str3;
                    this.n = str3;
                    this.h = 0;
                    d();
                    if (!TextUtils.isEmpty(this.a.A()) && "am".equals(this.a.B())) {
                        this.m = this.a.A();
                    }
                    a = rw1.a();
                    this.l = TextUtils.isEmpty(a) ? str3 : a;
                    if (bt2.a()) {
                        if (l().e(packageName, l03.K0)) {
                            c22 c22 = new c22(c());
                            String a3 = c22.a("ga_app_id");
                            if (!TextUtils.isEmpty(a3)) {
                                str3 = a3;
                            }
                            this.n = str3;
                            if (!TextUtils.isEmpty(a) || !TextUtils.isEmpty(a3)) {
                                this.m = c22.a("admob_app_id");
                            }
                            if (z2) {
                                b().B().a("App package, google app id", this.c, this.l);
                            }
                            this.j = null;
                            if (l().e(this.c, l03.t0)) {
                                d();
                                List<String> c2 = l().c("analytics.safelisted_events");
                                if (c2 != null) {
                                    if (c2.size() != 0) {
                                        Iterator<String> it = c2.iterator();
                                        while (true) {
                                            if (!it.hasNext()) {
                                                break;
                                            }
                                            if (!j().b("safelisted event", it.next())) {
                                                break;
                                            }
                                        }
                                    } else {
                                        b().w().a("Safelisted event list cannot be empty. Ignoring");
                                    }
                                    z32 = false;
                                }
                                if (z32) {
                                    this.j = c2;
                                }
                            }
                            if (Build.VERSION.SDK_INT >= 16) {
                                this.k = 0;
                                return;
                            } else if (packageManager != null) {
                                this.k = e52.a(c()) ? 1 : 0;
                                return;
                            } else {
                                this.k = 0;
                                return;
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(a)) {
                        this.m = new c22(c()).a("admob_app_id");
                    }
                    if (z2) {
                    }
                    this.j = null;
                    if (l().e(this.c, l03.t0)) {
                    }
                    if (Build.VERSION.SDK_INT >= 16) {
                    }
                } else {
                    b().z().a("Collection disabled with google_app_measurement_enable=0");
                }
            } else if (this.a.z()) {
                b().z().a("Collection disabled with firebase_analytics_collection_enabled=0");
            }
        }
        z2 = false;
        this.l = str3;
        this.m = str3;
        this.n = str3;
        this.h = 0;
        d();
        this.m = this.a.A();
        try {
            a = rw1.a();
            this.l = TextUtils.isEmpty(a) ? str3 : a;
            if (bt2.a()) {
            }
            if (!TextUtils.isEmpty(a)) {
            }
            if (z2) {
            }
        } catch (IllegalStateException e2) {
            b().t().a("getGoogleAppId or isMeasurementEnabled failed with exception. appId", t43.a(packageName), e2);
        }
        this.j = null;
        if (l().e(this.c, l03.t0)) {
        }
        if (Build.VERSION.SDK_INT >= 16) {
        }
    }

    @DexIgnore
    public final boolean z() {
        return true;
    }
}
