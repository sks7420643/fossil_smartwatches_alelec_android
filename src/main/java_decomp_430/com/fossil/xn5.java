package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xn5 implements Factory<wn5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public xn5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static xn5 a(Provider<ThemeRepository> provider) {
        return new xn5(provider);
    }

    @DexIgnore
    public static wn5 b(Provider<ThemeRepository> provider) {
        return new CustomizeHeartRateChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeHeartRateChartViewModel get() {
        return b(this.a);
    }
}
