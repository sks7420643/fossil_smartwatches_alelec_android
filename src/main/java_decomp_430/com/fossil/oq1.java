package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq1 {
    @DexIgnore
    public static <TInput, TResult, TException extends Throwable> TResult a(int i, TInput tinput, nq1<TInput, TResult, TException> nq1, pq1<TInput, TResult> pq1) throws Throwable {
        TResult apply;
        if (i < 1) {
            return nq1.apply(tinput);
        }
        do {
            apply = nq1.apply(tinput);
            tinput = pq1.a(tinput, apply);
            if (tinput == null || i - 1 < 1) {
                return apply;
            }
            apply = nq1.apply(tinput);
            tinput = pq1.a(tinput, apply);
            break;
        } while (i - 1 < 1);
        return apply;
    }
}
