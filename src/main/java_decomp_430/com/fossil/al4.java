package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class al4 {
    @DexIgnore
    public static int a(int i) {
        if (i >= 60) {
            return i / 60;
        }
        return 0;
    }

    @DexIgnore
    public static int b(int i) {
        return i >= 60 ? i - ((i / 60) * 60) : i;
    }

    @DexIgnore
    public static int c(int i) {
        if (i < 1 || i > 12) {
            return -1;
        }
        return (i * 30) - 1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public static String d(int i) {
        int i2 = i / 60;
        int i3 = i % 60;
        if (i2 == 0) {
            return i3 + " " + jm4.a((Context) PortfolioApp.T, 2131886118);
        } else if (i2 <= 0 || i3 != 0) {
            return String.format(jm4.a((Context) PortfolioApp.T, 2131886483), new Object[]{Integer.valueOf(i2), Integer.valueOf(i3)});
        } else {
            return i2 + " " + jm4.a((Context) PortfolioApp.T, 2131886117);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public static SpannableString e(int i) {
        int i2 = i / 60;
        int i3 = i % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.T)) {
            return new SpannableString(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i2)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}));
        }
        int i4 = 12;
        if (i < 720) {
            if (i2 == 0) {
                i2 = 12;
            }
            return yk4.b.a(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i2)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}), jm4.a((Context) PortfolioApp.T, 2131886102), 1.0f);
        }
        if (i2 > 12) {
            i4 = i2 - 12;
        }
        return yk4.b.a(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i4)}) + ":" + String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(i3)}), jm4.a((Context) PortfolioApp.T, 2131886104), 1.0f);
    }

    @DexIgnore
    public static int a() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }

    @DexIgnore
    public static String a(Date date) {
        return DateFormat.format("MMMM dd", date).toString();
    }

    @DexIgnore
    public static int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone.inDaylightTime(Calendar.getInstance().getTime())) {
            return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 60000;
        }
        return timeZone.getRawOffset() / 60000;
    }
}
