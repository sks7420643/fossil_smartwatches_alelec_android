package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m93 extends z33 {
    @DexIgnore
    public Handler c;
    @DexIgnore
    public u93 d; // = new u93(this);
    @DexIgnore
    public s93 e; // = new s93(this);
    @DexIgnore
    public r93 f; // = new r93(this);

    @DexIgnore
    public m93(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final void A() {
        a().a((Runnable) new p93(this, zzm().c()));
    }

    @DexIgnore
    public final void B() {
        g();
        if (this.c == null) {
            this.c = new br2(Looper.getMainLooper());
        }
    }

    @DexIgnore
    public final void a(long j) {
        g();
        B();
        b().B().a("Activity resumed, time", Long.valueOf(j));
        this.f.a(j);
        this.e.a(j);
        this.d.a(j);
    }

    @DexIgnore
    public final void b(long j) {
        g();
        B();
        b().B().a("Activity paused, time", Long.valueOf(j));
        this.f.b(j);
        this.e.b(j);
        u93 u93 = this.d;
        if (u93.b.l().e(u93.b.p().A(), l03.a0)) {
            u93.b.k().y.a(true);
        }
    }

    @DexIgnore
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final boolean a(boolean z, boolean z2) {
        return this.e.a(z, z2);
    }
}
