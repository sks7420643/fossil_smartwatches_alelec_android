package com.fossil;

import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk3 extends jn3<Object> implements Serializable {
    @DexIgnore
    public static /* final */ yk3 INSTANCE; // = new yk3();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public int compare(Object obj, Object obj2) {
        return 0;
    }

    @DexIgnore
    public <E> zl3<E> immutableSortedCopy(Iterable<E> iterable) {
        return zl3.copyOf(iterable);
    }

    @DexIgnore
    public <S> jn3<S> reverse() {
        return this;
    }

    @DexIgnore
    public <E> List<E> sortedCopy(Iterable<E> iterable) {
        return um3.a(iterable);
    }

    @DexIgnore
    public String toString() {
        return "Ordering.allEqual()";
    }
}
