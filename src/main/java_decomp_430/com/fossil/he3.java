package com.fossil;

import com.fossil.ud3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he3 implements td3 {
    @DexIgnore
    public /* final */ ud3.b a;

    @DexIgnore
    public he3(ud3.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void a(sd3 sd3) {
        this.a.a(ge3.a(sd3));
    }

    @DexIgnore
    public final void b(sd3 sd3, int i, int i2) {
        this.a.b(ge3.a(sd3), i, i2);
    }

    @DexIgnore
    public final void c(sd3 sd3, int i, int i2) {
        this.a.c(ge3.a(sd3), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || he3.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((he3) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final void a(sd3 sd3, int i, int i2) {
        this.a.a(ge3.a(sd3), i, i2);
    }
}
