package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a0 {
    @DexIgnore
    public static /* final */ int actionBarDivider; // = 2130968646;
    @DexIgnore
    public static /* final */ int actionBarItemBackground; // = 2130968647;
    @DexIgnore
    public static /* final */ int actionBarPopupTheme; // = 2130968648;
    @DexIgnore
    public static /* final */ int actionBarSize; // = 2130968649;
    @DexIgnore
    public static /* final */ int actionBarSplitStyle; // = 2130968650;
    @DexIgnore
    public static /* final */ int actionBarStyle; // = 2130968651;
    @DexIgnore
    public static /* final */ int actionBarTabBarStyle; // = 2130968652;
    @DexIgnore
    public static /* final */ int actionBarTabStyle; // = 2130968653;
    @DexIgnore
    public static /* final */ int actionBarTabTextStyle; // = 2130968654;
    @DexIgnore
    public static /* final */ int actionBarTheme; // = 2130968655;
    @DexIgnore
    public static /* final */ int actionBarWidgetTheme; // = 2130968656;
    @DexIgnore
    public static /* final */ int actionButtonStyle; // = 2130968657;
    @DexIgnore
    public static /* final */ int actionDropDownStyle; // = 2130968658;
    @DexIgnore
    public static /* final */ int actionLayout; // = 2130968659;
    @DexIgnore
    public static /* final */ int actionMenuTextAppearance; // = 2130968660;
    @DexIgnore
    public static /* final */ int actionMenuTextColor; // = 2130968661;
    @DexIgnore
    public static /* final */ int actionModeBackground; // = 2130968662;
    @DexIgnore
    public static /* final */ int actionModeCloseButtonStyle; // = 2130968663;
    @DexIgnore
    public static /* final */ int actionModeCloseDrawable; // = 2130968664;
    @DexIgnore
    public static /* final */ int actionModeCopyDrawable; // = 2130968665;
    @DexIgnore
    public static /* final */ int actionModeCutDrawable; // = 2130968666;
    @DexIgnore
    public static /* final */ int actionModeFindDrawable; // = 2130968667;
    @DexIgnore
    public static /* final */ int actionModePasteDrawable; // = 2130968668;
    @DexIgnore
    public static /* final */ int actionModePopupWindowStyle; // = 2130968669;
    @DexIgnore
    public static /* final */ int actionModeSelectAllDrawable; // = 2130968670;
    @DexIgnore
    public static /* final */ int actionModeShareDrawable; // = 2130968671;
    @DexIgnore
    public static /* final */ int actionModeSplitBackground; // = 2130968672;
    @DexIgnore
    public static /* final */ int actionModeStyle; // = 2130968673;
    @DexIgnore
    public static /* final */ int actionModeWebSearchDrawable; // = 2130968674;
    @DexIgnore
    public static /* final */ int actionOverflowButtonStyle; // = 2130968675;
    @DexIgnore
    public static /* final */ int actionOverflowMenuStyle; // = 2130968676;
    @DexIgnore
    public static /* final */ int actionProviderClass; // = 2130968677;
    @DexIgnore
    public static /* final */ int actionViewClass; // = 2130968679;
    @DexIgnore
    public static /* final */ int activityChooserViewStyle; // = 2130968688;
    @DexIgnore
    public static /* final */ int alertDialogButtonGroupStyle; // = 2130968738;
    @DexIgnore
    public static /* final */ int alertDialogCenterButtons; // = 2130968739;
    @DexIgnore
    public static /* final */ int alertDialogStyle; // = 2130968740;
    @DexIgnore
    public static /* final */ int alertDialogTheme; // = 2130968741;
    @DexIgnore
    public static /* final */ int allowStacking; // = 2130968744;
    @DexIgnore
    public static /* final */ int alpha; // = 2130968745;
    @DexIgnore
    public static /* final */ int alphabeticModifiers; // = 2130968749;
    @DexIgnore
    public static /* final */ int arrowHeadLength; // = 2130968754;
    @DexIgnore
    public static /* final */ int arrowShaftLength; // = 2130968755;
    @DexIgnore
    public static /* final */ int autoCompleteTextViewStyle; // = 2130968757;
    @DexIgnore
    public static /* final */ int autoSizeMaxTextSize; // = 2130968759;
    @DexIgnore
    public static /* final */ int autoSizeMinTextSize; // = 2130968760;
    @DexIgnore
    public static /* final */ int autoSizePresetSizes; // = 2130968761;
    @DexIgnore
    public static /* final */ int autoSizeStepGranularity; // = 2130968762;
    @DexIgnore
    public static /* final */ int autoSizeTextType; // = 2130968763;
    @DexIgnore
    public static /* final */ int background; // = 2130968768;
    @DexIgnore
    public static /* final */ int backgroundSplit; // = 2130968776;
    @DexIgnore
    public static /* final */ int backgroundStacked; // = 2130968777;
    @DexIgnore
    public static /* final */ int backgroundTint; // = 2130968779;
    @DexIgnore
    public static /* final */ int backgroundTintMode; // = 2130968780;
    @DexIgnore
    public static /* final */ int barLength; // = 2130968785;
    @DexIgnore
    public static /* final */ int borderlessButtonStyle; // = 2130968867;
    @DexIgnore
    public static /* final */ int buttonBarButtonStyle; // = 2130968887;
    @DexIgnore
    public static /* final */ int buttonBarNegativeButtonStyle; // = 2130968888;
    @DexIgnore
    public static /* final */ int buttonBarNeutralButtonStyle; // = 2130968889;
    @DexIgnore
    public static /* final */ int buttonBarPositiveButtonStyle; // = 2130968890;
    @DexIgnore
    public static /* final */ int buttonBarStyle; // = 2130968891;
    @DexIgnore
    public static /* final */ int buttonCompat; // = 2130968892;
    @DexIgnore
    public static /* final */ int buttonGravity; // = 2130968893;
    @DexIgnore
    public static /* final */ int buttonIconDimen; // = 2130968894;
    @DexIgnore
    public static /* final */ int buttonPanelSideLayout; // = 2130968895;
    @DexIgnore
    public static /* final */ int buttonStyle; // = 2130968903;
    @DexIgnore
    public static /* final */ int buttonStyleSmall; // = 2130968904;
    @DexIgnore
    public static /* final */ int buttonTint; // = 2130968905;
    @DexIgnore
    public static /* final */ int buttonTintMode; // = 2130968906;
    @DexIgnore
    public static /* final */ int checkboxStyle; // = 2130968943;
    @DexIgnore
    public static /* final */ int checkedTextViewStyle; // = 2130968950;
    @DexIgnore
    public static /* final */ int closeIcon; // = 2130968987;
    @DexIgnore
    public static /* final */ int closeItemLayout; // = 2130968994;
    @DexIgnore
    public static /* final */ int collapseContentDescription; // = 2130968996;
    @DexIgnore
    public static /* final */ int collapseIcon; // = 2130968997;
    @DexIgnore
    public static /* final */ int color; // = 2130969002;
    @DexIgnore
    public static /* final */ int colorAccent; // = 2130969003;
    @DexIgnore
    public static /* final */ int colorBackgroundFloating; // = 2130969005;
    @DexIgnore
    public static /* final */ int colorButtonNormal; // = 2130969006;
    @DexIgnore
    public static /* final */ int colorControlActivated; // = 2130969007;
    @DexIgnore
    public static /* final */ int colorControlHighlight; // = 2130969008;
    @DexIgnore
    public static /* final */ int colorControlNormal; // = 2130969009;
    @DexIgnore
    public static /* final */ int colorError; // = 2130969010;
    @DexIgnore
    public static /* final */ int colorPrimary; // = 2130969017;
    @DexIgnore
    public static /* final */ int colorPrimaryDark; // = 2130969018;
    @DexIgnore
    public static /* final */ int colorSwitchThumbNormal; // = 2130969025;
    @DexIgnore
    public static /* final */ int commitIcon; // = 2130969038;
    @DexIgnore
    public static /* final */ int contentDescription; // = 2130969053;
    @DexIgnore
    public static /* final */ int contentInsetEnd; // = 2130969054;
    @DexIgnore
    public static /* final */ int contentInsetEndWithActions; // = 2130969055;
    @DexIgnore
    public static /* final */ int contentInsetLeft; // = 2130969056;
    @DexIgnore
    public static /* final */ int contentInsetRight; // = 2130969057;
    @DexIgnore
    public static /* final */ int contentInsetStart; // = 2130969058;
    @DexIgnore
    public static /* final */ int contentInsetStartWithNavigation; // = 2130969059;
    @DexIgnore
    public static /* final */ int controlBackground; // = 2130969066;
    @DexIgnore
    public static /* final */ int customNavigationLayout; // = 2130969151;
    @DexIgnore
    public static /* final */ int defaultQueryHint; // = 2130969198;
    @DexIgnore
    public static /* final */ int dialogCornerRadius; // = 2130969222;
    @DexIgnore
    public static /* final */ int dialogPreferredPadding; // = 2130969223;
    @DexIgnore
    public static /* final */ int dialogTheme; // = 2130969224;
    @DexIgnore
    public static /* final */ int displayOptions; // = 2130969225;
    @DexIgnore
    public static /* final */ int divider; // = 2130969227;
    @DexIgnore
    public static /* final */ int dividerHorizontal; // = 2130969228;
    @DexIgnore
    public static /* final */ int dividerPadding; // = 2130969229;
    @DexIgnore
    public static /* final */ int dividerVertical; // = 2130969230;
    @DexIgnore
    public static /* final */ int drawableBottomCompat; // = 2130969233;
    @DexIgnore
    public static /* final */ int drawableEndCompat; // = 2130969234;
    @DexIgnore
    public static /* final */ int drawableLeftCompat; // = 2130969235;
    @DexIgnore
    public static /* final */ int drawableRightCompat; // = 2130969236;
    @DexIgnore
    public static /* final */ int drawableSize; // = 2130969237;
    @DexIgnore
    public static /* final */ int drawableStartCompat; // = 2130969238;
    @DexIgnore
    public static /* final */ int drawableTint; // = 2130969239;
    @DexIgnore
    public static /* final */ int drawableTintMode; // = 2130969240;
    @DexIgnore
    public static /* final */ int drawableTopCompat; // = 2130969241;
    @DexIgnore
    public static /* final */ int drawerArrowStyle; // = 2130969242;
    @DexIgnore
    public static /* final */ int dropDownListViewStyle; // = 2130969243;
    @DexIgnore
    public static /* final */ int dropdownListPreferredItemHeight; // = 2130969244;
    @DexIgnore
    public static /* final */ int editTextBackground; // = 2130969247;
    @DexIgnore
    public static /* final */ int editTextColor; // = 2130969248;
    @DexIgnore
    public static /* final */ int editTextStyle; // = 2130969250;
    @DexIgnore
    public static /* final */ int elevation; // = 2130969258;
    @DexIgnore
    public static /* final */ int expandActivityOverflowButtonDrawable; // = 2130969288;
    @DexIgnore
    public static /* final */ int firstBaselineToTopHeight; // = 2130969315;
    @DexIgnore
    public static /* final */ int font; // = 2130969326;
    @DexIgnore
    public static /* final */ int fontFamily; // = 2130969327;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969331;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969332;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969333;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969334;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969335;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969336;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969337;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969338;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969339;
    @DexIgnore
    public static /* final */ int gapBetweenBars; // = 2130969346;
    @DexIgnore
    public static /* final */ int goIcon; // = 2130969348;
    @DexIgnore
    public static /* final */ int height; // = 2130969379;
    @DexIgnore
    public static /* final */ int hideOnContentScroll; // = 2130969387;
    @DexIgnore
    public static /* final */ int homeAsUpIndicator; // = 2130969395;
    @DexIgnore
    public static /* final */ int homeLayout; // = 2130969396;
    @DexIgnore
    public static /* final */ int icon; // = 2130969398;
    @DexIgnore
    public static /* final */ int iconTint; // = 2130969404;
    @DexIgnore
    public static /* final */ int iconTintMode; // = 2130969405;
    @DexIgnore
    public static /* final */ int iconifiedByDefault; // = 2130969411;
    @DexIgnore
    public static /* final */ int imageButtonStyle; // = 2130969415;
    @DexIgnore
    public static /* final */ int indeterminateProgressStyle; // = 2130969420;
    @DexIgnore
    public static /* final */ int initialActivityCount; // = 2130969431;
    @DexIgnore
    public static /* final */ int isLightTheme; // = 2130969440;
    @DexIgnore
    public static /* final */ int itemPadding; // = 2130969454;
    @DexIgnore
    public static /* final */ int lastBaselineToBottomHeight; // = 2130969472;
    @DexIgnore
    public static /* final */ int layout; // = 2130969477;
    @DexIgnore
    public static /* final */ int lineHeight; // = 2130969554;
    @DexIgnore
    public static /* final */ int listChoiceBackgroundIndicator; // = 2130969557;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorMultipleAnimated; // = 2130969558;
    @DexIgnore
    public static /* final */ int listChoiceIndicatorSingleAnimated; // = 2130969559;
    @DexIgnore
    public static /* final */ int listDividerAlertDialog; // = 2130969560;
    @DexIgnore
    public static /* final */ int listItemLayout; // = 2130969561;
    @DexIgnore
    public static /* final */ int listLayout; // = 2130969562;
    @DexIgnore
    public static /* final */ int listMenuViewStyle; // = 2130969563;
    @DexIgnore
    public static /* final */ int listPopupWindowStyle; // = 2130969564;
    @DexIgnore
    public static /* final */ int listPreferredItemHeight; // = 2130969565;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightLarge; // = 2130969566;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightSmall; // = 2130969567;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingEnd; // = 2130969568;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingLeft; // = 2130969569;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingRight; // = 2130969570;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingStart; // = 2130969571;
    @DexIgnore
    public static /* final */ int logo; // = 2130969576;
    @DexIgnore
    public static /* final */ int logoDescription; // = 2130969577;
    @DexIgnore
    public static /* final */ int maxButtonHeight; // = 2130969611;
    @DexIgnore
    public static /* final */ int measureWithLargestChild; // = 2130969616;
    @DexIgnore
    public static /* final */ int menu; // = 2130969617;
    @DexIgnore
    public static /* final */ int multiChoiceItemLayout; // = 2130969630;
    @DexIgnore
    public static /* final */ int navigationContentDescription; // = 2130969635;
    @DexIgnore
    public static /* final */ int navigationIcon; // = 2130969636;
    @DexIgnore
    public static /* final */ int navigationMode; // = 2130969637;
    @DexIgnore
    public static /* final */ int numericModifiers; // = 2130969657;
    @DexIgnore
    public static /* final */ int overlapAnchor; // = 2130969658;
    @DexIgnore
    public static /* final */ int paddingBottomNoButtons; // = 2130969659;
    @DexIgnore
    public static /* final */ int paddingEnd; // = 2130969660;
    @DexIgnore
    public static /* final */ int paddingStart; // = 2130969662;
    @DexIgnore
    public static /* final */ int paddingTopNoTitle; // = 2130969663;
    @DexIgnore
    public static /* final */ int panelBackground; // = 2130969665;
    @DexIgnore
    public static /* final */ int panelMenuListTheme; // = 2130969666;
    @DexIgnore
    public static /* final */ int panelMenuListWidth; // = 2130969667;
    @DexIgnore
    public static /* final */ int popupMenuStyle; // = 2130969676;
    @DexIgnore
    public static /* final */ int popupTheme; // = 2130969677;
    @DexIgnore
    public static /* final */ int popupWindowStyle; // = 2130969678;
    @DexIgnore
    public static /* final */ int preserveIconSpacing; // = 2130969679;
    @DexIgnore
    public static /* final */ int progressBarPadding; // = 2130969689;
    @DexIgnore
    public static /* final */ int progressBarStyle; // = 2130969690;
    @DexIgnore
    public static /* final */ int queryBackground; // = 2130969703;
    @DexIgnore
    public static /* final */ int queryHint; // = 2130969704;
    @DexIgnore
    public static /* final */ int radioButtonStyle; // = 2130969705;
    @DexIgnore
    public static /* final */ int ratingBarStyle; // = 2130969708;
    @DexIgnore
    public static /* final */ int ratingBarStyleIndicator; // = 2130969709;
    @DexIgnore
    public static /* final */ int ratingBarStyleSmall; // = 2130969710;
    @DexIgnore
    public static /* final */ int searchHintIcon; // = 2130969758;
    @DexIgnore
    public static /* final */ int searchIcon; // = 2130969759;
    @DexIgnore
    public static /* final */ int searchViewStyle; // = 2130969761;
    @DexIgnore
    public static /* final */ int seekBarStyle; // = 2130969762;
    @DexIgnore
    public static /* final */ int selectableItemBackground; // = 2130969763;
    @DexIgnore
    public static /* final */ int selectableItemBackgroundBorderless; // = 2130969764;
    @DexIgnore
    public static /* final */ int showAsAction; // = 2130969792;
    @DexIgnore
    public static /* final */ int showDividers; // = 2130969793;
    @DexIgnore
    public static /* final */ int showText; // = 2130969795;
    @DexIgnore
    public static /* final */ int showTitle; // = 2130969796;
    @DexIgnore
    public static /* final */ int singleChoiceItemLayout; // = 2130969798;
    @DexIgnore
    public static /* final */ int spinBars; // = 2130969864;
    @DexIgnore
    public static /* final */ int spinnerDropDownItemStyle; // = 2130969865;
    @DexIgnore
    public static /* final */ int spinnerStyle; // = 2130969866;
    @DexIgnore
    public static /* final */ int splitTrack; // = 2130969867;
    @DexIgnore
    public static /* final */ int srcCompat; // = 2130969868;
    @DexIgnore
    public static /* final */ int state_above_anchor; // = 2130969877;
    @DexIgnore
    public static /* final */ int subMenuArrow; // = 2130969899;
    @DexIgnore
    public static /* final */ int submitBackground; // = 2130969901;
    @DexIgnore
    public static /* final */ int subtitle; // = 2130969902;
    @DexIgnore
    public static /* final */ int subtitleTextAppearance; // = 2130969903;
    @DexIgnore
    public static /* final */ int subtitleTextColor; // = 2130969904;
    @DexIgnore
    public static /* final */ int subtitleTextStyle; // = 2130969905;
    @DexIgnore
    public static /* final */ int suggestionRowLayout; // = 2130969906;
    @DexIgnore
    public static /* final */ int switchMinWidth; // = 2130969910;
    @DexIgnore
    public static /* final */ int switchPadding; // = 2130969911;
    @DexIgnore
    public static /* final */ int switchStyle; // = 2130969912;
    @DexIgnore
    public static /* final */ int switchTextAppearance; // = 2130969913;
    @DexIgnore
    public static /* final */ int textAllCaps; // = 2130969952;
    @DexIgnore
    public static /* final */ int textAppearanceLargePopupMenu; // = 2130969963;
    @DexIgnore
    public static /* final */ int textAppearanceListItem; // = 2130969965;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSecondary; // = 2130969966;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSmall; // = 2130969967;
    @DexIgnore
    public static /* final */ int textAppearancePopupMenuHeader; // = 2130969969;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultSubtitle; // = 2130969970;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultTitle; // = 2130969971;
    @DexIgnore
    public static /* final */ int textAppearanceSmallPopupMenu; // = 2130969972;
    @DexIgnore
    public static /* final */ int textColorAlertDialogListItem; // = 2130969984;
    @DexIgnore
    public static /* final */ int textColorSearchUrl; // = 2130969985;
    @DexIgnore
    public static /* final */ int textLocale; // = 2130969997;
    @DexIgnore
    public static /* final */ int theme; // = 2130970013;
    @DexIgnore
    public static /* final */ int thickness; // = 2130970016;
    @DexIgnore
    public static /* final */ int thumbTextPadding; // = 2130970017;
    @DexIgnore
    public static /* final */ int thumbTint; // = 2130970018;
    @DexIgnore
    public static /* final */ int thumbTintMode; // = 2130970019;
    @DexIgnore
    public static /* final */ int tickMark; // = 2130970024;
    @DexIgnore
    public static /* final */ int tickMarkTint; // = 2130970025;
    @DexIgnore
    public static /* final */ int tickMarkTintMode; // = 2130970026;
    @DexIgnore
    public static /* final */ int tint; // = 2130970028;
    @DexIgnore
    public static /* final */ int tintMode; // = 2130970029;
    @DexIgnore
    public static /* final */ int title; // = 2130970031;
    @DexIgnore
    public static /* final */ int titleMargin; // = 2130970033;
    @DexIgnore
    public static /* final */ int titleMarginBottom; // = 2130970034;
    @DexIgnore
    public static /* final */ int titleMarginEnd; // = 2130970035;
    @DexIgnore
    public static /* final */ int titleMarginStart; // = 2130970036;
    @DexIgnore
    public static /* final */ int titleMarginTop; // = 2130970037;
    @DexIgnore
    public static /* final */ int titleMargins; // = 2130970038;
    @DexIgnore
    public static /* final */ int titleTextAppearance; // = 2130970039;
    @DexIgnore
    public static /* final */ int titleTextColor; // = 2130970040;
    @DexIgnore
    public static /* final */ int titleTextStyle; // = 2130970041;
    @DexIgnore
    public static /* final */ int toolbarNavigationButtonStyle; // = 2130970049;
    @DexIgnore
    public static /* final */ int toolbarStyle; // = 2130970050;
    @DexIgnore
    public static /* final */ int tooltipForegroundColor; // = 2130970051;
    @DexIgnore
    public static /* final */ int tooltipFrameBackground; // = 2130970052;
    @DexIgnore
    public static /* final */ int tooltipText; // = 2130970053;
    @DexIgnore
    public static /* final */ int track; // = 2130970055;
    @DexIgnore
    public static /* final */ int trackTint; // = 2130970056;
    @DexIgnore
    public static /* final */ int trackTintMode; // = 2130970057;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130970059;
    @DexIgnore
    public static /* final */ int viewInflaterClass; // = 2130970079;
    @DexIgnore
    public static /* final */ int voiceIcon; // = 2130970095;
    @DexIgnore
    public static /* final */ int windowActionBar; // = 2130970162;
    @DexIgnore
    public static /* final */ int windowActionBarOverlay; // = 2130970163;
    @DexIgnore
    public static /* final */ int windowActionModeOverlay; // = 2130970164;
    @DexIgnore
    public static /* final */ int windowFixedHeightMajor; // = 2130970165;
    @DexIgnore
    public static /* final */ int windowFixedHeightMinor; // = 2130970166;
    @DexIgnore
    public static /* final */ int windowFixedWidthMajor; // = 2130970167;
    @DexIgnore
    public static /* final */ int windowFixedWidthMinor; // = 2130970168;
    @DexIgnore
    public static /* final */ int windowMinWidthMajor; // = 2130970169;
    @DexIgnore
    public static /* final */ int windowMinWidthMinor; // = 2130970170;
    @DexIgnore
    public static /* final */ int windowNoTitle; // = 2130970171;
}
