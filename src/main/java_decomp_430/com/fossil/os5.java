package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os5 {
    @DexIgnore
    public ns5 a;

    @DexIgnore
    public os5(ns5 ns5) {
        wg6.b(ns5, "mPairingView");
        this.a = ns5;
    }

    @DexIgnore
    public final ns5 a() {
        return this.a;
    }
}
