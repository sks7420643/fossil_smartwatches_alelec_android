package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yt implements hu {
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public /* final */ du<a, Bitmap> b; // = new du<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends zt<a> {
        @DexIgnore
        public a a(int i, int i2, Bitmap.Config config) {
            a aVar = (a) b();
            aVar.a(i, i2, config);
            return aVar;
        }

        @DexIgnore
        public a a() {
            return new a(this);
        }
    }

    @DexIgnore
    public static String d(Bitmap bitmap) {
        return c(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.b.a(this.a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @DexIgnore
    public String b(int i, int i2, Bitmap.Config config) {
        return c(i, i2, config);
    }

    @DexIgnore
    public String c(Bitmap bitmap) {
        return d(bitmap);
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }

    @DexIgnore
    public static String c(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    @DexIgnore
    public int b(Bitmap bitmap) {
        return r00.a(bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements iu {
        @DexIgnore
        public /* final */ b a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public Bitmap.Config d;

        @DexIgnore
        public a(b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c && this.d == aVar.d) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            int i = ((this.b * 31) + this.c) * 31;
            Bitmap.Config config = this.d;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return yt.c(this.b, this.c, this.d);
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.a.a(i, i2, config));
    }

    @DexIgnore
    public Bitmap a() {
        return this.b.a();
    }
}
