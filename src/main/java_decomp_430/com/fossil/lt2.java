package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt2 implements mt2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.lifecycle.app_backgrounded_engagement", false);
        b = vk2.a("measurement.lifecycle.app_backgrounded_tracking", false);
        c = vk2.a("measurement.lifecycle.app_in_background_parameter", false);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return c.b().booleanValue();
    }
}
