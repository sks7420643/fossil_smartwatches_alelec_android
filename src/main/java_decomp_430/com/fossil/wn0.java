package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn0 implements Parcelable.Creator<op0> {
    @DexIgnore
    public /* synthetic */ wn0(qg6 qg6) {
    }

    @DexIgnore
    public op0 createFromParcel(Parcel parcel) {
        return new op0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new op0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m67createFromParcel(Parcel parcel) {
        return new op0(parcel, (qg6) null);
    }
}
