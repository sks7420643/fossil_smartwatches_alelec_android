package com.fossil;

import android.database.Cursor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn implements un {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh b;
    @DexIgnore
    public /* final */ vh c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hh<tn> {
        @DexIgnore
        public a(vn vnVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(mi miVar, tn tnVar) {
            String str = tnVar.a;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            miVar.a(2, (long) tnVar.b);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo`(`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends vh {
        @DexIgnore
        public b(vn vnVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public vn(oh ohVar) {
        this.a = ohVar;
        this.b = new a(this, ohVar);
        this.c = new b(this, ohVar);
    }

    @DexIgnore
    public void a(tn tnVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(tnVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.s();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public tn a(String str) {
        rh b2 = rh.b("SELECT * FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? new tn(a2.getString(ai.b(a2, "work_spec_id")), a2.getInt(ai.b(a2, "system_id"))) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
