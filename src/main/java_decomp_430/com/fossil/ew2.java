package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ew2> CREATOR; // = new pw2();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore
    public ew2(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.a = z;
        this.b = z2;
        this.c = z3;
        this.d = z4;
        this.e = z5;
        this.f = z6;
    }

    @DexIgnore
    public final boolean B() {
        return this.c;
    }

    @DexIgnore
    public final boolean C() {
        return this.d;
    }

    @DexIgnore
    public final boolean D() {
        return this.a;
    }

    @DexIgnore
    public final boolean E() {
        return this.e;
    }

    @DexIgnore
    public final boolean F() {
        return this.b;
    }

    @DexIgnore
    public final boolean p() {
        return this.f;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, D());
        g22.a(parcel, 2, F());
        g22.a(parcel, 3, B());
        g22.a(parcel, 4, C());
        g22.a(parcel, 5, E());
        g22.a(parcel, 6, p());
        g22.a(parcel, a2);
    }
}
