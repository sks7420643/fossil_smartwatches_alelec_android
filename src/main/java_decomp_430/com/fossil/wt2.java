package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt2 implements tt2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Double> b;
    @DexIgnore
    public static /* final */ pk2<Long> c;
    @DexIgnore
    public static /* final */ pk2<Long> d;
    @DexIgnore
    public static /* final */ pk2<String> e;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.test.boolean_flag", false);
        b = vk2.a("measurement.test.double_flag", -3.0d);
        c = vk2.a("measurement.test.int_flag", -2);
        d = vk2.a("measurement.test.long_flag", -1);
        e = vk2.a("measurement.test.string_flag", "---");
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final double zzb() {
        return b.b().doubleValue();
    }

    @DexIgnore
    public final long zzc() {
        return c.b().longValue();
    }

    @DexIgnore
    public final long zzd() {
        return d.b().longValue();
    }

    @DexIgnore
    public final String zze() {
        return e.b();
    }
}
