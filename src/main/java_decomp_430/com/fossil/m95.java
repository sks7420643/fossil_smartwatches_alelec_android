package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m95 {
    @DexIgnore
    public /* final */ k95 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public m95(k95 k95, String str, String str2) {
        wg6.b(k95, "mView");
        wg6.b(str, "mPresetId");
        wg6.b(str2, "mMicroAppPos");
        this.a = k95;
    }

    @DexIgnore
    public final k95 a() {
        return this.a;
    }
}
