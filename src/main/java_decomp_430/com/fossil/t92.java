package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t92 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ s92 c;

    @DexIgnore
    public t92(s92 s92, Intent intent, Intent intent2) {
        this.c = s92;
        this.a = intent;
        this.b = intent2;
    }

    @DexIgnore
    public final void run() {
        this.c.handleIntent(this.a);
        this.c.a(this.b);
    }
}
