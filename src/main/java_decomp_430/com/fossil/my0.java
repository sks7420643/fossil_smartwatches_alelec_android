package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my0 {
    @DexIgnore
    public /* synthetic */ my0(qg6 qg6) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        r2 = com.fossil.cw0.a((java.lang.Enum<?>) r2);
     */
    @DexIgnore
    public final String a(short s) {
        String a;
        if (s == 256) {
            return "legacy_activity_file";
        }
        if (s == 23131) {
            return "legacy_ota";
        }
        switch (s) {
            case 1792:
                return "asset_background_file";
            case 1793:
                return "asset_notification_icon_file";
            case 1794:
                return "asset_localization_file";
            default:
                w31 a2 = w31.x.a(s);
                return (a2 == null || a == null) ? "unknown" : a;
        }
    }
}
