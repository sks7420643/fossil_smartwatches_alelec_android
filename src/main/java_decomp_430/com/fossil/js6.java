package com.fossil;

import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js6 {
    @DexIgnore
    public static /* final */ is6[] a; // = {new is6(is6.i, ""), new is6(is6.f, "GET"), new is6(is6.f, "POST"), new is6(is6.g, (String) ZendeskConfig.SLASH), new is6(is6.g, "/index.html"), new is6(is6.h, "http"), new is6(is6.h, "https"), new is6(is6.e, "200"), new is6(is6.e, "204"), new is6(is6.e, "206"), new is6(is6.e, "304"), new is6(is6.e, "400"), new is6(is6.e, "404"), new is6(is6.e, "500"), new is6("accept-charset", ""), new is6("accept-encoding", "gzip, deflate"), new is6("accept-language", ""), new is6("accept-ranges", ""), new is6("accept", ""), new is6("access-control-allow-origin", ""), new is6("age", ""), new is6("allow", ""), new is6((String) Constants.IF_AUTHORIZATION, ""), new is6("cache-control", ""), new is6("content-disposition", ""), new is6("content-encoding", ""), new is6("content-language", ""), new is6("content-length", ""), new is6("content-location", ""), new is6("content-range", ""), new is6("content-type", ""), new is6("cookie", ""), new is6((String) HardwareLog.COLUMN_DATE, ""), new is6((String) Constants.JSON_KEY_ETAG, ""), new is6("expect", ""), new is6("expires", ""), new is6("from", ""), new is6("host", ""), new is6("if-match", ""), new is6("if-modified-since", ""), new is6("if-none-match", ""), new is6("if-range", ""), new is6("if-unmodified-since", ""), new is6("last-modified", ""), new is6("link", ""), new is6("location", ""), new is6("max-forwards", ""), new is6("proxy-authenticate", ""), new is6("proxy-authorization", ""), new is6("range", ""), new is6("referer", ""), new is6("refresh", ""), new is6("retry-after", ""), new is6("server", ""), new is6("set-cookie", ""), new is6("strict-transport-security", ""), new is6("transfer-encoding", ""), new is6("user-agent", ""), new is6("vary", ""), new is6("via", ""), new is6("www-authenticate", "")};
    @DexIgnore
    public static /* final */ Map<mt6, Integer> b; // = a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<is6> a;
        @DexIgnore
        public /* final */ lt6 b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public is6[] e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;

        @DexIgnore
        public a(int i, zt6 zt6) {
            this(i, i, zt6);
        }

        @DexIgnore
        public final void a() {
            int i = this.d;
            int i2 = this.h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                b();
            } else {
                b(i2 - i);
            }
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.e, (Object) null);
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public List<is6> c() {
            ArrayList arrayList = new ArrayList(this.a);
            this.a.clear();
            return arrayList;
        }

        @DexIgnore
        public final boolean d(int i) {
            return i >= 0 && i <= js6.a.length - 1;
        }

        @DexIgnore
        public final void e(int i) throws IOException {
            if (d(i)) {
                this.a.add(js6.a[i]);
                return;
            }
            int a2 = a(i - js6.a.length);
            if (a2 >= 0) {
                is6[] is6Arr = this.e;
                if (a2 < is6Arr.length) {
                    this.a.add(is6Arr[a2]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public void f() throws IOException {
            while (!this.b.f()) {
                byte readByte = this.b.readByte() & 255;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    e(a((int) readByte, 127) - 1);
                } else if (readByte == 64) {
                    g();
                } else if ((readByte & 64) == 64) {
                    f(a((int) readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    this.d = a((int) readByte, 31);
                    int i = this.d;
                    if (i < 0 || i > this.c) {
                        throw new IOException("Invalid dynamic table size update " + this.d);
                    }
                    a();
                } else if (readByte == 16 || readByte == 0) {
                    h();
                } else {
                    g(a((int) readByte, 15) - 1);
                }
            }
        }

        @DexIgnore
        public final void g(int i) throws IOException {
            this.a.add(new is6(c(i), e()));
        }

        @DexIgnore
        public final void h() throws IOException {
            mt6 e2 = e();
            js6.a(e2);
            this.a.add(new is6(e2, e()));
        }

        @DexIgnore
        public a(int i, int i2, zt6 zt6) {
            this.a = new ArrayList();
            this.e = new is6[8];
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
            this.c = i;
            this.d = i2;
            this.b = st6.a(zt6);
        }

        @DexIgnore
        public final int d() throws IOException {
            return this.b.readByte() & 255;
        }

        @DexIgnore
        public final mt6 c(int i) throws IOException {
            if (d(i)) {
                return js6.a[i].a;
            }
            int a2 = a(i - js6.a.length);
            if (a2 >= 0) {
                is6[] is6Arr = this.e;
                if (a2 < is6Arr.length) {
                    return is6Arr[a2].a;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public final int a(int i) {
            return this.f + 1 + i;
        }

        @DexIgnore
        public final void g() throws IOException {
            mt6 e2 = e();
            js6.a(e2);
            a(-1, new is6(e2, e()));
        }

        @DexIgnore
        public final void a(int i, is6 is6) {
            this.a.add(is6);
            int i2 = is6.c;
            if (i != -1) {
                i2 -= this.e[a(i)].c;
            }
            int i3 = this.d;
            if (i2 > i3) {
                b();
                return;
            }
            int b2 = b((this.h + i2) - i3);
            if (i == -1) {
                int i4 = this.g + 1;
                is6[] is6Arr = this.e;
                if (i4 > is6Arr.length) {
                    is6[] is6Arr2 = new is6[(is6Arr.length * 2)];
                    System.arraycopy(is6Arr, 0, is6Arr2, is6Arr.length, is6Arr.length);
                    this.f = this.e.length - 1;
                    this.e = is6Arr2;
                }
                int i5 = this.f;
                this.f = i5 - 1;
                this.e[i5] = is6;
                this.g++;
            } else {
                this.e[i + a(i) + b2] = is6;
            }
            this.h += i2;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.e.length;
                while (true) {
                    length--;
                    if (length < this.f || i <= 0) {
                        is6[] is6Arr = this.e;
                        int i3 = this.f;
                        System.arraycopy(is6Arr, i3 + 1, is6Arr, i3 + 1 + i2, this.g);
                        this.f += i2;
                    } else {
                        is6[] is6Arr2 = this.e;
                        i -= is6Arr2[length].c;
                        this.h -= is6Arr2[length].c;
                        this.g--;
                        i2++;
                    }
                }
                is6[] is6Arr3 = this.e;
                int i32 = this.f;
                System.arraycopy(is6Arr3, i32 + 1, is6Arr3, i32 + 1 + i2, this.g);
                this.f += i2;
            }
            return i2;
        }

        @DexIgnore
        public mt6 e() throws IOException {
            int d2 = d();
            boolean z = (d2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 128;
            int a2 = a(d2, 127);
            if (z) {
                return mt6.of(qs6.b().a(this.b.h((long) a2)));
            }
            return this.b.e((long) a2);
        }

        @DexIgnore
        public final void f(int i) throws IOException {
            a(-1, new is6(c(i), e()));
        }

        @DexIgnore
        public int a(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int d2 = d();
                if ((d2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0) {
                    return i2 + (d2 << i4);
                }
                i2 += (d2 & 127) << i4;
                i4 += 7;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ jt6 a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;
        @DexIgnore
        public is6[] f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;

        @DexIgnore
        public b(jt6 jt6) {
            this(4096, true, jt6);
        }

        @DexIgnore
        public final int a(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.f.length;
                while (true) {
                    length--;
                    if (length < this.g || i2 <= 0) {
                        is6[] is6Arr = this.f;
                        int i4 = this.g;
                        System.arraycopy(is6Arr, i4 + 1, is6Arr, i4 + 1 + i3, this.h);
                        is6[] is6Arr2 = this.f;
                        int i5 = this.g;
                        Arrays.fill(is6Arr2, i5 + 1, i5 + 1 + i3, (Object) null);
                        this.g += i3;
                    } else {
                        is6[] is6Arr3 = this.f;
                        i2 -= is6Arr3[length].c;
                        this.i -= is6Arr3[length].c;
                        this.h--;
                        i3++;
                    }
                }
                is6[] is6Arr4 = this.f;
                int i42 = this.g;
                System.arraycopy(is6Arr4, i42 + 1, is6Arr4, i42 + 1 + i3, this.h);
                is6[] is6Arr22 = this.f;
                int i52 = this.g;
                Arrays.fill(is6Arr22, i52 + 1, i52 + 1 + i3, (Object) null);
                this.g += i3;
            }
            return i3;
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.f, (Object) null);
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
        }

        @DexIgnore
        public b(int i2, boolean z, jt6 jt6) {
            this.c = Integer.MAX_VALUE;
            this.f = new is6[8];
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
            this.e = i2;
            this.b = z;
            this.a = jt6;
        }

        @DexIgnore
        public void b(int i2) {
            int min = Math.min(i2, 16384);
            int i3 = this.e;
            if (i3 != min) {
                if (min < i3) {
                    this.c = Math.min(this.c, min);
                }
                this.d = true;
                this.e = min;
                a();
            }
        }

        @DexIgnore
        public final void a(is6 is6) {
            int i2 = is6.c;
            int i3 = this.e;
            if (i2 > i3) {
                b();
                return;
            }
            a((this.i + i2) - i3);
            int i4 = this.h + 1;
            is6[] is6Arr = this.f;
            if (i4 > is6Arr.length) {
                is6[] is6Arr2 = new is6[(is6Arr.length * 2)];
                System.arraycopy(is6Arr, 0, is6Arr2, is6Arr.length, is6Arr.length);
                this.g = this.f.length - 1;
                this.f = is6Arr2;
            }
            int i5 = this.g;
            this.g = i5 - 1;
            this.f[i5] = is6;
            this.h++;
            this.i += i2;
        }

        @DexIgnore
        public void a(List<is6> list) throws IOException {
            int i2;
            int i3;
            if (this.d) {
                int i4 = this.c;
                if (i4 < this.e) {
                    a(i4, 31, 32);
                }
                this.d = false;
                this.c = Integer.MAX_VALUE;
                a(this.e, 31, 32);
            }
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                is6 is6 = list.get(i5);
                mt6 asciiLowercase = is6.a.toAsciiLowercase();
                mt6 mt6 = is6.b;
                Integer num = js6.b.get(asciiLowercase);
                if (num != null) {
                    i3 = num.intValue() + 1;
                    if (i3 > 1 && i3 < 8) {
                        if (fr6.a((Object) js6.a[i3 - 1].b, (Object) mt6)) {
                            i2 = i3;
                        } else if (fr6.a((Object) js6.a[i3].b, (Object) mt6)) {
                            i2 = i3;
                            i3++;
                        }
                    }
                    i2 = i3;
                    i3 = -1;
                } else {
                    i3 = -1;
                    i2 = -1;
                }
                if (i3 == -1) {
                    int i6 = this.g + 1;
                    int length = this.f.length;
                    while (true) {
                        if (i6 >= length) {
                            break;
                        }
                        if (fr6.a((Object) this.f[i6].a, (Object) asciiLowercase)) {
                            if (fr6.a((Object) this.f[i6].b, (Object) mt6)) {
                                i3 = js6.a.length + (i6 - this.g);
                                break;
                            } else if (i2 == -1) {
                                i2 = (i6 - this.g) + js6.a.length;
                            }
                        }
                        i6++;
                    }
                }
                if (i3 != -1) {
                    a(i3, 127, Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                } else if (i2 == -1) {
                    this.a.writeByte(64);
                    a(asciiLowercase);
                    a(mt6);
                    a(is6);
                } else if (!asciiLowercase.startsWith(is6.d) || is6.i.equals(asciiLowercase)) {
                    a(i2, 63, 64);
                    a(mt6);
                    a(is6);
                } else {
                    a(i2, 15, 0);
                    a(mt6);
                }
            }
        }

        @DexIgnore
        public void a(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.a.writeByte(i2 | i4);
                return;
            }
            this.a.writeByte(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.a.writeByte(128 | (i5 & 127));
                i5 >>>= 7;
            }
            this.a.writeByte(i5);
        }

        @DexIgnore
        public void a(mt6 mt6) throws IOException {
            if (!this.b || qs6.b().a(mt6) >= mt6.size()) {
                a(mt6.size(), 127, 0);
                this.a.a(mt6);
                return;
            }
            jt6 jt6 = new jt6();
            qs6.b().a(mt6, jt6);
            mt6 m = jt6.m();
            a(m.size(), 127, Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            this.a.a(m);
        }

        @DexIgnore
        public final void a() {
            int i2 = this.e;
            int i3 = this.i;
            if (i2 >= i3) {
                return;
            }
            if (i2 == 0) {
                b();
            } else {
                a(i3 - i2);
            }
        }
    }

    @DexIgnore
    public static Map<mt6, Integer> a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(a.length);
        int i = 0;
        while (true) {
            is6[] is6Arr = a;
            if (i >= is6Arr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(is6Arr[i].a)) {
                linkedHashMap.put(a[i].a, Integer.valueOf(i));
            }
            i++;
        }
    }

    @DexIgnore
    public static mt6 a(mt6 mt6) throws IOException {
        int size = mt6.size();
        int i = 0;
        while (i < size) {
            byte b2 = mt6.getByte(i);
            if (b2 < 65 || b2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + mt6.utf8());
            }
        }
        return mt6;
    }
}
