package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ai4 {
    BACKGROUND(0),
    PHOTO(1),
    ADD(2);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ai4 a(int i) {
            ai4 ai4;
            ai4[] values = ai4.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    ai4 = null;
                    break;
                }
                ai4 = values[i2];
                if (ai4.getValue() == i) {
                    break;
                }
                i2++;
            }
            return ai4 != null ? ai4 : ai4.BACKGROUND;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public ai4(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final void setValue(int i) {
        this.value = i;
    }
}
