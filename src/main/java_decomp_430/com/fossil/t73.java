package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long a;
    @DexIgnore
    public /* final */ /* synthetic */ e73 b;

    @DexIgnore
    public t73(e73 e73, long j) {
        this.b = e73;
        this.a = j;
    }

    @DexIgnore
    public final void run() {
        this.b.k().p.a(this.a);
        this.b.b().A().a("Minimum session duration set", Long.valueOf(this.a));
    }
}
