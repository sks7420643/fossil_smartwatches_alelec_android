package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class df1 extends wm0 {
    @DexIgnore
    public /* final */ ArrayList<hl1> D; // = cw0.a(this.B, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.DEVICE_CONFIG}));
    @DexIgnore
    public c90 E;

    @DexIgnore
    public df1(ue1 ue1, q41 q41) {
        super(ue1, q41, eh1.GET_CURRENT_WORKOUT_SESSION, new px0(ue1));
    }

    @DexIgnore
    public void b(qv0 qv0) {
        this.E = ((px0) qv0).K;
    }

    @DexIgnore
    public Object d() {
        c90 c90 = this.E;
        return c90 != null ? c90 : new c90(0, a90.IDLE, b90.COMPLETED, 0, 0, 0, 0, 0, 0, 0, 0);
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.D;
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.WORKOUT_SESSION;
        c90 c90 = this.E;
        return cw0.a(k, bm0, (Object) c90 != null ? c90.a() : null);
    }
}
