package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv4 implements Factory<vv4> {
    @DexIgnore
    public static vv4 a(qv4 qv4, String str, ArrayList<Alarm> arrayList, Alarm alarm, zv4 zv4, rj4 rj4, yv4 yv4, UserRepository userRepository) {
        return new AlarmPresenter(qv4, str, arrayList, alarm, zv4, rj4, yv4, userRepository);
    }
}
