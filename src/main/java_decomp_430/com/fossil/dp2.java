package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp2 implements po2 {
    @DexIgnore
    public /* final */ ro2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public dp2(ro2 ro2, String str, Object[] objArr) {
        this.a = ro2;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        char c2 = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c2 |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.d = c2 | (charAt2 << i);
                return;
            }
        }
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object[] b() {
        return this.c;
    }

    @DexIgnore
    public final int zza() {
        return (this.d & 1) == 1 ? fn2.e.i : fn2.e.j;
    }

    @DexIgnore
    public final boolean zzb() {
        return (this.d & 2) == 2;
    }

    @DexIgnore
    public final ro2 zzc() {
        return this.a;
    }
}
