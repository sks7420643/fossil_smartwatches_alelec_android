package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.x1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b2 extends v1 implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, x1, View.OnKeyListener {
    @DexIgnore
    public static /* final */ int z; // = g0.abc_popup_menu_item_layout;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ q1 c;
    @DexIgnore
    public /* final */ p1 d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ x2 i;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener j; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener o; // = new b();
    @DexIgnore
    public PopupWindow.OnDismissListener p;
    @DexIgnore
    public View q;
    @DexIgnore
    public View r;
    @DexIgnore
    public x1.a s;
    @DexIgnore
    public ViewTreeObserver t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x; // = 0;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (b2.this.c() && !b2.this.i.m()) {
                View view = b2.this.r;
                if (view == null || !view.isShown()) {
                    b2.this.dismiss();
                } else {
                    b2.this.i.d();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = b2.this.t;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    b2.this.t = view.getViewTreeObserver();
                }
                b2 b2Var = b2.this;
                b2Var.t.removeGlobalOnLayoutListener(b2Var.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore
    public b2(Context context, q1 q1Var, View view, int i2, int i3, boolean z2) {
        this.b = context;
        this.c = q1Var;
        this.e = z2;
        this.d = new p1(q1Var, LayoutInflater.from(context), this.e, z);
        this.g = i2;
        this.h = i3;
        Resources resources = context.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d0.abc_config_prefDialogWidth));
        this.q = view;
        this.i = new x2(this.b, (AttributeSet) null, this.g, this.h);
        q1Var.a((x1) this, context);
    }

    @DexIgnore
    public void a(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    public void a(q1 q1Var) {
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.d.a(z2);
    }

    @DexIgnore
    public boolean c() {
        return !this.u && this.i.c();
    }

    @DexIgnore
    public void d() {
        if (!h()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @DexIgnore
    public void dismiss() {
        if (c()) {
            this.i.dismiss();
        }
    }

    @DexIgnore
    public ListView f() {
        return this.i.f();
    }

    @DexIgnore
    public final boolean h() {
        View view;
        if (c()) {
            return true;
        }
        if (this.u || (view = this.q) == null) {
            return false;
        }
        this.r = view;
        this.i.a((PopupWindow.OnDismissListener) this);
        this.i.a((AdapterView.OnItemClickListener) this);
        this.i.a(true);
        View view2 = this.r;
        boolean z2 = this.t == null;
        this.t = view2.getViewTreeObserver();
        if (z2) {
            this.t.addOnGlobalLayoutListener(this.j);
        }
        view2.addOnAttachStateChangeListener(this.o);
        this.i.a(view2);
        this.i.f(this.x);
        if (!this.v) {
            this.w = v1.a(this.d, (ViewGroup) null, this.b, this.f);
            this.v = true;
        }
        this.i.e(this.w);
        this.i.g(2);
        this.i.a(g());
        this.i.d();
        ListView f2 = this.i.f();
        f2.setOnKeyListener(this);
        if (this.y && this.c.h() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.b).inflate(g0.abc_popup_menu_header_item_layout, f2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.c.h());
            }
            frameLayout.setEnabled(false);
            f2.addHeaderView(frameLayout, (Object) null, false);
        }
        this.i.a((ListAdapter) this.d);
        this.i.d();
        return true;
    }

    @DexIgnore
    public void onDismiss() {
        this.u = true;
        this.c.close();
        ViewTreeObserver viewTreeObserver = this.t;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.t = this.r.getViewTreeObserver();
            }
            this.t.removeGlobalOnLayoutListener(this.j);
            this.t = null;
        }
        this.r.removeOnAttachStateChangeListener(this.o);
        PopupWindow.OnDismissListener onDismissListener = this.p;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.v = false;
        p1 p1Var = this.d;
        if (p1Var != null) {
            p1Var.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void b(int i2) {
        this.i.a(i2);
    }

    @DexIgnore
    public void c(int i2) {
        this.i.b(i2);
    }

    @DexIgnore
    public void c(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void a(x1.a aVar) {
        this.s = aVar;
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        if (c2Var.hasVisibleItems()) {
            w1 w1Var = new w1(this.b, c2Var, this.r, this.e, this.g, this.h);
            w1Var.a(this.s);
            w1Var.a(v1.b((q1) c2Var));
            w1Var.a(this.p);
            this.p = null;
            this.c.a(false);
            int a2 = this.i.a();
            int g2 = this.i.g();
            if ((Gravity.getAbsoluteGravity(this.x, x9.o(this.q)) & 7) == 5) {
                a2 += this.q.getWidth();
            }
            if (w1Var.a(a2, g2)) {
                x1.a aVar = this.s;
                if (aVar == null) {
                    return true;
                }
                aVar.a(c2Var);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z2) {
        if (q1Var == this.c) {
            dismiss();
            x1.a aVar = this.s;
            if (aVar != null) {
                aVar.a(q1Var, z2);
            }
        }
    }

    @DexIgnore
    public void a(View view) {
        this.q = view;
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.p = onDismissListener;
    }
}
