package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rq extends ByteArrayOutputStream {
    @DexIgnore
    public /* final */ gq a;

    @DexIgnore
    public rq(gq gqVar, int i) {
        this.a = gqVar;
        this.buf = this.a.a(Math.max(i, 256));
    }

    @DexIgnore
    public final void b(int i) {
        int i2 = this.count;
        if (i2 + i > this.buf.length) {
            byte[] a2 = this.a.a((i2 + i) * 2);
            System.arraycopy(this.buf, 0, a2, 0, this.count);
            this.a.a(this.buf);
            this.buf = a2;
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.a(this.buf);
        this.buf = null;
        super.close();
    }

    @DexIgnore
    public void finalize() {
        this.a.a(this.buf);
    }

    @DexIgnore
    public synchronized void write(byte[] bArr, int i, int i2) {
        b(i2);
        super.write(bArr, i, i2);
    }

    @DexIgnore
    public synchronized void write(int i) {
        b(1);
        super.write(i);
    }
}
