package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js4 implements Factory<is4> {
    @DexIgnore
    public /* final */ Provider<GoogleApiService> a;

    @DexIgnore
    public js4(Provider<GoogleApiService> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static js4 a(Provider<GoogleApiService> provider) {
        return new js4(provider);
    }

    @DexIgnore
    public static is4 b(Provider<GoogleApiService> provider) {
        return new GetAddress(provider.get());
    }

    @DexIgnore
    public GetAddress get() {
        return b(this.a);
    }
}
