package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveMinutesChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym5 implements Factory<xm5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public ym5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ym5 a(Provider<ThemeRepository> provider) {
        return new ym5(provider);
    }

    @DexIgnore
    public static xm5 b(Provider<ThemeRepository> provider) {
        return new CustomizeActiveMinutesChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeActiveMinutesChartViewModel get() {
        return b(this.a);
    }
}
