package com.fossil;

import android.content.ComponentName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f93 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ComponentName a;
    @DexIgnore
    public /* final */ /* synthetic */ d93 b;

    @DexIgnore
    public f93(d93 d93, ComponentName componentName) {
        this.b = d93;
        this.a = componentName;
    }

    @DexIgnore
    public final void run() {
        this.b.c.a(this.a);
    }
}
