package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l64 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ ImageView D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ NumberPicker F;
    @DexIgnore
    public /* final */ NumberPicker G;
    @DexIgnore
    public /* final */ NumberPicker H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat J;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleButton y;
    @DexIgnore
    public /* final */ FlexibleEditText z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l64(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleButton flexibleButton, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, ImageView imageView, RTLImageView rTLImageView, ImageView imageView2, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3, ConstraintLayout constraintLayout2, FlexibleSwitchCompat flexibleSwitchCompat, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = flexibleTextView4;
        this.v = flexibleTextView5;
        this.w = flexibleTextView6;
        this.x = flexibleTextView7;
        this.y = flexibleButton;
        this.z = flexibleEditText;
        this.A = flexibleEditText2;
        this.B = flexibleTextView8;
        this.C = flexibleTextView10;
        this.D = imageView;
        this.E = rTLImageView;
        this.F = numberPicker;
        this.G = numberPicker2;
        this.H = numberPicker3;
        this.I = constraintLayout2;
        this.J = flexibleSwitchCompat;
    }
}
