package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w94 extends v94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = new SparseIntArray();
    @DexIgnore
    public long r;

    /*
    static {
        t.put(2131362820, 1);
        t.put(2131363218, 2);
        t.put(2131362587, 3);
        t.put(2131362339, 4);
        t.put(2131362338, 5);
        t.put(2131362340, 6);
        t.put(2131361935, 7);
    }
    */

    @DexIgnore
    public w94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, s, t));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[7], objArr[5], objArr[4], objArr[6], objArr[3], objArr[1], objArr[0], objArr[2]);
        this.r = -1;
        this.q.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
