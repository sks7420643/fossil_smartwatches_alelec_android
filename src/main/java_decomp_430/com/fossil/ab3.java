package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ab3> CREATOR; // = new db3();
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public la3 c;
    @DexIgnore
    public long d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public j03 g;
    @DexIgnore
    public long h;
    @DexIgnore
    public j03 i;
    @DexIgnore
    public long j;
    @DexIgnore
    public j03 o;

    @DexIgnore
    public ab3(ab3 ab3) {
        w12.a(ab3);
        this.a = ab3.a;
        this.b = ab3.b;
        this.c = ab3.c;
        this.d = ab3.d;
        this.e = ab3.e;
        this.f = ab3.f;
        this.g = ab3.g;
        this.h = ab3.h;
        this.i = ab3.i;
        this.j = ab3.j;
        this.o = ab3.o;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a, false);
        g22.a(parcel, 3, this.b, false);
        g22.a(parcel, 4, (Parcelable) this.c, i2, false);
        g22.a(parcel, 5, this.d);
        g22.a(parcel, 6, this.e);
        g22.a(parcel, 7, this.f, false);
        g22.a(parcel, 8, (Parcelable) this.g, i2, false);
        g22.a(parcel, 9, this.h);
        g22.a(parcel, 10, (Parcelable) this.i, i2, false);
        g22.a(parcel, 11, this.j);
        g22.a(parcel, 12, (Parcelable) this.o, i2, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public ab3(String str, String str2, la3 la3, long j2, boolean z, String str3, j03 j03, long j3, j03 j032, long j4, j03 j033) {
        this.a = str;
        this.b = str2;
        this.c = la3;
        this.d = j2;
        this.e = z;
        this.f = str3;
        this.g = j03;
        this.h = j3;
        this.i = j032;
        this.j = j4;
        this.o = j033;
    }
}
