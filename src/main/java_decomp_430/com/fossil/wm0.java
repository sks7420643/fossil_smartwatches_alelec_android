package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wm0 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B;
    @DexIgnore
    public /* final */ qv0 C;

    @DexIgnore
    public wm0(ue1 ue1, q41 q41, eh1 eh1, qv0 qv0) {
        super(ue1, q41, eh1, (String) null, 8);
        this.C = qv0;
        ArrayList<hl1> arrayList = this.i;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        qv0 qv02 = this.C;
        if (!(qv02 instanceof pf1) && !(qv02 instanceof i81)) {
            if (qv02 instanceof ml0) {
                linkedHashSet.add(hl1.DEVICE_CONFIG);
            } else if (qv02 instanceof lj1) {
                linkedHashSet.add(hl1.FILE_CONFIG);
                linkedHashSet.add(hl1.TRANSFER_DATA);
            } else if (qv02 instanceof vd1) {
                linkedHashSet.add(hl1.TRANSFER_DATA);
            } else if (qv02 instanceof vf1) {
                linkedHashSet.add(hl1.FILE_CONFIG);
            } else if ((qv02 instanceof vv0) || (qv02 instanceof zo0)) {
                linkedHashSet.add(hl1.FILE_CONFIG);
                linkedHashSet.add(hl1.TRANSFER_DATA);
            } else if (qv02 instanceof rf1) {
                linkedHashSet.add(hl1.AUTHENTICATION);
            } else if ((qv02 instanceof g81) || (qv02 instanceof da1) || (qv02 instanceof z01)) {
                linkedHashSet.add(hl1.ASYNC);
            } else if (qv02 instanceof dz0) {
                linkedHashSet.add(((dz0) qv02).B.a());
            } else if (qv02 instanceof p41) {
                linkedHashSet.add(((p41) qv02).m().a());
                linkedHashSet.add(((p41) this.C).p().a());
            } else {
                linkedHashSet.add(hl1.DEVICE_INFORMATION);
            }
        }
        this.B = cw0.a(arrayList, (ArrayList<hl1>) new ArrayList(linkedHashSet));
    }

    @DexIgnore
    public void b(qv0 qv0) {
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public final void h() {
        if1.a((if1) this, this.C, (hg6) new qh0(this), (hg6) jj0.a, (ig6) null, (hg6) new dl0(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), this.C.h());
    }
}
