package com.fossil;

import com.fossil.tr6;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq6 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), fr6.a("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<pr6> d;
    @DexIgnore
    public /* final */ qr6 e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002b */
        public void run() {
            while (true) {
                long a2 = iq6.this.a(System.nanoTime());
                if (a2 != -1) {
                    if (a2 > 0) {
                        long j = a2 / 1000000;
                        long j2 = a2 - (1000000 * j);
                        synchronized (iq6.this) {
                            iq6.this.wait(j, (int) j2);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public iq6() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public pr6 a(aq6 aq6, tr6 tr6, ar6 ar6) {
        for (pr6 next : this.d) {
            if (next.a(aq6, ar6)) {
                tr6.a(next, true);
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public void b(pr6 pr6) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(pr6);
    }

    @DexIgnore
    public iq6(int i, long j, TimeUnit timeUnit) {
        this.c = new a();
        this.d = new ArrayDeque();
        this.e = new qr6();
        this.a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public Socket a(aq6 aq6, tr6 tr6) {
        for (pr6 next : this.d) {
            if (next.a(aq6, (ar6) null) && next.e() && next != tr6.c()) {
                return tr6.b(next);
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(pr6 pr6) {
        if (pr6.k || this.a == 0) {
            this.d.remove(pr6);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            long j2 = Long.MIN_VALUE;
            pr6 pr6 = null;
            int i = 0;
            int i2 = 0;
            for (pr6 next : this.d) {
                if (a(next, j) > 0) {
                    i2++;
                } else {
                    i++;
                    long j3 = j - next.o;
                    if (j3 > j2) {
                        pr6 = next;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.b) {
                if (i <= this.a) {
                    if (i > 0) {
                        long j4 = this.b - j2;
                        return j4;
                    } else if (i2 > 0) {
                        long j5 = this.b;
                        return j5;
                    } else {
                        this.f = false;
                        return -1;
                    }
                }
            }
            this.d.remove(pr6);
            fr6.a(pr6.g());
            return 0;
        }
    }

    @DexIgnore
    public final int a(pr6 pr6, long j) {
        List<Reference<tr6>> list = pr6.n;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                at6.d().a("A connection to " + pr6.f().a().k() + " was leaked. Did you forget to close a response body?", ((tr6.a) reference).a);
                list.remove(i);
                pr6.k = true;
                if (list.isEmpty()) {
                    pr6.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
