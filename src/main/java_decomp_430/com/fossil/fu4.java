package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu4 extends hg {
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ fu4 a;

        @DexIgnore
        public b(fu4 fu4) {
            this.a = fu4;
        }

        @DexIgnore
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            wg6.b(recyclerView, "recyclerView");
            fu4.super.onScrollStateChanged(recyclerView, i);
            if (i == 0) {
                try {
                    View c = this.a.c(recyclerView.getLayoutManager());
                    fu4 fu4 = this.a;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager == null) {
                        wg6.a();
                        throw null;
                    } else if (c != null) {
                        fu4.a(layoutManager.l(c));
                    } else {
                        wg6.a();
                        throw null;
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("BlurLinearSnapHelper", "attachToRecyclerView - e=" + e);
                }
            }
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            wg6.b(recyclerView, "recyclerView");
            fu4.super.onScrolled(recyclerView, i, i2);
            this.a.c(recyclerView.getLayoutManager());
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public final void a(int i) {
        this.f = i;
    }

    @DexIgnore
    public final void b(RecyclerView.m mVar, View view) {
        View view2;
        if (view != null && mVar != null) {
            view.setAlpha(1.0f);
            int i = -1;
            int i2 = 0;
            int e = mVar.e() - 1;
            if (e >= 0) {
                while (true) {
                    if (!wg6.a((Object) mVar.d(i2), (Object) view)) {
                        if (i2 == e) {
                            break;
                        }
                        i2++;
                    } else {
                        i = i2;
                        break;
                    }
                }
            }
            if (i >= 0) {
                View view3 = null;
                if (i > 0 && i < mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = mVar.d(i + 1);
                } else if (i == 0) {
                    view2 = mVar.d(i + 1);
                } else if (i == mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = null;
                } else {
                    view2 = null;
                }
                if (view3 != null) {
                    view3.setAlpha(0.8f);
                }
                if (view2 != null) {
                    view2.setAlpha(0.8f);
                }
            }
        }
    }

    @DexIgnore
    public View c(RecyclerView.m mVar) {
        View c = fu4.super.c(mVar);
        b(mVar, c);
        return c;
    }

    @DexIgnore
    public final int d() {
        return this.f;
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        fu4.super.a(recyclerView);
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new b(this));
        }
    }
}
