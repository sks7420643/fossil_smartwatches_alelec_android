package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y36 extends v36 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public y36(Context context, int i, r36 r36) {
        super(context, i, r36);
        this.m = b46.a(context).b();
        if (o == null) {
            o = m56.l(context);
        }
    }

    @DexIgnore
    public w36 a() {
        return w36.NETWORK_MONITOR;
    }

    @DexIgnore
    public void a(String str) {
        this.n = str;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        r56.a(jSONObject, "op", o);
        r56.a(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }
}
