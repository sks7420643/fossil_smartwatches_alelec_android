package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk1 {
    @DexIgnore
    public static /* final */ mi1 b; // = new mi1((qg6) null);
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic a;

    @DexIgnore
    public gk1(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        this.a = bluetoothGattCharacteristic;
        mi1 mi1 = b;
        UUID uuid = this.a.getUuid();
        wg6.a(uuid, "this.bluetoothGattCharacteristic.uuid");
        mi1.a(uuid);
    }
}
