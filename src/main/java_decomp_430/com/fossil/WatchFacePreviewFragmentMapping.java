package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WatchFacePreviewFragmentMapping extends WatchFacePreviewFragmentBinding {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131363218, 1);
        A.put(2131363097, 2);
        A.put(2131363071, 3);
        A.put(2131362025, 4);
        A.put(2131362546, 5);
        A.put(2131362001, 6);
        A.put(2131363339, 7);
        A.put(2131363331, 8);
        A.put(2131363338, 9);
        A.put(2131363335, 10);
    }
    */

    @DexIgnore
    public WatchFacePreviewFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 11, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFacePreviewFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[4], objArr[0], objArr[5], objArr[3], objArr[2], objArr[1], objArr[8], objArr[10], objArr[9], objArr[7]);
        this.y = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
