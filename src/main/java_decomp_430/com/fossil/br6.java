package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum br6 {
    TLS_1_3("TLSv1.3"),
    TLS_1_2("TLSv1.2"),
    TLS_1_1("TLSv1.1"),
    TLS_1_0("TLSv1"),
    SSL_3_0("SSLv3");
    
    @DexIgnore
    public /* final */ String javaName;

    @DexIgnore
    public br6(String str) {
        this.javaName = str;
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0076  */
    public static br6 forJavaName(String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode != 79201641) {
            if (hashCode != 79923350) {
                switch (hashCode) {
                    case -503070503:
                        if (str.equals("TLSv1.1")) {
                            c = 2;
                            break;
                        }
                    case -503070502:
                        if (str.equals("TLSv1.2")) {
                            c = 1;
                            break;
                        }
                    case -503070501:
                        if (str.equals("TLSv1.3")) {
                            c = 0;
                            break;
                        }
                }
            } else if (str.equals("TLSv1")) {
                c = 3;
                if (c == 0) {
                    return TLS_1_3;
                }
                if (c == 1) {
                    return TLS_1_2;
                }
                if (c == 2) {
                    return TLS_1_1;
                }
                if (c == 3) {
                    return TLS_1_0;
                }
                if (c == 4) {
                    return SSL_3_0;
                }
                throw new IllegalArgumentException("Unexpected TLS version: " + str);
            }
        } else if (str.equals("SSLv3")) {
            c = 4;
            if (c == 0) {
            }
        }
        c = 65535;
        if (c == 0) {
        }
    }

    @DexIgnore
    public static List<br6> forJavaNames(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String forJavaName : strArr) {
            arrayList.add(forJavaName(forJavaName));
        }
        return Collections.unmodifiableList(arrayList);
    }

    @DexIgnore
    public String javaName() {
        return this.javaName;
    }
}
