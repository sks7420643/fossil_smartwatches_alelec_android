package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j54 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ RecyclerView s;

    @DexIgnore
    public j54(Object obj, View view, int i, ImageView imageView, ConstraintLayout constraintLayout, RecyclerView recyclerView, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = imageView;
        this.r = constraintLayout;
        this.s = recyclerView;
    }
}
