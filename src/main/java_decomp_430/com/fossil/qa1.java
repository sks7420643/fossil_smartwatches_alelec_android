package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa1 {
    @DexIgnore
    public static /* final */ HashMap<Integer, lc6<td0, ud0>> a; // = he6.a(new lc6[]{qc6.a(3073, new lc6(td0.RING_PHONE, ud0.STANDARD)), qc6.a(6657, new lc6(td0.ALARM, ud0.STANDARD)), qc6.a(6658, new lc6(td0.ALARM, ud0.SEQUENCED)), qc6.a(7169, new lc6(td0.PROGRESS, ud0.STANDARD)), qc6.a(7170, new lc6(td0.PROGRESS, ud0.SWEEP)), qc6.a(7681, new lc6(td0.TWENTY_FOUR_HOUR, ud0.STANDARD)), qc6.a(7682, new lc6(td0.TWENTY_FOUR_HOUR, ud0.SEQUENCED)), qc6.a(1025, new lc6(td0.GOAL_TRACKING, ud0.STANDARD)), qc6.a(4097, new lc6(td0.SELFIE, ud0.STANDARD)), qc6.a(4609, new lc6(td0.MUSIC_CONTROL, ud0.PLAY_PAUSE)), qc6.a(4610, new lc6(td0.MUSIC_CONTROL, ud0.NEXT)), qc6.a(4611, new lc6(td0.MUSIC_CONTROL, ud0.PREVIOUS)), qc6.a(4612, new lc6(td0.MUSIC_CONTROL, ud0.VOLUME_UP)), qc6.a(4613, new lc6(td0.MUSIC_CONTROL, ud0.VOLUME_DOWN)), qc6.a(4614, new lc6(td0.MUSIC_CONTROL, ud0.STANDARD)), qc6.a(5121, new lc6(td0.DATE, ud0.STANDARD)), qc6.a(5122, new lc6(td0.DATE, ud0.SEQUENCED)), qc6.a(5633, new lc6(td0.TIME2, ud0.STANDARD)), qc6.a(5634, new lc6(td0.TIME2, ud0.SEQUENCED)), qc6.a(6145, new lc6(td0.ALERT, ud0.STANDARD)), qc6.a(6146, new lc6(td0.ALERT, ud0.SEQUENCED)), qc6.a(8193, new lc6(td0.STOPWATCH, ud0.STANDARD)), qc6.a(9217, new lc6(td0.COMMUTE_TIME, ud0.TRAVEL)), qc6.a(9218, new lc6(td0.COMMUTE_TIME, ud0.ETA))});
    @DexIgnore
    public static /* final */ qa1 b; // = new qa1();

    @DexIgnore
    public final td0 a(short s) {
        lc6 lc6 = (lc6) a.get(Integer.valueOf(s));
        if (lc6 != null) {
            return (td0) lc6.getFirst();
        }
        return td0.UNDEFINED;
    }

    @DexIgnore
    public final ud0 b(short s) {
        lc6 lc6 = (lc6) a.get(Integer.valueOf(s));
        if (lc6 != null) {
            return (ud0) lc6.getSecond();
        }
        return ud0.UNDEFINED;
    }
}
