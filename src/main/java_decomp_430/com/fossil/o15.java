package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o15 {
    @DexIgnore
    public /* final */ n15 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public o15(n15 n15, int i, LoaderManager loaderManager) {
        wg6.b(n15, "mView");
        wg6.b(loaderManager, "mLoaderManager");
        this.a = n15;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final n15 c() {
        return this.a;
    }
}
