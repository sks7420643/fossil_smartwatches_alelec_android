package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hs6 {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8),
    COMPRESSION_ERROR(9),
    CONNECT_ERROR(10),
    ENHANCE_YOUR_CALM(11),
    INADEQUATE_SECURITY(12),
    HTTP_1_1_REQUIRED(13);
    
    @DexIgnore
    public /* final */ int httpCode;

    @DexIgnore
    public hs6(int i) {
        this.httpCode = i;
    }

    @DexIgnore
    public static hs6 fromHttp2(int i) {
        for (hs6 hs6 : values()) {
            if (hs6.httpCode == i) {
                return hs6;
            }
        }
        return null;
    }
}
