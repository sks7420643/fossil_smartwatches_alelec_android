package com.fossil;

import com.zendesk.service.ErrorResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v66 implements ErrorResponse {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public v66(String str) {
        this.a = str;
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return false;
    }

    @DexIgnore
    public int o() {
        return -1;
    }
}
