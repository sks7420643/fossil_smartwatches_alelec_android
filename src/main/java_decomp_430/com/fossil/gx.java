package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gx implements rt<byte[]> {
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public gx(byte[] bArr) {
        q00.a(bArr);
        this.a = bArr;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public int b() {
        return this.a.length;
    }

    @DexIgnore
    public Class<byte[]> c() {
        return byte[].class;
    }

    @DexIgnore
    public byte[] get() {
        return this.a;
    }
}
