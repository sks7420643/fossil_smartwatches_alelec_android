package com.fossil;

import android.database.Cursor;
import com.fossil.rs1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fs1 implements rs1.b {
    @DexIgnore
    public /* final */ Map a;

    @DexIgnore
    public fs1(Map map) {
        this.a = map;
    }

    @DexIgnore
    public static rs1.b a(Map map) {
        return new fs1(map);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.a(this.a, (Cursor) obj);
    }
}
