package com.fossil;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uz extends vz<Drawable> {
    @DexIgnore
    public uz(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: e */
    public void c(Drawable drawable) {
        ((ImageView) this.a).setImageDrawable(drawable);
    }
}
