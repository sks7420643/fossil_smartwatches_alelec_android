package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ha1 {
    LEGACY_OTA_ENTER(new byte[]{8}),
    LEGACY_OTA_ENTER_RESPONSE(new byte[]{9}),
    LEGACY_OTA_RESET(new byte[]{10});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public ha1(byte[] bArr) {
        this.a = bArr;
    }
}
