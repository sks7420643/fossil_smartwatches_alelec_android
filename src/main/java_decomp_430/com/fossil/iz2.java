package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz2 implements Parcelable.Creator<StreetViewPanoramaCamera> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                f = f22.n(parcel, a);
            } else if (a2 == 3) {
                f2 = f22.n(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                f3 = f22.n(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new StreetViewPanoramaCamera(f, f2, f3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new StreetViewPanoramaCamera[i];
    }
}
