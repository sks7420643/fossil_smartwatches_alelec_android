package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q66 {
    @DexIgnore
    public static /* final */ int action0; // = 2131361856;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361884;
    @DexIgnore
    public static /* final */ int always; // = 2131361887;
    @DexIgnore
    public static /* final */ int beginning; // = 2131361908;
    @DexIgnore
    public static /* final */ int belvedere_dialog_listview; // = 2131361911;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_image; // = 2131361912;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_text; // = 2131361913;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361963;
    @DexIgnore
    public static /* final */ int cancel_action; // = 2131361967;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131361995;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361999;
    @DexIgnore
    public static /* final */ int collapseActionView; // = 2131362098;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362120;
    @DexIgnore
    public static /* final */ int custom; // = 2131362131;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362132;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362158;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362159;
    @DexIgnore
    public static /* final */ int disableHome; // = 2131362168;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362176;
    @DexIgnore
    public static /* final */ int end; // = 2131362177;
    @DexIgnore
    public static /* final */ int end_padder; // = 2131362178;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362200;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362201;
    @DexIgnore
    public static /* final */ int home; // = 2131362475;
    @DexIgnore
    public static /* final */ int homeAsUp; // = 2131362476;
    @DexIgnore
    public static /* final */ int icon; // = 2131362499;
    @DexIgnore
    public static /* final */ int ifRoom; // = 2131362506;
    @DexIgnore
    public static /* final */ int image; // = 2131362507;
    @DexIgnore
    public static /* final */ int info; // = 2131362512;
    @DexIgnore
    public static /* final */ int line1; // = 2131362654;
    @DexIgnore
    public static /* final */ int line3; // = 2131362655;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362662;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362663;
    @DexIgnore
    public static /* final */ int media_actions; // = 2131362701;
    @DexIgnore
    public static /* final */ int middle; // = 2131362704;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362733;
    @DexIgnore
    public static /* final */ int never; // = 2131362735;
    @DexIgnore
    public static /* final */ int none; // = 2131362739;
    @DexIgnore
    public static /* final */ int normal; // = 2131362740;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362774;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362823;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362824;
    @DexIgnore
    public static /* final */ int radio; // = 2131362827;
    @DexIgnore
    public static /* final */ int screen; // = 2131362909;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131362911;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131362912;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131362913;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131362917;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131362918;
    @DexIgnore
    public static /* final */ int search_button; // = 2131362919;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131362920;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131362921;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131362922;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131362923;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131362924;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131362925;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131362927;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131362931;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131362944;
    @DexIgnore
    public static /* final */ int showCustom; // = 2131362945;
    @DexIgnore
    public static /* final */ int showHome; // = 2131362946;
    @DexIgnore
    public static /* final */ int showTitle; // = 2131362947;
    @DexIgnore
    public static /* final */ int spacer; // = 2131362961;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131362963;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131362967;
    @DexIgnore
    public static /* final */ int src_in; // = 2131362968;
    @DexIgnore
    public static /* final */ int src_over; // = 2131362969;
    @DexIgnore
    public static /* final */ int status_bar_latest_event_content; // = 2131362973;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131362976;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131362994;
    @DexIgnore
    public static /* final */ int text; // = 2131363009;
    @DexIgnore
    public static /* final */ int text2; // = 2131363010;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363012;
    @DexIgnore
    public static /* final */ int time; // = 2131363031;
    @DexIgnore
    public static /* final */ int title; // = 2131363033;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363036;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363042;
    @DexIgnore
    public static /* final */ int up; // = 2131363248;
    @DexIgnore
    public static /* final */ int useLogo; // = 2131363255;
    @DexIgnore
    public static /* final */ int withText; // = 2131363346;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363348;
}
