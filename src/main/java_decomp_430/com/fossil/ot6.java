package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ot6 implements zt6 {
    @DexIgnore
    public /* final */ zt6 a;

    @DexIgnore
    public ot6(zt6 zt6) {
        if (zt6 != null) {
            this.a = zt6;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public long b(jt6 jt6, long j) throws IOException {
        return this.a.b(jt6, j);
    }

    @DexIgnore
    public final zt6 c() {
        return this.a;
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.a.toString() + ")";
    }

    @DexIgnore
    public au6 b() {
        return this.a.b();
    }
}
