package com.fossil;

import android.content.SharedPreferences;
import android.util.Pair;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l53 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ h53 e;

    @DexIgnore
    public l53(h53 h53, String str, long j) {
        this.e = h53;
        w12.b(str);
        w12.a(j > 0);
        this.a = String.valueOf(str).concat(":start");
        this.b = String.valueOf(str).concat(":count");
        this.c = String.valueOf(str).concat(":value");
        this.d = j;
    }

    @DexIgnore
    public final void a(String str, long j) {
        this.e.g();
        if (c() == 0) {
            b();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.e.A().getLong(this.b, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.e.A().edit();
            edit.putString(this.c, str);
            edit.putLong(this.b, 1);
            edit.apply();
            return;
        }
        long j3 = j2 + 1;
        boolean z = (this.e.j().t().nextLong() & RecyclerView.FOREVER_NS) < RecyclerView.FOREVER_NS / j3;
        SharedPreferences.Editor edit2 = this.e.A().edit();
        if (z) {
            edit2.putString(this.c, str);
        }
        edit2.putLong(this.b, j3);
        edit2.apply();
    }

    @DexIgnore
    public final void b() {
        this.e.g();
        long b2 = this.e.zzm().b();
        SharedPreferences.Editor edit = this.e.A().edit();
        edit.remove(this.b);
        edit.remove(this.c);
        edit.putLong(this.a, b2);
        edit.apply();
    }

    @DexIgnore
    public final long c() {
        return this.e.A().getLong(this.a, 0);
    }

    @DexIgnore
    public final Pair<String, Long> a() {
        long j;
        this.e.g();
        this.e.g();
        long c2 = c();
        if (c2 == 0) {
            b();
            j = 0;
        } else {
            j = Math.abs(c2 - this.e.zzm().b());
        }
        long j2 = this.d;
        if (j < j2) {
            return null;
        }
        if (j > (j2 << 1)) {
            b();
            return null;
        }
        String string = this.e.A().getString(this.c, (String) null);
        long j3 = this.e.A().getLong(this.b, 0);
        b();
        if (string == null || j3 <= 0) {
            return h53.C;
        }
        return new Pair<>(string, Long.valueOf(j3));
    }
}
