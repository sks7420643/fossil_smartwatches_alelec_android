package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws1 implements Factory<vs1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<Integer> b;

    @DexIgnore
    public ws1(Provider<Context> provider, Provider<Integer> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ws1 a(Provider<Context> provider, Provider<Integer> provider2) {
        return new ws1(provider, provider2);
    }

    @DexIgnore
    public vs1 get() {
        return new vs1((Context) this.a.get(), ((Integer) this.b.get()).intValue());
    }
}
