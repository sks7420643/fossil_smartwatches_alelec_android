package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn implements xn {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hh<wn> {
        @DexIgnore
        public a(yn ynVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(mi miVar, wn wnVar) {
            String str = wnVar.a;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            String str2 = wnVar.b;
            if (str2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkName`(`name`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public yn(oh ohVar) {
        this.a = ohVar;
        this.b = new a(this, ohVar);
    }

    @DexIgnore
    public void a(wn wnVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(wnVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }
}
