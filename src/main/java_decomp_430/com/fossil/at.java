package com.fossil;

import com.fossil.ku;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class at<DataType> implements ku.b {
    @DexIgnore
    public /* final */ sr<DataType> a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ xr c;

    @DexIgnore
    public at(sr<DataType> srVar, DataType datatype, xr xrVar) {
        this.a = srVar;
        this.b = datatype;
        this.c = xrVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
