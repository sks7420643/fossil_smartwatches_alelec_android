package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum n61 {
    PAIR_ANIMATION(new byte[]{5}),
    OPTIMAL_PAYLOAD(new byte[]{40}),
    HEARTBEAT_STATISTIC(new byte[]{41}),
    BLE_TROUBLESHOOT(new byte[]{83});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public n61(byte[] bArr) {
        this.a = bArr;
    }
}
