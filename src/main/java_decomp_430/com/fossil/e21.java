package com.fossil;

import com.fossil.r40;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e21 extends if1 {
    @DexIgnore
    public r40 B;
    @DexIgnore
    public /* final */ ArrayList<hl1> C; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG, hl1.TRANSFER_DATA}));

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public e21(ue1 ue1, q41 q41, String str) {
        super(r1, q41, eh1.FETCH_DEVICE_INFORMATION, str);
        ue1 ue12 = ue1;
        this.B = new r40(ue1.d(), ue12.t, "", "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
    }

    @DexIgnore
    public Object d() {
        return this.B;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.C;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new fj1(this.w), (hg6) new qy0(this), (hg6) new k01(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.DEVICE_INFO, (Object) this.B.a());
    }

    @DexIgnore
    public static final /* synthetic */ void a(e21 e21) {
        ue1 ue1 = e21.w;
        e21 e212 = e21;
        if1.a((if1) e212, (if1) new gq0(ue1, e21.x, lk1.b.a(ue1.t, w31.DEVICE_INFO), e21.z), (hg6) new rr0(e21), (hg6) new kt0(e21), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }
}
