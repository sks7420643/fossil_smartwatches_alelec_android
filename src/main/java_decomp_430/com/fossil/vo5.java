package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo5 {
    @DexIgnore
    public /* final */ bp5 a;

    @DexIgnore
    public vo5(bp5 bp5) {
        wg6.b(bp5, "view");
        jk3.a(bp5, "view cannot be null!", new Object[0]);
        wg6.a((Object) bp5, "checkNotNull(view, \"view cannot be null!\")");
        this.a = bp5;
    }

    @DexIgnore
    public final bp5 a() {
        return this.a;
    }
}
