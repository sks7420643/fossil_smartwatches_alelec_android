package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.fossil.x1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w1 implements s1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ q1 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public View f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public x1.a i;
    @DexIgnore
    public v1 j;
    @DexIgnore
    public PopupWindow.OnDismissListener k;
    @DexIgnore
    public /* final */ PopupWindow.OnDismissListener l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements PopupWindow.OnDismissListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onDismiss() {
            w1.this.e();
        }
    }

    @DexIgnore
    public w1(Context context, q1 q1Var, View view, boolean z, int i2) {
        this(context, q1Var, view, z, i2, 0);
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    @DexIgnore
    public void b() {
        if (d()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    public v1 c() {
        if (this.j == null) {
            this.j = a();
        }
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        v1 v1Var = this.j;
        return v1Var != null && v1Var.c();
    }

    @DexIgnore
    public void e() {
        this.j = null;
        PopupWindow.OnDismissListener onDismissListener = this.k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public void f() {
        if (!g()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    public boolean g() {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public w1(Context context, q1 q1Var, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new a();
        this.a = context;
        this.b = q1Var;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public void a(View view) {
        this.f = view;
    }

    @DexIgnore
    public void a(boolean z) {
        this.h = z;
        v1 v1Var = this.j;
        if (v1Var != null) {
            v1Var.b(z);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.fossil.x1, com.fossil.v1] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.fossil.b2] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.fossil.n1] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final v1 a() {
        Object r0;
        Display defaultDisplay = ((WindowManager) this.a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        if (Math.min(point.x, point.y) >= this.a.getResources().getDimensionPixelSize(d0.abc_cascading_menus_min_smallest_width)) {
            r0 = new n1(this.a, this.f, this.d, this.e, this.c);
        } else {
            r0 = new b2(this.a, this.b, this.f, this.d, this.e, this.c);
        }
        r0.a(this.b);
        r0.a(this.l);
        r0.a(this.f);
        r0.a(this.i);
        r0.b(this.h);
        r0.a(this.g);
        return r0;
    }

    @DexIgnore
    public final void a(int i2, int i3, boolean z, boolean z2) {
        v1 c2 = c();
        c2.c(z2);
        if (z) {
            if ((e9.a(this.g, x9.o(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            c2.b(i2);
            c2.c(i3);
            int i4 = (int) ((this.a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i3 + i4));
        }
        c2.d();
    }

    @DexIgnore
    public void a(x1.a aVar) {
        this.i = aVar;
        v1 v1Var = this.j;
        if (v1Var != null) {
            v1Var.a(aVar);
        }
    }
}
