package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du3 {
    @DexIgnore
    public Excluder a; // = Excluder.g;
    @DexIgnore
    public ru3 b; // = ru3.DEFAULT;
    @DexIgnore
    public cu3 c; // = bu3.IDENTITY;
    @DexIgnore
    public /* final */ Map<Type, eu3<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ List<su3> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<su3> f; // = new ArrayList();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i; // = 2;
    @DexIgnore
    public int j; // = 2;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public du3 a(int... iArr) {
        this.a = this.a.a(iArr);
        return this;
    }

    @DexIgnore
    public du3 b(zt3 zt3) {
        this.a = this.a.a(zt3, true, false);
        return this;
    }

    @DexIgnore
    public du3 a(bu3 bu3) {
        this.c = bu3;
        return this;
    }

    @DexIgnore
    public du3 a(zt3 zt3) {
        this.a = this.a.a(zt3, false, true);
        return this;
    }

    @DexIgnore
    public du3 a(Type type, Object obj) {
        boolean z = obj instanceof pu3;
        yu3.a(z || (obj instanceof hu3) || (obj instanceof eu3) || (obj instanceof TypeAdapter));
        if (obj instanceof eu3) {
            this.d.put(type, (eu3) obj);
        }
        if (z || (obj instanceof hu3)) {
            this.e.add(TreeTypeAdapter.a(TypeToken.get(type), obj));
        }
        if (obj instanceof TypeAdapter) {
            this.e.add(TypeAdapters.a(TypeToken.get(type), (TypeAdapter) obj));
        }
        return this;
    }

    @DexIgnore
    public Gson a() {
        ArrayList arrayList = r1;
        ArrayList arrayList2 = new ArrayList(this.e.size() + this.f.size() + 3);
        arrayList2.addAll(this.e);
        Collections.reverse(arrayList2);
        ArrayList arrayList3 = new ArrayList(this.f);
        Collections.reverse(arrayList3);
        arrayList2.addAll(arrayList3);
        a(this.h, this.i, this.j, arrayList2);
        return new Gson(this.a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.p, this.l, this.b, this.h, this.i, this.j, this.e, this.f, arrayList);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, List<su3> list) {
        yt3 yt3;
        yt3 yt32;
        yt3 yt33;
        if (str != null && !"".equals(str.trim())) {
            yt3 yt34 = new yt3(Date.class, str);
            yt3 = new yt3(Timestamp.class, str);
            yt33 = new yt3(java.sql.Date.class, str);
            yt32 = yt34;
        } else if (i2 != 2 && i3 != 2) {
            yt32 = new yt3(Date.class, i2, i3);
            yt3 yt35 = new yt3(Timestamp.class, i2, i3);
            yt3 yt36 = new yt3(java.sql.Date.class, i2, i3);
            yt3 = yt35;
            yt33 = yt36;
        } else {
            return;
        }
        list.add(TypeAdapters.a(Date.class, yt32));
        list.add(TypeAdapters.a(Timestamp.class, yt3));
        list.add(TypeAdapters.a(java.sql.Date.class, yt33));
    }
}
