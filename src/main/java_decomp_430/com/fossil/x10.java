package com.fossil;

import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x10 extends r86 implements na6 {
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public x10(i86 i86, String str, String str2, va6 va6, String str3) {
        super(i86, str, str2, va6, ta6.POST);
        this.g = str3;
    }

    @DexIgnore
    public boolean a(List<File> list) {
        ua6 a = a();
        a.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.j());
        a.c("X-CRASHLYTICS-API-KEY", this.g);
        int i = 0;
        for (File next : list) {
            a.a("session_analytics_file_" + i, next.getName(), "application/vnd.crashlytics.android.events", next);
            i++;
        }
        l86 g2 = c86.g();
        g2.d("Answers", "Sending " + list.size() + " analytics files to " + b());
        int g3 = a.g();
        l86 g4 = c86.g();
        g4.d("Answers", "Response code for analytics file send is " + g3);
        if (m96.a(g3) == 0) {
            return true;
        }
        return false;
    }
}
