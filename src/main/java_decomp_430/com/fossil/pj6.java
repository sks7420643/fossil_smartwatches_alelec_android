package com.fossil;

import com.j256.ormlite.logger.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pj6 implements gj6 {
    IGNORE_CASE(2, 0, 2, (int) null),
    MULTILINE(8, 0, 2, (int) null),
    LITERAL(16, 0, 2, (int) null),
    UNIX_LINES(1, 0, 2, (int) null),
    COMMENTS(4, 0, 2, (int) null),
    DOT_MATCHES_ALL(32, 0, 2, (int) null),
    CANON_EQ(Logger.DEFAULT_FULL_MESSAGE_LENGTH, 0, 2, (int) null);
    
    @DexIgnore
    public /* final */ int mask;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public pj6(int i, int i2) {
        this.value = i;
        this.mask = i2;
    }

    @DexIgnore
    public int getMask() {
        return this.mask;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
