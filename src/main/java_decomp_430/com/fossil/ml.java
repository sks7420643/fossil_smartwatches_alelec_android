package com.fossil;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ dm c;
    @DexIgnore
    public /* final */ sl d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Executor a;
        @DexIgnore
        public dm b;
        @DexIgnore
        public sl c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public int e; // = 4;
        @DexIgnore
        public int f; // = 0;
        @DexIgnore
        public int g; // = Integer.MAX_VALUE;
        @DexIgnore
        public int h; // = 20;

        @DexIgnore
        public a a(dm dmVar) {
            this.b = dmVar;
            return this;
        }

        @DexIgnore
        public ml a() {
            return new ml(this);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        ml a();
    }

    @DexIgnore
    public ml(a aVar) {
        Executor executor = aVar.a;
        if (executor == null) {
            this.a = a();
        } else {
            this.a = executor;
        }
        Executor executor2 = aVar.d;
        if (executor2 == null) {
            this.b = a();
        } else {
            this.b = executor2;
        }
        dm dmVar = aVar.b;
        if (dmVar == null) {
            this.c = dm.a();
        } else {
            this.c = dmVar;
        }
        sl slVar = aVar.c;
        if (slVar == null) {
            this.d = sl.a();
        } else {
            this.d = slVar;
        }
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
    }

    @DexIgnore
    public final Executor a() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    @DexIgnore
    public Executor b() {
        return this.a;
    }

    @DexIgnore
    public sl c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        if (Build.VERSION.SDK_INT == 23) {
            return this.h / 2;
        }
        return this.h;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public int g() {
        return this.e;
    }

    @DexIgnore
    public Executor h() {
        return this.b;
    }

    @DexIgnore
    public dm i() {
        return this.c;
    }
}
