package com.fossil;

import android.util.Log;
import com.fossil.rv1;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex1 implements kc3<Map<lw1<?>, String>> {
    @DexIgnore
    public yw1 a;
    @DexIgnore
    public /* final */ /* synthetic */ g02 b;

    @DexIgnore
    public ex1(g02 g02, yw1 yw1) {
        this.b = g02;
        this.a = yw1;
    }

    @DexIgnore
    public final void a() {
        this.a.onComplete();
    }

    @DexIgnore
    public final void onComplete(qc3<Map<lw1<?>, String>> qc3) {
        this.b.f.lock();
        try {
            if (!this.b.r) {
                this.a.onComplete();
                return;
            }
            if (qc3.e()) {
                Map unused = this.b.t = new p4(this.b.b.size());
                for (h02 a2 : this.b.b.values()) {
                    this.b.t.put(a2.a(), gv1.e);
                }
            } else if (qc3.a() instanceof tv1) {
                tv1 tv1 = (tv1) qc3.a();
                if (this.b.p) {
                    Map unused2 = this.b.t = new p4(this.b.b.size());
                    for (h02 h02 : this.b.b.values()) {
                        lw1 a3 = h02.a();
                        gv1 connectionResult = tv1.getConnectionResult((vv1<? extends rv1.d>) h02);
                        if (this.b.a((h02<?>) h02, connectionResult)) {
                            this.b.t.put(a3, new gv1(16));
                        } else {
                            this.b.t.put(a3, connectionResult);
                        }
                    }
                } else {
                    Map unused3 = this.b.t = tv1.zaj();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", qc3.a());
                Map unused4 = this.b.t = Collections.emptyMap();
            }
            if (this.b.c()) {
                this.b.s.putAll(this.b.t);
                if (this.b.j() == null) {
                    this.b.h();
                    this.b.i();
                    this.b.i.signalAll();
                }
            }
            this.a.onComplete();
            this.b.f.unlock();
        } finally {
            this.b.f.unlock();
        }
    }
}
