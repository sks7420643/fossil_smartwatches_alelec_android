package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr2 implements ir2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a; // = new vk2(qk2.a("com.google.android.gms.measurement")).a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true);

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
