package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xq1 {
    @DexIgnore
    public static rr1 a(Context context, ur1 ur1, fr1 fr1, zs1 zs1) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new dr1(context, ur1, fr1);
        }
        return new zq1(context, ur1, zs1, fr1);
    }
}
