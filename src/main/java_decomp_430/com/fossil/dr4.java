package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public int a;
    @DexIgnore
    public List<? extends er4> b;
    @DexIgnore
    public e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ dr4 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dr4$a$a")
        /* renamed from: com.fossil.dr4$a$a  reason: collision with other inner class name */
        public static final class C0009a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0009a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<er4> d = this.a.b.d();
                    if (d != null) {
                        er4 er4 = d.get(adapterPosition);
                        e c = this.a.b.c();
                        if (c != null) {
                            c.a(er4.a(), this.a.b.e(), adapterPosition, er4.b(), (Bundle) null);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<er4> d = this.a.b.d();
                if (d != null) {
                    er4 er4 = d.get(adapterPosition);
                    e c = this.a.b.c();
                    if (c == null) {
                        return true;
                    }
                    c.b(er4.a(), this.a.b.e(), adapterPosition, er4.b(), (Bundle) null);
                    return true;
                }
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dr4 dr4, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.b = dr4;
            view.setOnClickListener(new C0009a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363115);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public List<String> a; // = new ArrayList();
        @DexIgnore
        public TextView b;
        @DexIgnore
        public Spinner c;
        @DexIgnore
        public Button d;
        @DexIgnore
        public /* final */ /* synthetic */ dr4 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<er4> d = this.a.e.d();
                    if (d != null) {
                        er4 er4 = d.get(adapterPosition);
                        e c = this.a.e.c();
                        if (c != null) {
                            String a2 = er4.a();
                            int e = this.a.e.e();
                            Object b = er4.b();
                            Bundle bundle = new Bundle();
                            bundle.putInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS", this.a.c.getSelectedItemPosition());
                            c.a(a2, e, adapterPosition, b, bundle);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dr4 dr4, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.e = dr4;
            View findViewById = view.findViewById(2131363219);
            if (findViewById != null) {
                this.b = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131362957);
                if (findViewById2 != null) {
                    this.c = (Spinner) findViewById2;
                    View findViewById3 = view.findViewById(2131361957);
                    if (findViewById3 != null) {
                        Button button = (Button) findViewById3;
                        button.setOnClickListener(new a(this));
                        this.d = button;
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.Spinner");
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final TextView b() {
            return this.b;
        }

        @DexIgnore
        public final void c() {
            View view = this.itemView;
            wg6.a((Object) view, "itemView");
            ArrayAdapter arrayAdapter = new ArrayAdapter(view.getContext(), 17367048, this.a);
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter(arrayAdapter);
        }

        @DexIgnore
        public final void a(List<String> list) {
            wg6.b(list, ServerSetting.VALUE);
            this.a = list;
            c();
        }

        @DexIgnore
        public final Button a() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public SwitchCompat b;
        @DexIgnore
        public /* final */ /* synthetic */ dr4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ SwitchCompat a;
            @DexIgnore
            public /* final */ /* synthetic */ c b;

            @DexIgnore
            public a(SwitchCompat switchCompat, c cVar) {
                this.a = switchCompat;
                this.b = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<er4> d = this.b.c.d();
                    if (d != null) {
                        er4 er4 = d.get(adapterPosition);
                        if (er4 != null) {
                            ((gr4) er4).a(this.a.isChecked());
                            e c = this.b.c.c();
                            if (c != null) {
                                String a2 = er4.a();
                                int e = this.b.c.e();
                                Object b2 = er4.b();
                                Bundle bundle = new Bundle();
                                List<er4> d2 = this.b.c.d();
                                if (d2 != null) {
                                    er4 er42 = d2.get(adapterPosition);
                                    if (er42 != null) {
                                        bundle.putBoolean("DEBUG_BUNDLE_IS_CHECKED", ((gr4) er42).d());
                                        c.a(a2, e, adapterPosition, b2, bundle);
                                    } else {
                                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            this.b.c.notifyItemChanged(adapterPosition);
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dr4 dr4, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.c = dr4;
            View findViewById = view.findViewById(2131363116);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                SwitchCompat findViewById2 = view.findViewById(2131362904);
                if (findViewById2 != null) {
                    SwitchCompat switchCompat = findViewById2;
                    switchCompat.setOnClickListener(new a(switchCompat, this));
                    this.b = switchCompat;
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final SwitchCompat a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ dr4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<er4> d = this.a.c.d();
                    if (d != null) {
                        er4 er4 = d.get(adapterPosition);
                        e c = this.a.c.c();
                        if (c != null) {
                            c.a(er4.a(), this.a.c.e(), adapterPosition, er4.b(), (Bundle) null);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public b(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<er4> d = this.a.c.d();
                if (d != null) {
                    er4 er4 = d.get(adapterPosition);
                    e c = this.a.c.c();
                    if (c == null) {
                        return true;
                    }
                    c.b(er4.a(), this.a.c.e(), adapterPosition, er4.b(), (Bundle) null);
                    return true;
                }
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(dr4 dr4, View view) {
            super(view);
            wg6.b(view, "itemView");
            this.c = dr4;
            view.setOnClickListener(new a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363117);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363118);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public final e c() {
        return this.c;
    }

    @DexIgnore
    public final List<er4> d() {
        return this.b;
    }

    @DexIgnore
    public final int e() {
        return this.a;
    }

    @DexIgnore
    public int getItemCount() {
        List<? extends er4> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.er4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public int getItemViewType(int i) {
        List<? extends er4> list = this.b;
        er4 er4 = null;
        if ((list != null ? (er4) list.get(i) : null) instanceof gr4) {
            return jr4.CHILD_ITEM_WITH_SWITCH.ordinal();
        }
        List<? extends er4> list2 = this.b;
        if ((list2 != null ? (er4) list2.get(i) : null) instanceof hr4) {
            return jr4.CHILD_ITEM_WITH_TEXT.ordinal();
        }
        List list3 = this.b;
        if (list3 != null) {
            er4 = list3.get(i);
        }
        if (er4 instanceof fr4) {
            return jr4.CHILD_ITEM_WITH_SPINNER.ordinal();
        }
        return jr4.CHILD.ordinal();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        if (viewHolder instanceof a) {
            TextView a2 = ((a) viewHolder).a();
            List<? extends er4> list = this.b;
            if (list != null) {
                a2.setText(((er4) list.get(i)).c());
            } else {
                wg6.a();
                throw null;
            }
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            TextView b2 = cVar.b();
            List<? extends er4> list2 = this.b;
            if (list2 != null) {
                b2.setText(((er4) list2.get(i)).c());
                SwitchCompat a3 = cVar.a();
                List<? extends er4> list3 = this.b;
                if (list3 != null) {
                    Object obj = list3.get(i);
                    if (obj != null) {
                        a3.setChecked(((gr4) obj).d());
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } else if (viewHolder instanceof d) {
            d dVar = (d) viewHolder;
            TextView b3 = dVar.b();
            List<? extends er4> list4 = this.b;
            if (list4 != null) {
                b3.setText(((er4) list4.get(i)).c());
                TextView a4 = dVar.a();
                List<? extends er4> list5 = this.b;
                if (list5 != null) {
                    Object obj2 = list5.get(i);
                    if (obj2 != null) {
                        a4.setText(((hr4) obj2).d());
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } else if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            TextView b4 = bVar.b();
            List<? extends er4> list6 = this.b;
            if (list6 != null) {
                b4.setText(((er4) list6.get(i)).c());
                Button a5 = bVar.a();
                List<? extends er4> list7 = this.b;
                if (list7 != null) {
                    Object obj3 = list7.get(i);
                    if (obj3 != null) {
                        a5.setText(((fr4) obj3).d());
                        List<? extends er4> list8 = this.b;
                        if (list8 != null) {
                            Object obj4 = list8.get(i);
                            if (obj4 != null) {
                                bVar.a(((fr4) obj4).e());
                                return;
                            }
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                        }
                        wg6.a();
                        throw null;
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == jr4.CHILD.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558653, viewGroup, false);
            wg6.a((Object) inflate, "view");
            return new a(this, inflate);
        } else if (i == jr4.CHILD_ITEM_WITH_SWITCH.ordinal()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558655, viewGroup, false);
            wg6.a((Object) inflate2, "view");
            return new c(this, inflate2);
        } else if (i == jr4.CHILD_ITEM_WITH_TEXT.ordinal()) {
            View inflate3 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558656, viewGroup, false);
            wg6.a((Object) inflate3, "view");
            return new d(this, inflate3);
        } else if (i == jr4.CHILD_ITEM_WITH_SPINNER.ordinal()) {
            View inflate4 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558654, viewGroup, false);
            wg6.a((Object) inflate4, "view");
            return new b(this, inflate4);
        } else {
            throw new IllegalArgumentException("viewType is not appropriate");
        }
    }

    @DexIgnore
    public final void a(List<? extends er4> list) {
        this.b = list;
    }

    @DexIgnore
    public final void a(e eVar) {
        wg6.b(eVar, "itemClickListener");
        this.c = eVar;
    }
}
