package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y25 implements Factory<x25> {
    @DexIgnore
    public static x25 a(s25 s25, int i, ArrayList<wx4> arrayList, LoaderManager loaderManager, z24 z24, a35 a35) {
        return new NotificationHybridContactPresenter(s25, i, arrayList, loaderManager, z24, a35);
    }
}
