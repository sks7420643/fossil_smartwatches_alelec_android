package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface oy2 extends IInterface {
    @DexIgnore
    tx2 a(x52 x52, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    wx2 a(x52 x52, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    void a(x52 x52, int i) throws RemoteException;

    @DexIgnore
    vx2 b(x52 x52) throws RemoteException;

    @DexIgnore
    sx2 d(x52 x52) throws RemoteException;

    @DexIgnore
    qx2 zze() throws RemoteException;

    @DexIgnore
    mh2 zzf() throws RemoteException;
}
