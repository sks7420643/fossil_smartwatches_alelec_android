package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s52 implements Parcelable.Creator<r52> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        IBinder iBinder = null;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                str = f22.e(parcel, a);
            } else if (a2 == 2) {
                iBinder = f22.p(parcel, a);
            } else if (a2 == 3) {
                z = f22.i(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                z2 = f22.i(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new r52(str, iBinder, z, z2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new r52[i];
    }
}
