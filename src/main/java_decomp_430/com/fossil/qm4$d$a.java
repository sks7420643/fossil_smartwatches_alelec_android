package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.manager.FileDownloadManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1$repoResponse$1", f = "FileDownloadManager.kt", l = {90}, m = "invokeSuspend")
public final class qm4$d$a extends sf6 implements hg6<xe6<? super rx6<zq6>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ FileDownloadManager.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qm4$d$a(FileDownloadManager.d dVar, xe6 xe6) {
        super(1, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new qm4$d$a(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((qm4$d$a) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 b = this.this$0.this$0.b();
            String remoteUrl = this.this$0.$localFile.getRemoteUrl();
            this.label = 1;
            obj = b.downloadFile(remoteUrl, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
