package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class id4 extends hd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public long G;

    /*
    static {
        I.put(2131362133, 1);
        I.put(2131362542, 2);
        I.put(2131361904, 3);
        I.put(2131362637, 4);
        I.put(2131362517, 5);
        I.put(2131362187, 6);
        I.put(2131362518, 7);
        I.put(2131362189, 8);
        I.put(2131363147, 9);
        I.put(2131362515, 10);
        I.put(2131363146, 11);
        I.put(2131362351, 12);
        I.put(2131362219, 13);
        I.put(2131362214, 14);
        I.put(2131362221, 15);
        I.put(2131362361, 16);
        I.put(2131362362, 17);
        I.put(2131362895, 18);
        I.put(2131362453, 19);
        I.put(2131362454, 20);
        I.put(2131362897, 21);
        I.put(2131362899, 22);
    }
    */

    @DexIgnore
    public id4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 23, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public id4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[3], objArr[1], objArr[6], objArr[8], objArr[14], objArr[13], objArr[15], objArr[12], objArr[16], objArr[17], objArr[19], objArr[20], objArr[10], objArr[5], objArr[7], objArr[2], objArr[4], objArr[0], objArr[18], objArr[21], objArr[22], objArr[11], objArr[9]);
        this.G = -1;
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
