package com.fossil;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$pickerImageIntent$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class pj5$h$a extends sf6 implements ig6<il6, xe6<? super Intent>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ FragmentActivity $it;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj5$h$a(FragmentActivity fragmentActivity, xe6 xe6) {
        super(2, xe6);
        this.$it = fragmentActivity;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pj5$h$a pj5_h_a = new pj5$h$a(this.$it, xe6);
        pj5_h_a.p$ = (il6) obj;
        return pj5_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pj5$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return lk4.b(this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
