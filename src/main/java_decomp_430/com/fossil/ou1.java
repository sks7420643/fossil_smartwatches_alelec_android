package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ou1 extends ga2 implements nu1 {
    @DexIgnore
    public ou1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @DexIgnore
    public final void a(lu1 lu1, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel q = q();
        ia2.a(q, (IInterface) lu1);
        ia2.a(q, (Parcelable) googleSignInOptions);
        a(102, q);
    }

    @DexIgnore
    public final void b(lu1 lu1, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel q = q();
        ia2.a(q, (IInterface) lu1);
        ia2.a(q, (Parcelable) googleSignInOptions);
        a(103, q);
    }
}
