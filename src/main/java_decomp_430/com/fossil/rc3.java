package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rc3<TResult> {
    @DexIgnore
    public /* final */ od3<TResult> a; // = new od3<>();

    @DexIgnore
    public rc3() {
    }

    @DexIgnore
    public void a(TResult tresult) {
        this.a.a(tresult);
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.b(tresult);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.a.a(exc);
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.b(exc);
    }

    @DexIgnore
    public rc3(gc3 gc3) {
        gc3.a(new md3(this));
    }

    @DexIgnore
    public qc3<TResult> a() {
        return this.a;
    }
}
