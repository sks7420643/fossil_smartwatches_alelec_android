package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ca3 extends t63 implements v63 {
    @DexIgnore
    public /* final */ ea3 b;

    @DexIgnore
    public ca3(ea3 ea3) {
        super(ea3.v());
        w12.a(ea3);
        this.b = ea3;
    }

    @DexIgnore
    public sa3 m() {
        return this.b.m();
    }

    @DexIgnore
    public ia3 n() {
        return this.b.o();
    }

    @DexIgnore
    public yz2 o() {
        return this.b.l();
    }

    @DexIgnore
    public s53 p() {
        return this.b.j();
    }
}
