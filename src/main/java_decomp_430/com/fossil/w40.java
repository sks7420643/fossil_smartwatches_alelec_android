package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w40 extends p40 implements Parcelable, Comparable<w40> {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public w40(byte b2, byte b3) {
        this.a = b2;
        this.b = b3;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("major", Byte.valueOf(this.a)).put("minor", Byte.valueOf(this.b));
        wg6.a(put, "JSONObject()\n           \u2026     .put(\"minor\", minor)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(w40.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            w40 w40 = (w40) obj;
            return this.a == w40.a && this.b == w40.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.Version");
    }

    @DexIgnore
    public final byte getMajor() {
        return this.a;
    }

    @DexIgnore
    public final byte getMinor() {
        return this.b;
    }

    @DexIgnore
    public final String getShortDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append('.');
        sb.append(this.b);
        return sb.toString();
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append('.');
        sb.append(this.b);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w40> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final w40 a(byte[] bArr) {
            if (bArr.length < 2) {
                return null;
            }
            return new w40(bArr[0], bArr[1]);
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new w40(parcel.readByte(), parcel.readByte());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w40[i];
        }

        @DexIgnore
        public final w40 a(String str) {
            List a = yj6.a(str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null);
            if (a.size() >= 2) {
                Byte a2 = wj6.a((String) a.get(0));
                Byte a3 = wj6.a((String) a.get(1));
                if (!(a2 == null || a3 == null)) {
                    return new w40(a2.byteValue(), a3.byteValue());
                }
            }
            return new w40((byte) 0, (byte) 0);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(w40 w40) {
        byte b2 = this.a;
        byte b3 = w40.a;
        if (b2 <= b3) {
            if (b2 >= b3) {
                byte b4 = this.b;
                byte b5 = w40.b;
                if (b4 <= b5) {
                    return b4 < b5 ? -1 : 0;
                }
            }
        }
        return 1;
    }
}
