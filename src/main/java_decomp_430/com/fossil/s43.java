package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s43 extends SQLiteOpenHelper {
    @DexIgnore
    public /* final */ /* synthetic */ p43 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s43(p43 p43, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        this.a = p43;
    }

    @DexIgnore
    public final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        try {
            return super.getWritableDatabase();
        } catch (SQLiteDatabaseLockedException e) {
            throw e;
        } catch (SQLiteException unused) {
            this.a.b().t().a("Opening the local database failed, dropping and recreating it");
            if (!this.a.c().getDatabasePath("google_app_measurement_local.db").delete()) {
                this.a.b().t().a("Failed to delete corrupted local db file", "google_app_measurement_local.db");
            }
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException e2) {
                this.a.b().t().a("Failed to open local database. Events will bypass local storage", e2);
                return null;
            }
        }
    }

    @DexIgnore
    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        c03.a(this.a.b(), sQLiteDatabase);
    }

    @DexIgnore
    public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.String[]] */
    /* JADX WARNING: type inference failed for: r0v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void onOpen(SQLiteDatabase sQLiteDatabase) {
        if (Build.VERSION.SDK_INT < 15) {
            Object r0 = 0;
            try {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", r0);
                rawQuery.moveToFirst();
                r0 = rawQuery;
            } finally {
                if (r0 != 0) {
                    r0.close();
                }
            }
        }
        c03.a(this.a.b(), sQLiteDatabase, GraphRequest.DEBUG_MESSAGES_KEY, "create table if not exists messages ( type INTEGER NOT NULL, entry BLOB NOT NULL)", "type,entry", (String[]) null);
    }

    @DexIgnore
    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
