package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he0 extends if1 {
    @DexIgnore
    public /* final */ NotificationFilter[] B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ he0(ue1 ue1, q41 q41, NotificationFilter[] notificationFilterArr, String str, int i) {
        super(ue1, q41, eh1.SET_NOTIFICATION_FILTER, (i & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
        this.B = notificationFilterArr;
    }

    @DexIgnore
    public void h() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        boolean z = false;
        for (NotificationFilter iconConfig : this.B) {
            NotificationIconConfig iconConfig2 = iconConfig.getIconConfig();
            if (iconConfig2 != null) {
                for (NotificationIcon addIfAbsent : iconConfig2.b()) {
                    copyOnWriteArrayList.addIfAbsent(addIfAbsent);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new NotificationIcon[0]);
        if (array != null) {
            NotificationIcon[] notificationIconArr = (NotificationIcon[]) array;
            if (notificationIconArr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    co0 co0 = co0.d;
                    w40 w40 = this.x.a().i().get(Short.valueOf(w31.NOTIFICATION.a));
                    if (w40 == null) {
                        w40 = mi0.A.g();
                    }
                    if1.a((if1) this, (if1) new p21(this.w, this.x, eh1.PUT_NOTIFICATION_ICON, true, 1793, co0.a(notificationIconArr, 1793, w40), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), (hg6) new uk1(this), (hg6) new mm1(this), (ig6) new co1(this), (hg6) null, (hg6) null, 48, (Object) null);
                } catch (sw0 e) {
                    qs0.h.a(e);
                    a(km1.a(this.v, (eh1) null, sk1.INCOMPATIBLE_FIRMWARE, (bn0) null, 5));
                }
            } else {
                m();
            }
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NOTIFICATION_FILTERS, (Object) cw0.a((p40[]) this.B));
    }

    @DexIgnore
    public final void m() {
        short b = lk1.b.b(this.w.t, w31.NOTIFICATION_FILTER);
        try {
            qf0 qf0 = qf0.d;
            w40 w40 = this.x.a().i().get(Short.valueOf(w31.NOTIFICATION_FILTER.a));
            if (w40 == null) {
                w40 = mi0.A.g();
            }
            byte[] a = qf0.a(b, w40, this.B);
            if1.a((if1) this, (if1) new p21(this.w, this.x, eh1.PUT_NOTIFICATION_FILTER_RULE, true, b, a, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), (hg6) new gh1(this), (hg6) new aj1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
        } catch (sw0 e) {
            qs0.h.a(e);
            a(km1.a(this.v, (eh1) null, sk1.UNSUPPORTED_FORMAT, (bn0) null, 5));
        }
    }
}
