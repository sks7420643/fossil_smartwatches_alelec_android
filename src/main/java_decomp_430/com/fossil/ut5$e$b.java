package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
public final class ut5$e$b extends sf6 implements ig6<il6, xe6<? super List<Alarm>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut5$e$b(SignUpPresenter.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ut5$e$b ut5_e_b = new ut5$e$b(this.this$0, xe6);
        ut5_e_b.p$ = (il6) obj;
        return ut5_e_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ut5$e$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.p().getActiveAlarms();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
