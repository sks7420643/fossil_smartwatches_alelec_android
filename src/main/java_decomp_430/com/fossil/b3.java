package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b3 extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    @DexIgnore
    public Runnable a;
    @DexIgnore
    public c b;
    @DexIgnore
    public LinearLayoutCompat c; // = b();
    @DexIgnore
    public Spinner d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public ViewPropertyAnimator j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View a;

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public void run() {
            b3.this.smoothScrollTo(this.a.getLeft() - ((b3.this.getWidth() - this.a.getWidth()) / 2), 0);
            b3.this.a = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends BaseAdapter {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int getCount() {
            return b3.this.c.getChildCount();
        }

        @DexIgnore
        public Object getItem(int i) {
            return ((d) b3.this.c.getChildAt(i)).a();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return b3.this.a((ActionBar.b) getItem(i), true);
            }
            ((d) view).a((ActionBar.b) getItem(i));
            return view;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(View view) {
            ((d) view).a().e();
            int childCount = b3.this.c.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = b3.this.c.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.a) {
                b3 b3Var = b3.this;
                b3Var.j = null;
                b3Var.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            b3.this.setVisibility(0);
            this.a = false;
        }
    }

    /*
    static {
        new DecelerateInterpolator();
    }
    */

    @DexIgnore
    public b3(Context context) {
        super(context);
        new e();
        setHorizontalScrollBarEnabled(false);
        c1 a2 = c1.a(context);
        setContentHeight(a2.e());
        this.g = a2.d();
        addView(this.c, new ViewGroup.LayoutParams(-2, -1));
    }

    @DexIgnore
    public final Spinner a() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), (AttributeSet) null, a0.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    @DexIgnore
    public final LinearLayoutCompat b() {
        LinearLayoutCompat linearLayoutCompat = new LinearLayoutCompat(getContext(), (AttributeSet) null, a0.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        linearLayoutCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }

    @DexIgnore
    public final boolean c() {
        Spinner spinner = this.d;
        return spinner != null && spinner.getParent() == this;
    }

    @DexIgnore
    public final void d() {
        if (!c()) {
            if (this.d == null) {
                this.d = a();
            }
            removeView(this.c);
            addView(this.d, new ViewGroup.LayoutParams(-2, -1));
            if (this.d.getAdapter() == null) {
                this.d.setAdapter(new b());
            }
            Runnable runnable = this.a;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.a = null;
            }
            this.d.setSelection(this.i);
        }
    }

    @DexIgnore
    public final boolean e() {
        if (!c()) {
            return false;
        }
        removeView(this.d);
        addView(this.c, new ViewGroup.LayoutParams(-2, -1));
        setTabSelected(this.d.getSelectedItemPosition());
        return false;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Runnable runnable = this.a;
        if (runnable != null) {
            post(runnable);
        }
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        c1 a2 = c1.a(getContext());
        setContentHeight(a2.e());
        this.g = a2.d();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.a;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    @DexIgnore
    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((d) view).a().e();
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        boolean z = true;
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.c.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f = -1;
        } else {
            if (childCount > 2) {
                this.f = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.f = View.MeasureSpec.getSize(i2) / 2;
            }
            this.f = Math.min(this.f, this.g);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.h, 1073741824);
        if (z2 || !this.e) {
            z = false;
        }
        if (z) {
            this.c.measure(0, makeMeasureSpec);
            if (this.c.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                d();
            } else {
                e();
            }
        } else {
            e();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.i);
        }
    }

    @DexIgnore
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @DexIgnore
    public void setAllowCollapse(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void setContentHeight(int i2) {
        this.h = i2;
        requestLayout();
    }

    @DexIgnore
    public void setTabSelected(int i2) {
        this.i = i2;
        int childCount = this.c.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.c.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        Spinner spinner = this.d;
        if (spinner != null && i2 >= 0) {
            spinner.setSelection(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends LinearLayout {
        @DexIgnore
        public /* final */ int[] a; // = {16842964};
        @DexIgnore
        public ActionBar.b b;
        @DexIgnore
        public TextView c;
        @DexIgnore
        public ImageView d;
        @DexIgnore
        public View e;

        @DexIgnore
        public d(Context context, ActionBar.b bVar, boolean z) {
            super(context, (AttributeSet) null, a0.actionBarTabStyle);
            this.b = bVar;
            i3 a2 = i3.a(context, (AttributeSet) null, this.a, a0.actionBarTabStyle, 0);
            if (a2.g(0)) {
                setBackgroundDrawable(a2.b(0));
            }
            a2.a();
            if (z) {
                setGravity(8388627);
            }
            b();
        }

        @DexIgnore
        public void a(ActionBar.b bVar) {
            this.b = bVar;
            b();
        }

        @DexIgnore
        public void b() {
            ActionBar.b bVar = this.b;
            View b2 = bVar.b();
            CharSequence charSequence = null;
            if (b2 != null) {
                ViewParent parent = b2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(b2);
                    }
                    addView(b2);
                }
                this.e = b2;
                TextView textView = this.c;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.d;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.d.setImageDrawable((Drawable) null);
                    return;
                }
                return;
            }
            View view = this.e;
            if (view != null) {
                removeView(view);
                this.e = null;
            }
            Drawable c2 = bVar.c();
            CharSequence d2 = bVar.d();
            if (c2 != null) {
                if (this.d == null) {
                    AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.d = appCompatImageView;
                }
                this.d.setImageDrawable(c2);
                this.d.setVisibility(0);
            } else {
                ImageView imageView2 = this.d;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.d.setImageDrawable((Drawable) null);
                }
            }
            boolean z = !TextUtils.isEmpty(d2);
            if (z) {
                if (this.c == null) {
                    AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), (AttributeSet) null, a0.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.c = appCompatTextView;
                }
                this.c.setText(d2);
                this.c.setVisibility(0);
            } else {
                TextView textView2 = this.c;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.c.setText((CharSequence) null);
                }
            }
            ImageView imageView3 = this.d;
            if (imageView3 != null) {
                imageView3.setContentDescription(bVar.a());
            }
            if (!z) {
                charSequence = bVar.a();
            }
            k3.a(this, charSequence);
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        @DexIgnore
        public void onMeasure(int i, int i2) {
            int i3;
            super.onMeasure(i, i2);
            if (b3.this.f > 0 && getMeasuredWidth() > (i3 = b3.this.f)) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
            }
        }

        @DexIgnore
        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        @DexIgnore
        public ActionBar.b a() {
            return this.b;
        }
    }

    @DexIgnore
    public void a(int i2) {
        View childAt = this.c.getChildAt(i2);
        Runnable runnable = this.a;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        this.a = new a(childAt);
        post(this.a);
    }

    @DexIgnore
    public d a(ActionBar.b bVar, boolean z) {
        d dVar = new d(getContext(), bVar, z);
        if (z) {
            dVar.setBackgroundDrawable((Drawable) null);
            dVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.h));
        } else {
            dVar.setFocusable(true);
            if (this.b == null) {
                this.b = new c();
            }
            dVar.setOnClickListener(this.b);
        }
        return dVar;
    }
}
