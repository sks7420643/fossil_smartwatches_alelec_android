package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.util.DeviceUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1", f = "HomeProfilePresenter.kt", l = {383, 183, 186}, m = "invokeSuspend")
public final class pj5$l$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $deviceWrappers;
    @DexIgnore
    public /* final */ /* synthetic */ List $devices;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public boolean Z$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter.l this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$4$1$1$isLatestFw$1", f = "HomeProfilePresenter.kt", l = {186}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, xe6 xe6) {
            super(2, xe6);
            this.$activeSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.$activeSerial, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DeviceUtils a2 = DeviceUtils.g.a();
                String str = this.$activeSerial;
                this.L$0 = il6;
                this.label = 1;
                obj = a2.a(str, (Device) null, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super String>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $deviceModel;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pj5$l$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Device device, xe6 xe6, pj5$l$a pj5_l_a) {
            super(2, xe6);
            this.$deviceModel = device;
            this.this$0 = pj5_l_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$deviceModel, xe6, this.this$0);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.this$0.a.w.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj5$l$a(HomeProfilePresenter.l lVar, List list, ArrayList arrayList, xe6 xe6) {
        super(2, xe6);
        this.this$0 = lVar;
        this.$devices = list;
        this.$deviceWrappers = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pj5$l$a pj5_l_a = new pj5$l$a(this.this$0, this.$devices, this.$deviceWrappers, xe6);
        pj5_l_a.p$ = (il6) obj;
        return pj5_l_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pj5$l$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v5, resolved type: com.fossil.xp6} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e2 A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0135 A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x014b A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x014e A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x015e A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0264 A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0291 A[Catch:{ all -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02fe A[Catch:{ all -> 0x006e }] */
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        Iterator it;
        Device device;
        il6 il6;
        Object obj2;
        String str;
        pj5$l$a pj5_l_a;
        int i;
        Object obj3;
        Iterator it2;
        il6 il62;
        Object obj4;
        Object obj5;
        xp6 xp62;
        Object a2 = ff6.a();
        int i2 = this.label;
        if (i2 == 0) {
            nc6.a(obj);
            il6 = this.p$;
            xp62 = this.this$0.a.o;
            this.L$0 = il6;
            this.L$1 = xp62;
            this.label = 1;
            if (xp62.a((Object) null, this) == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            xp62 = (xp6) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i2 == 2) {
            Device device2 = (Device) this.L$4;
            Iterator it3 = (Iterator) this.L$3;
            String str2 = (String) this.L$2;
            xp6 = this.L$1;
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            obj4 = obj;
            it = it3;
            il62 = il63;
            obj5 = a2;
            device = device2;
            str = str2;
            pj5_l_a = this;
            String str3 = (String) obj4;
            dl6 a3 = pj5_l_a.this$0.a.b();
            a aVar = new a(str, (xe6) null);
            pj5_l_a.L$0 = il62;
            pj5_l_a.L$1 = xp6;
            pj5_l_a.L$2 = str;
            pj5_l_a.L$3 = it;
            pj5_l_a.L$4 = device;
            pj5_l_a.L$5 = str3;
            pj5_l_a.Z$0 = false;
            pj5_l_a.Z$1 = false;
            pj5_l_a.label = 3;
            obj2 = gk6.a(a3, aVar, pj5_l_a);
            if (obj2 != obj5) {
            }
            return obj5;
        } else if (i2 == 3) {
            boolean z = this.Z$1;
            boolean z2 = this.Z$0;
            String str4 = (String) this.L$5;
            device = (Device) this.L$4;
            it = (Iterator) this.L$3;
            String str5 = (String) this.L$2;
            xp6 = (xp6) this.L$1;
            il6 il64 = (il6) this.L$0;
            try {
                nc6.a(obj);
                obj2 = obj;
                boolean z3 = z;
                boolean z4 = z2;
                String str6 = str4;
                str = str5;
                il6 il65 = il64;
                Object obj6 = a2;
                pj5_l_a = this;
                boolean booleanValue = ((Boolean) obj2).booleanValue();
                if (device.getBatteryLevel() <= 100) {
                    i = 100;
                } else {
                    i = device.getBatteryLevel();
                }
                if (!wg6.a((Object) device.getDeviceId(), (Object) str)) {
                    pj5_l_a.$deviceWrappers.add(new HomeProfilePresenter.b(device.getDeviceId(), z4, str6, i, z3, booleanValue));
                } else {
                    pj5_l_a.$deviceWrappers.add(0, new HomeProfilePresenter.b(device.getDeviceId(), PortfolioApp.get.instance().h(device.getDeviceId()) == 2, str6, i, true, booleanValue));
                    if (!wg6.a((Object) "release", (Object) Constants.DEBUG)) {
                        if (!wg6.a((Object) "release", (Object) "staging")) {
                            if (!FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                                if (device.getBatteryLevel() > 25 || device.getBatteryLevel() <= 0) {
                                    pj5_l_a.this$0.a.m().a(false, false);
                                } else {
                                    pj5_l_a.this$0.a.m().a(true, false);
                                }
                            } else if (device.getBatteryLevel() > 10 || device.getBatteryLevel() <= 0) {
                                pj5_l_a.this$0.a.m().a(false, true);
                            } else {
                                pj5_l_a.this$0.a.m().a(true, true);
                            }
                        }
                    }
                    if (!FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId())) {
                        if (device.getBatteryLevel() >= PortfolioApp.get.instance().w().x() || device.getBatteryLevel() <= 0) {
                            pj5_l_a.this$0.a.m().a(false, false);
                        } else {
                            pj5_l_a.this$0.a.m().a(true, false);
                        }
                    } else if (device.getBatteryLevel() >= PortfolioApp.get.instance().w().x() || device.getBatteryLevel() <= 0) {
                        pj5_l_a.this$0.a.m().a(false, true);
                    } else {
                        pj5_l_a.this$0.a.m().a(true, true);
                    }
                }
                obj3 = obj6;
                it2 = it;
                if (it2.hasNext()) {
                    Device device3 = (Device) it2.next();
                    dl6 b2 = pj5_l_a.this$0.a.c();
                    b bVar = new b(device3, (xe6) null, pj5_l_a);
                    pj5_l_a.L$0 = il6;
                    pj5_l_a.L$1 = xp6;
                    pj5_l_a.L$2 = str;
                    pj5_l_a.L$3 = it2;
                    pj5_l_a.L$4 = device3;
                    pj5_l_a.label = 2;
                    obj4 = gk6.a(b2, bVar, pj5_l_a);
                    if (obj4 == obj3) {
                        return obj3;
                    }
                    il62 = il6;
                    obj5 = obj3;
                    device = device3;
                    it = it2;
                    String str32 = (String) obj4;
                    dl6 a32 = pj5_l_a.this$0.a.b();
                    a aVar2 = new a(str, (xe6) null);
                    pj5_l_a.L$0 = il62;
                    pj5_l_a.L$1 = xp6;
                    pj5_l_a.L$2 = str;
                    pj5_l_a.L$3 = it;
                    pj5_l_a.L$4 = device;
                    pj5_l_a.L$5 = str32;
                    pj5_l_a.Z$0 = false;
                    pj5_l_a.Z$1 = false;
                    pj5_l_a.label = 3;
                    obj2 = gk6.a(a32, aVar2, pj5_l_a);
                    if (obj2 != obj5) {
                        return obj5;
                    }
                    str6 = str32;
                    z4 = false;
                    z3 = false;
                    obj6 = obj5;
                    il65 = il62;
                    boolean booleanValue2 = ((Boolean) obj2).booleanValue();
                    if (device.getBatteryLevel() <= 100) {
                    }
                    if (!wg6.a((Object) device.getDeviceId(), (Object) str)) {
                    }
                    obj3 = obj6;
                    it2 = it;
                    if (it2.hasNext()) {
                    }
                    return obj5;
                    return obj3;
                }
                if (!wg6.a((Object) pj5_l_a.$deviceWrappers, (Object) pj5_l_a.this$0.a.k())) {
                    pj5_l_a.this$0.a.k().clear();
                    pj5_l_a.this$0.a.k().addAll(pj5_l_a.$deviceWrappers);
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = HomeProfilePresenter.C.a();
                local.d(a4, "update new device list " + pj5_l_a.this$0.a.k());
                pj5_l_a.this$0.a.m().c(pj5_l_a.this$0.a.k());
                if (System.currentTimeMillis() - PortfolioApp.get.instance().w().g(str) < 60000) {
                    pj5_l_a.this$0.a.n();
                }
                FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), "updatedDevices done");
                cd6 cd6 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } catch (Throwable th) {
                xp6.a((Object) null);
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        xp6 = xp62;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a5 = HomeProfilePresenter.C.a();
        local2.d(a5, "updatedDevices " + this.$devices + " currentDevices " + this.this$0.a.g);
        str = PortfolioApp.get.instance().e();
        it2 = this.$devices.iterator();
        obj3 = a2;
        pj5_l_a = this;
        if (it2.hasNext()) {
        }
        if (!wg6.a((Object) pj5_l_a.$deviceWrappers, (Object) pj5_l_a.this$0.a.k())) {
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String a42 = HomeProfilePresenter.C.a();
        local3.d(a42, "update new device list " + pj5_l_a.this$0.a.k());
        pj5_l_a.this$0.a.m().c(pj5_l_a.this$0.a.k());
        if (System.currentTimeMillis() - PortfolioApp.get.instance().w().g(str) < 60000) {
        }
        FLogger.INSTANCE.getLocal().d(HomeProfilePresenter.C.a(), "updatedDevices done");
        cd6 cd62 = cd6.a;
        xp6.a((Object) null);
        return cd6.a;
    }
}
