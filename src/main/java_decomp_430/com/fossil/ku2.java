package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku2 implements lu2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;
    @DexIgnore
    public static /* final */ pk2<Boolean> d;
    @DexIgnore
    public static /* final */ pk2<Boolean> e;
    @DexIgnore
    public static /* final */ pk2<Boolean> f;
    @DexIgnore
    public static /* final */ pk2<Boolean> g;
    @DexIgnore
    public static /* final */ pk2<Boolean> h;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.service.audience.scoped_filters_v27", false);
        b = vk2.a("measurement.service.audience.session_scoped_user_engagement", false);
        c = vk2.a("measurement.client.audience.scoped_engagement_removal_when_session_expired", true);
        d = vk2.a("measurement.service.audience.scoped_engagement_removal_when_session_expired", true);
        e = vk2.a("measurement.service.audience.session_scoped_event_aggregates", false);
        f = vk2.a("measurement.service.audience.use_bundle_timestamp_for_property_filters", false);
        vk2.a("measurement.id.scoped_audience_filters", 0);
        g = vk2.a("measurement.service.audience.not_prepend_timestamps_for_sequence_session_scoped_filters", false);
        h = vk2.a("measurement.service.audience.remove_disabled_session_scoped_user_engagement", false);
    }
    */

    @DexIgnore
    public final boolean e() {
        return h.b().booleanValue();
    }

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzd() {
        return c.b().booleanValue();
    }

    @DexIgnore
    public final boolean zze() {
        return d.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzf() {
        return e.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzg() {
        return f.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzh() {
        return g.b().booleanValue();
    }
}
