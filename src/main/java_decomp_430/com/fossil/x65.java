package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.background.BackgroundImgData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x65 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ArrayList<WatchFaceWrapper> c;
    @DexIgnore
    public d d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public View a;
        @DexIgnore
        public /* final */ /* synthetic */ x65 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(x65 x65, View view) {
            super(view);
            wg6.b(view, "view");
            this.b = x65;
            this.a = view.findViewById(2131362535);
            this.a.setOnClickListener(this);
        }

        @DexIgnore
        public void onClick(View view) {
            d d = this.b.d();
            if (d != null) {
                d.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public TextView c;
        @DexIgnore
        public View d;
        @DexIgnore
        public /* final */ /* synthetic */ x65 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                d d;
                if (!(this.a.e.getItemCount() <= this.a.getAdapterPosition() || this.a.getAdapterPosition() == -1 || (d = this.a.e.d()) == null)) {
                    Object obj = this.a.e.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.a((WatchFaceWrapper) obj);
                }
                b bVar = this.a;
                bVar.e.a(bVar.getAdapterPosition());
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x65$b$b")
        /* renamed from: com.fossil.x65$b$b  reason: collision with other inner class name */
        public static final class C0055b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0055b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                this.a.e.a(true);
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                d d;
                if (this.a.e.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.e.d()) != null) {
                    Object obj = this.a.e.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.b((WatchFaceWrapper) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public d(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.e.a(false);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(x65 x65, View view) {
            super(view);
            wg6.b(view, "view");
            this.e = x65;
            View findViewById = view.findViewById(2131362616);
            wg6.a((Object) findViewById, "view.findViewById(R.id.iv_remove)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131362547);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.iv_background_preview)");
            this.b = (ImageView) findViewById2;
            View findViewById3 = view.findViewById(2131363186);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.tv_name)");
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(2131363259);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.v_background_selected)");
            this.d = findViewById4;
            this.b.setOnClickListener(new a(this));
            this.b.setOnLongClickListener(new C0055b(this));
            this.a.setOnClickListener(new c(this));
            view.setOnClickListener(new d(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(WatchFaceWrapper watchFaceWrapper, int i) {
            wg6.b(watchFaceWrapper, "watchFaceWrapper");
            if (watchFaceWrapper.isRemoveIconVisible()) {
                this.a.setVisibility(0);
            } else {
                this.a.setVisibility(4);
            }
            Drawable combination = watchFaceWrapper.getCombination();
            if (combination != null) {
                this.b.setImageDrawable(combination);
            } else {
                this.b.setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231278));
            }
            this.c.setText("PHOTO " + watchFaceWrapper.getName());
            if (i == this.e.a) {
                this.c.setTextColor(w6.a(PortfolioApp.get.instance(), 2131100008));
                this.d.setVisibility(0);
                return;
            }
            this.c.setTextColor(w6.a(PortfolioApp.get.instance(), 2131100374));
            this.d.setVisibility(8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(WatchFaceWrapper watchFaceWrapper);

        @DexIgnore
        void b(WatchFaceWrapper watchFaceWrapper);
    }

    /*
    static {
        new c((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ x65(ArrayList arrayList, d dVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : dVar);
    }

    @DexIgnore
    public final WatchFaceWrapper c() {
        BackgroundImgData backgroundImgData = new BackgroundImgData("", "");
        BackgroundConfig backgroundConfig = r0;
        BackgroundConfig backgroundConfig2 = new BackgroundConfig(0, backgroundImgData, backgroundImgData, backgroundImgData, backgroundImgData, backgroundImgData);
        return new WatchFaceWrapper("9999", (Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null, (WatchFaceWrapper.MetaData) null, (WatchFaceWrapper.MetaData) null, (WatchFaceWrapper.MetaData) null, (WatchFaceWrapper.MetaData) null, "", backgroundConfig, ai4.ADD, false, 16384, (qg6) null);
    }

    @DexIgnore
    public final d d() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return this.c.get(i).getType().getValue();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        if (getItemCount() > i && i != -1 && (viewHolder instanceof b)) {
            WatchFaceWrapper watchFaceWrapper = this.c.get(i);
            wg6.a((Object) watchFaceWrapper, "mData[position]");
            ((b) viewHolder).a(watchFaceWrapper, i);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == ai4.PHOTO.getValue()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558659, viewGroup, false);
            wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026ackground, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558688, viewGroup, false);
        wg6.a((Object) inflate2, "LayoutInflater.from(pare\u2026round_add, parent, false)");
        return new a(this, inflate2);
    }

    @DexIgnore
    public x65(ArrayList<WatchFaceWrapper> arrayList, d dVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = dVar;
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        wg6.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaPhotoAdapter", "setData size = " + list.size());
        this.c.clear();
        this.c.addAll(list);
        this.c.add(c());
        a(this.b);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(WatchFaceWrapper watchFaceWrapper) {
        wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaPhotoAdapter", "removeItem() item = " + watchFaceWrapper);
        int indexOf = this.c.indexOf(watchFaceWrapper);
        if (indexOf != -1) {
            this.c.remove(watchFaceWrapper);
            int i = this.a;
            if (indexOf > i) {
                notifyItemRemoved(indexOf);
                return;
            }
            this.a = i - 1;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final int a(String str) {
        wg6.b(str, "id");
        Iterator<WatchFaceWrapper> it = this.c.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (wg6.a((Object) it.next().getId(), (Object) str)) {
                break;
            } else {
                i++;
            }
        }
        a(i);
        return i;
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(d dVar) {
        wg6.b(dVar, "listener");
        this.d = dVar;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
        for (WatchFaceWrapper removeIconVisible : this.c) {
            removeIconVisible.setRemoveIconVisible(z);
        }
        notifyDataSetChanged();
    }
}
