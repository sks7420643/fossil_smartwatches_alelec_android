package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bz3 extends qy3 {
    @DexIgnore
    public int a() {
        return 3;
    }

    @DexIgnore
    public void a(vy3 vy3) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!vy3.i()) {
                break;
            }
            char c = vy3.c();
            vy3.f++;
            a(c, sb);
            if (sb.length() % 3 == 0) {
                qy3.b(vy3, sb);
                int a = xy3.a(vy3.d(), vy3.f, a());
                if (a != a()) {
                    vy3.b(a);
                    break;
                }
            }
        }
        a(vy3, sb);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == 13) {
            sb.append(0);
        } else if (c == '*') {
            sb.append(1);
        } else if (c == '>') {
            sb.append(2);
        } else if (c == ' ') {
            sb.append(3);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            xy3.a(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    @DexIgnore
    public void a(vy3 vy3, StringBuilder sb) {
        vy3.l();
        int a = vy3.g().a() - vy3.a();
        vy3.f -= sb.length();
        if (vy3.f() > 1 || a > 1 || vy3.f() != a) {
            vy3.a(254);
        }
        if (vy3.e() < 0) {
            vy3.b(0);
        }
    }
}
