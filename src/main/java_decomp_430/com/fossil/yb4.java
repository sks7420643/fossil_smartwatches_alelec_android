package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yb4 extends xb4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E; // = new SparseIntArray();
    @DexIgnore
    public long C;

    /*
    static {
        E.put(2131362542, 1);
        E.put(2131362442, 2);
        E.put(2131362569, 3);
        E.put(2131362301, 4);
        E.put(2131362867, 5);
        E.put(2131362071, 6);
        E.put(2131362424, 7);
        E.put(2131362680, 8);
        E.put(2131362393, 9);
        E.put(2131363267, 10);
        E.put(2131362679, 11);
        E.put(2131362392, 12);
        E.put(2131363268, 13);
        E.put(2131362674, 14);
        E.put(2131362344, 15);
        E.put(2131363269, 16);
    }
    */

    @DexIgnore
    public yb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 17, D, E));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public yb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[4], objArr[15], objArr[12], objArr[9], objArr[7], objArr[2], objArr[1], objArr[3], objArr[14], objArr[11], objArr[8], objArr[0], objArr[5], objArr[10], objArr[13], objArr[16]);
        this.C = -1;
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
