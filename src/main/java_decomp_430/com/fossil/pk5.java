package com.fossil;

import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk5 implements MembersInjector<ProfileGoalEditActivity> {
    @DexIgnore
    public static void a(ProfileGoalEditActivity profileGoalEditActivity, ProfileGoalEditPresenter profileGoalEditPresenter) {
        profileGoalEditActivity.C = profileGoalEditPresenter;
    }
}
