package com.fossil;

import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class am4 {
    @DexIgnore
    public static /* final */ byte[] c; // = "from-data".getBytes();
    @DexIgnore
    public static /* final */ byte[] d; // = "attachment".getBytes();
    @DexIgnore
    public static /* final */ byte[] e; // = "inline".getBytes();
    @DexIgnore
    public Map<Integer, Object> a;
    @DexIgnore
    public byte[] b;

    @DexIgnore
    public am4() {
        this.a = null;
        this.b = null;
        this.a = new HashMap();
    }

    @DexIgnore
    public byte[] a() {
        return (byte[]) this.a.get(192);
    }

    @DexIgnore
    public void b(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("Content-Id may not be null or empty.");
        } else if (bArr.length > 1 && ((char) bArr[0]) == '<' && ((char) bArr[bArr.length - 1]) == '>') {
            this.a.put(192, bArr);
        } else {
            byte[] bArr2 = new byte[(bArr.length + 2)];
            bArr2[0] = 60;
            bArr2[bArr2.length - 1] = 62;
            System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
            this.a.put(192, bArr2);
        }
    }

    @DexIgnore
    public void c(byte[] bArr) {
        if (bArr != null) {
            this.a.put(142, bArr);
            return;
        }
        throw new NullPointerException("null content-location");
    }

    @DexIgnore
    public byte[] d() {
        return (byte[]) this.a.get(145);
    }

    @DexIgnore
    public void e(byte[] bArr) {
        if (bArr != null) {
            this.a.put(145, bArr);
            return;
        }
        throw new NullPointerException("null content-type");
    }

    @DexIgnore
    public void f(byte[] bArr) {
        if (bArr != null) {
            this.b = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.b, 0, bArr.length);
        }
    }

    @DexIgnore
    public void g(byte[] bArr) {
        if (bArr != null) {
            this.a.put(152, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }

    @DexIgnore
    public void h(byte[] bArr) {
        if (bArr != null) {
            this.a.put(151, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }

    @DexIgnore
    public void a(int i) {
        this.a.put(129, Integer.valueOf(i));
    }

    @DexIgnore
    public void d(byte[] bArr) {
        if (bArr != null) {
            this.a.put(Integer.valueOf(MFNetworkReturnCode.RESPONSE_OK), bArr);
            return;
        }
        throw new NullPointerException("null content-transfer-encoding");
    }

    @DexIgnore
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.a.put(197, bArr);
            return;
        }
        throw new NullPointerException("null content-disposition");
    }

    @DexIgnore
    public byte[] c() {
        return (byte[]) this.a.get(Integer.valueOf(MFNetworkReturnCode.RESPONSE_OK));
    }

    @DexIgnore
    public byte[] e() {
        return (byte[]) this.a.get(152);
    }

    @DexIgnore
    public byte[] f() {
        return (byte[]) this.a.get(151);
    }

    @DexIgnore
    public byte[] b() {
        return (byte[]) this.a.get(142);
    }
}
