package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ wc3 b;

    @DexIgnore
    public xc3(wc3 wc3, qc3 qc3) {
        this.b = wc3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        if (this.a.c()) {
            this.b.c.f();
            return;
        }
        try {
            this.b.c.a(this.b.b.then(this.a));
        } catch (oc3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.c.a((Exception) e.getCause());
            } else {
                this.b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.b.c.a(e2);
        }
    }
}
