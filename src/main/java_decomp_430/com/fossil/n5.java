package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n5 extends j5 {
    @DexIgnore
    public j5[] k0; // = new j5[4];
    @DexIgnore
    public int l0; // = 0;

    @DexIgnore
    public void K() {
        this.l0 = 0;
    }

    @DexIgnore
    public void b(j5 j5Var) {
        int i = this.l0 + 1;
        j5[] j5VarArr = this.k0;
        if (i > j5VarArr.length) {
            this.k0 = (j5[]) Arrays.copyOf(j5VarArr, j5VarArr.length * 2);
        }
        j5[] j5VarArr2 = this.k0;
        int i2 = this.l0;
        j5VarArr2[i2] = j5Var;
        this.l0 = i2 + 1;
    }
}
