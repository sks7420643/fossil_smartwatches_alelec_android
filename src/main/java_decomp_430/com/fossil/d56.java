package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d56 {
    @DexIgnore
    public static f56 c;
    @DexIgnore
    public static b56 d; // = m56.b();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();
    @DexIgnore
    public Integer a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public d56(Context context) {
        try {
            a(context);
            this.a = m56.p(context.getApplicationContext());
            this.b = b46.a(context).b();
        } catch (Throwable th) {
            d.a(th);
        }
    }

    @DexIgnore
    public static synchronized f56 a(Context context) {
        f56 f56;
        synchronized (d56.class) {
            if (c == null) {
                c = new f56(context.getApplicationContext());
            }
            f56 = c;
        }
        return f56;
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        Object obj;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            r56.a(jSONObject2, "cn", this.b);
            if (this.a != null) {
                jSONObject2.put("tn", this.a);
            }
            if (thread == null) {
                str = "ev";
                obj = jSONObject2;
            } else {
                str = "errkv";
                obj = jSONObject2.toString();
            }
            jSONObject.put(str, obj);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.a(th);
        }
    }
}
