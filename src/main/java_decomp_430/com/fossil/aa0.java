package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa0 extends z90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<aa0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new aa0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new aa0[i];
        }
    }

    @DexIgnore
    public aa0(byte b, vd0 vd0) {
        super(e90.RING_MY_PHONE_MICRO_APP, b, vd0);
    }

    @DexIgnore
    public /* synthetic */ aa0(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
