package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fn2;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo2<T> implements fp2<T> {
    @DexIgnore
    public static /* final */ int[] q; // = new int[0];
    @DexIgnore
    public static /* final */ Unsafe r; // = cq2.c();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ ro2 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int[] i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ xo2 l;
    @DexIgnore
    public /* final */ ao2 m;
    @DexIgnore
    public /* final */ wp2<?, ?> n;
    @DexIgnore
    public /* final */ um2<?> o;
    @DexIgnore
    public /* final */ ko2 p;

    @DexIgnore
    public uo2(int[] iArr, Object[] objArr, int i2, int i3, ro2 ro2, boolean z, boolean z2, int[] iArr2, int i4, int i5, xo2 xo2, ao2 ao2, wp2<?, ?> wp2, um2<?> um2, ko2 ko2) {
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        boolean z3 = ro2 instanceof fn2;
        this.g = z;
        this.f = um2 != null && um2.a(ro2);
        this.h = false;
        this.i = iArr2;
        this.j = i4;
        this.k = i5;
        this.l = xo2;
        this.m = ao2;
        this.n = wp2;
        this.o = um2;
        this.e = ro2;
        this.p = ko2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:168:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x03c9  */
    public static <T> uo2<T> a(Class<T> cls, po2 po2, xo2 xo2, ao2 ao2, wp2<?, ?> wp2, um2<?> um2, ko2 ko2) {
        int i2;
        int i3;
        char c2;
        int[] iArr;
        char c3;
        char c4;
        int i4;
        char c5;
        char c6;
        int i5;
        int i6;
        String str;
        char c7;
        int i7;
        char c8;
        int i8;
        int i9;
        int i10;
        int i11;
        Class<?> cls2;
        int i12;
        int i13;
        Field field;
        int i14;
        char charAt;
        int i15;
        char c9;
        Field field2;
        Field field3;
        int i16;
        char charAt2;
        int i17;
        char charAt3;
        int i18;
        char charAt4;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        char charAt5;
        int i25;
        char charAt6;
        int i26;
        char charAt7;
        int i27;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        po2 po22 = po2;
        if (po22 instanceof dp2) {
            dp2 dp2 = (dp2) po22;
            char c10 = 0;
            boolean z = dp2.zza() == fn2.e.j;
            String a2 = dp2.a();
            int length = a2.length();
            char charAt15 = a2.charAt(0);
            if (charAt15 >= 55296) {
                char c11 = charAt15 & 8191;
                int i28 = 1;
                int i29 = 13;
                while (true) {
                    i2 = i28 + 1;
                    charAt14 = a2.charAt(i28);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c11 |= (charAt14 & 8191) << i29;
                    i29 += 13;
                    i28 = i2;
                }
                charAt15 = (charAt14 << i29) | c11;
            } else {
                i2 = 1;
            }
            int i30 = i2 + 1;
            char charAt16 = a2.charAt(i2);
            if (charAt16 >= 55296) {
                char c12 = charAt16 & 8191;
                int i31 = 13;
                while (true) {
                    i3 = i30 + 1;
                    charAt13 = a2.charAt(i30);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c12 |= (charAt13 & 8191) << i31;
                    i31 += 13;
                    i30 = i3;
                }
                charAt16 = c12 | (charAt13 << i31);
            } else {
                i3 = i30;
            }
            if (charAt16 == 0) {
                iArr = q;
                c6 = 0;
                c5 = 0;
                i4 = 0;
                c4 = 0;
                c3 = 0;
                c2 = 0;
            } else {
                int i32 = i3 + 1;
                char charAt17 = a2.charAt(i3);
                if (charAt17 >= 55296) {
                    char c13 = charAt17 & 8191;
                    int i33 = 13;
                    while (true) {
                        i19 = i32 + 1;
                        charAt12 = a2.charAt(i32);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c13 |= (charAt12 & 8191) << i33;
                        i33 += 13;
                        i32 = i19;
                    }
                    charAt17 = (charAt12 << i33) | c13;
                } else {
                    i19 = i32;
                }
                int i34 = i19 + 1;
                char charAt18 = a2.charAt(i19);
                if (charAt18 >= 55296) {
                    char c14 = charAt18 & 8191;
                    int i35 = 13;
                    while (true) {
                        i20 = i34 + 1;
                        charAt11 = a2.charAt(i34);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c14 |= (charAt11 & 8191) << i35;
                        i35 += 13;
                        i34 = i20;
                    }
                    charAt18 = c14 | (charAt11 << i35);
                } else {
                    i20 = i34;
                }
                int i36 = i20 + 1;
                char charAt19 = a2.charAt(i20);
                if (charAt19 >= 55296) {
                    char c15 = charAt19 & 8191;
                    int i37 = 13;
                    while (true) {
                        i21 = i36 + 1;
                        charAt10 = a2.charAt(i36);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c15 |= (charAt10 & 8191) << i37;
                        i37 += 13;
                        i36 = i21;
                    }
                    charAt19 = (charAt10 << i37) | c15;
                } else {
                    i21 = i36;
                }
                int i38 = i21 + 1;
                c4 = a2.charAt(i21);
                if (c4 >= 55296) {
                    char c16 = c4 & 8191;
                    int i39 = 13;
                    while (true) {
                        i22 = i38 + 1;
                        charAt9 = a2.charAt(i38);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c16 |= (charAt9 & 8191) << i39;
                        i39 += 13;
                        i38 = i22;
                    }
                    c4 = (charAt9 << i39) | c16;
                } else {
                    i22 = i38;
                }
                int i40 = i22 + 1;
                c3 = a2.charAt(i22);
                if (c3 >= 55296) {
                    char c17 = c3 & 8191;
                    int i41 = 13;
                    while (true) {
                        i27 = i40 + 1;
                        charAt8 = a2.charAt(i40);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c17 |= (charAt8 & 8191) << i41;
                        i41 += 13;
                        i40 = i27;
                    }
                    c3 = (charAt8 << i41) | c17;
                    i40 = i27;
                }
                int i42 = i40 + 1;
                c6 = a2.charAt(i40);
                if (c6 >= 55296) {
                    char c18 = c6 & 8191;
                    int i43 = 13;
                    while (true) {
                        i26 = i42 + 1;
                        charAt7 = a2.charAt(i42);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c18 |= (charAt7 & 8191) << i43;
                        i43 += 13;
                        i42 = i26;
                    }
                    c6 = c18 | (charAt7 << i43);
                    i42 = i26;
                }
                int i44 = i42 + 1;
                char charAt20 = a2.charAt(i42);
                if (charAt20 >= 55296) {
                    int i45 = 13;
                    int i46 = i44;
                    char c19 = charAt20 & 8191;
                    int i47 = i46;
                    while (true) {
                        i25 = i47 + 1;
                        charAt6 = a2.charAt(i47);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c19 |= (charAt6 & 8191) << i45;
                        i45 += 13;
                        i47 = i25;
                    }
                    charAt20 = c19 | (charAt6 << i45);
                    i23 = i25;
                } else {
                    i23 = i44;
                }
                int i48 = i23 + 1;
                c10 = a2.charAt(i23);
                if (c10 >= 55296) {
                    int i49 = 13;
                    int i50 = i48;
                    char c20 = c10 & 8191;
                    int i51 = i50;
                    while (true) {
                        i24 = i51 + 1;
                        charAt5 = a2.charAt(i51);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c20 |= (charAt5 & 8191) << i49;
                        i49 += 13;
                        i51 = i24;
                    }
                    c10 = c20 | (charAt5 << i49);
                    i48 = i24;
                }
                iArr = new int[(c10 + c6 + charAt20)];
                i4 = (charAt17 << 1) + charAt18;
                int i52 = i48;
                c2 = charAt17;
                c5 = charAt19;
                i3 = i52;
            }
            Unsafe unsafe = r;
            Object[] b2 = dp2.b();
            Class<?> cls3 = dp2.zzc().getClass();
            int i53 = i4;
            int[] iArr2 = new int[(c3 * 3)];
            Object[] objArr = new Object[(c3 << 1)];
            int i54 = c10 + c6;
            char c21 = c10;
            int i55 = i54;
            int i56 = 0;
            int i57 = 0;
            while (i3 < length) {
                int i58 = i3 + 1;
                char charAt21 = a2.charAt(i3);
                char c22 = 55296;
                if (charAt21 >= 55296) {
                    int i59 = 13;
                    int i60 = i58;
                    char c23 = charAt21 & 8191;
                    int i61 = i60;
                    while (true) {
                        i18 = i61 + 1;
                        charAt4 = a2.charAt(i61);
                        if (charAt4 < c22) {
                            break;
                        }
                        c23 |= (charAt4 & 8191) << i59;
                        i59 += 13;
                        i61 = i18;
                        c22 = 55296;
                    }
                    charAt21 = c23 | (charAt4 << i59);
                    i5 = i18;
                } else {
                    i5 = i58;
                }
                int i62 = i5 + 1;
                char charAt22 = a2.charAt(i5);
                int i63 = length;
                char c24 = 55296;
                if (charAt22 >= 55296) {
                    int i64 = 13;
                    int i65 = i62;
                    char c25 = charAt22 & 8191;
                    int i66 = i65;
                    while (true) {
                        i17 = i66 + 1;
                        charAt3 = a2.charAt(i66);
                        if (charAt3 < c24) {
                            break;
                        }
                        c25 |= (charAt3 & 8191) << i64;
                        i64 += 13;
                        i66 = i17;
                        c24 = 55296;
                    }
                    charAt22 = c25 | (charAt3 << i64);
                    i6 = i17;
                } else {
                    i6 = i62;
                }
                char c26 = c10;
                char c27 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i56] = i57;
                    i56++;
                }
                int i67 = i56;
                if (c27 >= '3') {
                    int i68 = i6 + 1;
                    char charAt23 = a2.charAt(i6);
                    char c28 = 55296;
                    if (charAt23 >= 55296) {
                        char c29 = charAt23 & 8191;
                        int i69 = 13;
                        while (true) {
                            i16 = i68 + 1;
                            charAt2 = a2.charAt(i68);
                            if (charAt2 < c28) {
                                break;
                            }
                            c29 |= (charAt2 & 8191) << i69;
                            i69 += 13;
                            i68 = i16;
                            c28 = 55296;
                        }
                        charAt23 = c29 | (charAt2 << i69);
                        i68 = i16;
                    }
                    int i70 = c27 - '3';
                    int i71 = i68;
                    if (i70 == 9 || i70 == 17) {
                        c9 = 1;
                        objArr[((i57 / 3) << 1) + 1] = b2[i53];
                        i53++;
                    } else {
                        if (i70 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i57 / 3) << 1) + 1] = b2[i53];
                            i53++;
                        }
                        c9 = 1;
                    }
                    int i72 = charAt23 << c9;
                    Object obj = b2[i72];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = a(cls3, (String) obj);
                        b2[i72] = field2;
                    }
                    char c30 = c5;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i73 = i72 + 1;
                    Object obj2 = b2[i73];
                    int i74 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = a(cls3, (String) obj2);
                        b2[i73] = field3;
                    }
                    str = a2;
                    i10 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i7 = i53;
                    i9 = i74;
                    i11 = 0;
                    c7 = c30;
                    c8 = c4;
                    i8 = charAt21;
                    i13 = i71;
                } else {
                    char c31 = c5;
                    int i75 = i53 + 1;
                    Field a3 = a(cls3, (String) b2[i53]);
                    c8 = c4;
                    if (c27 == 9 || c27 == 17) {
                        c7 = c31;
                        objArr[((i57 / 3) << 1) + 1] = a3.getType();
                    } else {
                        if (c27 == 27 || c27 == '1') {
                            c7 = c31;
                            i15 = i75 + 1;
                            objArr[((i57 / 3) << 1) + 1] = b2[i75];
                        } else if (c27 == 12 || c27 == 30 || c27 == ',') {
                            c7 = c31;
                            if ((charAt15 & 1) == 1) {
                                i15 = i75 + 1;
                                objArr[((i57 / 3) << 1) + 1] = b2[i75];
                            }
                        } else if (c27 == '2') {
                            int i76 = c21 + 1;
                            iArr[c21] = i57;
                            int i77 = (i57 / 3) << 1;
                            int i78 = i75 + 1;
                            objArr[i77] = b2[i75];
                            if ((charAt22 & 2048) != 0) {
                                i75 = i78 + 1;
                                objArr[i77 + 1] = b2[i78];
                                c7 = c31;
                                c21 = i76;
                            } else {
                                c21 = i76;
                                i75 = i78;
                                c7 = c31;
                            }
                        } else {
                            c7 = c31;
                        }
                        i8 = charAt21;
                        i75 = i15;
                        i9 = (int) unsafe.objectFieldOffset(a3);
                        if ((charAt15 & 1) != 1 || c27 > 17) {
                            str = a2;
                            cls2 = cls3;
                            i7 = i75;
                            i12 = i6;
                            i11 = 0;
                            i10 = 0;
                        } else {
                            i12 = i6 + 1;
                            char charAt24 = a2.charAt(i6);
                            if (charAt24 >= 55296) {
                                char c32 = charAt24 & 8191;
                                int i79 = 13;
                                while (true) {
                                    i14 = i12 + 1;
                                    charAt = a2.charAt(i12);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c32 |= (charAt & 8191) << i79;
                                    i79 += 13;
                                    i12 = i14;
                                }
                                charAt24 = c32 | (charAt << i79);
                                i12 = i14;
                            }
                            int i80 = (c2 << 1) + (charAt24 / ' ');
                            Object obj3 = b2[i80];
                            str = a2;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = a(cls3, (String) obj3);
                                b2[i80] = field;
                            }
                            cls2 = cls3;
                            i7 = i75;
                            i10 = (int) unsafe.objectFieldOffset(field);
                            i11 = charAt24 % ' ';
                        }
                        if (c27 >= 18 && c27 <= '1') {
                            iArr[i55] = i9;
                            i55++;
                        }
                        i13 = i12;
                    }
                    i8 = charAt21;
                    i9 = (int) unsafe.objectFieldOffset(a3);
                    if ((charAt15 & 1) != 1 || c27 > 17) {
                    }
                    iArr[i55] = i9;
                    i55++;
                    i13 = i12;
                }
                int i81 = i57 + 1;
                iArr2[i57] = i8;
                int i82 = i81 + 1;
                iArr2[i81] = (c27 << 20) | ((charAt22 & 256) != 0 ? 268435456 : 0) | ((charAt22 & 512) != 0 ? 536870912 : 0) | i9;
                i57 = i82 + 1;
                iArr2[i82] = (i11 << 20) | i10;
                cls3 = cls2;
                c4 = c8;
                c10 = c26;
                i53 = i7;
                length = i63;
                z = z2;
                c5 = c7;
                i56 = i67;
                a2 = str;
            }
            boolean z3 = z;
            return new uo2(iArr2, objArr, c5, c4, dp2.zzc(), z, false, iArr, c10, i54, xo2, ao2, wp2, um2, ko2);
        }
        ((tp2) po22).zza();
        throw null;
    }

    @DexIgnore
    public static <T> boolean f(T t, long j2) {
        return ((Boolean) cq2.f(t, j2)).booleanValue();
    }

    @DexIgnore
    public final void b(T t, T t2, int i2) {
        int d2 = d(i2);
        int i3 = this.a[i2];
        long j2 = (long) (d2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = cq2.f(t, j2);
            Object f3 = cq2.f(t2, j2);
            if (f2 != null && f3 != null) {
                cq2.a((Object) t, j2, hn2.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                cq2.a((Object) t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    @DexIgnore
    public final mn2 c(int i2) {
        return (mn2) this.b[((i2 / 3) << 1) + 1];
    }

    @DexIgnore
    public final int d(int i2) {
        return this.a[i2 + 1];
    }

    @DexIgnore
    public final int e(int i2) {
        return this.a[i2 + 2];
    }

    @DexIgnore
    public final T zza() {
        return this.l.zza(this.e);
    }

    @DexIgnore
    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.a.length; i2 += 3) {
                int d2 = d(i2);
                long j2 = (long) (1048575 & d2);
                int i3 = this.a[i2];
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.e(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 1:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.d(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 2:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 3:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 4:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 5:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 6:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 7:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.c(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 8:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.f(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 9:
                        a(t, t2, i2);
                        break;
                    case 10:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.f(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 11:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 12:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 13:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 14:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 15:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.a((Object) t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 16:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.b(t2, j2));
                            b(t, i2);
                            break;
                        }
                    case 17:
                        a(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.m.a(t, t2, j2);
                        break;
                    case 50:
                        gp2.a(this.p, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 60:
                        b(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            cq2.a((Object) t, j2, cq2.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 68:
                        b(t, t2, i2);
                        break;
                }
            }
            if (!this.g) {
                gp2.a(this.n, t, t2);
                if (this.f) {
                    gp2.a(this.o, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void zzc(T t) {
        int i2;
        int i3 = this.j;
        while (true) {
            i2 = this.k;
            if (i3 >= i2) {
                break;
            }
            long d2 = (long) (d(this.i[i3]) & 1048575);
            Object f2 = cq2.f(t, d2);
            if (f2 != null) {
                this.p.zzd(f2);
                cq2.a((Object) t, d2, f2);
            }
            i3++;
        }
        int length = this.i.length;
        while (i2 < length) {
            this.m.a(t, (long) this.i[i2]);
            i2++;
        }
        this.n.b(t);
        if (this.f) {
            this.o.c(t);
        }
    }

    @DexIgnore
    public final boolean zzd(T t) {
        int i2;
        int i3 = 0;
        int i4 = -1;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i3 >= this.j) {
                return !this.f || this.o.a((Object) t).e();
            }
            int i6 = this.i[i3];
            int i7 = this.a[i6];
            int d2 = d(i6);
            if (!this.g) {
                int i8 = this.a[i6 + 2];
                int i9 = i8 & 1048575;
                i2 = 1 << (i8 >>> 20);
                if (i9 != i4) {
                    i5 = r.getInt(t, (long) i9);
                    i4 = i9;
                }
            } else {
                i2 = 0;
            }
            if (((268435456 & d2) != 0) && !a(t, i6, i5, i2)) {
                return false;
            }
            int i10 = (267386880 & d2) >>> 20;
            if (i10 != 9 && i10 != 17) {
                if (i10 != 27) {
                    if (i10 == 60 || i10 == 68) {
                        if (a(t, i7, i6) && !a((Object) t, d2, a(i6))) {
                            return false;
                        }
                    } else if (i10 != 49) {
                        if (i10 == 50 && !this.p.zzb(cq2.f(t, (long) (d2 & 1048575))).isEmpty()) {
                            this.p.a(b(i6));
                            throw null;
                        }
                    }
                }
                List list = (List) cq2.f(t, (long) (d2 & 1048575));
                if (!list.isEmpty()) {
                    fp2 a2 = a(i6);
                    int i11 = 0;
                    while (true) {
                        if (i11 >= list.size()) {
                            break;
                        } else if (!a2.zzd(list.get(i11))) {
                            z = false;
                            break;
                        } else {
                            i11++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (a(t, i6, i5, i2) && !a((Object) t, d2, a(i6))) {
                return false;
            }
            i3++;
        }
    }

    @DexIgnore
    public static <T> float c(T t, long j2) {
        return ((Float) cq2.f(t, j2)).floatValue();
    }

    @DexIgnore
    public static <T> int d(T t, long j2) {
        return ((Integer) cq2.f(t, j2)).intValue();
    }

    @DexIgnore
    public static <T> long e(T t, long j2) {
        return ((Long) cq2.f(t, j2)).longValue();
    }

    @DexIgnore
    public final int f(int i2) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return b(i2, 0);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.fossil.gp2.a(com.fossil.cq2.f(r10, r6), com.fossil.cq2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.fossil.cq2.b(r10, r6) == com.fossil.cq2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.fossil.cq2.b(r10, r6) == com.fossil.cq2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.fossil.gp2.a(com.fossil.cq2.f(r10, r6), com.fossil.cq2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.fossil.gp2.a(com.fossil.cq2.f(r10, r6), com.fossil.cq2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.fossil.gp2.a(com.fossil.cq2.f(r10, r6), com.fossil.cq2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.fossil.cq2.c(r10, r6) == com.fossil.cq2.c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.fossil.cq2.b(r10, r6) == com.fossil.cq2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.fossil.cq2.a((java.lang.Object) r10, r6) == com.fossil.cq2.a((java.lang.Object) r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.fossil.cq2.b(r10, r6) == com.fossil.cq2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.fossil.cq2.b(r10, r6) == com.fossil.cq2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.fossil.cq2.d(r10, r6)) == java.lang.Float.floatToIntBits(com.fossil.cq2.d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.fossil.cq2.e(r10, r6)) == java.lang.Double.doubleToLongBits(com.fossil.cq2.e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c1, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.fossil.gp2.a(com.fossil.cq2.f(r10, r6), com.fossil.cq2.f(r11, r6)) != false) goto L_0x01c2;
     */
    @DexIgnore
    public final boolean zza(T t, T t2) {
        int length = this.a.length;
        int i2 = 0;
        while (true) {
            boolean z = true;
            if (i2 < length) {
                int d2 = d(i2);
                long j2 = (long) (d2 & 1048575);
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 1:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 2:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 3:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 4:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 5:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 6:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 7:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 8:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 9:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 10:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 11:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 12:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 13:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 14:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 15:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 16:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 17:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = gp2.a(cq2.f(t, j2), cq2.f(t2, j2));
                        break;
                    case 50:
                        z = gp2.a(cq2.f(t, j2), cq2.f(t2, j2));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long e2 = (long) (e(i2) & 1048575);
                        if (cq2.a((Object) t, e2) == cq2.a((Object) t2, e2)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i2 += 3;
            } else if (!this.n.a(t).equals(this.n.a(t2))) {
                return false;
            } else {
                if (this.f) {
                    return this.o.a((Object) t).equals(this.o.a((Object) t2));
                }
                return true;
            }
        }
    }

    @DexIgnore
    public final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:167:0x046b  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0471  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    public final void b(T t, tq2 tq2) throws IOException {
        Map.Entry entry;
        int length;
        int i2;
        int i3;
        int i4;
        T t2 = t;
        tq2 tq22 = tq2;
        if (this.f) {
            xm2<?> a2 = this.o.a((Object) t2);
            if (!a2.a.isEmpty()) {
                entry = a2.c().next();
                length = this.a.length;
                Unsafe unsafe = r;
                i2 = 0;
                int i5 = -1;
                int i6 = 0;
                while (i2 < length) {
                    int d2 = d(i2);
                    int[] iArr = this.a;
                    int i7 = iArr[i2];
                    int i8 = (267386880 & d2) >>> 20;
                    if (this.g || i8 > 17) {
                        i3 = i5;
                        i4 = 0;
                    } else {
                        int i9 = iArr[i2 + 2];
                        i3 = i9 & 1048575;
                        if (i3 != i5) {
                            i6 = unsafe.getInt(t2, (long) i3);
                        } else {
                            i3 = i5;
                        }
                        i4 = 1 << (i9 >>> 20);
                    }
                    if (entry == null) {
                        long j2 = (long) (d2 & 1048575);
                        switch (i8) {
                            case 0:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zza(i7, cq2.e(t2, j2));
                                    break;
                                }
                            case 1:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zza(i7, cq2.d(t2, j2));
                                    break;
                                }
                            case 2:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zza(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 3:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzc(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 4:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzc(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 5:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzd(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 6:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzd(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 7:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zza(i7, cq2.c(t2, j2));
                                    break;
                                }
                            case 8:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    a(i7, unsafe.getObject(t2, j2), tq22);
                                    break;
                                }
                            case 9:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.a(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 10:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.a(i7, (yl2) unsafe.getObject(t2, j2));
                                    break;
                                }
                            case 11:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zze(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 12:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzb(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 13:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zza(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 14:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzb(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 15:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zzf(i7, unsafe.getInt(t2, j2));
                                    break;
                                }
                            case 16:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.zze(i7, unsafe.getLong(t2, j2));
                                    break;
                                }
                            case 17:
                                if ((i6 & i4) == 0) {
                                    break;
                                } else {
                                    tq22.b(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 18:
                                gp2.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 19:
                                gp2.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 20:
                                gp2.c(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 21:
                                gp2.d(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 22:
                                gp2.h(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 23:
                                gp2.f(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 24:
                                gp2.k(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 25:
                                gp2.n(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 26:
                                gp2.a(this.a[i2], (List<String>) (List) unsafe.getObject(t2, j2), tq22);
                                break;
                            case 27:
                                gp2.a(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), tq22, a(i2));
                                break;
                            case 28:
                                gp2.b(this.a[i2], (List<yl2>) (List) unsafe.getObject(t2, j2), tq22);
                                break;
                            case 29:
                                gp2.i(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 30:
                                gp2.m(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 31:
                                gp2.l(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 32:
                                gp2.g(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 33:
                                gp2.j(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 34:
                                gp2.e(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, false);
                                break;
                            case 35:
                                gp2.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 36:
                                gp2.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 37:
                                gp2.c(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 38:
                                gp2.d(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 39:
                                gp2.h(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 40:
                                gp2.f(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 41:
                                gp2.k(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 42:
                                gp2.n(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 43:
                                gp2.i(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 44:
                                gp2.m(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 45:
                                gp2.l(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 46:
                                gp2.g(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 47:
                                gp2.j(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 48:
                                gp2.e(this.a[i2], (List) unsafe.getObject(t2, j2), tq22, true);
                                break;
                            case 49:
                                gp2.b(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), tq22, a(i2));
                                break;
                            case 50:
                                a(tq22, i7, unsafe.getObject(t2, j2), i2);
                                break;
                            case 51:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zza(i7, b(t2, j2));
                                    break;
                                }
                            case 52:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zza(i7, c(t2, j2));
                                    break;
                                }
                            case 53:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zza(i7, e(t2, j2));
                                    break;
                                }
                            case 54:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzc(i7, e(t2, j2));
                                    break;
                                }
                            case 55:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzc(i7, d(t2, j2));
                                    break;
                                }
                            case 56:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzd(i7, e(t2, j2));
                                    break;
                                }
                            case 57:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzd(i7, d(t2, j2));
                                    break;
                                }
                            case 58:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zza(i7, f(t2, j2));
                                    break;
                                }
                            case 59:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    a(i7, unsafe.getObject(t2, j2), tq22);
                                    break;
                                }
                            case 60:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.a(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                            case 61:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.a(i7, (yl2) unsafe.getObject(t2, j2));
                                    break;
                                }
                            case 62:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zze(i7, d(t2, j2));
                                    break;
                                }
                            case 63:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzb(i7, d(t2, j2));
                                    break;
                                }
                            case 64:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zza(i7, d(t2, j2));
                                    break;
                                }
                            case 65:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzb(i7, e(t2, j2));
                                    break;
                                }
                            case 66:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zzf(i7, d(t2, j2));
                                    break;
                                }
                            case 67:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.zze(i7, e(t2, j2));
                                    break;
                                }
                            case 68:
                                if (!a(t2, i7, i2)) {
                                    break;
                                } else {
                                    tq22.b(i7, unsafe.getObject(t2, j2), a(i2));
                                    break;
                                }
                        }
                        i2 += 3;
                        i5 = i3;
                    } else {
                        this.o.a((Map.Entry<?, ?>) entry);
                        throw null;
                    }
                }
                if (entry != null) {
                    a(this.n, t2, tq22);
                    return;
                } else {
                    this.o.a(tq22, entry);
                    throw null;
                }
            }
        }
        entry = null;
        length = this.a.length;
        Unsafe unsafe2 = r;
        i2 = 0;
        int i52 = -1;
        int i62 = 0;
        while (i2 < length) {
        }
        if (entry != null) {
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01c3, code lost:
        r2 = (r2 * 53) + r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0227, code lost:
        r2 = r2 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0228, code lost:
        r1 = r1 + 3;
     */
    @DexIgnore
    public final int zza(T t) {
        int i2;
        int i3;
        int length = this.a.length;
        int i4 = 0;
        int i5 = 0;
        while (i4 < length) {
            int d2 = d(i4);
            int i6 = this.a[i4];
            long j2 = (long) (1048575 & d2);
            int i7 = 37;
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    i3 = i5 * 53;
                    i2 = hn2.a(Double.doubleToLongBits(cq2.e(t, j2)));
                case 1:
                    i3 = i5 * 53;
                    i2 = Float.floatToIntBits(cq2.d(t, j2));
                case 2:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.b(t, j2));
                case 3:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.b(t, j2));
                case 4:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 5:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.b(t, j2));
                case 6:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 7:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.c(t, j2));
                case 8:
                    i3 = i5 * 53;
                    i2 = ((String) cq2.f(t, j2)).hashCode();
                case 9:
                    Object f2 = cq2.f(t, j2);
                    if (f2 != null) {
                        i7 = f2.hashCode();
                        break;
                    }
                    break;
                case 10:
                    i3 = i5 * 53;
                    i2 = cq2.f(t, j2).hashCode();
                case 11:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 12:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 13:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 14:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.b(t, j2));
                case 15:
                    i3 = i5 * 53;
                    i2 = cq2.a((Object) t, j2);
                case 16:
                    i3 = i5 * 53;
                    i2 = hn2.a(cq2.b(t, j2));
                case 17:
                    Object f3 = cq2.f(t, j2);
                    if (f3 != null) {
                        i7 = f3.hashCode();
                        break;
                    }
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i5 * 53;
                    i2 = cq2.f(t, j2).hashCode();
                case 50:
                    i3 = i5 * 53;
                    i2 = cq2.f(t, j2).hashCode();
                case 51:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(Double.doubleToLongBits(b(t, j2)));
                    }
                case 52:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = Float.floatToIntBits(c(t, j2));
                    }
                case 53:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(e(t, j2));
                    }
                case 54:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(e(t, j2));
                    }
                case 55:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 56:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(e(t, j2));
                    }
                case 57:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 58:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(f(t, j2));
                    }
                case 59:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = ((String) cq2.f(t, j2)).hashCode();
                    }
                case 60:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = cq2.f(t, j2).hashCode();
                    }
                case 61:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = cq2.f(t, j2).hashCode();
                    }
                case 62:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 63:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 64:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 65:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(e(t, j2));
                    }
                case 66:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = d(t, j2);
                    }
                case 67:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = hn2.a(e(t, j2));
                    }
                case 68:
                    if (!a(t, i6, i4)) {
                        break;
                    } else {
                        i3 = i5 * 53;
                        i2 = cq2.f(t, j2).hashCode();
                    }
            }
        }
        int hashCode = (i5 * 53) + this.n.a(t).hashCode();
        return this.f ? (hashCode * 53) + this.o.a((Object) t).hashCode() : hashCode;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:398:0x0833, code lost:
        r9 = (r9 + r10) + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:417:0x090b, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:457:0x09be, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:470:0x09f8, code lost:
        r5 = r5 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:479:0x0a18, code lost:
        r3 = r3 + 3;
        r9 = r13;
     */
    @DexIgnore
    public final int zzb(T t) {
        int i2;
        int i3;
        long j2;
        int i4;
        int i5;
        int b2;
        int i6;
        int i7;
        int i8;
        int b3;
        int i9;
        int i10;
        int i11;
        T t2 = t;
        int i12 = 267386880;
        if (this.g) {
            Unsafe unsafe = r;
            int i13 = 0;
            int i14 = 0;
            while (i13 < this.a.length) {
                int d2 = d(i13);
                int i15 = (d2 & i12) >>> 20;
                int i16 = this.a[i13];
                long j3 = (long) (d2 & 1048575);
                int i17 = (i15 < ym2.zza.zza() || i15 > ym2.zzb.zza()) ? 0 : this.a[i13 + 2] & 1048575;
                switch (i15) {
                    case 0:
                        if (a(t2, i13)) {
                            b3 = pm2.b(i16, 0.0d);
                        } else {
                            continue;
                        }
                    case 1:
                        if (a(t2, i13)) {
                            b3 = pm2.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            continue;
                        }
                    case 2:
                        if (a(t2, i13)) {
                            b3 = pm2.d(i16, cq2.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 3:
                        if (a(t2, i13)) {
                            b3 = pm2.e(i16, cq2.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 4:
                        if (a(t2, i13)) {
                            b3 = pm2.f(i16, cq2.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 5:
                        if (a(t2, i13)) {
                            b3 = pm2.g(i16, 0);
                        } else {
                            continue;
                        }
                    case 6:
                        if (a(t2, i13)) {
                            b3 = pm2.i(i16, 0);
                        } else {
                            continue;
                        }
                    case 7:
                        if (a(t2, i13)) {
                            b3 = pm2.b(i16, true);
                        } else {
                            continue;
                        }
                    case 8:
                        if (a(t2, i13)) {
                            Object f2 = cq2.f(t2, j3);
                            if (f2 instanceof yl2) {
                                b3 = pm2.c(i16, (yl2) f2);
                            } else {
                                b3 = pm2.b(i16, (String) f2);
                            }
                        } else {
                            continue;
                        }
                    case 9:
                        if (a(t2, i13)) {
                            b3 = gp2.a(i16, cq2.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 10:
                        if (a(t2, i13)) {
                            b3 = pm2.c(i16, (yl2) cq2.f(t2, j3));
                        } else {
                            continue;
                        }
                    case 11:
                        if (a(t2, i13)) {
                            b3 = pm2.g(i16, cq2.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 12:
                        if (a(t2, i13)) {
                            b3 = pm2.k(i16, cq2.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 13:
                        if (a(t2, i13)) {
                            b3 = pm2.j(i16, 0);
                        } else {
                            continue;
                        }
                    case 14:
                        if (a(t2, i13)) {
                            b3 = pm2.h(i16, 0);
                        } else {
                            continue;
                        }
                    case 15:
                        if (a(t2, i13)) {
                            b3 = pm2.h(i16, cq2.a((Object) t2, j3));
                        } else {
                            continue;
                        }
                    case 16:
                        if (a(t2, i13)) {
                            b3 = pm2.f(i16, cq2.b(t2, j3));
                        } else {
                            continue;
                        }
                    case 17:
                        if (a(t2, i13)) {
                            b3 = pm2.c(i16, (ro2) cq2.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 18:
                        b3 = gp2.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 19:
                        b3 = gp2.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 20:
                        b3 = gp2.a(i16, (List<Long>) a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 21:
                        b3 = gp2.b(i16, (List<Long>) a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 22:
                        b3 = gp2.e(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 23:
                        b3 = gp2.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 24:
                        b3 = gp2.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 25:
                        b3 = gp2.j(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 26:
                        b3 = gp2.a(i16, a((Object) t2, j3));
                        i14 += b3;
                        break;
                    case 27:
                        b3 = gp2.a(i16, a((Object) t2, j3), a(i13));
                        i14 += b3;
                        break;
                    case 28:
                        b3 = gp2.b(i16, a((Object) t2, j3));
                        i14 += b3;
                        break;
                    case 29:
                        b3 = gp2.f(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 30:
                        b3 = gp2.d(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 31:
                        b3 = gp2.h(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 32:
                        b3 = gp2.i(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 33:
                        b3 = gp2.g(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 34:
                        b3 = gp2.c(i16, a((Object) t2, j3), false);
                        i14 += b3;
                        break;
                    case 35:
                        i10 = gp2.i((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 36:
                        i10 = gp2.h((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 37:
                        i10 = gp2.a((List<Long>) (List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 38:
                        i10 = gp2.b((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 39:
                        i10 = gp2.e((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 40:
                        i10 = gp2.i((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 41:
                        i10 = gp2.h((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 42:
                        i10 = gp2.j((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 43:
                        i10 = gp2.f((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 44:
                        i10 = gp2.d((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 45:
                        i10 = gp2.h((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 46:
                        i10 = gp2.i((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 47:
                        i10 = gp2.g((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 48:
                        i10 = gp2.c((List) unsafe.getObject(t2, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t2, (long) i17, i10);
                            }
                            i11 = pm2.e(i16);
                            i9 = pm2.g(i10);
                            break;
                        } else {
                            continue;
                        }
                    case 49:
                        b3 = gp2.b(i16, (List<ro2>) a((Object) t2, j3), a(i13));
                        i14 += b3;
                        break;
                    case 50:
                        b3 = this.p.zza(i16, cq2.f(t2, j3), b(i13));
                        i14 += b3;
                        break;
                    case 51:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.b(i16, 0.0d);
                        } else {
                            continue;
                        }
                    case 52:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            continue;
                        }
                    case 53:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.d(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 54:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.e(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 55:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.f(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 56:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.g(i16, 0);
                        } else {
                            continue;
                        }
                    case 57:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.i(i16, 0);
                        } else {
                            continue;
                        }
                    case 58:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.b(i16, true);
                        } else {
                            continue;
                        }
                    case 59:
                        if (a(t2, i16, i13)) {
                            Object f3 = cq2.f(t2, j3);
                            if (f3 instanceof yl2) {
                                b3 = pm2.c(i16, (yl2) f3);
                            } else {
                                b3 = pm2.b(i16, (String) f3);
                            }
                        } else {
                            continue;
                        }
                    case 60:
                        if (a(t2, i16, i13)) {
                            b3 = gp2.a(i16, cq2.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                    case 61:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.c(i16, (yl2) cq2.f(t2, j3));
                        } else {
                            continue;
                        }
                    case 62:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.g(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 63:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.k(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 64:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.j(i16, 0);
                        } else {
                            continue;
                        }
                    case 65:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.h(i16, 0);
                        } else {
                            continue;
                        }
                    case 66:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.h(i16, d(t2, j3));
                        } else {
                            continue;
                        }
                    case 67:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.f(i16, e(t2, j3));
                        } else {
                            continue;
                        }
                    case 68:
                        if (a(t2, i16, i13)) {
                            b3 = pm2.c(i16, (ro2) cq2.f(t2, j3), a(i13));
                        } else {
                            continue;
                        }
                }
                b3 = i11 + i9 + i10;
                i14 += b3;
                i13 += 3;
                i12 = 267386880;
            }
            return i14 + a(this.n, t2);
        }
        Unsafe unsafe2 = r;
        int i18 = 0;
        int i19 = 0;
        int i20 = -1;
        int i21 = 0;
        while (i18 < this.a.length) {
            int d3 = d(i18);
            int[] iArr = this.a;
            int i22 = iArr[i18];
            int i23 = (d3 & 267386880) >>> 20;
            if (i23 <= 17) {
                i3 = iArr[i18 + 2];
                int i24 = i3 & 1048575;
                i2 = 1 << (i3 >>> 20);
                if (i24 != i20) {
                    i21 = unsafe2.getInt(t2, (long) i24);
                } else {
                    i24 = i20;
                }
                i20 = i24;
            } else {
                i3 = (!this.h || i23 < ym2.zza.zza() || i23 > ym2.zzb.zza()) ? 0 : this.a[i18 + 2] & 1048575;
                i2 = 0;
            }
            long j4 = (long) (d3 & 1048575);
            switch (i23) {
                case 0:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i19 += pm2.b(i22, 0.0d);
                        break;
                    }
                case 1:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i19 += pm2.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        break;
                    }
                case 2:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = pm2.d(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 3:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = pm2.e(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 4:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = pm2.f(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 5:
                    j2 = 0;
                    if ((i21 & i2) != 0) {
                        i4 = pm2.g(i22, 0);
                    }
                    break;
                case 6:
                    if ((i21 & i2) != 0) {
                        i5 = pm2.i(i22, 0);
                    }
                    break;
                case 7:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.b(i22, true);
                    }
                    break;
                case 8:
                    if ((i21 & i2) != 0) {
                        Object object = unsafe2.getObject(t2, j4);
                        if (object instanceof yl2) {
                            b2 = pm2.c(i22, (yl2) object);
                        } else {
                            b2 = pm2.b(i22, (String) object);
                        }
                    }
                    break;
                case 9:
                    if ((i21 & i2) != 0) {
                        b2 = gp2.a(i22, unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 10:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.c(i22, (yl2) unsafe2.getObject(t2, j4));
                    }
                    break;
                case 11:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.g(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 12:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.k(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 13:
                    if ((i21 & i2) != 0) {
                        i5 = pm2.j(i22, 0);
                    }
                    break;
                case 14:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.h(i22, 0);
                    }
                    break;
                case 15:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.h(i22, unsafe2.getInt(t2, j4));
                    }
                    break;
                case 16:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.f(i22, unsafe2.getLong(t2, j4));
                    }
                    break;
                case 17:
                    if ((i21 & i2) != 0) {
                        b2 = pm2.c(i22, (ro2) unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 18:
                    b2 = gp2.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 19:
                    b2 = gp2.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 20:
                    b2 = gp2.a(i22, (List<Long>) (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 21:
                    b2 = gp2.b(i22, (List<Long>) (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 22:
                    b2 = gp2.e(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 23:
                    b2 = gp2.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 24:
                    b2 = gp2.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 25:
                    b2 = gp2.j(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 26:
                    b2 = gp2.a(i22, (List<?>) (List) unsafe2.getObject(t2, j4));
                    i19 += b2;
                    break;
                case 27:
                    b2 = gp2.a(i22, (List<?>) (List) unsafe2.getObject(t2, j4), a(i18));
                    i19 += b2;
                    break;
                case 28:
                    b2 = gp2.b(i22, (List) unsafe2.getObject(t2, j4));
                    i19 += b2;
                    break;
                case 29:
                    b2 = gp2.f(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 30:
                    b2 = gp2.d(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 31:
                    b2 = gp2.h(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 32:
                    b2 = gp2.i(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 33:
                    b2 = gp2.g(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 34:
                    b2 = gp2.c(i22, (List) unsafe2.getObject(t2, j4), false);
                    i19 += b2;
                    break;
                case 35:
                    i8 = gp2.i((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 36:
                    i8 = gp2.h((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 37:
                    i8 = gp2.a((List<Long>) (List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 38:
                    i8 = gp2.b((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 39:
                    i8 = gp2.e((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 40:
                    i8 = gp2.i((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 41:
                    i8 = gp2.h((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 42:
                    i8 = gp2.j((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 43:
                    i8 = gp2.f((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 44:
                    i8 = gp2.d((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 45:
                    i8 = gp2.h((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 46:
                    i8 = gp2.i((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 47:
                    i8 = gp2.g((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 48:
                    i8 = gp2.c((List) unsafe2.getObject(t2, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t2, (long) i3, i8);
                        }
                        i7 = pm2.e(i22);
                        i6 = pm2.g(i8);
                        break;
                    }
                    break;
                case 49:
                    b2 = gp2.b(i22, (List<ro2>) (List) unsafe2.getObject(t2, j4), a(i18));
                    i19 += b2;
                    break;
                case 50:
                    b2 = this.p.zza(i22, unsafe2.getObject(t2, j4), b(i18));
                    i19 += b2;
                    break;
                case 51:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.b(i22, 0.0d);
                    }
                    break;
                case 52:
                    if (a(t2, i22, i18)) {
                        i5 = pm2.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    break;
                case 53:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.d(i22, e(t2, j4));
                    }
                    break;
                case 54:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.e(i22, e(t2, j4));
                    }
                    break;
                case 55:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.f(i22, d(t2, j4));
                    }
                    break;
                case 56:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.g(i22, 0);
                    }
                    break;
                case 57:
                    if (a(t2, i22, i18)) {
                        i5 = pm2.i(i22, 0);
                    }
                    break;
                case 58:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.b(i22, true);
                    }
                    break;
                case 59:
                    if (a(t2, i22, i18)) {
                        Object object2 = unsafe2.getObject(t2, j4);
                        if (object2 instanceof yl2) {
                            b2 = pm2.c(i22, (yl2) object2);
                        } else {
                            b2 = pm2.b(i22, (String) object2);
                        }
                    }
                    break;
                case 60:
                    if (a(t2, i22, i18)) {
                        b2 = gp2.a(i22, unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
                case 61:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.c(i22, (yl2) unsafe2.getObject(t2, j4));
                    }
                    break;
                case 62:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.g(i22, d(t2, j4));
                    }
                    break;
                case 63:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.k(i22, d(t2, j4));
                    }
                    break;
                case 64:
                    if (a(t2, i22, i18)) {
                        i5 = pm2.j(i22, 0);
                    }
                    break;
                case 65:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.h(i22, 0);
                    }
                    break;
                case 66:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.h(i22, d(t2, j4));
                    }
                    break;
                case 67:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.f(i22, e(t2, j4));
                    }
                    break;
                case 68:
                    if (a(t2, i22, i18)) {
                        b2 = pm2.c(i22, (ro2) unsafe2.getObject(t2, j4), a(i18));
                    }
                    break;
            }
        }
        int a2 = i19 + a(this.n, t2);
        if (!this.f) {
            return a2;
        }
        xm2<?> a3 = this.o.a((Object) t2);
        if (a3.a.c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = a3.a.d().iterator();
            if (!it.hasNext()) {
                return a2 + 0;
            }
            Map.Entry next = it.next();
            xm2.b((zm2) next.getKey(), next.getValue());
            throw null;
        }
        Map.Entry<T, Object> a4 = a3.a.a(0);
        xm2.b((zm2) a4.getKey(), a4.getValue());
        throw null;
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @DexIgnore
    public final void a(T t, T t2, int i2) {
        long d2 = (long) (d(i2) & 1048575);
        if (a(t2, i2)) {
            Object f2 = cq2.f(t, d2);
            Object f3 = cq2.f(t2, d2);
            if (f2 != null && f3 != null) {
                cq2.a((Object) t, d2, hn2.a(f2, f3));
                b(t, i2);
            } else if (f3 != null) {
                cq2.a((Object) t, d2, f3);
                b(t, i2);
            }
        }
    }

    @DexIgnore
    public static <UT, UB> int a(wp2<UT, UB> wp2, T t) {
        return wp2.d(wp2.a(t));
    }

    @DexIgnore
    public static List<?> a(Object obj, long j2) {
        return (List) cq2.f(obj, j2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04bc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x04bd  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x096d  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0973  */
    public final void a(T t, tq2 tq2) throws IOException {
        Map.Entry entry;
        int length;
        int i2;
        Map.Entry entry2;
        int length2;
        if (tq2.zza() == fn2.e.l) {
            a(this.n, t, tq2);
            if (this.f) {
                xm2<?> a2 = this.o.a((Object) t);
                if (!a2.a.isEmpty()) {
                    entry2 = a2.d().next();
                    length2 = this.a.length - 3;
                    while (length2 >= 0) {
                        int d2 = d(length2);
                        int[] iArr = this.a;
                        int i3 = iArr[length2];
                        if (entry2 == null) {
                            switch ((d2 & 267386880) >>> 20) {
                                case 0:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, cq2.e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 1:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, cq2.d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 2:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, cq2.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 3:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i3, cq2.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 4:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 5:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i3, cq2.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 6:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 7:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, cq2.c(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 8:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        a(i3, cq2.f(t, (long) (d2 & 1048575)), tq2);
                                        break;
                                    }
                                case 9:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.a(i3, cq2.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 10:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.a(i3, (yl2) cq2.f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 11:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zze(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 12:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 13:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 14:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i3, cq2.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 15:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zzf(i3, cq2.a((Object) t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 16:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.zze(i3, cq2.b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 17:
                                    if (!a(t, length2)) {
                                        break;
                                    } else {
                                        tq2.b(i3, cq2.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 18:
                                    gp2.a(iArr[length2], (List<Double>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 19:
                                    gp2.b(iArr[length2], (List<Float>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 20:
                                    gp2.c(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 21:
                                    gp2.d(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 22:
                                    gp2.h(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 23:
                                    gp2.f(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 24:
                                    gp2.k(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 25:
                                    gp2.n(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 26:
                                    gp2.a(iArr[length2], (List<String>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2);
                                    break;
                                case 27:
                                    gp2.a(iArr[length2], (List<?>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, a(length2));
                                    break;
                                case 28:
                                    gp2.b(iArr[length2], (List<yl2>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2);
                                    break;
                                case 29:
                                    gp2.i(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 30:
                                    gp2.m(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 31:
                                    gp2.l(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 32:
                                    gp2.g(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 33:
                                    gp2.j(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 34:
                                    gp2.e(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, false);
                                    break;
                                case 35:
                                    gp2.a(iArr[length2], (List<Double>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 36:
                                    gp2.b(iArr[length2], (List<Float>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 37:
                                    gp2.c(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 38:
                                    gp2.d(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 39:
                                    gp2.h(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 40:
                                    gp2.f(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 41:
                                    gp2.k(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 42:
                                    gp2.n(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 43:
                                    gp2.i(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 44:
                                    gp2.m(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 45:
                                    gp2.l(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 46:
                                    gp2.g(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 47:
                                    gp2.j(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 48:
                                    gp2.e(iArr[length2], (List) cq2.f(t, (long) (d2 & 1048575)), tq2, true);
                                    break;
                                case 49:
                                    gp2.b(iArr[length2], (List<?>) (List) cq2.f(t, (long) (d2 & 1048575)), tq2, a(length2));
                                    break;
                                case 50:
                                    a(tq2, i3, cq2.f(t, (long) (d2 & 1048575)), length2);
                                    break;
                                case 51:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, b(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 52:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, c(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 53:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 54:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 55:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 56:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 57:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 58:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 59:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        a(i3, cq2.f(t, (long) (d2 & 1048575)), tq2);
                                        break;
                                    }
                                case 60:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.a(i3, cq2.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                                case 61:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.a(i3, (yl2) cq2.f(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 62:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zze(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 63:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 64:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zza(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 65:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 66:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zzf(i3, d(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 67:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.zze(i3, e(t, (long) (d2 & 1048575)));
                                        break;
                                    }
                                case 68:
                                    if (!a(t, i3, length2)) {
                                        break;
                                    } else {
                                        tq2.b(i3, cq2.f(t, (long) (d2 & 1048575)), a(length2));
                                        break;
                                    }
                            }
                            length2 -= 3;
                        } else {
                            this.o.a((Map.Entry<?, ?>) entry2);
                            throw null;
                        }
                    }
                    if (entry2 == null) {
                        this.o.a(tq2, entry2);
                        throw null;
                    }
                    return;
                }
            }
            entry2 = null;
            length2 = this.a.length - 3;
            while (length2 >= 0) {
            }
            if (entry2 == null) {
            }
        } else if (this.g) {
            if (this.f) {
                xm2<?> a3 = this.o.a((Object) t);
                if (!a3.a.isEmpty()) {
                    entry = a3.c().next();
                    length = this.a.length;
                    i2 = 0;
                    while (i2 < length) {
                        int d3 = d(i2);
                        int[] iArr2 = this.a;
                        int i4 = iArr2[i2];
                        if (entry == null) {
                            switch ((d3 & 267386880) >>> 20) {
                                case 0:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, cq2.e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 1:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, cq2.d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 2:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, cq2.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 3:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i4, cq2.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 4:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 5:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i4, cq2.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 6:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 7:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, cq2.c(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 8:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        a(i4, cq2.f(t, (long) (d3 & 1048575)), tq2);
                                        break;
                                    }
                                case 9:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.a(i4, cq2.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 10:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.a(i4, (yl2) cq2.f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 11:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zze(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 12:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 13:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 14:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i4, cq2.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 15:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zzf(i4, cq2.a((Object) t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 16:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.zze(i4, cq2.b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 17:
                                    if (!a(t, i2)) {
                                        break;
                                    } else {
                                        tq2.b(i4, cq2.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 18:
                                    gp2.a(iArr2[i2], (List<Double>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 19:
                                    gp2.b(iArr2[i2], (List<Float>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 20:
                                    gp2.c(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 21:
                                    gp2.d(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 22:
                                    gp2.h(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 23:
                                    gp2.f(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 24:
                                    gp2.k(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 25:
                                    gp2.n(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 26:
                                    gp2.a(iArr2[i2], (List<String>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2);
                                    break;
                                case 27:
                                    gp2.a(iArr2[i2], (List<?>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, a(i2));
                                    break;
                                case 28:
                                    gp2.b(iArr2[i2], (List<yl2>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2);
                                    break;
                                case 29:
                                    gp2.i(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 30:
                                    gp2.m(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 31:
                                    gp2.l(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 32:
                                    gp2.g(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 33:
                                    gp2.j(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 34:
                                    gp2.e(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, false);
                                    break;
                                case 35:
                                    gp2.a(iArr2[i2], (List<Double>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 36:
                                    gp2.b(iArr2[i2], (List<Float>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 37:
                                    gp2.c(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 38:
                                    gp2.d(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 39:
                                    gp2.h(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 40:
                                    gp2.f(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 41:
                                    gp2.k(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 42:
                                    gp2.n(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 43:
                                    gp2.i(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 44:
                                    gp2.m(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 45:
                                    gp2.l(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 46:
                                    gp2.g(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 47:
                                    gp2.j(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 48:
                                    gp2.e(iArr2[i2], (List) cq2.f(t, (long) (d3 & 1048575)), tq2, true);
                                    break;
                                case 49:
                                    gp2.b(iArr2[i2], (List<?>) (List) cq2.f(t, (long) (d3 & 1048575)), tq2, a(i2));
                                    break;
                                case 50:
                                    a(tq2, i4, cq2.f(t, (long) (d3 & 1048575)), i2);
                                    break;
                                case 51:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, b(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 52:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, c(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 53:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 54:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 55:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzc(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 56:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 57:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzd(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 58:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 59:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        a(i4, cq2.f(t, (long) (d3 & 1048575)), tq2);
                                        break;
                                    }
                                case 60:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.a(i4, cq2.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                                case 61:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.a(i4, (yl2) cq2.f(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 62:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zze(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 63:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 64:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zza(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 65:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzb(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 66:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zzf(i4, d(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 67:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.zze(i4, e(t, (long) (d3 & 1048575)));
                                        break;
                                    }
                                case 68:
                                    if (!a(t, i4, i2)) {
                                        break;
                                    } else {
                                        tq2.b(i4, cq2.f(t, (long) (d3 & 1048575)), a(i2));
                                        break;
                                    }
                            }
                            i2 += 3;
                        } else {
                            this.o.a((Map.Entry<?, ?>) entry);
                            throw null;
                        }
                    }
                    if (entry != null) {
                        a(this.n, t, tq2);
                        return;
                    } else {
                        this.o.a(tq2, entry);
                        throw null;
                    }
                }
            }
            entry = null;
            length = this.a.length;
            i2 = 0;
            while (i2 < length) {
            }
            if (entry != null) {
            }
        } else {
            b(t, tq2);
        }
    }

    @DexIgnore
    public final Object b(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    @DexIgnore
    public static <T> double b(T t, long j2) {
        return ((Double) cq2.f(t, j2)).doubleValue();
    }

    @DexIgnore
    public final void b(T t, int i2) {
        if (!this.g) {
            int e2 = e(i2);
            long j2 = (long) (e2 & 1048575);
            cq2.a((Object) t, j2, cq2.a((Object) t, j2) | (1 << (e2 >>> 20)));
        }
    }

    @DexIgnore
    public final void b(T t, int i2, int i3) {
        cq2.a((Object) t, (long) (e(i3) & 1048575), i2);
    }

    @DexIgnore
    public final int b(int i2, int i3) {
        int length = (this.a.length / 3) - 1;
        while (i3 <= length) {
            int i4 = (length + i3) >>> 1;
            int i5 = i4 * 3;
            int i6 = this.a[i5];
            if (i2 == i6) {
                return i5;
            }
            if (i2 < i6) {
                length = i4 - 1;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }

    @DexIgnore
    public final <K, V> void a(tq2 tq2, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            this.p.a(b(i3));
            throw null;
        }
    }

    @DexIgnore
    public static <UT, UB> void a(wp2<UT, UB> wp2, T t, tq2 tq2) throws IOException {
        wp2.a(wp2.a(t), tq2);
    }

    @DexIgnore
    public static zp2 a(Object obj) {
        fn2 fn2 = (fn2) obj;
        zp2 zp2 = fn2.zzb;
        if (zp2 != zp2.d()) {
            return zp2;
        }
        zp2 e2 = zp2.e();
        fn2.zzb = e2;
        return e2;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0422 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01eb  */
    public final int a(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.fossil.xl2 r30) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = r
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.fossil.nn2 r11 = (com.fossil.nn2) r11
            boolean r12 = r11.zza()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.fossil.nn2 r11 = r11.zza(r12)
            sun.misc.Unsafe r12 = r
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03e4;
                case 19: goto L_0x03a6;
                case 20: goto L_0x0365;
                case 21: goto L_0x0365;
                case 22: goto L_0x034b;
                case 23: goto L_0x030c;
                case 24: goto L_0x02cd;
                case 25: goto L_0x0276;
                case 26: goto L_0x01c3;
                case 27: goto L_0x01a9;
                case 28: goto L_0x0151;
                case 29: goto L_0x034b;
                case 30: goto L_0x0119;
                case 31: goto L_0x02cd;
                case 32: goto L_0x030c;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e4;
                case 36: goto L_0x03a6;
                case 37: goto L_0x0365;
                case 38: goto L_0x0365;
                case 39: goto L_0x034b;
                case 40: goto L_0x030c;
                case 41: goto L_0x02cd;
                case 42: goto L_0x0276;
                case 43: goto L_0x034b;
                case 44: goto L_0x0119;
                case 45: goto L_0x02cd;
                case 46: goto L_0x030c;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0422
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0422
            com.fossil.fp2 r1 = r0.a((int) r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.fossil.ul2.a((com.fossil.fp2) r22, (byte[]) r23, (int) r24, (int) r25, (int) r26, (com.fossil.xl2) r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0422
            int r8 = com.fossil.ul2.a(r3, r4, r7)
            int r9 = r7.a
            if (r2 != r9) goto L_0x0422
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.fossil.ul2.a((com.fossil.fp2) r22, (byte[]) r23, (int) r24, (int) r25, (int) r26, (com.fossil.xl2) r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.fossil.ul2.b(r3, r1, r7)
            long r4 = r7.b
            long r4 = com.fossil.km2.a((long) r4)
            r11.a((long) r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0423
        L_0x009e:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0422
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            int r1 = com.fossil.ul2.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = com.fossil.km2.a((long) r8)
            r11.a((long) r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.ul2.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = com.fossil.km2.a((long) r8)
            r11.a((long) r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.fossil.in2 r11 = (com.fossil.in2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.fossil.ul2.a(r3, r1, r7)
            int r4 = r7.a
            int r4 = com.fossil.km2.a((int) r4)
            r11.a(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0423
        L_0x00eb:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0422
            com.fossil.in2 r11 = (com.fossil.in2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = com.fossil.km2.a((int) r4)
            r11.a(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = com.fossil.km2.a((int) r4)
            r11.a(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.fossil.ul2.a((byte[]) r3, (int) r4, (com.fossil.nn2<?>) r11, (com.fossil.xl2) r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0422
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.fossil.ul2.a((int) r2, (byte[]) r3, (int) r4, (int) r5, (com.fossil.nn2<?>) r6, (com.fossil.xl2) r7)
        L_0x0131:
            com.fossil.fn2 r1 = (com.fossil.fn2) r1
            com.fossil.zp2 r3 = r1.zzb
            com.fossil.zp2 r4 = com.fossil.zp2.d()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.fossil.mn2 r4 = r0.c(r8)
            com.fossil.wp2<?, ?> r5 = r0.n
            r6 = r22
            java.lang.Object r3 = com.fossil.gp2.a(r6, r11, r4, r3, r5)
            com.fossil.zp2 r3 = (com.fossil.zp2) r3
            if (r3 == 0) goto L_0x014e
            r1.zzb = r3
        L_0x014e:
            r1 = r2
            goto L_0x0423
        L_0x0151:
            if (r6 != r10) goto L_0x0422
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x01a4
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x019f
            if (r4 != 0) goto L_0x0167
            com.fossil.yl2 r4 = com.fossil.yl2.zza
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.fossil.yl2 r6 = com.fossil.yl2.zza((byte[]) r3, (int) r1, (int) r4)
            r11.add(r6)
        L_0x016e:
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x019a
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0195
            if (r4 != 0) goto L_0x018d
            com.fossil.yl2 r4 = com.fossil.yl2.zza
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.fossil.yl2 r6 = com.fossil.yl2.zza((byte[]) r3, (int) r1, (int) r4)
            r11.add(r6)
            goto L_0x016e
        L_0x0195:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x019a:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x019f:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x01a4:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x01a9:
            if (r6 != r10) goto L_0x0422
            com.fossil.fp2 r1 = r0.a((int) r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.fossil.ul2.a(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0423
        L_0x01c3:
            if (r6 != r10) goto L_0x0422
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x0216
            int r4 = com.fossil.ul2.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0211
            if (r6 != 0) goto L_0x01de
            r11.add(r1)
            goto L_0x01e9
        L_0x01de:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.fossil.hn2.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01e8:
            int r4 = r4 + r6
        L_0x01e9:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.ul2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.ul2.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x020c
            if (r6 != 0) goto L_0x0201
            r11.add(r1)
            goto L_0x01e9
        L_0x0201:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.fossil.hn2.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01e8
        L_0x020c:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x0211:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x0216:
            int r4 = com.fossil.ul2.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0271
            if (r6 != 0) goto L_0x0224
            r11.add(r1)
            goto L_0x0237
        L_0x0224:
            int r8 = r4 + r6
            boolean r9 = com.fossil.fq2.a((byte[]) r3, (int) r4, (int) r8)
            if (r9 == 0) goto L_0x026c
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.fossil.hn2.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x0236:
            r4 = r8
        L_0x0237:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.ul2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.ul2.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0267
            if (r6 != 0) goto L_0x024f
            r11.add(r1)
            goto L_0x0237
        L_0x024f:
            int r8 = r4 + r6
            boolean r9 = com.fossil.fq2.a((byte[]) r3, (int) r4, (int) r8)
            if (r9 == 0) goto L_0x0262
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.fossil.hn2.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x0236
        L_0x0262:
            com.fossil.qn2 r1 = com.fossil.qn2.zzh()
            throw r1
        L_0x0267:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x026c:
            com.fossil.qn2 r1 = com.fossil.qn2.zzh()
            throw r1
        L_0x0271:
            com.fossil.qn2 r1 = com.fossil.qn2.zzb()
            throw r1
        L_0x0276:
            r1 = 0
            if (r6 != r10) goto L_0x029e
            com.fossil.wl2 r11 = (com.fossil.wl2) r11
            int r2 = com.fossil.ul2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = r4 + r2
        L_0x0282:
            if (r2 >= r4) goto L_0x0295
            int r2 = com.fossil.ul2.b(r3, r2, r7)
            long r5 = r7.b
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0290
            r5 = 1
            goto L_0x0291
        L_0x0290:
            r5 = 0
        L_0x0291:
            r11.a(r5)
            goto L_0x0282
        L_0x0295:
            if (r2 != r4) goto L_0x0299
            goto L_0x014e
        L_0x0299:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x029e:
            if (r6 != 0) goto L_0x0422
            com.fossil.wl2 r11 = (com.fossil.wl2) r11
            int r4 = com.fossil.ul2.b(r3, r4, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02ae
            r6 = 1
            goto L_0x02af
        L_0x02ae:
            r6 = 0
        L_0x02af:
            r11.a(r6)
        L_0x02b2:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.ul2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.ul2.b(r3, r6, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02c8
            r6 = 1
            goto L_0x02c9
        L_0x02c8:
            r6 = 0
        L_0x02c9:
            r11.a(r6)
            goto L_0x02b2
        L_0x02cd:
            if (r6 != r10) goto L_0x02ed
            com.fossil.in2 r11 = (com.fossil.in2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x02d8:
            if (r1 >= r2) goto L_0x02e4
            int r4 = com.fossil.ul2.a(r3, r1)
            r11.a(r4)
            int r1 = r1 + 4
            goto L_0x02d8
        L_0x02e4:
            if (r1 != r2) goto L_0x02e8
            goto L_0x0423
        L_0x02e8:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x02ed:
            if (r6 != r9) goto L_0x0422
            com.fossil.in2 r11 = (com.fossil.in2) r11
            int r1 = com.fossil.ul2.a(r18, r19)
            r11.a(r1)
        L_0x02f8:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.ul2.a(r3, r4)
            r11.a(r1)
            goto L_0x02f8
        L_0x030c:
            if (r6 != r10) goto L_0x032c
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x0317:
            if (r1 >= r2) goto L_0x0323
            long r4 = com.fossil.ul2.b(r3, r1)
            r11.a((long) r4)
            int r1 = r1 + 8
            goto L_0x0317
        L_0x0323:
            if (r1 != r2) goto L_0x0327
            goto L_0x0423
        L_0x0327:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x032c:
            if (r6 != r13) goto L_0x0422
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            long r8 = com.fossil.ul2.b(r18, r19)
            r11.a((long) r8)
        L_0x0337:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            long r8 = com.fossil.ul2.b(r3, r4)
            r11.a((long) r8)
            goto L_0x0337
        L_0x034b:
            if (r6 != r10) goto L_0x0353
            int r1 = com.fossil.ul2.a((byte[]) r3, (int) r4, (com.fossil.nn2<?>) r11, (com.fossil.xl2) r7)
            goto L_0x0423
        L_0x0353:
            if (r6 != 0) goto L_0x0422
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.fossil.ul2.a((int) r21, (byte[]) r22, (int) r23, (int) r24, (com.fossil.nn2<?>) r25, (com.fossil.xl2) r26)
            goto L_0x0423
        L_0x0365:
            if (r6 != r10) goto L_0x0385
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x0370:
            if (r1 >= r2) goto L_0x037c
            int r1 = com.fossil.ul2.b(r3, r1, r7)
            long r4 = r7.b
            r11.a((long) r4)
            goto L_0x0370
        L_0x037c:
            if (r1 != r2) goto L_0x0380
            goto L_0x0423
        L_0x0380:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x0385:
            if (r6 != 0) goto L_0x0422
            com.fossil.fo2 r11 = (com.fossil.fo2) r11
            int r1 = com.fossil.ul2.b(r3, r4, r7)
            long r8 = r7.b
            r11.a((long) r8)
        L_0x0392:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.ul2.b(r3, r4, r7)
            long r8 = r7.b
            r11.a((long) r8)
            goto L_0x0392
        L_0x03a6:
            if (r6 != r10) goto L_0x03c5
            com.fossil.en2 r11 = (com.fossil.en2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03b1:
            if (r1 >= r2) goto L_0x03bd
            float r4 = com.fossil.ul2.d(r3, r1)
            r11.a(r4)
            int r1 = r1 + 4
            goto L_0x03b1
        L_0x03bd:
            if (r1 != r2) goto L_0x03c0
            goto L_0x0423
        L_0x03c0:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x03c5:
            if (r6 != r9) goto L_0x0422
            com.fossil.en2 r11 = (com.fossil.en2) r11
            float r1 = com.fossil.ul2.d(r18, r19)
            r11.a(r1)
        L_0x03d0:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            float r1 = com.fossil.ul2.d(r3, r4)
            r11.a(r1)
            goto L_0x03d0
        L_0x03e4:
            if (r6 != r10) goto L_0x0403
            com.fossil.qm2 r11 = (com.fossil.qm2) r11
            int r1 = com.fossil.ul2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03ef:
            if (r1 >= r2) goto L_0x03fb
            double r4 = com.fossil.ul2.c(r3, r1)
            r11.a(r4)
            int r1 = r1 + 8
            goto L_0x03ef
        L_0x03fb:
            if (r1 != r2) goto L_0x03fe
            goto L_0x0423
        L_0x03fe:
            com.fossil.qn2 r1 = com.fossil.qn2.zza()
            throw r1
        L_0x0403:
            if (r6 != r13) goto L_0x0422
            com.fossil.qm2 r11 = (com.fossil.qm2) r11
            double r8 = com.fossil.ul2.c(r18, r19)
            r11.a(r8)
        L_0x040e:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.ul2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            double r8 = com.fossil.ul2.c(r3, r4)
            r11.a(r8)
            goto L_0x040e
        L_0x0422:
            r1 = r4
        L_0x0423:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uo2.a(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.fossil.xl2):int");
    }

    @DexIgnore
    public final <K, V> int a(T t, byte[] bArr, int i2, int i3, int i4, long j2, xl2 xl2) throws IOException {
        Unsafe unsafe = r;
        Object b2 = b(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.p.zzc(object)) {
            Object b3 = this.p.b(b2);
            this.p.zza(b3, object);
            unsafe.putObject(t, j2, b3);
        }
        this.p.a(b2);
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x018a, code lost:
        r2 = r4 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x019b, code lost:
        r2 = r4 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x019d, code lost:
        r12.putInt(r1, r13, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return r2;
     */
    @DexIgnore
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, xl2 xl2) throws IOException {
        int i10;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i11 = i2;
        int i12 = i4;
        int i13 = i5;
        int i14 = i6;
        long j3 = j2;
        int i15 = i9;
        xl2 xl22 = xl2;
        Unsafe unsafe = r;
        long j4 = (long) (this.a[i15 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i14 == 1) {
                    unsafe.putObject(t2, j3, Double.valueOf(ul2.c(bArr, i2)));
                    break;
                }
                break;
            case 52:
                if (i14 == 5) {
                    unsafe.putObject(t2, j3, Float.valueOf(ul2.d(bArr, i2)));
                    break;
                }
                break;
            case 53:
            case 54:
                if (i14 == 0) {
                    i10 = ul2.b(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, Long.valueOf(xl22.b));
                    break;
                }
                break;
            case 55:
            case 62:
                if (i14 == 0) {
                    i10 = ul2.a(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, Integer.valueOf(xl22.a));
                    break;
                }
                break;
            case 56:
            case 65:
                if (i14 == 1) {
                    unsafe.putObject(t2, j3, Long.valueOf(ul2.b(bArr, i2)));
                    break;
                }
                break;
            case 57:
            case 64:
                if (i14 == 5) {
                    unsafe.putObject(t2, j3, Integer.valueOf(ul2.a(bArr, i2)));
                    break;
                }
                break;
            case 58:
                if (i14 == 0) {
                    i10 = ul2.b(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, Boolean.valueOf(xl22.b != 0));
                    break;
                }
                break;
            case 59:
                if (i14 == 2) {
                    int a2 = ul2.a(bArr2, i11, xl22);
                    int i16 = xl22.a;
                    if (i16 == 0) {
                        unsafe.putObject(t2, j3, "");
                    } else if ((i7 & 536870912) == 0 || fq2.a(bArr2, a2, a2 + i16)) {
                        unsafe.putObject(t2, j3, new String(bArr2, a2, i16, hn2.a));
                        a2 += i16;
                    } else {
                        throw qn2.zzh();
                    }
                    unsafe.putInt(t2, j4, i13);
                    return a2;
                }
                break;
            case 60:
                if (i14 == 2) {
                    int a3 = ul2.a(a(i15), bArr2, i11, i3, xl22);
                    Object object = unsafe.getInt(t2, j4) == i13 ? unsafe.getObject(t2, j3) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j3, xl22.c);
                    } else {
                        unsafe.putObject(t2, j3, hn2.a(object, xl22.c));
                    }
                    unsafe.putInt(t2, j4, i13);
                    return a3;
                }
                break;
            case 61:
                if (i14 == 2) {
                    i10 = ul2.e(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, xl22.c);
                    break;
                }
                break;
            case 63:
                if (i14 == 0) {
                    int a4 = ul2.a(bArr2, i11, xl22);
                    int i17 = xl22.a;
                    mn2 c2 = c(i15);
                    if (c2 == null || c2.zza(i17)) {
                        unsafe.putObject(t2, j3, Integer.valueOf(i17));
                        i10 = a4;
                        break;
                    } else {
                        a((Object) t).a(i12, (Object) Long.valueOf((long) i17));
                        return a4;
                    }
                }
                break;
            case 66:
                if (i14 == 0) {
                    i10 = ul2.a(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, Integer.valueOf(km2.a(xl22.a)));
                    break;
                }
                break;
            case 67:
                if (i14 == 0) {
                    i10 = ul2.b(bArr2, i11, xl22);
                    unsafe.putObject(t2, j3, Long.valueOf(km2.a(xl22.b)));
                    break;
                }
                break;
            case 68:
                if (i14 == 3) {
                    i10 = ul2.a(a(i15), bArr, i2, i3, (i12 & -8) | 4, xl2);
                    Object object2 = unsafe.getInt(t2, j4) == i13 ? unsafe.getObject(t2, j3) : null;
                    if (object2 != null) {
                        unsafe.putObject(t2, j3, hn2.a(object2, xl22.c));
                        break;
                    } else {
                        unsafe.putObject(t2, j3, xl22.c);
                        break;
                    }
                }
                break;
        }
    }

    @DexIgnore
    public final fp2 a(int i2) {
        int i3 = (i2 / 3) << 1;
        fp2 fp2 = (fp2) this.b[i3];
        if (fp2 != null) {
            return fp2;
        }
        fp2 a2 = ap2.a().a((Class) this.b[i3 + 1]);
        this.b[i3] = a2;
        return a2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v27, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r20v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r26v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r26v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r26v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r26v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r26v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v28, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v21, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v22, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v21, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v23, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v31, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v22, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v23, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v24, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v25, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v26, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v27, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v28, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v29, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v30, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v31, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v33, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v35, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v26, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v44, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v36, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v37, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v32, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v32, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v29, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v30, resolved type: byte} */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0094, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x017a, code lost:
        r6 = r6 | r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0219, code lost:
        r6 = r6 | r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x021b, code lost:
        r3 = r8;
        r2 = r9;
        r1 = r11;
        r9 = r13;
        r11 = r35;
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0245, code lost:
        r33 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x02bc, code lost:
        r0 = r7 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02be, code lost:
        r6 = r6 | r23;
        r7 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x02c2, code lost:
        r3 = r8;
        r2 = r9;
        r1 = r11;
        r9 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x02c6, code lost:
        r13 = r34;
        r11 = r35;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x02cc, code lost:
        r18 = r33;
        r20 = r6;
        r2 = r7;
        r7 = r8;
        r19 = r9;
        r27 = r10;
        r25 = r11;
        r6 = r35;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03e4 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x043b  */
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, xl2 xl2) throws IOException {
        Unsafe unsafe;
        int i5;
        T t2;
        uo2 uo2;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        byte[] bArr2;
        int i16;
        xl2 xl22;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        T t3;
        int i24;
        int i25;
        int i26;
        int i27;
        int i28;
        int i29;
        int i30;
        xl2 xl23;
        byte[] bArr3;
        int i31;
        xl2 xl24;
        int i32;
        xl2 xl25;
        uo2 uo22 = this;
        T t4 = t;
        byte[] bArr4 = bArr;
        int i33 = i3;
        int i34 = i4;
        xl2 xl26 = xl2;
        Unsafe unsafe2 = r;
        int i35 = i2;
        int i36 = -1;
        int i37 = 0;
        int i38 = 0;
        int i39 = 0;
        int i40 = -1;
        while (true) {
            if (i35 < i33) {
                int i41 = i35 + 1;
                byte b2 = bArr4[i35];
                if (b2 < 0) {
                    i11 = ul2.a((int) b2, bArr4, i41, xl26);
                    i10 = xl26.a;
                } else {
                    i10 = b2;
                    i11 = i41;
                }
                int i42 = i10 >>> 3;
                int i43 = i10 & 7;
                if (i42 > i36) {
                    i12 = uo22.a(i42, i37 / 3);
                } else {
                    i12 = uo22.f(i42);
                }
                int i44 = i12;
                if (i44 == -1) {
                    i22 = i42;
                    i6 = i11;
                    i13 = i39;
                    i15 = i40;
                    unsafe = unsafe2;
                    i5 = i34;
                    i14 = 0;
                    i23 = i10;
                    if (i23 == i5 || i5 == 0) {
                        if (this.f) {
                            xl22 = xl2;
                            if (xl22.d != sm2.a()) {
                                i16 = i22;
                                if (xl22.d.a(this.e, i16) == null) {
                                    i18 = ul2.a(i23, bArr, i6, i3, a((Object) t), xl2);
                                    t4 = t;
                                    bArr2 = bArr;
                                    i33 = i3;
                                    i34 = i5;
                                    i17 = i23;
                                    uo22 = this;
                                    i19 = i16;
                                    i40 = i15;
                                    i20 = i14;
                                    i21 = i13;
                                } else {
                                    fn2.d dVar = t;
                                    dVar.n();
                                    xm2<fn2.c> xm2 = dVar.zzc;
                                    throw new NoSuchMethodError();
                                }
                            } else {
                                t3 = t;
                            }
                        } else {
                            t3 = t;
                            xl22 = xl2;
                        }
                        i35 = ul2.a(i23, bArr, i6, i3, a((Object) t), xl2);
                        bArr4 = bArr;
                        i33 = i3;
                        i38 = i23;
                        uo22 = this;
                        i36 = i22;
                        t4 = t3;
                        i40 = i15;
                        i37 = i14;
                        unsafe2 = unsafe;
                        i34 = i5;
                        i39 = i13;
                    } else {
                        i9 = -1;
                        uo2 = this;
                        t2 = t;
                        i38 = i23;
                        i7 = i15;
                        i8 = i13;
                    }
                } else {
                    int[] iArr = uo22.a;
                    int i45 = iArr[i44 + 1];
                    int i46 = (i45 & 267386880) >>> 20;
                    int i47 = i10;
                    long j2 = (long) (i45 & 1048575);
                    int i48 = i45;
                    if (i46 <= 17) {
                        int i49 = iArr[i44 + 2];
                        int i50 = 1 << (i49 >>> 20);
                        int i51 = i49 & 1048575;
                        if (i51 != i40) {
                            if (i40 != -1) {
                                unsafe2.putInt(t4, (long) i40, i39);
                            }
                            i39 = unsafe2.getInt(t4, (long) i51);
                            i40 = i51;
                        }
                        switch (i46) {
                            case 0:
                                i28 = i44;
                                i27 = i42;
                                i26 = i40;
                                long j3 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                i30 = i11;
                                if (i43 == 1) {
                                    cq2.a((Object) t4, j3, ul2.c(bArr3, i30));
                                    break;
                                }
                                break;
                            case 1:
                                i28 = i44;
                                i27 = i42;
                                i26 = i40;
                                long j4 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                i30 = i11;
                                if (i43 == 5) {
                                    cq2.a((Object) t4, j4, ul2.d(bArr3, i30));
                                    i31 = i30 + 4;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                i28 = i44;
                                i27 = i42;
                                i26 = i40;
                                long j5 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl2 xl27 = xl2;
                                i30 = i11;
                                if (i43 == 0) {
                                    int b3 = ul2.b(bArr3, i30, xl27);
                                    unsafe2.putLong(t, j5, xl27.b);
                                    i39 |= i50;
                                    i31 = b3;
                                    i38 = i29;
                                    i37 = i28;
                                    i36 = i27;
                                    xl26 = xl27;
                                    i40 = i26;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                i28 = i44;
                                i27 = i42;
                                i26 = i40;
                                long j6 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                i30 = i11;
                                if (i43 == 0) {
                                    i31 = ul2.a(bArr3, i30, xl23);
                                    unsafe2.putInt(t4, j6, xl23.a);
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                int i52 = i3;
                                i28 = i44;
                                i27 = i42;
                                long j7 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                if (i43 == 1) {
                                    i26 = i40;
                                    i30 = i11;
                                    unsafe2.putLong(t, j7, ul2.b(bArr3, i11));
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                i32 = i3;
                                i28 = i44;
                                i27 = i42;
                                long j8 = j2;
                                i29 = i47;
                                bArr4 = bArr;
                                xl24 = xl2;
                                if (i43 == 5) {
                                    unsafe2.putInt(t4, j8, ul2.a(bArr4, i11));
                                    i35 = i11 + 4;
                                    break;
                                }
                                break;
                            case 7:
                                i32 = i3;
                                i28 = i44;
                                i27 = i42;
                                long j9 = j2;
                                i29 = i47;
                                bArr4 = bArr;
                                xl24 = xl2;
                                if (i43 == 0) {
                                    int b4 = ul2.b(bArr4, i11, xl24);
                                    cq2.a((Object) t4, j9, xl24.b != 0);
                                    i39 |= i50;
                                    i35 = b4;
                                    break;
                                }
                                break;
                            case 8:
                                i32 = i3;
                                i28 = i44;
                                i27 = i42;
                                long j10 = j2;
                                i29 = i47;
                                bArr4 = bArr;
                                xl24 = xl2;
                                if (i43 == 2) {
                                    if ((i48 & 536870912) == 0) {
                                        i35 = ul2.c(bArr4, i11, xl24);
                                    } else {
                                        i35 = ul2.d(bArr4, i11, xl24);
                                    }
                                    unsafe2.putObject(t4, j10, xl24.c);
                                    break;
                                }
                                break;
                            case 9:
                                i28 = i44;
                                i27 = i42;
                                long j11 = j2;
                                i29 = i47;
                                bArr4 = bArr;
                                xl24 = xl2;
                                if (i43 != 2) {
                                    int i53 = i3;
                                    break;
                                } else {
                                    i32 = i3;
                                    i35 = ul2.a(uo22.a(i28), bArr4, i11, i32, xl24);
                                    if ((i39 & i50) != 0) {
                                        unsafe2.putObject(t4, j11, hn2.a(unsafe2.getObject(t4, j11), xl24.c));
                                        break;
                                    } else {
                                        unsafe2.putObject(t4, j11, xl24.c);
                                        break;
                                    }
                                }
                            case 10:
                                i28 = i44;
                                i27 = i42;
                                long j12 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                if (i43 == 2) {
                                    i31 = ul2.e(bArr3, i11, xl23);
                                    unsafe2.putObject(t4, j12, xl23.c);
                                    break;
                                }
                                break;
                            case 12:
                                i28 = i44;
                                i27 = i42;
                                long j13 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                if (i43 == 0) {
                                    i31 = ul2.a(bArr3, i11, xl23);
                                    int i54 = xl23.a;
                                    mn2 c2 = uo22.c(i28);
                                    if (c2 != null && !c2.zza(i54)) {
                                        a((Object) t).a(i29, (Object) Long.valueOf((long) i54));
                                        break;
                                    } else {
                                        unsafe2.putInt(t4, j13, i54);
                                        break;
                                    }
                                }
                                break;
                            case 15:
                                i28 = i44;
                                i27 = i42;
                                long j14 = j2;
                                i29 = i47;
                                bArr3 = bArr;
                                xl23 = xl2;
                                if (i43 == 0) {
                                    i31 = ul2.a(bArr3, i11, xl23);
                                    unsafe2.putInt(t4, j14, km2.a(xl23.a));
                                    break;
                                }
                                break;
                            case 16:
                                xl2 xl28 = xl2;
                                i28 = i44;
                                i27 = i42;
                                i29 = i47;
                                if (i43 != 0) {
                                    byte[] bArr5 = bArr;
                                    xl2 xl29 = xl28;
                                    break;
                                } else {
                                    long j15 = j2;
                                    bArr3 = bArr;
                                    int b5 = ul2.b(bArr3, i11, xl28);
                                    xl23 = xl28;
                                    unsafe2.putLong(t, j15, km2.a(xl28.b));
                                    i39 |= i50;
                                    i31 = b5;
                                    break;
                                }
                            case 17:
                                if (i43 != 3) {
                                    i28 = i44;
                                    i27 = i42;
                                    i29 = i47;
                                    byte[] bArr6 = bArr;
                                    xl2 xl210 = xl2;
                                    break;
                                } else {
                                    int i55 = i44;
                                    int i56 = i42;
                                    int i57 = i47;
                                    i35 = ul2.a(uo22.a(i44), bArr, i11, i3, (i42 << 3) | 4, xl2);
                                    if ((i39 & i50) == 0) {
                                        xl25 = xl2;
                                        unsafe2.putObject(t4, j2, xl25.c);
                                    } else {
                                        xl25 = xl2;
                                        unsafe2.putObject(t4, j2, hn2.a(unsafe2.getObject(t4, j2), xl25.c));
                                    }
                                    i39 |= i50;
                                    bArr4 = bArr;
                                    i33 = i3;
                                    i38 = i57;
                                    i37 = i55;
                                    i36 = i56;
                                    i34 = i4;
                                    xl26 = xl25;
                                    break;
                                }
                            default:
                                byte[] bArr7 = bArr;
                                xl2 xl211 = xl2;
                                i28 = i44;
                                i27 = i42;
                                i26 = i40;
                                i29 = i47;
                                break;
                        }
                    } else {
                        int i58 = i42;
                        i15 = i40;
                        int i59 = i47;
                        int i60 = i11;
                        long j16 = j2;
                        bArr4 = bArr;
                        xl2 xl212 = xl26;
                        int i61 = i44;
                        long j17 = j16;
                        if (i46 != 27) {
                            i13 = i39;
                            int i62 = i58;
                            if (i46 <= 49) {
                                i22 = i62;
                                int i63 = i60;
                                i24 = i59;
                                i14 = i61;
                                unsafe = unsafe2;
                                i18 = a(t, bArr, i60, i3, i59, i62, i43, i61, (long) i48, i46, j17, xl2);
                                if (i18 != i63) {
                                    uo22 = this;
                                    t4 = t;
                                    bArr2 = bArr;
                                    i33 = i3;
                                    i34 = i4;
                                    xl22 = xl2;
                                    i40 = i15;
                                    i20 = i14;
                                    i21 = i13;
                                    i19 = i22;
                                    i17 = i24;
                                }
                            } else {
                                int i64 = i43;
                                long j18 = j17;
                                i22 = i62;
                                i25 = i60;
                                i24 = i59;
                                i14 = i61;
                                unsafe = unsafe2;
                                int i65 = i48;
                                if (i46 != 50) {
                                    i18 = a(t, bArr, i25, i3, i24, i22, i64, i65, i46, j18, i14, xl2);
                                    if (i18 != i25) {
                                        i16 = i22;
                                        uo22 = this;
                                        t4 = t;
                                        bArr2 = bArr;
                                        i33 = i3;
                                        i34 = i4;
                                        xl22 = xl2;
                                        i17 = i24;
                                        i19 = i16;
                                        i40 = i15;
                                        i20 = i14;
                                        i21 = i13;
                                    }
                                } else if (i64 == 2) {
                                    a(t, bArr, i25, i3, i14, j18, xl2);
                                    throw null;
                                }
                            }
                            i5 = i4;
                            i6 = i18;
                            i23 = i24;
                            if (i23 == i5) {
                            }
                            if (this.f) {
                            }
                            i35 = ul2.a(i23, bArr, i6, i3, a((Object) t), xl2);
                            bArr4 = bArr;
                            i33 = i3;
                            i38 = i23;
                            uo22 = this;
                            i36 = i22;
                            t4 = t3;
                            i40 = i15;
                            i37 = i14;
                            unsafe2 = unsafe;
                            i34 = i5;
                            i39 = i13;
                        } else if (i43 == 2) {
                            nn2 nn2 = (nn2) unsafe2.getObject(t4, j17);
                            if (!nn2.zza()) {
                                int size = nn2.size();
                                nn2 = nn2.zza(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t4, j17, nn2);
                            }
                            i35 = ul2.a(uo22.a(i61), i59, bArr, i60, i3, nn2, xl2);
                            i34 = i4;
                            i36 = i58;
                            i38 = i59;
                            i37 = i61;
                            xl26 = xl212;
                            i40 = i15;
                            i39 = i39;
                            i33 = i3;
                        } else {
                            i13 = i39;
                            i22 = i58;
                            i25 = i60;
                            i24 = i59;
                            i14 = i61;
                            unsafe = unsafe2;
                        }
                        i5 = i4;
                        i6 = i25;
                        i23 = i24;
                        if (i23 == i5) {
                        }
                        if (this.f) {
                        }
                        i35 = ul2.a(i23, bArr, i6, i3, a((Object) t), xl2);
                        bArr4 = bArr;
                        i33 = i3;
                        i38 = i23;
                        uo22 = this;
                        i36 = i22;
                        t4 = t3;
                        i40 = i15;
                        i37 = i14;
                        unsafe2 = unsafe;
                        i34 = i5;
                        i39 = i13;
                    }
                }
                unsafe2 = unsafe;
            } else {
                int i66 = i39;
                unsafe = unsafe2;
                i5 = i34;
                t2 = t4;
                uo2 = uo22;
                i6 = i35;
                i7 = i40;
                i8 = i66;
                i9 = -1;
            }
        }
        if (i7 != i9) {
            unsafe.putInt(t2, (long) i7, i8);
        }
        zp2 zp2 = null;
        for (int i67 = uo2.j; i67 < uo2.k; i67++) {
            uo2.a((Object) t2, uo2.i[i67], zp2, uo2.n);
            zp2 = zp2;
        }
        if (zp2 != null) {
            uo2.n.b((Object) t2, zp2);
        }
        if (i5 == 0) {
            if (i6 != i3) {
                throw qn2.zzg();
            }
        } else if (i6 > i3 || i38 != i5) {
            throw qn2.zzg();
        }
        return i6;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v2, resolved type: byte} */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010b, code lost:
        r2 = r4;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x013d, code lost:
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0159, code lost:
        r0 = r8 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x015b, code lost:
        r1 = r7;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0160, code lost:
        r24 = r7;
        r15 = r8;
        r18 = r9;
        r19 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01e1, code lost:
        if (r0 == r15) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x022b, code lost:
        if (r0 == r15) goto L_0x022d;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(T t, byte[] bArr, int i2, int i3, xl2 xl2) throws IOException {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        Unsafe unsafe;
        int i9;
        int a2;
        int i10;
        int i11;
        int i12;
        int i13;
        uo2 uo2 = this;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i14 = i3;
        xl2 xl22 = xl2;
        if (uo2.g) {
            Unsafe unsafe2 = r;
            int i15 = -1;
            int i16 = i2;
            int i17 = -1;
            int i18 = 0;
            while (i16 < i14) {
                int i19 = i16 + 1;
                byte b2 = bArr2[i16];
                if (b2 < 0) {
                    i5 = ul2.a((int) b2, bArr2, i19, xl22);
                    i4 = xl22.a;
                } else {
                    i4 = b2;
                    i5 = i19;
                }
                int i20 = i4 >>> 3;
                int i21 = i4 & 7;
                if (i20 > i17) {
                    i6 = uo2.a(i20, i18 / 3);
                } else {
                    i6 = uo2.f(i20);
                }
                int i22 = i6;
                if (i22 == i15) {
                    i7 = i20;
                    i9 = i5;
                    unsafe = unsafe2;
                    i8 = 0;
                } else {
                    int i23 = uo2.a[i22 + 1];
                    int i24 = (267386880 & i23) >>> 20;
                    long j2 = (long) (1048575 & i23);
                    if (i24 <= 17) {
                        boolean z = true;
                        switch (i24) {
                            case 0:
                                long j3 = j2;
                                i11 = i22;
                                if (i21 == 1) {
                                    cq2.a((Object) t2, j3, ul2.c(bArr2, i5));
                                    break;
                                }
                                break;
                            case 1:
                                long j4 = j2;
                                i11 = i22;
                                if (i21 == 5) {
                                    cq2.a((Object) t2, j4, ul2.d(bArr2, i5));
                                    i12 = i5 + 4;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                long j5 = j2;
                                i11 = i22;
                                if (i21 == 0) {
                                    i13 = ul2.b(bArr2, i5, xl22);
                                    unsafe2.putLong(t, j5, xl22.b);
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                long j6 = j2;
                                i11 = i22;
                                if (i21 == 0) {
                                    i12 = ul2.a(bArr2, i5, xl22);
                                    unsafe2.putInt(t2, j6, xl22.a);
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                long j7 = j2;
                                if (i21 == 1) {
                                    i11 = i22;
                                    unsafe2.putLong(t, j7, ul2.b(bArr2, i5));
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                long j8 = j2;
                                if (i21 == 5) {
                                    unsafe2.putInt(t2, j8, ul2.a(bArr2, i5));
                                    i16 = i5 + 4;
                                    break;
                                }
                                break;
                            case 7:
                                long j9 = j2;
                                if (i21 == 0) {
                                    int b3 = ul2.b(bArr2, i5, xl22);
                                    if (xl22.b == 0) {
                                        z = false;
                                    }
                                    cq2.a((Object) t2, j9, z);
                                    i16 = b3;
                                    break;
                                }
                                break;
                            case 8:
                                long j10 = j2;
                                if (i21 == 2) {
                                    if ((536870912 & i23) == 0) {
                                        i16 = ul2.c(bArr2, i5, xl22);
                                    } else {
                                        i16 = ul2.d(bArr2, i5, xl22);
                                    }
                                    unsafe2.putObject(t2, j10, xl22.c);
                                    break;
                                }
                                break;
                            case 9:
                                long j11 = j2;
                                if (i21 == 2) {
                                    i16 = ul2.a(uo2.a(i22), bArr2, i5, i14, xl22);
                                    Object object = unsafe2.getObject(t2, j11);
                                    if (object != null) {
                                        unsafe2.putObject(t2, j11, hn2.a(object, xl22.c));
                                        break;
                                    } else {
                                        unsafe2.putObject(t2, j11, xl22.c);
                                        break;
                                    }
                                }
                                break;
                            case 10:
                                long j12 = j2;
                                if (i21 == 2) {
                                    i16 = ul2.e(bArr2, i5, xl22);
                                    unsafe2.putObject(t2, j12, xl22.c);
                                    break;
                                }
                                break;
                            case 12:
                                long j13 = j2;
                                i11 = i22;
                                if (i21 == 0) {
                                    i12 = ul2.a(bArr2, i5, xl22);
                                    unsafe2.putInt(t2, j13, xl22.a);
                                    break;
                                }
                                break;
                            case 15:
                                long j14 = j2;
                                i11 = i22;
                                if (i21 == 0) {
                                    i12 = ul2.a(bArr2, i5, xl22);
                                    unsafe2.putInt(t2, j14, km2.a(xl22.a));
                                    break;
                                }
                                break;
                            case 16:
                                if (i21 == 0) {
                                    i13 = ul2.b(bArr2, i5, xl22);
                                    i11 = i22;
                                    unsafe2.putLong(t, j2, km2.a(xl22.b));
                                    break;
                                }
                                break;
                        }
                    } else if (i24 == 27) {
                        if (i21 == 2) {
                            nn2 nn2 = (nn2) unsafe2.getObject(t2, j2);
                            if (!nn2.zza()) {
                                int size = nn2.size();
                                nn2 = nn2.zza(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t2, j2, nn2);
                            }
                            i16 = ul2.a(uo2.a(i22), i4, bArr, i5, i3, nn2, xl2);
                            i17 = i20;
                            i18 = i22;
                        }
                        i8 = i22;
                        i7 = i20;
                        i10 = i5;
                        unsafe = unsafe2;
                        i9 = i10;
                    } else {
                        i8 = i22;
                        if (i24 <= 49) {
                            long j15 = (long) i23;
                            int i25 = i21;
                            i7 = i20;
                            int i26 = i5;
                            unsafe = unsafe2;
                            a2 = a(t, bArr, i5, i3, i4, i20, i25, i8, j15, i24, j2, xl2);
                        } else {
                            long j16 = j2;
                            int i27 = i21;
                            i7 = i20;
                            i10 = i5;
                            unsafe = unsafe2;
                            int i28 = i24;
                            if (i28 == 50) {
                                if (i27 == 2) {
                                    a(t, bArr, i10, i3, i8, j16, xl2);
                                    throw null;
                                }
                                i9 = i10;
                            } else {
                                a2 = a(t, bArr, i10, i3, i4, i7, i27, i23, i28, j16, i8, xl2);
                            }
                        }
                        i9 = a2;
                    }
                    i15 = -1;
                }
                a2 = ul2.a(i4, bArr, i9, i3, a((Object) t), xl2);
                i15 = -1;
                uo2 = this;
                t2 = t;
                bArr2 = bArr;
                i14 = i3;
                xl22 = xl2;
                unsafe2 = unsafe;
                i18 = i8;
                i17 = i7;
            }
            if (i16 != i14) {
                throw qn2.zzg();
            }
            return;
        }
        int i29 = i14;
        a(t, bArr, i2, i3, 0, xl2);
    }

    @DexIgnore
    public final <UT, UB> UB a(Object obj, int i2, UB ub, wp2<UT, UB> wp2) {
        mn2 c2;
        int i3 = this.a[i2];
        Object f2 = cq2.f(obj, (long) (d(i2) & 1048575));
        if (f2 == null || (c2 = c(i2)) == null) {
            return ub;
        }
        a(i2, i3, this.p.zza(f2), c2, ub, wp2);
        throw null;
    }

    @DexIgnore
    public final <K, V, UT, UB> UB a(int i2, int i3, Map<K, V> map, mn2 mn2, UB ub, wp2<UT, UB> wp2) {
        this.p.a(b(i2));
        throw null;
    }

    @DexIgnore
    public static boolean a(Object obj, int i2, fp2 fp2) {
        return fp2.zzd(cq2.f(obj, (long) (i2 & 1048575)));
    }

    @DexIgnore
    public static void a(int i2, Object obj, tq2 tq2) throws IOException {
        if (obj instanceof String) {
            tq2.zza(i2, (String) obj);
        } else {
            tq2.a(i2, (yl2) obj);
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3, int i4) {
        if (this.g) {
            return a(t, i2);
        }
        return (i3 & i4) != 0;
    }

    @DexIgnore
    public final boolean a(T t, int i2) {
        if (this.g) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    return cq2.e(t, j2) != 0.0d;
                case 1:
                    return cq2.d(t, j2) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return cq2.b(t, j2) != 0;
                case 3:
                    return cq2.b(t, j2) != 0;
                case 4:
                    return cq2.a((Object) t, j2) != 0;
                case 5:
                    return cq2.b(t, j2) != 0;
                case 6:
                    return cq2.a((Object) t, j2) != 0;
                case 7:
                    return cq2.c(t, j2);
                case 8:
                    Object f2 = cq2.f(t, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof yl2) {
                        return !yl2.zza.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return cq2.f(t, j2) != null;
                case 10:
                    return !yl2.zza.equals(cq2.f(t, j2));
                case 11:
                    return cq2.a((Object) t, j2) != 0;
                case 12:
                    return cq2.a((Object) t, j2) != 0;
                case 13:
                    return cq2.a((Object) t, j2) != 0;
                case 14:
                    return cq2.b(t, j2) != 0;
                case 15:
                    return cq2.a((Object) t, j2) != 0;
                case 16:
                    return cq2.b(t, j2) != 0;
                case 17:
                    return cq2.f(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int e2 = e(i2);
            return (cq2.a((Object) t, (long) (e2 & 1048575)) & (1 << (e2 >>> 20))) != 0;
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3) {
        return cq2.a((Object) t, (long) (e(i3) & 1048575)) == i2;
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return b(i2, i3);
    }
}
