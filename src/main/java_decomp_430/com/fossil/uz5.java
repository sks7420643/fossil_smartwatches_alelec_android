package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uz5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static /* final */ String d; // = "uz5";
    @DexIgnore
    public /* final */ FragmentManager a;
    @DexIgnore
    public /* final */ List<Fragment> b;
    @DexIgnore
    public int[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public a(uz5 uz5, View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;

        @DexIgnore
        public b(RecyclerView.ViewHolder viewHolder) {
            this.a = viewHolder;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            this.a.itemView.removeOnAttachStateChangeListener(this);
            BaseFragment baseFragment = uz5.this.b.get(this.a.getAdapterPosition());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = uz5.d;
            local.d(str, "onBindViewHolder tag=" + baseFragment.h1() + " childFragmentManager=" + uz5.this.a + "this=" + this);
            Fragment b2 = uz5.this.a.b(baseFragment.h1());
            if (b2 != null) {
                hc b3 = uz5.this.a.b();
                b3.d(b2);
                b3.d();
            }
            hc b4 = uz5.this.a.b();
            b4.a(view.getId(), baseFragment, baseFragment.h1());
            b4.d();
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public uz5(FragmentManager fragmentManager, List<Fragment> list) {
        this.a = fragmentManager;
        this.b = list;
    }

    @DexIgnore
    public int getItemCount() {
        int size = this.b.size();
        int[] iArr = this.c;
        if (iArr == null || size != iArr.length) {
            this.c = new int[size];
        }
        return size;
    }

    @DexIgnore
    public long getItemId(int i) {
        this.c[i] = this.b.get(i).getId();
        int[] iArr = this.c;
        if (iArr[i] == 0) {
            iArr[i] = View.generateViewId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = d;
            local.d(str, "getItemId: position = " + i + ", id = " + this.c[i]);
        }
        return (long) this.c[i];
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "onBindViewHolder: position = " + i);
        Fragment b2 = this.a.b(viewHolder.itemView.getId());
        if (b2 != null) {
            hc b3 = this.a.b();
            b3.d(b2);
            b3.d();
        }
        viewHolder.itemView.setId((int) getItemId(i));
        viewHolder.itemView.addOnAttachStateChangeListener(new b(viewHolder));
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new a(this, frameLayout);
    }
}
