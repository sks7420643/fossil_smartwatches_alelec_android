package com.fossil;

import android.content.Context;
import com.fossil.rv1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v82 implements rv1.d.b {
    @DexIgnore
    public /* final */ GoogleSignInAccount a;

    @DexIgnore
    public v82(Context context, GoogleSignInAccount googleSignInAccount) {
        if ("<<default account>>".equals(googleSignInAccount.C())) {
            if (s42.g() && context.getPackageManager().hasSystemFeature("cn.google")) {
                this.a = null;
                return;
            }
        }
        this.a = googleSignInAccount;
    }

    @DexIgnore
    public final GoogleSignInAccount a() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof v82) && u12.a(((v82) obj).a, this.a);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        GoogleSignInAccount googleSignInAccount = this.a;
        if (googleSignInAccount != null) {
            return googleSignInAccount.hashCode();
        }
        return 0;
    }
}
