package com.fossil;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends i6 {
        @DexIgnore
        public /* final */ ActivityOptions a;

        @DexIgnore
        public a(ActivityOptions activityOptions) {
            this.a = activityOptions;
        }

        @DexIgnore
        public Bundle a() {
            return this.a.toBundle();
        }
    }

    @DexIgnore
    public static i6 a(Activity activity, u8<View, String>... u8VarArr) {
        if (Build.VERSION.SDK_INT < 21) {
            return new i6();
        }
        Pair[] pairArr = null;
        if (u8VarArr != null) {
            pairArr = new Pair[u8VarArr.length];
            for (int i = 0; i < u8VarArr.length; i++) {
                pairArr[i] = Pair.create(u8VarArr[i].a, u8VarArr[i].b);
            }
        }
        return new a(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    public Bundle a() {
        return null;
    }
}
