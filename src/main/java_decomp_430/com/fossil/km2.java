package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class km2 {
    @DexIgnore
    public km2() {
    }

    @DexIgnore
    public static int a(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    @DexIgnore
    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    @DexIgnore
    public static km2 a(byte[] bArr, int i, int i2, boolean z) {
        mm2 mm2 = new mm2(bArr, 0, i2, false);
        try {
            mm2.b(i2);
            return mm2;
        } catch (qn2 e) {
            throw new IllegalArgumentException(e);
        }
    }
}
