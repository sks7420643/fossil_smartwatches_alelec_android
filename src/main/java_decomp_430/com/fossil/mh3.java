package com.fossil;

import android.content.Context;
import android.util.DisplayMetrics;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mh3 extends LinearLayoutManager {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gg {
        @DexIgnore
        public a(mh3 mh3, Context context) {
            super(context);
        }

        @DexIgnore
        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public mh3(Context context, int i, boolean z) {
        super(context, i, z);
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, RecyclerView.State state, int i) {
        a aVar = new a(this, recyclerView.getContext());
        aVar.c(i);
        b((RecyclerView.v) aVar);
    }
}
