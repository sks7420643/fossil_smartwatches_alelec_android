package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv5 implements Factory<bv5> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<wr4> b;
    @DexIgnore
    public /* final */ Provider<ds4> c;
    @DexIgnore
    public /* final */ Provider<cj4> d;
    @DexIgnore
    public /* final */ Provider<an4> e;
    @DexIgnore
    public /* final */ Provider<yr4> f;
    @DexIgnore
    public /* final */ Provider<rr4> g;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> h;

    @DexIgnore
    public dv5(Provider<DeviceRepository> provider, Provider<wr4> provider2, Provider<ds4> provider3, Provider<cj4> provider4, Provider<an4> provider5, Provider<yr4> provider6, Provider<rr4> provider7, Provider<PortfolioApp> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static dv5 a(Provider<DeviceRepository> provider, Provider<wr4> provider2, Provider<ds4> provider3, Provider<cj4> provider4, Provider<an4> provider5, Provider<yr4> provider6, Provider<rr4> provider7, Provider<PortfolioApp> provider8) {
        return new dv5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static bv5 b(Provider<DeviceRepository> provider, Provider<wr4> provider2, Provider<ds4> provider3, Provider<cj4> provider4, Provider<an4> provider5, Provider<yr4> provider6, Provider<rr4> provider7, Provider<PortfolioApp> provider8) {
        return new bv5(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get());
    }

    @DexIgnore
    public bv5 get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
