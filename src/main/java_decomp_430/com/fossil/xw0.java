package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ x51 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xw0(x51 x51) {
        super(1);
        this.a = x51;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        int i;
        CopyOnWriteArrayList b = this.a.F;
        ArrayList<ie1> arrayList = ((te0) obj).V;
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = arrayList.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((ie1) next).f > 0) {
                i = 1;
            }
            if (i != 0) {
                arrayList2.add(next);
            }
        }
        b.addAll(arrayList2);
        x51 x51 = this.a;
        for (ie1 ie1 : x51.F) {
            i += (int) ie1.f;
        }
        x51.H = cw0.b(i);
        this.a.q();
        this.a.p();
        return cd6.a;
    }
}
