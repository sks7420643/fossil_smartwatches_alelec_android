package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class do2 extends ao2 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public do2() {
        super();
    }

    @DexIgnore
    public static <E> List<E> b(Object obj, long j) {
        return (List) cq2.f(obj, j);
    }

    @DexIgnore
    public final void a(Object obj, long j) {
        Object obj2;
        List list = (List) cq2.f(obj, j);
        if (list instanceof xn2) {
            obj2 = ((xn2) list).n();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof cp2) || !(list instanceof nn2)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                nn2 nn2 = (nn2) list;
                if (nn2.zza()) {
                    nn2.k();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        cq2.a(obj, j, obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: com.fossil.yn2} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: com.fossil.yn2} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: com.fossil.yn2} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static <L> List<L> a(Object obj, long j, int i) {
        yn2 yn2;
        List<L> list;
        List<L> b = b(obj, j);
        if (b.isEmpty()) {
            if (b instanceof xn2) {
                list = new yn2(i);
            } else if (!(b instanceof cp2) || !(b instanceof nn2)) {
                list = new ArrayList<>(i);
            } else {
                list = ((nn2) b).zza(i);
            }
            cq2.a(obj, j, (Object) list);
            return list;
        }
        if (c.isAssignableFrom(b.getClass())) {
            ArrayList arrayList = new ArrayList(b.size() + i);
            arrayList.addAll(b);
            cq2.a(obj, j, (Object) arrayList);
            yn2 = arrayList;
        } else if (b instanceof bq2) {
            yn2 yn22 = new yn2(b.size() + i);
            yn22.addAll((bq2) b);
            cq2.a(obj, j, (Object) yn22);
            yn2 = yn22;
        } else if (!(b instanceof cp2) || !(b instanceof nn2)) {
            return b;
        } else {
            nn2 nn2 = (nn2) b;
            if (nn2.zza()) {
                return b;
            }
            nn2 zza = nn2.zza(b.size() + i);
            cq2.a(obj, j, (Object) zza);
            return zza;
        }
        return yn2;
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        List b = b(obj2, j);
        List a = a(obj, j, b.size());
        int size = a.size();
        int size2 = b.size();
        if (size > 0 && size2 > 0) {
            a.addAll(b);
        }
        if (size > 0) {
            b = a;
        }
        cq2.a(obj, j, (Object) b);
    }
}
