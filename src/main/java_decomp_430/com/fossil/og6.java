package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og6 implements fi6<Object>, ng6 {
    @DexIgnore
    public /* final */ Class<?> a;

    @DexIgnore
    public og6(Class<?> cls) {
        wg6.b(cls, "jClass");
        this.a = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof og6) && wg6.a((Object) eg6.a(this), (Object) eg6.a((fi6) obj));
    }

    @DexIgnore
    public int hashCode() {
        return eg6.a(this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
