package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f41 extends xg6 implements hg6<bn0, Boolean> {
    @DexIgnore
    public static /* final */ f41 a; // = new f41();

    @DexIgnore
    public f41() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return Boolean.valueOf(((bn0) obj).c == il0.BLUETOOTH_OFF);
    }
}
