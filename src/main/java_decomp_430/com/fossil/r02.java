package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r02 implements Parcelable.Creator<Status> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        String str = null;
        PendingIntent pendingIntent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i2 = f22.q(parcel, a);
            } else if (a2 == 2) {
                str = f22.e(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) f22.a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 1000) {
                f22.v(parcel, a);
            } else {
                i = f22.q(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new Status(i, i2, str, pendingIntent);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new Status[i];
    }
}
