package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.wv1;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu1 extends i12<nu1> {
    @DexIgnore
    public /* final */ GoogleSignInOptions E;

    @DexIgnore
    public bu1(Context context, Looper looper, e12 e12, GoogleSignInOptions googleSignInOptions, wv1.b bVar, wv1.c cVar) {
        super(context, looper, 91, e12, bVar, cVar);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.a().a() : googleSignInOptions;
        if (!e12.d().isEmpty()) {
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(googleSignInOptions);
            for (Scope a : e12.d()) {
                aVar.a(a, new Scope[0]);
            }
            googleSignInOptions = aVar.a();
        }
        this.E = googleSignInOptions;
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }

    @DexIgnore
    public final GoogleSignInOptions H() {
        return this.E;
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        if (queryLocalInterface instanceof nu1) {
            return (nu1) queryLocalInterface;
        }
        return new ou1(iBinder);
    }

    @DexIgnore
    public final boolean d() {
        return true;
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final Intent l() {
        return cu1.a(u(), this.E);
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }
}
