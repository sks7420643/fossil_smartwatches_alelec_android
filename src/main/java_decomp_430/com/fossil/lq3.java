package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lq3<T> implements ys3<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Object a; // = c;
    @DexIgnore
    public volatile ys3<T> b;

    @DexIgnore
    public lq3(ys3<T> ys3) {
        this.b = ys3;
    }

    @DexIgnore
    public T get() {
        T t = this.a;
        if (t == c) {
            synchronized (this) {
                t = this.a;
                if (t == c) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
