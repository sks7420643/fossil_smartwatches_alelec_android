package com.fossil;

import android.util.Log;
import com.facebook.internal.Utility;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t52 {
    @DexIgnore
    public static /* final */ t52 d; // = new t52(true, (String) null, (Throwable) null);
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Throwable c;

    @DexIgnore
    public t52(boolean z, String str, Throwable th) {
        this.a = z;
        this.b = str;
        this.c = th;
    }

    @DexIgnore
    public static t52 a(Callable<String> callable) {
        return new v52(callable);
    }

    @DexIgnore
    public static t52 c() {
        return d;
    }

    @DexIgnore
    public final void b() {
        if (!this.a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.c != null) {
                Log.d("GoogleCertificatesRslt", a(), this.c);
            } else {
                Log.d("GoogleCertificatesRslt", a());
            }
        }
    }

    @DexIgnore
    public static t52 a(String str) {
        return new t52(false, str, (Throwable) null);
    }

    @DexIgnore
    public static t52 a(String str, Throwable th) {
        return new t52(false, str, th);
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public static String a(String str, l52 l52, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", new Object[]{z2 ? "debug cert rejected" : "not whitelisted", str, p42.a(g42.a(Utility.HASH_ALGORITHM_SHA1).digest(l52.q())), Boolean.valueOf(z), "12451009.false"});
    }
}
