package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ky4 extends xt4<jy4> {
    @DexIgnore
    int G();

    @DexIgnore
    void L();

    @DexIgnore
    int M();

    @DexIgnore
    void Q();

    @DexIgnore
    void V();

    @DexIgnore
    List<ContactGroup> W();

    @DexIgnore
    void Z();

    @DexIgnore
    void a();

    @DexIgnore
    void a(ContactGroup contactGroup);

    @DexIgnore
    void b();

    @DexIgnore
    void b(ArrayList<Integer> arrayList);

    @DexIgnore
    void c();

    @DexIgnore
    void close();

    @DexIgnore
    void i(String str);

    @DexIgnore
    void l(boolean z);

    @DexIgnore
    void m(List<ContactGroup> list);

    @DexIgnore
    void p(String str);
}
