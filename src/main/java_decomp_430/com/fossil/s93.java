package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s93 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ b03 c; // = new v93(this, this.d.a);
    @DexIgnore
    public /* final */ /* synthetic */ m93 d;

    @DexIgnore
    public s93(m93 m93) {
        this.d = m93;
        this.a = m93.zzm().c();
        this.b = this.a;
    }

    @DexIgnore
    public final void a(long j) {
        this.d.g();
        this.c.c();
        this.a = j;
        this.b = this.a;
    }

    @DexIgnore
    public final void b(long j) {
        this.c.c();
        if (this.a != 0) {
            this.d.k().w.a(this.d.k().w.a() + (j - this.a));
        }
    }

    @DexIgnore
    public final void c() {
        this.d.g();
        a(false, false);
        this.d.n().a(this.d.zzm().c());
    }

    @DexIgnore
    public final void a() {
        this.c.c();
        this.a = 0;
        this.b = this.a;
    }

    @DexIgnore
    public final long b() {
        long c2 = this.d.zzm().c();
        long j = c2 - this.b;
        this.b = c2;
        return j;
    }

    @DexIgnore
    public final boolean a(boolean z, boolean z2) {
        this.d.g();
        this.d.w();
        long c2 = this.d.zzm().c();
        this.d.k().v.a(this.d.zzm().b());
        long j = c2 - this.a;
        if (z || j >= 1000) {
            this.d.k().w.a(j);
            this.d.b().B().a("Recording user engagement, ms", Long.valueOf(j));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j);
            g83.a(this.d.r().A(), bundle, true);
            if (this.d.l().p(this.d.p().A())) {
                if (this.d.l().e(this.d.p().A(), l03.h0)) {
                    if (!z2) {
                        b();
                    }
                } else if (z2) {
                    bundle.putLong("_fr", 1);
                } else {
                    b();
                }
            }
            if (!this.d.l().e(this.d.p().A(), l03.h0) || !z2) {
                this.d.o().a("auto", "_e", bundle);
            }
            this.a = c2;
            this.c.c();
            this.c.a(Math.max(0, 3600000 - this.d.k().w.a()));
            return true;
        }
        this.d.b().B().a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j));
        return false;
    }
}
