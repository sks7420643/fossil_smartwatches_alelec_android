package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s24 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public s24(an4 an4, UserRepository userRepository, HybridPresetDao hybridPresetDao, DeviceDao deviceDao, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp) {
        wg6.b(an4, "mSharedPrefs");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(hybridPresetDao, "mHybridPresetDao");
        wg6.b(deviceDao, "mDeviceDao");
        wg6.b(notificationsRepository, "mNotificationsRepository");
        wg6.b(portfolioApp, "mApp");
    }
}
