package com.fossil;

import android.util.SparseArray;
import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bh extends ug {
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public bh(long j, RenderScript renderScript) {
        super(j, renderScript);
        new SparseArray();
        new SparseArray();
        new SparseArray();
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public long a(tg tgVar) {
        if (tgVar == null) {
            return 0;
        }
        eh e = tgVar.e();
        long b = e.g().b(this.c);
        RenderScript renderScript = this.c;
        long a = renderScript.a(tgVar.a(renderScript), e.a(this.c, b), e.h() * e.g().e());
        tgVar.a(a);
        return a;
    }

    @DexIgnore
    public void a(int i, tg tgVar, tg tgVar2, wg wgVar) {
        tg tgVar3 = tgVar;
        tg tgVar4 = tgVar2;
        if (tgVar3 == null && tgVar4 == null) {
            throw new yg("At least one of ain or aout is required to be non-null.");
        }
        long j = 0;
        long a = tgVar3 != null ? tgVar3.a(this.c) : 0;
        if (tgVar4 != null) {
            j = tgVar4.a(this.c);
        }
        long j2 = j;
        if (wgVar != null) {
            wgVar.a();
            throw null;
        } else if (this.d) {
            long a2 = a(tgVar3);
            long a3 = a(tgVar4);
            RenderScript renderScript = this.c;
            renderScript.a(a(renderScript), i, a2, a3, (byte[]) null, this.d);
        } else {
            RenderScript renderScript2 = this.c;
            renderScript2.a(a(renderScript2), i, a, j2, (byte[]) null, this.d);
        }
    }

    @DexIgnore
    public void a(int i, float f) {
        RenderScript renderScript = this.c;
        renderScript.a(a(renderScript), i, f, this.d);
    }

    @DexIgnore
    public void a(int i, ug ugVar) {
        ug ugVar2 = ugVar;
        long j = 0;
        if (this.d) {
            long a = a((tg) ugVar2);
            RenderScript renderScript = this.c;
            renderScript.a(a(renderScript), i, ugVar2 == null ? 0 : a, this.d);
            return;
        }
        RenderScript renderScript2 = this.c;
        long a2 = a(renderScript2);
        if (ugVar2 != null) {
            j = ugVar2.a(this.c);
        }
        renderScript2.a(a2, i, j, this.d);
    }
}
