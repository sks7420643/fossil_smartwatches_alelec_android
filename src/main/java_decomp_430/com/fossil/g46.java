package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ r36 b;

    @DexIgnore
    public g46(Context context, r36 r36) {
        this.a = context;
        this.b = r36;
    }

    @DexIgnore
    public final void run() {
        Context context = this.a;
        if (context == null) {
            q36.m.d("The Context of StatService.onResume() can not be null!");
        } else {
            q36.a(context, m56.k(context), this.b);
        }
    }
}
