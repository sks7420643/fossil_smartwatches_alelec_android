package com.fossil;

import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt implements vr {
    @DexIgnore
    public static /* final */ n00<Class<?>, byte[]> j; // = new n00<>(50);
    @DexIgnore
    public /* final */ xt b;
    @DexIgnore
    public /* final */ vr c;
    @DexIgnore
    public /* final */ vr d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ xr h;
    @DexIgnore
    public /* final */ bs<?> i;

    @DexIgnore
    public tt(xt xtVar, vr vrVar, vr vrVar2, int i2, int i3, bs<?> bsVar, Class<?> cls, xr xrVar) {
        this.b = xtVar;
        this.c = vrVar;
        this.d = vrVar2;
        this.e = i2;
        this.f = i3;
        this.i = bsVar;
        this.g = cls;
        this.h = xrVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        bs<?> bsVar = this.i;
        if (bsVar != null) {
            bsVar.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
        this.b.put(bArr);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof tt)) {
            return false;
        }
        tt ttVar = (tt) obj;
        if (this.f != ttVar.f || this.e != ttVar.e || !r00.b((Object) this.i, (Object) ttVar.i) || !this.g.equals(ttVar.g) || !this.c.equals(ttVar.c) || !this.d.equals(ttVar.d) || !this.h.equals(ttVar.h)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        bs<?> bsVar = this.i;
        if (bsVar != null) {
            hashCode = (hashCode * 31) + bsVar.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }

    @DexIgnore
    public final byte[] a() {
        byte[] a = j.a(this.g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.g.getName().getBytes(vr.a);
        j.b(this.g, bytes);
        return bytes;
    }
}
