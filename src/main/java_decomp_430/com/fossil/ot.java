package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot {
    @DexIgnore
    public /* final */ Map<vr, ht<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<vr, ht<?>> b; // = new HashMap();

    @DexIgnore
    public ht<?> a(vr vrVar, boolean z) {
        return a(z).get(vrVar);
    }

    @DexIgnore
    public void b(vr vrVar, ht<?> htVar) {
        Map<vr, ht<?>> a2 = a(htVar.h());
        if (htVar.equals(a2.get(vrVar))) {
            a2.remove(vrVar);
        }
    }

    @DexIgnore
    public void a(vr vrVar, ht<?> htVar) {
        a(htVar.h()).put(vrVar, htVar);
    }

    @DexIgnore
    public final Map<vr, ht<?>> a(boolean z) {
        return z ? this.b : this.a;
    }
}
