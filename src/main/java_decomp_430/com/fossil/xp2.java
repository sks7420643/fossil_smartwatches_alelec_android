package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp2 extends RuntimeException {
    @DexIgnore
    public /* final */ List<String> zza; // = null;

    @DexIgnore
    public xp2(ro2 ro2) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
