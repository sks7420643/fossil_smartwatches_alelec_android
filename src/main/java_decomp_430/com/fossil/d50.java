package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d50 extends z40 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<d50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            z40 createFromParcel = z40.CREATOR.createFromParcel(parcel);
            if (createFromParcel != null) {
                return (d50) createFromParcel;
            }
            throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotAlarm");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new d50[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ d50(e50 e50, k50 k50, c50 c50, int i, qg6 qg6) throws IllegalArgumentException {
        this(e50, (i & 2) != 0 ? null : k50, (i & 4) != 0 ? null : c50);
    }

    @DexIgnore
    public d50(e50 e50, k50 k50, c50 c50) throws IllegalArgumentException {
        super(e50, k50, c50);
    }

    @DexIgnore
    public e50 getFireTime() {
        a50[] c = c();
        int length = c.length;
        int i = 0;
        while (i < length) {
            a50 a50 = c[i];
            if (!(a50 instanceof e50)) {
                i++;
            } else if (a50 != null) {
                return (e50) a50;
            } else {
                throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotFireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
