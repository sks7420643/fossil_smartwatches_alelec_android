package com.fossil;

import com.fossil.p60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class m91 extends tg6 implements hg6<byte[], p60> {
    @DexIgnore
    public m91(p60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(p60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyStepGoalConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((p60.a) this.receiver).a((byte[]) obj);
    }
}
