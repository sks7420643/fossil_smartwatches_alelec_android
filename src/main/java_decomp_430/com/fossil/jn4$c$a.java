package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.WechatToken;
import com.portfolio.platform.data.source.remote.WechatApiService;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1", f = "MFLoginWechatManager.kt", l = {}, m = "invokeSuspend")
public final class jn4$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $authToken;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MFLoginWechatManager.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements dx6<WechatToken> {
        @DexIgnore
        public /* final */ /* synthetic */ jn4$c$a a;

        @DexIgnore
        public a(jn4$c$a jn4_c_a) {
            this.a = jn4_c_a;
        }

        @DexIgnore
        public void onFailure(Call<WechatToken> call, Throwable th) {
            wg6.b(call, "call");
            wg6.b(th, "t");
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "getWechatToken onFailure");
            if (th instanceof SocketTimeoutException) {
                this.a.this$0.c.a(MFNetworkReturnCode.CLIENT_TIMEOUT, (gv1) null, "");
            } else {
                this.a.this$0.c.a(601, (gv1) null, "");
            }
        }

        @DexIgnore
        public void onResponse(Call<WechatToken> call, rx6<WechatToken> rx6) {
            wg6.b(call, "call");
            wg6.b(rx6, "response");
            if (rx6.d()) {
                FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "getWechatToken isSuccessful");
                Object a2 = rx6.a();
                if (a2 != null) {
                    wg6.a(a2, "response.body()!!");
                    WechatToken wechatToken = (WechatToken) a2;
                    this.a.this$0.a.a(wechatToken.getOpenId());
                    SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                    signUpSocialAuth.setToken(wechatToken.getAccessToken());
                    signUpSocialAuth.setService("wechat");
                    this.a.this$0.c.a(signUpSocialAuth);
                    return;
                }
                wg6.a();
                throw null;
            }
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.d.a(), "getWechatToken isNotSuccessful");
            this.a.this$0.c.a(600, (gv1) null, rx6.e());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jn4$c$a(MFLoginWechatManager.c cVar, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
        this.$authToken = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        jn4$c$a jn4_c_a = new jn4$c$a(this.this$0, this.$authToken, xe6);
        jn4_c_a.p$ = (il6) obj;
        return jn4_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((jn4$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final Object invokeSuspend(Object obj) {
        String str;
        String str2;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.get.instance());
            WechatApiService a3 = this.this$0.a.a();
            if (a2 == null || (str = a2.getD()) == null) {
                str = "";
            }
            if (a2 == null || (str2 = a2.getE()) == null) {
                str2 = "";
            }
            a3.getWechatToken(str, str2, this.$authToken, "authorization_code").a(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
