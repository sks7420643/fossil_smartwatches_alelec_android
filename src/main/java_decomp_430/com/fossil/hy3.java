package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy3 implements Cloneable {
    @DexIgnore
    public int[] a;
    @DexIgnore
    public int b;

    @DexIgnore
    public hy3() {
        this.b = 0;
        this.a = new int[1];
    }

    @DexIgnore
    public static int[] c(int i) {
        return new int[((i + 31) / 32)];
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return (this.b + 7) / 8;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof hy3)) {
            return false;
        }
        hy3 hy3 = (hy3) obj;
        if (this.b != hy3.b || !Arrays.equals(this.a, hy3.a)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b * 31) + Arrays.hashCode(this.a);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(this.b);
        for (int i = 0; i < this.b; i++) {
            if ((i & 7) == 0) {
                sb.append(' ');
            }
            sb.append(b(i) ? 'X' : '.');
        }
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i) {
        if (i > (this.a.length << 5)) {
            int[] c = c(i);
            int[] iArr = this.a;
            System.arraycopy(iArr, 0, c, 0, iArr.length);
            this.a = c;
        }
    }

    @DexIgnore
    public boolean b(int i) {
        return ((1 << (i & 31)) & this.a[i / 32]) != 0;
    }

    @DexIgnore
    public hy3 clone() {
        return new hy3((int[]) this.a.clone(), this.b);
    }

    @DexIgnore
    public void b(hy3 hy3) {
        if (this.b == hy3.b) {
            int i = 0;
            while (true) {
                int[] iArr = this.a;
                if (i < iArr.length) {
                    iArr[i] = iArr[i] ^ hy3.a[i];
                    i++;
                } else {
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Sizes don't match");
        }
    }

    @DexIgnore
    public hy3(int[] iArr, int i) {
        this.a = iArr;
        this.b = i;
    }

    @DexIgnore
    public void a(boolean z) {
        a(this.b + 1);
        if (z) {
            int[] iArr = this.a;
            int i = this.b;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.b++;
    }

    @DexIgnore
    public void a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        a(this.b + i2);
        while (i2 > 0) {
            boolean z = true;
            if (((i >> (i2 - 1)) & 1) != 1) {
                z = false;
            }
            a(z);
            i2--;
        }
    }

    @DexIgnore
    public void a(hy3 hy3) {
        int i = hy3.b;
        a(this.b + i);
        for (int i2 = 0; i2 < i; i2++) {
            a(hy3.b(i2));
        }
    }

    @DexIgnore
    public void a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i4;
            int i7 = 0;
            for (int i8 = 0; i8 < 8; i8++) {
                if (b(i6)) {
                    i7 |= 1 << (7 - i8);
                }
                i6++;
            }
            bArr[i2 + i5] = (byte) i7;
            i5++;
            i4 = i6;
        }
    }
}
