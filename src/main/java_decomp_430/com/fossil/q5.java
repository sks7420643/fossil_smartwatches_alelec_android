package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q5 extends s5 {
    @DexIgnore
    public i5 c;
    @DexIgnore
    public q5 d;
    @DexIgnore
    public float e;
    @DexIgnore
    public q5 f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public q5 i;
    @DexIgnore
    public r5 j; // = null;
    @DexIgnore
    public int k; // = 1;
    @DexIgnore
    public r5 l; // = null;
    @DexIgnore
    public int m; // = 1;

    @DexIgnore
    public q5(i5 i5Var) {
        this.c = i5Var;
    }

    @DexIgnore
    public String a(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    @DexIgnore
    public void a(q5 q5Var, float f2) {
        if (this.b == 0 || !(this.f == q5Var || this.g == f2)) {
            this.f = q5Var;
            this.g = f2;
            if (this.b == 1) {
                b();
            }
            a();
        }
    }

    @DexIgnore
    public void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void d() {
        super.d();
        this.d = null;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.j = null;
        this.k = 1;
        this.l = null;
        this.m = 1;
        this.f = null;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = null;
        this.h = 0;
    }

    @DexIgnore
    public void e() {
        q5 q5Var;
        q5 q5Var2;
        q5 q5Var3;
        q5 q5Var4;
        q5 q5Var5;
        q5 q5Var6;
        float f2;
        float f3;
        float f4;
        float f5;
        q5 q5Var7;
        boolean z = true;
        if (this.b != 1 && this.h != 4) {
            r5 r5Var = this.j;
            if (r5Var != null) {
                if (r5Var.b == 1) {
                    this.e = ((float) this.k) * r5Var.c;
                } else {
                    return;
                }
            }
            r5 r5Var2 = this.l;
            if (r5Var2 != null) {
                if (r5Var2.b == 1) {
                    float f6 = r5Var2.c;
                } else {
                    return;
                }
            }
            if (this.h == 1 && ((q5Var7 = this.d) == null || q5Var7.b == 1)) {
                q5 q5Var8 = this.d;
                if (q5Var8 == null) {
                    this.f = this;
                    this.g = this.e;
                } else {
                    this.f = q5Var8.f;
                    this.g = q5Var8.g + this.e;
                }
                a();
            } else if (this.h == 2 && (q5Var4 = this.d) != null && q5Var4.b == 1 && (q5Var5 = this.i) != null && (q5Var6 = q5Var5.d) != null && q5Var6.b == 1) {
                if (z4.j() != null) {
                    z4.j().v++;
                }
                this.f = this.d.f;
                q5 q5Var9 = this.i;
                q5Var9.f = q5Var9.d.f;
                i5.d dVar = this.c.c;
                int i2 = 0;
                if (!(dVar == i5.d.RIGHT || dVar == i5.d.BOTTOM)) {
                    z = false;
                }
                if (z) {
                    f3 = this.d.g;
                    f2 = this.i.d.g;
                } else {
                    f3 = this.i.d.g;
                    f2 = this.d.g;
                }
                float f7 = f3 - f2;
                i5 i5Var = this.c;
                i5.d dVar2 = i5Var.c;
                if (dVar2 == i5.d.LEFT || dVar2 == i5.d.RIGHT) {
                    f5 = f7 - ((float) this.c.b.t());
                    f4 = this.c.b.V;
                } else {
                    f5 = f7 - ((float) i5Var.b.j());
                    f4 = this.c.b.W;
                }
                int b = this.c.b();
                int b2 = this.i.c.b();
                if (this.c.g() == this.i.c.g()) {
                    f4 = 0.5f;
                    b2 = 0;
                } else {
                    i2 = b;
                }
                float f8 = (float) i2;
                float f9 = (float) b2;
                float f10 = (f5 - f8) - f9;
                if (z) {
                    q5 q5Var10 = this.i;
                    q5Var10.g = q5Var10.d.g + f9 + (f10 * f4);
                    this.g = (this.d.g - f8) - (f10 * (1.0f - f4));
                } else {
                    this.g = this.d.g + f8 + (f10 * f4);
                    q5 q5Var11 = this.i;
                    q5Var11.g = (q5Var11.d.g - f9) - (f10 * (1.0f - f4));
                }
                a();
                this.i.a();
            } else if (this.h == 3 && (q5Var = this.d) != null && q5Var.b == 1 && (q5Var2 = this.i) != null && (q5Var3 = q5Var2.d) != null && q5Var3.b == 1) {
                if (z4.j() != null) {
                    z4.j().w++;
                }
                q5 q5Var12 = this.d;
                this.f = q5Var12.f;
                q5 q5Var13 = this.i;
                q5 q5Var14 = q5Var13.d;
                q5Var13.f = q5Var14.f;
                this.g = q5Var12.g + this.e;
                q5Var13.g = q5Var14.g + q5Var13.e;
                a();
                this.i.a();
            } else if (this.h == 5) {
                this.c.b.H();
            }
        }
    }

    @DexIgnore
    public float f() {
        return this.g;
    }

    @DexIgnore
    public void g() {
        i5 g2 = this.c.g();
        if (g2 != null) {
            if (g2.g() == this.c) {
                this.h = 4;
                g2.d().h = 4;
            }
            int b = this.c.b();
            i5.d dVar = this.c.c;
            if (dVar == i5.d.RIGHT || dVar == i5.d.BOTTOM) {
                b = -b;
            }
            a(g2.d(), b);
        }
    }

    @DexIgnore
    public String toString() {
        if (this.b != 1) {
            return "{ " + this.c + " UNRESOLVED} type: " + a(this.h);
        } else if (this.f == this) {
            return "[" + this.c + ", RESOLVED: " + this.g + "]  type: " + a(this.h);
        } else {
            return "[" + this.c + ", RESOLVED: " + this.f + ":" + this.g + "] type: " + a(this.h);
        }
    }

    @DexIgnore
    public void b(q5 q5Var, float f2) {
        this.i = q5Var;
    }

    @DexIgnore
    public void b(q5 q5Var, int i2, r5 r5Var) {
        this.i = q5Var;
        this.l = r5Var;
        this.m = i2;
    }

    @DexIgnore
    public void a(int i2, q5 q5Var, int i3) {
        this.h = i2;
        this.d = q5Var;
        this.e = (float) i3;
        this.d.a(this);
    }

    @DexIgnore
    public void a(q5 q5Var, int i2) {
        this.d = q5Var;
        this.e = (float) i2;
        this.d.a(this);
    }

    @DexIgnore
    public void a(q5 q5Var, int i2, r5 r5Var) {
        this.d = q5Var;
        this.d.a(this);
        this.j = r5Var;
        this.k = i2;
        this.j.a(this);
    }

    @DexIgnore
    public void a(z4 z4Var) {
        d5 e2 = this.c.e();
        q5 q5Var = this.f;
        if (q5Var == null) {
            z4Var.a(e2, (int) (this.g + 0.5f));
        } else {
            z4Var.a(e2, z4Var.a((Object) q5Var.c), (int) (this.g + 0.5f), 6);
        }
    }
}
