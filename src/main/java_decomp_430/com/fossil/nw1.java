package com.fossil;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.ew1;
import com.fossil.rv1;
import com.fossil.rv1.b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nw1<R extends ew1, A extends rv1.b> extends BasePendingResult<R> implements ow1<R> {
    @DexIgnore
    public /* final */ rv1.c<A> q;
    @DexIgnore
    public /* final */ rv1<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nw1(rv1<?> rv1, wv1 wv1) {
        super(wv1);
        w12.a(wv1, (Object) "GoogleApiClient must not be null");
        w12.a(rv1, (Object) "Api must not be null");
        this.q = rv1.a();
        this.r = rv1;
    }

    @DexIgnore
    public final void a(RemoteException remoteException) {
        c(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    @DexIgnore
    public abstract void a(A a) throws RemoteException;

    @DexIgnore
    public final void b(A a) throws DeadObjectException {
        if (a instanceof b22) {
            a = ((b22) a).H();
        }
        try {
            a(a);
        } catch (DeadObjectException e) {
            a((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            a(e2);
        }
    }

    @DexIgnore
    public final void c(Status status) {
        w12.a(!status.F(), (Object) "Failed result must not be success");
        ew1 a = a(status);
        a(a);
        d(a);
    }

    @DexIgnore
    public void d(R r2) {
    }

    @DexIgnore
    public final rv1<?> h() {
        return this.r;
    }

    @DexIgnore
    public final rv1.c<A> i() {
        return this.q;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.a((ew1) obj);
    }
}
