package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.Base64;
import android.util.TypedValue;
import android.util.Xml;
import com.facebook.stetho.server.http.HttpStatus;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a7 {

    @DexIgnore
    public interface a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements a {
        @DexIgnore
        public /* final */ c[] a;

        @DexIgnore
        public b(c[] cVarArr) {
            this.a = cVarArr;
        }

        @DexIgnore
        public c[] a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public c(String str, int i, boolean z, String str2, int i2, int i3) {
            this.a = str;
            this.b = i;
            this.c = z;
            this.d = str2;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public String a() {
            return this.a;
        }

        @DexIgnore
        public int b() {
            return this.f;
        }

        @DexIgnore
        public int c() {
            return this.e;
        }

        @DexIgnore
        public String d() {
            return this.d;
        }

        @DexIgnore
        public int e() {
            return this.b;
        }

        @DexIgnore
        public boolean f() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements a {
        @DexIgnore
        public /* final */ i8 a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(i8 i8Var, int i, int i2) {
            this.a = i8Var;
            this.c = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public i8 b() {
            return this.a;
        }

        @DexIgnore
        public int c() {
            return this.b;
        }
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    public static com.fossil.a7.a a(org.xmlpull.v1.XmlPullParser r3, android.content.res.Resources r4) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
        L_0x0000:
            int r0 = r3.next()
            r1 = 2
            if (r0 == r1) goto L_0x000b
            r2 = 1
            if (r0 == r2) goto L_0x000b
            goto L_0x0000
        L_0x000b:
            if (r0 != r1) goto L_0x0012
            com.fossil.a7$a r3 = b(r3, r4)
            return r3
        L_0x0012:
            org.xmlpull.v1.XmlPullParserException r3 = new org.xmlpull.v1.XmlPullParserException
            java.lang.String r4 = "No start tag found"
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a7.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):com.fossil.a7$a");
    }

    @DexIgnore
    public static a b(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, (String) null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return c(xmlPullParser, resources);
        }
        a(xmlPullParser);
        return null;
    }

    @DexIgnore
    public static a c(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), g6.FontFamily);
        String string = obtainAttributes.getString(g6.FontFamily_fontProviderAuthority);
        String string2 = obtainAttributes.getString(g6.FontFamily_fontProviderPackage);
        String string3 = obtainAttributes.getString(g6.FontFamily_fontProviderQuery);
        int resourceId = obtainAttributes.getResourceId(g6.FontFamily_fontProviderCerts, 0);
        int integer = obtainAttributes.getInteger(g6.FontFamily_fontProviderFetchStrategy, 1);
        int integer2 = obtainAttributes.getInteger(g6.FontFamily_fontProviderFetchTimeout, HttpStatus.HTTP_INTERNAL_SERVER_ERROR);
        obtainAttributes.recycle();
        if (string == null || string2 == null || string3 == null) {
            ArrayList arrayList = new ArrayList();
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (xmlPullParser.getName().equals("font")) {
                        arrayList.add(d(xmlPullParser, resources));
                    } else {
                        a(xmlPullParser);
                    }
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            return new b((c[]) arrayList.toArray(new c[arrayList.size()]));
        }
        while (xmlPullParser.next() != 3) {
            a(xmlPullParser);
        }
        return new d(new i8(string, string2, string3, a(resources, resourceId)), integer, integer2);
    }

    @DexIgnore
    public static c d(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), g6.FontFamilyFont);
        int i = obtainAttributes.getInt(obtainAttributes.hasValue(g6.FontFamilyFont_fontWeight) ? g6.FontFamilyFont_fontWeight : g6.FontFamilyFont_android_fontWeight, 400);
        boolean z = 1 == obtainAttributes.getInt(obtainAttributes.hasValue(g6.FontFamilyFont_fontStyle) ? g6.FontFamilyFont_fontStyle : g6.FontFamilyFont_android_fontStyle, 0);
        int i2 = obtainAttributes.hasValue(g6.FontFamilyFont_ttcIndex) ? g6.FontFamilyFont_ttcIndex : g6.FontFamilyFont_android_ttcIndex;
        String string = obtainAttributes.getString(obtainAttributes.hasValue(g6.FontFamilyFont_fontVariationSettings) ? g6.FontFamilyFont_fontVariationSettings : g6.FontFamilyFont_android_fontVariationSettings);
        int i3 = obtainAttributes.getInt(i2, 0);
        int i4 = obtainAttributes.hasValue(g6.FontFamilyFont_font) ? g6.FontFamilyFont_font : g6.FontFamilyFont_android_font;
        int resourceId = obtainAttributes.getResourceId(i4, 0);
        String string2 = obtainAttributes.getString(i4);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            a(xmlPullParser);
        }
        return new c(string2, i, z, string, i3, resourceId);
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return typedArray.getType(i);
        }
        TypedValue typedValue = new TypedValue();
        typedArray.getValue(i, typedValue);
        return typedValue.type;
    }

    @DexIgnore
    public static List<List<byte[]>> a(Resources resources, int i) {
        if (i == 0) {
            return Collections.emptyList();
        }
        TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            if (a(obtainTypedArray, 0) == 1) {
                for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                    int resourceId = obtainTypedArray.getResourceId(i2, 0);
                    if (resourceId != 0) {
                        arrayList.add(a(resources.getStringArray(resourceId)));
                    }
                }
            } else {
                arrayList.add(a(resources.getStringArray(i)));
            }
            obtainTypedArray.recycle();
            return arrayList;
        } finally {
            obtainTypedArray.recycle();
        }
    }

    @DexIgnore
    public static List<byte[]> a(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String decode : strArr) {
            arrayList.add(Base64.decode(decode, 0));
        }
        return arrayList;
    }

    @DexIgnore
    public static void a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            }
        }
    }
}
