package com.fossil;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l02 extends Fragment implements tw1 {
    @DexIgnore
    public static WeakHashMap<Activity, WeakReference<l02>> d; // = new WeakHashMap<>();
    @DexIgnore
    public Map<String, LifecycleCallback> a; // = new p4();
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public Bundle c;

    @DexIgnore
    public static l02 a(Activity activity) {
        l02 l02;
        WeakReference weakReference = d.get(activity);
        if (weakReference != null && (l02 = (l02) weakReference.get()) != null) {
            return l02;
        }
        try {
            l02 l022 = (l02) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            if (l022 == null || l022.isRemoving()) {
                l022 = new l02();
                activity.getFragmentManager().beginTransaction().add(l022, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            d.put(activity, new WeakReference(l022));
            return l022;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
        }
    }

    @DexIgnore
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback a2 : this.a.values()) {
            a2.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback a2 : this.a.values()) {
            a2.a(i, i2, intent);
        }
    }

    @DexIgnore
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = 1;
        this.c = bundle;
        for (Map.Entry next : this.a.entrySet()) {
            ((LifecycleCallback) next.getValue()).a(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    @DexIgnore
    public final void onDestroy() {
        super.onDestroy();
        this.b = 5;
        for (LifecycleCallback b2 : this.a.values()) {
            b2.b();
        }
    }

    @DexIgnore
    public final void onResume() {
        super.onResume();
        this.b = 3;
        for (LifecycleCallback c2 : this.a.values()) {
            c2.c();
        }
    }

    @DexIgnore
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.a.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).b(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    @DexIgnore
    public final void onStart() {
        super.onStart();
        this.b = 2;
        for (LifecycleCallback d2 : this.a.values()) {
            d2.d();
        }
    }

    @DexIgnore
    public final void onStop() {
        super.onStop();
        this.b = 4;
        for (LifecycleCallback e : this.a.values()) {
            e.e();
        }
    }

    @DexIgnore
    public final Activity w0() {
        return getActivity();
    }

    @DexIgnore
    public final <T extends LifecycleCallback> T a(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.a.get(str));
    }

    @DexIgnore
    public final void a(String str, LifecycleCallback lifecycleCallback) {
        if (!this.a.containsKey(str)) {
            this.a.put(str, lifecycleCallback);
            if (this.b > 0) {
                new fb2(Looper.getMainLooper()).post(new m02(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
}
