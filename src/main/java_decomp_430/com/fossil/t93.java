package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t93 implements Runnable {
    @DexIgnore
    public /* final */ r93 a;

    @DexIgnore
    public t93(r93 r93) {
        this.a = r93;
    }

    @DexIgnore
    public final void run() {
        r93 r93 = this.a;
        r93.b.g();
        r93.b.b().A().a("Application backgrounded");
        r93.b.o().b("auto", "_ab", new Bundle());
    }
}
