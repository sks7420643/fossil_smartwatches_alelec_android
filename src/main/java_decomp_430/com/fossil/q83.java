package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ra3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ev2 b;
    @DexIgnore
    public /* final */ /* synthetic */ l83 c;

    @DexIgnore
    public q83(l83 l83, ra3 ra3, ev2 ev2) {
        this.c = l83;
        this.a = ra3;
        this.b = ev2;
    }

    @DexIgnore
    public final void run() {
        try {
            l43 d = this.c.d;
            if (d == null) {
                this.c.b().t().a("Failed to get app instance id");
                return;
            }
            String c2 = d.c(this.a);
            if (c2 != null) {
                this.c.o().a(c2);
                this.c.k().l.a(c2);
            }
            this.c.I();
            this.c.j().a(this.b, c2);
        } catch (RemoteException e) {
            this.c.b().t().a("Failed to get app instance id", e);
        } finally {
            this.c.j().a(this.b, (String) null);
        }
    }
}
