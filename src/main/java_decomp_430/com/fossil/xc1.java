package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc1 implements Parcelable.Creator<se1> {
    @DexIgnore
    public /* synthetic */ xc1(qg6 qg6) {
    }

    @DexIgnore
    public final se1 a(byte b, byte[] bArr) {
        return new se1(b, new vd0(bArr));
    }

    @DexIgnore
    public se1 createFromParcel(Parcel parcel) {
        return new se1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new se1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m70createFromParcel(Parcel parcel) {
        return new se1(parcel, (qg6) null);
    }
}
