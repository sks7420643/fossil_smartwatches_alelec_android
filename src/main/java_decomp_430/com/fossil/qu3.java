package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu3 extends lu3 {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;

    @DexIgnore
    public qu3(String str) {
        super(str);
    }

    @DexIgnore
    public qu3(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public qu3(Throwable th) {
        super(th);
    }
}
