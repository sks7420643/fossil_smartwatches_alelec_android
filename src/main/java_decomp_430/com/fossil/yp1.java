package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp1 implements Factory<wp1> {
    @DexIgnore
    public /* final */ Provider<zs1> a;
    @DexIgnore
    public /* final */ Provider<zs1> b;
    @DexIgnore
    public /* final */ Provider<uq1> c;
    @DexIgnore
    public /* final */ Provider<lr1> d;
    @DexIgnore
    public /* final */ Provider<pr1> e;

    @DexIgnore
    public yp1(Provider<zs1> provider, Provider<zs1> provider2, Provider<uq1> provider3, Provider<lr1> provider4, Provider<pr1> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static yp1 a(Provider<zs1> provider, Provider<zs1> provider2, Provider<uq1> provider3, Provider<lr1> provider4, Provider<pr1> provider5) {
        return new yp1(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public wp1 get() {
        return new wp1((zs1) this.a.get(), (zs1) this.b.get(), (uq1) this.c.get(), (lr1) this.d.get(), (pr1) this.e.get());
    }
}
