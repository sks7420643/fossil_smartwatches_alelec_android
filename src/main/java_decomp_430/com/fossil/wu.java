package com.fossil;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.jv;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wu<Data> implements jv<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;
    @DexIgnore
    public /* final */ a<Data> b;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        fs<Data> a(AssetManager assetManager, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements kv<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public jv<Uri, ParcelFileDescriptor> a(nv nvVar) {
            return new wu(this.a, this);
        }

        @DexIgnore
        public fs<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new js(assetManager, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements kv<Uri, InputStream>, a<InputStream> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public jv<Uri, InputStream> a(nv nvVar) {
            return new wu(this.a, this);
        }

        @DexIgnore
        public fs<InputStream> a(AssetManager assetManager, String str) {
            return new ps(assetManager, str);
        }
    }

    @DexIgnore
    public wu(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    @DexIgnore
    public jv.a<Data> a(Uri uri, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(uri), this.b.a(this.a, uri.toString().substring(c)));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
