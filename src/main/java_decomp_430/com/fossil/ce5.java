package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce5 implements Factory<be5> {
    @DexIgnore
    public static DashboardGoalTrackingPresenter a(ae5 ae5, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, u04 u04) {
        return new DashboardGoalTrackingPresenter(ae5, goalTrackingRepository, userRepository, goalTrackingDao, goalTrackingDatabase, u04);
    }
}
