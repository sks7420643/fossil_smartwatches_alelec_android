package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.dp5;
import com.fossil.gs4;
import com.fossil.jt4;
import com.fossil.m24;
import com.fossil.ws4;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.home.HomeFragment;
import com.portfolio.platform.util.DeviceUtils;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomePresenter extends sw4 implements dp5.b {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((qg6) null);
    @DexIgnore
    public int e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public LiveData<List<InAppNotification>> g;
    @DexIgnore
    public /* final */ f h; // = new f(this);
    @DexIgnore
    public /* final */ h i; // = new h(this);
    @DexIgnore
    public /* final */ g j; // = new g(this);
    @DexIgnore
    public /* final */ tw4 k;
    @DexIgnore
    public /* final */ an4 l;
    @DexIgnore
    public /* final */ DeviceRepository m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase o;
    @DexIgnore
    public /* final */ z24 p;
    @DexIgnore
    public /* final */ cj4 q;
    @DexIgnore
    public /* final */ jt4 r;
    @DexIgnore
    public /* final */ ey5 s;
    @DexIgnore
    public /* final */ ServerSettingRepository t;
    @DexIgnore
    public /* final */ ws4 u;
    @DexIgnore
    public /* final */ dp5 v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomePresenter.w;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1", f = "HomePresenter.kt", l = {265}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1", f = "HomePresenter.kt", l = {265}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = bVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    il6 il6 = this.p$;
                    DeviceUtils a2 = DeviceUtils.g.a();
                    String str = this.this$0.$activeDeviceSerial;
                    this.L$0 = il6;
                    this.label = 1;
                    obj = a2.a(str, (Device) null, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HomePresenter homePresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$activeDeviceSerial, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "checkFirmware() enter checkFirmware");
                dl6 a3 = this.this$0.b();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean booleanValue = ((Boolean) obj).booleanValue();
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.$activeDeviceSerial;
            String a4 = HomePresenter.x.a();
            remote.i(component, session, str, a4, "[Sync OK] Is device has latest FW " + booleanValue);
            if (booleanValue) {
                FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "checkFirmware() fw is latest, skip OTA after sync success");
            } else {
                this.this$0.p();
            }
            FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "checkFirmware() exit checkFirmware");
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.d<jt4.a.c, jt4.a.C0017a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public c(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(jt4.a.c cVar) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "GetServerSettingUseCase onSuccess");
            this.a.a(true);
        }

        @DexIgnore
        public void a(jt4.a.C0017a aVar) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "GetServerSettingUseCase onError");
            this.a.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1", f = "HomePresenter.kt", l = {315}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $hasServerSettings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1", f = "HomePresenter.kt", l = {335, 336}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.HomePresenter$d$a$a")
            @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.HomePresenter$d$a$a  reason: collision with other inner class name */
            public static final class C0000a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0000a(a aVar, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0000a aVar = new C0000a(this.this$0, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0000a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    ff6.a();
                    if (this.label == 0) {
                        nc6.a(obj);
                        this.this$0.this$0.this$0.k.P();
                        return cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = dVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:23:0x00c7, code lost:
                r2 = com.fossil.hf6.a(java.lang.Integer.parseInt(r2));
             */
            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                long j;
                il6 il6;
                ServerSetting serverSetting;
                ServerSetting serverSetting2;
                ServerSetting serverSetting3;
                int i;
                int i2;
                Long a;
                Integer a2;
                Integer a3;
                Object a4 = ff6.a();
                int i3 = this.label;
                if (i3 == 0) {
                    nc6.a(obj);
                    il6 = this.p$;
                    ServerSetting generateSetting = this.this$0.this$0.t.generateSetting("crash-threshold", String.valueOf(3));
                    ServerSetting generateSetting2 = this.this$0.this$0.t.generateSetting("successful-sync-threshold", String.valueOf(4));
                    serverSetting3 = this.this$0.this$0.t.generateSetting("post-sync-threshold", String.valueOf(5));
                    d dVar = this.this$0;
                    if (dVar.$hasServerSettings) {
                        ServerSetting serverSettingByKey = dVar.this$0.t.getServerSettingByKey("crash-threshold");
                        if (serverSettingByKey != null) {
                            generateSetting = serverSettingByKey;
                        }
                        ServerSetting serverSettingByKey2 = this.this$0.this$0.t.getServerSettingByKey("successful-sync-threshold");
                        if (serverSettingByKey2 != null) {
                            generateSetting2 = serverSettingByKey2;
                        }
                        ServerSetting serverSettingByKey3 = this.this$0.this$0.t.getServerSettingByKey("post-sync-threshold");
                        if (serverSettingByKey3 == null) {
                            serverSettingByKey3 = serverSetting3;
                        }
                        serverSetting = generateSetting;
                        serverSetting2 = generateSetting2;
                        serverSetting3 = serverSettingByKey3;
                    } else {
                        serverSetting = generateSetting;
                        serverSetting2 = generateSetting2;
                    }
                    String value = serverSetting.getValue();
                    i2 = (value == null || a3 == null) ? 0 : a3.intValue();
                    String value2 = serverSetting2.getValue();
                    i = (value2 == null || (a2 = hf6.a(Integer.parseInt(value2))) == null) ? 0 : a2.intValue();
                    String value3 = serverSetting3.getValue();
                    j = (value3 == null || (a = hf6.a(Long.parseLong(value3))) == null) ? 0 : a.longValue();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a5 = HomePresenter.x.a();
                    local.d(a5, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                    if (this.this$0.this$0.s.a() && this.this$0.this$0.s.a(i2, i)) {
                        this.L$0 = il6;
                        this.L$1 = serverSetting;
                        this.L$2 = serverSetting2;
                        this.L$3 = serverSetting3;
                        this.I$0 = i2;
                        this.I$1 = i;
                        this.J$0 = j;
                        this.label = 1;
                        if (ul6.a(j, this) == a4) {
                            return a4;
                        }
                    }
                    return cd6.a;
                } else if (i3 == 1) {
                    long j2 = this.J$0;
                    int i4 = this.I$1;
                    int i5 = this.I$0;
                    serverSetting3 = (ServerSetting) this.L$3;
                    serverSetting2 = (ServerSetting) this.L$2;
                    serverSetting = (ServerSetting) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    j = j2;
                    i = i4;
                    i2 = i5;
                } else if (i3 == 2) {
                    ServerSetting serverSetting4 = (ServerSetting) this.L$3;
                    ServerSetting serverSetting5 = (ServerSetting) this.L$2;
                    ServerSetting serverSetting6 = (ServerSetting) this.L$1;
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                    this.this$0.this$0.l.s(false);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                cn6 j3 = this.this$0.this$0.d();
                C0000a aVar = new C0000a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = serverSetting;
                this.L$2 = serverSetting2;
                this.L$3 = serverSetting3;
                this.I$0 = i2;
                this.I$1 = i;
                this.J$0 = j;
                this.label = 2;
                if (gk6.a(j3, aVar, this) == a4) {
                    return a4;
                }
                this.this$0.this$0.l.s(false);
                return cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HomePresenter homePresenter, boolean z, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$hasServerSettings = z;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$hasServerSettings, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1", f = "HomePresenter.kt", l = {236}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    return this.this$0.this$0.m.getDeviceBySerial(this.this$0.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HomePresenter homePresenter, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$activeSerial = str;
            this.$lastFwTemp = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$activeSerial, this.$lastFwTemp, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            String str = null;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HomePresenter.x.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
            if (device != null) {
                str = device.getFirmwareRevision();
            }
            sb.append(str);
            local.d(a3, sb.toString());
            if (device != null && !xj6.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "Handle OTA complete on check last OTA success");
                this.this$0.l.n(this.$activeSerial);
                this.this$0.l.a(this.this$0.n.e(), -1);
                this.this$0.k.b(true);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public f(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && xj6.b(stringExtra, this.a.n.e(), true)) {
                this.a.k.a(intent);
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                int intExtra3 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomePresenter.x.a();
                local.d(a2, "Inside .syncReceiver syncResult=" + intExtra + ", serial=" + stringExtra + ", " + "errorCode=" + intExtra2 + " permissionErrors " + integerArrayListExtra + " originalSyncMode " + intExtra3);
                if (intExtra == 1) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    wg6.a((Object) stringExtra, "serial");
                    remote.i(component, session, stringExtra, HomePresenter.x.a(), "[Sync OK] Check FW to OTA");
                    this.a.k();
                    this.a.l();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public g(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            this.a.b(false);
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            boolean C = this.a.n.C();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.x.a();
            local.d(a2, "ota complete isSuccess" + booleanExtra + " isDeviceStillOta " + C);
            if (!C || !booleanExtra) {
                this.a.k.b(booleanExtra);
            }
            if (booleanExtra) {
                this.a.l.a(this.a.n.e(), -1);
                PortfolioApp.get.instance().a(this.a.q, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public h(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.x.a();
            local.d(a2, "---Inside .otaProgressReceiver ota progress " + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial())) {
                if (!this.a.n()) {
                    this.a.b(true);
                }
                if (xj6.b(otaEvent.getSerial(), this.a.n.e(), true)) {
                    this.a.k.c((int) otaEvent.getProcess());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements m24.e<ws4.d, ws4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ ws4.d $responseValue;

            @DexIgnore
            public a(ws4.d dVar) {
                this.$responseValue = dVar;
            }

            @DexIgnore
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = HomePresenter.x.a();
                local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            public String getRequestSubject() {
                return this.$responseValue.d();
            }

            @DexIgnore
            public List<String> getTags() {
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        public i(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(ws4.b bVar) {
            wg6.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "startCollectingUserFeedback onError");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ws4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomePresenter.x.a(), "startCollectingUserFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.k.a((ZendeskFeedbackConfiguration) new a(dVar));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements m24.e<gs4.d, gs4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        public j(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void a(UpdateFirmwareUsecase.c cVar) {
            wg6.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateFirmwareUsecase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.b(true);
            this.a.k.I();
        }
    }

    /*
    static {
        String simpleName = HomePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "HomePresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public HomePresenter(tw4 tw4, an4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, z24 z24, cj4 cj4, jt4 jt4, ey5 ey5, ServerSettingRepository serverSettingRepository, ws4 ws4, dp5 dp5) {
        wg6.b(tw4, "mView");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(portfolioApp, "mApp");
        wg6.b(updateFirmwareUsecase, "mUpdateFwUseCase");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(jt4, "mGetServerSettingUseCase");
        wg6.b(ey5, "mUserUtils");
        wg6.b(serverSettingRepository, "mServerSettingRepository");
        wg6.b(ws4, "mGetZendeskInformation");
        wg6.b(dp5, "mInAppManager");
        this.k = tw4;
        this.l = an4;
        this.m = deviceRepository;
        this.n = portfolioApp;
        this.o = updateFirmwareUsecase;
        this.p = z24;
        this.q = cj4;
        this.r = jt4;
        this.s = ey5;
        this.t = serverSettingRepository;
        this.u = ws4;
        this.v = dp5;
    }

    @DexIgnore
    public final void l() {
        this.p.a(this.r, new jt4.a.b(), new c(this));
    }

    @DexIgnore
    public final void m() {
        String e2 = this.n.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "Inside .checkLastOTASuccess, activeSerial=" + e2);
        if (!TextUtils.isEmpty(e2)) {
            String i2 = this.l.i(e2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = w;
            local2.d(str2, "Inside .checkLastOTASuccess, lastTempFw=" + i2);
            if (!TextUtils.isEmpty(i2)) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, e2, i2, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public final boolean n() {
        return this.f;
    }

    @DexIgnore
    public void o() {
        this.k.a(this);
        this.v.a((dp5.b) this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.HomePresenter$j] */
    public final void p() {
        FLogger.INSTANCE.getLocal().d(w, "Inside .updateFirmware");
        String e2 = this.n.e();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, e2, w, "[Sync OK] Start update FW");
        if (!TextUtils.isEmpty(e2)) {
            this.o.a(new UpdateFirmwareUsecase.b(e2, false, 2, (qg6) null), new j(this));
        }
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "setTab " + i2);
        this.e = i2;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.f = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, "start");
        Object r0 = this.n;
        h hVar = this.i;
        r0.registerReceiver(hVar, new IntentFilter(this.n.getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.j, CommunicateMode.OTA);
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.h, CommunicateMode.SYNC);
        BleCommandResultManager.d.a(CommunicateMode.SYNC, CommunicateMode.OTA);
        m();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        try {
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.j, CommunicateMode.OTA);
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.h, CommunicateMode.SYNC);
            this.n.unregisterReceiver(this.i);
            LiveData<List<InAppNotification>> liveData = this.g;
            if (liveData != null) {
                tw4 tw4 = this.k;
                if (tw4 != null) {
                    liveData.a((HomeFragment) tw4);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "Exception when stop presenter=" + e2);
        }
    }

    @DexIgnore
    public int h() {
        return this.e;
    }

    @DexIgnore
    public boolean i() {
        return this.f || this.n.C();
    }

    @DexIgnore
    public void j() {
        this.v.a();
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "checkFirmware(), isSkipOTA=" + this.l.V());
        if (!PortfolioApp.get.e() || !this.l.V()) {
            String e2 = this.n.e();
            boolean C = this.n.C();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = w;
            local2.d(str2, "checkFirmware() isDeviceOtaing=" + C);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str3 = w;
            remote.i(component, session, e2, str3, "[Sync OK] Is device OTAing " + C);
            if (!C) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, e2, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.ws4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.fossil.HomePresenter$i, com.portfolio.platform.CoroutineUseCase$e] */
    public void b(String str) {
        wg6.b(str, "subject");
        this.u.a(new ws4.c(str), new i(this));
    }

    @DexIgnore
    public void a(InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        this.k.a(inAppNotification);
    }

    @DexIgnore
    public void a(String str) {
        int i2;
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "on active serial change serial " + str);
        if (TextUtils.isEmpty(this.n.w().y())) {
            i2 = 0;
        } else {
            i2 = (TextUtils.isEmpty(str) || !DeviceHelper.o.e(str)) ? 1 : 2;
        }
        if (!TextUtils.isEmpty(str)) {
            this.k.a(FossilDeviceSerialPatternUtil.getDeviceBySerial(str), i2);
        } else {
            this.k.a((FossilDeviceSerialPatternUtil.DEVICE) null, i2);
        }
    }

    @DexIgnore
    public void b(InAppNotification inAppNotification) {
        if (inAppNotification != null) {
            this.v.a(inAppNotification);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, z, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.k.onActivityResult(i2, i3, intent);
    }
}
