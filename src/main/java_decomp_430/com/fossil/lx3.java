package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lx3 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7466929953374883507L;
    @DexIgnore
    public /* final */ List<String> missingFields;

    @DexIgnore
    public lx3(dx3 dx3) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
        this.missingFields = null;
    }

    @DexIgnore
    public static String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Message missing required fields: ");
        boolean z = true;
        for (String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        return sb.toString();
    }

    @DexIgnore
    public ax3 asInvalidProtocolBufferException() {
        return new ax3(getMessage());
    }

    @DexIgnore
    public List<String> getMissingFields() {
        return Collections.unmodifiableList(this.missingFields);
    }

    @DexIgnore
    public lx3(List<String> list) {
        super(a(list));
        this.missingFields = list;
    }
}
