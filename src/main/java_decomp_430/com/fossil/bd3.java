package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ad3 a;

    @DexIgnore
    public bd3(ad3 ad3) {
        this.a = ad3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a.b) {
            if (this.a.c != null) {
                this.a.c.onCanceled();
            }
        }
    }
}
