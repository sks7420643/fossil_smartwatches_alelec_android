package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hd6<T> implements Iterator<T>, ph6 {
    @DexIgnore
    public ne6 a; // = ne6.NotReady;
    @DexIgnore
    public T b;

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b(T t) {
        this.b = t;
        this.a = ne6.Ready;
    }

    @DexIgnore
    public final boolean c() {
        this.a = ne6.Failed;
        a();
        return this.a == ne6.Ready;
    }

    @DexIgnore
    public boolean hasNext() {
        if (this.a != ne6.Failed) {
            int i = gd6.a[this.a.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                return c();
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public T next() {
        if (hasNext()) {
            this.a = ne6.NotReady;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final void b() {
        this.a = ne6.Done;
    }
}
