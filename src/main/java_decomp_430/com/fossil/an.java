package com.fossil;

import android.content.Context;
import com.fossil.dn;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class an implements dn.a {
    @DexIgnore
    public static /* final */ String d; // = tl.a("WorkConstraintsTracker");
    @DexIgnore
    public /* final */ zm a;
    @DexIgnore
    public /* final */ dn<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public an(Context context, to toVar, zm zmVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = zmVar;
        this.b = new dn[]{new bn(applicationContext, toVar), new cn(applicationContext, toVar), new in(applicationContext, toVar), new en(applicationContext, toVar), new hn(applicationContext, toVar), new gn(applicationContext, toVar), new fn(applicationContext, toVar)};
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            for (dn<?> a2 : this.b) {
                a2.a();
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.a != null) {
                this.a.a(list);
            }
        }
    }

    @DexIgnore
    public void c(List<zn> list) {
        synchronized (this.c) {
            for (dn<?> a2 : this.b) {
                a2.a((dn.a) null);
            }
            for (dn<?> a3 : this.b) {
                a3.a(list);
            }
            for (dn<?> a4 : this.b) {
                a4.a((dn.a) this);
            }
        }
    }

    @DexIgnore
    public boolean a(String str) {
        synchronized (this.c) {
            for (dn<?> dnVar : this.b) {
                if (dnVar.a(str)) {
                    tl.a().a(d, String.format("Work %s constrained by %s", new Object[]{str, dnVar.getClass().getSimpleName()}), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                if (a(next)) {
                    tl.a().a(d, String.format("Constraints met for %s", new Object[]{next}), new Throwable[0]);
                    arrayList.add(next);
                }
            }
            if (this.a != null) {
                this.a.b(arrayList);
            }
        }
    }
}
