package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb1 extends em0 {
    @DexIgnore
    public static /* final */ g91 CREATOR; // = new g91((qg6) null);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ JSONObject d;

    @DexIgnore
    public cb1(byte b, int i, JSONObject jSONObject) {
        super(xh0.JSON_FILE_EVENT, b);
        this.c = i;
        this.d = jSONObject;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.REQUEST_ID, (Object) Integer.valueOf(this.c)), bm0.REQUEST_DATA, (Object) this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(cb1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            cb1 cb1 = (cb1) obj;
            return this.a == cb1.a && this.b == cb1.b && this.c == cb1.c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a.hashCode() * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.toString());
        }
    }

    @DexIgnore
    public /* synthetic */ cb1(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.d = new JSONObject(readString);
        } else {
            wg6.a();
            throw null;
        }
    }
}
