package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class do1 {
    @DexIgnore
    public static long a; // = 1800000;
    @DexIgnore
    public static /* final */ do1 b; // = new do1();

    @DexIgnore
    public final boolean a() {
        return gk0.f.a() != null;
    }

    @DexIgnore
    public final long b() {
        return a;
    }

    @DexIgnore
    public final String c() {
        return null;
    }

    @DexIgnore
    public final String d() {
        return gk0.f.e();
    }

    @DexIgnore
    public final void a(Context context) throws IllegalArgumentException, IllegalStateException {
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            gk0.f.a(context);
            oa1 oa1 = oa1.a;
            new Object[1][0] = "5.10.2-production-release";
            s81.d.a(context);
            k40.l.a(context);
            System.loadLibrary("FitnessAlgorithm");
            System.loadLibrary("EllipticCurveCrypto");
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void a(ge0 ge0) {
        gk0.f.a(ge0);
    }

    @DexIgnore
    public final void a(String str) {
        gk0.f.a(str);
        xq0.e.a().c = str;
    }
}
