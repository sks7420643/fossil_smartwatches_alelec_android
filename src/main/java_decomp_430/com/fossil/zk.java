package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseIntArray;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zk extends yk {
    @DexIgnore
    public /* final */ SparseIntArray d;
    @DexIgnore
    public /* final */ Parcel e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;

    @DexIgnore
    public zk(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new p4(), new p4(), new p4());
    }

    @DexIgnore
    public boolean a(int i2) {
        while (this.j < this.g) {
            int i3 = this.k;
            if (i3 == i2) {
                return true;
            }
            if (String.valueOf(i3).compareTo(String.valueOf(i2)) > 0) {
                return false;
            }
            this.e.setDataPosition(this.j);
            int readInt = this.e.readInt();
            this.k = this.e.readInt();
            this.j += readInt;
        }
        if (this.k == i2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void b(int i2) {
        a();
        this.i = i2;
        this.d.put(i2, this.e.dataPosition());
        c(0);
        c(i2);
    }

    @DexIgnore
    public void c(int i2) {
        this.e.writeInt(i2);
    }

    @DexIgnore
    public boolean d() {
        return this.e.readInt() != 0;
    }

    @DexIgnore
    public byte[] e() {
        int readInt = this.e.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.e.readByteArray(bArr);
        return bArr;
    }

    @DexIgnore
    public CharSequence f() {
        return (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this.e);
    }

    @DexIgnore
    public int g() {
        return this.e.readInt();
    }

    @DexIgnore
    public <T extends Parcelable> T h() {
        return this.e.readParcelable(zk.class.getClassLoader());
    }

    @DexIgnore
    public String i() {
        return this.e.readString();
    }

    @DexIgnore
    public zk(Parcel parcel, int i2, int i3, String str, p4<String, Method> p4Var, p4<String, Method> p4Var2, p4<String, Class> p4Var3) {
        super(p4Var, p4Var2, p4Var3);
        this.d = new SparseIntArray();
        this.i = -1;
        this.j = 0;
        this.k = -1;
        this.e = parcel;
        this.f = i2;
        this.g = i3;
        this.j = this.f;
        this.h = str;
    }

    @DexIgnore
    public yk b() {
        Parcel parcel = this.e;
        int dataPosition = parcel.dataPosition();
        int i2 = this.j;
        if (i2 == this.f) {
            i2 = this.g;
        }
        int i3 = i2;
        return new zk(parcel, dataPosition, i3, this.h + "  ", this.a, this.b, this.c);
    }

    @DexIgnore
    public void b(String str) {
        this.e.writeString(str);
    }

    @DexIgnore
    public void a() {
        int i2 = this.i;
        if (i2 >= 0) {
            int i3 = this.d.get(i2);
            int dataPosition = this.e.dataPosition();
            this.e.setDataPosition(i3);
            this.e.writeInt(dataPosition - i3);
            this.e.setDataPosition(dataPosition);
        }
    }

    @DexIgnore
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.e.writeInt(bArr.length);
            this.e.writeByteArray(bArr);
            return;
        }
        this.e.writeInt(-1);
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        this.e.writeParcelable(parcelable, 0);
    }

    @DexIgnore
    public void a(boolean z) {
        this.e.writeInt(z ? 1 : 0);
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        TextUtils.writeToParcel(charSequence, this.e, 0);
    }
}
