package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb3 extends b62<hb3> {
    @DexIgnore
    public static /* final */ jb3 c; // = new jb3();

    @DexIgnore
    public jb3() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View a(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) z52.e(((hb3) c.a(context)).a(z52.a(context), i, i2, str, i3));
            } catch (Exception unused) {
                return new gb3(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof hb3 ? (hb3) queryLocalInterface : new ib3(iBinder);
    }
}
