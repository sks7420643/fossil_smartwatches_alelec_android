package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vq0 {
    DST_STANDARD((byte) 0),
    DST_HALF((byte) 2),
    DST_SINGLE((byte) 4),
    DST_DOUBLE((byte) 8),
    DST_UNKNOWN((byte) 255);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public vq0(byte b) {
        this.a = b;
    }
}
