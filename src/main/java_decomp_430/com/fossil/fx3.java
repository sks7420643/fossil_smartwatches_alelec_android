package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.yw3;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx3 {
    @DexIgnore
    public static String a(dx3 dx3, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(str);
        a(dx3, sb, 0);
        return sb.toString();
    }

    @DexIgnore
    public static void a(dx3 dx3, StringBuilder sb, int i) {
        boolean z;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        TreeSet<String> treeSet = new TreeSet<>();
        for (Method method : dx3.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    treeSet.add(method.getName());
                }
            }
        }
        for (String replaceFirst : treeSet) {
            String replaceFirst2 = replaceFirst.replaceFirst("get", "");
            if (replaceFirst2.endsWith("List") && !replaceFirst2.endsWith("OrBuilderList")) {
                String str = replaceFirst2.substring(0, 1).toLowerCase() + replaceFirst2.substring(1, replaceFirst2.length() - 4);
                Method method2 = (Method) hashMap.get("get" + replaceFirst2);
                if (method2 != null) {
                    a(sb, i, a(str), yw3.a(method2, (Object) dx3, new Object[0]));
                }
            }
            if (((Method) hashMap2.get("set" + replaceFirst2)) != null) {
                if (replaceFirst2.endsWith("Bytes")) {
                    if (hashMap.containsKey("get" + replaceFirst2.substring(0, replaceFirst2.length() - 5))) {
                    }
                }
                String str2 = replaceFirst2.substring(0, 1).toLowerCase() + replaceFirst2.substring(1);
                Method method3 = (Method) hashMap.get("get" + replaceFirst2);
                Method method4 = (Method) hashMap.get("has" + replaceFirst2);
                if (method3 != null) {
                    Object a = yw3.a(method3, (Object) dx3, new Object[0]);
                    if (method4 == null) {
                        z = !a(a);
                    } else {
                        z = ((Boolean) yw3.a(method4, (Object) dx3, new Object[0])).booleanValue();
                    }
                    if (z) {
                        a(sb, i, a(str2), a);
                    }
                }
            }
        }
        if (dx3 instanceof yw3.e) {
            Iterator<Map.Entry<yw3.g, Object>> b = ((yw3.e) dx3).d.b();
            while (b.hasNext()) {
                Map.Entry next = b.next();
                a(sb, i, "[" + ((yw3.g) next.getKey()).a() + "]", next.getValue());
            }
        }
        mx3 mx3 = ((yw3) dx3).b;
        if (mx3 != null) {
            mx3.a(sb, i);
        }
    }

    @DexIgnore
    public static boolean a(Object obj) {
        if (obj instanceof Boolean) {
            return !((Boolean) obj).booleanValue();
        }
        if (obj instanceof Integer) {
            if (((Integer) obj).intValue() == 0) {
                return true;
            }
            return false;
        } else if (obj instanceof Float) {
            if (((Float) obj).floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return true;
            }
            return false;
        } else if (obj instanceof Double) {
            if (((Double) obj).doubleValue() == 0.0d) {
                return true;
            }
            return false;
        } else if (obj instanceof String) {
            return obj.equals("");
        } else {
            if (obj instanceof sw3) {
                return obj.equals(sw3.EMPTY);
            }
            if (obj instanceof dx3) {
                if (obj == ((dx3) obj).a()) {
                    return true;
                }
                return false;
            } else if (!(obj instanceof Enum) || ((Enum) obj).ordinal() != 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    @DexIgnore
    public static final void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj instanceof List) {
            for (Object a : (List) obj) {
                a(sb, i, str, a);
            }
            return;
        }
        sb.append(10);
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(' ');
        }
        sb.append(str);
        if (obj instanceof String) {
            sb.append(": \"");
            sb.append(kx3.a((String) obj));
            sb.append('\"');
        } else if (obj instanceof sw3) {
            sb.append(": \"");
            sb.append(kx3.a((sw3) obj));
            sb.append('\"');
        } else if (obj instanceof yw3) {
            sb.append(" {");
            a((yw3) obj, sb, i + 2);
            sb.append("\n");
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append("}");
        } else {
            sb.append(": ");
            sb.append(obj.toString());
        }
    }

    @DexIgnore
    public static final String a(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }
}
