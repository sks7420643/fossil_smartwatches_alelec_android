package com.fossil;

import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs4 extends y24<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vs4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            wg6.b(str, "oldPass");
            wg6.b(str2, "newPass");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements y24.c {
    }

    /*
    static {
        String simpleName = vs4.class.getSimpleName();
        wg6.a((Object) simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public vs4(AuthApiUserService authApiUserService) {
        wg6.b(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends kp4<ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ vs4 a;

        @DexIgnore
        public e(vs4 vs4) {
            this.a = vs4;
        }

        @DexIgnore
        public void a(Call<ku3> call, rx6<ku3> rx6) {
            wg6.b(call, "call");
            wg6.b(rx6, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vs4.f.a();
            local.d(a2, "changePassword onSuccessResponse response=" + rx6);
            this.a.a().onSuccess(new d());
        }

        @DexIgnore
        public void a(Call<ku3> call, ServerError serverError) {
            wg6.b(call, "call");
            wg6.b(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vs4.f.a();
            local.d(a2, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            y24.d a3 = this.a.a();
            Integer code = serverError.getCode();
            wg6.a((Object) code, "response.code");
            a3.a(new c(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        public void a(Call<ku3> call, Throwable th) {
            wg6.b(call, "call");
            wg6.b(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vs4.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(cd6.a);
            local.d(a2, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.a.a().a(new c(MFNetworkReturnCode.CLIENT_TIMEOUT, (String) null));
            } else {
                this.a.a().a(new c(601, (String) null));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(b bVar) {
        wg6.b(bVar, "requestValues");
        if (!hx5.b(PortfolioApp.get.instance())) {
            a().a(new c(601, ""));
            return;
        }
        ku3 ku3 = new ku3();
        try {
            ku3.a("oldPassword", bVar.b());
            ku3.a("newPassword", bVar.a());
        } catch (Exception unused) {
        }
        this.d.changePassword(ku3).a(new e(this));
    }
}
