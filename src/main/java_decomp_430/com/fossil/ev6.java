package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ev6 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ fv6 b;
    @DexIgnore
    public /* final */ mv6 c;

    @DexIgnore
    public ev6(String str, mv6 mv6) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (mv6 != null) {
            this.a = str;
            this.c = mv6;
            this.b = new fv6();
            a(mv6);
            b(mv6);
            c(mv6);
        } else {
            throw new IllegalArgumentException("Body may not be null");
        }
    }

    @DexIgnore
    public mv6 a() {
        return this.c;
    }

    @DexIgnore
    public fv6 b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (str != null) {
            this.b.a(new jv6(str, str2));
            return;
        }
        throw new IllegalArgumentException("Field name may not be null");
    }

    @DexIgnore
    public void b(mv6 mv6) {
        StringBuilder sb = new StringBuilder();
        sb.append(mv6.c());
        if (mv6.b() != null) {
            sb.append("; charset=");
            sb.append(mv6.b());
        }
        a("Content-Type", sb.toString());
    }

    @DexIgnore
    public void c(mv6 mv6) {
        a("Content-Transfer-Encoding", mv6.a());
    }

    @DexIgnore
    public void a(mv6 mv6) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(c());
        sb.append("\"");
        if (mv6.d() != null) {
            sb.append("; filename=\"");
            sb.append(mv6.d());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }
}
