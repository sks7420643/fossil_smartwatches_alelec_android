package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wf5$b$a<T> implements ld<yx5<? extends List<DailyHeartRateSummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter.b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1", f = "HeartRateOverviewWeekPresenter.kt", l = {62}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $data;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wf5$b$a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wf5$b$a$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$2$1$listRestingDataPoint$1", f = "HeartRateOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.wf5$b$a$a$a  reason: collision with other inner class name */
        public static final class C0051a extends sf6 implements ig6<il6, xe6<? super List<? extends Integer>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0051a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0051a aVar = new C0051a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0051a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    List<DailyHeartRateSummary> list = this.this$0.$data;
                    if (list == null) {
                        return null;
                    }
                    ArrayList arrayList = new ArrayList(rd6.a(list, 10));
                    for (DailyHeartRateSummary resting : list) {
                        Resting resting2 = resting.getResting();
                        arrayList.add(resting2 != null ? hf6.a(resting2.getValue()) : null);
                    }
                    return arrayList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wf5$b$a wf5_b_a, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = wf5_b_a;
            this.$data = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$data, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.a.this$0.b();
                C0051a aVar = new C0051a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, aVar, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) obj;
            if (list != null) {
                this.this$0.a.this$0.h.a(yd6.d(list), this.this$0.a.this$0.g);
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public wf5$b$a(HeartRateOverviewWeekPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<DailyHeartRateSummary>> yx5) {
        wh4 a2 = yx5.a();
        List list = (List) yx5.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - mHeartRateSummaries -- heartRateSummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("HeartRateOverviewWeekPresenter", sb.toString());
        if (a2 != wh4.DATABASE_LOADING) {
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, list, (xe6) null), 3, (Object) null);
        }
    }
}
