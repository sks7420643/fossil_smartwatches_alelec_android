package com.fossil;

import com.fossil.gs;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fx implements gs<ByteBuffer> {
    @DexIgnore
    public /* final */ ByteBuffer a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements gs.a<ByteBuffer> {
        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }

        @DexIgnore
        public gs<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new fx(byteBuffer);
        }
    }

    @DexIgnore
    public fx(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public ByteBuffer b() {
        this.a.position(0);
        return this.a;
    }
}
