package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j55 {
    @DexIgnore
    public /* final */ z45 a;

    @DexIgnore
    public j55(z45 z45) {
        wg6.b(z45, "mCommuteTimeSettingsContractView");
        this.a = z45;
    }

    @DexIgnore
    public final z45 a() {
        return this.a;
    }
}
