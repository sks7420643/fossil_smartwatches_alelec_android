package com.fossil;

import com.fossil.rv1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux1 extends yx1 {
    @DexIgnore
    public /* final */ ArrayList<rv1.f> b;
    @DexIgnore
    public /* final */ /* synthetic */ ox1 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ux1(ox1 ox1, ArrayList<rv1.f> arrayList) {
        super(ox1, (nx1) null);
        this.c = ox1;
        this.b = arrayList;
    }

    @DexIgnore
    public final void a() {
        this.c.a.r.q = this.c.i();
        ArrayList<rv1.f> arrayList = this.b;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            rv1.f fVar = arrayList.get(i);
            i++;
            fVar.a(this.c.o, this.c.a.r.q);
        }
    }
}
