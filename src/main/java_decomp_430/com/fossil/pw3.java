package com.fossil;

import com.fossil.dx3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pw3<MessageType extends dx3> implements gx3<MessageType> {
    /*
    static {
        ww3.a();
    }
    */

    @DexIgnore
    public final lx3 b(MessageType messagetype) {
        if (messagetype instanceof ow3) {
            return ((ow3) messagetype).f();
        }
        return new lx3((dx3) messagetype);
    }

    @DexIgnore
    public final MessageType a(MessageType messagetype) throws ax3 {
        if (messagetype == null || messagetype.b()) {
            return messagetype;
        }
        throw b(messagetype).asInvalidProtocolBufferException().setUnfinishedMessage(messagetype);
    }

    @DexIgnore
    public MessageType b(sw3 sw3, ww3 ww3) throws ax3 {
        MessageType messagetype;
        try {
            tw3 newCodedInput = sw3.newCodedInput();
            messagetype = (dx3) a(newCodedInput, ww3);
            newCodedInput.a(0);
            return messagetype;
        } catch (ax3 e) {
            throw e.setUnfinishedMessage(messagetype);
        } catch (ax3 e2) {
            throw e2;
        }
    }

    @DexIgnore
    public MessageType a(sw3 sw3, ww3 ww3) throws ax3 {
        MessageType b = b(sw3, ww3);
        a(b);
        return b;
    }
}
