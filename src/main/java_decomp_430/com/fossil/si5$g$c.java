package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class si5$g$c extends sf6 implements ig6<il6, xe6<? super Integer>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si5$g$c(HeartRateDetailPresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        si5$g$c si5_g_c = new si5$g$c(this.this$0, xe6);
        si5_g_c.p$ = (il6) obj;
        return si5_g_c;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((si5$g$c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        int i;
        Object obj2;
        Integer a;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            List f = this.this$0.this$0.k;
            if (f != null) {
                Iterator it = f.iterator();
                if (!it.hasNext()) {
                    obj2 = null;
                } else {
                    obj2 = it.next();
                    if (it.hasNext()) {
                        Integer a2 = hf6.a(((HeartRateSample) obj2).getMax());
                        do {
                            Object next = it.next();
                            Integer a3 = hf6.a(((HeartRateSample) next).getMax());
                            if (a2.compareTo(a3) < 0) {
                                obj2 = next;
                                a2 = a3;
                            }
                        } while (it.hasNext());
                    }
                }
                HeartRateSample heartRateSample = (HeartRateSample) obj2;
                if (!(heartRateSample == null || (a = hf6.a(heartRateSample.getMax())) == null)) {
                    i = a.intValue();
                    return hf6.a(i);
                }
            }
            i = 0;
            return hf6.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
