package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qe3 extends tv2 implements pe3 {
    @DexIgnore
    public qe3() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((DataHolder) uv2.a(parcel, DataHolder.CREATOR));
                return true;
            case 2:
                a((re3) uv2.a(parcel, re3.CREATOR));
                return true;
            case 3:
                a((te3) uv2.a(parcel, te3.CREATOR));
                return true;
            case 4:
                b((te3) uv2.a(parcel, te3.CREATOR));
                return true;
            case 5:
                b((List<te3>) parcel.createTypedArrayList(te3.CREATOR));
                return true;
            case 6:
                a((ye3) uv2.a(parcel, ye3.CREATOR));
                return true;
            case 7:
                a((ie3) uv2.a(parcel, ie3.CREATOR));
                return true;
            case 8:
                a((ee3) uv2.a(parcel, ee3.CREATOR));
                return true;
            case 9:
                a((we3) uv2.a(parcel, we3.CREATOR));
                return true;
            default:
                return false;
        }
    }
}
