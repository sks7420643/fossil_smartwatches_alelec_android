package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ru3 {
    DEFAULT {
        @DexIgnore
        public JsonElement serialize(Long l) {
            return new nu3((Number) l);
        }
    },
    STRING {
        @DexIgnore
        public JsonElement serialize(Long l) {
            return new nu3(String.valueOf(l));
        }
    };

    @DexIgnore
    public abstract JsonElement serialize(Long l);
}
