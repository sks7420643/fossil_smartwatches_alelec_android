package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ cd0 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<l90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new l90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new l90[i];
        }
    }

    @DexIgnore
    public l90(byte b, String str, cd0 cd0) {
        super(e90.COMMUTE_TIME_WATCH_APP, b);
        this.d = str;
        this.c = cd0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(l90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            l90 l90 = (l90) obj;
            return this.c == l90.c && !(wg6.a(this.d, l90.d) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.CommuteTimeWatchAppNotification");
    }

    @DexIgnore
    public final cd0 getAction() {
        return this.c;
    }

    @DexIgnore
    public final String getDestination() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
    }

    @DexIgnore
    public /* synthetic */ l90(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
            this.c = cd0.values()[parcel.readInt()];
            return;
        }
        wg6.a();
        throw null;
    }
}
