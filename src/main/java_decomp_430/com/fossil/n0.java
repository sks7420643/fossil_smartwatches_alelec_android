package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle$Delegate;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.stetho.websocket.CloseCodes;
import com.fossil.f9;
import com.fossil.g1;
import com.fossil.q1;
import com.fossil.u2;
import com.fossil.x1;
import java.lang.Thread;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n0 extends AppCompatDelegate implements q1.a, LayoutInflater.Factory2 {
    @DexIgnore
    public static /* final */ Map<Class<?>, Integer> e0; // = new p4();
    @DexIgnore
    public static /* final */ boolean f0; // = (Build.VERSION.SDK_INT < 21);
    @DexIgnore
    public static /* final */ int[] g0; // = {16842836};
    @DexIgnore
    public static boolean h0; // = true;
    @DexIgnore
    public static /* final */ boolean i0;
    @DexIgnore
    public TextView A;
    @DexIgnore
    public View B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public p[] K;
    @DexIgnore
    public p L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public boolean T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public m V;
    @DexIgnore
    public m W;
    @DexIgnore
    public boolean X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public /* final */ Runnable Z;
    @DexIgnore
    public boolean a0;
    @DexIgnore
    public Rect b0;
    @DexIgnore
    public Rect c0;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public AppCompatViewInflater d0;
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public Window f;
    @DexIgnore
    public k g;
    @DexIgnore
    public /* final */ m0 h;
    @DexIgnore
    public ActionBar i;
    @DexIgnore
    public MenuInflater j;
    @DexIgnore
    public CharSequence o;
    @DexIgnore
    public q2 p;
    @DexIgnore
    public i q;
    @DexIgnore
    public q r;
    @DexIgnore
    public ActionMode s;
    @DexIgnore
    public ActionBarContextView t;
    @DexIgnore
    public PopupWindow u;
    @DexIgnore
    public Runnable v;
    @DexIgnore
    public ba w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public ViewGroup z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ Thread.UncaughtExceptionHandler a;

        @DexIgnore
        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        @DexIgnore
        public final boolean a(Throwable th) {
            String message;
            if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                return false;
            }
            if (message.contains("drawable") || message.contains("Drawable")) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.a.uncaughtException(thread, notFoundException);
                return;
            }
            this.a.uncaughtException(thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            n0 n0Var = n0.this;
            if ((n0Var.Y & 1) != 0) {
                n0Var.f(0);
            }
            n0 n0Var2 = n0.this;
            if ((n0Var2.Y & 4096) != 0) {
                n0Var2.f(108);
            }
            n0 n0Var3 = n0.this;
            n0Var3.X = false;
            n0Var3.Y = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements s9 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public fa a(View view, fa faVar) {
            int e = faVar.e();
            int l = n0.this.l(e);
            if (e != l) {
                faVar = faVar.a(faVar.c(), l, faVar.d(), faVar.b());
            }
            return x9.b(view, faVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements u2.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(Rect rect) {
            rect.top = n0.this.l(rect.top);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ContentFrameLayout.a {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void onDetachedFromWindow() {
            n0.this.q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends da {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void b(View view) {
                n0.this.t.setAlpha(1.0f);
                n0.this.w.a((ca) null);
                n0.this.w = null;
            }

            @DexIgnore
            public void c(View view) {
                n0.this.t.setVisibility(0);
            }
        }

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void run() {
            n0 n0Var = n0.this;
            n0Var.u.showAtLocation(n0Var.t, 55, 0, 0);
            n0.this.r();
            if (n0.this.E()) {
                n0.this.t.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                n0 n0Var2 = n0.this;
                ba a2 = x9.a(n0Var2.t);
                a2.a(1.0f);
                n0Var2.w = a2;
                n0.this.w.a((ca) new a());
                return;
            }
            n0.this.t.setAlpha(1.0f);
            n0.this.t.setVisibility(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends da {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void b(View view) {
            n0.this.t.setAlpha(1.0f);
            n0.this.w.a((ca) null);
            n0.this.w = null;
        }

        @DexIgnore
        public void c(View view) {
            n0.this.t.setVisibility(0);
            n0.this.t.sendAccessibilityEvent(32);
            if (n0.this.t.getParent() instanceof View) {
                x9.J((View) n0.this.t.getParent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements ActionBarDrawerToggle$Delegate {
        @DexIgnore
        public h(n0 n0Var) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements ActionMode.Callback {
        @DexIgnore
        public ActionMode.Callback a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends da {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void b(View view) {
                n0.this.t.setVisibility(8);
                n0 n0Var = n0.this;
                PopupWindow popupWindow = n0Var.u;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (n0Var.t.getParent() instanceof View) {
                    x9.J((View) n0.this.t.getParent());
                }
                n0.this.t.removeAllViews();
                n0.this.w.a((ca) null);
                n0.this.w = null;
            }
        }

        @DexIgnore
        public j(ActionMode.Callback callback) {
            this.a = callback;
        }

        @DexIgnore
        public boolean a(ActionMode actionMode, Menu menu) {
            return this.a.a(actionMode, menu);
        }

        @DexIgnore
        public boolean b(ActionMode actionMode, Menu menu) {
            return this.a.b(actionMode, menu);
        }

        @DexIgnore
        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.a.a(actionMode, menuItem);
        }

        @DexIgnore
        public void a(ActionMode actionMode) {
            this.a.a(actionMode);
            n0 n0Var = n0.this;
            if (n0Var.u != null) {
                n0Var.f.getDecorView().removeCallbacks(n0.this.v);
            }
            n0 n0Var2 = n0.this;
            if (n0Var2.t != null) {
                n0Var2.r();
                n0 n0Var3 = n0.this;
                ba a2 = x9.a(n0Var3.t);
                a2.a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                n0Var3.w = a2;
                n0.this.w.a((ca) new a());
            }
            n0 n0Var4 = n0.this;
            m0 m0Var = n0Var4.h;
            if (m0Var != null) {
                m0Var.onSupportActionModeFinished(n0Var4.s);
            }
            n0.this.s = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends m {
        @DexIgnore
        public /* final */ PowerManager c;

        @DexIgnore
        public l(Context context) {
            super();
            this.c = (PowerManager) context.getSystemService("power");
        }

        @DexIgnore
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT < 21) {
                return null;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        @DexIgnore
        public int c() {
            if (Build.VERSION.SDK_INT < 21 || !this.c.isPowerSaveMode()) {
                return 1;
            }
            return 2;
        }

        @DexIgnore
        public void d() {
            n0.this.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class m {
        @DexIgnore
        public BroadcastReceiver a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends BroadcastReceiver {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                m.this.d();
            }
        }

        @DexIgnore
        public m() {
        }

        @DexIgnore
        public void a() {
            BroadcastReceiver broadcastReceiver = this.a;
            if (broadcastReceiver != null) {
                try {
                    n0.this.e.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.a = null;
            }
        }

        @DexIgnore
        public abstract IntentFilter b();

        @DexIgnore
        public abstract int c();

        @DexIgnore
        public abstract void d();

        @DexIgnore
        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.a == null) {
                    this.a = new a();
                }
                n0.this.e.registerReceiver(this.a, b2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n extends m {
        @DexIgnore
        public /* final */ s0 c;

        @DexIgnore
        public n(s0 s0Var) {
            super();
            this.c = s0Var;
        }

        @DexIgnore
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        @DexIgnore
        public int c() {
            return this.c.b() ? 2 : 1;
        }

        @DexIgnore
        public void d() {
            n0.this.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o extends ContentFrameLayout {
        @DexIgnore
        public o(Context context) {
            super(context);
        }

        @DexIgnore
        public final boolean a(int i2, int i3) {
            return i2 < -5 || i3 < -5 || i2 > getWidth() + 5 || i3 > getHeight() + 5;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return n0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            n0.this.e(0);
            return true;
        }

        @DexIgnore
        public void setBackgroundResource(int i2) {
            setBackgroundDrawable(u0.c(getContext(), i2));
        }
    }

    /*
    static {
        boolean z2 = false;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21 && i2 <= 25) {
            z2 = true;
        }
        i0 = z2;
        if (f0 && !h0) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }
    */

    @DexIgnore
    public n0(Activity activity, m0 m0Var) {
        this(activity, (Window) null, m0Var, activity);
    }

    @DexIgnore
    public final boolean A() {
        if (!this.U && (this.d instanceof Activity)) {
            PackageManager packageManager = this.e.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.e, this.d.getClass()), 0);
                this.T = (activityInfo == null || (activityInfo.configChanges & 512) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException e2) {
                Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                this.T = false;
            }
        }
        this.U = true;
        return this.T;
    }

    @DexIgnore
    public boolean B() {
        return this.x;
    }

    @DexIgnore
    public boolean C() {
        ActionMode actionMode = this.s;
        if (actionMode != null) {
            actionMode.a();
            return true;
        }
        ActionBar d2 = d();
        if (d2 == null || !d2.f()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ActionBar D() {
        return this.i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.z;
     */
    @DexIgnore
    public final boolean E() {
        ViewGroup viewGroup;
        return this.y && viewGroup != null && x9.E(viewGroup);
    }

    @DexIgnore
    public final void F() {
        if (this.y) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    @DexIgnore
    public final AppCompatActivity G() {
        Context context = this.e;
        while (context != null) {
            if (!(context instanceof AppCompatActivity)) {
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
                context = ((ContextWrapper) context).getBaseContext();
            } else {
                return (AppCompatActivity) context;
            }
        }
        return null;
    }

    @DexIgnore
    public void a(Context context) {
        a(false);
        this.N = true;
    }

    @DexIgnore
    public void a(ViewGroup viewGroup) {
    }

    @DexIgnore
    public void b(Bundle bundle) {
        s();
    }

    @DexIgnore
    public MenuInflater c() {
        if (this.j == null) {
            z();
            ActionBar actionBar = this.i;
            this.j = new h1(actionBar != null ? actionBar.h() : this.e);
        }
        return this.j;
    }

    @DexIgnore
    public ActionBar d() {
        z();
        return this.i;
    }

    @DexIgnore
    public void e() {
        LayoutInflater from = LayoutInflater.from(this.e);
        if (from.getFactory() == null) {
            g9.b(from, this);
        } else if (!(from.getFactory2() instanceof n0)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @DexIgnore
    public void f() {
        ActionBar d2 = d();
        if (d2 == null || !d2.i()) {
            g(0);
        }
    }

    @DexIgnore
    public void g() {
        AppCompatDelegate.b((AppCompatDelegate) this);
        if (this.X) {
            this.f.getDecorView().removeCallbacks(this.Z);
        }
        this.P = false;
        this.Q = true;
        ActionBar actionBar = this.i;
        if (actionBar != null) {
            actionBar.j();
        }
        o();
    }

    @DexIgnore
    public void h() {
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(true);
        }
    }

    @DexIgnore
    public void i() {
        this.P = true;
        l();
        AppCompatDelegate.a((AppCompatDelegate) this);
    }

    @DexIgnore
    public void j() {
        this.P = false;
        AppCompatDelegate.b((AppCompatDelegate) this);
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(false);
        }
        if (this.d instanceof Dialog) {
            o();
        }
    }

    @DexIgnore
    public final int k(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    @DexIgnore
    public int l(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        ActionBarContextView actionBarContextView = this.t;
        int i3 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.t.getLayoutParams();
            z2 = true;
            if (this.t.isShown()) {
                if (this.b0 == null) {
                    this.b0 = new Rect();
                    this.c0 = new Rect();
                }
                Rect rect = this.b0;
                Rect rect2 = this.c0;
                rect.set(0, i2, 0, 0);
                o3.a(this.z, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    View view = this.B;
                    if (view == null) {
                        this.B = new View(this.e);
                        this.B.setBackgroundColor(this.e.getResources().getColor(c0.abc_input_method_navigation_guard));
                        this.z.addView(this.B, -1, new ViewGroup.LayoutParams(-1, i2));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.B.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.B == null) {
                    z2 = false;
                }
                if (!this.G && z2) {
                    i2 = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z4 = true;
                } else {
                    z4 = false;
                }
                z2 = false;
            }
            if (z3) {
                this.t.setLayoutParams(marginLayoutParams);
            }
        }
        View view2 = this.B;
        if (view2 != null) {
            if (!z2) {
                i3 = 8;
            }
            view2.setVisibility(i3);
        }
        return i2;
    }

    @DexIgnore
    public final void m() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.z.findViewById(16908290);
        View decorView = this.f.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(j0.AppCompatTheme);
        obtainStyledAttributes.getValue(j0.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(j0.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(j0.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(j0.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(j0.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(j0.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(j0.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(j0.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(j0.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(j0.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @DexIgnore
    public final int n() {
        int i2 = this.R;
        return i2 != -100 ? i2 : AppCompatDelegate.k();
    }

    @DexIgnore
    public final void o() {
        m mVar = this.V;
        if (mVar != null) {
            mVar.a();
        }
        m mVar2 = this.W;
        if (mVar2 != null) {
            mVar2.a();
        }
    }

    @DexIgnore
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    @DexIgnore
    public final ViewGroup p() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.e.obtainStyledAttributes(j0.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(j0.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(j0.AppCompatTheme_windowNoTitle, false)) {
                b(1);
            } else if (obtainStyledAttributes.getBoolean(j0.AppCompatTheme_windowActionBar, false)) {
                b(108);
            }
            if (obtainStyledAttributes.getBoolean(j0.AppCompatTheme_windowActionBarOverlay, false)) {
                b(109);
            }
            if (obtainStyledAttributes.getBoolean(j0.AppCompatTheme_windowActionModeOverlay, false)) {
                b(10);
            }
            this.H = obtainStyledAttributes.getBoolean(j0.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            t();
            this.f.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.e);
            if (this.I) {
                if (this.G) {
                    viewGroup = (ViewGroup) from.inflate(g0.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    viewGroup = (ViewGroup) from.inflate(g0.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    x9.a((View) viewGroup, (s9) new c());
                } else {
                    ((u2) viewGroup).setOnFitSystemWindowsListener(new d());
                }
            } else if (this.H) {
                viewGroup = (ViewGroup) from.inflate(g0.abc_dialog_title_material, (ViewGroup) null);
                this.F = false;
                this.E = false;
            } else if (this.E) {
                TypedValue typedValue = new TypedValue();
                this.e.getTheme().resolveAttribute(a0.actionBarTheme, typedValue, true);
                int i2 = typedValue.resourceId;
                if (i2 != 0) {
                    context = new e1(this.e, i2);
                } else {
                    context = this.e;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(g0.abc_screen_toolbar, (ViewGroup) null);
                this.p = (q2) viewGroup.findViewById(f0.decor_content_parent);
                this.p.setWindowCallback(y());
                if (this.F) {
                    this.p.a(109);
                }
                if (this.C) {
                    this.p.a(2);
                }
                if (this.D) {
                    this.p.a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.p == null) {
                    this.A = (TextView) viewGroup.findViewById(f0.title);
                }
                o3.b(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(f0.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.f.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground((Drawable) null);
                    }
                }
                this.f.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.E + ", windowActionBarOverlay: " + this.F + ", android:windowIsFloating: " + this.H + ", windowActionModeOverlay: " + this.G + ", windowNoTitle: " + this.I + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    @DexIgnore
    public void q() {
        q1 q1Var;
        q2 q2Var = this.p;
        if (q2Var != null) {
            q2Var.g();
        }
        if (this.u != null) {
            this.f.getDecorView().removeCallbacks(this.v);
            if (this.u.isShowing()) {
                try {
                    this.u.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.u = null;
        }
        r();
        p a2 = a(0, false);
        if (a2 != null && (q1Var = a2.j) != null) {
            q1Var.close();
        }
    }

    @DexIgnore
    public void r() {
        ba baVar = this.w;
        if (baVar != null) {
            baVar.a();
        }
    }

    @DexIgnore
    public final void s() {
        if (!this.y) {
            this.z = p();
            CharSequence x2 = x();
            if (!TextUtils.isEmpty(x2)) {
                q2 q2Var = this.p;
                if (q2Var != null) {
                    q2Var.setWindowTitle(x2);
                } else if (D() != null) {
                    D().b(x2);
                } else {
                    TextView textView = this.A;
                    if (textView != null) {
                        textView.setText(x2);
                    }
                }
            }
            m();
            a(this.z);
            this.y = true;
            p a2 = a(0, false);
            if (this.Q) {
                return;
            }
            if (a2 == null || a2.j == null) {
                g(108);
            }
        }
    }

    @DexIgnore
    public final void t() {
        if (this.f == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                a(((Activity) obj).getWindow());
            }
        }
        if (this.f == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    @DexIgnore
    public final Context u() {
        ActionBar d2 = d();
        Context h2 = d2 != null ? d2.h() : null;
        return h2 == null ? this.e : h2;
    }

    @DexIgnore
    public final m v() {
        if (this.W == null) {
            this.W = new l(this.e);
        }
        return this.W;
    }

    @DexIgnore
    public final m w() {
        if (this.V == null) {
            this.V = new n(s0.a(this.e));
        }
        return this.V;
    }

    @DexIgnore
    public final CharSequence x() {
        Object obj = this.d;
        if (obj instanceof Activity) {
            return ((Activity) obj).getTitle();
        }
        return this.o;
    }

    @DexIgnore
    public final Window.Callback y() {
        return this.f.getCallback();
    }

    @DexIgnore
    public final void z() {
        s();
        if (this.E && this.i == null) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                this.i = new t0((Activity) obj, this.F);
            } else if (obj instanceof Dialog) {
                this.i = new t0((Dialog) obj);
            }
            ActionBar actionBar = this.i;
            if (actionBar != null) {
                actionBar.c(this.a0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements x1.a {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        public boolean a(q1 q1Var) {
            Window.Callback y = n0.this.y();
            if (y == null) {
                return true;
            }
            y.onMenuOpened(108, q1Var);
            return true;
        }

        @DexIgnore
        public void a(q1 q1Var, boolean z) {
            n0.this.b(q1Var);
        }
    }

    @DexIgnore
    public n0(Dialog dialog, m0 m0Var) {
        this(dialog.getContext(), dialog.getWindow(), m0Var, dialog);
    }

    @DexIgnore
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.z.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView((View) null, str, context, attributeSet);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public ViewGroup g;
        @DexIgnore
        public View h;
        @DexIgnore
        public View i;
        @DexIgnore
        public q1 j;
        @DexIgnore
        public o1 k;
        @DexIgnore
        public Context l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q; // = false;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public Bundle s;

        @DexIgnore
        public p(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public boolean a() {
            if (this.h == null) {
                return false;
            }
            if (this.i == null && this.k.c().getCount() <= 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(a0.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(a0.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(i0.Theme_AppCompat_CompactMenu, true);
            }
            e1 e1Var = new e1(context, 0);
            e1Var.getTheme().setTo(newTheme);
            this.l = e1Var;
            TypedArray obtainStyledAttributes = e1Var.obtainStyledAttributes(j0.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(j0.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(j0.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        public void a(q1 q1Var) {
            o1 o1Var;
            q1 q1Var2 = this.j;
            if (q1Var != q1Var2) {
                if (q1Var2 != null) {
                    q1Var2.b((x1) this.k);
                }
                this.j = q1Var;
                if (q1Var != null && (o1Var = this.k) != null) {
                    q1Var.a((x1) o1Var);
                }
            }
        }

        @DexIgnore
        public y1 a(x1.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new o1(this.l, g0.abc_list_menu_item_layout);
                this.k.a(aVar);
                this.j.a((x1) this.k);
            }
            return this.k.a(this.g);
        }
    }

    @DexIgnore
    public n0(Context context, Window window, m0 m0Var, Object obj) {
        Integer num;
        AppCompatActivity G2;
        this.w = null;
        this.x = true;
        this.R = -100;
        this.Z = new b();
        this.e = context;
        this.h = m0Var;
        this.d = obj;
        if (this.R == -100 && (this.d instanceof Dialog) && (G2 = G()) != null) {
            this.R = G2.getDelegate().b();
        }
        if (this.R == -100 && (num = e0.get(this.d.getClass())) != null) {
            this.R = num.intValue();
            e0.remove(this.d.getClass());
        }
        if (window != null) {
            a(window);
        }
        j2.c();
    }

    @DexIgnore
    public void a(Bundle bundle) {
        this.N = true;
        a(false);
        t();
        Object obj = this.d;
        if (obj instanceof Activity) {
            String str = null;
            try {
                str = n6.b((Activity) obj);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar D2 = D();
                if (D2 == null) {
                    this.a0 = true;
                } else {
                    D2.c(true);
                }
            }
        }
        this.O = true;
    }

    @DexIgnore
    public void d(int i2) {
        this.S = i2;
    }

    @DexIgnore
    public int h(int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 == -1) {
            return i2;
        }
        if (i2 != 0) {
            if (i2 == 1 || i2 == 2) {
                return i2;
            }
            if (i2 == 3) {
                return v().c();
            }
            throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
        } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) this.e.getSystemService(UiModeManager.class)).getNightMode() != 0) {
            return w().c();
        } else {
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends j1 {
        @DexIgnore
        public k(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public final android.view.ActionMode a(ActionMode.Callback callback) {
            g1.a aVar = new g1.a(n0.this.e, callback);
            androidx.appcompat.view.ActionMode a = n0.this.a((ActionMode.Callback) aVar);
            if (a != null) {
                return aVar.b(a);
            }
            return null;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return n0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || n0.this.c(keyEvent.getKeyCode(), keyEvent);
        }

        @DexIgnore
        public void onContentChanged() {
        }

        @DexIgnore
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof q1)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @DexIgnore
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            n0.this.i(i);
            return true;
        }

        @DexIgnore
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            n0.this.j(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, View view, Menu menu) {
            q1 q1Var = menu instanceof q1 ? (q1) menu : null;
            if (i == 0 && q1Var == null) {
                return false;
            }
            if (q1Var != null) {
                q1Var.d(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (q1Var != null) {
                q1Var.d(false);
            }
            return onPreparePanel;
        }

        @DexIgnore
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            q1 q1Var;
            p a = n0.this.a(0, true);
            if (a == null || (q1Var = a.j) == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, q1Var, i);
            }
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (n0.this.B()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (!n0.this.B() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return a(callback);
        }
    }

    @DexIgnore
    public boolean d(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.M;
            this.M = false;
            p a2 = a(0, false);
            if (a2 != null && a2.o) {
                if (!z2) {
                    a(a2, true);
                }
                return true;
            } else if (C()) {
                return true;
            }
        } else if (i2 == 82) {
            e(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public void f(int i2) {
        p a2;
        p a3 = a(i2, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.e(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.s();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i2 == 108 || i2 == 0) && this.p != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    @DexIgnore
    public void i(int i2) {
        ActionBar d2;
        if (i2 == 108 && (d2 = d()) != null) {
            d2.b(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements x1.a {
        @DexIgnore
        public q() {
        }

        @DexIgnore
        public void a(q1 q1Var, boolean z) {
            q1 m = q1Var.m();
            boolean z2 = m != q1Var;
            n0 n0Var = n0.this;
            if (z2) {
                q1Var = m;
            }
            p a2 = n0Var.a((Menu) q1Var);
            if (a2 == null) {
                return;
            }
            if (z2) {
                n0.this.a(a2.a, a2, m);
                n0.this.a(a2, true);
                return;
            }
            n0.this.a(a2, z);
        }

        @DexIgnore
        public boolean a(q1 q1Var) {
            Window.Callback y;
            if (q1Var != null) {
                return true;
            }
            n0 n0Var = n0.this;
            if (!n0Var.E || (y = n0Var.y()) == null || n0.this.Q) {
                return true;
            }
            y.onMenuOpened(108, q1Var);
            return true;
        }
    }

    @DexIgnore
    public void c(int i2) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.z.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.e).inflate(i2, viewGroup);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public void e(int i2) {
        a(a(i2, true), true);
    }

    @DexIgnore
    public boolean b(int i2) {
        int k2 = k(i2);
        if (this.I && k2 == 108) {
            return false;
        }
        if (this.E && k2 == 1) {
            this.E = false;
        }
        if (k2 == 1) {
            F();
            this.I = true;
            return true;
        } else if (k2 == 2) {
            F();
            this.C = true;
            return true;
        } else if (k2 == 5) {
            F();
            this.D = true;
            return true;
        } else if (k2 == 10) {
            F();
            this.G = true;
            return true;
        } else if (k2 == 108) {
            F();
            this.E = true;
            return true;
        } else if (k2 != 109) {
            return this.f.requestFeature(k2);
        } else {
            F();
            this.F = true;
            return true;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    public final boolean e(int i2, KeyEvent keyEvent) {
        boolean z2;
        boolean z3;
        q2 q2Var;
        if (this.s != null) {
            return false;
        }
        p a2 = a(i2, true);
        if (i2 == 0 && (q2Var = this.p) != null && q2Var.c() && !ViewConfiguration.get(this.e).hasPermanentMenuKey()) {
            if (this.p.a()) {
                z2 = this.p.e();
            } else if (!this.Q && b(a2, keyEvent)) {
                z2 = this.p.f();
            }
            if (z2) {
            }
            return z2;
        } else if (a2.o || a2.n) {
            z2 = a2.o;
            a(a2, true);
            if (z2) {
                AudioManager audioManager = (AudioManager) this.e.getSystemService("audio");
                if (audioManager != null) {
                    audioManager.playSoundEffect(0);
                } else {
                    Log.w("AppCompatDelegate", "Couldn't get audio manager");
                }
            }
            return z2;
        } else if (a2.m) {
            if (a2.r) {
                a2.m = false;
                z3 = b(a2, keyEvent);
            } else {
                z3 = true;
            }
            if (z3) {
                a(a2, keyEvent);
                z2 = true;
                if (z2) {
                }
                return z2;
            }
        }
        z2 = false;
        if (z2) {
        }
        return z2;
    }

    @DexIgnore
    public void j(int i2) {
        if (i2 == 108) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.b(false);
            }
        } else if (i2 == 0) {
            p a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    @DexIgnore
    public final void g(int i2) {
        this.Y = (1 << i2) | this.Y;
        if (!this.X) {
            x9.a(this.f.getDecorView(), this.Z);
            this.X = true;
        }
    }

    @DexIgnore
    public void c(Bundle bundle) {
        if (this.R != -100) {
            e0.put(this.d.getClass(), Integer.valueOf(this.R));
        }
    }

    @DexIgnore
    public void a(Toolbar toolbar) {
        if (this.d instanceof Activity) {
            ActionBar d2 = d();
            if (!(d2 instanceof t0)) {
                this.j = null;
                if (d2 != null) {
                    d2.j();
                }
                if (toolbar != null) {
                    q0 q0Var = new q0(toolbar, x(), this.g);
                    this.i = q0Var;
                    this.f.setCallback(q0Var.m());
                } else {
                    this.i = null;
                    this.f.setCallback(this.g);
                }
                f();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    @DexIgnore
    public boolean c(int i2, KeyEvent keyEvent) {
        ActionBar d2 = d();
        if (d2 != null && d2.a(i2, keyEvent)) {
            return true;
        }
        p pVar = this.L;
        if (pVar == null || !a(pVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.L == null) {
                p a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        }
        p pVar2 = this.L;
        if (pVar2 != null) {
            pVar2.n = true;
        }
        return true;
    }

    @DexIgnore
    public <T extends View> T a(int i2) {
        s();
        return this.f.findViewById(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    public androidx.appcompat.view.ActionMode b(ActionMode.Callback callback) {
        androidx.appcompat.view.ActionMode actionMode;
        androidx.appcompat.view.ActionMode actionMode2;
        m0 m0Var;
        e1 e1Var;
        r();
        androidx.appcompat.view.ActionMode actionMode3 = this.s;
        if (actionMode3 != null) {
            actionMode3.a();
        }
        if (!(callback instanceof j)) {
            callback = new j(callback);
        }
        m0 m0Var2 = this.h;
        if (m0Var2 != null && !this.Q) {
            try {
                actionMode = m0Var2.onWindowStartingSupportActionMode(callback);
            } catch (AbstractMethodError unused) {
            }
            if (actionMode == null) {
                this.s = actionMode;
            } else {
                boolean z2 = true;
                if (this.t == null) {
                    if (this.H) {
                        TypedValue typedValue = new TypedValue();
                        Resources.Theme theme = this.e.getTheme();
                        theme.resolveAttribute(a0.actionBarTheme, typedValue, true);
                        if (typedValue.resourceId != 0) {
                            Resources.Theme newTheme = this.e.getResources().newTheme();
                            newTheme.setTo(theme);
                            newTheme.applyStyle(typedValue.resourceId, true);
                            e1 e1Var2 = new e1(this.e, 0);
                            e1Var2.getTheme().setTo(newTheme);
                            e1Var = e1Var2;
                        } else {
                            e1Var = this.e;
                        }
                        this.t = new ActionBarContextView(e1Var);
                        this.u = new PopupWindow(e1Var, (AttributeSet) null, a0.actionModePopupWindowStyle);
                        ta.a(this.u, 2);
                        this.u.setContentView(this.t);
                        this.u.setWidth(-1);
                        e1Var.getTheme().resolveAttribute(a0.actionBarSize, typedValue, true);
                        this.t.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, e1Var.getResources().getDisplayMetrics()));
                        this.u.setHeight(-2);
                        this.v = new f();
                    } else {
                        ViewStubCompat viewStubCompat = (ViewStubCompat) this.z.findViewById(f0.action_mode_bar_stub);
                        if (viewStubCompat != null) {
                            viewStubCompat.setLayoutInflater(LayoutInflater.from(u()));
                            this.t = (ActionBarContextView) viewStubCompat.a();
                        }
                    }
                }
                if (this.t != null) {
                    r();
                    this.t.d();
                    Context context = this.t.getContext();
                    ActionBarContextView actionBarContextView = this.t;
                    if (this.u != null) {
                        z2 = false;
                    }
                    f1 f1Var = new f1(context, actionBarContextView, callback, z2);
                    if (callback.a((androidx.appcompat.view.ActionMode) f1Var, f1Var.c())) {
                        f1Var.i();
                        this.t.a(f1Var);
                        this.s = f1Var;
                        if (E()) {
                            this.t.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            ba a2 = x9.a(this.t);
                            a2.a(1.0f);
                            this.w = a2;
                            this.w.a((ca) new g());
                        } else {
                            this.t.setAlpha(1.0f);
                            this.t.setVisibility(0);
                            this.t.sendAccessibilityEvent(32);
                            if (this.t.getParent() instanceof View) {
                                x9.J((View) this.t.getParent());
                            }
                        }
                        if (this.u != null) {
                            this.f.getDecorView().post(this.v);
                        }
                    } else {
                        this.s = null;
                    }
                }
            }
            actionMode2 = this.s;
            if (!(actionMode2 == null || (m0Var = this.h) == null)) {
                m0Var.onSupportActionModeStarted(actionMode2);
            }
            return this.s;
        }
        actionMode = null;
        if (actionMode == null) {
        }
        actionMode2 = this.s;
        m0Var.onSupportActionModeStarted(actionMode2);
        return this.s;
    }

    @DexIgnore
    public final boolean c(p pVar) {
        Context context = this.e;
        int i2 = pVar.a;
        if ((i2 == 0 || i2 == 108) && this.p != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(a0.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(a0.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(a0.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                e1 e1Var = new e1(context, 0);
                e1Var.getTheme().setTo(theme2);
                context = e1Var;
            }
        }
        q1 q1Var = new q1(context);
        q1Var.a((q1.a) this);
        pVar.a(q1Var);
        return true;
    }

    @DexIgnore
    public void a(Configuration configuration) {
        ActionBar d2;
        if (this.E && this.y && (d2 = d()) != null) {
            d2.a(configuration);
        }
        j2.b().a(this.e);
        a(false);
    }

    @DexIgnore
    public void a(View view) {
        s();
        ViewGroup viewGroup = (ViewGroup) this.z.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public boolean l() {
        return a(true);
    }

    @DexIgnore
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        s();
        ((ViewGroup) this.z.findViewById(16908290)).addView(view, layoutParams);
        this.g.a().onContentChanged();
    }

    @DexIgnore
    public final void a(Window window) {
        if (this.f == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof k)) {
                this.g = new k(callback);
                window.setCallback(this.g);
                i3 a2 = i3.a(this.e, (AttributeSet) null, g0);
                Drawable c2 = a2.c(0);
                if (c2 != null) {
                    window.setBackgroundDrawable(c2);
                }
                a2.a();
                this.f = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    @DexIgnore
    public final void c(int i2, boolean z2) {
        Resources resources = this.e.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.uiMode = i2 | (resources.getConfiguration().uiMode & -49);
        resources.updateConfiguration(configuration, (DisplayMetrics) null);
        if (Build.VERSION.SDK_INT < 26) {
            p0.a(resources);
        }
        int i3 = this.S;
        if (i3 != 0) {
            this.e.setTheme(i3);
            if (Build.VERSION.SDK_INT >= 23) {
                this.e.getTheme().applyStyle(this.S, true);
            }
        }
        if (z2) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof LifecycleOwner) {
                    if (((LifecycleOwner) activity).getLifecycle().a().isAtLeast(Lifecycle.State.STARTED)) {
                        activity.onConfigurationChanged(configuration);
                    }
                } else if (this.P) {
                    activity.onConfigurationChanged(configuration);
                }
            }
        }
    }

    @DexIgnore
    public final void a(CharSequence charSequence) {
        this.o = charSequence;
        q2 q2Var = this.p;
        if (q2Var != null) {
            q2Var.setWindowTitle(charSequence);
        } else if (D() != null) {
            D().b(charSequence);
        } else {
            TextView textView = this.A;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    @DexIgnore
    public boolean a(q1 q1Var, MenuItem menuItem) {
        p a2;
        Window.Callback y2 = y();
        if (y2 == null || this.Q || (a2 = a((Menu) q1Var.m())) == null) {
            return false;
        }
        return y2.onMenuItemSelected(a2.a, menuItem);
    }

    @DexIgnore
    public void a(q1 q1Var) {
        a(q1Var, true);
    }

    @DexIgnore
    public androidx.appcompat.view.ActionMode a(ActionMode.Callback callback) {
        m0 m0Var;
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.s;
            if (actionMode != null) {
                actionMode.a();
            }
            j jVar = new j(callback);
            ActionBar d2 = d();
            if (d2 != null) {
                this.s = d2.a((ActionMode.Callback) jVar);
                androidx.appcompat.view.ActionMode actionMode2 = this.s;
                if (!(actionMode2 == null || (m0Var = this.h) == null)) {
                    m0Var.onSupportActionModeStarted(actionMode2);
                }
            }
            if (this.s == null) {
                this.s = b((ActionMode.Callback) jVar);
            }
            return this.s;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        View decorView;
        Object obj = this.d;
        boolean z2 = true;
        if (((obj instanceof f9.a) || (obj instanceof o0)) && (decorView = this.f.getDecorView()) != null && f9.a(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.g.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? a(keyCode, keyEvent) : d(keyCode, keyEvent);
    }

    @DexIgnore
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.M = z2;
        } else if (i2 == 82) {
            b(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean b(p pVar) {
        pVar.a(u());
        pVar.g = new o(pVar.l);
        pVar.c = 81;
        return true;
    }

    @DexIgnore
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = false;
        if (this.d0 == null) {
            String string = this.e.obtainStyledAttributes(j0.AppCompatTheme).getString(j0.AppCompatTheme_viewInflaterClass);
            if (string == null || AppCompatViewInflater.class.getName().equals(string)) {
                this.d0 = new AppCompatViewInflater();
            } else {
                try {
                    this.d0 = (AppCompatViewInflater) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.d0 = new AppCompatViewInflater();
                }
            }
        }
        if (f0) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z3 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        return this.d0.createView(view, str, context, attributeSet, z2, f0, true, n3.b());
    }

    @DexIgnore
    public final boolean b(p pVar, KeyEvent keyEvent) {
        q2 q2Var;
        q2 q2Var2;
        q2 q2Var3;
        if (this.Q) {
            return false;
        }
        if (pVar.m) {
            return true;
        }
        p pVar2 = this.L;
        if (!(pVar2 == null || pVar2 == pVar)) {
            a(pVar2, false);
        }
        Window.Callback y2 = y();
        if (y2 != null) {
            pVar.i = y2.onCreatePanelView(pVar.a);
        }
        int i2 = pVar.a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2 && (q2Var3 = this.p) != null) {
            q2Var3.b();
        }
        if (pVar.i == null && (!z2 || !(D() instanceof q0))) {
            if (pVar.j == null || pVar.r) {
                if (pVar.j == null && (!c(pVar) || pVar.j == null)) {
                    return false;
                }
                if (z2 && this.p != null) {
                    if (this.q == null) {
                        this.q = new i();
                    }
                    this.p.a(pVar.j, this.q);
                }
                pVar.j.s();
                if (!y2.onCreatePanelMenu(pVar.a, pVar.j)) {
                    pVar.a((q1) null);
                    if (z2 && (q2Var2 = this.p) != null) {
                        q2Var2.a((Menu) null, this.q);
                    }
                    return false;
                }
                pVar.r = false;
            }
            pVar.j.s();
            Bundle bundle = pVar.s;
            if (bundle != null) {
                pVar.j.c(bundle);
                pVar.s = null;
            }
            if (!y2.onPreparePanel(0, pVar.i, pVar.j)) {
                if (z2 && (q2Var = this.p) != null) {
                    q2Var.a((Menu) null, this.q);
                }
                pVar.j.r();
                return false;
            }
            pVar.p = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            pVar.j.setQwertyMode(pVar.p);
            pVar.j.r();
        }
        pVar.m = true;
        pVar.n = false;
        this.L = pVar;
        return true;
    }

    @DexIgnore
    public final boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || x9.D((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    @DexIgnore
    public final void a(p pVar, KeyEvent keyEvent) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        if (!pVar.o && !this.Q) {
            if (pVar.a == 0) {
                if ((this.e.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback y2 = y();
            if (y2 == null || y2.onMenuOpened(pVar.a, pVar.j)) {
                WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
                if (windowManager != null && b(pVar, keyEvent)) {
                    if (pVar.g == null || pVar.q) {
                        ViewGroup viewGroup = pVar.g;
                        if (viewGroup == null) {
                            if (!b(pVar) || pVar.g == null) {
                                return;
                            }
                        } else if (pVar.q && viewGroup.getChildCount() > 0) {
                            pVar.g.removeAllViews();
                        }
                        if (a(pVar) && pVar.a()) {
                            ViewGroup.LayoutParams layoutParams2 = pVar.h.getLayoutParams();
                            if (layoutParams2 == null) {
                                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                            }
                            pVar.g.setBackgroundResource(pVar.b);
                            ViewParent parent = pVar.h.getParent();
                            if (parent instanceof ViewGroup) {
                                ((ViewGroup) parent).removeView(pVar.h);
                            }
                            pVar.g.addView(pVar.h, layoutParams2);
                            if (!pVar.h.hasFocus()) {
                                pVar.h.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else {
                        View view = pVar.i;
                        if (!(view == null || (layoutParams = view.getLayoutParams()) == null || layoutParams.width != -1)) {
                            i2 = -1;
                            pVar.n = false;
                            WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i2, -2, pVar.d, pVar.e, CloseCodes.PROTOCOL_ERROR, 8519680, -3);
                            layoutParams3.gravity = pVar.c;
                            layoutParams3.windowAnimations = pVar.f;
                            windowManager.addView(pVar.g, layoutParams3);
                            pVar.o = true;
                            return;
                        }
                    }
                    i2 = -2;
                    pVar.n = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i2, -2, pVar.d, pVar.e, CloseCodes.PROTOCOL_ERROR, 8519680, -3);
                    layoutParams32.gravity = pVar.c;
                    layoutParams32.windowAnimations = pVar.f;
                    windowManager.addView(pVar.g, layoutParams32);
                    pVar.o = true;
                    return;
                }
                return;
            }
            a(pVar, true);
        }
    }

    @DexIgnore
    public void b(q1 q1Var) {
        if (!this.J) {
            this.J = true;
            this.p.g();
            Window.Callback y2 = y();
            if (y2 != null && !this.Q) {
                y2.onPanelClosed(108, q1Var);
            }
            this.J = false;
        }
    }

    @DexIgnore
    public final boolean b(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        p a2 = a(i2, true);
        if (!a2.o) {
            return b(a2, keyEvent);
        }
        return false;
    }

    @DexIgnore
    public int b() {
        return this.R;
    }

    @DexIgnore
    public final boolean b(int i2, boolean z2) {
        int i3 = this.e.getApplicationContext().getResources().getConfiguration().uiMode & 48;
        boolean z3 = true;
        int i4 = i2 != 1 ? i2 != 2 ? i3 : 32 : 16;
        boolean A2 = A();
        boolean z4 = false;
        if ((i0 || i4 != i3) && !A2 && Build.VERSION.SDK_INT >= 17 && !this.N && (this.d instanceof ContextThemeWrapper)) {
            Configuration configuration = new Configuration();
            configuration.uiMode = (configuration.uiMode & -49) | i4;
            try {
                ((ContextThemeWrapper) this.d).applyOverrideConfiguration(configuration);
                z4 = true;
            } catch (IllegalStateException e2) {
                Log.e("AppCompatDelegate", "updateForNightMode. Calling applyOverrideConfiguration() failed with an exception. Will fall back to using Resources.updateConfiguration()", e2);
            }
        }
        int i5 = this.e.getResources().getConfiguration().uiMode & 48;
        if (!z4 && i5 != i4 && z2 && !A2 && this.N && (Build.VERSION.SDK_INT >= 17 || this.O)) {
            Object obj = this.d;
            if (obj instanceof Activity) {
                h6.d((Activity) obj);
                z4 = true;
            }
        }
        if (z4 || i5 == i4) {
            z3 = z4;
        } else {
            c(i4, A2);
        }
        if (z3) {
            Object obj2 = this.d;
            if (obj2 instanceof AppCompatActivity) {
                ((AppCompatActivity) obj2).onNightModeChanged(i2);
            }
        }
        return z3;
    }

    @DexIgnore
    public final void a(q1 q1Var, boolean z2) {
        q2 q2Var = this.p;
        if (q2Var == null || !q2Var.c() || (ViewConfiguration.get(this.e).hasPermanentMenuKey() && !this.p.d())) {
            p a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback y2 = y();
        if (this.p.a() && z2) {
            this.p.e();
            if (!this.Q) {
                y2.onPanelClosed(108, a(0, true).j);
            }
        } else if (y2 != null && !this.Q) {
            if (this.X && (this.Y & 1) != 0) {
                this.f.getDecorView().removeCallbacks(this.Z);
                this.Z.run();
            }
            p a3 = a(0, true);
            q1 q1Var2 = a3.j;
            if (q1Var2 != null && !a3.r && y2.onPreparePanel(0, a3.i, q1Var2)) {
                y2.onMenuOpened(108, a3.j);
                this.p.f();
            }
        }
    }

    @DexIgnore
    public final boolean a(p pVar) {
        View view = pVar.i;
        if (view != null) {
            pVar.h = view;
            return true;
        } else if (pVar.j == null) {
            return false;
        } else {
            if (this.r == null) {
                this.r = new q();
            }
            pVar.h = (View) pVar.a((x1.a) this.r);
            if (pVar.h != null) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void a(p pVar, boolean z2) {
        ViewGroup viewGroup;
        q2 q2Var;
        if (!z2 || pVar.a != 0 || (q2Var = this.p) == null || !q2Var.a()) {
            WindowManager windowManager = (WindowManager) this.e.getSystemService("window");
            if (!(windowManager == null || !pVar.o || (viewGroup = pVar.g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(pVar.a, pVar, (Menu) null);
                }
            }
            pVar.m = false;
            pVar.n = false;
            pVar.o = false;
            pVar.h = null;
            pVar.q = true;
            if (this.L == pVar) {
                this.L = null;
                return;
            }
            return;
        }
        b(pVar.j);
    }

    @DexIgnore
    public void a(int i2, p pVar, Menu menu) {
        if (menu == null) {
            if (pVar == null && i2 >= 0) {
                p[] pVarArr = this.K;
                if (i2 < pVarArr.length) {
                    pVar = pVarArr[i2];
                }
            }
            if (pVar != null) {
                menu = pVar.j;
            }
        }
        if ((pVar == null || pVar.o) && !this.Q) {
            this.g.a().onPanelClosed(i2, menu);
        }
    }

    @DexIgnore
    public p a(Menu menu) {
        p[] pVarArr = this.K;
        int length = pVarArr != null ? pVarArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            p pVar = pVarArr[i2];
            if (pVar != null && pVar.j == menu) {
                return pVar;
            }
        }
        return null;
    }

    @DexIgnore
    public p a(int i2, boolean z2) {
        p[] pVarArr = this.K;
        if (pVarArr == null || pVarArr.length <= i2) {
            p[] pVarArr2 = new p[(i2 + 1)];
            if (pVarArr != null) {
                System.arraycopy(pVarArr, 0, pVarArr2, 0, pVarArr.length);
            }
            this.K = pVarArr2;
            pVarArr = pVarArr2;
        }
        p pVar = pVarArr[i2];
        if (pVar != null) {
            return pVar;
        }
        p pVar2 = new p(i2);
        pVarArr[i2] = pVar2;
        return pVar2;
    }

    @DexIgnore
    public final boolean a(p pVar, int i2, KeyEvent keyEvent, int i3) {
        q1 q1Var;
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((pVar.m || b(pVar, keyEvent)) && (q1Var = pVar.j) != null) {
            z2 = q1Var.performShortcut(i2, keyEvent, i3);
        }
        if (z2 && (i3 & 1) == 0 && this.p == null) {
            a(pVar, true);
        }
        return z2;
    }

    @DexIgnore
    public final boolean a(boolean z2) {
        if (this.Q) {
            return false;
        }
        int n2 = n();
        boolean b2 = b(h(n2), z2);
        if (n2 == 0) {
            w().e();
        } else {
            m mVar = this.V;
            if (mVar != null) {
                mVar.a();
            }
        }
        if (n2 == 3) {
            v().e();
        } else {
            m mVar2 = this.W;
            if (mVar2 != null) {
                mVar2.a();
            }
        }
        return b2;
    }

    @DexIgnore
    public final ActionBarDrawerToggle$Delegate a() {
        return new h(this);
    }
}
