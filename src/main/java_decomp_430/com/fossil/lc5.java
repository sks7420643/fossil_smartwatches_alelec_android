package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc5 {
    @DexIgnore
    public /* final */ gc5 a;
    @DexIgnore
    public /* final */ vc5 b;
    @DexIgnore
    public /* final */ qc5 c;

    @DexIgnore
    public lc5(gc5 gc5, vc5 vc5, qc5 qc5) {
        wg6.b(gc5, "mActivityOverviewDayView");
        wg6.b(vc5, "mActivityOverviewWeekView");
        wg6.b(qc5, "mActivityOverviewMonthView");
        this.a = gc5;
        this.b = vc5;
        this.c = qc5;
    }

    @DexIgnore
    public final gc5 a() {
        return this.a;
    }

    @DexIgnore
    public final qc5 b() {
        return this.c;
    }

    @DexIgnore
    public final vc5 c() {
        return this.b;
    }
}
