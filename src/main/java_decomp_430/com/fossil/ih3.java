package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ih3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int e; // = nh3.d().getMaximum(4);
    @DexIgnore
    public /* final */ hh3 a;
    @DexIgnore
    public /* final */ ch3<?> b;
    @DexIgnore
    public bh3 c;
    @DexIgnore
    public /* final */ zg3 d;

    @DexIgnore
    public ih3(hh3 hh3, ch3<?> ch3, zg3 zg3) {
        this.a = hh3;
        this.b = ch3;
        this.d = zg3;
    }

    @DexIgnore
    public final void a(Context context) {
        if (this.c == null) {
            this.c = new bh3(context);
        }
    }

    @DexIgnore
    public int b() {
        return (this.a.a() + this.a.f) - 1;
    }

    @DexIgnore
    public boolean c(int i) {
        return (i + 1) % this.a.e == 0;
    }

    @DexIgnore
    public int d(int i) {
        return (i - this.a.a()) + 1;
    }

    @DexIgnore
    public boolean e(int i) {
        return i >= a() && i <= b();
    }

    @DexIgnore
    public int getCount() {
        return this.a.f + a();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) (i / this.a.e);
    }

    @DexIgnore
    public boolean hasStableIds() {
        return true;
    }

    @DexIgnore
    public boolean b(int i) {
        return i % this.a.e == 0;
    }

    @DexIgnore
    public Long getItem(int i) {
        if (i < this.a.a() || i > b()) {
            return null;
        }
        return Long.valueOf(this.a.a(d(i)));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v19, types: [android.view.View] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
    public TextView getView(int i, View view, ViewGroup viewGroup) {
        Long item;
        a(viewGroup.getContext());
        TextView textView = (TextView) view;
        if (view == null) {
            textView = LayoutInflater.from(viewGroup.getContext()).inflate(tf3.mtrl_calendar_day, viewGroup, false);
        }
        int a2 = i - a();
        if (a2 >= 0) {
            hh3 hh3 = this.a;
            if (a2 < hh3.f) {
                int i2 = a2 + 1;
                textView.setTag(hh3);
                textView.setText(String.valueOf(i2));
                long a3 = this.a.a(i2);
                if (this.a.d == hh3.d().d) {
                    textView.setContentDescription(dh3.a(a3));
                } else {
                    textView.setContentDescription(dh3.b(a3));
                }
                textView.setVisibility(0);
                textView.setEnabled(true);
                item = getItem(i);
                if (item != null) {
                    return textView;
                }
                if (this.d.a().a(item.longValue())) {
                    textView.setEnabled(true);
                    for (Long longValue : this.b.o()) {
                        if (nh3.a(item.longValue()) == nh3.a(longValue.longValue())) {
                            this.c.b.a(textView);
                            return textView;
                        }
                    }
                    if (nh3.b().getTimeInMillis() == item.longValue()) {
                        this.c.c.a(textView);
                        return textView;
                    }
                    this.c.a.a(textView);
                    return textView;
                }
                textView.setEnabled(false);
                this.c.g.a(textView);
                return textView;
            }
        }
        textView.setVisibility(8);
        textView.setEnabled(false);
        item = getItem(i);
        if (item != null) {
        }
    }

    @DexIgnore
    public int a() {
        return this.a.a();
    }

    @DexIgnore
    public int a(int i) {
        return a() + (i - 1);
    }
}
