package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss5 implements Factory<qs5> {
    @DexIgnore
    public static PairingPresenter a(ns5 ns5, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, cj4 cj4, SetNotificationUseCase setNotificationUseCase, an4 an4) {
        return new PairingPresenter(ns5, linkDeviceUseCase, deviceRepository, cj4, setNotificationUseCase, an4);
    }
}
