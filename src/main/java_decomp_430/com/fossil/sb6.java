package com.fossil;

import android.content.Context;
import android.graphics.BitmapFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sb6 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public sb6(String str, int i, int i2, int i3) {
        this.a = str;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static sb6 a(Context context, String str) {
        if (str != null) {
            try {
                int d2 = z86.d(context);
                l86 g = c86.g();
                g.d("Fabric", "App icon resource ID is " + d2);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), d2, options);
                return new sb6(str, d2, options.outWidth, options.outHeight);
            } catch (Exception e) {
                c86.g().e("Fabric", "Failed to load icon", e);
            }
        }
        return null;
    }
}
