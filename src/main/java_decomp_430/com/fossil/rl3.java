package com.fossil;

import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rl3<E> extends ml3<E> implements Set<E> {
    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    public abstract Set<E> delegate();

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return yn3.a((Set<?>) this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return yn3.a((Set<?>) this);
    }

    @DexIgnore
    public boolean standardRemoveAll(Collection<?> collection) {
        jk3.a(collection);
        return yn3.a((Set<?>) this, collection);
    }
}
