package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jg6<T> implements Iterator<T>, ph6 {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ T[] b;

    @DexIgnore
    public jg6(T[] tArr) {
        wg6.b(tArr, "array");
        this.b = tArr;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.a < this.b.length;
    }

    @DexIgnore
    public T next() {
        try {
            T[] tArr = this.b;
            int i = this.a;
            this.a = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.a--;
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
