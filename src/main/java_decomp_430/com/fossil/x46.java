package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x46 extends SQLiteOpenHelper {
    @DexIgnore
    public String a; // = "";

    @DexIgnore
    public x46(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 3);
        this.a = str;
        context.getApplicationContext();
        if (n36.q()) {
            b56 j = o46.l;
            j.e("SQLiteOpenHelper " + this.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    public final void a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        String str = null;
        try {
            cursor = sQLiteDatabase.query("user", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
            try {
                ContentValues contentValues = new ContentValues();
                if (cursor.moveToNext()) {
                    str = cursor.getString(0);
                    cursor.getInt(1);
                    cursor.getString(2);
                    cursor.getLong(3);
                    contentValues.put("uid", r56.b(str));
                }
                if (str != null) {
                    sQLiteDatabase.update("user", contentValues, "uid=?", new String[]{str});
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                th = th;
                try {
                    o46.l.a(th);
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            o46.l.a(th);
        }
    }

    @DexIgnore
    public final void b(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            Cursor query = sQLiteDatabase.query("events", (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
            ArrayList<y46> arrayList = new ArrayList<>();
            while (query.moveToNext()) {
                arrayList.add(new y46(query.getLong(0), query.getString(1), query.getInt(2), query.getInt(3)));
            }
            ContentValues contentValues = new ContentValues();
            for (y46 y46 : arrayList) {
                contentValues.put("content", r56.b(y46.b));
                sQLiteDatabase.update("events", contentValues, "event_id=?", new String[]{Long.toString(y46.a)});
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public synchronized void close() {
        super.close();
    }

    @DexIgnore
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists events(event_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, content TEXT, status INTEGER, send_count INTEGER, timestamp LONG)");
        sQLiteDatabase.execSQL("create table if not exists user(uid TEXT PRIMARY KEY, user_type INTEGER, app_ver TEXT, ts INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists config(type INTEGER PRIMARY KEY NOT NULL, content TEXT, md5sum TEXT, version INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
        sQLiteDatabase.execSQL("CREATE INDEX if not exists status_idx ON events(status)");
    }

    @DexIgnore
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        b56 j = o46.l;
        j.b((Object) "upgrade DB from oldVersion " + i + " to newVersion " + i2);
        if (i == 1) {
            sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
        if (i == 2) {
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
    }
}
