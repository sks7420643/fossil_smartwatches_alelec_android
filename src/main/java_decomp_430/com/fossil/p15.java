package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p15 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(o15 o15) {
        LoaderManager b = o15.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
