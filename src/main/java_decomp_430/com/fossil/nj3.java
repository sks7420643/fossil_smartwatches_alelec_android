package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nj3 extends bb {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nj3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Bundle> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.ClassLoaderCreator<nj3> {
        @DexIgnore
        public nj3[] newArray(int i) {
            return new nj3[i];
        }

        @DexIgnore
        public nj3 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new nj3(parcel, classLoader, (a) null);
        }

        @DexIgnore
        public nj3 createFromParcel(Parcel parcel) {
            return new nj3(parcel, (ClassLoader) null, (a) null);
        }
    }

    @DexIgnore
    public /* synthetic */ nj3(Parcel parcel, ClassLoader classLoader, a aVar) {
        this(parcel, classLoader);
    }

    @DexIgnore
    public String toString() {
        return "ExtendableSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " states=" + this.c + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        int size = this.c.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            strArr[i2] = this.c.c(i2);
            bundleArr[i2] = this.c.e(i2);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }

    @DexIgnore
    public nj3(Parcelable parcelable) {
        super(parcelable);
        this.c = new SimpleArrayMap<>();
    }

    @DexIgnore
    public nj3(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.c = new SimpleArrayMap<>(readInt);
        for (int i = 0; i < readInt; i++) {
            this.c.put(strArr[i], bundleArr[i]);
        }
    }
}
