package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te0 extends lj1 {
    @DexIgnore
    public ArrayList<ie1> V;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ te0(short s, ue1 ue1, int i, int i2) {
        super(dl1.LIST_FILE, s, lx0.LIST_FILE, ue1, (i2 & 4) != 0 ? 3 : i);
        this.V = new ArrayList<>();
    }

    @DexIgnore
    public final void c(byte[] bArr) {
        int i = 0;
        while (true) {
            int i2 = i + 10;
            if (i2 <= bArr.length) {
                ByteBuffer order = ByteBuffer.wrap(md6.a(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN);
                this.V.add(new ie1(this.y.t, order.getShort(0), cw0.b(order.getInt(2)), cw0.b(order.getInt(6))));
                i = i2;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject i = super.i();
        bm0 bm0 = bm0.FILE_LIST;
        Object[] array = this.V.toArray(new ie1[0]);
        if (array != null) {
            return cw0.a(i, bm0, (Object) cw0.a((p40[]) array));
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void v() {
        c(this.L);
    }
}
