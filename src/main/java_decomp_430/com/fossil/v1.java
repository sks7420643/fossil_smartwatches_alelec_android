package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v1 implements a2, x1, AdapterView.OnItemClickListener {
    @DexIgnore
    public Rect a;

    @DexIgnore
    public static boolean b(q1 q1Var) {
        int size = q1Var.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = q1Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public void a(Context context, q1 q1Var) {
    }

    @DexIgnore
    public void a(Rect rect) {
        this.a = rect;
    }

    @DexIgnore
    public abstract void a(View view);

    @DexIgnore
    public abstract void a(PopupWindow.OnDismissListener onDismissListener);

    @DexIgnore
    public abstract void a(q1 q1Var);

    @DexIgnore
    public boolean a(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public boolean b(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public abstract void c(int i);

    @DexIgnore
    public abstract void c(boolean z);

    @DexIgnore
    public boolean e() {
        return true;
    }

    @DexIgnore
    public Rect g() {
        return this.a;
    }

    @DexIgnore
    public int getId() {
        return 0;
    }

    @DexIgnore
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ListAdapter listAdapter = (ListAdapter) adapterView.getAdapter();
        a(listAdapter).a.a((MenuItem) listAdapter.getItem(i), (x1) this, e() ? 0 : 4);
    }

    @DexIgnore
    public static int a(ListAdapter listAdapter, ViewGroup viewGroup, Context context, int i) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        ViewGroup viewGroup2 = viewGroup;
        View view = null;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < count; i4++) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i3) {
                view = null;
                i3 = itemViewType;
            }
            if (viewGroup2 == null) {
                viewGroup2 = new FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, viewGroup2);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth > i2) {
                i2 = measuredWidth;
            }
        }
        return i2;
    }

    @DexIgnore
    public static p1 a(ListAdapter listAdapter) {
        if (listAdapter instanceof HeaderViewListAdapter) {
            return (p1) ((HeaderViewListAdapter) listAdapter).getWrappedAdapter();
        }
        return (p1) listAdapter;
    }
}
