package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class md6 extends ld6 {
    @DexIgnore
    public static final <R> List<R> a(Object[] objArr, Class<R> cls) {
        wg6.b(objArr, "$this$filterIsInstance");
        wg6.b(cls, "klass");
        ArrayList arrayList = new ArrayList();
        a(objArr, arrayList, cls);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> b(T[] tArr) {
        wg6.b(tArr, "$this$asList");
        List<T> a = od6.a(tArr);
        wg6.a((Object) a, "ArraysUtilJVM.asList(this)");
        return a;
    }

    @DexIgnore
    public static final <T> void c(T[] tArr) {
        wg6.b(tArr, "$this$sort");
        if (tArr.length > 1) {
            Arrays.sort(tArr);
        }
    }

    @DexIgnore
    public static final <C extends Collection<? super R>, R> C a(Object[] objArr, C c, Class<R> cls) {
        wg6.b(objArr, "$this$filterIsInstanceTo");
        wg6.b(c, "destination");
        wg6.b(cls, "klass");
        for (Object obj : objArr) {
            if (cls.isInstance(obj)) {
                c.add(obj);
            }
        }
        return c;
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, T[] tArr2) {
        wg6.b(tArr, "$this$plus");
        wg6.b(tArr2, "elements");
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] copyOf = Arrays.copyOf(tArr, length + length2);
        System.arraycopy(tArr2, 0, copyOf, length, length2);
        wg6.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static /* synthetic */ Object[] a(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        a(objArr, objArr2, i, i2, i3);
        return objArr2;
    }

    @DexIgnore
    public static final <T> T[] a(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        wg6.b(tArr, "$this$copyInto");
        wg6.b(tArr2, "destination");
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, int i, int i2) {
        wg6.b(bArr, "$this$copyOfRangeImpl");
        kd6.a(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        wg6.a((Object) copyOfRange, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return copyOfRange;
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, T t, int i, int i2) {
        wg6.b(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte b) {
        wg6.b(bArr, "$this$plus");
        int length = bArr.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + 1);
        copyOf[length] = b;
        wg6.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        wg6.b(bArr, "$this$plus");
        wg6.b(bArr2, "elements");
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        wg6.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final void a(int[] iArr) {
        wg6.b(iArr, "$this$sort");
        if (iArr.length > 1) {
            Arrays.sort(iArr);
        }
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, Comparator<? super T> comparator) {
        wg6.b(tArr, "$this$sortWith");
        wg6.b(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }
}
