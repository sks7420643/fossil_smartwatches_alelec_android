package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq1 implements Factory<rr1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<ur1> b;
    @DexIgnore
    public /* final */ Provider<fr1> c;
    @DexIgnore
    public /* final */ Provider<zs1> d;

    @DexIgnore
    public yq1(Provider<Context> provider, Provider<ur1> provider2, Provider<fr1> provider3, Provider<zs1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static yq1 a(Provider<Context> provider, Provider<ur1> provider2, Provider<fr1> provider3, Provider<zs1> provider4) {
        return new yq1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static rr1 a(Context context, ur1 ur1, fr1 fr1, zs1 zs1) {
        rr1 a2 = xq1.a(context, ur1, fr1, zs1);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public rr1 get() {
        return a((Context) this.a.get(), (ur1) this.b.get(), (fr1) this.c.get(), (zs1) this.d.get());
    }
}
