package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qa6 implements Runnable {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ma6 b;

    @DexIgnore
    public qa6(Context context, ma6 ma6) {
        this.a = context;
        this.b = ma6;
    }

    @DexIgnore
    public void run() {
        try {
            z86.c(this.a, "Performing time based file roll over.");
            if (!this.b.b()) {
                this.b.c();
            }
        } catch (Exception e) {
            z86.a(this.a, "Failed to roll over file", (Throwable) e);
        }
    }
}
