package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ow {
    @DexIgnore
    public static /* final */ ow a; // = new e();
    @DexIgnore
    public static /* final */ ow b; // = new c();
    @DexIgnore
    public static /* final */ ow c; // = new d();
    @DexIgnore
    public static /* final */ ow d; // = new f();
    @DexIgnore
    public static /* final */ ow e; // = c;
    @DexIgnore
    public static /* final */ wr<ow> f; // = wr.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", e);
    @DexIgnore
    public static /* final */ boolean g; // = (Build.VERSION.SDK_INT >= 19);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            int min = Math.min(i2 / i4, i / i3);
            if (min == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(min));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            return g.MEMORY;
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int i5 = 1;
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return 1.0f / ((float) (max << i5));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            if (b(i, i2, i3, i4) == 1.0f) {
                return g.QUALITY;
            }
            return ow.a.a(i, i2, i3, i4);
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, ow.a.b(i, i2, i3, i4));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            if (ow.g) {
                return g.QUALITY;
            }
            return g.MEMORY;
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            if (ow.g) {
                return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
            }
            int max = Math.max(i2 / i4, i / i3);
            if (max == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(max));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends ow {
        @DexIgnore
        public g a(int i, int i2, int i3, int i4) {
            return g.QUALITY;
        }

        @DexIgnore
        public float b(int i, int i2, int i3, int i4) {
            return 1.0f;
        }
    }

    @DexIgnore
    public enum g {
        MEMORY,
        QUALITY
    }

    /*
    static {
        new a();
        new b();
    }
    */

    @DexIgnore
    public abstract g a(int i, int i2, int i3, int i4);

    @DexIgnore
    public abstract float b(int i, int i2, int i3, int i4);
}
