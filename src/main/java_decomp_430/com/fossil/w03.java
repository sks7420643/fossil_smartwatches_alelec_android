package com.fossil;

import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w03 extends a53 {
    @DexIgnore
    public /* final */ Map<String, Long> b; // = new p4();
    @DexIgnore
    public /* final */ Map<String, Integer> c; // = new p4();
    @DexIgnore
    public long d;

    @DexIgnore
    public w03(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final void a(String str, long j) {
        if (str == null || str.length() == 0) {
            b().t().a("Ad unit id must be a non-empty string");
        } else {
            a().a((Runnable) new vz2(this, str, j));
        }
    }

    @DexIgnore
    public final void b(String str, long j) {
        if (str == null || str.length() == 0) {
            b().t().a("Ad unit id must be a non-empty string");
        } else {
            a().a((Runnable) new y23(this, str, j));
        }
    }

    @DexIgnore
    public final void c(String str, long j) {
        e();
        g();
        w12.b(str);
        if (this.c.isEmpty()) {
            this.d = j;
        }
        Integer num = this.c.get(str);
        if (num != null) {
            this.c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.c.size() >= 100) {
            b().w().a("Too many ads visible");
        } else {
            this.c.put(str, 1);
            this.b.put(str, Long.valueOf(j));
        }
    }

    @DexIgnore
    public final void d(String str, long j) {
        e();
        g();
        w12.b(str);
        Integer num = this.c.get(str);
        if (num != null) {
            h83 A = r().A();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.c.remove(str);
                Long l = this.b.get(str);
                if (l == null) {
                    b().t().a("First ad unit exposure time was never set");
                } else {
                    this.b.remove(str);
                    a(str, j - l.longValue(), A);
                }
                if (this.c.isEmpty()) {
                    long j2 = this.d;
                    if (j2 == 0) {
                        b().t().a("First ad exposure time was never set");
                        return;
                    }
                    a(j - j2, A);
                    this.d = 0;
                    return;
                }
                return;
            }
            this.c.put(str, Integer.valueOf(intValue));
            return;
        }
        b().t().a("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    @DexIgnore
    public final void a(long j, h83 h83) {
        if (h83 == null) {
            b().B().a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            b().B().a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            g83.a(h83, bundle, true);
            o().a("am", "_xa", bundle);
        }
    }

    @DexIgnore
    public final void b(long j) {
        for (String put : this.b.keySet()) {
            this.b.put(put, Long.valueOf(j));
        }
        if (!this.b.isEmpty()) {
            this.d = j;
        }
    }

    @DexIgnore
    public final void a(String str, long j, h83 h83) {
        if (h83 == null) {
            b().B().a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            b().B().a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            g83.a(h83, bundle, true);
            o().a("am", "_xu", bundle);
        }
    }

    @DexIgnore
    public final void a(long j) {
        h83 A = r().A();
        for (String next : this.b.keySet()) {
            a(next, j - this.b.get(next).longValue(), A);
        }
        if (!this.b.isEmpty()) {
            a(j - this.d, A);
        }
        b(j);
    }
}
