package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ck6<T> extends ym6 implements rm6, xe6<T>, il6 {
    @DexIgnore
    public /* final */ af6 b; // = this.c.plus(this);
    @DexIgnore
    public /* final */ af6 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ck6(af6 af6, boolean z) {
        super(z);
        wg6.b(af6, "parentContext");
        this.c = af6;
    }

    @DexIgnore
    public final <R> void a(ll6 ll6, R r, ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6) {
        wg6.b(ll6, "start");
        wg6.b(ig6, "block");
        n();
        ll6.invoke(ig6, r, this);
    }

    @DexIgnore
    public void a(Throwable th, boolean z) {
        wg6.b(th, "cause");
    }

    @DexIgnore
    public final void f(Throwable th) {
        wg6.b(th, "exception");
        fl6.a(this.b, th);
    }

    @DexIgnore
    public final void g(Object obj) {
        if (obj instanceof vk6) {
            vk6 vk6 = (vk6) obj;
            a(vk6.a, vk6.a());
            return;
        }
        j(obj);
    }

    @DexIgnore
    public final af6 getContext() {
        return this.b;
    }

    @DexIgnore
    public final void h() {
        o();
    }

    @DexIgnore
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    public int j() {
        return 0;
    }

    @DexIgnore
    public void j(T t) {
    }

    @DexIgnore
    public af6 m() {
        return this.b;
    }

    @DexIgnore
    public final void n() {
        a((rm6) this.c.get(rm6.n));
    }

    @DexIgnore
    public void o() {
    }

    @DexIgnore
    public final void resumeWith(Object obj) {
        b(wk6.a(obj), j());
    }

    @DexIgnore
    public String g() {
        String a = cl6.a(this.b);
        if (a == null) {
            return super.g();
        }
        return '\"' + a + "\":" + super.g();
    }
}
