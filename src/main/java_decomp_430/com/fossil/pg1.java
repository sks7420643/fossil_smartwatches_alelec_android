package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg1 extends BluetoothGattCallback {
    @DexIgnore
    public /* final */ /* synthetic */ ue1 a;

    @DexIgnore
    public pg1(ue1 ue1) {
        this.a = ue1;
    }

    @DexIgnore
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        ue1 ue1 = this.a;
        mi1 mi1 = gk1.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            wg6.a(uuid, "characteristic!!.uuid");
            rg1 a2 = mi1.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            ue1.a(a2, value);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        ue1 ue1 = this.a;
        t31 a2 = t31.c.a(i);
        mi1 mi1 = gk1.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            wg6.a(uuid, "characteristic!!.uuid");
            rg1 a3 = mi1.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            ue1.a.post(new r31(ue1, a2, a3, value));
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        ue1 ue1 = this.a;
        t31 a2 = t31.c.a(i);
        mi1 mi1 = gk1.b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        wg6.a(uuid, "characteristic.uuid");
        rg1 a3 = mi1.a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        ue1.a.post(new eb1(ue1, a2, a3, value));
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        LinkedHashSet<Long> linkedHashSet;
        LinkedHashSet<Long> linkedHashSet2;
        BluetoothDevice device;
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        BluetoothDevice device2 = bluetoothGatt.getDevice();
        if ((device2 != null ? device2.getAddress() : null) != null) {
            BluetoothDevice device3 = bluetoothGatt.getDevice();
            String address = device3 != null ? device3.getAddress() : null;
            BluetoothGatt bluetoothGatt2 = this.a.b;
            if (wg6.a(address, (bluetoothGatt2 == null || (device = bluetoothGatt2.getDevice()) == null) ? null : device.getAddress())) {
                cc0 cc0 = cc0.DEBUG;
                Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                if (i == 0) {
                    ue1 ue1 = this.a;
                    a01 a01 = ue1.c;
                    if (a01 != null) {
                        ue1.a(a01);
                    }
                    ue1 ue12 = this.a;
                    ue12.c = null;
                    ue12.a(new a01(i, i2, le6.a(new Long[]{Long.valueOf(System.currentTimeMillis())})));
                } else {
                    a01 a012 = this.a.c;
                    if (a012 != null && a012.a == i && a012.b == i2) {
                        if (((a012 == null || (linkedHashSet2 = a012.c) == null) ? 0 : linkedHashSet2.size()) < 1000) {
                            a01 a013 = this.a.c;
                            if (!(a013 == null || (linkedHashSet = a013.c) == null)) {
                                linkedHashSet.add(Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    }
                    ue1 ue13 = this.a;
                    a01 a014 = ue13.c;
                    if (a014 != null) {
                        ue13.a(a014);
                    }
                    this.a.c = new a01(i, i2, le6.a(new Long[]{Long.valueOf(System.currentTimeMillis())}));
                }
                this.a.a(i2, i);
            }
        }
    }

    @DexIgnore
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        cc0 cc0 = cc0.DEBUG;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        wg6.a(characteristic, "descriptor.characteristic");
        new Object[1][0] = characteristic.getUuid();
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        byte[] bArr = value;
        if (uuid == null || uuid2 == null) {
            cc0 cc02 = cc0.DEBUG;
            Object[] objArr = {uuid, uuid2};
            return;
        }
        ue1 ue1 = this.a;
        ue1.a.post(new zc1(ue1, t31.c.a(i), gk1.b.a(uuid), pk0.d.a(uuid2), bArr));
    }

    @DexIgnore
    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.a.j = i - 3;
        }
        ue1 ue1 = this.a;
        ue1.a.post(new i91(ue1, t31.c.a(i2), i));
    }

    @DexIgnore
    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        ue1 ue1 = this.a;
        ue1.a.post(new n51(ue1, t31.c.a(i2), i));
    }

    @DexIgnore
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        super.onServicesDiscovered(bluetoothGatt, i);
        List<BluetoothGattService> services = bluetoothGatt.getServices();
        ue1 ue1 = this.a;
        wg6.a(services, "services");
        ue1.a((List<? extends BluetoothGattService>) services);
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = yd6.a(services, "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, te1.a, 30, (Object) null);
        ue1 ue12 = this.a;
        t31 a2 = t31.c.a(i);
        ArrayList arrayList = new ArrayList(rd6.a(services, 10));
        for (BluetoothGattService bluetoothGattService : services) {
            wg6.a(bluetoothGattService, "it");
            arrayList.add(bluetoothGattService.getUuid());
        }
        Object[] array = arrayList.toArray(new UUID[0]);
        if (array != null) {
            UUID[] uuidArr = (UUID[]) array;
            Set<rg1> keySet = this.a.i.keySet();
            wg6.a(keySet, "gattCharacteristicMap.keys");
            Object[] array2 = keySet.toArray(new rg1[0]);
            if (array2 != null) {
                ue12.a.post(new hy0(ue12, a2, uuidArr, (rg1[]) array2));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
