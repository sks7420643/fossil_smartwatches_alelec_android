package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ NumberPicker s;
    @DexIgnore
    public /* final */ NumberPicker t;
    @DexIgnore
    public /* final */ NumberPicker u;
    @DexIgnore
    public /* final */ ConstraintLayout v;

    @DexIgnore
    public h94(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, LinearLayout linearLayout, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = numberPicker;
        this.t = numberPicker2;
        this.u = numberPicker3;
        this.v = constraintLayout;
    }
}
