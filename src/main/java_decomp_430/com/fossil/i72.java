package com.fossil;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i72 {
    @DexIgnore
    public static /* final */ DataType a; // = new DataType("com.google.blood_pressure", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", j72.a, j72.e, j72.i, j72.j);
    @DexIgnore
    public static /* final */ DataType b; // = new DataType("com.google.blood_glucose", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", j72.k, j72.l, h72.I, j72.m, j72.n);
    @DexIgnore
    public static /* final */ DataType c; // = new DataType("com.google.oxygen_saturation", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", j72.o, j72.s, j72.w, j72.x, j72.y);
    @DexIgnore
    public static /* final */ DataType d; // = new DataType("com.google.body.temperature", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", j72.z, j72.A);
    @DexIgnore
    public static /* final */ DataType e; // = new DataType("com.google.body.temperature.basal", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", j72.z, j72.A);
    @DexIgnore
    public static /* final */ DataType f; // = new DataType("com.google.cervical_mucus", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", j72.B, j72.C);
    @DexIgnore
    public static /* final */ DataType g; // = new DataType("com.google.cervical_position", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", j72.D, j72.E, j72.F);
    @DexIgnore
    public static /* final */ DataType h; // = new DataType("com.google.menstruation", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", j72.G);
    @DexIgnore
    public static /* final */ DataType i; // = new DataType("com.google.ovulation_test", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", j72.H);
    @DexIgnore
    public static /* final */ DataType j; // = new DataType("com.google.vaginal_spotting", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", h72.d0);
    @DexIgnore
    public static /* final */ DataType k; // = new DataType("com.google.blood_pressure.summary", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", j72.b, j72.d, j72.c, j72.f, j72.h, j72.g, j72.i, j72.j);
    @DexIgnore
    public static /* final */ DataType l; // = new DataType("com.google.blood_glucose.summary", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", h72.W, h72.X, h72.Y, j72.l, h72.I, j72.m, j72.n);
    @DexIgnore
    public static /* final */ DataType m; // = new DataType("com.google.oxygen_saturation.summary", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", j72.p, j72.r, j72.q, j72.t, j72.v, j72.u, j72.w, j72.x, j72.y);
    @DexIgnore
    public static /* final */ DataType n; // = new DataType("com.google.body.temperature.summary", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", h72.W, h72.X, h72.Y, j72.A);
    @DexIgnore
    public static /* final */ DataType o; // = new DataType("com.google.body.temperature.basal.summary", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", h72.W, h72.X, h72.Y, j72.A);
}
