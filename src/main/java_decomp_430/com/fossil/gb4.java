package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gb4 extends fb4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j d0; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray e0; // = new SparseIntArray();
    @DexIgnore
    public long c0;

    /*
    static {
        e0.put(2131362584, 1);
        e0.put(2131362636, 2);
        e0.put(2131362637, 3);
        e0.put(2131363225, 4);
        e0.put(2131363158, 5);
        e0.put(2131363179, 6);
        e0.put(2131362478, 7);
        e0.put(2131363200, 8);
        e0.put(2131361971, 9);
        e0.put(2131363280, 10);
        e0.put(2131362479, 11);
        e0.put(2131362009, 12);
        e0.put(2131362503, 13);
        e0.put(2131363088, 14);
        e0.put(2131363090, 15);
        e0.put(2131363089, 16);
        e0.put(2131362005, 17);
        e0.put(2131362502, 18);
        e0.put(2131363079, 19);
        e0.put(2131363081, 20);
        e0.put(2131363080, 21);
        e0.put(2131362010, 22);
        e0.put(2131362504, 23);
        e0.put(2131363082, 24);
        e0.put(2131363084, 25);
        e0.put(2131363083, 26);
        e0.put(2131362020, 27);
        e0.put(2131362505, 28);
        e0.put(2131363085, 29);
        e0.put(2131363087, 30);
        e0.put(2131363086, 31);
        e0.put(2131362047, 32);
        e0.put(2131362445, 33);
        e0.put(2131362328, 34);
        e0.put(2131361949, 35);
        e0.put(2131363130, 36);
        e0.put(2131362649, 37);
        e0.put(2131362232, 38);
        e0.put(2131362873, 39);
        e0.put(2131362138, 40);
        e0.put(2131361920, 41);
        e0.put(2131363210, 42);
        e0.put(2131362688, 43);
        e0.put(2131361950, 44);
        e0.put(2131362623, 45);
        e0.put(2131361930, 46);
        e0.put(2131362551, 47);
        e0.put(2131363263, 48);
        e0.put(2131361954, 49);
        e0.put(2131362635, 50);
        e0.put(2131361933, 51);
        e0.put(2131362564, 52);
        e0.put(2131361944, 53);
        e0.put(2131362613, 54);
        e0.put(2131361938, 55);
        e0.put(2131362599, 56);
        e0.put(2131361927, 57);
        e0.put(2131362532, 58);
        e0.put(2131361952, 59);
        e0.put(2131362629, 60);
        e0.put(2131363176, 61);
    }
    */

    @DexIgnore
    public gb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 62, d0, e0));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.c0 = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.c0 != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.c0 = 1;
        }
        g();
    }

    @DexIgnore
    public gb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[41], objArr[57], objArr[46], objArr[51], objArr[55], objArr[53], objArr[35], objArr[44], objArr[59], objArr[49], objArr[9], objArr[17], objArr[12], objArr[22], objArr[27], objArr[32], objArr[40], objArr[38], objArr[34], objArr[33], objArr[7], objArr[11], objArr[18], objArr[13], objArr[23], objArr[28], objArr[58], objArr[47], objArr[52], objArr[1], objArr[56], objArr[54], objArr[45], objArr[60], objArr[50], objArr[2], objArr[3], objArr[37], objArr[43], objArr[0], objArr[39], objArr[19], objArr[21], objArr[20], objArr[24], objArr[26], objArr[25], objArr[29], objArr[31], objArr[30], objArr[14], objArr[16], objArr[15], objArr[36], objArr[5], objArr[61], objArr[6], objArr[8], objArr[42], objArr[4], objArr[48], objArr[10]);
        this.c0 = -1;
        this.M.setTag((Object) null);
        a(view);
        f();
    }
}
