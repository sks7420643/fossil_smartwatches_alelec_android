package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa5 implements Factory<ad5> {
    @DexIgnore
    public static ad5 a(na5 na5) {
        ad5 c = na5.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
