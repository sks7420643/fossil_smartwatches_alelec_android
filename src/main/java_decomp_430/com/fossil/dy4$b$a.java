package com.fossil;

import com.fossil.NotificationAppsPresenter;
import com.fossil.gy4;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy4$b$a implements y24.d<gy4.c, y24.a> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter.b a;

    @DexIgnore
    public dy4$b$a(NotificationAppsPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(gy4.c cVar) {
        FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onSuccess");
        NotificationAppsPresenter.this.k.a();
        NotificationAppsPresenter.this.k.close();
    }

    @DexIgnore
    public void a(y24.a aVar) {
        FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), ".Inside mSaveAppsNotification onError");
        NotificationAppsPresenter.this.k.a();
        NotificationAppsPresenter.this.k.close();
    }
}
