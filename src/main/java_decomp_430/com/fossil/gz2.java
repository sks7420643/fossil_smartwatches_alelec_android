package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz2 implements Parcelable.Creator<LatLng> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        double d = 0.0d;
        double d2 = 0.0d;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                d = f22.l(parcel, a);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                d2 = f22.l(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new LatLng(d, d2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LatLng[i];
    }
}
