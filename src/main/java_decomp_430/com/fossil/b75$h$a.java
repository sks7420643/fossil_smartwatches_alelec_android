package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.OutputFormat;
import com.fossil.imagefilters.OutputSettings;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
public final class b75$h$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ EditPhotoViewModel.h this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b75$h$a(EditPhotoViewModel.h hVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        b75$h$a b75_h_a = new b75$h$a(this.this$0, xe6);
        b75_h_a.p$ = (il6) obj;
        return b75_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((b75$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            EditPhotoViewModel.d dVar = this.this$0.$cropImageInfo;
            Bitmap a = lw4.a(lw4.a(dVar.c(), dVar.h(), dVar.d(), dVar.e(), dVar.a(), dVar.b(), dVar.f(), dVar.g()).a, 480, 480);
            EInkImageFilter create = EInkImageFilter.create();
            wg6.a((Object) create, "EInkImageFilter.create()");
            wg6.a((Object) a, "bitmap");
            Bitmap bitmap = a;
            create.apply(bitmap, this.this$0.$filterType, false, false, new OutputSettings(a.getWidth(), a.getHeight(), OutputFormat.BACKGROUND));
            StringBuilder sb = new StringBuilder();
            Context context = this.this$0.$context;
            wg6.a((Object) context, "context");
            sb.append(context.getFilesDir());
            sb.append(File.separator);
            lw4.a(this.this$0.$context, sb.toString(), "previewPhoto", a);
            this.this$0.this$0.d().a(new EditPhotoViewModel.e(true, (String) null, 2, (qg6) null));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
