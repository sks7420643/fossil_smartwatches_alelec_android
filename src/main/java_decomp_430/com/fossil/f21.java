package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f21 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ x51 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f21(x51 x51) {
        super(0);
        this.a = x51;
    }

    @DexIgnore
    public Object invoke() {
        ie1 c = this.a.n();
        if (c != null) {
            g01 g01 = new g01(c.c());
            ie1 a2 = this.a.a(g01.a, g01.b);
            if (a2 == null && (a2 = this.a.n()) == null) {
                wg6.a();
                throw null;
            }
            this.a.b(a2);
            return cd6.a;
        }
        wg6.a();
        throw null;
    }
}
