package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no1 extends yw3<no1, c> implements oo1 {
    @DexIgnore
    public static /* final */ no1 g; // = new no1();
    @DexIgnore
    public static volatile gx3<no1> h;
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;

    @DexIgnore
    public enum a implements zw3.a {
        zza(0),
        UNMETERED_ONLY(1),
        UNMETERED_OR_DAILY(2),
        FAST_IF_RADIO_AWAKE(3),
        NEVER(4),
        UNRECOGNIZED(-1);
        
        @DexIgnore
        public /* final */ int zzg;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.no1$a$a")
        /* renamed from: com.fossil.no1$a$a  reason: collision with other inner class name */
        public class C0033a implements zw3.b<a> {
        }

        /*
        static {
            zza = new a(MessengerShareContentUtility.PREVIEW_DEFAULT, 0, 0);
            UNMETERED_ONLY = new a("UNMETERED_ONLY", 1, 1);
            UNMETERED_OR_DAILY = new a("UNMETERED_OR_DAILY", 2, 2);
            FAST_IF_RADIO_AWAKE = new a("FAST_IF_RADIO_AWAKE", 3, 3);
            NEVER = new a("NEVER", 4, 4);
            UNRECOGNIZED = new a("UNRECOGNIZED", 5, -1);
            a[] aVarArr = {zza, UNMETERED_ONLY, UNMETERED_OR_DAILY, FAST_IF_RADIO_AWAKE, NEVER, UNRECOGNIZED};
            new C0033a();
        }
        */

        @DexIgnore
        public a(int i) {
            this.zzg = i;
        }

        @DexIgnore
        public static a zza(int i) {
            if (i == 0) {
                return zza;
            }
            if (i == 1) {
                return UNMETERED_ONLY;
            }
            if (i == 2) {
                return UNMETERED_OR_DAILY;
            }
            if (i == 3) {
                return FAST_IF_RADIO_AWAKE;
            }
            if (i != 4) {
                return null;
            }
            return NEVER;
        }

        @DexIgnore
        public final int getNumber() {
            return this.zzg;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends yw3.b<no1, c> implements oo1 {
        @DexIgnore
        public /* synthetic */ c(b bVar) {
            this();
        }

        @DexIgnore
        public c() {
            super(no1.g);
        }
    }

    /*
    static {
        g.g();
    }
    */

    @DexIgnore
    public static gx3<no1> k() {
        return g.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (b.a[jVar.ordinal()]) {
            case 1:
                return new no1();
            case 2:
                return g;
            case 3:
                return null;
            case 4:
                return new c((b) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                no1 no1 = (no1) obj2;
                this.d = kVar.a(!this.d.isEmpty(), this.d, !no1.d.isEmpty(), no1.d);
                this.e = kVar.a(this.e != 0, this.e, no1.e != 0, no1.e);
                boolean z2 = this.f != 0;
                int i = this.f;
                if (no1.f != 0) {
                    z = true;
                }
                this.f = kVar.a(z2, i, z, no1.f);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 10) {
                                this.d = tw3.k();
                            } else if (l == 16) {
                                this.e = tw3.c();
                            } else if (l == 24) {
                                this.f = tw3.d();
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (h == null) {
                    synchronized (no1.class) {
                        if (h == null) {
                            h = new yw3.c(g);
                        }
                    }
                }
                return h;
            default:
                throw new UnsupportedOperationException();
        }
        return g;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.d.isEmpty()) {
            i2 = 0 + uw3.b(1, this.d);
        }
        if (this.e != a.zza.getNumber()) {
            i2 += uw3.c(2, this.e);
        }
        int i3 = this.f;
        if (i3 != 0) {
            i2 += uw3.d(3, i3);
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        if (!this.d.isEmpty()) {
            uw3.a(1, this.d);
        }
        if (this.e != a.zza.getNumber()) {
            uw3.a(2, this.e);
        }
        int i = this.f;
        if (i != 0) {
            uw3.b(3, i);
        }
    }
}
