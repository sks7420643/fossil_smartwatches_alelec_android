package com.fossil;

import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1", f = "SignUpPresenter.kt", l = {239, 240, 241}, m = "invokeSuspend")
public final class ut5$j$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter.j this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$1", f = "SignUpPresenter.kt", l = {239}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super ap4<ActivitySettings>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ut5$j$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ut5$j$a ut5_j_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ut5_j_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                SummariesRepository x = this.this$0.this$0.a.x();
                this.L$0 = il6;
                this.label = 1;
                obj = x.fetchActivitySettings(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$2", f = "SignUpPresenter.kt", l = {240}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super ap4<MFSleepSettings>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ut5$j$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ut5$j$a ut5_j_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ut5_j_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                SleepSummariesRepository w = this.this$0.this$0.a.w();
                this.L$0 = il6;
                this.label = 1;
                obj = w.fetchLastSleepGoal(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$signupEmail$1$onSuccess$1$3", f = "SignUpPresenter.kt", l = {241}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super ap4<GoalSetting>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ut5$j$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ut5$j$a ut5_j_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ut5_j_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                GoalTrackingRepository u = this.this$0.this$0.a.u();
                this.L$0 = il6;
                this.label = 1;
                obj = u.fetchGoalSetting(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut5$j$a(SignUpPresenter.j jVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = jVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ut5$j$a ut5_j_a = new ut5$j$a(this.this$0, xe6);
        ut5_j_a.p$ = (il6) obj;
        return ut5_j_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ut5$j$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007f A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        dl6 b2;
        c cVar;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            dl6 b3 = this.this$0.a.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il62;
            this.label = 1;
            if (gk6.a(b3, aVar, this) == a2) {
                return a2;
            }
            il6 = il62;
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
            b2 = this.this$0.a.c();
            cVar = new c(this, (xe6) null);
            this.L$0 = il6;
            this.label = 3;
            if (gk6.a(b2, cVar, this) == a2) {
                return a2;
            }
            return cd6.a;
        } else if (i == 3) {
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        dl6 b4 = this.this$0.a.c();
        b bVar = new b(this, (xe6) null);
        this.L$0 = il6;
        this.label = 2;
        if (gk6.a(b4, bVar, this) == a2) {
            return a2;
        }
        b2 = this.this$0.a.c();
        cVar = new c(this, (xe6) null);
        this.L$0 = il6;
        this.label = 3;
        if (gk6.a(b2, cVar, this) == a2) {
        }
        return cd6.a;
    }
}
