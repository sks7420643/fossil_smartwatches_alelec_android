package com.fossil;

import com.fossil.fv;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dv {
    @DexIgnore
    public static final dv a = new fv.a().a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements dv {
        @DexIgnore
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    Map<String, String> a();
}
