package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lu1 extends IInterface {
    @DexIgnore
    void a(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException;

    @DexIgnore
    void a(Status status) throws RemoteException;

    @DexIgnore
    void b(Status status) throws RemoteException;
}
