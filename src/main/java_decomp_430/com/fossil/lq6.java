package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lq6 {
    @DexIgnore
    public static final lq6 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements lq6 {
        @DexIgnore
        public List<kq6> a(tq6 tq6) {
            return Collections.emptyList();
        }

        @DexIgnore
        public void a(tq6 tq6, List<kq6> list) {
        }
    }

    @DexIgnore
    List<kq6> a(tq6 tq6);

    @DexIgnore
    void a(tq6 tq6, List<kq6> list);
}
