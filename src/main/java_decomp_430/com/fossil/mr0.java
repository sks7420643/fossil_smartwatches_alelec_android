package com.fossil;

import com.fossil.a70;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class mr0 extends tg6 implements hg6<byte[], a70> {
    @DexIgnore
    public mr0(a70.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(a70.b.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/VibeStrengthConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((a70.b) this.receiver).a((byte[]) obj);
    }
}
