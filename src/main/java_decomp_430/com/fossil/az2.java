package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.x52;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<az2> CREATOR; // = new hz2();
    @DexIgnore
    public LatLng a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public xy2 d;
    @DexIgnore
    public float e; // = 0.5f;
    @DexIgnore
    public float f; // = 1.0f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public float j; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float o; // = 0.5f;
    @DexIgnore
    public float p; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public float q; // = 1.0f;
    @DexIgnore
    public float r;

    @DexIgnore
    public az2() {
    }

    @DexIgnore
    public final float B() {
        return this.e;
    }

    @DexIgnore
    public final float C() {
        return this.f;
    }

    @DexIgnore
    public final float D() {
        return this.o;
    }

    @DexIgnore
    public final float E() {
        return this.p;
    }

    @DexIgnore
    public final LatLng F() {
        return this.a;
    }

    @DexIgnore
    public final float G() {
        return this.j;
    }

    @DexIgnore
    public final String H() {
        return this.c;
    }

    @DexIgnore
    public final String I() {
        return this.b;
    }

    @DexIgnore
    public final float J() {
        return this.r;
    }

    @DexIgnore
    public final boolean K() {
        return this.g;
    }

    @DexIgnore
    public final boolean L() {
        return this.i;
    }

    @DexIgnore
    public final boolean M() {
        return this.h;
    }

    @DexIgnore
    public final az2 a(LatLng latLng) {
        if (latLng != null) {
            this.a = latLng;
            return this;
        }
        throw new IllegalArgumentException("latlng cannot be null - a position is required.");
    }

    @DexIgnore
    public final az2 e(String str) {
        this.c = str;
        return this;
    }

    @DexIgnore
    public final float p() {
        return this.q;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        IBinder iBinder;
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, (Parcelable) F(), i2, false);
        g22.a(parcel, 3, I(), false);
        g22.a(parcel, 4, H(), false);
        xy2 xy2 = this.d;
        if (xy2 == null) {
            iBinder = null;
        } else {
            iBinder = xy2.a().asBinder();
        }
        g22.a(parcel, 5, iBinder, false);
        g22.a(parcel, 6, B());
        g22.a(parcel, 7, C());
        g22.a(parcel, 8, K());
        g22.a(parcel, 9, M());
        g22.a(parcel, 10, L());
        g22.a(parcel, 11, G());
        g22.a(parcel, 12, D());
        g22.a(parcel, 13, E());
        g22.a(parcel, 14, p());
        g22.a(parcel, 15, J());
        g22.a(parcel, a2);
    }

    @DexIgnore
    public final az2 a(xy2 xy2) {
        this.d = xy2;
        return this;
    }

    @DexIgnore
    public az2(LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2, boolean z3, float f4, float f5, float f6, float f7, float f8) {
        this.a = latLng;
        this.b = str;
        this.c = str2;
        if (iBinder == null) {
            this.d = null;
        } else {
            this.d = new xy2(x52.a.a(iBinder));
        }
        this.e = f2;
        this.f = f3;
        this.g = z;
        this.h = z2;
        this.i = z3;
        this.j = f4;
        this.o = f5;
        this.p = f6;
        this.q = f7;
        this.r = f8;
    }
}
