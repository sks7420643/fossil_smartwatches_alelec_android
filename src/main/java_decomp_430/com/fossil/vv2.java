package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface vv2 {
    @DexIgnore
    yv1<Status> a(wv1 wv1, zv2 zv2);

    @DexIgnore
    yv1<Status> a(wv1 wv1, LocationRequest locationRequest, zv2 zv2);
}
