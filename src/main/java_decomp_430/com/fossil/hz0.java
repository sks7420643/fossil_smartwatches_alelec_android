package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hz0 implements sj0 {
    SUCCESS((byte) 0),
    UNKNOWN((byte) 255);
    
    @DexIgnore
    public static /* final */ nx0 f; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        f = new nx0((qg6) null);
    }
    */

    @DexIgnore
    public hz0(byte b2) {
        this.b = b2;
        this.a = cw0.a((Enum<?>) this);
    }

    @DexIgnore
    public boolean a() {
        return this == SUCCESS;
    }

    @DexIgnore
    public String getLogName() {
        return this.a;
    }
}
