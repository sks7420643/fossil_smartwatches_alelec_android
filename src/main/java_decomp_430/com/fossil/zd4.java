package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ FlexibleProgressBar H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ ViewPager2 J;
    @DexIgnore
    public /* final */ ViewPager2 K;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public /* final */ TabLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zd4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, LinearLayout linearLayout, ConstraintLayout constraintLayout5, TabLayout tabLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, RTLImageView rTLImageView, RTLImageView rTLImageView2, ImageView imageView, RTLImageView rTLImageView3, View view2, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout6, ViewPager2 viewPager2, ViewPager2 viewPager22, FlexibleTextView flexibleTextView11, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = constraintLayout3;
        this.t = linearLayout;
        this.u = tabLayout;
        this.v = flexibleTextView;
        this.w = flexibleTextView2;
        this.x = flexibleTextView3;
        this.y = flexibleTextView4;
        this.z = flexibleTextView5;
        this.A = flexibleTextView6;
        this.B = flexibleTextView7;
        this.C = flexibleTextView10;
        this.D = rTLImageView;
        this.E = rTLImageView2;
        this.F = rTLImageView3;
        this.G = view2;
        this.H = flexibleProgressBar;
        this.I = constraintLayout6;
        this.J = viewPager2;
        this.K = viewPager22;
    }
}
