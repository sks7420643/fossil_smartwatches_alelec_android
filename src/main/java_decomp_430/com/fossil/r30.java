package com.fossil;

import com.fossil.y30;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r30 implements y30 {
    @DexIgnore
    public /* final */ File a;

    @DexIgnore
    public r30(File file) {
        this.a = file;
    }

    @DexIgnore
    public Map<String, String> a() {
        return null;
    }

    @DexIgnore
    public String b() {
        return this.a.getName();
    }

    @DexIgnore
    public File c() {
        return null;
    }

    @DexIgnore
    public File[] d() {
        return this.a.listFiles();
    }

    @DexIgnore
    public String e() {
        return null;
    }

    @DexIgnore
    public y30.a getType() {
        return y30.a.NATIVE;
    }

    @DexIgnore
    public void remove() {
        for (File file : d()) {
            c86.g().d("CrashlyticsCore", "Removing native report file at " + file.getPath());
            file.delete();
        }
        c86.g().d("CrashlyticsCore", "Removing native report directory at " + this.a);
        this.a.delete();
    }
}
