package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o10 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ q10 b;
    @DexIgnore
    public n10 c;

    @DexIgnore
    public o10(Context context) {
        this(context, new q10());
    }

    @DexIgnore
    public n10 a() {
        if (this.c == null) {
            this.c = h10.b(this.a);
        }
        return this.c;
    }

    @DexIgnore
    public o10(Context context, q10 q10) {
        this.a = context;
        this.b = q10;
    }

    @DexIgnore
    public void a(a20 a20) {
        n10 a2 = a();
        if (a2 == null) {
            c86.g().d("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        p10 a3 = this.b.a(a20);
        if (a3 == null) {
            l86 g = c86.g();
            g.d("Answers", "Fabric event was not mappable to Firebase event: " + a20);
            return;
        }
        a2.a(a3.a(), a3.b());
        if ("levelEnd".equals(a20.g)) {
            a2.a("post_score", a3.b());
        }
    }
}
