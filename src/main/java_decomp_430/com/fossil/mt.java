package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt extends Exception {
    @DexIgnore
    public static /* final */ StackTraceElement[] a; // = new StackTraceElement[0];
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ List<Throwable> causes;
    @DexIgnore
    public Class<?> dataClass;
    @DexIgnore
    public pr dataSource;
    @DexIgnore
    public String detailMessage;
    @DexIgnore
    public Exception exception;
    @DexIgnore
    public vr key;

    @DexIgnore
    public mt(String str) {
        this(str, (List<Throwable>) Collections.emptyList());
    }

    @DexIgnore
    public static void b(List<Throwable> list, Appendable appendable) throws IOException {
        int size = list.size();
        int i = 0;
        while (i < size) {
            int i2 = i + 1;
            appendable.append("Cause (").append(String.valueOf(i2)).append(" of ").append(String.valueOf(size)).append("): ");
            Throwable th = list.get(i);
            if (th instanceof mt) {
                ((mt) th).a(appendable);
            } else {
                a(th, appendable);
            }
            i = i2;
        }
    }

    @DexIgnore
    public final void a(Throwable th, List<Throwable> list) {
        if (th instanceof mt) {
            for (Throwable a2 : ((mt) th).getCauses()) {
                a(a2, list);
            }
            return;
        }
        list.add(th);
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        return this;
    }

    @DexIgnore
    public List<Throwable> getCauses() {
        return this.causes;
    }

    @DexIgnore
    public String getMessage() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder(71);
        sb.append(this.detailMessage);
        String str3 = "";
        if (this.dataClass != null) {
            str = ", " + this.dataClass;
        } else {
            str = str3;
        }
        sb.append(str);
        if (this.dataSource != null) {
            str2 = ", " + this.dataSource;
        } else {
            str2 = str3;
        }
        sb.append(str2);
        if (this.key != null) {
            str3 = ", " + this.key;
        }
        sb.append(str3);
        List<Throwable> rootCauses = getRootCauses();
        if (rootCauses.isEmpty()) {
            return sb.toString();
        }
        if (rootCauses.size() == 1) {
            sb.append("\nThere was 1 cause:");
        } else {
            sb.append("\nThere were ");
            sb.append(rootCauses.size());
            sb.append(" causes:");
        }
        for (Throwable next : rootCauses) {
            sb.append(10);
            sb.append(next.getClass().getName());
            sb.append('(');
            sb.append(next.getMessage());
            sb.append(')');
        }
        sb.append("\n call GlideException#logRootCauses(String) for more detail");
        return sb.toString();
    }

    @DexIgnore
    public Exception getOrigin() {
        return this.exception;
    }

    @DexIgnore
    public List<Throwable> getRootCauses() {
        ArrayList arrayList = new ArrayList();
        a((Throwable) this, (List<Throwable>) arrayList);
        return arrayList;
    }

    @DexIgnore
    public void logRootCauses(String str) {
        List<Throwable> rootCauses = getRootCauses();
        int size = rootCauses.size();
        int i = 0;
        while (i < size) {
            StringBuilder sb = new StringBuilder();
            sb.append("Root cause (");
            int i2 = i + 1;
            sb.append(i2);
            sb.append(" of ");
            sb.append(size);
            sb.append(")");
            Log.i(str, sb.toString(), rootCauses.get(i));
            i = i2;
        }
    }

    @DexIgnore
    public void printStackTrace() {
        printStackTrace(System.err);
    }

    @DexIgnore
    public void setLoggingDetails(vr vrVar, pr prVar) {
        setLoggingDetails(vrVar, prVar, (Class<?>) null);
    }

    @DexIgnore
    public void setOrigin(Exception exc) {
        this.exception = exc;
    }

    @DexIgnore
    public mt(String str, Throwable th) {
        this(str, (List<Throwable>) Collections.singletonList(th));
    }

    @DexIgnore
    public void printStackTrace(PrintStream printStream) {
        a(printStream);
    }

    @DexIgnore
    public void setLoggingDetails(vr vrVar, pr prVar, Class<?> cls) {
        this.key = vrVar;
        this.dataSource = prVar;
        this.dataClass = cls;
    }

    @DexIgnore
    public mt(String str, List<Throwable> list) {
        this.detailMessage = str;
        setStackTrace(a);
        this.causes = list;
    }

    @DexIgnore
    public void printStackTrace(PrintWriter printWriter) {
        a(printWriter);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Appendable {
        @DexIgnore
        public /* final */ Appendable a;
        @DexIgnore
        public boolean b; // = true;

        @DexIgnore
        public a(Appendable appendable) {
            this.a = appendable;
        }

        @DexIgnore
        public final CharSequence a(CharSequence charSequence) {
            return charSequence == null ? "" : charSequence;
        }

        @DexIgnore
        public Appendable append(char c) throws IOException {
            boolean z = false;
            if (this.b) {
                this.b = false;
                this.a.append("  ");
            }
            if (c == 10) {
                z = true;
            }
            this.b = z;
            this.a.append(c);
            return this;
        }

        @DexIgnore
        public Appendable append(CharSequence charSequence) throws IOException {
            CharSequence a2 = a(charSequence);
            append(a2, 0, a2.length());
            return this;
        }

        @DexIgnore
        public Appendable append(CharSequence charSequence, int i, int i2) throws IOException {
            CharSequence a2 = a(charSequence);
            boolean z = false;
            if (this.b) {
                this.b = false;
                this.a.append("  ");
            }
            if (a2.length() > 0 && a2.charAt(i2 - 1) == 10) {
                z = true;
            }
            this.b = z;
            this.a.append(a2, i, i2);
            return this;
        }
    }

    @DexIgnore
    public final void a(Appendable appendable) {
        a((Throwable) this, appendable);
        a(getCauses(), (Appendable) new a(appendable));
    }

    @DexIgnore
    public static void a(Throwable th, Appendable appendable) {
        try {
            appendable.append(th.getClass().toString()).append(": ").append(th.getMessage()).append(10);
        } catch (IOException unused) {
            throw new RuntimeException(th);
        }
    }

    @DexIgnore
    public static void a(List<Throwable> list, Appendable appendable) {
        try {
            b(list, appendable);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
