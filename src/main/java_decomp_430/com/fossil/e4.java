package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e4 {
    @DexIgnore
    public static /* final */ int cardview_dark_background; // = 2131099717;
    @DexIgnore
    public static /* final */ int cardview_light_background; // = 2131099718;
    @DexIgnore
    public static /* final */ int cardview_shadow_end_color; // = 2131099719;
    @DexIgnore
    public static /* final */ int cardview_shadow_start_color; // = 2131099720;
}
