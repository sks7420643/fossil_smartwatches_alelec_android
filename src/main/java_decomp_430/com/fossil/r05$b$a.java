package com.fossil;

import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindTimeDao;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter$save$1$1", f = "RemindTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class r05$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemindTimePresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r05$b$a(RemindTimePresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        r05$b$a r05_b_a = new r05$b$a(this.this$0, xe6);
        r05_b_a.p$ = (il6) obj;
        return r05_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((r05$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            RemindTimeDao remindTimeDao = this.this$0.this$0.h.getRemindTimeDao();
            RemindTimeModel b = this.this$0.this$0.f;
            if (b != null) {
                remindTimeDao.upsertRemindTimeModel(b);
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
