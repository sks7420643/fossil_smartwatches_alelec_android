package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on1 {
    @DexIgnore
    public /* synthetic */ on1(qg6 qg6) {
    }

    @DexIgnore
    public final lf0 a(t31 t31) {
        switch (yl1.a[t31.a.ordinal()]) {
            case 1:
                return lf0.SUCCESS;
            case 2:
                return lf0.HID_PROXY_NOT_CONNECTED;
            case 3:
                return lf0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
            case 4:
                return lf0.HID_INPUT_DEVICE_DISABLED;
            case 5:
                return lf0.HID_UNKNOWN_ERROR;
            case 6:
                return lf0.BLUETOOTH_OFF;
            default:
                return lf0.GATT_ERROR;
        }
    }
}
