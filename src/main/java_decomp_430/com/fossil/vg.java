package com.fossil;

import androidx.renderscript.RenderScript;
import com.facebook.stetho.websocket.CloseCodes;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vg extends ug {
    @DexIgnore
    public int d;
    @DexIgnore
    public c e;
    @DexIgnore
    public b f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[c.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0051 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0072 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0089 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0095 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00a1 */
        /*
        static {
            try {
                b[b.PIXEL_LA.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[b.PIXEL_RGB.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[b.PIXEL_RGBA.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            a[c.FLOAT_32.ordinal()] = 1;
            a[c.FLOAT_64.ordinal()] = 2;
            a[c.SIGNED_8.ordinal()] = 3;
            a[c.SIGNED_16.ordinal()] = 4;
            a[c.SIGNED_32.ordinal()] = 5;
            a[c.SIGNED_64.ordinal()] = 6;
            a[c.UNSIGNED_8.ordinal()] = 7;
            a[c.UNSIGNED_16.ordinal()] = 8;
            a[c.UNSIGNED_32.ordinal()] = 9;
            a[c.UNSIGNED_64.ordinal()] = 10;
            a[c.BOOLEAN.ordinal()] = 11;
        }
        */
    }

    @DexIgnore
    public enum b {
        USER(0),
        PIXEL_L(7),
        PIXEL_A(8),
        PIXEL_LA(9),
        PIXEL_RGB(10),
        PIXEL_RGBA(11),
        PIXEL_DEPTH(12),
        PIXEL_YUV(13);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    @DexIgnore
    public vg(long j, RenderScript renderScript, c cVar, b bVar, boolean z, int i) {
        super(j, renderScript);
        if (cVar == c.UNSIGNED_5_6_5 || cVar == c.UNSIGNED_4_4_4_4 || cVar == c.UNSIGNED_5_5_5_1) {
            this.d = cVar.mSize;
        } else if (i == 3) {
            this.d = cVar.mSize * 4;
        } else {
            this.d = cVar.mSize * i;
        }
        this.e = cVar;
        this.f = bVar;
        this.g = z;
        this.h = i;
    }

    @DexIgnore
    public static vg a(RenderScript renderScript, c cVar) {
        b bVar = b.USER;
        return new vg(renderScript.a((long) cVar.mID, bVar.mID, false, 1), renderScript, cVar, bVar, false, 1);
    }

    @DexIgnore
    public static vg c(RenderScript renderScript) {
        if (renderScript.n == null) {
            renderScript.n = a(renderScript, c.UNSIGNED_8, b.PIXEL_A);
        }
        return renderScript.n;
    }

    @DexIgnore
    public static vg d(RenderScript renderScript) {
        if (renderScript.p == null) {
            renderScript.p = a(renderScript, c.UNSIGNED_4_4_4_4, b.PIXEL_RGBA);
        }
        return renderScript.p;
    }

    @DexIgnore
    public static vg f(RenderScript renderScript) {
        if (renderScript.o == null) {
            renderScript.o = a(renderScript, c.UNSIGNED_5_6_5, b.PIXEL_RGB);
        }
        return renderScript.o;
    }

    @DexIgnore
    public static vg g(RenderScript renderScript) {
        if (renderScript.m == null) {
            renderScript.m = a(renderScript, c.UNSIGNED_8);
        }
        return renderScript.m;
    }

    @DexIgnore
    public static vg h(RenderScript renderScript) {
        if (renderScript.r == null) {
            renderScript.r = a(renderScript, c.UNSIGNED_8, 4);
        }
        return renderScript.r;
    }

    @DexIgnore
    public long b(RenderScript renderScript) {
        return renderScript.b((long) this.e.mID, this.f.mID, this.g, this.h);
    }

    @DexIgnore
    public int e() {
        return this.d;
    }

    @DexIgnore
    public static vg e(RenderScript renderScript) {
        if (renderScript.q == null) {
            renderScript.q = a(renderScript, c.UNSIGNED_8, b.PIXEL_RGBA);
        }
        return renderScript.q;
    }

    @DexIgnore
    public enum c {
        NONE(0, 0),
        FLOAT_32(2, 4),
        FLOAT_64(3, 8),
        SIGNED_8(4, 1),
        SIGNED_16(5, 2),
        SIGNED_32(6, 4),
        SIGNED_64(7, 8),
        UNSIGNED_8(8, 1),
        UNSIGNED_16(9, 2),
        UNSIGNED_32(10, 4),
        UNSIGNED_64(11, 8),
        BOOLEAN(12, 1),
        UNSIGNED_5_6_5(13, 2),
        UNSIGNED_5_5_5_1(14, 2),
        UNSIGNED_4_4_4_4(15, 2),
        MATRIX_4X4(16, 64),
        MATRIX_3X3(17, 36),
        MATRIX_2X2(18, 16),
        RS_ELEMENT(1000),
        RS_TYPE(1001),
        RS_ALLOCATION(CloseCodes.PROTOCOL_ERROR),
        RS_SAMPLER(1003),
        RS_SCRIPT(1004);
        
        @DexIgnore
        public int mID;
        @DexIgnore
        public int mSize;

        @DexIgnore
        public c(int i, int i2) {
            this.mID = i;
            this.mSize = i2;
        }

        @DexIgnore
        public c(int i) {
            this.mID = i;
            this.mSize = 4;
            if (RenderScript.G == 8) {
                this.mSize = 32;
            }
        }
    }

    @DexIgnore
    public static vg a(RenderScript renderScript, c cVar, int i) {
        if (i < 2 || i > 4) {
            throw new yg("Vector size out of range 2-4.");
        }
        switch (a.a[cVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                b bVar = b.USER;
                return new vg(renderScript.a((long) cVar.mID, bVar.mID, false, i), renderScript, cVar, bVar, false, i);
            default:
                throw new yg("Cannot create vector of non-primitive type.");
        }
    }

    @DexIgnore
    public static vg a(RenderScript renderScript, c cVar, b bVar) {
        if (bVar != b.PIXEL_L && bVar != b.PIXEL_A && bVar != b.PIXEL_LA && bVar != b.PIXEL_RGB && bVar != b.PIXEL_RGBA && bVar != b.PIXEL_DEPTH && bVar != b.PIXEL_YUV) {
            throw new yg("Unsupported DataKind");
        } else if (cVar != c.UNSIGNED_8 && cVar != c.UNSIGNED_16 && cVar != c.UNSIGNED_5_6_5 && cVar != c.UNSIGNED_4_4_4_4 && cVar != c.UNSIGNED_5_5_5_1) {
            throw new yg("Unsupported DataType");
        } else if (cVar == c.UNSIGNED_5_6_5 && bVar != b.PIXEL_RGB) {
            throw new yg("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_5_5_5_1 && bVar != b.PIXEL_RGBA) {
            throw new yg("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_4_4_4_4 && bVar != b.PIXEL_RGBA) {
            throw new yg("Bad kind and type combo");
        } else if (cVar != c.UNSIGNED_16 || bVar == b.PIXEL_DEPTH) {
            int i = a.b[bVar.ordinal()];
            int i2 = i != 1 ? i != 2 ? i != 3 ? 1 : 4 : 3 : 2;
            return new vg(renderScript.a((long) cVar.mID, bVar.mID, true, i2), renderScript, cVar, bVar, true, i2);
        } else {
            throw new yg("Bad kind and type combo");
        }
    }

    @DexIgnore
    public boolean a(vg vgVar) {
        c cVar;
        if (equals(vgVar)) {
            return true;
        }
        if (this.d == vgVar.d && (cVar = this.e) != c.NONE && cVar == vgVar.e && this.h == vgVar.h) {
            return true;
        }
        return false;
    }
}
