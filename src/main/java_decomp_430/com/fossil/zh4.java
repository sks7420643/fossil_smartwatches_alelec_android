package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zh4 {
    IMPERIAL("imperial"),
    METRIC("metric");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public zh4(String str) {
        this.value = str;
    }

    @DexIgnore
    public static zh4 fromString(String str) {
        if (str.equalsIgnoreCase("imperial")) {
            return IMPERIAL;
        }
        return METRIC;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
