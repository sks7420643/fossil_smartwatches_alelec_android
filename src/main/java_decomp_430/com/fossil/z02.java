package com.fossil;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z02 implements Parcelable.Creator<DataHolder> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String[] strArr = null;
        CursorWindow[] cursorWindowArr = null;
        Bundle bundle = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                strArr = f22.f(parcel, a);
            } else if (a2 == 2) {
                cursorWindowArr = f22.b(parcel, a, CursorWindow.CREATOR);
            } else if (a2 == 3) {
                i2 = f22.q(parcel, a);
            } else if (a2 == 4) {
                bundle = f22.a(parcel, a);
            } else if (a2 != 1000) {
                f22.v(parcel, a);
            } else {
                i = f22.q(parcel, a);
            }
        }
        f22.h(parcel, b);
        DataHolder dataHolder = new DataHolder(i, strArr, cursorWindowArr, i2, bundle);
        dataHolder.C();
        return dataHolder;
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataHolder[i];
    }
}
