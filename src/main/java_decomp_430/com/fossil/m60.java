package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ long c; // = 4294967295L;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<m60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final m60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new m60(cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public m60 createFromParcel(Parcel parcel) {
            return new m60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new m60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m42createFromParcel(Parcel parcel) {
            return new m60(parcel, (qg6) null);
        }
    }

    /*
    static {
        vg6 vg6 = vg6.a;
    }
    */

    @DexIgnore
    public m60(long j) throws IllegalArgumentException {
        super(s60.DAILY_DISTANCE);
        this.b = j;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        long j = c;
        long j2 = this.b;
        if (!(0 <= j2 && j >= j2)) {
            StringBuilder b2 = ze0.b("centimeter(");
            b2.append(this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            b2.append(c);
            b2.append("].");
            throw new IllegalArgumentException(b2.toString());
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(m60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((m60) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
    }

    @DexIgnore
    public final long getCentimeter() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return Long.valueOf(this.b).hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
    }

    @DexIgnore
    public Long d() {
        return Long.valueOf(this.b);
    }

    @DexIgnore
    public /* synthetic */ m60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readLong();
        e();
    }
}
