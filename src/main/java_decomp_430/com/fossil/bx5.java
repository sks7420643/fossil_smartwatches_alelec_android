package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ bx5 b; // = new bx5();

    /*
    static {
        String simpleName = bx5.class.getSimpleName();
        wg6.a((Object) simpleName, "BackendURLUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return a(PortfolioApp.get.instance(), i);
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i) {
        wg6.b(portfolioApp, "app");
        int k = portfolioApp.w().k();
        boolean z = false;
        if (k == 0 ? !xj6.b("release", "release", true) : k == 1) {
            z = true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        if (i != 5) {
                            return "";
                        }
                        if (z) {
                            return w24.y.c();
                        }
                        return w24.y.b();
                    } else if (z) {
                        return w24.y.g();
                    } else {
                        return w24.y.f();
                    }
                } else if (z) {
                    return w24.y.r();
                } else {
                    return w24.y.q();
                }
            } else if (z) {
                return w24.y.p();
            } else {
                return w24.y.o();
            }
        } else if (z) {
            return w24.y.l();
        } else {
            return w24.y.i();
        }
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i, String str) {
        wg6.b(portfolioApp, "app");
        wg6.b(str, "version");
        int k = portfolioApp.w().k();
        boolean z = false;
        if (k == 0 ? !xj6.b("release", "release", true) : k == 1) {
            z = true;
        }
        if (i != 0) {
            return a(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return w24.y.m();
                }
            } else if (str.equals("1")) {
                return w24.y.l();
            }
            return w24.y.n();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return w24.y.j();
            }
        } else if (str.equals("1")) {
            return w24.y.i();
        }
        return w24.y.k();
    }
}
