package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs6 extends zq6 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ lt6 c;

    @DexIgnore
    public bs6(String str, long j, lt6 lt6) {
        this.a = str;
        this.b = j;
        this.c = lt6;
    }

    @DexIgnore
    public long contentLength() {
        return this.b;
    }

    @DexIgnore
    public uq6 contentType() {
        String str = this.a;
        if (str != null) {
            return uq6.b(str);
        }
        return null;
    }

    @DexIgnore
    public lt6 source() {
        return this.c;
    }
}
