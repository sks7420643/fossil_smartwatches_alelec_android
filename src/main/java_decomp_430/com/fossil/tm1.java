package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm1 extends rf1 {
    @DexIgnore
    public byte[] I; // = new byte[0];
    @DexIgnore
    public /* final */ nq0 J;
    @DexIgnore
    public /* final */ byte[] K;

    @DexIgnore
    public tm1(ue1 ue1, nq0 nq0, byte[] bArr) {
        super(ue1, sv0.SEND_PHONE_RANDOM_NUMBER, lx0.SEND_PHONE_RANDOM_NUMBER, 0, 8);
        this.J = nq0;
        this.K = bArr;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 17) {
            if (this.J.a == bArr[0]) {
                this.I = md6.a(bArr, 1, 17);
                cw0.a(jSONObject, bm0.BOTH_SIDES_RANDOM_NUMBERS, (Object) cw0.a(this.I, (String) null, 1));
                this.v = bn0.a(this.v, (lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27);
            } else {
                this.v = bn0.a(this.v, (lx0) null, (String) null, il0.WRONG_AUTHENTICATION_KEY_TYPE, (ch0) null, (sj0) null, 27);
            }
        } else {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_LENGTH, (ch0) null, (sj0) null, 27);
        }
        this.C = true;
        return jSONObject;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.AUTHENTICATION_KEY_TYPE, (Object) cw0.a((Enum<?>) this.J)), bm0.PHONE_RANDOM_NUMBER, (Object) cw0.a(this.K, (String) null, 1));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.BOTH_SIDES_RANDOM_NUMBERS, (Object) cw0.a(this.I, (String) null, 1));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(this.K.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.J.a).put(this.K).array();
        wg6.a(array, "ByteBuffer.allocate(1 + \u2026\n                .array()");
        return array;
    }
}
