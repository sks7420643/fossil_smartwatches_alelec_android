package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yf1 extends q81 {
    @DexIgnore
    public boolean n;
    @DexIgnore
    public /* final */ hn1<p51> o; // = this.m.a;
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic p;
    @DexIgnore
    public /* final */ boolean q;

    @DexIgnore
    public yf1(BluetoothDevice bluetoothDevice, BluetoothGattCharacteristic bluetoothGattCharacteristic, boolean z, vj0 vj0) {
        super(ma1.NOTIFY_CHARACTERISTIC_CHANGED, bluetoothDevice, vj0);
        this.p = bluetoothGattCharacteristic;
        this.q = z;
    }

    @DexIgnore
    public void a(fu0 fu0) {
        BluetoothDevice bluetoothDevice = this.l;
        BluetoothGattCharacteristic bluetoothGattCharacteristic = this.p;
        boolean z = this.q;
        BluetoothGattServer bluetoothGattServer = fu0.a;
        if (bluetoothGattServer != null ? bluetoothGattServer.notifyCharacteristicChanged(bluetoothDevice, bluetoothGattCharacteristic, z) : false) {
            this.n = true;
            return;
        }
        this.f = b31.a(this.f, (ma1) null, g11.GATT_ERROR, new t31(x11.START_FAIL, 0, 2), 1);
        a();
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return (p51 instanceof jl1) && wg6.a(((jl1) p51).b, this.l);
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.o;
    }

    @DexIgnore
    public boolean d() {
        g11 g11 = this.f.b;
        return (g11 == g11.TIMEOUT || g11 == g11.INTERRUPTED) && this.n;
    }

    @DexIgnore
    public void a(p51 p51) {
        super.a(p51);
        this.n = false;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.DEVICE, (Object) this.l.getAddress()), bm0.CHARACTERISTIC, (Object) this.p.getUuid().toString()), bm0.NEED_CONFIRM, (Object) Boolean.valueOf(this.q));
    }
}
