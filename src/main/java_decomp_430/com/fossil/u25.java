package com.fossil;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u25 implements Factory<ArrayList<wx4>> {
    @DexIgnore
    public static ArrayList<wx4> a(t25 t25) {
        ArrayList<wx4> a = t25.a();
        z76.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
