package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oc4 extends nc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362561, 1);
        B.put(2131362820, 2);
        B.put(2131363218, 3);
        B.put(2131363128, 4);
        B.put(2131362915, 5);
        B.put(2131362361, 6);
        B.put(2131363038, 7);
        B.put(2131362895, 8);
        B.put(2131362453, 9);
        B.put(2131363040, 10);
        B.put(2131362897, 11);
        B.put(2131363213, 12);
        B.put(2131361934, 13);
    }
    */

    @DexIgnore
    public oc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 14, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public oc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[13], objArr[6], objArr[9], objArr[1], objArr[2], objArr[0], objArr[8], objArr[11], objArr[5], objArr[7], objArr[10], objArr[4], objArr[12], objArr[3]);
        this.z = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
