package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dn<T> implements xm<T> {
    @DexIgnore
    public /* final */ List<String> a; // = new ArrayList();
    @DexIgnore
    public T b;
    @DexIgnore
    public mn<T> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(List<String> list);

        @DexIgnore
        void b(List<String> list);
    }

    @DexIgnore
    public dn(mn<T> mnVar) {
        this.c = mnVar;
    }

    @DexIgnore
    public void a(a aVar) {
        if (this.d != aVar) {
            this.d = aVar;
            b();
        }
    }

    @DexIgnore
    public abstract boolean a(zn znVar);

    @DexIgnore
    public final void b() {
        if (!this.a.isEmpty() && this.d != null) {
            T t = this.b;
            if (t == null || b(t)) {
                this.d.b(this.a);
            } else {
                this.d.a(this.a);
            }
        }
    }

    @DexIgnore
    public abstract boolean b(T t);

    @DexIgnore
    public void a(List<zn> list) {
        this.a.clear();
        for (zn next : list) {
            if (a(next)) {
                this.a.add(next.a);
            }
        }
        if (this.a.isEmpty()) {
            this.c.b(this);
        } else {
            this.c.a(this);
        }
        b();
    }

    @DexIgnore
    public void a() {
        if (!this.a.isEmpty()) {
            this.a.clear();
            this.c.b(this);
        }
    }

    @DexIgnore
    public boolean a(String str) {
        T t = this.b;
        return t != null && b(t) && this.a.contains(str);
    }

    @DexIgnore
    public void a(T t) {
        this.b = t;
        b();
    }
}
