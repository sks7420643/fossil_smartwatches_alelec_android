package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ua5 extends j24 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(String str, boolean z);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(int i);

    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE h();

    @DexIgnore
    public abstract int i();

    @DexIgnore
    public abstract void j();
}
