package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to4 implements Factory<so4> {
    @DexIgnore
    public /* final */ Provider<iq4> a;
    @DexIgnore
    public /* final */ Provider<an4> b;

    @DexIgnore
    public to4(Provider<iq4> provider, Provider<an4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static to4 a(Provider<iq4> provider, Provider<an4> provider2) {
        return new to4(provider, provider2);
    }

    @DexIgnore
    public static so4 b(Provider<iq4> provider, Provider<an4> provider2) {
        so4 so4 = new so4();
        uo4.a(so4, provider.get());
        uo4.a(so4, provider2.get());
        return so4;
    }

    @DexIgnore
    public so4 get() {
        return b(this.a, this.b);
    }
}
