package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class pj5$g$b extends sf6 implements ig6<il6, xe6<? super Bitmap>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeProfilePresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj5$g$b(HomeProfilePresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pj5$g$b pj5_g_b = new pj5$g$b(this.this$0, xe6);
        pj5_g_b.p$ = (il6) obj;
        return pj5_g_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pj5$g$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return jj4.a((Context) PortfolioApp.get.instance()).e().a(this.this$0.$imageUri).a(new nz().a(ft.a).a(true).a(new xj4())).c(MFNetworkReturnCode.RESPONSE_OK, MFNetworkReturnCode.RESPONSE_OK).get();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
