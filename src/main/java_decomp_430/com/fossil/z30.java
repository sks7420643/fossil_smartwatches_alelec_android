package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z30 {
    @DexIgnore
    public static /* final */ Map<String, String> g; // = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
    @DexIgnore
    public static /* final */ short[] h; // = {10, 20, 30, 60, 120, 300};
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ d30 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ c d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public Thread f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements d {
        @DexIgnore
        public boolean a() {
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a();
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        File[] a();

        @DexIgnore
        File[] b();

        @DexIgnore
        File[] c();
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        boolean a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends y86 {
        @DexIgnore
        public /* final */ float a;
        @DexIgnore
        public /* final */ d b;

        @DexIgnore
        public e(float f, d dVar) {
            this.a = f;
            this.b = dVar;
        }

        @DexIgnore
        public void a() {
            try {
                b();
            } catch (Exception e) {
                c86.g().e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", e);
            }
            Thread unused = z30.this.f = null;
        }

        @DexIgnore
        public final void b() {
            l86 g = c86.g();
            g.d("CrashlyticsCore", "Starting report processing in " + this.a + " second(s)...");
            float f = this.a;
            if (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    Thread.sleep((long) (f * 1000.0f));
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            List<y30> a2 = z30.this.a();
            if (!z30.this.e.a()) {
                if (a2.isEmpty() || this.b.a()) {
                    int i = 0;
                    while (!a2.isEmpty() && !z30.this.e.a()) {
                        l86 g2 = c86.g();
                        g2.d("CrashlyticsCore", "Attempting to send " + a2.size() + " report(s)");
                        for (y30 a3 : a2) {
                            z30.this.a(a3);
                        }
                        a2 = z30.this.a();
                        if (!a2.isEmpty()) {
                            int i2 = i + 1;
                            long j = (long) z30.h[Math.min(i, z30.h.length - 1)];
                            l86 g3 = c86.g();
                            g3.d("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + j + " seconds");
                            try {
                                Thread.sleep(j * 1000);
                                i = i2;
                            } catch (InterruptedException unused2) {
                                Thread.currentThread().interrupt();
                                return;
                            }
                        }
                    }
                    return;
                }
                l86 g4 = c86.g();
                g4.d("CrashlyticsCore", "User declined to send. Removing " + a2.size() + " Report(s).");
                for (y30 remove : a2) {
                    remove.remove();
                }
            }
        }
    }

    @DexIgnore
    public z30(String str, d30 d30, c cVar, b bVar) {
        if (d30 != null) {
            this.b = d30;
            this.c = str;
            this.d = cVar;
            this.e = bVar;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [java.lang.Runnable, com.fossil.z30$e] */
    public synchronized void a(float f2, d dVar) {
        if (this.f != null) {
            c86.g().d("CrashlyticsCore", "Report upload has already been started.");
            return;
        }
        this.f = new Thread(new e(f2, dVar), "Crashlytics Report Uploader");
        this.f.start();
    }

    @DexIgnore
    public boolean a(y30 y30) {
        boolean z;
        synchronized (this.a) {
            z = false;
            try {
                boolean a2 = this.b.a(new c30(this.c, y30));
                l86 g2 = c86.g();
                StringBuilder sb = new StringBuilder();
                sb.append("Crashlytics report upload ");
                sb.append(a2 ? "complete: " : "FAILED: ");
                sb.append(y30.b());
                g2.i("CrashlyticsCore", sb.toString());
                if (a2) {
                    y30.remove();
                    z = true;
                }
            } catch (Exception e2) {
                l86 g3 = c86.g();
                g3.e("CrashlyticsCore", "Error occurred sending report " + y30, e2);
            }
        }
        return z;
    }

    @DexIgnore
    public List<y30> a() {
        File[] c2;
        File[] b2;
        File[] a2;
        c86.g().d("CrashlyticsCore", "Checking for crash reports...");
        synchronized (this.a) {
            c2 = this.d.c();
            b2 = this.d.b();
            a2 = this.d.a();
        }
        LinkedList linkedList = new LinkedList();
        if (c2 != null) {
            for (File file : c2) {
                c86.g().d("CrashlyticsCore", "Found crash report " + file.getPath());
                linkedList.add(new c40(file));
            }
        }
        HashMap hashMap = new HashMap();
        if (b2 != null) {
            for (File file2 : b2) {
                String c3 = u20.c(file2);
                if (!hashMap.containsKey(c3)) {
                    hashMap.put(c3, new LinkedList());
                }
                ((List) hashMap.get(c3)).add(file2);
            }
        }
        for (String str : hashMap.keySet()) {
            c86.g().d("CrashlyticsCore", "Found invalid session: " + str);
            List list = (List) hashMap.get(str);
            linkedList.add(new j30(str, (File[]) list.toArray(new File[list.size()])));
        }
        if (a2 != null) {
            for (File r30 : a2) {
                linkedList.add(new r30(r30));
            }
        }
        if (linkedList.isEmpty()) {
            c86.g().d("CrashlyticsCore", "No reports found.");
        }
        return linkedList;
    }
}
