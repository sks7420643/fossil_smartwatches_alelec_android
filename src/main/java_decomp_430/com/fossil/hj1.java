package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj1 extends rf1 {
    @DexIgnore
    public byte[] I; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] J;

    @DexIgnore
    public hj1(ue1 ue1, byte[] bArr) {
        super(ue1, sv0.EXCHANGE_PUBLIC_KEYS, lx0.EXCHANGE_PUBLIC_KEYS, 0, 8);
        this.J = bArr;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 32) {
            this.I = md6.a(bArr, 0, 32);
            cw0.a(jSONObject, bm0.DEVICE_PUBLIC_KEY, (Object) cw0.a(this.I, (String) null, 1));
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27);
        } else {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_LENGTH, (ch0) null, (sj0) null, 27);
        }
        this.C = true;
        return jSONObject;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.PHONE_PUBLIC_KEY, (Object) cw0.a(this.J, (String) null, 1));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.DEVICE_PUBLIC_KEY, (Object) cw0.a(this.I, (String) null, 1));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(this.J.length).order(ByteOrder.LITTLE_ENDIAN).put(this.J).array();
        wg6.a(array, "ByteBuffer.allocate(phon\u2026\n                .array()");
        return array;
    }
}
