package com.fossil;

import com.fossil.lo6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ip6 extends ko6<mp6> {
    @DexIgnore
    public ip6() {
        super(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008f, code lost:
        r7 = r9;
     */
    @DexIgnore
    public final mp6 a(pp6 pp6) {
        Object obj;
        wg6.b(pp6, "mode");
        while (true) {
            lo6 lo6 = (lo6) this._cur$internal;
            while (true) {
                long j = lo6._state$internal;
                obj = null;
                if ((1152921504606846976L & j) == 0) {
                    lo6.a aVar = lo6.h;
                    boolean z = false;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((lo6.a & ((int) ((1152921503533105152L & j) >> 30))) == (lo6.a & i)) {
                        break;
                    }
                    Object obj2 = lo6.b.get(lo6.a & i);
                    if (obj2 != null) {
                        if (obj2 instanceof lo6.b) {
                            break;
                        }
                        if (((mp6) obj2).a() == pp6) {
                            z = true;
                        }
                        if (z) {
                            int i2 = (i + 1) & 1073741823;
                            if (!lo6.f.compareAndSet(lo6, j, lo6.h.a(j, i2))) {
                                if (lo6.d) {
                                    lo6 lo62 = lo6;
                                    do {
                                        lo62 = lo62.a(i, i2);
                                    } while (lo62 != null);
                                    break;
                                }
                            } else {
                                lo6.b.set(lo6.a & i, (Object) null);
                                break;
                            }
                        } else {
                            break;
                        }
                    } else if (lo6.d) {
                        break;
                    }
                } else {
                    obj = lo6.g;
                    break;
                }
            }
            if (obj != lo6.g) {
                return (mp6) obj;
            }
            ko6.a.compareAndSet(this, lo6, lo6.e());
        }
    }
}
