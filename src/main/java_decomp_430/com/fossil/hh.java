package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hh<T> extends vh {
    @DexIgnore
    public hh(oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public abstract void bind(mi miVar, T t);

    @DexIgnore
    public final void insert(T t) {
        mi acquire = acquire();
        try {
            bind(acquire, t);
            acquire.t();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long insertAndReturnId(T t) {
        mi acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.t();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(Collection<? extends T> collection) {
        mi acquire = acquire();
        try {
            long[] jArr = new long[collection.size()];
            int i = 0;
            for (Object bind : collection) {
                bind(acquire, bind);
                jArr[i] = acquire.t();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final Long[] insertAndReturnIdsArrayBox(Collection<? extends T> collection) {
        mi acquire = acquire();
        try {
            Long[] lArr = new Long[collection.size()];
            int i = 0;
            for (Object bind : collection) {
                bind(acquire, bind);
                lArr[i] = Long.valueOf(acquire.t());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final List<Long> insertAndReturnIdsList(T[] tArr) {
        mi acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(tArr.length);
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                arrayList.add(i, Long.valueOf(acquire.t()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(T[] tArr) {
        mi acquire = acquire();
        try {
            for (T bind : tArr) {
                bind(acquire, bind);
                acquire.t();
            }
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(T[] tArr) {
        mi acquire = acquire();
        try {
            long[] jArr = new long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                jArr[i] = acquire.t();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final Long[] insertAndReturnIdsArrayBox(T[] tArr) {
        mi acquire = acquire();
        try {
            Long[] lArr = new Long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                lArr[i] = Long.valueOf(acquire.t());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final List<Long> insertAndReturnIdsList(Collection<? extends T> collection) {
        mi acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(collection.size());
            int i = 0;
            for (Object bind : collection) {
                bind(acquire, bind);
                arrayList.add(i, Long.valueOf(acquire.t()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(Iterable<? extends T> iterable) {
        mi acquire = acquire();
        try {
            for (Object bind : iterable) {
                bind(acquire, bind);
                acquire.t();
            }
        } finally {
            release(acquire);
        }
    }
}
