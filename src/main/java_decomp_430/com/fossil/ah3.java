package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah3 {
    @DexIgnore
    public /* final */ Rect a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ hj3 f;

    @DexIgnore
    public ah3(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i, hj3 hj3, Rect rect) {
        y8.a(rect.left);
        y8.a(rect.top);
        y8.a(rect.right);
        y8.a(rect.bottom);
        this.a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.e = i;
        this.f = hj3;
    }

    @DexIgnore
    public static ah3 a(Context context, int i) {
        y8.a(i != 0, (Object) "Cannot create a CalendarItemStyle with a styleResId of 0");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, xf3.MaterialCalendarItem);
        Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(xf3.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(xf3.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(xf3.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(xf3.MaterialCalendarItem_android_insetBottom, 0));
        ColorStateList a2 = pi3.a(context, obtainStyledAttributes, xf3.MaterialCalendarItem_itemFillColor);
        ColorStateList a3 = pi3.a(context, obtainStyledAttributes, xf3.MaterialCalendarItem_itemTextColor);
        ColorStateList a4 = pi3.a(context, obtainStyledAttributes, xf3.MaterialCalendarItem_itemStrokeColor);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(xf3.MaterialCalendarItem_itemStrokeWidth, 0);
        hj3 a5 = hj3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(xf3.MaterialCalendarItem_itemShapeAppearanceOverlay, 0)).a();
        obtainStyledAttributes.recycle();
        return new ah3(a2, a3, a4, dimensionPixelSize, a5, rect);
    }

    @DexIgnore
    public int b() {
        return this.a.top;
    }

    @DexIgnore
    public void a(TextView textView) {
        dj3 dj3 = new dj3();
        dj3 dj32 = new dj3();
        dj3.setShapeAppearanceModel(this.f);
        dj32.setShapeAppearanceModel(this.f);
        dj3.a(this.c);
        dj3.a((float) this.e, this.d);
        textView.setTextColor(this.b);
        Drawable rippleDrawable = Build.VERSION.SDK_INT >= 21 ? new RippleDrawable(this.b.withAlpha(30), dj3, dj32) : dj3;
        Rect rect = this.a;
        x9.a((View) textView, (Drawable) new InsetDrawable(rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }

    @DexIgnore
    public int a() {
        return this.a.bottom;
    }
}
