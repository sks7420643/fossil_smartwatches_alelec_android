package com.fossil;

import android.media.session.MediaController;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$j$b extends MusicControlComponent.d {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.j g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$j$b(MusicControlComponent.j jVar, MediaController mediaController, MediaController mediaController2, String str) {
        super(mediaController2, str);
        this.g = jVar;
    }

    @DexIgnore
    public void a(MusicControlComponent.c cVar, MusicControlComponent.c cVar2) {
        wg6.b(cVar, "oldMetadata");
        wg6.b(cVar2, "newMetadata");
        super.a(cVar, cVar2);
        this.g.a.a(b());
    }

    @DexIgnore
    public void a(int i, int i2, MusicControlComponent.b bVar) {
        wg6.b(bVar, "controller");
        super.a(i, i2, bVar);
        if (i2 == 3) {
            this.g.a.a(bVar);
        } else if (this.g.a.e() == null) {
            this.g.a.a(bVar);
        }
        this.g.a.a(i2);
    }

    @DexIgnore
    public void a(MusicControlComponent.b bVar) {
        wg6.b(bVar, "controller");
        super.a(bVar);
        this.g.a.f().b(bVar);
        if (wg6.a((Object) bVar, (Object) this.g.a.e())) {
            this.g.a.a((MusicControlComponent.b) null);
        }
    }
}
