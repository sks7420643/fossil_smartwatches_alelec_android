package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp3<T> {
    @DexIgnore
    public /* final */ Set<Class<? super T>> a;
    @DexIgnore
    public /* final */ Set<gq3> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ zp3<T> e;
    @DexIgnore
    public /* final */ Set<Class<?>> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> {
        @DexIgnore
        public /* final */ Set<Class<? super T>> a;
        @DexIgnore
        public /* final */ Set<gq3> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public zp3<T> e;
        @DexIgnore
        public Set<Class<?>> f;

        @DexIgnore
        public wp3<T> b() {
            w12.b(this.e != null, "Missing required property: factory.");
            return new wp3(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, this.f);
        }

        @DexIgnore
        public b<T> c() {
            a(2);
            return this;
        }

        @DexIgnore
        public final b<T> d() {
            this.d = 1;
            return this;
        }

        @DexIgnore
        @SafeVarargs
        public b(Class<T> cls, Class<? super T>... clsArr) {
            this.a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.d = 0;
            this.f = new HashSet();
            w12.a(cls, (Object) "Null interface");
            this.a.add(cls);
            for (Class<? super T> a2 : clsArr) {
                w12.a(a2, (Object) "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        @DexIgnore
        public b<T> a(gq3 gq3) {
            w12.a(gq3, (Object) "Null dependency");
            a(gq3.a());
            this.b.add(gq3);
            return this;
        }

        @DexIgnore
        public b<T> a() {
            a(1);
            return this;
        }

        @DexIgnore
        public final b<T> a(int i) {
            w12.b(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public final void a(Class<?> cls) {
            w12.a(!this.a.contains(cls), (Object) "Components are not allowed to depend on interfaces they themselves provide.");
        }

        @DexIgnore
        public b<T> a(zp3<T> zp3) {
            w12.a(zp3, (Object) "Null factory");
            this.e = zp3;
            return this;
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(Object obj, xp3 xp3) {
        return obj;
    }

    @DexIgnore
    public static /* synthetic */ Object b(Object obj, xp3 xp3) {
        return obj;
    }

    @DexIgnore
    public Set<gq3> a() {
        return this.b;
    }

    @DexIgnore
    public zp3<T> b() {
        return this.e;
    }

    @DexIgnore
    public Set<Class<? super T>> c() {
        return this.a;
    }

    @DexIgnore
    public Set<Class<?>> d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.c == 1;
    }

    @DexIgnore
    public boolean f() {
        return this.c == 2;
    }

    @DexIgnore
    public boolean g() {
        return this.d == 0;
    }

    @DexIgnore
    public String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }

    @DexIgnore
    public wp3(Set<Class<? super T>> set, Set<gq3> set2, int i, int i2, zp3<T> zp3, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = zp3;
        this.f = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0]);
    }

    @DexIgnore
    public static <T> b<T> b(Class<T> cls) {
        b<T> a2 = a(cls);
        b unused = a2.d();
        return a2;
    }

    @DexIgnore
    @SafeVarargs
    public static <T> b<T> a(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> wp3<T> a(T t, Class<T> cls, Class<? super T>... clsArr) {
        b<T> a2 = a(cls, clsArr);
        a2.a((zp3<T>) up3.a((Object) t));
        return a2.b();
    }

    @DexIgnore
    public static <T> wp3<T> a(T t, Class<T> cls) {
        b<T> b2 = b(cls);
        b2.a((zp3<T>) vp3.a((Object) t));
        return b2.b();
    }
}
