package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp0 implements Parcelable.Creator<lr0> {
    @DexIgnore
    public /* synthetic */ tp0(qg6 qg6) {
    }

    @DexIgnore
    public final lr0 a(byte[] bArr) throws IllegalArgumentException {
        if (bArr.length == 3) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            bo0 a = bo0.e.a(order.get(0));
            if (a != null) {
                return new lr0(a, order.getShort(1));
            }
            StringBuilder b = ze0.b("Invalid action: ");
            b.append(order.get(0));
            b.append('.');
            throw new IllegalArgumentException(b.toString());
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data length ("), bArr.length, "), ", "require 3."));
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            return new lr0(bo0.valueOf(readString), (short) parcel.readInt());
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new lr0[i];
    }
}
