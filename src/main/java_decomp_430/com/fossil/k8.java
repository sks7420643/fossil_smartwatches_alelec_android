package com.fossil;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k8 {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public HandlerThread b;
    @DexIgnore
    public Handler c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Handler.Callback e; // = new a();
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Handler.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                k8.this.a();
                return true;
            } else if (i != 1) {
                return true;
            } else {
                k8.this.a((Runnable) message.obj);
                return true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Callable a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ d c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Object a;

            @DexIgnore
            public a(Object obj) {
                this.a = obj;
            }

            @DexIgnore
            public void run() {
                b.this.c.a(this.a);
            }
        }

        @DexIgnore
        public b(k8 k8Var, Callable callable, Handler handler, d dVar) {
            this.a = callable;
            this.b = handler;
            this.c = dVar;
        }

        @DexIgnore
        public void run() {
            Object obj;
            try {
                obj = this.a.call();
            } catch (Exception unused) {
                obj = null;
            }
            this.b.post(new a(obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicReference a;
        @DexIgnore
        public /* final */ /* synthetic */ Callable b;
        @DexIgnore
        public /* final */ /* synthetic */ ReentrantLock c;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicBoolean d;
        @DexIgnore
        public /* final */ /* synthetic */ Condition e;

        @DexIgnore
        public c(k8 k8Var, AtomicReference atomicReference, Callable callable, ReentrantLock reentrantLock, AtomicBoolean atomicBoolean, Condition condition) {
            this.a = atomicReference;
            this.b = callable;
            this.c = reentrantLock;
            this.d = atomicBoolean;
            this.e = condition;
        }

        @DexIgnore
        public void run() {
            try {
                this.a.set(this.b.call());
            } catch (Exception unused) {
            }
            this.c.lock();
            try {
                this.d.set(false);
                this.e.signal();
            } finally {
                this.c.unlock();
            }
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public k8(String str, int i, int i2) {
        this.h = str;
        this.g = i;
        this.f = i2;
        this.d = 0;
    }

    @DexIgnore
    public <T> void a(Callable<T> callable, d<T> dVar) {
        b(new b(this, callable, new Handler(), dVar));
    }

    @DexIgnore
    public final void b(Runnable runnable) {
        synchronized (this.a) {
            if (this.b == null) {
                this.b = new HandlerThread(this.h, this.g);
                this.b.start();
                this.c = new Handler(this.b.getLooper(), this.e);
                this.d++;
            }
            this.c.removeMessages(0);
            this.c.sendMessage(this.c.obtainMessage(1, runnable));
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|11|12|(4:25|14|15|16)(1:17)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003f */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    public <T> T a(Callable<T> callable, int i) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition newCondition = reentrantLock.newCondition();
        AtomicReference atomicReference = new AtomicReference();
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        b(new c(this, atomicReference, callable, reentrantLock, atomicBoolean, newCondition));
        reentrantLock.lock();
        try {
            if (!atomicBoolean.get()) {
                return atomicReference.get();
            }
            long nanos = TimeUnit.MILLISECONDS.toNanos((long) i);
            do {
                nanos = newCondition.awaitNanos(nanos);
                if (atomicBoolean.get()) {
                    T t = atomicReference.get();
                    reentrantLock.unlock();
                    return t;
                }
            } while (nanos > 0);
            throw new InterruptedException("timeout");
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    public void a(Runnable runnable) {
        runnable.run();
        synchronized (this.a) {
            this.c.removeMessages(0);
            this.c.sendMessageDelayed(this.c.obtainMessage(0), (long) this.f);
        }
    }

    @DexIgnore
    public void a() {
        synchronized (this.a) {
            if (!this.c.hasMessages(1)) {
                this.b.quit();
                this.b = null;
                this.c = null;
            }
        }
    }
}
