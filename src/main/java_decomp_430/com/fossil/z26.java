package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z26 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public b e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static Bundle a(z26 z26) {
            Bundle bundle = new Bundle();
            bundle.putInt("_wxobject_sdkVer", z26.a);
            bundle.putString("_wxobject_title", z26.b);
            bundle.putString("_wxobject_description", z26.c);
            bundle.putByteArray("_wxobject_thumbdata", z26.d);
            b bVar = z26.e;
            if (bVar != null) {
                bundle.putString("_wxobject_identifier_", a(bVar.getClass().getName()));
                z26.e.b(bundle);
            }
            bundle.putString("_wxobject_mediatagname", z26.f);
            bundle.putString("_wxobject_message_action", z26.g);
            bundle.putString("_wxobject_message_ext", z26.h);
            return bundle;
        }

        @DexIgnore
        public static z26 a(Bundle bundle) {
            z26 z26 = new z26();
            z26.a = bundle.getInt("_wxobject_sdkVer");
            z26.b = bundle.getString("_wxobject_title");
            z26.c = bundle.getString("_wxobject_description");
            z26.d = bundle.getByteArray("_wxobject_thumbdata");
            z26.f = bundle.getString("_wxobject_mediatagname");
            z26.g = bundle.getString("_wxobject_message_action");
            z26.h = bundle.getString("_wxobject_message_ext");
            String b = b(bundle.getString("_wxobject_identifier_"));
            if (b != null && b.length() > 0) {
                try {
                    z26.e = (b) Class.forName(b).newInstance();
                    z26.e.a(bundle);
                    return z26;
                } catch (Exception e) {
                    e.printStackTrace();
                    h26.a("MicroMsg.SDK.WXMediaMessage", "get media object from bundle failed: unknown ident " + b + ", ex = " + e.getMessage());
                }
            }
            return z26;
        }

        @DexIgnore
        public static String a(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
            }
            h26.a("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
            return str;
        }

        @DexIgnore
        public static String b(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
            }
            h26.a("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
            return str;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        int a();

        @DexIgnore
        void a(Bundle bundle);

        @DexIgnore
        void b(Bundle bundle);

        @DexIgnore
        boolean b();
    }

    @DexIgnore
    public z26() {
        this((b) null);
    }

    @DexIgnore
    public z26(b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public final boolean a() {
        String str;
        byte[] bArr;
        if (b() == 8 && ((bArr = this.d) == null || bArr.length == 0)) {
            str = "checkArgs fail, thumbData should not be null when send emoji";
        } else {
            byte[] bArr2 = this.d;
            if (bArr2 == null || bArr2.length <= 32768) {
                String str2 = this.b;
                if (str2 == null || str2.length() <= 512) {
                    String str3 = this.c;
                    if (str3 != null && str3.length() > 1024) {
                        str = "checkArgs fail, description is invalid";
                    } else if (this.e == null) {
                        str = "checkArgs fail, mediaObject is null";
                    } else {
                        String str4 = this.f;
                        if (str4 == null || str4.length() <= 64) {
                            String str5 = this.g;
                            if (str5 == null || str5.length() <= 2048) {
                                String str6 = this.h;
                                if (str6 == null || str6.length() <= 2048) {
                                    return this.e.b();
                                }
                                str = "checkArgs fail, messageExt is too long";
                            } else {
                                str = "checkArgs fail, messageAction is too long";
                            }
                        } else {
                            str = "checkArgs fail, mediaTagName is too long";
                        }
                    }
                } else {
                    str = "checkArgs fail, title is invalid";
                }
            } else {
                str = "checkArgs fail, thumbData is invalid";
            }
        }
        h26.a("MicroMsg.SDK.WXMediaMessage", str);
        return false;
    }

    @DexIgnore
    public final int b() {
        b bVar = this.e;
        if (bVar == null) {
            return 0;
        }
        return bVar.a();
    }
}
