package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.usecase.GetWeather;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp4 implements MembersInjector<ComplicationWeatherService> {
    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, GetWeather getWeather) {
        complicationWeatherService.h = getWeather;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, z24 z24) {
        complicationWeatherService.i = z24;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, an4 an4) {
        complicationWeatherService.j = an4;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, PortfolioApp portfolioApp) {
        complicationWeatherService.o = portfolioApp;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, CustomizeRealDataRepository customizeRealDataRepository) {
        complicationWeatherService.p = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, UserRepository userRepository) {
        complicationWeatherService.q = userRepository;
    }
}
