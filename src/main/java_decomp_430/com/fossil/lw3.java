package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lw3 {
    @DexIgnore
    public abstract lw3 a(iw3 iw3) throws IOException;

    @DexIgnore
    public String toString() {
        return mw3.a(this);
    }

    @DexIgnore
    public lw3 clone() throws CloneNotSupportedException {
        return (lw3) super.clone();
    }
}
