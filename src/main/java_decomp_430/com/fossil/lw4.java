package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.util.Log;
import android.util.Pair;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw4 {
    @DexIgnore
    public static /* final */ Rect a; // = new Rect();
    @DexIgnore
    public static /* final */ RectF b; // = new RectF();
    @DexIgnore
    public static /* final */ RectF c; // = new RectF();
    @DexIgnore
    public static /* final */ float[] d; // = new float[6];
    @DexIgnore
    public static /* final */ float[] e; // = new float[6];
    @DexIgnore
    public static int f;
    @DexIgnore
    public static Pair<String, WeakReference<Bitmap>> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore
    public static b a(Bitmap bitmap, Context context, Uri uri) {
        int i = 0;
        try {
            if (!(uri.getPath() == null || uri.getScheme() == null)) {
                if (uri.getScheme().equals("file")) {
                    i = new pb(uri.getPath()).b();
                } else {
                    Cursor query = context.getContentResolver().query(uri, new String[]{"orientation"}, (String) null, (String[]) null, (String) null);
                    if (query != null) {
                        if (query.moveToFirst()) {
                            i = query.getInt(0);
                        }
                        query.close();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return new b(bitmap, i);
    }

    @DexIgnore
    public static float b(float[] fArr) {
        return (f(fArr) + e(fArr)) / 2.0f;
    }

    @DexIgnore
    public static float c(float[] fArr) {
        return (a(fArr) + g(fArr)) / 2.0f;
    }

    @DexIgnore
    public static float d(float[] fArr) {
        return a(fArr) - g(fArr);
    }

    @DexIgnore
    public static float e(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }

    @DexIgnore
    public static float f(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }

    @DexIgnore
    public static float g(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static float h(float[] fArr) {
        return f(fArr) - e(fArr);
    }

    @DexIgnore
    public static b a(Bitmap bitmap, pb pbVar) {
        int a2 = pbVar.a("Orientation", 1);
        return new b(bitmap, a2 != 3 ? a2 != 6 ? a2 != 8 ? 0 : 270 : 90 : 180);
    }

    @DexIgnore
    public static a a(Context context, Uri uri, int i, int i2) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            BitmapFactory.Options a2 = a(contentResolver, uri);
            if (a2.outWidth == -1) {
                if (a2.outHeight == -1) {
                    throw new RuntimeException("File is not a picture");
                }
            }
            a2.inSampleSize = Math.max(a(a2.outWidth, a2.outHeight, i, i2), a(a2.outWidth, a2.outHeight));
            return new a(a(contentResolver, uri, a2), a2.inSampleSize);
        } catch (Exception e2) {
            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e2.getMessage(), e2);
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        Bitmap bitmap2;
        try {
            bitmap2 = Bitmap.createScaledBitmap(bitmap, i, i2, false);
            try {
                return a(bitmap);
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return bitmap2;
            }
        } catch (Exception e3) {
            e = e3;
            bitmap2 = null;
            e.printStackTrace();
            return bitmap2;
        }
    }

    @DexIgnore
    public static a a(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
        int i4 = 1;
        do {
            try {
                return new a(a(bitmap, fArr, i, z, i2, i3, 1.0f / ((float) i4), z2, z3), i4);
            } catch (OutOfMemoryError e2) {
                i4 *= 2;
                if (i4 > 8) {
                    throw e2;
                }
            }
        } while (i4 > 8);
        throw e2;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, float f2, boolean z2, boolean z3) {
        Bitmap bitmap2 = bitmap;
        int i4 = i;
        float f3 = f2;
        Rect a2 = a(fArr, bitmap.getWidth(), bitmap.getHeight(), z, i2, i3);
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i4, (float) (bitmap.getWidth() / 2), (float) (bitmap.getHeight() / 2));
        float f4 = z2 ? -f3 : f3;
        if (z3) {
            f3 = -f3;
        }
        matrix.postScale(f4, f3);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, a2.left, a2.top, a2.width(), a2.height(), matrix, true);
        if (createBitmap == bitmap2) {
            createBitmap = bitmap.copy(bitmap.getConfig(), false);
        }
        return i4 % 90 != 0 ? a(createBitmap, fArr, a2, i, z, i2, i3) : createBitmap;
    }

    @DexIgnore
    public static a a(Context context, Uri uri, float[] fArr, int i, int i2, int i3, boolean z, int i4, int i5, int i6, int i7, boolean z2, boolean z3) {
        OutOfMemoryError outOfMemoryError;
        int i8 = 1;
        do {
            try {
                return a(context, uri, fArr, i, i2, i3, z, i4, i5, i6, i7, z2, z3, i8);
            } catch (OutOfMemoryError e2) {
                outOfMemoryError = e2;
                i8 *= 2;
                if (i8 > 16) {
                    throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + outOfMemoryError.getMessage(), outOfMemoryError);
                }
            }
        } while (i8 > 16);
        throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + outOfMemoryError.getMessage(), outOfMemoryError);
    }

    @DexIgnore
    public static float a(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static Rect a(float[] fArr, int i, int i2, boolean z, int i3, int i4) {
        Rect rect = new Rect(Math.round(Math.max(0.0f, e(fArr))), Math.round(Math.max(0.0f, g(fArr))), Math.round(Math.min((float) i, f(fArr))), Math.round(Math.min((float) i2, a(fArr))));
        if (z) {
            a(rect, i3, i4);
        }
        return rect;
    }

    @DexIgnore
    public static void a(Rect rect, int i, int i2) {
        if (i == i2 && rect.width() != rect.height()) {
            if (rect.height() > rect.width()) {
                rect.bottom -= rect.height() - rect.width();
            } else {
                rect.right -= rect.width() - rect.height();
            }
        }
    }

    @DexIgnore
    public static Uri a(Context context, Bitmap bitmap, Uri uri) {
        boolean z = true;
        if (uri == null) {
            try {
                uri = Uri.fromFile(File.createTempFile("aic_state_store_temp", ".jpg", context.getCacheDir()));
            } catch (Exception e2) {
                Log.w("AIC", "Failed to write bitmap to temp file for image-cropper save instance state", e2);
                return null;
            }
        } else if (new File(uri.getPath()).exists()) {
            z = false;
        }
        if (z) {
            a(context, bitmap, uri, Bitmap.CompressFormat.JPEG, 95);
        }
        return uri;
    }

    @DexIgnore
    public static void a(Context context, Bitmap bitmap, Uri uri, Bitmap.CompressFormat compressFormat, int i) throws FileNotFoundException {
        OutputStream outputStream = null;
        try {
            outputStream = context.getContentResolver().openOutputStream(uri);
            bitmap.compress(compressFormat, i, outputStream);
        } finally {
            a((Closeable) outputStream);
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, int i2, CropImageView.j jVar) {
        if (i > 0 && i2 > 0) {
            try {
                if (jVar == CropImageView.j.RESIZE_FIT || jVar == CropImageView.j.RESIZE_INSIDE || jVar == CropImageView.j.RESIZE_EXACT) {
                    Bitmap bitmap2 = null;
                    if (jVar == CropImageView.j.RESIZE_EXACT) {
                        bitmap2 = Bitmap.createScaledBitmap(bitmap, i, i2, false);
                    } else {
                        float width = (float) bitmap.getWidth();
                        float height = (float) bitmap.getHeight();
                        float max = Math.max(width / ((float) i), height / ((float) i2));
                        if (max > 1.0f || jVar == CropImageView.j.RESIZE_FIT) {
                            bitmap2 = Bitmap.createScaledBitmap(bitmap, (int) (width / max), (int) (height / max), false);
                        }
                    }
                    if (bitmap2 != null) {
                        if (bitmap2 != bitmap) {
                            bitmap.recycle();
                        }
                        return bitmap2;
                    }
                }
            } catch (Exception e2) {
                Log.w("AIC", "Failed to resize cropped image, return bitmap before resize", e2);
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0063  */
    public static a a(Context context, Uri uri, float[] fArr, int i, int i2, int i3, boolean z, int i4, int i5, int i6, int i7, boolean z2, boolean z3, int i8) {
        int i9;
        int i10;
        Bitmap bitmap;
        int i11 = i;
        Rect a2 = a(fArr, i2, i3, z, i4, i5);
        if (i6 > 0) {
            i9 = i6;
        } else {
            i9 = a2.width();
        }
        if (i7 > 0) {
            i10 = i7;
        } else {
            i10 = a2.height();
        }
        Bitmap bitmap2 = null;
        int i12 = 1;
        try {
            a a3 = a(context, uri, a2, i9, i10, i8);
            bitmap2 = a3.a;
            i12 = a3.b;
        } catch (Exception unused) {
        }
        if (bitmap2 != null) {
            try {
                bitmap = a(bitmap2, i11, z2, z3);
                try {
                    if (i11 % 90 != 0) {
                        bitmap = a(bitmap, fArr, a2, i, z, i4, i5);
                    }
                    return new a(bitmap, i12);
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    if (bitmap != null) {
                        bitmap.recycle();
                    }
                    throw e;
                }
            } catch (OutOfMemoryError e3) {
                e = e3;
                bitmap = bitmap2;
                if (bitmap != null) {
                }
                throw e;
            }
        } else {
            return a(context, uri, fArr, i, z, i4, i5, i8, a2, i9, i10, z2, z3);
        }
    }

    @DexIgnore
    public static a a(Context context, Uri uri, float[] fArr, int i, boolean z, int i2, int i3, int i4, Rect rect, int i5, int i6, boolean z2, boolean z3) {
        Bitmap a2;
        Uri uri2 = uri;
        float[] fArr2 = fArr;
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            int a3 = a(rect.width(), rect.height(), i5, i6) * i4;
            options.inSampleSize = a3;
            a2 = a(context.getContentResolver(), uri2, options);
            if (a2 != null) {
                float[] fArr3 = new float[fArr2.length];
                System.arraycopy(fArr2, 0, fArr3, 0, fArr2.length);
                for (int i7 = 0; i7 < fArr3.length; i7++) {
                    fArr3[i7] = fArr3[i7] / ((float) options.inSampleSize);
                }
                bitmap = a(a2, fArr3, i, z, i2, i3, 1.0f, z2, z3);
                if (bitmap != a2) {
                    a2.recycle();
                }
            }
            return new a(bitmap, a3);
        } catch (OutOfMemoryError e2) {
            if (bitmap != null) {
                bitmap.recycle();
            }
            throw e2;
        } catch (Exception e3) {
            throw new RuntimeException("Failed to load sampled bitmap: " + uri2 + "\r\n" + e3.getMessage(), e3);
        } catch (Throwable th) {
            if (a2 != null) {
                a2.recycle();
            }
            throw th;
        }
    }

    @DexIgnore
    public static BitmapFactory.Options a(ContentResolver contentResolver, Uri uri) throws FileNotFoundException {
        InputStream inputStream;
        try {
            inputStream = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, a, options);
                options.inJustDecodeBounds = false;
                a((Closeable) inputStream);
                return options;
            } catch (Throwable th) {
                th = th;
                a((Closeable) inputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            a((Closeable) inputStream);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        a((java.io.Closeable) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r4.inSampleSize *= 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        a((java.io.Closeable) null);
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0011 */
    public static Bitmap a(ContentResolver contentResolver, Uri uri, BitmapFactory.Options options) throws FileNotFoundException {
        do {
            InputStream openInputStream = contentResolver.openInputStream(uri);
            Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, a, options);
            a((Closeable) openInputStream);
            return decodeStream;
        } while (options.inSampleSize > 512);
        throw new RuntimeException("Failed to decode image: " + uri);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0033, code lost:
        r7.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        return r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002e, code lost:
        a((java.io.Closeable) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        if (r7 == null) goto L_0x0036;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x003b */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008c  */
    public static a a(Context context, Uri uri, Rect rect, int i, int i2, int i3) {
        BitmapRegionDecoder bitmapRegionDecoder;
        InputStream inputStream = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = i3 * a(rect.width(), rect.height(), i, i2);
            InputStream openInputStream = context.getContentResolver().openInputStream(uri);
            try {
                bitmapRegionDecoder = BitmapRegionDecoder.newInstance(openInputStream, false);
                do {
                    a aVar = new a(bitmapRegionDecoder.decodeRegion(rect, options), options.inSampleSize);
                    try {
                        options.inSampleSize *= 2;
                    } catch (Exception e2) {
                        e = e2;
                        inputStream = openInputStream;
                        try {
                            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e.getMessage(), e);
                        } catch (Throwable th) {
                            th = th;
                            a((Closeable) inputStream);
                            if (bitmapRegionDecoder != null) {
                                bitmapRegionDecoder.recycle();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        inputStream = openInputStream;
                        a((Closeable) inputStream);
                        if (bitmapRegionDecoder != null) {
                        }
                        throw th;
                    }
                } while (options.inSampleSize > 512);
                a((Closeable) openInputStream);
                if (bitmapRegionDecoder != null) {
                    bitmapRegionDecoder.recycle();
                }
                return new a((Bitmap) null, 1);
            } catch (Exception e3) {
                e = e3;
                bitmapRegionDecoder = null;
                inputStream = openInputStream;
                throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e.getMessage(), e);
            } catch (Throwable th3) {
                th = th3;
                bitmapRegionDecoder = null;
                inputStream = openInputStream;
                a((Closeable) inputStream);
                if (bitmapRegionDecoder != null) {
                }
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            bitmapRegionDecoder = null;
            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e.getMessage(), e);
        } catch (Throwable th4) {
            th = th4;
            bitmapRegionDecoder = null;
            a((Closeable) inputStream);
            if (bitmapRegionDecoder != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, float[] fArr, Rect rect, int i, boolean z, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        if (i % 90 == 0) {
            return bitmap;
        }
        double radians = Math.toRadians((double) i);
        int i7 = (i < 90 || (i > 180 && i < 270)) ? rect.left : rect.right;
        int i8 = 0;
        int i9 = 0;
        while (true) {
            if (i9 < fArr.length) {
                if (fArr[i9] >= ((float) (i7 - 1)) && fArr[i9] <= ((float) (i7 + 1))) {
                    int i10 = i9 + 1;
                    i8 = (int) Math.abs(Math.sin(radians) * ((double) (((float) rect.bottom) - fArr[i10])));
                    i5 = (int) Math.abs(Math.cos(radians) * ((double) (fArr[i10] - ((float) rect.top))));
                    i6 = (int) Math.abs(((double) (fArr[i10] - ((float) rect.top))) / Math.sin(radians));
                    i4 = (int) Math.abs(((double) (((float) rect.bottom) - fArr[i10])) / Math.cos(radians));
                    break;
                }
                i9 += 2;
            } else {
                i4 = 0;
                i5 = 0;
                i6 = 0;
                break;
            }
        }
        rect.set(i8, i5, i6 + i8, i4 + i5);
        if (z) {
            a(rect, i2, i3);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top, rect.width(), rect.height());
        if (bitmap != createBitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static int a(int i, int i2, int i3, int i4) {
        int i5 = 1;
        if (i2 > i4 || i > i3) {
            while ((i2 / 2) / i5 > i4 && (i / 2) / i5 > i3) {
                i5 *= 2;
            }
        }
        return i5;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        if (f == 0) {
            f = a();
        }
        int i3 = 1;
        if (f > 0) {
            while (true) {
                int i4 = i2 / i3;
                int i5 = f;
                if (i4 <= i5 && i / i3 <= i5) {
                    break;
                }
                i3 *= 2;
            }
        }
        return i3;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, boolean z, boolean z2) {
        if (i <= 0 && !z && !z2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i);
        float f2 = -1.0f;
        float f3 = z ? -1.0f : 1.0f;
        if (!z2) {
            f2 = 1.0f;
        }
        matrix.postScale(f3, f2);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        if (createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static int a() {
        try {
            EGL10 egl10 = (EGL10) EGLContext.getEGL();
            EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            egl10.eglInitialize(eglGetDisplay, new int[2]);
            int[] iArr = new int[1];
            egl10.eglGetConfigs(eglGetDisplay, (EGLConfig[]) null, 0, iArr);
            EGLConfig[] eGLConfigArr = new EGLConfig[iArr[0]];
            egl10.eglGetConfigs(eglGetDisplay, eGLConfigArr, iArr[0], iArr);
            int[] iArr2 = new int[1];
            int i = 0;
            for (int i2 = 0; i2 < iArr[0]; i2++) {
                egl10.eglGetConfigAttrib(eglGetDisplay, eGLConfigArr[i2], 12332, iArr2);
                if (i < iArr2[0]) {
                    i = iArr2[0];
                }
            }
            egl10.eglTerminate(eglGetDisplay);
            return Math.max(i, 2048);
        } catch (Exception unused) {
            return 2048;
        }
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        Canvas canvas = new Canvas(createBitmap);
        int width = bitmap.getWidth() / 2;
        int height = bitmap.getHeight() / 2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setARGB(255, 0, 0, 0);
        canvas.drawCircle((float) width, (float) height, (float) Math.min(width, height), paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    @DexIgnore
    public static void a(Context context, String str, String str2, Bitmap bitmap) {
        try {
            a(context, bitmap, Uri.fromFile(new File(str.concat(str2))), Bitmap.CompressFormat.PNG, 100);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
