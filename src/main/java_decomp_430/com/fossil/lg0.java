package com.fossil;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg0 {
    @DexIgnore
    public static /* final */ b91 a; // = b91.CTR_NO_PADDING;
    @DexIgnore
    public static /* final */ lg0 b; // = new lg0();

    @DexIgnore
    public final byte[] a(String str, rg1 rg1, e71 e71, byte[] bArr) {
        zm1 a2 = ue0.b.a(str);
        byte[] a3 = a2.a();
        byte[] a4 = a2.a(rg1).a();
        if (a3 != null) {
            boolean z = false;
            if (true == (!(a3.length == 0))) {
                if (a4.length == 0) {
                    z = true;
                }
                if (!z) {
                    b91 b91 = a;
                    SecretKeySpec secretKeySpec = new SecretKeySpec(a3, "AES");
                    Cipher instance = Cipher.getInstance(b91.a);
                    instance.init(e71.a, secretKeySpec, new IvParameterSpec(a4));
                    byte[] doFinal = instance.doFinal(bArr);
                    wg6.a(doFinal, "cipher.doFinal(data)");
                    int ceil = (int) ((float) Math.ceil((double) (((float) bArr.length) / ((float) 16))));
                    ci0 ci0 = a2.d.get(rg1);
                    if (ci0 == null) {
                        return doFinal;
                    }
                    ci0.e += ceil;
                    return doFinal;
                }
            }
        }
        return bArr;
    }

    @DexIgnore
    public final byte[] b(String str, rg1 rg1, byte[] bArr) {
        return a(str, rg1, e71.ENCRYPT, bArr);
    }

    @DexIgnore
    public final byte[] a(String str, rg1 rg1, byte[] bArr) {
        return a(str, rg1, e71.DECRYPT, bArr);
    }
}
