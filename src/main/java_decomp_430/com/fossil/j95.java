package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j95 extends j24 {
    @DexIgnore
    public abstract void a(HybridCustomizeViewModel hybridCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(String str, String str2);

    @DexIgnore
    public abstract void h();
}
