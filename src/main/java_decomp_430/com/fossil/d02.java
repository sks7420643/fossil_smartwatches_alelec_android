package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d02 implements xy1 {
    @DexIgnore
    public /* final */ /* synthetic */ b02 a;

    @DexIgnore
    public d02(b02 b02) {
        this.a = b02;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        this.a.q.lock();
        try {
            this.a.a(bundle);
            gv1 unused = this.a.j = gv1.e;
            this.a.h();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ d02(b02 b02, e02 e02) {
        this(b02);
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        this.a.q.lock();
        try {
            gv1 unused = this.a.j = gv1;
            this.a.h();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a.q.lock();
        try {
            if (!this.a.p && this.a.o != null) {
                if (this.a.o.E()) {
                    boolean unused = this.a.p = true;
                    this.a.e.g(i);
                    this.a.q.unlock();
                    return;
                }
            }
            boolean unused2 = this.a.p = false;
            this.a.a(i, z);
        } finally {
            this.a.q.unlock();
        }
    }
}
