package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class by3 extends gy3 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public by3(gy3 gy3, int i, int i2) {
        super(gy3);
        this.c = (short) i;
        this.d = (short) i2;
    }

    @DexIgnore
    public void a(hy3 hy3, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    hy3.a(31, 5);
                    short s2 = this.d;
                    if (s2 > 62) {
                        hy3.a(s2 - 31, 16);
                    } else if (i == 0) {
                        hy3.a(Math.min(s2, 31), 5);
                    } else {
                        hy3.a(s2 - 31, 5);
                    }
                }
                hy3.a(bArr[this.c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("<");
        sb.append(this.c);
        sb.append("::");
        sb.append((this.c + this.d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
