package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gs3 implements pc3 {
    @DexIgnore
    public /* final */ FirebaseInstanceId a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public gs3(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    @DexIgnore
    public final qc3 then(Object obj) {
        return this.a.a(this.b, this.c, this.d, (String) obj);
    }
}
