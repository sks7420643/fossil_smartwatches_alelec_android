package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class af4 extends ze4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131361905, 1);
        A.put(2131362442, 2);
        A.put(2131361902, 3);
        A.put(2131362093, 4);
        A.put(2131362653, 5);
        A.put(2131362386, 6);
        A.put(2131363074, 7);
        A.put(2131362829, 8);
    }
    */

    @DexIgnore
    public af4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[3], objArr[1], objArr[4], objArr[6], objArr[2], objArr[5], objArr[8], objArr[0], objArr[7]);
        this.y = -1;
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
