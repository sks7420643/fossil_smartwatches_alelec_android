package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sc4 extends rc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362057, 1);
        B.put(2131362207, 2);
        B.put(2131362442, 3);
        B.put(2131362511, 4);
        B.put(2131363326, 5);
        B.put(2131362441, 6);
        B.put(2131362008, 7);
        B.put(2131362590, 8);
        B.put(2131362228, 9);
        B.put(2131362345, 10);
        B.put(2131362363, 11);
        B.put(2131362444, 12);
    }
    */

    @DexIgnore
    public sc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 13, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public sc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[7], objArr[1], objArr[2], objArr[9], objArr[10], objArr[11], objArr[6], objArr[3], objArr[12], objArr[4], objArr[8], objArr[0], objArr[5]);
        this.z = -1;
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
