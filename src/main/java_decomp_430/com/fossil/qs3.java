package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public qs3(String str, long j) {
        w12.a(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof qs3)) {
            return false;
        }
        qs3 qs3 = (qs3) obj;
        if (this.b != qs3.b || !this.a.equals(qs3.a)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(this.a, Long.valueOf(this.b));
    }
}
