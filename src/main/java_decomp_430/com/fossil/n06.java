package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n06 {
    @DexIgnore
    public static final n06 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n06 {
        @DexIgnore
        public Map<Class<?>, Set<l06>> a(Object obj) {
            return i06.b(obj);
        }

        @DexIgnore
        public Map<Class<?>, m06> b(Object obj) {
            return i06.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<l06>> a(Object obj);

    @DexIgnore
    Map<Class<?>, m06> b(Object obj);
}
