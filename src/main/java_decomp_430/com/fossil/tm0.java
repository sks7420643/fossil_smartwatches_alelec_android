package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm0 extends wm0 {
    @DexIgnore
    public /* final */ ArrayList<hl1> D; // = cw0.a(this.B, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.ASYNC}));
    @DexIgnore
    public /* final */ x70 E;

    @DexIgnore
    public tm0(ue1 ue1, q41 q41, x70 x70) {
        super(ue1, q41, eh1.NOTIFY_APP_NOTIFICATION_EVENT, new g81(x70, ue1));
        this.E = x70;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.D;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.APP_NOTIFICATION_EVENT, (Object) this.E.a());
    }
}
