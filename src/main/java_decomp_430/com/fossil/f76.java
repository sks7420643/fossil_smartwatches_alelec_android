package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f76 extends Application implements l76, o76, p76, m76, n76 {
    @DexIgnore
    public k76<Activity> a;
    @DexIgnore
    public k76<BroadcastReceiver> b;
    @DexIgnore
    public k76<Service> c;
    @DexIgnore
    public k76<ContentProvider> d;
    @DexIgnore
    public volatile boolean e; // = true;

    @DexIgnore
    public d76<ContentProvider> c() {
        f();
        return this.d;
    }

    @DexIgnore
    public abstract d76<? extends f76> e();

    @DexIgnore
    public final void f() {
        if (this.e) {
            synchronized (this) {
                if (this.e) {
                    e().a(this);
                    if (this.e) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        f();
    }

    @DexIgnore
    public k76<BroadcastReceiver> a() {
        return this.b;
    }

    @DexIgnore
    public k76<Service> b() {
        return this.c;
    }

    @DexIgnore
    public k76<Activity> d() {
        return this.a;
    }
}
