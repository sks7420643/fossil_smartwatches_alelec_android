package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.cf;
import com.fossil.xe;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze<Key, Value> {
    @DexIgnore
    public Key a;
    @DexIgnore
    public cf.h b;
    @DexIgnore
    public xe.b<Key, Value> c;
    @DexIgnore
    public cf.e d;
    @DexIgnore
    public Executor e; // = q3.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends uc<cf<Value>> {
        @DexIgnore
        public cf<Value> g;
        @DexIgnore
        public xe<Key, Value> h;
        @DexIgnore
        public /* final */ xe.c i; // = new C0059a();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ xe.b k;
        @DexIgnore
        public /* final */ /* synthetic */ cf.h l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ cf.e o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ze$a$a")
        /* renamed from: com.fossil.ze$a$a  reason: collision with other inner class name */
        public class C0059a implements xe.c {
            @DexIgnore
            public C0059a() {
            }

            @DexIgnore
            public void a() {
                a.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Executor executor, Object obj, xe.b bVar, cf.h hVar, Executor executor2, Executor executor3, cf.e eVar) {
            super(executor);
            this.j = obj;
            this.k = bVar;
            this.l = hVar;
            this.m = executor2;
            this.n = executor3;
            this.o = eVar;
        }

        @DexIgnore
        public cf<Value> a() {
            Object obj = this.j;
            cf<Value> cfVar = this.g;
            if (cfVar != null) {
                obj = cfVar.e();
            }
            do {
                xe<Key, Value> xeVar = this.h;
                if (xeVar != null) {
                    xeVar.removeInvalidatedCallback(this.i);
                }
                this.h = this.k.create();
                this.h.addInvalidatedCallback(this.i);
                cf.f fVar = new cf.f(this.h, this.l);
                fVar.b(this.m);
                fVar.a(this.n);
                fVar.a(this.o);
                fVar.a(obj);
                this.g = fVar.a();
            } while (this.g.h());
            return this.g;
        }
    }

    @DexIgnore
    public ze(xe.b<Key, Value> bVar, cf.h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.c = bVar;
            this.b = hVar;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    public LiveData<cf<Value>> a() {
        return a(this.a, this.b, this.d, this.c, q3.d(), this.e);
    }

    @DexIgnore
    public static <Key, Value> LiveData<cf<Value>> a(Key key, cf.h hVar, cf.e eVar, xe.b<Key, Value> bVar, Executor executor, Executor executor2) {
        return new a(executor2, key, bVar, hVar, executor, executor2, eVar).b();
    }
}
