package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lk6<T> extends xe6<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ Object a(lk6 lk6, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return lk6.a(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    @DexIgnore
    Object a(T t, Object obj);

    @DexIgnore
    void a(dl6 dl6, T t);

    @DexIgnore
    void b(hg6<? super Throwable, cd6> hg6);

    @DexIgnore
    void b(Object obj);

    @DexIgnore
    boolean isActive();
}
