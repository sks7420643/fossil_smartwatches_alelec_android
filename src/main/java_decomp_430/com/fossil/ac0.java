package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ac0 extends p40 {
    @DexIgnore
    public /* final */ bc0 a;

    @DexIgnore
    public ac0(bc0 bc0) {
        this.a = bc0;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.ERROR_CODE, (Object) getErrorCode().getLogName());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public bc0 getErrorCode() {
        return this.a;
    }
}
