package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.tj4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o25 extends y24<y24.b, b, y24.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ Context d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.c {
        @DexIgnore
        public /* final */ List<vx4> a;

        @DexIgnore
        public b(List<vx4> list) {
            wg6.b(list, "apps");
            this.a = list;
        }

        @DexIgnore
        public final List<vx4> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = d15.class.getSimpleName();
        wg6.a((Object) simpleName, "GetApps::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public o25(Context context) {
        wg6.b(context, "mContext");
        this.d = context;
    }

    @DexIgnore
    public void a(y24.b bVar) {
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase GetHybridApps");
        List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<tj4.b> it = tj4.f.f().iterator();
        while (it.hasNext()) {
            tj4.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !xj6.b(next.b(), this.d.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter appFilter = (AppFilter) it2.next();
                    wg6.a((Object) appFilter, "appFilter");
                    if (wg6.a((Object) appFilter.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(appFilter.getDbRowId());
                        installedApp.setCurrentHandGroup(appFilter.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        ud6.c(linkedList);
        a().onSuccess(new b(linkedList));
    }
}
