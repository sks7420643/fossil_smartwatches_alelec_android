package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b7 {
    @DexIgnore
    public static Shader a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            Resources.Theme theme2 = theme;
            TypedArray a2 = e7.a(resources, theme2, attributeSet, g6.GradientColor);
            float a3 = e7.a(a2, xmlPullParser2, "startX", g6.GradientColor_android_startX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a4 = e7.a(a2, xmlPullParser2, "startY", g6.GradientColor_android_startY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a5 = e7.a(a2, xmlPullParser2, "endX", g6.GradientColor_android_endX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a6 = e7.a(a2, xmlPullParser2, "endY", g6.GradientColor_android_endY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a7 = e7.a(a2, xmlPullParser2, "centerX", g6.GradientColor_android_centerX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a8 = e7.a(a2, xmlPullParser2, "centerY", g6.GradientColor_android_centerY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            int b = e7.b(a2, xmlPullParser2, "type", g6.GradientColor_android_type, 0);
            int a9 = e7.a(a2, xmlPullParser2, "startColor", g6.GradientColor_android_startColor, 0);
            boolean a10 = e7.a(xmlPullParser2, "centerColor");
            int a11 = e7.a(a2, xmlPullParser2, "centerColor", g6.GradientColor_android_centerColor, 0);
            int a12 = e7.a(a2, xmlPullParser2, "endColor", g6.GradientColor_android_endColor, 0);
            int b2 = e7.b(a2, xmlPullParser2, "tileMode", g6.GradientColor_android_tileMode, 0);
            float f = a7;
            float a13 = e7.a(a2, xmlPullParser2, "gradientRadius", g6.GradientColor_android_gradientRadius, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.recycle();
            a a14 = a(b(resources, xmlPullParser, attributeSet, theme), a9, a12, a10, a11);
            if (b == 1) {
                float f2 = f;
                if (a13 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    int[] iArr = a14.a;
                    return new RadialGradient(f2, a8, a13, iArr, a14.b, a(b2));
                }
                throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            } else if (b != 2) {
                return new LinearGradient(a3, a4, a5, a6, a14.a, a14.b, a(b2));
            } else {
                return new SweepGradient(f, a8, a14.a, a14.b);
            }
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0084, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r9.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' attribute!");
     */
    @DexIgnore
    public static a b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth;
        int depth2 = xmlPullParser.getDepth() + 1;
        ArrayList arrayList = new ArrayList(20);
        ArrayList arrayList2 = new ArrayList(20);
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1 && ((depth = xmlPullParser.getDepth()) >= depth2 || next != 3)) {
                if (next == 2 && depth <= depth2 && xmlPullParser.getName().equals("item")) {
                    TypedArray a2 = e7.a(resources, theme, attributeSet, g6.GradientColorItem);
                    boolean hasValue = a2.hasValue(g6.GradientColorItem_android_color);
                    boolean hasValue2 = a2.hasValue(g6.GradientColorItem_android_offset);
                    if (!hasValue || !hasValue2) {
                    } else {
                        int color = a2.getColor(g6.GradientColorItem_android_color, 0);
                        float f = a2.getFloat(g6.GradientColorItem_android_offset, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        a2.recycle();
                        arrayList2.add(Integer.valueOf(color));
                        arrayList.add(Float.valueOf(f));
                    }
                }
            }
        }
        if (arrayList2.size() > 0) {
            return new a((List<Integer>) arrayList2, (List<Float>) arrayList);
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ float[] b;

        @DexIgnore
        public a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; i++) {
                this.a[i] = list.get(i).intValue();
                this.b[i] = list2.get(i).floatValue();
            }
        }

        @DexIgnore
        public a(int i, int i2) {
            this.a = new int[]{i, i2};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f};
        }

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.a = new int[]{i, i2, i3};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
        }
    }

    @DexIgnore
    public static a a(a aVar, int i, int i2, boolean z, int i3) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new a(i, i3, i2);
        }
        return new a(i, i2);
    }

    @DexIgnore
    public static Shader.TileMode a(int i) {
        if (i == 1) {
            return Shader.TileMode.REPEAT;
        }
        if (i != 2) {
            return Shader.TileMode.CLAMP;
        }
        return Shader.TileMode.MIRROR;
    }
}
