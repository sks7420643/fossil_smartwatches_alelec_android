package com.fossil;

import androidx.loader.app.LoaderManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t25 {
    @DexIgnore
    public /* final */ s25 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<wx4> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public t25(s25 s25, int i, ArrayList<wx4> arrayList, LoaderManager loaderManager) {
        wg6.b(s25, "mView");
        wg6.b(loaderManager, "mLoaderManager");
        this.a = s25;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<wx4> a() {
        ArrayList<wx4> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final s25 d() {
        return this.a;
    }
}
