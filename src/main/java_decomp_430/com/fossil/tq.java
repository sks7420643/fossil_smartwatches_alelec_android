package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tq {
    @DexIgnore
    public static vp a(Context context, eq eqVar) {
        fq fqVar;
        String str;
        if (eqVar != null) {
            fqVar = new fq(eqVar);
        } else if (Build.VERSION.SDK_INT >= 9) {
            fqVar = new fq((eq) new mq());
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + "/" + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                str = "volley/0";
            }
            fqVar = new fq((lq) new iq(AndroidHttpClient.newInstance(str)));
        }
        return a(context, (op) fqVar);
    }

    @DexIgnore
    public static vp a(Context context, op opVar) {
        vp vpVar = new vp(new hq(new File(context.getCacheDir(), "volley")), opVar);
        vpVar.b();
        return vpVar;
    }

    @DexIgnore
    public static vp a(Context context) {
        return a(context, (eq) null);
    }
}
