package com.fossil;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ql3<E> extends ml3<E> implements Queue<E> {
    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    public abstract Queue<E> delegate();

    @DexIgnore
    public E element() {
        return delegate().element();
    }

    @DexIgnore
    public abstract boolean offer(E e);

    @DexIgnore
    public E peek() {
        return delegate().peek();
    }

    @DexIgnore
    public E poll() {
        return delegate().poll();
    }

    @DexIgnore
    public E remove() {
        return delegate().remove();
    }

    @DexIgnore
    public boolean standardOffer(E e) {
        try {
            return add(e);
        } catch (IllegalStateException unused) {
            return false;
        }
    }

    @DexIgnore
    public E standardPeek() {
        try {
            return element();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }

    @DexIgnore
    public E standardPoll() {
        try {
            return remove();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }
}
