package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h50 extends z40 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<h50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            z40 createFromParcel = z40.CREATOR.createFromParcel(parcel);
            if (createFromParcel != null) {
                return (h50) createFromParcel;
            }
            throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedAlarm");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new h50[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ h50(i50 i50, k50 k50, c50 c50, int i, qg6 qg6) {
        this(i50, (i & 2) != 0 ? null : k50, (i & 4) != 0 ? null : c50);
    }

    @DexIgnore
    public h50(i50 i50, k50 k50, c50 c50) {
        super(i50, k50, c50);
    }

    @DexIgnore
    public i50 getFireTime() {
        a50[] c = c();
        int length = c.length;
        int i = 0;
        while (i < length) {
            a50 a50 = c[i];
            if (!(a50 instanceof i50)) {
                i++;
            } else if (a50 != null) {
                return (i50) a50;
            } else {
                throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedFireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
