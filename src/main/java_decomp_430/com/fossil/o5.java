package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.j5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o5 {
    @DexIgnore
    public static boolean[] a; // = new boolean[3];

    @DexIgnore
    public static void a(k5 k5Var, z4 z4Var, j5 j5Var) {
        if (k5Var.C[0] != j5.b.WRAP_CONTENT && j5Var.C[0] == j5.b.MATCH_PARENT) {
            int i = j5Var.s.e;
            int t = k5Var.t() - j5Var.u.e;
            i5 i5Var = j5Var.s;
            i5Var.i = z4Var.a((Object) i5Var);
            i5 i5Var2 = j5Var.u;
            i5Var2.i = z4Var.a((Object) i5Var2);
            z4Var.a(j5Var.s.i, i);
            z4Var.a(j5Var.u.i, t);
            j5Var.a = 2;
            j5Var.a(i, t);
        }
        if (k5Var.C[1] != j5.b.WRAP_CONTENT && j5Var.C[1] == j5.b.MATCH_PARENT) {
            int i2 = j5Var.t.e;
            int j = k5Var.j() - j5Var.v.e;
            i5 i5Var3 = j5Var.t;
            i5Var3.i = z4Var.a((Object) i5Var3);
            i5 i5Var4 = j5Var.v;
            i5Var4.i = z4Var.a((Object) i5Var4);
            z4Var.a(j5Var.t.i, i2);
            z4Var.a(j5Var.v.i, j);
            if (j5Var.Q > 0 || j5Var.s() == 8) {
                i5 i5Var5 = j5Var.w;
                i5Var5.i = z4Var.a((Object) i5Var5);
                z4Var.a(j5Var.w.i, j5Var.Q + i2);
            }
            j5Var.b = 2;
            j5Var.e(i2, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[RETURN] */
    public static boolean a(j5 j5Var, int i) {
        j5.b[] bVarArr = j5Var.C;
        if (bVarArr[i] != j5.b.MATCH_CONSTRAINT) {
            return false;
        }
        char c = 1;
        if (j5Var.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (i != 0) {
                c = 0;
            }
            if (bVarArr[c] == j5.b.MATCH_CONSTRAINT) {
            }
            return false;
        }
        if (i == 0) {
            if (j5Var.e == 0 && j5Var.h == 0 && j5Var.i == 0) {
                return true;
            }
            return false;
        } else if (j5Var.f != 0 || j5Var.k != 0 || j5Var.l != 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static void a(int i, j5 j5Var) {
        j5 j5Var2 = j5Var;
        j5Var.J();
        q5 d = j5Var2.s.d();
        q5 d2 = j5Var2.t.d();
        q5 d3 = j5Var2.u.d();
        q5 d4 = j5Var2.v.d();
        boolean z = (i & 8) == 8;
        boolean z2 = j5Var2.C[0] == j5.b.MATCH_CONSTRAINT && a(j5Var2, 0);
        if (!(d.h == 4 || d3.h == 4)) {
            if (j5Var2.C[0] == j5.b.FIXED || (z2 && j5Var.s() == 8)) {
                if (j5Var2.s.d == null && j5Var2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, j5Var.n());
                    } else {
                        d3.a(d, j5Var.t());
                    }
                } else if (j5Var2.s.d != null && j5Var2.u.d == null) {
                    d.b(1);
                    d3.b(1);
                    if (z) {
                        d3.a(d, 1, j5Var.n());
                    } else {
                        d3.a(d, j5Var.t());
                    }
                } else if (j5Var2.s.d == null && j5Var2.u.d != null) {
                    d.b(1);
                    d3.b(1);
                    d.a(d3, -j5Var.t());
                    if (z) {
                        d.a(d3, -1, j5Var.n());
                    } else {
                        d.a(d3, -j5Var.t());
                    }
                } else if (!(j5Var2.s.d == null || j5Var2.u.d == null)) {
                    d.b(2);
                    d3.b(2);
                    if (z) {
                        j5Var.n().a(d);
                        j5Var.n().a(d3);
                        d.b(d3, -1, j5Var.n());
                        d3.b(d, 1, j5Var.n());
                    } else {
                        d.b(d3, (float) (-j5Var.t()));
                        d3.b(d, (float) j5Var.t());
                    }
                }
            } else if (z2) {
                int t = j5Var.t();
                d.b(1);
                d3.b(1);
                if (j5Var2.s.d == null && j5Var2.u.d == null) {
                    if (z) {
                        d3.a(d, 1, j5Var.n());
                    } else {
                        d3.a(d, t);
                    }
                } else if (j5Var2.s.d == null || j5Var2.u.d != null) {
                    if (j5Var2.s.d != null || j5Var2.u.d == null) {
                        if (!(j5Var2.s.d == null || j5Var2.u.d == null)) {
                            if (z) {
                                j5Var.n().a(d);
                                j5Var.n().a(d3);
                            }
                            if (j5Var2.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d.b(3);
                                d3.b(3);
                                d.b(d3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d3.b(d, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                d.b(2);
                                d3.b(2);
                                d.b(d3, (float) (-t));
                                d3.b(d, (float) t);
                                j5Var2.p(t);
                            }
                        }
                    } else if (z) {
                        d.a(d3, -1, j5Var.n());
                    } else {
                        d.a(d3, -t);
                    }
                } else if (z) {
                    d3.a(d, 1, j5Var.n());
                } else {
                    d3.a(d, t);
                }
            }
        }
        boolean z3 = j5Var2.C[1] == j5.b.MATCH_CONSTRAINT && a(j5Var2, 1);
        if (d2.h != 4 && d4.h != 4) {
            if (j5Var2.C[1] == j5.b.FIXED || (z3 && j5Var.s() == 8)) {
                if (j5Var2.t.d == null && j5Var2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, j5Var.m());
                    } else {
                        d4.a(d2, j5Var.j());
                    }
                    i5 i5Var = j5Var2.w;
                    if (i5Var.d != null) {
                        i5Var.d().b(1);
                        d2.a(1, j5Var2.w.d(), -j5Var2.Q);
                    }
                } else if (j5Var2.t.d != null && j5Var2.v.d == null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d4.a(d2, 1, j5Var.m());
                    } else {
                        d4.a(d2, j5Var.j());
                    }
                    if (j5Var2.Q > 0) {
                        j5Var2.w.d().a(1, d2, j5Var2.Q);
                    }
                } else if (j5Var2.t.d == null && j5Var2.v.d != null) {
                    d2.b(1);
                    d4.b(1);
                    if (z) {
                        d2.a(d4, -1, j5Var.m());
                    } else {
                        d2.a(d4, -j5Var.j());
                    }
                    if (j5Var2.Q > 0) {
                        j5Var2.w.d().a(1, d2, j5Var2.Q);
                    }
                } else if (j5Var2.t.d != null && j5Var2.v.d != null) {
                    d2.b(2);
                    d4.b(2);
                    if (z) {
                        d2.b(d4, -1, j5Var.m());
                        d4.b(d2, 1, j5Var.m());
                        j5Var.m().a(d2);
                        j5Var.n().a(d4);
                    } else {
                        d2.b(d4, (float) (-j5Var.j()));
                        d4.b(d2, (float) j5Var.j());
                    }
                    if (j5Var2.Q > 0) {
                        j5Var2.w.d().a(1, d2, j5Var2.Q);
                    }
                }
            } else if (z3) {
                int j = j5Var.j();
                d2.b(1);
                d4.b(1);
                if (j5Var2.t.d == null && j5Var2.v.d == null) {
                    if (z) {
                        d4.a(d2, 1, j5Var.m());
                    } else {
                        d4.a(d2, j);
                    }
                } else if (j5Var2.t.d == null || j5Var2.v.d != null) {
                    if (j5Var2.t.d != null || j5Var2.v.d == null) {
                        if (j5Var2.t.d != null && j5Var2.v.d != null) {
                            if (z) {
                                j5Var.m().a(d2);
                                j5Var.n().a(d4);
                            }
                            if (j5Var2.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d2.b(3);
                                d4.b(3);
                                d2.b(d4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d4.b(d2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                return;
                            }
                            d2.b(2);
                            d4.b(2);
                            d2.b(d4, (float) (-j));
                            d4.b(d2, (float) j);
                            j5Var2.h(j);
                            if (j5Var2.Q > 0) {
                                j5Var2.w.d().a(1, d2, j5Var2.Q);
                            }
                        }
                    } else if (z) {
                        d2.a(d4, -1, j5Var.m());
                    } else {
                        d2.a(d4, -j);
                    }
                } else if (z) {
                    d4.a(d2, 1, j5Var.m());
                } else {
                    d4.a(d2, j);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r7.e0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r7.f0 == 2) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0109  */
    public static boolean a(k5 k5Var, z4 z4Var, int i, int i2, h5 h5Var) {
        boolean z;
        boolean z2;
        float f;
        int i3;
        int i4;
        float f2;
        j5 j5Var;
        boolean z3;
        int i5;
        z4 z4Var2 = z4Var;
        int i6 = i;
        h5 h5Var2 = h5Var;
        j5 j5Var2 = h5Var2.a;
        j5 j5Var3 = h5Var2.c;
        j5 j5Var4 = h5Var2.b;
        j5 j5Var5 = h5Var2.d;
        j5 j5Var6 = h5Var2.e;
        float f3 = h5Var2.k;
        j5 j5Var7 = h5Var2.f;
        j5 j5Var8 = h5Var2.g;
        j5.b bVar = k5Var.C[i6];
        j5.b bVar2 = j5.b.WRAP_CONTENT;
        if (i6 == 0) {
            z2 = j5Var6.e0 == 0;
            z = j5Var6.e0 == 1;
        } else {
            z2 = j5Var6.f0 == 0;
            z = j5Var6.f0 == 1;
        }
        boolean z4 = true;
        j5 j5Var9 = j5Var2;
        int i7 = 0;
        boolean z5 = false;
        int i8 = 0;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (!z5) {
            if (j5Var9.s() != 8) {
                i8++;
                if (i6 == 0) {
                    i5 = j5Var9.t();
                } else {
                    i5 = j5Var9.j();
                }
                f4 += (float) i5;
                if (j5Var9 != j5Var4) {
                    f4 += (float) j5Var9.A[i2].b();
                }
                if (j5Var9 != j5Var5) {
                    f4 += (float) j5Var9.A[i2 + 1].b();
                }
                f5 = f5 + ((float) j5Var9.A[i2].b()) + ((float) j5Var9.A[i2 + 1].b());
            }
            i5 i5Var = j5Var9.A[i2];
            if (j5Var9.s() != 8 && j5Var9.C[i6] == j5.b.MATCH_CONSTRAINT) {
                i7++;
                if (i6 != 0) {
                    z3 = false;
                    if (j5Var9.f != 0) {
                        return false;
                    }
                    if (j5Var9.k == 0) {
                        if (j5Var9.l != 0) {
                        }
                    }
                    return z3;
                } else if (j5Var9.e != 0) {
                    return false;
                } else {
                    z3 = false;
                    if (!(j5Var9.h == 0 && j5Var9.i == 0)) {
                        return false;
                    }
                }
                if (j5Var9.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return z3;
                }
            }
            i5 i5Var2 = j5Var9.A[i2 + 1].d;
            if (i5Var2 != null) {
                j5 j5Var10 = i5Var2.b;
                i5[] i5VarArr = j5Var10.A;
                j5 j5Var11 = j5Var10;
                if (i5VarArr[i2].d != null && i5VarArr[i2].d.b == j5Var9) {
                    j5Var = j5Var11;
                    if (j5Var == null) {
                        j5Var9 = j5Var;
                    } else {
                        z5 = true;
                    }
                }
            }
            j5Var = null;
            if (j5Var == null) {
            }
        }
        q5 d = j5Var2.A[i2].d();
        int i9 = i2 + 1;
        q5 d2 = j5Var3.A[i9].d();
        q5 q5Var = d.d;
        if (q5Var == null) {
            return false;
        }
        j5 j5Var12 = j5Var2;
        q5 q5Var2 = d2.d;
        if (q5Var2 == null || q5Var.b != 1 || q5Var2.b != 1) {
            return false;
        }
        if (i7 > 0 && i7 != i8) {
            return false;
        }
        if (z4 || z2 || z) {
            f = j5Var4 != null ? (float) j5Var4.A[i2].b() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (j5Var5 != null) {
                f += (float) j5Var5.A[i9].b();
            }
        } else {
            f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f6 = d.d.g;
        float f7 = d2.d.g;
        float f8 = (f6 < f7 ? f7 - f6 : f6 - f7) - f4;
        if (i7 <= 0 || i7 != i8) {
            z4 z4Var3 = z4Var;
            if (f8 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z4 = true;
                z2 = false;
                z = false;
            }
            if (z4) {
                j5 j5Var13 = j5Var12;
                float b = f6 + ((f8 - f) * j5Var13.b(i6));
                while (j5Var13 != null) {
                    a5 a5Var = z4.q;
                    if (a5Var != null) {
                        a5Var.z--;
                        a5Var.r++;
                        a5Var.x++;
                    }
                    j5 j5Var14 = j5Var13.i0[i6];
                    if (j5Var14 != null || j5Var13 == j5Var3) {
                        if (i6 == 0) {
                            i4 = j5Var13.t();
                        } else {
                            i4 = j5Var13.j();
                        }
                        float b2 = b + ((float) j5Var13.A[i2].b());
                        j5Var13.A[i2].d().a(d.f, b2);
                        float f9 = b2 + ((float) i4);
                        j5Var13.A[i9].d().a(d.f, f9);
                        j5Var13.A[i2].d().a(z4Var3);
                        j5Var13.A[i9].d().a(z4Var3);
                        b = f9 + ((float) j5Var13.A[i9].b());
                    }
                    j5Var13 = j5Var14;
                }
                return true;
            }
            j5 j5Var15 = j5Var12;
            if (!z2 && !z) {
                return true;
            }
            if (z2 || z) {
                f8 -= f;
            }
            float f10 = f8 / ((float) (i8 + 1));
            if (z) {
                f10 = f8 / (i8 > 1 ? (float) (i8 - 1) : 2.0f);
            }
            float f11 = j5Var15.s() != 8 ? f6 + f10 : f6;
            if (z && i8 > 1) {
                f11 = ((float) j5Var4.A[i2].b()) + f6;
            }
            if (z2 && j5Var4 != null) {
                f11 += (float) j5Var4.A[i2].b();
            }
            while (j5Var15 != null) {
                a5 a5Var2 = z4.q;
                if (a5Var2 != null) {
                    a5Var2.z--;
                    a5Var2.r++;
                    a5Var2.x++;
                }
                j5 j5Var16 = j5Var15.i0[i6];
                if (j5Var16 != null || j5Var15 == j5Var3) {
                    if (i6 == 0) {
                        i3 = j5Var15.t();
                    } else {
                        i3 = j5Var15.j();
                    }
                    float f12 = (float) i3;
                    if (j5Var15 != j5Var4) {
                        f11 += (float) j5Var15.A[i2].b();
                    }
                    j5Var15.A[i2].d().a(d.f, f11);
                    j5Var15.A[i9].d().a(d.f, f11 + f12);
                    j5Var15.A[i2].d().a(z4Var3);
                    j5Var15.A[i9].d().a(z4Var3);
                    f11 += f12 + ((float) j5Var15.A[i9].b());
                    if (j5Var16 != null) {
                        if (j5Var16.s() != 8) {
                            f11 += f10;
                        }
                        j5Var15 = j5Var16;
                    }
                }
                j5Var15 = j5Var16;
            }
            return true;
        } else if (j5Var9.l() != null && j5Var9.l().C[i6] == j5.b.WRAP_CONTENT) {
            return false;
        } else {
            float f13 = (f8 + f4) - f5;
            float f14 = f6;
            j5 j5Var17 = j5Var12;
            while (j5Var17 != null) {
                a5 a5Var3 = z4.q;
                if (a5Var3 != null) {
                    a5Var3.z--;
                    a5Var3.r++;
                    a5Var3.x++;
                }
                j5 j5Var18 = j5Var17.i0[i6];
                if (j5Var18 != null || j5Var17 == j5Var3) {
                    float f15 = f13 / ((float) i7);
                    if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        float[] fArr = j5Var17.g0;
                        if (fArr[i6] == -1.0f) {
                            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            if (j5Var17.s() == 8) {
                                f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                            float b3 = f14 + ((float) j5Var17.A[i2].b());
                            j5Var17.A[i2].d().a(d.f, b3);
                            float f16 = b3 + f2;
                            j5Var17.A[i9].d().a(d.f, f16);
                            z4 z4Var4 = z4Var;
                            j5Var17.A[i2].d().a(z4Var4);
                            j5Var17.A[i9].d().a(z4Var4);
                            f14 = f16 + ((float) j5Var17.A[i9].b());
                        } else {
                            f15 = (fArr[i6] * f13) / f3;
                        }
                    }
                    f2 = f15;
                    if (j5Var17.s() == 8) {
                    }
                    float b32 = f14 + ((float) j5Var17.A[i2].b());
                    j5Var17.A[i2].d().a(d.f, b32);
                    float f162 = b32 + f2;
                    j5Var17.A[i9].d().a(d.f, f162);
                    z4 z4Var42 = z4Var;
                    j5Var17.A[i2].d().a(z4Var42);
                    j5Var17.A[i9].d().a(z4Var42);
                    f14 = f162 + ((float) j5Var17.A[i9].b());
                } else {
                    z4 z4Var5 = z4Var;
                }
                j5Var17 = j5Var18;
            }
            return true;
        }
    }

    @DexIgnore
    public static void a(j5 j5Var, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        j5Var.A[i3].d().f = j5Var.l().s.d();
        j5Var.A[i3].d().g = (float) i2;
        j5Var.A[i3].d().b = 1;
        j5Var.A[i4].d().f = j5Var.A[i3].d();
        j5Var.A[i4].d().g = (float) j5Var.d(i);
        j5Var.A[i4].d().b = 1;
    }
}
