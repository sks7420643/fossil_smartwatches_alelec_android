package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dd6 {
    @DexIgnore
    public static final int a(int i, int i2) {
        return wg6.a(i ^ Integer.MIN_VALUE, i2 ^ Integer.MIN_VALUE);
    }

    @DexIgnore
    public static final int a(long j, long j2) {
        return ((j ^ Long.MIN_VALUE) > (j2 ^ Long.MIN_VALUE) ? 1 : ((j ^ Long.MIN_VALUE) == (j2 ^ Long.MIN_VALUE) ? 0 : -1));
    }

    @DexIgnore
    public static final String a(long j) {
        return a(j, 10);
    }

    @DexIgnore
    public static final String a(long j, int i) {
        if (j >= 0) {
            cj6.a(i);
            String l = Long.toString(j, i);
            wg6.a((Object) l, "java.lang.Long.toString(this, checkRadix(radix))");
            return l;
        }
        long j2 = (long) i;
        long j3 = ((j >>> 1) / j2) << 1;
        long j4 = j - (j3 * j2);
        if (j4 >= j2) {
            j4 -= j2;
            j3++;
        }
        StringBuilder sb = new StringBuilder();
        cj6.a(i);
        String l2 = Long.toString(j3, i);
        wg6.a((Object) l2, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l2);
        cj6.a(i);
        String l3 = Long.toString(j4, i);
        wg6.a((Object) l3, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l3);
        return sb.toString();
    }
}
