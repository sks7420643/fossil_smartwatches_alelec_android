package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.j256.ormlite.logger.Logger;
import com.zendesk.sdk.R;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p24 extends hb {
    @DexIgnore
    public static /* final */ SparseIntArray a; // = new SparseIntArray(157);

    /*
    static {
        a.put(2131558434, 1);
        a.put(2131558438, 2);
        a.put(2131558493, 3);
        a.put(2131558494, 4);
        a.put(2131558495, 5);
        a.put(2131558496, 6);
        a.put(2131558497, 7);
        a.put(2131558498, 8);
        a.put(2131558499, 9);
        a.put(2131558500, 10);
        a.put(2131558501, 11);
        a.put(2131558502, 12);
        a.put(2131558503, 13);
        a.put(2131558504, 14);
        a.put(2131558505, 15);
        a.put(2131558506, 16);
        a.put(2131558507, 17);
        a.put(2131558508, 18);
        a.put(2131558509, 19);
        a.put(2131558510, 20);
        a.put(2131558511, 21);
        a.put(2131558512, 22);
        a.put(2131558513, 23);
        a.put(2131558514, 24);
        a.put(2131558515, 25);
        a.put(2131558516, 26);
        a.put(2131558517, 27);
        a.put(2131558519, 28);
        a.put(2131558520, 29);
        a.put(2131558521, 30);
        a.put(2131558523, 31);
        a.put(2131558524, 32);
        a.put(2131558525, 33);
        a.put(2131558526, 34);
        a.put(2131558527, 35);
        a.put(2131558528, 36);
        a.put(2131558529, 37);
        a.put(2131558530, 38);
        a.put(2131558531, 39);
        a.put(2131558532, 40);
        a.put(2131558533, 41);
        a.put(2131558534, 42);
        a.put(2131558535, 43);
        a.put(2131558536, 44);
        a.put(2131558537, 45);
        a.put(2131558538, 46);
        a.put(2131558539, 47);
        a.put(2131558540, 48);
        a.put(2131558541, 49);
        a.put(2131558542, 50);
        a.put(2131558543, 51);
        a.put(2131558544, 52);
        a.put(2131558545, 53);
        a.put(2131558546, 54);
        a.put(2131558547, 55);
        a.put(2131558548, 56);
        a.put(2131558549, 57);
        a.put(2131558550, 58);
        a.put(2131558551, 59);
        a.put(2131558552, 60);
        a.put(2131558553, 61);
        a.put(2131558554, 62);
        a.put(2131558555, 63);
        a.put(2131558556, 64);
        a.put(2131558557, 65);
        a.put(2131558558, 66);
        a.put(2131558559, 67);
        a.put(2131558560, 68);
        a.put(2131558561, 69);
        a.put(R.layout.fragment_help, 70);
        a.put(2131558563, 71);
        a.put(2131558564, 72);
        a.put(2131558565, 73);
        a.put(2131558566, 74);
        a.put(2131558567, 75);
        a.put(2131558568, 76);
        a.put(2131558569, 77);
        a.put(2131558570, 78);
        a.put(2131558571, 79);
        a.put(2131558573, 80);
        a.put(2131558574, 81);
        a.put(2131558575, 82);
        a.put(2131558576, 83);
        a.put(2131558577, 84);
        a.put(2131558578, 85);
        a.put(2131558579, 86);
        a.put(2131558580, 87);
        a.put(2131558581, 88);
        a.put(2131558582, 89);
        a.put(2131558583, 90);
        a.put(2131558584, 91);
        a.put(2131558585, 92);
        a.put(2131558586, 93);
        a.put(2131558587, 94);
        a.put(2131558588, 95);
        a.put(2131558590, 96);
        a.put(2131558591, 97);
        a.put(2131558592, 98);
        a.put(2131558593, 99);
        a.put(2131558594, 100);
        a.put(2131558595, 101);
        a.put(2131558596, 102);
        a.put(2131558597, 103);
        a.put(2131558598, 104);
        a.put(2131558599, 105);
        a.put(2131558600, 106);
        a.put(2131558601, 107);
        a.put(2131558602, 108);
        a.put(2131558605, 109);
        a.put(2131558607, 110);
        a.put(2131558608, 111);
        a.put(2131558609, 112);
        a.put(2131558610, 113);
        a.put(2131558611, 114);
        a.put(2131558612, 115);
        a.put(2131558613, 116);
        a.put(2131558614, 117);
        a.put(2131558616, 118);
        a.put(2131558617, 119);
        a.put(2131558619, 120);
        a.put(2131558620, 121);
        a.put(2131558621, 122);
        a.put(2131558623, 123);
        a.put(2131558624, 124);
        a.put(2131558625, 125);
        a.put(2131558626, 126);
        a.put(2131558630, 127);
        a.put(2131558631, Logger.DEFAULT_FULL_MESSAGE_LENGTH);
        a.put(2131558632, 129);
        a.put(2131558633, 130);
        a.put(2131558634, 131);
        a.put(2131558635, 132);
        a.put(2131558636, 133);
        a.put(2131558638, 134);
        a.put(2131558639, 135);
        a.put(2131558640, 136);
        a.put(2131558643, 137);
        a.put(2131558644, 138);
        a.put(2131558645, 139);
        a.put(2131558650, 140);
        a.put(2131558651, 141);
        a.put(2131558658, 142);
        a.put(2131558662, 143);
        a.put(2131558664, 144);
        a.put(2131558665, 145);
        a.put(2131558666, 146);
        a.put(2131558668, 147);
        a.put(2131558670, 148);
        a.put(2131558676, 149);
        a.put(2131558678, 150);
        a.put(2131558679, 151);
        a.put(2131558684, 152);
        a.put(2131558685, 153);
        a.put(2131558686, 154);
        a.put(2131558693, 155);
        a.put(2131558805, 156);
        a.put(2131558806, 157);
    }
    */

    @DexIgnore
    public final ViewDataBinding a(jb jbVar, View view, int i, Object obj) {
        switch (i) {
            case 1:
                if ("layout/activity_search_ringphone_0".equals(obj)) {
                    return new k54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for activity_search_ringphone is invalid. Received: " + obj);
            case 2:
                if ("layout/activity_webview_0".equals(obj)) {
                    return new m54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for activity_webview is invalid. Received: " + obj);
            case 3:
                if ("layout/fragment_about_0".equals(obj)) {
                    return new o54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_about is invalid. Received: " + obj);
            case 4:
                if ("layout/fragment_active_time_detail_0".equals(obj)) {
                    return new q54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_detail is invalid. Received: " + obj);
            case 5:
                if ("layout/fragment_active_time_overview_0".equals(obj)) {
                    return new s54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview is invalid. Received: " + obj);
            case 6:
                if ("layout/fragment_active_time_overview_day_0".equals(obj)) {
                    return new u54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_day is invalid. Received: " + obj);
            case 7:
                if ("layout/fragment_active_time_overview_month_0".equals(obj)) {
                    return new w54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_month is invalid. Received: " + obj);
            case 8:
                if ("layout/fragment_active_time_overview_week_0".equals(obj)) {
                    return new y54(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_active_time_overview_week is invalid. Received: " + obj);
            case 9:
                if ("layout/fragment_activity_detail_0".equals(obj)) {
                    return new a64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_detail is invalid. Received: " + obj);
            case 10:
                if ("layout/fragment_activity_overview_0".equals(obj)) {
                    return new c64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview is invalid. Received: " + obj);
            case 11:
                if ("layout/fragment_activity_overview_day_0".equals(obj)) {
                    return new e64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_day is invalid. Received: " + obj);
            case 12:
                if ("layout/fragment_activity_overview_month_0".equals(obj)) {
                    return new g64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_month is invalid. Received: " + obj);
            case 13:
                if ("layout/fragment_activity_overview_week_0".equals(obj)) {
                    return new i64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_activity_overview_week is invalid. Received: " + obj);
            case 14:
                if ("layout/fragment_add_photo_menu_0".equals(obj)) {
                    return new k64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_add_photo_menu is invalid. Received: " + obj);
            case 15:
                if ("layout/fragment_alarm_0".equals(obj)) {
                    return new m64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_alarm is invalid. Received: " + obj);
            case 16:
                if ("layout/fragment_allow_notification_service_0".equals(obj)) {
                    return new o64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_allow_notification_service is invalid. Received: " + obj);
            case 17:
                if ("layout/fragment_birthday_0".equals(obj)) {
                    return new q64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_birthday is invalid. Received: " + obj);
            case 18:
                if ("layout/fragment_calibration_0".equals(obj)) {
                    return new s64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calibration is invalid. Received: " + obj);
            case 19:
                if ("layout/fragment_calories_detail_0".equals(obj)) {
                    return new u64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_detail is invalid. Received: " + obj);
            case 20:
                if ("layout/fragment_calories_overview_0".equals(obj)) {
                    return new w64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview is invalid. Received: " + obj);
            case 21:
                if ("layout/fragment_calories_overview_day_0".equals(obj)) {
                    return new y64(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_day is invalid. Received: " + obj);
            case 22:
                if ("layout/fragment_calories_overview_month_0".equals(obj)) {
                    return new a74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_month is invalid. Received: " + obj);
            case 23:
                if ("layout/fragment_calories_overview_week_0".equals(obj)) {
                    return new c74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_calories_overview_week is invalid. Received: " + obj);
            case 24:
                if ("layout/fragment_commute_time_settings_0".equals(obj)) {
                    return new e74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings is invalid. Received: " + obj);
            case 25:
                if ("layout/fragment_commute_time_settings_default_address_0".equals(obj)) {
                    return new g74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_default_address is invalid. Received: " + obj);
            case 26:
                if ("layout/fragment_commute_time_settings_detail_0".equals(obj)) {
                    return new i74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_settings_detail is invalid. Received: " + obj);
            case 27:
                if ("layout/fragment_commute_time_watch_app_settings_0".equals(obj)) {
                    return new k74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_commute_time_watch_app_settings is invalid. Received: " + obj);
            case 28:
                if ("layout/fragment_complication_search_0".equals(obj)) {
                    return new m74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complication_search is invalid. Received: " + obj);
            case 29:
                if ("layout/fragment_complications_0".equals(obj)) {
                    return new o74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_complications is invalid. Received: " + obj);
            case 30:
                if ("layout/fragment_connected_apps_0".equals(obj)) {
                    return new ConnectedAppsFragmentMapping(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_connected_apps is invalid. Received: " + obj);
            case 31:
                if ("layout/fragment_customize_active_calories_chart_0".equals(obj)) {
                    return new s74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_active_calories_chart is invalid. Received: " + obj);
            case 32:
                if ("layout/fragment_customize_active_minute_chart_0".equals(obj)) {
                    return new u74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_active_minute_chart is invalid. Received: " + obj);
            case 33:
                if ("layout/fragment_customize_activity_chart_0".equals(obj)) {
                    return new w74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_activity_chart is invalid. Received: " + obj);
            case 34:
                if ("layout/fragment_customize_back_ground_0".equals(obj)) {
                    return new y74(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_back_ground is invalid. Received: " + obj);
            case 35:
                if ("layout/fragment_customize_button_0".equals(obj)) {
                    return new a84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_button is invalid. Received: " + obj);
            case 36:
                if ("layout/fragment_customize_font_0".equals(obj)) {
                    return new c84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_font is invalid. Received: " + obj);
            case 37:
                if ("layout/fragment_customize_goal_tracking_chart_0".equals(obj)) {
                    return new e84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_goal_tracking_chart is invalid. Received: " + obj);
            case 38:
                if ("layout/fragment_customize_heart_rate_chart_0".equals(obj)) {
                    return new g84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_heart_rate_chart is invalid. Received: " + obj);
            case 39:
                if ("layout/fragment_customize_ring_chart_0".equals(obj)) {
                    return new i84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_ring_chart is invalid. Received: " + obj);
            case 40:
                if ("layout/fragment_customize_sleep_chart_0".equals(obj)) {
                    return new k84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_sleep_chart is invalid. Received: " + obj);
            case 41:
                if ("layout/fragment_customize_text_0".equals(obj)) {
                    return new m84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_text is invalid. Received: " + obj);
            case 42:
                if ("layout/fragment_customize_theme_0".equals(obj)) {
                    return new o84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_theme is invalid. Received: " + obj);
            case 43:
                if ("layout/fragment_customize_tutorial_0".equals(obj)) {
                    return new q84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_customize_tutorial is invalid. Received: " + obj);
            case 44:
                if ("layout/fragment_dashboard_active_time_0".equals(obj)) {
                    return new s84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_active_time is invalid. Received: " + obj);
            case 45:
                if ("layout/fragment_dashboard_activity_0".equals(obj)) {
                    return new u84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_activity is invalid. Received: " + obj);
            case 46:
                if ("layout/fragment_dashboard_calories_0".equals(obj)) {
                    return new w84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_calories is invalid. Received: " + obj);
            case 47:
                if ("layout/fragment_dashboard_goal_tracking_0".equals(obj)) {
                    return new y84(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_goal_tracking is invalid. Received: " + obj);
            case 48:
                if ("layout/fragment_dashboard_heartrate_0".equals(obj)) {
                    return new a94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_heartrate is invalid. Received: " + obj);
            case 49:
                if ("layout/fragment_dashboard_sleep_0".equals(obj)) {
                    return new c94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_dashboard_sleep is invalid. Received: " + obj);
            case 50:
                if ("layout/fragment_delete_account_0".equals(obj)) {
                    return new e94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_delete_account is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding b(jb jbVar, View view, int i, Object obj) {
        switch (i) {
            case 51:
                if ("layout/fragment_diana_customize_edit_0".equals(obj)) {
                    return new g94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_diana_customize_edit is invalid. Received: " + obj);
            case 52:
                if ("layout/fragment_do_not_disturb_scheduled_time_0".equals(obj)) {
                    return new i94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_do_not_disturb_scheduled_time is invalid. Received: " + obj);
            case 53:
                if ("layout/fragment_edit_photo_0".equals(obj)) {
                    return new k94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_edit_photo is invalid. Received: " + obj);
            case 54:
                if ("layout/fragment_edit_response_0".equals(obj)) {
                    return new m94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_edit_response is invalid. Received: " + obj);
            case 55:
                if ("layout/fragment_email_verification_0".equals(obj)) {
                    return new o94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_email_verification is invalid. Received: " + obj);
            case 56:
                if ("layout/fragment_explore_watch_0".equals(obj)) {
                    return new q94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_explore_watch is invalid. Received: " + obj);
            case 57:
                if ("layout/fragment_find_device_0".equals(obj)) {
                    return new s94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_find_device is invalid. Received: " + obj);
            case 58:
                if ("layout/fragment_forgot_password_0".equals(obj)) {
                    return new u94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_forgot_password is invalid. Received: " + obj);
            case 59:
                if ("layout/fragment_getting_started_0".equals(obj)) {
                    return new w94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_getting_started is invalid. Received: " + obj);
            case 60:
                if ("layout/fragment_goal_tracking_detail_0".equals(obj)) {
                    return new y94(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_detail is invalid. Received: " + obj);
            case 61:
                if ("layout/fragment_goal_tracking_overview_0".equals(obj)) {
                    return new aa4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview is invalid. Received: " + obj);
            case 62:
                if ("layout/fragment_goal_tracking_overview_day_0".equals(obj)) {
                    return new ca4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_day is invalid. Received: " + obj);
            case 63:
                if ("layout/fragment_goal_tracking_overview_month_0".equals(obj)) {
                    return new ea4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_month is invalid. Received: " + obj);
            case 64:
                if ("layout/fragment_goal_tracking_overview_week_0".equals(obj)) {
                    return new ga4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_goal_tracking_overview_week is invalid. Received: " + obj);
            case 65:
                if ("layout/fragment_heartrate_detail_0".equals(obj)) {
                    return new ia4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_detail is invalid. Received: " + obj);
            case 66:
                if ("layout/fragment_heartrate_overview_0".equals(obj)) {
                    return new ka4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview is invalid. Received: " + obj);
            case 67:
                if ("layout/fragment_heartrate_overview_day_0".equals(obj)) {
                    return new ma4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_day is invalid. Received: " + obj);
            case 68:
                if ("layout/fragment_heartrate_overview_month_0".equals(obj)) {
                    return new oa4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_month is invalid. Received: " + obj);
            case 69:
                if ("layout/fragment_heartrate_overview_week_0".equals(obj)) {
                    return new qa4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_heartrate_overview_week is invalid. Received: " + obj);
            case 70:
                if ("layout/fragment_help_0".equals(obj)) {
                    return new sa4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_help is invalid. Received: " + obj);
            case 71:
                if ("layout/fragment_home_0".equals(obj)) {
                    return new ya4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + obj);
            case 72:
                if ("layout/fragment_home_alerts_0".equals(obj)) {
                    return new ua4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts is invalid. Received: " + obj);
            case 73:
                if ("layout/fragment_home_alerts_hybrid_0".equals(obj)) {
                    return new wa4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_alerts_hybrid is invalid. Received: " + obj);
            case 74:
                if ("layout/fragment_home_dashboard_0".equals(obj)) {
                    return new ab4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_dashboard is invalid. Received: " + obj);
            case 75:
                if ("layout/fragment_home_diana_customize_0".equals(obj)) {
                    return new cb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_diana_customize is invalid. Received: " + obj);
            case 76:
                if ("layout/fragment_home_hybrid_customize_0".equals(obj)) {
                    return new eb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_hybrid_customize is invalid. Received: " + obj);
            case 77:
                if ("layout/fragment_home_profile_0".equals(obj)) {
                    return new gb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_profile is invalid. Received: " + obj);
            case 78:
                if ("layout/fragment_home_update_firmware_0".equals(obj)) {
                    return new ib4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_home_update_firmware is invalid. Received: " + obj);
            case 79:
                if ("layout/fragment_hybrid_customize_edit_0".equals(obj)) {
                    return new kb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_hybrid_customize_edit is invalid. Received: " + obj);
            case 80:
                if ("layout/fragment_map_picker_0".equals(obj)) {
                    return new mb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_map_picker is invalid. Received: " + obj);
            case 81:
                if ("layout/fragment_micro_app_0".equals(obj)) {
                    return new ob4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app is invalid. Received: " + obj);
            case 82:
                if ("layout/fragment_micro_app_search_0".equals(obj)) {
                    return new qb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_micro_app_search is invalid. Received: " + obj);
            case 83:
                if ("layout/fragment_notification_allow_calls_and_messages_0".equals(obj)) {
                    return new sb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_allow_calls_and_messages is invalid. Received: " + obj);
            case 84:
                if ("layout/fragment_notification_apps_0".equals(obj)) {
                    return new ub4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_apps is invalid. Received: " + obj);
            case 85:
                if ("layout/fragment_notification_calls_and_messages_0".equals(obj)) {
                    return new wb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_calls_and_messages is invalid. Received: " + obj);
            case 86:
                if ("layout/fragment_notification_contacts_0".equals(obj)) {
                    return new ac4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts is invalid. Received: " + obj);
            case 87:
                if ("layout/fragment_notification_contacts_and_apps_assigned_0".equals(obj)) {
                    return new yb4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_contacts_and_apps_assigned is invalid. Received: " + obj);
            case 88:
                if ("layout/fragment_notification_dial_landing_0".equals(obj)) {
                    return new cc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_dial_landing is invalid. Received: " + obj);
            case 89:
                if ("layout/fragment_notification_hybrid_app_0".equals(obj)) {
                    return new ec4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_app is invalid. Received: " + obj);
            case 90:
                if ("layout/fragment_notification_hybrid_contact_0".equals(obj)) {
                    return new gc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_contact is invalid. Received: " + obj);
            case 91:
                if ("layout/fragment_notification_hybrid_everyone_0".equals(obj)) {
                    return new ic4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_hybrid_everyone is invalid. Received: " + obj);
            case 92:
                if ("layout/fragment_notification_settings_type_0".equals(obj)) {
                    return new kc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_settings_type is invalid. Received: " + obj);
            case 93:
                if ("layout/fragment_notification_watch_reminders_0".equals(obj)) {
                    return new mc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_notification_watch_reminders is invalid. Received: " + obj);
            case 94:
                if ("layout/fragment_onboarding_height_weight_0".equals(obj)) {
                    return new oc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_onboarding_height_weight is invalid. Received: " + obj);
            case 95:
                if ("layout/fragment_opt_in_0".equals(obj)) {
                    return new OptInFragmentMapping(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_opt_in is invalid. Received: " + obj);
            case 96:
                if ("layout/fragment_pairing_authorize_0".equals(obj)) {
                    return new sc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_authorize is invalid. Received: " + obj);
            case 97:
                if ("layout/fragment_pairing_device_found_0".equals(obj)) {
                    return new uc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_device_found is invalid. Received: " + obj);
            case 98:
                if ("layout/fragment_pairing_instructions_0".equals(obj)) {
                    return new wc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_instructions is invalid. Received: " + obj);
            case 99:
                if ("layout/fragment_pairing_look_for_device_0".equals(obj)) {
                    return new yc4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_pairing_look_for_device is invalid. Received: " + obj);
            case 100:
                if ("layout/fragment_permission_0".equals(obj)) {
                    return new ad4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_permission is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding c(jb jbVar, View view, int i, Object obj) {
        switch (i) {
            case 101:
                if ("layout/fragment_preferred_unit_0".equals(obj)) {
                    return new cd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_preferred_unit is invalid. Received: " + obj);
            case 102:
                if ("layout/fragment_preview_0".equals(obj)) {
                    return new WatchFacePreviewFragmentMapping(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_preview is invalid. Received: " + obj);
            case 103:
                if ("layout/fragment_profile_change_pass_0".equals(obj)) {
                    return new gd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_change_pass is invalid. Received: " + obj);
            case 104:
                if ("layout/fragment_profile_edit_0".equals(obj)) {
                    return new id4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_edit is invalid. Received: " + obj);
            case 105:
                if ("layout/fragment_profile_goals_edit_0".equals(obj)) {
                    return new kd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_goals_edit is invalid. Received: " + obj);
            case 106:
                if ("layout/fragment_profile_setup_0".equals(obj)) {
                    return new md4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_profile_setup is invalid. Received: " + obj);
            case 107:
                if ("layout/fragment_quick_response_0".equals(obj)) {
                    return new od4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_quick_response is invalid. Received: " + obj);
            case 108:
                if ("layout/fragment_remind_time_0".equals(obj)) {
                    return new qd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_remind_time is invalid. Received: " + obj);
            case 109:
                if ("layout/fragment_replace_battery_0".equals(obj)) {
                    return new sd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_replace_battery is invalid. Received: " + obj);
            case 110:
                if ("layout/fragment_search_second_timezone_0".equals(obj)) {
                    return new ud4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_search_second_timezone is invalid. Received: " + obj);
            case 111:
                if ("layout/fragment_signin_0".equals(obj)) {
                    return new wd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signin is invalid. Received: " + obj);
            case 112:
                if ("layout/fragment_signup_0".equals(obj)) {
                    return new yd4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_signup is invalid. Received: " + obj);
            case 113:
                if ("layout/fragment_sleep_detail_0".equals(obj)) {
                    return new ae4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_detail is invalid. Received: " + obj);
            case 114:
                if ("layout/fragment_sleep_overview_0".equals(obj)) {
                    return new ce4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview is invalid. Received: " + obj);
            case 115:
                if ("layout/fragment_sleep_overview_day_0".equals(obj)) {
                    return new ee4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_day is invalid. Received: " + obj);
            case 116:
                if ("layout/fragment_sleep_overview_month_0".equals(obj)) {
                    return new ge4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_month is invalid. Received: " + obj);
            case 117:
                if ("layout/fragment_sleep_overview_week_0".equals(obj)) {
                    return new ie4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_sleep_overview_week is invalid. Received: " + obj);
            case 118:
                if ("layout/fragment_themes_0".equals(obj)) {
                    return new ke4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_themes is invalid. Received: " + obj);
            case 119:
                if ("layout/fragment_troubleshooting_0".equals(obj)) {
                    return new me4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_troubleshooting is invalid. Received: " + obj);
            case 120:
                if ("layout/fragment_update_failed_troubleshooting_0".equals(obj)) {
                    return new oe4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_update_failed_troubleshooting is invalid. Received: " + obj);
            case 121:
                if ("layout/fragment_update_firmware_0".equals(obj)) {
                    return new qe4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_update_firmware is invalid. Received: " + obj);
            case 122:
                if ("layout/fragment_user_customize_theme_0".equals(obj)) {
                    return new se4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_user_customize_theme is invalid. Received: " + obj);
            case 123:
                if ("layout/fragment_watch_app_search_0".equals(obj)) {
                    return new ue4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_app_search is invalid. Received: " + obj);
            case 124:
                if ("layout/fragment_watch_apps_0".equals(obj)) {
                    return new we4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_apps is invalid. Received: " + obj);
            case 125:
                if ("layout/fragment_watch_setting_0".equals(obj)) {
                    return new WatchSettingFragmentMapping(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_watch_setting is invalid. Received: " + obj);
            case 126:
                if ("layout/fragment_weather_setting_0".equals(obj)) {
                    return new af4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for fragment_weather_setting is invalid. Received: " + obj);
            case 127:
                if ("layout/item_active_time_day_0".equals(obj)) {
                    return new cf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_day is invalid. Received: " + obj);
            case Logger.DEFAULT_FULL_MESSAGE_LENGTH /*128*/:
                if ("layout/item_active_time_week_0".equals(obj)) {
                    return new ef4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_week is invalid. Received: " + obj);
            case 129:
                if ("layout/item_active_time_workout_day_0".equals(obj)) {
                    return new gf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_active_time_workout_day is invalid. Received: " + obj);
            case 130:
                if ("layout/item_activity_day_0".equals(obj)) {
                    return new if4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_day is invalid. Received: " + obj);
            case 131:
                if ("layout/item_activity_week_0".equals(obj)) {
                    return new kf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_week is invalid. Received: " + obj);
            case 132:
                if ("layout/item_activity_workout_day_0".equals(obj)) {
                    return new mf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_activity_workout_day is invalid. Received: " + obj);
            case 133:
                if ("layout/item_address_commute_time_0".equals(obj)) {
                    return new of4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_address_commute_time is invalid. Received: " + obj);
            case 134:
                if ("layout/item_alarm_0".equals(obj)) {
                    return new qf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_alarm is invalid. Received: " + obj);
            case 135:
                if ("layout/item_app_hybrid_notification_0".equals(obj)) {
                    return new sf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_app_hybrid_notification is invalid. Received: " + obj);
            case 136:
                if ("layout/item_app_notification_0".equals(obj)) {
                    return new uf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_app_notification is invalid. Received: " + obj);
            case 137:
                if ("layout/item_calories_day_0".equals(obj)) {
                    return new wf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_day is invalid. Received: " + obj);
            case 138:
                if ("layout/item_calories_week_0".equals(obj)) {
                    return new yf4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_week is invalid. Received: " + obj);
            case 139:
                if ("layout/item_calories_workout_day_0".equals(obj)) {
                    return new ag4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_calories_workout_day is invalid. Received: " + obj);
            case 140:
                if ("layout/item_contact_0".equals(obj)) {
                    return new cg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_contact is invalid. Received: " + obj);
            case 141:
                if ("layout/item_contact_hybrid_0".equals(obj)) {
                    return new eg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_contact_hybrid is invalid. Received: " + obj);
            case 142:
                if ("layout/item_default_place_commute_time_0".equals(obj)) {
                    return new gg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_default_place_commute_time is invalid. Received: " + obj);
            case 143:
                if ("layout/item_favorite_contact_notification_0".equals(obj)) {
                    return new ig4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_favorite_contact_notification is invalid. Received: " + obj);
            case 144:
                if ("layout/item_goal_tracking_day_0".equals(obj)) {
                    return new kg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_day is invalid. Received: " + obj);
            case 145:
                if ("layout/item_goal_tracking_week_0".equals(obj)) {
                    return new mg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_goal_tracking_week is invalid. Received: " + obj);
            case 146:
                if ("layout/item_heart_rate_day_0".equals(obj)) {
                    return new og4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_day is invalid. Received: " + obj);
            case 147:
                if ("layout/item_heart_rate_week_0".equals(obj)) {
                    return new qg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heart_rate_week is invalid. Received: " + obj);
            case 148:
                if ("layout/item_heartrate_workout_day_0".equals(obj)) {
                    return new sg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_heartrate_workout_day is invalid. Received: " + obj);
            case 149:
                if ("layout/item_notification_hybrid_0".equals(obj)) {
                    return new ug4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_notification_hybrid is invalid. Received: " + obj);
            case 150:
                if ("layout/item_permission_0".equals(obj)) {
                    return new wg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_permission is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public final ViewDataBinding d(jb jbVar, View view, int i, Object obj) {
        switch (i) {
            case 151:
                if ("layout/item_recorded_goal_tracking_0".equals(obj)) {
                    return new yg4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_recorded_goal_tracking is invalid. Received: " + obj);
            case 152:
                if ("layout/item_single_permission_0".equals(obj)) {
                    return new ah4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_single_permission is invalid. Received: " + obj);
            case 153:
                if ("layout/item_sleep_day_0".equals(obj)) {
                    return new ch4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_day is invalid. Received: " + obj);
            case 154:
                if ("layout/item_sleep_week_0".equals(obj)) {
                    return new eh4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_sleep_week is invalid. Received: " + obj);
            case 155:
                if ("layout/item_workout_0".equals(obj)) {
                    return new gh4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for item_workout is invalid. Received: " + obj);
            case 156:
                if ("layout/view_no_device_0".equals(obj)) {
                    return new ih4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for view_no_device is invalid. Received: " + obj);
            case 157:
                if ("layout/view_tab_custom_0".equals(obj)) {
                    return new kh4(jbVar, view);
                }
                throw new IllegalArgumentException("The tag for view_tab_custom is invalid. Received: " + obj);
            default:
                return null;
        }
    }

    @DexIgnore
    public ViewDataBinding a(jb jbVar, View view, int i) {
        int i2 = a.get(i);
        if (i2 <= 0) {
            return null;
        }
        Object tag = view.getTag();
        if (tag != null) {
            int i3 = (i2 - 1) / 50;
            if (i3 == 0) {
                return a(jbVar, view, i2, tag);
            }
            if (i3 == 1) {
                return b(jbVar, view, i2, tag);
            }
            if (i3 == 2) {
                return c(jbVar, view, i2, tag);
            }
            if (i3 != 3) {
                return null;
            }
            return d(jbVar, view, i2, tag);
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    public ViewDataBinding a(jb jbVar, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }

    @DexIgnore
    public List<hb> a() {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new ob());
        return arrayList;
    }
}
