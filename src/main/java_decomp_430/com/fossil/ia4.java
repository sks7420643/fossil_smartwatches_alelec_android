package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ia4 extends ha4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L; // = new SparseIntArray();
    @DexIgnore
    public long J;

    /*
    static {
        L.put(2131362082, 1);
        L.put(2131362542, 2);
        L.put(2131363218, 3);
        L.put(2131362056, 4);
        L.put(2131362543, 5);
        L.put(2131362321, 6);
        L.put(2131362320, 7);
        L.put(2131362653, 8);
        L.put(2131362026, 9);
        L.put(2131362468, 10);
        L.put(2131362417, 11);
        L.put(2131362416, 12);
        L.put(2131362939, 13);
        L.put(2131362389, 14);
        L.put(2131362388, 15);
        L.put(2131362397, 16);
        L.put(2131362609, 17);
        L.put(2131361889, 18);
        L.put(2131362022, 19);
        L.put(2131362149, 20);
        L.put(2131362665, 21);
        L.put(2131363264, 22);
        L.put(2131362399, 23);
        L.put(2131362863, 24);
    }
    */

    @DexIgnore
    public ia4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 25, K, L));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.J != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.J = 1;
        }
        g();
    }

    @DexIgnore
    public ia4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[18], objArr[19], objArr[9], objArr[4], objArr[1], objArr[20], objArr[7], objArr[6], objArr[15], objArr[14], objArr[16], objArr[23], objArr[12], objArr[11], objArr[10], objArr[2], objArr[5], objArr[17], objArr[8], objArr[21], objArr[0], objArr[24], objArr[13], objArr[3], objArr[22]);
        this.J = -1;
        this.H.setTag((Object) null);
        a(view);
        f();
    }
}
