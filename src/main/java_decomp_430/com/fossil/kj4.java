package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj4 extends nz implements Cloneable {
    @DexIgnore
    public kj4 J() {
        kj4.super.J();
        return this;
    }

    @DexIgnore
    public kj4 K() {
        return kj4.super.K();
    }

    @DexIgnore
    public kj4 L() {
        return kj4.super.L();
    }

    @DexIgnore
    public kj4 M() {
        return kj4.super.M();
    }

    @DexIgnore
    public kj4 c() {
        return kj4.super.c();
    }

    @DexIgnore
    public kj4 b(boolean z) {
        return kj4.super.b(z);
    }

    @DexIgnore
    public kj4 clone() {
        return kj4.super.clone();
    }

    @DexIgnore
    public kj4 b(int i) {
        return kj4.super.b(i);
    }

    @DexIgnore
    public kj4 a(float f) {
        return kj4.super.a(f);
    }

    @DexIgnore
    public kj4 a(ft ftVar) {
        return kj4.super.a(ftVar);
    }

    @DexIgnore
    public kj4 a(br brVar) {
        return kj4.super.a(brVar);
    }

    @DexIgnore
    public kj4 a(boolean z) {
        return kj4.super.a(z);
    }

    @DexIgnore
    public kj4 a(int i, int i2) {
        return kj4.super.a(i, i2);
    }

    @DexIgnore
    public kj4 a(vr vrVar) {
        return kj4.super.a(vrVar);
    }

    @DexIgnore
    public <Y> kj4 a(wr<Y> wrVar, Y y) {
        return kj4.super.a(wrVar, y);
    }

    @DexIgnore
    public kj4 a(Class<?> cls) {
        return kj4.super.a(cls);
    }

    @DexIgnore
    public kj4 a(ow owVar) {
        return kj4.super.a(owVar);
    }

    @DexIgnore
    public kj4 a(bs<Bitmap> bsVar) {
        return kj4.super.a(bsVar);
    }

    @DexIgnore
    public kj4 a(gz<?> gzVar) {
        return kj4.super.a(gzVar);
    }

    @DexIgnore
    public kj4 a() {
        return kj4.super.a();
    }
}
