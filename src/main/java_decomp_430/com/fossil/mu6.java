package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mu6 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map longOpts; // = new HashMap();
    @DexIgnore
    public Map optionGroups; // = new HashMap();
    @DexIgnore
    public List requiredOpts; // = new ArrayList();
    @DexIgnore
    public Map shortOpts; // = new HashMap();

    @DexIgnore
    public mu6 addOption(String str, boolean z, String str2) {
        addOption(str, (String) null, z, str2);
        return this;
    }

    @DexIgnore
    public mu6 addOptionGroup(ku6 ku6) {
        if (ku6.isRequired()) {
            this.requiredOpts.add(ku6);
        }
        for (ju6 ju6 : ku6.getOptions()) {
            ju6.setRequired(false);
            addOption(ju6);
            this.optionGroups.put(ju6.getKey(), ku6);
        }
        return this;
    }

    @DexIgnore
    public ju6 getOption(String str) {
        String b = su6.b(str);
        if (this.shortOpts.containsKey(b)) {
            return (ju6) this.shortOpts.get(b);
        }
        return (ju6) this.longOpts.get(b);
    }

    @DexIgnore
    public ku6 getOptionGroup(ju6 ju6) {
        return (ku6) this.optionGroups.get(ju6.getKey());
    }

    @DexIgnore
    public Collection getOptionGroups() {
        return new HashSet(this.optionGroups.values());
    }

    @DexIgnore
    public Collection getOptions() {
        return Collections.unmodifiableCollection(helpOptions());
    }

    @DexIgnore
    public List getRequiredOptions() {
        return this.requiredOpts;
    }

    @DexIgnore
    public boolean hasOption(String str) {
        String b = su6.b(str);
        return this.shortOpts.containsKey(b) || this.longOpts.containsKey(b);
    }

    @DexIgnore
    public List helpOptions() {
        return new ArrayList(this.shortOpts.values());
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[ Options: [ short ");
        stringBuffer.append(this.shortOpts.toString());
        stringBuffer.append(" ] [ long ");
        stringBuffer.append(this.longOpts);
        stringBuffer.append(" ]");
        return stringBuffer.toString();
    }

    @DexIgnore
    public mu6 addOption(String str, String str2, boolean z, String str3) {
        addOption(new ju6(str, str2, z, str3));
        return this;
    }

    @DexIgnore
    public mu6 addOption(ju6 ju6) {
        String key = ju6.getKey();
        if (ju6.hasLongOpt()) {
            this.longOpts.put(ju6.getLongOpt(), ju6);
        }
        if (ju6.isRequired()) {
            if (this.requiredOpts.contains(key)) {
                List list = this.requiredOpts;
                list.remove(list.indexOf(key));
            }
            this.requiredOpts.add(key);
        }
        this.shortOpts.put(key, ju6);
        return this;
    }
}
