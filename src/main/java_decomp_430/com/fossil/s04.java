package com.fossil;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.fossil.wv1;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationRequest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s04 implements wv1.b, wv1.c, zv2 {
    @DexIgnore
    public static /* final */ String h; // = "s04";
    @DexIgnore
    public static s04 i;
    @DexIgnore
    public wv1 a;
    @DexIgnore
    public CopyOnWriteArrayList<b> b; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Context c;
    @DexIgnore
    public Timer d;
    @DexIgnore
    public TimerTask e;
    @DexIgnore
    public double f;
    @DexIgnore
    public double g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TimerTask {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            SecureRandom secureRandom = new SecureRandom();
            s04 s04 = s04.this;
            double unused = s04.f = s04.f + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            s04 s042 = s04.this;
            double unused2 = s042.g = s042.g + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Location location = new Location("");
            location.setLatitude(s04.this.f + 10.7604877d);
            location.setLongitude(s04.this.g + 106.698541d);
            Iterator it = s04.this.b.iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(location, 1);
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Location location, int i);
    }

    @DexIgnore
    public final void d() {
        Timer timer = this.d;
        if (timer != null) {
            timer.cancel();
        }
        TimerTask timerTask = this.e;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @DexIgnore
    public void f(Bundle bundle) {
        Log.i(h, "MFLocationService is connected");
        c();
    }

    @DexIgnore
    public void g(int i2) {
        String str = h;
        Log.i(str, "MFLocationService is suspended - i=" + i2);
    }

    @DexIgnore
    public void onLocationChanged(Location location) {
        String str = h;
        Log.d(str, "Inside " + h + ".onLocationUpdated - location=" + location);
        CopyOnWriteArrayList<b> copyOnWriteArrayList = this.b;
        if (copyOnWriteArrayList != null) {
            Iterator<b> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().a(location, 1);
            }
        }
    }

    @DexIgnore
    public final void c() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.j(0);
        locationRequest.k(1000);
        locationRequest.d(100);
        aw2.d.a(this.a, locationRequest, this);
    }

    @DexIgnore
    public static synchronized s04 a(Context context) {
        s04 s04;
        synchronized (s04.class) {
            if (i == null) {
                i = new s04();
            }
            i.c = context.getApplicationContext();
            s04 = i;
        }
        return s04;
    }

    @DexIgnore
    public final void b() {
        d();
        this.d = new Timer();
        this.e = new a();
        this.d.schedule(this.e, 0, 1000);
    }

    @DexIgnore
    public void a(b bVar) {
        if (!r04.a(this.c)) {
            bVar.a((Location) null, -1);
            return;
        }
        String str = h;
        Log.i(str, "Register Location Service - callback=" + bVar + ", size=" + this.b.size());
        this.b.add(bVar);
        if (q04.a(this.c, "fake").equals("true")) {
            b();
            return;
        }
        if (this.a == null) {
            wv1.a aVar = new wv1.a(this.c);
            aVar.a(aw2.c);
            aVar.a(this);
            aVar.a(this);
            this.a = aVar.a();
        }
        int a2 = a();
        if (a2 != 0) {
            bVar.a((Location) null, a2);
        } else {
            this.a.c();
        }
    }

    @DexIgnore
    public void b(b bVar) {
        this.b.remove(bVar);
        String str = h;
        Log.i(str, "Unregister Location Service - callback=" + bVar + ", size=" + this.b.size());
        if (q04.a(this.c, "fake").equals("true")) {
            d();
            return;
        }
        wv1 wv1 = this.a;
        if (wv1 != null && wv1.g()) {
            aw2.d.a(this.a, this);
            this.a.d();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0026 A[ADDED_TO_REGION] */
    public int a() {
        boolean z;
        boolean z2;
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.c) != 0) {
            return -2;
        }
        LocationManager locationManager = (LocationManager) this.c.getSystemService("location");
        try {
            z = locationManager.isProviderEnabled("gps");
            try {
                z2 = locationManager.isProviderEnabled("network");
            } catch (Exception unused) {
                z2 = false;
                if (!z) {
                }
                return 0;
            }
        } catch (Exception unused2) {
            z = false;
            z2 = false;
            if (!z) {
            }
            return 0;
        }
        if (!z || z2) {
            return 0;
        }
        return -1;
    }

    @DexIgnore
    public void a(gv1 gv1) {
        Log.e(h, "MFLocationService is failed to connect");
    }
}
