package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oi2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle f;
    @DexIgnore
    public /* final */ /* synthetic */ ov2.c g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oi2(ov2.c cVar, Activity activity, Bundle bundle) {
        super(ov2.this);
        this.g = cVar;
        this.e = activity;
        this.f = bundle;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        ov2.this.g.onActivityCreated(z52.a(this.e), this.f, this.b);
    }
}
