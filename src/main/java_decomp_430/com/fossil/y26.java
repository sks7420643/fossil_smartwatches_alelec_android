package com.fossil;

import android.os.Bundle;
import com.fossil.z26;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y26 extends m26 {
    @DexIgnore
    public z26 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public y26(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = z26.a.a(bundle);
    }

    @DexIgnore
    public boolean a() {
        z26 z26 = this.c;
        if (z26 == null) {
            return false;
        }
        return z26.a();
    }

    @DexIgnore
    public int b() {
        return 4;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        Bundle a = z26.a.a(this.c);
        super.b(a);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(a);
    }
}
