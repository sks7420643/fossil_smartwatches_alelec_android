package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kb4 extends jb4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G; // = new SparseIntArray();
    @DexIgnore
    public long E;

    /*
    static {
        G.put(2131363193, 1);
        G.put(2131363105, 2);
        G.put(2131362426, 3);
        G.put(2131362025, 4);
        G.put(2131362659, 5);
        G.put(2131362660, 6);
        G.put(2131362658, 7);
        G.put(2131362640, 8);
        G.put(2131362001, 9);
        G.put(2131362467, 10);
        G.put(2131362051, 11);
        G.put(2131363330, 12);
        G.put(2131363231, 13);
        G.put(2131363329, 14);
        G.put(2131363230, 15);
        G.put(2131363328, 16);
        G.put(2131363229, 17);
        G.put(2131363258, 18);
        G.put(2131362137, 19);
        G.put(2131362885, 20);
    }
    */

    @DexIgnore
    public kb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 21, F, G));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.E = 1;
        }
        g();
    }

    @DexIgnore
    public kb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[9], objArr[4], objArr[11], objArr[19], objArr[3], objArr[10], objArr[8], objArr[7], objArr[5], objArr[6], objArr[0], objArr[20], objArr[2], objArr[1], objArr[17], objArr[15], objArr[13], objArr[18], objArr[16], objArr[14], objArr[12]);
        this.E = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
