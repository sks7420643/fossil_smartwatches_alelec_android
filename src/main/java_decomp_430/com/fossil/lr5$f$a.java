package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1", f = "ProfileSetupPresenter.kt", l = {558}, m = "invokeSuspend")
public final class lr5$f$a extends sf6 implements ig6<il6, xe6<? super ap4<MFUser>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lr5$f$a(ProfileSetupPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        lr5$f$a lr5_f_a = new lr5$f$a(this.this$0, xe6);
        lr5_f_a.p$ = (il6) obj;
        return lr5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((lr5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            UserRepository e = this.this$0.this$0.z;
            MFUser mFUser = this.this$0.$user;
            this.L$0 = il6;
            this.label = 1;
            obj = e.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
