package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i15 implements Factory<h15> {
    @DexIgnore
    public static HomeAlertsHybridPresenter a(g15 g15, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, an4 an4) {
        return new HomeAlertsHybridPresenter(g15, alarmHelper, setAlarms, alarmsRepository, an4);
    }
}
