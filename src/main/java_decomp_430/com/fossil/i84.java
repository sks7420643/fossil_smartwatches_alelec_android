package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i84 extends h84 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView D;
    @DexIgnore
    public long E;

    /*
    static {
        G.put(2131362016, 1);
        G.put(2131363100, 2);
        G.put(2131363289, 3);
        G.put(2131362574, 4);
        G.put(2131362015, 5);
        G.put(2131363099, 6);
        G.put(2131363288, 7);
        G.put(2131362573, 8);
        G.put(2131362050, 9);
        G.put(2131363178, 10);
        G.put(2131363295, 11);
        G.put(2131362581, 12);
        G.put(2131362076, 13);
        G.put(2131363214, 14);
        G.put(2131363321, 15);
        G.put(2131362586, 16);
        G.put(2131362089, 17);
        G.put(2131362858, 18);
        G.put(2131362857, 19);
        G.put(2131362859, 20);
        G.put(2131362860, 21);
        G.put(2131362206, 22);
    }
    */

    @DexIgnore
    public i84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 23, F, G));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.E = 1;
        }
        g();
    }

    @DexIgnore
    public i84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[1], objArr[9], objArr[13], objArr[17], objArr[22], objArr[8], objArr[4], objArr[12], objArr[16], objArr[19], objArr[18], objArr[20], objArr[21], objArr[6], objArr[2], objArr[10], objArr[14], objArr[7], objArr[3], objArr[11], objArr[15]);
        this.E = -1;
        this.D = objArr[0];
        this.D.setTag((Object) null);
        a(view);
        f();
    }
}
