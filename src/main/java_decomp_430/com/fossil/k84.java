package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k84 extends j84 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362012, 1);
        B.put(2131363092, 2);
        B.put(2131363285, 3);
        B.put(2131362571, 4);
        B.put(2131362045, 5);
        B.put(2131363167, 6);
        B.put(2131363293, 7);
        B.put(2131362579, 8);
        B.put(2131362032, 9);
        B.put(2131363122, 10);
        B.put(2131363292, 11);
        B.put(2131362578, 12);
        B.put(2131362949, 13);
        B.put(2131362206, 14);
    }
    */

    @DexIgnore
    public k84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 15, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public k84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[9], objArr[5], objArr[14], objArr[4], objArr[12], objArr[8], objArr[13], objArr[2], objArr[10], objArr[6], objArr[3], objArr[11], objArr[7]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
