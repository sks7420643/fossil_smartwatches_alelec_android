package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.internal.FacebookWebFallbackDialog;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mj3 {
    @DexIgnore
    public static mj3 e;
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());
    @DexIgnore
    public c c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Handler.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            mj3.this.a((c) message.obj);
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);

        @DexIgnore
        void d();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ WeakReference<b> a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c(int i, b bVar) {
            this.a = new WeakReference<>(bVar);
            this.b = i;
        }

        @DexIgnore
        public boolean a(b bVar) {
            return bVar != null && this.a.get() == bVar;
        }
    }

    @DexIgnore
    public static mj3 b() {
        if (e == null) {
            e = new mj3();
        }
        return e;
    }

    @DexIgnore
    public void a(int i, b bVar) {
        synchronized (this.a) {
            if (b(bVar)) {
                this.c.b = i;
                this.b.removeCallbacksAndMessages(this.c);
                b(this.c);
                return;
            }
            if (c(bVar)) {
                this.d.b = i;
            } else {
                this.d = new c(i, bVar);
            }
            if (this.c == null || !a(this.c, 4)) {
                this.c = null;
                a();
            }
        }
    }

    @DexIgnore
    public final boolean c(b bVar) {
        c cVar = this.d;
        return cVar != null && cVar.a(bVar);
    }

    @DexIgnore
    public void d(b bVar) {
        synchronized (this.a) {
            if (b(bVar)) {
                this.c = null;
                if (this.d != null) {
                    a();
                }
            }
        }
    }

    @DexIgnore
    public void e(b bVar) {
        synchronized (this.a) {
            if (b(bVar)) {
                b(this.c);
            }
        }
    }

    @DexIgnore
    public void f(b bVar) {
        synchronized (this.a) {
            if (b(bVar) && !this.c.c) {
                this.c.c = true;
                this.b.removeCallbacksAndMessages(this.c);
            }
        }
    }

    @DexIgnore
    public void g(b bVar) {
        synchronized (this.a) {
            if (b(bVar) && this.c.c) {
                this.c.c = false;
                b(this.c);
            }
        }
    }

    @DexIgnore
    public final boolean b(b bVar) {
        c cVar = this.c;
        return cVar != null && cVar.a(bVar);
    }

    @DexIgnore
    public final void b(c cVar) {
        int i = cVar.b;
        if (i != -2) {
            if (i <= 0) {
                i = i == -1 ? FacebookWebFallbackDialog.OS_BACK_BUTTON_RESPONSE_TIMEOUT_MILLISECONDS : 2750;
            }
            this.b.removeCallbacksAndMessages(cVar);
            Handler handler = this.b;
            handler.sendMessageDelayed(Message.obtain(handler, 0, cVar), (long) i);
        }
    }

    @DexIgnore
    public void a(b bVar, int i) {
        synchronized (this.a) {
            if (b(bVar)) {
                a(this.c, i);
            } else if (c(bVar)) {
                a(this.d, i);
            }
        }
    }

    @DexIgnore
    public boolean a(b bVar) {
        boolean z;
        synchronized (this.a) {
            if (!b(bVar)) {
                if (!c(bVar)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final void a() {
        c cVar = this.d;
        if (cVar != null) {
            this.c = cVar;
            this.d = null;
            b bVar = (b) this.c.a.get();
            if (bVar != null) {
                bVar.d();
            } else {
                this.c = null;
            }
        }
    }

    @DexIgnore
    public final boolean a(c cVar, int i) {
        b bVar = (b) cVar.a.get();
        if (bVar == null) {
            return false;
        }
        this.b.removeCallbacksAndMessages(cVar);
        bVar.a(i);
        return true;
    }

    @DexIgnore
    public void a(c cVar) {
        synchronized (this.a) {
            if (this.c == cVar || this.d == cVar) {
                a(cVar, 2);
            }
        }
    }
}
