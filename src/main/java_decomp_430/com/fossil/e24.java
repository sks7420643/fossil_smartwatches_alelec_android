package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e24 implements Factory<el4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public e24(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static e24 a(b14 b14) {
        return new e24(b14);
    }

    @DexIgnore
    public static el4 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static el4 c(b14 b14) {
        el4 p = b14.p();
        z76.a(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    public el4 get() {
        return b(this.a);
    }
}
