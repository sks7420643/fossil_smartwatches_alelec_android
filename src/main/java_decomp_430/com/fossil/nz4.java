package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nz4 implements Factory<mz4> {
    @DexIgnore
    public static /* final */ nz4 a; // = new nz4();

    @DexIgnore
    public static nz4 a() {
        return a;
    }

    @DexIgnore
    public static mz4 b() {
        return new mz4();
    }

    @DexIgnore
    public mz4 get() {
        return b();
    }
}
