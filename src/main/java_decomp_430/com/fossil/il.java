package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il {
    @DexIgnore
    public /* final */ kl a;

    @DexIgnore
    public il(ViewPager2 viewPager2, kl klVar, RecyclerView recyclerView) {
        this.a = klVar;
    }

    @DexIgnore
    public boolean a() {
        return this.a.d();
    }
}
