package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k34 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ ArrayList<Explore> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleTextView a;
        @DexIgnore
        public ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(k34 k34, View view) {
            super(view);
            wg6.b(view, "view");
            this.a = (FlexibleTextView) view.findViewById(2131362312);
            this.b = (ImageView) view.findViewById(2131362602);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(Explore explore, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareTutorialAdapter", "build - position=" + i);
            if (explore != null) {
                Object r5 = this.a;
                wg6.a((Object) r5, "ftvContent");
                r5.setText(explore.getDescription());
                ImageView imageView = this.b;
                wg6.a((Object) imageView, "ivIcon");
                imageView.setBackground(w6.c(PortfolioApp.get.instance(), explore.getBackground()));
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public k34(ArrayList<Explore> arrayList) {
        wg6.b(arrayList, "mData");
        this.a = arrayList;
    }

    @DexIgnore
    public final void a(List<? extends Explore> list) {
        wg6.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        if (getItemCount() > i && i != -1) {
            ((b) viewHolder).a(this.a.get(i), i);
        }
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558663, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026_tutorial, parent, false)");
        return new b(this, inflate);
    }
}
