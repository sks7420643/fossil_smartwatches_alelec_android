package com.fossil;

import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dm3<K, V> extends im3<Map.Entry<K, V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ bm3<K, V> map;

        @DexIgnore
        public a(bm3<K, V> bm3) {
            this.map = bm3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends dm3<K, V> {
        @DexIgnore
        public /* final */ transient bm3<K, V> b;
        @DexIgnore
        public /* final */ transient Map.Entry<K, V>[] c;

        @DexIgnore
        public b(bm3<K, V> bm3, Map.Entry<K, V>[] entryArr) {
            this.b = bm3;
            this.c = entryArr;
        }

        @DexIgnore
        public zl3<Map.Entry<K, V>> createAsList() {
            return new nn3(this, (Object[]) this.c);
        }

        @DexIgnore
        public bm3<K, V> map() {
            return this.b;
        }

        @DexIgnore
        public jo3<Map.Entry<K, V>> iterator() {
            return qm3.a((T[]) this.c);
        }
    }

    @DexIgnore
    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = map().get(entry.getKey());
        if (obj2 == null || !obj2.equals(entry.getValue())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return map().hashCode();
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return map().isHashCodeFast();
    }

    @DexIgnore
    public boolean isPartialView() {
        return map().isPartialView();
    }

    @DexIgnore
    public abstract bm3<K, V> map();

    @DexIgnore
    public int size() {
        return map().size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new a(map());
    }
}
