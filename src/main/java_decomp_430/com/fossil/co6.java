package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class co6<T> extends po6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(co6.class, Object.class, "_consensus");
    @DexIgnore
    public volatile Object _consensus; // = bo6.a;

    @DexIgnore
    public final Object a(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == bo6.a) {
            obj2 = b(c(obj));
        }
        a(obj, obj2);
        return obj2;
    }

    @DexIgnore
    public abstract void a(T t, Object obj);

    @DexIgnore
    public final Object b(Object obj) {
        return d(obj) ? obj : this._consensus;
    }

    @DexIgnore
    public abstract Object c(T t);

    @DexIgnore
    public final boolean d(Object obj) {
        if (nl6.a()) {
            if (!(obj != bo6.a)) {
                throw new AssertionError();
            }
        }
        return a.compareAndSet(this, bo6.a, obj);
    }
}
