package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an6<T> extends sl6<T> {
    @DexIgnore
    public ig6<? super il6, ? super xe6<? super T>, ? extends Object> d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public an6(af6 af6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6) {
        super(af6, false);
        wg6.b(af6, "parentContext");
        wg6.b(ig6, "block");
        this.d = ig6;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Object, com.fossil.an6, com.fossil.xe6] */
    public void o() {
        ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6 = this.d;
        if (ig6 != null) {
            this.d = null;
            cp6.a(ig6, this, this);
            return;
        }
        throw new IllegalStateException("Already started".toString());
    }
}
