package com.fossil;

import android.os.Handler;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mp implements xp {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ /* synthetic */ Handler a;

        @DexIgnore
        public a(mp mpVar, Handler handler) {
            this.a = handler;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ up a;
        @DexIgnore
        public /* final */ wp b;
        @DexIgnore
        public /* final */ Runnable c;

        @DexIgnore
        public b(up upVar, wp wpVar, Runnable runnable) {
            this.a = upVar;
            this.b = wpVar;
            this.c = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.a.isCanceled()) {
                this.a.finish("canceled-at-delivery");
                return;
            }
            if (this.b.a()) {
                this.a.deliverResponse(this.b.a);
            } else {
                this.a.deliverError(this.b.c);
            }
            if (this.b.d) {
                this.a.addMarker("intermediate-response");
            } else {
                this.a.finish("done");
            }
            Runnable runnable = this.c;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public mp(Handler handler) {
        this.a = new a(this, handler);
    }

    @DexIgnore
    public void a(up<?> upVar, wp<?> wpVar) {
        a(upVar, wpVar, (Runnable) null);
    }

    @DexIgnore
    public void a(up<?> upVar, wp<?> wpVar, Runnable runnable) {
        upVar.markDelivered();
        upVar.addMarker("post-response");
        this.a.execute(new b(upVar, wpVar, runnable));
    }

    @DexIgnore
    public void a(up<?> upVar, bq bqVar) {
        upVar.addMarker("post-error");
        this.a.execute(new b(upVar, wp.a(bqVar), (Runnable) null));
    }
}
