package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xb3> CREATOR; // = new wb3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ gv1 b;
    @DexIgnore
    public /* final */ y12 c;

    @DexIgnore
    public xb3(int i, gv1 gv1, y12 y12) {
        this.a = i;
        this.b = gv1;
        this.c = y12;
    }

    @DexIgnore
    public final y12 B() {
        return this.c;
    }

    @DexIgnore
    public final gv1 p() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, (Parcelable) this.b, i, false);
        g22.a(parcel, 3, (Parcelable) this.c, i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public xb3(int i) {
        this(new gv1(8, (PendingIntent) null), (y12) null);
    }

    @DexIgnore
    public xb3(gv1 gv1, y12 y12) {
        this(1, gv1, (y12) null);
    }
}
