package com.fossil;

import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.util.DeviceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
public final class br5$g$a extends sf6 implements ig6<il6, xe6<? super lc6<? extends String, ? extends String>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public br5$g$a(UpdateFirmwarePresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        br5$g$a br5_g_a = new br5$g$a(this.this$0, xe6);
        br5_g_a.p$ = (il6) obj;
        return br5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((br5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return DeviceUtils.g.a().a(this.this$0.$activeSerial, (Device) null);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
