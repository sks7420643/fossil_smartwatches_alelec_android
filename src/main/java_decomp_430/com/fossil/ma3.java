package com.fossil;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.Utility;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma3 extends s63 {
    @DexIgnore
    public static /* final */ String[] g; // = {"firebase_", "google_", "ga_"};
    @DexIgnore
    public SecureRandom c;
    @DexIgnore
    public /* final */ AtomicLong d; // = new AtomicLong(0);
    @DexIgnore
    public int e;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public ma3(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public static boolean d(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    @DexIgnore
    public static boolean e(String str) {
        w12.b(str);
        if (str.charAt(0) != '_' || str.equals("_ep")) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean f(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    @DexIgnore
    public static boolean g(String str) {
        w12.a(str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    @DexIgnore
    public static int h(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        return "_id".equals(str) ? 256 : 36;
    }

    @DexIgnore
    public static MessageDigest x() {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final Bundle a(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString("source", str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString("medium", str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e2) {
            b().w().a("Install referrer url isn't a hierarchical URI", e2);
            return null;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        if (str2 == null) {
            b().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            b().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        b().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            b().v().a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    public final boolean c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (g(str)) {
                return true;
            }
            if (this.a.z()) {
                b().v().a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", t43.a(str));
            }
            return false;
        } else if (TextUtils.isEmpty(str2)) {
            if (this.a.z()) {
                b().v().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            }
            return false;
        } else if (g(str2)) {
            return true;
        } else {
            b().v().a("Invalid admob_app_id. Analytics disabled.", t43.a(str2));
            return false;
        }
    }

    @DexIgnore
    public final void m() {
        g();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                b().w().a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    @DexIgnore
    public final boolean q() {
        return true;
    }

    @DexIgnore
    public final long s() {
        long andIncrement;
        long j;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                long nextLong = new Random(System.nanoTime() ^ zzm().b()).nextLong();
                int i = this.e + 1;
                this.e = i;
                j = nextLong + ((long) i);
            }
            return j;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1, 1);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    @DexIgnore
    public final SecureRandom t() {
        g();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    @DexIgnore
    public final int u() {
        if (this.f == null) {
            this.f = Integer.valueOf(kv1.a().b(c()) / 1000);
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final String v() {
        byte[] bArr = new byte[16];
        t().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    @DexIgnore
    public final boolean w() {
        try {
            c().getClassLoader().loadClass("com.google.firebase.remoteconfig.FirebaseRemoteConfig");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String s = l().s();
        d();
        return s.equals(str);
    }

    @DexIgnore
    public final int b(String str) {
        if (!b("user property", str)) {
            return 6;
        }
        if (!a("user property", z63.a, str)) {
            return 15;
        }
        if (!a("user property", 24, str)) {
            return 6;
        }
        return 0;
    }

    @DexIgnore
    public final Object c(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return a(h(str), obj, true);
        }
        return a(h(str), obj, false);
    }

    @DexIgnore
    public static boolean c(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null || !serviceInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final int b(String str, Object obj) {
        boolean z;
        if ("_ldl".equals(str)) {
            z = a("user property referrer", str, h(str), obj, false);
        } else {
            z = a("user property", str, h(str), obj, false);
        }
        return z ? 0 : 7;
    }

    @DexIgnore
    public final boolean c(String str) {
        g();
        if (g52.b(c()).a(str) == 0) {
            return true;
        }
        b().A().a("Permission not granted", str);
        return false;
    }

    @DexIgnore
    public final boolean b(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo b = g52.b(context).b(str, 64);
            if (b == null || b.signatures == null || b.signatures.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(b.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (CertificateException e2) {
            b().t().a("Error obtaining certificate", e2);
            return true;
        } catch (PackageManager.NameNotFoundException e3) {
            b().t().a("Package name not found", e3);
            return true;
        }
    }

    @DexIgnore
    public static boolean a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    @DexIgnore
    public static Bundle b(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String str : bundle2.keySet()) {
            Object obj = bundle2.get(str);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        if (parcelableArr[i] instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        if (str2 == null) {
            b().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            b().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                b().v().a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    b().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean a(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            b().v().a("Name is required and can't be null. Type", str);
            return false;
        }
        w12.a(str2);
        String[] strArr2 = g;
        int length = strArr2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            b().v().a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            w12.a(strArr);
            int length2 = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    z2 = false;
                    break;
                } else if (d(str2, strArr[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                b().v().a("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static ArrayList<Bundle> b(List<ab3> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (ab3 next : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", next.a);
            bundle.putString("origin", next.b);
            bundle.putLong("creation_timestamp", next.d);
            bundle.putString("name", next.c.b);
            u63.a(bundle, next.c.zza());
            bundle.putBoolean("active", next.e);
            String str = next.f;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            j03 j03 = next.g;
            if (j03 != null) {
                bundle.putString("timed_out_event_name", j03.a);
                i03 i03 = next.g.b;
                if (i03 != null) {
                    bundle.putBundle("timed_out_event_params", i03.zzb());
                }
            }
            bundle.putLong("trigger_timeout", next.h);
            j03 j032 = next.i;
            if (j032 != null) {
                bundle.putString("triggered_event_name", j032.a);
                i03 i032 = next.i.b;
                if (i032 != null) {
                    bundle.putBundle("triggered_event_params", i032.zzb());
                }
            }
            bundle.putLong("triggered_timestamp", next.c.c);
            bundle.putLong("time_to_live", next.j);
            j03 j033 = next.o;
            if (j033 != null) {
                bundle.putString("expired_event_name", j033.a);
                i03 i033 = next.o.b;
                if (i033 != null) {
                    bundle.putBundle("expired_event_params", i033.zzb());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean a(String str, int i, String str2) {
        if (str2 == null) {
            b().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            b().v().a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    @DexIgnore
    public final int a(String str) {
        if (!b("event", str)) {
            return 2;
        }
        if (!a("event", x63.a, str)) {
            return 13;
        }
        if (!a("event", 40, str)) {
            return 2;
        }
        return 0;
    }

    @DexIgnore
    public final boolean a(String str, String str2, int i, Object obj, boolean z) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                String valueOf = String.valueOf(obj);
                if (valueOf.codePointCount(0, valueOf.length()) > i) {
                    b().y().a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                    return false;
                }
            } else if ((obj instanceof Bundle) && z) {
                return true;
            } else {
                if ((obj instanceof Parcelable[]) && z) {
                    for (Parcelable parcelable : (Parcelable[]) obj) {
                        if (!(parcelable instanceof Bundle)) {
                            b().y().a("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof ArrayList) || !z) {
                    return false;
                } else {
                    ArrayList arrayList = (ArrayList) obj;
                    int size = arrayList.size();
                    int i2 = 0;
                    while (i2 < size) {
                        Object obj2 = arrayList.get(i2);
                        i2++;
                        if (!(obj2 instanceof Bundle)) {
                            b().y().a("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean a(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    @DexIgnore
    public static Object a(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return a(String.valueOf(obj), i, z);
            }
            return null;
        }
    }

    @DexIgnore
    public static String a(String str, int i, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    @DexIgnore
    public final Object a(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return a(256, obj, true);
        }
        if (!f(str)) {
            i = 100;
        }
        return a(i, obj, false);
    }

    @DexIgnore
    public static Bundle[] a(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            return (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
        } else if (!(obj instanceof ArrayList)) {
            return null;
        } else {
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        if (a("event param", 40, r15) == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007e, code lost:
        if (a("event param", 40, r15) == false) goto L_0x0071;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0141  */
    public final Bundle a(String str, String str2, Bundle bundle, List<String> list, boolean z, boolean z2) {
        Set<String> set;
        int i;
        String str3;
        int i2;
        int i3;
        boolean z3;
        boolean z4;
        int i4;
        int i5;
        String str4 = str;
        Bundle bundle2 = bundle;
        List<String> list2 = list;
        String[] strArr = null;
        if (bundle2 == null) {
            return null;
        }
        Bundle bundle3 = new Bundle(bundle2);
        if (l().e(str4, l03.s0)) {
            set = new TreeSet<>(bundle.keySet());
        } else {
            set = bundle.keySet();
        }
        int i6 = 0;
        for (String str5 : set) {
            if (list2 == null || !list2.contains(str5)) {
                i = 14;
                if (z) {
                    if (a("event param", str5)) {
                        if (!a("event param", strArr, str5)) {
                            i5 = 14;
                            if (i5 == 0) {
                                if (b("event param", str5)) {
                                    if (a("event param", strArr, str5)) {
                                    }
                                }
                                i = 3;
                            } else {
                                i = i5;
                            }
                            if (i == 0) {
                                if (a(bundle3, i)) {
                                    bundle3.putString("_ev", a(str5, 40, true));
                                    if (i == 3) {
                                        a(bundle3, (Object) str5);
                                    }
                                }
                                bundle3.remove(str5);
                            } else {
                                Object obj = bundle2.get(str5);
                                g();
                                if (z2) {
                                    if (obj instanceof Parcelable[]) {
                                        i4 = ((Parcelable[]) obj).length;
                                    } else {
                                        if (obj instanceof ArrayList) {
                                            i4 = ((ArrayList) obj).size();
                                        }
                                        z4 = true;
                                        if (!z4) {
                                            i3 = 17;
                                            str3 = "_ev";
                                            i2 = 40;
                                            if (i3 == 0 && str3.equals(str5)) {
                                                if (a(bundle3, i3)) {
                                                    bundle3.putString(str3, a(str5, i2, true));
                                                    a(bundle3, bundle2.get(str5));
                                                }
                                                bundle3.remove(str5);
                                            } else if (!e(str5) || (i6 = i6 + 1) <= 25) {
                                                String str6 = str2;
                                            } else {
                                                StringBuilder sb = new StringBuilder(48);
                                                sb.append("Event can't contain more than 25 params");
                                                b().v().a(sb.toString(), i().a(str2), i().a(bundle2));
                                                a(bundle3, 5);
                                                bundle3.remove(str5);
                                            }
                                        }
                                    }
                                    if (i4 > 1000) {
                                        b().y().a("Parameter array is too long; discarded. Value kind, name, array length", "param", str5, Integer.valueOf(i4));
                                        z4 = false;
                                        if (!z4) {
                                        }
                                    }
                                    z4 = true;
                                    if (!z4) {
                                    }
                                }
                                if ((!l().f(str4) || !f(str2)) && !f(str5)) {
                                    str3 = "_ev";
                                    i2 = 40;
                                    z3 = a("param", str5, 100, obj, z2);
                                } else {
                                    str3 = "_ev";
                                    i2 = 40;
                                    z3 = a("param", str5, 256, obj, z2);
                                }
                                i3 = z3 ? 0 : 4;
                                if (i3 == 0 && str3.equals(str5)) {
                                }
                            }
                            strArr = null;
                        }
                    }
                    i5 = 3;
                    if (i5 == 0) {
                    }
                    if (i == 0) {
                    }
                    strArr = null;
                }
                i5 = 0;
                if (i5 == 0) {
                }
                if (i == 0) {
                }
                strArr = null;
            }
            i = 0;
            if (i == 0) {
            }
            strArr = null;
        }
        return bundle3;
    }

    @DexIgnore
    public static boolean a(Bundle bundle, int i) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    @DexIgnore
    public static void a(Bundle bundle, Object obj) {
        w12.a(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                b().y().a("Not putting event parameter. Invalid value type. name, type", i().b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    @DexIgnore
    public final void a(int i, String str, String str2, int i2) {
        a((String) null, i, str, str2, i2);
    }

    @DexIgnore
    public final void a(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        a(bundle, i);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.a.d();
        this.a.v().a("auto", "_err", bundle);
    }

    @DexIgnore
    public static long a(byte[] bArr) {
        w12.a(bArr);
        int i = 0;
        w12.b(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    @DexIgnore
    public static boolean a(Context context, boolean z) {
        w12.a(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return c(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return c(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    @DexIgnore
    public static boolean a(Boolean bool, Boolean bool2) {
        if (bool == null && bool2 == null) {
            return true;
        }
        if (bool == null) {
            return false;
        }
        return bool.equals(bool2);
    }

    @DexIgnore
    public static boolean a(List<String> list, List<String> list2) {
        if (list == null && list2 == null) {
            return true;
        }
        if (list == null) {
            return false;
        }
        return list.equals(list2);
    }

    @DexIgnore
    public final Bundle a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object a = a(str, bundle.get(str));
                if (a == null) {
                    b().y().a("Param value can't be null", i().b(str));
                } else {
                    a(bundle2, str, a);
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final j03 a(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (a(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            Bundle bundle3 = bundle2;
            bundle3.putString("_o", str3);
            return new j03(str2, new i03(a(a(str, str2, bundle3, l42.a("_o"), false, false))), str3, j);
        }
        b().t().a("Invalid conditional property event name", i().c(str2));
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final long a(Context context, String str) {
        g();
        w12.a(context);
        w12.b(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest x = x();
        if (x == null) {
            b().t().a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!b(context, str)) {
                    PackageInfo b = g52.b(context).b(c().getPackageName(), 64);
                    if (b.signatures != null && b.signatures.length > 0) {
                        return a(x.digest(b.signatures[0].toByteArray()));
                    }
                    b().w().a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                b().t().a("Package name not found", e2);
            }
        }
        return 0;
    }

    @DexIgnore
    public static byte[] a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public final int a(int i) {
        return kv1.a().a(c(), (int) nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }

    @DexIgnore
    public static long a(long j, long j2) {
        return (j + (j2 * 60000)) / 86400000;
    }

    @DexIgnore
    public final void a(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            b().w().a("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }

    @DexIgnore
    public final void a(ev2 ev2, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning string value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(ev2 ev2, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning long value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(ev2 ev2, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning int value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(ev2 ev2, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning byte array to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(ev2 ev2, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning boolean value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(ev2 ev2, Bundle bundle) {
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning bundle value to wrapper", e2);
        }
    }

    @DexIgnore
    public static Bundle a(List<la3> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (la3 next : list) {
            String str = next.e;
            if (str != null) {
                bundle.putString(next.b, str);
            } else {
                Long l = next.d;
                if (l != null) {
                    bundle.putLong(next.b, l.longValue());
                } else {
                    Double d2 = next.g;
                    if (d2 != null) {
                        bundle.putDouble(next.b, d2.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    @DexIgnore
    public final void a(ev2 ev2, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            ev2.d(bundle);
        } catch (RemoteException e2) {
            this.a.b().w().a("Error returning bundle list to wrapper", e2);
        }
    }

    @DexIgnore
    public final URL a(long j, String str, String str2, long j2) {
        try {
            w12.b(str2);
            w12.b(str);
            String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", new Object[]{String.format("v%s.%s", new Object[]{Long.valueOf(j), Integer.valueOf(u())}), str2, str, Long.valueOf(j2)});
            if (str.equals(l().t())) {
                format = format.concat("&ddl_test=1");
            }
            return new URL(format);
        } catch (IllegalArgumentException | MalformedURLException e2) {
            b().t().a("Failed to create BOW URL for Deferred Deep Link. exception", e2.getMessage());
            return null;
        }
    }

    @DexIgnore
    @SuppressLint({"ApplySharedPref"})
    public final boolean a(String str, double d2) {
        try {
            SharedPreferences.Editor edit = c().getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
            edit.putString("deeplink", str);
            edit.putLong("timestamp", Double.doubleToRawLongBits(d2));
            return edit.commit();
        } catch (Exception e2) {
            b().t().a("Failed to persist Deferred Deep Link. exception", e2);
            return false;
        }
    }
}
