package com.fossil;

import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.api.internal.zzc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n02 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ LifecycleCallback a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ zzc c;

    @DexIgnore
    public n02(zzc zzc, LifecycleCallback lifecycleCallback, String str) {
        this.c = zzc;
        this.a = lifecycleCallback;
        this.b = str;
    }

    @DexIgnore
    public final void run() {
        if (this.c.b > 0) {
            this.a.a(this.c.c != null ? this.c.c.getBundle(this.b) : null);
        }
        if (this.c.b >= 2) {
            this.a.d();
        }
        if (this.c.b >= 3) {
            this.a.c();
        }
        if (this.c.b >= 4) {
            this.a.e();
        }
        if (this.c.b >= 5) {
            this.a.b();
        }
    }
}
