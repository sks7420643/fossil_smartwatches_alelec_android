package com.fossil;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ko3<E> extends jo3<E> implements ListIterator<E> {
    @DexIgnore
    @Deprecated
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }
}
