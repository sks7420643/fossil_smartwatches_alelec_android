package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface d55 extends k24<c55> {
    @DexIgnore
    void E(String str);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void j(List<String> list);

    @DexIgnore
    void setTitle(String str);

    @DexIgnore
    void y(String str);
}
