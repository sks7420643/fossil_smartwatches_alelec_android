package com.fossil;

import android.text.TextUtils;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wx5 {
    @DexIgnore
    public static wx5 a;

    @DexIgnore
    public static wx5 a() {
        if (a == null) {
            a = new wx5();
        }
        return a;
    }

    @DexIgnore
    public List<ContactGroup> b(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingSms = zm4.p.a().b().getContactGroupsMatchingSms(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingSms == null ? new ArrayList() : contactGroupsMatchingSms;
    }

    @DexIgnore
    public List<ContactGroup> a(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingIncomingCall = zm4.p.a().b().getContactGroupsMatchingIncomingCall(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingIncomingCall == null ? new ArrayList() : contactGroupsMatchingIncomingCall;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0014 A[LOOP:0: B:3:0x0014->B:16:0x0014, LOOP_END, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    public Contact a(String str, int i) {
        Contact contact;
        List<ContactGroup> allContactGroups = zm4.p.a().b().getAllContactGroups(i);
        if (allContactGroups == null) {
            return null;
        }
        loop0:
        for (ContactGroup contacts : allContactGroups) {
            Iterator it = contacts.getContacts().iterator();
            while (true) {
                if (!it.hasNext()) {
                    contact = (Contact) it.next();
                    if (TextUtils.equals(str, contact.getFirstName()) || contact.containsPhoneNumber(str)) {
                        return contact;
                    }
                    if (!it.hasNext()) {
                    }
                }
            }
            return contact;
        }
        return null;
    }
}
