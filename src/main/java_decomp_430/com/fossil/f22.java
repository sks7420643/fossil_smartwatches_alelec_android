package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f22 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RuntimeException {
        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public a(String str, Parcel parcel) {
            super(r2.toString());
            int dataPosition = parcel.dataPosition();
            int dataSize = parcel.dataSize();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 41);
            sb.append(str);
            sb.append(" Parcel: pos=");
            sb.append(dataPosition);
            sb.append(" size=");
            sb.append(dataSize);
        }
    }

    @DexIgnore
    public static int a(int i) {
        return i & 65535;
    }

    @DexIgnore
    public static int a(Parcel parcel) {
        return parcel.readInt();
    }

    @DexIgnore
    public static int b(Parcel parcel) {
        int a2 = a(parcel);
        int u = u(parcel, a2);
        int dataPosition = parcel.dataPosition();
        if (a(a2) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(a2));
            throw new a(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = u + dataPosition;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("Size read is invalid start=");
        sb.append(dataPosition);
        sb.append(" end=");
        sb.append(i);
        throw new a(sb.toString(), parcel);
    }

    @DexIgnore
    public static float[] c(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        float[] createFloatArray = parcel.createFloatArray();
        parcel.setDataPosition(dataPosition + u);
        return createFloatArray;
    }

    @DexIgnore
    public static int[] d(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + u);
        return createIntArray;
    }

    @DexIgnore
    public static String e(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + u);
        return readString;
    }

    @DexIgnore
    public static String[] f(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + u);
        return createStringArray;
    }

    @DexIgnore
    public static ArrayList<String> g(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + u);
        return createStringArrayList;
    }

    @DexIgnore
    public static void h(Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(i);
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static boolean i(Parcel parcel, int i) {
        a(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    @DexIgnore
    public static Boolean j(Parcel parcel, int i) {
        int u = u(parcel, i);
        if (u == 0) {
            return null;
        }
        a(parcel, i, u, 4);
        return Boolean.valueOf(parcel.readInt() != 0);
    }

    @DexIgnore
    public static byte k(Parcel parcel, int i) {
        a(parcel, i, 4);
        return (byte) parcel.readInt();
    }

    @DexIgnore
    public static double l(Parcel parcel, int i) {
        a(parcel, i, 8);
        return parcel.readDouble();
    }

    @DexIgnore
    public static Double m(Parcel parcel, int i) {
        int u = u(parcel, i);
        if (u == 0) {
            return null;
        }
        a(parcel, i, u, 8);
        return Double.valueOf(parcel.readDouble());
    }

    @DexIgnore
    public static float n(Parcel parcel, int i) {
        a(parcel, i, 4);
        return parcel.readFloat();
    }

    @DexIgnore
    public static Float o(Parcel parcel, int i) {
        int u = u(parcel, i);
        if (u == 0) {
            return null;
        }
        a(parcel, i, u, 4);
        return Float.valueOf(parcel.readFloat());
    }

    @DexIgnore
    public static IBinder p(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + u);
        return readStrongBinder;
    }

    @DexIgnore
    public static int q(Parcel parcel, int i) {
        a(parcel, i, 4);
        return parcel.readInt();
    }

    @DexIgnore
    public static Integer r(Parcel parcel, int i) {
        int u = u(parcel, i);
        if (u == 0) {
            return null;
        }
        a(parcel, i, u, 4);
        return Integer.valueOf(parcel.readInt());
    }

    @DexIgnore
    public static long s(Parcel parcel, int i) {
        a(parcel, i, 8);
        return parcel.readLong();
    }

    @DexIgnore
    public static Long t(Parcel parcel, int i) {
        int u = u(parcel, i);
        if (u == 0) {
            return null;
        }
        a(parcel, i, u, 8);
        return Long.valueOf(parcel.readLong());
    }

    @DexIgnore
    public static int u(Parcel parcel, int i) {
        return (i & -65536) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    @DexIgnore
    public static void v(Parcel parcel, int i) {
        parcel.setDataPosition(parcel.dataPosition() + u(parcel, i));
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, int i2) {
        int u = u(parcel, i);
        if (u != i2) {
            String hexString = Integer.toHexString(u);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i2);
            sb.append(" got ");
            sb.append(u);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(i2);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new a(sb.toString(), parcel);
        }
    }

    @DexIgnore
    public static <T> ArrayList<T> c(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(dataPosition + u);
        return createTypedArrayList;
    }

    @DexIgnore
    public static <T extends Parcelable> T a(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        T t = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + u);
        return t;
    }

    @DexIgnore
    public static byte[] b(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + u);
        return createByteArray;
    }

    @DexIgnore
    public static Bundle a(Parcel parcel, int i) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + u);
        return readBundle;
    }

    @DexIgnore
    public static <T> T[] b(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u == 0) {
            return null;
        }
        T[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + u);
        return createTypedArray;
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, List list, ClassLoader classLoader) {
        int u = u(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (u != 0) {
            parcel.readList(list, classLoader);
            parcel.setDataPosition(dataPosition + u);
        }
    }
}
