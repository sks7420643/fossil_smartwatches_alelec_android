package com.fossil;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax6 extends xw6<Fragment> {
    @DexIgnore
    public ax6(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        ((Fragment) b()).requestPermissions(strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return ((Fragment) b()).shouldShowRequestPermissionRationale(str);
    }

    @DexIgnore
    public FragmentManager c() {
        return ((Fragment) b()).getChildFragmentManager();
    }

    @DexIgnore
    public Context a() {
        return ((Fragment) b()).getActivity();
    }
}
