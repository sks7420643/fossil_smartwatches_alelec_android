package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fs3 implements Runnable {
    @DexIgnore
    public /* final */ FirebaseInstanceId a;

    @DexIgnore
    public fs3(FirebaseInstanceId firebaseInstanceId) {
        this.a = firebaseInstanceId;
    }

    @DexIgnore
    public final void run() {
        this.a.i();
    }
}
