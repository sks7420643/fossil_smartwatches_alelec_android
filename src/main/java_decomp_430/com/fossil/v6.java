package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v6 {
    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: android.os.CancellationSignal} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: android.os.CancellationSignal} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v4, resolved type: android.os.CancellationSignal} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, z7 z7Var) {
        CancellationSignal cancellationSignal;
        if (Build.VERSION.SDK_INT >= 16) {
            if (z7Var != null) {
                try {
                    cancellationSignal = z7Var.b();
                } catch (Exception e) {
                    if (e instanceof OperationCanceledException) {
                        throw new f8();
                    }
                    throw e;
                }
            } else {
                cancellationSignal = null;
            }
            return contentResolver.query(uri, strArr, str, strArr2, str2, cancellationSignal);
        }
        if (z7Var != null) {
            z7Var.d();
        }
        return contentResolver.query(uri, strArr, str, strArr2, str2);
    }
}
