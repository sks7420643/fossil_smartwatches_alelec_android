package com.fossil;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo {
    @DexIgnore
    public static /* final */ zo c; // = new zo();
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public ThreadLocal<Integer> a;

        @DexIgnore
        public b() {
            this.a = new ThreadLocal<>();
        }

        @DexIgnore
        public final int a() {
            Integer num = this.a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.a.remove();
            } else {
                this.a.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        @DexIgnore
        public final int b() {
            Integer num = this.a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.a.set(Integer.valueOf(intValue));
            return intValue;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            if (b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                zo.a().execute(runnable);
            }
            a();
        }
    }

    @DexIgnore
    public zo() {
        this.a = !c() ? Executors.newCachedThreadPool() : vo.a();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new b();
    }

    @DexIgnore
    public static ExecutorService a() {
        return c.a;
    }

    @DexIgnore
    public static Executor b() {
        return c.b;
    }

    @DexIgnore
    public static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
