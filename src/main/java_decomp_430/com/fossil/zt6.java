package com.fossil;

import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zt6 extends Closeable {
    @DexIgnore
    long b(jt6 jt6, long j) throws IOException;

    @DexIgnore
    au6 b();

    @DexIgnore
    void close() throws IOException;
}
