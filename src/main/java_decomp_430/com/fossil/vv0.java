package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vv0 extends p41 {
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public byte[] F;
    @DexIgnore
    public /* final */ rg1 G;
    @DexIgnore
    public /* final */ rg1 H;
    @DexIgnore
    public /* final */ short I;

    @DexIgnore
    public vv0(du0 du0, short s, lx0 lx0, ue1 ue1, int i) {
        super(lx0, ue1, i);
        this.I = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(du0.a).putShort(this.I).array();
        wg6.a(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.E = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(du0.a()).putShort(this.I).array();
        wg6.a(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.F = array2;
        rg1 rg1 = rg1.FTC;
        this.G = rg1;
        this.H = rg1;
    }

    @DexIgnore
    public final sj0 a(byte b) {
        return zh0.j.a(b);
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.F = bArr;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.FILE_HANDLE, (Object) cw0.a(this.I));
    }

    @DexIgnore
    public final rg1 m() {
        return this.H;
    }

    @DexIgnore
    public byte[] o() {
        return this.E;
    }

    @DexIgnore
    public final rg1 p() {
        return this.G;
    }

    @DexIgnore
    public byte[] r() {
        return this.F;
    }

    @DexIgnore
    public final short s() {
        return this.I;
    }
}
