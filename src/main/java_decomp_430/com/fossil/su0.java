package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su0 implements Parcelable.Creator<lw0> {
    @DexIgnore
    public /* synthetic */ su0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new lw0(parcel);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new lw0[i];
    }
}
