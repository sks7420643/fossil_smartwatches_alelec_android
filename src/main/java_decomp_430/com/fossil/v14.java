package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v14 implements Factory<in4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public v14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static v14 a(b14 b14) {
        return new v14(b14);
    }

    @DexIgnore
    public static in4 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static in4 c(b14 b14) {
        in4 k = b14.k();
        z76.a(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    public in4 get() {
        return b(this.a);
    }
}
