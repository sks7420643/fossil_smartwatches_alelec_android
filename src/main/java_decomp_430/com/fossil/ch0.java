package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch0 extends p40 {
    @DexIgnore
    public static /* final */ fk1 d; // = new fk1((qg6) null);
    @DexIgnore
    public /* final */ hm0 a;
    @DexIgnore
    public /* final */ lf0 b;
    @DexIgnore
    public /* final */ t31 c;

    @DexIgnore
    public /* synthetic */ ch0(hm0 hm0, lf0 lf0, t31 t31, int i) {
        hm0 = (i & 1) != 0 ? hm0.UNKNOWN : hm0;
        t31 = (i & 4) != 0 ? new t31(x11.SUCCESS, 0, 2) : t31;
        this.a = hm0;
        this.b = lf0;
        this.c = t31;
    }

    @DexIgnore
    public static /* synthetic */ ch0 a(ch0 ch0, hm0 hm0, lf0 lf0, t31 t31, int i) {
        if ((i & 1) != 0) {
            hm0 = ch0.a;
        }
        if ((i & 2) != 0) {
            lf0 = ch0.b;
        }
        if ((i & 4) != 0) {
            t31 = ch0.c;
        }
        return ch0.a(hm0, lf0, t31);
    }

    @DexIgnore
    public final ch0 a(hm0 hm0, lf0 lf0, t31 t31) {
        return new ch0(hm0, lf0, t31);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(jSONObject, bm0.COMMAND_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.b));
            if (this.c.a != x11.SUCCESS) {
                cw0.a(jSONObject, bm0.GATT_RESULT, (Object) this.c.a());
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ch0)) {
            return false;
        }
        ch0 ch0 = (ch0) obj;
        return wg6.a(this.a, ch0.a) && wg6.a(this.b, ch0.b) && wg6.a(this.c, ch0.c);
    }

    @DexIgnore
    public int hashCode() {
        hm0 hm0 = this.a;
        int i = 0;
        int hashCode = (hm0 != null ? hm0.hashCode() : 0) * 31;
        lf0 lf0 = this.b;
        int hashCode2 = (hashCode + (lf0 != null ? lf0.hashCode() : 0)) * 31;
        t31 t31 = this.c;
        if (t31 != null) {
            i = t31.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("Result(commandId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", gattResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public ch0(hm0 hm0, lf0 lf0, t31 t31) {
        this.a = hm0;
        this.b = lf0;
        this.c = t31;
    }
}
