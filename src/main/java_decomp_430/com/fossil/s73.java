package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ e73 b;

    @DexIgnore
    public s73(e73 e73, AtomicReference atomicReference) {
        this.b = e73;
        this.a = atomicReference;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                this.a.set(Double.valueOf(this.b.l().c(this.b.p().A(), l03.Q)));
                this.a.notify();
            } catch (Throwable th) {
                this.a.notify();
                throw th;
            }
        }
    }
}
