package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import com.fossil.yq6;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.FileDownloadManager;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b14 {
    @DexIgnore
    public /* final */ PortfolioApp a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ b14 a;

        @DexIgnore
        public a(b14 b14) {
            this.a = b14;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            yq6.a f = chain.t().f();
            f.a("Content-Type", Constants.CONTENT_TYPE);
            f.a(com.zendesk.sdk.network.Constants.USER_AGENT_HEADER, mh4.b.a());
            f.a("locale", this.a.a.o());
            return chain.a(f.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ b14 a;

        @DexIgnore
        public b(b14 b14) {
            this.a = b14;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            yq6.a f = chain.t().f();
            f.a("Content-Type", Constants.CONTENT_TYPE);
            f.a(com.zendesk.sdk.network.Constants.USER_AGENT_HEADER, mh4.b.a());
            f.a("locale", this.a.a.o());
            return chain.a(f.a());
        }
    }

    @DexIgnore
    public b14(PortfolioApp portfolioApp) {
        wg6.b(portfolioApp, "mApplication");
        this.a = portfolioApp;
    }

    @DexIgnore
    public final PortfolioApp b() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Context c() {
        Context applicationContext = this.a.getApplicationContext();
        wg6.a((Object) applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ShortcutApiService d(ip4 ip4, mp4 mp4) {
        wg6.b(ip4, "interceptor");
        wg6.b(mp4, "authenticator");
        return (ShortcutApiService) a(ip4, mp4, ShortcutApiService.class);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final ContentResolver e() {
        ContentResolver contentResolver = this.a.getContentResolver();
        wg6.a((Object) contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final hn4 f() {
        return new hn4();
    }

    @DexIgnore
    public final FileDownloadManager g() {
        return new FileDownloadManager();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final FirmwareFileRepository h() {
        Context applicationContext = this.a.getApplicationContext();
        wg6.a((Object) applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final GuestApiService i() {
        b bVar = new b(this);
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((Interceptor) bVar);
        return (GuestApiService) lp4.g.a(GuestApiService.class);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final ce j() {
        ce a2 = ce.a(this.a);
        wg6.a((Object) a2, "LocalBroadcastManager.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final in4 k() {
        return new in4();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final s04 l() {
        s04 a2 = s04.a((Context) this.a);
        wg6.a((Object) a2, "MFLocationService.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final zm4 m() {
        return zm4.p.a();
    }

    @DexIgnore
    public final z24 n() {
        return new z24(new b34());
    }

    @DexIgnore
    public final ey5 o() {
        return ey5.d.a();
    }

    @DexIgnore
    public final el4 p() {
        el4 b2 = el4.b();
        wg6.a((Object) b2, "WatchHelper.getInstance()");
        return b2;
    }

    @DexIgnore
    public final MFLoginWechatManager q() {
        return new MFLoginWechatManager();
    }

    @DexIgnore
    public final kn4 r() {
        return new kn4();
    }

    @DexIgnore
    public final an4 a(Context context) {
        wg6.b(context, "context");
        return new an4(context);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final AuthApiUserService b(ip4 ip4, mp4 mp4) {
        wg6.b(ip4, "interceptor");
        wg6.b(mp4, "authenticator");
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((bq6) mp4);
        lp4.g.a((Interceptor) ip4);
        return (AuthApiUserService) lp4.g.a(AuthApiUserService.class);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final GoogleApiService c(ip4 ip4, mp4 mp4) {
        wg6.b(ip4, "interceptor");
        wg6.b(mp4, "authenticator");
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 4) + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((bq6) mp4);
        lp4.g.a((Interceptor) ip4);
        return (GoogleApiService) lp4.g.a(GoogleApiService.class);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final AuthApiGuestService d() {
        a aVar = new a(this);
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((Interceptor) aVar);
        return (AuthApiGuestService) lp4.g.a(AuthApiGuestService.class);
    }

    @DexIgnore
    public final ApplicationEventListener a(PortfolioApp portfolioApp, an4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, cj4 cj4, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, FileRepository fileRepository, ThemeRepository themeRepository, RingStyleRepository ringStyleRepository) {
        PortfolioApp portfolioApp2 = portfolioApp;
        cj4 cj42 = cj4;
        RingStyleRepository ringStyleRepository2 = ringStyleRepository;
        wg6.b(portfolioApp2, "app");
        wg6.b(an4, "sharedPreferencesManager");
        wg6.b(hybridPresetRepository, "hybridPresetRepository");
        wg6.b(categoryRepository, "categoryRepository");
        wg6.b(watchAppRepository, "watchAppRepository");
        wg6.b(complicationRepository, "complicationRepository");
        wg6.b(microAppRepository, "microAppRepository");
        wg6.b(dianaPresetRepository, "dianaPresetRepository");
        wg6.b(deviceRepository, "deviceRepository");
        wg6.b(userRepository, "userRepository");
        wg6.b(alarmsRepository, "alarmsRepository");
        wg6.b(cj4, "deviceSettingFactory");
        wg6.b(watchFaceRepository, "watchFaceRepository");
        wg6.b(watchLocalizationRepository, "watchLocalizationRepository");
        wg6.b(fileRepository, "fileRepository");
        wg6.b(themeRepository, "themeRepository");
        wg6.b(ringStyleRepository, "ringStyleRepository");
        return new ApplicationEventListener(portfolioApp2, an4, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, ringStyleRepository2, deviceRepository, userRepository, cj42, alarmsRepository, watchFaceRepository, watchLocalizationRepository, fileRepository, themeRepository);
    }

    @DexIgnore
    public final WatchParamHelper a(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        wg6.b(deviceRepository, "deviceRepository");
        wg6.b(portfolioApp, "app");
        return new WatchParamHelper(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final AnalyticsHelper a() {
        return AnalyticsHelper.f.c();
    }

    @DexIgnore
    public final AlarmHelper a(an4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        wg6.b(an4, "sharedPreferencesManager");
        wg6.b(userRepository, "userRepository");
        wg6.b(alarmsRepository, "alarmsRepository");
        return new AlarmHelper(an4, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final kk4 a(Context context, u04 u04, an4 an4) {
        wg6.b(context, "context");
        wg6.b(u04, "appExecutors");
        wg6.b(an4, "sharedPreferencesManager");
        return new kk4(context, u04.b(), an4);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final <S> S a(ip4 ip4, mp4 mp4, Class<S> cls) {
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 0) + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((bq6) mp4);
        lp4.g.a((Interceptor) ip4);
        return lp4.g.a(cls);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final ApiServiceV2 a(ip4 ip4, mp4 mp4) {
        wg6.b(ip4, "interceptor");
        wg6.b(mp4, "authenticator");
        lp4 lp4 = lp4.g;
        lp4.a(bx5.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        lp4 lp42 = lp4.g;
        File cacheDir = this.a.getCacheDir();
        wg6.a((Object) cacheDir, "mApplication.cacheDir");
        lp42.a(cacheDir);
        lp4.g.a((bq6) mp4);
        lp4.g.a((Interceptor) ip4);
        return (ApiServiceV2) lp4.g.a(ApiServiceV2.class);
    }

    @DexIgnore
    public final t24 a(an4 an4, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, cj4 cj4, AlarmsRepository alarmsRepository) {
        wg6.b(an4, "mSharedPrefs");
        UserRepository userRepository2 = userRepository;
        wg6.b(userRepository2, "mUserRepository");
        GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase2 = getHybridDeviceSettingUseCase;
        wg6.b(getHybridDeviceSettingUseCase2, "getHybridUseCase");
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        wg6.b(notificationsRepository2, "mNotificationRepository");
        PortfolioApp portfolioApp2 = portfolioApp;
        wg6.b(portfolioApp2, "mApp");
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        wg6.b(goalTrackingRepository2, "goalTrackingRepo");
        GoalTrackingDatabase goalTrackingDatabase2 = goalTrackingDatabase;
        wg6.b(goalTrackingDatabase2, "goalTrackingDatabase");
        DeviceDao deviceDao2 = deviceDao;
        wg6.b(deviceDao2, "deviceDao");
        HybridCustomizeDatabase hybridCustomizeDatabase2 = hybridCustomizeDatabase;
        wg6.b(hybridCustomizeDatabase2, "mHybridCustomizeDatabase");
        MicroAppLastSettingRepository microAppLastSettingRepository2 = microAppLastSettingRepository;
        wg6.b(microAppLastSettingRepository2, "microAppLastSettingRepository");
        cj4 cj42 = cj4;
        wg6.b(cj42, "deviceSettingFactory");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        wg6.b(alarmsRepository2, "alarmsRepository");
        return new t24(an4, userRepository2, notificationsRepository2, getHybridDeviceSettingUseCase2, portfolioApp2, goalTrackingRepository2, goalTrackingDatabase2, deviceDao2, hybridCustomizeDatabase2, microAppLastSettingRepository2, cj42, alarmsRepository2);
    }

    @DexIgnore
    public final s24 a(an4 an4, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        wg6.b(an4, "mSharedPrefs");
        wg6.b(userRepository, "userRepository");
        wg6.b(hybridPresetDao, "hybridPresetDao");
        wg6.b(notificationsRepository, "notificationsRepository");
        wg6.b(deviceDao, "deviceDao");
        wg6.b(portfolioApp, "app");
        return new s24(an4, userRepository, hybridPresetDao, deviceDao, notificationsRepository, portfolioApp);
    }

    @DexIgnore
    public final MigrationHelper a(an4 an4, t24 t24, s24 s24) {
        wg6.b(an4, "mSharedPrefs");
        wg6.b(t24, "migrationManager");
        wg6.b(s24, "legacyMigrationManager");
        return new MigrationHelper(an4, t24, s24);
    }

    @DexIgnore
    public final LinkStreamingManager a(HybridPresetRepository hybridPresetRepository, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, an4 an4, SetNotificationUseCase setNotificationUseCase) {
        wg6.b(hybridPresetRepository, "hybridPresetRepository");
        wg6.b(quickResponseRepository, "quickResponseRepository");
        wg6.b(alarmsRepository, "alarmsRepository");
        wg6.b(an4, "sharedPreferencesManager");
        wg6.b(setNotificationUseCase, "setNotificationUseCase");
        return new LinkStreamingManager(hybridPresetRepository, alarmsRepository, quickResponseRepository, setNotificationUseCase);
    }

    @DexIgnore
    public final dp5 a(InAppNotificationRepository inAppNotificationRepository) {
        wg6.b(inAppNotificationRepository, "repository");
        return new dp5(inAppNotificationRepository);
    }
}
