package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum m70 {
    SUCCESS((byte) 0),
    NO_MUSIC_PLAYER((byte) 1),
    FAIL_TO_TRIGGER((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public m70(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
