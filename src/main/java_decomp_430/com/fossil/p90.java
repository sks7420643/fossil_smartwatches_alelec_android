package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ l70 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<p90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new p90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new p90[i];
        }
    }

    @DexIgnore
    public p90(byte b, l70 l70) {
        super(e90.MUSIC_CONTROL, b);
        this.c = l70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.t0, (Object) cw0.a((Enum<?>) this.c));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(p90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((p90) obj).c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.MusicControlNotification");
    }

    @DexIgnore
    public final l70 getAction() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }

    @DexIgnore
    public /* synthetic */ p90(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.c = l70.valueOf(readString);
            return;
        }
        wg6.a();
        throw null;
    }
}
