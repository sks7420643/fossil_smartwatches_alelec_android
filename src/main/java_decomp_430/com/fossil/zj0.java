package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj0 extends ul0 {
    @DexIgnore
    public static long g; // = 100;
    @DexIgnore
    public static /* final */ zj0 h; // = new zj0();

    @DexIgnore
    public zj0() {
        super("raw_minute_data", 204800, 20971520, "raw_minute_data", "raw_minute_data", new ge0("", "", ""), 1800, new fp0(), lp0.MINUTE_DATA_REFERENCE, false);
    }

    @DexIgnore
    public long a() {
        return g;
    }
}
