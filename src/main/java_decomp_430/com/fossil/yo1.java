package com.fossil;

import com.fossil.no1;
import com.fossil.uo1;
import com.fossil.wo1;
import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo1 extends yw3<yo1, b> implements zo1 {
    @DexIgnore
    public static /* final */ yo1 q; // = new yo1();
    @DexIgnore
    public static volatile gx3<yo1> r;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public Object f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public uo1 i;
    @DexIgnore
    public zw3.c<wo1> j; // = yw3.i();
    @DexIgnore
    public zw3.c<sw3> o; // = yw3.i();
    @DexIgnore
    public int p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[c.zza().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|18|(2:19|20)|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|18|(2:19|20)|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|1|2|3|5|6|7|9|10|11|12|13|14|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0075 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007f */
        /*
        static {
            try {
                b[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            b[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            b[yw3.j.NEW_BUILDER.ordinal()] = 4;
            b[yw3.j.VISIT.ordinal()] = 5;
            b[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            b[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                b[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused3) {
            }
            a[c.LOG_SOURCE.ordinal()] = 1;
            a[c.LOG_SOURCE_NAME.ordinal()] = 2;
            try {
                a[c.SOURCE_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused4) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<yo1, b> implements zo1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(long j) {
            d();
            ((yo1) this.b).g = j;
            return this;
        }

        @DexIgnore
        public b b(long j) {
            d();
            ((yo1) this.b).h = j;
            return this;
        }

        @DexIgnore
        public b() {
            super(yo1.q);
        }

        @DexIgnore
        public b a(uo1 uo1) {
            d();
            yo1.a((yo1) this.b, uo1);
            return this;
        }

        @DexIgnore
        public b a(int i) {
            d();
            yo1.a((yo1) this.b, i);
            return this;
        }

        @DexIgnore
        public b a(String str) {
            d();
            yo1.a((yo1) this.b, str);
            return this;
        }

        @DexIgnore
        public b a(wo1.b bVar) {
            d();
            yo1.a((yo1) this.b, bVar);
            return this;
        }

        @DexIgnore
        public b a(no1.a aVar) {
            d();
            ((yo1) this.b).a(aVar);
            return this;
        }
    }

    @DexIgnore
    public enum c implements zw3.a {
        LOG_SOURCE(2),
        LOG_SOURCE_NAME(6),
        SOURCE_NOT_SET(0);
        
        @DexIgnore
        public /* final */ int zze;

        @DexIgnore
        public c(int i) {
            this.zze = i;
        }

        @DexIgnore
        public static c[] zza() {
            return (c[]) a.clone();
        }

        @DexIgnore
        public int getNumber() {
            return this.zze;
        }

        @DexIgnore
        public static c zza(int i) {
            if (i == 0) {
                return SOURCE_NOT_SET;
            }
            if (i == 2) {
                return LOG_SOURCE;
            }
            if (i != 6) {
                return null;
            }
            return LOG_SOURCE_NAME;
        }
    }

    /*
    static {
        q.g();
    }
    */

    @DexIgnore
    public static b k() {
        return (b) q.c();
    }

    @DexIgnore
    public static gx3<yo1> l() {
        return q.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (a.b[jVar.ordinal()]) {
            case 1:
                return new yo1();
            case 2:
                return q;
            case 3:
                this.j.l();
                this.o.l();
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                yo1 yo1 = (yo1) obj2;
                this.g = kVar.a(this.g != 0, this.g, yo1.g != 0, yo1.g);
                this.h = kVar.a(this.h != 0, this.h, yo1.h != 0, yo1.h);
                this.i = (uo1) kVar.a(this.i, yo1.i);
                this.j = kVar.a(this.j, yo1.j);
                this.o = kVar.a(this.o, yo1.o);
                this.p = kVar.a(this.p != 0, this.p, yo1.p != 0, yo1.p);
                int ordinal = c.zza(yo1.e).ordinal();
                if (ordinal == 0) {
                    if (this.e == 2) {
                        z = true;
                    }
                    this.f = kVar.a(z, this.f, yo1.f);
                } else if (ordinal == 1) {
                    if (this.e == 6) {
                        z = true;
                    }
                    this.f = kVar.b(z, this.f, yo1.f);
                } else if (ordinal == 2) {
                    if (this.e != 0) {
                        z = true;
                    }
                    kVar.a(z);
                }
                if (kVar == yw3.i.a) {
                    int i2 = yo1.e;
                    if (i2 != 0) {
                        this.e = i2;
                    }
                    this.d |= yo1.d;
                }
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 10) {
                                uo1.b bVar = this.i != null ? (uo1.b) this.i.c() : null;
                                this.i = (uo1) tw3.a(uo1.m(), ww3);
                                if (bVar != null) {
                                    bVar.b(this.i);
                                    this.i = (uo1) bVar.c();
                                }
                            } else if (l == 16) {
                                this.e = 2;
                                this.f = Integer.valueOf(tw3.d());
                            } else if (l == 26) {
                                if (!this.j.m()) {
                                    this.j = yw3.a(this.j);
                                }
                                this.j.add((wo1) tw3.a(wo1.l(), ww3));
                            } else if (l == 32) {
                                this.g = tw3.e();
                            } else if (l == 42) {
                                if (!this.o.m()) {
                                    this.o = yw3.a(this.o);
                                }
                                this.o.add(tw3.b());
                            } else if (l == 50) {
                                String k = tw3.k();
                                this.e = 6;
                                this.f = k;
                            } else if (l == 64) {
                                this.h = tw3.e();
                            } else if (l == 72) {
                                this.p = tw3.c();
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (r == null) {
                    synchronized (yo1.class) {
                        if (r == null) {
                            r = new yw3.c(q);
                        }
                    }
                }
                return r;
            default:
                throw new UnsupportedOperationException();
        }
        return q;
    }

    @DexIgnore
    public int d() {
        int i2;
        int i3 = this.c;
        if (i3 != -1) {
            return i3;
        }
        uo1 uo1 = this.i;
        if (uo1 != null) {
            if (uo1 == null) {
                uo1 = uo1.k();
            }
            i2 = uw3.b(1, (dx3) uo1) + 0;
        } else {
            i2 = 0;
        }
        if (this.e == 2) {
            i2 += uw3.d(2, ((Integer) this.f).intValue());
        }
        int i4 = i2;
        for (int i5 = 0; i5 < this.j.size(); i5++) {
            i4 += uw3.b(3, (dx3) this.j.get(i5));
        }
        long j2 = this.g;
        if (j2 != 0) {
            i4 += uw3.d(4, j2);
        }
        int i6 = 0;
        for (int i7 = 0; i7 < this.o.size(); i7++) {
            i6 += uw3.a(this.o.get(i7));
        }
        int size = (this.o.size() * 1) + i4 + i6;
        int i8 = this.e;
        if (i8 == 6) {
            size += uw3.b(6, i8 == 6 ? (String) this.f : "");
        }
        long j3 = this.h;
        if (j3 != 0) {
            size += uw3.d(8, j3);
        }
        if (this.p != no1.a.zza.getNumber()) {
            size += uw3.c(9, this.p);
        }
        this.c = size;
        return size;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        uo1 uo1 = this.i;
        if (uo1 != null) {
            if (uo1 == null) {
                uo1 = uo1.k();
            }
            uw3.a(1, (dx3) uo1);
        }
        if (this.e == 2) {
            uw3.b(2, ((Integer) this.f).intValue());
        }
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            uw3.a(3, (dx3) this.j.get(i2));
        }
        long j2 = this.g;
        if (j2 != 0) {
            uw3.a(4, j2);
        }
        for (int i3 = 0; i3 < this.o.size(); i3++) {
            uw3.a(5, this.o.get(i3));
        }
        int i4 = this.e;
        if (i4 == 6) {
            uw3.a(6, i4 == 6 ? (String) this.f : "");
        }
        long j3 = this.h;
        if (j3 != 0) {
            uw3.a(8, j3);
        }
        if (this.p != no1.a.zza.getNumber()) {
            uw3.a(9, this.p);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(yo1 yo1, uo1 uo1) {
        if (uo1 != null) {
            yo1.i = uo1;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void a(yo1 yo1, int i2) {
        yo1.e = 2;
        yo1.f = Integer.valueOf(i2);
    }

    @DexIgnore
    public static /* synthetic */ void a(yo1 yo1, String str) {
        if (str != null) {
            yo1.e = 6;
            yo1.f = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static /* synthetic */ void a(yo1 yo1, wo1.b bVar) {
        if (!yo1.j.m()) {
            yo1.j = yw3.a(yo1.j);
        }
        yo1.j.add((wo1) bVar.build());
    }

    @DexIgnore
    public final void a(no1.a aVar) {
        if (aVar != null) {
            this.p = aVar.getNumber();
            return;
        }
        throw new NullPointerException();
    }
}
