package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum hm0 {
    CLOSE,
    CONNECT,
    DISCONNECT,
    DISCOVER_SERVICE,
    READ_CHARACTERISTIC,
    REQUEST_MTU,
    SUBSCRIBE_CHARACTERISTIC,
    WRITE_CHARACTERISTIC,
    READ_RSSI,
    CREATE_BOND,
    REMOVE_BOND,
    CONNECT_HID,
    DISCONNECT_HID,
    UNKNOWN
}
