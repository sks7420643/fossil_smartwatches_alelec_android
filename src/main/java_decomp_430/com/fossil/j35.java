package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j35 implements Factory<i35> {
    @DexIgnore
    public static i35 a(e35 e35, int i, ArrayList<wx4> arrayList, z24 z24, a35 a35) {
        return new NotificationHybridEveryonePresenter(e35, i, arrayList, z24, a35);
    }
}
