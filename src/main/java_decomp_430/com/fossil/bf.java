package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bf<T> {
    @DexIgnore
    public static /* final */ bf e; // = new bf(Collections.emptyList(), 0);
    @DexIgnore
    public static /* final */ bf f; // = new bf(Collections.emptyList(), 0);
    @DexIgnore
    public /* final */ List<T> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<T> {
        @DexIgnore
        public abstract void a(int i, bf<T> bfVar);

        @DexIgnore
        public abstract void a(int i, Throwable th, boolean z);
    }

    @DexIgnore
    public bf(List<T> list, int i, int i2, int i3) {
        this.a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static <T> bf<T> b() {
        return e;
    }

    @DexIgnore
    public static <T> bf<T> c() {
        return f;
    }

    @DexIgnore
    public boolean a() {
        return this == f;
    }

    @DexIgnore
    public String toString() {
        return "Result " + this.b + ", " + this.a + ", " + this.c + ", offset " + this.d;
    }

    @DexIgnore
    public bf(List<T> list, int i) {
        this.a = list;
        this.b = 0;
        this.c = 0;
        this.d = i;
    }
}
