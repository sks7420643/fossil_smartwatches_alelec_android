package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w60 extends r60 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c((qg6) null);
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ b f;
    @DexIgnore
    public /* final */ b g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ b i;
    @DexIgnore
    public /* final */ b j;
    @DexIgnore
    public /* final */ b k;
    @DexIgnore
    public /* final */ short l;
    @DexIgnore
    public /* final */ short m;

    @DexIgnore
    public enum a {
        DAILY((byte) 0),
        EXTENDED((byte) 1),
        TIME_ONLY((byte) 2),
        CUSTOM((byte) 3);
        
        @DexIgnore
        public static /* final */ C0052a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w60$a$a")
        /* renamed from: com.fossil.w60$a$a  reason: collision with other inner class name */
        public static final class C0052a {
            @DexIgnore
            public /* synthetic */ C0052a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new C0052a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore
    public enum b {
        OFF((byte) 0),
        ON((byte) 1);
        
        @DexIgnore
        public static /* final */ a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new a((qg6) null);
        }
        */

        @DexIgnore
        public b(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<w60> {
        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
        }

        @DexIgnore
        public final w60 a(byte[] bArr) throws IllegalArgumentException {
            byte[] bArr2 = bArr;
            if (bArr2.length == 14) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new w60(a.c.a(order.get(0)), b.c.a(order.get(1)), b.c.a(order.get(2)), b.c.a(order.get(3)), b.c.a(order.get(4)), b.c.a(order.get(5)), b.c.a(order.get(6)), b.c.a(order.get(7)), b.c.a(order.get(8)), b.c.a(order.get(9)), order.getShort(10), order.getShort(12));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr2.length, ", require: 14"));
        }

        @DexIgnore
        public w60 createFromParcel(Parcel parcel) {
            return new w60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m66createFromParcel(Parcel parcel) {
            return new w60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public w60(a aVar, b bVar, b bVar2, b bVar3, b bVar4, b bVar5, b bVar6, b bVar7, b bVar8, b bVar9, short s, short s2) {
        super(s60.HELLAS_BATTERY);
        this.b = aVar;
        this.c = bVar;
        this.d = bVar2;
        this.e = bVar3;
        this.f = bVar4;
        this.g = bVar5;
        this.h = bVar6;
        this.i = bVar7;
        this.j = bVar8;
        this.k = bVar9;
        this.l = s;
        this.m = s2;
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(14).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).put(this.c.a()).put(this.d.a()).put(this.e.a()).put(this.f.a()).put(this.g.a()).put(this.h.a()).put(this.i.a()).put(this.j.a()).put(this.k.a()).putShort(this.l).putShort(this.m).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(w60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            w60 w60 = (w60) obj;
            return this.b == w60.b && this.c == w60.c && this.d == w60.d && this.e == w60.e && this.f == w60.f && this.g == w60.g && this.h == w60.h && this.i == w60.i && this.j == w60.j && this.k == w60.k && this.l == w60.l && this.m == w60.m;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HellasBatteryConfig");
    }

    @DexIgnore
    public final b getAlwaysOnScreenState() {
        return this.d;
    }

    @DexIgnore
    public final a getBatteryMode() {
        return this.b;
    }

    @DexIgnore
    public final short getBleFromMinuteOffset() {
        return this.l;
    }

    @DexIgnore
    public final short getBleToMinuteOffset() {
        return this.m;
    }

    @DexIgnore
    public final b getGoogleDetectState() {
        return this.k;
    }

    @DexIgnore
    public final b getLocationState() {
        return this.g;
    }

    @DexIgnore
    public final b getNfcState() {
        return this.c;
    }

    @DexIgnore
    public final b getSpeakerState() {
        return this.i;
    }

    @DexIgnore
    public final b getTiltToWakeState() {
        return this.f;
    }

    @DexIgnore
    public final b getTouchToWakeState() {
        return this.e;
    }

    @DexIgnore
    public final b getVibrationState() {
        return this.h;
    }

    @DexIgnore
    public final b getWifiState() {
        return this.j;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = Short.valueOf(this.l).hashCode();
        return Short.valueOf(this.m).hashCode() + ((hashCode10 + ((hashCode9 + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.b.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.h.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.i.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.j.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.k.a());
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.l));
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.m));
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.BATTERY_MODE, (Object) cw0.a((Enum<?>) this.b));
            cw0.a(jSONObject, bm0.NFC_STATE, (Object) cw0.a((Enum<?>) this.c));
            cw0.a(jSONObject, bm0.ALWAYS_ON_SCREEN_STATE, (Object) cw0.a((Enum<?>) this.d));
            cw0.a(jSONObject, bm0.TOUCH_TO_WAKE_STATE, (Object) cw0.a((Enum<?>) this.e));
            cw0.a(jSONObject, bm0.TILT_TO_WAKE_STATE, (Object) cw0.a((Enum<?>) this.f));
            cw0.a(jSONObject, bm0.LOCATION_STATE, (Object) cw0.a((Enum<?>) this.g));
            cw0.a(jSONObject, bm0.VIBRATION_STATE, (Object) cw0.a((Enum<?>) this.h));
            cw0.a(jSONObject, bm0.SPEAKER_STATE, (Object) cw0.a((Enum<?>) this.i));
            cw0.a(jSONObject, bm0.WIFI_STATE, (Object) cw0.a((Enum<?>) this.j));
            cw0.a(jSONObject, bm0.GOOGLE_DETECT_STATE, (Object) cw0.a((Enum<?>) this.k));
            cw0.a(jSONObject, bm0.BLE_FROM_MINUTE_OFFET, (Object) Short.valueOf(this.l));
            cw0.a(jSONObject, bm0.BLE_TO_MINUTE_OFFET, (Object) Short.valueOf(this.m));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ w60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = a.c.a(parcel.readByte());
        this.c = b.c.a(parcel.readByte());
        this.d = b.c.a(parcel.readByte());
        this.e = b.c.a(parcel.readByte());
        this.f = b.c.a(parcel.readByte());
        this.g = b.c.a(parcel.readByte());
        this.h = b.c.a(parcel.readByte());
        this.i = b.c.a(parcel.readByte());
        this.j = b.c.a(parcel.readByte());
        this.k = b.c.a(parcel.readByte());
        this.l = (short) parcel.readInt();
        this.m = (short) parcel.readInt();
    }
}
