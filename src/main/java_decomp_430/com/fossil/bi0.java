package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi0 extends vf1 {
    @DexIgnore
    public rg1 K;
    @DexIgnore
    public /* final */ long L;
    @DexIgnore
    public /* final */ long M;
    @DexIgnore
    public /* final */ long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bi0(long j, long j2, long j3, short s, ue1 ue1, int i, int i2) {
        super(dl1.d, s, lx0.PUT_FILE, ue1, (i2 & 32) != 0 ? 3 : i);
        this.L = j;
        this.M = j2;
        this.N = j3;
        this.K = rg1.UNKNOWN;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        rg1 rg1;
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (true ^ (bArr.length == 0)) {
            rg1 = rg1.p.a(bArr[0]);
        } else {
            rg1 = rg1.FTD;
        }
        this.K = rg1;
        return cw0.a(jSONObject, bm0.SOCKET_ID, (Object) this.K.a);
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(cw0.a(super.h(), bm0.OFFSET, (Object) Long.valueOf(this.L)), bm0.LENGTH, (Object) Long.valueOf(this.M)), bm0.TOTAL_LENGTH, (Object) Long.valueOf(this.N));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.SOCKET_ID, (Object) this.K.a);
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).putInt((int) this.M).putInt((int) this.N).array();
        wg6.a(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }
}
