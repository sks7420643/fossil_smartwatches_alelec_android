package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class os1 implements rs1.b {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public os1(String str) {
        this.a = str;
    }

    @DexIgnore
    public static rs1.b a(String str) {
        return new os1(str);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.a(this.a, (SQLiteDatabase) obj);
    }
}
