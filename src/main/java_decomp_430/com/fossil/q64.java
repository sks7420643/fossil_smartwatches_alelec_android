package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q64 extends p64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362208, 1);
        y.put(2131362442, 2);
        y.put(2131362017, 3);
        y.put(2131362750, 4);
        y.put(2131362747, 5);
        y.put(2131362752, 6);
        y.put(2131362223, 7);
    }
    */

    @DexIgnore
    public q64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public q64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[3], objArr[1], objArr[7], objArr[2], objArr[5], objArr[4], objArr[6]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
