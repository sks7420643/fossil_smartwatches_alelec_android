package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nh1 extends rf1 {
    @DexIgnore
    public long I;
    @DexIgnore
    public /* final */ long J;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b  */
    public nh1(ue1 ue1, long j) {
        super(ue1, sv0.PROCESS_USER_AUTHORIZATION, lx0.CONFIRM_AUTHORIZATION, 0, 8);
        boolean z;
        this.J = j;
        long j2 = this.J;
        if (j2 >= 0) {
            vg6 vg6 = vg6.a;
            if (j2 <= 4294967295L) {
                z = true;
                if (!z) {
                    this.I = this.J;
                    return;
                }
                StringBuilder b = ze0.b("timeoutInMs (");
                b.append(this.J);
                b.append(") must be in [0, [");
                vg6 vg62 = vg6.a;
                b.append(4294967295L);
                b.append("]].");
                throw new IllegalArgumentException(b.toString().toString());
            }
        }
        z = false;
        if (!z) {
        }
    }

    @DexIgnore
    public void a(long j) {
        this.I = j;
    }

    @DexIgnore
    public long e() {
        return this.I;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.TIMEOUT_IN_MS, (Object) Long.valueOf(this.J));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.J).array();
        wg6.a(array, "ByteBuffer.allocate(4)\n \u2026\n                .array()");
        return array;
    }
}
