package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hx6 extends RuntimeException {
    @DexIgnore
    public /* final */ transient rx6<?> a;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String message;

    @DexIgnore
    public hx6(rx6<?> rx6) {
        super(a(rx6));
        this.code = rx6.b();
        this.message = rx6.e();
        this.a = rx6;
    }

    @DexIgnore
    public static String a(rx6<?> rx6) {
        vx6.a(rx6, "response == null");
        return "HTTP " + rx6.b() + " " + rx6.e();
    }

    @DexIgnore
    public int code() {
        return this.code;
    }

    @DexIgnore
    public String message() {
        return this.message;
    }

    @DexIgnore
    public rx6<?> response() {
        return this.a;
    }
}
