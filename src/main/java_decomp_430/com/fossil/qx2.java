package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qx2 extends IInterface {
    @DexIgnore
    x52 a(float f) throws RemoteException;

    @DexIgnore
    x52 a(LatLng latLng, float f) throws RemoteException;
}
