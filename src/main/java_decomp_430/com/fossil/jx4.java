package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx4 implements Factory<x85> {
    @DexIgnore
    public static x85 a(ex4 ex4) {
        x85 e = ex4.e();
        z76.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
