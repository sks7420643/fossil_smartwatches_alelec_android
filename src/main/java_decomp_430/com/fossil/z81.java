package com.fossil;

import android.os.Parcel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z81 extends f51 {
    @DexIgnore
    public static /* final */ c71 CREATOR; // = new c71((qg6) null);

    @DexIgnore
    public z81(vd0 vd0, w40 w40) {
        super(vd0, w40);
    }

    @DexIgnore
    public List<uj1> b() {
        return pd6.a(new rz0());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public /* synthetic */ z81(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
