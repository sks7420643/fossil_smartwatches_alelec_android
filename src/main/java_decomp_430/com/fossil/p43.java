package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.facebook.GraphRequest;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p43 extends z33 {
    @DexIgnore
    public /* final */ s43 c; // = new s43(this, c(), "google_app_measurement_local.db");
    @DexIgnore
    public boolean d;

    @DexIgnore
    public p43(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final void A() {
        e();
        g();
        try {
            int delete = D().delete(GraphRequest.DEBUG_MESSAGES_KEY, (String) null, (String[]) null) + 0;
            if (delete > 0) {
                b().B().a("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            b().t().a("Error resetting local analytics data. error", e);
        }
    }

    @DexIgnore
    public final boolean B() {
        return a(3, new byte[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0089, code lost:
        r3 = r3 + 1;
     */
    @DexIgnore
    public final boolean C() {
        g();
        e();
        if (this.d || !E()) {
            return false;
        }
        int i = 0;
        int i2 = 5;
        while (i < 5) {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = D();
                if (sQLiteDatabase == null) {
                    this.d = true;
                    if (sQLiteDatabase != null) {
                        sQLiteDatabase.close();
                    }
                    return false;
                }
                sQLiteDatabase.beginTransaction();
                sQLiteDatabase.delete(GraphRequest.DEBUG_MESSAGES_KEY, "type == ?", new String[]{Integer.toString(3)});
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                return true;
            } catch (SQLiteFullException e) {
                b().t().a("Error deleting app launch break from local database", e);
                this.d = true;
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (SQLiteDatabaseLockedException unused) {
                SystemClock.sleep((long) i2);
                i2 += 20;
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (SQLiteException e2) {
                if (sQLiteDatabase != null) {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                }
                b().t().a("Error deleting app launch break from local database", e2);
                this.d = true;
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }
        b().w().a("Error deleting app launch break from local database in reasonable time");
        return false;
    }

    @DexIgnore
    public final SQLiteDatabase D() throws SQLiteException {
        if (this.d) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.d = true;
        return null;
    }

    @DexIgnore
    public final boolean E() {
        return c().getDatabasePath("google_app_measurement_local.db").exists();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [int, boolean] */
    /* JADX WARNING: type inference failed for: r8v0 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r8v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r8v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r8v5 */
    /* JADX WARNING: type inference failed for: r8v6 */
    /* JADX WARNING: type inference failed for: r8v7 */
    /* JADX WARNING: type inference failed for: r8v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cc, code lost:
        r8 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e1, code lost:
        if (r8.inTransaction() != false) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e3, code lost:
        r8.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f6, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00fb, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ff, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0100, code lost:
        r10 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x010c, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0111, code lost:
        r10.close();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0130 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0130 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:9:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00dd A[SYNTHETIC, Splitter:B:55:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0130 A[SYNTHETIC] */
    public final boolean a(int i, byte[] bArr) {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor2;
        e();
        g();
        Object r3 = 0;
        if (this.d) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        while (i3 < i2) {
            Object r8 = 0;
            try {
                sQLiteDatabase = D();
                if (sQLiteDatabase == null) {
                    try {
                        this.d = true;
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return r3;
                    } catch (SQLiteFullException e) {
                        e = e;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        cursor = null;
                        r8 = sQLiteDatabase;
                        if (r8 != 0) {
                        }
                        b().t().a("Error writing entry to local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (r8 == 0) {
                        }
                        i3++;
                        r3 = 0;
                        i2 = 5;
                    } catch (Throwable th) {
                        th = th;
                        cursor = null;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    sQLiteDatabase.beginTransaction();
                    long j = 0;
                    cursor = sQLiteDatabase.rawQuery("select count(1) from messages", (String[]) null);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                j = cursor.getLong(r3);
                            }
                        } catch (SQLiteFullException e3) {
                            e = e3;
                            r8 = cursor;
                            cursor2 = r8;
                            b().t().a("Error writing entry to local database", e);
                            this.d = true;
                            cursor2 = r8;
                            if (r8 != 0) {
                                r8.close();
                            }
                            if (sQLiteDatabase == null) {
                                sQLiteDatabase.close();
                            }
                            i3++;
                            r3 = 0;
                            i2 = 5;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            r8 = cursor;
                            try {
                                cursor2 = r8;
                                SystemClock.sleep((long) i4);
                                cursor2 = r8;
                                i4 += 20;
                                if (r8 != 0) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                i3++;
                                r3 = 0;
                                i2 = 5;
                            } catch (Throwable th2) {
                                th = th2;
                                cursor = cursor2;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                throw th;
                            }
                        } catch (SQLiteException e4) {
                            e = e4;
                            r8 = sQLiteDatabase;
                            if (r8 != 0) {
                            }
                            b().t().a("Error writing entry to local database", e);
                            this.d = true;
                            if (cursor != null) {
                            }
                            if (r8 == 0) {
                            }
                            i3++;
                            r3 = 0;
                            i2 = 5;
                        } catch (Throwable th3) {
                            th = th3;
                            if (cursor != null) {
                                cursor.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                            }
                            throw th;
                        }
                    }
                    if (j >= 100000) {
                        b().t().a("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        String[] strArr = new String[1];
                        strArr[r3] = Long.toString(j2);
                        long delete = (long) sQLiteDatabase.delete(GraphRequest.DEBUG_MESSAGES_KEY, "rowid in (select rowid from messages order by rowid asc limit ?)", strArr);
                        if (delete != j2) {
                            b().t().a("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    sQLiteDatabase.insertOrThrow(GraphRequest.DEBUG_MESSAGES_KEY, (String) null, contentValues);
                    sQLiteDatabase.setTransactionSuccessful();
                    sQLiteDatabase.endTransaction();
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (sQLiteDatabase == null) {
                        return true;
                    }
                    sQLiteDatabase.close();
                    return true;
                }
            } catch (SQLiteFullException e5) {
                e = e5;
                sQLiteDatabase = null;
                cursor2 = r8;
                b().t().a("Error writing entry to local database", e);
                this.d = true;
                cursor2 = r8;
                if (r8 != 0) {
                }
                if (sQLiteDatabase == null) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (SQLiteDatabaseLockedException unused3) {
                sQLiteDatabase = null;
                cursor2 = r8;
                SystemClock.sleep((long) i4);
                cursor2 = r8;
                i4 += 20;
                if (r8 != 0) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (SQLiteException e6) {
                e = e6;
                cursor = null;
                if (r8 != 0) {
                }
                b().t().a("Error writing entry to local database", e);
                this.d = true;
                if (cursor != null) {
                }
                if (r8 == 0) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (Throwable th4) {
                th = th4;
                sQLiteDatabase = null;
                cursor = null;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        b().w().a("Failed to write entry to local database");
        return false;
    }

    @DexIgnore
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final boolean a(j03 j03) {
        Parcel obtain = Parcel.obtain();
        j03.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(0, marshall);
        }
        b().w().a("Event is too long for local database. Sending event directly to service");
        return false;
    }

    @DexIgnore
    public final boolean a(la3 la3) {
        Parcel obtain = Parcel.obtain();
        la3.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(1, marshall);
        }
        b().w().a("User property too long for local database. Sending directly to service");
        return false;
    }

    @DexIgnore
    public final boolean a(ab3 ab3) {
        j();
        byte[] a = ma3.a((Parcelable) ab3);
        if (a.length <= 131072) {
            return a(2, a);
        }
        b().w().a("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v3, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v4, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v5, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v7, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v8, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v11, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v12, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v14, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v15, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v16, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v17, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v23, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v24, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v25, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v29, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v31, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v32, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v33, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v34, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v42, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v43, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v44, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v45, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v46, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v47, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v48, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v49, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v50, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v52, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v55, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v58, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v59, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v60, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v61, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX WARNING: type inference failed for: r24v13 */
    /* JADX WARNING: type inference failed for: r24v30 */
    /* JADX WARNING: type inference failed for: r24v53 */
    /* JADX WARNING: type inference failed for: r24v56 */
    /* JADX WARNING: type inference failed for: r24v57 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:77|78|79|80) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:92|93|94|95) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:62|63|64|65|200) */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x01eb, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x01ec, code lost:
        r13 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x01f0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x01f1, code lost:
        r13 = r15;
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x01f4, code lost:
        r13 = r15;
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x01f7, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x01f8, code lost:
        r13 = r15;
        r24 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0091, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0092, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0096, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0097, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x009b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009c, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        b().t().a("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        r11.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        b().t().a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r11.recycle();
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        b().t().a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r11.recycle();
        r0 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:62:0x00ee */
    /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x0120 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:92:0x0154 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:141:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0204 A[SYNTHETIC, Splitter:B:151:0x0204] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x024f  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0259  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0252 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0252 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0252 A[SYNTHETIC] */
    public final List<e22> a(int i) {
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor;
        int i2;
        SQLiteDatabase sQLiteDatabase2;
        SQLiteDatabase sQLiteDatabase3;
        SQLiteDatabase sQLiteDatabase4;
        SQLiteDatabase sQLiteDatabase5;
        SQLiteDatabase sQLiteDatabase6;
        Cursor cursor2;
        Parcel obtain;
        Parcel obtain2;
        Parcel obtain3;
        String[] strArr;
        String str;
        g();
        e();
        if (this.d) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!E()) {
            return arrayList;
        }
        int i3 = 0;
        int i4 = 5;
        int i5 = i;
        while (i3 < 5) {
            try {
                SQLiteDatabase D = D();
                if (D == null) {
                    try {
                        this.d = true;
                        if (D != null) {
                            D.close();
                        }
                        return null;
                    } catch (SQLiteFullException e) {
                        e = e;
                        cursor = null;
                        sQLiteDatabase3 = D;
                        sQLiteDatabase2 = i5;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        cursor = null;
                        sQLiteDatabase3 = D;
                        sQLiteDatabase5 = i5;
                        if (sQLiteDatabase3 != null) {
                        }
                        b().t().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase3 != null) {
                        }
                        i3++;
                        i5 = i2;
                    } catch (Throwable th) {
                        th = th;
                        cursor = null;
                        sQLiteDatabase = D;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    D.beginTransaction();
                    long j = -1;
                    if (l().a(l03.D0)) {
                        SQLiteDatabase sQLiteDatabase7 = i5;
                        SQLiteDatabase sQLiteDatabase8 = i5;
                        SQLiteDatabase sQLiteDatabase9 = i5;
                        SQLiteDatabase sQLiteDatabase10 = i5;
                        long a = a(D);
                        if (a != -1) {
                            str = "rowid<?";
                            strArr = new String[]{String.valueOf(a)};
                        } else {
                            str = null;
                            strArr = null;
                        }
                        SQLiteDatabase sQLiteDatabase11 = D;
                        try {
                            sQLiteDatabase7 = sQLiteDatabase11;
                            sQLiteDatabase8 = sQLiteDatabase11;
                            sQLiteDatabase9 = sQLiteDatabase11;
                            sQLiteDatabase10 = sQLiteDatabase11;
                            cursor2 = D.query(GraphRequest.DEBUG_MESSAGES_KEY, new String[]{"rowid", "type", "entry"}, str, strArr, (String) null, (String) null, "rowid asc", Integer.toString(100));
                            sQLiteDatabase6 = sQLiteDatabase11;
                        } catch (SQLiteFullException e3) {
                            e = e3;
                            SQLiteDatabase sQLiteDatabase12 = sQLiteDatabase7;
                            SQLiteDatabase sQLiteDatabase13 = sQLiteDatabase12;
                            SQLiteDatabase sQLiteDatabase14 = sQLiteDatabase12;
                            cursor = null;
                            sQLiteDatabase2 = sQLiteDatabase14;
                            b().t().a("Error reading entries from local database", e);
                            this.d = true;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase3 != null) {
                            }
                            i3++;
                            i5 = i2;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            SQLiteDatabase sQLiteDatabase15 = sQLiteDatabase8;
                            int i6 = sQLiteDatabase8;
                            cursor = null;
                            sQLiteDatabase4 = i6;
                            SystemClock.sleep((long) i4);
                            i4 += 20;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase3 != null) {
                            }
                            i3++;
                            i5 = i2;
                        } catch (SQLiteException e4) {
                            e = e4;
                            SQLiteDatabase sQLiteDatabase16 = sQLiteDatabase9;
                            sQLiteDatabase3 = sQLiteDatabase16;
                            SQLiteDatabase sQLiteDatabase17 = sQLiteDatabase16;
                            cursor = null;
                            sQLiteDatabase5 = sQLiteDatabase17;
                            if (sQLiteDatabase3 != null) {
                            }
                            b().t().a("Error reading entries from local database", e);
                            this.d = true;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase3 != null) {
                            }
                            i3++;
                            i5 = i2;
                        } catch (Throwable th2) {
                            th = th2;
                            SQLiteDatabase sQLiteDatabase18 = sQLiteDatabase10;
                            sQLiteDatabase = sQLiteDatabase18;
                            cursor = null;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase != null) {
                            }
                            throw th;
                        }
                    } else {
                        SQLiteDatabase sQLiteDatabase19 = D;
                        cursor2 = sQLiteDatabase19.query(GraphRequest.DEBUG_MESSAGES_KEY, new String[]{"rowid", "type", "entry"}, (String) null, (String[]) null, (String) null, (String) null, "rowid asc", Integer.toString(100));
                        sQLiteDatabase6 = sQLiteDatabase19;
                    }
                    cursor = cursor2;
                    while (cursor.moveToNext()) {
                        try {
                            j = cursor.getLong(0);
                            int i7 = cursor.getInt(1);
                            byte[] blob = cursor.getBlob(2);
                            if (i7 == 0) {
                                obtain3 = Parcel.obtain();
                                obtain3.unmarshall(blob, 0, blob.length);
                                obtain3.setDataPosition(0);
                                j03 createFromParcel = j03.CREATOR.createFromParcel(obtain3);
                                obtain3.recycle();
                                if (createFromParcel != null) {
                                    arrayList.add(createFromParcel);
                                }
                            } else if (i7 == 1) {
                                obtain2 = Parcel.obtain();
                                obtain2.unmarshall(blob, 0, blob.length);
                                obtain2.setDataPosition(0);
                                la3 la3 = la3.CREATOR.createFromParcel(obtain2);
                                obtain2.recycle();
                                if (la3 != null) {
                                    arrayList.add(la3);
                                }
                            } else if (i7 == 2) {
                                obtain = Parcel.obtain();
                                obtain.unmarshall(blob, 0, blob.length);
                                obtain.setDataPosition(0);
                                ab3 ab3 = ab3.CREATOR.createFromParcel(obtain);
                                obtain.recycle();
                                if (ab3 != null) {
                                    arrayList.add(ab3);
                                }
                            } else if (i7 == 3) {
                                b().w().a("Skipping app launch break");
                            } else {
                                b().t().a("Unknown record type in local database");
                            }
                        } catch (SQLiteFullException e5) {
                            e = e5;
                            sQLiteDatabase3 = sQLiteDatabase6;
                            sQLiteDatabase2 = sQLiteDatabase6;
                        } catch (SQLiteDatabaseLockedException unused3) {
                            sQLiteDatabase3 = sQLiteDatabase6;
                            sQLiteDatabase4 = sQLiteDatabase6;
                            SystemClock.sleep((long) i4);
                            i4 += 20;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase3 != null) {
                            }
                            i3++;
                            i5 = i2;
                        } catch (SQLiteException e6) {
                            e = e6;
                            sQLiteDatabase3 = sQLiteDatabase6;
                            sQLiteDatabase5 = sQLiteDatabase6;
                            if (sQLiteDatabase3 != null) {
                            }
                            b().t().a("Error reading entries from local database", e);
                            this.d = true;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase3 != null) {
                            }
                            i3++;
                            i5 = i2;
                        } catch (Throwable th3) {
                            th = th3;
                            sQLiteDatabase = sQLiteDatabase6;
                            if (cursor != null) {
                            }
                            if (sQLiteDatabase != null) {
                            }
                            throw th;
                        }
                    }
                    sQLiteDatabase3 = sQLiteDatabase6;
                    try {
                        if (sQLiteDatabase3.delete(GraphRequest.DEBUG_MESSAGES_KEY, "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                            b().t().a("Fewer entries removed from local database than expected");
                        }
                        sQLiteDatabase3.setTransactionSuccessful();
                        sQLiteDatabase3.endTransaction();
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase3 != null) {
                            sQLiteDatabase3.close();
                        }
                        return arrayList;
                    } catch (SQLiteFullException e7) {
                        e = e7;
                        sQLiteDatabase2 = sQLiteDatabase6;
                        b().t().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase3 != null) {
                        }
                        i3++;
                        i5 = i2;
                    } catch (SQLiteDatabaseLockedException unused4) {
                        sQLiteDatabase4 = sQLiteDatabase6;
                        SystemClock.sleep((long) i4);
                        i4 += 20;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase3 != null) {
                        }
                        i3++;
                        i5 = i2;
                    } catch (SQLiteException e8) {
                        e = e8;
                        sQLiteDatabase5 = sQLiteDatabase6;
                        if (sQLiteDatabase3 != null) {
                        }
                        b().t().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase3 != null) {
                        }
                        i3++;
                        i5 = i2;
                    }
                }
            } catch (SQLiteFullException e9) {
                e = e9;
                cursor = null;
                sQLiteDatabase3 = null;
                sQLiteDatabase2 = i5;
                b().t().a("Error reading entries from local database", e);
                this.d = true;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase3 != null) {
                    sQLiteDatabase3.close();
                    i2 = sQLiteDatabase2;
                } else {
                    i2 = sQLiteDatabase2;
                }
                i3++;
                i5 = i2;
            } catch (SQLiteDatabaseLockedException unused5) {
                cursor = null;
                sQLiteDatabase3 = null;
                sQLiteDatabase4 = i5;
                SystemClock.sleep((long) i4);
                i4 += 20;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase3 != null) {
                    sQLiteDatabase3.close();
                    i2 = sQLiteDatabase4;
                } else {
                    i2 = sQLiteDatabase4;
                }
                i3++;
                i5 = i2;
            } catch (SQLiteException e10) {
                e = e10;
                cursor = null;
                sQLiteDatabase3 = null;
                sQLiteDatabase5 = i5;
                if (sQLiteDatabase3 != null) {
                    try {
                        if (sQLiteDatabase3.inTransaction()) {
                            sQLiteDatabase3.endTransaction();
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                }
                b().t().a("Error reading entries from local database", e);
                this.d = true;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase3 != null) {
                    sQLiteDatabase3.close();
                    i2 = sQLiteDatabase5;
                } else {
                    i2 = sQLiteDatabase5;
                }
                i3++;
                i5 = i2;
            } catch (Throwable th5) {
                th = th5;
                cursor = null;
                sQLiteDatabase = null;
                if (cursor != null) {
                    cursor.close();
                }
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }
        b().w().a("Failed to read events from database in reasonable time");
        return null;
    }

    @DexIgnore
    public static long a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.query(GraphRequest.DEBUG_MESSAGES_KEY, new String[]{"rowid"}, "type=?", new String[]{"3"}, (String) null, (String) null, "rowid desc", "1");
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            if (cursor == null) {
                return -1;
            }
            cursor.close();
            return -1;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
