package com.fossil;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u9 {
    @DexIgnore
    public Object a;

    @DexIgnore
    public u9(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public static u9 a(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new u9(PointerIcon.getSystemIcon(context, i));
        }
        return new u9((Object) null);
    }
}
