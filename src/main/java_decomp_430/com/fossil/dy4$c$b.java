package com.fossil;

import com.fossil.NotificationAppsPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class dy4$c$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $notificationSettings;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ dy4$c$b a;

        @DexIgnore
        public a(dy4$c$b dy4_c_b) {
            this.a = dy4_c_b;
        }

        @DexIgnore
        public final void run() {
            this.a.this$0.this$0.q.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy4$c$b(NotificationAppsPresenter.c cVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
        this.$notificationSettings = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        dy4$c$b dy4_c_b = new dy4$c$b(this.this$0, this.$notificationSettings, xe6);
        dy4_c_b.p$ = (il6) obj;
        return dy4_c_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((dy4$c$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.q.runInTransaction(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
