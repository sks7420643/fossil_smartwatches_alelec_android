package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ DashBar s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RulerValuePicker u;
    @DexIgnore
    public /* final */ RulerValuePicker v;
    @DexIgnore
    public /* final */ TabLayout w;
    @DexIgnore
    public /* final */ TabLayout x;
    @DexIgnore
    public /* final */ FlexibleButton y;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nc4(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, DashBar dashBar, ConstraintLayout constraintLayout, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ScrollView scrollView, TabLayout tabLayout, TabLayout tabLayout2, FlexibleTextView flexibleTextView3, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = dashBar;
        this.t = constraintLayout;
        this.u = rulerValuePicker;
        this.v = rulerValuePicker2;
        this.w = tabLayout;
        this.x = tabLayout2;
        this.y = flexibleButton2;
    }
}
