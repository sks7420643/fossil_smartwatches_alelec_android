package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m43<V> {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ j43<V> b;
    @DexIgnore
    public /* final */ V c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public volatile V e;
    @DexIgnore
    public volatile V f;

    @DexIgnore
    public m43(String str, V v, V v2, j43<V> j43) {
        this.d = new Object();
        this.e = null;
        this.f = null;
        this.a = str;
        this.c = v;
        this.b = j43;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processLoop(RegionMaker.java:225)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:106)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    @DexIgnore
    /* JADX WARNING: CFG modification limit reached, blocks count: 180 */
    public final V a(V r6) {
        /*
            r5 = this;
            java.lang.Object r0 = r5.d
            monitor-enter(r0)
            monitor-exit(r0)     // Catch:{ all -> 0x0094 }
            if (r6 == 0) goto L_0x0007
            return r6
        L_0x0007:
            com.fossil.bb3 r6 = com.fossil.l03.a
            if (r6 != 0) goto L_0x000e
            V r6 = r5.c
            return r6
        L_0x000e:
            java.lang.Object r6 = g
            monitor-enter(r6)
            boolean r0 = com.fossil.bb3.a()     // Catch:{ all -> 0x008f }
            if (r0 == 0) goto L_0x0022
            V r0 = r5.f     // Catch:{ all -> 0x008f }
            if (r0 != 0) goto L_0x001e
            V r0 = r5.c     // Catch:{ all -> 0x008f }
            goto L_0x0020
        L_0x001e:
            V r0 = r5.f     // Catch:{ all -> 0x008f }
        L_0x0020:
            monitor-exit(r6)     // Catch:{ all -> 0x008f }
            return r0
        L_0x0022:
            boolean r0 = com.fossil.bb3.a()     // Catch:{ all -> 0x008f }
            if (r0 != 0) goto L_0x0087
            com.fossil.bb3 r0 = com.fossil.l03.a     // Catch:{ all -> 0x008f }
            java.util.List r0 = com.fossil.l03.b     // Catch:{ SecurityException -> 0x0066 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ SecurityException -> 0x0066 }
        L_0x0032:
            boolean r1 = r0.hasNext()     // Catch:{ SecurityException -> 0x0066 }
            if (r1 == 0) goto L_0x006a
            java.lang.Object r1 = r0.next()     // Catch:{ SecurityException -> 0x0066 }
            com.fossil.m43 r1 = (com.fossil.m43) r1     // Catch:{ SecurityException -> 0x0066 }
            java.lang.Object r2 = g     // Catch:{ SecurityException -> 0x0066 }
            monitor-enter(r2)     // Catch:{ SecurityException -> 0x0066 }
            boolean r3 = com.fossil.bb3.a()     // Catch:{ all -> 0x0063 }
            if (r3 != 0) goto L_0x005b
            r3 = 0
            com.fossil.j43<V> r4 = r1.b     // Catch:{ IllegalStateException -> 0x0057 }
            if (r4 == 0) goto L_0x0053
            com.fossil.j43<V> r4 = r1.b     // Catch:{ IllegalStateException -> 0x0057 }
            java.lang.Object r4 = r4.zza()     // Catch:{ IllegalStateException -> 0x0057 }
            goto L_0x0054
        L_0x0053:
            r4 = r3
        L_0x0054:
            r1.f = r4     // Catch:{ IllegalStateException -> 0x0057 }
            goto L_0x0059
        L_0x0057:
            r1.f = r3     // Catch:{ all -> 0x0063 }
        L_0x0059:
            monitor-exit(r2)     // Catch:{ all -> 0x0063 }
            goto L_0x0032
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0063 }
            java.lang.String r1 = "Refreshing flag cache must be done on a worker thread."
            r0.<init>(r1)     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ SecurityException -> 0x0066 }
        L_0x0066:
            r0 = move-exception
            com.fossil.l03.a((java.lang.Exception) r0)     // Catch:{ all -> 0x008f }
        L_0x006a:
            monitor-exit(r6)     // Catch:{ all -> 0x008f }
            com.fossil.j43<V> r6 = r5.b
            if (r6 != 0) goto L_0x0074
            com.fossil.bb3 r6 = com.fossil.l03.a
            V r6 = r5.c
            return r6
        L_0x0074:
            java.lang.Object r6 = r6.zza()     // Catch:{ SecurityException -> 0x007e, IllegalStateException -> 0x0079 }
            return r6
        L_0x0079:
            com.fossil.bb3 r6 = com.fossil.l03.a
            V r6 = r5.c
            return r6
        L_0x007e:
            r6 = move-exception
            com.fossil.l03.a((java.lang.Exception) r6)
            com.fossil.bb3 r6 = com.fossil.l03.a
            V r6 = r5.c
            return r6
        L_0x0087:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x008f }
            java.lang.String r1 = "Tried to refresh flag cache on main thread or on package side."
            r0.<init>(r1)     // Catch:{ all -> 0x008f }
            throw r0     // Catch:{ all -> 0x008f }
        L_0x008f:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x008f }
            throw r0
        L_0x0092:
            monitor-exit(r0)     // Catch:{ all -> 0x0094 }
            throw r6
        L_0x0094:
            r6 = move-exception
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.m43.a(java.lang.Object):java.lang.Object");
    }
}
