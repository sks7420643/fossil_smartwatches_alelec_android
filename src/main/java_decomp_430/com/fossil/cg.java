package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cg extends RecyclerView.l implements RecyclerView.n {
    @DexIgnore
    public g A;
    @DexIgnore
    public /* final */ RecyclerView.p B; // = new b();
    @DexIgnore
    public Rect C;
    @DexIgnore
    public long D;
    @DexIgnore
    public /* final */ List<View> a; // = new ArrayList();
    @DexIgnore
    public /* final */ float[] b; // = new float[2];
    @DexIgnore
    public RecyclerView.ViewHolder c; // = null;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public f m;
    @DexIgnore
    public int n; // = 0;
    @DexIgnore
    public int o;
    @DexIgnore
    public List<h> p; // = new ArrayList();
    @DexIgnore
    public int q;
    @DexIgnore
    public RecyclerView r;
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public VelocityTracker t;
    @DexIgnore
    public List<RecyclerView.ViewHolder> u;
    @DexIgnore
    public List<Integer> v;
    @DexIgnore
    public RecyclerView.h w; // = null;
    @DexIgnore
    public View x; // = null;
    @DexIgnore
    public int y; // = -1;
    @DexIgnore
    public d9 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            cg cgVar = cg.this;
            if (cgVar.c != null && cgVar.f()) {
                cg cgVar2 = cg.this;
                RecyclerView.ViewHolder viewHolder = cgVar2.c;
                if (viewHolder != null) {
                    cgVar2.b(viewHolder);
                }
                cg cgVar3 = cg.this;
                cgVar3.r.removeCallbacks(cgVar3.s);
                x9.a((View) cg.this.r, (Runnable) this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends h {
        @DexIgnore
        public /* final */ /* synthetic */ int n;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder o;
        @DexIgnore
        public /* final */ /* synthetic */ cg p;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cg cgVar, RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4, int i3, RecyclerView.ViewHolder viewHolder2) {
            super(viewHolder, i, i2, f, f2, f3, f4);
            this.p = cgVar;
            this.n = i3;
            this.o = viewHolder2;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (!this.k) {
                if (this.n <= 0) {
                    cg cgVar = this.p;
                    cgVar.m.a(cgVar.r, this.o);
                } else {
                    this.p.a.add(this.o.itemView);
                    this.h = true;
                    int i = this.n;
                    if (i > 0) {
                        this.p.a((h) this, i);
                    }
                }
                cg cgVar2 = this.p;
                View view = cgVar2.x;
                View view2 = this.o.itemView;
                if (view == view2) {
                    cgVar2.c(view2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ h a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public d(h hVar, int i) {
            this.a = hVar;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            RecyclerView recyclerView = cg.this.r;
            if (recyclerView != null && recyclerView.isAttachedToWindow()) {
                h hVar = this.a;
                if (!hVar.k && hVar.e.getAdapterPosition() != -1) {
                    RecyclerView.j itemAnimator = cg.this.r.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.isRunning((RecyclerView.j.a) null)) && !cg.this.c()) {
                        cg.this.m.b(this.a.e, this.b);
                    } else {
                        cg.this.r.post(this);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements RecyclerView.h {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public int a(int i, int i2) {
            cg cgVar = cg.this;
            View view = cgVar.x;
            if (view == null) {
                return i2;
            }
            int i3 = cgVar.y;
            if (i3 == -1) {
                i3 = cgVar.r.indexOfChild(view);
                cg.this.y = i3;
            }
            if (i2 == i - 1) {
                return i3;
            }
            return i2 < i3 ? i2 : i2 + 1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean a; // = true;

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void a() {
            this.a = false;
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @DexIgnore
        public void onLongPress(MotionEvent motionEvent) {
            View b2;
            RecyclerView.ViewHolder childViewHolder;
            int i;
            if (this.a && (b2 = cg.this.b(motionEvent)) != null && (childViewHolder = cg.this.r.getChildViewHolder(b2)) != null) {
                cg cgVar = cg.this;
                if (cgVar.m.d(cgVar.r, childViewHolder) && motionEvent.getPointerId(0) == (i = cg.this.l)) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    float x = motionEvent.getX(findPointerIndex);
                    float y = motionEvent.getY(findPointerIndex);
                    cg cgVar2 = cg.this;
                    cgVar2.d = x;
                    cgVar2.e = y;
                    cgVar2.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    cgVar2.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    if (cgVar2.m.c()) {
                        cg.this.c(childViewHolder, 2);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ float a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ RecyclerView.ViewHolder e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ ValueAnimator g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public boolean l; // = false;
        @DexIgnore
        public float m;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ValueAnimator.AnimatorUpdateListener {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.a(valueAnimator.getAnimatedFraction());
            }
        }

        @DexIgnore
        public h(RecyclerView.ViewHolder viewHolder, int i2, int i3, float f2, float f3, float f4, float f5) {
            this.f = i3;
            this.e = viewHolder;
            this.a = f2;
            this.b = f3;
            this.c = f4;
            this.d = f5;
            this.g = ValueAnimator.ofFloat(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
            this.g.addUpdateListener(new a());
            this.g.setTarget(viewHolder.itemView);
            this.g.addListener(this);
            a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public void a(long j2) {
            this.g.setDuration(j2);
        }

        @DexIgnore
        public void b() {
            this.e.setIsRecyclable(false);
            this.g.start();
        }

        @DexIgnore
        public void c() {
            float f2 = this.a;
            float f3 = this.c;
            if (f2 == f3) {
                this.i = this.e.itemView.getTranslationX();
            } else {
                this.i = f2 + (this.m * (f3 - f2));
            }
            float f4 = this.b;
            float f5 = this.d;
            if (f4 == f5) {
                this.j = this.e.itemView.getTranslationY();
            } else {
                this.j = f4 + (this.m * (f5 - f4));
            }
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            a(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.l) {
                this.e.setIsRecyclable(true);
            }
            this.l = true;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }

        @DexIgnore
        public void a() {
            this.g.cancel();
        }

        @DexIgnore
        public void a(float f2) {
            this.m = f2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i extends f {
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public i(int i, int i2) {
            this.d = i2;
            this.e = i;
        }

        @DexIgnore
        public int c(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return f.d(e(recyclerView, viewHolder), f(recyclerView, viewHolder));
        }

        @DexIgnore
        public int e(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.e;
        }

        @DexIgnore
        public int f(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.d;
        }
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void a(View view, View view2, int i, int i2);
    }

    @DexIgnore
    public cg(f fVar) {
        this.m = fVar;
    }

    @DexIgnore
    public static boolean a(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= f4 + ((float) view.getWidth()) && f3 >= f5 && f3 <= f5 + ((float) view.getHeight());
    }

    @DexIgnore
    public final void b() {
        this.r.removeItemDecoration(this);
        this.r.removeOnItemTouchListener(this.B);
        this.r.removeOnChildAttachStateChangeListener(this);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            this.m.a(this.r, this.p.get(0).e);
        }
        this.p.clear();
        this.x = null;
        this.y = -1;
        e();
        i();
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0137  */
    public void c(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z2;
        boolean z3;
        ViewParent parent;
        int i3;
        float f2;
        float f3;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i4 = i2;
        if (viewHolder2 != this.c || i4 != this.n) {
            this.D = Long.MIN_VALUE;
            int i5 = this.n;
            a(viewHolder2, true);
            this.n = i4;
            if (i4 == 2) {
                if (viewHolder2 != null) {
                    this.x = viewHolder2.itemView;
                    a();
                } else {
                    throw new IllegalArgumentException("Must pass a ViewHolder when dragging");
                }
            }
            int i6 = (1 << ((i4 * 8) + 8)) - 1;
            RecyclerView.ViewHolder viewHolder3 = this.c;
            if (viewHolder3 != null) {
                if (viewHolder3.itemView.getParent() != null) {
                    if (i5 == 2) {
                        i3 = 0;
                    } else {
                        i3 = c(viewHolder3);
                    }
                    e();
                    if (i3 == 1 || i3 == 2) {
                        f2 = Math.signum(this.i) * ((float) this.r.getHeight());
                        f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    } else {
                        if (i3 == 4 || i3 == 8 || i3 == 16 || i3 == 32) {
                            f3 = Math.signum(this.h) * ((float) this.r.getWidth());
                        } else {
                            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        }
                        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                    int i7 = i5 == 2 ? 8 : i3 > 0 ? 2 : 4;
                    a(this.b);
                    float[] fArr = this.b;
                    float f4 = fArr[0];
                    float f5 = fArr[1];
                    c cVar = r0;
                    c cVar2 = new c(this, viewHolder3, i7, i5, f4, f5, f3, f2, i3, viewHolder3);
                    cVar.a(this.m.a(this.r, i7, f3 - f4, f2 - f5));
                    this.p.add(cVar);
                    cVar.b();
                    z2 = true;
                } else {
                    RecyclerView.ViewHolder viewHolder4 = viewHolder3;
                    c(viewHolder4.itemView);
                    this.m.a(this.r, viewHolder4);
                    z2 = false;
                }
                this.c = null;
            } else {
                z2 = false;
            }
            if (viewHolder2 != null) {
                this.o = (this.m.b(this.r, viewHolder2) & i6) >> (this.n * 8);
                this.j = (float) viewHolder2.itemView.getLeft();
                this.k = (float) viewHolder2.itemView.getTop();
                this.c = viewHolder2;
                if (i4 == 2) {
                    z3 = false;
                    this.c.itemView.performHapticFeedback(0);
                    parent = this.r.getParent();
                    if (parent != null) {
                        if (this.c != null) {
                            z3 = true;
                        }
                        parent.requestDisallowInterceptTouchEvent(z3);
                    }
                    if (!z2) {
                        this.r.getLayoutManager().A();
                    }
                    this.m.a(this.c, this.n);
                    this.r.invalidate();
                }
            }
            z3 = false;
            parent = this.r.getParent();
            if (parent != null) {
            }
            if (!z2) {
            }
            this.m.a(this.c, this.n);
            this.r.invalidate();
        }
    }

    @DexIgnore
    public void d() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        this.t = VelocityTracker.obtain();
    }

    @DexIgnore
    public final void e() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.t = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c5, code lost:
        if (r1 > 0) goto L_0x00c9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0104 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0110  */
    public boolean f() {
        int i2;
        int i3;
        int i4;
        int width;
        if (this.c == null) {
            this.D = Long.MIN_VALUE;
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = this.D;
        long j3 = j2 == Long.MIN_VALUE ? 0 : currentTimeMillis - j2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        if (this.C == null) {
            this.C = new Rect();
        }
        layoutManager.a(this.c.itemView, this.C);
        if (layoutManager.a()) {
            int i5 = (int) (this.j + this.h);
            int paddingLeft = (i5 - this.C.left) - this.r.getPaddingLeft();
            if (this.h < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && paddingLeft < 0) {
                i2 = paddingLeft;
                if (layoutManager.b()) {
                }
                i3 = 0;
                if (i2 != 0) {
                }
                int i6 = i2;
                if (i3 == 0) {
                }
                if (i4 == 0) {
                }
                if (this.D == Long.MIN_VALUE) {
                }
                this.r.scrollBy(i4, i3);
                return true;
            } else if (this.h > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (width = ((i5 + this.c.itemView.getWidth()) + this.C.right) - (this.r.getWidth() - this.r.getPaddingRight())) > 0) {
                i2 = width;
                if (layoutManager.b()) {
                    int i7 = (int) (this.k + this.i);
                    int paddingTop = (i7 - this.C.top) - this.r.getPaddingTop();
                    if (this.i < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && paddingTop < 0) {
                        i3 = paddingTop;
                        if (i2 != 0) {
                            i2 = this.m.a(this.r, this.c.itemView.getWidth(), i2, this.r.getWidth(), j3);
                        }
                        int i62 = i2;
                        if (i3 == 0) {
                            int a2 = this.m.a(this.r, this.c.itemView.getHeight(), i3, this.r.getHeight(), j3);
                            i4 = i62;
                            i3 = a2;
                        } else {
                            i4 = i62;
                        }
                        if (i4 == 0 || i3 != 0) {
                            if (this.D == Long.MIN_VALUE) {
                                this.D = currentTimeMillis;
                            }
                            this.r.scrollBy(i4, i3);
                            return true;
                        }
                        this.D = Long.MIN_VALUE;
                        return false;
                    } else if (this.i > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        i3 = ((i7 + this.c.itemView.getHeight()) + this.C.bottom) - (this.r.getHeight() - this.r.getPaddingBottom());
                    }
                }
                i3 = 0;
                if (i2 != 0) {
                }
                int i622 = i2;
                if (i3 == 0) {
                }
                if (i4 == 0) {
                }
                if (this.D == Long.MIN_VALUE) {
                }
                this.r.scrollBy(i4, i3);
                return true;
            }
        }
        i2 = 0;
        if (layoutManager.b()) {
        }
        i3 = 0;
        if (i2 != 0) {
        }
        int i6222 = i2;
        if (i3 == 0) {
        }
        if (i4 == 0) {
        }
        if (this.D == Long.MIN_VALUE) {
        }
        this.r.scrollBy(i4, i3);
        return true;
    }

    @DexIgnore
    public final void g() {
        this.q = ViewConfiguration.get(this.r.getContext()).getScaledTouchSlop();
        this.r.addItemDecoration(this);
        this.r.addOnItemTouchListener(this.B);
        this.r.addOnChildAttachStateChangeListener(this);
        h();
    }

    @DexIgnore
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        rect.setEmpty();
    }

    @DexIgnore
    public final void h() {
        this.A = new g();
        this.z = new d9(this.r.getContext(), this.A);
    }

    @DexIgnore
    public final void i() {
        g gVar = this.A;
        if (gVar != null) {
            gVar.a();
            this.A = null;
        }
        if (this.z != null) {
            this.z = null;
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        this.y = -1;
        if (this.c != null) {
            a(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f2 = fArr[1];
            f3 = f4;
        } else {
            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.m.a(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        if (this.c != null) {
            a(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f2 = fArr[1];
            f3 = f4;
        } else {
            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.m.b(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {
        @DexIgnore
        public static /* final */ Interpolator b; // = new a();
        @DexIgnore
        public static /* final */ Interpolator c; // = new b();
        @DexIgnore
        public int a; // = -1;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class b implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        }

        @DexIgnore
        public static int b(int i, int i2) {
            int i3;
            int i4 = i & 789516;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 << 2;
            } else {
                int i6 = i4 << 1;
                i5 |= -789517 & i6;
                i3 = (i6 & 789516) << 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public static int c(int i, int i2) {
            return i2 << (i * 8);
        }

        @DexIgnore
        public static int d(int i, int i2) {
            int c2 = c(0, i2 | i);
            return c(2, i) | c(1, i2) | c2;
        }

        @DexIgnore
        public float a(float f) {
            return f;
        }

        @DexIgnore
        public float a(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public int a() {
            return 0;
        }

        @DexIgnore
        public int a(int i, int i2) {
            int i3;
            int i4 = i & 3158064;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 >> 2;
            } else {
                int i6 = i4 >> 1;
                i5 |= -3158065 & i6;
                i3 = (i6 & 3158064) >> 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public RecyclerView.ViewHolder a(RecyclerView.ViewHolder viewHolder, List<RecyclerView.ViewHolder> list, int i, int i2) {
            int i3;
            int bottom;
            int top;
            int abs;
            int left;
            int abs2;
            int right;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            int width = i + viewHolder2.itemView.getWidth();
            int height = i2 + viewHolder2.itemView.getHeight();
            int left2 = i - viewHolder2.itemView.getLeft();
            int top2 = i2 - viewHolder2.itemView.getTop();
            int size = list.size();
            RecyclerView.ViewHolder viewHolder3 = null;
            int i4 = -1;
            for (int i5 = 0; i5 < size; i5++) {
                RecyclerView.ViewHolder viewHolder4 = list.get(i5);
                if (left2 <= 0 || (right = viewHolder4.itemView.getRight() - width) >= 0 || viewHolder4.itemView.getRight() <= viewHolder2.itemView.getRight() || (i3 = Math.abs(right)) <= i4) {
                    i3 = i4;
                } else {
                    viewHolder3 = viewHolder4;
                }
                if (left2 < 0 && (left = viewHolder4.itemView.getLeft() - i) > 0 && viewHolder4.itemView.getLeft() < viewHolder2.itemView.getLeft() && (abs2 = Math.abs(left)) > i3) {
                    i3 = abs2;
                    viewHolder3 = viewHolder4;
                }
                if (top2 < 0 && (top = viewHolder4.itemView.getTop() - i2) > 0 && viewHolder4.itemView.getTop() < viewHolder2.itemView.getTop() && (abs = Math.abs(top)) > i3) {
                    i3 = abs;
                    viewHolder3 = viewHolder4;
                }
                if (top2 <= 0 || (bottom = viewHolder4.itemView.getBottom() - height) >= 0 || viewHolder4.itemView.getBottom() <= viewHolder2.itemView.getBottom() || (i4 = Math.abs(bottom)) <= i3) {
                    i4 = i3;
                } else {
                    viewHolder3 = viewHolder4;
                }
            }
            return viewHolder3;
        }

        @DexIgnore
        public boolean a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return true;
        }

        @DexIgnore
        public float b(float f) {
            return f;
        }

        @DexIgnore
        public float b(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public final int b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return a(c(recyclerView, viewHolder), x9.o(recyclerView));
        }

        @DexIgnore
        public abstract void b(RecyclerView.ViewHolder viewHolder, int i);

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public abstract boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2);

        @DexIgnore
        public abstract int c(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        public boolean c() {
            return true;
        }

        @DexIgnore
        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            Canvas canvas2 = canvas;
            List<h> list2 = list;
            int size = list.size();
            boolean z = false;
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list2.get(i2);
                int save = canvas.save();
                b(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                b(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            for (int i3 = size - 1; i3 >= 0; i3--) {
                h hVar2 = list2.get(i3);
                if (hVar2.l && !hVar2.h) {
                    list2.remove(i3);
                } else if (!hVar2.l) {
                    z = true;
                }
            }
            if (z) {
                recyclerView.invalidate();
            }
        }

        @DexIgnore
        public boolean d(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return (b(recyclerView, viewHolder) & 16711680) != 0;
        }

        @DexIgnore
        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            eg.a.a(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public void a(RecyclerView.ViewHolder viewHolder, int i) {
            if (viewHolder != null) {
                eg.a.b(viewHolder.itemView);
            }
        }

        @DexIgnore
        public final int a(RecyclerView recyclerView) {
            if (this.a == -1) {
                this.a = recyclerView.getResources().getDimensionPixelSize(pf.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.a;
        }

        @DexIgnore
        public void a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int i, RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4) {
            RecyclerView.m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof j) {
                ((j) layoutManager).a(viewHolder.itemView, viewHolder2.itemView, i3, i4);
                return;
            }
            if (layoutManager.a()) {
                if (layoutManager.f(viewHolder2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.i(viewHolder2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
            if (layoutManager.b()) {
                if (layoutManager.j(viewHolder2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.e(viewHolder2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
        }

        @DexIgnore
        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            Canvas canvas2 = canvas;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                hVar.c();
                int save = canvas.save();
                a(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                a(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        @DexIgnore
        public void a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            eg.a.a(viewHolder.itemView);
        }

        @DexIgnore
        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            eg.a.b(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public long a(RecyclerView recyclerView, int i, float f, float f2) {
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i == 8 ? 200 : 250;
            }
            if (i == 8) {
                return itemAnimator.getMoveDuration();
            }
            return itemAnimator.getRemoveDuration();
        }

        @DexIgnore
        public int a(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f = 1.0f;
            int signum = (int) (((float) (((int) Math.signum((float) i2)) * a(recyclerView))) * c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (((float) signum) * b.getInterpolation(f));
            if (interpolation == 0) {
                return i2 > 0 ? 1 : -1;
            }
            return interpolation;
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.r;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                b();
            }
            this.r = recyclerView;
            if (recyclerView != null) {
                Resources resources = recyclerView.getResources();
                this.f = resources.getDimension(pf.item_touch_helper_swipe_escape_velocity);
                this.g = resources.getDimension(pf.item_touch_helper_swipe_escape_max_velocity);
                g();
            }
        }
    }

    @DexIgnore
    public final void a(float[] fArr) {
        if ((this.o & 12) != 0) {
            fArr[0] = (this.j + this.h) - ((float) this.c.itemView.getLeft());
        } else {
            fArr[0] = this.c.itemView.getTranslationX();
        }
        if ((this.o & 3) != 0) {
            fArr[1] = (this.k + this.i) - ((float) this.c.itemView.getTop());
        } else {
            fArr[1] = this.c.itemView.getTranslationY();
        }
    }

    @DexIgnore
    public void b(RecyclerView.ViewHolder viewHolder) {
        if (!this.r.isLayoutRequested() && this.n == 2) {
            float a2 = this.m.a(viewHolder);
            int i2 = (int) (this.j + this.h);
            int i3 = (int) (this.k + this.i);
            if (((float) Math.abs(i3 - viewHolder.itemView.getTop())) >= ((float) viewHolder.itemView.getHeight()) * a2 || ((float) Math.abs(i2 - viewHolder.itemView.getLeft())) >= ((float) viewHolder.itemView.getWidth()) * a2) {
                List<RecyclerView.ViewHolder> a3 = a(viewHolder);
                if (a3.size() != 0) {
                    RecyclerView.ViewHolder a4 = this.m.a(viewHolder, a3, i2, i3);
                    if (a4 == null) {
                        this.u.clear();
                        this.v.clear();
                        return;
                    }
                    int adapterPosition = a4.getAdapterPosition();
                    int adapterPosition2 = viewHolder.getAdapterPosition();
                    if (this.m.b(this.r, viewHolder, a4)) {
                        this.m.a(this.r, viewHolder, adapterPosition2, a4, adapterPosition, i2, i3);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(h hVar, int i2) {
        this.r.post(new d(hVar, i2));
    }

    @DexIgnore
    public final List<RecyclerView.ViewHolder> a(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        List<RecyclerView.ViewHolder> list = this.u;
        if (list == null) {
            this.u = new ArrayList();
            this.v = new ArrayList();
        } else {
            list.clear();
            this.v.clear();
        }
        int a2 = this.m.a();
        int round = Math.round(this.j + this.h) - a2;
        int round2 = Math.round(this.k + this.i) - a2;
        int i2 = a2 * 2;
        int width = viewHolder2.itemView.getWidth() + round + i2;
        int height = viewHolder2.itemView.getHeight() + round2 + i2;
        int i3 = (round + width) / 2;
        int i4 = (round2 + height) / 2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int e2 = layoutManager.e();
        int i5 = 0;
        while (i5 < e2) {
            View d2 = layoutManager.d(i5);
            if (d2 != viewHolder2.itemView && d2.getBottom() >= round2 && d2.getTop() <= height && d2.getRight() >= round && d2.getLeft() <= width) {
                RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(d2);
                if (this.m.a(this.r, this.c, childViewHolder)) {
                    int abs = Math.abs(i3 - ((d2.getLeft() + d2.getRight()) / 2));
                    int abs2 = Math.abs(i4 - ((d2.getTop() + d2.getBottom()) / 2));
                    int i6 = (abs * abs) + (abs2 * abs2);
                    int size = this.u.size();
                    int i7 = 0;
                    int i8 = 0;
                    while (i7 < size && i6 > this.v.get(i7).intValue()) {
                        i8++;
                        i7++;
                        RecyclerView.ViewHolder viewHolder3 = viewHolder;
                    }
                    this.u.add(i8, childViewHolder);
                    this.v.add(i8, Integer.valueOf(i6));
                }
            }
            i5++;
            viewHolder2 = viewHolder;
        }
        return this.u;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements RecyclerView.p {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            cg.this.z.a(motionEvent);
            VelocityTracker velocityTracker = cg.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (cg.this.l != -1) {
                int actionMasked = motionEvent.getActionMasked();
                int findPointerIndex = motionEvent.findPointerIndex(cg.this.l);
                if (findPointerIndex >= 0) {
                    cg.this.a(actionMasked, motionEvent, findPointerIndex);
                }
                cg cgVar = cg.this;
                RecyclerView.ViewHolder viewHolder = cgVar.c;
                if (viewHolder != null) {
                    int i = 0;
                    if (actionMasked != 1) {
                        if (actionMasked != 2) {
                            if (actionMasked == 3) {
                                VelocityTracker velocityTracker2 = cgVar.t;
                                if (velocityTracker2 != null) {
                                    velocityTracker2.clear();
                                }
                            } else if (actionMasked == 6) {
                                int actionIndex = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex) == cg.this.l) {
                                    if (actionIndex == 0) {
                                        i = 1;
                                    }
                                    cg.this.l = motionEvent.getPointerId(i);
                                    cg cgVar2 = cg.this;
                                    cgVar2.a(motionEvent, cgVar2.o, actionIndex);
                                    return;
                                }
                                return;
                            } else {
                                return;
                            }
                        } else if (findPointerIndex >= 0) {
                            cgVar.a(motionEvent, cgVar.o, findPointerIndex);
                            cg.this.b(viewHolder);
                            cg cgVar3 = cg.this;
                            cgVar3.r.removeCallbacks(cgVar3.s);
                            cg.this.s.run();
                            cg.this.r.invalidate();
                            return;
                        } else {
                            return;
                        }
                    }
                    cg.this.c((RecyclerView.ViewHolder) null, 0);
                    cg.this.l = -1;
                }
            }
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            int findPointerIndex;
            h a2;
            cg.this.z.a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                cg.this.l = motionEvent.getPointerId(0);
                cg.this.d = motionEvent.getX();
                cg.this.e = motionEvent.getY();
                cg.this.d();
                cg cgVar = cg.this;
                if (cgVar.c == null && (a2 = cgVar.a(motionEvent)) != null) {
                    cg cgVar2 = cg.this;
                    cgVar2.d -= a2.i;
                    cgVar2.e -= a2.j;
                    cgVar2.a(a2.e, true);
                    if (cg.this.a.remove(a2.e.itemView)) {
                        cg cgVar3 = cg.this;
                        cgVar3.m.a(cgVar3.r, a2.e);
                    }
                    cg.this.c(a2.e, a2.f);
                    cg cgVar4 = cg.this;
                    cgVar4.a(motionEvent, cgVar4.o, 0);
                }
            } else if (actionMasked == 3 || actionMasked == 1) {
                cg cgVar5 = cg.this;
                cgVar5.l = -1;
                cgVar5.c((RecyclerView.ViewHolder) null, 0);
            } else {
                int i = cg.this.l;
                if (i != -1 && (findPointerIndex = motionEvent.findPointerIndex(i)) >= 0) {
                    cg.this.a(actionMasked, motionEvent, findPointerIndex);
                }
            }
            VelocityTracker velocityTracker = cg.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (cg.this.c != null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                cg.this.c((RecyclerView.ViewHolder) null, 0);
            }
        }
    }

    @DexIgnore
    public View b(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        RecyclerView.ViewHolder viewHolder = this.c;
        if (viewHolder != null) {
            View view = viewHolder.itemView;
            if (a(view, x2, y2, this.j + this.h, this.k + this.i)) {
                return view;
            }
        }
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            View view2 = hVar.e.itemView;
            if (a(view2, x2, y2, hVar.i, hVar.j)) {
                return view2;
            }
        }
        return this.r.findChildViewUnder(x2, y2);
    }

    @DexIgnore
    public boolean c() {
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!this.p.get(i2).l) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int b(RecyclerView.ViewHolder viewHolder, int i2) {
        if ((i2 & 3) == 0) {
            return 0;
        }
        int i3 = 2;
        int i4 = this.i > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 2 : 1;
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null && this.l > -1) {
            f fVar = this.m;
            float f2 = this.g;
            fVar.b(f2);
            velocityTracker.computeCurrentVelocity(1000, f2);
            float xVelocity = this.t.getXVelocity(this.l);
            float yVelocity = this.t.getYVelocity(this.l);
            if (yVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = 1;
            }
            float abs = Math.abs(yVelocity);
            if ((i3 & i2) != 0 && i3 == i4) {
                f fVar2 = this.m;
                float f3 = this.f;
                fVar2.a(f3);
                if (abs >= f3 && abs > Math.abs(xVelocity)) {
                    return i3;
                }
            }
        }
        float height = ((float) this.r.getHeight()) * this.m.b(viewHolder);
        if ((i2 & i4) == 0 || Math.abs(this.i) <= height) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public final RecyclerView.ViewHolder c(MotionEvent motionEvent) {
        View b2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int i2 = this.l;
        if (i2 == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i2);
        float abs = Math.abs(motionEvent.getX(findPointerIndex) - this.d);
        float abs2 = Math.abs(motionEvent.getY(findPointerIndex) - this.e);
        int i3 = this.q;
        if (abs < ((float) i3) && abs2 < ((float) i3)) {
            return null;
        }
        if (abs > abs2 && layoutManager.a()) {
            return null;
        }
        if ((abs2 <= abs || !layoutManager.b()) && (b2 = b(motionEvent)) != null) {
            return this.r.getChildViewHolder(b2);
        }
        return null;
    }

    @DexIgnore
    public void a(View view) {
        c(view);
        RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(view);
        if (childViewHolder != null) {
            RecyclerView.ViewHolder viewHolder = this.c;
            if (viewHolder == null || childViewHolder != viewHolder) {
                a(childViewHolder, false);
                if (this.a.remove(childViewHolder.itemView)) {
                    this.m.a(this.r, childViewHolder);
                    return;
                }
                return;
            }
            c((RecyclerView.ViewHolder) null, 0);
        }
    }

    @DexIgnore
    public final int c(RecyclerView.ViewHolder viewHolder) {
        if (this.n == 2) {
            return 0;
        }
        int c2 = this.m.c(this.r, viewHolder);
        int a2 = (this.m.a(c2, x9.o(this.r)) & 65280) >> 8;
        if (a2 == 0) {
            return 0;
        }
        int i2 = (c2 & 65280) >> 8;
        if (Math.abs(this.h) > Math.abs(this.i)) {
            int a3 = a(viewHolder, a2);
            if (a3 > 0) {
                return (i2 & a3) == 0 ? f.b(a3, x9.o(this.r)) : a3;
            }
            int b2 = b(viewHolder, a2);
            if (b2 > 0) {
                return b2;
            }
        } else {
            int b3 = b(viewHolder, a2);
            if (b3 > 0) {
                return b3;
            }
            int a4 = a(viewHolder, a2);
            if (a4 > 0) {
                return (i2 & a4) == 0 ? f.b(a4, x9.o(this.r)) : a4;
            }
        }
        return 0;
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder, boolean z2) {
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e == viewHolder) {
                hVar.k |= z2;
                if (!hVar.l) {
                    hVar.a();
                }
                this.p.remove(size);
                return;
            }
        }
    }

    @DexIgnore
    public void a(int i2, MotionEvent motionEvent, int i3) {
        RecyclerView.ViewHolder c2;
        int b2;
        if (this.c == null && i2 == 2 && this.n != 2 && this.m.b() && this.r.getScrollState() != 1 && (c2 = c(motionEvent)) != null && (b2 = (this.m.b(this.r, c2) & 65280) >> 8) != 0) {
            float x2 = motionEvent.getX(i3);
            float y2 = motionEvent.getY(i3);
            float f2 = x2 - this.d;
            float f3 = y2 - this.e;
            float abs = Math.abs(f2);
            float abs2 = Math.abs(f3);
            int i4 = this.q;
            if (abs >= ((float) i4) || abs2 >= ((float) i4)) {
                if (abs > abs2) {
                    if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 4) == 0) {
                        return;
                    }
                    if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 8) == 0) {
                        return;
                    }
                } else if (f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 1) == 0) {
                    return;
                } else {
                    if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 2) == 0) {
                        return;
                    }
                }
                this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.l = motionEvent.getPointerId(0);
                c(c2, 1);
            }
        }
    }

    @DexIgnore
    public void c(View view) {
        if (view == this.x) {
            this.x = null;
            if (this.w != null) {
                this.r.setChildDrawingOrderCallback((RecyclerView.h) null);
            }
        }
    }

    @DexIgnore
    public h a(MotionEvent motionEvent) {
        if (this.p.isEmpty()) {
            return null;
        }
        View b2 = b(motionEvent);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e.itemView == b2) {
                return hVar;
            }
        }
        return null;
    }

    @DexIgnore
    public void a(MotionEvent motionEvent, int i2, int i3) {
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        this.h = x2 - this.d;
        this.i = y2 - this.e;
        if ((i2 & 4) == 0) {
            this.h = Math.max(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.h);
        }
        if ((i2 & 8) == 0) {
            this.h = Math.min(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.h);
        }
        if ((i2 & 1) == 0) {
            this.i = Math.max(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
        if ((i2 & 2) == 0) {
            this.i = Math.min(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
    }

    @DexIgnore
    public final int a(RecyclerView.ViewHolder viewHolder, int i2) {
        if ((i2 & 12) == 0) {
            return 0;
        }
        int i3 = 8;
        int i4 = this.h > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 8 : 4;
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null && this.l > -1) {
            f fVar = this.m;
            float f2 = this.g;
            fVar.b(f2);
            velocityTracker.computeCurrentVelocity(1000, f2);
            float xVelocity = this.t.getXVelocity(this.l);
            float yVelocity = this.t.getYVelocity(this.l);
            if (xVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = 4;
            }
            float abs = Math.abs(xVelocity);
            if ((i3 & i2) != 0 && i4 == i3) {
                f fVar2 = this.m;
                float f3 = this.f;
                fVar2.a(f3);
                if (abs >= f3 && abs > Math.abs(yVelocity)) {
                    return i3;
                }
            }
        }
        float width = ((float) this.r.getWidth()) * this.m.b(viewHolder);
        if ((i2 & i4) == 0 || Math.abs(this.h) <= width) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public final void a() {
        if (Build.VERSION.SDK_INT < 21) {
            if (this.w == null) {
                this.w = new e();
            }
            this.r.setChildDrawingOrderCallback(this.w);
        }
    }
}
