package com.fossil;

import com.fossil.dr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lv {
    @DexIgnore
    public /* final */ nv a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Map<Class<?>, C0029a<?>> a; // = new HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lv$a$a")
        /* renamed from: com.fossil.lv$a$a  reason: collision with other inner class name */
        public static class C0029a<Model> {
            @DexIgnore
            public /* final */ List<jv<Model, ?>> a;

            @DexIgnore
            public C0029a(List<jv<Model, ?>> list) {
                this.a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.a.clear();
        }

        @DexIgnore
        public <Model> void a(Class<Model> cls, List<jv<Model, ?>> list) {
            if (this.a.put(cls, new C0029a(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @DexIgnore
        public <Model> List<jv<Model, ?>> a(Class<Model> cls) {
            C0029a aVar = this.a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.a;
        }
    }

    @DexIgnore
    public lv(v8<List<Throwable>> v8Var) {
        this(new nv(v8Var));
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, kv<? extends Model, ? extends Data> kvVar) {
        this.a.a(cls, cls2, kvVar);
        this.b.a();
    }

    @DexIgnore
    public final synchronized <A> List<jv<A, ?>> b(Class<A> cls) {
        List<jv<A, ?>> a2;
        a2 = this.b.a(cls);
        if (a2 == null) {
            a2 = Collections.unmodifiableList(this.a.a(cls));
            this.b.a(cls, a2);
        }
        return a2;
    }

    @DexIgnore
    public lv(nv nvVar) {
        this.b = new a();
        this.a = nvVar;
    }

    @DexIgnore
    public <A> List<jv<A, ?>> a(A a2) {
        List b2 = b(b(a2));
        if (!b2.isEmpty()) {
            int size = b2.size();
            List<jv<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            for (int i = 0; i < size; i++) {
                jv jvVar = (jv) b2.get(i);
                if (jvVar.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(jvVar);
                }
            }
            if (!emptyList.isEmpty()) {
                return emptyList;
            }
            throw new dr.c(a2, b2);
        }
        throw new dr.c(a2);
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return a2.getClass();
    }

    @DexIgnore
    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.a.b(cls);
    }
}
