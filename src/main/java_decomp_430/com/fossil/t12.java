package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t12 {
    @DexIgnore
    public static /* final */ l12 b; // = new l12("LibraryVersion", "");
    @DexIgnore
    public static t12 c; // = new t12();
    @DexIgnore
    public ConcurrentHashMap<String, String> a; // = new ConcurrentHashMap<>();

    @DexIgnore
    public static t12 a() {
        return c;
    }

    @DexIgnore
    public String a(String str) {
        w12.a(str, (Object) "Please provide a valid libraryName");
        if (this.a.containsKey(str)) {
            return this.a.get(str);
        }
        Properties properties = new Properties();
        String str2 = null;
        try {
            InputStream resourceAsStream = t12.class.getResourceAsStream(String.format("/%s.properties", new Object[]{str}));
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
                str2 = properties.getProperty("version", (String) null);
                l12 l12 = b;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12 + String.valueOf(str2).length());
                sb.append(str);
                sb.append(" version is ");
                sb.append(str2);
                l12.c("LibraryVersion", sb.toString());
            } else {
                l12 l122 = b;
                String valueOf = String.valueOf(str);
                l122.b("LibraryVersion", valueOf.length() != 0 ? "Failed to get app version for libraryName: ".concat(valueOf) : new String("Failed to get app version for libraryName: "));
            }
        } catch (IOException e) {
            l12 l123 = b;
            String valueOf2 = String.valueOf(str);
            l123.a("LibraryVersion", valueOf2.length() != 0 ? "Failed to get app version for libraryName: ".concat(valueOf2) : new String("Failed to get app version for libraryName: "), e);
        }
        if (str2 == null) {
            b.a("LibraryVersion", ".properties file is dropped during release process. Failure to read app version isexpected druing Google internal testing where locally-built libraries are used");
            str2 = "UNKNOWN";
        }
        this.a.put(str, str2);
        return str2;
    }
}
