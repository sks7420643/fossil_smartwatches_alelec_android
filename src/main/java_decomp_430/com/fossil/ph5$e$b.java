package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$samples$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ph5$e$b extends sf6 implements ig6<il6, xe6<? super List<ActivitySample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityDetailPresenter.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ph5$e$b(ActivityDetailPresenter.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ph5$e$b ph5_e_b = new ph5$e$b(this.this$0, xe6);
        ph5_e_b.p$ = (il6) obj;
        return ph5_e_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ph5$e$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0;
            return activityDetailPresenter.a(activityDetailPresenter.g, (List<ActivitySample>) this.this$0.this$0.l);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
