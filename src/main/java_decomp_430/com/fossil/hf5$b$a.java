package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {51, 59, 66}, m = "invokeSuspend")
public final class hf5$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $data;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public long J$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewDayPresenter.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hf5$b$a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hf5$b$a$a$a")
        /* renamed from: com.fossil.hf5$b$a$a$a  reason: collision with other inner class name */
        public static final class C0013a<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ue6.a(Long.valueOf(((HeartRateSample) t).getStartTimeId().getMillis()), Long.valueOf(((HeartRateSample) t2).getStartTimeId().getMillis()));
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hf5$b$a hf5_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hf5_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                List list = this.this$0.$data;
                if (list == null) {
                    return null;
                }
                if (list.size() > 1) {
                    ud6.a(list, new C0013a());
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $listTimeZoneChange;
        @DexIgnore
        public /* final */ /* synthetic */ List $listTodayHeartRateModel;
        @DexIgnore
        public /* final */ /* synthetic */ int $maxHR;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $previousTimeZoneOffset;
        @DexIgnore
        public /* final */ /* synthetic */ long $startOfDay;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hf5$b$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hf5$b$a hf5_b_a, long j, hh6 hh6, List list, List list2, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hf5_b_a;
            this.$startOfDay = j;
            this.$previousTimeZoneOffset = hh6;
            this.$listTimeZoneChange = list;
            this.$listTodayHeartRateModel = list2;
            this.$maxHR = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Iterator it;
            char c;
            StringBuilder sb;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                List list = this.this$0.$data;
                if (list == null) {
                    return null;
                }
                for (Iterator it2 = list.iterator(); it2.hasNext(); it2 = it) {
                    HeartRateSample heartRateSample = (HeartRateSample) it2.next();
                    long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                    long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / 60000;
                    long j = (millis2 + millis) / ((long) 2);
                    if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                        int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                        String a = zk4.a(hourOfDay);
                        nh6 nh6 = nh6.a;
                        Object[] objArr = {hf6.a(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / DateTimeConstants.SECONDS_PER_HOUR)), hf6.a(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        List list2 = this.$listTimeZoneChange;
                        Integer a2 = hf6.a((int) j);
                        it = it2;
                        lc6 lc6 = new lc6(hf6.a(hourOfDay), hf6.a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(a);
                        if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                            sb = new StringBuilder();
                            c = '+';
                        } else {
                            sb = new StringBuilder();
                            c = '-';
                        }
                        sb.append(c);
                        sb.append(format);
                        sb2.append(sb.toString());
                        list2.add(new pc6(a2, lc6, sb2.toString()));
                        this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                    } else {
                        it = it2;
                    }
                    if (!this.$listTodayHeartRateModel.isEmpty()) {
                        jz5 jz5 = (jz5) yd6.f(this.$listTodayHeartRateModel);
                        if (millis - ((long) jz5.a()) > 1) {
                            this.$listTodayHeartRateModel.add(new jz5(0, 0, 0, jz5.a(), (int) millis, (int) j));
                        }
                    }
                    int average = (int) heartRateSample.getAverage();
                    if (heartRateSample.getMax() == this.$maxHR) {
                        average = heartRateSample.getMax();
                    }
                    this.$listTodayHeartRateModel.add(new jz5(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j));
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super Integer>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hf5$b$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(hf5$b$a hf5_b_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hf5_b_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            int i;
            Object obj2;
            Integer a;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                List list = this.this$0.$data;
                if (list != null) {
                    Iterator it = list.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        obj2 = it.next();
                        if (it.hasNext()) {
                            Integer a2 = hf6.a(((HeartRateSample) obj2).getMax());
                            do {
                                Object next = it.next();
                                Integer a3 = hf6.a(((HeartRateSample) next).getMax());
                                if (a2.compareTo(a3) < 0) {
                                    obj2 = next;
                                    a2 = a3;
                                }
                            } while (it.hasNext());
                        }
                    }
                    HeartRateSample heartRateSample = (HeartRateSample) obj2;
                    if (!(heartRateSample == null || (a = hf6.a(heartRateSample.getMax())) == null)) {
                        i = a.intValue();
                        return hf6.a(i);
                    }
                }
                i = 0;
                return hf6.a(i);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hf5$b$a(HeartRateOverviewDayPresenter.b bVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$data = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        hf5$b$a hf5_b_a = new hf5$b$a(this.this$0, this.$data, xe6);
        hf5_b_a.p$ = (il6) obj;
        return hf5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((hf5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x012c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0352  */
    public final Object invokeSuspend(Object obj) {
        List list;
        List list2;
        int i;
        int i2;
        long j;
        List list3;
        il6 il6;
        Object obj2;
        dl6 a2;
        b bVar;
        Object obj3;
        il6 il62;
        List list4;
        long j2;
        Object a3 = ff6.a();
        int i3 = this.label;
        if (i3 == 0) {
            nc6.a(obj);
            il6 il63 = this.p$;
            ArrayList arrayList = new ArrayList();
            dl6 a4 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il63;
            this.L$1 = arrayList;
            this.label = 1;
            if (gk6.a(a4, aVar, this) == a3) {
                return a3;
            }
            ArrayList arrayList2 = arrayList;
            il62 = il63;
            list4 = arrayList2;
        } else if (i3 == 1) {
            list4 = (List) this.L$1;
            il62 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i3 == 2) {
            long j3 = this.J$0;
            nc6.a(obj);
            j = j3;
            list3 = (List) this.L$1;
            il6 = (il6) this.L$0;
            obj2 = obj;
            int intValue = ((Number) obj2).intValue();
            hh6 hh6 = new hh6();
            hh6.element = -1;
            ArrayList arrayList3 = new ArrayList();
            a2 = this.this$0.a.b();
            ArrayList arrayList4 = arrayList3;
            int i4 = intValue;
            bVar = r0;
            List list5 = list3;
            b bVar2 = new b(this, j, hh6, arrayList4, list3, i4, (xe6) null);
            this.L$0 = il6;
            this.L$1 = list5;
            this.J$0 = j;
            this.I$0 = i4;
            this.L$2 = hh6;
            list2 = arrayList4;
            this.L$3 = list2;
            this.label = 3;
            obj3 = a3;
            if (gk6.a(a2, bVar, this) != obj3) {
                return obj3;
            }
            list = list5;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + yi4.a(list2));
            if (!(!list2.isEmpty()) || list2.size() <= 1) {
            }
            int i5 = i + DateTimeConstants.MINUTES_PER_DAY;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i5);
            this.this$0.a.h.a(i5, list, list2);
            return cd6.a;
        } else if (i3 == 3) {
            list2 = (List) this.L$3;
            hh6 hh62 = (hh6) this.L$2;
            list = (List) this.L$1;
            il6 il64 = (il6) this.L$0;
            nc6.a(obj);
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateOverviewDayPresenter", "listTimeZoneChange=" + yi4.a(list2));
            if (!(!list2.isEmpty()) || list2.size() <= 1) {
                i = 0;
                list2.clear();
            } else {
                float floatValue = ((Number) ((lc6) ((pc6) list2.get(0)).getSecond()).getSecond()).floatValue();
                float floatValue2 = ((Number) ((lc6) ((pc6) list2.get(qd6.a(list2))).getSecond()).getSecond()).floatValue();
                float f = (float) 0;
                if ((floatValue >= f || floatValue2 >= f) && (floatValue < f || floatValue2 < f)) {
                    i2 = Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                    if (floatValue2 < f) {
                        i2 = -i2;
                    }
                } else {
                    i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                }
                int i6 = i2;
                ArrayList<pc6> arrayList5 = new ArrayList<>();
                for (Object next : list2) {
                    if (hf6.a(((Number) ((lc6) ((pc6) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                        arrayList5.add(next);
                    }
                }
                ArrayList<lc6> arrayList6 = new ArrayList<>(rd6.a(arrayList5, 10));
                for (pc6 second : arrayList5) {
                    arrayList6.add((lc6) second.getSecond());
                }
                ArrayList arrayList7 = new ArrayList(rd6.a(arrayList6, 10));
                for (lc6 first : arrayList6) {
                    arrayList7.add(hf6.a(((Number) first.getFirst()).intValue()));
                }
                if (!arrayList7.contains(hf6.a(0))) {
                    String a5 = this.this$0.a.a(hf6.a(floatValue));
                    Integer a6 = hf6.a(0);
                    lc6 lc6 = new lc6(hf6.a(0), hf6.a(floatValue));
                    list2.add(0, new pc6(a6, lc6, "12a" + a5));
                }
                ArrayList<pc6> arrayList8 = new ArrayList<>();
                for (Object next2 : list2) {
                    if (hf6.a(((Number) ((lc6) ((pc6) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                        arrayList8.add(next2);
                    }
                }
                ArrayList<lc6> arrayList9 = new ArrayList<>(rd6.a(arrayList8, 10));
                for (pc6 second2 : arrayList8) {
                    arrayList9.add((lc6) second2.getSecond());
                }
                ArrayList arrayList10 = new ArrayList(rd6.a(arrayList9, 10));
                for (lc6 first2 : arrayList9) {
                    arrayList10.add(hf6.a(((Number) first2.getFirst()).intValue()));
                }
                if (!arrayList10.contains(hf6.a(24))) {
                    String a7 = this.this$0.a.a(hf6.a(floatValue2));
                    Integer a8 = hf6.a(i6 + DateTimeConstants.MINUTES_PER_DAY);
                    lc6 lc62 = new lc6(hf6.a(24), hf6.a(floatValue2));
                    list2.add(new pc6(a8, lc62, "12a" + a7));
                }
                i = i6;
            }
            int i52 = i + DateTimeConstants.MINUTES_PER_DAY;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateOverviewDayPresenter", "minutesOfDay=" + i52);
            this.this$0.a.h.a(i52, list, list2);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List list6 = this.$data;
        if (list6 == null || !(!list6.isEmpty())) {
            Date o = bk4.o(HeartRateOverviewDayPresenter.b(this.this$0.a));
            wg6.a((Object) o, "DateHelper.getStartOfDay(mDate)");
            j2 = o.getTime();
        } else {
            DateTime withTimeAtStartOfDay = ((HeartRateSample) this.$data.get(0)).getStartTimeId().withTimeAtStartOfDay();
            wg6.a((Object) withTimeAtStartOfDay, "data[0].getStartTimeId().withTimeAtStartOfDay()");
            j2 = withTimeAtStartOfDay.getMillis();
        }
        dl6 a9 = this.this$0.a.b();
        c cVar = new c(this, (xe6) null);
        this.L$0 = il62;
        this.L$1 = list4;
        this.J$0 = j2;
        this.label = 2;
        obj2 = gk6.a(a9, cVar, this);
        if (obj2 == a3) {
            return a3;
        }
        list3 = list4;
        il6 = il62;
        j = j2;
        int intValue2 = ((Number) obj2).intValue();
        hh6 hh63 = new hh6();
        hh63.element = -1;
        ArrayList arrayList32 = new ArrayList();
        a2 = this.this$0.a.b();
        ArrayList arrayList42 = arrayList32;
        int i42 = intValue2;
        bVar = bVar2;
        List list52 = list3;
        b bVar22 = new b(this, j, hh63, arrayList42, list3, i42, (xe6) null);
        this.L$0 = il6;
        this.L$1 = list52;
        this.J$0 = j;
        this.I$0 = i42;
        this.L$2 = hh63;
        list2 = arrayList42;
        this.L$3 = list2;
        this.label = 3;
        obj3 = a3;
        if (gk6.a(a2, bVar, this) != obj3) {
        }
    }
}
