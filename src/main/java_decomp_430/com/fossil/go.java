package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.am;
import com.fossil.wl;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class go implements Runnable {
    @DexIgnore
    public /* final */ fm a; // = new fm();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends go {
        @DexIgnore
        public /* final */ /* synthetic */ lm b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public a(lm lmVar, String str, boolean z) {
            this.b = lmVar;
            this.c = str;
            this.d = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public void a() {
            WorkDatabase g = this.b.g();
            g.beginTransaction();
            try {
                for (String a : g.d().c(this.c)) {
                    a(this.b, a);
                }
                g.setTransactionSuccessful();
                g.endTransaction();
                if (this.d) {
                    a(this.b);
                }
            } catch (Throwable th) {
                g.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void a(lm lmVar, String str) {
        a(lmVar.g(), str);
        lmVar.e().d(str);
        for (hm a2 : lmVar.f()) {
            a2.a(str);
        }
    }

    @DexIgnore
    public void run() {
        try {
            a();
            this.a.a(wl.a);
        } catch (Throwable th) {
            this.a.a(new wl.b.a(th));
        }
    }

    @DexIgnore
    public void a(lm lmVar) {
        im.a(lmVar.c(), lmVar.g(), lmVar.f());
    }

    @DexIgnore
    public final void a(WorkDatabase workDatabase, String str) {
        ao d = workDatabase.d();
        rn a2 = workDatabase.a();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            am.a d2 = d.d(str2);
            if (!(d2 == am.a.SUCCEEDED || d2 == am.a.FAILED)) {
                d.a(am.a.CANCELLED, str2);
            }
            linkedList.addAll(a2.a(str2));
        }
    }

    @DexIgnore
    public static go a(String str, lm lmVar, boolean z) {
        return new a(lmVar, str, z);
    }
}
