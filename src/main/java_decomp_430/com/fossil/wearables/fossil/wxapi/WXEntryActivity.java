package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.c36;
import com.fossil.hy5;
import com.fossil.m26;
import com.fossil.n26;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WXEntryActivity extends Activity implements c36 {
    /*
    static {
        Class<WXEntryActivity> cls = WXEntryActivity.class;
    }
    */

    @DexIgnore
    public void a(m26 m26) {
        hy5.a().a(m26);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        hy5.a().a(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        hy5.a().a(getIntent(), this);
    }

    @DexIgnore
    public void a(n26 n26) {
        hy5.a().a(n26);
        finish();
    }
}
