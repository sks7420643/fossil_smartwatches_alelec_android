package com.fossil.wearables.fsl.sleep;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "sleep_date")
public class MFSleepDay extends BaseModel {
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_GOAL_MINUTES; // = "goalMinutes";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_OWNER; // = "owner";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_SLEEP_MINUTES; // = "sleepMinutes";
    @DexIgnore
    public static /* final */ String COLUMN_SLEEP_STATE_DIST_IN_MINUTE; // = "sleepStateDistInMinute";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_OFFSET; // = "timezoneOffset";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sleep_date";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public DateTime createdAt;
    @DexIgnore
    @DatabaseField(columnName = "date")
    public String date;
    @DexIgnore
    @DatabaseField(columnName = "goalMinutes")
    public int goalMinutes;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "owner")
    public String owner;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = "sleepMinutes")
    public int sleepMinutes;
    @DexIgnore
    @DatabaseField(columnName = "sleepStateDistInMinute")
    public String sleepStateDistInMinute;
    @DexIgnore
    @DatabaseField(columnName = "timezoneOffset")
    public int timezoneOffset;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public DateTime updatedAt;

    @DexIgnore
    public MFSleepDay() {
    }

    @DexIgnore
    public DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getDate() {
        return this.date;
    }

    @DexIgnore
    public int getGoalMinutes() {
        return this.goalMinutes;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public String getOwner() {
        return this.owner;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public int getSleepMinutes() {
        return this.sleepMinutes;
    }

    @DexIgnore
    public String getSleepStateDistInMinute() {
        return this.sleepStateDistInMinute;
    }

    @DexIgnore
    public int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public void setDate(String str) {
        this.date = str;
    }

    @DexIgnore
    public void setGoalMinutes(int i) {
        this.goalMinutes = i;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setOwner(String str) {
        this.owner = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setSleepMinutes(int i) {
        this.sleepMinutes = i;
    }

    @DexIgnore
    public void setSleepStateDistInMinute(String str) {
        this.sleepStateDistInMinute = str;
    }

    @DexIgnore
    public void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepDay{date='" + this.date + '\'' + ", timezoneOffset=" + this.timezoneOffset + ", goalMinutes=" + this.goalMinutes + ", sleepMinutes=" + this.sleepMinutes + ", sleepStateDistInMinute='" + this.sleepStateDistInMinute + '\'' + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", objectId='" + this.objectId + '\'' + ", owner='" + this.owner + '\'' + ", pinType=" + this.pinType + '\'' + '}';
    }

    @DexIgnore
    public MFSleepDay(String str, int i, int i2, int i3, String str2, DateTime dateTime) {
        this.date = str;
        this.timezoneOffset = i;
        this.goalMinutes = i2;
        this.sleepMinutes = i3;
        this.sleepStateDistInMinute = str2;
        this.updatedAt = dateTime;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepDay(String str, int i, int i2, int i3, String str2, DateTime dateTime, DateTime dateTime2, String str3, String str4) {
        this.date = str;
        this.timezoneOffset = i;
        this.goalMinutes = i2;
        this.sleepMinutes = i3;
        this.sleepStateDistInMinute = str2;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.objectId = str3;
        this.owner = str4;
        this.pinType = 0;
    }
}
