package com.fossil.wearables.fsl.codeword;

import android.text.TextUtils;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WordGroup extends BaseFeatureModel {
    @DexIgnore
    public static /* final */ Pattern LatinCharacterPattern; // = Pattern.compile("\\p{InBasic_Latin}+");
    @DexIgnore
    public static /* final */ Pattern NonLatinCharacterPattern; // = Pattern.compile("[^\\p{InBasic_Latin}]+");
    @DexIgnore
    public static /* final */ char[] charsToIgnore; // = {'!', '?', '\'', '\\', '\"', ';', ':', '.', ',', 8206, 8207};
    @DexIgnore
    public static /* final */ Pattern emojiPattern; // = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", 66);
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<Word> words;

    @DexIgnore
    public static boolean codeWordIsInString(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String str3 = str2;
        String str4 = str;
        int i = 0;
        while (true) {
            char[] cArr = charsToIgnore;
            if (i >= cArr.length) {
                break;
            }
            if (str3.indexOf(cArr[i]) != -1) {
                str3 = str3.replace(Character.toString(charsToIgnore[i]), "");
            }
            if (str4.indexOf(charsToIgnore[i]) != -1) {
                str4 = str4.replace(Character.toString(charsToIgnore[i]), "");
            }
            i++;
        }
        if (str4.isEmpty()) {
            return false;
        }
        if (emojiPattern.matcher(str4).find()) {
            Log.i("WordGroup", "emoji found");
            return str3.contains(str4);
        }
        if (LatinCharacterPattern.matcher(str4).find()) {
            Log.i("WordGroup", "Latin found");
            String[] split = str3.split(" ");
            for (String equalsIgnoreCase : split) {
                if (str4.equalsIgnoreCase(equalsIgnoreCase)) {
                    return true;
                }
            }
        }
        if (!NonLatinCharacterPattern.matcher(str4).find()) {
            return false;
        }
        Log.i("WordGroup", "Non Latin found");
        return str3.contains(str4);
    }

    @DexIgnore
    public static final boolean isValidWord(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String str2 = str;
        int i = 0;
        while (true) {
            char[] cArr = charsToIgnore;
            if (i >= cArr.length) {
                break;
            }
            if (str2.indexOf(cArr[i]) != -1) {
                str2 = str2.replace(Character.toString(charsToIgnore[i]), "");
            }
            i++;
        }
        if (TextUtils.isEmpty(str2)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public List<Word> containsString(String str) {
        ArrayList arrayList = new ArrayList();
        List<Word> words2 = getWords();
        for (int i = 0; i < words2.size(); i++) {
            Word word = words2.get(i);
            if (codeWordIsInString(word.getValue(), str)) {
                arrayList.add(word);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<Word> getWords() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<Word> foreignCollection = this.words;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.words);
    }

    @DexIgnore
    public void setWords(ForeignCollection<Word> foreignCollection) {
        this.words = foreignCollection;
    }
}
