package com.fossil.wearables.fsl.dial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DialListItem {
    @DexIgnore
    int getDbRowId();

    @DexIgnore
    String getDisplayName();

    @DexIgnore
    Bitmap getThumbnailBitmap();

    @DexIgnore
    Drawable getThumbnailDrawable(Context context);
}
