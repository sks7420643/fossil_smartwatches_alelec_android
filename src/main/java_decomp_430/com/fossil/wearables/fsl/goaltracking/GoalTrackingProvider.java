package com.fossil.wearables.fsl.goaltracking;

import android.util.SparseArray;
import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GoalTrackingProvider extends BaseProvider {
    @DexIgnore
    void deleteAllGoalPhases(long j);

    @DexIgnore
    void deleteGoalTrackingSummary(long j);

    @DexIgnore
    void disposeGoalTrackingFromDB(long j);

    @DexIgnore
    void disposeGoalTrackingFromDB(String str);

    @DexIgnore
    GoalTracking getActiveGoalTracking();

    @DexIgnore
    List<GoalTracking> getAllGoalTrackings();

    @DexIgnore
    GoalPhase getGoalPhase(long j);

    @DexIgnore
    List<GoalPhase> getGoalPhases(long j);

    @DexIgnore
    List<GoalPhase> getGoalPhases(long j, long j2);

    @DexIgnore
    GoalTracking getGoalTracking(long j);

    @DexIgnore
    GoalTracking getGoalTracking(String str);

    @DexIgnore
    GoalTracking getGoalTracking(String str, Frequency frequency, int i);

    @DexIgnore
    List<GoalTracking> getGoalTracking(GoalStatus goalStatus);

    @DexIgnore
    GoalTracking getGoalTrackingByServerId(String str);

    @DexIgnore
    SparseArray<List<GoalTrackingEvent>> getGoalTrackingEvent(int i, int i2, long j);

    @DexIgnore
    SparseArray<List<GoalTrackingEvent>> getGoalTrackingEvent(long j);

    @DexIgnore
    List<GoalTrackingEvent> getGoalTrackingEventByTrackingTimes(List<Long> list);

    @DexIgnore
    List<Integer> getGoalTrackingEventDates(int i, int i2, long j);

    @DexIgnore
    List<Integer> getGoalTrackingEventDates(long j);

    @DexIgnore
    List<GoalTrackingSummary> getGoalTrackingSummaries(long j, long j2);

    @DexIgnore
    List<GoalTrackingSummary> getGoalTrackingSummariesPaging(long j, long j2);

    @DexIgnore
    GoalTrackingSummary getGoalTrackingSummary(long j);

    @DexIgnore
    GoalTracking getGoalTrackingsByIdIncludeDeleted(long j);

    @DexIgnore
    GoalTracking getGoalTrackingsByServerIdIncludeDeleted(String str);

    @DexIgnore
    GoalTracking getLatestEndedGoalTracking();

    @DexIgnore
    GoalTracking getOldedGoalTracking();

    @DexIgnore
    List<GoalTracking> getPendingGoalTracking();

    @DexIgnore
    List<GoalTrackingEvent> getPendingGoalTrackingEvents();

    @DexIgnore
    void saveGoalPhase(GoalPhase goalPhase);

    @DexIgnore
    void saveGoalTracking(GoalTracking goalTracking);

    @DexIgnore
    void saveGoalTrackingEvent(GoalTrackingEvent goalTrackingEvent);

    @DexIgnore
    void saveGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void setGoalTrackingStatus(String str, GoalStatus goalStatus);

    @DexIgnore
    void updateGoalTrackingEvent(GoalTrackingEvent goalTrackingEvent);
}
