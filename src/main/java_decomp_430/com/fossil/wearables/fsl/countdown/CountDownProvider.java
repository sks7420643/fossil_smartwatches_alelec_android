package com.fossil.wearables.fsl.countdown;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface CountDownProvider {
    @DexIgnore
    void deleteCountDown(long j);

    @DexIgnore
    void deleteCountDown(String str);

    @DexIgnore
    CountDown getActiveCountDown();

    @DexIgnore
    List<CountDown> getAllCountDown();

    @DexIgnore
    CountDown getCountDown(String str, long j);

    @DexIgnore
    CountDown getCountDownByClientId(String str);

    @DexIgnore
    CountDown getCountDownById(long j);

    @DexIgnore
    CountDown getCountDownByServerId(String str);

    @DexIgnore
    List<CountDown> getCountDownsByStatus(int i);

    @DexIgnore
    List<CountDown> getPastCountDownsPaging(long j, long j2);

    @DexIgnore
    void saveCountDown(CountDown countDown);

    @DexIgnore
    void setCountDownStatus(String str, CountDownStatus countDownStatus);
}
