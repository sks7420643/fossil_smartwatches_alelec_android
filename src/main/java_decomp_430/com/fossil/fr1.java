package com.fossil;

import android.app.job.JobInfo;
import com.fossil.cr1;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fr1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public zs1 a;
        @DexIgnore
        public Map<go1, b> b; // = new HashMap();

        @DexIgnore
        public a a(zs1 zs1) {
            this.a = zs1;
            return this;
        }

        @DexIgnore
        public a a(go1 go1, b bVar) {
            this.b.put(go1, bVar);
            return this;
        }

        @DexIgnore
        public fr1 a() {
            if (this.a == null) {
                throw new NullPointerException("missing required property: clock");
            } else if (this.b.keySet().size() >= go1.values().length) {
                Map<go1, b> map = this.b;
                this.b = new HashMap();
                return fr1.a(this.a, map);
            } else {
                throw new IllegalStateException("Not all priorities have been configured");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract a a(long j);

            @DexIgnore
            public abstract a a(Set<c> set);

            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(long j);
        }

        @DexIgnore
        public static a d() {
            cr1.b bVar = new cr1.b();
            bVar.a((Set<c>) Collections.emptySet());
            return bVar;
        }

        @DexIgnore
        public abstract long a();

        @DexIgnore
        public abstract Set<c> b();

        @DexIgnore
        public abstract long c();
    }

    @DexIgnore
    public enum c {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    @DexIgnore
    public static fr1 a(zs1 zs1) {
        a c2 = c();
        go1 go1 = go1.DEFAULT;
        b.a d = b.d();
        d.a(30000);
        d.b(86400000);
        c2.a(go1, d.a());
        go1 go12 = go1.HIGHEST;
        b.a d2 = b.d();
        d2.a(1000);
        d2.b(86400000);
        c2.a(go12, d2.a());
        go1 go13 = go1.VERY_LOW;
        b.a d3 = b.d();
        d3.a(86400000);
        d3.b(86400000);
        d3.a((Set<c>) EnumSet.of(c.NETWORK_UNMETERED, c.DEVICE_IDLE));
        c2.a(go13, d3.a());
        c2.a(zs1);
        return c2.a();
    }

    @DexIgnore
    public static a c() {
        return new a();
    }

    @DexIgnore
    public abstract zs1 a();

    @DexIgnore
    public abstract Map<go1, b> b();

    @DexIgnore
    public static fr1 a(zs1 zs1, Map<go1, b> map) {
        return new br1(zs1, map);
    }

    @DexIgnore
    public long a(go1 go1, long j, int i) {
        long a2 = j - a().a();
        b bVar = b().get(go1);
        return Math.min(Math.max(((long) Math.pow(2.0d, (double) (i - 1))) * bVar.a(), a2), bVar.c());
    }

    @DexIgnore
    public JobInfo.Builder a(JobInfo.Builder builder, go1 go1, long j, int i) {
        builder.setMinimumLatency(a(go1, j, i));
        a(builder, b().get(go1).b());
        return builder;
    }

    @DexIgnore
    public final void a(JobInfo.Builder builder, Set<c> set) {
        if (set.contains(c.NETWORK_UNMETERED)) {
            builder.setRequiredNetworkType(2);
        } else {
            builder.setRequiredNetworkType(1);
        }
        if (set.contains(c.DEVICE_CHARGING)) {
            builder.setRequiresCharging(true);
        }
        if (set.contains(c.DEVICE_IDLE)) {
            builder.setRequiresDeviceIdle(true);
        }
    }
}
