package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1$1", f = "ProfileGoalEditPresenter.kt", l = {59}, m = "invokeSuspend")
public final class wk5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements GoalTrackingRepository.UpdateGoalSettingCallback {
        @DexIgnore
        public /* final */ /* synthetic */ wk5$d$a a;

        @DexIgnore
        public a(wk5$d$a wk5_d_a) {
            this.a = wk5_d_a;
        }

        @DexIgnore
        public void onFail(zo4<GoalSetting> zo4) {
            String str;
            wg6.b(zo4, Constants.YO_ERROR_POST);
            sk5 m = this.a.this$0.this$0.m();
            int a2 = zo4.a();
            ServerError c = zo4.c();
            if (c == null || (str = c.getMessage()) == null) {
                str = "";
            }
            m.d(a2, str);
            this.a.this$0.this$0.o();
        }

        @DexIgnore
        public void onSuccess(cp4<GoalSetting> cp4) {
            wg6.b(cp4, "success");
            this.a.this$0.this$0.q();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wk5$d$a(ProfileGoalEditPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wk5$d$a wk5_d_a = new wk5$d$a(this.this$0, xe6);
        wk5_d_a.p$ = (il6) obj;
        return wk5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wk5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            GoalTrackingRepository c = this.this$0.this$0.r;
            GoalSetting goalSetting = new GoalSetting(this.this$0.this$0.k);
            a aVar = new a(this);
            this.L$0 = il6;
            this.label = 1;
            if (c.updateGoalSetting(goalSetting, aVar, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
