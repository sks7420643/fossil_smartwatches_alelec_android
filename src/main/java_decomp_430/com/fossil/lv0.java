package com.fossil;

import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class lv0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[h91.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[h91.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[q40.c.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[u40.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e; // = new int[eh1.values().length];

    /*
    static {
        a[h91.CONNECTED.ordinal()] = 1;
        a[h91.DISCONNECTED.ordinal()] = 2;
        a[h91.CONNECTING.ordinal()] = 3;
        a[h91.DISCONNECTING.ordinal()] = 4;
        b[h91.DISCONNECTED.ordinal()] = 1;
        b[h91.CONNECTING.ordinal()] = 2;
        b[h91.CONNECTED.ordinal()] = 3;
        b[h91.DISCONNECTING.ordinal()] = 4;
        c[q40.c.DISCONNECTED.ordinal()] = 1;
        c[q40.c.CONNECTING.ordinal()] = 2;
        c[q40.c.CONNECTED.ordinal()] = 3;
        c[q40.c.UPGRADING_FIRMWARE.ordinal()] = 4;
        c[q40.c.DISCONNECTING.ordinal()] = 5;
        d[u40.BLUETOOTH_OFF.ordinal()] = 1;
        e[eh1.READ_RSSI.ordinal()] = 1;
        e[eh1.STREAMING.ordinal()] = 2;
        e[eh1.DISCONNECT_HID.ordinal()] = 3;
        e[eh1.CONNECT_HID.ordinal()] = 4;
        e[eh1.OTA.ordinal()] = 5;
    }
    */
}
