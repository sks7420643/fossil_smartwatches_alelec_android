package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv1 {
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_button; // = 2131887100;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_text; // = 2131887101;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_title; // = 2131887102;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_button; // = 2131887103;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_text; // = 2131887104;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_title; // = 2131887105;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_channel_name; // = 2131887106;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_ticker; // = 2131887107;
    @DexIgnore
    public static /* final */ int common_google_play_services_unsupported_text; // = 2131887109;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_button; // = 2131887110;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_text; // = 2131887111;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_title; // = 2131887112;
    @DexIgnore
    public static /* final */ int common_google_play_services_updating_text; // = 2131887113;
    @DexIgnore
    public static /* final */ int common_google_play_services_wear_update_text; // = 2131887114;
    @DexIgnore
    public static /* final */ int common_open_on_phone; // = 2131887115;
    @DexIgnore
    public static /* final */ int common_signin_button_text; // = 2131887116;
    @DexIgnore
    public static /* final */ int common_signin_button_text_long; // = 2131887117;
}
