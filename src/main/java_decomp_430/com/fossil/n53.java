package com.fossil;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n53 {
    @DexIgnore
    public /* final */ x53 a;

    @DexIgnore
    public n53(x53 x53) {
        this.a = x53;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.isEmpty()) {
            this.a.b().z().a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.a.a().g();
        if (!a()) {
            this.a.b().z().a("Install Referrer Reporter is not available");
            return;
        }
        this.a.b().z().a("Install Referrer Reporter is initializing");
        m53 m53 = new m53(this, str);
        this.a.a().g();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.a.c().getPackageManager();
        if (packageManager == null) {
            this.a.b().w().a("Failed to obtain Package Manager to verify binding conditions");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.a.b().z().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ResolveInfo resolveInfo = queryIntentServices.get(0);
        ServiceInfo serviceInfo = resolveInfo.serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (resolveInfo.serviceInfo.name == null || !"com.android.vending".equals(str2) || !a()) {
                this.a.b().z().a("Play Store missing or incompatible. Version 8.3.73 or later required");
                return;
            }
            try {
                this.a.b().z().a("Install Referrer Service is", b42.a().a(this.a.c(), new Intent(intent), m53, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.a.b().t().a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        try {
            f52 b = g52.b(this.a.c());
            if (b == null) {
                this.a.b().z().a("Failed to retrieve Package Manager to check Play Store compatibility");
                return false;
            } else if (b.b("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            this.a.b().z().a("Failed to retrieve Play Store version", e);
            return false;
        }
    }

    @DexIgnore
    public final Bundle a(String str, el2 el2) {
        this.a.a().g();
        if (el2 == null) {
            this.a.b().w().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle d = el2.d(bundle);
            if (d != null) {
                return d;
            }
            this.a.b().t().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.a.b().t().a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
