package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bq3 implements ys3 {
    @DexIgnore
    public /* final */ eq3 a;
    @DexIgnore
    public /* final */ wp3 b;

    @DexIgnore
    public bq3(eq3 eq3, wp3 wp3) {
        this.a = eq3;
        this.b = wp3;
    }

    @DexIgnore
    public static ys3 a(eq3 eq3, wp3 wp3) {
        return new bq3(eq3, wp3);
    }

    @DexIgnore
    public Object get() {
        return this.b.b().a(new nq3(this.b, this.a));
    }
}
