package com.fossil;

import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue0 {
    @DexIgnore
    public static /* final */ Hashtable<String, zm1> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ue0 b; // = new ue0();

    @DexIgnore
    public final void a(String str, byte[] bArr) {
        synchronized (a) {
            b.a(str).a = bArr;
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final void a(String str, byte[] bArr, byte[] bArr2) {
        synchronized (a) {
            b.a(str).a(bArr, bArr2);
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final zm1 a(String str) {
        zm1 zm1;
        synchronized (a) {
            zm1 = a.get(str);
            if (zm1 == null) {
                zm1 = new zm1();
            }
            a.put(str, zm1);
        }
        return zm1;
    }
}
