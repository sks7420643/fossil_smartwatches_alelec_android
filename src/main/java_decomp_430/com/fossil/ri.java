package com.fossil;

import android.database.sqlite.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ri extends qi implements mi {
    @DexIgnore
    public /* final */ SQLiteStatement b;

    @DexIgnore
    public ri(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.b = sQLiteStatement;
    }

    @DexIgnore
    public int s() {
        return this.b.executeUpdateDelete();
    }

    @DexIgnore
    public long t() {
        return this.b.executeInsert();
    }
}
