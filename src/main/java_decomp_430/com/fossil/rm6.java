package com.fossil;

import com.fossil.af6;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rm6 extends af6.b {
    @DexIgnore
    public static final b n = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements af6.c<rm6> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.m;
        }
        */
    }

    @DexIgnore
    am6 a(hg6<? super Throwable, cd6> hg6);

    @DexIgnore
    am6 a(boolean z, boolean z2, hg6<? super Throwable, cd6> hg6);

    @DexIgnore
    qk6 a(sk6 sk6);

    @DexIgnore
    void a(CancellationException cancellationException);

    @DexIgnore
    /* synthetic */ void cancel();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    CancellationException k();

    @DexIgnore
    boolean start();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends af6.b> E a(rm6 rm6, af6.c<E> cVar) {
            wg6.b(cVar, "key");
            return af6.b.a.a((af6.b) rm6, cVar);
        }

        @DexIgnore
        public static af6 a(rm6 rm6, af6 af6) {
            wg6.b(af6, "context");
            return af6.b.a.a((af6.b) rm6, af6);
        }

        @DexIgnore
        public static <R> R a(rm6 rm6, R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
            wg6.b(ig6, "operation");
            return af6.b.a.a(rm6, r, ig6);
        }

        @DexIgnore
        public static /* synthetic */ void a(rm6 rm6, CancellationException cancellationException, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    cancellationException = null;
                }
                rm6.a(cancellationException);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: cancel");
        }

        @DexIgnore
        public static af6 b(rm6 rm6, af6.c<?> cVar) {
            wg6.b(cVar, "key");
            return af6.b.a.b(rm6, cVar);
        }

        @DexIgnore
        public static /* synthetic */ am6 a(rm6 rm6, boolean z, boolean z2, hg6 hg6, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return rm6.a(z, z2, hg6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }
    }
}
