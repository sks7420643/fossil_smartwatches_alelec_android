package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf1 extends if1 {
    @DexIgnore
    public mw0 B;

    @DexIgnore
    public cf1(ue1 ue1, q41 q41, String str) {
        super(ue1, q41, eh1.CREATE_BOND, str);
    }

    @DexIgnore
    public Object d() {
        mw0 mw0 = this.B;
        return mw0 != null ? mw0 : mw0.BOND_NONE;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new jx0(this.w), (hg6) new q91(this), (hg6) mb1.a, (ig6) null, (hg6) new hd1(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.w.getBondState()));
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.NEW_BOND_STATE;
        mw0 mw0 = this.B;
        return cw0.a(k, bm0, (Object) mw0 != null ? cw0.a((Enum<?>) mw0) : null);
    }
}
