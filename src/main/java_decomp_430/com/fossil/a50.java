package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a50 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ qk0 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<a50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final a50[] a(byte[] bArr) {
            Object obj;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr.length - 1) - 2) {
                qk0 a = qk0.f.a(bArr[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(md6.a(bArr, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                wg6.a(order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int b = cw0.b(order.getShort());
                byte[] a2 = md6.a(bArr, i3, i3 + b);
                if (a == null) {
                    obj = null;
                } else {
                    int i4 = eh0.b[a.ordinal()];
                    if (i4 == 1) {
                        obj = b50.CREATOR.a(a2);
                    } else if (i4 == 2) {
                        obj = k50.CREATOR.a(a2);
                    } else if (i4 == 3) {
                        obj = c50.CREATOR.a(a2);
                    } else {
                        throw new kc6();
                    }
                }
                if (obj != null) {
                    arrayList.add(obj);
                }
                i += b + 3;
            }
            Object[] array = arrayList.toArray(new a50[0]);
            if (array != null) {
                return (a50[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            qk0 qk0 = qk0.values()[parcel.readInt()];
            parcel.setDataPosition(parcel.dataPosition() - 4);
            int i = eh0.a[qk0.ordinal()];
            if (i == 1) {
                return b50.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return k50.CREATOR.createFromParcel(parcel);
            }
            if (i == 3) {
                return c50.CREATOR.createFromParcel(parcel);
            }
            throw new kc6();
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new a50[i];
        }
    }

    @DexIgnore
    public a50(qk0 qk0) {
        this.a = qk0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(new JSONObject(), bm0.TYPE, (Object) cw0.a((Enum<?>) this.a));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] c = c();
        byte[] array = ByteBuffer.allocate(c.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a).putShort((short) c.length).put(c).array();
        wg6.a(array, "ByteBuffer.allocate(SUB_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((a50) obj).a;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.AlarmSubEntry");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
    }
}
