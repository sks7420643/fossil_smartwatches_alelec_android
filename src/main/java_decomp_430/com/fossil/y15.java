package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y15 {
    @DexIgnore
    public /* final */ x15 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public y15(x15 x15, LoaderManager loaderManager) {
        wg6.b(x15, "mView");
        wg6.b(loaderManager, "mLoaderManager");
        this.a = x15;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final x15 b() {
        return this.a;
    }
}
