package com.fossil;

import android.content.Context;
import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xp1 implements Closeable {

    @DexIgnore
    public interface a {
        @DexIgnore
        a a(Context context);

        @DexIgnore
        xp1 build();
    }

    @DexIgnore
    public void close() throws IOException {
        k().close();
    }

    @DexIgnore
    public abstract ur1 k();

    @DexIgnore
    public abstract wp1 l();
}
