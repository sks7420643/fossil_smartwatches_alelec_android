package com.fossil;

import com.fossil.po1;
import com.fossil.yw3;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap1 extends yw3<ap1, b> implements bp1 {
    @DexIgnore
    public static /* final */ ap1 f; // = new ap1();
    @DexIgnore
    public static volatile gx3<ap1> g;
    @DexIgnore
    public long d;
    @DexIgnore
    public po1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<ap1, b> implements bp1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b() {
            super(ap1.f);
        }
    }

    /*
    static {
        f.g();
    }
    */

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new ap1();
            case 2:
                return f;
            case 3:
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                ap1 ap1 = (ap1) obj2;
                this.d = kVar.a(this.d != 0, this.d, ap1.d != 0, ap1.d);
                this.e = (po1) kVar.a(this.e, ap1.e);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 8) {
                                this.d = tw3.e();
                            } else if (l == 26) {
                                po1.b bVar = this.e != null ? (po1.b) this.e.c() : null;
                                this.e = (po1) tw3.a(po1.l(), ww3);
                                if (bVar != null) {
                                    bVar.b(this.e);
                                    this.e = (po1) bVar.c();
                                }
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (g == null) {
                    synchronized (ap1.class) {
                        if (g == null) {
                            g = new yw3.c(f);
                        }
                    }
                }
                return g;
            default:
                throw new UnsupportedOperationException();
        }
        return f;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        long j = this.d;
        if (j != 0) {
            i2 = 0 + uw3.d(1, j);
        }
        po1 po1 = this.e;
        if (po1 != null) {
            if (po1 == null) {
                po1 = po1.k();
            }
            i2 += uw3.b(3, (dx3) po1);
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public long j() {
        return this.d;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        long j = this.d;
        if (j != 0) {
            uw3.a(1, j);
        }
        po1 po1 = this.e;
        if (po1 != null) {
            if (po1 == null) {
                po1 = po1.k();
            }
            uw3.a(3, (dx3) po1);
        }
    }

    @DexIgnore
    public static ap1 a(InputStream inputStream) throws IOException {
        return (ap1) yw3.a(f, inputStream);
    }
}
