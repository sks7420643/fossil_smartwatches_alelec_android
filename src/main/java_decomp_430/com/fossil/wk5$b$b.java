package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk5$b$b extends sf6 implements ig6<il6, xe6<? super ap4<MFUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ProfileGoalEditPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wk5$b$b(MFUser mFUser, xe6 xe6, ProfileGoalEditPresenter.b bVar) {
        super(2, xe6);
        this.$it = mFUser;
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wk5$b$b wk5_b_b = new wk5$b$b(this.$it, xe6, this.this$0);
        wk5_b_b.p$ = (il6) obj;
        return wk5_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wk5$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            UserRepository h = this.this$0.this$0.s;
            MFUser mFUser = this.$it;
            this.L$0 = il6;
            this.label = 1;
            obj = h.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
