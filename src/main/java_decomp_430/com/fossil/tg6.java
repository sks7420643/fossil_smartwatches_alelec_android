package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg6 extends mg6 implements sg6, ii6 {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public tg6(int i) {
        this.arity = i;
    }

    @DexIgnore
    public ei6 computeReflected() {
        kh6.a(this);
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof tg6) {
            tg6 tg6 = (tg6) obj;
            if (getOwner() != null ? getOwner().equals(tg6.getOwner()) : tg6.getOwner() == null) {
                if (!getName().equals(tg6.getName()) || !getSignature().equals(tg6.getSignature()) || !wg6.a(getBoundReceiver(), tg6.getBoundReceiver())) {
                    return false;
                }
                return true;
            }
            return false;
        } else if (obj instanceof ii6) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner() == null ? 0 : getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @DexIgnore
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @DexIgnore
    public boolean isInline() {
        return getReflected().isInline();
    }

    @DexIgnore
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @DexIgnore
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public String toString() {
        ei6 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }

    @DexIgnore
    public ii6 getReflected() {
        return (ii6) super.getReflected();
    }

    @DexIgnore
    public tg6(int i, Object obj) {
        super(obj);
        this.arity = i;
    }
}
