package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qy3 implements uy3 {
    @DexIgnore
    public static void b(vy3 vy3, StringBuilder sb) {
        vy3.a(a((CharSequence) sb, 0));
        sb.delete(0, 3);
    }

    @DexIgnore
    public int a() {
        return 1;
    }

    @DexIgnore
    public void a(vy3 vy3) {
        int a;
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!vy3.i()) {
                break;
            }
            char c = vy3.c();
            vy3.f++;
            int a2 = a(c, sb);
            int a3 = vy3.a() + ((sb.length() / 3) << 1);
            vy3.c(a3);
            int a4 = vy3.g().a() - a3;
            if (vy3.i()) {
                if (sb.length() % 3 == 0 && (a = xy3.a(vy3.d(), vy3.f, a())) != a()) {
                    vy3.b(a);
                    break;
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                if (sb.length() % 3 == 2 && (a4 < 2 || a4 > 2)) {
                    a2 = a(vy3, sb, sb2, a2);
                }
                while (sb.length() % 3 == 1 && ((a2 <= 3 && a4 != 1) || a2 > 3)) {
                    a2 = a(vy3, sb, sb2, a2);
                }
            }
        }
        a(vy3, sb);
    }

    @DexIgnore
    public final int a(vy3 vy3, StringBuilder sb, StringBuilder sb2, int i) {
        int length = sb.length();
        sb.delete(length - i, length);
        vy3.f--;
        int a = a(vy3.c(), sb2);
        vy3.k();
        return a;
    }

    @DexIgnore
    public void a(vy3 vy3, StringBuilder sb) {
        int length = sb.length() % 3;
        int a = vy3.a() + ((sb.length() / 3) << 1);
        vy3.c(a);
        int a2 = vy3.g().a() - a;
        if (length == 2) {
            sb.append(0);
            while (sb.length() >= 3) {
                b(vy3, sb);
            }
            if (vy3.i()) {
                vy3.a(254);
            }
        } else if (a2 == 1 && length == 1) {
            while (sb.length() >= 3) {
                b(vy3, sb);
            }
            if (vy3.i()) {
                vy3.a(254);
            }
            vy3.f--;
        } else if (length == 0) {
            while (sb.length() >= 3) {
                b(vy3, sb);
            }
            if (a2 > 0 || vy3.i()) {
                vy3.a(254);
            }
        } else {
            throw new IllegalStateException("Unexpected case. Please report!");
        }
        vy3.b(0);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == ' ') {
            sb.append(3);
            return 1;
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
            return 1;
        } else if (c >= 'A' && c <= 'Z') {
            sb.append((char) ((c - 'A') + 14));
            return 1;
        } else if (c >= 0 && c <= 31) {
            sb.append(0);
            sb.append(c);
            return 2;
        } else if (c >= '!' && c <= '/') {
            sb.append(1);
            sb.append((char) (c - '!'));
            return 2;
        } else if (c >= ':' && c <= '@') {
            sb.append(1);
            sb.append((char) ((c - ':') + 15));
            return 2;
        } else if (c >= '[' && c <= '_') {
            sb.append(1);
            sb.append((char) ((c - '[') + 22));
            return 2;
        } else if (c >= '`' && c <= 127) {
            sb.append(2);
            sb.append((char) (c - '`'));
            return 2;
        } else if (c >= 128) {
            sb.append("\u0001\u001e");
            return a((char) (c - 128), sb) + 2;
        } else {
            throw new IllegalArgumentException("Illegal character: " + c);
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int charAt = (charSequence.charAt(i) * 1600) + (charSequence.charAt(i + 1) * '(') + charSequence.charAt(i + 2) + 1;
        return new String(new char[]{(char) (charAt / 256), (char) (charAt % 256)});
    }
}
