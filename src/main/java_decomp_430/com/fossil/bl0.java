package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ if1 a;
    @DexIgnore
    public /* final */ /* synthetic */ hg6 b;
    @DexIgnore
    public /* final */ /* synthetic */ hg6 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bl0(if1 if1, hg6 hg6, hg6 hg62) {
        super(1);
        this.a = if1;
        this.b = hg6;
        this.c = hg62;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        qv0 qv0 = (qv0) obj;
        if (((Boolean) this.b.invoke(qv0.v)).booleanValue()) {
            this.a.a(qv0.v);
        } else {
            this.c.invoke(qv0);
        }
        return cd6.a;
    }
}
