package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv4 implements Factory<ArrayList<Alarm>> {
    @DexIgnore
    public static ArrayList<Alarm> a(rv4 rv4) {
        ArrayList<Alarm> b = rv4.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
