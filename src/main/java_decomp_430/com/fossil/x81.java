package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x81 implements Parcelable.Creator<ta1> {
    @DexIgnore
    public /* synthetic */ x81(qg6 qg6) {
    }

    @DexIgnore
    public ta1 createFromParcel(Parcel parcel) {
        return new ta1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ta1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m69createFromParcel(Parcel parcel) {
        return new ta1(parcel, (qg6) null);
    }
}
