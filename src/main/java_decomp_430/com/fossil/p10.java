package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p10 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public p10(String str, Bundle bundle) {
        this.a = str;
        this.b = bundle;
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public Bundle b() {
        return this.b;
    }
}
