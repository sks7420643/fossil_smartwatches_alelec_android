package com.fossil;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq2 extends AbstractList<String> implements xn2, RandomAccess {
    @DexIgnore
    public /* final */ xn2 a;

    @DexIgnore
    public bq2(xn2 xn2) {
        this.a = xn2;
    }

    @DexIgnore
    public final void a(yl2 yl2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return (String) this.a.get(i);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new dq2(this);
    }

    @DexIgnore
    public final ListIterator<String> listIterator(int i) {
        return new aq2(this, i);
    }

    @DexIgnore
    public final xn2 n() {
        return this;
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }

    @DexIgnore
    public final Object zzb(int i) {
        return this.a.zzb(i);
    }

    @DexIgnore
    public final List<?> zzb() {
        return this.a.zzb();
    }
}
