package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pc3<TResult, TContinuationResult> {
    @DexIgnore
    qc3<TContinuationResult> then(TResult tresult) throws Exception;
}
