package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk0 implements Parcelable.Creator<em0> {
    @DexIgnore
    public /* synthetic */ lk0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            xh0 valueOf = xh0.valueOf(readString);
            parcel.setDataPosition(0);
            switch (ri0.a[valueOf.ordinal()]) {
                case 1:
                    return cb1.CREATOR.createFromParcel(parcel);
                case 2:
                    return i71.CREATOR.createFromParcel(parcel);
                case 3:
                    return zz0.CREATOR.createFromParcel(parcel);
                case 4:
                    return zg0.CREATOR.createFromParcel(parcel);
                case 5:
                    return ji1.CREATOR.createFromParcel(parcel);
                case 6:
                    return zs0.CREATOR.createFromParcel(parcel);
                case 7:
                    return wl1.CREATOR.createFromParcel(parcel);
                case 8:
                    return se1.CREATOR.createFromParcel(parcel);
                case 9:
                    return si0.CREATOR.createFromParcel(parcel);
                case 10:
                    return op0.CREATOR.createFromParcel(parcel);
                case 11:
                    return new lw0(parcel);
                case 12:
                    return new p31(parcel);
                default:
                    throw new kc6();
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new em0[i];
    }
}
