package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.a35;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x25$b$b implements y24.d<a35.d, a35.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridContactPresenter.b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1", f = "NotificationHybridContactPresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a35.d $successResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ x25$b$b this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x25$b$b$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.x25$b$b$a$a  reason: collision with other inner class name */
        public static final class C0054a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0054a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0054a aVar = new C0054a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0054a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    for (ContactGroup contactGroup : this.this$0.$successResponse.a()) {
                        for (Contact contact : contactGroup.getContacts()) {
                            wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
                            wx4.setAdded(true);
                            Contact contact2 = wx4.getContact();
                            if (contact2 != null) {
                                wg6.a((Object) contact, "contact");
                                contact2.setDbRowId(contact.getDbRowId());
                                contact2.setUseSms(contact.isUseSms());
                                contact2.setUseCall(contact.isUseCall());
                            }
                            wx4.setCurrentHandGroup(contactGroup.getHour());
                            wg6.a((Object) contact, "contact");
                            List phoneNumbers = contact.getPhoneNumbers();
                            wg6.a((Object) phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                Object obj2 = contact.getPhoneNumbers().get(0);
                                wg6.a(obj2, "contact.phoneNumbers[0]");
                                String number = ((PhoneNumber) obj2).getNumber();
                                if (!TextUtils.isEmpty(number)) {
                                    wx4.setHasPhoneNumber(true);
                                    wx4.setPhoneNumber(number);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a = NotificationHybridContactPresenter.m.a();
                                    local.d(a, " filter selected contact, phoneNumber=" + number);
                                }
                            }
                            Iterator it = this.this$0.this$0.a.this$0.h.iterator();
                            int i = 0;
                            while (true) {
                                if (!it.hasNext()) {
                                    i = -1;
                                    break;
                                }
                                Contact contact3 = ((wx4) it.next()).getContact();
                                if (hf6.a(contact3 != null && contact3.getContactId() == contact.getContactId()).booleanValue()) {
                                    break;
                                }
                                i++;
                            }
                            if (i != -1) {
                                wx4.setCurrentHandGroup(this.this$0.this$0.a.this$0.g);
                                wg6.a(this.this$0.this$0.a.this$0.h.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                            } else if (wx4.getCurrentHandGroup() == this.this$0.this$0.a.this$0.g) {
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a2 = NotificationHybridContactPresenter.m.a();
                            local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.this$0.this$0.a.this$0.j().add(wx4);
                        }
                    }
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(x25$b$b x25_b_b, a35.d dVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = x25_b_b;
            this.$successResponse = dVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$successResponse, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                rl6 a2 = ik6.a(il6, this.this$0.a.this$0.b(), (ll6) null, new C0054a(this, (xe6) null), 2, (Object) null);
                this.L$0 = il6;
                this.L$1 = a2;
                this.label = 1;
                if (a2.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                rl6 rl6 = (rl6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.a.this$0.h.isEmpty()) {
                for (wx4 add : this.this$0.a.this$0.h) {
                    this.this$0.a.this$0.j().add(add);
                }
            }
            this.this$0.a.this$0.f.a(this.this$0.a.this$0.j(), nx5.a.a(), this.this$0.a.this$0.g);
            this.this$0.a.this$0.i.a(0, new Bundle(), this.this$0.a.this$0);
            return cd6.a;
        }
    }

    @DexIgnore
    public x25$b$b(NotificationHybridContactPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(a35.d dVar) {
        wg6.b(dVar, "successResponse");
        FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), "GetAllContactGroup onSuccess");
        rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, dVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(a35.b bVar) {
        wg6.b(bVar, "errorResponse");
        FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), "GetAllContactGroup onError");
    }
}
