package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm1 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG, hl1.TRANSFER_DATA}));
    @DexIgnore
    public /* final */ long C; // = ((long) this.M.length);
    @DexIgnore
    public long D;
    @DexIgnore
    public float E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public /* final */ it0 L; // = mi0.A.p();
    @DexIgnore
    public /* final */ byte[] M;
    @DexIgnore
    public /* final */ boolean N;
    @DexIgnore
    public /* final */ short O;
    @DexIgnore
    public /* final */ float P;

    @DexIgnore
    public hm1(ue1 ue1, q41 q41, byte[] bArr, boolean z, short s, float f, String str) {
        super(ue1, q41, eh1.LEGACY_OTA, str);
        this.M = bArr;
        this.N = z;
        this.O = s;
        this.P = f;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(cw0.a(super.i(), bm0.FILE_CRC, (Object) Long.valueOf(h51.a.a(this.M, q11.CRC32))), bm0.SKIP_RESUME, (Object) Boolean.valueOf(this.N)), bm0.FILE_HANDLE, (Object) cw0.a(this.O));
    }

    @DexIgnore
    public void j() {
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = 0;
    }

    @DexIgnore
    public final void m() {
        long j = this.H + this.I;
        if1.a((if1) this, (qv0) new o81(j, Math.min(6144, this.C - j), this.C, this.O, this.w, 0, 32), (hg6) new g21(this), (hg6) new c41(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public final void n() {
        if1.a((if1) this, (qv0) new wf1(this.O, this.w, 0, 4), (hg6) new ef1(this), (hg6) new ah1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public final void o() {
        long j = this.G;
        long a = h51.a.a(this.M, (int) 0, (int) j, q11.CRC32);
        if1.a((if1) this, (qv0) new th1(0, j, this.C, this.O, this.w, 0, 32), (hg6) new vi1(this, a), (hg6) new pk1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (if1) new yy0(this.w, this.x, this.L, this.z), (hg6) new y51(this), (hg6) new v71(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }
}
