package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a35 extends y24<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.a {
        @DexIgnore
        public b(String str) {
            wg6.b(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements y24.c {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        public d(List<? extends ContactGroup> list) {
            wg6.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = a35.class.getSimpleName();
        wg6.a((Object) simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public void a(c cVar) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            a().onSuccess(new d(allContactGroups));
        } else {
            a().a(new b("Get all contact group failed"));
        }
    }
}
