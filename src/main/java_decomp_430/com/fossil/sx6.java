package com.fossil;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sx6<T> {
    @DexIgnore
    public static <T> sx6<T> a(Retrofit retrofit3, Method method) {
        qx6 a = qx6.a(retrofit3, method);
        Type genericReturnType = method.getGenericReturnType();
        if (vx6.c(genericReturnType)) {
            throw vx6.a(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
        } else if (genericReturnType != Void.TYPE) {
            return ix6.a(retrofit3, method, a);
        } else {
            throw vx6.a(method, "Service methods cannot return void.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract T a(Object[] objArr);
}
