package com.fossil;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa1 {
    @DexIgnore
    public static /* final */ xa1 a; // = new xa1();

    @DexIgnore
    public final byte[] a(e71 e71, b91 b91, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance(b91.a);
        instance.init(e71.a, secretKeySpec, new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        wg6.a(doFinal, "cipher.doFinal(data)");
        return doFinal;
    }

    @DexIgnore
    public final byte[] a(b91 b91, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        return a(e71.DECRYPT, b91, bArr, bArr2, bArr3);
    }
}
