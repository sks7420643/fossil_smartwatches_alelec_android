package com.fossil;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n65 implements Factory<m65> {
    @DexIgnore
    public static ComplicationSearchPresenter a(j65 j65, ComplicationRepository complicationRepository, an4 an4) {
        return new ComplicationSearchPresenter(j65, complicationRepository, an4);
    }
}
