package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mi1 {
    @DexIgnore
    public /* synthetic */ mi1(qg6 qg6) {
    }

    @DexIgnore
    public final rg1 a(UUID uuid) {
        if (wg6.a(uuid, mi0.A.k())) {
            return rg1.MODEL_NUMBER;
        }
        if (wg6.a(uuid, mi0.A.l())) {
            return rg1.SERIAL_NUMBER;
        }
        if (wg6.a(uuid, mi0.A.j())) {
            return rg1.FIRMWARE_VERSION;
        }
        if (wg6.a(uuid, mi0.A.m())) {
            return rg1.SOFTWARE_REVISION;
        }
        if (wg6.a(uuid, mi0.A.s())) {
            return rg1.DC;
        }
        if (wg6.a(uuid, mi0.A.t())) {
            return rg1.FTC;
        }
        if (wg6.a(uuid, mi0.A.u())) {
            return rg1.FTD;
        }
        if (wg6.a(uuid, mi0.A.q())) {
            return rg1.ASYNC;
        }
        if (wg6.a(uuid, mi0.A.v())) {
            return rg1.FTD_1;
        }
        if (wg6.a(uuid, mi0.A.r())) {
            return rg1.AUTHENTICATION;
        }
        if (wg6.a(uuid, mi0.A.o())) {
            return rg1.HEART_RATE;
        }
        return rg1.UNKNOWN;
    }
}
