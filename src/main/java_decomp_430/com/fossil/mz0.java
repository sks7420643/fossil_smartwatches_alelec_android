package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz0 {
    @DexIgnore
    public /* synthetic */ mz0(qg6 qg6) {
    }

    @DexIgnore
    public final g11 a(t31 t31) {
        int i = sx0.a[t31.a.ordinal()];
        if (i == 1) {
            return g11.SUCCESS;
        }
        if (i != 2) {
            return g11.GATT_ERROR;
        }
        return g11.BLUETOOTH_OFF;
    }
}
