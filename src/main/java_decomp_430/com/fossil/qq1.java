package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class qq1 implements Runnable {
    @DexIgnore
    public /* final */ sq1 a;
    @DexIgnore
    public /* final */ rp1 b;
    @DexIgnore
    public /* final */ ko1 c;
    @DexIgnore
    public /* final */ np1 d;

    @DexIgnore
    public qq1(sq1 sq1, rp1 rp1, ko1 ko1, np1 np1) {
        this.a = sq1;
        this.b = rp1;
        this.c = ko1;
        this.d = np1;
    }

    @DexIgnore
    public static Runnable a(sq1 sq1, rp1 rp1, ko1 ko1, np1 np1) {
        return new qq1(sq1, rp1, ko1, np1);
    }

    @DexIgnore
    public void run() {
        sq1.a(this.a, this.b, this.c, this.d);
    }
}
