package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.c12;
import com.fossil.rv1.d;
import com.fossil.wv1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv1<O extends d> {
    @DexIgnore
    public /* final */ a<?, O> a;
    @DexIgnore
    public /* final */ g<?> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<T extends f, O> extends e<T, O> {
        @DexIgnore
        @Deprecated
        public T a(Context context, Looper looper, e12 e12, O o, wv1.b bVar, wv1.c cVar) {
            throw null;
        }
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<C extends b> {
    }

    @DexIgnore
    public interface d {

        @DexIgnore
        public interface a extends c, e {
            @DexIgnore
            Account g();
        }

        @DexIgnore
        public interface b extends c {
            @DexIgnore
            GoogleSignInAccount a();
        }

        @DexIgnore
        public interface c extends d {
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.rv1$d$d")
        /* renamed from: com.fossil.rv1$d$d  reason: collision with other inner class name */
        public static final class C0044d implements e {
        }

        @DexIgnore
        public interface e extends d {
        }

        @DexIgnore
        public interface f extends c, e {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T extends b, O> {
        @DexIgnore
        public int a() {
            return Integer.MAX_VALUE;
        }

        @DexIgnore
        public List<Scope> a(O o) {
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public interface f extends b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(c12.c cVar);

        @DexIgnore
        void a(c12.e eVar);

        @DexIgnore
        void a(n12 n12, Set<Scope> set);

        @DexIgnore
        void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        @DexIgnore
        boolean c();

        @DexIgnore
        boolean d();

        @DexIgnore
        Set<Scope> e();

        @DexIgnore
        boolean f();

        @DexIgnore
        String g();

        @DexIgnore
        boolean i();

        @DexIgnore
        int j();

        @DexIgnore
        iv1[] k();

        @DexIgnore
        Intent l();

        @DexIgnore
        boolean m();

        @DexIgnore
        IBinder n();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<C extends f> extends c<C> {
    }

    @DexIgnore
    public interface h<T extends IInterface> extends b {
        @DexIgnore
        T a(IBinder iBinder);

        @DexIgnore
        void a(int i, T t);

        @DexIgnore
        String o();

        @DexIgnore
        String p();
    }

    @DexIgnore
    public <C extends f> rv1(String str, a<C, O> aVar, g<C> gVar) {
        w12.a(aVar, (Object) "Cannot construct an Api with a null ClientBuilder");
        w12.a(gVar, (Object) "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.a = aVar;
        this.b = gVar;
    }

    @DexIgnore
    public final c<?> a() {
        g<?> gVar = this.b;
        if (gVar != null) {
            return gVar;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final e<?, O> c() {
        return this.a;
    }

    @DexIgnore
    public final a<?, O> d() {
        w12.b(this.a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.a;
    }
}
