package com.fossil;

import android.os.Binder;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d63 extends o43 {
    @DexIgnore
    public /* final */ ea3 a;
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public String c;

    @DexIgnore
    public d63(ea3 ea3) {
        this(ea3, (String) null);
    }

    @DexIgnore
    public final void a(j03 j03, ra3 ra3) {
        w12.a(j03);
        b(ra3, false);
        a((Runnable) new l63(this, j03, ra3));
    }

    @DexIgnore
    public final void b(ra3 ra3) {
        b(ra3, false);
        a((Runnable) new c63(this, ra3));
    }

    @DexIgnore
    public final String c(ra3 ra3) {
        b(ra3, false);
        return this.a.d(ra3);
    }

    @DexIgnore
    public final void d(ra3 ra3) {
        b(ra3, false);
        a((Runnable) new o63(this, ra3));
    }

    @DexIgnore
    public d63(ea3 ea3, String str) {
        w12.a(ea3);
        this.a = ea3;
        this.c = null;
    }

    @DexIgnore
    public final j03 b(j03 j03, ra3 ra3) {
        i03 i03;
        boolean z = false;
        if (!(!"_cmp".equals(j03.a) || (i03 = j03.b) == null || i03.zza() == 0)) {
            String h = j03.b.h("_cis");
            if (!TextUtils.isEmpty(h) && (("referrer broadcast".equals(h) || "referrer API".equals(h)) && this.a.i().j(ra3.a))) {
                z = true;
            }
        }
        if (!z) {
            return j03;
        }
        this.a.b().z().a("Event has been filtered ", j03.toString());
        return new j03("_cmpx", j03.b, j03.c, j03.d);
    }

    @DexIgnore
    public final void a(j03 j03, String str, String str2) {
        w12.a(j03);
        w12.b(str);
        a(str, true);
        a((Runnable) new k63(this, j03, str));
    }

    @DexIgnore
    public final byte[] a(j03 j03, String str) {
        w12.b(str);
        w12.a(j03);
        a(str, true);
        this.a.b().A().a("Log and bundle. event", this.a.p().a(j03.a));
        long a2 = this.a.zzm().a() / 1000000;
        try {
            byte[] bArr = (byte[]) this.a.a().b(new n63(this, j03, str)).get();
            if (bArr == null) {
                this.a.b().t().a("Log and bundle returned null. appId", t43.a(str));
                bArr = new byte[0];
            }
            this.a.b().A().a("Log and bundle processed. event, size, time_ms", this.a.p().a(j03.a), Integer.valueOf(bArr.length), Long.valueOf((this.a.zzm().a() / 1000000) - a2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to log and bundle. appId, event, error", t43.a(str), this.a.p().a(j03.a), e);
            return null;
        }
    }

    @DexIgnore
    public final void b(ra3 ra3, boolean z) {
        w12.a(ra3);
        a(ra3.a, false);
        this.a.q().c(ra3.b, ra3.v);
    }

    @DexIgnore
    public final void a(la3 la3, ra3 ra3) {
        w12.a(la3);
        b(ra3, false);
        a((Runnable) new m63(this, la3, ra3));
    }

    @DexIgnore
    public final List<la3> a(ra3 ra3, boolean z) {
        b(ra3, false);
        try {
            List<na3> list = (List) this.a.a().a(new p63(this, ra3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (na3 na3 : list) {
                if (z || !ma3.f(na3.c)) {
                    arrayList.add(new la3(na3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to get user attributes. appId", t43.a(ra3.a), e);
            return null;
        }
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.b == null) {
                        if (!"com.google.android.gms".equals(this.c) && !v42.a(this.a.c(), Binder.getCallingUid())) {
                            if (!ov1.a(this.a.c()).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.b = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.b = Boolean.valueOf(z2);
                    }
                    if (this.b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.a.b().t().a("Measurement Service called with invalid calling package. appId", t43.a(str));
                    throw e;
                }
            }
            if (this.c == null && nv1.uidHasPackageName(this.a.c(), Binder.getCallingUid(), str)) {
                this.c = str;
            }
            if (!str.equals(this.c)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
            return;
        }
        this.a.b().t().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) {
        a((Runnable) new r63(this, str2, str3, str, j));
    }

    @DexIgnore
    public final void a(ab3 ab3, ra3 ra3) {
        w12.a(ab3);
        w12.a(ab3.c);
        b(ra3, false);
        ab3 ab32 = new ab3(ab3);
        ab32.a = ra3.a;
        a((Runnable) new q63(this, ab32, ra3));
    }

    @DexIgnore
    public final void a(ab3 ab3) {
        w12.a(ab3);
        w12.a(ab3.c);
        a(ab3.a, true);
        a((Runnable) new f63(this, new ab3(ab3)));
    }

    @DexIgnore
    public final List<la3> a(String str, String str2, boolean z, ra3 ra3) {
        b(ra3, false);
        try {
            List<na3> list = (List) this.a.a().a(new e63(this, ra3, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (na3 na3 : list) {
                if (z || !ma3.f(na3.c)) {
                    arrayList.add(new la3(na3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to get user attributes. appId", t43.a(ra3.a), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<la3> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<na3> list = (List) this.a.a().a(new h63(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (na3 na3 : list) {
                if (z || !ma3.f(na3.c)) {
                    arrayList.add(new la3(na3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to get user attributes. appId", t43.a(str), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<ab3> a(String str, String str2, ra3 ra3) {
        b(ra3, false);
        try {
            return (List) this.a.a().a(new g63(this, ra3, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<ab3> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.a.a().a(new j63(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.b().t().a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final void a(ra3 ra3) {
        a(ra3.a, false);
        a((Runnable) new i63(this, ra3));
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        w12.a(runnable);
        if (this.a.a().s()) {
            runnable.run();
        } else {
            this.a.a().a(runnable);
        }
    }
}
