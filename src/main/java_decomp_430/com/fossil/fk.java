package com.fossil;

import android.annotation.SuppressLint;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fk extends kk {
    @DexIgnore
    public static boolean f; // = true;

    @DexIgnore
    public void a(View view) {
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void a(View view, float f2) {
        if (f) {
            try {
                view.setTransitionAlpha(f2);
                return;
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        view.setAlpha(f2);
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public float b(View view) {
        if (f) {
            try {
                return view.getTransitionAlpha();
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        return view.getAlpha();
    }

    @DexIgnore
    public void c(View view) {
    }
}
