package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w5 {
    @DexIgnore
    public static /* final */ int bottom; // = 2131361915;
    @DexIgnore
    public static /* final */ int end; // = 2131362177;
    @DexIgnore
    public static /* final */ int gone; // = 2131362463;
    @DexIgnore
    public static /* final */ int invisible; // = 2131362523;
    @DexIgnore
    public static /* final */ int left; // = 2131362651;
    @DexIgnore
    public static /* final */ int packed; // = 2131362769;
    @DexIgnore
    public static /* final */ int parent; // = 2131362773;
    @DexIgnore
    public static /* final */ int percent; // = 2131362784;
    @DexIgnore
    public static /* final */ int right; // = 2131362837;
    @DexIgnore
    public static /* final */ int spread; // = 2131362964;
    @DexIgnore
    public static /* final */ int spread_inside; // = 2131362965;
    @DexIgnore
    public static /* final */ int start; // = 2131362972;
    @DexIgnore
    public static /* final */ int top; // = 2131363041;
    @DexIgnore
    public static /* final */ int wrap; // = 2131363347;
}
