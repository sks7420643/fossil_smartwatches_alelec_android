package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a82 implements Parcelable.Creator<g72> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                str = f22.e(parcel, a);
            } else if (a2 == 2) {
                str2 = f22.e(parcel, a);
            } else if (a2 == 4) {
                str3 = f22.e(parcel, a);
            } else if (a2 == 5) {
                i = f22.q(parcel, a);
            } else if (a2 != 6) {
                f22.v(parcel, a);
            } else {
                i2 = f22.q(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new g72(str, str2, str3, i, i2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new g72[i];
    }
}
