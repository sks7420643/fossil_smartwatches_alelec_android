package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class er3 extends cr3<Bundle> {
    @DexIgnore
    public er3(int i, int i2, Bundle bundle) {
        super(i, 1, bundle);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("data");
        if (bundle2 == null) {
            bundle2 = Bundle.EMPTY;
        }
        a(bundle2);
    }

    @DexIgnore
    public final boolean a() {
        return false;
    }
}
