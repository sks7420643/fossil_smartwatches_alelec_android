package com.fossil;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj1 {
    @DexIgnore
    public /* final */ Hashtable<hl1, LinkedHashSet<if1>> a; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Hashtable<hl1, Integer> b;

    @DexIgnore
    public /* synthetic */ nj1(Hashtable hashtable, qg6 qg6) {
        this.b = hashtable;
        Set<hl1> keySet = this.b.keySet();
        wg6.a(keySet, "resourceQuotas.keys");
        for (hl1 put : keySet) {
            this.a.put(put, new LinkedHashSet());
        }
    }

    @DexIgnore
    public final boolean a(if1 if1) {
        Object obj;
        synchronized (this.a) {
            synchronized (this.b) {
                oa1 oa1 = oa1.a;
                Object[] objArr = {cw0.a((Enum<?>) if1.y), if1.z, this.a};
                Iterator<hl1> it = if1.f().iterator();
                while (it.hasNext()) {
                    hl1 next = it.next();
                    LinkedHashSet linkedHashSet = this.a.get(next);
                    if (linkedHashSet == null) {
                        linkedHashSet = new LinkedHashSet();
                    }
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    wg6.a(num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator it2 = linkedHashSet.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it2.next();
                        if (((if1) obj).b(if1)) {
                            break;
                        }
                    }
                    if (obj == null) {
                        if (linkedHashSet.size() < intValue) {
                            linkedHashSet.add(if1);
                            this.a.put(next, linkedHashSet);
                        } else {
                            Set<Map.Entry<hl1, LinkedHashSet<if1>>> entrySet = this.a.entrySet();
                            wg6.a(entrySet, "resourceHolders.entries");
                            for (Map.Entry value : entrySet) {
                                ((LinkedHashSet) value.getValue()).remove(if1);
                            }
                            oa1 oa12 = oa1.a;
                            Object[] objArr2 = {cw0.a((Enum<?>) if1.y), if1.z, this.a};
                            return false;
                        }
                    }
                }
                oa1 oa13 = oa1.a;
                Object[] objArr3 = {cw0.a((Enum<?>) if1.y), if1.z, this.a};
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(if1 if1) {
        synchronized (this.a) {
            synchronized (this.b) {
                oa1 oa1 = oa1.a;
                Object[] objArr = {cw0.a((Enum<?>) if1.y), if1.z, this.a};
                Iterator<hl1> it = if1.f().iterator();
                while (it.hasNext()) {
                    LinkedHashSet linkedHashSet = this.a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(if1);
                    }
                }
                oa1 oa12 = oa1.a;
                Object[] objArr2 = {cw0.a((Enum<?>) if1.y), if1.z, this.a};
                cd6 cd6 = cd6.a;
            }
            cd6 cd62 = cd6.a;
        }
    }
}
