package com.fossil;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f96 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicLong b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f96$a$a")
        /* renamed from: com.fossil.f96$a$a  reason: collision with other inner class name */
        public class C0011a extends y86 {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable a;

            @DexIgnore
            public C0011a(a aVar, Runnable runnable) {
                this.a = runnable;
            }

            @DexIgnore
            public void a() {
                this.a.run();
            }
        }

        @DexIgnore
        public a(String str, AtomicLong atomicLong) {
            this.a = str;
            this.b = atomicLong;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new C0011a(this, runnable));
            newThread.setName(this.a + this.b.getAndIncrement());
            return newThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends y86 {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ TimeUnit d;

        @DexIgnore
        public b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.a = str;
            this.b = executorService;
            this.c = j;
            this.d = timeUnit;
        }

        @DexIgnore
        public void a() {
            try {
                l86 g = c86.g();
                g.d("Fabric", "Executing shutdown hook for " + this.a);
                this.b.shutdown();
                if (!this.b.awaitTermination(this.c, this.d)) {
                    l86 g2 = c86.g();
                    g2.d("Fabric", this.a + " did not shut down in the allocated time. Requesting immediate shutdown.");
                    this.b.shutdownNow();
                }
            } catch (InterruptedException unused) {
                c86.g().d("Fabric", String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", new Object[]{this.a}));
                this.b.shutdownNow();
            }
        }
    }

    @DexIgnore
    public static ExecutorService a(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(c(str));
        a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    @DexIgnore
    public static ScheduledExecutorService b(String str) {
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor(c(str));
        a(str, newSingleThreadScheduledExecutor);
        return newSingleThreadScheduledExecutor;
    }

    @DexIgnore
    public static final ThreadFactory c(String str) {
        return new a(str, new AtomicLong(1));
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService) {
        a(str, executorService, 2, TimeUnit.SECONDS);
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        b bVar = new b(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bVar, "Crashlytics Shutdown Hook for " + str));
    }
}
