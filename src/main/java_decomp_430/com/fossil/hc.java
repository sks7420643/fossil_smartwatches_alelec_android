package com.fossil;

import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import java.lang.reflect.Modifier;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hc {
    @DexIgnore
    public ArrayList<a> a; // = new ArrayList<>();
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public int l;
    @DexIgnore
    public CharSequence m;
    @DexIgnore
    public ArrayList<String> n;
    @DexIgnore
    public ArrayList<String> o;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public ArrayList<Runnable> q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public Fragment b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public Lifecycle.State g;
        @DexIgnore
        public Lifecycle.State h;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a(int i, Fragment fragment) {
            this.a = i;
            this.b = fragment;
            Lifecycle.State state = Lifecycle.State.RESUMED;
            this.g = state;
            this.h = state;
        }

        @DexIgnore
        public a(int i, Fragment fragment, Lifecycle.State state) {
            this.a = i;
            this.b = fragment;
            this.g = fragment.mMaxState;
            this.h = state;
        }
    }

    @DexIgnore
    public hc(xb xbVar, ClassLoader classLoader) {
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public void a(a aVar) {
        this.a.add(aVar);
        aVar.c = this.b;
        aVar.d = this.c;
        aVar.e = this.d;
        aVar.f = this.e;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public hc b(int i2, Fragment fragment, String str) {
        if (i2 != 0) {
            a(i2, fragment, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    @DexIgnore
    public hc c(Fragment fragment) {
        a(new a(4, fragment));
        return this;
    }

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public hc d(Fragment fragment) {
        a(new a(3, fragment));
        return this;
    }

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public hc e(Fragment fragment) {
        a(new a(5, fragment));
        return this;
    }

    @DexIgnore
    public hc e() {
        if (!this.g) {
            this.h = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    @DexIgnore
    public hc b(Fragment fragment) {
        a(new a(6, fragment));
        return this;
    }

    @DexIgnore
    public hc a(Fragment fragment, String str) {
        a(0, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public hc a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public hc a(ViewGroup viewGroup, Fragment fragment, String str) {
        fragment.mContainer = viewGroup;
        a(viewGroup.getId(), fragment, str);
        return this;
    }

    @DexIgnore
    public void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from instance state.");
        }
        if (str != null) {
            String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.mFragmentId;
                if (i4 == 0 || i4 == i2) {
                    fragment.mFragmentId = i2;
                    fragment.mContainerId = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        a(new a(i3, fragment));
    }

    @DexIgnore
    public hc a(int i2, Fragment fragment) {
        b(i2, fragment, (String) null);
        return this;
    }

    @DexIgnore
    public hc a(Fragment fragment) {
        a(new a(7, fragment));
        return this;
    }

    @DexIgnore
    public hc a(Fragment fragment, Lifecycle.State state) {
        a(new a(10, fragment, state));
        return this;
    }

    @DexIgnore
    public hc a(String str) {
        if (this.h) {
            this.g = true;
            this.i = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    @DexIgnore
    public hc a(boolean z) {
        this.p = z;
        return this;
    }
}
