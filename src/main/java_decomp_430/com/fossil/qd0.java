package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ pd0 b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ short h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public /* final */ td0 j;
    @DexIgnore
    public /* final */ ud0 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                wg6.a(createByteArray, "parcel.createByteArray()!!");
                Parcelable readParcelable = parcel.readParcelable(pd0.class.getClassLoader());
                if (readParcelable != null) {
                    return new qd0(createByteArray, (pd0) readParcelable);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new qd0[i];
        }
    }

    @DexIgnore
    public qd0(byte[] bArr, pd0 pd0) {
        this.a = bArr;
        this.b = pd0;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        boolean z = false;
        this.c = order.get(0);
        this.d = order.get(1);
        this.e = order.get(2);
        this.f = order.getShort(3);
        this.g = order.getShort(7) > 0 ? true : z;
        this.h = cw0.b(order.get(9));
        this.i = order.get(10);
        this.j = qa1.b.a(this.f);
        this.k = qa1.b.b(this.f);
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.MICRO_APP_ID, (Object) this.j.name()), bm0.MICRO_APP_VERSION, (Object) Byte.valueOf(this.e)), bm0.MINOR_VERSION, (Object) Byte.valueOf(this.d)), bm0.MAJOR_VERSION, (Object) Byte.valueOf(this.c)), bm0.VARIATION_NUMBER, (Object) Byte.valueOf(this.i)), bm0.VARIANT, (Object) this.k), bm0.RUN_TIME, (Object) Short.valueOf(this.h)), bm0.HAS_CUSTOMIZATION, (Object) Boolean.valueOf(this.g)), bm0.CRC, (Object) Long.valueOf(h51.a.a(this.a, q11.CRC32)));
    }

    @DexIgnore
    public final short b() {
        return this.f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(qd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            qd0 qd0 = (qd0) obj;
            return Arrays.equals(this.a, qd0.a) && !(wg6.a(this.b, qd0.b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration");
    }

    @DexIgnore
    public final pd0 getCustomization() {
        return this.b;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public final boolean getHasCustomization() {
        return this.g;
    }

    @DexIgnore
    public final byte getMajorVersion() {
        return this.c;
    }

    @DexIgnore
    public final td0 getMicroAppId() {
        return this.j;
    }

    @DexIgnore
    public final ud0 getMicroAppVariantId() {
        return this.k;
    }

    @DexIgnore
    public final byte getMicroAppVersion() {
        return this.e;
    }

    @DexIgnore
    public final byte getMinorVersion() {
        return this.d;
    }

    @DexIgnore
    public final byte getVariationNumber() {
        return this.i;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.a) * 31;
        pd0 pd0 = this.b;
        return hashCode + (pd0 != null ? pd0.hashCode() : 0);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i2);
        }
    }
}
