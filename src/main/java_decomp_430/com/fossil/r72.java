package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r72 implements Parcelable.Creator<l72> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        Bundle bundle = null;
        int[] iArr = null;
        float[] fArr = null;
        byte[] bArr = null;
        int i = 0;
        boolean z = false;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel, a);
                    break;
                case 2:
                    z = f22.i(parcel, a);
                    break;
                case 3:
                    f = f22.n(parcel, a);
                    break;
                case 4:
                    str = f22.e(parcel, a);
                    break;
                case 5:
                    bundle = f22.a(parcel, a);
                    break;
                case 6:
                    iArr = f22.d(parcel, a);
                    break;
                case 7:
                    fArr = f22.c(parcel, a);
                    break;
                case 8:
                    bArr = f22.b(parcel, a);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new l72(i, z, f, str, bundle, iArr, fArr, bArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new l72[i];
    }
}
