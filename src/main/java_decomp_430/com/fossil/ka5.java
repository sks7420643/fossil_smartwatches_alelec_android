package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka5 implements Factory<ja5> {
    @DexIgnore
    public static /* final */ ka5 a; // = new ka5();

    @DexIgnore
    public static ka5 a() {
        return a;
    }

    @DexIgnore
    public static ja5 b() {
        return new ja5();
    }

    @DexIgnore
    public ja5 get() {
        return b();
    }
}
