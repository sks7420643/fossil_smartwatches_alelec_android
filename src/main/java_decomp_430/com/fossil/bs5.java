package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs5 {
    @DexIgnore
    public as5 a;

    @DexIgnore
    public bs5(as5 as5) {
        wg6.b(as5, "mPairingInstructionsView");
        this.a = as5;
    }

    @DexIgnore
    public final as5 a() {
        return this.a;
    }
}
