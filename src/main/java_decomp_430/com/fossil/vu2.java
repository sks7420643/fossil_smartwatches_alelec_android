package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu2 implements su2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a; // = new vk2(qk2.a("com.google.android.gms.measurement")).a("measurement.integration.disable_firebase_instance_id", false);

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
