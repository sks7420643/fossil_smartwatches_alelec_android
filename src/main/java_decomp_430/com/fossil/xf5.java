package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xf5 implements Factory<wf5> {
    @DexIgnore
    public static HeartRateOverviewWeekPresenter a(vf5 vf5, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new HeartRateOverviewWeekPresenter(vf5, userRepository, heartRateSummaryRepository);
    }
}
