package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h32 implements Parcelable.Creator<y12> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        IBinder iBinder = null;
        gv1 gv1 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                iBinder = f22.p(parcel, a);
            } else if (a2 == 3) {
                gv1 = f22.a(parcel, a, gv1.CREATOR);
            } else if (a2 == 4) {
                z = f22.i(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                z2 = f22.i(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new y12(i, iBinder, gv1, z, z2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new y12[i];
    }
}
