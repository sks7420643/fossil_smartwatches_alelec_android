package com.fossil;

import com.fossil.ej5;
import com.fossil.fj5;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yi5 extends k24<xi5> {
    @DexIgnore
    void a(MFSleepDay mFSleepDay);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c0();

    @DexIgnore
    void d(ArrayList<ej5.a> arrayList);

    @DexIgnore
    void p(List<fj5.b> list);
}
