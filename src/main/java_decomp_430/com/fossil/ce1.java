package com.fossil;

import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ce1 {
    @DexIgnore
    public /* final */ LinkedList<q81> a; // = new LinkedList<>();
    @DexIgnore
    public q81 b;
    @DexIgnore
    public /* final */ fu0 c;

    @DexIgnore
    public ce1(fu0 fu0) {
        this.c = fu0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r3.a.add(r4);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public final void a(q81 q81) {
        q81 poll;
        oa1 oa1 = oa1.a;
        new Object[1][0] = cw0.a((Enum<?>) q81.k);
        synchronized (this.a) {
            this.a.add(c(q81), q81);
            cd6 cd6 = cd6.a;
        }
        if (this.b == null && (poll = this.a.poll()) != null) {
            b(poll);
        }
    }

    @DexIgnore
    public final void b(q81 q81) {
        this.b = q81;
        q81 q812 = this.b;
        if (q812 != null) {
            q812.i = new hc1(this);
            fu0 fu0 = this.c;
            if (!q812.e) {
                oa1 oa1 = oa1.a;
                new Object[1][0] = q812.a(true);
                hn1<p51> c2 = q812.c();
                if (c2 != null) {
                    c2.a((ef0<p51>) q812);
                }
                q812.a(fu0);
                q812.d.postDelayed(new dr0(new x41(q812)), q812.c);
            }
        }
    }

    @DexIgnore
    public final int c(q81 q81) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (q81.a < this.a.get(i).a) {
                    return i;
                }
            }
            int size2 = this.a.size();
            return size2;
        }
    }
}
