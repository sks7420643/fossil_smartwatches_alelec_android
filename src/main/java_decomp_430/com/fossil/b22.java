package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b22<T extends IInterface> extends i12<T> {
    @DexIgnore
    public /* final */ rv1.h<T> E;

    @DexIgnore
    public String A() {
        return this.E.p();
    }

    @DexIgnore
    public rv1.h<T> H() {
        return this.E;
    }

    @DexIgnore
    public T a(IBinder iBinder) {
        return this.E.a(iBinder);
    }

    @DexIgnore
    public String z() {
        return this.E.o();
    }

    @DexIgnore
    public void a(int i, T t) {
        this.E.a(i, t);
    }
}
