package com.fossil;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.facebook.login.LoginStatusClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e73 extends z33 {
    @DexIgnore
    public v73 c;
    @DexIgnore
    public y63 d;
    @DexIgnore
    public /* final */ Set<c73> e; // = new CopyOnWriteArraySet();
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ AtomicReference<String> g; // = new AtomicReference<>();
    @DexIgnore
    public boolean h; // = true;

    @DexIgnore
    public e73(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final void A() {
        if (c().getApplicationContext() instanceof Application) {
            ((Application) c().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    @DexIgnore
    public final Boolean B() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) a().a(atomicReference, 15000, "boolean test flag value", new g73(this, atomicReference));
    }

    @DexIgnore
    public final String C() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) a().a(atomicReference, 15000, "String test flag value", new o73(this, atomicReference));
    }

    @DexIgnore
    public final Long D() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) a().a(atomicReference, 15000, "long test flag value", new q73(this, atomicReference));
    }

    @DexIgnore
    public final Integer E() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) a().a(atomicReference, 15000, "int test flag value", new p73(this, atomicReference));
    }

    @DexIgnore
    public final Double F() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) a().a(atomicReference, 15000, "double test flag value", new s73(this, atomicReference));
    }

    @DexIgnore
    public final String G() {
        e();
        return this.g.get();
    }

    @DexIgnore
    public final void H() {
        g();
        e();
        w();
        if (this.a.l()) {
            if (l().e(this.a.H().A(), l03.C0)) {
                cb3 l = l();
                l.d();
                Boolean b = l.b("google_analytics_deferred_deep_link_enabled");
                if (b != null && b.booleanValue()) {
                    b().A().a("Deferred Deep Link feature enabled.");
                    a().a((Runnable) new d73(this));
                }
            }
            q().D();
            this.h = false;
            String x = k().x();
            if (!TextUtils.isEmpty(x)) {
                h().n();
                if (!x.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", x);
                    a("auto", "_ou", bundle);
                }
            }
        }
    }

    @DexIgnore
    public final String I() {
        h83 B = this.a.E().B();
        if (B != null) {
            return B.a;
        }
        return null;
    }

    @DexIgnore
    public final String J() {
        h83 B = this.a.E().B();
        if (B != null) {
            return B.b;
        }
        return null;
    }

    @DexIgnore
    public final String K() {
        if (this.a.A() != null) {
            return this.a.A();
        }
        try {
            return rw1.a();
        } catch (IllegalStateException e2) {
            this.a.b().t().a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    @DexIgnore
    public final void L() {
        if (l().e(p().A(), l03.j0)) {
            g();
            String a = k().s.a();
            if (a != null) {
                if ("unset".equals(a)) {
                    a("app", "_npa", (Object) null, zzm().b());
                } else {
                    a("app", "_npa", (Object) Long.valueOf(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(a) ? 1 : 0), zzm().b());
                }
            }
        }
        if (!this.a.g() || !this.h) {
            b().A().a("Updating Scion state (FE)");
            q().B();
            return;
        }
        b().A().a("Recording app launch after enabling measurement for the first time (FE)");
        H();
    }

    @DexIgnore
    public final void a(boolean z) {
        w();
        e();
        a().a((Runnable) new r73(this, z));
    }

    @DexIgnore
    public final void b(boolean z) {
        w();
        e();
        a().a((Runnable) new u73(this, z));
    }

    @DexIgnore
    public final void c(boolean z) {
        g();
        e();
        w();
        b().A().a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        k().b(z);
        L();
    }

    @DexIgnore
    public final void d(Bundle bundle) {
        Bundle bundle2 = bundle;
        g();
        w();
        w12.a(bundle);
        w12.b(bundle2.getString("name"));
        if (!this.a.g()) {
            b().A().a("Conditional property not cleared since collection is disabled");
            return;
        }
        la3 la3 = new la3(bundle2.getString("name"), 0, (Object) null, (String) null);
        try {
            j03 a = j().a(bundle2.getString("app_id"), bundle2.getString("expired_event_name"), bundle2.getBundle("expired_event_params"), bundle2.getString("origin"), bundle2.getLong("creation_timestamp"), true, false);
            la3 la32 = la3;
            q().a(new ab3(bundle2.getString("app_id"), bundle2.getString("origin"), la32, bundle2.getLong("creation_timestamp"), bundle2.getBoolean("active"), bundle2.getString("trigger_event_name"), (j03) null, bundle2.getLong("trigger_timeout"), (j03) null, bundle2.getLong("time_to_live"), a));
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final void a(long j) {
        e();
        a().a((Runnable) new t73(this, j));
    }

    @DexIgnore
    public final void b(long j) {
        e();
        a().a((Runnable) new w73(this, j));
    }

    @DexIgnore
    public final void c(long j) {
        a((String) null);
        a().a((Runnable) new i73(this, j));
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z) {
        a(str, str2, bundle, false, true, zzm().b());
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        e();
        g();
        a(str, str2, zzm().b(), bundle);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, zzm().b());
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Bundle bundle) {
        e();
        g();
        a(str, str2, j, bundle, true, this.d == null || ma3.f(str2), false, (String) null);
    }

    @DexIgnore
    public final void c(String str, String str2, Bundle bundle) {
        e();
        b((String) null, str, str2, bundle);
    }

    @DexIgnore
    public final void b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        a().a((Runnable) new f73(this, str, str2, j, ma3.b(bundle), z, z2, z3, str3));
    }

    @DexIgnore
    public final void c(Bundle bundle) {
        Bundle bundle2 = bundle;
        g();
        w();
        w12.a(bundle);
        w12.b(bundle2.getString("name"));
        w12.b(bundle2.getString("origin"));
        w12.a(bundle2.get("value"));
        if (!this.a.g()) {
            b().A().a("Conditional property not sent since collection is disabled");
            return;
        }
        la3 la3 = new la3(bundle2.getString("name"), bundle2.getLong("triggered_timestamp"), bundle2.get("value"), bundle2.getString("origin"));
        try {
            j03 a = j().a(bundle2.getString("app_id"), bundle2.getString("triggered_event_name"), bundle2.getBundle("triggered_event_params"), bundle2.getString("origin"), 0, true, false);
            q().a(new ab3(bundle2.getString("app_id"), bundle2.getString("origin"), la3, bundle2.getLong("creation_timestamp"), false, bundle2.getString("trigger_event_name"), j().a(bundle2.getString("app_id"), bundle2.getString("timed_out_event_name"), bundle2.getBundle("timed_out_event_params"), bundle2.getString("origin"), 0, true, false), bundle2.getLong("trigger_timeout"), a, bundle2.getLong("time_to_live"), j().a(bundle2.getString("app_id"), bundle2.getString("expired_event_name"), bundle2.getBundle("expired_event_params"), bundle2.getString("origin"), 0, true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        String str4;
        String str5;
        String str6;
        String str7;
        String[] strArr;
        String str8;
        String str9;
        int i;
        h83 h83;
        int i2;
        long j2;
        ArrayList arrayList;
        Bundle bundle2;
        String str10;
        boolean z4;
        Class<?> cls;
        List<String> F;
        String str11 = str;
        String str12 = str2;
        long j3 = j;
        Bundle bundle3 = bundle;
        String str13 = str3;
        w12.b(str);
        w12.a(bundle);
        g();
        w();
        if (!this.a.g()) {
            b().A().a("Event not sent since app measurement is disabled");
        } else if (!l().e(p().A(), l03.t0) || (F = p().F()) == null || F.contains(str12)) {
            int i3 = 0;
            if (!this.f) {
                this.f = true;
                try {
                    if (!this.a.D()) {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService", true, c().getClassLoader());
                    } else {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService");
                    }
                    try {
                        cls.getDeclaredMethod("initialize", new Class[]{Context.class}).invoke((Object) null, new Object[]{c()});
                    } catch (Exception e2) {
                        b().w().a("Failed to invoke Tag Manager's initialize() method", e2);
                    }
                } catch (ClassNotFoundException unused) {
                    b().z().a("Tag Manager is not found and thus will not be used");
                }
            }
            if (l().e(p().A(), l03.E0) && "_cmp".equals(str12) && bundle3.containsKey("gclid")) {
                a("auto", "_lgclid", (Object) bundle3.getString("gclid"), zzm().b());
            }
            if (z3) {
                d();
                if (!"_iap".equals(str12)) {
                    ma3 w = this.a.w();
                    int i4 = 2;
                    if (w.a("event", str12)) {
                        if (!w.a("event", x63.a, str12)) {
                            i4 = 13;
                        } else if (w.a("event", 40, str12)) {
                            i4 = 0;
                        }
                    }
                    if (i4 != 0) {
                        b().v().a("Invalid public event name. Event will not be logged (FE)", i().a(str12));
                        this.a.w();
                        this.a.w().a(i4, "_ev", ma3.a(str12, 40, true), str12 != null ? str2.length() : 0);
                        return;
                    }
                }
            }
            d();
            h83 A = r().A();
            if (A != null && !bundle3.containsKey("_sc")) {
                A.d = true;
            }
            g83.a(A, bundle3, z && z3);
            boolean equals = "am".equals(str11);
            boolean f2 = ma3.f(str2);
            if (z && this.d != null && !f2 && !equals) {
                b().A().a("Passing event to registered event handler (FE)", i().a(str12), i().a(bundle3));
                this.d.a(str, str2, bundle, j);
            } else if (this.a.l()) {
                int a = j().a(str12);
                if (a != 0) {
                    b().v().a("Invalid event name. Event will not be logged (FE)", i().a(str12));
                    j();
                    String a2 = ma3.a(str12, 40, true);
                    if (str12 != null) {
                        i3 = str2.length();
                    }
                    this.a.w().a(str3, a, "_ev", a2, i3);
                    return;
                }
                List a3 = l42.a((T[]) new String[]{"_o", "_sn", "_sc", "_si"});
                String str14 = str13;
                String str15 = str12;
                Bundle a4 = j().a(str3, str2, bundle, a3, z3, true);
                h83 h832 = (a4 == null || !a4.containsKey("_sc") || !a4.containsKey("_si")) ? null : new h83(a4.getString("_sn"), a4.getString("_sc"), Long.valueOf(a4.getLong("_si")).longValue());
                h83 h833 = h832 == null ? A : h832;
                String str16 = "_ae";
                if (l().p(str14)) {
                    d();
                    if (r().A() != null && str16.equals(str15)) {
                        long b = t().e.b();
                        if (b > 0) {
                            j().a(a4, b);
                        }
                    }
                }
                if (es2.a() && l().a(l03.T0)) {
                    if (!"auto".equals(str11) && "_ssr".equals(str15)) {
                        ma3 j4 = j();
                        String string = a4.getString("_ffr");
                        if (u42.a(string)) {
                            str10 = null;
                        } else {
                            str10 = string.trim();
                        }
                        if (ma3.d(str10, j4.k().B.a())) {
                            j4.b().A().a("Not logging duplicate session_start_with_rollout event");
                            z4 = false;
                        } else {
                            j4.k().B.a(str10);
                            z4 = true;
                        }
                        if (!z4) {
                            return;
                        }
                    } else if (str16.equals(str15)) {
                        String a5 = j().k().B.a();
                        if (!TextUtils.isEmpty(a5)) {
                            a4.putString("_ffr", a5);
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(a4);
                long nextLong = j().t().nextLong();
                if (!l().e(p().A(), l03.a0) || k().v.a() <= 0 || !k().a(j) || !k().y.a()) {
                    str4 = "_o";
                } else {
                    b().B().a("Current session is expired, remove the session number, ID, and engagement time");
                    if (l().e(p().A(), l03.W)) {
                        String str17 = str14;
                        str4 = "_o";
                        a("auto", "_sid", (Object) null, zzm().b());
                    } else {
                        str4 = "_o";
                    }
                    if (l().e(p().A(), l03.X)) {
                        a("auto", "_sno", (Object) null, zzm().b());
                    }
                    if (iu2.a() && l().e(p().A(), l03.y0)) {
                        a("auto", "_se", (Object) null, zzm().b());
                    }
                }
                if (!l().o(p().A()) || a4.getLong("extend_session", 0) != 1) {
                    str5 = str2;
                    long j5 = j;
                } else {
                    b().B().a("EXTEND_SESSION param attached: initiate a new session or extend the current active session");
                    str5 = str2;
                    this.a.s().d.a(j, true);
                }
                String[] strArr2 = (String[]) a4.keySet().toArray(new String[bundle.size()]);
                Arrays.sort(strArr2);
                int length = strArr2.length;
                int i5 = 0;
                int i6 = 0;
                while (i5 < length) {
                    String str18 = strArr2[i5];
                    Object obj = a4.get(str18);
                    j();
                    Bundle[] a6 = ma3.a(obj);
                    if (a6 != null) {
                        strArr = strArr2;
                        a4.putInt(str18, a6.length);
                        int i7 = 0;
                        while (i7 < a6.length) {
                            Bundle bundle4 = a6[i7];
                            g83.a(h833, bundle4, true);
                            int i8 = i5;
                            String str19 = str4;
                            long j6 = nextLong;
                            Bundle bundle5 = bundle4;
                            ArrayList arrayList3 = arrayList2;
                            Bundle a7 = j().a(str3, "_ep", bundle5, a3, z3, false);
                            a7.putString("_en", str5);
                            a7.putLong("_eid", j6);
                            a7.putString("_gn", str18);
                            a7.putInt("_ll", a6.length);
                            a7.putInt("_i", i7);
                            arrayList3.add(a7);
                            i7++;
                            a4 = a4;
                            arrayList2 = arrayList3;
                            nextLong = j6;
                            length = length;
                            h833 = h833;
                            i5 = i8;
                            str4 = str19;
                            str16 = str16;
                            String str20 = str;
                        }
                        h83 = h833;
                        str9 = str4;
                        i = i5;
                        i2 = length;
                        j2 = nextLong;
                        arrayList = arrayList2;
                        str8 = str16;
                        bundle2 = a4;
                        i6 += a6.length;
                    } else {
                        h83 = h833;
                        strArr = strArr2;
                        str9 = str4;
                        i = i5;
                        i2 = length;
                        j2 = nextLong;
                        arrayList = arrayList2;
                        str8 = str16;
                        bundle2 = a4;
                        int i9 = i6;
                    }
                    i5 = i + 1;
                    long j7 = j;
                    strArr2 = strArr;
                    a4 = bundle2;
                    arrayList2 = arrayList;
                    nextLong = j2;
                    length = i2;
                    h833 = h83;
                    str4 = str9;
                    str16 = str8;
                    String str21 = str;
                }
                String str22 = str4;
                long j8 = nextLong;
                ArrayList arrayList4 = arrayList2;
                String str23 = str16;
                Bundle bundle6 = a4;
                int i10 = i6;
                if (i10 != 0) {
                    bundle6.putLong("_eid", j8);
                    bundle6.putInt("_epc", i10);
                }
                int i11 = 0;
                while (i11 < arrayList4.size()) {
                    Bundle bundle7 = (Bundle) arrayList4.get(i11);
                    if (i11 != 0) {
                        str7 = "_ep";
                        str6 = str;
                    } else {
                        str6 = str;
                        str7 = str5;
                    }
                    String str24 = str22;
                    bundle7.putString(str24, str6);
                    if (z2) {
                        bundle7 = j().a(bundle7);
                    }
                    Bundle bundle8 = bundle7;
                    b().A().a("Logging event (FE)", i().a(str5), i().a(bundle8));
                    ArrayList arrayList5 = arrayList4;
                    String str25 = str5;
                    String str26 = str24;
                    q().a(new j03(str7, new i03(bundle8), str, j), str3);
                    if (!equals) {
                        for (c73 onEvent : this.e) {
                            onEvent.onEvent(str, str2, new Bundle(bundle8), j);
                        }
                    }
                    i11++;
                    str5 = str25;
                    arrayList4 = arrayList5;
                    str22 = str26;
                }
                String str27 = str5;
                d();
                if (r().A() != null && str23.equals(str27)) {
                    t().a(true, true);
                }
            }
        } else {
            b().A().a("Dropping non-safelisted event. event name, origin", str12, str11);
        }
    }

    @DexIgnore
    public final void b(c73 c73) {
        e();
        w();
        w12.a(c73);
        if (!this.e.remove(c73)) {
            b().w().a("OnEventListener had not been registered");
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        w12.a(bundle);
        w12.b(bundle.getString("app_id"));
        m();
        throw null;
    }

    @DexIgnore
    public final void b(Bundle bundle, long j) {
        w12.a(bundle);
        u63.a(bundle, "app_id", String.class, null);
        u63.a(bundle, "origin", String.class, null);
        u63.a(bundle, "name", String.class, null);
        u63.a(bundle, "value", Object.class, null);
        u63.a(bundle, "trigger_event_name", String.class, null);
        u63.a(bundle, "trigger_timeout", Long.class, 0L);
        u63.a(bundle, "timed_out_event_name", String.class, null);
        u63.a(bundle, "timed_out_event_params", Bundle.class, null);
        u63.a(bundle, "triggered_event_name", String.class, null);
        u63.a(bundle, "triggered_event_params", Bundle.class, null);
        u63.a(bundle, "time_to_live", Long.class, 0L);
        u63.a(bundle, "expired_event_name", String.class, null);
        u63.a(bundle, "expired_event_params", Bundle.class, null);
        w12.b(bundle.getString("name"));
        w12.b(bundle.getString("origin"));
        w12.a(bundle.get("value"));
        bundle.putLong("creation_timestamp", j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (j().b(string) != 0) {
            b().t().a("Invalid conditional user property name", i().c(string));
        } else if (j().b(string, obj) != 0) {
            b().t().a("Invalid conditional user property value", i().c(string), obj);
        } else {
            Object c2 = j().c(string, obj);
            if (c2 == null) {
                b().t().a("Unable to normalize conditional user property value", i().c(string), obj);
                return;
            }
            u63.a(bundle, c2);
            long j2 = bundle.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle.getString("trigger_event_name")) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong("time_to_live");
                if (j3 > 15552000000L || j3 < 1) {
                    b().t().a("Invalid conditional user property time to live", i().c(string), Long.valueOf(j3));
                } else {
                    a().a((Runnable) new j73(this, bundle));
                }
            } else {
                b().t().a("Invalid conditional user property timeout", i().c(string), Long.valueOf(j2));
            }
        }
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Bundle bundle) {
        long b = zzm().b();
        w12.b(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong("creation_timestamp", b);
        if (str3 != null) {
            bundle2.putString("expired_event_name", str3);
            bundle2.putBundle("expired_event_params", bundle);
        }
        a().a((Runnable) new m73(this, bundle2));
    }

    @DexIgnore
    public final ArrayList<Bundle> b(String str, String str2, String str3) {
        if (a().s()) {
            b().t().a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (bb3.a()) {
            b().t().a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.a.a().a((Runnable) new l73(this, atomicReference, str, str2, str3));
                try {
                    atomicReference.wait(LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
                } catch (InterruptedException e2) {
                    b().w().a("Interrupted waiting for get conditional user properties", str, e2);
                }
            }
            List list = (List) atomicReference.get();
            if (list != null) {
                return ma3.b((List<ab3>) list);
            }
            b().w().a("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    @DexIgnore
    public final Map<String, Object> b(String str, String str2, String str3, boolean z) {
        if (a().s()) {
            b().t().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (bb3.a()) {
            b().t().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.a.a().a((Runnable) new n73(this, atomicReference, str, str2, str3, z));
                try {
                    atomicReference.wait(LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
                } catch (InterruptedException e2) {
                    b().w().a("Interrupted waiting for get user properties", e2);
                }
            }
            List<la3> list = (List) atomicReference.get();
            if (list == null) {
                b().w().a("Timed out waiting for handle get user properties");
                return Collections.emptyMap();
            }
            p4 p4Var = new p4(list.size());
            for (la3 la3 : list) {
                p4Var.put(la3.b, la3.zza());
            }
            return p4Var;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        e();
        b(str == null ? "app" : str, str2, j, bundle == null ? new Bundle() : bundle, z2, !z2 || this.d == null || ma3.f(str2), !z, (String) null);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z) {
        a(str, str2, obj, z, zzm().b());
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = j().b(str2);
        } else {
            ma3 j2 = j();
            if (j2.a("user property", str2)) {
                if (!j2.a("user property", z63.a, str2)) {
                    i = 15;
                } else if (j2.a("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            j();
            String a = ma3.a(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.a.w().a(i, "_ev", a, i2);
        } else if (obj != null) {
            int b = j().b(str2, obj);
            if (b != 0) {
                j();
                String a2 = ma3.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.a.w().a(b, "_ev", a2, i2);
                return;
            }
            Object c2 = j().c(str2, obj);
            if (c2 != null) {
                a(str3, str2, j, c2);
            }
        } else {
            a(str3, str2, j, (Object) null);
        }
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Object obj) {
        a().a((Runnable) new h73(this, str, str2, obj, j));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008f  */
    public final void a(String str, String str2, Object obj, long j) {
        Long l;
        String str3;
        w12.b(str);
        w12.b(str2);
        g();
        e();
        w();
        if (l().e(p().A(), l03.j0) && "allow_personalized_ads".equals(str2)) {
            if (obj instanceof String) {
                String str4 = (String) obj;
                if (!TextUtils.isEmpty(str4)) {
                    String str5 = "false";
                    Long valueOf = Long.valueOf(str5.equals(str4.toLowerCase(Locale.ENGLISH)) ? 1 : 0);
                    k53 k53 = k().s;
                    if (valueOf.longValue() == 1) {
                        str5 = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
                    }
                    k53.a(str5);
                    l = valueOf;
                    str3 = "_npa";
                    if (!this.a.g()) {
                        b().A().a("User property not set since app measurement is disabled");
                        return;
                    } else if (this.a.l()) {
                        b().A().a("Setting user property (FE)", i().a(str3), l);
                        q().a(new la3(str3, j, l, str));
                        return;
                    } else {
                        return;
                    }
                }
            }
            if (obj == null) {
                k().s.a("unset");
                l = obj;
                str3 = "_npa";
                if (!this.a.g()) {
                }
            }
        }
        str3 = str2;
        l = obj;
        if (!this.a.g()) {
        }
    }

    @DexIgnore
    public final void a(String str) {
        this.g.set(str);
    }

    @DexIgnore
    public final void a(y63 y63) {
        y63 y632;
        g();
        e();
        w();
        if (!(y63 == null || y63 == (y632 = this.d))) {
            w12.b(y632 == null, "EventInterceptor already set.");
        }
        this.d = y63;
    }

    @DexIgnore
    public final void a(c73 c73) {
        e();
        w();
        w12.a(c73);
        if (!this.e.add(c73)) {
            b().w().a("OnEventListener already registered");
        }
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        a(bundle, zzm().b());
    }

    @DexIgnore
    public final void a(Bundle bundle, long j) {
        w12.a(bundle);
        e();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            b().w().a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        b(bundle2, j);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, Bundle bundle) {
        w12.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final ArrayList<Bundle> a(String str, String str2) {
        e();
        return b((String) null, str, str2);
    }

    @DexIgnore
    public final ArrayList<Bundle> a(String str, String str2, String str3) {
        w12.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        e();
        return b((String) null, str, str2, z);
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        w12.b(str);
        m();
        throw null;
    }
}
