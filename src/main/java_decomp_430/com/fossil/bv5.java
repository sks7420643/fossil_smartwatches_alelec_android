package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ds4;
import com.fossil.m24;
import com.fossil.rr4;
import com.fossil.wr4;
import com.fossil.yr4;
import com.google.gson.Gson;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bv5 extends l24 {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public MutableLiveData<b> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Handler g; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<Device> i;
    @DexIgnore
    public c j;
    @DexIgnore
    public b k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ Runnable m;
    @DexIgnore
    public ld<Device> n;
    @DexIgnore
    public /* final */ f o;
    @DexIgnore
    public /* final */ DeviceRepository p;
    @DexIgnore
    public /* final */ SetVibrationStrengthUseCase q;
    @DexIgnore
    public /* final */ UnlinkDeviceUseCase r;
    @DexIgnore
    public /* final */ cj4 s;
    @DexIgnore
    public /* final */ an4 t;
    @DexIgnore
    public /* final */ SwitchActiveDeviceUseCase u;
    @DexIgnore
    public /* final */ rr4 v;
    @DexIgnore
    public /* final */ PortfolioApp w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bv5.x;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public c b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public lc6<Integer, String> e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore
        public b() {
            this(false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8191, (qg6) null);
        }

        @DexIgnore
        public b(boolean z, c cVar, String str, Integer num, lc6<Integer, String> lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.a = z;
            this.b = cVar;
            this.c = str;
            this.d = num;
            this.e = lc6;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final boolean d() {
            return this.f;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public final lc6<Integer, String> f() {
            return this.e;
        }

        @DexIgnore
        public final boolean g() {
            return this.l;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public final String i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.m;
        }

        @DexIgnore
        public final c k() {
            return this.b;
        }

        @DexIgnore
        public final String l() {
            return this.c;
        }

        @DexIgnore
        public final Integer m() {
            return this.d;
        }

        @DexIgnore
        public String toString() {
            String a2 = new Gson().a(this);
            wg6.a((Object) a2, "Gson().toJson(this)");
            return a2;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(boolean z, c cVar, String str, Integer num, lc6 lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? null : cVar, (r0 & 4) != 0 ? null : str, (r0 & 8) != 0 ? null : num, (r0 & 16) != 0 ? null : lc6, (r0 & 32) != 0 ? false : z2, (r0 & 64) != 0 ? null : str2, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? null : str3, (r0 & 256) != 0 ? null : str4, (r0 & 512) != 0 ? null : str5, (r0 & 1024) == 0 ? str6 : null, (r0 & 2048) != 0 ? false : z3, (r0 & 4096) == 0 ? z4 : r2);
            int i3 = i2;
            boolean z5 = false;
            boolean z6 = (i3 & 1) != 0 ? false : z;
        }

        @DexIgnore
        public final void a(c cVar) {
            this.b = cVar;
        }

        @DexIgnore
        public final void a(String str) {
            this.c = str;
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, boolean z, c cVar, String str, Integer num, lc6 lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, Object obj) {
            int i3 = i2;
            boolean z5 = false;
            boolean z6 = (i3 & 1) != 0 ? false : z;
            String str7 = null;
            c cVar2 = (i3 & 2) != 0 ? null : cVar;
            String str8 = (i3 & 4) != 0 ? null : str;
            Integer num2 = (i3 & 8) != 0 ? null : num;
            lc6 lc62 = (i3 & 16) != 0 ? null : lc6;
            boolean z7 = (i3 & 32) != 0 ? false : z2;
            String str9 = (i3 & 64) != 0 ? null : str2;
            String str10 = (i3 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? null : str3;
            String str11 = (i3 & 256) != 0 ? null : str4;
            String str12 = (i3 & 512) != 0 ? null : str5;
            if ((i3 & 1024) == 0) {
                str7 = str6;
            }
            boolean z8 = (i3 & 2048) != 0 ? false : z3;
            if ((i3 & 4096) == 0) {
                z5 = z4;
            }
            bVar.a(z6, cVar2, str8, num2, lc62, z7, str9, str10, str11, str12, str7, z8, z5);
        }

        @DexIgnore
        public final synchronized void a(boolean z, c cVar, String str, Integer num, lc6<Integer, String> lc6, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.a = z;
            this.b = cVar;
            this.c = str;
            this.d = num;
            this.e = lc6;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public Device a;
        @DexIgnore
        public String b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Boolean e;

        @DexIgnore
        public c(Device device, String str, boolean z, boolean z2, Boolean bool) {
            wg6.b(device, "deviceModel");
            wg6.b(str, "deviceName");
            this.a = device;
            this.b = str;
            this.c = z;
            this.d = z2;
            this.e = bool;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public final boolean e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return wg6.a((Object) this.a, (Object) cVar.a) && wg6.a((Object) this.b, (Object) cVar.b) && this.c == cVar.c && this.d == cVar.d && wg6.a((Object) this.e, (Object) cVar.e);
        }

        @DexIgnore
        public int hashCode() {
            Device device = this.a;
            int i = 0;
            int hashCode = (device != null ? device.hashCode() : 0) * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            int i2 = (hashCode2 + (z ? 1 : 0)) * 31;
            boolean z2 = this.d;
            if (z2) {
                z2 = true;
            }
            int i3 = (i2 + (z2 ? 1 : 0)) * 31;
            Boolean bool = this.e;
            if (bool != null) {
                i = bool.hashCode();
            }
            return i3 + i;
        }

        @DexIgnore
        public String toString() {
            return "WatchSettingWrapper(deviceModel=" + this.a + ", deviceName=" + this.b + ", isConnected=" + this.c + ", isActive=" + this.d + ", shouldShowVibrationUI=" + this.e + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Device device, String str, boolean z, boolean z2, Boolean bool, int i, qg6 qg6) {
            this(device, str, z, z2, (i & 16) != 0 ? null : bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ds4.d, ds4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(bv5 bv5, String str) {
            this.a = bv5;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UnlinkDeviceUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "removeDevice success serial=" + this.a.h + ".value");
            l24.a(this.a, false, true, 1, (Object) null);
            b.a(this.a.k, true, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8190, (Object) null);
            this.a.f();
        }

        @DexIgnore
        public void a(UnlinkDeviceUseCase.c cVar) {
            Integer num;
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "remove device " + this.a.h + ".value fail due to " + cVar.b());
            l24.a(this.a, false, true, 1, (Object) null);
            String b2 = cVar.b();
            switch (b2.hashCode()) {
                case -1767173543:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_STOP_WORKOUT_FAIL")) {
                        return;
                    }
                    break;
                case -1697024179:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_LACK_PERMISSION")) {
                        if (cVar.c() != null) {
                            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                            bv5 bv5 = this.a;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                            if (array != null) {
                                uh4[] uh4Arr = (uh4[]) array;
                                bv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                            } else {
                                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                        this.a.f();
                        return;
                    }
                    return;
                case -643272338:
                    if (b2.equals("UNLINK_FAIL_ON_SERVER")) {
                        if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                            num = 600;
                        } else {
                            num = cVar.c().get(0);
                        }
                        wg6.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                        int intValue = num.intValue();
                        b h = this.a.k;
                        Integer valueOf = Integer.valueOf(intValue);
                        String a3 = cVar.a();
                        if (a3 == null) {
                            a3 = "";
                        }
                        b.a(h, false, (c) null, (String) null, (Integer) null, new lc6(valueOf, a3), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8175, (Object) null);
                        this.a.f();
                        return;
                    }
                    return;
                case 1447890910:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_SYNC_FAIL")) {
                        return;
                    }
                    break;
                case 1768665169:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_PENDING_WORKOUT")) {
                        b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, this.b, (String) null, false, false, 7679, (Object) null);
                        this.a.f();
                        return;
                    }
                    return;
                default:
                    return;
            }
            b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, this.b, false, false, 7167, (Object) null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<yr4.d, yr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public e(bv5 bv5, String str) {
            this.a = bv5;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SwitchActiveDeviceUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "removeDevice(), switch to newDevice=" + this.b + " success");
            l24.a(this.a, false, true, 1, (Object) null);
            this.a.i(this.b);
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(SwitchActiveDeviceUseCase.c cVar) {
            Integer num;
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "removeDevice switch to " + this.b + " fail due to " + cVar.b());
            l24.a(this.a, false, true, 1, (Object) null);
            int b2 = cVar.b();
            if (b2 == 113) {
                if (cVar.c() != null) {
                    List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                    wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    bv5 bv5 = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                    if (array != null) {
                        uh4[] uh4Arr = (uh4[]) array;
                        bv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                this.a.f();
            } else if (b2 == 114) {
                if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                    num = 600;
                } else {
                    num = cVar.c().get(0);
                }
                wg6.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                int intValue = num.intValue();
                b h = this.a.k;
                Integer valueOf = Integer.valueOf(intValue);
                String a3 = cVar.a();
                if (a3 == null) {
                    a3 = "";
                }
                b.a(h, false, (c) null, (String) null, (Integer) null, new lc6(valueOf, a3), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8175, (Object) null);
                this.a.f();
            } else if (b2 == 117) {
                b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, this.b, (String) null, (String) null, false, false, 7935, (Object) null);
                this.a.f();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;

        @DexIgnore
        public f(bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Device device;
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "onConnectionStateChangeReceiver, serial=" + stringExtra);
            if (wg6.a((Object) stringExtra, (Object) (String) this.a.h.a()) && xj6.b(stringExtra, this.a.w.e(), true)) {
                FLogger.INSTANCE.getLocal().d(bv5.y.a(), "onConnectionStateChanged");
                LiveData c = this.a.i;
                if (c != null && (device = (Device) c.a()) != null) {
                    this.a.a(device);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;

        @DexIgnore
        public g(bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<Device> apply(String str) {
            DeviceRepository d = this.a.p;
            wg6.a((Object) str, "serial");
            return d.getDeviceBySerialAsLiveData(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;

        @DexIgnore
        public h(bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        public final void run() {
            this.a.p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ld<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;

        @DexIgnore
        public i(bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Device device) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "on device changed " + device);
            this.a.a(device);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Device $this_run;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements ig6<il6, xe6<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = jVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    return this.this$0.this$0.p.getDeviceNameBySerial(this.this$0.$this_run.getDeviceId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(Device device, xe6 xe6, bv5 bv5, Device device2) {
            super(2, xe6);
            this.$this_run = device;
            this.this$0 = bv5;
            this.$device$inlined = device2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            j jVar = new j(this.$this_run, xe6, this.this$0, this.$device$inlined);
            jVar.p$ = (il6) obj;
            return jVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((j) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            String str;
            Object obj2;
            boolean z;
            boolean z2;
            Object a2 = ff6.a();
            int i = this.label;
            boolean z3 = false;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = bv5.y.a();
                local.d(a3, "onDeviceChanged - device: " + this.$device$inlined);
                str = PortfolioApp.get.instance().e();
                boolean z4 = !FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
                if (this.$this_run.getVibrationStrength() == null) {
                    this.$this_run.setVibrationStrength(hf6.a(50));
                    cd6 cd6 = cd6.a;
                }
                dl6 b = zl6.b();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.Z$0 = z4;
                this.label = 1;
                obj2 = gk6.a(b, aVar, this);
                if (obj2 == a2) {
                    return a2;
                }
                z = z4;
            } else if (i == 1) {
                z = this.Z$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                str = (String) this.L$1;
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str2 = (String) obj2;
            if (this.$this_run.getBatteryLevel() > 100) {
                this.$this_run.setBatteryLevel(100);
            }
            if (wg6.a((Object) this.$this_run.getDeviceId(), (Object) str)) {
                try {
                    IButtonConnectivity b2 = PortfolioApp.get.b();
                    if (b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2) {
                        z3 = true;
                    }
                    z2 = z3;
                } catch (Exception unused) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a4 = bv5.y.a();
                    local2.e(a4, "exception when get gatt state of " + this.$this_run);
                    z2 = false;
                }
                this.this$0.j = new c(this.$device$inlined, str2, z2, true, hf6.a(z));
            } else {
                this.this$0.j = new c(this.$device$inlined, str2, false, false, hf6.a(z));
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a5 = bv5.y.a();
            local3.d(a5, "onDeviceChanged, mDeviceWrapper=" + this.this$0.j);
            b h = this.this$0.k;
            c e = this.this$0.j;
            if (e != null) {
                b.a(h, false, e, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8189, (Object) null);
                this.this$0.f();
                this.this$0.g.removeCallbacksAndMessages((Object) null);
                this.this$0.p();
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements m24.e<yr4.d, yr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public k(bv5 bv5, String str) {
            this.a = bv5;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SwitchActiveDeviceUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            l24.a(this.a, false, true, 1, (Object) null);
            this.a.i(this.b);
            this.a.a(dVar.a());
            this.a.l();
        }

        @DexIgnore
        public void a(SwitchActiveDeviceUseCase.c cVar) {
            Integer num;
            wg6.b(cVar, "errorValue");
            if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                num = 600;
            } else {
                num = cVar.c().get(0);
            }
            wg6.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
            int intValue = num.intValue();
            l24.a(this.a, false, true, 1, (Object) null);
            b h = this.a.k;
            Integer valueOf = Integer.valueOf(intValue);
            String a2 = cVar.a();
            if (a2 == null) {
                a2 = "";
            }
            b.a(h, false, (c) null, (String) null, (Integer) null, new lc6(valueOf, a2), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8175, (Object) null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements m24.e<rr4.e, rr4.d> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;

        @DexIgnore
        public l(bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rr4.e eVar) {
            wg6.b(eVar, "responseValue");
            l24.a(this.a, false, true, 1, (Object) null);
        }

        @DexIgnore
        public void a(rr4.d dVar) {
            wg6.b(dVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "reconnectActiveDevice fail!! errorValue=" + dVar.a());
            l24.a(this.a, false, true, 1, (Object) null);
            int c = dVar.c();
            if (c == 1101 || c == 1112 || c == 1113) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(dVar.b());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                bv5 bv5 = this.a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    bv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    this.a.f();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8159, (Object) null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements m24.e<cs4, as4> {
        @DexIgnore
        public /* final */ /* synthetic */ bv5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public m(bv5 bv5, String str, int i) {
            this.a = bv5;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(cs4 cs4) {
            wg6.b(cs4, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            int i = this.c;
            if (i == 0) {
                bv5 bv5 = this.a;
                Object a3 = bv5.h.a();
                if (a3 != null) {
                    wg6.a(a3, "mSerialLiveData.value!!");
                    bv5.a((String) a3, 1);
                    return;
                }
                wg6.a();
                throw null;
            } else if (i == 1) {
                bv5 bv52 = this.a;
                Object a4 = bv52.h.a();
                if (a4 != null) {
                    bv52.a((String) a4);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(as4 as4) {
            wg6.b(as4, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + as4.a());
            int i = cv5.a[as4.a().ordinal()];
            if (i == 1) {
                FLogger.INSTANCE.getLocal().d(bv5.y.a(), "Sync of this device is in progress, keep waiting for result");
            } else if (i == 2) {
                if (as4.b() != null) {
                    List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(new ArrayList(as4.b()));
                    wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    bv5 bv5 = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                    if (array != null) {
                        uh4[] uh4Arr = (uh4[]) array;
                        bv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                        this.a.f();
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                l24.a(this.a, false, true, 1, (Object) null);
            } else if (i == 3) {
                int i2 = this.c;
                if (i2 == 0) {
                    b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, this.b, (String) null, (String) null, (String) null, (String) null, false, false, 8127, (Object) null);
                    this.a.f();
                } else if (i2 == 1) {
                    b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, this.b, (String) null, false, false, 7679, (Object) null);
                    this.a.f();
                }
                l24.a(this.a, false, true, 1, (Object) null);
            } else if (i == 4) {
                int i3 = this.c;
                if (i3 == 0) {
                    b h = this.a.k;
                    Object a3 = this.a.h.a();
                    if (a3 != null) {
                        b.a(h, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) a3, (String) null, (String) null, (String) null, false, false, 8063, (Object) null);
                        this.a.f();
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i3 == 1) {
                    b h2 = this.a.k;
                    Object a4 = this.a.h.a();
                    if (a4 != null) {
                        b.a(h2, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) a4, false, false, 7167, (Object) null);
                        this.a.f();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                l24.a(this.a, false, true, 1, (Object) null);
            } else if (i != 5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a5 = bv5.y.a();
                local2.d(a5, "errorValue.lastErrorCode=" + as4.a());
                b.a(this.a.k, false, (c) null, (String) null, (Integer) null, (lc6) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8159, (Object) null);
                this.a.f();
                l24.a(this.a, false, true, 1, (Object) null);
            } else {
                FLogger.INSTANCE.getLocal().d(bv5.y.a(), "User deny stopping workout");
                String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.USER_CANCELLED);
                int i4 = this.c;
                if (i4 == 0) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.SWITCH_DEVICE;
                    Object a6 = this.a.h.a();
                    if (a6 != null) {
                        wg6.a(a6, "mSerialLiveData.value!!");
                        remote.e(component, session, (String) a6, bv5.y.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i4 == 1) {
                    FLogger.INSTANCE.getRemote().e(FLogger.Component.APP, FLogger.Session.REMOVE_DEVICE, this.b, bv5.y.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                }
                l24.a(this.a, false, true, 1, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements m24.e<wr4.d, wr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ Device a;
        @DexIgnore
        public /* final */ /* synthetic */ bv5 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public n(Device device, String str, bv5 bv5, int i) {
            this.a = device;
            this.b = bv5;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetVibrationStrengthUseCase.d dVar) {
            Device a2;
            wg6.b(dVar, "responseValue");
            Integer vibrationStrength = this.a.getVibrationStrength();
            l24.a(this.b, false, true, 1, (Object) null);
            b.a(this.b.k, false, (c) null, (String) null, Integer.valueOf(this.c), (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8183, (Object) null);
            this.b.f();
            c e = this.b.j;
            if (!(e == null || (a2 = e.a()) == null)) {
                a2.setVibrationStrength(vibrationStrength);
            }
            FLogger.INSTANCE.getLocal().d(bv5.y.a(), "updateVibrationLevel success");
        }

        @DexIgnore
        public void a(SetVibrationStrengthUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bv5.y.a();
            local.d(a2, "updateVibrationLevel fail!! errorValue=" + cVar.a());
            l24.a(this.b, false, true, 1, (Object) null);
            int c2 = cVar.c();
            if (c2 != 1101) {
                if (c2 == 8888) {
                    b.a(this.b.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, true, false, 6143, (Object) null);
                    this.b.f();
                    return;
                } else if (!(c2 == 1112 || c2 == 1113)) {
                    b.a(this.b.k, false, (c) null, (String) null, (Integer) null, (lc6) null, true, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8159, (Object) null);
                    this.b.f();
                    return;
                }
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(cVar.b());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            bv5 bv5 = this.b;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                bv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                this.b.f();
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = bv5.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchSettingViewModel::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public bv5(DeviceRepository deviceRepository, SetVibrationStrengthUseCase setVibrationStrengthUseCase, UnlinkDeviceUseCase unlinkDeviceUseCase, cj4 cj4, an4 an4, SwitchActiveDeviceUseCase switchActiveDeviceUseCase, rr4 rr4, PortfolioApp portfolioApp) {
        DeviceRepository deviceRepository2 = deviceRepository;
        SetVibrationStrengthUseCase setVibrationStrengthUseCase2 = setVibrationStrengthUseCase;
        UnlinkDeviceUseCase unlinkDeviceUseCase2 = unlinkDeviceUseCase;
        cj4 cj42 = cj4;
        an4 an42 = an4;
        SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = switchActiveDeviceUseCase;
        rr4 rr42 = rr4;
        PortfolioApp portfolioApp2 = portfolioApp;
        wg6.b(deviceRepository2, "mDeviceRepository");
        wg6.b(setVibrationStrengthUseCase2, "mSetVibrationStrengthUseCase");
        wg6.b(unlinkDeviceUseCase2, "mUnlinkDeviceUseCase");
        wg6.b(cj42, "mDeviceSettingFactory");
        wg6.b(an42, "mSharedPreferencesManager");
        wg6.b(switchActiveDeviceUseCase2, "mSwitchActiveDeviceUseCase");
        wg6.b(rr42, "mReconnectDeviceUseCase");
        wg6.b(portfolioApp2, "mApp");
        this.p = deviceRepository2;
        this.q = setVibrationStrengthUseCase2;
        this.r = unlinkDeviceUseCase2;
        this.s = cj42;
        this.t = an42;
        this.u = switchActiveDeviceUseCase2;
        this.v = rr42;
        this.w = portfolioApp2;
        LiveData<Device> b2 = sd.b(this.h, new g(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026lAsLiveData(serial)\n    }");
        this.i = b2;
        this.k = new b(false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8191, (qg6) null);
        this.m = new h(this);
        this.n = new i(this);
        this.o = new f(this);
    }

    @DexIgnore
    public final MutableLiveData<b> j() {
        return this.f;
    }

    @DexIgnore
    public final String k() {
        return (String) this.h.a();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.rr4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.bv5$l] */
    public final void l() {
        FLogger.INSTANCE.getLocal().d(x, "reconnectActiveDevice");
        l24.a(this, true, false, 2, (Object) null);
        Object r1 = this.v;
        Object a2 = this.h.a();
        if (a2 != null) {
            wg6.a(a2, "mSerialLiveData.value!!");
            r1.a(new rr4.c((String) a2), new l(this));
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "removeDevice serial=" + this.h + ".value");
        l24.a(this, true, false, 2, (Object) null);
        String e2 = this.w.e();
        if (!wg6.a((Object) (String) this.h.a(), (Object) e2) || xj6.a(e2)) {
            a((String) this.h.a());
        } else {
            b(e2, 1);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void n() {
        b.a(this.k, false, (c) null, (String) null, (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8191, (Object) null);
        this.q.g();
        this.v.e();
        this.u.h();
        BleCommandResultManager.d.a(CommunicateMode.FORCE_CONNECT, CommunicateMode.SET_VIBRATION_STRENGTH, CommunicateMode.SWITCH_DEVICE);
        Object r1 = this.w;
        f fVar = this.o;
        r1.registerReceiver(fVar, new IntentFilter(this.w.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<Device> liveData = this.i;
        if (liveData != null) {
            liveData.a(this.n);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void o() {
        try {
            this.u.i();
            LiveData<Device> liveData = this.i;
            if (liveData != null) {
                liveData.b(this.n);
            }
            this.v.f();
            this.q.h();
            this.w.unregisterReceiver(this.o);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.e(str, "stop with " + e2);
        }
        this.g.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v17, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void p() {
        CharSequence charSequence;
        String str = (String) this.h.a();
        if (str != null) {
            if (TextUtils.equals(PortfolioApp.get.instance().e(), str)) {
                PortfolioApp portfolioApp = this.w;
                wg6.a((Object) str, "it");
                if (portfolioApp.i(str)) {
                    charSequence = this.w.getString(2131886551);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = x;
                    local.d(str2, "updateSyncTime " + charSequence);
                    b.a(this.k, false, (c) null, charSequence.toString(), (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8187, (Object) null);
                    this.l = charSequence.toString();
                    f();
                    this.g.postDelayed(this.m, 60000);
                }
            }
            long g2 = this.t.g(str);
            if (((int) g2) == 0) {
                charSequence = "";
            } else if (System.currentTimeMillis() - g2 < 60000) {
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886942);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                Object[] objArr = {1};
                charSequence = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) charSequence, "java.lang.String.format(format, *args)");
            } else {
                charSequence = DateUtils.getRelativeTimeSpanString(g2, System.currentTimeMillis(), TimeUnit.SECONDS.toMillis(1));
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str22 = x;
            local2.d(str22, "updateSyncTime " + charSequence);
            b.a(this.k, false, (c) null, charSequence.toString(), (Integer) null, (lc6) null, false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8187, (Object) null);
            this.l = charSequence.toString();
            f();
            this.g.postDelayed(this.m, 60000);
        }
    }

    @DexIgnore
    public final boolean q() {
        String str = (String) this.h.a();
        if (str == null) {
            return true;
        }
        if (FossilDeviceSerialPatternUtil.isSamSlimDevice(str) || FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        a(str);
    }

    @DexIgnore
    public final void c(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        this.w.a(str, true);
    }

    @DexIgnore
    public final void d(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        this.w.a(str, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void e() {
        FLogger.INSTANCE.getLocal().d(x, "checkToReconnectOrSwitchActiveDevice");
        String e2 = this.w.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "activeSerial=" + e2 + ", mSerialLiveData=" + ((String) this.h.a()));
        if (wg6.a((Object) (String) this.h.a(), (Object) e2)) {
            l();
        } else if (!hx5.b(PortfolioApp.get.instance())) {
            b.a(this.k, false, (c) null, (String) null, (Integer) null, new lc6(601, ""), false, (String) null, (String) null, (String) null, (String) null, (String) null, false, false, 8175, (Object) null);
            f();
        } else {
            l24.a(this, true, false, 2, (Object) null);
            if (!xj6.a(e2)) {
                b(e2, 0);
                return;
            }
            Object a2 = this.h.a();
            if (a2 != null) {
                wg6.a(a2, "mSerialLiveData.value!!");
                a((String) a2, 1);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void f(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        a(str, 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r0.b();
     */
    @DexIgnore
    public final String g() {
        String b2;
        c cVar = this.j;
        return (cVar == null || b2 == null) ? "" : b2;
    }

    @DexIgnore
    public final c h() {
        return this.j;
    }

    @DexIgnore
    public final String i() {
        return this.l;
    }

    @DexIgnore
    public final void g(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        this.w.a(str, false);
    }

    @DexIgnore
    public final void h(String str) {
        wg6.b(str, "serial");
        l24.a(this, true, false, 2, (Object) null);
        this.w.a(str, false);
    }

    @DexIgnore
    public final void i(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "setWatchSerial, serial=" + str);
        this.h.b(str);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.bv5$m] */
    public final void b(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "syncDevice - serial=" + str + ", userAction=" + i2);
        this.s.b(str).a(new bs4(FossilDeviceSerialPatternUtil.getDeviceBySerial(str) != FossilDeviceSerialPatternUtil.DEVICE.DIANA ? 10 : 15, str, false), new m(this, str, i2));
    }

    @DexIgnore
    public final void f() {
        this.k.a(h());
        this.k.a(i());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, ".emitUIState(), mUiModelWrapper=" + this.k);
        this.f.a(this.k);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.fossil.bv5$d, com.portfolio.platform.CoroutineUseCase$e] */
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "removeDevice " + str);
        this.r.a(str != null ? new UnlinkDeviceUseCase.b(str) : null, new d(this, str));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.bv5$n] */
    public final void a(int i2) {
        c cVar;
        Device a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateVibrationLevel " + i2 + " of " + ((String) this.h.a()));
        String str2 = (String) this.h.a();
        if (str2 != null && (cVar = this.j) != null && (a2 = cVar.a()) != null) {
            Integer vibrationStrength = a2.getVibrationStrength();
            if ((vibrationStrength == null || vibrationStrength.intValue() != i2) && !FossilDeviceSerialPatternUtil.isSamSlimDevice(str2) && !FossilDeviceSerialPatternUtil.isDianaDevice(str2)) {
                l24.a(this, true, false, 2, (Object) null);
                Object r2 = this.q;
                wg6.a((Object) str2, "it");
                r2.a(new SetVibrationStrengthUseCase.b(str2, i2), new n(a2, str2, this, i2));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.bv5$k] */
    public final void e(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "force switch to device " + str);
        l24.a(this, true, false, 2, (Object) null);
        this.u.a(new SwitchActiveDeviceUseCase.b(str, 4), new k(this, str));
    }

    @DexIgnore
    public final void a(Device device) {
        if (device != null) {
            rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new j(device, (xe6) null, this, device), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r6v1, types: [com.fossil.bv5$e, com.portfolio.platform.CoroutineUseCase$e] */
    public final void a(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "switch to device " + str + " mode " + i2);
        this.u.a(new SwitchActiveDeviceUseCase.b(str, i2), new e(this, str));
    }
}
