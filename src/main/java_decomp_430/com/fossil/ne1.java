package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne1 {
    @DexIgnore
    public static /* final */ ne1 a; // = new ne1();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        boolean z = true;
        for (String a2 : strArr) {
            z = z && w6.a(context, a2) == 0;
        }
        return z;
    }
}
