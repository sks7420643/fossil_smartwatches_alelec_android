package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j61 extends l61 {
    @DexIgnore
    public j61(lx0 lx0, ue1 ue1) {
        super(lx0, ue1);
    }

    @DexIgnore
    public final void b(ok0 ok0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject a;
        this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
        if (ok0.d.c.a == x11.START_FAIL) {
            nn0 nn0 = this.f;
            if (nn0 != null) {
                nn0.i = false;
            }
            nn0 nn02 = this.f;
            if (!(nn02 == null || (jSONObject2 = nn02.m) == null || (a = cw0.a(jSONObject2, bm0.MESSAGE, (Object) cw0.a((Enum<?>) ao0.START_FAIL))) == null)) {
                cw0.a(a, bm0.ERROR_DETAIL, (Object) ok0.d.a());
            }
        } else {
            nn0 nn03 = this.f;
            if (nn03 != null) {
                nn03.i = true;
            }
            nn0 nn04 = this.f;
            if (!(nn04 == null || (jSONObject = nn04.m) == null)) {
                cw0.a(jSONObject, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
            }
        }
        k();
        a(this.v);
    }

    @DexIgnore
    public final String c(bn0 bn0) {
        il0 il0 = bn0.c;
        il0 il02 = il0.EMPTY_SERVICES;
        if (il0 == il02) {
            return cw0.a((Enum<?>) il02);
        }
        t31 t31 = bn0.d.c;
        x11 x11 = t31.a;
        x11 x112 = x11.START_FAIL;
        if (x11 == x112) {
            return cw0.a((Enum<?>) x112);
        }
        if (il0 == il0.REQUEST_TIMEOUT || il0 == il0.RESPONSE_TIMEOUT) {
            return cw0.a((Enum<?>) il0.RESPONSE_TIMEOUT);
        }
        return cw0.a((Enum<?>) ao0.m.a(t31.b));
    }

    @DexIgnore
    public final String b(bn0 bn0) {
        x11 x11 = bn0.d.c.a;
        x11 x112 = x11.START_FAIL;
        if (x11 == x112) {
            return cw0.a((Enum<?>) x112);
        }
        return cw0.a((Enum<?>) il0.SUCCESS);
    }
}
