package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum f04 {
    L(1),
    M(0),
    Q(3),
    H(2);
    
    @DexIgnore
    public static /* final */ f04[] a; // = null;
    @DexIgnore
    public /* final */ int bits;

    /*
    static {
        f04 f04;
        f04 f042;
        f04 f043;
        f04 f044;
        a = new f04[]{f042, f04, f044, f043};
    }
    */

    @DexIgnore
    public f04(int i) {
        this.bits = i;
    }

    @DexIgnore
    public static f04 forBits(int i) {
        if (i >= 0) {
            f04[] f04Arr = a;
            if (i < f04Arr.length) {
                return f04Arr[i];
            }
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int getBits() {
        return this.bits;
    }
}
