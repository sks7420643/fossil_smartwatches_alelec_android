package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o43 extends dk2 implements l43 {
    @DexIgnore
    public o43() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((j03) li2.a(parcel, j03.CREATOR), (ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                a((la3) li2.a(parcel, la3.CREATOR), (ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 4:
                d((ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                a((j03) li2.a(parcel, j03.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                b((ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<la3> a = a((ra3) li2.a(parcel, ra3.CREATOR), li2.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                return true;
            case 9:
                byte[] a2 = a((j03) li2.a(parcel, j03.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                return true;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String c = c((ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(c);
                return true;
            case 12:
                a((ab3) li2.a(parcel, ab3.CREATOR), (ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                a((ab3) li2.a(parcel, ab3.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<la3> a3 = a(parcel.readString(), parcel.readString(), li2.a(parcel), (ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a3);
                return true;
            case 15:
                List<la3> a4 = a(parcel.readString(), parcel.readString(), parcel.readString(), li2.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 16:
                List<ab3> a5 = a(parcel.readString(), parcel.readString(), (ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 17:
                List<ab3> a6 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 18:
                a((ra3) li2.a(parcel, ra3.CREATOR));
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
