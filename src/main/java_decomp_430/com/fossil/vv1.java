package com.fossil;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.e12;
import com.fossil.qw1;
import com.fossil.rv1;
import com.fossil.rv1.d;
import com.fossil.uw1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vv1<O extends rv1.d> implements xv1<O> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ rv1<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ lw1<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ wv1 g;
    @DexIgnore
    public /* final */ zw1 h;
    @DexIgnore
    public /* final */ qw1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a c; // = new C0051a().a();
        @DexIgnore
        public /* final */ zw1 a;
        @DexIgnore
        public /* final */ Looper b;

        @DexIgnore
        public a(zw1 zw1, Account account, Looper looper) {
            this.a = zw1;
            this.b = looper;
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vv1$a$a")
        /* renamed from: com.fossil.vv1$a$a  reason: collision with other inner class name */
        public static class C0051a {
            @DexIgnore
            public zw1 a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public C0051a a(zw1 zw1) {
                w12.a(zw1, (Object) "StatusExceptionMapper must not be null.");
                this.a = zw1;
                return this;
            }

            @DexIgnore
            public C0051a a(Looper looper) {
                w12.a(looper, (Object) "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public a a() {
                if (this.a == null) {
                    this.a = new kw1();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.a, this.b);
            }
        }
    }

    @DexIgnore
    public vv1(Context context, rv1<O> rv1, Looper looper) {
        w12.a(context, (Object) "Null context is not permitted.");
        w12.a(rv1, (Object) "Api must not be null.");
        w12.a(looper, (Object) "Looper must not be null.");
        this.a = context.getApplicationContext();
        this.b = rv1;
        this.c = null;
        this.e = looper;
        this.d = lw1.a(rv1);
        this.g = new sy1(this);
        this.i = qw1.a(this.a);
        this.f = this.i.b();
        this.h = new kw1();
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(int i2, T t) {
        t.g();
        this.i.a(this, i2, (nw1<? extends ew1, rv1.b>) t);
        return t;
    }

    @DexIgnore
    public <A extends rv1.b, T extends nw1<? extends ew1, A>> T b(T t) {
        a(1, t);
        return t;
    }

    @DexIgnore
    public e12.a c() {
        Account account;
        Set<Scope> set;
        GoogleSignInAccount a2;
        GoogleSignInAccount a3;
        e12.a aVar = new e12.a();
        O o = this.c;
        if (!(o instanceof rv1.d.b) || (a3 = ((rv1.d.b) o).a()) == null) {
            O o2 = this.c;
            account = o2 instanceof rv1.d.a ? ((rv1.d.a) o2).g() : null;
        } else {
            account = a3.p();
        }
        aVar.a(account);
        O o3 = this.c;
        if (!(o3 instanceof rv1.d.b) || (a2 = ((rv1.d.b) o3).a()) == null) {
            set = Collections.emptySet();
        } else {
            set = a2.J();
        }
        aVar.a((Collection<Scope>) set);
        aVar.a(this.a.getClass().getName());
        aVar.b(this.a.getPackageName());
        return aVar;
    }

    @DexIgnore
    public final rv1<O> d() {
        return this.b;
    }

    @DexIgnore
    public O e() {
        return this.c;
    }

    @DexIgnore
    public Context f() {
        return this.a;
    }

    @DexIgnore
    public final int g() {
        return this.f;
    }

    @DexIgnore
    public Looper h() {
        return this.e;
    }

    @DexIgnore
    public wv1 b() {
        return this.g;
    }

    @DexIgnore
    public final <TResult, A extends rv1.b> qc3<TResult> a(int i2, bx1<A, TResult> bx1) {
        rc3 rc3 = new rc3();
        this.i.a(this, i2, bx1, rc3, this.h);
        return rc3.a();
    }

    @DexIgnore
    public <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t) {
        a(0, t);
        return t;
    }

    @DexIgnore
    public <TResult, A extends rv1.b> qc3<TResult> a(bx1<A, TResult> bx1) {
        return a(0, bx1);
    }

    @DexIgnore
    @Deprecated
    public <A extends rv1.b, T extends xw1<A, ?>, U extends dx1<A, ?>> qc3<Void> a(T t, U u) {
        w12.a(t);
        w12.a(u);
        w12.a(t.b(), (Object) "Listener has already been released.");
        w12.a(u.a(), (Object) "Listener has already been released.");
        w12.a(t.b().equals(u.a()), (Object) "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.a(this, (xw1<rv1.b, ?>) t, (dx1<rv1.b, ?>) u);
    }

    @DexIgnore
    public vv1(Activity activity, rv1<O> rv1, O o, a aVar) {
        w12.a(activity, (Object) "Null activity is not permitted.");
        w12.a(rv1, (Object) "Api must not be null.");
        w12.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = activity.getApplicationContext();
        this.b = rv1;
        this.c = o;
        this.e = aVar.b;
        this.d = lw1.a(this.b, this.c);
        this.g = new sy1(this);
        this.i = qw1.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        if (!(activity instanceof GoogleApiActivity)) {
            hx1.a(activity, this.i, this.d);
        }
        this.i.a((vv1<?>) this);
    }

    @DexIgnore
    public qc3<Boolean> a(uw1.a<?> aVar) {
        w12.a(aVar, (Object) "Listener key cannot be null.");
        return this.i.a(this, aVar);
    }

    @DexIgnore
    public rv1.f a(Looper looper, qw1.a<O> aVar) {
        return this.b.d().a(this.a, looper, c().a(), this.c, aVar, aVar);
    }

    @DexIgnore
    public lw1<O> a() {
        return this.d;
    }

    @DexIgnore
    public dz1 a(Context context, Handler handler) {
        return new dz1(context, handler, c().a());
    }

    @DexIgnore
    public vv1(Context context, rv1<O> rv1, O o, a aVar) {
        w12.a(context, (Object) "Null context is not permitted.");
        w12.a(rv1, (Object) "Api must not be null.");
        w12.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = context.getApplicationContext();
        this.b = rv1;
        this.c = o;
        this.e = aVar.b;
        this.d = lw1.a(this.b, this.c);
        this.g = new sy1(this);
        this.i = qw1.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        this.i.a((vv1<?>) this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public vv1(Activity activity, rv1<O> rv1, O o, zw1 zw1) {
        this(activity, rv1, o, r0.a());
        a.C0051a aVar = new a.C0051a();
        aVar.a(zw1);
        aVar.a(activity.getMainLooper());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public vv1(Context context, rv1<O> rv1, O o, zw1 zw1) {
        this(context, rv1, o, r0.a());
        a.C0051a aVar = new a.C0051a();
        aVar.a(zw1);
    }
}
