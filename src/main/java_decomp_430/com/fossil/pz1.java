package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.qw1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pz1<T> extends ey1 {
    @DexIgnore
    public /* final */ rc3<T> a;

    @DexIgnore
    public pz1(int i, rc3<T> rc3) {
        super(i);
        this.a = rc3;
    }

    @DexIgnore
    public void a(Status status) {
        this.a.b((Exception) new sv1(status));
    }

    @DexIgnore
    public abstract void d(qw1.a<?> aVar) throws RemoteException;

    @DexIgnore
    public void a(RuntimeException runtimeException) {
        this.a.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(qw1.a<?> aVar) throws DeadObjectException {
        try {
            d(aVar);
        } catch (DeadObjectException e) {
            a(az1.a((RemoteException) e));
            throw e;
        } catch (RemoteException e2) {
            a(az1.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
