package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c22 {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ String b; // = this.a.getResourcePackageName(pv1.common_google_play_services_unknown_issue);

    @DexIgnore
    public c22(Context context) {
        w12.a(context);
        this.a = context.getResources();
    }

    @DexIgnore
    public String a(String str) {
        int identifier = this.a.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
