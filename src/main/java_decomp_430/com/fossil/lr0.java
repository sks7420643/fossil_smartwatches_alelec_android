package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ tp0 CREATOR; // = new tp0((qg6) null);
    @DexIgnore
    public /* final */ bo0 a;
    @DexIgnore
    public /* final */ short b;

    @DexIgnore
    public lr0(bo0 bo0, short s) {
        this.a = bo0;
        this.b = s;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.t0, (Object) cw0.a((Enum<?>) this.a)), bm0.FILE_HANDLE, (Object) cw0.a(this.b));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(lr0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            lr0 lr0 = (lr0) obj;
            return this.a == lr0.a && this.b == lr0.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame");
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
