package com.fossil;

import com.facebook.stetho.inspector.protocol.module.Database;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum m40 implements bc0 {
    BLUETOOTH_OFF(0, 255),
    LOCATION_PERMISSION_NOT_GRANTED(247, 255),
    LOCATION_SERVICE_NOT_ENABLED(248, 255),
    ALREADY_STARTED(249, 1),
    REGISTRATION_FAILED(Database.MAX_EXECUTE_RESULTS, 2),
    SYSTEM_INTERNAL_ERROR(251, 3),
    UNSUPPORTED(252, 4),
    OUT_OF_HARDWARE_RESOURCE(253, 5),
    SCANNING_TOO_FREQUENTLY(254, 6),
    UNKNOWN_ERROR(255, 255);
    
    @DexIgnore
    public static /* final */ a e; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final m40 a(int i) {
            m40 m40;
            m40[] values = m40.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    m40 = null;
                    break;
                }
                m40 = values[i2];
                if (m40.c == i) {
                    break;
                }
                i2++;
            }
            return m40 != null ? m40 : m40.UNKNOWN_ERROR;
        }
    }

    /*
    static {
        e = new a((qg6) null);
    }
    */

    @DexIgnore
    public m40(int i, int i2) {
        this.b = i;
        this.c = i2;
        this.a = cw0.a((Enum<?>) this);
    }

    @DexIgnore
    public int getCode() {
        return this.b;
    }

    @DexIgnore
    public String getLogName() {
        return this.a;
    }
}
