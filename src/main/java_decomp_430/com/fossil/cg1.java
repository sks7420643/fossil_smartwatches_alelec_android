package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg1 implements Parcelable.Creator<yh1> {
    @DexIgnore
    public /* synthetic */ cg1(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        Object[] createTypedArray = parcel.createTypedArray(nd0.CREATOR);
        if (createTypedArray != null) {
            wg6.a(createTypedArray, "parcel.createTypedArray(MicroAppMapping.CREATOR)!!");
            nd0[] nd0Arr = (nd0[]) createTypedArray;
            Parcelable readParcelable = parcel.readParcelable(w40.class.getClassLoader());
            if (readParcelable != null) {
                return new yh1(nd0Arr, (w40) readParcelable);
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new yh1[i];
    }
}
