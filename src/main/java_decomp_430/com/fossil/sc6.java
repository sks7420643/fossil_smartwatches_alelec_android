package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc6 implements Comparable<sc6> {
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public /* synthetic */ sc6(byte b) {
        this.a = b;
    }

    @DexIgnore
    public static boolean a(byte b, Object obj) {
        if (obj instanceof sc6) {
            if (b == ((sc6) obj).a()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static final /* synthetic */ sc6 b(byte b) {
        return new sc6(b);
    }

    @DexIgnore
    public static byte c(byte b) {
        return b;
    }

    @DexIgnore
    public static int d(byte b) {
        return b;
    }

    @DexIgnore
    public static String e(byte b) {
        return String.valueOf(b & 255);
    }

    @DexIgnore
    public final /* synthetic */ byte a() {
        return this.a;
    }

    @DexIgnore
    public final int a(byte b) {
        return a(this.a, b);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return a(((sc6) obj).a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        byte b = this.a;
        d(b);
        return b;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public static int a(byte b, byte b2) {
        return wg6.a((int) b & 255, (int) b2 & 255);
    }
}
