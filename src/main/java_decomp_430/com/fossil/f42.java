package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f42 implements Parcelable.Creator<WakeLockEvent> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        ArrayList<String> arrayList = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel2, a);
                    break;
                case 2:
                    j = f22.s(parcel2, a);
                    break;
                case 4:
                    str = f22.e(parcel2, a);
                    break;
                case 5:
                    i3 = f22.q(parcel2, a);
                    break;
                case 6:
                    arrayList = f22.g(parcel2, a);
                    break;
                case 8:
                    j2 = f22.s(parcel2, a);
                    break;
                case 10:
                    str3 = f22.e(parcel2, a);
                    break;
                case 11:
                    i2 = f22.q(parcel2, a);
                    break;
                case 12:
                    str2 = f22.e(parcel2, a);
                    break;
                case 13:
                    str4 = f22.e(parcel2, a);
                    break;
                case 14:
                    i4 = f22.q(parcel2, a);
                    break;
                case 15:
                    f = f22.n(parcel2, a);
                    break;
                case 16:
                    j3 = f22.s(parcel2, a);
                    break;
                case 17:
                    str5 = f22.e(parcel2, a);
                    break;
                case 18:
                    z = f22.i(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new WakeLockEvent(i, j, i2, str, i3, arrayList, str2, j2, i4, str3, str4, f, j3, str5, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new WakeLockEvent[i];
    }
}
