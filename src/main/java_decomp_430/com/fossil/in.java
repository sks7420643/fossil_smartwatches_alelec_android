package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class in extends dn<Boolean> {
    @DexIgnore
    public in(Context context, to toVar) {
        super(pn.a(context, toVar).d());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.i();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
