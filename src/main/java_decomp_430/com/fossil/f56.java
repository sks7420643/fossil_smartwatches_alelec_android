package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f56 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public DisplayMetrics c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public Context o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;

    @DexIgnore
    public f56(Context context) {
        this.b = "2.0.3";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.o = context.getApplicationContext();
        this.c = m56.g(this.o);
        this.a = m56.m(this.o);
        this.h = n36.d(this.o);
        this.i = m56.l(this.o);
        this.j = TimeZone.getDefault().getID();
        this.l = m56.r(this.o);
        this.k = m56.s(this.o);
        this.m = this.o.getPackageName();
        if (this.d >= 14) {
            this.p = m56.y(this.o);
        }
        this.q = m56.x(this.o).toString();
        this.r = m56.w(this.o);
        this.s = m56.d();
        this.n = m56.b(this.o);
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        if (thread == null) {
            if (this.c != null) {
                jSONObject.put("sr", this.c.widthPixels + "*" + this.c.heightPixels);
                jSONObject.put("dpi", this.c.xdpi + "*" + this.c.ydpi);
            }
            if (b46.a(this.o).e()) {
                JSONObject jSONObject2 = new JSONObject();
                r56.a(jSONObject2, "bs", r56.d(this.o));
                r56.a(jSONObject2, "ss", r56.e(this.o));
                if (jSONObject2.length() > 0) {
                    r56.a(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray a2 = r56.a(this.o, 10);
            if (a2 != null && a2.length() > 0) {
                r56.a(jSONObject, "wflist", a2.toString());
            }
            str = this.p;
            str2 = "sen";
        } else {
            r56.a(jSONObject, "thn", thread.getName());
            r56.a(jSONObject, "qq", n36.f(this.o));
            r56.a(jSONObject, "cui", n36.c(this.o));
            if (m56.c(this.r) && this.r.split(ZendeskConfig.SLASH).length == 2) {
                r56.a(jSONObject, "fram", this.r.split(ZendeskConfig.SLASH)[0]);
            }
            if (m56.c(this.s) && this.s.split(ZendeskConfig.SLASH).length == 2) {
                r56.a(jSONObject, "from", this.s.split(ZendeskConfig.SLASH)[0]);
            }
            if (o46.b(this.o).a(this.o) != null) {
                jSONObject.put("ui", o46.b(this.o).a(this.o).b());
            }
            str = n36.e(this.o);
            str2 = "mid";
        }
        r56.a(jSONObject, str2, str);
        r56.a(jSONObject, "pcn", m56.t(this.o));
        r56.a(jSONObject, "osn", Build.VERSION.RELEASE);
        r56.a(jSONObject, "av", this.a);
        r56.a(jSONObject, "ch", this.h);
        r56.a(jSONObject, "mf", this.f);
        r56.a(jSONObject, "sv", this.b);
        r56.a(jSONObject, "osd", Build.DISPLAY);
        r56.a(jSONObject, "prod", Build.PRODUCT);
        r56.a(jSONObject, "tags", Build.TAGS);
        r56.a(jSONObject, "id", Build.ID);
        r56.a(jSONObject, "fng", Build.FINGERPRINT);
        r56.a(jSONObject, "lch", this.n);
        r56.a(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        r56.a(jSONObject, "op", this.i);
        r56.a(jSONObject, "lg", this.g);
        r56.a(jSONObject, "md", this.e);
        r56.a(jSONObject, "tz", this.j);
        int i2 = this.l;
        if (i2 != 0) {
            jSONObject.put("jb", i2);
        }
        r56.a(jSONObject, "sd", this.k);
        r56.a(jSONObject, "apn", this.m);
        r56.a(jSONObject, "cpu", this.q);
        r56.a(jSONObject, "abi", Build.CPU_ABI);
        r56.a(jSONObject, "abi2", Build.CPU_ABI2);
        r56.a(jSONObject, "ram", this.r);
        r56.a(jSONObject, "rom", this.s);
    }
}
