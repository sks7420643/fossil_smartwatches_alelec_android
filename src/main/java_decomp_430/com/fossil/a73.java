package com.fossil;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a73 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public mv2 g;
    @DexIgnore
    public boolean h; // = true;

    @DexIgnore
    public a73(Context context, mv2 mv2) {
        w12.a(context);
        Context applicationContext = context.getApplicationContext();
        w12.a(applicationContext);
        this.a = applicationContext;
        if (mv2 != null) {
            this.g = mv2;
            this.b = mv2.f;
            this.c = mv2.e;
            this.d = mv2.d;
            this.h = mv2.c;
            this.f = mv2.b;
            Bundle bundle = mv2.g;
            if (bundle != null) {
                this.e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
