package com.fossil;

import com.fossil.af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl6 extends ve6 {
    @DexIgnore
    public static /* final */ a b; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements af6.c<hl6> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof hl6) && wg6.a((Object) this.a, (Object) ((hl6) obj).a);
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final String o() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineName(" + this.a + ')';
    }
}
