package com.fossil;

import com.fossil.li6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ni6<T, R> extends li6<R>, hg6<T, R> {

    @DexIgnore
    public interface a<T, R> extends li6.b<R>, hg6<T, R> {
    }

    @DexIgnore
    R get(T t);

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    a<T, R> getGetter();
}
