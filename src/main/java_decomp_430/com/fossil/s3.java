package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s3 {
    @DexIgnore
    public abstract void a(Runnable runnable);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public void b(Runnable runnable) {
        if (a()) {
            runnable.run();
        } else {
            c(runnable);
        }
    }

    @DexIgnore
    public abstract void c(Runnable runnable);
}
