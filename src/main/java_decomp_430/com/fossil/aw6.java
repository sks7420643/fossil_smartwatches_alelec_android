package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aw6 implements xv6 {
    @DexIgnore
    public String a;
    @DexIgnore
    public iw6 b;
    @DexIgnore
    public Queue<dw6> c;

    @DexIgnore
    public aw6(iw6 iw6, Queue<dw6> queue) {
        this.b = iw6;
        this.a = iw6.c();
        this.c = queue;
    }

    @DexIgnore
    public final void a(bw6 bw6, String str, Object[] objArr, Throwable th) {
        a(bw6, (zv6) null, str, objArr, th);
    }

    @DexIgnore
    public void debug(String str) {
        a(bw6.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void error(String str) {
        a(bw6.ERROR, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void info(String str) {
        a(bw6.INFO, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    public void trace(String str) {
        a(bw6.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void warn(String str) {
        a(bw6.WARN, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public final void a(bw6 bw6, zv6 zv6, String str, Object[] objArr, Throwable th) {
        dw6 dw6 = new dw6();
        dw6.a(System.currentTimeMillis());
        dw6.a(bw6);
        dw6.a(this.b);
        dw6.a(this.a);
        dw6.a(zv6);
        dw6.b(str);
        dw6.a(objArr);
        dw6.a(th);
        dw6.c(Thread.currentThread().getName());
        this.c.add(dw6);
    }

    @DexIgnore
    public void debug(String str, Throwable th) {
        a(bw6.DEBUG, str, (Object[]) null, th);
    }

    @DexIgnore
    public void error(String str, Object... objArr) {
        a(bw6.ERROR, str, objArr, (Throwable) null);
    }

    @DexIgnore
    public void info(String str, Object obj) {
        a(bw6.INFO, str, new Object[]{obj}, (Throwable) null);
    }

    @DexIgnore
    public void trace(String str, Throwable th) {
        a(bw6.TRACE, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Object obj, Object obj2) {
        a(bw6.WARN, str, new Object[]{obj, obj2}, (Throwable) null);
    }

    @DexIgnore
    public void error(String str, Throwable th) {
        a(bw6.ERROR, str, (Object[]) null, th);
    }

    @DexIgnore
    public void info(String str, Throwable th) {
        a(bw6.INFO, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Throwable th) {
        a(bw6.WARN, str, (Object[]) null, th);
    }
}
