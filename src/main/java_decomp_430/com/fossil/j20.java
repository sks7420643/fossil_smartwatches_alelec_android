package com.fossil;

import com.fossil.j96;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j20 extends i86<Boolean> implements e96 {
    @DexIgnore
    public Map<j96.a, String> b() {
        return Collections.emptyMap();
    }

    @DexIgnore
    public String h() {
        return "com.crashlytics.sdk.android:beta";
    }

    @DexIgnore
    public String j() {
        return "1.2.10.27";
    }

    @DexIgnore
    public Boolean c() {
        c86.g().d("Beta", "Beta kit initializing...");
        return true;
    }
}
