package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fn extends dn<ym> {
    @DexIgnore
    public static /* final */ String e; // = tl.a("NetworkMeteredCtrlr");

    @DexIgnore
    public fn(Context context, to toVar) {
        super(pn.a(context, toVar).c());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.b() == ul.METERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(ym ymVar) {
        if (Build.VERSION.SDK_INT < 26) {
            tl.a().a(e, "Metered network constraint is not supported before API 26, only checking for connected state.", new Throwable[0]);
            return !ymVar.a();
        } else if (!ymVar.a() || !ymVar.b()) {
            return true;
        } else {
            return false;
        }
    }
}
