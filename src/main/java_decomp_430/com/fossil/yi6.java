package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yi6 extends xi6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ti6<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public a(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    @DexIgnore
    public static final <T> ti6<T> a(Iterator<? extends T> it) {
        wg6.b(it, "$this$asSequence");
        return a(new a(it));
    }

    @DexIgnore
    public static final <T> ti6<T> a(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$constrainOnce");
        return ti6 instanceof qi6 ? ti6 : new qi6(ti6);
    }

    @DexIgnore
    public static final <T> ti6<T> a(gg6<? extends T> gg6, hg6<? super T, ? extends T> hg6) {
        wg6.b(gg6, "seedFunction");
        wg6.b(hg6, "nextFunction");
        return new si6(gg6, hg6);
    }
}
