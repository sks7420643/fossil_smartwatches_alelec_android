package com.fossil;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw3 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[0];

    @DexIgnore
    public interface a {
    }

    @DexIgnore
    public interface b<T extends a> {
    }

    @DexIgnore
    public interface c<E> extends List<E>, RandomAccess {
        @DexIgnore
        c<E> b(int i);

        @DexIgnore
        void l();

        @DexIgnore
        boolean m();
    }

    /*
    static {
        Charset.forName("ISO-8859-1");
        ByteBuffer.wrap(b);
        tw3.a(b);
    }
    */

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = i2; i5 < i2 + i3; i5++) {
            i4 = (i4 * 31) + bArr[i5];
        }
        return i4;
    }

    @DexIgnore
    public static int a(long j) {
        return (int) (j ^ (j >>> 32));
    }
}
