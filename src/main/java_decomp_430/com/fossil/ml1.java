package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ml1 {
    SIMPLE_MOVEMENT((byte) 1);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public ml1(byte b) {
        this.a = b;
    }
}
