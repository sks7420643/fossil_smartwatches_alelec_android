package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo3<E> extends im3<E> {
    @DexIgnore
    public transient int b;
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public bo3(E e) {
        jk3.a(e);
        this.element = e;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.element.equals(obj);
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        objArr[i] = this.element;
        return i + 1;
    }

    @DexIgnore
    public zl3<E> createAsList() {
        return zl3.of(this.element);
    }

    @DexIgnore
    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = this.element.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return this.b != 0;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }

    @DexIgnore
    public jo3<E> iterator() {
        return qm3.a(this.element);
    }

    @DexIgnore
    public bo3(E e, int i) {
        this.element = e;
        this.b = i;
    }
}
