package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn implements rn {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hh<qn> {
        @DexIgnore
        public a(sn snVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(mi miVar, qn qnVar) {
            String str = qnVar.a;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            String str2 = qnVar.b;
            if (str2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency`(`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public sn(oh ohVar) {
        this.a = ohVar;
        this.b = new a(this, ohVar);
    }

    @DexIgnore
    public void a(qn qnVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(qnVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public boolean b(String str) {
        rh b2 = rh.b("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z = false;
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public boolean c(String str) {
        rh b2 = rh.b("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z = false;
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public List<String> a(String str) {
        rh b2 = rh.b("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
