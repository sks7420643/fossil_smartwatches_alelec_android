package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rh0 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        r40 r40;
        String str = null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == 81199830 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED")) {
            ii1 ii1 = (ii1) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            q40.c cVar = (q40.c) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE");
            q40.c cVar2 = (q40.c) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE");
            oa1 oa1 = oa1.a;
            kj0 kj0 = kj0.i;
            Object[] objArr = new Object[3];
            if (!(ii1 == null || (r40 = ii1.r) == null)) {
                str = r40.getMacAddress();
            }
            objArr[0] = str;
            objArr[1] = cVar;
            objArr[2] = cVar2;
            if (ii1 != null && cVar != null && cVar2 != null) {
                kj0.i.a(ii1);
            }
        }
    }
}
