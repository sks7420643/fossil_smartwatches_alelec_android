package com.fossil;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y06 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ m16 a;
    @DexIgnore
    public /* final */ WeakReference<ImageView> b;
    @DexIgnore
    public v06 c;

    @DexIgnore
    public y06(m16 m16, ImageView imageView, v06 v06) {
        this.a = m16;
        this.b = new WeakReference<>(imageView);
        this.c = v06;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        ImageView imageView = (ImageView) this.b.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = (ImageView) this.b.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        if (width > 0 && height > 0) {
            viewTreeObserver.removeOnPreDrawListener(this);
            m16 m16 = this.a;
            m16.c();
            m16.a(width, height);
            m16.a(imageView, this.c);
        }
        return true;
    }
}
