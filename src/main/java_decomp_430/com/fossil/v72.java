package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v72 implements Parcelable.Creator<DataSet> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        f72 f72 = null;
        ArrayList<f72> arrayList2 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                f72 = f22.a(parcel, a, f72.CREATOR);
            } else if (a2 == 1000) {
                i = f22.q(parcel, a);
            } else if (a2 == 3) {
                f22.a(parcel, a, (List) arrayList, v72.class.getClassLoader());
            } else if (a2 == 4) {
                arrayList2 = f22.c(parcel, a, f72.CREATOR);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                z = f22.i(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new DataSet(i, f72, arrayList, arrayList2, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataSet[i];
    }
}
