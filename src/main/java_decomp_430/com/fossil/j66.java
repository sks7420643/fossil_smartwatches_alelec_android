package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j66 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<j66> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ m66 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<j66> {
        @DexIgnore
        public j66 createFromParcel(Parcel parcel) {
            return new j66(parcel, (a) null);
        }

        @DexIgnore
        public j66[] newArray(int i) {
            return new j66[i];
        }
    }

    @DexIgnore
    public /* synthetic */ j66(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public m66 a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeParcelable(this.b, i);
        parcel.writeSerializable(this.c);
    }

    @DexIgnore
    public j66(Intent intent, int i, m66 m66) {
        this.b = intent;
        this.a = i;
        this.c = m66;
    }

    @DexIgnore
    public void a(Activity activity) {
        activity.startActivityForResult(this.b, this.a);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        fragment.startActivityForResult(this.b, this.a);
    }

    @DexIgnore
    public j66(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = (Intent) parcel.readParcelable(j66.class.getClassLoader());
        this.c = (m66) parcel.readSerializable();
    }
}
