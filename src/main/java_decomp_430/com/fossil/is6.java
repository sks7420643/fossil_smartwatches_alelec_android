package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class is6 {
    @DexIgnore
    public static /* final */ mt6 d; // = mt6.encodeUtf8(":");
    @DexIgnore
    public static /* final */ mt6 e; // = mt6.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ mt6 f; // = mt6.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ mt6 g; // = mt6.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ mt6 h; // = mt6.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ mt6 i; // = mt6.encodeUtf8(":authority");
    @DexIgnore
    public /* final */ mt6 a;
    @DexIgnore
    public /* final */ mt6 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(sq6 sq6);
    }

    @DexIgnore
    public is6(String str, String str2) {
        this(mt6.encodeUtf8(str), mt6.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof is6)) {
            return false;
        }
        is6 is6 = (is6) obj;
        if (!this.a.equals(is6.a) || !this.b.equals(is6.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.a.hashCode()) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return fr6.a("%s: %s", this.a.utf8(), this.b.utf8());
    }

    @DexIgnore
    public is6(mt6 mt6, String str) {
        this(mt6, mt6.encodeUtf8(str));
    }

    @DexIgnore
    public is6(mt6 mt6, mt6 mt62) {
        this.a = mt6;
        this.b = mt62;
        this.c = mt6.size() + 32 + mt62.size();
    }
}
