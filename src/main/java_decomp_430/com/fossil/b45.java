package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b45 {
    @DexIgnore
    public /* final */ z35 a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public b45(z35 z35, String str, int i, String str2, String str3) {
        wg6.b(z35, "mView");
        wg6.b(str, "mPresetId");
        this.a = z35;
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final z35 b() {
        return this.a;
    }
}
