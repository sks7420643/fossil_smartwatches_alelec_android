package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k74 extends j74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public long u;

    /*
    static {
        w.put(2131362561, 1);
        w.put(2131362442, 2);
        w.put(2131362534, 3);
        w.put(2131362653, 4);
        w.put(2131362861, 5);
    }
    */

    @DexIgnore
    public k74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public k74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[3], objArr[1], objArr[4], objArr[0], objArr[5]);
        this.u = -1;
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
