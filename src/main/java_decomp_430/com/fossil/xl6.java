package com.fossil;

import com.fossil.mc6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl6 {
    @DexIgnore
    public static /* final */ uo6 a; // = new uo6("UNDEFINED");

    @DexIgnore
    public static final <T> void b(xe6<? super T> xe6, T t) {
        wg6.b(xe6, "$this$resumeDirect");
        if (xe6 instanceof vl6) {
            xe6<T> xe62 = ((vl6) xe6).h;
            mc6.a aVar = mc6.Companion;
            xe62.resumeWith(mc6.m1constructorimpl(t));
            return;
        }
        mc6.a aVar2 = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(t));
    }

    @DexIgnore
    public static final void a(yl6<?> yl6) {
        em6 b = on6.b.b();
        if (b.p()) {
            b.a(yl6);
            return;
        }
        b.c(true);
        try {
            a(yl6, yl6.b(), 3);
            do {
            } while (b.D());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
    }

    @DexIgnore
    public static final <T> void b(xe6<? super T> xe6, Throwable th) {
        wg6.b(xe6, "$this$resumeDirectWithException");
        wg6.b(th, "exception");
        if (xe6 instanceof vl6) {
            xe6<T> xe62 = ((vl6) xe6).h;
            mc6.a aVar = mc6.Companion;
            xe62.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(th, (xe6<?>) xe62))));
            return;
        }
        mc6.a aVar2 = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(th, (xe6<?>) xe6))));
    }

    @DexIgnore
    public static final <T> void a(xe6<? super T> xe6, T t) {
        boolean z;
        af6 context;
        Object b;
        wg6.b(xe6, "$this$resumeCancellable");
        if (xe6 instanceof vl6) {
            vl6 vl6 = (vl6) xe6;
            if (vl6.g.b(vl6.getContext())) {
                vl6.d = t;
                vl6.c = 1;
                vl6.g.a(vl6.getContext(), vl6);
                return;
            }
            em6 b2 = on6.b.b();
            if (b2.p()) {
                vl6.d = t;
                vl6.c = 1;
                b2.a((yl6<?>) vl6);
                return;
            }
            b2.c(true);
            try {
                rm6 rm6 = (rm6) vl6.getContext().get(rm6.n);
                if (rm6 == null || rm6.isActive()) {
                    z = false;
                } else {
                    CancellationException k = rm6.k();
                    mc6.a aVar = mc6.Companion;
                    vl6.resumeWith(mc6.m1constructorimpl(nc6.a((Throwable) k)));
                    z = true;
                }
                if (!z) {
                    context = vl6.getContext();
                    b = yo6.b(context, vl6.f);
                    xe6<T> xe62 = vl6.h;
                    mc6.a aVar2 = mc6.Companion;
                    xe62.resumeWith(mc6.m1constructorimpl(t));
                    cd6 cd6 = cd6.a;
                    yo6.a(context, b);
                }
                do {
                } while (b2.D());
            } catch (Throwable th) {
                try {
                    vl6.a(th, (Throwable) null);
                } catch (Throwable th2) {
                    b2.a(true);
                    throw th2;
                }
            }
            b2.a(true);
            return;
        }
        mc6.a aVar3 = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(t));
    }

    @DexIgnore
    public static final <T> void a(xe6<? super T> xe6, Throwable th) {
        af6 context;
        Object b;
        wg6.b(xe6, "$this$resumeCancellableWithException");
        wg6.b(th, "exception");
        if (xe6 instanceof vl6) {
            vl6 vl6 = (vl6) xe6;
            af6 context2 = vl6.h.getContext();
            boolean z = false;
            vk6 vk6 = new vk6(th, false, 2, (qg6) null);
            if (vl6.g.b(context2)) {
                vl6.d = new vk6(th, false, 2, (qg6) null);
                vl6.c = 1;
                vl6.g.a(context2, vl6);
                return;
            }
            em6 b2 = on6.b.b();
            if (b2.p()) {
                vl6.d = vk6;
                vl6.c = 1;
                b2.a((yl6<?>) vl6);
                return;
            }
            b2.c(true);
            try {
                rm6 rm6 = (rm6) vl6.getContext().get(rm6.n);
                if (rm6 != null && !rm6.isActive()) {
                    CancellationException k = rm6.k();
                    mc6.a aVar = mc6.Companion;
                    vl6.resumeWith(mc6.m1constructorimpl(nc6.a((Throwable) k)));
                    z = true;
                }
                if (!z) {
                    context = vl6.getContext();
                    b = yo6.b(context, vl6.f);
                    xe6<T> xe62 = vl6.h;
                    mc6.a aVar2 = mc6.Companion;
                    xe62.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(th, (xe6<?>) xe62))));
                    cd6 cd6 = cd6.a;
                    yo6.a(context, b);
                }
                do {
                } while (b2.D());
            } catch (Throwable th2) {
                try {
                    vl6.a(th2, (Throwable) null);
                } catch (Throwable th3) {
                    b2.a(true);
                    throw th3;
                }
            }
            b2.a(true);
            return;
        }
        mc6.a aVar3 = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(th, (xe6<?>) xe6))));
    }

    @DexIgnore
    public static final boolean a(vl6<? super cd6> vl6) {
        wg6.b(vl6, "$this$yieldUndispatched");
        cd6 cd6 = cd6.a;
        em6 b = on6.b.b();
        if (b.B()) {
            return false;
        }
        if (b.p()) {
            vl6.d = cd6;
            vl6.c = 1;
            b.a((yl6<?>) vl6);
            return true;
        }
        b.c(true);
        try {
            vl6.run();
            do {
            } while (b.D());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
        return false;
    }

    @DexIgnore
    public static final <T> void a(yl6<? super T> yl6, int i) {
        wg6.b(yl6, "$this$dispatch");
        xe6<? super T> b = yl6.b();
        if (!jn6.b(i) || !(b instanceof vl6) || jn6.a(i) != jn6.a(yl6.c)) {
            a(yl6, b, i);
            return;
        }
        dl6 dl6 = ((vl6) b).g;
        af6 context = b.getContext();
        if (dl6.b(context)) {
            dl6.a(context, yl6);
        } else {
            a((yl6<?>) yl6);
        }
    }

    @DexIgnore
    public static final <T> void a(yl6<? super T> yl6, xe6<? super T> xe6, int i) {
        wg6.b(yl6, "$this$resume");
        wg6.b(xe6, "delegate");
        Object c = yl6.c();
        Throwable a2 = yl6.a(c);
        if (a2 != null) {
            if (!(xe6 instanceof yl6)) {
                a2 = to6.a(a2, (xe6<?>) xe6);
            }
            jn6.b(xe6, a2, i);
            return;
        }
        jn6.a(xe6, yl6.c(c), i);
    }
}
