package com.fossil;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx6<T> {
    @DexIgnore
    public /* final */ Response a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ zq6 c;

    @DexIgnore
    public rx6(Response response, T t, zq6 zq6) {
        this.a = response;
        this.b = t;
        this.c = zq6;
    }

    @DexIgnore
    public static <T> rx6<T> a(T t, Response response) {
        vx6.a(response, "rawResponse == null");
        if (response.B()) {
            return new rx6<>(response, t, (zq6) null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    @DexIgnore
    public int b() {
        return this.a.n();
    }

    @DexIgnore
    public zq6 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.B();
    }

    @DexIgnore
    public String e() {
        return this.a.C();
    }

    @DexIgnore
    public Response f() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static <T> rx6<T> a(zq6 zq6, Response response) {
        vx6.a(zq6, "body == null");
        vx6.a(response, "rawResponse == null");
        if (!response.B()) {
            return new rx6<>(response, (Object) null, zq6);
        }
        throw new IllegalArgumentException("rawResponse should not be successful response");
    }

    @DexIgnore
    public T a() {
        return this.b;
    }
}
