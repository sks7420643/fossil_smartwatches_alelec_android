package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum f90 {
    GET,
    SET;
    
    @DexIgnore
    public static /* final */ a b; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final f90 a(bo0 bo0) {
            int i = kb1.a[bo0.ordinal()];
            if (i == 1) {
                return f90.GET;
            }
            if (i == 2) {
                return f90.SET;
            }
            throw new kc6();
        }
    }

    /*
    static {
        b = new a((qg6) null);
    }
    */
}
