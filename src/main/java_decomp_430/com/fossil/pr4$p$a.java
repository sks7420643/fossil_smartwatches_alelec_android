package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
public final class pr4$p$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DownloadFirmwareByDeviceModelUsecase.d $responseValue;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase.p this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pr4$p$a(LinkDeviceUseCase.p pVar, DownloadFirmwareByDeviceModelUsecase.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = pVar;
        this.$responseValue = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pr4$p$a pr4_p_a = new pr4$p$a(this.this$0, this.$responseValue, xe6);
        pr4_p_a.p$ = (il6) obj;
        return pr4_p_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pr4$p$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            String a = this.$responseValue.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkDeviceUseCase.r.a();
            local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a);
            FirmwareData a3 = UpdateFirmwareUsecase.g.a(this.this$0.a.m, this.this$0.a.f());
            if (a3 == null) {
                a3 = new EmptyFirmwareData();
            }
            PortfolioApp instance = PortfolioApp.get.instance();
            String h = this.this$0.a.h();
            if (h != null) {
                instance.a(h, (PairingResponse) PairingResponse.CREATOR.buildPairingUpdateFWResponse(a3));
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
