package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr2 implements pr2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a; // = new vk2(qk2.a("com.google.android.gms.measurement")).a("measurement.upload.disable_is_uploader", true);

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
