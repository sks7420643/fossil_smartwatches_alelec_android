package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class si6<T> implements ti6<T> {
    @DexIgnore
    public /* final */ gg6<T> a;
    @DexIgnore
    public /* final */ hg6<T, T> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, ph6 {
        @DexIgnore
        public T a;
        @DexIgnore
        public int b; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ si6 c;

        @DexIgnore
        public a(si6 si6) {
            this.c = si6;
        }

        @DexIgnore
        public final void a() {
            T t;
            if (this.b == -2) {
                t = this.c.a.invoke();
            } else {
                hg6 b2 = this.c.b;
                T t2 = this.a;
                if (t2 != null) {
                    t = b2.invoke(t2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.a = t;
            this.b = this.a == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b < 0) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        public T next() {
            if (this.b < 0) {
                a();
            }
            if (this.b != 0) {
                T t = this.a;
                if (t != null) {
                    this.b = -1;
                    return t;
                }
                throw new rc6("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public si6(gg6<? extends T> gg6, hg6<? super T, ? extends T> hg6) {
        wg6.b(gg6, "getInitialValue");
        wg6.b(hg6, "getNextValue");
        this.a = gg6;
        this.b = hg6;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
