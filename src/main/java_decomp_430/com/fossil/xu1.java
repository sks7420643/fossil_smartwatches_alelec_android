package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu1 implements Parcelable.Creator<SignInAccount> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = str;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 4) {
                str = f22.e(parcel, a);
            } else if (a2 == 7) {
                googleSignInAccount = (GoogleSignInAccount) f22.a(parcel, a, GoogleSignInAccount.CREATOR);
            } else if (a2 != 8) {
                f22.v(parcel, a);
            } else {
                str2 = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new SignInAccount(str, googleSignInAccount, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInAccount[i];
    }
}
