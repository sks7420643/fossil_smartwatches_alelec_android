package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z45 extends k24<y45> {
    @DexIgnore
    void A(String str);

    @DexIgnore
    void G(String str);

    @DexIgnore
    void O(boolean z);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void a(CommuteTimeSetting commuteTimeSetting);

    @DexIgnore
    void b();

    @DexIgnore
    void c(String str, String str2);

    @DexIgnore
    void j(List<String> list);

    @DexIgnore
    void u(boolean z);

    @DexIgnore
    void z(String str);
}
