package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f60 extends x50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<f60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public f60 createFromParcel(Parcel parcel) {
            return new f60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new f60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m13createFromParcel(Parcel parcel) {
            return new f60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public f60() {
        super(z50.WEATHER, (ic0) null, (lc0) null, (mc0) null, 14);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ f60(lc0 lc0, mc0 mc0, int i, qg6 qg6) {
        this(lc0, (i & 2) != 0 ? new mc0(mc0.CREATOR.a()) : mc0);
    }

    @DexIgnore
    public f60(lc0 lc0, mc0 mc0) {
        super(z50.WEATHER, (ic0) null, lc0, mc0, 2);
    }

    @DexIgnore
    public /* synthetic */ f60(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
