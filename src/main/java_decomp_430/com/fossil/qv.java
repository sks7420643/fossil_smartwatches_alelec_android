package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.jv;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qv<Data> implements jv<String, Data> {
    @DexIgnore
    public /* final */ jv<Uri, Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements kv<String, AssetFileDescriptor> {
        @DexIgnore
        public jv<String, AssetFileDescriptor> a(nv nvVar) {
            return new qv(nvVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements kv<String, ParcelFileDescriptor> {
        @DexIgnore
        public jv<String, ParcelFileDescriptor> a(nv nvVar) {
            return new qv(nvVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements kv<String, InputStream> {
        @DexIgnore
        public jv<String, InputStream> a(nv nvVar) {
            return new qv(nvVar.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public qv(jv<Uri, Data> jvVar) {
        this.a = jvVar;
    }

    @DexIgnore
    public static Uri b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return c(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? c(str) : parse;
    }

    @DexIgnore
    public static Uri c(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    public boolean a(String str) {
        return true;
    }

    @DexIgnore
    public jv.a<Data> a(String str, int i, int i2, xr xrVar) {
        Uri b2 = b(str);
        if (b2 == null || !this.a.a(b2)) {
            return null;
        }
        return this.a.a(b2, i, i2, xrVar);
    }
}
