package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDaySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj5 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public ArrayList<b> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ OverviewSleepDayChart a;
        @DexIgnore
        public /* final */ OverviewSleepDaySummary b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ FlexibleTextView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ FlexibleTextView h;
        @DexIgnore
        public /* final */ FlexibleTextView i;
        @DexIgnore
        public /* final */ FlexibleTextView j;
        @DexIgnore
        public /* final */ FlexibleTextView k;
        @DexIgnore
        public /* final */ FlexibleTextView l;
        @DexIgnore
        public /* final */ FlexibleTextView m;
        @DexIgnore
        public /* final */ FlexibleTextView n;
        @DexIgnore
        public /* final */ FlexibleTextView o;
        @DexIgnore
        public /* final */ FlexibleTextView p;
        @DexIgnore
        public /* final */ FlexibleTextView q;
        @DexIgnore
        public /* final */ ConstraintLayout r;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            wg6.b(view, "item");
            View findViewById = view.findViewById(2131362949);
            wg6.a((Object) findViewById, "item.findViewById(R.id.sleep_chart)");
            this.a = (OverviewSleepDayChart) findViewById;
            View findViewById2 = view.findViewById(2131363277);
            wg6.a((Object) findViewById2, "item.findViewById(R.id.v_summary_chart)");
            this.b = (OverviewSleepDaySummary) findViewById2;
            View findViewById3 = view.findViewById(2131363091);
            wg6.a((Object) findViewById3, "item.findViewById(R.id.tv_awake)");
            this.c = (FlexibleTextView) findViewById3;
            View findViewById4 = view.findViewById(2131363166);
            wg6.a((Object) findViewById4, "item.findViewById(R.id.tv_light)");
            this.d = (FlexibleTextView) findViewById4;
            View findViewById5 = view.findViewById(2131363121);
            wg6.a((Object) findViewById5, "item.findViewById(R.id.tv_deep)");
            this.e = (FlexibleTextView) findViewById5;
            View findViewById6 = view.findViewById(2131363095);
            wg6.a((Object) findViewById6, "item.findViewById(R.id.tv_awake_min)");
            this.f = (FlexibleTextView) findViewById6;
            View findViewById7 = view.findViewById(2131363170);
            wg6.a((Object) findViewById7, "item.findViewById(R.id.tv_light_min)");
            this.g = (FlexibleTextView) findViewById7;
            View findViewById8 = view.findViewById(2131363125);
            wg6.a((Object) findViewById8, "item.findViewById(R.id.tv_deep_min)");
            this.h = (FlexibleTextView) findViewById8;
            View findViewById9 = view.findViewById(2131363093);
            wg6.a((Object) findViewById9, "item.findViewById(R.id.tv_awake_hour)");
            this.i = (FlexibleTextView) findViewById9;
            View findViewById10 = view.findViewById(2131363168);
            wg6.a((Object) findViewById10, "item.findViewById(R.id.tv_light_hour)");
            this.j = (FlexibleTextView) findViewById10;
            View findViewById11 = view.findViewById(2131363123);
            wg6.a((Object) findViewById11, "item.findViewById(R.id.tv_deep_hour)");
            this.k = (FlexibleTextView) findViewById11;
            View findViewById12 = view.findViewById(2131363094);
            wg6.a((Object) findViewById12, "item.findViewById(R.id.tv_awake_hour_unit)");
            this.l = (FlexibleTextView) findViewById12;
            View findViewById13 = view.findViewById(2131363169);
            wg6.a((Object) findViewById13, "item.findViewById(R.id.tv_light_hour_unit)");
            this.m = (FlexibleTextView) findViewById13;
            View findViewById14 = view.findViewById(2131363124);
            wg6.a((Object) findViewById14, "item.findViewById(R.id.tv_deep_hour_unit)");
            this.n = (FlexibleTextView) findViewById14;
            View findViewById15 = view.findViewById(2131363096);
            wg6.a((Object) findViewById15, "item.findViewById(R.id.tv_awake_min_unit)");
            this.o = (FlexibleTextView) findViewById15;
            View findViewById16 = view.findViewById(2131363171);
            wg6.a((Object) findViewById16, "item.findViewById(R.id.tv_light_min_unit)");
            this.p = (FlexibleTextView) findViewById16;
            View findViewById17 = view.findViewById(2131363126);
            wg6.a((Object) findViewById17, "item.findViewById(R.id.tv_deep_min_unit)");
            this.q = (FlexibleTextView) findViewById17;
            ConstraintLayout findViewById18 = view.findViewById(2131362073);
            wg6.a((Object) findViewById18, "item.findViewById(R.id.cl_sleep_chart)");
            this.r = findViewById18;
        }

        @DexIgnore
        public final ConstraintLayout a() {
            return this.r;
        }

        @DexIgnore
        public final OverviewSleepDayChart b() {
            return this.a;
        }

        @DexIgnore
        public final OverviewSleepDaySummary c() {
            return this.b;
        }

        @DexIgnore
        public final FlexibleTextView d() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView e() {
            return this.i;
        }

        @DexIgnore
        public final FlexibleTextView f() {
            return this.l;
        }

        @DexIgnore
        public final FlexibleTextView g() {
            return this.f;
        }

        @DexIgnore
        public final FlexibleTextView h() {
            return this.o;
        }

        @DexIgnore
        public final FlexibleTextView i() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView j() {
            return this.k;
        }

        @DexIgnore
        public final FlexibleTextView k() {
            return this.n;
        }

        @DexIgnore
        public final FlexibleTextView l() {
            return this.h;
        }

        @DexIgnore
        public final FlexibleTextView m() {
            return this.q;
        }

        @DexIgnore
        public final FlexibleTextView n() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView o() {
            return this.j;
        }

        @DexIgnore
        public final FlexibleTextView p() {
            return this.m;
        }

        @DexIgnore
        public final FlexibleTextView q() {
            return this.g;
        }

        @DexIgnore
        public final FlexibleTextView r() {
            return this.p;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public ut4 a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public b(ut4 ut4, float f2, float f3, float f4, int i, int i2, int i3, int i4) {
            wg6.b(ut4, "sleepChartModel");
            this.a = ut4;
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = i;
            this.f = i2;
            this.g = i3;
            this.h = i4;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final float b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.g;
        }

        @DexIgnore
        public final float d() {
            return this.d;
        }

        @DexIgnore
        public final int e() {
            return this.f;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && Float.compare(this.b, bVar.b) == 0 && Float.compare(this.c, bVar.c) == 0 && Float.compare(this.d, bVar.d) == 0 && this.e == bVar.e && this.f == bVar.f && this.g == bVar.g && this.h == bVar.h;
        }

        @DexIgnore
        public final float f() {
            return this.c;
        }

        @DexIgnore
        public final ut4 g() {
            return this.a;
        }

        @DexIgnore
        public final int h() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            ut4 ut4 = this.a;
            return ((((((((((((((ut4 != null ? ut4.hashCode() : 0) * 31) + c.a(this.b)) * 31) + c.a(this.c)) * 31) + c.a(this.d)) * 31) + d.a(this.e)) * 31) + d.a(this.f)) * 31) + d.a(this.g)) * 31) + d.a(this.h);
        }

        @DexIgnore
        public String toString() {
            return "SleepUIData(sleepChartModel=" + this.a + ", awakePer=" + this.b + ", lightPer=" + this.c + ", deepPer=" + this.d + ", awakeMin=" + this.e + ", lightMin=" + this.f + ", deepMin=" + this.g + ", timeZoneOffsetInSecond=" + this.h + ")";
        }
    }

    @DexIgnore
    public fj5(ArrayList<b> arrayList) {
        wg6.b(arrayList, "data");
        this.e = arrayList;
        String b2 = ThemeManager.l.a().b("awakeSleep");
        this.a = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("lightSleep");
        this.b = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("deepSleep");
        this.c = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("nonBrandSurface");
        this.d = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r13v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        b bVar = this.e.get(i);
        wg6.a((Object) bVar, "data[position]");
        b bVar2 = bVar;
        aVar.d().setText(yk4.b.a(2131886480, bVar2.b()));
        aVar.n().setText(yk4.b.a(2131886482, bVar2.f()));
        aVar.i().setText(yk4.b.a(2131886481, bVar2.d()));
        aVar.g().setText(String.valueOf(al4.b(bVar2.a())));
        aVar.q().setText(String.valueOf(al4.b(bVar2.e())));
        aVar.l().setText(String.valueOf(al4.b(bVar2.c())));
        aVar.e().setText(String.valueOf(al4.a(bVar2.a())));
        aVar.o().setText(String.valueOf(al4.a(bVar2.e())));
        aVar.j().setText(String.valueOf(al4.a(bVar2.c())));
        OverviewSleepDayChart b2 = aVar.b();
        BarChart.a(b2, 0, this.a, this.b, this.c, (String) null, (sh4) null, 49, (Object) null);
        b2.setTimeZoneOffsetInSecond(bVar2.h());
        b2.a(bVar2.g());
        aVar.c().a(bVar2.b(), bVar2.f(), bVar2.d(), this.a, this.b, this.c);
        aVar.d().setTextColor(this.a);
        aVar.n().setTextColor(this.b);
        aVar.i().setTextColor(this.c);
        aVar.e().setTextColor(this.a);
        aVar.g().setTextColor(this.a);
        aVar.f().setTextColor(this.a);
        aVar.h().setTextColor(this.a);
        aVar.o().setTextColor(this.b);
        aVar.q().setTextColor(this.b);
        aVar.p().setTextColor(this.b);
        aVar.r().setTextColor(this.b);
        aVar.j().setTextColor(this.c);
        aVar.l().setTextColor(this.c);
        aVar.k().setTextColor(this.c);
        aVar.m().setTextColor(this.c);
        aVar.a().setBackgroundColor(this.d);
    }

    @DexIgnore
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558673, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026p_session, parent, false)");
        return new a(inflate);
    }

    @DexIgnore
    public final void a(List<b> list) {
        wg6.b(list, "data");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }
}
