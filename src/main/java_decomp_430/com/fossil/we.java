package com.fossil;

import com.fossil.bf;
import com.fossil.cf;
import com.fossil.ef;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class we<K, V> extends cf<V> implements ef.a {
    @DexIgnore
    public /* final */ ve<K, V> u;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public /* final */ boolean y;
    @DexIgnore
    public bf.a<V> z; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public b(int i, Object obj) {
            this.a = i;
            this.b = obj;
        }

        @DexIgnore
        public void run() {
            if (!we.this.h()) {
                if (we.this.u.isInvalid()) {
                    we.this.c();
                    return;
                }
                we weVar = we.this;
                weVar.u.dispatchLoadBefore(this.a, this.b, weVar.d.a, weVar.a, weVar.z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public c(int i, Object obj) {
            this.a = i;
            this.b = obj;
        }

        @DexIgnore
        public void run() {
            if (!we.this.h()) {
                if (we.this.u.isInvalid()) {
                    we.this.c();
                    return;
                }
                we weVar = we.this;
                weVar.u.dispatchLoadAfter(this.a, this.b, weVar.d.a, weVar.a, weVar.z);
            }
        }
    }

    @DexIgnore
    public we(ve<K, V> veVar, Executor executor, Executor executor2, cf.e<V> eVar, cf.h hVar, K k, int i) {
        super(new ef(), executor, executor2, eVar, hVar);
        boolean z2 = false;
        this.u = veVar;
        this.f = i;
        if (this.u.isInvalid()) {
            c();
        } else {
            ve<K, V> veVar2 = this.u;
            cf.h hVar2 = this.d;
            veVar2.dispatchLoadInitial(k, hVar2.e, hVar2.a, hVar2.c, this.a, this.z);
        }
        if (this.u.supportsPageDropping() && this.d.d != Integer.MAX_VALUE) {
            z2 = true;
        }
        this.y = z2;
    }

    @DexIgnore
    public static int c(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    public static int d(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    public void a(cf<V> cfVar, cf.g gVar) {
        ef<T> efVar = cfVar.e;
        int g = this.e.g() - efVar.g();
        int h = this.e.h() - efVar.h();
        int p = efVar.p();
        int e = efVar.e();
        if (efVar.isEmpty() || g < 0 || h < 0 || this.e.p() != Math.max(p - g, 0) || this.e.e() != Math.max(e - h, 0) || this.e.o() != efVar.o() + g + h) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (g != 0) {
            int min = Math.min(p, g);
            int i = g - min;
            int e2 = efVar.e() + efVar.o();
            if (min != 0) {
                gVar.a(e2, min);
            }
            if (i != 0) {
                gVar.b(e2 + min, i);
            }
        }
        if (h != 0) {
            int min2 = Math.min(e, h);
            int i2 = h - min2;
            if (min2 != 0) {
                gVar.a(e, min2);
            }
            if (i2 != 0) {
                gVar.b(0, i2);
            }
        }
    }

    @DexIgnore
    public void b() {
        this.t.b(cf.l.START, cf.i.DONE, (Throwable) null);
    }

    @DexIgnore
    public void c(int i) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public xe<?, V> d() {
        return this.u;
    }

    @DexIgnore
    public void e(int i) {
        int d = d(this.d.b, i, this.e.e());
        int c2 = c(this.d.b, i, this.e.e() + this.e.o());
        this.v = Math.max(d, this.v);
        if (this.v > 0 && this.t.e() == cf.i.IDLE) {
            p();
        }
        this.w = Math.max(c2, this.w);
        if (this.w > 0 && this.t.a() == cf.i.IDLE) {
            o();
        }
    }

    @DexIgnore
    public boolean g() {
        return true;
    }

    @DexIgnore
    public final void o() {
        this.t.b(cf.l.END, cf.i.LOADING, (Throwable) null);
        this.b.execute(new c(((this.e.e() + this.e.o()) - 1) + this.e.j(), this.e.d()));
    }

    @DexIgnore
    public final void p() {
        this.t.b(cf.l.START, cf.i.LOADING, (Throwable) null);
        this.b.execute(new b(this.e.e() + this.e.j(), this.e.c()));
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        this.w = (this.w - i2) - i3;
        if (this.w > 0) {
            o();
        } else {
            this.t.b(cf.l.END, cf.i.IDLE, (Throwable) null);
        }
        d(i, i2);
        e(i + i2, i3);
    }

    @DexIgnore
    public void c(int i, int i2) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public Object e() {
        return this.u.getKey(this.f, this.g);
    }

    @DexIgnore
    public void a(int i) {
        boolean z2 = false;
        e(0, i);
        if (this.e.e() > 0 || this.e.p() > 0) {
            z2 = true;
        }
        this.x = z2;
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        this.v = (this.v - i2) - i3;
        if (this.v > 0) {
            p();
        } else {
            this.t.b(cf.l.START, cf.i.IDLE, (Throwable) null);
        }
        d(i, i2);
        e(0, i3);
        f(i3);
    }

    @DexIgnore
    public void a() {
        this.t.b(cf.l.END, cf.i.DONE, (Throwable) null);
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends bf.a<V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, bf<V> bfVar) {
            if (bfVar.a()) {
                we.this.c();
            } else if (!we.this.h()) {
                List<T> list = bfVar.a;
                boolean z = true;
                if (i == 0) {
                    we weVar = we.this;
                    weVar.e.a(bfVar.b, list, bfVar.c, bfVar.d, weVar);
                    we weVar2 = we.this;
                    if (weVar2.f == -1) {
                        weVar2.f = bfVar.b + bfVar.d + (list.size() / 2);
                    }
                } else {
                    we weVar3 = we.this;
                    boolean z2 = weVar3.f > weVar3.e.f();
                    we weVar4 = we.this;
                    boolean z3 = weVar4.y && weVar4.e.b(weVar4.d.d, weVar4.h, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            we weVar5 = we.this;
                            weVar5.e.a(list, (ef.a) weVar5);
                        } else {
                            we weVar6 = we.this;
                            weVar6.w = 0;
                            weVar6.t.b(cf.l.END, cf.i.IDLE, (Throwable) null);
                        }
                    } else if (i != 2) {
                        throw new IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        we weVar7 = we.this;
                        weVar7.e.b(list, (ef.a) weVar7);
                    } else {
                        we weVar8 = we.this;
                        weVar8.v = 0;
                        weVar8.t.b(cf.l.START, cf.i.IDLE, (Throwable) null);
                    }
                    we weVar9 = we.this;
                    if (weVar9.y) {
                        if (z2) {
                            if (weVar9.t.e() != cf.i.LOADING) {
                                we weVar10 = we.this;
                                if (weVar10.e.b(weVar10.x, weVar10.d.d, weVar10.h, weVar10)) {
                                    we.this.t.b(cf.l.START, cf.i.IDLE, (Throwable) null);
                                }
                            }
                        } else if (weVar9.t.a() != cf.i.LOADING) {
                            we weVar11 = we.this;
                            if (weVar11.e.a(weVar11.x, weVar11.d.d, weVar11.h, (ef.a) weVar11)) {
                                we.this.t.b(cf.l.END, cf.i.IDLE, (Throwable) null);
                            }
                        }
                    }
                }
                we weVar12 = we.this;
                if (weVar12.c != null) {
                    boolean z4 = weVar12.e.size() == 0;
                    boolean z5 = !z4 && i == 2 && bfVar.a.size() == 0;
                    if (!(!z4 && i == 1 && bfVar.a.size() == 0)) {
                        z = false;
                    }
                    we.this.a(z4, z5, z);
                }
            }
        }

        @DexIgnore
        public void a(int i, Throwable th, boolean z) {
            cf.i iVar = z ? cf.i.RETRYABLE_ERROR : cf.i.ERROR;
            if (i == 2) {
                we.this.t.b(cf.l.START, iVar, th);
            } else if (i == 1) {
                we.this.t.b(cf.l.END, iVar, th);
            } else {
                throw new IllegalStateException("TODO");
            }
        }
    }
}
