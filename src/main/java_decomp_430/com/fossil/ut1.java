package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ut1 {
    @DexIgnore
    public static int b; // = 31;
    @DexIgnore
    public int a; // = 1;

    @DexIgnore
    public ut1 a(Object obj) {
        this.a = (b * this.a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    @DexIgnore
    public final ut1 a(boolean z) {
        this.a = (b * this.a) + (z ? 1 : 0);
        return this;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }
}
