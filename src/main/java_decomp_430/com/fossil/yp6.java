package com.fossil;

import com.fossil.jo6;
import com.fossil.lk6;
import com.fossil.mc6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp6 implements xp6, vp6<Object, xp6> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(yp6.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends b {
        @DexIgnore
        public /* final */ lk6<cd6> e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Object obj, lk6<? super cd6> lk6) {
            super(obj);
            wg6.b(lk6, "cont");
            this.e = lk6;
        }

        @DexIgnore
        public void a(Object obj) {
            wg6.b(obj, "token");
            this.e.b(obj);
        }

        @DexIgnore
        public Object m() {
            return lk6.a.a(this.e, cd6.a, (Object) null, 2, (Object) null);
        }

        @DexIgnore
        public String toString() {
            return "LockCont[" + this.d + ", " + this.e + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends jo6 implements am6 {
        @DexIgnore
        public /* final */ Object d;

        @DexIgnore
        public b(Object obj) {
            this.d = obj;
        }

        @DexIgnore
        public abstract void a(Object obj);

        @DexIgnore
        public final void dispose() {
            j();
        }

        @DexIgnore
        public abstract Object m();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ho6 {
        @DexIgnore
        public Object d;

        @DexIgnore
        public c(Object obj) {
            wg6.b(obj, "owner");
            this.d = obj;
        }

        @DexIgnore
        public String toString() {
            return "LockedQueue[" + this.d + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends po6 {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public d(c cVar) {
            wg6.b(cVar, "queue");
            this.a = cVar;
        }

        @DexIgnore
        public Object a(Object obj) {
            Object b = this.a.m() ? zp6.e : this.a;
            if (obj != null) {
                yp6 yp6 = (yp6) obj;
                yp6.a.compareAndSet(yp6, this, b);
                if (yp6._state == this.a) {
                    return zp6.a;
                }
                return null;
            }
            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends jo6.a {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ yp6 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(jo6 jo6, jo6 jo62, Object obj, lk6 lk6, a aVar, yp6 yp6, Object obj2) {
            super(jo62);
            this.d = obj;
            this.e = yp6;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(jo6 jo6) {
            wg6.b(jo6, "affected");
            if (this.e._state == this.d) {
                return null;
            }
            return io6.a();
        }
    }

    @DexIgnore
    public yp6(boolean z) {
        this._state = z ? zp6.d : zp6.e;
    }

    @DexIgnore
    public Object a(Object obj, xe6<? super cd6> xe6) {
        if (b(obj)) {
            return cd6.a;
        }
        return b(obj, xe6);
    }

    @DexIgnore
    public boolean b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof wp6) {
                if (((wp6) obj2).a != zp6.c) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? zp6.d : new wp6(obj))) {
                    return true;
                }
            } else if (obj2 instanceof c) {
                if (((c) obj2).d == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof po6) {
                ((po6) obj2).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof wp6) {
                return "Mutex[" + ((wp6) obj).a + ']';
            } else if (obj instanceof po6) {
                ((po6) obj).a(this);
            } else if (obj instanceof c) {
                return "Mutex[" + ((c) obj).d + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }

    @DexIgnore
    public void a(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof wp6) {
                if (obj == null) {
                    if (((wp6) obj2).a == zp6.c) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    wp6 wp6 = (wp6) obj2;
                    if (wp6.a != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + wp6.a + " but expected " + obj).toString());
                    }
                }
                if (a.compareAndSet(this, obj2, zp6.e)) {
                    return;
                }
            } else if (obj2 instanceof po6) {
                ((po6) obj2).a(this);
            } else if (obj2 instanceof c) {
                if (obj != null) {
                    c cVar = (c) obj2;
                    if (cVar.d != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + cVar.d + " but expected " + obj).toString());
                    }
                }
                c cVar2 = (c) obj2;
                jo6 k = cVar2.k();
                if (k == null) {
                    d dVar = new d(cVar2);
                    if (a.compareAndSet(this, obj2, dVar) && dVar.a(this) == null) {
                        return;
                    }
                } else {
                    b bVar = (b) k;
                    Object m = bVar.m();
                    if (m != null) {
                        Object obj3 = bVar.d;
                        if (obj3 == null) {
                            obj3 = zp6.b;
                        }
                        cVar2.d = obj3;
                        bVar.a(m);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object b(Object obj, xe6<? super cd6> xe6) {
        Object obj2 = obj;
        mk6 mk6 = new mk6(ef6.a(xe6), 0);
        a aVar = new a(obj2, mk6);
        while (true) {
            Object obj3 = this._state;
            if (obj3 instanceof wp6) {
                wp6 wp6 = (wp6) obj3;
                if (wp6.a != zp6.c) {
                    a.compareAndSet(this, obj3, new c(wp6.a));
                } else {
                    if (a.compareAndSet(this, obj3, obj2 == null ? zp6.d : new wp6(obj2))) {
                        cd6 cd6 = cd6.a;
                        mc6.a aVar2 = mc6.Companion;
                        mk6.resumeWith(mc6.m1constructorimpl(cd6));
                        break;
                    }
                }
            } else if (obj3 instanceof c) {
                c cVar = (c) obj3;
                boolean z = true;
                if (cVar.d != obj2) {
                    e eVar = new e(aVar, aVar, obj3, mk6, aVar, this, obj);
                    while (true) {
                        Object e2 = cVar.e();
                        if (e2 != null) {
                            int a2 = ((jo6) e2).a(aVar, cVar, eVar);
                            if (a2 != 1) {
                                if (a2 == 2) {
                                    z = false;
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    }
                    if (z) {
                        nk6.a((lk6<?>) mk6, (jo6) aVar);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj2).toString());
                }
            } else if (obj3 instanceof po6) {
                ((po6) obj3).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj3).toString());
            }
        }
        Object e3 = mk6.e();
        if (e3 == ff6.a()) {
            nf6.c(xe6);
        }
        return e3;
    }
}
