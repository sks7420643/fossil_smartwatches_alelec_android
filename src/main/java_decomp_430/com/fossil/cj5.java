package com.fossil;

import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj5 implements Factory<bj5> {
    @DexIgnore
    public static SleepDetailPresenter a(yi5 yi5, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        return new SleepDetailPresenter(yi5, sleepSummariesRepository, sleepSessionsRepository);
    }
}
