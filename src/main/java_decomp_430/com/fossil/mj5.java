package com.fossil;

import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj5 {
    @DexIgnore
    public /* final */ Date a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public mj5(Date date, long j) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        this.a = date;
        this.b = j;
    }

    @DexIgnore
    public final Date a() {
        return this.a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof mj5)) {
            return false;
        }
        mj5 mj5 = (mj5) obj;
        return wg6.a((Object) this.a, (Object) mj5.a) && this.b == mj5.b;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.a;
        return ((date != null ? date.hashCode() : 0) * 31) + e.a(this.b);
    }

    @DexIgnore
    public String toString() {
        return "ActivityDailyBest(date=" + this.a + ", value=" + this.b + ")";
    }
}
