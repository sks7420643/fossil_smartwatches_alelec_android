package com.fossil;

import com.fossil.a10;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a10<T extends a10> {
    @DexIgnore
    public /* final */ b10 a; // = new b10(20, 100, c86.h());
    @DexIgnore
    public /* final */ z00 b; // = new z00(this.a);

    @DexIgnore
    public Map<String, Object> a() {
        return this.b.b;
    }

    @DexIgnore
    public T a(String str, String str2) {
        this.b.a(str, str2);
        return this;
    }

    @DexIgnore
    public T a(String str, Number number) {
        this.b.a(str, number);
        return this;
    }
}
