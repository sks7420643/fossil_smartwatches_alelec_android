package com.fossil;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl3<E> extends ql3<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Queue<E> delegate;
    @DexIgnore
    public /* final */ int maxSize;

    @DexIgnore
    public jl3(int i) {
        jk3.a(i >= 0, "maxSize (%s) must >= 0", i);
        this.delegate = new ArrayDeque(i);
        this.maxSize = i;
    }

    @DexIgnore
    public static <E> jl3<E> create(int i) {
        return new jl3<>(i);
    }

    @DexIgnore
    public boolean add(E e) {
        jk3.a(e);
        if (this.maxSize == 0) {
            return true;
        }
        if (size() == this.maxSize) {
            this.delegate.remove();
        }
        this.delegate.add(e);
        return true;
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        int size = collection.size();
        if (size < this.maxSize) {
            return standardAddAll(collection);
        }
        clear();
        return pm3.a(this, pm3.a(collection, size - this.maxSize));
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Queue delegate2 = delegate();
        jk3.a(obj);
        return delegate2.contains(obj);
    }

    @DexIgnore
    public boolean offer(E e) {
        return add(e);
    }

    @DexIgnore
    public int remainingCapacity() {
        return this.maxSize - size();
    }

    @DexIgnore
    public boolean remove(Object obj) {
        Queue delegate2 = delegate();
        jk3.a(obj);
        return delegate2.remove(obj);
    }

    @DexIgnore
    public Queue<E> delegate() {
        return this.delegate;
    }
}
