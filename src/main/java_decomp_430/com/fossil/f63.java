package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f63 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ab3 a;
    @DexIgnore
    public /* final */ /* synthetic */ d63 b;

    @DexIgnore
    public f63(d63 d63, ab3 ab3) {
        this.b = d63;
        this.a = ab3;
    }

    @DexIgnore
    public final void run() {
        this.b.a.t();
        if (this.a.c.zza() == null) {
            this.b.a.b(this.a);
        } else {
            this.b.a.a(this.a);
        }
    }
}
