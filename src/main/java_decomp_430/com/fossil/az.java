package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class az {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ sr<T> b;

        @DexIgnore
        public a(Class<T> cls, sr<T> srVar) {
            this.a = cls;
            this.b = srVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <T> sr<T> a(Class<T> cls) {
        for (a next : this.a) {
            if (next.a(cls)) {
                return next.b;
            }
        }
        return null;
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, sr<T> srVar) {
        this.a.add(new a(cls, srVar));
    }
}
