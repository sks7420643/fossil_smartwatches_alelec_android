package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b36 {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(Intent intent, c36 c36);

    @DexIgnore
    boolean a(m26 m26);

    @DexIgnore
    boolean a(String str);
}
