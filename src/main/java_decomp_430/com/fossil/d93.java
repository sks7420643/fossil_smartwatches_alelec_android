package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.c12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d93 implements ServiceConnection, c12.a, c12.b {
    @DexIgnore
    public volatile boolean a;
    @DexIgnore
    public volatile u43 b;
    @DexIgnore
    public /* final */ /* synthetic */ l83 c;

    @DexIgnore
    public d93(l83 l83) {
        this.c = l83;
    }

    @DexIgnore
    public final void a(Intent intent) {
        this.c.g();
        Context c2 = this.c.c();
        b42 a2 = b42.a();
        synchronized (this) {
            if (this.a) {
                this.c.b().B().a("Connection attempt already in progress");
                return;
            }
            this.c.b().B().a("Using local app measurement service");
            this.a = true;
            a2.a(c2, intent, this.c.c, 129);
        }
    }

    @DexIgnore
    public final void b() {
        this.c.g();
        Context c2 = this.c.c();
        synchronized (this) {
            if (this.a) {
                this.c.b().B().a("Connection attempt already in progress");
            } else if (this.b == null || (!this.b.f() && !this.b.c())) {
                this.b = new u43(c2, Looper.getMainLooper(), this, this);
                this.c.b().B().a("Connecting to remote service");
                this.a = true;
                this.b.p();
            } else {
                this.c.b().B().a("Already awaiting connection attempt");
            }
        }
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        w12.a("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.a().a((Runnable) new e93(this, (l43) this.b.y()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.b = null;
                this.a = false;
            }
        }
    }

    @DexIgnore
    public final void g(int i) {
        w12.a("MeasurementServiceConnection.onConnectionSuspended");
        this.c.b().A().a("Service connection suspended");
        this.c.a().a((Runnable) new h93(this));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.c.b().t().a("Service connect failed to get IMeasurementService");
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0062 */
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        l43 l43;
        w12.a("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.a = false;
                this.c.b().t().a("Service connected with null binder");
                return;
            }
            l43 l432 = null;
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                if (iBinder != null) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (queryLocalInterface instanceof l43) {
                        l43 = (l43) queryLocalInterface;
                    } else {
                        l43 = new n43(iBinder);
                    }
                    l432 = l43;
                }
                this.c.b().B().a("Bound to IMeasurementService interface");
            } else {
                this.c.b().t().a("Got binder with a wrong descriptor", interfaceDescriptor);
            }
            if (l432 == null) {
                this.a = false;
                try {
                    b42.a().a(this.c.c(), this.c.c);
                } catch (IllegalArgumentException unused) {
                }
            } else {
                this.c.a().a((Runnable) new c93(this, l432));
            }
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        w12.a("MeasurementServiceConnection.onServiceDisconnected");
        this.c.b().A().a("Service disconnected");
        this.c.a().a((Runnable) new f93(this, componentName));
    }

    @DexIgnore
    public final void a() {
        if (this.b != null && (this.b.c() || this.b.f())) {
            this.b.a();
        }
        this.b = null;
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        w12.a("MeasurementServiceConnection.onConnectionFailed");
        t43 r = this.c.a.r();
        if (r != null) {
            r.w().a("Service connection failed", gv1);
        }
        synchronized (this) {
            this.a = false;
            this.b = null;
        }
        this.c.a().a((Runnable) new g93(this));
    }
}
