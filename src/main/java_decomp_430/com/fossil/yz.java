package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yz<R> extends ny {
    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    void a(jz jzVar);

    @DexIgnore
    void a(xz xzVar);

    @DexIgnore
    void a(R r, b00<? super R> b00);

    @DexIgnore
    void b(Drawable drawable);

    @DexIgnore
    void b(xz xzVar);

    @DexIgnore
    void c(Drawable drawable);

    @DexIgnore
    jz d();
}
