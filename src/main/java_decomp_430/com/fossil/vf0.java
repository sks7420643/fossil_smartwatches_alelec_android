package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vf0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ al0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vf0(al0 al0) {
        super(1);
        this.a = al0;
    }

    @DexIgnore
    public final void a(qv0 qv0) {
        n41 n41 = (n41) qv0;
        List g = nd6.g(n41.A);
        List g2 = nd6.g(n41.B);
        String a2 = yd6.a(g, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, yn1.a, 31, (Object) null);
        String a3 = yd6.a(g2, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, im1.a, 31, (Object) null);
        al0 al0 = this.a;
        ue1 ue1 = al0.w;
        cc0 cc0 = cc0.DEBUG;
        String str = al0.a;
        new Object[1][0] = a2;
        new Object[1][0] = a3;
        al0.G.clear();
        CopyOnWriteArrayList<rg1> copyOnWriteArrayList = al0.G;
        ArrayList arrayList = new ArrayList();
        for (Object next : g2) {
            if (al0.P.contains((rg1) next)) {
                arrayList.add(next);
            }
        }
        copyOnWriteArrayList.addAll(arrayList);
        this.a.p();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((qv0) obj);
        return cd6.a;
    }
}
