package com.fossil;

import com.fossil.ji6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ki6<R> extends mi6<R>, ji6<R> {

    @DexIgnore
    public interface a<R> extends ji6.a<R>, hg6<R, cd6> {
    }

    @DexIgnore
    a<R> getSetter();
}
