package com.fossil;

import com.fossil.af6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we6 implements af6, Serializable {
    @DexIgnore
    public /* final */ af6.b element;
    @DexIgnore
    public /* final */ af6 left;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Serializable {
        @DexIgnore
        public static /* final */ C0050a Companion; // = new C0050a((qg6) null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ af6[] elements;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.we6$a$a")
        /* renamed from: com.fossil.we6$a$a  reason: collision with other inner class name */
        public static final class C0050a {
            @DexIgnore
            public C0050a() {
            }

            @DexIgnore
            public /* synthetic */ C0050a(qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public a(af6[] af6Arr) {
            wg6.b(af6Arr, "elements");
            this.elements = af6Arr;
        }

        @DexIgnore
        private final Object readResolve() {
            af6[] af6Arr = this.elements;
            af6 af6 = bf6.INSTANCE;
            for (af6 plus : af6Arr) {
                af6 = af6.plus(plus);
            }
            return af6;
        }

        @DexIgnore
        public final af6[] getElements() {
            return this.elements;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements ig6<String, af6.b, String> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final String invoke(String str, af6.b bVar) {
            wg6.b(str, "acc");
            wg6.b(bVar, "element");
            if (str.length() == 0) {
                return bVar.toString();
            }
            return str + ", " + bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements ig6<cd6, af6.b, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ af6[] $elements;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $index;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(af6[] af6Arr, hh6 hh6) {
            super(2);
            this.$elements = af6Arr;
            this.$index = hh6;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
            invoke((cd6) obj, (af6.b) obj2);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(cd6 cd6, af6.b bVar) {
            wg6.b(cd6, "<anonymous parameter 0>");
            wg6.b(bVar, "element");
            af6[] af6Arr = this.$elements;
            hh6 hh6 = this.$index;
            int i = hh6.element;
            hh6.element = i + 1;
            af6Arr[i] = bVar;
        }
    }

    @DexIgnore
    public we6(af6 af6, af6.b bVar) {
        wg6.b(af6, "left");
        wg6.b(bVar, "element");
        this.left = af6;
        this.element = bVar;
    }

    @DexIgnore
    private final Object writeReplace() {
        int a2 = a();
        af6[] af6Arr = new af6[a2];
        hh6 hh6 = new hh6();
        boolean z = false;
        hh6.element = 0;
        fold(cd6.a, new c(af6Arr, hh6));
        if (hh6.element == a2) {
            z = true;
        }
        if (z) {
            return new a(af6Arr);
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final int a() {
        int i = 2;
        we6 we6 = this;
        while (true) {
            af6 af6 = we6.left;
            if (!(af6 instanceof we6)) {
                af6 = null;
            }
            we6 = (we6) af6;
            if (we6 == null) {
                return i;
            }
            i++;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof we6) {
                we6 we6 = (we6) obj;
                if (we6.a() != a() || !we6.a(this)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public <R> R fold(R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
        wg6.b(ig6, "operation");
        return ig6.invoke(this.left.fold(r, ig6), this.element);
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        we6 we6 = this;
        while (true) {
            E e = we6.element.get(cVar);
            if (e != null) {
                return e;
            }
            af6 af6 = we6.left;
            if (!(af6 instanceof we6)) {
                return af6.get(cVar);
            }
            we6 = (we6) af6;
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.left.hashCode() + this.element.hashCode();
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        if (this.element.get(cVar) != null) {
            return this.left;
        }
        af6 minusKey = this.left.minusKey(cVar);
        if (minusKey == this.left) {
            return this;
        }
        if (minusKey == bf6.INSTANCE) {
            return this.element;
        }
        return new we6(minusKey, this.element);
    }

    @DexIgnore
    public af6 plus(af6 af6) {
        wg6.b(af6, "context");
        return af6.a.a(this, af6);
    }

    @DexIgnore
    public String toString() {
        return "[" + ((String) fold("", b.INSTANCE)) + "]";
    }

    @DexIgnore
    public final boolean a(af6.b bVar) {
        return wg6.a((Object) get(bVar.getKey()), (Object) bVar);
    }

    @DexIgnore
    public final boolean a(we6 we6) {
        while (a(we6.element)) {
            af6 af6 = we6.left;
            if (af6 instanceof we6) {
                we6 = (we6) af6;
            } else if (af6 != null) {
                return a((af6.b) af6);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
            }
        }
        return false;
    }
}
