package com.fossil;

import android.os.Build;
import android.os.CancellationSignal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z7 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public a b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onCancel();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r0.onCancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001a, code lost:
        if (r1 == null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0020, code lost:
        if (android.os.Build.VERSION.SDK_INT < 16) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0022, code lost:
        ((android.os.CancellationSignal) r1).cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0028, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.d = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0033, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r4.d = false;
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0039, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x003a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0 == null) goto L_0x001a;
     */
    @DexIgnore
    public void a() {
        synchronized (this) {
            if (!this.a) {
                this.a = true;
                this.d = true;
                a aVar = this.b;
                Object obj = this.c;
            }
        }
    }

    @DexIgnore
    public Object b() {
        Object obj;
        if (Build.VERSION.SDK_INT < 16) {
            return null;
        }
        synchronized (this) {
            if (this.c == null) {
                this.c = new CancellationSignal();
                if (this.a) {
                    ((CancellationSignal) this.c).cancel();
                }
            }
            obj = this.c;
        }
        return obj;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this) {
            z = this.a;
        }
        return z;
    }

    @DexIgnore
    public void d() {
        if (c()) {
            throw new f8();
        }
    }

    @DexIgnore
    public final void e() {
        while (this.d) {
            try {
                wait();
            } catch (InterruptedException unused) {
            }
        }
    }

    @DexIgnore
    public void a(a aVar) {
        synchronized (this) {
            e();
            if (this.b != aVar) {
                this.b = aVar;
                if (this.a) {
                    if (aVar != null) {
                        aVar.onCancel();
                    }
                }
            }
        }
    }
}
