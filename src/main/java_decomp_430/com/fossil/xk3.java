package com.fossil;

import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xk3<K, V> extends sk3<K, V> implements xn3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7431625294878419160L;

    @DexIgnore
    public xk3(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    public abstract Set<V> createCollection();

    @DexIgnore
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    public Set<V> createUnmodifiableEmptyCollection() {
        return im3.of();
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entries() {
        return (Set) super.entries();
    }

    @DexIgnore
    public Set<V> get(K k) {
        return (Set) super.get(k);
    }

    @DexIgnore
    public Set<V> removeAll(Object obj) {
        return (Set) super.removeAll(obj);
    }

    @DexIgnore
    public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (Set) super.replaceValues(k, iterable);
    }
}
