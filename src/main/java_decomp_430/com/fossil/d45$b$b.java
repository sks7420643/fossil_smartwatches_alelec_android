package com.fossil;

import android.os.Parcelable;
import com.fossil.DianaCustomizeEditPresenter;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d45$b$b extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaPresetWatchAppSetting $watchApp;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeEditPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d45$b$b(DianaCustomizeEditPresenter.b bVar, DianaPresetWatchAppSetting dianaPresetWatchAppSetting, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$watchApp = dianaPresetWatchAppSetting;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        d45$b$b d45_b_b = new d45$b$b(this.this$0, this.$watchApp, xe6);
        d45_b_b.p$ = (il6) obj;
        return d45_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((d45$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return DianaCustomizeEditPresenter.g(this.this$0.this$0).g(this.$watchApp.getId());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
