package com.fossil;

import android.content.Intent;
import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr4 extends m24<b, d, c> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ e e; // = new e();
    @DexIgnore
    public /* final */ NotificationsRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ List<wx4> a;
        @DexIgnore
        public /* final */ List<vx4> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(List<wx4> list, List<vx4> list2, int i) {
            wg6.b(list, "contactWrapperList");
            wg6.b(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
            this.c = i;
        }

        @DexIgnore
        public final List<vx4> a() {
            return this.b;
        }

        @DexIgnore
        public final List<wx4> b() {
            return this.a;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v10, types: [com.fossil.tr4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r5v12, types: [com.fossil.tr4, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetNotificationFiltersUserCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS && tr4.this.d()) {
                boolean z = false;
                tr4.this.a(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    tr4.this.a(new d());
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                tr4.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public tr4(NotificationsRepository notificationsRepository, DeviceRepository deviceRepository) {
        wg6.b(notificationsRepository, "mNotificationsRepository");
        wg6.b(deviceRepository, "mDeviceRepository");
        this.f = notificationsRepository;
        this.g = deviceRepository;
    }

    @DexIgnore
    public final List<AppNotificationFilter> b(List<wx4> list, short s, boolean z) {
        Contact contact;
        ArrayList arrayList = new ArrayList();
        for (wx4 next : list) {
            short s2 = -1;
            if (next.isAdded() && (contact = next.getContact()) != null) {
                if (contact.isUseCall()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                    }
                    NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                    appNotificationFilter.setSender(contact.getDisplayName());
                    appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                    appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                    arrayList.add(appNotificationFilter);
                }
                if (contact.isUseSms()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                    }
                    NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                    appNotificationFilter2.setSender(contact.getDisplayName());
                    appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                    appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public String c() {
        return "SetNotificationFiltersUserCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.e, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v0, types: [com.fossil.tr4, com.portfolio.platform.CoroutineUseCase] */
    public Object a(b bVar, xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "running UseCase");
            this.d = true;
            Integer num = null;
            List<wx4> b2 = bVar != null ? bVar.b() : null;
            List<vx4> a2 = bVar != null ? bVar.a() : null;
            if (bVar != null) {
                num = hf6.a(bVar.c());
            }
            if (b2 == null || a2 == null || num == null) {
                a(new c(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG, -1, new ArrayList()));
            } else {
                String e2 = PortfolioApp.get.instance().e();
                boolean a3 = NotificationAppHelper.b.a(this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(e2)), e2);
                short c2 = (short) al4.c(num.intValue());
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(a(this.f.getAllNotificationsByHour(e2, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), b2, a2, c2, a3));
                arrayList.addAll(b(b2, c2, a3));
                arrayList.addAll(a(a2, c2, a3));
                AppNotificationFilterSettings appNotificationFilterSettings = new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
                PortfolioApp.get.instance().a(appNotificationFilterSettings, e2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetNotificationFiltersUserCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
            }
            return new Object();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SetNotificationFiltersUserCase", "Error inside SetNotificationFiltersUserCase.connectDevice - e=" + e3);
            return new c(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0209 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0085 A[EDGE_INSN: B:111:0x0085->B:27:0x0085 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0191 A[EDGE_INSN: B:116:0x0191->B:73:0x0191 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01c5  */
    public final List<AppNotificationFilter> a(SparseArray<List<BaseFeatureModel>> sparseArray, List<wx4> list, List<vx4> list2, short s, boolean z) {
        int i;
        T t;
        boolean z2;
        short s2;
        String str;
        boolean z3;
        T t2;
        int i2;
        boolean z4;
        Contact contact;
        SparseArray<List<BaseFeatureModel>> sparseArray2 = sparseArray;
        ArrayList arrayList = new ArrayList();
        if (sparseArray2 != null) {
            int size = sparseArray.size();
            int i3 = 0;
            short s3 = -1;
            while (i3 < size) {
                Iterator it = sparseArray2.valueAt(i3).iterator();
                while (it.hasNext()) {
                    ContactGroup contactGroup = (BaseFeatureModel) it.next();
                    if (contactGroup.isEnabled()) {
                        int i4 = 10000;
                        if (contactGroup instanceof ContactGroup) {
                            ContactGroup contactGroup2 = contactGroup;
                            short s4 = s3;
                            boolean z5 = false;
                            short s5 = -1;
                            for (Contact contact2 : contactGroup2.getContacts()) {
                                Iterator<T> it2 = list.iterator();
                                while (true) {
                                    if (!it2.hasNext()) {
                                        t2 = null;
                                        break;
                                    }
                                    t2 = it2.next();
                                    wx4 wx4 = (wx4) t2;
                                    if (!(wx4.getContact() == null || (contact = wx4.getContact()) == null)) {
                                        int contactId = contact.getContactId();
                                        wg6.a((Object) contact2, "existedContact");
                                        if (contactId == contact2.getContactId()) {
                                            z4 = true;
                                            continue;
                                            if (z4) {
                                                break;
                                            }
                                        }
                                    }
                                    z4 = false;
                                    continue;
                                    if (z4) {
                                    }
                                }
                                wx4 wx42 = (wx4) t2;
                                if (wx42 == null || !wx42.isAdded()) {
                                    if (wx42 == null) {
                                        s5 = (short) al4.c(contactGroup2.getHour());
                                    }
                                    if (z5) {
                                        wg6.a((Object) contact2, "existedContact");
                                        if (contact2.isUseCall()) {
                                            if (z) {
                                                s4 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s5, s5, s4, i4);
                                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                                            appNotificationFilter.setSender(contact2.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                                            appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (contact2.isUseSms()) {
                                            if (z) {
                                                s4 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(s5, s5, s4, 10000);
                                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                            i2 = size;
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                                            appNotificationFilter2.setSender(contact2.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                                            appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                            SparseArray<List<BaseFeatureModel>> sparseArray3 = sparseArray;
                                            size = i2;
                                            i4 = 10000;
                                        }
                                    }
                                    i2 = size;
                                    SparseArray<List<BaseFeatureModel>> sparseArray32 = sparseArray;
                                    size = i2;
                                    i4 = 10000;
                                } else {
                                    s5 = s;
                                }
                                z5 = true;
                                if (z5) {
                                }
                                i2 = size;
                                SparseArray<List<BaseFeatureModel>> sparseArray322 = sparseArray;
                                size = i2;
                                i4 = 10000;
                            }
                            i = size;
                            s3 = s4;
                        } else {
                            i = size;
                            if (!(contactGroup instanceof AppFilter)) {
                                continue;
                            } else {
                                AppFilter appFilter = (AppFilter) contactGroup;
                                String type = appFilter.getType();
                                if (!(type == null || xj6.a(type))) {
                                    Iterator<T> it3 = list2.iterator();
                                    while (true) {
                                        if (!it3.hasNext()) {
                                            t = null;
                                            break;
                                        }
                                        t = it3.next();
                                        AppWrapper appWrapper = (AppWrapper) t;
                                        if (appWrapper.getInstalledApp() != null) {
                                            InstalledApp installedApp = appWrapper.getInstalledApp();
                                            if (installedApp != null) {
                                                if (wg6.a((Object) installedApp.getIdentifier(), (Object) appFilter.getType())) {
                                                    z3 = true;
                                                    continue;
                                                    if (z3) {
                                                        break;
                                                    }
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                        z3 = false;
                                        continue;
                                        if (z3) {
                                        }
                                    }
                                    AppWrapper appWrapper2 = (AppWrapper) t;
                                    if (appWrapper2 != null) {
                                        InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                                        if (installedApp2 != null) {
                                            Boolean isSelected = installedApp2.isSelected();
                                            wg6.a((Object) isSelected, "app.installedApp!!.isSelected");
                                            if (isSelected.booleanValue()) {
                                                s2 = s;
                                                z2 = true;
                                                if (z2) {
                                                    if (z) {
                                                        s3 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                                    }
                                                    if (appFilter.getName() == null) {
                                                        str = "";
                                                    } else {
                                                        str = appFilter.getName();
                                                    }
                                                    NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(s2, s2, s3, 10000);
                                                    wg6.a((Object) str, "appName");
                                                    String type2 = appFilter.getType();
                                                    wg6.a((Object) type2, "item.type");
                                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(str, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                                    appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
                                                    appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                                                    arrayList.add(appNotificationFilter3);
                                                }
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    }
                                    if (appWrapper2 == null) {
                                        s2 = (short) al4.c(appFilter.getHour());
                                        z2 = true;
                                        if (z2) {
                                        }
                                    } else {
                                        s2 = -1;
                                        z2 = false;
                                        if (z2) {
                                        }
                                    }
                                } else {
                                    continue;
                                }
                            }
                        }
                    } else {
                        i = size;
                    }
                    SparseArray<List<BaseFeatureModel>> sparseArray4 = sparseArray;
                    size = i;
                }
                int i5 = size;
                i3++;
                sparseArray2 = sparseArray;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(List<vx4> list, short s, boolean z) {
        short s2;
        ArrayList arrayList = new ArrayList();
        if (!z) {
            s2 = -1;
        } else {
            s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        }
        Iterator<vx4> it = list.iterator();
        while (it.hasNext()) {
            InstalledApp installedApp = it.next().getInstalledApp();
            if (installedApp != null) {
                Boolean isSelected = installedApp.isSelected();
                wg6.a((Object) isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    String title = installedApp.getTitle() == null ? "" : installedApp.getTitle();
                    NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s, s, s2, 10000);
                    wg6.a((Object) title, "appName");
                    String identifier = installedApp.getIdentifier();
                    wg6.a((Object) identifier, "it.identifier");
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(title, identifier, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                    appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                    appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        return arrayList;
    }
}
