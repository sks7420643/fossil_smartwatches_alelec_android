package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn1 extends xg6 implements hg6<UUID, String> {
    @DexIgnore
    public static /* final */ yn1 a; // = new yn1();

    @DexIgnore
    public yn1() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        String uuid = ((UUID) obj).toString();
        wg6.a(uuid, "it.toString()");
        return uuid;
    }
}
