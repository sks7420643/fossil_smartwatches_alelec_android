package com.fossil;

import android.util.Log;
import com.fossil.qw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ gv1 a;
    @DexIgnore
    public /* final */ /* synthetic */ qw1.b b;

    @DexIgnore
    public ty1(qw1.b bVar, gv1 gv1) {
        this.b = bVar;
        this.a = gv1;
    }

    @DexIgnore
    public final void run() {
        qw1.a aVar = (qw1.a) qw1.this.i.get(this.b.b);
        if (aVar != null) {
            if (this.a.E()) {
                boolean unused = this.b.e = true;
                if (this.b.a.m()) {
                    this.b.a();
                    return;
                }
                try {
                    this.b.a.a((n12) null, this.b.a.e());
                } catch (SecurityException e) {
                    Log.e("GoogleApiManager", "Failed to get service from broker. ", e);
                    aVar.a(new gv1(10));
                }
            } else {
                aVar.a(this.a);
            }
        }
    }
}
