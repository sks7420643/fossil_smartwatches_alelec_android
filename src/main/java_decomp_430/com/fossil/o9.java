package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface o9 extends q9 {
    @DexIgnore
    void a(View view, int i);

    @DexIgnore
    void a(View view, int i, int i2, int i3, int i4, int i5);

    @DexIgnore
    void a(View view, int i, int i2, int[] iArr, int i3);

    @DexIgnore
    boolean a(View view, View view2, int i, int i2);

    @DexIgnore
    void b(View view, View view2, int i, int i2);
}
