package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.manager.validation.DataValidationManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn4 implements Factory<mn4> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> c;

    @DexIgnore
    public nn4(Provider<DianaPresetRepository> provider, Provider<an4> provider2, Provider<WatchFaceRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static nn4 a(Provider<DianaPresetRepository> provider, Provider<an4> provider2, Provider<WatchFaceRepository> provider3) {
        return new nn4(provider, provider2, provider3);
    }

    @DexIgnore
    public static mn4 b(Provider<DianaPresetRepository> provider, Provider<an4> provider2, Provider<WatchFaceRepository> provider3) {
        return new DataValidationManager(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public DataValidationManager get() {
        return b(this.a, this.b, this.c);
    }
}
