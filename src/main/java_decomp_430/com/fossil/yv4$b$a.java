package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$1", f = "DeleteAlarm.kt", l = {39}, m = "invokeSuspend")
public final class yv4$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $requestAlarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAlarm.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yv4$b$a(DeleteAlarm.b bVar, Alarm alarm, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$requestAlarm = alarm;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        yv4$b$a yv4_b_a = new yv4$b$a(this.this$0, this.$requestAlarm, xe6);
        yv4_b_a.p$ = (il6) obj;
        return yv4_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((yv4$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            AlarmsRepository a2 = DeleteAlarm.this.g;
            Alarm alarm = this.$requestAlarm;
            this.L$0 = il6;
            this.label = 1;
            if (a2.deleteAlarm(alarm, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
