package com.fossil;

import android.content.Context;
import com.fossil.nu;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu extends nu {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements nu.a {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        @DexIgnore
        public File a() {
            File cacheDir = this.a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            String str = this.b;
            return str != null ? new File(cacheDir, str) : cacheDir;
        }
    }

    @DexIgnore
    public pu(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public pu(Context context, String str, long j) {
        super(new a(context, str), j);
    }
}
