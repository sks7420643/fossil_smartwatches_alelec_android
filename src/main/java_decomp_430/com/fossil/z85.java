package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z85 implements Factory<y85> {
    @DexIgnore
    public static HomeHybridCustomizePresenter a(PortfolioApp portfolioApp, x85 x85, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, an4 an4) {
        return new HomeHybridCustomizePresenter(portfolioApp, x85, microAppRepository, hybridPresetRepository, setHybridPresetToWatchUseCase, an4);
    }
}
