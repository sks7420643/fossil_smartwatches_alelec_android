package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xf2 extends e22 implements ew1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xf2> CREATOR; // = new yf2();
    @DexIgnore
    public /* final */ Status a;

    /*
    static {
        new xf2(Status.e);
    }
    */

    @DexIgnore
    public xf2(Status status) {
        this.a = status;
    }

    @DexIgnore
    public final Status o() {
        return this.a;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) o(), i, false);
        g22.a(parcel, a2);
    }
}
