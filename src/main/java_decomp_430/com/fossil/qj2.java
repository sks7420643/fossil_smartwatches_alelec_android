package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fn2;
import com.fossil.mj2;
import com.fossil.uj2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj2 extends fn2<qj2, a> implements to2 {
    @DexIgnore
    public static /* final */ qj2 zzav;
    @DexIgnore
    public static volatile yo2<qj2> zzaw;
    @DexIgnore
    public int zzaa;
    @DexIgnore
    public String zzab; // = "";
    @DexIgnore
    public String zzac; // = "";
    @DexIgnore
    public boolean zzad;
    @DexIgnore
    public nn2<kj2> zzae; // = fn2.m();
    @DexIgnore
    public String zzaf; // = "";
    @DexIgnore
    public int zzag;
    @DexIgnore
    public int zzah;
    @DexIgnore
    public int zzai;
    @DexIgnore
    public String zzaj; // = "";
    @DexIgnore
    public long zzak;
    @DexIgnore
    public long zzal;
    @DexIgnore
    public String zzam; // = "";
    @DexIgnore
    public String zzan; // = "";
    @DexIgnore
    public int zzao;
    @DexIgnore
    public String zzap; // = "";
    @DexIgnore
    public rj2 zzaq;
    @DexIgnore
    public ln2 zzar; // = fn2.k();
    @DexIgnore
    public long zzas;
    @DexIgnore
    public long zzat;
    @DexIgnore
    public String zzau; // = "";
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public int zze;
    @DexIgnore
    public nn2<mj2> zzf; // = fn2.m();
    @DexIgnore
    public nn2<uj2> zzg; // = fn2.m();
    @DexIgnore
    public long zzh;
    @DexIgnore
    public long zzi;
    @DexIgnore
    public long zzj;
    @DexIgnore
    public long zzk;
    @DexIgnore
    public long zzl;
    @DexIgnore
    public String zzm; // = "";
    @DexIgnore
    public String zzn; // = "";
    @DexIgnore
    public String zzo; // = "";
    @DexIgnore
    public String zzp; // = "";
    @DexIgnore
    public int zzq;
    @DexIgnore
    public String zzr; // = "";
    @DexIgnore
    public String zzs; // = "";
    @DexIgnore
    public String zzt; // = "";
    @DexIgnore
    public long zzu;
    @DexIgnore
    public long zzv;
    @DexIgnore
    public String zzw; // = "";
    @DexIgnore
    public boolean zzx;
    @DexIgnore
    public String zzy; // = "";
    @DexIgnore
    public long zzz;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<qj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(qj2.zzav);
        }

        @DexIgnore
        public final a a(int i) {
            f();
            ((qj2) this.b).d(1);
            return this;
        }

        @DexIgnore
        public final mj2 b(int i) {
            return ((qj2) this.b).b(i);
        }

        @DexIgnore
        public final a c(int i) {
            f();
            ((qj2) this.b).e(i);
            return this;
        }

        @DexIgnore
        public final uj2 d(int i) {
            return ((qj2) this.b).c(i);
        }

        @DexIgnore
        public final a e(int i) {
            f();
            ((qj2) this.b).f(i);
            return this;
        }

        @DexIgnore
        public final a f(int i) {
            f();
            ((qj2) this.b).g(i);
            return this;
        }

        @DexIgnore
        public final a g(String str) {
            f();
            ((qj2) this.b).g(str);
            return this;
        }

        @DexIgnore
        public final a h(String str) {
            f();
            ((qj2) this.b).h(str);
            return this;
        }

        @DexIgnore
        public final a i(String str) {
            f();
            ((qj2) this.b).i(str);
            return this;
        }

        @DexIgnore
        public final List<mj2> j() {
            return Collections.unmodifiableList(((qj2) this.b).b0());
        }

        @DexIgnore
        public final int k() {
            return ((qj2) this.b).d0();
        }

        @DexIgnore
        public final a l() {
            f();
            ((qj2) this.b).V();
            return this;
        }

        @DexIgnore
        public final List<uj2> m() {
            return Collections.unmodifiableList(((qj2) this.b).e0());
        }

        @DexIgnore
        public final int n() {
            return ((qj2) this.b).f0();
        }

        @DexIgnore
        public final long o() {
            return ((qj2) this.b).j0();
        }

        @DexIgnore
        public final long p() {
            return ((qj2) this.b).l0();
        }

        @DexIgnore
        public final a q() {
            f();
            ((qj2) this.b).X();
            return this;
        }

        @DexIgnore
        public final a r() {
            f();
            ((qj2) this.b).Y();
            return this;
        }

        @DexIgnore
        public final String s() {
            return ((qj2) this.b).w0();
        }

        @DexIgnore
        public final a t() {
            f();
            ((qj2) this.b).Z();
            return this;
        }

        @DexIgnore
        public final String v() {
            return ((qj2) this.b).B();
        }

        @DexIgnore
        public final a w() {
            f();
            ((qj2) this.b).a0();
            return this;
        }

        @DexIgnore
        public final a x() {
            f();
            ((qj2) this.b).c0();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a b(long j) {
            f();
            ((qj2) this.b).b(j);
            return this;
        }

        @DexIgnore
        public final a d(long j) {
            f();
            ((qj2) this.b).d(j);
            return this;
        }

        @DexIgnore
        public final a k(String str) {
            f();
            ((qj2) this.b).k(str);
            return this;
        }

        @DexIgnore
        public final a n(String str) {
            f();
            ((qj2) this.b).n((String) null);
            return this;
        }

        @DexIgnore
        public final a o(String str) {
            f();
            ((qj2) this.b).o(str);
            return this;
        }

        @DexIgnore
        public final a a(int i, mj2.a aVar) {
            f();
            ((qj2) this.b).a(i, aVar);
            return this;
        }

        @DexIgnore
        public final a c(long j) {
            f();
            ((qj2) this.b).c(j);
            return this;
        }

        @DexIgnore
        public final a e(long j) {
            f();
            ((qj2) this.b).e(j);
            return this;
        }

        @DexIgnore
        public final a f(String str) {
            f();
            ((qj2) this.b).f(str);
            return this;
        }

        @DexIgnore
        public final a g(long j) {
            f();
            ((qj2) this.b).g(j);
            return this;
        }

        @DexIgnore
        public final a h(long j) {
            f();
            ((qj2) this.b).h(j);
            return this;
        }

        @DexIgnore
        public final a i(long j) {
            f();
            ((qj2) this.b).i(j);
            return this;
        }

        @DexIgnore
        public final a l(String str) {
            f();
            ((qj2) this.b).l(str);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            f();
            ((qj2) this.b).b(str);
            return this;
        }

        @DexIgnore
        public final a d(String str) {
            f();
            ((qj2) this.b).d(str);
            return this;
        }

        @DexIgnore
        public final a j(String str) {
            f();
            ((qj2) this.b).j(str);
            return this;
        }

        @DexIgnore
        public final a k(long j) {
            f();
            ((qj2) this.b).k(j);
            return this;
        }

        @DexIgnore
        public final a m(String str) {
            f();
            ((qj2) this.b).m(str);
            return this;
        }

        @DexIgnore
        public final a a(mj2.a aVar) {
            f();
            ((qj2) this.b).a(aVar);
            return this;
        }

        @DexIgnore
        public final a c(String str) {
            f();
            ((qj2) this.b).c(str);
            return this;
        }

        @DexIgnore
        public final a e(String str) {
            f();
            ((qj2) this.b).e(str);
            return this;
        }

        @DexIgnore
        public final a f(long j) {
            f();
            ((qj2) this.b).f(j);
            return this;
        }

        @DexIgnore
        public final a g(int i) {
            f();
            ((qj2) this.b).h(i);
            return this;
        }

        @DexIgnore
        public final a h(int i) {
            f();
            ((qj2) this.b).i(i);
            return this;
        }

        @DexIgnore
        public final a i(int i) {
            f();
            ((qj2) this.b).j(i);
            return this;
        }

        @DexIgnore
        public final a l(long j) {
            f();
            ((qj2) this.b).l(j);
            return this;
        }

        @DexIgnore
        public final a b(boolean z) {
            f();
            ((qj2) this.b).b(z);
            return this;
        }

        @DexIgnore
        public final a j(long j) {
            f();
            ((qj2) this.b).j(j);
            return this;
        }

        @DexIgnore
        public final a a(Iterable<? extends mj2> iterable) {
            f();
            ((qj2) this.b).a(iterable);
            return this;
        }

        @DexIgnore
        public final a c(Iterable<? extends Integer> iterable) {
            f();
            ((qj2) this.b).c(iterable);
            return this;
        }

        @DexIgnore
        public final a b(Iterable<? extends kj2> iterable) {
            f();
            ((qj2) this.b).b(iterable);
            return this;
        }

        @DexIgnore
        public final a a(int i, uj2 uj2) {
            f();
            ((qj2) this.b).a(i, uj2);
            return this;
        }

        @DexIgnore
        public final a a(uj2 uj2) {
            f();
            ((qj2) this.b).a(uj2);
            return this;
        }

        @DexIgnore
        public final a a(uj2.a aVar) {
            f();
            ((qj2) this.b).a(aVar);
            return this;
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((qj2) this.b).a(j);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((qj2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final a a(boolean z) {
            f();
            ((qj2) this.b).a(z);
            return this;
        }
    }

    /*
    static {
        qj2 qj2 = new qj2();
        zzav = qj2;
        fn2.a(qj2.class, qj2);
    }
    */

    @DexIgnore
    public static a z0() {
        return (a) zzav.h();
    }

    @DexIgnore
    public final String A() {
        return this.zzab;
    }

    @DexIgnore
    public final String B() {
        return this.zzac;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 8388608) != 0;
    }

    @DexIgnore
    public final boolean D() {
        return this.zzad;
    }

    @DexIgnore
    public final List<kj2> E() {
        return this.zzae;
    }

    @DexIgnore
    public final String F() {
        return this.zzaf;
    }

    @DexIgnore
    public final boolean G() {
        return (this.zzc & 33554432) != 0;
    }

    @DexIgnore
    public final int H() {
        return this.zzag;
    }

    @DexIgnore
    public final String I() {
        return this.zzaj;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 536870912) != 0;
    }

    @DexIgnore
    public final long K() {
        return this.zzak;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 1073741824) != 0;
    }

    @DexIgnore
    public final long M() {
        return this.zzal;
    }

    @DexIgnore
    public final String N() {
        return this.zzam;
    }

    @DexIgnore
    public final boolean O() {
        return (this.zzd & 2) != 0;
    }

    @DexIgnore
    public final int P() {
        return this.zze;
    }

    @DexIgnore
    public final int Q() {
        return this.zzao;
    }

    @DexIgnore
    public final String R() {
        return this.zzap;
    }

    @DexIgnore
    public final boolean S() {
        return (this.zzd & 16) != 0;
    }

    @DexIgnore
    public final long T() {
        return this.zzas;
    }

    @DexIgnore
    public final void U() {
        if (!this.zzf.zza()) {
            this.zzf = fn2.a(this.zzf);
        }
    }

    @DexIgnore
    public final void V() {
        this.zzf = fn2.m();
    }

    @DexIgnore
    public final void W() {
        if (!this.zzg.zza()) {
            this.zzg = fn2.a(this.zzg);
        }
    }

    @DexIgnore
    public final void X() {
        this.zzc &= -17;
        this.zzk = 0;
    }

    @DexIgnore
    public final void Y() {
        this.zzc &= -33;
        this.zzl = 0;
    }

    @DexIgnore
    public final void Z() {
        this.zzc &= -2097153;
        this.zzab = zzav.zzab;
    }

    @DexIgnore
    public final void a(int i, mj2.a aVar) {
        U();
        this.zzf.set(i, (mj2) aVar.i());
    }

    @DexIgnore
    public final void a0() {
        this.zzae = fn2.m();
    }

    @DexIgnore
    public final mj2 b(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final List<mj2> b0() {
        return this.zzf;
    }

    @DexIgnore
    public final uj2 c(int i) {
        return this.zzg.get(i);
    }

    @DexIgnore
    public final void c0() {
        this.zzc &= Integer.MAX_VALUE;
        this.zzam = zzav.zzam;
    }

    @DexIgnore
    public final void d(int i) {
        this.zzc |= 1;
        this.zze = i;
    }

    @DexIgnore
    public final int d0() {
        return this.zzf.size();
    }

    @DexIgnore
    public final void e(int i) {
        U();
        this.zzf.remove(i);
    }

    @DexIgnore
    public final List<uj2> e0() {
        return this.zzg;
    }

    @DexIgnore
    public final void f(int i) {
        W();
        this.zzg.remove(i);
    }

    @DexIgnore
    public final int f0() {
        return this.zzg.size();
    }

    @DexIgnore
    public final void g(int i) {
        this.zzc |= 1024;
        this.zzq = i;
    }

    @DexIgnore
    public final boolean g0() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final void h(String str) {
        if (str != null) {
            this.zzc |= 65536;
            this.zzw = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final long h0() {
        return this.zzh;
    }

    @DexIgnore
    public final void i(String str) {
        if (str != null) {
            this.zzc |= 262144;
            this.zzy = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean i0() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final void j(String str) {
        if (str != null) {
            this.zzc |= 2097152;
            this.zzab = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final long j0() {
        return this.zzi;
    }

    @DexIgnore
    public final void k(String str) {
        if (str != null) {
            this.zzc |= 4194304;
            this.zzac = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean k0() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final void l(String str) {
        if (str != null) {
            this.zzc |= 16777216;
            this.zzaf = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final long l0() {
        return this.zzj;
    }

    @DexIgnore
    public final void m(String str) {
        if (str != null) {
            this.zzc |= 268435456;
            this.zzaj = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean m0() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final long n0() {
        return this.zzk;
    }

    @DexIgnore
    public final long o() {
        return this.zzu;
    }

    @DexIgnore
    public final boolean o0() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 32768) != 0;
    }

    @DexIgnore
    public final long p0() {
        return this.zzl;
    }

    @DexIgnore
    public final long q() {
        return this.zzv;
    }

    @DexIgnore
    public final String q0() {
        return this.zzm;
    }

    @DexIgnore
    public final String r() {
        return this.zzw;
    }

    @DexIgnore
    public final String r0() {
        return this.zzn;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 131072) != 0;
    }

    @DexIgnore
    public final String s0() {
        return this.zzp;
    }

    @DexIgnore
    public final boolean t() {
        return this.zzx;
    }

    @DexIgnore
    public final boolean t0() {
        return (this.zzc & 1024) != 0;
    }

    @DexIgnore
    public final String u() {
        return this.zzo;
    }

    @DexIgnore
    public final int u0() {
        return this.zzq;
    }

    @DexIgnore
    public final String v() {
        return this.zzy;
    }

    @DexIgnore
    public final String v0() {
        return this.zzr;
    }

    @DexIgnore
    public final boolean w() {
        return (this.zzc & 524288) != 0;
    }

    @DexIgnore
    public final String w0() {
        return this.zzs;
    }

    @DexIgnore
    public final long x() {
        return this.zzz;
    }

    @DexIgnore
    public final String x0() {
        return this.zzt;
    }

    @DexIgnore
    public final boolean y() {
        return (this.zzc & 1048576) != 0;
    }

    @DexIgnore
    public final boolean y0() {
        return (this.zzc & 16384) != 0;
    }

    @DexIgnore
    public final int z() {
        return this.zzaa;
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 4;
        this.zzi = j;
    }

    @DexIgnore
    public final void c(long j) {
        this.zzc |= 8;
        this.zzj = j;
    }

    @DexIgnore
    public final void n(String str) {
        if (str != null) {
            this.zzc |= RecyclerView.UNDEFINED_DURATION;
            this.zzam = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void o(String str) {
        if (str != null) {
            this.zzd |= 4;
            this.zzap = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(mj2.a aVar) {
        U();
        this.zzf.add((mj2) aVar.i());
    }

    @DexIgnore
    public final void d(long j) {
        this.zzc |= 16;
        this.zzk = j;
    }

    @DexIgnore
    public final void e(long j) {
        this.zzc |= 32;
        this.zzl = j;
    }

    @DexIgnore
    public final void f(String str) {
        if (str != null) {
            this.zzc |= 4096;
            this.zzs = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void g(String str) {
        if (str != null) {
            this.zzc |= 8192;
            this.zzt = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(String str) {
        if (str != null) {
            this.zzc |= 128;
            this.zzn = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void c(String str) {
        if (str != null) {
            this.zzc |= 256;
            this.zzo = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void h(long j) {
        this.zzc |= 524288;
        this.zzz = j;
    }

    @DexIgnore
    public final void i(int i) {
        this.zzc |= 33554432;
        this.zzag = i;
    }

    @DexIgnore
    public final void j(long j) {
        this.zzc |= 1073741824;
        this.zzal = j;
    }

    @DexIgnore
    public final void k(long j) {
        this.zzd |= 16;
        this.zzas = j;
    }

    @DexIgnore
    public final void l(long j) {
        this.zzd |= 32;
        this.zzat = j;
    }

    @DexIgnore
    public final void a(Iterable<? extends mj2> iterable) {
        U();
        ql2.a(iterable, this.zzf);
    }

    @DexIgnore
    public final void d(String str) {
        if (str != null) {
            this.zzc |= 512;
            this.zzp = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void e(String str) {
        if (str != null) {
            this.zzc |= 2048;
            this.zzr = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void f(long j) {
        this.zzc |= 16384;
        this.zzu = j;
    }

    @DexIgnore
    public final void g(long j) {
        this.zzc |= 32768;
        this.zzv = j;
    }

    @DexIgnore
    public final void h(int i) {
        this.zzc |= 1048576;
        this.zzaa = i;
    }

    @DexIgnore
    public final void i(long j) {
        this.zzc |= 536870912;
        this.zzak = j;
    }

    @DexIgnore
    public final void j(int i) {
        this.zzd |= 2;
        this.zzao = i;
    }

    @DexIgnore
    public final void a(int i, uj2 uj2) {
        if (uj2 != null) {
            W();
            this.zzg.set(i, uj2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(boolean z) {
        this.zzc |= 8388608;
        this.zzad = z;
    }

    @DexIgnore
    public final void c(Iterable<? extends Integer> iterable) {
        if (!this.zzar.zza()) {
            ln2 ln2 = this.zzar;
            int size = ln2.size();
            this.zzar = ln2.zzb(size == 0 ? 10 : size << 1);
        }
        ql2.a(iterable, this.zzar);
    }

    @DexIgnore
    public final void b(Iterable<? extends kj2> iterable) {
        if (!this.zzae.zza()) {
            this.zzae = fn2.a(this.zzae);
        }
        ql2.a(iterable, this.zzae);
    }

    @DexIgnore
    public final void a(uj2 uj2) {
        if (uj2 != null) {
            W();
            this.zzg.add(uj2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(uj2.a aVar) {
        W();
        this.zzg.add((uj2) aVar.i());
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zzh = j;
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 64;
            this.zzm = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(boolean z) {
        this.zzc |= 131072;
        this.zzx = z;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new qj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzav, "\u0001+\u0000\u0002\u00012+\u0000\u0004\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0002\u0001\u0005\u0002\u0002\u0006\u0002\u0003\u0007\u0002\u0005\b\b\u0006\t\b\u0007\n\b\b\u000b\b\t\f\u0004\n\r\b\u000b\u000e\b\f\u0010\b\r\u0011\u0002\u000e\u0012\u0002\u000f\u0013\b\u0010\u0014\u0007\u0011\u0015\b\u0012\u0016\u0002\u0013\u0017\u0004\u0014\u0018\b\u0015\u0019\b\u0016\u001a\u0002\u0004\u001c\u0007\u0017\u001d\u001b\u001e\b\u0018\u001f\u0004\u0019 \u0004\u001a!\u0004\u001b\"\b\u001c#\u0002\u001d$\u0002\u001e%\b\u001f&\b '\u0004!)\b\",\t#-\u001d.\u0002$/\u0002%2\b&", new Object[]{"zzc", "zzd", "zze", "zzf", mj2.class, "zzg", uj2.class, "zzh", "zzi", "zzj", "zzl", "zzm", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzaa", "zzab", "zzac", "zzk", "zzad", "zzae", kj2.class, "zzaf", "zzag", "zzah", "zzai", "zzaj", "zzak", "zzal", "zzam", "zzan", "zzao", "zzap", "zzaq", "zzar", "zzas", "zzat", "zzau"});
            case 4:
                return zzav;
            case 5:
                yo2<qj2> yo2 = zzaw;
                if (yo2 == null) {
                    synchronized (qj2.class) {
                        yo2 = zzaw;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzav);
                            zzaw = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
