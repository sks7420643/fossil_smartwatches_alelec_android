package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j31 implements Parcelable.Creator<f51> {
    @DexIgnore
    public /* synthetic */ j31(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (wg6.a(readString, ay0.class.getName())) {
            return ay0.CREATOR.createFromParcel(parcel);
        }
        if (wg6.a(readString, z81.class.getName())) {
            return z81.CREATOR.createFromParcel(parcel);
        }
        if (wg6.a(readString, o11.class.getName())) {
            return o11.CREATOR.createFromParcel(parcel);
        }
        return null;
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new f51[i];
    }
}
