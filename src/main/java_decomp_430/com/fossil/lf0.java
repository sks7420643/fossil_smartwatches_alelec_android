package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum lf0 {
    SUCCESS(0),
    NOT_START(1),
    GATT_ERROR(7),
    UNEXPECTED_RESULT(8),
    INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT(9),
    BLUETOOTH_OFF(10),
    UNSUPPORTED(11),
    HID_PROXY_NOT_CONNECTED(256),
    HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
    HID_INPUT_DEVICE_DISABLED(258),
    HID_UNKNOWN_ERROR(511),
    INTERRUPTED(254),
    CONNECTION_DROPPED(255);
    
    @DexIgnore
    public static /* final */ on1 o; // = null;

    /*
    static {
        o = new on1((qg6) null);
    }
    */

    @DexIgnore
    public lf0(int i) {
    }
}
