package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ll3<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ hk3<Iterable<E>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ll3<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.b = iterable2;
        }

        @DexIgnore
        public Iterator<E> iterator() {
            return this.b.iterator();
        }
    }

    @DexIgnore
    public ll3() {
        this.a = hk3.absent();
    }

    @DexIgnore
    public final Iterable<E> a() {
        return this.a.or(this);
    }

    @DexIgnore
    public final im3<E> b() {
        return im3.copyOf(a());
    }

    @DexIgnore
    public String toString() {
        return pm3.d(a());
    }

    @DexIgnore
    public static <E> ll3<E> a(Iterable<E> iterable) {
        return iterable instanceof ll3 ? (ll3) iterable : new a(iterable, iterable);
    }

    @DexIgnore
    public ll3(Iterable<E> iterable) {
        jk3.a(iterable);
        this.a = hk3.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public final ll3<E> a(kk3<? super E> kk3) {
        return a(pm3.b(a(), kk3));
    }
}
