package com.fossil;

import android.content.res.Resources;
import com.zendesk.sdk.attachment.AttachmentHelper;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Collection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eb6 extends r86 implements jb6 {
    @DexIgnore
    public eb6(i86 i86, String str, String str2, va6 va6, ta6 ta6) {
        super(i86, str, str2, va6, ta6);
    }

    @DexIgnore
    public boolean a(hb6 hb6) {
        ua6 a = a();
        a(a, hb6);
        b(a, hb6);
        l86 g = c86.g();
        g.d("Fabric", "Sending app info to " + b());
        if (hb6.j != null) {
            l86 g2 = c86.g();
            g2.d("Fabric", "App icon hash is " + hb6.j.a);
            l86 g3 = c86.g();
            g3.d("Fabric", "App icon size is " + hb6.j.c + "x" + hb6.j.d);
        }
        int g4 = a.g();
        String str = "POST".equals(a.m()) ? "Create" : "Update";
        l86 g5 = c86.g();
        g5.d("Fabric", str + " app request ID: " + a.c("X-REQUEST-ID"));
        l86 g6 = c86.g();
        g6.d("Fabric", "Result was " + g4);
        return m96.a(g4) == 0;
    }

    @DexIgnore
    public final ua6 b(ua6 ua6, hb6 hb6) {
        ua6.e("app[identifier]", hb6.b);
        ua6.e("app[name]", hb6.f);
        ua6.e("app[display_version]", hb6.c);
        ua6.e("app[build_version]", hb6.d);
        ua6.a("app[source]", (Number) Integer.valueOf(hb6.g));
        ua6.e("app[minimum_sdk_version]", hb6.h);
        ua6.e("app[built_sdk_version]", hb6.i);
        if (!z86.b(hb6.e)) {
            ua6.e("app[instance_identifier]", hb6.e);
        }
        if (hb6.j != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.e.d().getResources().openRawResource(hb6.j.b);
                ua6.e("app[icon][hash]", hb6.j.a);
                ua6.a("app[icon][data]", "icon.png", AttachmentHelper.DEFAULT_MIMETYPE, inputStream);
                ua6.a("app[icon][width]", (Number) Integer.valueOf(hb6.j.c));
                ua6.a("app[icon][height]", (Number) Integer.valueOf(hb6.j.d));
            } catch (Resources.NotFoundException e) {
                l86 g = c86.g();
                g.e("Fabric", "Failed to find app icon with resource ID: " + hb6.j.b, e);
            } catch (Throwable th) {
                z86.a((Closeable) inputStream, "Failed to close app icon InputStream.");
                throw th;
            }
            z86.a((Closeable) inputStream, "Failed to close app icon InputStream.");
        }
        Collection<k86> collection = hb6.k;
        if (collection != null) {
            for (k86 next : collection) {
                ua6.e(b(next), next.c());
                ua6.e(a(next), next.a());
            }
        }
        return ua6;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, hb6 hb6) {
        ua6.c("X-CRASHLYTICS-API-KEY", hb6.a);
        ua6.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        ua6.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.j());
        return ua6;
    }

    @DexIgnore
    public String a(k86 k86) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{k86.b()});
    }

    @DexIgnore
    public String b(k86 k86) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{k86.b()});
    }
}
