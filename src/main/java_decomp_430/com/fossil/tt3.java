package com.fossil;

import com.fossil.wp3;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tt3 implements xt3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ut3 b;

    @DexIgnore
    public tt3(Set<vt3> set, ut3 ut3) {
        this.a = a(set);
        this.b = ut3;
    }

    @DexIgnore
    public static wp3<xt3> b() {
        wp3.b<xt3> a2 = wp3.a(xt3.class);
        a2.a(gq3.b(vt3.class));
        a2.a((zp3<xt3>) st3.a());
        return a2.b();
    }

    @DexIgnore
    public String a() {
        if (this.b.a().isEmpty()) {
            return this.a;
        }
        return this.a + ' ' + a(this.b.a());
    }

    @DexIgnore
    public static String a(Set<vt3> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<vt3> it = set.iterator();
        while (it.hasNext()) {
            vt3 next = it.next();
            sb.append(next.a());
            sb.append('/');
            sb.append(next.b());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static /* synthetic */ xt3 a(xp3 xp3) {
        return new tt3(xp3.d(vt3.class), ut3.b());
    }
}
