package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ x51 a;
    @DexIgnore
    public /* final */ /* synthetic */ ie1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lt0(x51 x51, ie1 ie1) {
        super(1);
        this.a = x51;
        this.b = ie1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        byte[] bArr;
        qv0 qv0 = (qv0) obj;
        fl1 fl1 = (fl1) qv0;
        g01 g01 = new g01(this.b.c());
        ie1 a2 = this.a.a(g01.a, g01.b);
        x51 x51 = this.a;
        ue1 ue1 = x51.w;
        cc0 cc0 = cc0.DEBUG;
        String str = x51.a;
        Object[] objArr = new Object[4];
        objArr[0] = ue1.t;
        objArr[1] = Byte.valueOf(g01.a);
        objArr[2] = Byte.valueOf(g01.b);
        objArr[3] = a2 != null ? a2.toString() : null;
        if (a2 == null || (bArr = a2.e) == null) {
            bArr = new byte[0];
        }
        ie1 ie1 = new ie1(this.a.w.t, g01.a, g01.b, cw0.a(bArr, fl1.V), fl1.t(), fl1.s(), this.a.g(), false);
        if (qv0.v.c == il0.SUCCESS) {
            this.a.d(ie1);
        } else {
            this.a.a(ie1);
        }
        return cd6.a;
    }
}
