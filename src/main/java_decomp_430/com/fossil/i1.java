package com.fossil;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i1 {
    @DexIgnore
    public /* final */ ArrayList<ba> a; // = new ArrayList<>();
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Interpolator c;
    @DexIgnore
    public ca d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ da f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends da {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = false;
            i1.this.b();
        }

        @DexIgnore
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == i1.this.a.size()) {
                ca caVar = i1.this.d;
                if (caVar != null) {
                    caVar.b((View) null);
                }
                a();
            }
        }

        @DexIgnore
        public void c(View view) {
            if (!this.a) {
                this.a = true;
                ca caVar = i1.this.d;
                if (caVar != null) {
                    caVar.c((View) null);
                }
            }
        }
    }

    @DexIgnore
    public i1 a(ba baVar) {
        if (!this.e) {
            this.a.add(baVar);
        }
        return this;
    }

    @DexIgnore
    public void b() {
        this.e = false;
    }

    @DexIgnore
    public void c() {
        if (!this.e) {
            Iterator<ba> it = this.a.iterator();
            while (it.hasNext()) {
                ba next = it.next();
                long j = this.b;
                if (j >= 0) {
                    next.a(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.a(interpolator);
                }
                if (this.d != null) {
                    next.a((ca) this.f);
                }
                next.c();
            }
            this.e = true;
        }
    }

    @DexIgnore
    public i1 a(ba baVar, ba baVar2) {
        this.a.add(baVar);
        baVar2.b(baVar.b());
        this.a.add(baVar2);
        return this;
    }

    @DexIgnore
    public void a() {
        if (this.e) {
            Iterator<ba> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            this.e = false;
        }
    }

    @DexIgnore
    public i1 a(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    @DexIgnore
    public i1 a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    @DexIgnore
    public i1 a(ca caVar) {
        if (!this.e) {
            this.d = caVar;
        }
        return this;
    }
}
