package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed3<TResult> implements kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public lc3 c;

    @DexIgnore
    public ed3(Executor executor, lc3 lc3) {
        this.a = executor;
        this.c = lc3;
    }

    @DexIgnore
    public final void onComplete(qc3<TResult> qc3) {
        if (!qc3.e() && !qc3.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new fd3(this, qc3));
                }
            }
        }
    }
}
