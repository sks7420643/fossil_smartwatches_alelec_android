package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class bc implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bc> CREATOR; // = new a();
    @DexIgnore
    public ArrayList<ec> a;
    @DexIgnore
    public ArrayList<String> b;
    @DexIgnore
    public ub[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<bc> {
        @DexIgnore
        public bc createFromParcel(Parcel parcel) {
            return new bc(parcel);
        }

        @DexIgnore
        public bc[] newArray(int i) {
            return new bc[i];
        }
    }

    @DexIgnore
    public bc() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.a);
        parcel.writeStringList(this.b);
        parcel.writeTypedArray(this.c, i);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
    }

    @DexIgnore
    public bc(Parcel parcel) {
        this.a = parcel.createTypedArrayList(ec.CREATOR);
        this.b = parcel.createStringArrayList();
        this.c = (ub[]) parcel.createTypedArray(ub.CREATOR);
        this.d = parcel.readInt();
        this.e = parcel.readString();
    }
}
