package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ca6 extends RuntimeException {
    @DexIgnore
    public ca6() {
    }

    @DexIgnore
    public ca6(String str) {
        super(str);
    }

    @DexIgnore
    public ca6(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public ca6(Throwable th) {
        super(th);
    }
}
