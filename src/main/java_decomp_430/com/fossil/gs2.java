package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs2 implements hs2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a; // = new vk2(qk2.a("com.google.android.gms.measurement")).a("measurement.client.firebase_feature_rollout.v1.enable", true);

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
