package com.fossil;

import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj4 implements vr {
    @DexIgnore
    public /* final */ List<BaseFeatureModel> b;

    @DexIgnore
    public oj4(List<? extends BaseFeatureModel> list) {
        this.b = list;
    }

    @DexIgnore
    public final List<BaseFeatureModel> a() {
        return this.b;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        wg6.b(messageDigest, "messageDigest");
        List<BaseFeatureModel> list = this.b;
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            Iterator<BaseFeatureModel> it = list.iterator();
            while (it.hasNext()) {
                AppFilter appFilter = (BaseFeatureModel) it.next();
                if (appFilter instanceof AppFilter) {
                    sb.append(appFilter.getType());
                    wg6.a((Object) sb, "id.append(item.type)");
                } else if (appFilter != null) {
                    List contacts = ((ContactGroup) appFilter).getContacts();
                    if (contacts != null && (!contacts.isEmpty())) {
                        Object obj = contacts.get(0);
                        wg6.a(obj, "contactList[0]");
                        sb.append(((Contact) obj).getFirstName());
                        Object obj2 = contacts.get(0);
                        wg6.a(obj2, "contactList[0]");
                        sb.append(((Contact) obj2).getLastName());
                        Object obj3 = contacts.get(0);
                        wg6.a(obj3, "contactList[0]");
                        sb.append(((Contact) obj3).getPhotoThumbUri());
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.wearables.fsl.contact.ContactGroup");
                }
            }
        }
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "id.toString()");
        Charset charset = vr.a;
        wg6.a((Object) charset, "Key.CHARSET");
        if (sb2 != null) {
            byte[] bytes = sb2.getBytes(charset);
            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }
}
