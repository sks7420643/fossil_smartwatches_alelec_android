package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov0 extends j61 {
    @DexIgnore
    public long A;
    @DexIgnore
    public h91 B; // = h91.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean C;

    @DexIgnore
    public ov0(ue1 ue1, boolean z, long j) {
        super(lx0.CONNECT, ue1);
        this.C = z;
        this.A = j;
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public void a(ni1 ni1) {
    }

    @DexIgnore
    public long e() {
        return this.A;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(super.h(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.y.getBondState())), bm0.BLUETOOTH_DEVICE_TYPE, (Object) Integer.valueOf(this.y.w.getType())), bm0.AUTO_CONNECT, (Object) Boolean.valueOf(this.C)), bm0.PERIPHERAL_CURRENT_STATE, (Object) cw0.a((Enum<?>) this.y.s)), bm0.MAC_ADDRESS, (Object) this.y.t);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) this.B));
    }

    @DexIgnore
    public ok0 l() {
        return new jr0(this.C, this.y.v);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.B = ((jr0) ok0).k;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) this.B)), 7));
    }
}
