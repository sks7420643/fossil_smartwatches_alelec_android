package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs6 extends at6 {
    @DexIgnore
    public /* final */ Method c;
    @DexIgnore
    public /* final */ Method d;

    @DexIgnore
    public xs6(Method method, Method method2) {
        this.c = method;
        this.d = method2;
    }

    @DexIgnore
    public static xs6 f() {
        try {
            return new xs6(SSLParameters.class.getMethod("setApplicationProtocols", new Class[]{String[].class}), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, String str, List<wq6> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> a = at6.a(list);
            this.c.invoke(sSLParameters, new Object[]{a.toArray(new String[a.size()])});
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw fr6.a("unable to set ssl parameters", (Exception) e);
        }
    }

    @DexIgnore
    public String b(SSLSocket sSLSocket) {
        try {
            String str = (String) this.d.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw fr6.a("unable to get selected protocols", (Exception) e);
        }
    }
}
