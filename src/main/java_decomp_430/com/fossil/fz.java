package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fz {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ as<T> b;

        @DexIgnore
        public a(Class<T> cls, as<T> asVar) {
            this.a = cls;
            this.b = asVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <Z> void a(Class<Z> cls, as<Z> asVar) {
        this.a.add(new a(cls, asVar));
    }

    @DexIgnore
    public synchronized <Z> as<Z> a(Class<Z> cls) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            a aVar = this.a.get(i);
            if (aVar.a(cls)) {
                return aVar.b;
            }
        }
        return null;
    }
}
