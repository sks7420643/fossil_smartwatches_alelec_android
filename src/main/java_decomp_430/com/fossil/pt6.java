package com.fossil;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pt6 extends au6 {
    @DexIgnore
    public au6 e;

    @DexIgnore
    public pt6(au6 au6) {
        if (au6 != null) {
            this.e = au6;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public final pt6 a(au6 au6) {
        if (au6 != null) {
            this.e = au6;
            return this;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public au6 b() {
        return this.e.b();
    }

    @DexIgnore
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    public boolean d() {
        return this.e.d();
    }

    @DexIgnore
    public void e() throws IOException {
        this.e.e();
    }

    @DexIgnore
    public final au6 g() {
        return this.e;
    }

    @DexIgnore
    public au6 a(long j, TimeUnit timeUnit) {
        return this.e.a(j, timeUnit);
    }

    @DexIgnore
    public au6 a(long j) {
        return this.e.a(j);
    }

    @DexIgnore
    public au6 a() {
        return this.e.a();
    }
}
