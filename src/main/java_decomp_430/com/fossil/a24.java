package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a24 implements Factory<an4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public a24(b14 b14, Provider<Context> provider) {
        this.a = b14;
        this.b = provider;
    }

    @DexIgnore
    public static a24 a(b14 b14, Provider<Context> provider) {
        return new a24(b14, provider);
    }

    @DexIgnore
    public static an4 b(b14 b14, Provider<Context> provider) {
        return a(b14, provider.get());
    }

    @DexIgnore
    public static an4 a(b14 b14, Context context) {
        an4 a2 = b14.a(context);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public an4 get() {
        return b(this.a, this.b);
    }
}
