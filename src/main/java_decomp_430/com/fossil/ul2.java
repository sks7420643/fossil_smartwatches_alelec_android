package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul2 {
    @DexIgnore
    public static int a(byte[] bArr, int i, xl2 xl2) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return a((int) b, bArr, i2, xl2);
        }
        xl2.a = b;
        return i2;
    }

    @DexIgnore
    public static int b(byte[] bArr, int i, xl2 xl2) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            xl2.b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & Byte.MAX_VALUE)) << i4;
            int i6 = i5;
            b = b2;
            i3 = i6;
        }
        xl2.b = j2;
        return i3;
    }

    @DexIgnore
    public static double c(byte[] bArr, int i) {
        return Double.longBitsToDouble(b(bArr, i));
    }

    @DexIgnore
    public static float d(byte[] bArr, int i) {
        return Float.intBitsToFloat(a(bArr, i));
    }

    @DexIgnore
    public static int e(byte[] bArr, int i, xl2 xl2) throws qn2 {
        int a = a(bArr, i, xl2);
        int i2 = xl2.a;
        if (i2 < 0) {
            throw qn2.zzb();
        } else if (i2 > bArr.length - a) {
            throw qn2.zza();
        } else if (i2 == 0) {
            xl2.c = yl2.zza;
            return a;
        } else {
            xl2.c = yl2.zza(bArr, a, i2);
            return a + i2;
        }
    }

    @DexIgnore
    public static int c(byte[] bArr, int i, xl2 xl2) throws qn2 {
        int a = a(bArr, i, xl2);
        int i2 = xl2.a;
        if (i2 < 0) {
            throw qn2.zzb();
        } else if (i2 == 0) {
            xl2.c = "";
            return a;
        } else {
            xl2.c = new String(bArr, a, i2, hn2.a);
            return a + i2;
        }
    }

    @DexIgnore
    public static int d(byte[] bArr, int i, xl2 xl2) throws qn2 {
        int a = a(bArr, i, xl2);
        int i2 = xl2.a;
        if (i2 < 0) {
            throw qn2.zzb();
        } else if (i2 == 0) {
            xl2.c = "";
            return a;
        } else {
            xl2.c = fq2.b(bArr, a, i2);
            return a + i2;
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, xl2 xl2) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            xl2.a = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            xl2.a = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            xl2.a = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            xl2.a = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                xl2.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    @DexIgnore
    public static long b(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    @DexIgnore
    public static int a(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static int a(fp2 fp2, byte[] bArr, int i, int i2, xl2 xl2) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        int i4 = b;
        if (b < 0) {
            i3 = a((int) b, bArr, i3, xl2);
            i4 = xl2.a;
        }
        int i5 = i3;
        if (i4 < 0 || i4 > i2 - i5) {
            throw qn2.zza();
        }
        Object zza = fp2.zza();
        int i6 = i4 + i5;
        fp2.a(zza, bArr, i5, i6, xl2);
        fp2.zzc(zza);
        xl2.c = zza;
        return i6;
    }

    @DexIgnore
    public static int a(fp2 fp2, byte[] bArr, int i, int i2, int i3, xl2 xl2) throws IOException {
        uo2 uo2 = (uo2) fp2;
        Object zza = uo2.zza();
        int a = uo2.a(zza, bArr, i, i2, i3, xl2);
        uo2.zzc(zza);
        xl2.c = zza;
        return a;
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, nn2<?> nn2, xl2 xl2) {
        in2 in2 = (in2) nn2;
        int a = a(bArr, i2, xl2);
        in2.a(xl2.a);
        while (a < i3) {
            int a2 = a(bArr, a, xl2);
            if (i != xl2.a) {
                break;
            }
            a = a(bArr, a2, xl2);
            in2.a(xl2.a);
        }
        return a;
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, nn2<?> nn2, xl2 xl2) throws IOException {
        in2 in2 = (in2) nn2;
        int a = a(bArr, i, xl2);
        int i2 = xl2.a + a;
        while (a < i2) {
            a = a(bArr, a, xl2);
            in2.a(xl2.a);
        }
        if (a == i2) {
            return a;
        }
        throw qn2.zza();
    }

    @DexIgnore
    public static int a(fp2<?> fp2, int i, byte[] bArr, int i2, int i3, nn2<?> nn2, xl2 xl2) throws IOException {
        int a = a((fp2) fp2, bArr, i2, i3, xl2);
        nn2.add(xl2.c);
        while (a < i3) {
            int a2 = a(bArr, a, xl2);
            if (i != xl2.a) {
                break;
            }
            a = a((fp2) fp2, bArr, a2, i3, xl2);
            nn2.add(xl2.c);
        }
        return a;
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, zp2 zp2, xl2 xl2) throws qn2 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int b = b(bArr, i2, xl2);
                zp2.a(i, (Object) Long.valueOf(xl2.b));
                return b;
            } else if (i4 == 1) {
                zp2.a(i, (Object) Long.valueOf(b(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int a = a(bArr, i2, xl2);
                int i5 = xl2.a;
                if (i5 < 0) {
                    throw qn2.zzb();
                } else if (i5 <= bArr.length - a) {
                    if (i5 == 0) {
                        zp2.a(i, (Object) yl2.zza);
                    } else {
                        zp2.a(i, (Object) yl2.zza(bArr, a, i5));
                    }
                    return a + i5;
                } else {
                    throw qn2.zza();
                }
            } else if (i4 == 3) {
                zp2 e = zp2.e();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int a2 = a(bArr, i2, xl2);
                    int i8 = xl2.a;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = a2;
                        break;
                    }
                    int a3 = a(i7, bArr, a2, i3, e, xl2);
                    i7 = i8;
                    i2 = a3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw qn2.zzg();
                }
                zp2.a(i, (Object) e);
                return i2;
            } else if (i4 == 5) {
                zp2.a(i, (Object) Integer.valueOf(a(bArr, i2)));
                return i2 + 4;
            } else {
                throw qn2.zzd();
            }
        } else {
            throw qn2.zzd();
        }
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3, xl2 xl2) throws qn2 {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return b(bArr, i2, xl2);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return a(bArr, i2, xl2) + xl2.a;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = a(bArr, i2, xl2);
                    i6 = xl2.a;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = a(i6, bArr, i2, i3, xl2);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw qn2.zzg();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw qn2.zzd();
            }
        } else {
            throw qn2.zzd();
        }
    }
}
