package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj5 extends uj5 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ vj5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = yj5.class.getSimpleName();
        wg6.a((Object) simpleName, "AboutPresenter::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public yj5(vj5 vj5) {
        wg6.b(vj5, "mView");
        this.e = vj5;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(f, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(f, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }
}
