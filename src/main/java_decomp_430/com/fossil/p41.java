package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p41 extends t21 {
    @DexIgnore
    public /* final */ byte[] A; // = new byte[0];
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public /* final */ boolean D; // = true;

    @DexIgnore
    public p41(lx0 lx0, ue1 ue1, int i) {
        super(lx0, ue1, i);
    }

    @DexIgnore
    public abstract sj0 a(byte b);

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        return new JSONObject();
    }

    @DexIgnore
    public final void a(ok0 ok0) {
    }

    @DexIgnore
    public final void b(sg1 sg1) {
        if (d(sg1)) {
            e(sg1);
        } else if (c(sg1)) {
            f(sg1);
        }
        if (this.B && this.C) {
            a(this.v);
        }
    }

    @DexIgnore
    public final void c(ok0 ok0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.B = true;
        nn0 nn0 = this.f;
        if (nn0 != null) {
            nn0.i = true;
        }
        nn0 nn02 = this.f;
        if (!(nn02 == null || (jSONObject2 = nn02.m) == null)) {
            cw0.a(jSONObject2, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
        }
        if (this.v.c == il0.NOT_START) {
            this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
            il0 il0 = this.v.c;
            il0 il02 = il0.SUCCESS;
        }
        nn0 nn03 = this.f;
        if (nn03 != null) {
            nn03.i = true;
        }
        nn0 nn04 = this.f;
        if (!(nn04 == null || (jSONObject = nn04.m) == null)) {
            cw0.a(jSONObject, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
        }
        k();
        if (this.C) {
            a(this.v);
        }
    }

    @DexIgnore
    public boolean c(sg1 sg1) {
        return false;
    }

    @DexIgnore
    public final boolean d(sg1 sg1) {
        if (sg1.a == p() && sg1.b.length >= r().length) {
            if (Arrays.equals(r(), md6.a(sg1.b, 0, r().length))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void e(sg1 sg1) {
        byte[] bArr = sg1.b;
        JSONObject jSONObject = new JSONObject();
        if (q()) {
            sj0 a = a(bArr[r().length]);
            this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(a).c, (ch0) null, a, 11);
            if (this.v.c == il0.SUCCESS) {
                jSONObject = a(md6.a(bArr, r().length + 1, bArr.length));
            } else {
                this.C = true;
            }
        } else {
            jSONObject = a(md6.a(bArr, r().length, bArr.length));
        }
        this.g.add(new ne0(0, sg1.a, bArr, jSONObject, 1));
    }

    @DexIgnore
    public void f(sg1 sg1) {
    }

    @DexIgnore
    public final ok0 l() {
        ByteBuffer put = ByteBuffer.allocate(o().length + n().length).order(ByteOrder.LITTLE_ENDIAN).put(o()).put(n());
        rg1 m = m();
        byte[] array = put.array();
        wg6.a(array, "byteBuffer.array()");
        return new fb1(m, array, this.y.v);
    }

    @DexIgnore
    public abstract rg1 m();

    @DexIgnore
    public byte[] n() {
        return this.A;
    }

    @DexIgnore
    public abstract byte[] o();

    @DexIgnore
    public abstract rg1 p();

    @DexIgnore
    public boolean q() {
        return this.D;
    }

    @DexIgnore
    public abstract byte[] r();
}
