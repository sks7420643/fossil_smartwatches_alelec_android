package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx extends ix<Drawable> {
    @DexIgnore
    public jx(Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    public static rt<Drawable> a(Drawable drawable) {
        if (drawable != null) {
            return new jx(drawable);
        }
        return null;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public int b() {
        return Math.max(1, this.a.getIntrinsicWidth() * this.a.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    public Class<Drawable> c() {
        return this.a.getClass();
    }
}
