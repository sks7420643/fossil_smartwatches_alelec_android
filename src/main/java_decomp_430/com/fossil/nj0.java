package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj0 extends if1 {
    @DexIgnore
    public int B;
    @DexIgnore
    public fo0 C;
    @DexIgnore
    public /* final */ ArrayList<hl1> D; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ it0[] E;

    @DexIgnore
    public nj0(ue1 ue1, q41 q41, String str, it0[] it0Arr) {
        super(ue1, q41, eh1.TRY_SET_CONNECTION_PARAMS, str);
        this.E = it0Arr;
    }

    @DexIgnore
    public Object d() {
        fo0 fo0 = this.C;
        return fo0 != null ? fo0 : new fo0(0, 0, 0);
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.D;
    }

    @DexIgnore
    public void h() {
        m();
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.ACCEPTED_CONNECTION_PARAMS;
        fo0 fo0 = this.C;
        return cw0.a(k, bm0, (Object) fo0 != null ? fo0.a() : null);
    }

    @DexIgnore
    public final void m() {
        int i = this.B;
        it0[] it0Arr = this.E;
        if (i < it0Arr.length) {
            if1.a((if1) this, (qv0) new p61(it0Arr[i], this.w), (hg6) new cg0(this), (hg6) new uh0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            return;
        }
        a(km1.a(this.v, (eh1) null, sk1.EXCHANGED_VALUE_NOT_SATISFIED, (bn0) null, 5));
    }
}
