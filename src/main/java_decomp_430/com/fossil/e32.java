package com.fossil;

import com.fossil.v12;
import com.fossil.yv1;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e32 implements yv1.a {
    @DexIgnore
    public /* final */ /* synthetic */ yv1 a;
    @DexIgnore
    public /* final */ /* synthetic */ rc3 b;
    @DexIgnore
    public /* final */ /* synthetic */ v12.a c;
    @DexIgnore
    public /* final */ /* synthetic */ v12.b d;

    @DexIgnore
    public e32(yv1 yv1, rc3 rc3, v12.a aVar, v12.b bVar) {
        this.a = yv1;
        this.b = rc3;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    public final void a(Status status) {
        if (status.F()) {
            this.b.a(this.c.a(this.a.a(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.a((Exception) this.d.a(status));
    }
}
