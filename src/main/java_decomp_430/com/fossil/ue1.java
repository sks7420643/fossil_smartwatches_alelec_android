package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.k40;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue1 implements Parcelable {
    @DexIgnore
    public static /* final */ gy0 CREATOR; // = new gy0((qg6) null);
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public BluetoothGatt b;
    @DexIgnore
    public a01 c;
    @DexIgnore
    public /* final */ BluetoothGattCallback d;
    @DexIgnore
    public hr0 e;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<es0> f;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<xt0> g;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<j71> h;
    @DexIgnore
    public /* final */ HashMap<rg1, gk1> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public /* final */ BroadcastReceiver p;
    @DexIgnore
    public /* final */ BroadcastReceiver q;
    @DexIgnore
    public /* final */ BroadcastReceiver r;
    @DexIgnore
    public h91 s;
    @DexIgnore
    public /* final */ String t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ at0 v;
    @DexIgnore
    public /* final */ BluetoothDevice w;

    @DexIgnore
    public /* synthetic */ ue1(BluetoothDevice bluetoothDevice, qg6 qg6) {
        this.w = bluetoothDevice;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.a = new Handler(myLooper);
            this.d = new pg1(this);
            this.e = new hr0(this);
            this.f = new CopyOnWriteArraySet<>();
            this.g = new CopyOnWriteArraySet<>();
            this.h = new CopyOnWriteArraySet<>();
            this.i = new HashMap<>();
            this.j = 20;
            this.o = this.j;
            this.p = new ki1(this);
            this.q = new ek1(this);
            this.r = new bh0(this);
            this.s = h91.DISCONNECTED;
            String address = this.w.getAddress();
            wg6.a(address, "bluetoothDevice.address");
            this.t = address;
            this.v = new at0();
            BroadcastReceiver broadcastReceiver = this.p;
            String[] strArr = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"};
            Context a2 = gk0.f.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (String addAction : strArr) {
                    intentFilter.addAction(addAction);
                }
                ce.a(a2).a(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = this.q;
            String[] strArr2 = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED"};
            Context a3 = gk0.f.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (String addAction2 : strArr2) {
                    intentFilter2.addAction(addAction2);
                }
                ce.a(a3).a(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = this.r;
            String[] strArr3 = {"com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED"};
            Context a4 = gk0.f.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (String addAction3 : strArr3) {
                    intentFilter3.addAction(addAction3);
                }
                ce.a(a4).a(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.u = z;
    }

    @DexIgnore
    public final void c() {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new hy0(this, new t31(x11.BLUETOOTH_OFF, 0, 2), new UUID[0], new rg1[0]));
            return;
        }
        BluetoothGatt bluetoothGatt = this.b;
        if (bluetoothGatt == null) {
            this.a.post(new hy0(this, new t31(x11.GATT_NULL, 0, 2), new UUID[0], new rg1[0]));
        } else if (true != bluetoothGatt.discoverServices()) {
            this.a.post(new hy0(this, new t31(x11.START_FAIL, 0, 2), new UUID[0], new rg1[0]));
        }
    }

    @DexIgnore
    public final String d() {
        String name = this.w.getName();
        return name != null ? name : "";
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final m51 e() {
        return m51.f.a(s81.d.c(this.w));
    }

    @DexIgnore
    public final boolean f() {
        if (!fm0.f.a()) {
            return false;
        }
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            return false;
        }
        try {
            cc0 cc02 = cc0.DEBUG;
            Method method = this.w.getClass().getMethod("removeBond", new Class[0]);
            wg6.a(method, "bluetoothDevice.javaClass.getMethod(\"removeBond\")");
            Object invoke = method.invoke(this.w, new Object[0]);
            if (invoke != null) {
                return ((Boolean) invoke).booleanValue();
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (Exception e2) {
            qs0.h.a(e2);
            return false;
        }
    }

    @DexIgnore
    public final void g() {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new n51(this, new t31(x11.BLUETOOTH_OFF, 0, 2), 0));
            return;
        }
        t31 t31 = new t31(x11.SUCCESS, 0, 2);
        BluetoothGatt bluetoothGatt = this.b;
        if (bluetoothGatt == null) {
            t31 = new t31(x11.GATT_NULL, 0, 2);
        } else if (true != bluetoothGatt.readRemoteRssi()) {
            t31 = new t31(x11.START_FAIL, 0, 2);
        }
        if (t31.a != x11.SUCCESS) {
            this.a.post(new n51(this, t31, 0));
        }
    }

    @DexIgnore
    public final mw0 getBondState() {
        return mw0.e.a(this.w.getBondState());
    }

    @DexIgnore
    public final void h() {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new k71(this, new t31(x11.BLUETOOTH_OFF, 0, 2), getBondState(), getBondState()));
        } else if (mw0.BOND_NONE == getBondState()) {
            this.a.post(new k71(this, new t31(x11.SUCCESS, 0, 2), getBondState(), getBondState()));
        } else if (!f()) {
            this.a.post(new k71(this, new t31(x11.START_FAIL, 0, 2), getBondState(), getBondState()));
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.w, i2);
        }
    }

    @DexIgnore
    public final h91 b(int i2) {
        if (i2 == 0) {
            return h91.DISCONNECTED;
        }
        if (i2 == 1) {
            return h91.CONNECTING;
        }
        if (i2 == 2) {
            return h91.CONNECTED;
        }
        if (i2 != 3) {
            return h91.DISCONNECTED;
        }
        return h91.DISCONNECTING;
    }

    @DexIgnore
    public final void a(a01 a01) {
        nn0 nn0 = new nn0("connection_state_changed", og0.CENTRAL_EVENT, this.t, "", "", a01.a == 0, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.STATUS, (Object) Integer.valueOf(a01.a)), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) b(a01.b))), 448);
        Long l = (Long) yd6.e(a01.c);
        if (l != null) {
            long longValue = l.longValue();
            nn0.a = longValue;
            cw0.a(nn0.m, bm0.START_TIME, (Object) Double.valueOf(cw0.a(longValue)));
            JSONObject jSONObject = nn0.m;
            bm0 bm0 = bm0.END_TIME;
            Long l2 = (Long) yd6.f(a01.c);
            if (l2 != null) {
                longValue = l2.longValue();
            }
            cw0.a(jSONObject, bm0, (Object) Double.valueOf(cw0.a(longValue)));
            JSONArray jSONArray = new JSONArray();
            for (Number longValue2 : a01.c) {
                jSONArray.put(cw0.a(longValue2.longValue()));
            }
            cw0.a(nn0.m, bm0.TIMESTAMPS, (Object) jSONArray);
            qs0.h.a(nn0);
        }
    }

    @DexIgnore
    public final void b(rg1 rg1, byte[] bArr) {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new eb1(this, new t31(x11.BLUETOOTH_OFF, 0, 2), rg1, new byte[0]));
            return;
        }
        t31 t31 = new t31(x11.SUCCESS, 0, 2);
        if (this.b == null) {
            t31 = new t31(x11.GATT_NULL, 0, 2);
        } else {
            gk1 gk1 = this.i.get(rg1);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = gk1 != null ? gk1.a : null;
            if (bluetoothGattCharacteristic != null) {
                bluetoothGattCharacteristic.setValue(bArr);
            }
            if (bluetoothGattCharacteristic == null) {
                t31 = new t31(x11.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.b;
                if (bluetoothGatt == null || true != bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                    t31 = new t31(x11.START_FAIL, 0, 2);
                }
            }
        }
        cc0 cc02 = cc0.DEBUG;
        Object[] objArr = {rg1, Integer.valueOf(bArr.length), cw0.a(bArr, (String) null, 1)};
        if (t31.a != x11.SUCCESS) {
            this.a.post(new eb1(this, t31, rg1, new byte[0]));
        }
    }

    @DexIgnore
    public final void b() {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new bt0(this, new t31(x11.BLUETOOTH_OFF, 0, 2), getBondState(), getBondState()));
        } else if (mw0.BONDED == getBondState()) {
            this.a.post(new bt0(this, new t31(x11.SUCCESS, 0, 2), getBondState(), getBondState()));
        } else if (!this.w.createBond()) {
            this.a.post(new bt0(this, new t31(x11.START_FAIL, 0, 2), getBondState(), getBondState()));
        }
    }

    @DexIgnore
    public final void a(int i2, h91 h91) {
        Iterator<xt0> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new ir0(it.next(), h91, i2));
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(rg1 rg1, byte[] bArr) {
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {rg1.name(), cw0.a(bArr, (String) null, 1)};
        Iterator<xt0> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new ti0(it.next(), rg1, bArr));
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        if (r8 != 3) goto L_0x0106;
     */
    @DexIgnore
    public final void a(int i2, int i3) {
        synchronized (this.s) {
            h91 b2 = b(i2);
            h91 h91 = this.s;
            if (b2 != h91) {
                if (i2 != 0) {
                    if (i2 != 1) {
                        if (i2 == 2) {
                            if (this.b != null) {
                                this.s = b2;
                                cc0 cc0 = cc0.DEBUG;
                                Object[] objArr = {h91, b2, Integer.valueOf(i3)};
                                this.a.post(new uu0(this, t31.c.a(i3), this.s));
                                this.a.post(new yn0(this, t31.c.a(i3), this.s));
                                a(i3, this.s);
                                a(this.s);
                            }
                        }
                    }
                    this.s = b2;
                    cc0 cc02 = cc0.DEBUG;
                    Object[] objArr2 = {h91, b2, Integer.valueOf(i3)};
                    this.a.post(new uu0(this, t31.c.a(i3), this.s));
                    this.a.post(new yn0(this, t31.c.a(i3), this.s));
                    a(i3, this.s);
                    a(this.s);
                } else {
                    this.s = b2;
                    cc0 cc03 = cc0.DEBUG;
                    Object[] objArr3 = {h91, b2, Integer.valueOf(i3)};
                    hr0 hr0 = this.e;
                    this.i.clear();
                    this.j = 20;
                    this.o = this.j;
                    this.e = new hr0(this);
                    a((ok0) new rp0(this.v));
                    this.a.post(new uu0(this, t31.c.a(i3), this.s));
                    this.a.post(new yn0(this, t31.c.a(i3), this.s));
                    this.a.post(new xl1(hr0));
                    a(i3, this.s);
                    a(this.s);
                }
                cd6 cd6 = cd6.a;
            }
        }
    }

    @DexIgnore
    public final void a(h91 h91) {
        for (j71 v11 : this.h) {
            try {
                this.a.post(new v11(v11, this, h91));
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(m51 m51, m51 m512) {
        this.a.post(new nw0(this, new t31(x11.SUCCESS, 0, 2), m51, m512));
        this.a.post(new qp0(this, new t31(x11.SUCCESS, 0, 2), m51, m512));
        for (j71 b01 : this.h) {
            try {
                this.a.post(new b01(b01, this, m51, m512));
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(k40.c cVar) {
        if (cVar != k40.c.ENABLED) {
            a(0, ao0.BLUETOOTH_OFF.a);
        }
    }

    @DexIgnore
    public final void a(List<? extends BluetoothGattService> list) {
        for (BluetoothGattService characteristics : list) {
            for (BluetoothGattCharacteristic next : characteristics.getCharacteristics()) {
                mi1 mi1 = gk1.b;
                UUID uuid = next.getUuid();
                wg6.a(uuid, "characteristic.uuid");
                rg1 a2 = mi1.a(uuid);
                if (a2 != rg1.UNKNOWN) {
                    this.i.put(a2, new gk1(next));
                }
            }
        }
    }

    @DexIgnore
    public final void a(ok0 ok0) {
        ok0.e = new nn1(this, ok0);
        ok0.f = new kf0(this, ok0);
        this.e.a(ok0);
    }

    @DexIgnore
    public final void a(ok0 ok0, lf0 lf0) {
        if (ok0 != null) {
            hr0 hr0 = this.e;
            if (!wg6.a(ok0, hr0.b)) {
                ok0.a((gg6<cd6>) pp0.a);
                hr0.a.remove(ok0);
            }
            ok0.a(lf0);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        BluetoothGatt bluetoothGatt;
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new yn0(this, new t31(x11.BLUETOOTH_OFF, 0, 2), h91.DISCONNECTED));
            return;
        }
        int i2 = db1.a[this.s.ordinal()];
        if (i2 == 1) {
            a(1, 0);
            BluetoothGatt bluetoothGatt2 = this.b;
            if (bluetoothGatt2 == null) {
                cc0 cc02 = cc0.DEBUG;
                if (Build.VERSION.SDK_INT >= 23) {
                    bluetoothGatt = this.w.connectGatt(gk0.f.a(), z, this.d, 2);
                } else {
                    bluetoothGatt = this.w.connectGatt(gk0.f.a(), z, this.d);
                }
                this.b = bluetoothGatt;
                if (this.b == null) {
                    cc0 cc03 = cc0.DEBUG;
                    a(0, ao0.START_FAIL.a);
                    return;
                }
                cc0 cc04 = cc0.DEBUG;
            } else if (true != bluetoothGatt2.connect()) {
                cc0 cc05 = cc0.DEBUG;
                a(0, ao0.START_FAIL.a);
            } else {
                cc0 cc06 = cc0.DEBUG;
            }
        } else if (i2 != 2 && i2 == 3) {
            if (this.b != null) {
                this.a.post(new yn0(this, new t31(x11.SUCCESS, 0, 2), h91.CONNECTED));
                return;
            }
            a(0, ao0.SUCCESS.a);
        }
    }

    @DexIgnore
    public final void a() {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            t31 t31 = new t31(x11.BLUETOOTH_OFF, 0, 2);
            m51 m51 = m51.DISCONNECTED;
            this.a.post(new qp0(this, t31, m51, m51));
            return;
        }
        int i2 = db1.c[e().ordinal()];
        if (i2 == 1) {
            t31 a2 = t31.c.a(s81.d.a(this.w));
            if (a2.a != x11.SUCCESS) {
                m51 m512 = m51.DISCONNECTED;
                this.a.post(new qp0(this, a2, m512, m512));
            }
        } else if (i2 != 2 && i2 == 3) {
            t31 t312 = new t31(x11.SUCCESS, 0, 2);
            m51 m513 = m51.CONNECTED;
            this.a.post(new qp0(this, t312, m513, m513));
        }
    }

    @DexIgnore
    public final boolean a(BluetoothGatt bluetoothGatt) {
        boolean z = false;
        Object obj = null;
        if (fm0.f.a()) {
            if (k40.l.d() != k40.c.ENABLED) {
                cc0 cc0 = cc0.DEBUG;
                obj = "Peripheral.refreshDeviceCache: Bluetooth Off.";
            } else {
                try {
                    cc0 cc02 = cc0.DEBUG;
                    Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                    wg6.a(method, "gatt.javaClass.getMethod(\"refresh\")");
                    Object invoke = method.invoke(bluetoothGatt, new Object[0]);
                    if (invoke != null) {
                        z = ((Boolean) invoke).booleanValue();
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Boolean");
                    }
                } catch (Exception e2) {
                    obj = e2.getLocalizedMessage();
                    if (obj == null) {
                        obj = e2.toString();
                    }
                    qs0.h.a(e2);
                }
            }
        }
        og0 og0 = og0.REQUEST;
        String str = this.t;
        String a2 = ze0.a("UUID.randomUUID().toString()");
        JSONObject jSONObject = new JSONObject();
        bm0 bm0 = bm0.ERROR_DETAIL;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        qs0.h.a(new nn0("refresh_device_cache", og0, str, "", a2, z, (String) null, (r40) null, (bw0) null, cw0.a(jSONObject, bm0, obj), 448));
        return z;
    }

    @DexIgnore
    public final void a(int i2) {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new i91(this, new t31(x11.BLUETOOTH_OFF, 0, 2), 0));
        } else if (i2 == this.j) {
            this.a.post(new i91(this, new t31(x11.SUCCESS, 0, 2), this.j));
        } else {
            BluetoothGatt bluetoothGatt = this.b;
            if (bluetoothGatt == null) {
                this.a.post(new i91(this, new t31(x11.GATT_NULL, 0, 2), this.j));
            } else if (true != bluetoothGatt.requestMtu(i2)) {
                this.a.post(new i91(this, new t31(x11.START_FAIL, 0, 2), this.j));
            }
        }
    }

    @DexIgnore
    public final void a(rg1 rg1) {
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            this.a.post(new r31(this, new t31(x11.BLUETOOTH_OFF, 0, 2), rg1, new byte[0]));
            return;
        }
        t31 t31 = new t31(x11.SUCCESS, 0, 2);
        if (this.b == null) {
            t31 = new t31(x11.GATT_NULL, 0, 2);
        } else {
            gk1 gk1 = this.i.get(rg1);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = gk1 != null ? gk1.a : null;
            if (bluetoothGattCharacteristic == null) {
                t31 = new t31(x11.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.b;
                if (bluetoothGatt == null || true != bluetoothGatt.readCharacteristic(bluetoothGattCharacteristic)) {
                    t31 = new t31(x11.START_FAIL, 0, 2);
                }
            }
        }
        if (t31.a != x11.SUCCESS) {
            this.a.post(new r31(this, t31, rg1, new byte[0]));
        }
    }

    @DexIgnore
    public final boolean a(rg1 rg1, boolean z) {
        BluetoothGatt bluetoothGatt;
        if (k40.l.d() != k40.c.ENABLED) {
            cc0 cc0 = cc0.DEBUG;
            return false;
        }
        gk1 gk1 = this.i.get(rg1);
        BluetoothGattCharacteristic bluetoothGattCharacteristic = gk1 != null ? gk1.a : null;
        if (bluetoothGattCharacteristic == null || (bluetoothGatt = this.b) == null || true != bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, z)) {
            return false;
        }
        return true;
    }
}
