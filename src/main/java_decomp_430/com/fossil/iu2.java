package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu2 implements dl2<lu2> {
    @DexIgnore
    public static iu2 b; // = new iu2();
    @DexIgnore
    public /* final */ dl2<lu2> a;

    @DexIgnore
    public iu2(dl2<lu2> dl2) {
        this.a = hl2.a(dl2);
    }

    @DexIgnore
    public static boolean a() {
        return ((lu2) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((lu2) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((lu2) b.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((lu2) b.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((lu2) b.zza()).zze();
    }

    @DexIgnore
    public static boolean f() {
        return ((lu2) b.zza()).zzf();
    }

    @DexIgnore
    public static boolean g() {
        return ((lu2) b.zza()).zzg();
    }

    @DexIgnore
    public static boolean h() {
        return ((lu2) b.zza()).zzh();
    }

    @DexIgnore
    public static boolean i() {
        return ((lu2) b.zza()).e();
    }

    @DexIgnore
    public final /* synthetic */ Object zza() {
        return this.a.zza();
    }

    @DexIgnore
    public iu2() {
        this(hl2.a(new ku2()));
    }
}
