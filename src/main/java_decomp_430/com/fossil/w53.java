package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w53 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ u53 b;

    @DexIgnore
    public w53(u53 u53, String str) {
        this.b = u53;
        w12.a(str);
        this.a = str;
    }

    @DexIgnore
    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.b.b().t().a(this.a, th);
    }
}
