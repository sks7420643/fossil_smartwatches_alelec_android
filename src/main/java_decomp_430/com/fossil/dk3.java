package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dk3<F, T> extends ak3<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ ck3<F, ? extends T> function;
    @DexIgnore
    public /* final */ ak3<T> resultEquivalence;

    @DexIgnore
    public dk3(ck3<F, ? extends T> ck3, ak3<T> ak3) {
        jk3.a(ck3);
        this.function = ck3;
        jk3.a(ak3);
        this.resultEquivalence = ak3;
    }

    @DexIgnore
    public boolean doEquivalent(F f, F f2) {
        return this.resultEquivalence.equivalent(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    public int doHash(F f) {
        return this.resultEquivalence.hash(this.function.apply(f));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dk3)) {
            return false;
        }
        dk3 dk3 = (dk3) obj;
        if (!this.function.equals(dk3.function) || !this.resultEquivalence.equals(dk3.resultEquivalence)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return gk3.a(this.function, this.resultEquivalence);
    }

    @DexIgnore
    public String toString() {
        return this.resultEquivalence + ".onResultOf(" + this.function + ")";
    }
}
