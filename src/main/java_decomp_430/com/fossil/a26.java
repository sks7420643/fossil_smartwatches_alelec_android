package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a26 {
    @DexIgnore
    public static a26 c;
    @DexIgnore
    public Map<Integer, z16> a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public a26(Context context) {
        this.b = context.getApplicationContext();
        this.a = new HashMap(3);
        this.a.put(1, new y16(context));
        this.a.put(2, new v16(context));
        this.a.put(4, new x16(context));
    }

    @DexIgnore
    public static synchronized a26 a(Context context) {
        a26 a26;
        synchronized (a26.class) {
            if (c == null) {
                c = new a26(context);
            }
            a26 = c;
        }
        return a26;
    }

    @DexIgnore
    public final w16 a() {
        return a((List<Integer>) new ArrayList(Arrays.asList(new Integer[]{1, 2, 4})));
    }

    @DexIgnore
    public final w16 a(List<Integer> list) {
        w16 c2;
        if (list.size() >= 0) {
            for (Integer num : list) {
                z16 z16 = this.a.get(num);
                if (z16 != null && (c2 = z16.c()) != null && b26.b(c2.c)) {
                    return c2;
                }
            }
        }
        return new w16();
    }

    @DexIgnore
    public final void a(String str) {
        w16 a2 = a();
        a2.c = str;
        if (!b26.a(a2.a)) {
            a2.a = b26.a(this.b);
        }
        if (!b26.a(a2.b)) {
            a2.b = b26.b(this.b);
        }
        a2.d = System.currentTimeMillis();
        for (Map.Entry<Integer, z16> value : this.a.entrySet()) {
            ((z16) value.getValue()).a(a2);
        }
    }
}
