package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qd4 extends pd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public long t;

    /*
    static {
        v.put(2131362442, 1);
        v.put(2131362692, 2);
        v.put(2131362755, 3);
        v.put(2131362223, 4);
    }
    */

    @DexIgnore
    public qd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 5, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public qd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[4], objArr[1], objArr[2], objArr[3]);
        this.t = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
