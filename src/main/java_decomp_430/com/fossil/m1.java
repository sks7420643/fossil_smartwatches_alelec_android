package com.fossil;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public Map<v7, MenuItem> b;
    @DexIgnore
    public Map<w7, SubMenu> c;

    @DexIgnore
    public m1(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof v7)) {
            return menuItem;
        }
        v7 v7Var = (v7) menuItem;
        if (this.b == null) {
            this.b = new p4();
        }
        MenuItem menuItem2 = this.b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        u1 u1Var = new u1(this.a, v7Var);
        this.b.put(v7Var, u1Var);
        return u1Var;
    }

    @DexIgnore
    public final void b() {
        Map<v7, MenuItem> map = this.b;
        if (map != null) {
            map.clear();
        }
        Map<w7, SubMenu> map2 = this.c;
        if (map2 != null) {
            map2.clear();
        }
    }

    @DexIgnore
    public final void b(int i) {
        Map<v7, MenuItem> map = this.b;
        if (map != null) {
            Iterator<v7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof w7)) {
            return subMenu;
        }
        w7 w7Var = (w7) subMenu;
        if (this.c == null) {
            this.c = new p4();
        }
        SubMenu subMenu2 = this.c.get(w7Var);
        if (subMenu2 != null) {
            return subMenu2;
        }
        d2 d2Var = new d2(this.a, w7Var);
        this.c.put(w7Var, d2Var);
        return d2Var;
    }

    @DexIgnore
    public final void a(int i) {
        Map<v7, MenuItem> map = this.b;
        if (map != null) {
            Iterator<v7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }
}
