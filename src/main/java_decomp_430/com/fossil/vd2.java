package com.fossil;

import android.os.RemoteException;
import com.fossil.rv1;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vd2 extends hc2 {
    @DexIgnore
    public /* final */ /* synthetic */ DataSet s;
    @DexIgnore
    public /* final */ /* synthetic */ boolean t; // = false;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vd2(wd2 wd2, wv1 wv1, DataSet dataSet, boolean z) {
        super(wv1);
        this.s = dataSet;
    }

    @DexIgnore
    public final /* synthetic */ void a(rv1.b bVar) throws RemoteException {
        ((gd2) ((ac2) bVar).y()).a(new n82(this.s, (nd2) new be2(this), this.t));
    }
}
