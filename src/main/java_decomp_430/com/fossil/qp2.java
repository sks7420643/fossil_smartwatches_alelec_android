package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qp2 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ jp2 a;

    @DexIgnore
    public qp2(jp2 jp2) {
        this.a = jp2;
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.a.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    @DexIgnore
    public void clear() {
        this.a.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.a.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> iterator() {
        return new rp2(this.a, (ip2) null);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.a.remove(entry.getKey());
        return true;
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public /* synthetic */ qp2(jp2 jp2, ip2 ip2) {
        this(jp2);
    }
}
