package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ii6<R> extends ei6<R>, fc6<R> {
    @DexIgnore
    boolean isExternal();

    @DexIgnore
    boolean isInfix();

    @DexIgnore
    boolean isInline();

    @DexIgnore
    boolean isOperator();

    @DexIgnore
    boolean isSuspend();
}
