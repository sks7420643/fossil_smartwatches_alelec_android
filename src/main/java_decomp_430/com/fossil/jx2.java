package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx2 {
    @DexIgnore
    public /* final */ rx2 a;
    @DexIgnore
    public px2 b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onCancel();

        @DexIgnore
        void onFinish();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void l();
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ny2 {
        @DexIgnore
        public /* final */ a a;

        @DexIgnore
        public d(a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public final void onCancel() {
            this.a.onCancel();
        }

        @DexIgnore
        public final void onFinish() {
            this.a.onFinish();
        }
    }

    @DexIgnore
    public jx2(rx2 rx2) {
        w12.a(rx2);
        this.a = rx2;
    }

    @DexIgnore
    public final void a(hx2 hx2) {
        try {
            this.a.c(hx2.a());
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final CameraPosition b() {
        try {
            return this.a.n();
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final px2 c() {
        try {
            if (this.b == null) {
                this.b = new px2(this.a.o());
            }
            return this.b;
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final void a(hx2 hx2, a aVar) {
        d dVar;
        try {
            rx2 rx2 = this.a;
            x52 a2 = hx2.a();
            if (aVar == null) {
                dVar = null;
            } else {
                dVar = new d(aVar);
            }
            rx2.a(a2, dVar);
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final void b(hx2 hx2) {
        try {
            this.a.a(hx2.a());
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final zy2 a(az2 az2) {
        try {
            ph2 a2 = this.a.a(az2);
            if (a2 != null) {
                return new zy2(a2);
            }
            return null;
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final void a() {
        try {
            this.a.clear();
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        if (cVar == null) {
            try {
                this.a.a((vy2) null);
            } catch (RemoteException e) {
                throw new bz2(e);
            }
        } else {
            this.a.a((vy2) new rz2(this, cVar));
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        if (bVar == null) {
            try {
                this.a.a((ty2) null);
            } catch (RemoteException e) {
                throw new bz2(e);
            }
        } else {
            this.a.a((ty2) new sz2(this, bVar));
        }
    }
}
