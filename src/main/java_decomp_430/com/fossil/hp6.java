package com.fossil;

import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hp6 extends jm6 {
    @DexIgnore
    public ep6 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public hp6(int i, int i2, long j, String str) {
        wg6.b(str, "schedulerName");
        this.b = i;
        this.c = i2;
        this.d = j;
        this.e = str;
        this.a = o();
    }

    @DexIgnore
    public void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        try {
            ep6.a(this.a, runnable, (np6) null, false, 6, (Object) null);
        } catch (RejectedExecutionException unused) {
            pl6.g.a(af6, runnable);
        }
    }

    @DexIgnore
    public void b(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        try {
            ep6.a(this.a, runnable, (np6) null, true, 2, (Object) null);
        } catch (RejectedExecutionException unused) {
            pl6.g.b(af6, runnable);
        }
    }

    @DexIgnore
    public final ep6 o() {
        return new ep6(this.b, this.c, this.d, this.e);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ hp6(int i, int i2, String str, int i3, qg6 qg6) {
        this((i3 & 1) != 0 ? qp6.c : i, (i3 & 2) != 0 ? qp6.d : i2, (i3 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    public final void a(Runnable runnable, np6 np6, boolean z) {
        wg6.b(runnable, "block");
        wg6.b(np6, "context");
        try {
            this.a.a(runnable, np6, z);
        } catch (RejectedExecutionException unused) {
            pl6.g.a((Runnable) this.a.a(runnable, np6));
        }
    }

    @DexIgnore
    public final dl6 b(int i) {
        if (i > 0) {
            return new jp6(this, i, pp6.PROBABLY_BLOCKING);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i).toString());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public hp6(int i, int i2, String str) {
        this(i, i2, qp6.e, str);
        wg6.b(str, "schedulerName");
    }
}
