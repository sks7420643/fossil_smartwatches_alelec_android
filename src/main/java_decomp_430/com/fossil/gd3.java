package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gd3<TResult> implements kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public mc3<? super TResult> c;

    @DexIgnore
    public gd3(Executor executor, mc3<? super TResult> mc3) {
        this.a = executor;
        this.c = mc3;
    }

    @DexIgnore
    public final void onComplete(qc3<TResult> qc3) {
        if (qc3.e()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new hd3(this, qc3));
                }
            }
        }
    }
}
