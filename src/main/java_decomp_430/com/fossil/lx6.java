package com.fossil;

import com.fossil.dq6;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx6<T> implements Call<T> {
    @DexIgnore
    public /* final */ qx6 a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ dq6.a c;
    @DexIgnore
    public /* final */ fx6<zq6, T> d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public dq6 f;
    @DexIgnore
    public Throwable g;
    @DexIgnore
    public boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements eq6 {
        @DexIgnore
        public /* final */ /* synthetic */ dx6 a;

        @DexIgnore
        public a(dx6 dx6) {
            this.a = dx6;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.a.onFailure(lx6.this, th);
            } catch (Throwable th2) {
                vx6.a(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        public void onFailure(dq6 dq6, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        public void onResponse(dq6 dq6, Response response) {
            try {
                try {
                    this.a.onResponse(lx6.this, lx6.this.a(response));
                } catch (Throwable th) {
                    vx6.a(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                vx6.a(th2);
                a(th2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zq6 {
        @DexIgnore
        public /* final */ zq6 a;
        @DexIgnore
        public /* final */ lt6 b;
        @DexIgnore
        public IOException c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ot6 {
            @DexIgnore
            public a(zt6 zt6) {
                super(zt6);
            }

            @DexIgnore
            public long b(jt6 jt6, long j) throws IOException {
                try {
                    return b.super.b(jt6, j);
                } catch (IOException e) {
                    b.this.c = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public b(zq6 zq6) {
            this.a = zq6;
            this.b = st6.a(new a(zq6.source()));
        }

        @DexIgnore
        public void close() {
            this.a.close();
        }

        @DexIgnore
        public long contentLength() {
            return this.a.contentLength();
        }

        @DexIgnore
        public uq6 contentType() {
            return this.a.contentType();
        }

        @DexIgnore
        public void k() throws IOException {
            IOException iOException = this.c;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        public lt6 source() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zq6 {
        @DexIgnore
        public /* final */ uq6 a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public c(uq6 uq6, long j) {
            this.a = uq6;
            this.b = j;
        }

        @DexIgnore
        public long contentLength() {
            return this.b;
        }

        @DexIgnore
        public uq6 contentType() {
            return this.a;
        }

        @DexIgnore
        public lt6 source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public lx6(qx6 qx6, Object[] objArr, dq6.a aVar, fx6<zq6, T> fx6) {
        this.a = qx6;
        this.b = objArr;
        this.c = aVar;
        this.d = fx6;
    }

    @DexIgnore
    public void a(dx6<T> dx6) {
        dq6 dq6;
        Throwable th;
        vx6.a(dx6, "callback == null");
        synchronized (this) {
            if (!this.h) {
                this.h = true;
                dq6 = this.f;
                th = this.g;
                if (dq6 == null && th == null) {
                    try {
                        dq6 a2 = a();
                        this.f = a2;
                        dq6 = a2;
                    } catch (Throwable th2) {
                        th = th2;
                        vx6.a(th);
                        this.g = th;
                    }
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            dx6.onFailure(this, th);
            return;
        }
        if (this.e) {
            dq6.cancel();
        }
        dq6.a(new a(dx6));
    }

    @DexIgnore
    public void cancel() {
        dq6 dq6;
        this.e = true;
        synchronized (this) {
            dq6 = this.f;
        }
        if (dq6 != null) {
            dq6.cancel();
        }
    }

    @DexIgnore
    public rx6<T> s() throws IOException {
        dq6 dq6;
        synchronized (this) {
            if (!this.h) {
                this.h = true;
                if (this.g == null) {
                    dq6 = this.f;
                    if (dq6 == null) {
                        try {
                            dq6 = a();
                            this.f = dq6;
                        } catch (IOException | Error | RuntimeException e2) {
                            vx6.a(e2);
                            this.g = e2;
                            throw e2;
                        }
                    }
                } else if (this.g instanceof IOException) {
                    throw ((IOException) this.g);
                } else if (this.g instanceof RuntimeException) {
                    throw ((RuntimeException) this.g);
                } else {
                    throw ((Error) this.g);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.e) {
            dq6.cancel();
        }
        return a(dq6.s());
    }

    @DexIgnore
    public synchronized yq6 t() {
        dq6 dq6 = this.f;
        if (dq6 != null) {
            return dq6.t();
        } else if (this.g == null) {
            try {
                dq6 a2 = a();
                this.f = a2;
                return a2.t();
            } catch (RuntimeException e2) {
                e = e2;
                vx6.a(e);
                this.g = e;
                throw e;
            } catch (Error e3) {
                e = e3;
                vx6.a(e);
                this.g = e;
                throw e;
            } catch (IOException e4) {
                this.g = e4;
                throw new RuntimeException("Unable to create request.", e4);
            }
        } else if (this.g instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.g);
        } else if (this.g instanceof RuntimeException) {
            throw ((RuntimeException) this.g);
        } else {
            throw ((Error) this.g);
        }
    }

    @DexIgnore
    public boolean v() {
        boolean z = true;
        if (this.e) {
            return true;
        }
        synchronized (this) {
            if (this.f == null || !this.f.v()) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public lx6<T> clone() {
        return new lx6<>(this.a, this.b, this.c, this.d);
    }

    @DexIgnore
    public final dq6 a() throws IOException {
        dq6 a2 = this.c.a(this.a.a(this.b));
        if (a2 != null) {
            return a2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    public rx6<T> a(Response response) throws IOException {
        zq6 k = response.k();
        Response.a E = response.E();
        E.a(new c(k.contentType(), k.contentLength()));
        Response a2 = E.a();
        int n = a2.n();
        if (n < 200 || n >= 300) {
            try {
                return rx6.a(vx6.a(k), a2);
            } finally {
                k.close();
            }
        } else if (n == 204 || n == 205) {
            k.close();
            return rx6.a(null, a2);
        } else {
            b bVar = new b(k);
            try {
                return rx6.a(this.d.a(bVar), a2);
            } catch (RuntimeException e2) {
                bVar.k();
                throw e2;
            }
        }
    }
}
