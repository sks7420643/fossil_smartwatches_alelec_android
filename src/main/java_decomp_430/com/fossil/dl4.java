package com.fossil;

import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl4 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = qd6.a((T[]) new String[]{"weather", "commute-time"});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = qd6.a((T[]) new String[]{"commute-time"});
    @DexIgnore
    public static /* final */ dl4 c; // = new dl4();

    /*
    static {
        qd6.a((T[]) new String[]{Constants.MUSIC, "weather", "commute-time"});
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(String str) {
        wg6.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886364);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return a2;
            }
        } else if (str.equals("commute-time")) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886202);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r4.equals("commute-time") != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r4.equals("weather") == false) goto L_0x0077;
     */
    @DexIgnore
    public final List<String> b(String str) {
        wg6.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode != 104263205) {
                if (hashCode == 1223440372) {
                }
            } else if (str.equals(Constants.MUSIC)) {
                return qd6.a((T[]) new String[]{InAppPermission.NOTIFICATION_ACCESS});
            }
            return new ArrayList();
        }
        int i = Build.VERSION.SDK_INT;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
        if (i >= 29) {
            return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
        }
        return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
    }

    @DexIgnore
    public final Integer c(String str) {
        wg6.b(str, "watchAppId");
        return (str.hashCode() == -829740640 && str.equals("commute-time")) ? 2131820544 : null;
    }

    @DexIgnore
    public final boolean d(String str) {
        wg6.b(str, "watchAppId");
        List<String> b2 = b(str);
        String[] a2 = xx5.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "isPermissionGrantedForWatchApp " + str + " granted=" + a2 + " required=" + b2);
        for (String a3 : b2) {
            if (!nd6.a((T[]) a2, a3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean e(String str) {
        wg6.b(str, "watchAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean f(String str) {
        wg6.b(str, "watchAppId");
        return a.contains(str);
    }
}
