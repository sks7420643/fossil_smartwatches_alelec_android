package com.fossil;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l96 implements Closeable {
    @DexIgnore
    public static /* final */ Logger g; // = Logger.getLogger(l96.class.getName());
    @DexIgnore
    public /* final */ RandomAccessFile a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public b d;
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ byte[] f; // = new byte[16];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements d {
        @DexIgnore
        public boolean a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder b;

        @DexIgnore
        public a(l96 l96, StringBuilder sb) {
            this.b = sb;
        }

        @DexIgnore
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.a) {
                this.a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ b c; // = new b(0, 0);
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.a + ", length = " + this.b + "]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends InputStream {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public /* synthetic */ c(l96 l96, b bVar, a aVar) {
            this(bVar);
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws IOException {
            Object unused = l96.b(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = this.b;
            if (i3 <= 0) {
                return -1;
            }
            if (i2 > i3) {
                i2 = i3;
            }
            l96.this.a(this.a, bArr, i, i2);
            this.a = l96.this.e(this.a + i2);
            this.b -= i2;
            return i2;
        }

        @DexIgnore
        public c(b bVar) {
            this.a = l96.this.e(bVar.a + 4);
            this.b = bVar.b;
        }

        @DexIgnore
        public int read() throws IOException {
            if (this.b == 0) {
                return -1;
            }
            l96.this.a.seek((long) this.a);
            int read = l96.this.a.read();
            this.a = l96.this.e(this.a + 1);
            this.b--;
            return read;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(InputStream inputStream, int i) throws IOException;
    }

    @DexIgnore
    public l96(File file) throws IOException {
        if (!file.exists()) {
            a(file);
        }
        this.a = b(file);
        m();
    }

    @DexIgnore
    public static void b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    @DexIgnore
    public final b c(int i) throws IOException {
        if (i == 0) {
            return b.c;
        }
        this.a.seek((long) i);
        return new b(i, this.a.readInt());
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public final void d(int i) throws IOException {
        this.a.setLength((long) i);
        this.a.getChannel().force(true);
    }

    @DexIgnore
    public final int e(int i) {
        int i2 = this.b;
        return i < i2 ? i : (i + 16) - i2;
    }

    @DexIgnore
    public synchronized void k() throws IOException {
        a(4096, 0, 0, 0);
        this.c = 0;
        this.d = b.c;
        this.e = b.c;
        if (this.b > 4096) {
            d(4096);
        }
        this.b = 4096;
    }

    @DexIgnore
    public synchronized boolean l() {
        return this.c == 0;
    }

    @DexIgnore
    public final void m() throws IOException {
        this.a.seek(0);
        this.a.readFully(this.f);
        this.b = a(this.f, 0);
        if (((long) this.b) <= this.a.length()) {
            this.c = a(this.f, 4);
            int a2 = a(this.f, 8);
            int a3 = a(this.f, 12);
            this.d = c(a2);
            this.e = c(a3);
            return;
        }
        throw new IOException("File is truncated. Expected length: " + this.b + ", Actual length: " + this.a.length());
    }

    @DexIgnore
    public final int n() {
        return this.b - p();
    }

    @DexIgnore
    public synchronized void o() throws IOException {
        if (l()) {
            throw new NoSuchElementException();
        } else if (this.c == 1) {
            k();
        } else {
            int e2 = e(this.d.a + 4 + this.d.b);
            a(e2, this.f, 0, 4);
            int a2 = a(this.f, 0);
            a(this.b, this.c - 1, e2, this.e.a);
            this.c--;
            this.d = new b(e2, a2);
        }
    }

    @DexIgnore
    public int p() {
        if (this.c == 0) {
            return 16;
        }
        b bVar = this.e;
        int i = bVar.a;
        int i2 = this.d.a;
        if (i >= i2) {
            return (i - i2) + 4 + bVar.b + 16;
        }
        return (((i + 4) + bVar.b) + this.b) - i2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(l96.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.b);
        sb.append(", size=");
        sb.append(this.c);
        sb.append(", first=");
        sb.append(this.d);
        sb.append(", last=");
        sb.append(this.e);
        sb.append(", element lengths=[");
        try {
            a((d) new a(this, sb));
        } catch (IOException e2) {
            g.log(Level.WARNING, "read error", e2);
        }
        sb.append("]]");
        return sb.toString();
    }

    @DexIgnore
    public static void a(byte[] bArr, int... iArr) {
        int i = 0;
        for (int b2 : iArr) {
            b(bArr, i, b2);
            i += 4;
        }
    }

    @DexIgnore
    public static RandomAccessFile b(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    @DexIgnore
    public final void b(int i, byte[] bArr, int i2, int i3) throws IOException {
        int e2 = e(i);
        int i4 = e2 + i3;
        int i5 = this.b;
        if (i4 <= i5) {
            this.a.seek((long) e2);
            this.a.write(bArr, i2, i3);
            return;
        }
        int i6 = i5 - e2;
        this.a.seek((long) e2);
        this.a.write(bArr, i2, i6);
        this.a.seek(16);
        this.a.write(bArr, i2 + i6, i3 - i6);
    }

    @DexIgnore
    public static int a(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    @DexIgnore
    public final void a(int i, int i2, int i3, int i4) throws IOException {
        a(this.f, i, i2, i3, i4);
        this.a.seek(0);
        this.a.write(this.f);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void a(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b2 = b(file2);
        try {
            b2.setLength(4096);
            b2.seek(0);
            byte[] bArr = new byte[16];
            a(bArr, 4096, 0, 0, 0);
            b2.write(bArr);
            b2.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }

    @DexIgnore
    public final void b(int i) throws IOException {
        int i2 = i + 4;
        int n = n();
        if (n < i2) {
            int i3 = this.b;
            do {
                n += i3;
                i3 <<= 1;
            } while (n < i2);
            d(i3);
            b bVar = this.e;
            int e2 = e(bVar.a + 4 + bVar.b);
            if (e2 < this.d.a) {
                FileChannel channel = this.a.getChannel();
                channel.position((long) this.b);
                long j = (long) (e2 - 4);
                if (channel.transferTo(16, j, channel) != j) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i4 = this.e.a;
            int i5 = this.d.a;
            if (i4 < i5) {
                int i6 = (this.b + i4) - 16;
                a(i3, this.c, i5, i6);
                this.e = new b(i6, this.e.b);
            } else {
                a(i3, this.c, i5, i4);
            }
            this.b = i3;
        }
    }

    @DexIgnore
    public final void a(int i, byte[] bArr, int i2, int i3) throws IOException {
        int e2 = e(i);
        int i4 = e2 + i3;
        int i5 = this.b;
        if (i4 <= i5) {
            this.a.seek((long) e2);
            this.a.readFully(bArr, i2, i3);
            return;
        }
        int i6 = i5 - e2;
        this.a.seek((long) e2);
        this.a.readFully(bArr, i2, i6);
        this.a.seek(16);
        this.a.readFully(bArr, i2 + i6, i3 - i6);
    }

    @DexIgnore
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized void a(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        b(bArr, "buffer");
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        b(i2);
        boolean l = l();
        if (l) {
            i3 = 16;
        } else {
            i3 = e(this.e.a + 4 + this.e.b);
        }
        b bVar = new b(i3, i2);
        b(this.f, 0, i2);
        b(bVar.a, this.f, 0, 4);
        b(bVar.a + 4, bArr, i, i2);
        a(this.b, this.c + 1, l ? bVar.a : this.d.a, bVar.a);
        this.e = bVar;
        this.c++;
        if (l) {
            this.d = this.e;
        }
    }

    @DexIgnore
    public synchronized void a(d dVar) throws IOException {
        int i = this.d.a;
        for (int i2 = 0; i2 < this.c; i2++) {
            b c2 = c(i);
            dVar.a(new c(this, c2, (a) null), c2.b);
            i = e(c2.a + 4 + c2.b);
        }
    }

    @DexIgnore
    public boolean a(int i, int i2) {
        return (p() + 4) + i <= i2;
    }
}
