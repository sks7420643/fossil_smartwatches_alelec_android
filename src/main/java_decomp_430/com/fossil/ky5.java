package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.portfolio.platform.view.ColorPanelView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ky5 extends BaseAdapter {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {
        @DexIgnore
        public View a;
        @DexIgnore
        public ColorPanelView b;
        @DexIgnore
        public ImageView c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public a(int i) {
                this.a = i;
            }

            @DexIgnore
            public void onClick(View view) {
                ky5 ky5 = ky5.this;
                int i = ky5.c;
                int i2 = this.a;
                if (i != i2) {
                    ky5.c = i2;
                    ky5.notifyDataSetChanged();
                }
                ky5 ky52 = ky5.this;
                ky52.a.a(ky52.b[this.a]);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ky5$b$b")
        /* renamed from: com.fossil.ky5$b$b  reason: collision with other inner class name */
        public class C0022b implements View.OnLongClickListener {
            @DexIgnore
            public C0022b() {
            }

            @DexIgnore
            public boolean onLongClick(View view) {
                b.this.b.c();
                return true;
            }
        }

        @DexIgnore
        public b(Context context) {
            this.a = View.inflate(context, ky5.this.d == 0 ? 2131558454 : 2131558453, (ViewGroup) null);
            this.b = (ColorPanelView) this.a.findViewById(2131362127);
            this.c = (ImageView) this.a.findViewById(2131362124);
            this.d = this.b.getBorderColor();
            this.a.setTag(this);
        }

        @DexIgnore
        public final void a(int i) {
            ky5 ky5 = ky5.this;
            if (i != ky5.c || f7.a(ky5.b[i]) < 0.65d) {
                this.c.setColorFilter((ColorFilter) null);
            } else {
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            }
        }

        @DexIgnore
        public final void b(int i) {
            this.b.setOnClickListener(new a(i));
            this.b.setOnLongClickListener(new C0022b());
        }

        @DexIgnore
        public void c(int i) {
            int i2 = ky5.this.b[i];
            int alpha = Color.alpha(i2);
            this.b.setColor(i2);
            this.c.setImageResource(ky5.this.c == i ? 2131230974 : 0);
            if (alpha == 255) {
                a(i);
            } else if (alpha <= 165) {
                this.b.setBorderColor(i2 | -16777216);
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            } else {
                this.b.setBorderColor(this.d);
                this.c.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
            }
            b(i);
        }
    }

    @DexIgnore
    public ky5(a aVar, int[] iArr, int i, int i2) {
        this.a = aVar;
        this.b = iArr;
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    public void a() {
        this.c = -1;
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getCount() {
        return this.b.length;
    }

    @DexIgnore
    public Object getItem(int i) {
        return Integer.valueOf(this.b[i]);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        b bVar;
        if (view == null) {
            bVar = new b(viewGroup.getContext());
            view2 = bVar.a;
        } else {
            view2 = view;
            bVar = (b) view.getTag();
        }
        bVar.c(i);
        return view2;
    }
}
