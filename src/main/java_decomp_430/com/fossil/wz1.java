package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz1 implements Runnable {
    @DexIgnore
    public /* final */ xz1 a;
    @DexIgnore
    public /* final */ /* synthetic */ vz1 b;

    @DexIgnore
    public wz1(vz1 vz1, xz1 xz1) {
        this.b = vz1;
        this.a = xz1;
    }

    @DexIgnore
    public final void run() {
        if (this.b.b) {
            gv1 a2 = this.a.a();
            if (a2.D()) {
                vz1 vz1 = this.b;
                vz1.a.startActivityForResult(GoogleApiActivity.a(vz1.a(), a2.C(), this.a.b(), false), 1);
            } else if (this.b.e.c(a2.p())) {
                vz1 vz12 = this.b;
                vz12.e.a(vz12.a(), this.b.a, a2.p(), 2, this.b);
            } else if (a2.p() == 18) {
                Dialog a3 = jv1.a(this.b.a(), (DialogInterface.OnCancelListener) this.b);
                vz1 vz13 = this.b;
                vz13.e.a(vz13.a().getApplicationContext(), (uy1) new yz1(this, a3));
            } else {
                this.b.a(a2, this.a.b());
            }
        }
    }
}
