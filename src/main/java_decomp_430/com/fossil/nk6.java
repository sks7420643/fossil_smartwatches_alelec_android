package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nk6 {
    @DexIgnore
    public static final void a(lk6<?> lk6, jo6 jo6) {
        wg6.b(lk6, "$this$removeOnCancellation");
        wg6.b(jo6, "node");
        lk6.b((hg6<? super Throwable, cd6>) new hn6(jo6));
    }

    @DexIgnore
    public static final void a(lk6<?> lk6, am6 am6) {
        wg6.b(lk6, "$this$disposeOnCancellation");
        wg6.b(am6, "handle");
        lk6.b((hg6<? super Throwable, cd6>) new bm6(am6));
    }
}
