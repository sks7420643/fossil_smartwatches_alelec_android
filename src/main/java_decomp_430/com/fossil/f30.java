package com.fossil;

import com.facebook.stetho.dumpapp.DumpappHttpSocketLikeHandler;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f30 extends r86 implements d30 {
    @DexIgnore
    public f30(i86 i86, String str, String str2, va6 va6) {
        super(i86, str, str2, va6, ta6.POST);
    }

    @DexIgnore
    public boolean a(c30 c30) {
        ua6 a = a();
        a(a, c30);
        a(a, c30.b);
        l86 g = c86.g();
        g.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a.g();
        l86 g3 = c86.g();
        g3.d("CrashlyticsCore", "Create report request ID: " + a.c("X-REQUEST-ID"));
        l86 g4 = c86.g();
        g4.d("CrashlyticsCore", "Result was: " + g2);
        return m96.a(g2) == 0;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, c30 c30) {
        ua6.c("X-CRASHLYTICS-API-KEY", c30.a);
        ua6.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        ua6.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.j());
        for (Map.Entry<String, String> a : c30.b.a().entrySet()) {
            ua6.a(a);
        }
        return ua6;
    }

    @DexIgnore
    public final ua6 a(ua6 ua6, y30 y30) {
        ua6.e("report[identifier]", y30.b());
        if (y30.d().length == 1) {
            c86.g().d("CrashlyticsCore", "Adding single file " + y30.e() + " to report " + y30.b());
            ua6.a("report[file]", y30.e(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, y30.c());
            return ua6;
        }
        int i = 0;
        for (File file : y30.d()) {
            c86.g().d("CrashlyticsCore", "Adding file " + file.getName() + " to report " + y30.b());
            StringBuilder sb = new StringBuilder();
            sb.append("report[file");
            sb.append(i);
            sb.append("]");
            ua6.a(sb.toString(), file.getName(), DumpappHttpSocketLikeHandler.DumpappLegacyHttpHandler.CONTENT_TYPE, file);
            i++;
        }
        return ua6;
    }
}
