package com.fossil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ua6 {
    @DexIgnore
    public static c k; // = c.a;
    @DexIgnore
    public HttpURLConnection a; // = null;
    @DexIgnore
    public /* final */ URL b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public f d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public int h; // = 8192;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends b<ua6> {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream c;
        @DexIgnore
        public /* final */ /* synthetic */ OutputStream d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Closeable closeable, boolean z, InputStream inputStream, OutputStream outputStream) {
            super(closeable, z);
            this.c = inputStream;
            this.d = outputStream;
        }

        @DexIgnore
        public ua6 b() throws IOException {
            byte[] bArr = new byte[ua6.this.h];
            while (true) {
                int read = this.c.read(bArr);
                if (read == -1) {
                    return ua6.this;
                }
                this.d.write(bArr, 0, read);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<V> extends e<V> {
        @DexIgnore
        public /* final */ Closeable a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(Closeable closeable, boolean z) {
            this.a = closeable;
            this.b = z;
        }

        @DexIgnore
        public void a() throws IOException {
            Closeable closeable = this.a;
            if (closeable instanceof Flushable) {
                ((Flushable) closeable).flush();
            }
            if (this.b) {
                try {
                    this.a.close();
                } catch (IOException unused) {
                }
            } else {
                this.a.close();
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        public static final c a = new a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements c {
            @DexIgnore
            public HttpURLConnection a(URL url) throws IOException {
                return (HttpURLConnection) url.openConnection();
            }

            @DexIgnore
            public HttpURLConnection a(URL url, Proxy proxy) throws IOException {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        }

        @DexIgnore
        HttpURLConnection a(URL url) throws IOException;

        @DexIgnore
        HttpURLConnection a(URL url, Proxy proxy) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends RuntimeException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = -1170466989781746231L;

        @DexIgnore
        public d(IOException iOException) {
            super(iOException);
        }

        @DexIgnore
        public IOException getCause() {
            return (IOException) super.getCause();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<V> implements Callable<V> {
        @DexIgnore
        public abstract void a() throws IOException;

        @DexIgnore
        public abstract V b() throws d, IOException;

        @DexIgnore
        public V call() throws d {
            boolean z = true;
            try {
                V b = b();
                try {
                    a();
                    return b;
                } catch (IOException e) {
                    throw new d(e);
                }
            } catch (d e2) {
                throw e2;
            } catch (IOException e3) {
                throw new d(e3);
            } catch (Throwable th) {
                th = th;
                a();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends BufferedOutputStream {
        @DexIgnore
        public /* final */ CharsetEncoder a;

        @DexIgnore
        public f(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.a = Charset.forName(ua6.f(str)).newEncoder();
        }

        @DexIgnore
        public f e(String str) throws IOException {
            ByteBuffer encode = this.a.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    @DexIgnore
    public ua6(CharSequence charSequence, String str) throws d {
        try {
            this.b = new URL(charSequence.toString());
            this.c = str;
        } catch (MalformedURLException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public static StringBuilder b(String str, StringBuilder sb) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            sb.append('/');
        }
        return sb;
    }

    @DexIgnore
    public static String c(CharSequence charSequence) throws d {
        int i2;
        try {
            URL url = new URL(charSequence.toString());
            String host = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                host = host + ':' + Integer.toString(port);
            }
            try {
                String aSCIIString = new URI(url.getProtocol(), host, url.getPath(), url.getQuery(), url.getRef()).toASCIIString();
                int indexOf = aSCIIString.indexOf(63);
                if (indexOf <= 0 || (i2 = indexOf + 1) >= aSCIIString.length()) {
                    return aSCIIString;
                }
                return aSCIIString.substring(0, i2) + aSCIIString.substring(i2).replace("+", "%2B").replace("#", "%23");
            } catch (URISyntaxException e2) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(e2);
                throw new d(iOException);
            }
        } catch (IOException e3) {
            throw new d(e3);
        }
    }

    @DexIgnore
    public static ua6 d(CharSequence charSequence) throws d {
        return new ua6(charSequence, "GET");
    }

    @DexIgnore
    public static String f(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    @DexIgnore
    public int g() throws d {
        try {
            e();
            return l().getResponseCode();
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public String h() {
        return c("Content-Encoding");
    }

    @DexIgnore
    public int i() {
        return d("Content-Length");
    }

    @DexIgnore
    public final HttpURLConnection j() {
        HttpURLConnection httpURLConnection;
        try {
            if (this.i != null) {
                httpURLConnection = k.a(this.b, k());
            } else {
                httpURLConnection = k.a(this.b);
            }
            httpURLConnection.setRequestMethod(this.c);
            return httpURLConnection;
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public final Proxy k() {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.i, this.j));
    }

    @DexIgnore
    public HttpURLConnection l() {
        if (this.a == null) {
            this.a = j();
        }
        return this.a;
    }

    @DexIgnore
    public String m() {
        return l().getRequestMethod();
    }

    @DexIgnore
    public ua6 n() throws IOException {
        if (this.d != null) {
            return this;
        }
        l().setDoOutput(true);
        this.d = new f(l().getOutputStream(), b(l().getRequestProperty("Content-Type"), "charset"), this.h);
        return this;
    }

    @DexIgnore
    public ua6 o() throws IOException {
        if (!this.e) {
            this.e = true;
            b("multipart/form-data; boundary=00content0boundary00");
            n();
            this.d.e("--00content0boundary00\r\n");
        } else {
            this.d.e("\r\n--00content0boundary00\r\n");
        }
        return this;
    }

    @DexIgnore
    public InputStream p() throws d {
        InputStream inputStream;
        if (g() < 400) {
            try {
                inputStream = l().getInputStream();
            } catch (IOException e2) {
                throw new d(e2);
            }
        } else {
            inputStream = l().getErrorStream();
            if (inputStream == null) {
                try {
                    inputStream = l().getInputStream();
                } catch (IOException e3) {
                    throw new d(e3);
                }
            }
        }
        if (!this.g || !"gzip".equals(h())) {
            return inputStream;
        }
        try {
            return new GZIPInputStream(inputStream);
        } catch (IOException e4) {
            throw new d(e4);
        }
    }

    @DexIgnore
    public URL q() {
        return l().getURL();
    }

    @DexIgnore
    public String toString() {
        return m() + ' ' + q();
    }

    @DexIgnore
    public static StringBuilder a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(63);
        int length = sb.length() - 1;
        if (indexOf == -1) {
            sb.append('?');
        } else if (indexOf < length && str.charAt(length) != '&') {
            sb.append('&');
        }
        return sb;
    }

    @DexIgnore
    public static ua6 e(CharSequence charSequence) throws d {
        return new ua6(charSequence, "POST");
    }

    @DexIgnore
    public static ua6 f(CharSequence charSequence) throws d {
        return new ua6(charSequence, "PUT");
    }

    @DexIgnore
    public int d(String str) throws d {
        return a(str, -1);
    }

    @DexIgnore
    public static ua6 b(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = c((CharSequence) a2);
        }
        return e((CharSequence) a2);
    }

    @DexIgnore
    public String d(String str, String str2) {
        return b(c(str), str2);
    }

    @DexIgnore
    public ua6 e() throws IOException {
        f fVar = this.d;
        if (fVar == null) {
            return this;
        }
        if (this.e) {
            fVar.e("\r\n--00content0boundary00--\r\n");
        }
        if (this.f) {
            try {
                this.d.close();
            } catch (IOException unused) {
            }
        } else {
            this.d.close();
        }
        this.d = null;
        return this;
    }

    @DexIgnore
    public ua6 f() throws d {
        try {
            e();
            return this;
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public String d() {
        return d("Content-Type", "charset");
    }

    @DexIgnore
    public static ua6 b(CharSequence charSequence) throws d {
        return new ua6(charSequence, "DELETE");
    }

    @DexIgnore
    public ua6 f(String str, String str2) throws d {
        a((CharSequence) str);
        a((CharSequence) ": ");
        a((CharSequence) str2);
        a((CharSequence) "\r\n");
        return this;
    }

    @DexIgnore
    public BufferedInputStream b() throws d {
        return new BufferedInputStream(p(), this.h);
    }

    @DexIgnore
    public static String a(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map == null || map.isEmpty()) {
            return charSequence2;
        }
        StringBuilder sb = new StringBuilder(charSequence2);
        b(charSequence2, sb);
        a(charSequence2, sb);
        Iterator<Map.Entry<?, ?>> it = map.entrySet().iterator();
        Map.Entry next = it.next();
        sb.append(next.getKey().toString());
        sb.append('=');
        Object value = next.getValue();
        if (value != null) {
            sb.append(value);
        }
        while (it.hasNext()) {
            sb.append('&');
            Map.Entry next2 = it.next();
            sb.append(next2.getKey().toString());
            sb.append('=');
            Object value2 = next2.getValue();
            if (value2 != null) {
                sb.append(value2);
            }
        }
        return sb.toString();
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006f A[EDGE_INSN: B:30:0x006f->B:29:0x006f ?: BREAK  , SYNTHETIC] */
    public java.lang.String b(java.lang.String r9, java.lang.String r10) {
        /*
            r8 = this;
            r0 = 0
            if (r9 == 0) goto L_0x006f
            int r1 = r9.length()
            if (r1 != 0) goto L_0x000a
            goto L_0x006f
        L_0x000a:
            int r1 = r9.length()
            r2 = 59
            int r3 = r9.indexOf(r2)
            r4 = 1
            int r3 = r3 + r4
            if (r3 == 0) goto L_0x006f
            if (r3 != r1) goto L_0x001b
            goto L_0x006f
        L_0x001b:
            int r5 = r9.indexOf(r2, r3)
            r6 = -1
            if (r5 != r6) goto L_0x0023
        L_0x0022:
            r5 = r1
        L_0x0023:
            if (r3 >= r5) goto L_0x006f
            r7 = 61
            int r7 = r9.indexOf(r7, r3)
            if (r7 == r6) goto L_0x0066
            if (r7 >= r5) goto L_0x0066
            java.lang.String r3 = r9.substring(r3, r7)
            java.lang.String r3 = r3.trim()
            boolean r3 = r10.equals(r3)
            if (r3 == 0) goto L_0x0066
            int r7 = r7 + 1
            java.lang.String r3 = r9.substring(r7, r5)
            java.lang.String r3 = r3.trim()
            int r7 = r3.length()
            if (r7 == 0) goto L_0x0066
            r9 = 2
            if (r7 <= r9) goto L_0x0065
            r9 = 0
            char r9 = r3.charAt(r9)
            r10 = 34
            if (r10 != r9) goto L_0x0065
            int r7 = r7 - r4
            char r9 = r3.charAt(r7)
            if (r10 != r9) goto L_0x0065
            java.lang.String r9 = r3.substring(r4, r7)
            return r9
        L_0x0065:
            return r3
        L_0x0066:
            int r3 = r5 + 1
            int r5 = r9.indexOf(r2, r3)
            if (r5 != r6) goto L_0x0023
            goto L_0x0022
        L_0x006f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ua6.b(java.lang.String, java.lang.String):java.lang.String");
    }

    @DexIgnore
    public ua6 e(String str, String str2) {
        a(str, (String) null, str2);
        return this;
    }

    @DexIgnore
    public ByteArrayOutputStream c() {
        int i2 = i();
        if (i2 > 0) {
            return new ByteArrayOutputStream(i2);
        }
        return new ByteArrayOutputStream();
    }

    @DexIgnore
    public ua6 b(String str) {
        a(str, (String) null);
        return this;
    }

    @DexIgnore
    public ua6 b(String str, String str2, String str3) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(str);
        if (str2 != null) {
            sb.append("\"; filename=\"");
            sb.append(str2);
        }
        sb.append('\"');
        f("Content-Disposition", sb.toString());
        if (str3 != null) {
            f("Content-Type", str3);
        }
        a((CharSequence) "\r\n");
        return this;
    }

    @DexIgnore
    public ua6 c(String str, String str2) {
        l().setRequestProperty(str, str2);
        return this;
    }

    @DexIgnore
    public String c(String str) throws d {
        f();
        return l().getHeaderField(str);
    }

    @DexIgnore
    public static ua6 a(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = c((CharSequence) a2);
        }
        return d((CharSequence) a2);
    }

    @DexIgnore
    public String a(String str) throws d {
        ByteArrayOutputStream c2 = c();
        try {
            a((InputStream) b(), (OutputStream) c2);
            return c2.toString(f(str));
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public String a() throws d {
        return a(d());
    }

    @DexIgnore
    public ua6 a(int i2) {
        l().setConnectTimeout(i2);
        return this;
    }

    @DexIgnore
    public ua6 a(Map.Entry<String, String> entry) {
        c(entry.getKey(), entry.getValue());
        return this;
    }

    @DexIgnore
    public int a(String str, int i2) throws d {
        f();
        return l().getHeaderFieldInt(str, i2);
    }

    @DexIgnore
    public ua6 a(boolean z) {
        l().setUseCaches(z);
        return this;
    }

    @DexIgnore
    public ua6 a(String str, String str2) {
        if (str2 == null || str2.length() <= 0) {
            c("Content-Type", str);
            return this;
        }
        c("Content-Type", str + "; charset=" + str2);
        return this;
    }

    @DexIgnore
    public ua6 a(InputStream inputStream, OutputStream outputStream) throws IOException {
        return (ua6) new a(inputStream, this.f, inputStream, outputStream).call();
    }

    @DexIgnore
    public ua6 a(String str, String str2, String str3) throws d {
        a(str, str2, (String) null, str3);
        return this;
    }

    @DexIgnore
    public ua6 a(String str, String str2, String str3, String str4) throws d {
        try {
            o();
            b(str, str2, str3);
            this.d.e(str4);
            return this;
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public ua6 a(String str, Number number) throws d {
        a(str, (String) null, number);
        return this;
    }

    @DexIgnore
    public ua6 a(String str, String str2, Number number) throws d {
        a(str, str2, number != null ? number.toString() : null);
        return this;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0023 A[SYNTHETIC, Splitter:B:19:0x0023] */
    public ua6 a(String str, String str2, String str3, File file) throws d {
        BufferedInputStream bufferedInputStream = null;
        try {
            BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(file));
            try {
                a(str, str2, str3, (InputStream) bufferedInputStream2);
                try {
                    bufferedInputStream2.close();
                } catch (IOException unused) {
                }
                return this;
            } catch (IOException e2) {
                e = e2;
                bufferedInputStream = bufferedInputStream2;
                try {
                    throw new d(e);
                } catch (Throwable th) {
                    th = th;
                    if (bufferedInputStream != null) {
                        try {
                            bufferedInputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedInputStream = bufferedInputStream2;
                if (bufferedInputStream != null) {
                }
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            throw new d(e);
        }
    }

    @DexIgnore
    public ua6 a(String str, String str2, String str3, InputStream inputStream) throws d {
        try {
            o();
            b(str, str2, str3);
            a(inputStream, (OutputStream) this.d);
            return this;
        } catch (IOException e2) {
            throw new d(e2);
        }
    }

    @DexIgnore
    public ua6 a(CharSequence charSequence) throws d {
        try {
            n();
            this.d.e(charSequence.toString());
            return this;
        } catch (IOException e2) {
            throw new d(e2);
        }
    }
}
