package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v52 extends t52 {
    @DexIgnore
    public /* final */ Callable<String> e;

    @DexIgnore
    public v52(Callable<String> callable) {
        super(false, (String) null, (Throwable) null);
        this.e = callable;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.e.call();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
