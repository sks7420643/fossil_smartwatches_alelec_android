package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc6 implements Comparable<wc6> {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public /* synthetic */ wc6(long j) {
        this.a = j;
    }

    @DexIgnore
    public static boolean a(long j, Object obj) {
        if (obj instanceof wc6) {
            if (j == ((wc6) obj).a()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static final /* synthetic */ wc6 b(long j) {
        return new wc6(j);
    }

    @DexIgnore
    public static long c(long j) {
        return j;
    }

    @DexIgnore
    public static int d(long j) {
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public static String e(long j) {
        return dd6.a(j);
    }

    @DexIgnore
    public final int a(long j) {
        return a(this.a, j);
    }

    @DexIgnore
    public final /* synthetic */ long a() {
        return this.a;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return a(((wc6) obj).a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return d(this.a);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public static int a(long j, long j2) {
        return dd6.a(j, j2);
    }
}
