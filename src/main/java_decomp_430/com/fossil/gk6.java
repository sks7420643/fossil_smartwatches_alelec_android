package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk6 {
    @DexIgnore
    public static final <T> rl6<T> a(il6 il6, af6 af6, ll6 ll6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6) {
        return ik6.a(il6, af6, ll6, ig6);
    }

    @DexIgnore
    public static final rm6 b(il6 il6, af6 af6, ll6 ll6, ig6<? super il6, ? super xe6<? super cd6>, ? extends Object> ig6) {
        return ik6.b(il6, af6, ll6, ig6);
    }

    @DexIgnore
    public static final <T> T a(af6 af6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6) throws InterruptedException {
        return hk6.a(af6, ig6);
    }

    @DexIgnore
    public static final <T> Object a(af6 af6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6, xe6<? super T> xe6) {
        return ik6.a(af6, ig6, xe6);
    }
}
