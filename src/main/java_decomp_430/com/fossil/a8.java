package com.fossil;

import android.content.res.Configuration;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a8 {
    @DexIgnore
    public static b8 a(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 24) {
            return b8.a(configuration.getLocales());
        }
        return b8.a(configuration.locale);
    }
}
