package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g50 extends y40 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<g50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final a50[] a(a50... a50Arr) {
            Object[] array = nd6.e(a50Arr).toArray(new a50[0]);
            if (array != null) {
                return (a50[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new g50[i];
        }

        @DexIgnore
        public g50 createFromParcel(Parcel parcel) {
            return (g50) y40.CREATOR.createFromParcel(parcel);
        }
    }

    @DexIgnore
    public g50(b50 b50, k50 k50, c50 c50) {
        super(nf0.REMINDER, CREATOR.a(b50, k50, c50));
    }
}
