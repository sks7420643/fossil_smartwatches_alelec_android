package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p92 {
    @DexIgnore
    public static p92 a;

    @DexIgnore
    public static synchronized p92 a() {
        p92 p92;
        synchronized (p92.class) {
            if (a == null) {
                a = new l92();
            }
            p92 = a;
        }
        return p92;
    }

    @DexIgnore
    public abstract q92<Boolean> a(String str, boolean z);
}
