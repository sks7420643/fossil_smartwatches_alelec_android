package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r70 extends p40 implements Parcelable {
    @DexIgnore
    public /* final */ t70 a;

    @DexIgnore
    public r70(t70 t70) {
        this.a = t70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(new JSONObject(), bm0.TYPE, (Object) this.a);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final t70 getActionType() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
    }

    @DexIgnore
    public r70(Parcel parcel) {
        this(t70.values()[parcel.readInt()]);
    }
}
