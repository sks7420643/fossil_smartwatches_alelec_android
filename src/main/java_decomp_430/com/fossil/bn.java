package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bn extends dn<Boolean> {
    @DexIgnore
    public bn(Context context, to toVar) {
        super(pn.a(context, toVar).a());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.g();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
