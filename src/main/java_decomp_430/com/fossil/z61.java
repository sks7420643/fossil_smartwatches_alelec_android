package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z61 extends vh {
    @DexIgnore
    public z61(w81 w81, oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ?";
    }
}
