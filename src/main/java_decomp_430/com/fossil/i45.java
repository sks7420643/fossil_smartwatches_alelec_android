package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i45 implements Factory<h45> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<ComplicationLastSettingRepository> c;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> d;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppLastSettingRepository> f;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> g;

    @DexIgnore
    public i45(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<RingStyleRepository> provider5, Provider<WatchAppLastSettingRepository> provider6, Provider<WatchFaceRepository> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static i45 a(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<RingStyleRepository> provider5, Provider<WatchAppLastSettingRepository> provider6, Provider<WatchFaceRepository> provider7) {
        return new i45(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    public static h45 b(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<RingStyleRepository> provider5, Provider<WatchAppLastSettingRepository> provider6, Provider<WatchFaceRepository> provider7) {
        return new DianaCustomizeViewModel(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get());
    }

    @DexIgnore
    public DianaCustomizeViewModel get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
    }
}
