package com.fossil;

import android.os.Bundle;
import android.text.Spanned;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ir5 extends k24<hr5> {
    @DexIgnore
    void B0();

    @DexIgnore
    void D(boolean z);

    @DexIgnore
    void E(boolean z);

    @DexIgnore
    void M(String str);

    @DexIgnore
    void U0();

    @DexIgnore
    void a(Bundle bundle);

    @DexIgnore
    void a(Spanned spanned);

    @DexIgnore
    void a(rh4 rh4);

    @DexIgnore
    void a(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    void a(boolean z, boolean z2, String str);

    @DexIgnore
    void b(Spanned spanned);

    @DexIgnore
    void b(MFUser mFUser);

    @DexIgnore
    void b(boolean z, String str);

    @DexIgnore
    void c(Spanned spanned);

    @DexIgnore
    void c(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void d(Spanned spanned);

    @DexIgnore
    void g();

    @DexIgnore
    void g(int i, String str);

    @DexIgnore
    void i();

    @DexIgnore
    void k();

    @DexIgnore
    void n0();
}
