package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad6 implements Collection<zc6>, ph6 {
    @DexIgnore
    public /* final */ short[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends re6 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ short[] b;

        @DexIgnore
        public a(short[] sArr) {
            wg6.b(sArr, "array");
            this.b = sArr;
        }

        @DexIgnore
        public short a() {
            int i = this.a;
            short[] sArr = this.b;
            if (i < sArr.length) {
                this.a = i + 1;
                short s = sArr[i];
                zc6.c(s);
                return s;
            }
            throw new NoSuchElementException(String.valueOf(i));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(short[] sArr, Object obj) {
        return (obj instanceof ad6) && wg6.a((Object) sArr, (Object) ((ad6) obj).b());
    }

    @DexIgnore
    public static int b(short[] sArr) {
        if (sArr != null) {
            return Arrays.hashCode(sArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(short[] sArr) {
        return sArr.length == 0;
    }

    @DexIgnore
    public static re6 d(short[] sArr) {
        return new a(sArr);
    }

    @DexIgnore
    public static String e(short[] sArr) {
        return "UShortArray(storage=" + Arrays.toString(sArr) + ")";
    }

    @DexIgnore
    public boolean a(short s) {
        return a(this.a, s);
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends zc6> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ short[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof zc6) {
            return a(((zc6) obj).a());
        }
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<zc6>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    public re6 iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(short[] sArr) {
        return sArr.length;
    }

    @DexIgnore
    public static boolean a(short[] sArr, short s) {
        return nd6.a(sArr, s);
    }

    @DexIgnore
    public static boolean a(short[] sArr, Collection<zc6> collection) {
        boolean z;
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        for (T next : collection) {
            if (!(next instanceof zc6) || !nd6.a(sArr, ((zc6) next).a())) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
