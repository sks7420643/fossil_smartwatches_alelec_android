package com.fossil;

import com.fossil.fq1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq1 extends fq1 {
    @DexIgnore
    public /* final */ fq1.a a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public aq1(fq1.a aVar, long j) {
        if (aVar != null) {
            this.a = aVar;
            this.b = j;
            return;
        }
        throw new NullPointerException("Null status");
    }

    @DexIgnore
    public long a() {
        return this.b;
    }

    @DexIgnore
    public fq1.a b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fq1)) {
            return false;
        }
        fq1 fq1 = (fq1) obj;
        if (!this.a.equals(fq1.b()) || this.b != fq1.a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "BackendResponse{status=" + this.a + ", nextRequestWaitMillis=" + this.b + "}";
    }
}
