package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pb3 extends IInterface {
    @DexIgnore
    void a(gv1 gv1, ob3 ob3) throws RemoteException;

    @DexIgnore
    void a(xb3 xb3) throws RemoteException;

    @DexIgnore
    void a(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;

    @DexIgnore
    void d(Status status) throws RemoteException;

    @DexIgnore
    void e(Status status) throws RemoteException;
}
