package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u31<T> {
    @DexIgnore
    public /* final */ w40 a;

    @DexIgnore
    public u31(w40 w40) {
        this.a = w40;
    }

    @DexIgnore
    public abstract byte[] a(T t);

    @DexIgnore
    public abstract byte[] a(short s, T t);
}
