package com.fossil;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j00 extends FilterInputStream {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public int b;

    @DexIgnore
    public j00(InputStream inputStream, long j) {
        super(inputStream);
        this.a = j;
    }

    @DexIgnore
    public static InputStream a(InputStream inputStream, long j) {
        return new j00(inputStream, j);
    }

    @DexIgnore
    public synchronized int available() throws IOException {
        return (int) Math.max(this.a - ((long) this.b), (long) this.in.available());
    }

    @DexIgnore
    public final int b(int i) throws IOException {
        if (i >= 0) {
            this.b += i;
        } else if (this.a - ((long) this.b) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.a + ", but read: " + this.b);
        }
        return i;
    }

    @DexIgnore
    public synchronized int read() throws IOException {
        int read;
        read = super.read();
        b(read >= 0 ? 1 : -1);
        return read;
    }

    @DexIgnore
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int read;
        read = super.read(bArr, i, i2);
        b(read);
        return read;
    }
}
