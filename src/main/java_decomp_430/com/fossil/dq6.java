package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dq6 extends Cloneable {

    @DexIgnore
    public interface a {
        @DexIgnore
        dq6 a(yq6 yq6);
    }

    @DexIgnore
    void a(eq6 eq6);

    @DexIgnore
    void cancel();

    @DexIgnore
    Response s() throws IOException;

    @DexIgnore
    yq6 t();

    @DexIgnore
    boolean v();
}
