package com.fossil;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m35 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<o35> c;
    @DexIgnore
    public ArrayList<o35> d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public WatchFaceWrapper f;

    @DexIgnore
    public m35(String str, String str2, ArrayList<o35> arrayList, ArrayList<o35> arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper) {
        wg6.b(str, "mPresetId");
        wg6.b(str2, "mPresetName");
        wg6.b(arrayList, "mComplications");
        wg6.b(arrayList2, "mWatchApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = arrayList2;
        this.e = z;
        this.f = watchFaceWrapper;
    }

    @DexIgnore
    public final ArrayList<o35> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<o35> e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m35)) {
            return false;
        }
        m35 m35 = (m35) obj;
        return wg6.a((Object) this.a, (Object) m35.a) && wg6.a((Object) this.b, (Object) m35.b) && wg6.a((Object) this.c, (Object) m35.c) && wg6.a((Object) this.d, (Object) m35.d) && this.e == m35.e && wg6.a((Object) this.f, (Object) m35.f);
    }

    @DexIgnore
    public final WatchFaceWrapper f() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<o35> arrayList = this.c;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<o35> arrayList2 = this.d;
        int hashCode4 = (hashCode3 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        WatchFaceWrapper watchFaceWrapper = this.f;
        if (watchFaceWrapper != null) {
            i = watchFaceWrapper.hashCode();
        }
        return i2 + i;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.a + ", mPresetName=" + this.b + ", mComplications=" + this.c + ", mWatchApps=" + this.d + ", mIsActive=" + this.e + ", mWatchFaceWrapper=" + this.f + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ m35(String str, String str2, ArrayList arrayList, ArrayList arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper, int i, qg6 qg6) {
        this(str, str2, arrayList, arrayList2, z, (i & 32) != 0 ? null : watchFaceWrapper);
    }
}
