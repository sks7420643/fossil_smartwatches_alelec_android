package com.fossil;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q82 extends Service {
    @DexIgnore
    public a a;

    @DexIgnore
    public abstract List<f72> a(List<DataType> list);

    @DexIgnore
    @TargetApi(19)
    public final void a() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (s42.e()) {
            ((AppOpsManager) getSystemService("appops")).checkPackage(callingUid, "com.google.android.gms");
            return;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        if (packagesForUid != null) {
            int length = packagesForUid.length;
            int i = 0;
            while (i < length) {
                if (!packagesForUid[i].equals("com.google.android.gms")) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new SecurityException("Unauthorized caller");
    }

    @DexIgnore
    public abstract boolean a(f72 f72);

    @DexIgnore
    public abstract boolean a(r82 r82);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.fitness.service.FitnessSensorService".equals(intent.getAction())) {
            return null;
        }
        if (Log.isLoggable("FitnessSensorService", 3)) {
            String valueOf = String.valueOf(intent);
            String name = q82.class.getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20 + String.valueOf(name).length());
            sb.append("Intent ");
            sb.append(valueOf);
            sb.append(" received by ");
            sb.append(name);
            Log.d("FitnessSensorService", sb.toString());
        }
        a aVar = this.a;
        aVar.asBinder();
        return aVar;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.a = new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ie2 {
        @DexIgnore
        public /* final */ q82 a;

        @DexIgnore
        public a(q82 q82) {
            this.a = q82;
        }

        @DexIgnore
        public final void a(de2 de2, wc2 wc2) throws RemoteException {
            this.a.a();
            wc2.a(new o82(this.a.a(de2.p()), Status.e));
        }

        @DexIgnore
        public final void a(r82 r82, nd2 nd2) throws RemoteException {
            this.a.a();
            if (this.a.a(r82)) {
                nd2.c(Status.e);
            } else {
                nd2.c(new Status(13));
            }
        }

        @DexIgnore
        public final void a(ee2 ee2, nd2 nd2) throws RemoteException {
            this.a.a();
            if (this.a.a(ee2.p())) {
                nd2.c(Status.e);
            } else {
                nd2.c(new Status(13));
            }
        }
    }
}
