package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class te6 extends se6 {
    @DexIgnore
    public static final boolean a(int[] iArr, int[] iArr2) {
        wg6.b(iArr, "$this$contentEquals");
        wg6.b(iArr2, "other");
        return Arrays.equals(iArr, iArr2);
    }

    @DexIgnore
    public static final boolean a(long[] jArr, long[] jArr2) {
        wg6.b(jArr, "$this$contentEquals");
        wg6.b(jArr2, "other");
        return Arrays.equals(jArr, jArr2);
    }

    @DexIgnore
    public static final boolean a(byte[] bArr, byte[] bArr2) {
        wg6.b(bArr, "$this$contentEquals");
        wg6.b(bArr2, "other");
        return Arrays.equals(bArr, bArr2);
    }

    @DexIgnore
    public static final boolean a(short[] sArr, short[] sArr2) {
        wg6.b(sArr, "$this$contentEquals");
        wg6.b(sArr2, "other");
        return Arrays.equals(sArr, sArr2);
    }
}
