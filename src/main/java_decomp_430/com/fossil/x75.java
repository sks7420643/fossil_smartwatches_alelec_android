package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x75 implements Factory<w75> {
    @DexIgnore
    public /* final */ Provider<an4> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public x75(Provider<an4> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static x75 a(Provider<an4> provider, Provider<UserRepository> provider2) {
        return new x75(provider, provider2);
    }

    @DexIgnore
    public static w75 b(Provider<an4> provider, Provider<UserRepository> provider2) {
        return new CommuteTimeSettingsDetailViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel get() {
        return b(this.a, this.b);
    }
}
