package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ NotificationType a;
    @DexIgnore
    public /* final */ yd0 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            NotificationType notificationType = NotificationType.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(yd0.class.getClassLoader());
            if (readParcelable != null) {
                return new zd0(notificationType, (yd0) readParcelable);
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new zd0[i];
        }
    }

    @DexIgnore
    public zd0(NotificationType notificationType, yd0 yd0) {
        this.a = notificationType;
        this.b = yd0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.TYPE, (Object) cw0.a((Enum<?>) this.a)), bm0.REPLY_MESSAGE_GROUP, (Object) this.b.a());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(zd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            zd0 zd0 = (zd0) obj;
            return this.a == zd0.a && !(wg6.a(this.b, zd0.b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageMapping");
    }

    @DexIgnore
    public final NotificationType getNotificationType() {
        return this.a;
    }

    @DexIgnore
    public final yd0 getReplyMessageGroup() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeParcelable(this.b, i);
    }
}
