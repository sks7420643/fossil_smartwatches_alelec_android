package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q40 extends Parcelable {

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceStateChanged(q40 q40, c cVar, c cVar2);

        @DexIgnore
        void onEventReceived(q40 q40, d90 d90);
    }

    @DexIgnore
    public enum c {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING
    }

    @DexIgnore
    public enum d {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING;
        
        @DexIgnore
        public static /* final */ qo0 f; // = null;

        /*
        static {
            f = new qo0((qg6) null);
        }
        */
    }

    @DexIgnore
    <T> T a(Class<T> cls);

    @DexIgnore
    void a(b bVar);

    @DexIgnore
    a getBondState();

    @DexIgnore
    c getState();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    zb0<cd6> k();

    @DexIgnore
    r40 l();

    @DexIgnore
    public enum a {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ C0039a b; // = null;

        /*
        static {
            b = new C0039a((qg6) null);
        }
        */

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.q40$a$a")
        /* renamed from: com.fossil.q40$a$a  reason: collision with other inner class name */
        public static final class C0039a {
            @DexIgnore
            public /* synthetic */ C0039a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(mw0 mw0) {
                int i = el0.a[mw0.ordinal()];
                if (i == 1) {
                    return a.BOND_NONE;
                }
                if (i == 2) {
                    return a.BONDING;
                }
                if (i == 3) {
                    return a.BONDED;
                }
                throw new kc6();
            }

            @DexIgnore
            public final a a(int i) {
                if (i == 11) {
                    return a.BONDING;
                }
                if (i != 12) {
                    return a.BOND_NONE;
                }
                return a.BONDED;
            }
        }
    }
}
