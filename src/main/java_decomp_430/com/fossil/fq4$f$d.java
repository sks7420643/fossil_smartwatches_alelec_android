package com.fossil;

import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$metadata$2", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class fq4$f$d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.c $currentMetadata;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.c $oldMetadata;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$f$d(MusicControlComponent.f fVar, MusicControlComponent.c cVar, MusicControlComponent.c cVar2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$oldMetadata = cVar;
        this.$currentMetadata = cVar2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        fq4$f$d fq4_f_d = new fq4$f$d(this.this$0, this.$oldMetadata, this.$currentMetadata, xe6);
        fq4_f_d.p$ = (il6) obj;
        return fq4_f_d;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((fq4$f$d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.a(this.$oldMetadata, this.$currentMetadata);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
