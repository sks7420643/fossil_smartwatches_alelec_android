package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg5$b$a<T> implements ld<cf<SleepSummary>> {
    @DexIgnore
    public /* final */ /* synthetic */ DashboardSleepPresenter.b a;

    @DexIgnore
    public bg5$b$a(DashboardSleepPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(cf<SleepSummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging observer size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardSleepPresenter", sb.toString());
        if (cfVar != null) {
            this.a.this$0.k().a(cfVar);
        }
    }
}
