package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ue6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ hg6[] a;

        @DexIgnore
        public a(hg6[] hg6Arr) {
            this.a = hg6Arr;
        }

        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.b(t, t2, this.a);
        }
    }

    @DexIgnore
    public static final <T> int b(T t, T t2, hg6<? super T, ? extends Comparable<?>>[] hg6Arr) {
        for (hg6<? super T, ? extends Comparable<?>> hg6 : hg6Arr) {
            int a2 = a((Comparable) hg6.invoke(t), (Comparable) hg6.invoke(t2));
            if (a2 != 0) {
                return a2;
            }
        }
        return 0;
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> Comparator<T> a(hg6<? super T, ? extends Comparable<?>>... hg6Arr) {
        wg6.b(hg6Arr, "selectors");
        if (hg6Arr.length > 0) {
            return new a(hg6Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
