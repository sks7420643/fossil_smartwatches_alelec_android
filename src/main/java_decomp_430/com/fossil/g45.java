package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g45 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ String a; // = ThemeManager.l.a().b("onPrimaryButton");
    @DexIgnore
    public ArrayList<m35> b;
    @DexIgnore
    public DianaComplicationRingStyle c;
    @DexIgnore
    public d d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ g45 c;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.view.View, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r2v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(g45 g45, View view) {
            super(view);
            wg6.b(view, "view");
            this.c = g45;
            View findViewById = view.findViewById(2131361955);
            if (findViewById != null) {
                this.a = (FlexibleButton) findViewById;
                View findViewById2 = view.findViewById(2131363113);
                wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
                this.b = (TextView) findViewById2;
                if (!TextUtils.isEmpty(g45.c())) {
                    int parseColor = Color.parseColor(g45.c());
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231027);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        this.a.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                cl4.a(this.a, this);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleButton");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r3v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final void a(boolean z) {
            if (z) {
                this.a.a("flexible_button_disabled");
                this.a.setClickable(false);
                this.b.setText(PortfolioApp.get.instance().getString(2131886385));
                return;
            }
            this.a.a("flexible_button_primary");
            this.a.setClickable(true);
            this.b.setText(PortfolioApp.get.instance().getString(2131886372));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == 2131361955) {
                this.c.e().H0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void G0();

        @DexIgnore
        void H0();

        @DexIgnore
        void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2);

        @DexIgnore
        void b(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(String str, String str2);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public g45(ArrayList<m35> arrayList, DianaComplicationRingStyle dianaComplicationRingStyle, d dVar) {
        wg6.b(arrayList, "mData");
        wg6.b(dVar, "mListener");
        this.b = arrayList;
        this.c = dianaComplicationRingStyle;
        this.d = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final DianaComplicationRingStyle d() {
        return this.c;
    }

    @DexIgnore
    public final d e() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.b.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.b.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            m35 m35 = this.b.get(i);
            wg6.a((Object) m35, "mData[position]");
            m35 m352 = m35;
            if (i != 0) {
                z = false;
            }
            bVar.a(m352, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.b.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558660, viewGroup, false);
            wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558652, viewGroup, false);
        wg6.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(List<m35> list, DianaComplicationRingStyle dianaComplicationRingStyle) {
        wg6.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + list);
        this.c = dianaComplicationRingStyle;
        this.b.clear();
        this.b.addAll(list);
        this.b.add(new m35("", "", new ArrayList(), new ArrayList(), false, (WatchFaceWrapper) null, 32, (qg6) null));
        notifyDataSetChanged();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public CustomizeWidget b;
        @DexIgnore
        public CustomizeWidget c;
        @DexIgnore
        public CustomizeWidget d;
        @DexIgnore
        public CustomizeWidget e;
        @DexIgnore
        public View f;
        @DexIgnore
        public ImageView g;
        @DexIgnore
        public CustomizeWidget h;
        @DexIgnore
        public CustomizeWidget i;
        @DexIgnore
        public CustomizeWidget j;
        @DexIgnore
        public ImageButton o;
        @DexIgnore
        public FlexibleTextView p;
        @DexIgnore
        public View q;
        @DexIgnore
        public FlexibleButton r;
        @DexIgnore
        public View s;
        @DexIgnore
        public View t;
        @DexIgnore
        public View u;
        @DexIgnore
        public View v;
        @DexIgnore
        public m35 w;
        @DexIgnore
        public /* final */ /* synthetic */ g45 x;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v54, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r2v56, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r2v57, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v58, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v59, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v60, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v62, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v63, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v64, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(g45 g45, View view) {
            super(view);
            wg6.b(view, "view");
            this.x = g45;
            View findViewById = view.findViewById(2131363193);
            wg6.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.a = (TextView) findViewById;
            this.a.setOnClickListener(this);
            wg6.a((Object) view.findViewById(2131362025), "view.findViewById(R.id.cl_complications)");
            wg6.a((Object) view.findViewById(2131362059), "view.findViewById(R.id.cl_preset_buttons)");
            View findViewById2 = view.findViewById(2131362639);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.iv_watch_hands_background)");
            this.f = findViewById2;
            View findViewById3 = view.findViewById(2131362640);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.iv_watch_theme_background)");
            this.g = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131363339);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.wc_top)");
            this.b = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(2131363331);
            wg6.a((Object) findViewById5, "view.findViewById(R.id.wc_bottom)");
            this.c = (CustomizeWidget) findViewById5;
            View findViewById6 = view.findViewById(2131363338);
            wg6.a((Object) findViewById6, "view.findViewById(R.id.wc_start)");
            this.d = (CustomizeWidget) findViewById6;
            View findViewById7 = view.findViewById(2131363335);
            wg6.a((Object) findViewById7, "view.findViewById(R.id.wc_end)");
            this.e = (CustomizeWidget) findViewById7;
            View findViewById8 = view.findViewById(2131362484);
            wg6.a((Object) findViewById8, "view.findViewById(R.id.ib_edit_themes)");
            this.o = (ImageButton) findViewById8;
            View findViewById9 = view.findViewById(2131363278);
            wg6.a((Object) findViewById9, "view.findViewById(R.id.v_underline)");
            this.v = findViewById9;
            View findViewById10 = view.findViewById(2131363330);
            wg6.a((Object) findViewById10, "view.findViewById(R.id.wa_top)");
            this.h = (CustomizeWidget) findViewById10;
            View findViewById11 = view.findViewById(2131363329);
            wg6.a((Object) findViewById11, "view.findViewById(R.id.wa_middle)");
            this.i = (CustomizeWidget) findViewById11;
            View findViewById12 = view.findViewById(2131363328);
            wg6.a((Object) findViewById12, "view.findViewById(R.id.wa_bottom)");
            this.j = (CustomizeWidget) findViewById12;
            View findViewById13 = view.findViewById(2131362658);
            wg6.a((Object) findViewById13, "view.findViewById(R.id.line_bottom)");
            this.u = findViewById13;
            View findViewById14 = view.findViewById(2131362659);
            wg6.a((Object) findViewById14, "view.findViewById(R.id.line_center)");
            this.t = findViewById14;
            View findViewById15 = view.findViewById(2131362660);
            wg6.a((Object) findViewById15, "view.findViewById(R.id.line_top)");
            this.s = findViewById15;
            View findViewById16 = view.findViewById(2131362650);
            wg6.a((Object) findViewById16, "view.findViewById(R.id.layout_right)");
            this.q = findViewById16;
            View findViewById17 = view.findViewById(2131361960);
            wg6.a((Object) findViewById17, "view.findViewById(R.id.btn_right)");
            this.r = (FlexibleButton) findViewById17;
            View findViewById18 = view.findViewById(2131363165);
            wg6.a((Object) findViewById18, "view.findViewById(R.id.tv_left)");
            this.p = (FlexibleTextView) findViewById18;
            this.r.setOnClickListener(this);
            this.q.setOnClickListener(this);
            this.p.setOnClickListener(this);
            this.b.setOnClickListener(this);
            this.c.setOnClickListener(this);
            this.d.setOnClickListener(this);
            this.e.setOnClickListener(this);
            this.o.setOnClickListener(this);
            this.h.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.j.setOnClickListener(this);
        }

        @DexIgnore
        public final List<u8<View, String>> a() {
            u8[] u8VarArr = new u8[7];
            TextView textView = this.a;
            u8VarArr[0] = new u8(textView, textView.getTransitionName());
            View view = this.f;
            u8VarArr[1] = new u8(view, view.getTransitionName());
            ImageView imageView = this.g;
            if (imageView != null) {
                u8VarArr[2] = new u8(imageView, imageView.getTransitionName());
                View view2 = this.v;
                u8VarArr[3] = new u8(view2, view2.getTransitionName());
                View view3 = this.u;
                u8VarArr[4] = new u8(view3, view3.getTransitionName());
                View view4 = this.t;
                u8VarArr[5] = new u8(view4, view4.getTransitionName());
                View view5 = this.s;
                u8VarArr[6] = new u8(view5, view5.getTransitionName());
                return qd6.c(u8VarArr);
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v8, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v10, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        /* JADX WARNING: type inference failed for: r2v12, types: [com.portfolio.platform.view.CustomizeWidget, java.lang.Object, android.view.ViewGroup] */
        public final List<u8<CustomizeWidget, String>> b() {
            Object r2 = this.b;
            Object r22 = this.c;
            Object r23 = this.e;
            Object r24 = this.d;
            Object r25 = this.h;
            Object r26 = this.i;
            Object r27 = this.j;
            return qd6.c(new u8(r2, r2.getTransitionName()), new u8(r22, r22.getTransitionName()), new u8(r23, r23.getTransitionName()), new u8(r24, r24.getTransitionName()), new u8(r25, r25.getTransitionName()), new u8(r26, r26.getTransitionName()), new u8(r27, r27.getTransitionName()));
        }

        @DexIgnore
        public void onClick(View view) {
            String str;
            String c2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.w == null) && view != null) {
                switch (view.getId()) {
                    case 2131361960:
                        this.x.e().G0();
                        return;
                    case 2131362484:
                        this.x.e().b(this.w, a(), b());
                        return;
                    case 2131363165:
                        if (this.x.b.size() > 2) {
                            Object obj = this.x.b.get(1);
                            wg6.a(obj, "mData[1]");
                            m35 m35 = (m35) obj;
                            d e2 = this.x.e();
                            m35 m352 = this.w;
                            if (m352 != null) {
                                boolean b2 = m352.b();
                                m35 m353 = this.w;
                                if (m353 != null) {
                                    e2.b(b2, m353.d(), m35.d(), m35.c());
                                    return;
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case 2131363193:
                        d e3 = this.x.e();
                        m35 m354 = this.w;
                        String str2 = "";
                        if (m354 == null || (str = m354.d()) == null) {
                            str = str2;
                        }
                        m35 m355 = this.w;
                        if (!(m355 == null || (c2 = m355.c()) == null)) {
                            str2 = c2;
                        }
                        e3.b(str, str2);
                        return;
                    case 2131363328:
                        this.x.e().a(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363329:
                        this.x.e().a(this.w, a(), b(), "middle", getAdapterPosition());
                        return;
                    case 2131363330:
                        this.x.e().a(this.w, a(), b(), "top", getAdapterPosition());
                        return;
                    case 2131363331:
                        this.x.e().b(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363335:
                        this.x.e().b(this.w, a(), b(), "right", getAdapterPosition());
                        return;
                    case 2131363338:
                        this.x.e().b(this.w, a(), b(), "left", getAdapterPosition());
                        return;
                    case 2131363339:
                        this.x.e().b(this.w, a(), b(), "top", getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        /* JADX WARNING: type inference failed for: r10v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r10v18, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r3v62, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r10v19, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r10v21, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r10v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r10v24, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r10v29, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r10v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r10v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r10v38, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: Code restructure failed: missing block: B:245:0x00fa, code lost:
            continue;
         */
        @DexIgnore
        public final void a(m35 m35, boolean z) {
            Drawable background;
            Drawable topComplication;
            Drawable topComplication2;
            Drawable leftComplication;
            Drawable rightComplication;
            wg6.b(m35, "preset");
            if (z) {
                this.q.setSelected(true);
                this.r.setText(PortfolioApp.get.instance().getString(2131886381));
                if (!TextUtils.isEmpty(this.x.c())) {
                    int parseColor = Color.parseColor(this.x.c());
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231067);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        this.r.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                        cd6 cd6 = cd6.a;
                    }
                }
                this.r.a("flexible_button_right_applied");
                this.r.setClickable(false);
                if (this.x.b.size() == 2) {
                    this.p.setVisibility(4);
                } else {
                    this.p.setVisibility(0);
                }
            } else {
                this.q.setSelected(false);
                this.r.setText(PortfolioApp.get.instance().getString(2131886373));
                this.r.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.r.a("flexible_button_right_apply");
                this.r.setClickable(true);
                this.p.setVisibility(0);
            }
            this.w = m35;
            this.a.setText(m35.d());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(m35.a());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(m35.e());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with complications=" + arrayList + " watchapps=" + arrayList2 + " mWatchFaceWrapper=" + m35.f());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                o35 o35 = (o35) it.next();
                String c2 = o35.c();
                if (c2 != null) {
                    switch (c2.hashCode()) {
                        case -1383228885:
                            if (c2.equals("bottom")) {
                                this.c.b(o35.a());
                                this.c.setBottomContent(o35.d());
                                WatchFaceWrapper f2 = m35.f();
                                if (f2 == null || (topComplication = f2.getTopComplication()) == null) {
                                    this.c.setBackgroundRes(2131230895);
                                    cd6 cd62 = cd6.a;
                                } else {
                                    this.c.setBackgroundDrawableCus(topComplication);
                                    cd6 cd63 = cd6.a;
                                }
                                WatchFaceWrapper f3 = m35.f();
                                if ((f3 != null ? f3.getBottomMetaData() : null) != null) {
                                    CustomizeWidget customizeWidget = this.c;
                                    WatchFaceWrapper f4 = m35.f();
                                    if (f4 != null) {
                                        WatchFaceWrapper.MetaData topMetaData = f4.getTopMetaData();
                                        if (topMetaData != null) {
                                            customizeWidget.a((Integer) null, (Integer) null, topMetaData.getUnselectedForegroundColor(), (Integer) null);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    DianaComplicationRingStyle d2 = this.x.d();
                                    if ((d2 != null ? d2.getMetaData() : null) != null) {
                                        DianaComplicationRingStyle d3 = this.x.d();
                                        if (d3 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (!TextUtils.isEmpty(d3.getMetaData().getUnselectedForegroundColor())) {
                                            CustomizeWidget customizeWidget2 = this.c;
                                            DianaComplicationRingStyle d4 = this.x.d();
                                            if (d4 != null) {
                                                String unselectedForegroundColor = d4.getMetaData().getUnselectedForegroundColor();
                                                if (unselectedForegroundColor != null) {
                                                    customizeWidget2.a((Integer) null, (Integer) null, Integer.valueOf(Color.parseColor(unselectedForegroundColor)), (Integer) null);
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    }
                                    this.c.g();
                                }
                                this.c.h();
                                break;
                            } else {
                                continue;
                            }
                        case 115029:
                            if (c2.equals("top")) {
                                this.b.b(o35.a());
                                this.b.setBottomContent(o35.d());
                                WatchFaceWrapper f5 = m35.f();
                                if (f5 == null || (topComplication2 = f5.getTopComplication()) == null) {
                                    this.b.setBackgroundRes(2131230895);
                                    cd6 cd64 = cd6.a;
                                } else {
                                    this.b.setBackgroundDrawableCus(topComplication2);
                                    cd6 cd65 = cd6.a;
                                }
                                WatchFaceWrapper f6 = m35.f();
                                if ((f6 != null ? f6.getTopMetaData() : null) != null) {
                                    CustomizeWidget customizeWidget3 = this.b;
                                    WatchFaceWrapper f7 = m35.f();
                                    if (f7 != null) {
                                        WatchFaceWrapper.MetaData topMetaData2 = f7.getTopMetaData();
                                        if (topMetaData2 != null) {
                                            customizeWidget3.a((Integer) null, (Integer) null, topMetaData2.getUnselectedForegroundColor(), (Integer) null);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    DianaComplicationRingStyle d5 = this.x.d();
                                    if ((d5 != null ? d5.getMetaData() : null) != null) {
                                        DianaComplicationRingStyle d6 = this.x.d();
                                        if (d6 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (!TextUtils.isEmpty(d6.getMetaData().getUnselectedForegroundColor())) {
                                            CustomizeWidget customizeWidget4 = this.b;
                                            DianaComplicationRingStyle d7 = this.x.d();
                                            if (d7 != null) {
                                                String unselectedForegroundColor2 = d7.getMetaData().getUnselectedForegroundColor();
                                                if (unselectedForegroundColor2 != null) {
                                                    customizeWidget4.a((Integer) null, (Integer) null, Integer.valueOf(Color.parseColor(unselectedForegroundColor2)), (Integer) null);
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    }
                                    this.b.g();
                                }
                                this.b.h();
                                break;
                            } else {
                                continue;
                            }
                            break;
                        case 3317767:
                            if (c2.equals("left")) {
                                this.d.b(o35.a());
                                this.d.setBottomContent(o35.d());
                                WatchFaceWrapper f8 = m35.f();
                                if (f8 == null || (leftComplication = f8.getLeftComplication()) == null) {
                                    this.d.setBackgroundRes(2131230895);
                                    cd6 cd66 = cd6.a;
                                } else {
                                    this.d.setBackgroundDrawableCus(leftComplication);
                                    cd6 cd67 = cd6.a;
                                }
                                WatchFaceWrapper f9 = m35.f();
                                if ((f9 != null ? f9.getLeftMetaData() : null) != null) {
                                    CustomizeWidget customizeWidget5 = this.d;
                                    WatchFaceWrapper f10 = m35.f();
                                    if (f10 != null) {
                                        WatchFaceWrapper.MetaData topMetaData3 = f10.getTopMetaData();
                                        if (topMetaData3 != null) {
                                            customizeWidget5.a((Integer) null, (Integer) null, topMetaData3.getUnselectedForegroundColor(), (Integer) null);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    DianaComplicationRingStyle d8 = this.x.d();
                                    if ((d8 != null ? d8.getMetaData() : null) != null) {
                                        DianaComplicationRingStyle d9 = this.x.d();
                                        if (d9 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (!TextUtils.isEmpty(d9.getMetaData().getUnselectedForegroundColor())) {
                                            CustomizeWidget customizeWidget6 = this.d;
                                            DianaComplicationRingStyle d10 = this.x.d();
                                            if (d10 != null) {
                                                String unselectedForegroundColor3 = d10.getMetaData().getUnselectedForegroundColor();
                                                if (unselectedForegroundColor3 != null) {
                                                    customizeWidget6.a((Integer) null, (Integer) null, Integer.valueOf(Color.parseColor(unselectedForegroundColor3)), (Integer) null);
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    }
                                    this.d.g();
                                }
                                this.d.h();
                                break;
                            } else {
                                continue;
                            }
                        case 108511772:
                            if (c2.equals("right")) {
                                this.e.b(o35.a());
                                this.e.setBottomContent(o35.d());
                                WatchFaceWrapper f11 = m35.f();
                                if (f11 == null || (rightComplication = f11.getRightComplication()) == null) {
                                    this.e.setBackgroundRes(2131230895);
                                    cd6 cd68 = cd6.a;
                                } else {
                                    this.e.setBackgroundDrawableCus(rightComplication);
                                    cd6 cd69 = cd6.a;
                                }
                                WatchFaceWrapper f12 = m35.f();
                                if ((f12 != null ? f12.getRightMetaData() : null) != null) {
                                    CustomizeWidget customizeWidget7 = this.e;
                                    WatchFaceWrapper f13 = m35.f();
                                    if (f13 != null) {
                                        WatchFaceWrapper.MetaData topMetaData4 = f13.getTopMetaData();
                                        if (topMetaData4 != null) {
                                            customizeWidget7.a((Integer) null, (Integer) null, topMetaData4.getUnselectedForegroundColor(), (Integer) null);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    DianaComplicationRingStyle d11 = this.x.d();
                                    if ((d11 != null ? d11.getMetaData() : null) != null) {
                                        DianaComplicationRingStyle d12 = this.x.d();
                                        if (d12 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (!TextUtils.isEmpty(d12.getMetaData().getUnselectedForegroundColor())) {
                                            CustomizeWidget customizeWidget8 = this.e;
                                            DianaComplicationRingStyle d13 = this.x.d();
                                            if (d13 != null) {
                                                String unselectedForegroundColor4 = d13.getMetaData().getUnselectedForegroundColor();
                                                if (unselectedForegroundColor4 != null) {
                                                    customizeWidget8.a((Integer) null, (Integer) null, Integer.valueOf(Color.parseColor(unselectedForegroundColor4)), (Integer) null);
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    }
                                    this.e.g();
                                }
                                this.e.h();
                                break;
                            } else {
                                continue;
                            }
                            break;
                    }
                }
            }
            WatchFaceWrapper f14 = m35.f();
            if (f14 == null || (background = f14.getBackground()) == null) {
                this.g.setImageDrawable(PortfolioApp.get.instance().getDrawable(2131231278));
                cd6 cd610 = cd6.a;
            } else {
                this.g.setImageDrawable(background);
                cd6 cd611 = cd6.a;
            }
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                o35 o352 = (o35) it2.next();
                String c3 = o352.c();
                if (c3 != null) {
                    int hashCode = c3.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && c3.equals("top")) {
                                this.h.d(o352.a());
                                this.h.h();
                            }
                        } else if (c3.equals("middle")) {
                            this.i.d(o352.a());
                            this.i.h();
                        }
                    } else if (c3.equals("bottom")) {
                        this.j.d(o352.a());
                        this.j.h();
                    }
                }
            }
        }
    }
}
