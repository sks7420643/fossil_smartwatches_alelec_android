package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r7 extends Drawable implements Drawable.Callback, q7, p7 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode g; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public int a;
    @DexIgnore
    public PorterDuff.Mode b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public t7 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Drawable f;

    @DexIgnore
    public r7(t7 t7Var, Resources resources) {
        this.d = t7Var;
        a(resources);
    }

    @DexIgnore
    public final void a(Resources resources) {
        Drawable.ConstantState constantState;
        t7 t7Var = this.d;
        if (t7Var != null && (constantState = t7Var.b) != null) {
            a(constantState.newDrawable(resources));
        }
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public final t7 c() {
        return new t7(this.d);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.f.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        t7 t7Var = this.d;
        return changingConfigurations | (t7Var != null ? t7Var.getChangingConfigurations() : 0) | this.f.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        t7 t7Var = this.d;
        if (t7Var == null || !t7Var.a()) {
            return null;
        }
        this.d.a = getChangingConfigurations();
        return this.d;
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.f.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.f.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.f.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.f.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.f.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.f.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.f.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.f.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.f.isAutoMirrored();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = r1.d;
     */
    @DexIgnore
    public boolean isStateful() {
        t7 t7Var;
        ColorStateList colorStateList = (!b() || t7Var == null) ? null : t7Var.c;
        return (colorStateList != null && colorStateList.isStateful()) || this.f.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        this.f.jumpToCurrentState();
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.e && super.mutate() == this) {
            this.d = c();
            Drawable drawable = this.f;
            if (drawable != null) {
                drawable.mutate();
            }
            t7 t7Var = this.d;
            if (t7Var != null) {
                Drawable drawable2 = this.f;
                t7Var.b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.e = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.f.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        this.f.setAutoMirrored(z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.f.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.f.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.f.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.f.setFilterBitmap(z);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return a(iArr) || this.f.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        this.d.c = colorStateList;
        a(getState());
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        this.d.d = mode;
        a(getState());
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        if (!b()) {
            return false;
        }
        t7 t7Var = this.d;
        ColorStateList colorStateList = t7Var.c;
        PorterDuff.Mode mode = t7Var.d;
        if (colorStateList == null || mode == null) {
            this.c = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.c && colorForState == this.a && mode == this.b)) {
                setColorFilter(colorForState, mode);
                this.a = colorForState;
                this.b = mode;
                this.c = true;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public r7(Drawable drawable) {
        this.d = c();
        a(drawable);
    }

    @DexIgnore
    public final Drawable a() {
        return this.f;
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        Drawable drawable2 = this.f;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.f = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            t7 t7Var = this.d;
            if (t7Var != null) {
                t7Var.b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }
}
