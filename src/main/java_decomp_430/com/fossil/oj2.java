package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj2 extends fn2<oj2, a> implements to2 {
    @DexIgnore
    public static /* final */ oj2 zzi;
    @DexIgnore
    public static volatile yo2<oj2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public float zzg;
    @DexIgnore
    public double zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<oj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(oj2.zzi);
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((oj2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            f();
            ((oj2) this.b).b(str);
            return this;
        }

        @DexIgnore
        public final a j() {
            f();
            ((oj2) this.b).v();
            return this;
        }

        @DexIgnore
        public final a k() {
            f();
            ((oj2) this.b).w();
            return this;
        }

        @DexIgnore
        public final a l() {
            f();
            ((oj2) this.b).x();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((oj2) this.b).a(j);
            return this;
        }

        @DexIgnore
        public final a a(double d) {
            f();
            ((oj2) this.b).a(d);
            return this;
        }
    }

    /*
    static {
        oj2 oj2 = new oj2();
        zzi = oj2;
        fn2.a(oj2.class, oj2);
    }
    */

    @DexIgnore
    public static a y() {
        return (a) zzi.h();
    }

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 1;
            this.zzd = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(String str) {
        if (str != null) {
            this.zzc |= 2;
            this.zze = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final String n() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean o() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String p() {
        return this.zze;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long r() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final double t() {
        return this.zzh;
    }

    @DexIgnore
    public final void v() {
        this.zzc &= -3;
        this.zze = zzi.zze;
    }

    @DexIgnore
    public final void w() {
        this.zzc &= -5;
        this.zzf = 0;
    }

    @DexIgnore
    public final void x() {
        this.zzc &= -17;
        this.zzh = 0.0d;
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 4;
        this.zzf = j;
    }

    @DexIgnore
    public final void a(double d) {
        this.zzc |= 16;
        this.zzh = d;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new oj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0001\u0003\u0005\u0000\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                yo2<oj2> yo2 = zzj;
                if (yo2 == null) {
                    synchronized (oj2.class) {
                        yo2 = zzj;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzi);
                            zzj = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
