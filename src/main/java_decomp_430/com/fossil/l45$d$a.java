package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l45$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l45$d$a(HomeDianaCustomizePresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l45$d$a l45_d_a = new l45$d$a(this.this$0, xe6);
        l45_d_a.p$ = (il6) obj;
        return l45_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l45$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            DianaPresetRepository k = this.this$0.this$0.x;
            String id = this.this$0.$preset.getId();
            this.L$0 = il6;
            this.label = 1;
            if (k.deletePresetById(id, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
