package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn3<T> extends jn3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ jn3<? super T> ordering;

    @DexIgnore
    public gn3(jn3<? super T> jn3) {
        this.ordering = jn3;
    }

    @DexIgnore
    public int compare(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return this.ordering.compare(t, t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof gn3) {
            return this.ordering.equals(((gn3) obj).ordering);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.ordering.hashCode() ^ 957692532;
    }

    @DexIgnore
    public <S extends T> jn3<S> nullsFirst() {
        return this;
    }

    @DexIgnore
    public <S extends T> jn3<S> nullsLast() {
        return this.ordering.nullsLast();
    }

    @DexIgnore
    public <S extends T> jn3<S> reverse() {
        return this.ordering.reverse().nullsLast();
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".nullsFirst()";
    }
}
