package com.fossil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.v;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public class w implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<w> CREATOR; // = new a();
    @DexIgnore
    public /* final */ boolean a; // = false;
    @DexIgnore
    public /* final */ Handler b; // = null;
    @DexIgnore
    public v c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<w> {
        @DexIgnore
        public w createFromParcel(Parcel parcel) {
            return new w(parcel);
        }

        @DexIgnore
        public w[] newArray(int i) {
            return new w[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends v.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(int i, Bundle bundle) {
            w wVar = w.this;
            Handler handler = wVar.b;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                wVar.a(i, bundle);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Bundle b;

        @DexIgnore
        public c(int i, Bundle bundle) {
            this.a = i;
            this.b = bundle;
        }

        @DexIgnore
        public void run() {
            w.this.a(this.a, this.b);
        }
    }

    @DexIgnore
    public w(Parcel parcel) {
        this.c = v.a.a(parcel.readStrongBinder());
    }

    @DexIgnore
    public void a(int i, Bundle bundle) {
    }

    @DexIgnore
    public void b(int i, Bundle bundle) {
        if (this.a) {
            Handler handler = this.b;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                a(i, bundle);
            }
        } else {
            v vVar = this.c;
            if (vVar != null) {
                try {
                    vVar.a(i, bundle);
                } catch (RemoteException unused) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.c == null) {
                this.c = new b();
            }
            parcel.writeStrongBinder(this.c.asBinder());
        }
    }
}
