package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum lp0 {
    MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE,
    SDK_LOG_PREFERENCE,
    MINUTE_DATA_REFERENCE,
    HARDWARE_LOG_REFERENCE,
    TEXT_ENCRYPTION_PREFERENCE
}
