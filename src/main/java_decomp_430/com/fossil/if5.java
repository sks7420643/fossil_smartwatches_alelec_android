package com.fossil;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if5 implements Factory<hf5> {
    @DexIgnore
    public static HeartRateOverviewDayPresenter a(gf5 gf5, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new HeartRateOverviewDayPresenter(gf5, heartRateSampleRepository, workoutSessionRepository);
    }
}
