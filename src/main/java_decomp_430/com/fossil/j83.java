package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ h83 b;
    @DexIgnore
    public /* final */ /* synthetic */ h83 c;
    @DexIgnore
    public /* final */ /* synthetic */ g83 d;

    @DexIgnore
    public j83(g83 g83, boolean z, h83 h83, h83 h832) {
        this.d = g83;
        this.a = z;
        this.b = h83;
        this.c = h832;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0061, code lost:
        if (com.fossil.ma3.d(r10.b.a, r10.c.a) != false) goto L_0x0064;
     */
    @DexIgnore
    public final void run() {
        boolean z;
        g83 g83;
        h83 h83;
        boolean z2 = false;
        if (this.d.l().p(this.d.p().A())) {
            z = this.a && this.d.c != null;
            if (z) {
                g83 g832 = this.d;
                g832.a(g832.c, true);
            }
        } else {
            if (this.a && (h83 = g83.c) != null) {
                (g83 = this.d).a(h83, true);
            }
            z = false;
        }
        h83 h832 = this.b;
        if (h832 != null) {
            long j = h832.c;
            h83 h833 = this.c;
            if (j == h833.c) {
                if (ma3.d(h832.b, h833.b)) {
                }
            }
        }
        z2 = true;
        if (z2) {
            Bundle bundle = new Bundle();
            g83.a(this.c, bundle, true);
            h83 h834 = this.b;
            if (h834 != null) {
                String str = h834.a;
                if (str != null) {
                    bundle.putString("_pn", str);
                }
                bundle.putString("_pc", this.b.b);
                bundle.putLong("_pi", this.b.c);
            }
            if (this.d.l().p(this.d.p().A()) && z) {
                long b2 = this.d.t().e.b();
                if (b2 > 0) {
                    this.d.j().a(bundle, b2);
                }
            }
            this.d.o().b("auto", "_vs", bundle);
        }
        g83 g833 = this.d;
        g833.c = this.c;
        g833.q().a(this.c);
    }
}
