package com.fossil;

import com.fossil.ws4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl5$e$a extends BaseZendeskFeedbackConfiguration {
    @DexIgnore
    public /* final */ /* synthetic */ ws4.d $responseValue;

    @DexIgnore
    public pl5$e$a(ws4.d dVar) {
        this.$responseValue = dVar;
    }

    @DexIgnore
    public String getAdditionalInfo() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a = DeleteAccountPresenter.k.a();
        local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
        return this.$responseValue.a();
    }

    @DexIgnore
    public String getRequestSubject() {
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "getRequestSubject");
        return this.$responseValue.d();
    }

    @DexIgnore
    public List<String> getTags() {
        FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "getTags");
        return this.$responseValue.e();
    }
}
