package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx4 implements Factory<ax4> {
    @DexIgnore
    public static HomePresenter a(tw4 tw4, an4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, z24 z24, cj4 cj4, jt4 jt4, ey5 ey5, ServerSettingRepository serverSettingRepository, ws4 ws4, dp5 dp5) {
        return new HomePresenter(tw4, an4, deviceRepository, portfolioApp, updateFirmwareUsecase, z24, cj4, jt4, ey5, serverSettingRepository, ws4, dp5);
    }
}
