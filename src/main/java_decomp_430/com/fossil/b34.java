package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.y24;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b34 implements a34 {
    @DexIgnore
    public static /* final */ int c; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int d; // = Math.max(2, Math.min(c - 1, 4));
    @DexIgnore
    public static /* final */ int e; // = ((c * 2) + 1);
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(Integer.MAX_VALUE);
    @DexIgnore
    public /* final */ Handler a; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ ThreadPoolExecutor b; // = new ThreadPoolExecutor(d, e, 30, TimeUnit.SECONDS, g, f);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ y24.d a;
        @DexIgnore
        public /* final */ /* synthetic */ y24.a b;

        @DexIgnore
        public b(b34 b34, y24.d dVar, y24.a aVar) {
            this.a = dVar;
            this.b = aVar;
        }

        @DexIgnore
        public void run() {
            y24.d dVar = this.a;
            if (dVar != null) {
                dVar.a(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ y24.d a;
        @DexIgnore
        public /* final */ /* synthetic */ y24.c b;

        @DexIgnore
        public c(b34 b34, y24.d dVar, y24.c cVar) {
            this.a = dVar;
            this.b = cVar;
        }

        @DexIgnore
        public void run() {
            y24.d dVar = this.a;
            if (dVar != null) {
                dVar.onSuccess(this.b);
            }
        }
    }

    @DexIgnore
    public b34() {
        this.b.allowCoreThreadTimeOut(true);
    }

    @DexIgnore
    public <P extends y24.c, E extends y24.a> void a(E e2, y24.d<P, E> dVar) {
        this.a.post(new b(this, dVar, e2));
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    public <P extends y24.c, E extends y24.a> void a(P p, y24.d<P, E> dVar) {
        this.a.post(new c(this, dVar, p));
    }
}
