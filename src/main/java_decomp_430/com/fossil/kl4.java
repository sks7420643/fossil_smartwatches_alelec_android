package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl4 extends jl4 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kl4(AnalyticsHelper analyticsHelper, String str, String str2) {
        super(analyticsHelper, str, str2);
        wg6.b(analyticsHelper, "analyticsHelper");
        wg6.b(str, "traceName");
        cv6.a(wg6.a((Object) str, (Object) "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final boolean a(kl4 kl4) {
        if (kl4 == null) {
            return false;
        }
        return wg6.a((Object) kl4.i, (Object) this.i);
    }

    @DexIgnore
    public final kl4 b(String str) {
        wg6.b(str, "viewName");
        this.i = str;
        a("view_name", str);
        return this;
    }

    @DexIgnore
    public final String e() {
        return this.i;
    }

    @DexIgnore
    public String toString() {
        return "View name: " + this.i + ", running: " + b();
    }
}
