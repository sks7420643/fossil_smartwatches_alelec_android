package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jv<Model, Data> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> {
        @DexIgnore
        public /* final */ vr a;
        @DexIgnore
        public /* final */ List<vr> b;
        @DexIgnore
        public /* final */ fs<Data> c;

        @DexIgnore
        public a(vr vrVar, fs<Data> fsVar) {
            this(vrVar, Collections.emptyList(), fsVar);
        }

        @DexIgnore
        public a(vr vrVar, List<vr> list, fs<Data> fsVar) {
            q00.a(vrVar);
            this.a = vrVar;
            q00.a(list);
            this.b = list;
            q00.a(fsVar);
            this.c = fsVar;
        }
    }

    @DexIgnore
    a<Data> a(Model model, int i, int i2, xr xrVar);

    @DexIgnore
    boolean a(Model model);
}
