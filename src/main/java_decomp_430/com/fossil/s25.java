package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s25 extends k24<r25> {
    @DexIgnore
    void F0();

    @DexIgnore
    void J();

    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(ArrayList<wx4> arrayList);

    @DexIgnore
    void a(List<wx4> list, FilterQueryProvider filterQueryProvider, int i);
}
