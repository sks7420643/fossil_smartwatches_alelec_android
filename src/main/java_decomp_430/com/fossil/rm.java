package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.sm;
import com.fossil.um;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rm implements zm, em, um.b {
    @DexIgnore
    public static /* final */ String j; // = tl.a("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ sm d;
    @DexIgnore
    public /* final */ an e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public PowerManager.WakeLock h;
    @DexIgnore
    public boolean i; // = false;

    @DexIgnore
    public rm(Context context, int i2, String str, sm smVar) {
        this.a = context;
        this.b = i2;
        this.d = smVar;
        this.c = str;
        this.e = new an(this.a, smVar.d(), this);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        tl.a().a(j, String.format("onExecuted %s, %s", new Object[]{str, Boolean.valueOf(z)}), new Throwable[0]);
        a();
        if (z) {
            Intent b2 = pm.b(this.a, this.c);
            sm smVar = this.d;
            smVar.a((Runnable) new sm.b(smVar, b2, this.b));
        }
        if (this.i) {
            Intent a2 = pm.a(this.a);
            sm smVar2 = this.d;
            smVar2.a((Runnable) new sm.b(smVar2, a2, this.b));
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        if (list.contains(this.c)) {
            synchronized (this.f) {
                if (this.g == 0) {
                    this.g = 1;
                    tl.a().a(j, String.format("onAllConstraintsMet for %s", new Object[]{this.c}), new Throwable[0]);
                    if (this.d.c().c(this.c)) {
                        this.d.f().a(this.c, 600000, this);
                    } else {
                        a();
                    }
                } else {
                    tl.a().a(j, String.format("Already started work for %s", new Object[]{this.c}), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        synchronized (this.f) {
            if (this.g < 2) {
                this.g = 2;
                tl.a().a(j, String.format("Stopping work for WorkSpec %s", new Object[]{this.c}), new Throwable[0]);
                this.d.a((Runnable) new sm.b(this.d, pm.c(this.a, this.c), this.b));
                if (this.d.c().b(this.c)) {
                    tl.a().a(j, String.format("WorkSpec %s needs to be rescheduled", new Object[]{this.c}), new Throwable[0]);
                    this.d.a((Runnable) new sm.b(this.d, pm.b(this.a, this.c), this.b));
                } else {
                    tl.a().a(j, String.format("Processor does not have WorkSpec %s. No need to reschedule ", new Object[]{this.c}), new Throwable[0]);
                }
            } else {
                tl.a().a(j, String.format("Already stopped work for %s", new Object[]{this.c}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        tl.a().a(j, String.format("Exceeded time limits on execution for %s", new Object[]{str}), new Throwable[0]);
        c();
    }

    @DexIgnore
    public void a(List<String> list) {
        c();
    }

    @DexIgnore
    public void b() {
        this.h = oo.a(this.a, String.format("%s (%s)", new Object[]{this.c, Integer.valueOf(this.b)}));
        tl.a().a(j, String.format("Acquiring wakelock %s for WorkSpec %s", new Object[]{this.h, this.c}), new Throwable[0]);
        this.h.acquire();
        zn e2 = this.d.e().g().d().e(this.c);
        if (e2 == null) {
            c();
            return;
        }
        this.i = e2.b();
        if (!this.i) {
            tl.a().a(j, String.format("No constraints for %s", new Object[]{this.c}), new Throwable[0]);
            b(Collections.singletonList(this.c));
            return;
        }
        this.e.c(Collections.singletonList(e2));
    }

    @DexIgnore
    public final void a() {
        synchronized (this.f) {
            this.e.a();
            this.d.f().a(this.c);
            if (this.h != null && this.h.isHeld()) {
                tl.a().a(j, String.format("Releasing wakelock %s for WorkSpec %s", new Object[]{this.h, this.c}), new Throwable[0]);
                this.h.release();
            }
        }
    }
}
