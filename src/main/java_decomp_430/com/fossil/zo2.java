package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo2 {
    @DexIgnore
    public static /* final */ xo2 a; // = c();
    @DexIgnore
    public static /* final */ xo2 b; // = new wo2();

    @DexIgnore
    public static xo2 a() {
        return a;
    }

    @DexIgnore
    public static xo2 b() {
        return b;
    }

    @DexIgnore
    public static xo2 c() {
        try {
            return (xo2) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
