package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gs1 implements rs1.d {
    @DexIgnore
    public /* final */ SQLiteDatabase a;

    @DexIgnore
    public gs1(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @DexIgnore
    public static rs1.d a(SQLiteDatabase sQLiteDatabase) {
        return new gs1(sQLiteDatabase);
    }

    @DexIgnore
    public Object a() {
        return this.a.beginTransaction();
    }
}
