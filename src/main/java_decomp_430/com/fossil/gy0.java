package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gy0 implements Parcelable.Creator<ue1> {
    @DexIgnore
    public /* synthetic */ gy0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        u11 u11 = u11.b;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            return u11.a((BluetoothDevice) readParcelable);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ue1[i];
    }
}
