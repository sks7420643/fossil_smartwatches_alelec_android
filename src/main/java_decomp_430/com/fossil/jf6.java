package com.fossil;

import com.fossil.af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jf6 extends gf6 {
    @DexIgnore
    public /* final */ af6 _context;
    @DexIgnore
    public transient xe6<Object> intercepted;

    @DexIgnore
    public jf6(xe6<Object> xe6, af6 af6) {
        super(xe6);
        this._context = af6;
    }

    @DexIgnore
    public af6 getContext() {
        af6 af6 = this._context;
        if (af6 != null) {
            return af6;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final xe6<Object> intercepted() {
        xe6<Object> xe6 = this.intercepted;
        if (xe6 == null) {
            ye6 ye6 = (ye6) getContext().get(ye6.l);
            if (ye6 == null || (xe6 = ye6.c(this)) == null) {
                xe6 = this;
            }
            this.intercepted = xe6;
        }
        return xe6;
    }

    @DexIgnore
    public void releaseIntercepted() {
        xe6<Object> xe6 = this.intercepted;
        if (!(xe6 == null || xe6 == this)) {
            af6.b bVar = getContext().get(ye6.l);
            if (bVar != null) {
                ((ye6) bVar).b(xe6);
            } else {
                wg6.a();
                throw null;
            }
        }
        this.intercepted = if6.a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public jf6(xe6<Object> xe6) {
        this(xe6, xe6 != null ? xe6.getContext() : null);
    }
}
