package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en1 extends xg6 implements hg6<ac0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ie0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public en1(ie0 ie0) {
        super(1);
        this.a = ie0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        ac0 ac0 = (ac0) obj;
        kj0 kj0 = kj0.i;
        kj0.d.remove(this.a.a);
        t40 t40 = (t40) ac0;
        if (t40.getErrorCode() == u40.REQUEST_UNSUPPORTED) {
            kj0.i.d(this.a.a);
            this.a.a.a(ac0);
        } else if (t40.getErrorCode() == u40.REQUEST_FAILED && t40.b().b == sk1.HID_INPUT_DEVICE_DISABLED) {
            kj0 kj02 = kj0.i;
            kj0.c.add(this.a.a);
        } else {
            kj0.i.a(this.a.a, yj1.b.a("HID_EXPONENT_BACK_OFF_TAG"));
        }
        return cd6.a;
    }
}
