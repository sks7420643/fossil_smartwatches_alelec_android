package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
public final class pj5$g$a extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Bitmap $bitmap;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pj5$g$a(Bitmap bitmap, xe6 xe6) {
        super(2, xe6);
        this.$bitmap = bitmap;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pj5$g$a pj5_g_a = new pj5$g$a(this.$bitmap, xe6);
        pj5_g_a.p$ = (il6) obj;
        return pj5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pj5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            Bitmap bitmap = this.$bitmap;
            if (bitmap != null) {
                return cx5.a(bitmap);
            }
            wg6.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
