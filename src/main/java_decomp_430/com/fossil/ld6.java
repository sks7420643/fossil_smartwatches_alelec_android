package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ld6 extends kd6 {
    @DexIgnore
    public static final <T> boolean a(T[] tArr, T[] tArr2) {
        wg6.b(tArr, "$this$contentDeepEqualsImpl");
        wg6.b(tArr2, "other");
        if (tArr == tArr2) {
            return true;
        }
        if (tArr.length != tArr2.length) {
            return false;
        }
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            T t = tArr[i];
            T t2 = tArr2[i];
            if (t != t2) {
                if (t == null || t2 == null) {
                    return false;
                }
                if (!(t instanceof Object[]) || !(t2 instanceof Object[])) {
                    if (!(t instanceof byte[]) || !(t2 instanceof byte[])) {
                        if (!(t instanceof short[]) || !(t2 instanceof short[])) {
                            if (!(t instanceof int[]) || !(t2 instanceof int[])) {
                                if (!(t instanceof long[]) || !(t2 instanceof long[])) {
                                    if (!(t instanceof float[]) || !(t2 instanceof float[])) {
                                        if (!(t instanceof double[]) || !(t2 instanceof double[])) {
                                            if (!(t instanceof char[]) || !(t2 instanceof char[])) {
                                                if (!(t instanceof boolean[]) || !(t2 instanceof boolean[])) {
                                                    if (!(t instanceof tc6) || !(t2 instanceof tc6)) {
                                                        if (!(t instanceof ad6) || !(t2 instanceof ad6)) {
                                                            if (!(t instanceof vc6) || !(t2 instanceof vc6)) {
                                                                if (!(t instanceof xc6) || !(t2 instanceof xc6)) {
                                                                    if (!wg6.a((Object) t, (Object) t2)) {
                                                                        return false;
                                                                    }
                                                                } else if (!te6.a(((xc6) t).b(), ((xc6) t2).b())) {
                                                                    return false;
                                                                }
                                                            } else if (!te6.a(((vc6) t).b(), ((vc6) t2).b())) {
                                                                return false;
                                                            }
                                                        } else if (!te6.a(((ad6) t).b(), ((ad6) t2).b())) {
                                                            return false;
                                                        }
                                                    } else if (!te6.a(((tc6) t).b(), ((tc6) t2).b())) {
                                                        return false;
                                                    }
                                                } else if (!Arrays.equals((boolean[]) t, (boolean[]) t2)) {
                                                    return false;
                                                }
                                            } else if (!Arrays.equals((char[]) t, (char[]) t2)) {
                                                return false;
                                            }
                                        } else if (!Arrays.equals((double[]) t, (double[]) t2)) {
                                            return false;
                                        }
                                    } else if (!Arrays.equals((float[]) t, (float[]) t2)) {
                                        return false;
                                    }
                                } else if (!Arrays.equals((long[]) t, (long[]) t2)) {
                                    return false;
                                }
                            } else if (!Arrays.equals((int[]) t, (int[]) t2)) {
                                return false;
                            }
                        } else if (!Arrays.equals((short[]) t, (short[]) t2)) {
                            return false;
                        }
                    } else if (!Arrays.equals((byte[]) t, (byte[]) t2)) {
                        return false;
                    }
                } else if (!a((Object[]) t, (Object[]) t2)) {
                    return false;
                }
            }
        }
        return true;
    }
}
