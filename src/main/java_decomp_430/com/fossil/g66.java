package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g66 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ r36 c;

    @DexIgnore
    public g66(String str, Context context, r36 r36) {
        this.a = str;
        this.b = context;
        this.c = r36;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (q36.k) {
                if (q36.k.size() >= n36.h()) {
                    b56 f = q36.m;
                    f.d("The number of page events exceeds the maximum value " + Integer.toString(n36.h()));
                    return;
                }
                String unused = q36.i = this.a;
                if (q36.k.containsKey(q36.i)) {
                    b56 f2 = q36.m;
                    f2.c("Duplicate PageID : " + q36.i + ", onResume() repeated?");
                    return;
                }
                q36.k.put(q36.i, Long.valueOf(System.currentTimeMillis()));
                q36.a(this.b, true, this.c);
            }
        } catch (Throwable th) {
            q36.m.a(th);
            q36.a(this.b, th);
        }
    }
}
