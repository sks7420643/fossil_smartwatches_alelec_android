package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum l70 {
    PLAY((byte) 0),
    PAUSE((byte) 1),
    TOGGLE_PLAY_PAUSE((byte) 2),
    NEXT((byte) 3),
    PREVIOUS((byte) 4),
    VOLUME_UP((byte) 5),
    VOLUME_DOWN((byte) 6);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final l70 a(byte b) {
            for (l70 l70 : l70.values()) {
                if (l70.a() == b) {
                    return l70;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public l70(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
