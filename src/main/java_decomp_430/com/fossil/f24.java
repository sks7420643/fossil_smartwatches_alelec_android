package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.WatchParamHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f24 implements Factory<gl4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public f24(b14 b14, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static f24 a(b14 b14, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new f24(b14, provider, provider2);
    }

    @DexIgnore
    public static gl4 b(b14 b14, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return a(b14, provider.get(), provider2.get());
    }

    @DexIgnore
    public static WatchParamHelper a(b14 b14, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        WatchParamHelper a2 = b14.a(deviceRepository, portfolioApp);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public WatchParamHelper get() {
        return b(this.a, this.b, this.c);
    }
}
