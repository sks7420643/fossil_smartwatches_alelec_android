package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r91 extends x51 {
    @DexIgnore
    public /* final */ boolean T;

    @DexIgnore
    public r91(ue1 ue1, q41 q41, eh1 eh1, HashMap<io0, Object> hashMap, String str) {
        super(ue1, q41, eh1, lk1.b.a(ue1.t, w31.HARDWARE_LOG), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        this.T = true;
    }

    @DexIgnore
    public boolean b() {
        return this.T;
    }

    @DexIgnore
    public Object d() {
        return r();
    }

    @DexIgnore
    public final byte[][] r() {
        ArrayList<ie1> arrayList = this.G;
        ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
        for (ie1 ie1 : arrayList) {
            arrayList2.add(ie1.e);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ r91(ue1 ue1, q41 q41, HashMap hashMap, String str, int i) {
        this(ue1, q41, eh1.GET_HARDWARE_LOG, (HashMap<io0, Object>) (i & 4) != 0 ? new HashMap() : hashMap, (i & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
    }
}
