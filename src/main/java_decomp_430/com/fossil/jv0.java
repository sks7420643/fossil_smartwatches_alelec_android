package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ yy0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jv0(yy0 yy0) {
        super(1);
        this.a = yy0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        this.a.B = ((p61) obj).s();
        fo0 m = this.a.m();
        if (m == null) {
            yy0 yy0 = this.a;
            yy0.a(km1.a(yy0.v, (eh1) null, sk1.FLOW_BROKEN, (bn0) null, 5));
        } else if (xp0.a.a(m, this.a.D)) {
            yy0 yy02 = this.a;
            yy02.a(km1.a(yy02.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else {
            yy0 yy03 = this.a;
            yy03.a(km1.a(yy03.v, (eh1) null, sk1.EXCHANGED_VALUE_NOT_SATISFIED, (bn0) null, 5));
        }
        return cd6.a;
    }
}
