package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hn extends dn<ym> {
    @DexIgnore
    public hn(Context context, to toVar) {
        super(pn.a(context, toVar).c());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.b() == ul.UNMETERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(ym ymVar) {
        return !ymVar.a() || ymVar.b();
    }
}
