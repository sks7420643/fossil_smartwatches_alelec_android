package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.hc;
import java.io.PrintWriter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb extends hc implements FragmentManager.f, FragmentManager.i {
    @DexIgnore
    public /* final */ FragmentManager r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public tb(FragmentManager fragmentManager) {
        super(r0, r1 != null ? r1.c().getClassLoader() : null);
        xb u = fragmentManager.u();
        FragmentHostCallback<?> fragmentHostCallback = fragmentManager.o;
        this.t = -1;
        this.r = fragmentManager;
    }

    @DexIgnore
    public void a(String str, PrintWriter printWriter) {
        a(str, printWriter, true);
    }

    @DexIgnore
    public hc b(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.b(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot detach Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    public hc c(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.c(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot hide Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    public hc d(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.d(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot remove Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    public hc e(Fragment fragment) {
        FragmentManager fragmentManager = fragment.mFragmentManager;
        if (fragmentManager == null || fragmentManager == this.r) {
            super.e(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot show Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    @DexIgnore
    public void f() {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            hc.a aVar = this.a.get(i);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(this.f);
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.a(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.d);
                    this.r.s(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.d);
                    this.r.l(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.w(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.d);
                    this.r.g(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.c);
                    this.r.a(fragment, false);
                    this.r.c(fragment);
                    break;
                case 8:
                    this.r.u(fragment);
                    break;
                case 9:
                    this.r.u((Fragment) null);
                    break;
                case 10:
                    this.r.a(fragment, aVar.h);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
            }
            if (!(this.p || aVar.a == 1 || fragment == null)) {
                this.r.p(fragment);
            }
        }
        if (!this.p) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public String g() {
        return this.i;
    }

    @DexIgnore
    public boolean h() {
        for (int i = 0; i < this.a.size(); i++) {
            if (b(this.a.get(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void i() {
        if (this.q != null) {
            for (int i = 0; i < this.q.size(); i++) {
                this.q.get(i).run();
            }
            this.q = null;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.t >= 0) {
            sb.append(" #");
            sb.append(this.t);
        }
        if (this.i != null) {
            sb.append(" ");
            sb.append(this.i);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.i);
            printWriter.print(" mIndex=");
            printWriter.print(this.t);
            printWriter.print(" mCommitted=");
            printWriter.println(this.s);
            if (this.f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f));
            }
            if (!(this.b == 0 && this.c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.c));
            }
            if (!(this.d == 0 && this.e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.e));
            }
            if (!(this.j == 0 && this.k == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.j));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.k);
            }
            if (!(this.l == 0 && this.m == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.l));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.m);
            }
        }
        if (!this.a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                hc.a aVar = this.a.get(i);
                switch (aVar.a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = "cmd=" + aVar.a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.b);
                if (z) {
                    if (!(aVar.c == 0 && aVar.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.d));
                    }
                    if (aVar.e != 0 || aVar.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f));
                    }
                }
            }
        }
    }

    @DexIgnore
    public int b() {
        return b(true);
    }

    @DexIgnore
    public void c() {
        e();
        this.r.b((FragmentManager.i) this, false);
    }

    @DexIgnore
    public void d() {
        e();
        this.r.b((FragmentManager.i) this, true);
    }

    @DexIgnore
    public int b(boolean z) {
        if (!this.s) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new s8("FragmentManager"));
                a("  ", printWriter);
                printWriter.close();
            }
            this.s = true;
            if (this.g) {
                this.t = this.r.a();
            } else {
                this.t = -1;
            }
            this.r.a((FragmentManager.i) this, z);
            return this.t;
        }
        throw new IllegalStateException("commit already called");
    }

    @DexIgnore
    public void c(boolean z) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            hc.a aVar = this.a.get(size);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(FragmentManager.e(this.f));
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.s(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.e);
                    this.r.a(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.e);
                    this.r.w(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.l(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.e);
                    this.r.c(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f);
                    this.r.a(fragment, true);
                    this.r.g(fragment);
                    break;
                case 8:
                    this.r.u((Fragment) null);
                    break;
                case 9:
                    this.r.u(fragment);
                    break;
                case 10:
                    this.r.a(fragment, aVar.g);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
            }
            if (!(this.p || aVar.a == 3 || fragment == null)) {
                this.r.p(fragment);
            }
        }
        if (!this.p && z) {
            FragmentManager fragmentManager = this.r;
            fragmentManager.a(fragmentManager.n, true);
        }
    }

    @DexIgnore
    public boolean b(int i) {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.a.get(i2).b;
            int i3 = fragment != null ? fragment.mContainerId : 0;
            if (i3 != 0 && i3 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public Fragment b(ArrayList<Fragment> arrayList, Fragment fragment) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            hc.a aVar = this.a.get(size);
            int i = aVar.a;
            if (i != 1) {
                if (i != 3) {
                    switch (i) {
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = aVar.b;
                            break;
                        case 10:
                            aVar.h = aVar.g;
                            break;
                    }
                }
                arrayList.add(aVar.b);
            }
            arrayList.remove(aVar.b);
        }
        return fragment;
    }

    @DexIgnore
    public static boolean b(hc.a aVar) {
        Fragment fragment = aVar.b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    @DexIgnore
    public void a(int i, Fragment fragment, String str, int i2) {
        super.a(i, fragment, str, i2);
        fragment.mFragmentManager = this.r;
    }

    @DexIgnore
    public hc a(Fragment fragment, Lifecycle.State state) {
        if (fragment.mFragmentManager != this.r) {
            throw new IllegalArgumentException("Cannot setMaxLifecycle for Fragment not attached to FragmentManager " + this.r);
        } else if (state.isAtLeast(Lifecycle.State.CREATED)) {
            super.a(fragment, state);
            return this;
        } else {
            throw new IllegalArgumentException("Cannot set maximum Lifecycle below " + Lifecycle.State.CREATED);
        }
    }

    @DexIgnore
    public void a(int i) {
        if (this.g) {
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            int size = this.a.size();
            for (int i2 = 0; i2 < size; i2++) {
                hc.a aVar = this.a.get(i2);
                Fragment fragment = aVar.b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i;
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.b + " to " + aVar.b.mBackStackNesting);
                    }
                }
            }
        }
    }

    @DexIgnore
    public int a() {
        return b(false);
    }

    @DexIgnore
    public boolean a(ArrayList<tb> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.g) {
            return true;
        }
        this.r.a(this);
        return true;
    }

    @DexIgnore
    public boolean a(ArrayList<tb> arrayList, int i, int i2) {
        if (i2 == i) {
            return false;
        }
        int size = this.a.size();
        int i3 = -1;
        for (int i4 = 0; i4 < size; i4++) {
            Fragment fragment = this.a.get(i4).b;
            int i5 = fragment != null ? fragment.mContainerId : 0;
            if (!(i5 == 0 || i5 == i3)) {
                for (int i6 = i; i6 < i2; i6++) {
                    tb tbVar = arrayList.get(i6);
                    int size2 = tbVar.a.size();
                    for (int i7 = 0; i7 < size2; i7++) {
                        Fragment fragment2 = tbVar.a.get(i7).b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i5) {
                            return true;
                        }
                    }
                }
                i3 = i5;
            }
        }
        return false;
    }

    @DexIgnore
    public Fragment a(ArrayList<Fragment> arrayList, Fragment fragment) {
        ArrayList<Fragment> arrayList2 = arrayList;
        Fragment fragment2 = fragment;
        int i = 0;
        while (i < this.a.size()) {
            hc.a aVar = this.a.get(i);
            int i2 = aVar.a;
            if (i2 != 1) {
                if (i2 == 2) {
                    Fragment fragment3 = aVar.b;
                    int i3 = fragment3.mContainerId;
                    Fragment fragment4 = fragment2;
                    int i4 = i;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        Fragment fragment5 = arrayList2.get(size);
                        if (fragment5.mContainerId == i3) {
                            if (fragment5 == fragment3) {
                                z = true;
                            } else {
                                if (fragment5 == fragment4) {
                                    this.a.add(i4, new hc.a(9, fragment5));
                                    i4++;
                                    fragment4 = null;
                                }
                                hc.a aVar2 = new hc.a(3, fragment5);
                                aVar2.c = aVar.c;
                                aVar2.e = aVar.e;
                                aVar2.d = aVar.d;
                                aVar2.f = aVar.f;
                                this.a.add(i4, aVar2);
                                arrayList2.remove(fragment5);
                                i4++;
                            }
                        }
                    }
                    if (z) {
                        this.a.remove(i4);
                        i4--;
                    } else {
                        aVar.a = 1;
                        arrayList2.add(fragment3);
                    }
                    i = i4;
                    fragment2 = fragment4;
                } else if (i2 == 3 || i2 == 6) {
                    arrayList2.remove(aVar.b);
                    Fragment fragment6 = aVar.b;
                    if (fragment6 == fragment2) {
                        this.a.add(i, new hc.a(9, fragment6));
                        i++;
                        fragment2 = null;
                    }
                } else if (i2 != 7) {
                    if (i2 == 8) {
                        this.a.add(i, new hc.a(9, fragment2));
                        i++;
                        fragment2 = aVar.b;
                    }
                }
                i++;
            }
            arrayList2.add(aVar.b);
            i++;
        }
        return fragment2;
    }

    @DexIgnore
    public void a(Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i = 0; i < this.a.size(); i++) {
            hc.a aVar = this.a.get(i);
            if (b(aVar)) {
                aVar.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
}
