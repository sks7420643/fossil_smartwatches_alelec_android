package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.am;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nm implements hm, zm, em {
    @DexIgnore
    public static /* final */ String h; // = tl.a("GreedyScheduler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ lm b;
    @DexIgnore
    public /* final */ an c;
    @DexIgnore
    public List<zn> d; // = new ArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ Object f;
    @DexIgnore
    public Boolean g;

    @DexIgnore
    public nm(Context context, to toVar, lm lmVar) {
        this.a = context;
        this.b = lmVar;
        this.c = new an(context, toVar, this);
        this.f = new Object();
    }

    @DexIgnore
    public void a(zn... znVarArr) {
        if (this.g == null) {
            this.g = Boolean.valueOf(TextUtils.equals(this.a.getPackageName(), a()));
        }
        if (!this.g.booleanValue()) {
            tl.a().c(h, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (zn znVar : znVarArr) {
            if (znVar.b == am.a.ENQUEUED && !znVar.d() && znVar.g == 0 && !znVar.c()) {
                if (!znVar.b()) {
                    tl.a().a(h, String.format("Starting work for %s", new Object[]{znVar.a}), new Throwable[0]);
                    this.b.a(znVar.a);
                } else if (Build.VERSION.SDK_INT >= 23 && znVar.j.h()) {
                    tl.a().a(h, String.format("Ignoring WorkSpec %s, Requires device idle.", new Object[]{znVar}), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !znVar.j.e()) {
                    arrayList.add(znVar);
                    arrayList2.add(znVar.a);
                } else {
                    tl.a().a(h, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", new Object[]{znVar}), new Throwable[0]);
                }
            }
        }
        synchronized (this.f) {
            if (!arrayList.isEmpty()) {
                tl.a().a(h, String.format("Starting tracking for [%s]", new Object[]{TextUtils.join(",", arrayList2)}), new Throwable[0]);
                this.d.addAll(arrayList);
                this.c.c(this.d);
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        for (String next : list) {
            tl.a().a(h, String.format("Constraints met: Scheduling work ID %s", new Object[]{next}), new Throwable[0]);
            this.b.a(next);
        }
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this.f) {
            int size = this.d.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.d.get(i).a.equals(str)) {
                    tl.a().a(h, String.format("Stopping tracking for %s", new Object[]{str}), new Throwable[0]);
                    this.d.remove(i);
                    this.c.c(this.d);
                    break;
                } else {
                    i++;
                }
            }
        }
    }

    @DexIgnore
    public final void b() {
        if (!this.e) {
            this.b.e().a((em) this);
            this.e = true;
        }
    }

    @DexIgnore
    public void a(String str) {
        if (this.g == null) {
            this.g = Boolean.valueOf(TextUtils.equals(this.a.getPackageName(), a()));
        }
        if (!this.g.booleanValue()) {
            tl.a().c(h, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        tl.a().a(h, String.format("Cancelling work ID %s", new Object[]{str}), new Throwable[0]);
        this.b.b(str);
    }

    @DexIgnore
    public void a(List<String> list) {
        for (String next : list) {
            tl.a().a(h, String.format("Constraints not met: Cancelling work ID %s", new Object[]{next}), new Throwable[0]);
            this.b.b(next);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        b(str);
    }

    @DexIgnore
    public final String a() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.a.getSystemService("activity");
        if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }
}
