package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface eu2 extends IInterface {
    @DexIgnore
    void beginAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException;

    @DexIgnore
    void endAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void generateEventId(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getAppInstanceId(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getCachedAppInstanceId(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getConditionalUserProperties(String str, String str2, ev2 ev2) throws RemoteException;

    @DexIgnore
    void getCurrentScreenClass(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getCurrentScreenName(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getGmpAppId(ev2 ev2) throws RemoteException;

    @DexIgnore
    void getMaxUserProperties(String str, ev2 ev2) throws RemoteException;

    @DexIgnore
    void getTestFlag(ev2 ev2, int i) throws RemoteException;

    @DexIgnore
    void getUserProperties(String str, String str2, boolean z, ev2 ev2) throws RemoteException;

    @DexIgnore
    void initForTests(Map map) throws RemoteException;

    @DexIgnore
    void initialize(x52 x52, mv2 mv2, long j) throws RemoteException;

    @DexIgnore
    void isDataCollectionEnabled(ev2 ev2) throws RemoteException;

    @DexIgnore
    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException;

    @DexIgnore
    void logEventAndBundle(String str, String str2, Bundle bundle, ev2 ev2, long j) throws RemoteException;

    @DexIgnore
    void logHealthData(int i, String str, x52 x52, x52 x522, x52 x523) throws RemoteException;

    @DexIgnore
    void onActivityCreated(x52 x52, Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void onActivityDestroyed(x52 x52, long j) throws RemoteException;

    @DexIgnore
    void onActivityPaused(x52 x52, long j) throws RemoteException;

    @DexIgnore
    void onActivityResumed(x52 x52, long j) throws RemoteException;

    @DexIgnore
    void onActivitySaveInstanceState(x52 x52, ev2 ev2, long j) throws RemoteException;

    @DexIgnore
    void onActivityStarted(x52 x52, long j) throws RemoteException;

    @DexIgnore
    void onActivityStopped(x52 x52, long j) throws RemoteException;

    @DexIgnore
    void performAction(Bundle bundle, ev2 ev2, long j) throws RemoteException;

    @DexIgnore
    void registerOnMeasurementEventListener(jv2 jv2) throws RemoteException;

    @DexIgnore
    void resetAnalyticsData(long j) throws RemoteException;

    @DexIgnore
    void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void setCurrentScreen(x52 x52, String str, String str2, long j) throws RemoteException;

    @DexIgnore
    void setDataCollectionEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setEventInterceptor(jv2 jv2) throws RemoteException;

    @DexIgnore
    void setInstanceIdProvider(kv2 kv2) throws RemoteException;

    @DexIgnore
    void setMeasurementEnabled(boolean z, long j) throws RemoteException;

    @DexIgnore
    void setMinimumSessionDuration(long j) throws RemoteException;

    @DexIgnore
    void setSessionTimeoutDuration(long j) throws RemoteException;

    @DexIgnore
    void setUserId(String str, long j) throws RemoteException;

    @DexIgnore
    void setUserProperty(String str, String str2, x52 x52, boolean z, long j) throws RemoteException;

    @DexIgnore
    void unregisterOnMeasurementEventListener(jv2 jv2) throws RemoteException;
}
