package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c34 extends RecyclerView.g<c> {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public List<Alarm> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Alarm alarm);

        @DexIgnore
        void b(Alarm alarm);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ pf4 a;
        @DexIgnore
        public /* final */ /* synthetic */ c34 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.b((Alarm) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.a((Alarm) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v5, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(c34 c34, pf4 pf4) {
            super(pf4.d());
            wg6.b(pf4, "binding");
            this.b = c34;
            this.a = pf4;
            this.a.q.setOnClickListener(new a(this));
            this.a.u.setOnClickListener(new b(this));
            String b2 = ThemeManager.l.a().b("nonBrandSurface");
            if (!TextUtils.isEmpty(b2)) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r3v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r3v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        @SuppressLint({"SetTextI18n"})
        public final void a(Alarm alarm, boolean z) {
            Alarm alarm2 = alarm;
            boolean z2 = z;
            wg6.b(alarm2, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            FLogger.INSTANCE.getLocal().d(c34.c, "Alarm: " + alarm2 + ", isSingleAlarm=" + z2);
            int totalMinutes = alarm.getTotalMinutes();
            int hour = alarm.getHour();
            int minute = alarm.getMinute();
            View d = this.a.d();
            wg6.a((Object) d, "binding.root");
            if (DateFormat.is24HourFormat(d.getContext())) {
                Object r3 = this.a.s;
                wg6.a((Object) r3, "binding.ftvTime");
                StringBuilder sb = new StringBuilder();
                nh6 nh6 = nh6.a;
                Locale locale = Locale.US;
                wg6.a((Object) locale, "Locale.US");
                Object[] objArr = {Integer.valueOf(hour)};
                String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                sb.append(format);
                sb.append(':');
                nh6 nh62 = nh6.a;
                Locale locale2 = Locale.US;
                wg6.a((Object) locale2, "Locale.US");
                Object[] objArr2 = {Integer.valueOf(minute)};
                String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                sb.append(format2);
                r3.setText(sb.toString());
            } else {
                int i = 12;
                if (totalMinutes < 720) {
                    if (hour != 0) {
                        i = hour;
                    }
                    Object r32 = this.a.s;
                    wg6.a((Object) r32, "binding.ftvTime");
                    yk4 yk4 = yk4.b;
                    StringBuilder sb2 = new StringBuilder();
                    nh6 nh63 = nh6.a;
                    Locale locale3 = Locale.US;
                    wg6.a((Object) locale3, "Locale.US");
                    Object[] objArr3 = {Integer.valueOf(i)};
                    String format3 = String.format(locale3, "%02d", Arrays.copyOf(objArr3, objArr3.length));
                    wg6.a((Object) format3, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format3);
                    sb2.append(':');
                    nh6 nh64 = nh6.a;
                    Locale locale4 = Locale.US;
                    wg6.a((Object) locale4, "Locale.US");
                    Object[] objArr4 = {Integer.valueOf(minute)};
                    String format4 = String.format(locale4, "%02d", Arrays.copyOf(objArr4, objArr4.length));
                    wg6.a((Object) format4, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format4);
                    sb2.append(' ');
                    String sb3 = sb2.toString();
                    View d2 = this.a.d();
                    wg6.a((Object) d2, "binding.root");
                    String a2 = jm4.a(d2.getContext(), 2131886102);
                    wg6.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    r32.setText(yk4.a(sb3, a2, 1.0f));
                } else {
                    if (hour > 12) {
                        i = hour - 12;
                    }
                    Object r33 = this.a.s;
                    wg6.a((Object) r33, "binding.ftvTime");
                    yk4 yk42 = yk4.b;
                    StringBuilder sb4 = new StringBuilder();
                    nh6 nh65 = nh6.a;
                    Locale locale5 = Locale.US;
                    wg6.a((Object) locale5, "Locale.US");
                    Object[] objArr5 = {Integer.valueOf(i)};
                    String format5 = String.format(locale5, "%02d", Arrays.copyOf(objArr5, objArr5.length));
                    wg6.a((Object) format5, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format5);
                    sb4.append(':');
                    nh6 nh66 = nh6.a;
                    Locale locale6 = Locale.US;
                    wg6.a((Object) locale6, "Locale.US");
                    Object[] objArr6 = {Integer.valueOf(minute)};
                    String format6 = String.format(locale6, "%02d", Arrays.copyOf(objArr6, objArr6.length));
                    wg6.a((Object) format6, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format6);
                    sb4.append(' ');
                    String sb5 = sb4.toString();
                    View d3 = this.a.d();
                    wg6.a((Object) d3, "binding.root");
                    String a3 = jm4.a(d3.getContext(), 2131886104);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                    r33.setText(yk42.a(sb5, a3, 1.0f));
                }
            }
            int[] days = alarm.getDays();
            int length = days != null ? days.length : 0;
            StringBuilder sb6 = new StringBuilder("");
            if (length > 0 && alarm.isRepeated()) {
                if (length == 7) {
                    sb6.append(jm4.a((Context) PortfolioApp.get.instance(), 2131886108));
                    wg6.a((Object) sb6, "strDays.append(LanguageH\u2026_Alerts_Label__EveryDay))");
                } else {
                    if (length == 2) {
                        c34 c34 = this.b;
                        if (days == null) {
                            wg6.a();
                            throw null;
                        } else if (c34.a(days)) {
                            sb6.append(jm4.a((Context) PortfolioApp.get.instance(), 2131886109));
                            wg6.a((Object) sb6, "strDays.append(LanguageH\u2026rts_Label__EveryWeekend))");
                        }
                    }
                    if (days != null) {
                        md6.a(days);
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = 1;
                            while (true) {
                                if (i3 > 7) {
                                    break;
                                } else if (i3 == days[i2]) {
                                    sb6.append(yk4.b.a(i3));
                                    if (i2 < length - 1) {
                                        sb6.append(", ");
                                    }
                                } else {
                                    i3++;
                                }
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            Object r34 = this.a.t;
            wg6.a((Object) r34, "binding.ftvTitle");
            r34.setText(alarm.getTitle());
            Object r35 = this.a.r;
            wg6.a((Object) r35, "binding.ftvRepeatedDays");
            r35.setText(sb6.toString());
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
            wg6.a((Object) flexibleSwitchCompat, "binding.swEnabled");
            flexibleSwitchCompat.setChecked(alarm.isActive());
            if (z2) {
                CardView cardView = this.a.q;
                wg6.a((Object) cardView, "binding.cvRoot");
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                qk4 b2 = qk4.b();
                wg6.a((Object) b2, "MeasureHelper.getInstance()");
                layoutParams.width = (int) (((float) b2.a()) - gy5.a(32.0f));
                return;
            }
            CardView cardView2 = this.a.q;
            wg6.a((Object) cardView2, "binding.cvRoot");
            ViewGroup.LayoutParams layoutParams2 = cardView2.getLayoutParams();
            qk4 b3 = qk4.b();
            wg6.a((Object) b3, "MeasureHelper.getInstance()");
            layoutParams2.width = (int) ((((float) b3.a()) - gy5.a(32.0f)) / 2.2f);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = c34.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmsAdapter::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        pf4 a2 = pf4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemAlarmBinding.inflate\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        Alarm alarm = this.a.get(i);
        boolean z = true;
        if (this.a.size() != 1) {
            z = false;
        }
        cVar.a(alarm, z);
    }

    @DexIgnore
    public final void a(List<Alarm> list) {
        wg6.b(list, "alarms");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        if (iArr.length != 2) {
            return false;
        }
        if ((iArr[0] == 1 && iArr[1] == 7) || (iArr[0] == 7 && iArr[1] == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
