package com.fossil;

import com.fossil.cx6;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx6 extends cx6.a {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements cx6<Object, Call<?>> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public a(gx6 gx6, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public Call<Object> a(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor a;
        @DexIgnore
        public /* final */ Call<T> b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements dx6<T> {
            @DexIgnore
            public /* final */ /* synthetic */ dx6 a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gx6$b$a$a")
            /* renamed from: com.fossil.gx6$b$a$a  reason: collision with other inner class name */
            public class C0002a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ rx6 a;

                @DexIgnore
                public C0002a(rx6 rx6) {
                    this.a = rx6;
                }

                @DexIgnore
                public void run() {
                    if (b.this.b.v()) {
                        a aVar = a.this;
                        aVar.a.onFailure(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.a.onResponse(b.this, this.a);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gx6$b$a$b")
            /* renamed from: com.fossil.gx6$b$a$b  reason: collision with other inner class name */
            public class C0003b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable a;

                @DexIgnore
                public C0003b(Throwable th) {
                    this.a = th;
                }

                @DexIgnore
                public void run() {
                    a aVar = a.this;
                    aVar.a.onFailure(b.this, this.a);
                }
            }

            @DexIgnore
            public a(dx6 dx6) {
                this.a = dx6;
            }

            @DexIgnore
            public void onFailure(Call<T> call, Throwable th) {
                b.this.a.execute(new C0003b(th));
            }

            @DexIgnore
            public void onResponse(Call<T> call, rx6<T> rx6) {
                b.this.a.execute(new C0002a(rx6));
            }
        }

        @DexIgnore
        public b(Executor executor, Call<T> call) {
            this.a = executor;
            this.b = call;
        }

        @DexIgnore
        public void a(dx6<T> dx6) {
            vx6.a(dx6, "callback == null");
            this.b.a(new a(dx6));
        }

        @DexIgnore
        public void cancel() {
            this.b.cancel();
        }

        @DexIgnore
        public rx6<T> s() throws IOException {
            return this.b.s();
        }

        @DexIgnore
        public yq6 t() {
            return this.b.t();
        }

        @DexIgnore
        public boolean v() {
            return this.b.v();
        }

        @DexIgnore
        public Call<T> clone() {
            return new b(this.a, this.b.clone());
        }
    }

    @DexIgnore
    public gx6(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public cx6<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (cx6.a.a(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = vx6.b(0, (ParameterizedType) type);
            if (!vx6.a(annotationArr, (Class<? extends Annotation>) tx6.class)) {
                executor = this.a;
            }
            return new a(this, b2, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
