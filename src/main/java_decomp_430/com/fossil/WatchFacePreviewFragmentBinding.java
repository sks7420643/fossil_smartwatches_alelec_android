package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchFacePreviewFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ TextView s;
    @DexIgnore
    public /* final */ TextView t;
    @DexIgnore
    public /* final */ CustomizeWidget wc_bottom;
    @DexIgnore
    public /* final */ CustomizeWidget wc_end;
    @DexIgnore
    public /* final */ CustomizeWidget wc_start;
    @DexIgnore
    public /* final */ CustomizeWidget wc_top;

    @DexIgnore
    public WatchFacePreviewFragmentBinding(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, TextView textView, TextView textView2, TextView textView3, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4) {
        super(obj, view, i);
        this.q = constraintLayout2;
        this.r = imageView;
        this.s = textView;
        this.t = textView2;
        this.wc_bottom = customizeWidget;
        this.wc_end = customizeWidget2;
        this.wc_start = customizeWidget3;
        this.wc_top = customizeWidget4;
    }
}
