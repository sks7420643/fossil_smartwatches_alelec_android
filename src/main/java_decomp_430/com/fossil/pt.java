package com.fossil;

import com.fossil.et;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pt<Data, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ v8<List<Throwable>> a;
    @DexIgnore
    public /* final */ List<? extends et<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public pt(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<et<Data, ResourceType, Transcode>> list, v8<List<Throwable>> v8Var) {
        this.a = v8Var;
        q00.a(list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public rt<Transcode> a(gs<Data> gsVar, xr xrVar, int i, int i2, et.a<ResourceType> aVar) throws mt {
        List<Throwable> a2 = this.a.a();
        q00.a(a2);
        List list = a2;
        try {
            return a(gsVar, xrVar, i, i2, aVar, list);
        } finally {
            this.a.a(list);
        }
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }

    @DexIgnore
    public final rt<Transcode> a(gs<Data> gsVar, xr xrVar, int i, int i2, et.a<ResourceType> aVar, List<Throwable> list) throws mt {
        List<Throwable> list2 = list;
        int size = this.b.size();
        rt<Transcode> rtVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                rtVar = ((et) this.b.get(i3)).a(gsVar, i, i2, xrVar, aVar);
            } catch (mt e) {
                list2.add(e);
            }
            if (rtVar != null) {
                break;
            }
        }
        if (rtVar != null) {
            return rtVar;
        }
        throw new mt(this.c, (List<Throwable>) new ArrayList(list2));
    }
}
