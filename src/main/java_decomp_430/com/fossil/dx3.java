package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dx3 extends ex3 {

    @DexIgnore
    public interface a extends ex3, Cloneable {
        @DexIgnore
        a a(dx3 dx3);

        @DexIgnore
        dx3 build();
    }

    @DexIgnore
    void a(uw3 uw3) throws IOException;

    @DexIgnore
    a c();

    @DexIgnore
    int d();

    @DexIgnore
    gx3<? extends dx3> e();
}
