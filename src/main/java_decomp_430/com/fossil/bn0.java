package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn0 extends p40 {
    @DexIgnore
    public static /* final */ eg0 f; // = new eg0((qg6) null);
    @DexIgnore
    public /* final */ lx0 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ il0 c;
    @DexIgnore
    public /* final */ ch0 d;
    @DexIgnore
    public /* final */ sj0 e;

    @DexIgnore
    public /* synthetic */ bn0(lx0 lx0, String str, il0 il0, ch0 ch0, sj0 sj0, int i) {
        lx0 = (i & 1) != 0 ? lx0.UNKNOWN : lx0;
        str = (i & 2) != 0 ? "" : str;
        ch0 = (i & 8) != 0 ? new ch0((hm0) null, lf0.SUCCESS, (t31) null, 5) : ch0;
        sj0 = (i & 16) != 0 ? null : sj0;
        this.a = lx0;
        this.b = str;
        this.c = il0;
        this.d = ch0;
        this.e = sj0;
    }

    @DexIgnore
    public static /* synthetic */ bn0 a(bn0 bn0, lx0 lx0, String str, il0 il0, ch0 ch0, sj0 sj0, int i) {
        if ((i & 1) != 0) {
            lx0 = bn0.a;
        }
        lx0 lx02 = lx0;
        if ((i & 2) != 0) {
            str = bn0.b;
        }
        String str2 = str;
        if ((i & 4) != 0) {
            il0 = bn0.c;
        }
        il0 il02 = il0;
        if ((i & 8) != 0) {
            ch0 = bn0.d;
        }
        ch0 ch02 = ch0;
        if ((i & 16) != 0) {
            sj0 = bn0.e;
        }
        return bn0.a(lx02, str2, il02, ch02, sj0);
    }

    @DexIgnore
    public final bn0 a(lx0 lx0, String str, il0 il0, ch0 ch0, sj0 sj0) {
        return new bn0(lx0, str, il0, ch0, sj0);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(jSONObject, bm0.REQUEST_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.c));
            sj0 sj0 = this.e;
            if (sj0 != null && !sj0.a()) {
                cw0.a(jSONObject, bm0.RESPONSE_STATUS, (Object) this.e.getLogName());
            }
            if (this.d.b != lf0.SUCCESS) {
                cw0.a(jSONObject, bm0.COMMAND_RESULT, (Object) this.d.a());
            }
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bn0)) {
            return false;
        }
        bn0 bn0 = (bn0) obj;
        return wg6.a(this.a, bn0.a) && wg6.a(this.b, bn0.b) && wg6.a(this.c, bn0.c) && wg6.a(this.d, bn0.d) && wg6.a(this.e, bn0.e);
    }

    @DexIgnore
    public int hashCode() {
        lx0 lx0 = this.a;
        int i = 0;
        int hashCode = (lx0 != null ? lx0.hashCode() : 0) * 31;
        String str = this.b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        il0 il0 = this.c;
        int hashCode3 = (hashCode2 + (il0 != null ? il0.hashCode() : 0)) * 31;
        ch0 ch0 = this.d;
        int hashCode4 = (hashCode3 + (ch0 != null ? ch0.hashCode() : 0)) * 31;
        sj0 sj0 = this.e;
        if (sj0 != null) {
            i = sj0.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("Result(requestId=");
        b2.append(this.a);
        b2.append(", requestUuid=");
        b2.append(this.b);
        b2.append(", resultCode=");
        b2.append(this.c);
        b2.append(", commandResult=");
        b2.append(this.d);
        b2.append(", responseStatus=");
        b2.append(this.e);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public bn0(lx0 lx0, String str, il0 il0, ch0 ch0, sj0 sj0) {
        this.a = lx0;
        this.b = str;
        this.c = il0;
        this.d = ch0;
        this.e = sj0;
    }
}
