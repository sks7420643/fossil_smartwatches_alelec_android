package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ld2 extends uc2 implements md2 {
    @DexIgnore
    public ld2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    public final void a(h82 h82) throws RemoteException {
        Parcel zza = zza();
        sd2.a(zza, (Parcelable) h82);
        a(3, zza);
    }
}
