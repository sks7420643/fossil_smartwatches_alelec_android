package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db3 implements Parcelable.Creator<ab3> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        la3 la3 = null;
        String str3 = null;
        j03 j03 = null;
        j03 j032 = null;
        j03 j033 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 2:
                    str = f22.e(parcel2, a);
                    break;
                case 3:
                    str2 = f22.e(parcel2, a);
                    break;
                case 4:
                    la3 = f22.a(parcel2, a, la3.CREATOR);
                    break;
                case 5:
                    j = f22.s(parcel2, a);
                    break;
                case 6:
                    z = f22.i(parcel2, a);
                    break;
                case 7:
                    str3 = f22.e(parcel2, a);
                    break;
                case 8:
                    j03 = f22.a(parcel2, a, j03.CREATOR);
                    break;
                case 9:
                    j2 = f22.s(parcel2, a);
                    break;
                case 10:
                    j032 = f22.a(parcel2, a, j03.CREATOR);
                    break;
                case 11:
                    j3 = f22.s(parcel2, a);
                    break;
                case 12:
                    j033 = f22.a(parcel2, a, j03.CREATOR);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new ab3(str, str2, la3, j, z, str3, j03, j2, j032, j3, j033);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ab3[i];
    }
}
