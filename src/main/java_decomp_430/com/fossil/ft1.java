package com.fossil;

import android.util.SparseArray;
import java.util.EnumMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft1 {
    @DexIgnore
    public static SparseArray<go1> a; // = new SparseArray<>();
    @DexIgnore
    public static EnumMap<go1, Integer> b; // = new EnumMap<>(go1.class);

    /*
    static {
        b.put(go1.DEFAULT, 0);
        b.put(go1.VERY_LOW, 1);
        b.put(go1.HIGHEST, 2);
        for (go1 next : b.keySet()) {
            a.append(b.get(next).intValue(), next);
        }
    }
    */

    @DexIgnore
    public static go1 a(int i) {
        go1 go1 = a.get(i);
        if (go1 != null) {
            return go1;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i);
    }

    @DexIgnore
    public static int a(go1 go1) {
        Integer num = b.get(go1);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + go1);
    }
}
