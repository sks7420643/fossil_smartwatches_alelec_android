package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc5$b$a<T> implements ld<cf<ActivitySummary>> {
    @DexIgnore
    public /* final */ /* synthetic */ DashboardActivityPresenter.b a;

    @DexIgnore
    public bc5$b$a(DashboardActivityPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(cf<ActivitySummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging observer size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardActivityPresenter", sb.toString());
        if (cfVar != null) {
            this.a.this$0.h.a(cfVar);
        }
    }
}
