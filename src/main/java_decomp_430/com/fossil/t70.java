package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum t70 {
    ACCEPT_PHONE_CALL((byte) 0),
    REJECT_PHONE_CALL((byte) 1),
    DISMISS_NOTIFICATION((byte) 2),
    REPLY_MESSAGE((byte) 3);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final t70 a(byte b) {
            for (t70 t70 : t70.values()) {
                if (t70.a() == b) {
                    return t70;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public t70(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
