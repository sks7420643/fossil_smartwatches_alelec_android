package com.fossil;

import android.content.Context;
import com.fossil.np1;
import com.fossil.rp1;
import com.fossil.xp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wp1 implements vp1 {
    @DexIgnore
    public static volatile xp1 e;
    @DexIgnore
    public /* final */ zs1 a;
    @DexIgnore
    public /* final */ zs1 b;
    @DexIgnore
    public /* final */ uq1 c;
    @DexIgnore
    public /* final */ lr1 d;

    @DexIgnore
    public wp1(zs1 zs1, zs1 zs12, uq1 uq1, lr1 lr1, pr1 pr1) {
        this.a = zs1;
        this.b = zs12;
        this.c = uq1;
        this.d = lr1;
        pr1.a();
    }

    @DexIgnore
    public static void a(Context context) {
        if (e == null) {
            synchronized (wp1.class) {
                if (e == null) {
                    xp1.a m = lp1.m();
                    m.a(context);
                    e = m.build();
                }
            }
        }
    }

    @DexIgnore
    public static wp1 b() {
        xp1 xp1 = e;
        if (xp1 != null) {
            return xp1.l();
        }
        throw new IllegalStateException("Not initialized!");
    }

    @DexIgnore
    @Deprecated
    public jo1 a(String str) {
        rp1.a d2 = rp1.d();
        d2.a(str);
        return new sp1(d2.a(), this);
    }

    @DexIgnore
    public lr1 a() {
        return this.d;
    }

    @DexIgnore
    public void a(qp1 qp1, ko1 ko1) {
        this.c.a(qp1.d().a(qp1.a().c()), a(qp1), ko1);
    }

    @DexIgnore
    public final np1 a(qp1 qp1) {
        np1.a i = np1.i();
        i.a(this.a.a());
        i.b(this.b.a());
        i.a(qp1.e());
        i.a(qp1.b());
        i.a(qp1.a().a());
        return i.a();
    }
}
