package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bw<T> implements rt<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public bw(T t) {
        q00.a(t);
        this.a = t;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final int b() {
        return 1;
    }

    @DexIgnore
    public Class<T> c() {
        return this.a.getClass();
    }

    @DexIgnore
    public final T get() {
        return this.a;
    }
}
