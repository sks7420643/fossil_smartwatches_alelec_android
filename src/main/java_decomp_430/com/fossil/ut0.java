package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import com.facebook.places.PlaceManager;
import com.fossil.r40;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut0 {
    @DexIgnore
    public static /* final */ Hashtable<String, ii1> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ut0 c;

    /*
    static {
        String string;
        ut0 ut0 = new ut0();
        c = ut0;
        SharedPreferences a2 = cw0.a(lp0.MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE);
        if (a2 != null && (string = a2.getString("com.fossil.blesdk.device.cache", (String) null)) != null) {
            wg6.a(string, "getSharedPreferences(Sha\u2026ENCE_KEY, null) ?: return");
            try {
                JSONArray jSONArray = new JSONArray(Crypto.instance.decryptAES_CBC(string));
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string2 = jSONObject.getString(PlaceManager.PARAM_MAC_ADDRESS);
                    String string3 = jSONObject.getString("serial_number");
                    wg6.a(string3, "serialNumber");
                    wg6.a(string2, "macAddress");
                    ut0.a(string3, string2);
                }
            } catch (Exception e) {
                qs0.h.a(e);
            }
        }
    }
    */

    @DexIgnore
    public final ii1 a(BluetoothDevice bluetoothDevice, String str) {
        ii1 ii1;
        String str2 = str;
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice.getAddress(), str2};
        synchronized (a) {
            ii1 ii12 = a.get(bluetoothDevice.getAddress());
            if (ii12 == null) {
                ii12 = new ii1(bluetoothDevice, str2, (qg6) null);
                a.put(ii12.r.getMacAddress(), ii12);
                if (r40.t.a(str2)) {
                    c.a(str2, ii12.r.getMacAddress());
                }
            } else if (!r40.t.a(ii12.r.getSerialNumber()) && r40.t.a(str2)) {
                ii1 ii13 = ii12;
                ii13.r = r40.a(ii12.r, (String) null, (String) null, str, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262139);
                a.put(ii13.r.getMacAddress(), ii13);
                ii1 = ii13;
                c.a(str, ii13.r.getMacAddress());
            }
            ii1 = ii12;
        }
        return ii1;
    }

    @DexIgnore
    public final String b(String str) {
        synchronized (b) {
            for (Map.Entry next : b.entrySet()) {
                String str2 = (String) next.getKey();
                if (xj6.b((String) next.getValue(), str, true)) {
                    return str2;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public final String a(String str) {
        String str2;
        synchronized (b) {
            str2 = b.get(str);
        }
        return str2;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (r40.t.a(str) && BluetoothAdapter.checkBluetoothAddress(str2)) {
            synchronized (b) {
                b.put(str, str2);
                cd6 cd6 = cd6.a;
            }
        }
    }

    @DexIgnore
    public final void a() {
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            for (Map.Entry next : b.entrySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("serial_number", next.getKey());
                    jSONObject.put(PlaceManager.PARAM_MAC_ADDRESS, next.getValue());
                } catch (Exception e) {
                    qs0.h.a(e);
                }
                jSONArray.put(jSONObject);
            }
            cd6 cd6 = cd6.a;
        }
        String jSONArray2 = jSONArray.toString();
        wg6.a(jSONArray2, "array.toString()");
        String encryptAES_CBC = Crypto.instance.encryptAES_CBC(jSONArray2);
        SharedPreferences a2 = cw0.a(lp0.MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE);
        if (a2 != null) {
            a2.edit().putString("com.fossil.blesdk.device.cache", encryptAES_CBC).apply();
        }
    }
}
