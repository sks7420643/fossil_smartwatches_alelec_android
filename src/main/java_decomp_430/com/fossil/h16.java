package com.fossil;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import com.fossil.n16;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h16 extends n16 {
    @DexIgnore
    public /* final */ Downloader a;
    @DexIgnore
    public /* final */ p16 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore
    public h16(Downloader downloader, p16 p16) {
        this.a = downloader;
        this.b = p16;
    }

    @DexIgnore
    public int a() {
        return 2;
    }

    @DexIgnore
    public boolean a(l16 l16) {
        String scheme = l16.d.getScheme();
        return "http".equals(scheme) || "https".equals(scheme);
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        Downloader.Response load = this.a.load(l16.d, l16.c);
        if (load == null) {
            return null;
        }
        Picasso.LoadedFrom loadedFrom = load.c ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap a2 = load.a();
        if (a2 != null) {
            return new n16.a(a2, loadedFrom);
        }
        InputStream c = load.c();
        if (c == null) {
            return null;
        }
        if (loadedFrom == Picasso.LoadedFrom.DISK && load.b() == 0) {
            t16.a(c);
            throw new a("Received response with 0 content-length header.");
        }
        if (loadedFrom == Picasso.LoadedFrom.NETWORK && load.b() > 0) {
            this.b.a(load.b());
        }
        return new n16.a(c, loadedFrom);
    }

    @DexIgnore
    public boolean a(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }
}
