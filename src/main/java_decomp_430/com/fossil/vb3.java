package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vb3> CREATOR; // = new ub3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ x12 b;

    @DexIgnore
    public vb3(int i, x12 x12) {
        this.a = i;
        this.b = x12;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, (Parcelable) this.b, i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public vb3(x12 x12) {
        this(1, x12);
    }
}
