package com.fossil;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wh implements Executor {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ ArrayDeque<Runnable> b; // = new ArrayDeque<>();
    @DexIgnore
    public Runnable c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable a;

        @DexIgnore
        public a(Runnable runnable) {
            this.a = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.a.run();
            } finally {
                wh.this.a();
            }
        }
    }

    @DexIgnore
    public wh(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public synchronized void a() {
        Runnable poll = this.b.poll();
        this.c = poll;
        if (poll != null) {
            this.a.execute(this.c);
        }
    }

    @DexIgnore
    public synchronized void execute(Runnable runnable) {
        this.b.offer(new a(runnable));
        if (this.c == null) {
            a();
        }
    }
}
