package com.fossil;

import com.fossil.lo6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ko6<E> {
    @DexIgnore
    public static /* final */ /* synthetic */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(ko6.class, Object.class, "_cur$internal");
    @DexIgnore
    public volatile /* synthetic */ Object _cur$internal;

    @DexIgnore
    public ko6(boolean z) {
        this._cur$internal = new lo6(8, z);
    }

    @DexIgnore
    public final void a() {
        while (true) {
            lo6 lo6 = (lo6) this._cur$internal;
            if (!lo6.a()) {
                a.compareAndSet(this, lo6, lo6.e());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int b() {
        return ((lo6) this._cur$internal).b();
    }

    @DexIgnore
    public final E c() {
        E e;
        E e2;
        while (true) {
            lo6 lo6 = (lo6) this._cur$internal;
            while (true) {
                long j = lo6._state$internal;
                e = null;
                if ((1152921504606846976L & j) == 0) {
                    lo6.a aVar = lo6.h;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((lo6.a & ((int) ((1152921503533105152L & j) >> 30))) == (lo6.a & i)) {
                        break;
                    }
                    e2 = lo6.b.get(lo6.a & i);
                    if (e2 != null) {
                        if (!(e2 instanceof lo6.b)) {
                            int i2 = (i + 1) & 1073741823;
                            if (!lo6.f.compareAndSet(lo6, j, lo6.h.a(j, i2))) {
                                if (lo6.d) {
                                    lo6 lo62 = lo6;
                                    do {
                                        lo62 = lo62.a(i, i2);
                                    } while (lo62 != null);
                                    break;
                                }
                            } else {
                                lo6.b.set(lo6.a & i, (Object) null);
                                break;
                            }
                        } else {
                            break;
                        }
                    } else if (lo6.d) {
                        break;
                    }
                } else {
                    e = lo6.g;
                    break;
                }
            }
            e = e2;
            if (e != lo6.g) {
                return e;
            }
            a.compareAndSet(this, lo6, lo6.e());
        }
    }

    @DexIgnore
    public final boolean a(E e) {
        wg6.b(e, "element");
        while (true) {
            lo6 lo6 = (lo6) this._cur$internal;
            int a2 = lo6.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, lo6, lo6.e());
            } else if (a2 == 2) {
                return false;
            }
        }
    }
}
