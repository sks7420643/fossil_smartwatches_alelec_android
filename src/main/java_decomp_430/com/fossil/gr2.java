package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr2 implements dr2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.app_launch.event_ordering_fix", false);
        vk2.a("measurement.id.app_launch.event_ordering_fix", 0);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
