package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu4 extends RecyclerView.g<c> {
    @DexIgnore
    public List<lc6<ShineDevice, String>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ fr b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, c cVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ View d;
        @DexIgnore
        public /* final */ /* synthetic */ pu4 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void onImageCallback(String str, String str2) {
                wg6.b(str, "serial");
                wg6.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "set image for serial=" + str + " path " + str2);
                this.a.e.b.a(str2).a(new nz().b(2131231364).c()).a(this.a.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void onImageCallback(String str, String str2) {
                wg6.b(str, "fastPairId");
                wg6.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "setImageCallback() for wearOS, fastPairId=" + str);
                this.a.e.b.a(str2).a(new nz().b(2131231098).c()).a(this.a.c);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pu4 pu4, View view) {
            super(view);
            wg6.b(view, "view");
            this.e = pu4;
            View findViewById = view.findViewById(2131362331);
            wg6.a((Object) findViewById, "view.findViewById(R.id.ftv_device_serial)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362330);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.ftv_device_name)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362567);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.iv_device_image)");
            this.c = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131362908);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.scan_device_container)");
            this.d = findViewById4;
            this.d.setOnClickListener(this);
        }

        @DexIgnore
        public void onClick(View view) {
            b a2;
            wg6.b(view, "view");
            if (this.e.getItemCount() > getAdapterPosition() && getAdapterPosition() != -1 && (a2 = this.e.c) != null) {
                a2.a(view, this, getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v10, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r9v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r9v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r9v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public final void a(ShineDevice shineDevice, String str) {
            wg6.b(shineDevice, "shineDevice");
            wg6.b(str, "deviceName");
            this.b.setText(str);
            String serial = shineDevice.getSerial();
            wg6.a((Object) serial, "shineDevice.serial");
            boolean z = true;
            if (serial.length() > 0) {
                Object r9 = this.a;
                nh6 nh6 = nh6.a;
                Locale locale = Locale.US;
                wg6.a((Object) locale, "Locale.US");
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886972);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026 R.string.Serial_pattern)");
                Object[] objArr = {shineDevice.getSerial()};
                String format = String.format(locale, a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                r9.setText(format);
                this.a.setVisibility(0);
            } else {
                this.a.setVisibility(4);
            }
            String serial2 = shineDevice.getSerial();
            wg6.a((Object) serial2, "shineDevice.serial");
            if (serial2.length() == 0) {
                String fastPairId = shineDevice.getFastPairId();
                wg6.a((Object) fastPairId, "shineDevice.fastPairId");
                if (fastPairId.length() != 0) {
                    z = false;
                }
                if (z) {
                    this.c.setImageDrawable(PortfolioApp.get.instance().getDrawable(2131231098));
                    return;
                }
            }
            a(shineDevice);
        }

        @DexIgnore
        public final void a(ShineDevice shineDevice) {
            String serial = shineDevice.getSerial();
            wg6.a((Object) serial, "shineDevice.serial");
            if (serial.length() > 0) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                String serial2 = shineDevice.getSerial();
                wg6.a((Object) serial2, "shineDevice.serial");
                CloudImageHelper.ItemImage type = with.setSerialNumber(serial2).setSerialPrefix(DeviceHelper.o.b(shineDevice.getSerial())).setType(Constants.DeviceType.TYPE_LARGE);
                ImageView imageView = this.c;
                DeviceHelper.a aVar = DeviceHelper.o;
                String serial3 = shineDevice.getSerial();
                wg6.a((Object) serial3, "shineDevice.serial");
                type.setPlaceHolder(imageView, aVar.b(serial3, DeviceHelper.ImageStyle.LARGE)).setImageCallback(new a(this)).download();
                return;
            }
            CloudImageHelper.ItemImage with2 = CloudImageHelper.Companion.getInstance().with();
            String fastPairId = shineDevice.getFastPairId();
            wg6.a((Object) fastPairId, "shineDevice.fastPairId");
            with2.setFastPairId(fastPairId).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder(this.c, 2131231098).setImageCallback(new b(this)).downloadForWearOS();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<lc6<? extends ShineDevice, ? extends String>> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        /* renamed from: a */
        public final int compare(lc6<ShineDevice, String> lc6, lc6<ShineDevice, String> lc62) {
            int i;
            if (lc6 == null || lc62 == null) {
                int i2 = -1;
                int i3 = lc6 == null ? -1 : 1;
                if (lc62 != null) {
                    i2 = 1;
                }
                i = i3 - i2;
            } else {
                i = lc6.getFirst().getRssi() - lc62.getFirst().getRssi();
            }
            return -i;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public pu4(fr frVar, b bVar) {
        wg6.b(frVar, "mRequestManager");
        this.b = frVar;
        this.c = bVar;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void a(List<lc6<ShineDevice, String>> list) {
        wg6.b(list, "data");
        this.a.clear();
        ud6.a(list, d.a);
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558682, viewGroup, false);
        wg6.a((Object) inflate, "v");
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.width = (int) (((float) viewGroup.getMeasuredWidth()) * 0.5f);
        inflate.setLayoutParams(layoutParams);
        return new c(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "viewHolder");
        cVar.a((ShineDevice) this.a.get(i).getFirst(), (String) this.a.get(i).getSecond());
    }
}
