package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.zg3;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eh3 implements zg3.c {
    @DexIgnore
    public static /* final */ Parcelable.Creator<eh3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<eh3> {
        @DexIgnore
        public eh3 createFromParcel(Parcel parcel) {
            return new eh3(parcel.readLong(), (a) null);
        }

        @DexIgnore
        public eh3[] newArray(int i) {
            return new eh3[i];
        }
    }

    @DexIgnore
    public /* synthetic */ eh3(long j, a aVar) {
        this(j);
    }

    @DexIgnore
    public static eh3 b(long j) {
        return new eh3(j);
    }

    @DexIgnore
    public boolean a(long j) {
        return j >= this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof eh3) && this.a == ((eh3) obj).a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.a)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
    }

    @DexIgnore
    public eh3(long j) {
        this.a = j;
    }
}
