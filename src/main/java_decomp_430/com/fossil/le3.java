package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le3 implements Parcelable.Creator<ke3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                str = f22.e(parcel, a);
            } else if (a2 == 3) {
                str2 = f22.e(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                str3 = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new ke3(str, str2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ke3[i];
    }
}
