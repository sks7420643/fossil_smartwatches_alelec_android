package com.fossil;

import kotlinx.coroutines.TimeoutKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sn6<U, T extends U> extends ck6<T> implements Runnable, xe6<T>, kf6 {
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ xe6<U> e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sn6(long j, xe6<? super U> xe6) {
        super(xe6.getContext(), true);
        wg6.b(xe6, "uCont");
        this.d = j;
        this.e = xe6;
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (obj instanceof vk6) {
            jn6.a(this.e, ((vk6) obj).a, i);
        } else {
            jn6.b(this.e, obj, i);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public String g() {
        return super.g() + "(timeMillis=" + this.d + ')';
    }

    @DexIgnore
    public kf6 getCallerFrame() {
        xe6<U> xe6 = this.e;
        if (!(xe6 instanceof kf6)) {
            xe6 = null;
        }
        return (kf6) xe6;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }

    @DexIgnore
    public void run() {
        a((Throwable) TimeoutKt.a(this.d, (rm6) this));
    }
}
