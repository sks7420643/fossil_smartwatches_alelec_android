package com.fossil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.android.AndroidDispatcherFactory;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo6 {
    @DexIgnore
    public static /* final */ boolean a; // = vo6.a("kotlinx.coroutines.fast.service.loader", true);
    @DexIgnore
    public static /* final */ cn6 b;

    /*
    static {
        mo6 mo6 = new mo6();
        b = mo6.a();
    }
    */

    @DexIgnore
    public final cn6 a() {
        List<S> list;
        T t;
        cn6 a2;
        try {
            if (a) {
                Class<MainDispatcherFactory> cls = MainDispatcherFactory.class;
                go6 go6 = go6.a;
                ClassLoader classLoader = cls.getClassLoader();
                wg6.a((Object) classLoader, "clz.classLoader");
                list = go6.a(cls, classLoader);
            } else {
                Iterator it = Arrays.asList(new MainDispatcherFactory[]{new AndroidDispatcherFactory()}).iterator();
                wg6.a((Object) it, "ServiceLoader.load(\n    \u2026             ).iterator()");
                list = aj6.f(yi6.a(it));
            }
            Iterator<T> it2 = list.iterator();
            if (!it2.hasNext()) {
                t = null;
            } else {
                t = it2.next();
                if (it2.hasNext()) {
                    int loadPriority = ((MainDispatcherFactory) t).getLoadPriority();
                    do {
                        T next = it2.next();
                        int loadPriority2 = ((MainDispatcherFactory) next).getLoadPriority();
                        if (loadPriority < loadPriority2) {
                            t = next;
                            loadPriority = loadPriority2;
                        }
                    } while (it2.hasNext());
                }
            }
            MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory) t;
            if (mainDispatcherFactory == null || (a2 = no6.a(mainDispatcherFactory, list)) == null) {
                return new oo6((Throwable) null, (String) null, 2, (qg6) null);
            }
            return a2;
        } catch (Throwable th) {
            return new oo6(th, (String) null, 2, (qg6) null);
        }
    }
}
