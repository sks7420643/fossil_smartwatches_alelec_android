package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r21 extends j61 {
    @DexIgnore
    public h91 A; // = h91.DISCONNECTED;

    @DexIgnore
    public r21(ue1 ue1) {
        super(lx0.DISCONNECT, ue1);
    }

    @DexIgnore
    public void a(ni1 ni1) {
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.A = ((ow0) ok0).k;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) this.A)), 7));
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.PERIPHERAL_CURRENT_STATE, (Object) cw0.a((Enum<?>) this.y.s)), bm0.MAC_ADDRESS, (Object) this.y.t);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) this.A));
    }

    @DexIgnore
    public ok0 l() {
        return new ow0(this.y.v);
    }
}
