package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym4 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static ym4 b;
    @DexIgnore
    public static /* final */ a c; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(ym4 ym4) {
            ym4.b = ym4;
        }

        @DexIgnore
        public final ym4 b() {
            return ym4.b;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final synchronized ym4 a() {
            ym4 b;
            if (ym4.c.b() == null) {
                ym4.c.a(new ym4((qg6) null));
            }
            b = ym4.c.b();
            if (b == null) {
                wg6.a();
                throw null;
            }
            return b;
        }
    }

    /*
    static {
        String simpleName = ym4.class.getSimpleName();
        wg6.a((Object) simpleName, "PhoneCallManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public ym4() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void b() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.get.instance().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            wg6.a((Object) cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            wg6.a((Object) cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            wg6.a((Object) cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", new Class[]{String.class});
            wg6.a((Object) method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod("asInterface", new Class[]{IBinder.class});
            wg6.a((Object) method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface((IInterface) null, "fake");
            Object invoke = method.invoke(method2.invoke((Object) null, new Object[]{binder}), new Object[]{"phone"});
            if (invoke != null) {
                Method method3 = cls2.getMethod("asInterface", new Class[]{IBinder.class});
                wg6.a((Object) method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke((Object) null, new Object[]{(IBinder) invoke});
                Method method4 = cls.getMethod("endCall", new Class[0]);
                wg6.a((Object) method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when decline call " + e2);
        }
    }

    @DexIgnore
    public /* synthetic */ ym4(qg6 qg6) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        try {
            Object systemService = PortfolioApp.get.instance().getSystemService("telecom");
            if (systemService != null) {
                ((TelecomManager) systemService).acceptRingingCall();
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.telecom.TelecomManager");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when accept call call " + e);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v13, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r13v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(String str, String str2, BroadcastReceiver broadcastReceiver) {
        wg6.b(str, "number");
        wg6.b(str2, "message");
        wg6.b(broadcastReceiver, "receiver");
        nh6 nh6 = nh6.a;
        Object[] objArr = {PortfolioApp.get.instance().i()};
        String format = String.format("DELIVERY_%s", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a;
        local.d(str3, "sendSMSTo with " + format);
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.get.instance(), 0, new Intent(format), 0);
        PortfolioApp.get.instance().registerReceiver(broadcastReceiver, new IntentFilter(format));
        if (xm4.d.a(PortfolioApp.get.instance().getBaseContext(), 102)) {
            SmsManager smsManager = SmsManager.getDefault();
            if (smsManager != null) {
                smsManager.sendTextMessage(str, (String) null, str2, broadcast, (PendingIntent) null);
                return;
            }
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("SEND_SMS_PERMISSION_REQUIRED", true);
        broadcastReceiver.onReceive(PortfolioApp.get.instance().getBaseContext(), intent);
    }
}
