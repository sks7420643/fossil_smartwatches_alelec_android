package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendar;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh3 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ MaterialCalendar<?> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public void onClick(View view) {
            oh3.this.a.a(hh3.a(this.a, oh3.this.a.h1().c));
            oh3.this.a.a(MaterialCalendar.k.DAY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;

        @DexIgnore
        public b(TextView textView) {
            super(textView);
            this.a = textView;
        }
    }

    @DexIgnore
    public oh3(MaterialCalendar<?> materialCalendar) {
        this.a = materialCalendar;
    }

    @DexIgnore
    public int b(int i) {
        return i - this.a.f1().e().d;
    }

    @DexIgnore
    public int c(int i) {
        return this.a.f1().e().d + i;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.f1().f();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        int c = c(i);
        String string = bVar.a.getContext().getString(vf3.mtrl_picker_navigate_to_year_description);
        bVar.a.setText(String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(c)}));
        bVar.a.setContentDescription(String.format(string, new Object[]{Integer.valueOf(c)}));
        bh3 g1 = this.a.g1();
        Calendar b2 = nh3.b();
        ah3 ah3 = b2.get(1) == c ? g1.f : g1.d;
        for (Long longValue : this.a.i1().o()) {
            b2.setTimeInMillis(longValue.longValue());
            if (b2.get(1) == c) {
                ah3 = g1.e;
            }
        }
        ah3.a(bVar.a);
        bVar.a.setOnClickListener(a(c));
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(tf3.mtrl_calendar_year, viewGroup, false));
    }

    @DexIgnore
    public final View.OnClickListener a(int i) {
        return new a(i);
    }
}
