package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl1 {
    @DexIgnore
    public /* synthetic */ zl1(qg6 qg6) {
    }

    @DexIgnore
    public final pn1[] a(int i) {
        pn1[] values = pn1.values();
        ArrayList arrayList = new ArrayList();
        for (pn1 pn1 : values) {
            if ((pn1.a & i) != 0) {
                arrayList.add(pn1);
            }
        }
        Object[] array = arrayList.toArray(new pn1[0]);
        if (array != null) {
            return (pn1[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
