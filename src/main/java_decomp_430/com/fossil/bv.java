package com.fossil;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bv<Data> implements jv<File, Data> {
    @DexIgnore
    public /* final */ d<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements kv<File, Data> {
        @DexIgnore
        public /* final */ d<Data> a;

        @DexIgnore
        public a(d<Data> dVar) {
            this.a = dVar;
        }

        @DexIgnore
        public final jv<File, Data> a(nv nvVar) {
            return new bv(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b() {
            super(new a());
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<ParcelFileDescriptor> {
            @DexIgnore
            public Class<ParcelFileDescriptor> getDataClass() {
                return ParcelFileDescriptor.class;
            }

            @DexIgnore
            public ParcelFileDescriptor a(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, 268435456);
            }

            @DexIgnore
            public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }
        }
    }

    @DexIgnore
    public interface d<Data> {
        @DexIgnore
        Data a(File file) throws FileNotFoundException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a<InputStream> {
        @DexIgnore
        public e() {
            super(new a());
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<InputStream> {
            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public bv(d<Data> dVar) {
        this.a = dVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public jv.a<Data> a(File file, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(file), new c(file, this.a));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Data> implements fs<Data> {
        @DexIgnore
        public /* final */ File a;
        @DexIgnore
        public /* final */ d<Data> b;
        @DexIgnore
        public Data c;

        @DexIgnore
        public c(File file, d<Data> dVar) {
            this.a = file;
            this.b = dVar;
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super Data> aVar) {
            try {
                this.c = this.b.a(this.a);
                aVar.a(this.c);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }

        @DexIgnore
        public void a() {
            Data data = this.c;
            if (data != null) {
                try {
                    this.b.a(data);
                } catch (IOException unused) {
                }
            }
        }
    }
}
