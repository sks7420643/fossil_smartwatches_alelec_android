package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o42 {
    @DexIgnore
    public static Boolean a;
    @DexIgnore
    public static Boolean b;
    @DexIgnore
    public static Boolean c;

    @DexIgnore
    @TargetApi(21)
    public static boolean a(Context context) {
        if (b == null) {
            b = Boolean.valueOf(s42.g() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    @DexIgnore
    @TargetApi(20)
    public static boolean b(Context context) {
        if (a == null) {
            a = Boolean.valueOf(s42.f() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return a.booleanValue();
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean c(Context context) {
        if (!b(context)) {
            return false;
        }
        if (s42.h()) {
            return a(context) && !s42.i();
        }
        return true;
    }

    @DexIgnore
    public static boolean d(Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }

    @DexIgnore
    public static boolean a() {
        return "user".equals(Build.TYPE);
    }
}
