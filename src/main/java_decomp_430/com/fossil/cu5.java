package com.fossil;

import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu5 implements MembersInjector<au5> {
    @DexIgnore
    public static void a(au5 au5, RequestEmailOtp requestEmailOtp) {
        au5.e = requestEmailOtp;
    }

    @DexIgnore
    public static void a(au5 au5, VerifyEmailOtp verifyEmailOtp) {
        au5.f = verifyEmailOtp;
    }

    @DexIgnore
    public static void a(au5 au5) {
        au5.m();
    }
}
