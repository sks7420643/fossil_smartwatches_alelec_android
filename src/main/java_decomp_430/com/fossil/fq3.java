package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fq3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ wp3<?> a;
        @DexIgnore
        public /* final */ Set<b> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<b> c; // = new HashSet();

        @DexIgnore
        public b(wp3<?> wp3) {
            this.a = wp3;
        }

        @DexIgnore
        public void a(b bVar) {
            this.b.add(bVar);
        }

        @DexIgnore
        public void b(b bVar) {
            this.c.add(bVar);
        }

        @DexIgnore
        public void c(b bVar) {
            this.c.remove(bVar);
        }

        @DexIgnore
        public boolean d() {
            return this.c.isEmpty();
        }

        @DexIgnore
        public wp3<?> a() {
            return this.a;
        }

        @DexIgnore
        public Set<b> b() {
            return this.b;
        }

        @DexIgnore
        public boolean c() {
            return this.b.isEmpty();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Class<?> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!cVar.a.equals(this.a) || cVar.b != this.b) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.b).hashCode();
        }

        @DexIgnore
        public c(Class<?> cls, boolean z) {
            this.a = cls;
            this.b = z;
        }
    }

    @DexIgnore
    public static void a(List<wp3<?>> list) {
        Set<b> b2 = b(list);
        Set<b> a2 = a(b2);
        int i = 0;
        while (!a2.isEmpty()) {
            b next = a2.iterator().next();
            a2.remove(next);
            i++;
            for (b next2 : next.b()) {
                next2.c(next);
                if (next2.d()) {
                    a2.add(next2);
                }
            }
        }
        if (i != list.size()) {
            ArrayList arrayList = new ArrayList();
            for (b next3 : b2) {
                if (!next3.d() && !next3.c()) {
                    arrayList.add(next3.a());
                }
            }
            throw new hq3(arrayList);
        }
    }

    @DexIgnore
    public static Set<b> b(List<wp3<?>> list) {
        Set<b> set;
        HashMap hashMap = new HashMap(list.size());
        for (wp3 next : list) {
            b bVar = new b(next);
            Iterator it = next.c().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class cls = (Class) it.next();
                    c cVar = new c(cls, !next.g());
                    if (!hashMap.containsKey(cVar)) {
                        hashMap.put(cVar, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(cVar);
                    if (set2.isEmpty() || cVar.b) {
                        set2.add(bVar);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", new Object[]{cls}));
                    }
                }
            }
        }
        for (Set<b> it2 : hashMap.values()) {
            for (b bVar2 : it2) {
                for (gq3 next2 : bVar2.a().a()) {
                    if (next2.b() && (set = (Set) hashMap.get(new c(next2.a(), next2.d()))) != null) {
                        for (b bVar3 : set) {
                            bVar2.a(bVar3);
                            bVar3.b(bVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set addAll : hashMap.values()) {
            hashSet.addAll(addAll);
        }
        return hashSet;
    }

    @DexIgnore
    public static Set<b> a(Set<b> set) {
        HashSet hashSet = new HashSet();
        for (b next : set) {
            if (next.d()) {
                hashSet.add(next);
            }
        }
        return hashSet;
    }
}
