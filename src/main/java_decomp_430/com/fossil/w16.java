package com.fossil;

import android.util.Log;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w16 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public String b; // = null;
    @DexIgnore
    public String c; // = ShareWebViewClient.RESP_SUCC_CODE;
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    public static w16 a(String str) {
        w16 w16 = new w16();
        if (b26.a(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    w16.a = jSONObject.getString("ui");
                }
                if (!jSONObject.isNull("mc")) {
                    w16.b = jSONObject.getString("mc");
                }
                if (!jSONObject.isNull("mid")) {
                    w16.c = jSONObject.getString("mid");
                }
                if (!jSONObject.isNull("ts")) {
                    w16.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return w16;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            b26.a(jSONObject, "ui", this.a);
            b26.a(jSONObject, "mc", this.b);
            b26.a(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final String toString() {
        return b().toString();
    }
}
