package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl6<T> extends yl6<T> implements kf6, xe6<T> {
    @DexIgnore
    public Object d; // = xl6.a;
    @DexIgnore
    public /* final */ kf6 e;
    @DexIgnore
    public /* final */ Object f;
    @DexIgnore
    public /* final */ dl6 g;
    @DexIgnore
    public /* final */ xe6<T> h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vl6(dl6 dl6, xe6<? super T> xe6) {
        super(0);
        wg6.b(dl6, "dispatcher");
        wg6.b(xe6, "continuation");
        this.g = dl6;
        this.h = xe6;
        xe6<T> xe62 = this.h;
        this.e = (kf6) (!(xe62 instanceof kf6) ? null : xe62);
        this.f = yo6.a(getContext());
    }

    @DexIgnore
    public xe6<T> b() {
        return this;
    }

    @DexIgnore
    public Object c() {
        Object obj = this.d;
        if (nl6.a()) {
            if (!(obj != xl6.a)) {
                throw new AssertionError();
            }
        }
        this.d = xl6.a;
        return obj;
    }

    @DexIgnore
    public final void d(T t) {
        af6 context = this.h.getContext();
        this.d = t;
        this.c = 1;
        this.g.b(context, this);
    }

    @DexIgnore
    public kf6 getCallerFrame() {
        return this.e;
    }

    @DexIgnore
    public af6 getContext() {
        return this.h.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        af6 context;
        Object b;
        af6 context2 = this.h.getContext();
        Object a = wk6.a(obj);
        if (this.g.b(context2)) {
            this.d = a;
            this.c = 0;
            this.g.a(context2, this);
            return;
        }
        em6 b2 = on6.b.b();
        if (b2.p()) {
            this.d = a;
            this.c = 0;
            b2.a((yl6<?>) this);
            return;
        }
        b2.c(true);
        try {
            context = getContext();
            b = yo6.b(context, this.f);
            this.h.resumeWith(obj);
            cd6 cd6 = cd6.a;
            yo6.a(context, b);
            do {
            } while (b2.D());
        } catch (Throwable th) {
            try {
                a(th, (Throwable) null);
            } catch (Throwable th2) {
                b2.a(true);
                throw th2;
            }
        }
        b2.a(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.g + ", " + ol6.a((xe6<?>) this.h) + ']';
    }
}
