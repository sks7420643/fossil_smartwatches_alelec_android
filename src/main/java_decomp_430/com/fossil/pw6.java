package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.fragment.app.Fragment;
import com.fossil.h6;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pw6 {

    @DexIgnore
    public interface a extends h6.b {
        @DexIgnore
        void a(int i, List<String> list);

        @DexIgnore
        void b(int i, List<String> list);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);

        @DexIgnore
        void b(int i);
    }

    @DexIgnore
    public static boolean a(Context context, String... strArr) {
        if (Build.VERSION.SDK_INT < 23) {
            Log.w("EasyPermissions", "hasPermissions: API version < M, returning true by default");
            return true;
        } else if (context != null) {
            for (String a2 : strArr) {
                if (w6.a(context, a2) != 0) {
                    return false;
                }
            }
            return true;
        } else {
            throw new IllegalArgumentException("Can't check permissions for null context");
        }
    }

    @DexIgnore
    public static void a(qw6 qw6) {
        if (a(qw6.a().a(), qw6.c())) {
            a(qw6.a().b(), qw6.f(), qw6.c());
        } else {
            qw6.a().a(qw6.e(), qw6.d(), qw6.b(), qw6.g(), qw6.f(), qw6.c());
        }
    }

    @DexIgnore
    public static void a(int i, String[] strArr, int[] iArr, Object... objArr) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < strArr.length; i2++) {
            String str = strArr[i2];
            if (iArr[i2] == 0) {
                arrayList.add(str);
            } else {
                arrayList2.add(str);
            }
        }
        for (a aVar : objArr) {
            if (!arrayList.isEmpty() && (aVar instanceof a)) {
                aVar.b(i, arrayList);
            }
            if (!arrayList2.isEmpty() && (aVar instanceof a)) {
                aVar.a(i, arrayList2);
            }
            if (!arrayList.isEmpty() && arrayList2.isEmpty()) {
                a((Object) aVar, i);
            }
        }
    }

    @DexIgnore
    public static boolean a(Fragment fragment, List<String> list) {
        return zw6.a(fragment).a(list);
    }

    @DexIgnore
    public static boolean a(Fragment fragment, String str) {
        return zw6.a(fragment).a(str);
    }

    @DexIgnore
    public static void a(Object obj, int i, String[] strArr) {
        int[] iArr = new int[strArr.length];
        for (int i2 = 0; i2 < strArr.length; i2++) {
            iArr[i2] = 0;
        }
        a(i, strArr, iArr, obj);
    }

    @DexIgnore
    public static void a(Object obj, int i) {
        Class cls = obj.getClass();
        if (a(obj)) {
            cls = cls.getSuperclass();
        }
        while (cls != null) {
            for (Method method : cls.getDeclaredMethods()) {
                nw6 nw6 = (nw6) method.getAnnotation(nw6.class);
                if (nw6 != null && nw6.value() == i) {
                    if (method.getParameterTypes().length <= 0) {
                        try {
                            if (!method.isAccessible()) {
                                method.setAccessible(true);
                            }
                            method.invoke(obj, new Object[0]);
                        } catch (IllegalAccessException e) {
                            Log.e("EasyPermissions", "runDefaultMethod:IllegalAccessException", e);
                        } catch (InvocationTargetException e2) {
                            Log.e("EasyPermissions", "runDefaultMethod:InvocationTargetException", e2);
                        }
                    } else {
                        throw new RuntimeException("Cannot execute method " + method.getName() + " because it is non-void method and/or has input parameters.");
                    }
                }
            }
            cls = cls.getSuperclass();
        }
    }

    @DexIgnore
    public static boolean a(Object obj) {
        if (!obj.getClass().getSimpleName().endsWith("_")) {
            return false;
        }
        try {
            return Class.forName("org.androidannotations.api.view.HasViews").isInstance(obj);
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }
}
