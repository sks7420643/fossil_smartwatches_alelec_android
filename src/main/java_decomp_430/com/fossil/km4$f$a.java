package com.fossil;

import com.portfolio.platform.localization.LocalizationManager;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km4$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocalizationManager.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public km4$f$a(xe6 xe6, LocalizationManager.f fVar) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        km4$f$a km4_f_a = new km4$f$a(xe6, this.this$0);
        km4_f_a.p$ = (il6) obj;
        return km4_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((km4$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            StringBuilder sb = new StringBuilder();
            File filesDir = this.this$0.a.g().getFilesDir();
            wg6.a((Object) filesDir, "mContext.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(ZendeskConfig.SLASH);
            sb.append("language.zip");
            String sb2 = sb.toString();
            File file = new File(sb2);
            if (!file.exists()) {
                LocalizationManager localizationManager = this.this$0.a;
                this.L$0 = il6;
                this.L$1 = sb2;
                this.L$2 = file;
                this.label = 1;
                if (localizationManager.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            File file2 = (File) this.L$2;
            String str = (String) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
