package com.fossil;

import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1$name$1", f = "UserCustomizeThemeViewModel.kt", l = {31}, m = "invokeSuspend")
public final class qo5$c$a extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserCustomizeThemeViewModel.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qo5$c$a(UserCustomizeThemeViewModel.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qo5$c$a qo5_c_a = new qo5$c$a(this.this$0, xe6);
        qo5_c_a.p$ = (il6) obj;
        return qo5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qo5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            this.L$0 = this.p$;
            this.label = 1;
            obj = this.this$0.this$0.c.getThemeById((String) this.this$0.$id.element, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il6 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj != null) {
            return ((Theme) obj).getName();
        }
        wg6.a();
        throw null;
    }
}
