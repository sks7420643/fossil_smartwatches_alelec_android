package com.fossil;

import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s85 implements Factory<r85> {
    @DexIgnore
    public static WatchAppSearchPresenter a(o85 o85, WatchAppRepository watchAppRepository, an4 an4) {
        return new WatchAppSearchPresenter(o85, watchAppRepository, an4);
    }
}
