package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ic3<TResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(qc3<TResult> qc3) throws Exception;
}
