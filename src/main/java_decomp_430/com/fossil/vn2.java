package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vn2 {
    @DexIgnore
    public volatile ro2 a;
    @DexIgnore
    public volatile yl2 b;

    /*
    static {
        sm2.a();
    }
    */

    @DexIgnore
    public final ro2 a(ro2 ro2) {
        ro2 ro22 = this.a;
        this.b = null;
        this.a = ro2;
        return ro22;
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    public final ro2 b(ro2 ro2) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = ro2;
                    this.b = yl2.zza;
                    this.a = ro2;
                    this.b = yl2.zza;
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof vn2)) {
            return false;
        }
        vn2 vn2 = (vn2) obj;
        ro2 ro2 = this.a;
        ro2 ro22 = vn2.a;
        if (ro2 == null && ro22 == null) {
            return b().equals(vn2.b());
        }
        if (ro2 != null && ro22 != null) {
            return ro2.equals(ro22);
        }
        if (ro2 != null) {
            return ro2.equals(vn2.b(ro2.a()));
        }
        return b(ro22.a()).equals(ro22);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final int a() {
        if (this.b != null) {
            return this.b.zza();
        }
        if (this.a != null) {
            return this.a.e();
        }
        return 0;
    }

    @DexIgnore
    public final yl2 b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                yl2 yl2 = this.b;
                return yl2;
            }
            if (this.a == null) {
                this.b = yl2.zza;
            } else {
                this.b = this.a.d();
            }
            yl2 yl22 = this.b;
            return yl22;
        }
    }
}
