package com.fossil;

import com.zendesk.sdk.network.Constants;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r86 {
    @DexIgnore
    public static /* final */ Pattern f; // = Pattern.compile("http(s?)://[^\\/]+", 2);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ va6 b;
    @DexIgnore
    public /* final */ ta6 c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ i86 e;

    @DexIgnore
    public r86(i86 i86, String str, String str2, va6 va6, ta6 ta6) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (va6 != null) {
            this.e = i86;
            this.d = str;
            this.a = a(str2);
            this.b = va6;
            this.c = ta6;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    @DexIgnore
    public ua6 a() {
        return a((Map<String, String>) Collections.emptyMap());
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public ua6 a(Map<String, String> map) {
        ua6 a2 = this.b.a(this.c, b(), map);
        a2.a(false);
        a2.a(10000);
        a2.c(Constants.USER_AGENT_HEADER, "Crashlytics Android SDK/" + this.e.j());
        a2.c("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return a2;
    }

    @DexIgnore
    public final String a(String str) {
        return !z86.b(this.d) ? f.matcher(str).replaceFirst(this.d) : str;
    }
}
