package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1$currentUser$1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
public final class ut5$c$a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut5$c$a(SignUpPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ut5$c$a ut5_c_a = new ut5$c$a(this.this$0, xe6);
        ut5_c_a.p$ = (il6) obj;
        return ut5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ut5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.y().getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
