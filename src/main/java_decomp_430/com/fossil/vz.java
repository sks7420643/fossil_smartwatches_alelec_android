package com.fossil;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.b00;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vz<Z> extends zz<ImageView, Z> implements b00.a {
    @DexIgnore
    public Animatable g;

    @DexIgnore
    public vz(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        super.a(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        super.b(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public void c(Drawable drawable) {
        super.c(drawable);
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.stop();
        }
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public abstract void c(Z z);

    @DexIgnore
    public void d(Drawable drawable) {
        ((ImageView) this.a).setImageDrawable(drawable);
    }

    @DexIgnore
    public final void d(Z z) {
        c(z);
        b(z);
    }

    @DexIgnore
    public void a(Z z, b00<? super Z> b00) {
        if (b00 == null || !b00.a(z, this)) {
            d(z);
        } else {
            b(z);
        }
    }

    @DexIgnore
    public final void b(Z z) {
        if (z instanceof Animatable) {
            this.g = (Animatable) z;
            this.g.start();
            return;
        }
        this.g = null;
    }

    @DexIgnore
    public void c() {
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    public void a() {
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.start();
        }
    }
}
