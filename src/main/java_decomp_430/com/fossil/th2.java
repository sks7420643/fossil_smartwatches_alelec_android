package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.ov2;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class th2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Context g;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle h;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public th2(ov2 ov2, String str, String str2, Context context, Bundle bundle) {
        super(ov2);
        this.i = ov2;
        this.e = str;
        this.f = str2;
        this.g = context;
        this.h = bundle;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0072, code lost:
        if (r4 < r3) goto L_0x0074;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0054 A[Catch:{ RemoteException -> 0x009e }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0060 A[Catch:{ RemoteException -> 0x009e }] */
    public final void a() {
        String str;
        String str2;
        String str3;
        boolean z;
        int i2;
        boolean z2;
        try {
            List unused = this.i.d = new ArrayList();
            if (ov2.c(this.e, this.f)) {
                String str4 = this.f;
                str2 = this.e;
                str = str4;
                str3 = this.i.a;
            } else {
                str3 = null;
                str2 = null;
                str = null;
            }
            ov2.i(this.g);
            if (!ov2.j.booleanValue()) {
                if (str2 == null) {
                    z = false;
                    eu2 unused2 = this.i.g = this.i.a(this.g, z);
                    if (this.i.g != null) {
                        Log.w(this.i.a, "Failed to connect to measurement client.");
                        return;
                    }
                    int d = ov2.h(this.g);
                    int e2 = ov2.g(this.g);
                    if (z) {
                        i2 = Math.max(d, e2);
                    } else {
                        i2 = d > 0 ? d : e2;
                        if (d > 0) {
                        }
                        z2 = false;
                        this.i.g.initialize(z52.a(this.g), new mv2(18202, (long) i2, z2, str3, str2, str, this.h), this.a);
                        return;
                    }
                    z2 = true;
                    this.i.g.initialize(z52.a(this.g), new mv2(18202, (long) i2, z2, str3, str2, str, this.h), this.a);
                    return;
                }
            }
            z = true;
            eu2 unused3 = this.i.g = this.i.a(this.g, z);
            if (this.i.g != null) {
            }
        } catch (RemoteException e3) {
            this.i.a((Exception) e3, true, false);
        }
    }
}
