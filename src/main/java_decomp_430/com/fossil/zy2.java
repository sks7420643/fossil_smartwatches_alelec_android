package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy2 {
    @DexIgnore
    public /* final */ ph2 a;

    @DexIgnore
    public zy2(ph2 ph2) {
        w12.a(ph2);
        this.a = ph2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof zy2)) {
            return false;
        }
        try {
            return this.a.a(((zy2) obj).a);
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.j();
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }
}
