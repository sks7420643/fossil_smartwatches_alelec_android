package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or3 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ os3 c;
    @DexIgnore
    public /* final */ Map<String, qs3> d;

    @DexIgnore
    public or3(Context context) {
        this(context, new os3());
    }

    @DexIgnore
    public static String b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    @DexIgnore
    public final synchronized String a() {
        return this.a.getString("topic_operation_queue", "");
    }

    @DexIgnore
    public final synchronized boolean c() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public or3(Context context, os3 os3) {
        this.d = new p4();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.c = os3;
        File file = new File(w6.c(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !c()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    b();
                    FirebaseInstanceId.l().f();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final synchronized void a(String str) {
        this.a.edit().putString("topic_operation_queue", str).apply();
    }

    @DexIgnore
    public final synchronized void b() {
        this.d.clear();
        os3.a(this.b);
        this.a.edit().clear().commit();
    }

    @DexIgnore
    public final synchronized void c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        SharedPreferences.Editor edit = this.a.edit();
        for (String next : this.a.getAll().keySet()) {
            if (next.startsWith(concat)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }

    @DexIgnore
    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public final synchronized nr3 a(String str, String str2, String str3) {
        return nr3.b(this.a.getString(b(str, str2, str3), (String) null));
    }

    @DexIgnore
    public final synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = nr3.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            SharedPreferences.Editor edit = this.a.edit();
            edit.putString(b(str, str2, str3), a2);
            edit.commit();
        }
    }

    @DexIgnore
    public final synchronized qs3 b(String str) {
        qs3 qs3;
        qs3 qs32 = this.d.get(str);
        if (qs32 != null) {
            return qs32;
        }
        try {
            qs3 = this.c.a(this.b, str);
        } catch (rs3 unused) {
            Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
            FirebaseInstanceId.l().f();
            qs3 = this.c.b(this.b, str);
        }
        this.d.put(str, qs3);
        return qs3;
    }
}
