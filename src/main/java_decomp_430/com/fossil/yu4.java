package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu4 extends df<GoalTrackingSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ PortfolioApp l;
    @DexIgnore
    public /* final */ bv4 m;
    @DexIgnore
    public /* final */ FragmentManager n;
    @DexIgnore
    public /* final */ BaseFragment o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4) {
            wg6.b(str, "mDayOfWeek");
            wg6.b(str2, "mDayOfMonth");
            wg6.b(str3, "mDailyValue");
            wg6.b(str4, "mDailyUnit");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final Date c() {
            return this.a;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.c;
        }

        @DexIgnore
        public final boolean g() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, int i, qg6 qg6) {
            this(date, r14, (i & 4) == 0 ? z2 : r0, (i & 8) != 0 ? "" : str, (i & 16) != 0 ? "" : str2, (i & 32) != 0 ? "" : str3, (i & 64) != 0 ? "" : str4);
            date = (i & 1) != 0 ? null : date;
            boolean z3 = false;
            boolean z4 = (i & 2) != 0 ? false : z;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.f;
        }

        @DexIgnore
        public final void c(String str) {
            wg6.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void d(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.g;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.g = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ jg4 b;
        @DexIgnore
        public /* final */ /* synthetic */ yu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.c.m.b(a2);
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(yu4 yu4, jg4 jg4, View view) {
            super(view);
            wg6.b(jg4, "binding");
            wg6.b(view, "root");
            this.c = yu4;
            this.b = jg4;
            this.b.d().setOnClickListener(new a(this));
            this.b.r.setTextColor(yu4.g);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void a(GoalTrackingSummary goalTrackingSummary) {
            b a2 = this.c.a(goalTrackingSummary);
            this.a = a2.c();
            Object r0 = this.b.u;
            wg6.a((Object) r0, "binding.ftvDayOfWeek");
            r0.setText(a2.e());
            Object r02 = this.b.t;
            wg6.a((Object) r02, "binding.ftvDayOfMonth");
            r02.setText(a2.d());
            Object r03 = this.b.s;
            wg6.a((Object) r03, "binding.ftvDailyValue");
            r03.setText(a2.b());
            Object r04 = this.b.r;
            wg6.a((Object) r04, "binding.ftvDailyUnit");
            r04.setText(a2.a());
            ConstraintLayout constraintLayout = this.b.q;
            wg6.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.f());
            Object r05 = this.b.u;
            wg6.a((Object) r05, "binding.ftvDayOfWeek");
            r05.setSelected(a2.g());
            Object r06 = this.b.t;
            wg6.a((Object) r06, "binding.ftvDayOfMonth");
            r06.setSelected(a2.g());
            if (a2.g()) {
                this.b.q.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.h);
                this.b.t.setBackgroundColor(this.c.h);
                this.b.u.setTextColor(this.c.k);
                this.b.t.setTextColor(this.c.k);
            } else if (a2.f()) {
                this.b.q.setBackgroundColor(this.c.f);
                this.b.u.setBackgroundColor(this.c.f);
                this.b.t.setBackgroundColor(this.c.f);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            } else {
                this.b.q.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.j);
                this.b.t.setBackgroundColor(this.c.j);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wg6.b(str, "mWeekly");
            wg6.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ lg4 f;
        @DexIgnore
        public /* final */ /* synthetic */ yu4 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.d != null && this.a.e != null) {
                    bv4 e = this.a.g.m;
                    Date b = this.a.d;
                    if (b != null) {
                        Date a2 = this.a.e;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(yu4 yu4, lg4 lg4) {
            super(yu4, r0, r1);
            wg6.b(lg4, "binding");
            this.g = yu4;
            jg4 jg4 = lg4.r;
            if (jg4 != null) {
                wg6.a((Object) jg4, "binding.dailyItem!!");
                View d2 = lg4.d();
                wg6.a((Object) d2, "binding.root");
                this.f = lg4;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(GoalTrackingSummary goalTrackingSummary) {
            d b = this.g.b(goalTrackingSummary);
            this.e = b.a();
            this.d = b.b();
            Object r1 = this.f.s;
            wg6.a((Object) r1, "binding.ftvWeekly");
            r1.setText(b.c());
            Object r12 = this.f.t;
            wg6.a((Object) r12, "binding.ftvWeeklyValue");
            r12.setText(b.d());
            super.a(goalTrackingSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ yu4 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(yu4 yu4, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = yu4;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wg6.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.o.getId() + ", isAdded=" + this.a.o.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.n.b(this.a.o.h1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                hc b3 = this.a.n.b();
                b3.a(view.getId(), this.a.o, this.a.o.h1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                hc b4 = this.a.n.b();
                b4.d(b2);
                b4.d();
                hc b5 = this.a.n.b();
                b5.a(view.getId(), this.a.o, this.a.o.h1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.o.getId() + ", isAdded2=" + this.a.o.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wg6.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yu4(av4 av4, PortfolioApp portfolioApp, bv4 bv4, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(av4);
        wg6.b(av4, "difference");
        wg6.b(portfolioApp, "mApp");
        wg6.b(bv4, "mOnItemClick");
        wg6.b(fragmentManager, "mFragmentManager");
        wg6.b(baseFragment, "mFragment");
        this.l = portfolioApp;
        this.m = bv4;
        this.n = fragmentManager;
        this.o = baseFragment;
        String b2 = ThemeManager.l.a().b("primaryText");
        this.e = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("nonBrandSurface");
        this.f = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.g = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("hybridGoalTrackingTab");
        this.h = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("secondaryText");
        this.i = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.j = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
        String b8 = ThemeManager.l.a().b("onHybridGoalTrackingTab");
        this.k = Color.parseColor(b8 == null ? "#FFFFFF" : b8);
    }

    @DexIgnore
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return yu4.super.getItemId(i2);
        }
        if (this.o.getId() == 0) {
            return 1010101;
        }
        return (long) this.o.getId();
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) getItem(i2);
        if (goalTrackingSummary == null) {
            return 1;
        }
        Calendar calendar = this.d;
        wg6.a((Object) calendar, "mCalendar");
        calendar.setTime(goalTrackingSummary.getDate());
        Calendar calendar2 = this.d;
        wg6.a((Object) calendar2, "mCalendar");
        Boolean t = bk4.t(calendar2.getTime());
        wg6.a((Object) t, "DateHelper.isToday(mCalendar.time)");
        if (t.booleanValue() || this.d.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wg6.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wg6.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardGoalTrackingAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wg6.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((GoalTrackingSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((GoalTrackingSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((GoalTrackingSummary) getItem(i2));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            jg4 a2 = jg4.a(from, viewGroup, false);
            wg6.a((Object) a2, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wg6.a((Object) d2, "itemGoalTrackingDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            jg4 a3 = jg4.a(from, viewGroup, false);
            wg6.a((Object) a3, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wg6.a((Object) d3, "itemGoalTrackingDayBinding.root");
            return new c(this, a3, d3);
        } else {
            lg4 a4 = lg4.a(from, viewGroup, false);
            wg6.a((Object) a4, "ItemGoalTrackingWeekBind\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(cf<GoalTrackingSummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardGoalTrackingAdapter", sb.toString());
        yu4.super.b(cfVar);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final b a(GoalTrackingSummary goalTrackingSummary) {
        b bVar = new b((Date) null, false, false, (String) null, (String) null, (String) null, (String) null, 127, (qg6) null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            int i2 = instance.get(7);
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            Boolean t = bk4.t(instance.getTime());
            wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
            if (t.booleanValue()) {
                String a2 = jm4.a((Context) this.l, 2131886525);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026rackingToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(yk4.b.b(i2));
            }
            bVar.b(String.valueOf(goalTrackingSummary.getTotalTracked()));
            bVar.a("/ " + goalTrackingSummary.getGoalTarget() + " " + jm4.a((Context) this.l, 2131886524));
            boolean z = false;
            if (goalTrackingSummary.getGoalTarget() > 0) {
                if (goalTrackingSummary.getTotalTracked() >= goalTrackingSummary.getGoalTarget()) {
                    z = true;
                }
                bVar.b(z);
            } else {
                bVar.b(false);
            }
            if (goalTrackingSummary.getTotalTracked() <= 0) {
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final d b(GoalTrackingSummary goalTrackingSummary) {
        String str;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (qg6) null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            Boolean t = bk4.t(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String b2 = bk4.b(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String b3 = bk4.b(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            wg6.a((Object) t, "isToday");
            if (t.booleanValue()) {
                str = jm4.a((Context) this.l, 2131886527);
                wg6.a((Object) str, "LanguageHelper.getString\u2026ingToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = b3 + ' ' + i5 + " - " + b3 + ' ' + i2;
            } else if (i7 == i4) {
                str = b3 + ' ' + i5 + " - " + b2 + ' ' + i2;
            } else {
                str = b3 + ' ' + i5 + ", " + i7 + " - " + b2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            if (goalTrackingSummary.getTotalTargetOfWeek() > 0) {
                int a2 = rh6.a((((float) goalTrackingSummary.getTotalValueOfWeek()) / ((float) goalTrackingSummary.getTotalTargetOfWeek())) * ((float) 100));
                StringBuilder sb = new StringBuilder();
                sb.append(a2);
                sb.append('%');
                dVar.b(sb.toString());
            }
        }
        return dVar;
    }
}
