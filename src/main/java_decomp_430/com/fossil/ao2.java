package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ao2 {
    @DexIgnore
    public static /* final */ ao2 a; // = new do2();
    @DexIgnore
    public static /* final */ ao2 b; // = new bo2();

    @DexIgnore
    public ao2() {
    }

    @DexIgnore
    public static ao2 a() {
        return a;
    }

    @DexIgnore
    public static ao2 b() {
        return b;
    }

    @DexIgnore
    public abstract void a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);
}
