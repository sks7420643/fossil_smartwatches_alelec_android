package com.fossil;

import com.fossil.ql2;
import com.fossil.rl2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rl2<MessageType extends ql2<MessageType, BuilderType>, BuilderType extends rl2<MessageType, BuilderType>> implements qo2 {
    @DexIgnore
    public final /* synthetic */ qo2 a(ro2 ro2) {
        if (a().getClass().isInstance(ro2)) {
            a((ql2) ro2);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public abstract BuilderType a(byte[] bArr, int i, int i2) throws qn2;

    @DexIgnore
    public abstract BuilderType a(byte[] bArr, int i, int i2, sm2 sm2) throws qn2;

    @DexIgnore
    public final /* synthetic */ qo2 a(byte[] bArr, sm2 sm2) throws qn2 {
        a(bArr, 0, bArr.length, sm2);
        return this;
    }

    @DexIgnore
    public final /* synthetic */ qo2 a(byte[] bArr) throws qn2 {
        a(bArr, 0, bArr.length);
        return this;
    }
}
