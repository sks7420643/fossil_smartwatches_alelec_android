package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum di4 {
    START("start"),
    STOP("stop"),
    PAUSE("pause"),
    RESUME("resume"),
    IDLE("idle");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final di4 a(String str) {
            di4 di4;
            wg6.b(str, ServerSetting.VALUE);
            di4[] values = di4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    di4 = null;
                    break;
                }
                di4 = values[i];
                String mValue = di4.getMValue();
                String lowerCase = str.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (wg6.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return di4 != null ? di4 : di4.IDLE;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final di4 a(int i) {
            if (2130970060 == WorkoutState.START.ordinal()) {
                return di4.START;
            }
            if (2130970060 == WorkoutState.END.ordinal()) {
                return di4.STOP;
            }
            if (2130970060 == WorkoutState.PAUSE.ordinal()) {
                return di4.PAUSE;
            }
            if (2130970060 == WorkoutState.RESUME.ordinal()) {
                return di4.RESUME;
            }
            return di4.IDLE;
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public di4(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        wg6.b(str, "<set-?>");
        this.mValue = str;
    }
}
