package com.fossil;

import com.facebook.stetho.inspector.protocol.module.Database;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py3 implements uy3 {
    @DexIgnore
    public int a() {
        return 5;
    }

    @DexIgnore
    public void a(vy3 vy3) {
        StringBuilder sb = new StringBuilder();
        sb.append(0);
        while (true) {
            if (!vy3.i()) {
                break;
            }
            sb.append(vy3.c());
            vy3.f++;
            int a = xy3.a(vy3.d(), vy3.f, a());
            if (a != a()) {
                vy3.b(a);
                break;
            }
        }
        int length = sb.length() - 1;
        int a2 = vy3.a() + length + 1;
        vy3.c(a2);
        boolean z = vy3.g().a() - a2 > 0;
        if (vy3.i() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / Database.MAX_EXECUTE_RESULTS) + 249));
                sb.insert(1, (char) (length % Database.MAX_EXECUTE_RESULTS));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: " + length);
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            vy3.a(a(sb.charAt(i), vy3.a() + 1));
        }
    }

    @DexIgnore
    public static char a(char c, int i) {
        int i2 = c + ((i * 149) % 255) + 1;
        return i2 <= 255 ? (char) i2 : (char) (i2 - 256);
    }
}
