package com.fossil;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.fossil.n16;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e16 extends x06 {
    @DexIgnore
    public static /* final */ String[] b; // = {"orientation"};

    @DexIgnore
    public enum a {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);
        
        @DexIgnore
        public /* final */ int androidKind;
        @DexIgnore
        public /* final */ int height;
        @DexIgnore
        public /* final */ int width;

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.androidKind = i;
            this.width = i2;
            this.height = i3;
        }
    }

    @DexIgnore
    public e16(Context context) {
        super(context);
    }

    @DexIgnore
    public boolean a(l16 l16) {
        Uri uri = l16.d;
        return "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        Bitmap bitmap;
        l16 l162 = l16;
        ContentResolver contentResolver = this.a.getContentResolver();
        int a2 = a(contentResolver, l162.d);
        String type = contentResolver.getType(l162.d);
        boolean z = type != null && type.startsWith("video/");
        if (l16.c()) {
            a a3 = a(l162.h, l162.i);
            if (!z && a3 == a.FULL) {
                return new n16.a((Bitmap) null, c(l16), Picasso.LoadedFrom.DISK, a2);
            }
            long parseId = ContentUris.parseId(l162.d);
            BitmapFactory.Options b2 = n16.b(l16);
            b2.inJustDecodeBounds = true;
            BitmapFactory.Options options = b2;
            n16.a(l162.h, l162.i, a3.width, a3.height, b2, l16);
            if (z) {
                bitmap = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, parseId, a3 == a.FULL ? 1 : a3.androidKind, options);
            } else {
                bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, parseId, a3.androidKind, options);
            }
            if (bitmap != null) {
                return new n16.a(bitmap, (InputStream) null, Picasso.LoadedFrom.DISK, a2);
            }
        }
        return new n16.a((Bitmap) null, c(l16), Picasso.LoadedFrom.DISK, a2);
    }

    @DexIgnore
    public static a a(int i, int i2) {
        a aVar = a.MICRO;
        if (i <= aVar.width && i2 <= aVar.height) {
            return aVar;
        }
        a aVar2 = a.MINI;
        if (i > aVar2.width || i2 > aVar2.height) {
            return a.FULL;
        }
        return aVar2;
    }

    @DexIgnore
    public static int a(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = null;
        try {
            Cursor query = contentResolver.query(uri, b, (String) null, (String[]) null, (String) null);
            if (query != null) {
                if (query.moveToFirst()) {
                    int i = query.getInt(0);
                    if (query != null) {
                        query.close();
                    }
                    return i;
                }
            }
            if (query != null) {
                query.close();
            }
            return 0;
        } catch (RuntimeException unused) {
            if (cursor != null) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
