package com.fossil;

import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
public final class oy4$h$d extends sf6 implements ig6<il6, xe6<? super List<? extends NotificationSettingsModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.h this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oy4$h$d(NotificationCallsAndMessagesPresenter.h hVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        oy4$h$d oy4_h_d = new oy4$h$d(this.this$0, xe6);
        oy4_h_d.p$ = (il6) obj;
        return oy4_h_d;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((oy4$h$d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.z.getListNotificationSettingsNoLiveData();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
