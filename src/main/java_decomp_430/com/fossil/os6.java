package com.fossil;

import com.fossil.is6;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os6 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ ms6 d;
    @DexIgnore
    public /* final */ Deque<sq6> e; // = new ArrayDeque();
    @DexIgnore
    public is6.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ a i;
    @DexIgnore
    public /* final */ c j; // = new c();
    @DexIgnore
    public /* final */ c k; // = new c();
    @DexIgnore
    public hs6 l; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements zt6 {
        @DexIgnore
        public /* final */ jt6 a; // = new jt6();
        @DexIgnore
        public /* final */ jt6 b; // = new jt6();
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b(long j) {
            this.c = j;
        }

        @DexIgnore
        public final void a(long j) {
            os6.this.d.a(j);
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            hs6 hs6;
            long j2;
            is6.a aVar;
            sq6 sq6;
            long j3 = j;
            if (j3 >= 0) {
                while (true) {
                    synchronized (os6.this) {
                        os6.this.j.g();
                        try {
                            hs6 = os6.this.l != null ? os6.this.l : null;
                            if (!this.d) {
                                if (os6.this.e.isEmpty() || os6.this.f == null) {
                                    if (this.b.p() > 0) {
                                        j2 = this.b.b(jt6, Math.min(j3, this.b.p()));
                                        os6.this.a += j2;
                                        if (hs6 == null && os6.this.a >= ((long) (os6.this.d.r.c() / 2))) {
                                            os6.this.d.b(os6.this.c, os6.this.a);
                                            os6.this.a = 0;
                                        }
                                    } else {
                                        jt6 jt62 = jt6;
                                        if (this.e || hs6 != null) {
                                            j2 = -1;
                                        } else {
                                            os6.this.k();
                                        }
                                    }
                                    sq6 = null;
                                    aVar = null;
                                } else {
                                    sq6 = (sq6) os6.this.e.removeFirst();
                                    aVar = os6.this.f;
                                    jt6 jt63 = jt6;
                                    j2 = -1;
                                }
                                os6.this.j.k();
                                if (sq6 != null && aVar != null) {
                                    aVar.a(sq6);
                                }
                            } else {
                                throw new IOException("stream closed");
                            }
                        } finally {
                            os6.this.j.k();
                        }
                    }
                }
                if (j2 != -1) {
                    a(j2);
                    return j2;
                } else if (hs6 == null) {
                    return -1;
                } else {
                    throw new ts6(hs6);
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j3);
            }
        }

        @DexIgnore
        public void close() throws IOException {
            long p;
            ArrayList<sq6> arrayList;
            is6.a aVar;
            synchronized (os6.this) {
                this.d = true;
                p = this.b.p();
                this.b.k();
                arrayList = null;
                if (os6.this.e.isEmpty() || os6.this.f == null) {
                    aVar = null;
                } else {
                    arrayList = new ArrayList<>(os6.this.e);
                    os6.this.e.clear();
                    aVar = os6.this.f;
                }
                os6.this.notifyAll();
            }
            if (p > 0) {
                a(p);
            }
            os6.this.a();
            if (aVar != null) {
                for (sq6 a2 : arrayList) {
                    aVar.a(a2);
                }
            }
        }

        @DexIgnore
        public void a(lt6 lt6, long j) throws IOException {
            boolean z;
            boolean z2;
            boolean z3;
            while (j > 0) {
                synchronized (os6.this) {
                    z = this.e;
                    z2 = true;
                    z3 = this.b.p() + j > this.c;
                }
                if (z3) {
                    lt6.skip(j);
                    os6.this.c(hs6.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    lt6.skip(j);
                    return;
                } else {
                    long b2 = lt6.b(this.a, j);
                    if (b2 != -1) {
                        j -= b2;
                        synchronized (os6.this) {
                            if (this.b.p() != 0) {
                                z2 = false;
                            }
                            this.b.a((zt6) this.a);
                            if (z2) {
                                os6.this.notifyAll();
                            }
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        public au6 b() {
            return os6.this.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ht6 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        public void i() {
            os6.this.c(hs6.CANCEL);
        }

        @DexIgnore
        public void k() throws IOException {
            if (h()) {
                throw b((IOException) null);
            }
        }
    }

    @DexIgnore
    public os6(int i2, ms6 ms6, boolean z, boolean z2, sq6 sq6) {
        if (ms6 != null) {
            this.c = i2;
            this.d = ms6;
            this.b = (long) ms6.s.c();
            this.h = new b((long) ms6.r.c());
            this.i = new a();
            this.h.e = z2;
            this.i.c = z;
            if (sq6 != null) {
                this.e.add(sq6);
            }
            if (f() && sq6 != null) {
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet");
            } else if (!f() && sq6 == null) {
                throw new IllegalStateException("remotely-initiated streams should have headers");
            }
        } else {
            throw new NullPointerException("connection == null");
        }
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public yt6 d() {
        synchronized (this) {
            if (!this.g) {
                if (!f()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.i;
    }

    @DexIgnore
    public zt6 e() {
        return this.h;
    }

    @DexIgnore
    public boolean f() {
        if (this.d.a == ((this.c & 1) == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public synchronized boolean g() {
        if (this.l != null) {
            return false;
        }
        if ((this.h.e || this.h.d) && ((this.i.c || this.i.b) && this.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public au6 h() {
        return this.j;
    }

    @DexIgnore
    public void i() {
        boolean g2;
        synchronized (this) {
            this.h.e = true;
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.d(this.c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        r2.j.k();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public synchronized sq6 j() throws IOException {
        this.j.g();
        while (this.e.isEmpty() && this.l == null) {
            k();
        }
        this.j.k();
        if (!this.e.isEmpty()) {
        } else {
            throw new ts6(this.l);
        }
        return this.e.removeFirst();
    }

    @DexIgnore
    public void k() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public au6 l() {
        return this.k;
    }

    @DexIgnore
    public void a(hs6 hs6) throws IOException {
        if (b(hs6)) {
            this.d.b(this.c, hs6);
        }
    }

    @DexIgnore
    public final boolean b(hs6 hs6) {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            if (this.h.e && this.i.c) {
                return false;
            }
            this.l = hs6;
            notifyAll();
            this.d.d(this.c);
            return true;
        }
    }

    @DexIgnore
    public void c(hs6 hs6) {
        if (b(hs6)) {
            this.d.c(this.c, hs6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements yt6 {
        @DexIgnore
        public /* final */ jt6 a; // = new jt6();
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            this.a.a(jt6, j);
            while (this.a.p() >= 16384) {
                a(false);
            }
        }

        @DexIgnore
        public au6 b() {
            return os6.this.k;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
            if (r8.a.p() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
            if (r8.a.p() <= 0) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
            r0 = r8.d;
            r0.d.a(r0.c, true, (com.fossil.jt6) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
            r2 = r8.d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.b = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
            r8.d.d.flush();
            r8.d.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r8.d.i.c != false) goto L_0x003a;
         */
        @DexIgnore
        public void close() throws IOException {
            synchronized (os6.this) {
                if (this.b) {
                }
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            synchronized (os6.this) {
                os6.this.b();
            }
            while (this.a.p() > 0) {
                a(false);
                os6.this.d.flush();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (os6.this) {
                os6.this.k.g();
                while (os6.this.b <= 0 && !this.c && !this.b && os6.this.l == null) {
                    try {
                        os6.this.k();
                    } catch (Throwable th) {
                        os6.this.k.k();
                        throw th;
                    }
                }
                os6.this.k.k();
                os6.this.b();
                min = Math.min(os6.this.b, this.a.p());
                os6.this.b -= min;
            }
            os6.this.k.g();
            try {
                os6.this.d.a(os6.this.c, z && min == this.a.p(), this.a, min);
            } finally {
                os6.this.k.k();
            }
        }
    }

    @DexIgnore
    public void a(List<is6> list) {
        boolean g2;
        synchronized (this) {
            this.g = true;
            this.e.add(fr6.b(list));
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.d(this.c);
        }
    }

    @DexIgnore
    public synchronized void d(hs6 hs6) {
        if (this.l == null) {
            this.l = hs6;
            notifyAll();
        }
    }

    @DexIgnore
    public void a(lt6 lt6, int i2) throws IOException {
        this.h.a(lt6, (long) i2);
    }

    @DexIgnore
    public void b() throws IOException {
        a aVar = this.i;
        if (aVar.b) {
            throw new IOException("stream closed");
        } else if (!aVar.c) {
            hs6 hs6 = this.l;
            if (hs6 != null) {
                throw new ts6(hs6);
            }
        } else {
            throw new IOException("stream finished");
        }
    }

    @DexIgnore
    public void a() throws IOException {
        boolean z;
        boolean g2;
        synchronized (this) {
            z = !this.h.e && this.h.d && (this.i.c || this.i.b);
            g2 = g();
        }
        if (z) {
            a(hs6.CANCEL);
        } else if (!g2) {
            this.d.d(this.c);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }
}
