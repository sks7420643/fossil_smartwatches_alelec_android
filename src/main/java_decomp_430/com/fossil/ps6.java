package com.fossil;

import com.fossil.js6;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps6 implements Closeable {
    @DexIgnore
    public static /* final */ Logger g; // = Logger.getLogger(ks6.class.getName());
    @DexIgnore
    public /* final */ kt6 a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ jt6 c; // = new jt6();
    @DexIgnore
    public int d; // = 16384;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ js6.b f; // = new js6.b(this.c);

    @DexIgnore
    public ps6(kt6 kt6, boolean z) {
        this.a = kt6;
        this.b = z;
    }

    @DexIgnore
    public synchronized void a(ss6 ss6) throws IOException {
        if (!this.e) {
            this.d = ss6.c(this.d);
            if (ss6.b() != -1) {
                this.f.b(ss6.b());
            }
            a(0, 0, (byte) 4, (byte) 1);
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void b(ss6 ss6) throws IOException {
        if (!this.e) {
            int i = 0;
            a(0, ss6.d() * 6, (byte) 4, (byte) 0);
            while (i < 10) {
                if (ss6.d(i)) {
                    this.a.writeShort(i == 4 ? 3 : i == 7 ? 4 : i);
                    this.a.writeInt(ss6.a(i));
                }
                i++;
            }
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public final void c(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.d, j);
            long j2 = (long) min;
            j -= j2;
            a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.a.a(this.c, j2);
        }
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        this.e = true;
        this.a.close();
    }

    @DexIgnore
    public synchronized void flush() throws IOException {
        if (!this.e) {
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void k() throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (this.b) {
            if (g.isLoggable(Level.FINE)) {
                g.fine(fr6.a(">> CONNECTION %s", ks6.a.hex()));
            }
            this.a.write(ks6.a.toByteArray());
            this.a.flush();
        }
    }

    @DexIgnore
    public int l() {
        return this.d;
    }

    @DexIgnore
    public synchronized void a(int i, int i2, List<is6> list) throws IOException {
        if (!this.e) {
            this.f.a(list);
            long p = this.c.p();
            int min = (int) Math.min((long) (this.d - 4), p);
            long j = (long) min;
            int i3 = (p > j ? 1 : (p == j ? 0 : -1));
            a(i, min + 4, (byte) 5, i3 == 0 ? (byte) 4 : 0);
            this.a.writeInt(i2 & Integer.MAX_VALUE);
            this.a.a(this.c, j);
            if (i3 > 0) {
                c(i, p - j);
            }
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void b(int i, long j) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            ks6.a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            throw null;
        } else {
            a(i, 4, (byte) 8, (byte) 0);
            this.a.writeInt((int) j);
            this.a.flush();
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, int i2, List<is6> list) throws IOException {
        if (!this.e) {
            a(z, i, list);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i, hs6 hs6) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (hs6.httpCode != -1) {
            a(i, 4, (byte) 3, (byte) 0);
            this.a.writeInt(hs6.httpCode);
            this.a.flush();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, jt6 jt6, int i2) throws IOException {
        if (!this.e) {
            byte b2 = 0;
            if (z) {
                b2 = (byte) 1;
            }
            a(i, b2, jt6, i2);
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public void a(int i, byte b2, jt6 jt6, int i2) throws IOException {
        a(i, i2, (byte) 0, b2);
        if (i2 > 0) {
            this.a.a(jt6, (long) i2);
        }
    }

    @DexIgnore
    public synchronized void a(boolean z, int i, int i2) throws IOException {
        if (!this.e) {
            a(0, 8, (byte) 6, z ? (byte) 1 : 0);
            this.a.writeInt(i);
            this.a.writeInt(i2);
            this.a.flush();
        } else {
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public synchronized void a(int i, hs6 hs6, byte[] bArr) throws IOException {
        if (this.e) {
            throw new IOException("closed");
        } else if (hs6.httpCode != -1) {
            a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.a.writeInt(i);
            this.a.writeInt(hs6.httpCode);
            if (bArr.length > 0) {
                this.a.write(bArr);
            }
            this.a.flush();
        } else {
            ks6.a("errorCode.httpCode == -1", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public void a(int i, int i2, byte b2, byte b3) throws IOException {
        if (g.isLoggable(Level.FINE)) {
            g.fine(ks6.a(false, i, i2, b2, b3));
        }
        int i3 = this.d;
        if (i2 > i3) {
            ks6.a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(i3), Integer.valueOf(i2));
            throw null;
        } else if ((Integer.MIN_VALUE & i) == 0) {
            a(this.a, i2);
            this.a.writeByte(b2 & 255);
            this.a.writeByte(b3 & 255);
            this.a.writeInt(i & Integer.MAX_VALUE);
        } else {
            ks6.a("reserved bit set: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public static void a(kt6 kt6, int i) throws IOException {
        kt6.writeByte((i >>> 16) & 255);
        kt6.writeByte((i >>> 8) & 255);
        kt6.writeByte(i & 255);
    }

    @DexIgnore
    public void a(boolean z, int i, List<is6> list) throws IOException {
        if (!this.e) {
            this.f.a(list);
            long p = this.c.p();
            int min = (int) Math.min((long) this.d, p);
            long j = (long) min;
            int i2 = (p > j ? 1 : (p == j ? 0 : -1));
            byte b2 = i2 == 0 ? (byte) 4 : 0;
            if (z) {
                b2 = (byte) (b2 | 1);
            }
            a(i, min, (byte) 1, b2);
            this.a.a(this.c, j);
            if (i2 > 0) {
                c(i, p - j);
                return;
            }
            return;
        }
        throw new IOException("closed");
    }
}
