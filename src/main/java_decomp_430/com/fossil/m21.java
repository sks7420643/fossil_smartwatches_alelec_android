package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m21 {
    @DexIgnore
    public /* synthetic */ m21(qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(l50[] l50Arr, short s, w40 w40) {
        Object[] array = nd6.d(l50Arr).toArray(new l50[0]);
        if (array != null) {
            l50[] l50Arr2 = (l50[]) array;
            if (l50Arr2.length == 0) {
                return new byte[0];
            }
            return co0.d.a(s, w40, l50Arr2);
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
