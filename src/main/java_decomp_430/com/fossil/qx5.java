package com.fossil;

import android.annotation.TargetApi;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import com.portfolio.platform.PortfolioApp;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx5 {
    @DexIgnore
    public static /* final */ KeyStore a;
    @DexIgnore
    public static /* final */ qx5 b;

    /*
    static {
        qx5 qx5 = new qx5();
        b = qx5;
        a = qx5.a();
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final KeyPair a(String str) {
        wg6.b(str, "alias");
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        Calendar instance2 = Calendar.getInstance();
        Calendar instance3 = Calendar.getInstance();
        instance3.add(1, 20);
        KeyPairGeneratorSpec.Builder serialNumber = new KeyPairGeneratorSpec.Builder(PortfolioApp.get.instance()).setAlias(str).setSerialNumber(BigInteger.ONE);
        KeyPairGeneratorSpec.Builder subject = serialNumber.setSubject(new X500Principal("CN=" + str + " CA Certificate"));
        wg6.a((Object) instance2, "startDate");
        KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
        wg6.a((Object) instance3, "endDate");
        KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
        wg6.a((Object) endDate, "KeyPairGeneratorSpec.Bui\u2026.setEndDate(endDate.time)");
        instance.initialize(endDate.build());
        KeyPair generateKeyPair = instance.generateKeyPair();
        wg6.a((Object) generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey b(String str) {
        KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
        KeyGenParameterSpec build = new KeyGenParameterSpec.Builder(str, 3).setBlockModes(new String[]{"GCM"}).setEncryptionPaddings(new String[]{"NoPadding"}).build();
        wg6.a((Object) build, "KeyGenParameterSpec.Buil\u2026\n                .build()");
        instance.init(build);
        SecretKey generateKey = instance.generateKey();
        wg6.a((Object) generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final KeyPair c(String str) {
        wg6.b(str, "alias");
        PrivateKey privateKey = (PrivateKey) a.getKey(str, (char[]) null);
        Certificate certificate = a.getCertificate(str);
        PublicKey publicKey = certificate != null ? certificate.getPublicKey() : null;
        if (privateKey == null || publicKey == null) {
            return null;
        }
        return new KeyPair(publicKey, privateKey);
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey d(String str) {
        wg6.b(str, "aliasName");
        if (!a.containsAlias(str)) {
            return b(str);
        }
        KeyStore.Entry entry = a.getEntry(str, (KeyStore.ProtectionParameter) null);
        if (entry != null) {
            SecretKey secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            wg6.a((Object) secretKey, "secretKeyEntry.secretKey");
            return secretKey;
        }
        throw new rc6("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    @TargetApi(23)
    public final KeyStore.SecretKeyEntry e(String str) {
        wg6.b(str, "aliasName");
        KeyStore.Entry entry = a.getEntry(str, (KeyStore.ProtectionParameter) null);
        if (entry != null) {
            return (KeyStore.SecretKeyEntry) entry;
        }
        throw new rc6("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    public final void f(String str) {
        wg6.b(str, "alias");
        a.deleteEntry(str);
    }

    @DexIgnore
    public final KeyStore a() {
        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
        instance.load((KeyStore.LoadStoreParameter) null);
        wg6.a((Object) instance, "mKeyStore");
        return instance;
    }
}
