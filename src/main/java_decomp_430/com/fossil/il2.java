package com.fossil;

import java.io.Serializable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il2<T> implements dl2<T>, Serializable {
    @DexIgnore
    public /* final */ T zza;

    @DexIgnore
    public il2(T t) {
        this.zza = t;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof il2)) {
            return false;
        }
        T t = this.zza;
        T t2 = ((il2) obj).zza;
        if (t == t2) {
            return true;
        }
        if (t == null || !t.equals(t2)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zza});
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Suppliers.ofInstance(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public final T zza() {
        return this.zza;
    }
}
