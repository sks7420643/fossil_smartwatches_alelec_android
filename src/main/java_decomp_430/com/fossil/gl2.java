package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl2<T> implements dl2<T>, Serializable {
    @DexIgnore
    public volatile transient boolean a;
    @DexIgnore
    public transient T b;
    @DexIgnore
    public /* final */ dl2<T> zza;

    @DexIgnore
    public gl2(dl2<T> dl2) {
        bl2.a(dl2);
        this.zza = dl2;
    }

    @DexIgnore
    public final String toString() {
        Object obj;
        if (this.a) {
            String valueOf = String.valueOf(this.b);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        } else {
            obj = this.zza;
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    public final T zza() {
        if (!this.a) {
            synchronized (this) {
                if (!this.a) {
                    T zza2 = this.zza.zza();
                    this.b = zza2;
                    this.a = true;
                    return zza2;
                }
            }
        }
        return this.b;
    }
}
