package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.uirenew.login.LoginPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1", f = "LoginPresenter.kt", l = {450, 451, 452, 453, 454}, m = "invokeSuspend")
public final class np5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public np5$d$a(LoginPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        np5$d$a np5_d_a = new np5$d$a(this.this$0, xe6);
        np5_d_a.p$ = (il6) obj;
        return np5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((np5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0087 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ae A[RETURN] */
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        DeviceRepository r;
        GoalTrackingRepository u;
        SleepSummariesRepository w;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            WatchLocalizationRepository z = this.this$0.this$0.z();
            this.L$0 = il62;
            this.label = 1;
            if (z.getWatchLocalizationFromServer(false, this) == a) {
                return a;
            }
            il6 = il62;
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
            w = this.this$0.this$0.w();
            this.L$0 = il6;
            this.label = 3;
            if (w.fetchLastSleepGoal(this) == a) {
                return a;
            }
            u = this.this$0.this$0.u();
            this.L$0 = il6;
            this.label = 4;
            if (u.fetchGoalSetting(this) == a) {
            }
            r = this.this$0.this$0.r();
            this.L$0 = il6;
            this.label = 5;
            if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
            }
            return cd6.a;
        } else if (i == 3) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
            u = this.this$0.this$0.u();
            this.L$0 = il6;
            this.label = 4;
            if (u.fetchGoalSetting(this) == a) {
                return a;
            }
            r = this.this$0.this$0.r();
            this.L$0 = il6;
            this.label = 5;
            if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
            }
            return cd6.a;
        } else if (i == 4) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
            r = this.this$0.this$0.r();
            this.L$0 = il6;
            this.label = 5;
            if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
                return a;
            }
            return cd6.a;
        } else if (i == 5) {
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SummariesRepository x = this.this$0.this$0.x();
        this.L$0 = il6;
        this.label = 2;
        if (x.fetchActivitySettings(this) == a) {
            return a;
        }
        w = this.this$0.this$0.w();
        this.L$0 = il6;
        this.label = 3;
        if (w.fetchLastSleepGoal(this) == a) {
        }
        u = this.this$0.this$0.u();
        this.L$0 = il6;
        this.label = 4;
        if (u.fetchGoalSetting(this) == a) {
        }
        r = this.this$0.this$0.r();
        this.L$0 = il6;
        this.label = 5;
        if (DeviceRepository.downloadSupportedSku$default(r, 0, this, 1, (Object) null) == a) {
        }
        return cd6.a;
    }
}
