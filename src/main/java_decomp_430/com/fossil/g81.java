package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g81 extends l61 {
    @DexIgnore
    public /* final */ x70 A;

    @DexIgnore
    public g81(x70 x70, ue1 ue1) {
        super(lx0.NOTIFY_APP_NOTIFICATION_EVENT, ue1);
        this.A = x70;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.APP_NOTIFICATION_EVENT, (Object) this.A.a());
    }

    @DexIgnore
    public ok0 l() {
        rg1 rg1 = rg1.ASYNC;
        x70 x70 = this.A;
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(8).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(kl0.NOTIFY.a);
        order.put(xh0.APP_NOTIFICATION_EVENT.a);
        order.putInt(x70.getNotificationUid());
        order.put(x70.getAction().a());
        order.put(x70.getActionStatus().a());
        byte[] array = order.array();
        wg6.a(array, "appNotificationEventData.array()");
        return new fb1(rg1, array, this.y.v);
    }
}
