package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface p12 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends db2 implements p12 {
        @DexIgnore
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @DexIgnore
        public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) eb2.a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                b(parcel.readInt(), (Bundle) eb2.a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                a(parcel.readInt(), parcel.readStrongBinder(), (j32) eb2.a(parcel, j32.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    @DexIgnore
    void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(int i, IBinder iBinder, j32 j32) throws RemoteException;

    @DexIgnore
    void b(int i, Bundle bundle) throws RemoteException;
}
