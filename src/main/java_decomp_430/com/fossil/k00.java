package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k00 extends InputStream {
    @DexIgnore
    public static /* final */ Queue<k00> c; // = r00.a(0);
    @DexIgnore
    public InputStream a;
    @DexIgnore
    public IOException b;

    @DexIgnore
    public static k00 b(InputStream inputStream) {
        k00 poll;
        synchronized (c) {
            poll = c.poll();
        }
        if (poll == null) {
            poll = new k00();
        }
        poll.a(inputStream);
        return poll;
    }

    @DexIgnore
    public void a(InputStream inputStream) {
        this.a = inputStream;
    }

    @DexIgnore
    public int available() throws IOException {
        return this.a.available();
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public IOException k() {
        return this.b;
    }

    @DexIgnore
    public void l() {
        this.b = null;
        this.a = null;
        synchronized (c) {
            c.offer(this);
        }
    }

    @DexIgnore
    public void mark(int i) {
        this.a.mark(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.a.markSupported();
    }

    @DexIgnore
    public int read(byte[] bArr) {
        try {
            return this.a.read(bArr);
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }

    @DexIgnore
    public synchronized void reset() throws IOException {
        this.a.reset();
    }

    @DexIgnore
    public long skip(long j) {
        try {
            return this.a.skip(j);
        } catch (IOException e) {
            this.b = e;
            return 0;
        }
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.a.read(bArr, i, i2);
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }

    @DexIgnore
    public int read() {
        try {
            return this.a.read();
        } catch (IOException e) {
            this.b = e;
            return -1;
        }
    }
}
