package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ FlexibleButton C;
    @DexIgnore
    public /* final */ FlexibleButton D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ ImageView G;
    @DexIgnore
    public /* final */ ImageView H;
    @DexIgnore
    public /* final */ ImageView I;
    @DexIgnore
    public /* final */ ImageView J;
    @DexIgnore
    public /* final */ RTLImageView K;
    @DexIgnore
    public /* final */ FossilCircleImageView L;
    @DexIgnore
    public /* final */ NestedScrollView M;
    @DexIgnore
    public /* final */ RecyclerView N;
    @DexIgnore
    public /* final */ AutoResizeTextView O;
    @DexIgnore
    public /* final */ FlexibleTextView P;
    @DexIgnore
    public /* final */ AutoResizeTextView Q;
    @DexIgnore
    public /* final */ FlexibleTextView R;
    @DexIgnore
    public /* final */ AutoResizeTextView S;
    @DexIgnore
    public /* final */ FlexibleTextView T;
    @DexIgnore
    public /* final */ FlexibleTextView U;
    @DexIgnore
    public /* final */ FlexibleTextView V;
    @DexIgnore
    public /* final */ FlexibleTextView W;
    @DexIgnore
    public /* final */ FlexibleTextView X;
    @DexIgnore
    public /* final */ FlexibleTextView Y;
    @DexIgnore
    public /* final */ FlexibleTextView Z;
    @DexIgnore
    public /* final */ FlexibleTextView a0;
    @DexIgnore
    public /* final */ View b0;
    @DexIgnore
    public /* final */ RelativeLayout q;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ RelativeLayout s;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ RelativeLayout u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ RelativeLayout w;
    @DexIgnore
    public /* final */ RelativeLayout x;
    @DexIgnore
    public /* final */ RelativeLayout y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fb4(Object obj, View view, int i, Barrier barrier, RelativeLayout relativeLayout, RelativeLayout relativeLayout2, RelativeLayout relativeLayout3, RelativeLayout relativeLayout4, RelativeLayout relativeLayout5, FlexibleButton flexibleButton, RelativeLayout relativeLayout6, RelativeLayout relativeLayout7, RelativeLayout relativeLayout8, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, Barrier barrier2, Guideline guideline, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, RTLImageView rTLImageView5, RTLImageView rTLImageView6, RTLImageView rTLImageView7, RTLImageView rTLImageView8, RTLImageView rTLImageView9, FossilCircleImageView fossilCircleImageView, FossilCircleImageView fossilCircleImageView2, ConstraintLayout constraintLayout7, LinearLayout linearLayout, NestedScrollView nestedScrollView, RecyclerView recyclerView, AutoResizeTextView autoResizeTextView, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, AutoResizeTextView autoResizeTextView2, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, AutoResizeTextView autoResizeTextView3, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, FlexibleTextView flexibleTextView16, FlexibleTextView flexibleTextView17, FlexibleTextView flexibleTextView18, View view2, Guideline guideline2) {
        super(obj, view, i);
        this.q = relativeLayout;
        this.r = relativeLayout2;
        this.s = relativeLayout3;
        this.t = relativeLayout4;
        this.u = relativeLayout5;
        this.v = flexibleButton;
        this.w = relativeLayout6;
        this.x = relativeLayout7;
        this.y = relativeLayout8;
        this.z = constraintLayout;
        this.A = constraintLayout2;
        this.B = constraintLayout6;
        this.C = flexibleButton2;
        this.D = flexibleButton3;
        this.E = flexibleTextView;
        this.F = flexibleTextView2;
        this.G = imageView;
        this.H = imageView2;
        this.I = imageView3;
        this.J = imageView4;
        this.K = rTLImageView4;
        this.L = fossilCircleImageView;
        this.M = nestedScrollView;
        this.N = recyclerView;
        this.O = autoResizeTextView;
        this.P = flexibleTextView3;
        this.Q = autoResizeTextView2;
        this.R = flexibleTextView5;
        this.S = autoResizeTextView3;
        this.T = flexibleTextView7;
        this.U = flexibleTextView9;
        this.V = flexibleTextView10;
        this.W = flexibleTextView12;
        this.X = flexibleTextView13;
        this.Y = flexibleTextView14;
        this.Z = flexibleTextView15;
        this.a0 = flexibleTextView18;
        this.b0 = view2;
    }
}
