package com.fossil;

import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1", f = "SignUpPresenter.kt", l = {501, 502, 503, 504, 505, 506}, m = "invokeSuspend")
public final class ut5$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut5$e$a(SignUpPresenter.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ut5$e$a ut5_e_a = new ut5$e$a(this.this$0, xe6);
        ut5_e_a.p$ = (il6) obj;
        return ut5_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ut5$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        r6 = r5.this$0.this$0.x();
        r5.L$0 = r1;
        r5.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006e, code lost:
        if (r6.fetchActivitySettings(r5) != r0) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
        r6 = r5.this$0.this$0.w();
        r5.L$0 = r1;
        r5.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0082, code lost:
        if (r6.fetchLastSleepGoal(r5) != r0) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0084, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        r6 = r5.this$0.this$0.u();
        r5.L$0 = r1;
        r5.label = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
        if (r6.fetchGoalSetting(r5) != r0) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0098, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0099, code lost:
        r6 = r5.this$0.this$0.p();
        r5.L$0 = r1;
        r5.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00aa, code lost:
        if (r6.downloadAlarms(r5) != r0) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00ac, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ad, code lost:
        r6 = r5.this$0.this$0.r();
        r5.L$0 = r1;
        r5.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bf, code lost:
        if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r6, 0, r5, 1, (java.lang.Object) null) != r0) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c1, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c4, code lost:
        return com.fossil.cd6.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        switch (this.label) {
            case 0:
                nc6.a(obj);
                il6 = this.p$;
                WatchLocalizationRepository z = this.this$0.this$0.z();
                this.L$0 = il6;
                this.label = 1;
                if (z.getWatchLocalizationFromServer(false, this) == a) {
                    return a;
                }
                break;
            case 1:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 2:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 3:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 4:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 5:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 6:
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
