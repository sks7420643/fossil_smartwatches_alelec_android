package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk3<T> extends hk3<T> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ T reference;

    @DexIgnore
    public mk3(T t) {
        this.reference = t;
    }

    @DexIgnore
    public Set<T> asSet() {
        return Collections.singleton(this.reference);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof mk3) {
            return this.reference.equals(((mk3) obj).reference);
        }
        return false;
    }

    @DexIgnore
    public T get() {
        return this.reference;
    }

    @DexIgnore
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @DexIgnore
    public boolean isPresent() {
        return true;
    }

    @DexIgnore
    public T or(T t) {
        jk3.a(t, (Object) "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }

    @DexIgnore
    public T orNull() {
        return this.reference;
    }

    @DexIgnore
    public String toString() {
        return "Optional.of(" + this.reference + ")";
    }

    @DexIgnore
    public <V> hk3<V> transform(ck3<? super T, V> ck3) {
        V apply = ck3.apply(this.reference);
        jk3.a(apply, (Object) "the Function passed to Optional.transform() must not return null.");
        return new mk3(apply);
    }

    @DexIgnore
    public hk3<T> or(hk3<? extends T> hk3) {
        jk3.a(hk3);
        return this;
    }

    @DexIgnore
    public T or(nk3<? extends T> nk3) {
        jk3.a(nk3);
        return this.reference;
    }
}
