package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt2 implements nt2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.personalized_ads_signals_collection_enabled", true);
        b = vk2.a("measurement.personalized_ads_property_translation_enabled", true);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzb() {
        return b.b().booleanValue();
    }
}
