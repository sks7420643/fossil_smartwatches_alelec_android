package com.fossil;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw0 implements ThreadFactory {
    @DexIgnore
    public /* final */ AtomicInteger a; // = new AtomicInteger(1);

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, cy0.class.getSimpleName() + " #" + this.a.getAndIncrement());
    }
}
