package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he2 implements Parcelable.Creator<ee2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        f72 f72 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                f72 = (f72) f22.a(parcel, a, f72.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new ee2(f72);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ee2[i];
    }
}
