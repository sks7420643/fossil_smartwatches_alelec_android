package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl6 {
    @DexIgnore
    public static final il6 a() {
        return new eo6(mn6.a((rm6) null, 1, (Object) null).plus(zl6.c()));
    }

    @DexIgnore
    public static final il6 a(af6 af6) {
        wg6.b(af6, "context");
        if (af6.get(rm6.n) == null) {
            af6 = af6.plus(vm6.a((rm6) null, 1, (Object) null));
        }
        return new eo6(af6);
    }

    @DexIgnore
    public static /* synthetic */ void a(il6 il6, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        a(il6, cancellationException);
    }

    @DexIgnore
    public static final void a(il6 il6, CancellationException cancellationException) {
        wg6.b(il6, "$this$cancel");
        rm6 rm6 = (rm6) il6.m().get(rm6.n);
        if (rm6 != null) {
            rm6.a(cancellationException);
            return;
        }
        throw new IllegalStateException(("Scope cannot be cancelled because it does not have a job: " + il6).toString());
    }
}
