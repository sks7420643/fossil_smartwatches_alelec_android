package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx2 {
    @DexIgnore
    public static /* final */ int[] MapAttrs; // = {2130968750, 2130968919, 2130968920, 2130968921, 2130968922, 2130968923, 2130968924, 2130968925, 2130969473, 2130969474, 2130969475, 2130969476, 2130969572, 2130969585, 2130970061, 2130970062, 2130970063, 2130970064, 2130970065, 2130970066, 2130970067, 2130970068, 2130970078, 2130970175};
    @DexIgnore
    public static /* final */ int MapAttrs_ambientEnabled; // = 0;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraBearing; // = 1;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMaxZoomPreference; // = 2;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMinZoomPreference; // = 3;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLat; // = 4;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLng; // = 5;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTilt; // = 6;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraZoom; // = 7;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLatitude; // = 8;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLongitude; // = 9;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLatitude; // = 10;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLongitude; // = 11;
    @DexIgnore
    public static /* final */ int MapAttrs_liteMode; // = 12;
    @DexIgnore
    public static /* final */ int MapAttrs_mapType; // = 13;
    @DexIgnore
    public static /* final */ int MapAttrs_uiCompass; // = 14;
    @DexIgnore
    public static /* final */ int MapAttrs_uiMapToolbar; // = 15;
    @DexIgnore
    public static /* final */ int MapAttrs_uiRotateGestures; // = 16;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGestures; // = 17;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGesturesDuringRotateOrZoom; // = 18;
    @DexIgnore
    public static /* final */ int MapAttrs_uiTiltGestures; // = 19;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomControls; // = 20;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomGestures; // = 21;
    @DexIgnore
    public static /* final */ int MapAttrs_useViewLifecycle; // = 22;
    @DexIgnore
    public static /* final */ int MapAttrs_zOrderOnTop; // = 23;
}
