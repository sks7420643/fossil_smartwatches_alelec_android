package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.n12;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h12 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<h12> CREATOR; // = new l32();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public String d;
    @DexIgnore
    public IBinder e;
    @DexIgnore
    public Scope[] f;
    @DexIgnore
    public Bundle g;
    @DexIgnore
    public Account h;
    @DexIgnore
    public iv1[] i;
    @DexIgnore
    public iv1[] j;
    @DexIgnore
    public boolean o;

    @DexIgnore
    public h12(int i2) {
        this.a = 4;
        this.c = kv1.a;
        this.b = i2;
        this.o = true;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d, false);
        g22.a(parcel, 5, this.e, false);
        g22.a(parcel, 6, (T[]) this.f, i2, false);
        g22.a(parcel, 7, this.g, false);
        g22.a(parcel, 8, (Parcelable) this.h, i2, false);
        g22.a(parcel, 10, (T[]) this.i, i2, false);
        g22.a(parcel, 11, (T[]) this.j, i2, false);
        g22.a(parcel, 12, this.o);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public h12(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, iv1[] iv1Arr, iv1[] iv1Arr2, boolean z) {
        this.a = i2;
        this.b = i3;
        this.c = i4;
        if ("com.google.android.gms".equals(str)) {
            this.d = "com.google.android.gms";
        } else {
            this.d = str;
        }
        if (i2 < 2) {
            this.h = iBinder != null ? a12.a(n12.a.a(iBinder)) : null;
        } else {
            this.e = iBinder;
            this.h = account;
        }
        this.f = scopeArr;
        this.g = bundle;
        this.i = iv1Arr;
        this.j = iv1Arr2;
        this.o = z;
    }
}
