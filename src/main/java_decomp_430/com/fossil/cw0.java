package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cw0 {
    @DexIgnore
    public static final double a(long j) {
        return ((double) j) / ((double) 1000);
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(ArrayList<T> arrayList, ArrayList<T> arrayList2) {
        ArrayList<T> arrayList3 = new ArrayList<>(arrayList);
        arrayList3.addAll(arrayList2);
        return arrayList3;
    }

    @DexIgnore
    public static final int b(short s) {
        return s < 0 ? s + 65536 : s;
    }

    @DexIgnore
    public static final long b(int i) {
        long j = (long) i;
        return i < 0 ? j + 4294967296L : j;
    }

    @DexIgnore
    public static final short b(byte b) {
        return b < 0 ? (short) (b + 256) : (short) b;
    }

    @DexIgnore
    public static final byte[] b(byte[] bArr, int i) {
        long b = b(i);
        for (int length = bArr.length - 1; length >= 0; length--) {
            long b2 = b + ((long) b(bArr[length]));
            long j = (long) 256;
            long j2 = b2 % j;
            b = b2 / j;
            bArr[length] = (byte) ((int) j2);
            if (b == 0) {
                break;
            }
        }
        return bArr;
    }

    @DexIgnore
    public static final String a(Enum<?> enumR) {
        String name = enumR.name();
        Locale h = mi0.A.h();
        if (name != null) {
            String lowerCase = name.toLowerCase(h);
            wg6.a(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            return lowerCase;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final JSONArray a(String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        for (String put : strArr) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    @DexIgnore
    public static /* synthetic */ String a(byte[] bArr, String str, int i) {
        if ((i & 1) != 0) {
            str = "";
        }
        int i2 = 0;
        if (bArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        while (i2 < bArr.length - 1) {
            sb.append(a(bArr[i2]));
            sb.append(str);
            i2++;
        }
        if (i2 == bArr.length - 1) {
            sb.append(a(bArr[i2]));
        }
        String sb2 = sb.toString();
        wg6.a(sb2, "sb.toString()");
        return sb2;
    }

    @DexIgnore
    public static final String a(String str) {
        if ((str.length() > 0) && ak6.e(str) == ((char) 0)) {
            return str;
        }
        return str + 0;
    }

    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        JSONObject jSONObject3 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                jSONObject3.putOpt(next, jSONObject.get(next));
            } catch (JSONException e) {
                qs0.h.a(e);
            }
        }
        Iterator<String> keys2 = jSONObject2.keys();
        while (keys2.hasNext()) {
            String next2 = keys2.next();
            try {
                jSONObject3.putOpt(next2, jSONObject2.get(next2));
            } catch (JSONException e2) {
                qs0.h.a(e2);
            }
        }
        return jSONObject3;
    }

    @DexIgnore
    public static final SharedPreferences a(lp0 lp0) {
        String name = lp0.name();
        Context a = gk0.f.a();
        if (a != null) {
            return a.getSharedPreferences(name, 0);
        }
        return null;
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, int i, Charset charset, CodingErrorAction codingErrorAction, int i2) {
        if ((i2 & 2) != 0) {
            charset = ej6.a;
        }
        if ((i2 & 4) != 0) {
            codingErrorAction = CodingErrorAction.IGNORE;
            wg6.a(codingErrorAction, "CodingErrorAction.IGNORE");
        }
        CharsetDecoder newDecoder = charset.newDecoder();
        wg6.a(newDecoder, "charset.newDecoder()");
        byte[] bytes = str.getBytes(charset);
        wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
        int min = Math.min(i, bytes.length);
        ByteBuffer wrap = ByteBuffer.wrap(bytes, 0, min);
        wg6.a(wrap, "ByteBuffer.wrap(stringByteArray, 0, lengthInByte)");
        CharBuffer allocate = CharBuffer.allocate(min);
        wg6.a(allocate, "CharBuffer.allocate(lengthInByte)");
        newDecoder.onMalformedInput(codingErrorAction);
        newDecoder.decode(wrap, allocate, true);
        newDecoder.flush(allocate);
        char[] array = allocate.array();
        wg6.a(array, "charBuffer.array()");
        return new String(array, 0, allocate.position());
    }

    @DexIgnore
    public static final JSONArray a(p40[] p40Arr) {
        JSONArray jSONArray = new JSONArray();
        for (p40 a : p40Arr) {
            jSONArray.put(a.a());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        if (bArr2.length == 0) {
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            wg6.a(copyOf, "java.util.Arrays.copyOf(this, size)");
            return copyOf;
        }
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    @DexIgnore
    public static final JSONArray a(FitnessData[] fitnessDataArr) {
        ArrayList<Short> values;
        JSONArray jSONArray = new JSONArray();
        for (FitnessData fitnessData : fitnessDataArr) {
            JSONObject jSONObject = new JSONObject();
            bm0 bm0 = bm0.WORKOUT_SESSION;
            ArrayList<WorkoutSession> workouts = fitnessData.getWorkouts();
            a(jSONObject, bm0, workouts != null ? Integer.valueOf(workouts.size()) : JSONObject.NULL);
            bm0 bm02 = bm0.SLEEP_SESSION;
            ArrayList<SleepSession> sleeps = fitnessData.getSleeps();
            a(jSONObject, bm02, sleeps != null ? Integer.valueOf(sleeps.size()) : JSONObject.NULL);
            a(jSONObject, bm0.TIMEZONE_OFFSET_IN_SECOND, (Object) Integer.valueOf(fitnessData.getTimezoneOffsetInSecond()));
            bm0 bm03 = bm0.TOTAL_ACTIVE_MINUTES;
            ActiveMinute activeMinute = fitnessData.getActiveMinute();
            a(jSONObject, bm03, activeMinute != null ? Integer.valueOf(activeMinute.getTotal()) : JSONObject.NULL);
            bm0 bm04 = bm0.TOTAL_DISTANCE;
            Distance distance = fitnessData.getDistance();
            a(jSONObject, bm04, distance != null ? Double.valueOf(distance.getTotal()) : JSONObject.NULL);
            a(jSONObject, bm0.END_TIME, (Object) Integer.valueOf(fitnessData.getEndTime()));
            a(jSONObject, bm0.START_TIME, (Object) Integer.valueOf(fitnessData.getStartTime()));
            bm0 bm05 = bm0.TOTAL_STEPS;
            Step step = fitnessData.getStep();
            a(jSONObject, bm05, step != null ? Integer.valueOf(step.getTotal()) : JSONObject.NULL);
            bm0 bm06 = bm0.TOTAL_CALORIES;
            Calorie calorie = fitnessData.getCalorie();
            a(jSONObject, bm06, calorie != null ? Integer.valueOf(calorie.getTotal()) : JSONObject.NULL);
            bm0 bm07 = bm0.HEART_RATE_RECORD;
            HeartRate heartrate = fitnessData.getHeartrate();
            a(jSONObject, bm07, (heartrate == null || (values = heartrate.getValues()) == null) ? JSONObject.NULL : Integer.valueOf(values.size()));
            bm0 bm08 = bm0.RESTING;
            ArrayList<Resting> resting = fitnessData.getResting();
            a(jSONObject, bm08, resting != null ? Integer.valueOf(resting.size()) : JSONObject.NULL);
            bm0 bm09 = bm0.GOAL_TRACKING;
            ArrayList<GoalTracking> goals = fitnessData.getGoals();
            a(jSONObject, bm09, goals != null ? Integer.valueOf(goals.size()) : JSONObject.NULL);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final String a(int i) {
        cj6.a(16);
        String l = Long.toString(((long) i) & 4294967295L, 16);
        wg6.a(l, "java.lang.Long.toString(this, checkRadix(radix))");
        Locale h = mi0.A.h();
        if (l != null) {
            String upperCase = l.toUpperCase(h);
            wg6.a(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return yj6.a(upperCase, 8, '0');
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(short s) {
        cj6.a(16);
        String num = Integer.toString(s & 65535, 16);
        wg6.a(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        Locale h = mi0.A.h();
        if (num != null) {
            String upperCase = num.toUpperCase(h);
            wg6.a(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return yj6.a(upperCase, 4, '0');
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(byte b) {
        cj6.a(16);
        String num = Integer.toString(b & 255, 16);
        wg6.a(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        Locale h = mi0.A.h();
        if (num != null) {
            String upperCase = num.toUpperCase(h);
            wg6.a(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return yj6.a(upperCase, 2, '0');
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final JSONArray a(s60[] s60Arr) {
        JSONArray jSONArray = new JSONArray();
        for (s60 b : s60Arr) {
            jSONArray.put(b.b());
        }
        return jSONArray;
    }

    @DexIgnore
    public static /* synthetic */ JSONObject a(JSONObject jSONObject, JSONObject jSONObject2, boolean z, int i) {
        if ((i & 2) != 0) {
            z = true;
        }
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (z || !jSONObject.has(next)) {
                try {
                    jSONObject.putOpt(next, jSONObject2.get(next));
                } catch (JSONException e) {
                    qs0.h.a(e);
                }
            }
        }
        return jSONObject;
    }

    @DexIgnore
    public static final byte a(Set<? extends k70> set) {
        byte b = (byte) 0;
        for (k70 b2 : set) {
            b = (byte) (b | b2.b());
        }
        return b;
    }

    @DexIgnore
    public static final float a(float f, int i) {
        float pow = (float) Math.pow((double) 10.0f, (double) i);
        return ((float) rh6.b(f * pow)) / pow;
    }

    @DexIgnore
    public static final JSONArray a(UUID[] uuidArr) {
        JSONArray jSONArray = new JSONArray();
        for (UUID uuid : uuidArr) {
            jSONArray.put(uuid.toString());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray a(rg1[] rg1Arr) {
        JSONArray jSONArray = new JSONArray();
        for (rg1 rg1 : rg1Arr) {
            jSONArray.put(rg1.a);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final byte[][] a(byte[] bArr, int i) {
        if (i <= 0) {
            return new byte[][]{bArr};
        }
        ArrayList arrayList = new ArrayList();
        uh6 a = ci6.a(ci6.d(0, bArr.length), i);
        int a2 = a.a();
        int b = a.b();
        int c = a.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                arrayList.add(md6.a(bArr, a2, Math.min(a2 + i, bArr.length)));
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, bm0 bm0, Object obj) {
        JSONObject put = jSONObject.put(a((Enum<?>) bm0), obj);
        wg6.a(put, "this.put(key.lowerCaseName, value)");
        return put;
    }
}
