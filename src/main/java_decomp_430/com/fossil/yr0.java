package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr0 extends wm0 {
    @DexIgnore
    public int D;

    @DexIgnore
    public yr0(ue1 ue1, q41 q41) {
        super(ue1, q41, eh1.READ_RSSI, new pf1(ue1));
    }

    @DexIgnore
    public void b(qv0 qv0) {
        if (qv0 instanceof pf1) {
            this.D = ((pf1) qv0).A;
        }
    }

    @DexIgnore
    public Object d() {
        return Integer.valueOf(this.D);
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.RSSI, (Object) Integer.valueOf(this.D));
    }
}
