package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f74 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ LinearLayout x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ RecyclerView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f74(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ImageView imageView2, ConstraintLayout constraintLayout, View view2, ImageView imageView3, LinearLayout linearLayout, ConstraintLayout constraintLayout2, RecyclerView recyclerView, View view3) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = rTLImageView;
        this.v = imageView2;
        this.w = view2;
        this.x = linearLayout;
        this.y = constraintLayout2;
        this.z = recyclerView;
    }
}
