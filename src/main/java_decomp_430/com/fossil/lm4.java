package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lm4 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public void a(ku3 ku3) {
        if (ku3.d("platform")) {
            ku3.a("platform").f();
        }
        if (ku3.d("data")) {
            this.a = ku3.a("data").d().a("url").f();
        }
        if (ku3.d("metadata")) {
            this.b = ku3.a("metadata").d().a("checksum").f();
        }
        if (ku3.d("metadata")) {
            this.c = ku3.a("metadata").d().a("appVersion").f();
        }
        if (ku3.d("createdAt")) {
            ku3.a("createdAt").f();
        }
        if (ku3.d("updatedAt")) {
            this.d = ku3.a("updatedAt").f();
        }
        if (ku3.d("objectId")) {
            ku3.a("objectId").f();
        }
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
