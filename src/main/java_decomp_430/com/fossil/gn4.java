package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.manager.WeatherManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn4 implements MembersInjector<en4> {
    @DexIgnore
    public static void a(WeatherManager weatherManager, PortfolioApp portfolioApp) {
        weatherManager.a = portfolioApp;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, ApiServiceV2 apiServiceV2) {
        weatherManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, LocationSource locationSource) {
        weatherManager.c = locationSource;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, UserRepository userRepository) {
        weatherManager.d = userRepository;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, CustomizeRealDataRepository customizeRealDataRepository) {
        weatherManager.e = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, DianaPresetRepository dianaPresetRepository) {
        weatherManager.f = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(WeatherManager weatherManager, GoogleApiService googleApiService) {
        weatherManager.g = googleApiService;
    }
}
