package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex4 {
    @DexIgnore
    public /* final */ va5 a;
    @DexIgnore
    public /* final */ k45 b;
    @DexIgnore
    public /* final */ x85 c;
    @DexIgnore
    public /* final */ oj5 d;
    @DexIgnore
    public /* final */ ox4 e;
    @DexIgnore
    public /* final */ g15 f;
    @DexIgnore
    public /* final */ ij5 g;

    @DexIgnore
    public ex4(va5 va5, k45 k45, x85 x85, oj5 oj5, ox4 ox4, g15 g15, ij5 ij5) {
        wg6.b(va5, "mDashboardView");
        wg6.b(k45, "mDianaCustomizeView");
        wg6.b(x85, "mHybridCustomizeView");
        wg6.b(oj5, "mProfileView");
        wg6.b(ox4, "mAlertsView");
        wg6.b(g15, "mAlertsHybridView");
        wg6.b(ij5, "mUpdateFirmwareView");
        this.a = va5;
        this.b = k45;
        this.c = x85;
        this.d = oj5;
        this.e = ox4;
        this.f = g15;
        this.g = ij5;
    }

    @DexIgnore
    public final g15 a() {
        return this.f;
    }

    @DexIgnore
    public final ox4 b() {
        return this.e;
    }

    @DexIgnore
    public final va5 c() {
        return this.a;
    }

    @DexIgnore
    public final k45 d() {
        return this.b;
    }

    @DexIgnore
    public final x85 e() {
        return this.c;
    }

    @DexIgnore
    public final oj5 f() {
        return this.d;
    }

    @DexIgnore
    public final ij5 g() {
        return this.g;
    }
}
