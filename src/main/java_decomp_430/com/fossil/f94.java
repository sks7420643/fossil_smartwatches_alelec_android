package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ViewPager2 A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ CustomizeWidget H;
    @DexIgnore
    public /* final */ CustomizeWidget I;
    @DexIgnore
    public /* final */ CustomizeWidget J;
    @DexIgnore
    public /* final */ CustomizeWidget K;
    @DexIgnore
    public /* final */ CustomizeWidget L;
    @DexIgnore
    public /* final */ CustomizeWidget M;
    @DexIgnore
    public /* final */ CustomizeWidget N;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ CardView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f94(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, CardView cardView, FlexibleTextView flexibleTextView, View view3, View view4, ImageView imageView, ImageView imageView2, View view5, View view6, View view7, ConstraintLayout constraintLayout4, ViewPager2 viewPager2, RTLImageView rTLImageView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, View view8, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4, CustomizeWidget customizeWidget5, CustomizeWidget customizeWidget6, CustomizeWidget customizeWidget7) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout3;
        this.s = cardView;
        this.t = flexibleTextView;
        this.u = imageView;
        this.v = imageView2;
        this.w = view5;
        this.x = view6;
        this.y = view7;
        this.z = constraintLayout4;
        this.A = viewPager2;
        this.B = rTLImageView;
        this.C = flexibleTextView2;
        this.D = flexibleTextView3;
        this.E = flexibleTextView4;
        this.F = flexibleTextView5;
        this.G = view8;
        this.H = customizeWidget;
        this.I = customizeWidget2;
        this.J = customizeWidget3;
        this.K = customizeWidget4;
        this.L = customizeWidget5;
        this.M = customizeWidget6;
        this.N = customizeWidget7;
    }
}
