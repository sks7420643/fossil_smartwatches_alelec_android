package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bg2 extends pg2 implements ag2 {
    @DexIgnore
    public bg2() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((xf2) zg2.a(parcel, xf2.CREATOR));
        return true;
    }
}
