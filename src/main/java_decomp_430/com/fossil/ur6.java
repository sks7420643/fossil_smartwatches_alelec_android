package com.fossil;

import com.fossil.sq6;
import com.fossil.yq6;
import com.zendesk.sdk.network.Constants;
import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur6 implements Interceptor {
    @DexIgnore
    public /* final */ lq6 a;

    @DexIgnore
    public ur6(lq6 lq6) {
        this.a = lq6;
    }

    @DexIgnore
    public final String a(List<kq6> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            kq6 kq6 = list.get(i);
            sb.append(kq6.a());
            sb.append('=');
            sb.append(kq6.b());
        }
        return sb.toString();
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        yq6 t = chain.t();
        yq6.a f = t.f();
        RequestBody a2 = t.a();
        if (a2 != null) {
            uq6 b = a2.b();
            if (b != null) {
                f.b("Content-Type", b.toString());
            }
            long a3 = a2.a();
            if (a3 != -1) {
                f.b("Content-Length", Long.toString(a3));
                f.a("Transfer-Encoding");
            } else {
                f.b("Transfer-Encoding", "chunked");
                f.a("Content-Length");
            }
        }
        boolean z = false;
        if (t.a("Host") == null) {
            f.b("Host", fr6.a(t.g(), false));
        }
        if (t.a("Connection") == null) {
            f.b("Connection", "Keep-Alive");
        }
        if (t.a("Accept-Encoding") == null && t.a("Range") == null) {
            z = true;
            f.b("Accept-Encoding", "gzip");
        }
        List<kq6> a4 = this.a.a(t.g());
        if (!a4.isEmpty()) {
            f.b("Cookie", a(a4));
        }
        if (t.a(Constants.USER_AGENT_HEADER) == null) {
            f.b(Constants.USER_AGENT_HEADER, gr6.a());
        }
        Response a5 = chain.a(f.a());
        yr6.a(this.a, t.g(), a5.p());
        Response.a E = a5.E();
        E.a(t);
        if (z && "gzip".equalsIgnoreCase(a5.e("Content-Encoding")) && yr6.b(a5)) {
            qt6 qt6 = new qt6(a5.k().source());
            sq6.a a6 = a5.p().a();
            a6.c("Content-Encoding");
            a6.c("Content-Length");
            E.a(a6.a());
            E.a((zq6) new bs6(a5.e("Content-Type"), -1, st6.a((zt6) qt6)));
        }
        return E.a();
    }
}
