package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum s60 {
    BIOMETRIC_PROFILE(1, "biometric_profile"),
    DAILY_STEP(2, "daily_step"),
    DAILY_STEP_GOAL(3, "daily_step_goal"),
    DAILY_CALORIE(4, "daily_calories"),
    DAILY_CALORIE_GOAL(5, "daily_calories_goal"),
    DAILY_TOTAL_ACTIVE_MINUTE(6, "daily_active_minutes"),
    DAILY_ACTIVE_MINUTE_GOAL(7, "daily_active_minute_goal"),
    DAILY_DISTANCE(8, "daily_distance"),
    INACTIVE_NUDGE(9, "inactive_nudge"),
    VIBE_STRENGTH(10, "vibe_strength_level"),
    DO_NOT_DISTURB_SCHEDULE(11, "dnd_schedule"),
    TIME(12, "time"),
    BATTERY(13, "battery"),
    HEART_RATE_MODE(14, "heart_rate_mode"),
    DAILY_SLEEP(15, "daily_sleep"),
    DISPLAY_UNIT(16, "display_unit"),
    SECOND_TIMEZONE_OFFSET(17, "second_timezone_offset"),
    CURRENT_HEART_RATE(18, "current_heart_rate"),
    HELLAS_BATTERY(19, "hellas_battery");
    
    @DexIgnore
    public static /* final */ a d; // = null;
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ String b;

    /*
    static {
        d = new a((qg6) null);
    }
    */

    @DexIgnore
    public s60(short s, String str) {
        this.a = s;
        this.b = str;
    }

    @DexIgnore
    public final short a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final s60 a(short s) {
            for (s60 s60 : s60.values()) {
                if (s60.a() == s) {
                    return s60;
                }
            }
            return null;
        }

        @DexIgnore
        public final s60 a(String str) {
            for (s60 s60 : s60.values()) {
                if (wg6.a(str, s60.b())) {
                    return s60;
                }
            }
            return null;
        }
    }
}
