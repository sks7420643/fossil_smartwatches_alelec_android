package com.fossil;

import android.content.Context;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v36 {
    @DexIgnore
    public static String l;
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public c56 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public r36 k; // = null;

    @DexIgnore
    public v36(Context context, int i2, r36 r36) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = n36.d(context);
        this.h = m56.m(context);
        this.a = n36.b(context);
        if (r36 != null) {
            this.k = r36;
            if (m56.c(r36.a())) {
                this.a = r36.a();
            }
            if (m56.c(r36.b())) {
                this.g = r36.b();
            }
            if (m56.c(r36.c())) {
                this.h = r36.c();
            }
            this.i = r36.d();
        }
        this.f = n36.c(context);
        this.d = o46.b(context).a(context);
        w36 a2 = a();
        w36 w36 = w36.NETWORK_DETECTOR;
        this.e = a2 != w36 ? m56.v(context).intValue() : -w36.a();
        if (!b26.b(l)) {
            String e2 = n36.e(context);
            l = e2;
            if (!m56.c(e2)) {
                l = ShareWebViewClient.RESP_SUCC_CODE;
            }
        }
    }

    @DexIgnore
    public abstract w36 a();

    @DexIgnore
    public abstract boolean a(JSONObject jSONObject);

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public boolean b(JSONObject jSONObject) {
        try {
            r56.a(jSONObject, "ky", this.a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.b());
                r56.a(jSONObject, "mc", this.d.c());
                int d2 = this.d.d();
                jSONObject.put("ut", d2);
                if (d2 == 0 && m56.z(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            r56.a(jSONObject, "cui", this.f);
            if (a() != w36.SESSION_ENV) {
                r56.a(jSONObject, "av", this.h);
                r56.a(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            r56.a(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", m56.a(this.j, false));
            return a(jSONObject);
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public r36 c() {
        return this.k;
    }

    @DexIgnore
    public Context d() {
        return this.j;
    }

    @DexIgnore
    public boolean e() {
        return this.i;
    }

    @DexIgnore
    public String f() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }
}
