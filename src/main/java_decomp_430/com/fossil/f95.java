package com.fossil;

import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f95 implements Factory<e95> {
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> c;

    @DexIgnore
    public f95(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static f95 a(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        return new f95(provider, provider2, provider3);
    }

    @DexIgnore
    public static e95 b(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        return new HybridCustomizeViewModel(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public HybridCustomizeViewModel get() {
        return b(this.a, this.b, this.c);
    }
}
