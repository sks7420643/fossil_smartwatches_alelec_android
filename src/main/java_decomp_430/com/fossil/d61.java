package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d61 extends xk1 {
    @DexIgnore
    public /* final */ y40[] R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ d61(ue1 ue1, q41 q41, y40[] y40Arr, short s, String str, int i) {
        super(ue1, q41, eh1.SET_ALARMS, true, (i & 8) != 0 ? lk1.b.b(ue1.t, w31.ALARM) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? ze0.a("UUID.randomUUID().toString()") : str, 32);
        this.R = y40Arr;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.ALARMS, (Object) cw0.a((p40[]) this.R));
    }

    @DexIgnore
    public byte[] n() {
        tg1 tg1 = tg1.d;
        short s = this.C;
        w40 w40 = this.x.a().i().get(Short.valueOf(w31.ALARM.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        return tg1.a(s, w40, this.R);
    }
}
