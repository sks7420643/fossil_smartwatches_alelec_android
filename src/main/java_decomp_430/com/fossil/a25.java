package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a25 implements Factory<x15> {
    @DexIgnore
    public static x15 a(y15 y15) {
        x15 b = y15.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
