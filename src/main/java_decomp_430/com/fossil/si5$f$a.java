package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class si5$f$a extends sf6 implements ig6<il6, xe6<? super DailyHeartRateSummary>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si5$f$a(HeartRateDetailPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        si5$f$a si5_f_a = new si5$f$a(this.this$0, xe6);
        si5_f_a.p$ = (il6) obj;
        return si5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((si5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            HeartRateDetailPresenter heartRateDetailPresenter = this.this$0.this$0;
            return heartRateDetailPresenter.b(heartRateDetailPresenter.f, (List<DailyHeartRateSummary>) this.this$0.this$0.h);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
