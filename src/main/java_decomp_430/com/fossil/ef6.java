package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ef6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends pf6 {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ hg6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xe6 xe6, xe6 xe62, hg6 hg6) {
            super(xe62);
            this.$completion = xe6;
            this.$this_createCoroutineUnintercepted$inlined = hg6;
        }

        @DexIgnore
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                nc6.a(obj);
                hg6 hg6 = this.$this_createCoroutineUnintercepted$inlined;
                if (hg6 != null) {
                    oh6.a((Object) hg6, 1);
                    return hg6.invoke(this);
                }
                throw new rc6("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends jf6 {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ af6 $context;
        @DexIgnore
        public /* final */ /* synthetic */ hg6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xe6 xe6, af6 af6, xe6 xe62, af6 af62, hg6 hg6) {
            super(xe62, af62);
            this.$completion = xe6;
            this.$context = af6;
            this.$this_createCoroutineUnintercepted$inlined = hg6;
        }

        @DexIgnore
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                nc6.a(obj);
                hg6 hg6 = this.$this_createCoroutineUnintercepted$inlined;
                if (hg6 != null) {
                    oh6.a((Object) hg6, 1);
                    return hg6.invoke(this);
                }
                throw new rc6("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends pf6 {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ ig6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xe6 xe6, xe6 xe62, ig6 ig6, Object obj) {
            super(xe62);
            this.$completion = xe6;
            this.$this_createCoroutineUnintercepted$inlined = ig6;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                nc6.a(obj);
                ig6 ig6 = this.$this_createCoroutineUnintercepted$inlined;
                if (ig6 != null) {
                    oh6.a((Object) ig6, 2);
                    return ig6.invoke(this.$receiver$inlined, this);
                }
                throw new rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends jf6 {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ af6 $context;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ ig6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xe6 xe6, af6 af6, xe6 xe62, af6 af62, ig6 ig6, Object obj) {
            super(xe62, af62);
            this.$completion = xe6;
            this.$context = af6;
            this.$this_createCoroutineUnintercepted$inlined = ig6;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                nc6.a(obj);
                ig6 ig6 = this.$this_createCoroutineUnintercepted$inlined;
                if (ig6 != null) {
                    oh6.a((Object) ig6, 2);
                    return ig6.invoke(this.$receiver$inlined, this);
                }
                throw new rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore
    public static final <T> xe6<cd6> a(hg6<? super xe6<? super T>, ? extends Object> hg6, xe6<? super T> xe6) {
        wg6.b(hg6, "$this$createCoroutineUnintercepted");
        wg6.b(xe6, "completion");
        nf6.a(xe6);
        if (hg6 instanceof gf6) {
            return ((gf6) hg6).create(xe6);
        }
        af6 context = xe6.getContext();
        if (context == bf6.INSTANCE) {
            if (xe6 != null) {
                return new a(xe6, xe6, hg6);
            }
            throw new rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (xe6 != null) {
            return new b(xe6, context, xe6, context, hg6);
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final <R, T> xe6<cd6> a(ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6, R r, xe6<? super T> xe6) {
        wg6.b(ig6, "$this$createCoroutineUnintercepted");
        wg6.b(xe6, "completion");
        nf6.a(xe6);
        if (ig6 instanceof gf6) {
            return ((gf6) ig6).create(r, xe6);
        }
        af6 context = xe6.getContext();
        if (context == bf6.INSTANCE) {
            if (xe6 != null) {
                return new c(xe6, xe6, ig6, r);
            }
            throw new rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (xe6 != null) {
            return new d(xe6, context, xe6, context, ig6, r);
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final <T> xe6<T> a(xe6<? super T> xe6) {
        xe6<Object> intercepted;
        wg6.b(xe6, "$this$intercepted");
        jf6 jf6 = (jf6) (!(xe6 instanceof jf6) ? null : xe6);
        return (jf6 == null || (intercepted = jf6.intercepted()) == null) ? xe6 : intercepted;
    }
}
