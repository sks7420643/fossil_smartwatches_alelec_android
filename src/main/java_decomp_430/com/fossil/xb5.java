package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb5 implements Factory<wb5> {
    @DexIgnore
    public static ActiveTimeOverviewWeekPresenter a(vb5 vb5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new ActiveTimeOverviewWeekPresenter(vb5, userRepository, summariesRepository, portfolioApp);
    }
}
