package com.fossil;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.WindowInsetsFrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CropImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ RecyclerView s;
    @DexIgnore
    public /* final */ TextView t;
    @DexIgnore
    public /* final */ TextView u;

    @DexIgnore
    public j94(Object obj, View view, int i, CropImageView cropImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, WindowInsetsFrameLayout windowInsetsFrameLayout, RecyclerView recyclerView, TextView textView, TextView textView2, TextView textView3) {
        super(obj, view, i);
        this.q = cropImageView;
        this.r = constraintLayout;
        this.s = recyclerView;
        this.t = textView;
        this.u = textView2;
    }
}
