package com.fossil;

import java.util.concurrent.Executor;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sq1 implements uq1 {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(wp1.class.getName());
    @DexIgnore
    public /* final */ rr1 a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ dq1 c;
    @DexIgnore
    public /* final */ ur1 d;
    @DexIgnore
    public /* final */ ys1 e;

    @DexIgnore
    public sq1(Executor executor, dq1 dq1, rr1 rr1, ur1 ur1, ys1 ys1) {
        this.b = executor;
        this.c = dq1;
        this.a = rr1;
        this.d = ur1;
        this.e = ys1;
    }

    @DexIgnore
    public void a(rp1 rp1, np1 np1, ko1 ko1) {
        this.b.execute(qq1.a(this, rp1, ko1, np1));
    }

    @DexIgnore
    public static /* synthetic */ void a(sq1 sq1, rp1 rp1, ko1 ko1, np1 np1) {
        try {
            lq1 a2 = sq1.c.a(rp1.a());
            if (a2 == null) {
                String format = String.format("Transport backend '%s' is not registered", new Object[]{rp1.a()});
                f.warning(format);
                ko1.a(new IllegalArgumentException(format));
                return;
            }
            sq1.e.a(rq1.a(sq1, rp1, a2.a(np1)));
            ko1.a((Exception) null);
        } catch (Exception e2) {
            Logger logger = f;
            logger.warning("Error scheduling event " + e2.getMessage());
            ko1.a(e2);
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(sq1 sq1, rp1 rp1, np1 np1) {
        sq1.d.a(rp1, np1);
        sq1.a.a(rp1, 1);
        return null;
    }
}
