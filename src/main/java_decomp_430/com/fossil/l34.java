package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l34 extends dc {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l34(FragmentManager fragmentManager) {
        super(fragmentManager);
        wg6.b(fragmentManager, "fm");
    }

    @DexIgnore
    public int a() {
        return this.g.size();
    }

    @DexIgnore
    public Fragment c(int i) {
        Fragment fragment = this.g.get(i);
        wg6.a((Object) fragment, "mFragmentList.get(position)");
        return fragment;
    }

    @DexIgnore
    public final void a(Fragment fragment, String str) {
        wg6.b(fragment, "fragment");
        wg6.b(str, Explore.COLUMN_TITLE);
        this.g.add(fragment);
        this.h.add(str);
    }

    @DexIgnore
    public CharSequence a(int i) {
        String str = this.h.get(i);
        wg6.a((Object) str, "mFragmentTitleList.get(position)");
        return str;
    }
}
