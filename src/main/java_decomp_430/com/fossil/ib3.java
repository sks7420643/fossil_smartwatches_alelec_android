package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib3 extends rv2 implements hb3 {
    @DexIgnore
    public ib3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    public final x52 a(x52 x52, int i, int i2, String str, int i3) throws RemoteException {
        Parcel q = q();
        sv2.a(q, x52);
        q.writeInt(i);
        q.writeInt(i2);
        q.writeString(str);
        q.writeInt(i3);
        Parcel a = a(1, q);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
