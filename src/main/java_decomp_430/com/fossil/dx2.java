package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx2 extends uf2 implements bx2 {
    @DexIgnore
    public dx2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }
}
