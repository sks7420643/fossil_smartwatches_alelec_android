package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class is3 implements pq3 {
    @DexIgnore
    public /* final */ FirebaseInstanceId.a a;

    @DexIgnore
    public is3(FirebaseInstanceId.a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public final void a(oq3 oq3) {
        FirebaseInstanceId.a aVar = this.a;
        synchronized (aVar) {
            if (aVar.a()) {
                FirebaseInstanceId.this.j();
            }
        }
    }
}
