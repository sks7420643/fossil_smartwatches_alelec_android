package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux3 implements xx3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[qx3.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|(3:25|26|28)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|28) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[qx3.EAN_8.ordinal()] = 1;
            a[qx3.UPC_E.ordinal()] = 2;
            a[qx3.EAN_13.ordinal()] = 3;
            a[qx3.UPC_A.ordinal()] = 4;
            a[qx3.QR_CODE.ordinal()] = 5;
            a[qx3.CODE_39.ordinal()] = 6;
            a[qx3.CODE_93.ordinal()] = 7;
            a[qx3.CODE_128.ordinal()] = 8;
            a[qx3.ITF.ordinal()] = 9;
            a[qx3.PDF_417.ordinal()] = 10;
            a[qx3.CODABAR.ordinal()] = 11;
            a[qx3.DATA_MATRIX.ordinal()] = 12;
            try {
                a[qx3.AZTEC.ordinal()] = 13;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        xx3 xx3;
        switch (a.a[qx3.ordinal()]) {
            case 1:
                xx3 = new mz3();
                break;
            case 2:
                xx3 = new vz3();
                break;
            case 3:
                xx3 = new lz3();
                break;
            case 4:
                xx3 = new rz3();
                break;
            case 5:
                xx3 = new e04();
                break;
            case 6:
                xx3 = new hz3();
                break;
            case 7:
                xx3 = new jz3();
                break;
            case 8:
                xx3 = new fz3();
                break;
            case 9:
                xx3 = new oz3();
                break;
            case 10:
                xx3 = new wz3();
                break;
            case 11:
                xx3 = new dz3();
                break;
            case 12:
                xx3 = new ny3();
                break;
            case 13:
                xx3 = new zx3();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + qx3);
        }
        return xx3.a(str, qx3, i, i2, map);
    }
}
