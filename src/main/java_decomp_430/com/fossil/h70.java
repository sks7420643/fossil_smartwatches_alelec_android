package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum h70 {
    C(0),
    F(1);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final h70 a(int i) {
            for (h70 h70 : h70.values()) {
                if (h70.a() == i) {
                    return h70;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public h70(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
