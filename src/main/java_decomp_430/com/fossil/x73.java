package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface x73 {
    @DexIgnore
    int a(String str);

    @DexIgnore
    List<Bundle> a(String str, String str2);

    @DexIgnore
    Map<String, Object> a(String str, String str2, boolean z);

    @DexIgnore
    void a(c73 c73);

    @DexIgnore
    void a(String str, String str2, Bundle bundle);

    @DexIgnore
    void a(String str, String str2, Object obj);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(String str, String str2, Bundle bundle);

    @DexIgnore
    void d(Bundle bundle);

    @DexIgnore
    String zza();

    @DexIgnore
    void zza(String str);

    @DexIgnore
    String zzb();

    @DexIgnore
    String zzc();

    @DexIgnore
    String zzd();

    @DexIgnore
    long zze();
}
