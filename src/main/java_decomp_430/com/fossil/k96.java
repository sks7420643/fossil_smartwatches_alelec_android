package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k96 {
    @DexIgnore
    public /* final */ q86<String> a; // = new a(this);
    @DexIgnore
    public /* final */ o86<String> b; // = new o86<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements q86<String> {
        @DexIgnore
        public a(k96 k96) {
        }

        @DexIgnore
        public String a(Context context) throws Exception {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    }

    @DexIgnore
    public String a(Context context) {
        try {
            String a2 = this.b.a(context, this.a);
            if ("".equals(a2)) {
                return null;
            }
            return a2;
        } catch (Exception e) {
            c86.g().e("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
