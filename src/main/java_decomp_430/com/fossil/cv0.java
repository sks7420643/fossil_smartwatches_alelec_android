package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv0 extends xg6 implements ig6<qv0, Float, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ x51 a;
    @DexIgnore
    public /* final */ /* synthetic */ ie1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cv0(x51 x51, ie1 ie1) {
        super(2);
        this.a = x51;
        this.b = ie1;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        qv0 qv0 = (qv0) obj;
        float floatValue = ((Number) obj2).floatValue();
        if (this.a.H > 0) {
            x51 x51 = this.a;
            floatValue = ((((float) (this.b.f - this.a.L)) * floatValue) + ((float) (x51.L + x51.I))) / ((float) this.a.H);
        }
        if (Math.abs(floatValue - this.a.J) > this.a.S || floatValue == 1.0f) {
            x51 x512 = this.a;
            x512.J = floatValue;
            x512.a(floatValue);
        }
        return cd6.a;
    }
}
