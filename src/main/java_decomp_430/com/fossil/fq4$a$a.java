package com.fossil;

import android.content.Context;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fq4$a$a extends tg6 implements hg6<Context, fq4> {
    @DexIgnore
    public static /* final */ fq4$a$a INSTANCE; // = new fq4$a$a();

    @DexIgnore
    public fq4$a$a() {
        super(1);
    }

    @DexIgnore
    public final String getName() {
        return "<init>";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(MusicControlComponent.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "<init>(Landroid/content/Context;)V";
    }

    @DexIgnore
    public final MusicControlComponent invoke(Context context) {
        wg6.b(context, "p1");
        return new MusicControlComponent(context);
    }
}
