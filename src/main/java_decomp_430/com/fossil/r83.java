package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ j03 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ ev2 c;
    @DexIgnore
    public /* final */ /* synthetic */ l83 d;

    @DexIgnore
    public r83(l83 l83, j03 j03, String str, ev2 ev2) {
        this.d = l83;
        this.a = j03;
        this.b = str;
        this.c = ev2;
    }

    @DexIgnore
    public final void run() {
        byte[] bArr = null;
        try {
            l43 d2 = this.d.d;
            if (d2 == null) {
                this.d.b().t().a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            bArr = d2.a(this.a, this.b);
            this.d.I();
            this.d.j().a(this.c, bArr);
        } catch (RemoteException e) {
            this.d.b().t().a("Failed to send event to the service to bundle", e);
        } finally {
            this.d.j().a(this.c, bArr);
        }
    }
}
