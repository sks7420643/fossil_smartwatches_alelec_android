package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.jv;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wv implements jv<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public jv<Uri, InputStream> a(nv nvVar) {
            return new wv(this.a);
        }
    }

    @DexIgnore
    public wv(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public jv.a<InputStream> a(Uri uri, int i, int i2, xr xrVar) {
        if (ss.a(i, i2)) {
            return new jv.a<>(new g00(uri), ts.a(this.a, uri));
        }
        return null;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ss.a(uri);
    }
}
