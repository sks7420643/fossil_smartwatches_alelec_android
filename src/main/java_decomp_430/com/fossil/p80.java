package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p80 extends be0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ ce0 b; // = ce0.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ce0 c; // = ce0.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ce0 d; // = ce0.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<o80> a; // = new HashSet<>();

    @DexIgnore
    public p80(o80[] o80Arr) {
        vd6.a(this.a, o80Arr);
    }

    @DexIgnore
    public JSONObject a() {
        return b();
    }

    @DexIgnore
    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (o80 o80 : this.a) {
            jSONArray.put(o80.a());
            cw0.a(jSONObject, o80.b(), false, 2);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(p80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.a, ((p80) obj).a) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public final o80 getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((o80) t).getButtonEvent() == d) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (o80) t;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final o80 getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((o80) t).getButtonEvent() == c) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (o80) t;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final o80 getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((o80) t).getButtonEvent() == b) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (o80) t;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.a.toArray(new o80[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<p80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public p80 createFromParcel(Parcel parcel) {
            return new p80(parcel);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new p80[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m52createFromParcel(Parcel parcel) {
            return new p80(parcel);
        }
    }

    @DexIgnore
    public p80(o80 o80, o80 o802, o80 o803) {
        o80.a(b);
        o802.a(c);
        o803.a(d);
        this.a.add(o80);
        this.a.add(o802);
        this.a.add(o803);
    }

    @DexIgnore
    public p80(Parcel parcel) {
        super(parcel);
        HashSet<o80> hashSet = this.a;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(o80.class.getClassLoader());
        if (readParcelableArray != null) {
            vd6.a(hashSet, (o80[]) readParcelableArray);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }
}
