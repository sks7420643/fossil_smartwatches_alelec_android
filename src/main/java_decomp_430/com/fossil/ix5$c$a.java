package com.fossil;

import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.util.DeviceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix5$c$a extends xg6 implements hg6<Firmware, rl6<? extends Boolean>> {
    @DexIgnore
    public /* final */ /* synthetic */ il6 $this_withContext;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceUtils.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1", f = "DeviceUtils.kt", l = {93}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Firmware $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ix5$c$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ix5$c$a ix5_c_a, Firmware firmware, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ix5_c_a;
            this.$it = firmware;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$it, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DeviceUtils deviceUtils = this.this$0.this$0.this$0;
                Firmware firmware = this.$it;
                this.L$0 = il6;
                this.label = 1;
                obj = deviceUtils.a(firmware, (xe6<? super Boolean>) this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ix5$c$a(DeviceUtils.c cVar, il6 il6) {
        super(1);
        this.this$0 = cVar;
        this.$this_withContext = il6;
    }

    @DexIgnore
    public final rl6<Boolean> invoke(Firmware firmware) {
        wg6.b(firmware, "it");
        return ik6.a(this.$this_withContext, (af6) null, (ll6) null, new a(this, firmware, (xe6) null), 3, (Object) null);
    }
}
