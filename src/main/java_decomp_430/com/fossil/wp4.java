package com.fossil;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import com.fossil.p6;
import com.fossil.sk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wp4 extends cq4 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public LocationSource d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wp4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = wp4.class.getSimpleName();
        wg6.a((Object) simpleName, "LocationSupportedService::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v9, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r9v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(LocationSource.ErrorState errorState) {
        String str;
        wg6.b(errorState, "errorState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = e;
        local.d(str2, "sendNotificationWeather, errorState=" + errorState);
        Intent intent = new Intent();
        if (errorState == LocationSource.ErrorState.LOCATION_PERMISSION_OFF) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", PortfolioApp.get.instance().getPackageName(), (String) null));
            str = jm4.a(getApplicationContext(), 2131886695);
            wg6.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else if (errorState == LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = jm4.a(getApplicationContext(), 2131887041);
            wg6.a((Object) str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (errorState == LocationSource.ErrorState.LOCATION_SERVICE_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = jm4.a(getApplicationContext(), 2131886695);
            wg6.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else {
            str = "";
        }
        String str3 = str;
        PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 134217728);
        sk4.a aVar = sk4.a;
        String string = PortfolioApp.get.instance().getString(2131887051);
        wg6.a((Object) string, "PortfolioApp.instance.ge\u2026ring(R.string.brand_name)");
        wg6.a((Object) activity, "pendingIntent");
        aVar.b(this, 8, string, str3, activity, (List<? extends p6.a>) null);
    }

    @DexIgnore
    public final LocationSource e() {
        LocationSource locationSource = this.d;
        if (locationSource != null) {
            return locationSource;
        }
        wg6.d("mLocationSource");
        throw null;
    }
}
