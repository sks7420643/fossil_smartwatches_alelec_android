package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa5 implements Factory<wa5> {
    @DexIgnore
    public static HomeDashboardPresenter a(va5 va5, PortfolioApp portfolioApp, DeviceRepository deviceRepository, cj4 cj4, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, HeartRateSampleRepository heartRateSampleRepository) {
        return new HomeDashboardPresenter(va5, portfolioApp, deviceRepository, cj4, summariesRepository, goalTrackingRepository, sleepSummariesRepository, setReplyMessageMappingUseCase, quickResponseRepository, heartRateSampleRepository);
    }
}
