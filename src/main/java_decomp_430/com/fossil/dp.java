package com.fossil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dp<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = zo.b();
    @DexIgnore
    public static volatile g j;
    @DexIgnore
    public static dp<?> k; // = new dp<>((Object) null);
    @DexIgnore
    public static dp<Boolean> l; // = new dp<>(true);
    @DexIgnore
    public static dp<Boolean> m; // = new dp<>(false);
    @DexIgnore
    public static dp<?> n; // = new dp<>(true);
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public fp g;
    @DexIgnore
    public List<bp<TResult, Void>> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements bp<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ ep a;
        @DexIgnore
        public /* final */ /* synthetic */ bp b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ ap d;

        @DexIgnore
        public a(dp dpVar, ep epVar, bp bpVar, Executor executor, ap apVar) {
            this.a = epVar;
            this.b = bpVar;
            this.c = executor;
            this.d = apVar;
        }

        @DexIgnore
        public Void then(dp<TResult> dpVar) {
            dp.d(this.a, this.b, dpVar, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements bp<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ ep a;
        @DexIgnore
        public /* final */ /* synthetic */ bp b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ ap d;

        @DexIgnore
        public b(dp dpVar, ep epVar, bp bpVar, Executor executor, ap apVar) {
            this.a = epVar;
            this.b = bpVar;
            this.c = executor;
            this.d = apVar;
        }

        @DexIgnore
        public Void then(dp<TResult> dpVar) {
            dp.c(this.a, this.b, dpVar, this.c, this.d);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements bp<TResult, dp<TContinuationResult>> {
        @DexIgnore
        public /* final */ /* synthetic */ ap a;
        @DexIgnore
        public /* final */ /* synthetic */ bp b;

        @DexIgnore
        public c(dp dpVar, ap apVar, bp bpVar) {
            this.a = apVar;
            this.b = bpVar;
        }

        @DexIgnore
        public dp<TContinuationResult> then(dp<TResult> dpVar) {
            ap apVar = this.a;
            if (apVar != null) {
                apVar.a();
                throw null;
            } else if (dpVar.e()) {
                return dp.b(dpVar.a());
            } else {
                if (dpVar.c()) {
                    return dp.h();
                }
                return dpVar.a((bp<TResult, TContinuationResult>) this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ap a;
        @DexIgnore
        public /* final */ /* synthetic */ ep b;
        @DexIgnore
        public /* final */ /* synthetic */ bp c;
        @DexIgnore
        public /* final */ /* synthetic */ dp d;

        @DexIgnore
        public d(ap apVar, ep epVar, bp bpVar, dp dpVar) {
            this.a = apVar;
            this.b = epVar;
            this.c = bpVar;
            this.d = dpVar;
        }

        @DexIgnore
        public void run() {
            ap apVar = this.a;
            if (apVar == null) {
                try {
                    this.b.a(this.c.then(this.d));
                } catch (CancellationException unused) {
                    this.b.b();
                } catch (Exception e) {
                    this.b.a(e);
                }
            } else {
                apVar.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ap a;
        @DexIgnore
        public /* final */ /* synthetic */ ep b;
        @DexIgnore
        public /* final */ /* synthetic */ bp c;
        @DexIgnore
        public /* final */ /* synthetic */ dp d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements bp<TContinuationResult, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public Void then(dp<TContinuationResult> dpVar) {
                ap apVar = e.this.a;
                if (apVar == null) {
                    if (dpVar.c()) {
                        e.this.b.b();
                    } else if (dpVar.e()) {
                        e.this.b.a(dpVar.a());
                    } else {
                        e.this.b.a(dpVar.b());
                    }
                    return null;
                }
                apVar.a();
                throw null;
            }
        }

        @DexIgnore
        public e(ap apVar, ep epVar, bp bpVar, dp dpVar) {
            this.a = apVar;
            this.b = epVar;
            this.c = bpVar;
            this.d = dpVar;
        }

        @DexIgnore
        public void run() {
            ap apVar = this.a;
            if (apVar == null) {
                try {
                    dp dpVar = (dp) this.c.then(this.d);
                    if (dpVar == null) {
                        this.b.a(null);
                    } else {
                        dpVar.a(new a());
                    }
                } catch (CancellationException unused) {
                    this.b.b();
                } catch (Exception e) {
                    this.b.a(e);
                }
            } else {
                apVar.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends ep<TResult> {
        @DexIgnore
        public f(dp dpVar) {
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(dp<?> dpVar, gp gpVar);
    }

    /*
    static {
        zo.a();
        vo.b();
    }
    */

    @DexIgnore
    public dp() {
    }

    @DexIgnore
    public static <TResult> dp<TResult> h() {
        return n;
    }

    @DexIgnore
    public static <TResult> dp<TResult>.f i() {
        return new f(new dp());
    }

    @DexIgnore
    public static g j() {
        return j;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = a() != null;
        }
        return z;
    }

    @DexIgnore
    public final void f() {
        synchronized (this.a) {
            for (bp then : this.h) {
                try {
                    then.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean g() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public Exception a() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult b() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public dp(TResult tresult) {
        a(tresult);
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(ep<TContinuationResult> epVar, bp<TResult, TContinuationResult> bpVar, dp<TResult> dpVar, Executor executor, ap apVar) {
        try {
            executor.execute(new d(apVar, epVar, bpVar, dpVar));
        } catch (Exception e2) {
            epVar.a((Exception) new cp(e2));
        }
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> c(bp<TResult, TContinuationResult> bpVar, Executor executor, ap apVar) {
        return a(new c(this, apVar, bpVar), executor);
    }

    @DexIgnore
    public static <TResult> dp<TResult> b(TResult tresult) {
        if (tresult == null) {
            return k;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? l : m;
        }
        ep epVar = new ep();
        epVar.a(tresult);
        return epVar.a();
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void c(ep<TContinuationResult> epVar, bp<TResult, dp<TContinuationResult>> bpVar, dp<TResult> dpVar, Executor executor, ap apVar) {
        try {
            executor.execute(new e(apVar, epVar, bpVar, dpVar));
        } catch (Exception e2) {
            epVar.a((Exception) new cp(e2));
        }
    }

    @DexIgnore
    public dp(boolean z) {
        if (z) {
            g();
        } else {
            a((Object) null);
        }
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> a(bp<TResult, TContinuationResult> bpVar, Executor executor, ap apVar) {
        boolean d2;
        ep epVar = new ep();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new a(this, epVar, bpVar, executor, apVar));
            }
        }
        if (d2) {
            d(epVar, bpVar, this, executor, apVar);
        }
        return epVar.a();
    }

    @DexIgnore
    public static <TResult> dp<TResult> b(Exception exc) {
        ep epVar = new ep();
        epVar.a(exc);
        return epVar.a();
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> b(bp<TResult, dp<TContinuationResult>> bpVar, Executor executor, ap apVar) {
        boolean d2;
        ep epVar = new ep();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new b(this, epVar, bpVar, executor, apVar));
            }
        }
        if (d2) {
            c(epVar, bpVar, this, executor, apVar);
        }
        return epVar.a();
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> a(bp<TResult, TContinuationResult> bpVar) {
        return a(bpVar, i, (ap) null);
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> a(bp<TResult, dp<TContinuationResult>> bpVar, Executor executor) {
        return b(bpVar, executor, (ap) null);
    }

    @DexIgnore
    public boolean a(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public <TContinuationResult> dp<TContinuationResult> b(bp<TResult, TContinuationResult> bpVar) {
        return c(bpVar, i, (ap) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    @DexIgnore
    public boolean a(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            f();
            if (!this.f && j() != null) {
                this.g = new fp(this);
            }
        }
    }
}
