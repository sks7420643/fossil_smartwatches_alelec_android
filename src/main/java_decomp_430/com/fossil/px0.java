package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px0 extends ml0 {
    @DexIgnore
    public /* final */ boolean J;
    @DexIgnore
    public c90 K; // = new c90(0, a90.IDLE, b90.COMPLETED, 0, 0, 0, 0, 0, 0, 0, 0);

    @DexIgnore
    public px0(ue1 ue1) {
        super(tf1.GET_CURRENT_WORKOUT_SESSION, lx0.GET_CURRENT_WORKOUT_SESSION, ue1, 0, 8);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 29) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            a90 a = a90.c.a(order.get(4));
            b90 a2 = b90.c.a(order.get(5));
            if (a == null || a2 == null) {
                this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_DATA, (ch0) null, (sj0) null, 27);
            } else {
                this.K = new c90(cw0.b(order.getInt(0)), a, a2, cw0.b(order.getInt(6)), cw0.b(order.getInt(10)), cw0.b(order.getInt(14)), cw0.b(order.getInt(18)), cw0.b(order.getInt(22)), cw0.b(order.get(26)), cw0.b(order.get(27)), cw0.b(order.get(28)));
                cw0.a(jSONObject, bm0.WORKOUT_SESSION, (Object) this.K.a());
                this.v = bn0.a(this.v, (lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27);
            }
        } else {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_LENGTH, (ch0) null, (sj0) null, 27);
        }
        this.C = true;
        return jSONObject;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.WORKOUT_SESSION, (Object) this.K.a());
    }

    @DexIgnore
    public boolean q() {
        return this.J;
    }
}
