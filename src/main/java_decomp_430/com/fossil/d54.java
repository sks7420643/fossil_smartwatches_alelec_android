package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class d54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon18 a;
    @DexIgnore
    private /* final */ /* synthetic */ List b;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback c;

    @DexIgnore
    public /* synthetic */ d54(PresetRepository.Anon18 anon18, List list, PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
        this.a = anon18;
        this.b = list;
        this.c = pushPendingSavedPresetsCallback;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
