package com.fossil;

import android.util.Log;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir3 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Map<Pair<String, String>, qc3<sq3>> b; // = new p4();

    @DexIgnore
    public ir3(Executor executor) {
        this.a = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        return r4;
     */
    @DexIgnore
    public final synchronized qc3<sq3> a(String str, String str2, kr3 kr3) {
        Pair pair = new Pair(str, str2);
        qc3<sq3> qc3 = this.b.get(pair);
        if (qc3 == null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(pair);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb.append("Making new request for: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            qc3<TContinuationResult> b2 = kr3.zza().b(this.a, new hr3(this, pair));
            this.b.put(pair, b2);
            return b2;
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(pair);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 29);
            sb2.append("Joining ongoing request for: ");
            sb2.append(valueOf2);
            Log.d("FirebaseInstanceId", sb2.toString());
        }
    }

    @DexIgnore
    public final /* synthetic */ qc3 a(Pair pair, qc3 qc3) throws Exception {
        synchronized (this) {
            this.b.remove(pair);
        }
        return qc3;
    }
}
