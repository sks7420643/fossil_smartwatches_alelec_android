package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e20 implements h20 {
    @DexIgnore
    public /* final */ y00 a;

    @DexIgnore
    public e20(y00 y00) {
        this.a = y00;
    }

    @DexIgnore
    public static e20 a() throws NoClassDefFoundError, IllegalStateException {
        return a(y00.o());
    }

    @DexIgnore
    public static e20 a(y00 y00) throws IllegalStateException {
        if (y00 != null) {
            return new e20(y00);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    public void a(g20 g20) {
        try {
            this.a.a(g20.a());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
