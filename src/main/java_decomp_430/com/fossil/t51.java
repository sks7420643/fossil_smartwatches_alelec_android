package com.fossil;

import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.NativeProtocol;
import com.fossil.cd0;
import com.fossil.ce0;
import com.fossil.q40;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t51 {
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0249, code lost:
        r7 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0272, code lost:
        r7 = r8;
     */
    @DexIgnore
    public final void a(ii1 ii1, em0 em0) {
        Object obj;
        Object obj2;
        String optString;
        hd0 a;
        Object obj3;
        switch (ny0.d[em0.a.ordinal()]) {
            case 1:
                cb1 cb1 = (cb1) em0;
                oa1 oa1 = oa1.a;
                new Object[1][0] = cb1.a(2);
                byte b = cb1.b;
                int i = cb1.c;
                ArrayList arrayList = new ArrayList();
                Iterator<String> keys = cb1.d.keys();
                wg6.a(keys, "jsonRequestEvent.requestedApps.keys()");
                while (keys.hasNext()) {
                    String next = keys.next();
                    JSONObject optJSONObject = cb1.d.optJSONObject(next);
                    if (optJSONObject != null) {
                        if (next != null) {
                            switch (next.hashCode()) {
                                case -1119206702:
                                    if (!(!next.equals("ringMyPhone") || (optString = optJSONObject.optString(NativeProtocol.WEB_DIALOG_ACTION)) == null || (a = hd0.b.a(optString)) == null)) {
                                        obj2 = new ba0(b, i, a);
                                        break;
                                    }
                                case -755744823:
                                    if (next.equals("commuteApp._.config.commute_info")) {
                                        String optString2 = optJSONObject.optString("dest");
                                        cd0.a aVar = cd0.c;
                                        String optString3 = optJSONObject.optString(NativeProtocol.WEB_DIALOG_ACTION);
                                        wg6.a(optString3, "dataJson.optString(UIScriptConstant.ACTION)");
                                        cd0 a2 = aVar.a(optString3);
                                        if (!(optString2 == null || a2 == null)) {
                                            int i2 = ny0.e[a2.ordinal()];
                                            if (i2 == 1) {
                                                obj2 = new w90(b, i, optString2);
                                                break;
                                            } else if (i2 == 2) {
                                                obj3 = new l90(b, optString2, a2);
                                                break;
                                            } else {
                                                throw new kc6();
                                            }
                                        }
                                    }
                                case -186117117:
                                    if (next.equals("chanceOfRainSSE._.config.info")) {
                                        obj = new t90(b, i);
                                        break;
                                    }
                                case 65217736:
                                    if (next.equals("iftttApp._.config.buttons")) {
                                        ce0.a aVar2 = ce0.b;
                                        String optString4 = optJSONObject.optString("button_evt");
                                        wg6.a(optString4, "dataJson.optString(\"button_evt\")");
                                        ce0 a3 = aVar2.a(optString4);
                                        if (a3 != null) {
                                            String optString5 = optJSONObject.optString("alias", AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                                            wg6.a(optString5, "alias");
                                            obj = new y90(b, i, a3, optString5);
                                            break;
                                        }
                                    }
                                case 126482114:
                                    if (next.equals("weatherInfo")) {
                                        obj = new ca0(b, i);
                                        break;
                                    }
                                case 427103396:
                                    if (next.equals("buddyChallengeApp")) {
                                        try {
                                            a51 a51 = x61.f;
                                            String optString6 = optJSONObject.optString(cw0.a((Enum<?>) bm0.TYPE), "");
                                            wg6.a(optString6, "dataJson.optString(JSONKey.TYPE.lowerCaseName, \"\")");
                                            x61 a4 = a51.a(optString6);
                                            if (a4 != null) {
                                                int i3 = ny0.f[a4.ordinal()];
                                                if (i3 != 1) {
                                                    if (i3 == 2) {
                                                        String string = optJSONObject.getString(cw0.a((Enum<?>) bm0.CHALLENGE_ID));
                                                        wg6.a(string, "dataJson\n               \u2026ALLENGE_ID.lowerCaseName)");
                                                        obj2 = new r90(b, i, string);
                                                        break;
                                                    } else if (i3 == 3) {
                                                        obj = new s90(b, i);
                                                        break;
                                                    } else {
                                                        throw new kc6();
                                                    }
                                                } else {
                                                    obj3 = new k90(b, i, new gc0(optJSONObject.optLong(cw0.a((Enum<?>) bm0.STEP), 0), optJSONObject.optLong(cw0.a((Enum<?>) bm0.CALORIES), 0)));
                                                    break;
                                                }
                                            }
                                        } catch (JSONException unused) {
                                        }
                                    }
                                    obj = null;
                                    break;
                                case 1255636834:
                                    if (next.equals("weatherApp._.config.locations")) {
                                        obj = new da0(b, i);
                                        break;
                                    }
                            }
                        }
                        obj = null;
                        if (obj != null) {
                            arrayList.add(obj);
                        }
                    }
                }
                Object[] array = arrayList.toArray(new d90[0]);
                if (array != null) {
                    for (d90 a5 : (d90[]) array) {
                        ii1.a(a5);
                    }
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            case 2:
                ii1.a(em0);
                return;
            case 3:
                ii1.b(true);
                ii1.a(em0);
                return;
            case 5:
                zg0 zg0 = (zg0) em0;
                h90 h90 = new h90(em0.b, zg0.d, zg0.c);
                q40.b bVar = ii1.t;
                if (bVar != null) {
                    bVar.onEventReceived(ii1, h90);
                    return;
                }
                return;
            case 6:
                p90 p90 = new p90(em0.b, ((ji1) em0).c);
                q40.b bVar2 = ii1.t;
                if (bVar2 != null) {
                    bVar2.onEventReceived(ii1, p90);
                    return;
                }
                return;
            case 7:
                if (ii1.u()) {
                    ii1.a((em0) (zs0) em0).c(new h01(em0, ii1));
                    return;
                }
                return;
            case 8:
                se1 se1 = (se1) em0;
                int i4 = ny0.c[se1.c.getMicroAppId().ordinal()];
                if (i4 == 1) {
                    int i5 = ny0.b[se1.c.getVariant().ordinal()];
                    if (i5 == 1) {
                        u90 u90 = new u90(em0.b, se1.c);
                        q40.b bVar3 = ii1.t;
                        if (bVar3 != null) {
                            bVar3.onEventReceived(ii1, u90);
                            return;
                        }
                        return;
                    } else if (i5 == 2) {
                        v90 v90 = new v90(em0.b, se1.c);
                        q40.b bVar4 = ii1.t;
                        if (bVar4 != null) {
                            bVar4.onEventReceived(ii1, v90);
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                } else if (i4 == 2) {
                    aa0 aa0 = new aa0(em0.b, se1.c);
                    q40.b bVar5 = ii1.t;
                    if (bVar5 != null) {
                        bVar5.onEventReceived(ii1, aa0);
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 9:
                ii1.a(em0);
                return;
            case 10:
                ii1.a(em0).c(new b21(ii1, em0));
                return;
            case 11:
                lw0 lw0 = (lw0) em0;
                ii1.a(em0).c(new x31(ii1, em0));
                return;
            case 12:
                p31 p31 = (p31) em0;
                o90 o90 = new o90(em0.b, p31.c, p31.d, p31.e, p31.f);
                q40.b bVar6 = ii1.t;
                if (bVar6 != null) {
                    bVar6.onEventReceived(ii1, o90);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
