package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.h60;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk1 extends if1 {
    @DexIgnore
    public /* final */ boolean B; // = true;
    @DexIgnore
    public int C;
    @DexIgnore
    public /* final */ h60 D; // = new h60((byte) 26, h60.a.MALE, 170, 65, h60.b.LEFT_WRIST);
    @DexIgnore
    public /* final */ ArrayList<hl1> E; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.DEVICE_CONFIG, hl1.FILE_CONFIG}));

    @DexIgnore
    public mk1(ue1 ue1, q41 q41) {
        super(ue1, q41, eh1.CLEAN_UP_DEVICE, (String) null, 8);
    }

    @DexIgnore
    public static final /* synthetic */ void a(mk1 mk1) {
        int i = mk1.C;
        if (i < 2) {
            mk1.C = i + 1;
            if (fm0.f.b(mk1.x.a())) {
                if1.a((if1) mk1, (if1) new h21(mk1.w, mk1.x, mk1.D, true, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, mk1.z, 32), (hg6) new lb1(mk1), (hg6) new gd1(mk1), (ig6) new bf1(mk1), (hg6) null, (hg6) null, 48, (Object) null);
                return;
            }
            if1.a((if1) mk1, (qv0) new zd1(hk1.ACTIVITY_FILE.a, mk1.w, 0, 4), (hg6) new xg1(mk1), (hg6) new si1(mk1), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            return;
        }
        mk1.m();
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.E;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new d11(this.w), (hg6) new r71(this), (hg6) new p91(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public final void m() {
        qz0.a.a(this.w.t);
        a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
    }

    @DexIgnore
    public boolean b() {
        return this.B;
    }
}
