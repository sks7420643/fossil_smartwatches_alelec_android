package com.fossil;

import com.fossil.li6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mi6<R> extends li6<R>, gg6<R> {

    @DexIgnore
    public interface a<R> extends li6.b<R>, gg6<R> {
    }

    @DexIgnore
    R get();

    @DexIgnore
    Object getDelegate();

    @DexIgnore
    a<R> getGetter();
}
