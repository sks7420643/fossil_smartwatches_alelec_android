package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ h83 a;
    @DexIgnore
    public /* final */ /* synthetic */ l83 b;

    @DexIgnore
    public s83(l83 l83, h83 h83) {
        this.b = l83;
        this.a = h83;
    }

    @DexIgnore
    public final void run() {
        l43 d = this.b.d;
        if (d == null) {
            this.b.b().t().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.a == null) {
                d.a(0, (String) null, (String) null, this.b.c().getPackageName());
            } else {
                d.a(this.a.c, this.a.a, this.a.b, this.b.c().getPackageName());
            }
            this.b.I();
        } catch (RemoteException e) {
            this.b.b().t().a("Failed to send current screen to the service", e);
        }
    }
}
