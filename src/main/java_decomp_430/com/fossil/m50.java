package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m50 extends be0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ArrayList<l50> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<m50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final boolean a(l50 l50, l50 l502) {
            return wg6.a(l50.getFileName(), l502.getFileName()) && !Arrays.equals(l50.getFileData(), l502.getFileData());
        }

        @DexIgnore
        public m50 createFromParcel(Parcel parcel) {
            return new m50(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new m50[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m41createFromParcel(Parcel parcel) {
            return new m50(parcel, (qg6) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<l50, Boolean> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        public Object invoke(Object obj) {
            return Boolean.valueOf(((l50) obj).e());
        }
    }

    @DexIgnore
    public m50(l50 l50, l50 l502, l50 l503, l50 l504, l50 l505) throws IllegalArgumentException {
        l50.a(new dc0(0, 0, 0));
        l502.a(new dc0(0, 62, 1));
        l503.a(new dc0(90, 62, 1));
        l504.a(new dc0(180, 62, 1));
        l505.a(new dc0(270, 62, 1));
        this.a.addAll(qd6.a(new l50[]{l50, l502, l503, l504, l505}));
        e();
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            bm0 bm0 = bm0.BACKGROUND_IMAGE_CONFIG;
            Object[] array = this.a.toArray(new l50[0]);
            if (array != null) {
                cw0.a(jSONObject, bm0, (Object) cw0.a((p40[]) array));
                return jSONObject;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        } catch (JSONException e) {
            qs0.h.a(e);
        }
    }

    @DexIgnore
    public JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        for (l50 f : this.a) {
            jSONArray.put(f.f());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.backgrounds", jSONArray);
        wg6.a(put, "JSONObject().put(UIScrip\u2026oundsAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final ArrayList<l50> d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int size = this.a.size() - 1;
        for (int i = 0; i < size; i++) {
            int size2 = this.a.size();
            int i2 = i;
            while (i2 < size2) {
                a aVar = CREATOR;
                l50 l50 = this.a.get(i);
                wg6.a(l50, "backgroundImageList[i]");
                l50 l502 = this.a.get(i2);
                wg6.a(l502, "backgroundImageList[j]");
                if (!aVar.a(l50, l502)) {
                    i2++;
                } else {
                    StringBuilder b2 = ze0.b("Background images ");
                    b2.append(this.a.get(i).getFileName());
                    b2.append(" and  ");
                    b2.append(this.a.get(i2).getFileName());
                    b2.append(' ');
                    b2.append("have same names but different data.");
                    throw new IllegalArgumentException(b2.toString());
                }
            }
        }
        vd6.a(this.a, b.a);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(m50.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.a, ((m50) obj).a) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImageConfig");
    }

    @DexIgnore
    public final l50 getBottomBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            l50 l50 = (l50) t;
            boolean z = true;
            if (l50.getPositionConfig().getAngle() != 180 || l50.getPositionConfig().getDistanceFromCenter() != 62 || l50.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (l50) t;
    }

    @DexIgnore
    public final l50 getLeftBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            l50 l50 = (l50) t;
            boolean z = true;
            if (l50.getPositionConfig().getAngle() != 270 || l50.getPositionConfig().getDistanceFromCenter() != 62 || l50.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (l50) t;
    }

    @DexIgnore
    public final l50 getMainBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            l50 l50 = (l50) t;
            if (l50.getPositionConfig().getAngle() == 0 && l50.getPositionConfig().getDistanceFromCenter() == 0 && l50.getPositionConfig().getZIndex() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (l50) t;
    }

    @DexIgnore
    public final l50 getRightBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            l50 l50 = (l50) t;
            boolean z = true;
            if (l50.getPositionConfig().getAngle() != 90 || l50.getPositionConfig().getDistanceFromCenter() != 62 || l50.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (l50) t;
    }

    @DexIgnore
    public final l50 getTopBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            l50 l50 = (l50) t;
            boolean z = true;
            if (l50.getPositionConfig().getAngle() != 0 || l50.getPositionConfig().getDistanceFromCenter() != 62 || l50.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (l50) t;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(this.a);
        }
    }

    @DexIgnore
    public /* synthetic */ m50(Parcel parcel, qg6 qg6) {
        super(parcel);
        ArrayList createTypedArrayList = parcel.createTypedArrayList(l50.CREATOR);
        if (createTypedArrayList != null) {
            this.a.addAll(createTypedArrayList);
        }
    }
}
