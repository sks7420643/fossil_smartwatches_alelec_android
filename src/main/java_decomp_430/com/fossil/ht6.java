package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ht6 extends au6 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static ht6 j;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ht6 f;
    @DexIgnore
    public long g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements yt6 {
        @DexIgnore
        public /* final */ /* synthetic */ yt6 a;

        @DexIgnore
        public a(yt6 yt6) {
            this.a = yt6;
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            bu6.a(jt6.b, 0, j);
            while (true) {
                long j2 = 0;
                if (j > 0) {
                    vt6 vt6 = jt6.a;
                    while (true) {
                        if (j2 >= 65536) {
                            break;
                        }
                        j2 += (long) (vt6.c - vt6.b);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        vt6 = vt6.f;
                    }
                    ht6.this.g();
                    try {
                        this.a.a(jt6, j2);
                        j -= j2;
                        ht6.this.a(true);
                    } catch (IOException e) {
                        throw ht6.this.a(e);
                    } catch (Throwable th) {
                        ht6.this.a(false);
                        throw th;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public au6 b() {
            return ht6.this;
        }

        @DexIgnore
        public void close() throws IOException {
            ht6.this.g();
            try {
                this.a.close();
                ht6.this.a(true);
            } catch (IOException e) {
                throw ht6.this.a(e);
            } catch (Throwable th) {
                ht6.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            ht6.this.g();
            try {
                this.a.flush();
                ht6.this.a(true);
            } catch (IOException e) {
                throw ht6.this.a(e);
            } catch (Throwable th) {
                ht6.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Thread {
        @DexIgnore
        public c() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1.i();
         */
        @DexIgnore
        public void run() {
            while (true) {
                try {
                    synchronized (ht6.class) {
                        ht6 j = ht6.j();
                        if (j != null) {
                            if (j == ht6.j) {
                                ht6.j = null;
                                return;
                            }
                        }
                    }
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    @DexIgnore
    public static synchronized void a(ht6 ht6, long j2, boolean z) {
        Class<ht6> cls = ht6.class;
        synchronized (cls) {
            if (j == null) {
                j = new ht6();
                new c().start();
            }
            long nanoTime = System.nanoTime();
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (i2 != 0 && z) {
                ht6.g = Math.min(j2, ht6.c() - nanoTime) + nanoTime;
            } else if (i2 != 0) {
                ht6.g = j2 + nanoTime;
            } else if (z) {
                ht6.g = ht6.c();
            } else {
                throw new AssertionError();
            }
            long b2 = ht6.b(nanoTime);
            ht6 ht62 = j;
            while (true) {
                if (ht62.f == null) {
                    break;
                } else if (b2 < ht62.f.b(nanoTime)) {
                    break;
                } else {
                    ht62 = ht62.f;
                }
            }
            ht6.f = ht62.f;
            ht62.f = ht6;
            if (ht62 == j) {
                cls.notify();
            }
        }
    }

    @DexIgnore
    public static ht6 j() throws InterruptedException {
        Class<ht6> cls = ht6.class;
        ht6 ht6 = j.f;
        if (ht6 == null) {
            long nanoTime = System.nanoTime();
            cls.wait(h);
            if (j.f != null || System.nanoTime() - nanoTime < i) {
                return null;
            }
            return j;
        }
        long b2 = ht6.b(System.nanoTime());
        if (b2 > 0) {
            long j2 = b2 / 1000000;
            cls.wait(j2, (int) (b2 - (1000000 * j2)));
            return null;
        }
        j.f = ht6.f;
        ht6.f = null;
        return ht6;
    }

    @DexIgnore
    public final long b(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final void g() {
        if (!this.e) {
            long f2 = f();
            boolean d = d();
            if (f2 != 0 || d) {
                this.e = true;
                a(this, f2, d);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit");
    }

    @DexIgnore
    public final boolean h() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return a(this);
    }

    @DexIgnore
    public void i() {
    }

    @DexIgnore
    public IOException b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements zt6 {
        @DexIgnore
        public /* final */ /* synthetic */ zt6 a;

        @DexIgnore
        public b(zt6 zt6) {
            this.a = zt6;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            ht6.this.g();
            try {
                long b2 = this.a.b(jt6, j);
                ht6.this.a(true);
                return b2;
            } catch (IOException e) {
                throw ht6.this.a(e);
            } catch (Throwable th) {
                ht6.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            try {
                this.a.close();
                ht6.this.a(true);
            } catch (IOException e) {
                throw ht6.this.a(e);
            } catch (Throwable th) {
                ht6.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.a + ")";
        }

        @DexIgnore
        public au6 b() {
            return ht6.this;
        }
    }

    @DexIgnore
    public static synchronized boolean a(ht6 ht6) {
        synchronized (ht6.class) {
            for (ht6 ht62 = j; ht62 != null; ht62 = ht62.f) {
                if (ht62.f == ht6) {
                    ht62.f = ht6.f;
                    ht6.f = null;
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final yt6 a(yt6 yt6) {
        return new a(yt6);
    }

    @DexIgnore
    public final zt6 a(zt6 zt6) {
        return new b(zt6);
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        if (h() && z) {
            throw b((IOException) null);
        }
    }

    @DexIgnore
    public final IOException a(IOException iOException) throws IOException {
        if (!h()) {
            return iOException;
        }
        return b(iOException);
    }
}
