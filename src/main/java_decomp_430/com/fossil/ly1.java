package com.fossil;

import android.os.Looper;
import android.os.Message;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ly1 extends bb2 {
    @DexIgnore
    public /* final */ /* synthetic */ jy1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ly1(jy1 jy1, Looper looper) {
        super(looper);
        this.a = jy1;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((iy1) message.obj).a(this.a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GACStateManager", sb.toString());
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
