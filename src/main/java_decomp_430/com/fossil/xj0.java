package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj0 extends an1 {
    @DexIgnore
    public /* final */ BluetoothGattDescriptor c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ byte[] g;

    @DexIgnore
    public xj0(BluetoothDevice bluetoothDevice, int i, BluetoothGattDescriptor bluetoothGattDescriptor, boolean z, boolean z2, int i2, byte[] bArr) {
        super(bluetoothDevice, i);
        this.c = bluetoothGattDescriptor;
        this.d = z;
        this.e = z2;
        this.f = i2;
        this.g = bArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000f, code lost:
        r2 = r2.getUuid();
     */
    @DexIgnore
    public JSONObject a() {
        UUID uuid;
        JSONObject a = super.a();
        bm0 bm0 = bm0.CHARACTERISTIC;
        BluetoothGattCharacteristic characteristic = this.c.getCharacteristic();
        String uuid2 = (characteristic == null || uuid == null) ? null : uuid.toString();
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(a, bm0, (Object) uuid2), bm0.DESCRIPTOR, (Object) this.c.getUuid().toString()), bm0.PREPARED_WRITE, (Object) Boolean.valueOf(this.d)), bm0.RESPONSE_NEEDED, (Object) Boolean.valueOf(this.e)), bm0.OFFSET, (Object) Integer.valueOf(this.f)), bm0.VALUE, (Object) cw0.a(this.g, (String) null, 1));
    }
}
