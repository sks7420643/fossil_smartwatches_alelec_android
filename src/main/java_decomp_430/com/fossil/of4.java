package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class of4 extends nf4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = new SparseIntArray();
    @DexIgnore
    public long r;

    /*
    static {
        t.put(2131362602, 1);
        t.put(2131362442, 2);
        t.put(2131362312, 3);
        t.put(2131362575, 4);
        t.put(2131362653, 5);
    }
    */

    @DexIgnore
    public of4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, s, t));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    public of4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[3], objArr[2], objArr[4], objArr[1], objArr[5]);
        this.r = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
