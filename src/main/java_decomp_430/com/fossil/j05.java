package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j05 implements MembersInjector<NotificationWatchRemindersFragment> {
    @DexIgnore
    public static void a(NotificationWatchRemindersFragment notificationWatchRemindersFragment, InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
        notificationWatchRemindersFragment.j = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public static void a(NotificationWatchRemindersFragment notificationWatchRemindersFragment, RemindTimePresenter remindTimePresenter) {
        notificationWatchRemindersFragment.o = remindTimePresenter;
    }
}
