package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y42 implements ThreadFactory {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ThreadFactory b;

    @DexIgnore
    public y42(String str) {
        this(str, 0);
    }

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        Thread newThread = this.b.newThread(new a52(runnable, 0));
        newThread.setName(this.a);
        return newThread;
    }

    @DexIgnore
    public y42(String str, int i) {
        this.b = Executors.defaultThreadFactory();
        w12.a(str, (Object) "Name must not be null");
        this.a = str;
    }
}
