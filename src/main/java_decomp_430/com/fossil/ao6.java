package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ao6<T> {
    @DexIgnore
    public Object[] a; // = new Object[16];
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public final void a(T t) {
        wg6.b(t, "element");
        Object[] objArr = this.a;
        int i = this.c;
        objArr[i] = t;
        this.c = (objArr.length - 1) & (i + 1);
        if (this.c == this.b) {
            a();
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.b == this.c;
    }

    @DexIgnore
    public final T c() {
        int i = this.b;
        if (i == this.c) {
            return null;
        }
        T[] tArr = this.a;
        T t = tArr[i];
        tArr[i] = null;
        this.b = (i + 1) & (tArr.length - 1);
        if (t != null) {
            return t;
        }
        throw new rc6("null cannot be cast to non-null type T");
    }

    @DexIgnore
    public final void a() {
        Object[] objArr = this.a;
        int length = objArr.length;
        Object[] objArr2 = new Object[(length << 1)];
        Object[] objArr3 = objArr2;
        md6.a(objArr, objArr3, 0, this.b, 0, 10, (Object) null);
        Object[] objArr4 = this.a;
        int length2 = objArr4.length;
        int i = this.b;
        md6.a(objArr4, objArr2, length2 - i, 0, i, 4, (Object) null);
        this.a = objArr3;
        this.b = 0;
        this.c = length;
    }
}
