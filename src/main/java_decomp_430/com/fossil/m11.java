package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m11 implements Parcelable.Creator<h31> {
    @DexIgnore
    public /* synthetic */ m11(qg6 qg6) {
    }

    @DexIgnore
    public h31 createFromParcel(Parcel parcel) {
        return new h31(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new h31[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m40createFromParcel(Parcel parcel) {
        return new h31(parcel, (qg6) null);
    }
}
