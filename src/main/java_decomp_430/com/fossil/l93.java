package com.fossil;

import android.app.job.JobParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class l93 implements Runnable {
    @DexIgnore
    public /* final */ j93 a;
    @DexIgnore
    public /* final */ t43 b;
    @DexIgnore
    public /* final */ JobParameters c;

    @DexIgnore
    public l93(j93 j93, t43 t43, JobParameters jobParameters) {
        this.a = j93;
        this.b = t43;
        this.c = jobParameters;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
