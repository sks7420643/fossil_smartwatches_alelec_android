package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RecyclerView w;

    @DexIgnore
    public nd4(Object obj, View view, int i, RTLImageView rTLImageView, RTLImageView rTLImageView2, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ConstraintLayout constraintLayout, RecyclerView recyclerView, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = rTLImageView2;
        this.s = flexibleEditText;
        this.t = flexibleTextView2;
        this.u = flexibleTextView3;
        this.v = constraintLayout;
        this.w = recyclerView;
    }
}
