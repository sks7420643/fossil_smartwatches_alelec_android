package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c53 implements Runnable {
    @DexIgnore
    public /* final */ URL a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ z43 c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Map<String, String> e;
    @DexIgnore
    public /* final */ /* synthetic */ x43 f;

    @DexIgnore
    public c53(x43 x43, String str, URL url, byte[] bArr, Map<String, String> map, z43 z43) {
        this.f = x43;
        w12.b(str);
        w12.a(url);
        w12.a(z43);
        this.a = url;
        this.b = bArr;
        this.c = z43;
        this.d = str;
        this.e = map;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c6 A[SYNTHETIC, Splitter:B:44:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0101 A[SYNTHETIC, Splitter:B:57:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011b  */
    public final void run() {
        Map map;
        IOException iOException;
        int i;
        HttpURLConnection httpURLConnection;
        Map map2;
        this.f.f();
        OutputStream outputStream = null;
        try {
            httpURLConnection = this.f.a(this.a);
            try {
                if (this.e != null) {
                    for (Map.Entry next : this.e.entrySet()) {
                        httpURLConnection.addRequestProperty((String) next.getKey(), (String) next.getValue());
                    }
                }
                if (this.b != null) {
                    byte[] c2 = this.f.n().c(this.b);
                    this.f.b().B().a("Uploading data. size", Integer.valueOf(c2.length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.addRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
                    httpURLConnection.setFixedLengthStreamingMode(c2.length);
                    httpURLConnection.connect();
                    OutputStream outputStream2 = httpURLConnection.getOutputStream();
                    try {
                        outputStream2.write(c2);
                        outputStream2.close();
                    } catch (IOException e2) {
                        map2 = null;
                        iOException = e2;
                        outputStream = outputStream2;
                    } catch (Throwable th) {
                        th = th;
                        map = null;
                        outputStream = outputStream2;
                        i = 0;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, (byte[]) null, map));
                        throw th;
                    }
                }
                i = httpURLConnection.getResponseCode();
                try {
                    map = httpURLConnection.getHeaderFields();
                    try {
                        byte[] a2 = x43.a(httpURLConnection);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, a2, map));
                    } catch (IOException e3) {
                        e = e3;
                        iOException = e;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.f.a().a((Runnable) new d53(this.d, this.c, i, iOException, (byte[]) null, map));
                    } catch (Throwable th2) {
                        th = th2;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, (byte[]) null, map));
                        throw th;
                    }
                } catch (IOException e4) {
                    e = e4;
                    map = null;
                    iOException = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    this.f.a().a((Runnable) new d53(this.d, this.c, i, iOException, (byte[]) null, map));
                } catch (Throwable th3) {
                    th = th3;
                    map = null;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, (byte[]) null, map));
                    throw th;
                }
            } catch (IOException e5) {
                e = e5;
                map2 = null;
                iOException = e;
                i = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                this.f.a().a((Runnable) new d53(this.d, this.c, i, iOException, (byte[]) null, map));
            } catch (Throwable th4) {
                th = th4;
                map = null;
                i = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, (byte[]) null, map));
                throw th;
            }
        } catch (IOException e6) {
            e = e6;
            httpURLConnection = null;
            map2 = null;
            iOException = e;
            i = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e7) {
                    this.f.b().t().a("Error closing HTTP compressed POST connection output stream. appId", t43.a(this.d), e7);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            this.f.a().a((Runnable) new d53(this.d, this.c, i, iOException, (byte[]) null, map));
        } catch (Throwable th5) {
            th = th5;
            httpURLConnection = null;
            map = null;
            i = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e8) {
                    this.f.b().t().a("Error closing HTTP compressed POST connection output stream. appId", t43.a(this.d), e8);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            this.f.a().a((Runnable) new d53(this.d, this.c, i, (Throwable) null, (byte[]) null, map));
            throw th;
        }
    }
}
