package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MisfitButtonState implements Parcelable {
    STATE_SUBSCRIBING(0),
    STATE_SUBSCRIBED(1),
    STATE_UNSUBSCRIBING(2),
    STATE_UNSUBSCRIBED(3),
    STATE_PAIRED(4),
    STATE_UNPAIRED(5),
    STATE_UNKNOWN(-1);
    
    @DexIgnore
    public static /* final */ Parcelable.Creator<MisfitButtonState> CREATOR; // = new Anon1();
    @DexIgnore
    public int mId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MisfitButtonState> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MisfitButtonState createFromParcel(Parcel parcel) {
            return MisfitButtonState.getValue(parcel.readInt());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MisfitButtonState[] newArray(int i) {
            return new MisfitButtonState[i];
        }
    }

    @DexIgnore
    public MisfitButtonState(int i) {
        this.mId = i;
    }

    @DexIgnore
    public static MisfitButtonState getValue(int i) {
        MisfitButtonState[] values = values();
        for (MisfitButtonState misfitButtonState : values) {
            if (misfitButtonState.mId == i) {
                return misfitButtonState;
            }
        }
        return STATE_UNKNOWN;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
    }
}
