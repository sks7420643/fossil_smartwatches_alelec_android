package com.misfit.frameworks.buttonservice.extensions;

import com.fossil.ee7;
import com.fossil.u60;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmExtensionKt {
    @DexIgnore
    public static final AlarmSetting.AlarmDay convertCalendarDayToBleDay(int i) {
        switch (i) {
            case 1:
                return AlarmSetting.AlarmDay.SUNDAY;
            case 2:
                return AlarmSetting.AlarmDay.MONDAY;
            case 3:
                return AlarmSetting.AlarmDay.TUESDAY;
            case 4:
                return AlarmSetting.AlarmDay.WEDNESDAY;
            case 5:
                return AlarmSetting.AlarmDay.THURSDAY;
            case 6:
                return AlarmSetting.AlarmDay.FRIDAY;
            case 7:
                return AlarmSetting.AlarmDay.SATURDAY;
            default:
                FLogger.INSTANCE.getLocal().e("AlarmExtension", "Calendar day isn't correct");
                return AlarmSetting.AlarmDay.MONDAY;
        }
    }

    @DexIgnore
    public static final boolean isSame(List<AlarmSetting> list, List<AlarmSetting> list2) {
        boolean z = true;
        boolean z2 = list == null || list.isEmpty();
        boolean z3 = list2 == null || list2.isEmpty();
        if (z2 || z3) {
            if (!z2 || !z3) {
                return false;
            }
            return true;
        } else if (list != null) {
            int size = list.size();
            if (list2 == null) {
                ee7.a();
                throw null;
            } else if (size != list2.size()) {
                return false;
            } else {
                ArrayList arrayList = new ArrayList(list);
                ArrayList arrayList2 = new ArrayList(list2);
                sortMultiAlarmSettingList(arrayList);
                sortMultiAlarmSettingList(arrayList2);
                int size2 = arrayList.size();
                for (int i = 0; i < size2; i++) {
                    Object obj = arrayList.get(i);
                    ee7.a(obj, "tempSetting1[i]");
                    Object obj2 = arrayList2.get(i);
                    ee7.a(obj2, "tempSetting2[i]");
                    z = ee7.a((Object) ((AlarmSetting) obj).toString(), (Object) ((AlarmSetting) obj2).toString());
                    if (!z) {
                        break;
                    }
                }
                return z;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public static final void sortMultiAlarmSettingList(List<AlarmSetting> list) {
        Collections.sort(list, AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1.INSTANCE);
    }

    @DexIgnore
    public static final ArrayList<AlarmSetting> toBleAlarmSettings(List<? extends Alarm> list) {
        ee7.b(list, "$this$toBleAlarmSettings");
        ArrayList<AlarmSetting> arrayList = new ArrayList<>();
        for (Alarm alarm : list) {
            int[] days = alarm.getDays();
            if ((days != null ? days.length : 0) == 0) {
                int alarmMinute = alarm.getAlarmMinute() / 60;
                String alarmTitle = alarm.getAlarmTitle();
                ee7.a((Object) alarmTitle, "alarm.alarmTitle");
                String alarmMessage = alarm.getAlarmMessage();
                ee7.a((Object) alarmMessage, "alarm.alarmMessage");
                arrayList.add(new AlarmSetting(alarmTitle, alarmMessage, alarmMinute, alarm.getAlarmMinute() - (alarmMinute * 60)));
            } else {
                HashSet hashSet = new HashSet();
                if (days != null) {
                    for (int i : days) {
                        hashSet.add(convertCalendarDayToBleDay(i));
                    }
                    int alarmMinute2 = alarm.getAlarmMinute() / 60;
                    arrayList.add(new AlarmSetting(alarm.getAlarmTitle(), alarm.getAlarmMessage(), alarmMinute2, alarm.getAlarmMinute() - (alarmMinute2 * 60), alarm.isRepeat(), hashSet));
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final u60[] toSDKV2Setting(List<AlarmSetting> list) {
        ee7.b(list, "$this$toSDKV2Setting");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            u60 sDKV2Setting = it.next().toSDKV2Setting();
            if (sDKV2Setting != null) {
                arrayList.add(sDKV2Setting);
            }
        }
        Object[] array = arrayList.toArray(new u60[0]);
        if (array != null) {
            return (u60[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
