package com.misfit.frameworks.buttonservice.extensions;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.i97;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SynchronizeQueue<T> {
    @DexIgnore
    public /* final */ List<T> listSet; // = new ArrayList();
    @DexIgnore
    public /* final */ Object locker; // = new Object();

    @DexIgnore
    public final void add(T t) {
        synchronized (this.locker) {
            this.listSet.add(t);
        }
    }

    @DexIgnore
    public final void addAll(List<T> list) {
        ee7.b(list, "items");
        synchronized (this.locker) {
            this.listSet.addAll(list);
        }
    }

    @DexIgnore
    public final void clear() {
        synchronized (this.locker) {
            this.listSet.clear();
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final int getSize() {
        return this.listSet.size();
    }

    @DexIgnore
    public final boolean isEmpty() {
        boolean isEmpty;
        synchronized (this.locker) {
            isEmpty = this.listSet.isEmpty();
        }
        return isEmpty;
    }

    @DexIgnore
    public final T poll() {
        T t;
        synchronized (this.locker) {
            Iterator<T> it = this.listSet.iterator();
            if (it.hasNext()) {
                t = it.next();
                this.listSet.remove(t);
            } else {
                t = null;
            }
        }
        return t;
    }

    @DexIgnore
    public final void remove(T t) {
        synchronized (this.locker) {
            this.listSet.remove(t);
        }
    }

    @DexIgnore
    public final SynchronizeQueue<T> sortWith(Comparator<? super T> comparator) {
        ee7.b(comparator, "comparator");
        ea7.a((Iterable) this.listSet, (Comparator) comparator);
        return this;
    }
}
