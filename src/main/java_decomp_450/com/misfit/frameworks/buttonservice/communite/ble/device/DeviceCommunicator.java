package com.misfit.frameworks.buttonservice.communite.ble.device;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.facebook.places.model.PlaceFields;
import com.fossil.ac0;
import com.fossil.bb0;
import com.fossil.bc0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.dc0;
import com.fossil.eb0;
import com.fossil.ec0;
import com.fossil.ee7;
import com.fossil.fb0;
import com.fossil.fitness.FitnessData;
import com.fossil.gb0;
import com.fossil.i97;
import com.fossil.ib0;
import com.fossil.ie0;
import com.fossil.jb0;
import com.fossil.kb0;
import com.fossil.l60;
import com.fossil.lb0;
import com.fossil.m90;
import com.fossil.mb0;
import com.fossil.nb0;
import com.fossil.ob0;
import com.fossil.pb0;
import com.fossil.qb0;
import com.fossil.r60;
import com.fossil.rb0;
import com.fossil.tf0;
import com.fossil.x87;
import com.fossil.xb0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ClearLinkMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.NotifyNotificationEventSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBiometricDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoComplicationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoLocalizationDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetComplicationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetFrontLightEnableSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetHeartRateModeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDeviceConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDisplayUnitSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetInactiveNudgeConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetLinkMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMinimumStepThresholdSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetPresetAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetReplyMessageMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWorkoutConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.StopCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.VerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator extends BleCommunicatorAbs {
    @DexIgnore
    public /* final */ HashMap<String, yb0> mDeviceAppRequests; // = new HashMap<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator(Context context, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(new BleAdapterImpl(context, str, str2), context, str, communicationResultCallback);
        ee7.b(context, "context");
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        ee7.b(communicationResultCallback, "communicationResultCallback");
    }

    @DexIgnore
    private final synchronized void sendDeviceAppResponseFromQueue(DeviceAppResponse deviceAppResponse) {
        if (!deviceAppResponse.getLifeTimeObject().isExpire()) {
            if (deviceAppResponse.isForceUpdate()) {
                sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceData());
            } else {
                String name = deviceAppResponse.getDeviceEventId().name();
                yb0 yb0 = this.mDeviceAppRequests.get(name);
                if (yb0 != null) {
                    sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceResponse(yb0, new r60((byte) getBleAdapter().getMicroAppMajorVersion(), (byte) getBleAdapter().getMicroAppMinorVersion())));
                } else {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.BLE;
                    FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
                    String serial = getSerial();
                    String tag = getTAG();
                    String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
                    ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
                    remote.e(component, session, serial, tag, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. deviceAppRequest with key[" + name + "] does not exist.");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag2 = getTAG();
                    local.e(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue(), deviceAppRequest with key[" + name + "] does not exist.");
                }
            }
        }
    }

    @DexIgnore
    private final synchronized void sendDianaNotification(NotificationBaseObj notificationBaseObj) {
        addToQuickCommandQueue(notificationBaseObj);
    }

    @DexIgnore
    private final synchronized void sendMusicResponseFromQueue(MusicResponse musicResponse) {
        ie0<i97> e;
        if (!musicResponse.getLifeCountDownObject().isExpire()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() = " + musicResponse.toString());
            ie0<i97> ie0 = null;
            if (musicResponse instanceof NotifyMusicEventResponse) {
                m90 sDKMusicEvent = ((NotifyMusicEventResponse) musicResponse).toSDKMusicEvent();
                if (sDKMusicEvent != null) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.BLE;
                    FLogger.Session session = FLogger.Session.OTHER;
                    String serial = getSerial();
                    String tag2 = getTAG();
                    remote.d(component, session, serial, tag2, "NotifyMusicEvent: " + musicResponse.toRemoteLogString());
                    ie0 = getBleAdapter().notifyMusicEvent(FLogger.Session.OTHER, sDKMusicEvent);
                } else {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag3 = getTAG();
                    local2.e(tag3, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() fail cause by sdkMusicEvent=" + sDKMusicEvent);
                }
            } else if (musicResponse instanceof MusicTrackInfoResponse) {
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.BLE;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String serial2 = getSerial();
                String tag4 = getTAG();
                remote2.d(component2, session2, serial2, tag4, "SendTrackInfo: " + musicResponse.toRemoteLogString());
                ie0 = getBleAdapter().sendTrackInfo(FLogger.Session.OTHER, ((MusicTrackInfoResponse) musicResponse).toSDKTrackInfo());
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag5 = getTAG();
                local3.d(tag5, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() no support value = " + musicResponse.toRemoteLogString());
            }
            musicResponse.getLifeCountDownObject().goDown();
            if (!(ie0 == null || (e = ie0.c(new DeviceCommunicator$sendMusicResponseFromQueue$Anon1(this, musicResponse))) == null)) {
                e.b(new DeviceCommunicator$sendMusicResponseFromQueue$Anon2(this, musicResponse));
            }
        }
    }

    @DexIgnore
    private final synchronized void sendNotificationFromQueue(NotificationBaseObj notificationBaseObj) {
        ie0<i97> e;
        if (!notificationBaseObj.getLifeCountDownObject().isExpire()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, " .sendNotificationFromQueue() = " + notificationBaseObj + ", serial=" + getSerial());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.d(component, session, serial, tag2, "Send notification: " + notificationBaseObj.toRemoteLogString());
            ie0<i97> sendNotification = getBleAdapter().sendNotification(FLogger.Session.OTHER, notificationBaseObj);
            notificationBaseObj.getLifeCountDownObject().goDown();
            if (!(sendNotification == null || (e = sendNotification.c(new DeviceCommunicator$sendNotificationFromQueue$Anon1(this, notificationBaseObj))) == null)) {
                e.b(new DeviceCommunicator$sendNotificationFromQueue$Anon2(this, notificationBaseObj));
            }
        } else {
            getCommunicationResultCallback().onNotificationSent(notificationBaseObj.getUid(), false);
        }
    }

    @DexIgnore
    private final void sendResponseToWatch(DeviceAppResponse deviceAppResponse, cf0 cf0) {
        ie0<i97> e;
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = getSerial();
        String tag = getTAG();
        remote.i(component, session, serial, tag, "Send respond: " + deviceAppResponse);
        if (cf0 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local.d(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + deviceAppResponse.toString());
            ie0<i97> sendRespond = getBleAdapter().sendRespond(FLogger.Session.HANDLE_WATCH_REQUEST, cf0);
            if (sendRespond != null && (e = sendRespond.c(new DeviceCommunicator$sendResponseToWatch$Anon1(this, deviceAppResponse))) != null) {
                e.b(new DeviceCommunicator$sendResponseToWatch$Anon2(this, deviceAppResponse));
                return;
            }
            return;
        }
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial2 = getSerial();
        String tag3 = getTAG();
        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
        remote2.e(component2, session2, serial2, tag3, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. device request type is not match with device response");
        FLogger.INSTANCE.getLocal().e(getTAG(), " .sendDeviceAppResponseFromQueue(), device request type is not match with device response or could not get DeviceData from DeviceAppResponse when forceUpdate");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void confirmStopWorkout(String str, boolean z) {
        ee7.b(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IReadWorkoutStateSession) {
            ((IReadWorkoutStateSession) currentSession).confirmStopWorkout(str, z);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".confirmStopWorkout() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.i(component, session, str, tag2, "confirmStopWorkout FAILED, currentSession=" + currentSession);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean disableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean enableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public List<FitnessData> getSyncData() {
        if (getCurrentSession() instanceof SyncSession) {
            log("Current session is Sync Session, start get sync data.");
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                return ((SyncSession) currentSession).getSyncData();
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession");
        }
        log("Current session is not Sync Session, empty sync data.");
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void handleEventReceived(l60 l60, bb0 bb0) {
        ee7.b(l60, "device");
        ee7.b(bb0, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleEventReceived, device = " + l60 + ".deviceInformation.serialNumber, event = " + bb0);
        if (bb0 instanceof yb0) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.i(component, session, serial, tag2, "Receive device request, device=" + getSerial() + ", device event=" + bb0);
            this.mDeviceAppRequests.put(bb0.getDeviceEventId().name(), bb0);
            Bundle bundle = new Bundle();
            if (bb0 instanceof dc0) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((dc0) bb0).getAction().ordinal());
            } else if (bb0 instanceof xb0) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, tf0.START.ordinal());
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((xb0) bb0).getDestination());
            } else if (bb0 instanceof rb0) {
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((rb0) bb0).getChallengeId());
            } else if (bb0 instanceof ec0) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, bb0);
            } else if (bb0 instanceof bc0) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, bb0);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(bb0.getDeviceEventId().ordinal(), bundle, getSerial());
        } else if (bb0 instanceof kb0) {
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.BLE;
            FLogger.Session session2 = FLogger.Session.OTHER;
            String serial2 = getSerial();
            String tag3 = getTAG();
            remote2.i(component2, session2, serial2, tag3, "Receive device notification, device=" + getSerial() + ", event=" + bb0);
            Bundle bundle2 = new Bundle();
            if (bb0 instanceof mb0) {
                bundle2.putInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.Companion.fromSDKMusicAction(((mb0) bb0).getAction()).ordinal());
            } else if (bb0 instanceof nb0) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((nb0) bb0).getAction().ordinal());
            } else if (bb0 instanceof eb0) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((eb0) bb0).getAction().ordinal());
            } else if (bb0 instanceof jb0) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((jb0) bb0).getAction().ordinal());
            } else if (bb0 instanceof fb0) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, cb0.APP_NOTIFICATION_CONTROL.ordinal());
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, bb0);
            } else if (bb0 instanceof ib0) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((ib0) bb0).getAction().ordinal());
            } else if (bb0 instanceof lb0) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, new EncryptedData((lb0) bb0));
            } else if (bb0 instanceof ob0) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((ob0) bb0).getSessionId());
            } else if (bb0 instanceof pb0) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((pb0) bb0).getSessionId());
            } else if (bb0 instanceof qb0) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, bb0);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(bb0.getDeviceEventId().ordinal(), bundle2, getSerial());
        } else if (bb0 instanceof ac0) {
            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
            FLogger.Component component3 = FLogger.Component.BLE;
            FLogger.Session session3 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial3 = getSerial();
            String tag4 = getTAG();
            remote3.i(component3, session3, serial3, tag4, "Receive micro app request, device=" + getSerial() + ", event=" + bb0);
            this.mDeviceAppRequests.put(bb0.getDeviceEventId().name(), bb0);
            getCommunicationResultCallback().onDeviceAppsRequest(bb0.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else if (bb0 instanceof gb0) {
            IRemoteFLogger remote4 = FLogger.INSTANCE.getRemote();
            FLogger.Component component4 = FLogger.Component.BLE;
            FLogger.Session session4 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial4 = getSerial();
            String tag5 = getTAG();
            remote4.i(component4, session4, serial4, tag5, "Receive authentication request notification, device=" + getSerial() + ", event=" + bb0);
            getCommunicationResultCallback().onDeviceAppsRequest(bb0.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else {
            super.handleEventReceived(l60, bb0);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onPing() {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onPing();
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onPing() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onPing FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void onQuickCommandAction(Object obj) {
        ee7.b(obj, Constants.COMMAND);
        if (obj instanceof DeviceAppResponse) {
            sendDeviceAppResponseFromQueue((DeviceAppResponse) obj);
        } else if (obj instanceof MusicResponse) {
            sendMusicResponseFromQueue((MusicResponse) obj);
        } else if (obj instanceof NotificationBaseObj) {
            sendNotificationFromQueue((NotificationBaseObj) obj);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveCurrentSecretKey(byte[] bArr) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveCurrentSecretKey(bArr);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveCurrentSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveCurrentSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceivePushSecretKeyResponse(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IVerifySecretKeySession) {
            ((IVerifySecretKeySession) currentSession).onReceiveServerResponse(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceivePushSecretKeyResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceivePushSecretKeyResponse FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerRandomKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveRandomKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerRandomKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerRandomKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerSecretKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveServerSecretKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) {
        ee7.b(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (!(currentSession instanceof ISetWatchParamStateSession)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onSetWatchParamResponse() FAILED, current session=" + currentSession);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String tag2 = getTAG();
            remote.i(component, session, str, tag2, "onSetWatchParamResponse FAILED, currentSession=" + currentSession);
        } else if (!z) {
            ((ISetWatchParamStateSession) currentSession).onGetWatchParamFailed();
        } else if (watchParamsFileMapping == null) {
            ((ISetWatchParamStateSession) currentSession).doNextState();
        } else {
            ((ISetWatchParamStateSession) currentSession).setLatestWatchParam(str, watchParamsFileMapping);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean sendingEncryptedDataSession(byte[] bArr, boolean z) {
        ee7.b(bArr, "encryptedData");
        SharePreferencesUtils.getInstance(getBleAdapter().getContext()).setBoolean(SharePreferencesUtils.BC_STATUS, z);
        queueSessionAndStart(new SendingEncryptedDataSession(bArr, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setMinimumStepThresholdSession(long j) {
        queueSessionAndStart(new SetMinimumStepThresholdSession(j, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setSecretKey(byte[] bArr) {
        BleAdapterImpl mBleAdapter = getMBleAdapter();
        FLogger.Session session = FLogger.Session.OTHER;
        if (bArr != null) {
            mBleAdapter.setSecretKey(session, bArr);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutConfigSession(WorkoutConfigData workoutConfigData) {
        ee7.b(workoutConfigData, "workoutConfigData");
        queueSessionAndStart(new SetWorkoutConfigSession(workoutConfigData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutGPSData(Location location) {
        ee7.b(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .setWorkoutGPSData() = " + location.toString());
        getBleAdapter().setWorkoutGPSData(location, FLogger.Session.WORKOUT_TETHER_GPS);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCalibrationSession() {
        queueSessionAndStart(new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCleanLinkMappingSession(List<? extends BLEMapping> list) {
        ee7.b(list, "mappings");
        return queueSessionAndStart(new ClearLinkMappingSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startConnectionDeviceSession(boolean z, UserProfile userProfile) {
        queueSessionAndStart(new ConnectDeviceSession(z, userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startInstallWatchApps(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISetWatchAppFileSession) {
            ((ISetWatchAppFileSession) currentSession).onWatchAppFilesReady(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onWatchAppFilesReady() FAILED, current session=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startNotifyNotificationEvent(NotificationBaseObj notificationBaseObj) {
        ee7.b(notificationBaseObj, "notifyNotificationEvent");
        queueSessionAndStart(new NotifyNotificationEventSession(notificationBaseObj, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile) {
        ee7.b(firmwareData, "firmwareData");
        ee7.b(userProfile, "userProfile");
        queueSessionAndStart(new OtaSession(firmwareData, userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startPairingSession(UserProfile userProfile) {
        ee7.b(userProfile, "userProfile");
        queueSessionAndStart(new PairingNewDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startReadCurrentWorkoutSession() {
        queueSessionAndStart(new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z) {
        ee7.b(deviceAppResponse, "deviceAppResponse");
        deviceAppResponse.getLifeTimeObject().startExpireTimeCountDown();
        deviceAppResponse.setForceUpdate(z);
        addToQuickCommandQueue(deviceAppResponse);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public synchronized void startSendMusicAppResponse(MusicResponse musicResponse) {
        ee7.b(musicResponse, "musicResponse");
        addToQuickCommandQueue(musicResponse);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        ee7.b(notificationBaseObj, "newNotification");
        sendDianaNotification(notificationBaseObj);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        ee7.b(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetAutoBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBiometricData(UserBiometricData userBiometricData) {
        ee7.b(userBiometricData, "userBiometricData");
        queueSessionAndStart(new SetAutoBiometricDataSession(userBiometricData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings) {
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        queueSessionAndStart(new SetAutoComplicationSession(complicationAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMapping(List<? extends BLEMapping> list) {
        ee7.b(list, "mappings");
        return queueSessionAndStart(new SetAutoMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        ee7.b(list, "multipleAlarmList");
        queueSessionAndStart(new SetAutoMultiAlarmsSession(list, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetAutoNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoSecondTimezone(String str) {
        ee7.b(str, "secondTimezoneId");
        return queueSessionAndStart(new SetAutoSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetAutoWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        ee7.b(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings) {
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        queueSessionAndStart(new SetComplicationSession(complicationAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetFrontLightEnable(boolean z) {
        queueSessionAndStart(new SetFrontLightEnableSession(z, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetHeartRateMode(HeartRateMode heartRateMode) {
        ee7.b(heartRateMode, "heartRateMode");
        queueSessionAndStart(new SetHeartRateModeSession(heartRateMode, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDeviceConfig(UserProfile userProfile) {
        ee7.b(userProfile, "userProfile");
        queueSessionAndStart(new SetImplicitDeviceConfigSession(userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit) {
        ee7.b(userDisplayUnit, "userDisplayUnit");
        queueSessionAndStart(new SetImplicitDisplayUnitSession(userDisplayUnit, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData) {
        ee7.b(inactiveNudgeData, "inactiveNudgeData");
        queueSessionAndStart(new SetInactiveNudgeConfigSession(inactiveNudgeData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLinkMappingSession(List<? extends BLEMapping> list) {
        ee7.b(list, "mappings");
        return queueSessionAndStart(new SetLinkMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        ee7.b(localizationData, "localizationData");
        queueSessionAndStart(new SetAutoLocalizationDataSession(localizationData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        ee7.b(list, "multipleAlarmList");
        queueSessionAndStart(new SetMultiAlarmsSession(getBleAdapter(), list, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig) {
        queueSessionAndStart(new SetPresetAppsSession(complicationAppMappingSettings, watchAppMappingSettings, backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetReplyMessageMappingSettings(ReplyMessageMappingGroup replyMessageMappingGroup) {
        ee7.b(replyMessageMappingGroup, "replyMessageMappings");
        queueSessionAndStart(new SetReplyMessageMappingSession(replyMessageMappingGroup, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetSecondTimezoneSession(String str) {
        ee7.b(str, "secondTimezoneId");
        return queueSessionAndStart(new SetSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startStopCurrentWorkoutSession() {
        queueSessionAndStart(new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSwitchDeviceSession(UserProfile userProfile) {
        ee7.b(userProfile, "userProfile");
        queueSessionAndStart(new SwitchActiveDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSyncingSession(UserProfile userProfile) {
        ee7.b(userProfile, "userProfile");
        queueSessionAndStart(new SyncSession(getBleAdapter(), getBleSessionCallback(), userProfile));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startVerifySecretKeySession() {
        return queueSessionAndStart(new VerifySecretKeySession(getBleAdapter(), getBleSessionCallback()));
    }
}
