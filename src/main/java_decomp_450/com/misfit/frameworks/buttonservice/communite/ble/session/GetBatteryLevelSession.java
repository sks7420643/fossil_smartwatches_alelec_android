package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b80;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetBatteryLevelSession extends EnableMaintainingSession {
    @DexIgnore
    public int mBatteryLevel; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetBatteryLevelState extends BleStateAbs {
        @DexIgnore
        public ie0<HashMap<o80, n80>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetBatteryLevelState() {
            super(GetBatteryLevelSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            GetBatteryLevelSession.this.log("Get Battery Level");
            ie0<HashMap<o80, n80>> deviceConfig = GetBatteryLevelSession.this.getBleAdapter().getDeviceConfig(GetBatteryLevelSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                GetBatteryLevelSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
            getBatteryLevelSession.mBatteryLevel = getBatteryLevelSession.getBleAdapter().getBatteryLevel();
            GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
            getBatteryLevelSession2.log("Get Battery Level Failed, return the old one:" + GetBatteryLevelSession.this.mBatteryLevel);
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<o80, n80> hashMap) {
            ee7.b(hashMap, "deviceConfiguration");
            stopTimeout();
            if (hashMap.containsKey(o80.BATTERY)) {
                GetBatteryLevelSession getBatteryLevelSession = GetBatteryLevelSession.this;
                n80 n80 = hashMap.get(o80.BATTERY);
                if (n80 != null) {
                    getBatteryLevelSession.mBatteryLevel = ((b80) n80).getPercentage();
                    GetBatteryLevelSession getBatteryLevelSession2 = GetBatteryLevelSession.this;
                    getBatteryLevelSession2.log("Get Battery Level Success, value=" + GetBatteryLevelSession.this.mBatteryLevel);
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
                }
            } else {
                GetBatteryLevelSession.this.log("Get Battery Level Success, but no value.");
            }
            GetBatteryLevelSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<HashMap<o80, n80>> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetBatteryLevelSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.GET_BATTERY_LEVEL, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt(Constants.BATTERY, this.mBatteryLevel);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        GetBatteryLevelSession getBatteryLevelSession = new GetBatteryLevelSession(getBleAdapter(), getBleSessionCallback());
        getBatteryLevelSession.setDevice(getDevice());
        return getBatteryLevelSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_BATTERY_LEVEL_STATE;
        String name = GetBatteryLevelState.class.getName();
        ee7.a((Object) name, "GetBatteryLevelState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
