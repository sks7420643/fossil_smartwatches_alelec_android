package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.w80;
import com.fossil.we7;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetVibrationStrengthSession extends EnableMaintainingSession {
    @DexIgnore
    public w80.a mVibrationStrengthLevel; // = w80.a.MEDIUM;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public ie0<HashMap<o80, n80>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetVibrationStrengthState() {
            super(GetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<o80, n80> hashMap) {
            GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
            we7 we7 = we7.a;
            String format = String.format("Get configuration  " + GetVibrationStrengthSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(new Object[]{hashMap.get(o80.TIME), hashMap.get(o80.BATTERY), hashMap.get(o80.BIOMETRIC_PROFILE), hashMap.get(o80.DAILY_STEP), hashMap.get(o80.DAILY_STEP_GOAL), hashMap.get(o80.DAILY_CALORIE), hashMap.get(o80.DAILY_CALORIE_GOAL), hashMap.get(o80.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(o80.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(o80.DAILY_DISTANCE), hashMap.get(o80.INACTIVE_NUDGE), hashMap.get(o80.VIBE_STRENGTH), hashMap.get(o80.DO_NOT_DISTURB_SCHEDULE)}, 13));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            getVibrationStrengthSession.log(format);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            GetVibrationStrengthSession.this.log("Get Vibration Strength Level");
            ie0<HashMap<o80, n80>> deviceConfig = GetVibrationStrengthSession.this.getBleAdapter().getDeviceConfig(GetVibrationStrengthSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                GetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            GetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_GET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<o80, n80> hashMap) {
            ee7.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(o80.VIBE_STRENGTH)) {
                GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
                n80 n80 = hashMap.get(o80.VIBE_STRENGTH);
                if (n80 != null) {
                    getVibrationStrengthSession.mVibrationStrengthLevel = ((w80) n80).getVibeStrengthLevel();
                    GetVibrationStrengthSession getVibrationStrengthSession2 = GetVibrationStrengthSession.this;
                    getVibrationStrengthSession2.log("Get Vibration Strength Level Success, value=" + GetVibrationStrengthSession.this.mVibrationStrengthLevel);
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                GetVibrationStrengthSession.this.log("Get Vibration Strength Level Success, but no value.");
            }
            GetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<HashMap<o80, n80>> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetVibrationStrengthSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(ButtonService.Companion.getVIBRATION_STRENGTH_LEVEL(), VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, this.mVibrationStrengthLevel, false, 2, null));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        GetVibrationStrengthSession getVibrationStrengthSession = new GetVibrationStrengthSession(getBleAdapter(), getBleSessionCallback());
        getVibrationStrengthSession.setDevice(getDevice());
        return getVibrationStrengthSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE;
        String name = GetVibrationStrengthState.class.getName();
        ee7.a((Object) name, "GetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
