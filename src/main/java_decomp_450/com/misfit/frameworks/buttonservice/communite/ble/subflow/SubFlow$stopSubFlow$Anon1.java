package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$stopSubFlow$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$Anon1(SubFlow subFlow, int i, fb7 fb7) {
        super(2, fb7);
        this.this$0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SubFlow$stopSubFlow$Anon1 subFlow$stopSubFlow$Anon1 = new SubFlow$stopSubFlow$Anon1(this.this$0, this.$failureCode, fb7);
        subFlow$stopSubFlow$Anon1.p$ = (yi7) obj;
        return subFlow$stopSubFlow$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((SubFlow$stopSubFlow$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            if (!BleState.Companion.isNull(this.this$0.getMCurrentState())) {
                this.this$0.getMCurrentState().stopTimeout();
                boolean unused = this.this$0.enterSubState(new NullBleState(this.this$0.getTAG()));
            }
            this.this$0.onStop(this.$failureCode);
            this.this$0.setExist(false);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
