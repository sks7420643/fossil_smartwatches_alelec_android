package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.bb0;
import com.fossil.ee7;
import com.fossil.l60;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicatorAbs$mStateCallback$Anon1 implements l60.b {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public BleCommunicatorAbs$mStateCallback$Anon1(BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    @Override // com.fossil.l60.b
    public void onDeviceStateChanged(l60 l60, l60.c cVar, l60.c cVar2) {
        ee7.b(l60, "device");
        ee7.b(cVar, "previousState");
        ee7.b(cVar2, "newState");
        this.this$0.handleDeviceStateChanged(l60, cVar, cVar2);
    }

    @DexIgnore
    @Override // com.fossil.l60.b
    public void onEventReceived(l60 l60, bb0 bb0) {
        ee7.b(l60, "device");
        ee7.b(bb0, Constants.EVENT);
        this.this$0.handleEventReceived(l60, bb0);
    }
}
