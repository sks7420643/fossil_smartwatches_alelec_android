package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.he0;
import com.fossil.i97;
import com.fossil.jg0;
import com.fossil.ke0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoMappingsSession extends SetAutoSettingsSession {
    @DexIgnore
    public List<? extends MicroAppMapping> mappings;
    @DexIgnore
    public List<? extends MicroAppMapping> oldMappings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoMappingState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneSetAutoMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoMappingsSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class WatchSetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public he0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public WatchSetMicroAppMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetAutoMappingsSession.this.getContext(), SetAutoMappingsSession.this.getSerial())) {
                SetAutoMappingsSession.this.log("Reach the limit retry. Stop.");
                SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
                setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, true);
                SetAutoMappingsSession.this.stop(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppSuccess() {
            SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
            setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, false);
            SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
            setAutoMappingsSession2.enterState(setAutoMappingsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (!SetAutoMappingsSession.this.mappings.isEmpty()) {
                BleAdapterImpl access$getBleAdapter$p = SetAutoMappingsSession.this.getBleAdapter();
                FLogger.Session access$getLogSession$p = SetAutoMappingsSession.this.getLogSession();
                List<jg0> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(SetAutoMappingsSession.this.mappings);
                ee7.a((Object) convertToSDKMapping, "MicroAppMapping.convertToSDKMapping(mappings)");
                he0<i97> configureMicroApp = access$getBleAdapter$p.configureMicroApp(access$getLogSession$p, convertToSDKMapping, this);
                this.task = configureMicroApp;
                if (configureMicroApp == null) {
                    SetAutoMappingsSession.this.stop(10000);
                } else {
                    startTimeout();
                }
            } else {
                SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
                setAutoMappingsSession.log("Micro app mapping is empty, size=" + SetAutoMappingsSession.this.mappings.size());
                SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
                setAutoMappingsSession2.enterStateAsync(setAutoMappingsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            }
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            he0<i97> he0 = this.task;
            if (he0 != null) {
                he0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoMappingsSession(List<? extends BLEMapping> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_MAPPING, bleAdapterImpl, bleSessionCallback);
        ee7.b(list, "microAppMappings");
        ee7.b(bleAdapterImpl, "bleAdapter");
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(list);
        ee7.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
        this.mappings = convertToMicroAppMapping;
    }

    @DexIgnore
    private final void storeMappings(List<? extends MicroAppMapping> list, boolean z) {
        DevicePreferenceUtils.setAutoSetMapping(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a(list));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        List<BLEMapping> convertToBLEMapping = MicroAppMapping.convertToBLEMapping(this.mappings);
        ee7.a((Object) convertToBLEMapping, "MicroAppMapping.convertToBLEMapping(mappings)");
        SetAutoMappingsSession setAutoMappingsSession = new SetAutoMappingsSession(convertToBLEMapping, getBleAdapter(), getBleSessionCallback());
        setAutoMappingsSession.setDevice(getDevice());
        return setAutoMappingsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        if (getBleAdapter().isDeviceReady()) {
            List<? extends MicroAppMapping> list = this.oldMappings;
            if (list == null) {
                ee7.d("oldMappings");
                throw null;
            } else if (BLEMapping.isMappingTheSame(list, this.mappings)) {
                log("New mappings and old mappings are the same. No need to set again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                List<? extends MicroAppMapping> list2 = this.mappings;
                List<? extends MicroAppMapping> list3 = this.oldMappings;
                if (list3 == null) {
                    ee7.d("oldMappings");
                    throw null;
                } else if (BLEMapping.compareTimeStamp(list2, list3).longValue() > 0) {
                    storeMappings(this.mappings, true);
                    return createConcreteState(BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE);
                } else {
                    log("Old mappings timestamp is greater then the new one. No need to set again.");
                    return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                }
            }
        } else {
            storeMappings(this.mappings, true);
            log("Device is not ready. Cannot set mapping.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getBleAdapter().getContext(), getBleAdapter().getSerial()));
        ee7.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertT\u2026text, bleAdapter.serial))");
        this.oldMappings = convertToMicroAppMapping;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE;
        String name = WatchSetMicroAppMappingState.class.getName();
        ee7.a((Object) name, "WatchSetMicroAppMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoMappingState.class.getName();
        ee7.a((Object) name2, "DoneSetAutoMappingState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
