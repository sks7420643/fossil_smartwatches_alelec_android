package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.he0;
import com.fossil.hg0;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.buttonservice.utils.WatchAppUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SendingEncryptedDataSession extends EnableMaintainingSession {
    @DexIgnore
    public boolean isBcOn;
    @DexIgnore
    public /* final */ byte[] mEncryptedData;
    @DexIgnore
    public SharePreferencesUtils shared;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SendingEncryptedDataState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SendingEncryptedDataState() {
            super(SendingEncryptedDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<i97> sendingEncryptedData = SendingEncryptedDataSession.this.getBleAdapter().sendingEncryptedData(SendingEncryptedDataSession.this.mEncryptedData, SendingEncryptedDataSession.this.getLogSession(), this);
            this.task = sendingEncryptedData;
            if (sendingEncryptedData == null) {
                SendingEncryptedDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSendingEncryptedDataSessionFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SENDING_ENCRYPTED_DATA_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSendingEncryptedDataSessionSuccess() {
            stopTimeout();
            SendingEncryptedDataSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppFiles extends BleStateAbs {
        @DexIgnore
        public he0<hg0[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppFiles() {
            super(SendingEncryptedDataSession.this.getTAG());
        }

        @DexIgnore
        private final hg0[] prepareWatchAppFiles() {
            return WatchAppUtils.INSTANCE.getWatchAppFiles(SendingEncryptedDataSession.this.getBleAdapter().getContext());
        }

        @DexIgnore
        private final BleState sendingEncryptedDataState() {
            return SendingEncryptedDataSession.this.createConcreteState(BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:0x005f, code lost:
            if ((r0.length == 0) != false) goto L_0x0061;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0064  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0084  */
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onEnter() {
            /*
                r9 = this;
                super.onEnter()
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.String r1 = "Enter Set Watch App Files"
                r0.log(r1)
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.String r0 = r0.getSerial()
                boolean r0 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r0)
                r1 = 1
                if (r0 == 0) goto L_0x008c
                com.fossil.hg0[] r0 = r9.prepareWatchAppFiles()
                r2 = 0
                r3 = 0
                if (r0 == 0) goto L_0x0035
                int r4 = r0.length
                r5 = 0
            L_0x0021:
                if (r5 >= r4) goto L_0x0035
                r6 = r0[r5]
                java.lang.String r7 = r6.getBundleId()
                java.lang.String r8 = "buddyChallengeApp"
                boolean r7 = com.fossil.nh7.a(r7, r8, r1)
                if (r7 == 0) goto L_0x0032
                goto L_0x0036
            L_0x0032:
                int r5 = r5 + 1
                goto L_0x0021
            L_0x0035:
                r6 = r3
            L_0x0036:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r4 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                boolean r4 = r4.isBcOn
                if (r4 == 0) goto L_0x0041
                if (r6 != 0) goto L_0x0041
                r0 = r3
            L_0x0041:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r3 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "Enter Set Watch App Files - watchApps: "
                r4.append(r5)
                r4.append(r0)
                java.lang.String r4 = r4.toString()
                r3.log(r4)
                if (r0 == 0) goto L_0x0061
                int r3 = r0.length
                if (r3 != 0) goto L_0x005e
                r3 = 1
                goto L_0x005f
            L_0x005e:
                r3 = 0
            L_0x005f:
                if (r3 == 0) goto L_0x0062
            L_0x0061:
                r2 = 1
            L_0x0062:
                if (r2 != 0) goto L_0x0084
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r2 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r2 = r2.getBleAdapter()
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r3 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.log.FLogger$Session r3 = r3.getLogSession()
                com.fossil.he0 r0 = r2.setWatchAppFiles(r3, r0, r9)
                r9.task = r0
                if (r0 != 0) goto L_0x0080
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                r2 = 10000(0x2710, float:1.4013E-41)
                r0.stop(r2)
                goto L_0x0095
            L_0x0080:
                r9.startTimeout()
                goto L_0x0095
            L_0x0084:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                r2 = 1935(0x78f, float:2.712E-42)
                r0.stop(r2)
                goto L_0x0095
            L_0x008c:
                com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.this
                com.misfit.frameworks.buttonservice.communite.ble.BleState r2 = r9.sendingEncryptedDataState()
                r0.enterSendingEncryptedState(r2)
            L_0x0095:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession.SetWatchAppFiles.onEnter():boolean");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileFailed() {
            stopTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files Failed");
            SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileSuccess() {
            stopTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files success");
            SendingEncryptedDataSession.access$getShared$p(SendingEncryptedDataSession.this).setBoolean(SharePreferencesUtils.BC_STATUS, true);
            SharePreferencesUtils access$getShared$p = SendingEncryptedDataSession.access$getShared$p(SendingEncryptedDataSession.this);
            access$getShared$p.setBoolean(SharePreferencesUtils.BC_APP_READY + SendingEncryptedDataSession.this.getSerial(), true);
            SendingEncryptedDataSession.this.enterSendingEncryptedState(sendingEncryptedDataState());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SendingEncryptedDataSession.this.log("Set Watch App Files Timeout");
            he0<hg0[]> he0 = this.task;
            if (he0 == null) {
                SendingEncryptedDataSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SendingEncryptedDataSession(byte[] bArr, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.SENDING_ENCRYPTED_DATA_SESSION, bleAdapterImpl, bleSessionCallback);
        ee7.b(bArr, "mEncryptedData");
        ee7.b(bleAdapterImpl, "bleAdapterV2");
        this.mEncryptedData = bArr;
    }

    @DexIgnore
    public static final /* synthetic */ SharePreferencesUtils access$getShared$p(SendingEncryptedDataSession sendingEncryptedDataSession) {
        SharePreferencesUtils sharePreferencesUtils = sendingEncryptedDataSession.shared;
        if (sharePreferencesUtils != null) {
            return sharePreferencesUtils;
        }
        ee7.d("shared");
        throw null;
    }

    @DexIgnore
    private final void enterSendingEncryptedState(BleState bleState) {
        enterStateAsync(bleState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SendingEncryptedDataSession sendingEncryptedDataSession = new SendingEncryptedDataSession(this.mEncryptedData, getBleAdapter(), getBleSessionCallback());
        sendingEncryptedDataSession.setDevice(getDevice());
        return sendingEncryptedDataSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        SharePreferencesUtils sharePreferencesUtils = this.shared;
        if (sharePreferencesUtils != null) {
            Boolean bool = sharePreferencesUtils.getBoolean(SharePreferencesUtils.BC_APP_READY + getSerial(), false);
            log("stateAfterEnableMaintainingConnection - isBcOn: " + this.isBcOn + " - bcAppReady: " + bool);
            if (this.isBcOn && !bool.booleanValue()) {
                return createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
            }
            getSessionStateMap().remove(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
            return createConcreteState(BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE);
        }
        ee7.d("shared");
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        SharePreferencesUtils instance = SharePreferencesUtils.getInstance(getContext());
        ee7.a((Object) instance, "SharePreferencesUtils.getInstance(context)");
        this.shared = instance;
        if (instance != null) {
            Boolean bool = instance.getBoolean(SharePreferencesUtils.BC_STATUS, true);
            ee7.a((Object) bool, "shared.getBoolean(ShareP\u2026cesUtils.BC_STATUS, true)");
            this.isBcOn = bool.booleanValue();
            SharePreferencesUtils sharePreferencesUtils = this.shared;
            if (sharePreferencesUtils != null) {
                Boolean bool2 = sharePreferencesUtils.getBoolean(SharePreferencesUtils.BC_APP_READY + getSerial(), false);
                log("initStateMap - isBcOn: " + this.isBcOn + " - bcAppReady: " + bool2);
                if (this.isBcOn && !bool2.booleanValue()) {
                    HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
                    BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
                    String name = SetWatchAppFiles.class.getName();
                    ee7.a((Object) name, "SetWatchAppFiles::class.java.name");
                    sessionStateMap.put(sessionState, name);
                }
                HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
                BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SENDING_ENCRYPTED_DATA_STATE;
                String name2 = SendingEncryptedDataState.class.getName();
                ee7.a((Object) name2, "SendingEncryptedDataState::class.java.name");
                sessionStateMap2.put(sessionState2, name2);
                return;
            }
            ee7.d("shared");
            throw null;
        }
        ee7.d("shared");
        throw null;
    }
}
