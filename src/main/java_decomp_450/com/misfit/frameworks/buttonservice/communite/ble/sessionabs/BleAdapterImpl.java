package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.ab0;
import com.fossil.ad0;
import com.fossil.ae0;
import com.fossil.b80;
import com.fossil.bd0;
import com.fossil.blesdk.device.feature.CustomCommandFeature;
import com.fossil.c80;
import com.fossil.c90;
import com.fossil.cd0;
import com.fossil.ce0;
import com.fossil.cf0;
import com.fossil.dd0;
import com.fossil.de0;
import com.fossil.ed0;
import com.fossil.ee0;
import com.fossil.ee7;
import com.fossil.eg0;
import com.fossil.fd0;
import com.fossil.fe0;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.fitness.GpsDataProvider;
import com.fossil.g60;
import com.fossil.gd0;
import com.fossil.gd7;
import com.fossil.ge0;
import com.fossil.hd0;
import com.fossil.he0;
import com.fossil.hg0;
import com.fossil.i70;
import com.fossil.i97;
import com.fossil.id0;
import com.fossil.ie0;
import com.fossil.j70;
import com.fossil.jd0;
import com.fossil.je0;
import com.fossil.jg0;
import com.fossil.k70;
import com.fossil.kc0;
import com.fossil.kd0;
import com.fossil.l60;
import com.fossil.l70;
import com.fossil.lc0;
import com.fossil.ld0;
import com.fossil.m60;
import com.fossil.m70;
import com.fossil.m90;
import com.fossil.mc0;
import com.fossil.md0;
import com.fossil.n80;
import com.fossil.n90;
import com.fossil.nc0;
import com.fossil.nd0;
import com.fossil.o80;
import com.fossil.o90;
import com.fossil.oc0;
import com.fossil.od0;
import com.fossil.qc0;
import com.fossil.qd0;
import com.fossil.r60;
import com.fossil.r80;
import com.fossil.rc0;
import com.fossil.s60;
import com.fossil.sc0;
import com.fossil.sd0;
import com.fossil.tc0;
import com.fossil.td0;
import com.fossil.ud0;
import com.fossil.vc0;
import com.fossil.vd0;
import com.fossil.w80;
import com.fossil.wd0;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.xd0;
import com.fossil.y90;
import com.fossil.zc0;
import com.fossil.zd0;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.extensions.LocationExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.DianaCalibrationObj;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl extends BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public l60 deviceObj;
    @DexIgnore
    public boolean isScanning;
    @DexIgnore
    public HashMap<o80, n80> mDeviceConfiguration;
    @DexIgnore
    public l60.b mDeviceStateListener;
    @DexIgnore
    public ScanService scanService;
    @DexIgnore
    public byte[] tSecretKey;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class BleScanServiceCallback implements ScanService.Callback {
        @DexIgnore
        public /* final */ ISessionSdkCallback callback;

        @DexIgnore
        public BleScanServiceCallback(ISessionSdkCallback iSessionSdkCallback) {
            this.callback = iSessionSdkCallback;
        }

        @DexIgnore
        public final ISessionSdkCallback getCallback() {
            return this.callback;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onDeviceFound(l60 l60, int i) {
            ee7.b(l60, "device");
            if (l60.getState() == l60.c.CONNECTED || l60.getState() == l60.c.UPGRADING_FIRMWARE) {
                i = 0;
            } else if (l60.getBondState() == l60.a.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onDeviceFound(l60, i);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onScanFail(g60 g60) {
            ee7.b(g60, "scanError");
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onScanFail(g60);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[l60.c.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[l60.c.DISCONNECTED.ordinal()] = 1;
            $EnumSwitchMapping$0[l60.c.CONNECTED.ordinal()] = 2;
            $EnumSwitchMapping$0[l60.c.UPGRADING_FIRMWARE.ordinal()] = 3;
            $EnumSwitchMapping$0[l60.c.CONNECTING.ordinal()] = 4;
            $EnumSwitchMapping$0[l60.c.DISCONNECTING.ordinal()] = 5;
        }
        */
    }

    /*
    static {
        String simpleName = BleAdapterImpl.class.getSimpleName();
        ee7.a((Object) simpleName, "BleAdapterImpl::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl(Context context, String str, String str2) {
        super(context, str, str2);
        ee7.b(context, "context");
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
    }

    @DexIgnore
    private final void requestPairing(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "requestPairing(), serial=" + getSerial());
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            id0 id0 = (id0) l60.a(id0.class);
            if (id0 == null || id0.e().c(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1(this, iSessionSdkCallback, session)).b(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon2(this, iSessionSdkCallback, session)) == null) {
                log(session, "Device[" + getSerial() + "] does not support RequestPairingFeature");
                iSessionSdkCallback.onAuthorizeDeviceFailed();
                i97 i97 = i97.a;
                return;
            }
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final l60 buildDeviceBySerial(String str, String str2, long j) {
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        if (this.scanService == null) {
            this.scanService = new ScanService(getContext(), null, j);
        }
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            return scanService2.buildDeviceBySerial(str, str2);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> calibrationApplyHandPosition(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Apply Hands Positions");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            qd0 qd0 = (qd0) l60.a(qd0.class);
            if (qd0 != null) {
                return qd0.s().c(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingCalibrationPositionFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> calibrationMoveHand(FLogger.Session session, HandCalibrationObj handCalibrationObj, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(handCalibrationObj, "handCalibrationObj");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        DianaCalibrationObj consume = DianaCalibrationObj.Companion.consume(handCalibrationObj);
        j70[] j70Arr = {new j70(consume.getHandId(), consume.getDegree(), consume.getHandMovingDirection(), consume.getHandMovingSpeed())};
        log(session, "Move Hand: handMovingType=" + consume.getHandMovingType() + ", handMovingConfigs=" + j70Arr + ' ');
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            cd0 cd0 = (cd0) l60.a(cd0.class);
            if (cd0 != null) {
                return cd0.a(consume.getHandMovingType(), j70Arr).c(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon1(this, consume, j70Arr, session, iSessionSdkCallback)).b(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon2(this, consume, j70Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> calibrationReleaseHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Release Hand Control");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            hd0 hd0 = (hd0) l60.a(hd0.class);
            if (hd0 != null) {
                return hd0.i().c(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReleasingHandsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> calibrationRequestHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Request Hand Control");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            jd0 jd0 = (jd0) l60.a(jd0.class);
            if (jd0 != null) {
                return jd0.j().c(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support RequestingHandsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> calibrationResetHandToZeroDegree(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Reset Hands to 0 degrees.");
        j70[] j70Arr = {new j70(i70.HOUR, 0, k70.SHORTEST_PATH, l70.FULL), new j70(i70.MINUTE, 0, k70.SHORTEST_PATH, l70.FULL), new j70(i70.SUB_EYE, 0, k70.SHORTEST_PATH, l70.FULL)};
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            cd0 cd0 = (cd0) l60.a(cd0.class);
            if (cd0 != null) {
                return cd0.a(m70.POSITION, j70Arr).c(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon1(this, j70Arr, session, iSessionSdkCallback)).b(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon2(this, j70Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void clearCache(FLogger.Session session) {
        ee7.b(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Clear cache, serial=" + getSerial());
        l60 l60 = this.deviceObj;
        if (l60 == null || l60.k() == null) {
            log(session, "clearCache() failed, deviceObj is null.");
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public void closeConnection(FLogger.Session session, boolean z) {
        ee7.b(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Close Connection, serial=" + getSerial());
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            qc0 qc0 = (qc0) l60.a(qc0.class);
            if (qc0 != null) {
                qc0.a();
                log(session, "Close Connection, serial=" + getSerial() + ", OK");
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support DisconnectingFeature");
            return;
        }
        logAppError(session, "closeConnection failed. DeviceObject is null. Connect has not been established.", ErrorCodeBuilder.Step.DISCONNECT, ErrorCodeBuilder.AppError.UNKNOWN);
    }

    @DexIgnore
    public final he0<i97> configureMicroApp(FLogger.Session session, List<jg0> list, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(list, "mappings");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Configure MicroApp, mapping=" + list);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            nc0 nc0 = (nc0) l60.a(nc0.class);
            if (nc0 != null) {
                Object[] array = list.toArray(new jg0[0]);
                if (array != null) {
                    return nc0.a((jg0[]) array).e((gd7<? super i97, i97>) new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support ConfiguringMicroAppFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> confirmAuthorization(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "confirmAuthorization(), timeout=" + j);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            kc0 kc0 = (kc0) l60.a(kc0.class);
            if (kc0 != null) {
                return kc0.a(j).c(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).b(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support AuthorizationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final he0<String> doOTA(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(bArr, "firmwareData");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do OTA");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            fd0 fd0 = (fd0) l60.a(fd0.class);
            if (fd0 != null) {
                he0<String> a = fd0.a(bArr, null);
                a.g(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback));
                return a.e((gd7<? super String, i97>) new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon3(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support OTAFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final Boolean enableMaintainConnection(FLogger.Session session) {
        ee7.b(session, "logSession");
        log(session, "Enable Maintain Connection");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            rc0 rc0 = (rc0) l60.a(rc0.class);
            if (rc0 != null) {
                return Boolean.valueOf(rc0.g());
            }
            log(session, "Device[" + getSerial() + "] does not support EnablingMaintainConnectionFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> eraseDataFile(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Erase DataFiles");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            mc0 mc0 = (mc0) l60.a(mc0.class);
            if (mc0 != null) {
                return mc0.cleanUp().e((gd7<? super i97, i97>) new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support CleanUpDeviceFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void error(FLogger.Session session, String str, ErrorCodeBuilder.Step step, String str2) {
        ee7.b(session, "logSession");
        ee7.b(str, "errorCode");
        ee7.b(step, "step");
        ee7.b(str2, "errorMessage");
        MFLog activeLog = MFLogManager.getInstance(getContext()).getActiveLog(getSerial());
        if (activeLog != null) {
            activeLog.error('[' + getSerial() + "] " + str2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, '[' + getSerial() + "] error=" + str + ' ' + str2);
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, session, getSerial(), TAG, str, step, str2);
    }

    @DexIgnore
    public final ie0<byte[]> exchangeSecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        ie0<byte[]> d;
        ee7.b(session, "logSession");
        ee7.b(bArr, "data");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Exchange Secret Key");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            sc0 sc0 = (sc0) l60.a(sc0.class);
            if (sc0 != null && (d = sc0.d(bArr).c(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).b(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support ExchangingSecretKeyFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<m60> fetchDeviceInfo(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Fetch Device Information");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            tc0 tc0 = (tc0) l60.a(tc0.class);
            if (tc0 != null) {
                return tc0.c().e((gd7<? super m60, i97>) new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support FetchingDeviceInformationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getBatteryLevel() {
        HashMap<o80, n80> hashMap = this.mDeviceConfiguration;
        if (hashMap == null || !hashMap.containsKey(o80.BATTERY)) {
            return -1;
        }
        n80 n80 = hashMap.get(o80.BATTERY);
        if (n80 != null) {
            return ((b80) n80).getPercentage();
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentLabelVersion() {
        m60 n;
        s60 eLabelVersions;
        r60 currentVersion;
        String shortDescription;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (eLabelVersions = n.getELabelVersions()) == null || (currentVersion = eLabelVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentWatchParamVersion() {
        m60 n;
        s60 watchParameterVersions;
        r60 currentVersion;
        String shortDescription;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (watchParameterVersions = n.getWatchParameterVersions()) == null || (currentVersion = watchParameterVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    public final ie0<HashMap<o80, n80>> getDeviceConfig(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Device Configuration");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            vc0 vc0 = (vc0) l60.a(vc0.class);
            if (vc0 != null) {
                return vc0.m().e((gd7<? super HashMap<o80, n80>, i97>) new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support GettingConfigurationsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getDeviceModel() {
        m60 n;
        String modelNumber;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (modelNumber = n.getModelNumber()) == null) ? "" : modelNumber;
    }

    @DexIgnore
    public final l60 getDeviceObj() {
        return this.deviceObj;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getFirmwareVersion() {
        m60 n;
        String firmwareVersion;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (firmwareVersion = n.getFirmwareVersion()) == null) ? "" : firmwareVersion;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getGattState() {
        return getGattState(this.deviceObj);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public HeartRateMode getHeartRateMode() {
        HeartRateMode heartRateMode;
        HashMap<o80, n80> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(o80.HEART_RATE_MODE)) {
                HeartRateMode.Companion companion = HeartRateMode.Companion;
                n80 n80 = hashMap.get(o80.HEART_RATE_MODE);
                if (n80 != null) {
                    heartRateMode = companion.consume(((r80) n80).getHeartRateMode());
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
                }
            } else {
                heartRateMode = HeartRateMode.NONE;
            }
            if (heartRateMode != null) {
                return heartRateMode;
            }
        }
        return HeartRateMode.NONE;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getHidState() {
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocale() {
        m60 n;
        String localeString;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (localeString = n.getLocaleString()) == null) ? "" : localeString;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocaleVersion() {
        m60 n;
        s60 localeVersions;
        l60 l60 = this.deviceObj;
        return String.valueOf((l60 == null || (n = l60.n()) == null || (localeVersions = n.getLocaleVersions()) == null) ? null : localeVersions.getCurrentVersion());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMajorVersion() {
        m60 n;
        r60 microAppVersion;
        l60 l60 = this.deviceObj;
        if (l60 == null || (n = l60.n()) == null || (microAppVersion = n.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMinorVersion() {
        m60 n;
        r60 microAppVersion;
        l60 l60 = this.deviceObj;
        if (l60 == null || (n = l60.n()) == null || (microAppVersion = n.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMinor();
    }

    @DexIgnore
    public final byte[] getSecretKeyThroughSDK(FLogger.Session session) {
        ee7.b(session, "logSession");
        log(session, "Get Secret Key Through SDK");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            zc0 zc0 = (zc0) l60.a(zc0.class);
            if (zc0 != null) {
                setTSecretKey(zc0.f());
                log(session, "Secret key from SDK " + getTSecretKey());
                return getTSecretKey();
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public byte getSupportedWatchParamMajor() {
        m60 n;
        s60 watchParameterVersions;
        r60 supportedVersion;
        l60 l60 = this.deviceObj;
        if (l60 == null || (n = l60.n()) == null || (watchParameterVersions = n.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public byte getSupportedWatchParamMinor() {
        m60 n;
        s60 watchParameterVersions;
        r60 supportedVersion;
        l60 l60 = this.deviceObj;
        if (l60 == null || (n = l60.n()) == null || (watchParameterVersions = n.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMinor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getSupportedWatchParamVersion() {
        m60 n;
        s60 watchParameterVersions;
        r60 supportedVersion;
        String shortDescription;
        l60 l60 = this.deviceObj;
        return (l60 == null || (n = l60.n()) == null || (watchParameterVersions = n.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null || (shortDescription = supportedVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public byte[] getTSecretKey() {
        return this.tSecretKey;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public r60 getUiPackageOSVersion() {
        m60 n;
        l60 l60 = this.deviceObj;
        if (l60 == null || (n = l60.n()) == null) {
            return null;
        }
        return n.getUiPackageOSVersion();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public VibrationStrengthObj getVibrationStrength() {
        VibrationStrengthObj vibrationStrengthObj;
        HashMap<o80, n80> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(o80.VIBE_STRENGTH)) {
                n80 n80 = hashMap.get(o80.VIBE_STRENGTH);
                if (n80 != null) {
                    w80.a vibeStrengthLevel = ((w80) n80).getVibeStrengthLevel();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "Device config not null, vibeStrengthConfig not null, return vibrationStrength: value = " + vibeStrengthLevel);
                    vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, vibeStrengthLevel, false, 2, null);
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Device config not null but vibeStrengthConfig is null, vibrationStrength return default value MEDIUM");
                vibrationStrengthObj = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(w80.a.MEDIUM, true);
            }
            if (vibrationStrengthObj != null) {
                return vibrationStrengthObj;
            }
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Device config null, vibrationStrength return default value MEDIUM");
        return VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(w80.a.MEDIUM, true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public boolean isDeviceReady() {
        l60 l60 = this.deviceObj;
        boolean z = false;
        if (l60 != null) {
            if (l60.getState() == l60.c.CONNECTED) {
                z = true;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".isDeviceReady() - serial=");
            sb.append(getSerial());
            sb.append(", state=");
            sb.append(l60.getState());
            sb.append(", ready=");
            sb.append(!z ? "NO" : "YES");
            local.d(str, sb.toString());
            return z;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.e(str2, "Is device ready " + getSerial() + ". FAILED: deviceObject is NULL");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public <T> T isSupportedFeature(Class<T> cls) {
        ee7.b(cls, "feature");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            return (T) l60.a(cls);
        }
        return null;
    }

    @DexIgnore
    public final void logAppError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        ee7.b(session, "logSession");
        ee7.b(str, "message");
        ee7.b(step, "step");
        ee7.b(appError, "error");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", appError=" + appError);
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, appError), step, str);
    }

    @DexIgnore
    public final void logSdkError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, je0 je0) {
        ee7.b(session, "logSession");
        ee7.b(str, "message");
        ee7.b(step, "step");
        ee7.b(je0, "sdkError");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", sdkError=" + je0.getErrorCode());
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, je0), step, str);
    }

    @DexIgnore
    public final ie0<i97> notifyMusicEvent(FLogger.Session session, m90 m90) {
        ee7.b(session, "logSession");
        ee7.b(m90, "musicEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Notify Music Event, musicEvent=" + m90);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Notify Music Event");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ed0 ed0 = (ed0) l60.a(ed0.class);
            if (ed0 != null) {
                return ed0.a(m90);
            }
            log(session, "Device[" + getSerial() + "] does not support NotifyingMusicEventFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> notifyNotificationEvent(FLogger.Session session, NotificationBaseObj notificationBaseObj, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(notificationBaseObj, "notifyNotificationEvent");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do notifyNotificationEvent");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            dd0 dd0 = (dd0) l60.a(dd0.class);
            DianaNotificationObj dianaNotificationObj = (DianaNotificationObj) notificationBaseObj;
            if (dd0 != null) {
                return dd0.a(dianaNotificationObj.toSDKNotifyNotificationEvent()).c(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon1(this, dianaNotificationObj, session, iSessionSdkCallback)).b(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon2(this, dianaNotificationObj, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support notifyNotificationEvent");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> playDeviceAnimation(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Play Device Animation");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            bd0 bd0 = (bd0) l60.a(bd0.class);
            if (bd0 != null) {
                return bd0.b().c(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support HandAnimationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<ab0> readCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ie0<ab0> d;
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Workout Session");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ad0 ad0 = (ad0) l60.a(ad0.class);
            if (ad0 != null && (d = ad0.p().c(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingWorkoutSessionFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final he0<FitnessData[]> readDataFile(FLogger.Session session, c80 c80, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(c80, "biometricProfile");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read DataFiles");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            fe0 fe0 = (fe0) l60.a(fe0.class);
            if (fe0 != null) {
                he0<FitnessData[]> a = fe0.a(c80);
                a.g(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(this, c80, session, iSessionSdkCallback));
                return a.e((gd7<? super FitnessData[], i97>) new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(this, c80, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon3(this, c80, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SynchronizationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<Integer> readRssi(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read Rssi");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            gd0 gd0 = (gd0) l60.a(gd0.class);
            if (gd0 != null) {
                return gd0.h().c(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReadingRSSIFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void registerBluetoothStateCallback(l60.b bVar) {
        ee7.b(bVar, Constants.CALLBACK);
        this.mDeviceStateListener = bVar;
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            l60.a(bVar);
        }
    }

    @DexIgnore
    public final void sendCustomCommand(CustomRequest customRequest) {
        ie0 sendCustomCommand$default;
        ie0 e;
        ee7.b(customRequest, Constants.COMMAND);
        FLogger.Session session = FLogger.Session.OTHER;
        log(session, "Send Custom Command: " + customRequest);
        l60 l60 = this.deviceObj;
        if (!(l60 instanceof oc0)) {
            l60 = null;
        }
        oc0 oc0 = (oc0) l60;
        if (oc0 == null || (sendCustomCommand$default = CustomCommandFeature.DefaultImpls.sendCustomCommand$default(oc0, customRequest.getCustomCommand(), c90.DC, 0, 0, 12, (Object) null)) == null || (e = sendCustomCommand$default.c(new BleAdapterImpl$sendCustomCommand$Anon1(this, customRequest))) == null || e.b(new BleAdapterImpl$sendCustomCommand$Anon2(this)) == null) {
            log(FLogger.Session.OTHER, "Send Custom Command not support");
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final ie0<i97> sendNotification(FLogger.Session session, NotificationBaseObj notificationBaseObj) {
        ee7.b(session, "logSession");
        ee7.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Notification, notification=" + notificationBaseObj);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Notification");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            kd0 kd0 = (kd0) l60.a(kd0.class);
            if (kd0 != null) {
                o90 sDKNotification = notificationBaseObj.toSDKNotification();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "send sdk notification " + sDKNotification.toString());
                return kd0.a(sDKNotification);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingAppNotificationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> sendRespond(FLogger.Session session, cf0 cf0) {
        ee7.b(session, "logSession");
        ee7.b(cf0, "deviceData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Respond, deviceData=" + cf0);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Respond");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ld0 ld0 = (ld0) l60.a(ld0.class);
            if (ld0 != null) {
                return ld0.a(cf0);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingDataFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> sendTrackInfo(FLogger.Session session, n90 n90) {
        ee7.b(session, "logSession");
        ee7.b(n90, "trackInfo");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Track Info, trackInfo=" + n90);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Track Info");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            nd0 nd0 = (nd0) l60.a(nd0.class);
            if (nd0 != null) {
                return nd0.a(n90);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingTrackInfoFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> sendingEncryptedData(byte[] bArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(bArr, "encryptedData");
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "sendingEncryptedData");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            md0 md0 = (md0) l60.a(md0.class);
            if (md0 != null) {
                return md0.c(bArr).e((gd7<? super i97, i97>) new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SendingEncryptedDataFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setAlarms(FLogger.Session session, List<AlarmSetting> list, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(list, "alarms");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set List Alarms: " + list);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            od0 od0 = (od0) l60.a(od0.class);
            if (od0 != null) {
                return od0.a(AlarmExtensionKt.toSDKV2Setting(list)).e((gd7<? super i97, i97>) new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingAlarmFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setBackgroundImage(FLogger.Session session, BackgroundConfig backgroundConfig, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(backgroundConfig, "backgroundConfig");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Background Image: " + backgroundConfig);
        String directory = FileUtils.getDirectory(getContext(), FileType.WATCH_FACE);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            xd0 xd0 = (xd0) l60.a(xd0.class);
            if (xd0 != null) {
                ee7.a((Object) directory, "directory");
                return xd0.a(backgroundConfig.toSDKBackgroundImageConfig(directory)).e((gd7<? super i97, i97>) new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(this, backgroundConfig, directory, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(this, backgroundConfig, directory, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - set BackgroundImageFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setComplications(FLogger.Session session, ComplicationAppMappingSettings complicationAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Complication: " + complicationAppMappingSettings);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            xd0 xd0 = (xd0) l60.a(xd0.class);
            boolean goalRingEnable = DevicePreferenceUtils.getGoalRingEnable(getContext());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "setComplication, isRingEnabled=" + goalRingEnable);
            if (xd0 != null) {
                return xd0.a(complicationAppMappingSettings.toSDKSetting(goalRingEnable)).e((gd7<? super i97, i97>) new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon1(this, complicationAppMappingSettings, goalRingEnable, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setComplications$$inlined$let$lambda$Anon2(this, complicationAppMappingSettings, goalRingEnable, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setComplications");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final boolean setDevice(l60 l60) {
        ee7.b(l60, "deviceObj");
        setDeviceObj(l60);
        return true;
    }

    @DexIgnore
    public final ie0<o80[]> setDeviceConfig(FLogger.Session session, n80[] n80Arr, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(n80Arr, "deviceConfigItems");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Device Config: " + new Gson().a(n80Arr));
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            sd0 sd0 = (sd0) l60.a(sd0.class);
            if (sd0 != null) {
                return sd0.a(n80Arr).e((gd7<? super o80[], i97>) new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(this, n80Arr, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon2(this, n80Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingConfigurationsFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void setDeviceObj(l60 l60) {
        this.deviceObj = l60;
        if (l60 != null) {
            if (!TextUtils.isEmpty(l60.n().getMacAddress())) {
                setMacAddress(l60.n().getMacAddress());
            }
            l60.a(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final ie0<i97> setFrontLightEnable(FLogger.Session session, boolean z, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Front Light Enable: " + z);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ud0 ud0 = (ud0) l60.a(ud0.class);
            if (ud0 != null) {
                return ud0.a(z).e((gd7<? super i97, i97>) new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon1(this, z, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon2(this, z, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingFrontLightFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final he0<r60> setLabelFile(FLogger.Session session, eg0 eg0, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(eg0, "labelFile");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setLabelFile File: " + eg0);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            td0 td0 = (td0) l60.a(td0.class);
            if (td0 != null) {
                return td0.a(eg0).e((gd7<? super r60, i97>) new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon1(this, eg0, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon2(this, eg0, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support setLabelFiles");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final he0<String> setLocalizationData(LocalizationData localizationData, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(localizationData, "localizationData");
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Localization");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            vd0 vd0 = (vd0) l60.a(vd0.class);
            if (vd0 != null) {
                return vd0.a(localizationData.toLocalizationFile()).e((gd7<? super String, i97>) new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon1(this, localizationData, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon2(this, localizationData, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setMinimumStepThreshold(long j, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setMinimumStepThreshold");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            lc0 lc0 = (lc0) l60.a(lc0.class);
            if (lc0 != null) {
                return lc0.b(j).e((gd7<? super i97, i97>) new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support BuddyChallengeFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final he0<i97> setNotificationFilters(FLogger.Session session, List<AppNotificationFilter> list, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(list, "notificationFilters");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKNotificationFilter(getContext()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Set Notification Filter: " + list + " bleNotifications " + arrayList);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Set Notification Filter");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            wd0 wd0 = (wd0) l60.a(wd0.class);
            if (wd0 != null) {
                Object[] array = arrayList.toArray(new y90[0]);
                if (array != null) {
                    he0<i97> a = wd0.a((y90[]) array);
                    a.g(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(this, arrayList, session, iSessionSdkCallback));
                    return a.e((gd7<? super i97, i97>) new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon2(this, arrayList, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon3(this, arrayList, session, iSessionSdkCallback));
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0126, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0127, code lost:
        com.fossil.hc7.a(r15, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x012b, code lost:
        throw r0;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.he0<com.fossil.i97> setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger.Session r18, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup r19, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback r20) {
        /*
            r17 = this;
            r9 = r17
            r0 = r18
            java.lang.String r1 = "logSession"
            com.fossil.ee7.b(r0, r1)
            java.lang.String r1 = "replyMessageGroup"
            r10 = r19
            com.fossil.ee7.b(r10, r1)
            java.lang.String r1 = "callback"
            r11 = r20
            com.fossil.ee7.b(r11, r1)
            com.fossil.l60 r1 = r9.deviceObj
            r12 = 0
            if (r1 == 0) goto L_0x012c
            java.lang.Class<com.fossil.yd0> r2 = com.fossil.yd0.class
            java.lang.Object r1 = r1.a(r2)
            r13 = r1
            com.fossil.yd0 r13 = (com.fossil.yd0) r13
            com.fossil.se7 r14 = new com.fossil.se7
            r14.<init>()
            android.content.Context r1 = r17.getContext()
            android.content.res.AssetManager r1 = r1.getAssets()
            java.lang.String r2 = r19.getIconFwPath()
            java.io.InputStream r15 = r1.open(r2)
            com.fossil.da0 r1 = new com.fossil.da0     // Catch:{ all -> 0x0123 }
            java.lang.String r2 = r19.getIconFwPath()     // Catch:{ all -> 0x0123 }
            java.lang.String r3 = "it"
            com.fossil.ee7.a(r15, r3)     // Catch:{ all -> 0x0123 }
            byte[] r3 = com.fossil.gc7.a(r15)     // Catch:{ all -> 0x0123 }
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0123 }
            r14.element = r1     // Catch:{ all -> 0x0123 }
            java.util.List r1 = r19.getReplyMessageList()     // Catch:{ all -> 0x0123 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0123 }
            r3 = 10
            int r3 = com.fossil.x97.a(r1, r3)     // Catch:{ all -> 0x0123 }
            r2.<init>(r3)     // Catch:{ all -> 0x0123 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0123 }
        L_0x0061:
            boolean r3 = r1.hasNext()     // Catch:{ all -> 0x0123 }
            if (r3 == 0) goto L_0x0097
            java.lang.Object r3 = r1.next()     // Catch:{ all -> 0x0123 }
            com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping r3 = (com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping) r3     // Catch:{ all -> 0x0123 }
            java.lang.String r4 = r3.getId()     // Catch:{ all -> 0x0123 }
            if (r4 == 0) goto L_0x0093
            byte r4 = java.lang.Byte.parseByte(r4)     // Catch:{ all -> 0x0123 }
            java.lang.String r3 = r3.getMessage()     // Catch:{ all -> 0x0123 }
            if (r3 == 0) goto L_0x007e
            goto L_0x0080
        L_0x007e:
            java.lang.String r3 = ""
        L_0x0080:
            T r5 = r14.element     // Catch:{ all -> 0x0123 }
            com.fossil.da0 r5 = (com.fossil.da0) r5     // Catch:{ all -> 0x0123 }
            if (r5 == 0) goto L_0x008f
            com.fossil.x90 r6 = new com.fossil.x90     // Catch:{ all -> 0x0123 }
            r6.<init>(r4, r3, r5)     // Catch:{ all -> 0x0123 }
            r2.add(r6)     // Catch:{ all -> 0x0123 }
            goto L_0x0061
        L_0x008f:
            com.fossil.ee7.a()     // Catch:{ all -> 0x0123 }
            throw r12
        L_0x0093:
            com.fossil.ee7.a()
            throw r12
        L_0x0097:
            r1 = 0
            com.fossil.x90[] r1 = new com.fossil.x90[r1]
            java.lang.Object[] r1 = r2.toArray(r1)
            if (r1 == 0) goto L_0x011b
            com.fossil.x90[] r1 = (com.fossil.x90[]) r1
            com.fossil.ug0 r2 = new com.fossil.ug0
            r2.<init>(r1)
            com.fossil.tg0 r1 = new com.fossil.tg0
            r1.<init>()
            com.fossil.ba0 r3 = com.fossil.ba0.INCOMING_CALL
            r1.a(r3, r2)
            com.fossil.ba0 r3 = com.fossil.ba0.TEXT
            r1.a(r3, r2)
            com.fossil.ba0 r3 = com.fossil.ba0.MISSED_CALL
            r1.a(r3, r2)
            com.fossil.vg0[] r8 = r1.a()
            if (r13 == 0) goto L_0x00f9
            com.fossil.he0 r7 = r13.a(r8)
            com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1 r6 = new com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1
            r1 = r6
            r2 = r8
            r3 = r17
            r4 = r14
            r5 = r19
            r12 = r6
            r6 = r13
            r10 = r7
            r7 = r18
            r16 = r8
            r8 = r20
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            com.fossil.he0 r10 = r10.e(r12)
            com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2 r12 = new com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2
            r1 = r12
            r2 = r16
            r3 = r17
            r4 = r14
            r5 = r19
            r6 = r13
            r7 = r18
            r8 = r20
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            com.fossil.he0 r0 = r10.b(r12)
            r1 = 0
            com.fossil.hc7.a(r15, r1)
            return r0
        L_0x00f9:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Device["
            r1.append(r2)
            java.lang.String r2 = r17.getSerial()
            r1.append(r2)
            java.lang.String r2 = "] does not support SettingReplyMessageFeature"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.log(r0, r1)
            r0 = 0
            com.fossil.hc7.a(r15, r0)
            return r0
        L_0x011b:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        L_0x0123:
            r0 = move-exception
            r1 = r0
            throw r1     // Catch:{ all -> 0x0126 }
        L_0x0126:
            r0 = move-exception
            r2 = r0
            com.fossil.hc7.a(r15, r1)
            throw r2
        L_0x012c:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl.setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger$Session, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback):com.fossil.he0");
    }

    @DexIgnore
    public final void setSecretKey(FLogger.Session session, byte[] bArr) {
        ee7.b(session, "logSession");
        ee7.b(bArr, "secretKey");
        log(session, "Set Secret Key");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            zd0 zd0 = (zd0) l60.a(zd0.class);
            if (zd0 != null) {
                zd0.e(bArr);
                setTSecretKey(bArr);
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Set Secret Key. FAILED: deviceObject is NULL");
    }

    @DexIgnore
    public void setTSecretKey(byte[] bArr) {
        this.tSecretKey = bArr;
    }

    @DexIgnore
    public final he0<hg0[]> setWatchAppFiles(FLogger.Session session, hg0[] hg0Arr, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(hg0Arr, "watchAppFiles");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch App Files: " + hg0Arr);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ae0 ae0 = (ae0) l60.a(ae0.class);
            if (ae0 != null) {
                he0<hg0[]> d = ae0.a(hg0Arr).e((gd7<? super hg0[], i97>) new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon1(this, hg0Arr, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon2(this, hg0Arr, session, iSessionSdkCallback));
                d.g(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon3(this, hg0Arr, session, iSessionSdkCallback));
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support SettingUpWatchAppFeature - setWatchAppFiles");
            iSessionSdkCallback.onSetWatchAppFileFailed();
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setWatchApps(FLogger.Session session, WatchAppMappingSettings watchAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch Apps: " + watchAppMappingSettings);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            xd0 xd0 = (xd0) l60.a(xd0.class);
            if (xd0 != null) {
                return xd0.a(watchAppMappingSettings.toSDKSetting()).e((gd7<? super i97, i97>) new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon1(this, watchAppMappingSettings, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon2(this, watchAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setWatchApps");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ie0<i97> setWatchParams(FLogger.Session session, WatchParamsFileMapping watchParamsFileMapping, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(watchParamsFileMapping, "watchParamFileMapping");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set WatchParams: " + watchParamsFileMapping);
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ce0 ce0 = (ce0) l60.a(ce0.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Setting watchParam with file value = " + watchParamsFileMapping.toSDKSetting());
            if (ce0 != null) {
                return ce0.a(watchParamsFileMapping.toSDKSetting()).e((gd7<? super i97, i97>) new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(this, watchParamsFileMapping, session, iSessionSdkCallback)).b((gd7<? super je0, i97>) new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon2(this, watchParamsFileMapping, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchParameterFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void setWorkoutGPSData(Location location, FLogger.Session session) {
        ee7.b(location, "data");
        ee7.b(session, "logSession");
        GpsDataPoint gpsDataPoint = LocationExtensionKt.toGpsDataPoint(location);
        GpsDataProvider.defaultProvider().onLocationChanged(gpsDataPoint);
        log(session, "setWorkoutGPSData dataPoint " + gpsDataPoint);
    }

    @DexIgnore
    public final ie0<byte[]> startAuthenticate(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(bArr, "phoneRandomNumber");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Start Authenticate phoneRandomNumber " + ConversionUtils.INSTANCE.byteArrayToString(bArr));
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Start Authenticate phoneRandomNumber");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            de0 de0 = (de0) l60.a(de0.class);
            if (de0 != null) {
                return de0.a(bArr).c(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).b(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StartingAuthenticateFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void startScanning(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Start scanning...");
        this.isScanning = true;
        ScanService scanService2 = new ScanService(getContext(), new BleScanServiceCallback(iSessionSdkCallback), j);
        this.scanService = scanService2;
        if (scanService2 != null) {
            scanService2.setActiveDeviceLog(getSerial());
            ScanService scanService3 = this.scanService;
            if (scanService3 != null) {
                scanService3.startScanWithAutoStopTimer();
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final ie0<i97> stopCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ee7.b(session, "logSession");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Stop Current Workout Session");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ee0 ee0 = (ee0) l60.a(ee0.class);
            if (ee0 != null) {
                return ee0.d().c(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).b(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StoppingWorkoutSessionFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void stopScanning(FLogger.Session session) {
        ee7.b(session, "logSession");
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.stopScan();
        }
        if (this.isScanning) {
            log(session, "Stop scanning for " + getSerial());
        }
        this.isScanning = false;
    }

    @DexIgnore
    public final ie0<Boolean> verifySecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        ie0<Boolean> d;
        ee7.b(session, "logSession");
        ee7.b(bArr, "secretKey");
        ee7.b(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Verify Secret Key");
        l60 l60 = this.deviceObj;
        if (l60 != null) {
            ge0 ge0 = (ge0) l60.a(ge0.class);
            if (ge0 != null && (d = ge0.b(bArr).c(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).b(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return d;
            }
            log(session, "Device[" + getSerial() + "] does not support VerifyingSecretKeyFeature");
            return null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    private final int getGattState(l60 l60) {
        if (l60 != null) {
            int i = WhenMappings.$EnumSwitchMapping$0[l60.getState().ordinal()];
            if (i != 1) {
                if (i == 2 || i == 3) {
                    return 2;
                }
                if (i == 4) {
                    return 1;
                }
                if (i != 5) {
                    return 0;
                }
                return 3;
            }
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Inside " + TAG + ".getGattState - serial=" + getSerial() + ", shineDevice=NULL");
        }
        return 0;
    }
}
