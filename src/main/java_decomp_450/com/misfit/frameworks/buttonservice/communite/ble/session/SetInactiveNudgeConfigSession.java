package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.o80;
import com.fossil.t80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetInactiveNudgeConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ InactiveNudgeData mInactiveNudgeData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetInactiveNudgeConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetInactiveNudgeConfigState() {
            super(SetInactiveNudgeConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            a90 a90 = new a90();
            a90.a(SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getRepeatInterval(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.isEnable() ? t80.a.ENABLE : t80.a.DISABLE);
            ie0<o80[]> deviceConfig = SetInactiveNudgeConfigSession.this.getBleAdapter().setDeviceConfig(SetInactiveNudgeConfigSession.this.getLogSession(), a90.a(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetInactiveNudgeConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetInactiveNudgeConfigSession.this.getContext(), SetInactiveNudgeConfigSession.this.getSerial())) {
                SetInactiveNudgeConfigSession.this.log("Reach the limit retry. Stop.");
                SetInactiveNudgeConfigSession.this.stop(FailureCode.FAILED_TO_SET_INACTIVE_NUDGE_CONFIG);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetInactiveNudgeConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SetInactiveNudgeConfigSession.this.log("Set Inactive nudge timeout. Cancel.");
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        ee7.b(inactiveNudgeData, "mInactiveNudgeData");
        ee7.b(bleAdapterImpl, "bleAdapterV2");
        this.mInactiveNudgeData = inactiveNudgeData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetInactiveNudgeConfigSession setInactiveNudgeConfigSession = new SetInactiveNudgeConfigSession(this.mInactiveNudgeData, getBleAdapter(), getBleSessionCallback());
        setInactiveNudgeConfigSession.setDevice(getDevice());
        return setInactiveNudgeConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE;
        String name = SetInactiveNudgeConfigState.class.getName();
        ee7.a((Object) name, "SetInactiveNudgeConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
