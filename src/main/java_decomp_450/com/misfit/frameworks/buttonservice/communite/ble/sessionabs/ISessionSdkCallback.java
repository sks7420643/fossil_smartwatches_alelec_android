package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.ab0;
import com.fossil.fitness.FitnessData;
import com.fossil.g60;
import com.fossil.ke0;
import com.fossil.l60;
import com.fossil.m60;
import com.fossil.n80;
import com.fossil.o80;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(ke0 ke0);

    @DexIgnore
    void onApplyHandPositionSuccess();

    @DexIgnore
    void onAuthenticateDeviceFail(ke0 ke0);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    void onAuthorizeDeviceFailed();

    @DexIgnore
    void onAuthorizeDeviceSuccess();

    @DexIgnore
    void onConfigureMicroAppFail(ke0 ke0);

    @DexIgnore
    void onConfigureMicroAppSuccess();

    @DexIgnore
    void onDataTransferCompleted();

    @DexIgnore
    void onDataTransferFailed(ke0 ke0);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(l60 l60, int i);

    @DexIgnore
    void onEraseDataFilesFailed(ke0 ke0);

    @DexIgnore
    void onEraseDataFilesSuccess();

    @DexIgnore
    void onEraseHWLogFailed(ke0 ke0);

    @DexIgnore
    void onEraseHWLogSuccess();

    @DexIgnore
    void onExchangeSecretKeyFail(ke0 ke0);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(ke0 ke0);

    @DexIgnore
    void onFetchDeviceInfoSuccess(m60 m60);

    @DexIgnore
    void onGetDeviceConfigFailed(ke0 ke0);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<o80, n80> hashMap);

    @DexIgnore
    void onGetWatchParamsFail();

    @DexIgnore
    void onMoveHandFailed(ke0 ke0);

    @DexIgnore
    void onMoveHandSuccess();

    @DexIgnore
    void onNextSession();

    @DexIgnore
    void onNotifyNotificationEventFailed(ke0 ke0);

    @DexIgnore
    void onNotifyNotificationEventSuccess();

    @DexIgnore
    void onPlayAnimationFail(ke0 ke0);

    @DexIgnore
    void onPlayAnimationSuccess();

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, ke0 ke0);

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(ke0 ke0);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(ab0 ab0);

    @DexIgnore
    void onReadDataFilesFailed(ke0 ke0);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(ke0 ke0);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    void onReadHWLogSuccess();

    @DexIgnore
    void onReadRssiFailed(ke0 ke0);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(ke0 ke0);

    @DexIgnore
    void onReleaseHandControlSuccess();

    @DexIgnore
    void onRequestHandControlFailed(ke0 ke0);

    @DexIgnore
    void onRequestHandControlSuccess();

    @DexIgnore
    void onResetHandsFailed(ke0 ke0);

    @DexIgnore
    void onResetHandsSuccess();

    @DexIgnore
    void onScanFail(g60 g60);

    @DexIgnore
    void onSendMicroAppDataFail(ke0 ke0);

    @DexIgnore
    void onSendMicroAppDataSuccess();

    @DexIgnore
    void onSendingEncryptedDataSessionFailed(ke0 ke0);

    @DexIgnore
    void onSendingEncryptedDataSessionSuccess();

    @DexIgnore
    void onSetAlarmFailed(ke0 ke0);

    @DexIgnore
    void onSetAlarmSuccess();

    @DexIgnore
    void onSetBackgroundImageFailed(ke0 ke0);

    @DexIgnore
    void onSetBackgroundImageSuccess();

    @DexIgnore
    void onSetComplicationFailed(ke0 ke0);

    @DexIgnore
    void onSetComplicationSuccess();

    @DexIgnore
    void onSetDeviceConfigFailed(ke0 ke0);

    @DexIgnore
    void onSetDeviceConfigSuccess();

    @DexIgnore
    void onSetFrontLightFailed(ke0 ke0);

    @DexIgnore
    void onSetFrontLightSuccess();

    @DexIgnore
    void onSetLabelFileFailed(ke0 ke0);

    @DexIgnore
    void onSetLabelFileSuccess();

    @DexIgnore
    void onSetLocalizationDataFail(ke0 ke0);

    @DexIgnore
    void onSetLocalizationDataSuccess();

    @DexIgnore
    void onSetMinimumStepThresholdSessionFailed(ke0 ke0);

    @DexIgnore
    void onSetMinimumStepThresholdSessionSuccess();

    @DexIgnore
    void onSetNotificationFilterFailed(ke0 ke0);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    void onSetNotificationFilterSuccess();

    @DexIgnore
    void onSetReplyMessageMappingError(ke0 ke0);

    @DexIgnore
    void onSetReplyMessageMappingSuccess();

    @DexIgnore
    void onSetWatchAppFailed(ke0 ke0);

    @DexIgnore
    void onSetWatchAppFileFailed();

    @DexIgnore
    void onSetWatchAppFileProgressChanged(float f);

    @DexIgnore
    void onSetWatchAppFileSuccess();

    @DexIgnore
    void onSetWatchAppSuccess();

    @DexIgnore
    void onSetWatchParamsFail(ke0 ke0);

    @DexIgnore
    void onSetWatchParamsSuccess();

    @DexIgnore
    void onSetWorkoutGPSDataSessionFailed(ke0 ke0);

    @DexIgnore
    void onSetWorkoutGPSDataSessionSuccess();

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(ke0 ke0);

    @DexIgnore
    void onStopCurrentWorkoutSessionSuccess();

    @DexIgnore
    void onVerifySecretKeyFail(ke0 ke0);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
