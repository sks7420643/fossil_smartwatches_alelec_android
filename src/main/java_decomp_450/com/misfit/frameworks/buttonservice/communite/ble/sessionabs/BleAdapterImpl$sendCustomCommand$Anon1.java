package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.feature.CustomCommandFeature;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$sendCustomCommand$Anon1 extends fe7 implements gd7<CustomCommandFeature.CustomResponse[], i97> {
    @DexIgnore
    public /* final */ /* synthetic */ CustomRequest $command;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$sendCustomCommand$Anon1(BleAdapterImpl bleAdapterImpl, CustomRequest customRequest) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$command = customRequest;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(CustomCommandFeature.CustomResponse[] customResponseArr) {
        invoke(customResponseArr);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(CustomCommandFeature.CustomResponse[] customResponseArr) {
        ee7.b(customResponseArr, "it");
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = FLogger.Session.OTHER;
        bleAdapterImpl.log(session, "Send Custom Command Success: " + this.$command);
    }
}
