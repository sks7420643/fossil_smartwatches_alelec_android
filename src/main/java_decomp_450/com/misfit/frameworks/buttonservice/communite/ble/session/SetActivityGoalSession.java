package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.a90;
import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetActivityGoalSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ int activeTimeGoal;
    @DexIgnore
    public /* final */ int caloriesGoal;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isRingEnabled;
    @DexIgnore
    public /* final */ int stepGoal;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetActivityGoalsState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetActivityGoalsState() {
            super(SetActivityGoalSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareConfigData() {
            a90 a90 = new a90();
            a90.e((long) SetActivityGoalSession.this.stepGoal);
            a90.b((long) SetActivityGoalSession.this.caloriesGoal);
            a90.a(SetActivityGoalSession.this.activeTimeGoal);
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetActivityGoalSession setActivityGoalSession = SetActivityGoalSession.this;
            setActivityGoalSession.log("Set Activity Goal: stepGoal=" + SetActivityGoalSession.this.stepGoal + ", caloriesGoal=" + SetActivityGoalSession.this.caloriesGoal + ", activeTimeGoal=" + SetActivityGoalSession.this.activeTimeGoal + ", isRingEnabled=" + SetActivityGoalSession.this.isRingEnabled);
            ie0<o80[]> deviceConfig = SetActivityGoalSession.this.getBleAdapter().setDeviceConfig(SetActivityGoalSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetActivityGoalSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetActivityGoalSession.this.getContext(), SetActivityGoalSession.this.getSerial())) {
                SetActivityGoalSession.this.log("Reach the limit retry. Stop.");
                SetActivityGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            if (FossilDeviceSerialPatternUtil.isDianaDevice(SetActivityGoalSession.this.getSerial())) {
                SetActivityGoalSession setActivityGoalSession = SetActivityGoalSession.this;
                setActivityGoalSession.enterStateAsync(setActivityGoalSession.createConcreteState(BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE));
                return;
            }
            SetActivityGoalSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
            SetActivityGoalSession.this.stop(FailureCode.FAILED_TO_SET_STEP_GOAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplicationsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetComplicationsState() {
            super(SetActivityGoalSession.this.getTAG());
        }

        @DexIgnore
        private final Bundle exportBundle() {
            Bundle bundle = new Bundle();
            bundle.putBoolean(ButtonService.GOAL_RING_ENABLED, SetActivityGoalSession.this.isRingEnabled);
            return bundle;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (SetActivityGoalSession.this.complicationAppMappingSettings != null) {
                BleAdapterImpl access$getBleAdapter$p = SetActivityGoalSession.this.getBleAdapter();
                FLogger.Session access$getLogSession$p = SetActivityGoalSession.this.getLogSession();
                ComplicationAppMappingSettings access$getComplicationAppMappingSettings$p = SetActivityGoalSession.this.complicationAppMappingSettings;
                if (access$getComplicationAppMappingSettings$p != null) {
                    ie0<i97> complications = access$getBleAdapter$p.setComplications(access$getLogSession$p, access$getComplicationAppMappingSettings$p, this);
                    this.task = complications;
                    if (complications == null) {
                        SetActivityGoalSession.this.stop(10000);
                        return true;
                    }
                    startTimeout();
                    return true;
                }
                ee7.a();
                throw null;
            }
            SetActivityGoalSession.this.stop(1920);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetActivityGoalSession.this.getContext(), SetActivityGoalSession.this.getSerial())) {
                SetActivityGoalSession.this.log("Reach the limit retry. Stop.");
                SetActivityGoalSession.this.stop(1920);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoComplicationAppSettings(SetActivityGoalSession.this.getBleAdapter().getContext(), SetActivityGoalSession.this.getBleAdapter().getSerial(), new Gson().a(SetActivityGoalSession.this.complicationAppMappingSettings));
            SetActivityGoalSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
            SetActivityGoalSession.this.stop(1920);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetActivityGoalSession(int i, int i2, int i3, boolean z, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.SET_STEP_GOAL, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.stepGoal = i;
        this.caloriesGoal = i2;
        this.activeTimeGoal = i3;
        this.isRingEnabled = z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putBoolean(ButtonService.GOAL_RING_ENABLED, this.isRingEnabled);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetActivityGoalSession setActivityGoalSession = new SetActivityGoalSession(this.stepGoal, this.caloriesGoal, this.activeTimeGoal, this.isRingEnabled, getBleAdapter(), getBleSessionCallback());
        setActivityGoalSession.setDevice(getDevice());
        return setActivityGoalSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_ACTIVITY_GOALS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        DevicePreferenceUtils.setGoalRingEnable(getContext(), this.isRingEnabled);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_ACTIVITY_GOALS_STATE;
        String name = SetActivityGoalsState.class.getName();
        ee7.a((Object) name, "SetActivityGoalsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE;
        String name2 = SetComplicationsState.class.getName();
        ee7.a((Object) name2, "SetComplicationsState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
