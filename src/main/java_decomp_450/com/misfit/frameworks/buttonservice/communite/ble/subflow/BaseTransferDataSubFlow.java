package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.ba7;
import com.fossil.ee7;
import com.fossil.f80;
import com.fossil.fitness.FitnessData;
import com.fossil.g80;
import com.fossil.h80;
import com.fossil.he0;
import com.fossil.i80;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.j80;
import com.fossil.k80;
import com.fossil.ke0;
import com.fossil.l80;
import com.fossil.m80;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.w97;
import com.fossil.we7;
import com.fossil.x87;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.db.DataCollectorHelper;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferDataSubFlow extends SubFlow {
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeMin;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public long calorie;
    @DexIgnore
    public long calorieGoal;
    @DexIgnore
    public long dailyStepInfo;
    @DexIgnore
    public int deepMin;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public /* final */ boolean isClearDataOptional;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightMin;
    @DexIgnore
    public List<FitnessData> mSyncData;
    @DexIgnore
    public /* final */ MFLog mflog;
    @DexIgnore
    public long realTimeSteps;
    @DexIgnore
    public long realTimeStepsInfo;
    @DexIgnore
    public long stepGoal;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int totalSleepInMin;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EraseDataFileState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public EraseDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<i97> eraseDataFile = BaseTransferDataSubFlow.this.getBleAdapter().eraseDataFile(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = eraseDataFile;
            if (eraseDataFile == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
                return;
            }
            BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesSuccess() {
            stopTimeout();
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Erase DataFiles timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                if (ie0 != null) {
                    ie0.e();
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
            } else {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<HashMap<o80, n80>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetDeviceConfigState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<o80, n80> hashMap) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            we7 we7 = we7.a;
            String format = String.format("Get configuration  " + BaseTransferDataSubFlow.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tdaily sleep: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(new Object[]{hashMap.get(o80.TIME), hashMap.get(o80.BATTERY), hashMap.get(o80.BIOMETRIC_PROFILE), hashMap.get(o80.DAILY_STEP), hashMap.get(o80.DAILY_STEP_GOAL), hashMap.get(o80.DAILY_CALORIE), hashMap.get(o80.DAILY_CALORIE_GOAL), hashMap.get(o80.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(o80.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(o80.DAILY_DISTANCE), hashMap.get(o80.DAILY_SLEEP), hashMap.get(o80.DAILY_DISTANCE), hashMap.get(o80.DAILY_DISTANCE), hashMap.get(o80.INACTIVE_NUDGE), hashMap.get(o80.VIBE_STRENGTH), hashMap.get(o80.DO_NOT_DISTURB_SCHEDULE)}, 16));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            baseTransferDataSubFlow.log(format);
        }

        @DexIgnore
        private final void readConfig(HashMap<o80, n80> hashMap) {
            if (!BaseTransferDataSubFlow.this.getUserProfile().isNewDevice()) {
                n80 n80 = hashMap.get(o80.DAILY_STEP_GOAL);
                if (n80 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                    if (n80 != null) {
                        baseTransferDataSubFlow.stepGoal = ((l80) n80).getStep();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
                    }
                }
                n80 n802 = hashMap.get(o80.DAILY_STEP);
                if (n802 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                    if (n802 != null) {
                        baseTransferDataSubFlow2.realTimeSteps = ((k80) n802).getStep();
                        BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                        baseTransferDataSubFlow3.realTimeStepsInfo = baseTransferDataSubFlow3.realTimeSteps;
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                    }
                }
                n80 n803 = hashMap.get(o80.DAILY_ACTIVE_MINUTE_GOAL);
                if (n803 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow4 = BaseTransferDataSubFlow.this;
                    if (n803 != null) {
                        baseTransferDataSubFlow4.activeMinuteGoal = ((f80) n803).getMinute();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
                    }
                }
                n80 n804 = hashMap.get(o80.DAILY_TOTAL_ACTIVE_MINUTE);
                if (n804 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow5 = BaseTransferDataSubFlow.this;
                    if (n804 != null) {
                        baseTransferDataSubFlow5.activeMinute = ((m80) n804).getMinute();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
                    }
                }
                n80 n805 = hashMap.get(o80.DAILY_CALORIE_GOAL);
                if (n805 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow6 = BaseTransferDataSubFlow.this;
                    if (n805 != null) {
                        baseTransferDataSubFlow6.calorieGoal = ((h80) n805).getCalorie();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
                    }
                }
                n80 n806 = hashMap.get(o80.DAILY_CALORIE);
                if (n806 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow7 = BaseTransferDataSubFlow.this;
                    if (n806 != null) {
                        baseTransferDataSubFlow7.calorie = ((g80) n806).getCalorie();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
                    }
                }
                n80 n807 = hashMap.get(o80.DAILY_DISTANCE);
                if (n807 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow8 = BaseTransferDataSubFlow.this;
                    if (n807 != null) {
                        baseTransferDataSubFlow8.distanceInCentimeter = ((i80) n807).getCentimeter();
                    } else {
                        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
                    }
                }
                n80 n808 = hashMap.get(o80.DAILY_SLEEP);
                if (n808 != null) {
                    if (!(n808 instanceof j80)) {
                        n808 = null;
                    }
                    j80 j80 = (j80) n808;
                    if (j80 != null) {
                        BaseTransferDataSubFlow.this.totalSleepInMin = j80.getTotalSleepInMinute();
                        BaseTransferDataSubFlow.this.awakeMin = j80.getAwakeInMinute();
                        BaseTransferDataSubFlow.this.lightMin = j80.getLightSleepInMinute();
                        BaseTransferDataSubFlow.this.deepMin = j80.getDeepSleepInMinute();
                        return;
                    }
                    return;
                }
                return;
            }
            BaseTransferDataSubFlow.this.log("We skip read real time steps cause by full sync");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ie0<HashMap<o80, n80>> deviceConfig = BaseTransferDataSubFlow.this.getBleAdapter().getDeviceConfig(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig != null) {
                startTimeout();
                return true;
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return true;
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<o80, n80> hashMap) {
            ee7.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            readConfig(hashMap);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Get device configuration timeout. Cancel.");
            ie0<HashMap<o80, n80>> ie0 = this.task;
            if (ie0 != null) {
                if (ie0 != null) {
                    ie0.e();
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PlayDeviceAnimationState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<i97> playDeviceAnimation = BaseTransferDataSubFlow.this.getBleAdapter().playDeviceAnimation(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = playDeviceAnimation;
            if (playDeviceAnimation == null) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPlayDeviceAnimation(boolean z, ke0 ke0) {
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Play device animation timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ProcessAndStoreDataState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ProcessAndStoreDataState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setTimeout(CommuteTimeService.z);
            BaseTransferDataSubFlow.this.log("Process and store data of device with serial " + BaseTransferDataSubFlow.this.getSerial());
        }

        @DexIgnore
        private final Bundle bundleData() {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.SYNC_RESULT, BaseTransferDataSubFlow.this.getSyncTime());
            bundle.putLong(ButtonService.Companion.getREALTIME_STEPS(), BaseTransferDataSubFlow.this.realTimeSteps);
            return bundle;
        }

        @DexIgnore
        private final List<FitnessData> readDataFromCache() {
            ArrayList arrayList = new ArrayList();
            List<DataFile> allDataFiles = DataFileProvider.getInstance(BaseTransferDataSubFlow.this.getBleAdapter().getContext()).getAllDataFiles(BaseTransferDataSubFlow.this.getSyncTime(), BaseTransferDataSubFlow.this.getSerial());
            Gson gson = new Gson();
            if (allDataFiles.size() > 0) {
                ee7.a((Object) allDataFiles, "cacheData");
                long j = -1;
                int i = 0;
                for (T t : allDataFiles) {
                    try {
                        ee7.a((Object) t, "it");
                        FitnessData fitnessData = (FitnessData) gson.a(t.getDataFile(), FitnessData.class);
                        ee7.a((Object) fitnessData, "fitnessData");
                        arrayList.add(fitnessData);
                        if (t.getSyncTime() != j) {
                            if (j != -1) {
                                BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                            }
                            j = t.getSyncTime();
                            i = 0;
                        }
                        i++;
                    } catch (Exception e) {
                        BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got cache data of ");
                        ee7.a((Object) t, "it");
                        sb.append(t.getSyncTime());
                        sb.append(" exception=");
                        sb.append(e);
                        baseTransferDataSubFlow.log(sb.toString());
                    }
                }
                if (j != -1 && i > 0) {
                    BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                }
            } else {
                BaseTransferDataSubFlow.this.log("No cache data.");
            }
            return arrayList;
        }

        @DexIgnore
        private final void saveAsCache(List<FitnessData> list) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("Save latest sync data as cache, sync time=" + BaseTransferDataSubFlow.this.getSyncTime() + ", fitness data size=" + list.size());
            int i = 0;
            for (T t : list) {
                int i2 = i + 1;
                if (i >= 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(BaseTransferDataSubFlow.this.getSyncTime());
                    sb.append('_');
                    sb.append(i);
                    DataCollectorHelper.saveDataFile(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), new DataFile(sb.toString(), new Gson().a((Object) t), BaseTransferDataSubFlow.this.getSerial(), BaseTransferDataSubFlow.this.getSyncTime()));
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter ProcessAndStoreDataState");
            List<FitnessData> readDataFromCache = readDataFromCache();
            saveAsCache(BaseTransferDataSubFlow.this.mSyncData);
            readDataFromCache.addAll(BaseTransferDataSubFlow.this.mSyncData);
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("set syncdata from cache data with size " + readDataFromCache.size());
            BaseTransferDataSubFlow.this.mSyncData = readDataFromCache;
            startTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferDataSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReceivedSyncData(bundleData());
                return true;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Process and Store data timeout.");
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void updateCurrentStepsAndStepGoal(boolean z, UserProfile userProfile) {
            ee7.b(userProfile, "newUserProfile");
            if (z) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", keep setting in device");
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", " + "{" + "currentSteps=" + userProfile.getCurrentSteps() + ", stepGoal=" + userProfile.getGoalSteps() + ',' + "activeMinute=" + userProfile.getActiveMinute() + ", activeMinuteGoal=" + userProfile.getActiveMinuteGoal() + ',' + "calories=" + userProfile.getCalories() + ", caloriesGoal=" + userProfile.getCaloriesGoal() + ',' + "distanceInCentimeter=" + userProfile.getDistanceInCentimeter() + "totalSleepMin=" + userProfile.getTotalSleepInMinute() + "awakeMin=" + userProfile.getAwakeInMinute() + "lightMin=" + userProfile.getLightSleepInMinute() + "deepMin=" + userProfile.getDeepSleepInMinute() + "}");
            }
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(userProfile.getCurrentSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(userProfile.getGoalSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(userProfile.getActiveMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(userProfile.getActiveMinuteGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(userProfile.getCalories());
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(userProfile.getCaloriesGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(userProfile.getDistanceInCentimeter());
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(userProfile.getTotalSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(userProfile.getAwakeInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(userProfile.getLightSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(userProfile.getDeepSleepInMinute());
            } else {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(BaseTransferDataSubFlow.this.realTimeSteps);
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(BaseTransferDataSubFlow.this.stepGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(BaseTransferDataSubFlow.this.activeMinute);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(BaseTransferDataSubFlow.this.activeMinuteGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(BaseTransferDataSubFlow.this.calorie);
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(BaseTransferDataSubFlow.this.calorieGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(BaseTransferDataSubFlow.this.distanceInCentimeter);
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(BaseTransferDataSubFlow.this.totalSleepInMin);
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(BaseTransferDataSubFlow.this.awakeMin);
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(BaseTransferDataSubFlow.this.lightMin);
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(BaseTransferDataSubFlow.this.deepMin);
            }
            FLogger.INSTANCE.updateSessionDetailInfo(new SessionDetailInfo(BaseTransferDataSubFlow.this.getBleAdapter().getBatteryLevel(), (int) BaseTransferDataSubFlow.this.realTimeStepsInfo, (int) BaseTransferDataSubFlow.this.dailyStepInfo));
            MFSyncLog syncLog = BaseTransferDataSubFlow.this.getSyncLog();
            if (syncLog != null) {
                syncLog.setRealTimeStep(BaseTransferDataSubFlow.this.getUserProfile().getCurrentSteps());
            }
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadDataFileState extends BleStateAbs {
        @DexIgnore
        public he0<FitnessData[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "cancelCurrentBleTask is " + this.task);
            he0<FitnessData[]> he0 = this.task;
            if (he0 != null) {
                he0.e();
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            try {
                he0<FitnessData[]> readDataFile = BaseTransferDataSubFlow.this.getBleAdapter().readDataFile(BaseTransferDataSubFlow.this.getLogSession(), BaseTransferDataSubFlow.this.getUserProfile().getUserBiometricData().toSDKBiometricProfile(), this);
                this.task = readDataFile;
                if (readDataFile == null) {
                    BaseTransferDataSubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            } catch (Exception e) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Read Data Files:  ex=" + e.getMessage());
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.errorLog("ReadDataFileState: catchException=" + e.getMessage(), FLogger.Component.BLE, ErrorCodeBuilder.Step.UNKNOWN, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), BaseTransferDataSubFlow.this.getSerial())) {
                BaseTransferDataSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
            ee7.b(fitnessDataArr, "data");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onReadDataFilesSuccess(), sdk = " + new Gson().a(fitnessDataArr));
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("data size from SDK " + fitnessDataArr.length);
            ba7.a(BaseTransferDataSubFlow.this.mSyncData, fitnessDataArr);
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Read DataFiles timeout. Cancel.");
            he0<FitnessData[]> he0 = this.task;
            if (he0 == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferDataSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, BleSession.BleSessionCallback bleSessionCallback2, boolean z) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        ee7.b(str, "tagName");
        ee7.b(bleSession, "bleSession");
        ee7.b(session, "logSession");
        ee7.b(str2, "serial");
        ee7.b(bleAdapterImpl, "bleAdapter");
        ee7.b(userProfile2, "userProfile");
        this.mflog = mFLog;
        this.userProfile = userProfile2;
        this.bleSessionCallback = bleSessionCallback2;
        this.isClearDataOptional = z;
        this.syncLog = (MFSyncLog) (!(mFLog instanceof MFSyncLog) ? null : mFLog);
        this.realTimeSteps = this.userProfile.getCurrentSteps();
        this.stepGoal = this.userProfile.getGoalSteps();
        this.activeMinute = this.userProfile.getActiveMinute();
        this.activeMinuteGoal = this.userProfile.getActiveMinuteGoal();
        this.calorie = this.userProfile.getCalories();
        this.calorieGoal = this.userProfile.getCaloriesGoal();
        this.distanceInCentimeter = this.userProfile.getDistanceInCentimeter();
        this.totalSleepInMin = this.userProfile.getTotalSleepInMinute();
        this.awakeMin = this.userProfile.getAwakeInMinute();
        this.lightMin = this.userProfile.getLightSleepInMinute();
        this.deepMin = this.userProfile.getDeepSleepInMinute();
        this.isNewDevice = this.userProfile.isNewDevice();
        this.realTimeStepsInfo = this.userProfile.getCurrentSteps();
        this.dailyStepInfo = this.userProfile.getCurrentSteps();
        this.mSyncData = new ArrayList();
        this.syncTime = System.currentTimeMillis() / ((long) 1000);
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData dataSize " + this.mSyncData.size());
        return this.mSyncData;
    }

    @DexIgnore
    public final MFSyncLog getSyncLog() {
        return this.syncLog;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        ee7.a((Object) name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        ee7.a((Object) name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.ERASE_DATA_FILE_STATE;
        String name3 = EraseDataFileState.class.getName();
        ee7.a((Object) name3, "EraseDataFileState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.READ_DATA_FILE_STATE;
        String name4 = ReadDataFileState.class.getName();
        ee7.a((Object) name4, "ReadDataFileState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE;
        String name5 = ProcessAndStoreDataState.class.getName();
        ee7.a((Object) name5, "ProcessAndStoreDataState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isClearDataOptional() {
        return this.isClearDataOptional;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE));
        return true;
    }

    @DexIgnore
    public final void setSyncLog(MFSyncLog mFSyncLog) {
        this.syncLog = mFSyncLog;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        ee7.b(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".updateCurrentStepAndStepGoalFromApp() - currentState=" + getMCurrentState());
        if (getMCurrentState() instanceof ProcessAndStoreDataState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((ProcessAndStoreDataState) mCurrentState).updateCurrentStepsAndStepGoal(z, userProfile2);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal " + "to device caused by current state null or not an instance of ProcessAndStoreDataState.");
    }
}
