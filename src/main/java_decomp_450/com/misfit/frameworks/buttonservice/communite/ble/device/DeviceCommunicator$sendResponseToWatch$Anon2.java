package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.je0;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendResponseToWatch$Anon2 extends fe7 implements gd7<je0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceAppResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendResponseToWatch$2$1", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendResponseToWatch$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(DeviceCommunicator$sendResponseToWatch$Anon2 deviceCommunicator$sendResponseToWatch$Anon2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = deviceCommunicator$sendResponseToWatch$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                DeviceCommunicator$sendResponseToWatch$Anon2 deviceCommunicator$sendResponseToWatch$Anon2 = this.this$0;
                BleCommunicator.startSendDeviceAppResponse$default(deviceCommunicator$sendResponseToWatch$Anon2.this$0, deviceCommunicator$sendResponseToWatch$Anon2.$response, false, 2, null);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendResponseToWatch$Anon2(DeviceCommunicator deviceCommunicator, DeviceAppResponse deviceAppResponse) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$response = deviceAppResponse;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(je0 je0) {
        invoke(je0);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(je0 je0) {
        ee7.b(je0, "error");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = this.this$0.getSerial();
        String access$getTAG$p = this.this$0.getTAG();
        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, je0);
        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
        remote.e(component, session, serial, access$getTAG$p, build, step, "Send respond: " + this.$response.getDeviceEventId().name() + " Failed");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$0.getTAG();
        local.d(access$getTAG$p2, "device with serial = " + this.this$0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", push back by result error=" + je0.getErrorCode());
        ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new Anon1_Level2(this, null), 3, null);
    }
}
