package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRssiSession extends BleSessionAbs {
    @DexIgnore
    public int mRemoteRssi; // = 100;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRssiStep extends BleStateAbs {
        @DexIgnore
        public ie0<Integer> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadRssiStep() {
            super(ReadRssiSession.this.getTAG());
            setTimeout(VideoUploader.RETRY_DELAY_UNIT_MS);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<Integer> readRssi = ReadRssiSession.this.getBleAdapter().readRssi(ReadRssiSession.this.getLogSession(), this);
            this.task = readRssi;
            if (readRssi == null) {
                ReadRssiSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadRssiFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            ReadRssiSession.this.stop(FailureCode.FAILED_TO_READ_RSSI);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadRssiSuccess(int i) {
            stopTimeout();
            ReadRssiSession.this.mRemoteRssi = i;
            ReadRssiSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<Integer> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRssiSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.READ_RSSI, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt("rssi", this.mRemoteRssi);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ReadRssiSession readRssiSession = new ReadRssiSession(getBleAdapter(), getBleSessionCallback());
        readRssiSession.setDevice(getDevice());
        return readRssiSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_RSSI_STATE;
        String name = ReadRssiStep.class.getName();
        ee7.a((Object) name, "ReadRssiStep::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        ee7.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        if (!BluetoothUtils.isBluetoothEnable()) {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon1(this), 500);
            return true;
        } else if (getBleAdapter().getDeviceObj() != null) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.READ_RSSI_STATE));
            return true;
        } else {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon3$Anon1_Level2(this), 500);
            return true;
        }
    }
}
