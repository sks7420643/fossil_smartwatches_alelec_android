package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendNotificationFromQueue$Anon1 extends fe7 implements gd7<i97, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$Anon1(DeviceCommunicator deviceCommunicator, NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(i97 i97) {
        invoke(i97);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(i97 i97) {
        ee7.b(i97, "it");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = this.this$0.getSerial();
        String access$getTAG$p = this.this$0.getTAG();
        remote.i(component, session, serial, access$getTAG$p, "Send notification: " + this.$notification.toRemoteLogString() + " Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$0.getTAG();
        local.d(access$getTAG$p2, " .sendNotificationFromQueue() = " + this.$notification + ", result=success");
        this.this$0.getCommunicationResultCallback().onNotificationSent(this.$notification.getUid(), true);
    }
}
