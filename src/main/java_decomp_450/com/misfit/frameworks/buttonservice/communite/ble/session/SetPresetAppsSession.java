package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetPresetAppsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ BackgroundConfig mBackgroundImageConfig;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings mComplicationAppMappingSettings;
    @DexIgnore
    public /* final */ WatchAppMappingSettings mWatchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBackgroundImageConfigState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mBackgroundImageConfig != null) {
                ie0<i97> backgroundImage = SetPresetAppsSession.this.getBleAdapter().setBackgroundImage(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mBackgroundImageConfig, this);
                this.task = backgroundImage;
                if (backgroundImage == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("mWatchAppMappingSettings mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession.this.stop(0);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoBackgroundImageConfig(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a(SetPresetAppsSession.this.mBackgroundImageConfig));
            SetPresetAppsSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplicationsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetComplicationsState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mComplicationAppMappingSettings != null) {
                ie0<i97> complications = SetPresetAppsSession.this.getBleAdapter().setComplications(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mComplicationAppMappingSettings, this);
                this.task = complications;
                if (complications == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("SetComplicationsState mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateWithDelayTime(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE), 500);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(1920);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoComplicationAppSettings(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a(SetPresetAppsSession.this.mComplicationAppMappingSettings));
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateAsync(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppsState() {
            super(SetPresetAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (SetPresetAppsSession.this.mWatchAppMappingSettings != null) {
                ie0<i97> watchApps = SetPresetAppsSession.this.getBleAdapter().setWatchApps(SetPresetAppsSession.this.getLogSession(), SetPresetAppsSession.this.mWatchAppMappingSettings, this);
                this.task = watchApps;
                if (watchApps == null) {
                    SetPresetAppsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetPresetAppsSession.this.log("mWatchAppMappingSettings mComplicationAppMappingSettings is null, skip state");
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateAsync(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetPresetAppsSession.this.getContext(), SetPresetAppsSession.this.getSerial())) {
                SetPresetAppsSession.this.log("Reach the limit retry. Stop.");
                SetPresetAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoWatchAppSettings(SetPresetAppsSession.this.getBleAdapter().getContext(), SetPresetAppsSession.this.getBleAdapter().getSerial(), new Gson().a(SetPresetAppsSession.this.mWatchAppMappingSettings));
            SetPresetAppsSession setPresetAppsSession = SetPresetAppsSession.this;
            setPresetAppsSession.enterStateAsync(setPresetAppsSession.createConcreteState(BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetPresetAppsSession(ComplicationAppMappingSettings complicationAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings, BackgroundConfig backgroundConfig, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_PRESET_APPS_DATA, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mComplicationAppMappingSettings = complicationAppMappingSettings;
        this.mWatchAppMappingSettings = watchAppMappingSettings;
        this.mBackgroundImageConfig = backgroundConfig;
        setLogSession(FLogger.Session.SET_PRESET_APPS);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_COMPLICATION_APPS || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_WATCH_APPS || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetPresetAppsSession setPresetAppsSession = new SetPresetAppsSession(this.mComplicationAppMappingSettings, this.mWatchAppMappingSettings, this.mBackgroundImageConfig, getBleAdapter(), getBleSessionCallback());
        setPresetAppsSession.setDevice(getDevice());
        return setPresetAppsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE;
        String name = SetComplicationsState.class.getName();
        ee7.a((Object) name, "SetComplicationsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name2 = SetWatchAppsState.class.getName();
        ee7.a((Object) name2, "SetWatchAppsState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name3 = SetBackgroundImageConfigState.class.getName();
        ee7.a((Object) name3, "SetBackgroundImageConfigState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }
}
