package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.ee7;
import com.fossil.eg0;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.r60;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon1 extends fe7 implements gd7<r60, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ eg0 $labelFile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, eg0 eg0, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$labelFile$inlined = eg0;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(r60 r60) {
        invoke(r60);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(r60 r60) {
        ee7.b(r60, "it");
        this.this$0.log(this.$logSession$inlined, "setLabelFile success.");
        this.$callback$inlined.onSetLabelFileSuccess();
    }
}
