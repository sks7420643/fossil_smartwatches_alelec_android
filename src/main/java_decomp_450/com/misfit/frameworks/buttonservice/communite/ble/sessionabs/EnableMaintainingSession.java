package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.facebook.internal.NativeProtocol;
import com.fossil.ee7;
import com.fossil.g60;
import com.fossil.l60;
import com.fossil.mh7;
import com.fossil.we7;
import com.fossil.zc0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class EnableMaintainingSession extends BleSessionAbs implements IExchangeKeySession, ISetWatchParamStateSession, ISetWatchAppFileSession {
    @DexIgnore
    public boolean isSkipEnableMaintainingConnection;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EnableMaintainingConnectionState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public EnableMaintainingConnectionState() {
            super(EnableMaintainingSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (!EnableMaintainingSession.this.isSkipEnableMaintainingConnection()) {
                Boolean enableMaintainConnection = EnableMaintainingSession.this.getBleAdapter().enableMaintainConnection(EnableMaintainingSession.this.getLogSession());
                if (enableMaintainConnection != null) {
                    if (enableMaintainConnection.booleanValue()) {
                        EnableMaintainingSession.this.log("Enable maintaining connection succeeded");
                    } else {
                        EnableMaintainingSession.this.log("Enable maintaining connection failed");
                        EnableMaintainingSession.this.errorLog("EnableMaintainingConnectionState: failed", ErrorCodeBuilder.Step.ENABLE_MAINTAINING_CONNECTION, ErrorCodeBuilder.AppError.UNKNOWN);
                        EnableMaintainingSession.this.stop(2000);
                    }
                }
            } else {
                EnableMaintainingSession.this.log("Skip enable maintaining connection");
            }
            if (EnableMaintainingSession.this.getBleAdapter().isSupportedFeature(zc0.class) != null) {
                EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
                enableMaintainingSession.enterStateAsync(enableMaintainingSession.createConcreteState(BleSessionAbs.SessionState.GET_SECRET_KEY));
                return true;
            }
            EnableMaintainingSession enableMaintainingSession2 = EnableMaintainingSession.this;
            enableMaintainingSession2.enterStateAsync(enableMaintainingSession2.getStateAfterEnableMaintainingConnection());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetSecretKeyState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetSecretKeyState() {
            super(EnableMaintainingSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (EnableMaintainingSession.this.getBleAdapter().getSecretKeyThroughSDK(EnableMaintainingSession.this.getLogSession()) == null) {
                EnableMaintainingSession.this.log("Get current secret key");
                if (EnableMaintainingSession.this.getBleSessionCallback() != null) {
                    startTimeout();
                    BleSession.BleSessionCallback access$getBleSessionCallback$p = EnableMaintainingSession.this.getBleSessionCallback();
                    if (access$getBleSessionCallback$p != null) {
                        access$getBleSessionCallback$p.onAskForCurrentSecretKey(EnableMaintainingSession.this.getSerial());
                        return true;
                    }
                    ee7.a();
                    throw null;
                }
                EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
                enableMaintainingSession.enterStateAsync(enableMaintainingSession.getStateAfterEnableMaintainingConnection());
                return true;
            }
            EnableMaintainingSession.this.log("Secret key is valid, go to next step");
            EnableMaintainingSession enableMaintainingSession2 = EnableMaintainingSession.this;
            enableMaintainingSession2.enterStateAsync(enableMaintainingSession2.getStateAfterEnableMaintainingConnection());
            return true;
        }

        @DexIgnore
        public final void onPing() {
            stopTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            EnableMaintainingSession.this.log("Timeout. No secret key received.");
            EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
            enableMaintainingSession.enterStateAsync(enableMaintainingSession.getStateAfterEnableMaintainingConnection());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class ScanningState extends BleStateAbs {
        @DexIgnore
        public boolean found;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ScanningState() {
            super(EnableMaintainingSession.this.getTAG());
            setTimeout(30000);
        }

        @DexIgnore
        public final boolean getFound() {
            return this.found;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onDeviceFound(l60 l60, int i) {
            ee7.b(l60, "device");
            String serialNumber = l60.n().getSerialNumber();
            String macAddress = l60.n().getMacAddress();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".handleOnDeviceScanned() - [Found: " + serialNumber + ", " + macAddress + "], [Candidate: " + EnableMaintainingSession.this.getBleAdapter().getSerial() + ", " + EnableMaintainingSession.this.getBleAdapter().getMacAddress() + "]");
            if (mh7.a((CharSequence) serialNumber)) {
                serialNumber = DeviceUtils.getInstance(EnableMaintainingSession.this.getContext()).getSerial(EnableMaintainingSession.this.getContext(), macAddress);
                ee7.a((Object) serialNumber, "DeviceUtils.getInstance(\u2026xt, scanDeviceMacAddress)");
            }
            boolean z = (mh7.a(EnableMaintainingSession.this.getSerial()) ^ true) && ee7.a(EnableMaintainingSession.this.getSerial(), serialNumber);
            this.found = z;
            this.found = z | ee7.a((Object) macAddress, (Object) EnableMaintainingSession.this.getBleAdapter().getMacAddress());
            EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
            we7 we7 = we7.a;
            String format = String.format("Found: %s, MAC %s", Arrays.copyOf(new Object[]{serialNumber, macAddress}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            enableMaintainingSession.log(format);
            if (!this.found) {
                return;
            }
            if (EnableMaintainingSession.this.getBleAdapter().setDevice(l60)) {
                EnableMaintainingSession enableMaintainingSession2 = EnableMaintainingSession.this;
                enableMaintainingSession2.log("Found device: " + serialNumber);
                stopTimeout();
                EnableMaintainingSession.this.getBleAdapter().stopScanning(EnableMaintainingSession.this.getLogSession());
                EnableMaintainingSession.this.enterStateAsync(stateAfterFoundDevice());
                return;
            }
            EnableMaintainingSession enableMaintainingSession3 = EnableMaintainingSession.this;
            enableMaintainingSession3.log("Found device: " + serialNumber + ", device obj is not compatible");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            boolean z;
            int i;
            super.onEnter();
            boolean isBluetoothEnable = BluetoothUtils.isBluetoothEnable();
            boolean isLocationPermissionGranted = LocationUtils.isLocationPermissionGranted(EnableMaintainingSession.this.getContext());
            boolean isLocationEnable = LocationUtils.isLocationEnable(EnableMaintainingSession.this.getContext());
            l60 buildDeviceBySerial = EnableMaintainingSession.this.getBleAdapter().buildDeviceBySerial(EnableMaintainingSession.this.getSerial(), EnableMaintainingSession.this.getBleAdapter().getMacAddress(), (long) EnableMaintainingSession.this.getStartTime());
            if (buildDeviceBySerial != null) {
                z = EnableMaintainingSession.this.getBleAdapter().setDevice(buildDeviceBySerial);
                if (!z) {
                    EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
                    enableMaintainingSession.log("Retrieve device: " + EnableMaintainingSession.this.getSerial() + ", but device obj is not compatible");
                }
            } else {
                EnableMaintainingSession.this.log("Retrieve device: Not found.");
                z = false;
            }
            int i2 = FailureCode.BLUETOOTH_IS_DISABLED;
            if (!z) {
                EnableMaintainingSession.this.log("Start scan.");
                if (!isBluetoothEnable || !isLocationPermissionGranted || !isLocationEnable) {
                    if (!isLocationEnable) {
                        i = FailureCode.LOCATION_SERVICE_DISABLED;
                        EnableMaintainingSession.this.log("In start scan step: LocationService is off.");
                        EnableMaintainingSession.this.errorLog("Scan device", ErrorCodeBuilder.Step.START_SCAN, ErrorCodeBuilder.AppError.LOCATION_SERVICE_DISABLED);
                        EnableMaintainingSession.this.addRequiredPermissionCode(FailureCode.LOCATION_SERVICE_DISABLED);
                    } else {
                        i = FailureCode.BLUETOOTH_IS_DISABLED;
                    }
                    if (!isLocationPermissionGranted) {
                        i = FailureCode.LOCATION_ACCESS_DENIED;
                        EnableMaintainingSession.this.log("In start scan step: Location permission is disable.");
                        EnableMaintainingSession.this.errorLog("Scan device", ErrorCodeBuilder.Step.START_SCAN, ErrorCodeBuilder.AppError.LOCATION_ACCESS_DENIED);
                        EnableMaintainingSession.this.addRequiredPermissionCode(FailureCode.LOCATION_ACCESS_DENIED);
                    }
                    if (!isBluetoothEnable) {
                        EnableMaintainingSession.this.log("In start scan step: Bluetooth is disable.");
                        EnableMaintainingSession.this.errorLog("Scan device", ErrorCodeBuilder.Step.START_SCAN, ErrorCodeBuilder.AppError.BLUETOOTH_DISABLED);
                        EnableMaintainingSession.this.addRequiredPermissionCode(FailureCode.BLUETOOTH_IS_DISABLED);
                    } else {
                        i2 = i;
                    }
                    EnableMaintainingSession.this.stop(i2);
                } else {
                    startTimeout();
                    EnableMaintainingSession.this.getBleAdapter().startScanning(EnableMaintainingSession.this.getLogSession(), (long) EnableMaintainingSession.this.getStartTime(), this);
                }
            } else if (!isBluetoothEnable) {
                EnableMaintainingSession.this.log("Bluetooth is disable.");
                EnableMaintainingSession.this.errorLog("After retrieve device", ErrorCodeBuilder.Step.RETRIEVE_DEVICE_BY_SERIAL, ErrorCodeBuilder.AppError.BLUETOOTH_DISABLED);
                EnableMaintainingSession.this.addRequiredPermissionCode(FailureCode.BLUETOOTH_IS_DISABLED);
                EnableMaintainingSession.this.stop(FailureCode.BLUETOOTH_IS_DISABLED);
                return true;
            } else {
                EnableMaintainingSession enableMaintainingSession2 = EnableMaintainingSession.this;
                enableMaintainingSession2.log("Retrieve device success: " + EnableMaintainingSession.this.getSerial());
                EnableMaintainingSession.this.enterStateAsync(stateAfterFoundDevice());
            }
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onExit() {
            stopTimeout();
            EnableMaintainingSession.this.getBleAdapter().stopScanning(EnableMaintainingSession.this.getLogSession());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onFatal(int i) {
            super.onFatal(i);
            stopTimeout();
            EnableMaintainingSession.this.getBleAdapter().stopScanning(EnableMaintainingSession.this.getLogSession());
            EnableMaintainingSession.this.stop(i);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onScanFail(g60 g60) {
            ee7.b(g60, "scanError");
            EnableMaintainingSession enableMaintainingSession = EnableMaintainingSession.this;
            enableMaintainingSession.log("The scanning failed with failureCode: " + EnableMaintainingSession.this.getFailureCode());
            EnableMaintainingSession.this.errorLog("The scanning", ErrorCodeBuilder.Step.START_SCAN, g60);
            onFatal(FailureCode.getFailureCodeNoDeviceFound(EnableMaintainingSession.this.getContext(), EnableMaintainingSession.this.getFailureCode()));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            EnableMaintainingSession.this.log("The scanning timeout.");
            EnableMaintainingSession.this.errorLog("The scanning", ErrorCodeBuilder.Step.START_SCAN, ErrorCodeBuilder.AppError.DEVICE_NOT_FOUND);
            onFatal(FailureCode.getFailureCodeNoDeviceFound(EnableMaintainingSession.this.getContext(), FailureCode.DEVICE_NOT_FOUND));
        }

        @DexIgnore
        public final void setFound(boolean z) {
            this.found = z;
        }

        @DexIgnore
        public BleState stateAfterFoundDevice() {
            return EnableMaintainingSession.this.createConcreteState(BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EnableMaintainingSession(SessionType sessionType, CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(sessionType, communicateMode, bleAdapterImpl, bleSessionCallback);
        ee7.b(sessionType, "sessionType");
        ee7.b(communicateMode, "communicateMode");
        ee7.b(bleAdapterImpl, "bleAdapter");
        setSerial(bleAdapterImpl.getSerial());
        setContext(bleAdapterImpl.getContext());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession
    public void doNextState() {
    }

    @DexIgnore
    public abstract BleState getStateAfterEnableMaintainingConnection();

    @DexIgnore
    public void initSettings() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SCANNING_STATE;
        String name = ScanningState.class.getName();
        ee7.a((Object) name, "ScanningState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE;
        String name2 = EnableMaintainingConnectionState.class.getName();
        ee7.a((Object) name2, "EnableMaintainingConnectionState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.GET_SECRET_KEY;
        String name3 = GetSecretKeyState.class.getName();
        ee7.a((Object) name3, "GetSecretKeyState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    public final boolean isSkipEnableMaintainingConnection() {
        return this.isSkipEnableMaintainingConnection;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession
    public void onGetWatchParamFailed() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onPing() {
        BleState currentState = getCurrentState();
        if (currentState instanceof GetSecretKeyState) {
            ((GetSecretKeyState) currentState).onPing();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveCurrentSecretKey(byte[] bArr) {
        log("onReceiveCurrentSecretKey " + bArr);
        if (bArr != null) {
            getBleAdapter().setSecretKey(getLogSession(), bArr);
        }
        enterStateAsync(getStateAfterEnableMaintainingConnection());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveRandomKey(byte[] bArr, int i) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        ee7.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        initSettings();
        l60 deviceObj = getBleAdapter().getDeviceObj();
        if (deviceObj == null) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.SCANNING_STATE));
            return true;
        } else if (!BluetoothUtils.isBluetoothEnable()) {
            errorLog("Check bluetooth before run session", ErrorCodeBuilder.Step.ENABLE_MAINTAINING_CONNECTION, ErrorCodeBuilder.AppError.BLUETOOTH_DISABLED);
            addRequiredPermissionCode(FailureCode.BLUETOOTH_IS_DISABLED);
            enterTaskWithDelayTime(new EnableMaintainingSession$onStart$$inlined$let$lambda$Anon1(this), 500);
            return true;
        } else if (deviceObj.isActive()) {
            log("Device connection has been active.");
            enterStateAsync(getStateAfterEnableMaintainingConnection());
            return true;
        } else {
            log("Device connection has not been active.");
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE));
            return true;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession
    public void onWatchAppFilesReady(boolean z) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        ee7.b(str, "serial");
        ee7.b(watchParamsFileMapping, "watchParamsData");
    }

    @DexIgnore
    public final void setSkipEnableMaintainingConnection(boolean z) {
        this.isSkipEnableMaintainingConnection = z;
    }
}
