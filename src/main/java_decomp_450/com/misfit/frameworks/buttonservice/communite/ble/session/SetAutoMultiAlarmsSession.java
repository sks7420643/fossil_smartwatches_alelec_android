package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.od0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoMultiAlarmsSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ List<AlarmSetting> mAlarmSettingList;
    @DexIgnore
    public List<AlarmSetting> mOldMultiAlarmSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoMultiAlarmsSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetListAlarmsState() {
            super(SetAutoMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetAutoMultiAlarmsSession.this.log("Set auto multi alarms.");
            ie0<i97> alarms = SetAutoMultiAlarmsSession.this.getBleAdapter().setAlarms(SetAutoMultiAlarmsSession.this.getLogSession(), SetAutoMultiAlarmsSession.this.mAlarmSettingList, this);
            this.task = alarms;
            if (alarms == null) {
                SetAutoMultiAlarmsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetAutoMultiAlarmsSession.this.getContext(), SetAutoMultiAlarmsSession.this.getSerial())) {
                SetAutoMultiAlarmsSession.this.log("Reach the limit retry. Stop.");
                SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = SetAutoMultiAlarmsSession.this;
                setAutoMultiAlarmsSession.storeSettings(setAutoMultiAlarmsSession.mAlarmSettingList, true);
                SetAutoMultiAlarmsSession.this.stop(FailureCode.FAILED_TO_SET_ALARM);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmSuccess() {
            stopTimeout();
            SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = SetAutoMultiAlarmsSession.this;
            setAutoMultiAlarmsSession.storeSettings(setAutoMultiAlarmsSession.mAlarmSettingList, false);
            SetAutoMultiAlarmsSession setAutoMultiAlarmsSession2 = SetAutoMultiAlarmsSession.this;
            setAutoMultiAlarmsSession2.enterStateAsync(setAutoMultiAlarmsSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoMultiAlarmsSession(List<AlarmSetting> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_MULTI_ALARM, bleAdapterImpl, bleSessionCallback);
        ee7.b(list, "mAlarmSettingList");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mAlarmSettingList = list;
        setLogSession(FLogger.Session.SET_ALARM);
    }

    @DexIgnore
    private final void storeSettings(List<AlarmSetting> list, boolean z) {
        DevicePreferenceUtils.setAutoListAlarm(getBleAdapter().getContext(), list);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoMultiAlarmsSession setAutoMultiAlarmsSession = new SetAutoMultiAlarmsSession(this.mAlarmSettingList, getBleAdapter(), getBleSessionCallback());
        setAutoMultiAlarmsSession.setDevice(getDevice());
        return setAutoMultiAlarmsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        List<AlarmSetting> autoListAlarm = DevicePreferenceUtils.getAutoListAlarm(getContext());
        ee7.a((Object) autoListAlarm, "DevicePreferenceUtils.getAutoListAlarm(context)");
        this.mOldMultiAlarmSettings = autoListAlarm;
        if (getBleAdapter().isSupportedFeature(od0.class) != null) {
            List<AlarmSetting> list = this.mOldMultiAlarmSettings;
            if (list != null) {
                if (!list.isEmpty()) {
                    List<AlarmSetting> list2 = this.mOldMultiAlarmSettings;
                    if (list2 == null) {
                        ee7.d("mOldMultiAlarmSettings");
                        throw null;
                    } else if (AlarmExtensionKt.isSame(list2, this.mAlarmSettingList)) {
                        log("The multi alarms are the same, no need to store again.");
                        bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                    }
                }
                storeSettings(this.mAlarmSettingList, true);
                bleState = createConcreteState(BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE);
            } else {
                ee7.d("mOldMultiAlarmSettings");
                throw null;
            }
        } else {
            log("This device does not support set multi alarm.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE;
        String name = SetListAlarmsState.class.getName();
        ee7.a((Object) name, "SetListAlarmsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        ee7.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        ee7.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
