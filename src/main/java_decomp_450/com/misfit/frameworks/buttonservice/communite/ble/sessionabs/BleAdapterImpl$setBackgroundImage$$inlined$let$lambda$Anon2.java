package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.je0;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2 extends fe7 implements gd7<je0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ BackgroundConfig $backgroundConfig$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $directory$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, BackgroundConfig backgroundConfig, String str, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$backgroundConfig$inlined = backgroundConfig;
        this.$directory$inlined = str;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(je0 je0) {
        invoke(je0);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(je0 je0) {
        ee7.b(je0, "it");
        this.this$0.logSdkError(this.$logSession$inlined, "Set Background Image", ErrorCodeBuilder.Step.SET_BACKGROUND_IMAGE, je0);
        this.$callback$inlined.onSetBackgroundImageFailed(je0.getErrorCode());
    }
}
