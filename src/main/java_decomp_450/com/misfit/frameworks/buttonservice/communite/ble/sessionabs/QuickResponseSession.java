package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.facebook.internal.NativeProtocol;
import com.fossil.ee7;
import com.fossil.l60;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class QuickResponseSession extends BleSessionAbs {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EnableMaintainingConnectionState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public EnableMaintainingConnectionState() {
            super(QuickResponseSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Boolean enableMaintainConnection = QuickResponseSession.this.getBleAdapter().enableMaintainConnection(QuickResponseSession.this.getLogSession());
            if (enableMaintainConnection != null) {
                if (enableMaintainConnection.booleanValue()) {
                    QuickResponseSession.this.log("Enable maintaining connection succeeded");
                } else {
                    QuickResponseSession.this.log("Enable maintaining connection failed");
                    QuickResponseSession.this.stop(2000);
                }
            }
            QuickResponseSession quickResponseSession = QuickResponseSession.this;
            quickResponseSession.enterStateAsync(quickResponseSession.getFirstState());
            return true;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public QuickResponseSession(SessionType sessionType, CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(sessionType, communicateMode, bleAdapterImpl, bleSessionCallback);
        ee7.b(sessionType, "sessionType");
        ee7.b(communicateMode, "communicateMode");
        ee7.b(bleAdapterImpl, "bleAdapter");
        setSerial(bleAdapterImpl.getSerial());
        setContext(bleAdapterImpl.getContext());
    }

    @DexIgnore
    public abstract BleState getFirstState();

    @DexIgnore
    public void initSettings() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE;
        String name = EnableMaintainingConnectionState.class.getName();
        ee7.a((Object) name, "EnableMaintainingConnectionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        ee7.b(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        initSettings();
        if (getBleAdapter().isDeviceReady()) {
            l60 deviceObj = getBleAdapter().getDeviceObj();
            if (!(deviceObj != null ? deviceObj.isActive() : false)) {
                enterStateAsync(createConcreteState(BleSessionAbs.SessionState.ENABLE_MAINTAINING_CONNECTION_STATE));
                return true;
            }
            enterStateAsync(getFirstState());
            return true;
        }
        log("Device is disconnected, end now.");
        enterTaskWithDelayTime(new QuickResponseSession$onStart$Anon1(this), 500);
        return true;
    }
}
