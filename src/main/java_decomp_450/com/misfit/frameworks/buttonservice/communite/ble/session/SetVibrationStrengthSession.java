package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetVibrationStrengthSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ VibrationStrengthObj mVibrationStrengthLevelObj;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetVibrationStrengthState() {
            super(SetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareData() {
            a90 a90 = new a90();
            a90.a(SetVibrationStrengthSession.this.mVibrationStrengthLevelObj.toSDKVibrationStrengthLevel());
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetVibrationStrengthSession setVibrationStrengthSession = SetVibrationStrengthSession.this;
            setVibrationStrengthSession.log("Set Vibration Strength, value=" + SetVibrationStrengthSession.this.mVibrationStrengthLevelObj);
            ie0<o80[]> deviceConfig = SetVibrationStrengthSession.this.getBleAdapter().setDeviceConfig(SetVibrationStrengthSession.this.getLogSession(), prepareData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_SET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_VIBRATION_STRENGTH, bleAdapterImpl, bleSessionCallback);
        ee7.b(vibrationStrengthObj, "mVibrationStrengthLevelObj");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mVibrationStrengthLevelObj = vibrationStrengthObj;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetVibrationStrengthSession setVibrationStrengthSession = new SetVibrationStrengthSession(this.mVibrationStrengthLevelObj, getBleAdapter(), getBleSessionCallback());
        setVibrationStrengthSession.setDevice(getDevice());
        return setVibrationStrengthSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE;
        String name = SetVibrationStrengthState.class.getName();
        ee7.a((Object) name, "SetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
