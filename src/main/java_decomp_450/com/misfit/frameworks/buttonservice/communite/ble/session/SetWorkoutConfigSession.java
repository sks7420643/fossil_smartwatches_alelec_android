package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutConfigSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ WorkoutConfigData mWorkoutConfigData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWorkoutConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWorkoutConfigState() {
            super(SetWorkoutConfigSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareConfigData() {
            a90 a90 = new a90();
            a90.a(SetWorkoutConfigSession.this.mWorkoutConfigData.diameterInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.tireSizeInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.chainring, SetWorkoutConfigSession.this.mWorkoutConfigData.cog);
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetWorkoutConfigSession setWorkoutConfigSession = SetWorkoutConfigSession.this;
            setWorkoutConfigSession.log("SetWorkoutConfigSession: WorkoutConfigData=" + SetWorkoutConfigSession.this.mWorkoutConfigData);
            ie0<o80[]> deviceConfig = SetWorkoutConfigSession.this.getBleAdapter().setDeviceConfig(SetWorkoutConfigSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetWorkoutConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetWorkoutConfigSession.this.getContext(), SetWorkoutConfigSession.this.getSerial())) {
                SetWorkoutConfigSession.this.log("Reach the limit retry. Stop.");
                SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetWorkoutConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
            SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWorkoutConfigSession(WorkoutConfigData workoutConfigData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.DEVICE_SETTING, CommunicateMode.SET_WORKOUT_CONFIG_SESSION, bleAdapterImpl, bleSessionCallback);
        ee7.b(workoutConfigData, "mWorkoutConfigData");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mWorkoutConfigData = workoutConfigData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWorkoutConfigSession setWorkoutConfigSession = new SetWorkoutConfigSession(this.mWorkoutConfigData, getBleAdapter(), getBleSessionCallback());
        setWorkoutConfigSession.setDevice(getDevice());
        return setWorkoutConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE;
        String name = SetWorkoutConfigState.class.getName();
        ee7.a((Object) name, "SetWorkoutConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
