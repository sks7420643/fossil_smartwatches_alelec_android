package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.b90;
import com.fossil.c80;
import com.fossil.d90;
import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.t80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDeviceConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE);
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDeviceConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDeviceConfigState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            a90 a90 = new a90();
            a90.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            a90.d(SetImplicitDeviceConfigSession.this.userProfile.getCurrentSteps());
            a90.e(SetImplicitDeviceConfigSession.this.userProfile.getGoalSteps());
            a90.b(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinute());
            a90.a(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinuteGoal());
            a90.a(SetImplicitDeviceConfigSession.this.userProfile.getCalories());
            a90.b(SetImplicitDeviceConfigSession.this.userProfile.getCaloriesGoal());
            a90.c(SetImplicitDeviceConfigSession.this.userProfile.getDistanceInCentimeter());
            a90.b(SetImplicitDeviceConfigSession.this.userProfile.getTotalSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getAwakeInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getLightSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getDeepSleepInMinute());
            UserDisplayUnit displayUnit = SetImplicitDeviceConfigSession.this.userProfile.getDisplayUnit();
            if (displayUnit != null) {
                a90.a(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), b90.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDeviceConfigSession.this.getBleAdapter().getContext()), d90.MONTH_DAY_YEAR);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No user display unit.");
                i97 i97 = i97.a;
            }
            InactiveNudgeData inactiveNudgeData = SetImplicitDeviceConfigSession.this.userProfile.getInactiveNudgeData();
            if (inactiveNudgeData != null) {
                a90.a(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? t80.a.ENABLE : t80.a.DISABLE);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No inactive nudge config.");
                i97 i972 = i97.a;
            }
            try {
                c80 sDKBiometricProfile = SetImplicitDeviceConfigSession.this.userProfile.getUserBiometricData().toSDKBiometricProfile();
                a90.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
                setImplicitDeviceConfigSession.log("Set Device Config: exception=" + e.getMessage());
            }
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<o80[]> deviceConfig = SetImplicitDeviceConfigSession.this.getBleAdapter().setDeviceConfig(SetImplicitDeviceConfigSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetImplicitDeviceConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SetImplicitDeviceConfigSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
            setImplicitDeviceConfigSession.enterStateAsync(setImplicitDeviceConfigSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDeviceConfigSession.this.log("Set Device Config timeout. Cancel.");
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDeviceConfigSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, bleAdapterImpl, bleSessionCallback);
        ee7.b(userProfile2, "userProfile");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = new SetImplicitDeviceConfigSession(this.userProfile, getBleAdapter(), getBleSessionCallback());
        setImplicitDeviceConfigSession.setDevice(getDevice());
        return setImplicitDeviceConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE;
        String name = SetDeviceConfigState.class.getName();
        ee7.a((Object) name, "SetDeviceConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        ee7.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        ee7.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
