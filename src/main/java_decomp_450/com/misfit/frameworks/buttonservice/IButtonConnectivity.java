package com.misfit.frameworks.buttonservice;

import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface IButtonConnectivity extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Stub extends Binder implements IButtonConnectivity {
        @DexIgnore
        public static /* final */ String DESCRIPTOR; // = "com.misfit.frameworks.buttonservice.IButtonConnectivity";
        @DexIgnore
        public static /* final */ int TRANSACTION_addLog; // = 63;
        @DexIgnore
        public static /* final */ int TRANSACTION_cancelPairDevice; // = 8;
        @DexIgnore
        public static /* final */ int TRANSACTION_changePendingLogKey; // = 67;
        @DexIgnore
        public static /* final */ int TRANSACTION_confirmBCStatus; // = 117;
        @DexIgnore
        public static /* final */ int TRANSACTION_confirmStopWorkout; // = 111;
        @DexIgnore
        public static /* final */ int TRANSACTION_connectAllButton; // = 4;
        @DexIgnore
        public static /* final */ int TRANSACTION_deleteDataFiles; // = 19;
        @DexIgnore
        public static /* final */ int TRANSACTION_deleteHeartRateFiles; // = 75;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceCancelCalibration; // = 49;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceClearMapping; // = 36;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceCompleteCalibration; // = 48;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceDisconnect; // = 9;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceForceReconnect; // = 5;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceGetBatteryLevel; // = 23;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceGetCountDown; // = 59;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceGetRssi; // = 24;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceMovingHand; // = 46;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceOta; // = 25;
        @DexIgnore
        public static /* final */ int TRANSACTION_devicePlayAnimation; // = 20;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceReadRealTimeStep; // = 21;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSendNotification; // = 55;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetAutoCountdownSetting; // = 60;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetAutoListAlarm; // = 54;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetAutoSecondTimezone; // = 52;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetDisableCountDown; // = 58;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetEnableCountDown; // = 57;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetInactiveNudgeConfig; // = 99;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetListAlarm; // = 53;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetMapping; // = 35;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetSecondTimeZone; // = 51;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceSetVibrationStrength; // = 56;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceStartCalibration; // = 45;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceStartScan; // = 14;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceStartSync; // = 16;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceStopScan; // = 15;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceUnlink; // = 10;
        @DexIgnore
        public static /* final */ int TRANSACTION_deviceUpdateActivityGoals; // = 22;
        @DexIgnore
        public static /* final */ int TRANSACTION_disableHeartRateNotification; // = 74;
        @DexIgnore
        public static /* final */ int TRANSACTION_enableHeartRateNotification; // = 73;
        @DexIgnore
        public static /* final */ int TRANSACTION_endLog; // = 65;
        @DexIgnore
        public static /* final */ int TRANSACTION_forceSwitchDeviceWithoutErase; // = 12;
        @DexIgnore
        public static /* final */ int TRANSACTION_forceUpdateDeviceData; // = 79;
        @DexIgnore
        public static /* final */ int TRANSACTION_getActiveSerial; // = 43;
        @DexIgnore
        public static /* final */ int TRANSACTION_getAutoMapping; // = 38;
        @DexIgnore
        public static /* final */ int TRANSACTION_getCommunicatorModeBySerial; // = 69;
        @DexIgnore
        public static /* final */ int TRANSACTION_getDeviceProfile; // = 27;
        @DexIgnore
        public static /* final */ int TRANSACTION_getGattState; // = 28;
        @DexIgnore
        public static /* final */ int TRANSACTION_getHIDState; // = 29;
        @DexIgnore
        public static /* final */ int TRANSACTION_getListActiveCommunicator; // = 70;
        @DexIgnore
        public static /* final */ int TRANSACTION_getPairedDevice; // = 26;
        @DexIgnore
        public static /* final */ int TRANSACTION_getPairedSerial; // = 44;
        @DexIgnore
        public static /* final */ int TRANSACTION_getSyncData; // = 17;
        @DexIgnore
        public static /* final */ int TRANSACTION_init; // = 1;
        @DexIgnore
        public static /* final */ int TRANSACTION_interrupt; // = 50;
        @DexIgnore
        public static /* final */ int TRANSACTION_interruptCurrentSession; // = 113;
        @DexIgnore
        public static /* final */ int TRANSACTION_isLinking; // = 33;
        @DexIgnore
        public static /* final */ int TRANSACTION_isSyncing; // = 31;
        @DexIgnore
        public static /* final */ int TRANSACTION_isUpdatingFirmware; // = 32;
        @DexIgnore
        public static /* final */ int TRANSACTION_logOut; // = 30;
        @DexIgnore
        public static /* final */ int TRANSACTION_notifyNotificationEvent; // = 86;
        @DexIgnore
        public static /* final */ int TRANSACTION_notifyWatchAppFilesReady; // = 116;
        @DexIgnore
        public static /* final */ int TRANSACTION_onPing; // = 105;
        @DexIgnore
        public static /* final */ int TRANSACTION_onSetWatchParamResponse; // = 112;
        @DexIgnore
        public static /* final */ int TRANSACTION_pairDevice; // = 6;
        @DexIgnore
        public static /* final */ int TRANSACTION_pairDeviceResponse; // = 7;
        @DexIgnore
        public static /* final */ int TRANSACTION_playVibration; // = 34;
        @DexIgnore
        public static /* final */ int TRANSACTION_readCurrentWorkoutSession; // = 93;
        @DexIgnore
        public static /* final */ int TRANSACTION_removeActiveSerial; // = 41;
        @DexIgnore
        public static /* final */ int TRANSACTION_removePairedSerial; // = 42;
        @DexIgnore
        public static /* final */ int TRANSACTION_resetDeviceSettingToDefault; // = 100;
        @DexIgnore
        public static /* final */ int TRANSACTION_resetHandsToZeroDegree; // = 47;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendCurrentSecretKey; // = 104;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendCustomCommand; // = 110;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendDeviceAppResponse; // = 78;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendMicroAppRemoteActivity; // = 68;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendMusicAppResponse; // = 80;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendPushSecretKeyResponse; // = 114;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendRandomKey; // = 106;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendServerSecretKey; // = 103;
        @DexIgnore
        public static /* final */ int TRANSACTION_sendingEncryptedDataSession; // = 98;
        @DexIgnore
        public static /* final */ int TRANSACTION_setActiveSerial; // = 39;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoBackgroundImageConfig; // = 88;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoComplicationAppSettings; // = 77;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoMapping; // = 37;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoNotificationFilterSettings; // = 89;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoUserBiometricData; // = 92;
        @DexIgnore
        public static /* final */ int TRANSACTION_setAutoWatchAppSettings; // = 87;
        @DexIgnore
        public static /* final */ int TRANSACTION_setBackgroundImageConfig; // = 83;
        @DexIgnore
        public static /* final */ int TRANSACTION_setComplicationApps; // = 76;
        @DexIgnore
        public static /* final */ int TRANSACTION_setFrontLightEnable; // = 102;
        @DexIgnore
        public static /* final */ int TRANSACTION_setHeartRateMode; // = 101;
        @DexIgnore
        public static /* final */ int TRANSACTION_setImplicitDeviceConfig; // = 91;
        @DexIgnore
        public static /* final */ int TRANSACTION_setImplicitDisplayUnitSettings; // = 90;
        @DexIgnore
        public static /* final */ int TRANSACTION_setLocalizationData; // = 108;
        @DexIgnore
        public static /* final */ int TRANSACTION_setMinimumStepThresholdSession; // = 97;
        @DexIgnore
        public static /* final */ int TRANSACTION_setNotificationFilterSettings; // = 84;
        @DexIgnore
        public static /* final */ int TRANSACTION_setPairedSerial; // = 40;
        @DexIgnore
        public static /* final */ int TRANSACTION_setPresetApps; // = 82;
        @DexIgnore
        public static /* final */ int TRANSACTION_setReplyMessageMappingSetting; // = 85;
        @DexIgnore
        public static /* final */ int TRANSACTION_setSecretKey; // = 107;
        @DexIgnore
        public static /* final */ int TRANSACTION_setWatchApps; // = 81;
        @DexIgnore
        public static /* final */ int TRANSACTION_setWorkoutConfig; // = 96;
        @DexIgnore
        public static /* final */ int TRANSACTION_setWorkoutDetectionSetting; // = 109;
        @DexIgnore
        public static /* final */ int TRANSACTION_setWorkoutGPSDataSession; // = 95;
        @DexIgnore
        public static /* final */ int TRANSACTION_simulateDisconnection; // = 71;
        @DexIgnore
        public static /* final */ int TRANSACTION_simulatePusherEvent; // = 72;
        @DexIgnore
        public static /* final */ int TRANSACTION_startLog; // = 64;
        @DexIgnore
        public static /* final */ int TRANSACTION_stopCurrentWorkoutSession; // = 94;
        @DexIgnore
        public static /* final */ int TRANSACTION_stopLogService; // = 66;
        @DexIgnore
        public static /* final */ int TRANSACTION_switchActiveDevice; // = 11;
        @DexIgnore
        public static /* final */ int TRANSACTION_switchDeviceResponse; // = 13;
        @DexIgnore
        public static /* final */ int TRANSACTION_updateActiveDeviceInfoLog; // = 2;
        @DexIgnore
        public static /* final */ int TRANSACTION_updateAppInfo; // = 62;
        @DexIgnore
        public static /* final */ int TRANSACTION_updateAppLogInfo; // = 3;
        @DexIgnore
        public static /* final */ int TRANSACTION_updatePercentageGoalProgress; // = 18;
        @DexIgnore
        public static /* final */ int TRANSACTION_updateUserId; // = 61;
        @DexIgnore
        public static /* final */ int TRANSACTION_verifySecretKeySession; // = 115;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Proxy implements IButtonConnectivity {
            @DexIgnore
            public IBinder mRemote;

            @DexIgnore
            public Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void addLog(int i, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(63, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.mRemote;
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void cancelPairDevice(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void changePendingLogKey(int i, String str, int i2, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeInt(i2);
                    obtain.writeString(str2);
                    this.mRemote.transact(67, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void confirmBCStatus(boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(117, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void confirmStopWorkout(String str, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(111, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void connectAllButton() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deleteDataFiles(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deleteHeartRateFiles(List<String> list, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStringList(list);
                    obtain.writeString(str);
                    this.mRemote.transact(75, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceCancelCalibration(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(49, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceClearMapping(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceCompleteCalibration(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(48, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceDisconnect(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceForceReconnect(String str, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceGetBatteryLevel(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceGetCountDown(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(59, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceGetRssi(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceMovingHand(String str, HandCalibrationObj handCalibrationObj) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (handCalibrationObj != null) {
                        obtain.writeInt(1);
                        handCalibrationObj.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceOta(String str, FirmwareData firmwareData, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (firmwareData != null) {
                        obtain.writeInt(1);
                        firmwareData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long devicePlayAnimation(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceReadRealTimeStep(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSendNotification(String str, NotificationBaseObj notificationBaseObj) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (notificationBaseObj != null) {
                        obtain.writeInt(1);
                        notificationBaseObj.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(55, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceSetAutoCountdownSetting(long j, long j2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeLong(j);
                    obtain.writeLong(j2);
                    this.mRemote.transact(60, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceSetAutoListAlarm(List<Alarm> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(54, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceSetAutoSecondTimezone(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(52, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetDisableCountDown(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(58, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetEnableCountDown(String str, long j, long j2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeLong(j);
                    obtain.writeLong(j2);
                    this.mRemote.transact(57, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetInactiveNudgeConfig(String str, InactiveNudgeData inactiveNudgeData) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (inactiveNudgeData != null) {
                        obtain.writeInt(1);
                        inactiveNudgeData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(99, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetListAlarm(String str, List<Alarm> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(53, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetMapping(String str, List<BLEMapping> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(35, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetSecondTimeZone(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(51, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceSetVibrationStrength(String str, VibrationStrengthObj vibrationStrengthObj) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (vibrationStrengthObj != null) {
                        obtain.writeInt(1);
                        vibrationStrengthObj.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(56, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceStartCalibration(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(45, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceStartScan() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceStartSync(String str, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void deviceStopScan() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceUnlink(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long deviceUpdateActivityGoals(String str, int i, int i2, int i3, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long disableHeartRateNotification(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(74, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long enableHeartRateNotification(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(73, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int endLog(int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(65, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public boolean forceSwitchDeviceWithoutErase(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    boolean z = false;
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void forceUpdateDeviceData(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (deviceAppResponse != null) {
                        obtain.writeInt(1);
                        deviceAppResponse.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(79, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public List<String> getActiveSerial() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(43, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArrayList();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public List<BLEMapping> getAutoMapping(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(38, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(BLEMapping.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int getCommunicatorModeBySerial(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(69, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public MisfitDeviceProfile getDeviceProfile(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? MisfitDeviceProfile.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int getGattState(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int getHIDState(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(29, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int[] getListActiveCommunicator() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(70, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createIntArray();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public List<MisfitDeviceProfile> getPairedDevice() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(MisfitDeviceProfile.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public List<String> getPairedSerial() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(44, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArrayList();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public List<FitnessData> getSyncData(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(FitnessData.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void init(String str, String str2, String str3, char c, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeInt(c);
                    if (appLogInfo != null) {
                        obtain.writeInt(1);
                        appLogInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (activeDeviceInfo != null) {
                        obtain.writeInt(1);
                        activeDeviceInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (cloudLogConfig != null) {
                        obtain.writeInt(1);
                        cloudLogConfig.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void interrupt(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(50, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void interruptCurrentSession(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(113, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public boolean isLinking(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    boolean z = false;
                    this.mRemote.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public boolean isSyncing(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    boolean z = false;
                    this.mRemote.transact(31, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public boolean isUpdatingFirmware(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    boolean z = false;
                    this.mRemote.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void logOut() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long notifyNotificationEvent(NotificationBaseObj notificationBaseObj, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (notificationBaseObj != null) {
                        obtain.writeInt(1);
                        notificationBaseObj.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(86, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long notifyWatchAppFilesReady(String str, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(116, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long onPing(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(105, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    if (watchParamsFileMapping != null) {
                        obtain.writeInt(1);
                        watchParamsFileMapping.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(112, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long pairDevice(String str, String str2, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long pairDeviceResponse(String str, PairingResponse pairingResponse) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (pairingResponse != null) {
                        obtain.writeInt(1);
                        pairingResponse.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long playVibration(String str, int i, int i2, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(34, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long readCurrentWorkoutSession(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(93, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void removeActiveSerial(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(41, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void removePairedSerial(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(42, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void resetDeviceSettingToDefault(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(100, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long resetHandsToZeroDegree(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(47, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long sendCurrentSecretKey(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(104, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void sendCustomCommand(String str, CustomRequest customRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (customRequest != null) {
                        obtain.writeInt(1);
                        customRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(110, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void sendDeviceAppResponse(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (deviceAppResponse != null) {
                        obtain.writeInt(1);
                        deviceAppResponse.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(78, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void sendMicroAppRemoteActivity(String str, DeviceAppResponse deviceAppResponse) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (deviceAppResponse != null) {
                        obtain.writeInt(1);
                        deviceAppResponse.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(68, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void sendMusicAppResponse(MusicResponse musicResponse, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (musicResponse != null) {
                        obtain.writeInt(1);
                        musicResponse.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(80, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void sendPushSecretKeyResponse(String str, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(114, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long sendRandomKey(String str, String str2, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.mRemote.transact(106, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long sendServerSecretKey(String str, String str2, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.mRemote.transact(103, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long sendingEncryptedDataSession(byte[] bArr, String str, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeByteArray(bArr);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(98, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setActiveSerial(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(39, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (backgroundConfig != null) {
                        obtain.writeInt(1);
                        backgroundConfig.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(88, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoComplicationAppSettings(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (complicationAppMappingSettings != null) {
                        obtain.writeInt(1);
                        complicationAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(77, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoMapping(String str, List<BLEMapping> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(37, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (appNotificationFilterSettings != null) {
                        obtain.writeInt(1);
                        appNotificationFilterSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(89, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoUserBiometricData(UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(92, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setAutoWatchAppSettings(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (watchAppMappingSettings != null) {
                        obtain.writeInt(1);
                        watchAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(87, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (backgroundConfig != null) {
                        obtain.writeInt(1);
                        backgroundConfig.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(83, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (complicationAppMappingSettings != null) {
                        obtain.writeInt(1);
                        complicationAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(76, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setFrontLightEnable(String str, boolean z) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    this.mRemote.transact(102, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setHeartRateMode(String str, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(101, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setImplicitDeviceConfig(UserProfile userProfile, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(91, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (userDisplayUnit != null) {
                        obtain.writeInt(1);
                        userDisplayUnit.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(90, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setLocalizationData(LocalizationData localizationData, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (localizationData != null) {
                        obtain.writeInt(1);
                        localizationData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(108, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setMinimumStepThresholdSession(long j, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeLong(j);
                    obtain.writeString(str);
                    this.mRemote.transact(97, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (appNotificationFilterSettings != null) {
                        obtain.writeInt(1);
                        appNotificationFilterSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(84, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setPairedSerial(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(40, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (watchAppMappingSettings != null) {
                        obtain.writeInt(1);
                        watchAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (complicationAppMappingSettings != null) {
                        obtain.writeInt(1);
                        complicationAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (backgroundConfig != null) {
                        obtain.writeInt(1);
                        backgroundConfig.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(82, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setReplyMessageMappingSetting(ReplyMessageMappingGroup replyMessageMappingGroup, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (replyMessageMappingGroup != null) {
                        obtain.writeInt(1);
                        replyMessageMappingGroup.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(85, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setSecretKey(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(107, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (watchAppMappingSettings != null) {
                        obtain.writeInt(1);
                        watchAppMappingSettings.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(81, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void setWorkoutConfig(WorkoutConfigData workoutConfigData, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (workoutConfigData != null) {
                        obtain.writeInt(1);
                        workoutConfigData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(96, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (workoutDetectionSetting != null) {
                        obtain.writeInt(1);
                        workoutDetectionSetting.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(109, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long setWorkoutGPSDataSession(String str, Location location) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (location != null) {
                        obtain.writeInt(1);
                        location.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(95, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void simulateDisconnection(String str, int i, int i2, int i3, int i4) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    this.mRemote.transact(71, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void simulatePusherEvent(String str, int i, int i2, int i3, int i4, int i5) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    obtain.writeInt(i5);
                    this.mRemote.transact(72, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public int startLog(int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(64, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long stopCurrentWorkoutSession(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(94, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void stopLogService(int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(66, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public boolean switchActiveDevice(String str, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    boolean z = true;
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long switchDeviceResponse(String str, boolean z, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    obtain.writeInt(i);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void updateActiveDeviceInfoLog(ActiveDeviceInfo activeDeviceInfo) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (activeDeviceInfo != null) {
                        obtain.writeInt(1);
                        activeDeviceInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void updateAppInfo(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(62, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void updateAppLogInfo(AppLogInfo appLogInfo) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (appLogInfo != null) {
                        obtain.writeInt(1);
                        appLogInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void updatePercentageGoalProgress(String str, boolean z, UserProfile userProfile) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    if (userProfile != null) {
                        obtain.writeInt(1);
                        userProfile.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public void updateUserId(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(61, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
            public long verifySecretKeySession(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(115, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readLong();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        @DexIgnore
        public static IButtonConnectivity asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IButtonConnectivity)) {
                return new Proxy(iBinder);
            }
            return (IButtonConnectivity) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 1598968902) {
                boolean z = false;
                boolean z2 = false;
                boolean z3 = false;
                boolean z4 = false;
                boolean z5 = false;
                boolean z6 = false;
                boolean z7 = false;
                boolean z8 = false;
                boolean z9 = false;
                boolean z10 = false;
                ActiveDeviceInfo activeDeviceInfo = null;
                WatchParamsFileMapping watchParamsFileMapping = null;
                CustomRequest customRequest = null;
                WorkoutDetectionSetting workoutDetectionSetting = null;
                LocalizationData localizationData = null;
                InactiveNudgeData inactiveNudgeData = null;
                WorkoutConfigData workoutConfigData = null;
                Location location = null;
                UserProfile userProfile = null;
                UserProfile userProfile2 = null;
                UserDisplayUnit userDisplayUnit = null;
                AppNotificationFilterSettings appNotificationFilterSettings = null;
                BackgroundConfig backgroundConfig = null;
                WatchAppMappingSettings watchAppMappingSettings = null;
                NotificationBaseObj notificationBaseObj = null;
                ReplyMessageMappingGroup replyMessageMappingGroup = null;
                AppNotificationFilterSettings appNotificationFilterSettings2 = null;
                BackgroundConfig backgroundConfig2 = null;
                BackgroundConfig backgroundConfig3 = null;
                WatchAppMappingSettings watchAppMappingSettings2 = null;
                MusicResponse musicResponse = null;
                DeviceAppResponse deviceAppResponse = null;
                DeviceAppResponse deviceAppResponse2 = null;
                ComplicationAppMappingSettings complicationAppMappingSettings = null;
                ComplicationAppMappingSettings complicationAppMappingSettings2 = null;
                DeviceAppResponse deviceAppResponse3 = null;
                VibrationStrengthObj vibrationStrengthObj = null;
                NotificationBaseObj notificationBaseObj2 = null;
                HandCalibrationObj handCalibrationObj = null;
                UserProfile userProfile3 = null;
                UserProfile userProfile4 = null;
                UserProfile userProfile5 = null;
                UserProfile userProfile6 = null;
                PairingResponse pairingResponse = null;
                UserProfile userProfile7 = null;
                UserProfile userProfile8 = null;
                AppLogInfo appLogInfo = null;
                switch (i) {
                    case 1:
                        parcel.enforceInterface(DESCRIPTOR);
                        init(parcel.readString(), parcel.readString(), parcel.readString(), (char) parcel.readInt(), parcel.readInt() != 0 ? AppLogInfo.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? ActiveDeviceInfo.CREATOR.createFromParcel(parcel) : null, parcel.readInt() != 0 ? CloudLogConfig.CREATOR.createFromParcel(parcel) : null);
                        parcel2.writeNoException();
                        return true;
                    case 2:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            activeDeviceInfo = ActiveDeviceInfo.CREATOR.createFromParcel(parcel);
                        }
                        updateActiveDeviceInfoLog(activeDeviceInfo);
                        parcel2.writeNoException();
                        return true;
                    case 3:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            appLogInfo = AppLogInfo.CREATOR.createFromParcel(parcel);
                        }
                        updateAppLogInfo(appLogInfo);
                        parcel2.writeNoException();
                        return true;
                    case 4:
                        parcel.enforceInterface(DESCRIPTOR);
                        connectAllButton();
                        parcel2.writeNoException();
                        return true;
                    case 5:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString = parcel.readString();
                        if (parcel.readInt() != 0) {
                            userProfile8 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        long deviceForceReconnect = deviceForceReconnect(readString, userProfile8);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceForceReconnect);
                        return true;
                    case 6:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString2 = parcel.readString();
                        String readString3 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            userProfile7 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        long pairDevice = pairDevice(readString2, readString3, userProfile7);
                        parcel2.writeNoException();
                        parcel2.writeLong(pairDevice);
                        return true;
                    case 7:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString4 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            pairingResponse = PairingResponse.CREATOR.createFromParcel(parcel);
                        }
                        long pairDeviceResponse = pairDeviceResponse(readString4, pairingResponse);
                        parcel2.writeNoException();
                        parcel2.writeLong(pairDeviceResponse);
                        return true;
                    case 8:
                        parcel.enforceInterface(DESCRIPTOR);
                        cancelPairDevice(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 9:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceDisconnect(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 10:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceUnlink = deviceUnlink(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceUnlink);
                        return true;
                    case 11:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString5 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            userProfile6 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        boolean switchActiveDevice = switchActiveDevice(readString5, userProfile6);
                        parcel2.writeNoException();
                        parcel2.writeInt(switchActiveDevice ? 1 : 0);
                        return true;
                    case 12:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean forceSwitchDeviceWithoutErase = forceSwitchDeviceWithoutErase(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(forceSwitchDeviceWithoutErase ? 1 : 0);
                        return true;
                    case 13:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString6 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        long switchDeviceResponse = switchDeviceResponse(readString6, z, parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeLong(switchDeviceResponse);
                        return true;
                    case 14:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceStartScan();
                        parcel2.writeNoException();
                        return true;
                    case 15:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceStopScan();
                        parcel2.writeNoException();
                        return true;
                    case 16:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString7 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            userProfile5 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        long deviceStartSync = deviceStartSync(readString7, userProfile5);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceStartSync);
                        return true;
                    case 17:
                        parcel.enforceInterface(DESCRIPTOR);
                        List<FitnessData> syncData = getSyncData(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeTypedList(syncData);
                        return true;
                    case 18:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString8 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z10 = true;
                        }
                        if (parcel.readInt() != 0) {
                            userProfile4 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        updatePercentageGoalProgress(readString8, z10, userProfile4);
                        parcel2.writeNoException();
                        return true;
                    case 19:
                        parcel.enforceInterface(DESCRIPTOR);
                        deleteDataFiles(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 20:
                        parcel.enforceInterface(DESCRIPTOR);
                        long devicePlayAnimation = devicePlayAnimation(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(devicePlayAnimation);
                        return true;
                    case 21:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceReadRealTimeStep = deviceReadRealTimeStep(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceReadRealTimeStep);
                        return true;
                    case 22:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceUpdateActivityGoals = deviceUpdateActivityGoals(parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt() != 0);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceUpdateActivityGoals);
                        return true;
                    case 23:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceGetBatteryLevel = deviceGetBatteryLevel(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceGetBatteryLevel);
                        return true;
                    case 24:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceGetRssi = deviceGetRssi(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceGetRssi);
                        return true;
                    case 25:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString9 = parcel.readString();
                        FirmwareData createFromParcel = parcel.readInt() != 0 ? FirmwareData.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            userProfile3 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        long deviceOta = deviceOta(readString9, createFromParcel, userProfile3);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceOta);
                        return true;
                    case 26:
                        parcel.enforceInterface(DESCRIPTOR);
                        List<MisfitDeviceProfile> pairedDevice = getPairedDevice();
                        parcel2.writeNoException();
                        parcel2.writeTypedList(pairedDevice);
                        return true;
                    case 27:
                        parcel.enforceInterface(DESCRIPTOR);
                        MisfitDeviceProfile deviceProfile = getDeviceProfile(parcel.readString());
                        parcel2.writeNoException();
                        if (deviceProfile != null) {
                            parcel2.writeInt(1);
                            deviceProfile.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 28:
                        parcel.enforceInterface(DESCRIPTOR);
                        int gattState = getGattState(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(gattState);
                        return true;
                    case 29:
                        parcel.enforceInterface(DESCRIPTOR);
                        int hIDState = getHIDState(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(hIDState);
                        return true;
                    case 30:
                        parcel.enforceInterface(DESCRIPTOR);
                        logOut();
                        parcel2.writeNoException();
                        return true;
                    case 31:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean isSyncing = isSyncing(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(isSyncing ? 1 : 0);
                        return true;
                    case 32:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean isUpdatingFirmware = isUpdatingFirmware(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(isUpdatingFirmware ? 1 : 0);
                        return true;
                    case 33:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean isLinking = isLinking(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(isLinking ? 1 : 0);
                        return true;
                    case 34:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString10 = parcel.readString();
                        int readInt = parcel.readInt();
                        int readInt2 = parcel.readInt();
                        if (parcel.readInt() != 0) {
                            z9 = true;
                        }
                        long playVibration = playVibration(readString10, readInt, readInt2, z9);
                        parcel2.writeNoException();
                        parcel2.writeLong(playVibration);
                        return true;
                    case 35:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceSetMapping = deviceSetMapping(parcel.readString(), parcel.createTypedArrayList(BLEMapping.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetMapping);
                        return true;
                    case 36:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceClearMapping = deviceClearMapping(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceClearMapping);
                        return true;
                    case 37:
                        parcel.enforceInterface(DESCRIPTOR);
                        setAutoMapping(parcel.readString(), parcel.createTypedArrayList(BLEMapping.CREATOR));
                        parcel2.writeNoException();
                        return true;
                    case 38:
                        parcel.enforceInterface(DESCRIPTOR);
                        List<BLEMapping> autoMapping = getAutoMapping(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeTypedList(autoMapping);
                        return true;
                    case 39:
                        parcel.enforceInterface(DESCRIPTOR);
                        setActiveSerial(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 40:
                        parcel.enforceInterface(DESCRIPTOR);
                        setPairedSerial(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 41:
                        parcel.enforceInterface(DESCRIPTOR);
                        removeActiveSerial(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 42:
                        parcel.enforceInterface(DESCRIPTOR);
                        removePairedSerial(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 43:
                        parcel.enforceInterface(DESCRIPTOR);
                        List<String> activeSerial = getActiveSerial();
                        parcel2.writeNoException();
                        parcel2.writeStringList(activeSerial);
                        return true;
                    case 44:
                        parcel.enforceInterface(DESCRIPTOR);
                        List<String> pairedSerial = getPairedSerial();
                        parcel2.writeNoException();
                        parcel2.writeStringList(pairedSerial);
                        return true;
                    case 45:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceStartCalibration = deviceStartCalibration(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceStartCalibration);
                        return true;
                    case 46:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString11 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            handCalibrationObj = HandCalibrationObj.CREATOR.createFromParcel(parcel);
                        }
                        deviceMovingHand(readString11, handCalibrationObj);
                        parcel2.writeNoException();
                        return true;
                    case 47:
                        parcel.enforceInterface(DESCRIPTOR);
                        long resetHandsToZeroDegree = resetHandsToZeroDegree(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(resetHandsToZeroDegree);
                        return true;
                    case 48:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceCompleteCalibration = deviceCompleteCalibration(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceCompleteCalibration);
                        return true;
                    case 49:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceCancelCalibration = deviceCancelCalibration(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceCancelCalibration);
                        return true;
                    case 50:
                        parcel.enforceInterface(DESCRIPTOR);
                        interrupt(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 51:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceSetSecondTimeZone = deviceSetSecondTimeZone(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetSecondTimeZone);
                        return true;
                    case 52:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceSetAutoSecondTimezone(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 53:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceSetListAlarm = deviceSetListAlarm(parcel.readString(), parcel.createTypedArrayList(Alarm.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetListAlarm);
                        return true;
                    case 54:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceSetAutoListAlarm(parcel.createTypedArrayList(Alarm.CREATOR));
                        parcel2.writeNoException();
                        return true;
                    case 55:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString12 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            notificationBaseObj2 = NotificationBaseObj.CREATOR.createFromParcel(parcel);
                        }
                        long deviceSendNotification = deviceSendNotification(readString12, notificationBaseObj2);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSendNotification);
                        return true;
                    case 56:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString13 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            vibrationStrengthObj = VibrationStrengthObj.CREATOR.createFromParcel(parcel);
                        }
                        long deviceSetVibrationStrength = deviceSetVibrationStrength(readString13, vibrationStrengthObj);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetVibrationStrength);
                        return true;
                    case 57:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceSetEnableCountDown = deviceSetEnableCountDown(parcel.readString(), parcel.readLong(), parcel.readLong());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetEnableCountDown);
                        return true;
                    case 58:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceSetDisableCountDown = deviceSetDisableCountDown(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetDisableCountDown);
                        return true;
                    case 59:
                        parcel.enforceInterface(DESCRIPTOR);
                        long deviceGetCountDown = deviceGetCountDown(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceGetCountDown);
                        return true;
                    case 60:
                        parcel.enforceInterface(DESCRIPTOR);
                        deviceSetAutoCountdownSetting(parcel.readLong(), parcel.readLong());
                        parcel2.writeNoException();
                        return true;
                    case 61:
                        parcel.enforceInterface(DESCRIPTOR);
                        updateUserId(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 62:
                        parcel.enforceInterface(DESCRIPTOR);
                        updateAppInfo(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 63:
                        parcel.enforceInterface(DESCRIPTOR);
                        addLog(parcel.readInt(), parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 64:
                        parcel.enforceInterface(DESCRIPTOR);
                        int startLog = startLog(parcel.readInt(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(startLog);
                        return true;
                    case 65:
                        parcel.enforceInterface(DESCRIPTOR);
                        int endLog = endLog(parcel.readInt(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(endLog);
                        return true;
                    case 66:
                        parcel.enforceInterface(DESCRIPTOR);
                        stopLogService(parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 67:
                        parcel.enforceInterface(DESCRIPTOR);
                        changePendingLogKey(parcel.readInt(), parcel.readString(), parcel.readInt(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 68:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString14 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            deviceAppResponse3 = DeviceAppResponse.CREATOR.createFromParcel(parcel);
                        }
                        sendMicroAppRemoteActivity(readString14, deviceAppResponse3);
                        parcel2.writeNoException();
                        return true;
                    case 69:
                        parcel.enforceInterface(DESCRIPTOR);
                        int communicatorModeBySerial = getCommunicatorModeBySerial(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeInt(communicatorModeBySerial);
                        return true;
                    case 70:
                        parcel.enforceInterface(DESCRIPTOR);
                        int[] listActiveCommunicator = getListActiveCommunicator();
                        parcel2.writeNoException();
                        parcel2.writeIntArray(listActiveCommunicator);
                        return true;
                    case 71:
                        parcel.enforceInterface(DESCRIPTOR);
                        simulateDisconnection(parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 72:
                        parcel.enforceInterface(DESCRIPTOR);
                        simulatePusherEvent(parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
                        parcel2.writeNoException();
                        return true;
                    case 73:
                        parcel.enforceInterface(DESCRIPTOR);
                        long enableHeartRateNotification = enableHeartRateNotification(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(enableHeartRateNotification);
                        return true;
                    case 74:
                        parcel.enforceInterface(DESCRIPTOR);
                        long disableHeartRateNotification = disableHeartRateNotification(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(disableHeartRateNotification);
                        return true;
                    case 75:
                        parcel.enforceInterface(DESCRIPTOR);
                        deleteHeartRateFiles(parcel.createStringArrayList(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 76:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            complicationAppMappingSettings2 = ComplicationAppMappingSettings.CREATOR.createFromParcel(parcel);
                        }
                        long complicationApps = setComplicationApps(complicationAppMappingSettings2, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(complicationApps);
                        return true;
                    case 77:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            complicationAppMappingSettings = ComplicationAppMappingSettings.CREATOR.createFromParcel(parcel);
                        }
                        setAutoComplicationAppSettings(complicationAppMappingSettings, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 78:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            deviceAppResponse2 = DeviceAppResponse.CREATOR.createFromParcel(parcel);
                        }
                        sendDeviceAppResponse(deviceAppResponse2, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 79:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            deviceAppResponse = DeviceAppResponse.CREATOR.createFromParcel(parcel);
                        }
                        forceUpdateDeviceData(deviceAppResponse, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 80:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            musicResponse = MusicResponse.CREATOR.createFromParcel(parcel);
                        }
                        sendMusicAppResponse(musicResponse, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 81:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            watchAppMappingSettings2 = WatchAppMappingSettings.CREATOR.createFromParcel(parcel);
                        }
                        long watchApps = setWatchApps(watchAppMappingSettings2, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(watchApps);
                        return true;
                    case 82:
                        parcel.enforceInterface(DESCRIPTOR);
                        WatchAppMappingSettings createFromParcel2 = parcel.readInt() != 0 ? WatchAppMappingSettings.CREATOR.createFromParcel(parcel) : null;
                        ComplicationAppMappingSettings createFromParcel3 = parcel.readInt() != 0 ? ComplicationAppMappingSettings.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            backgroundConfig3 = BackgroundConfig.CREATOR.createFromParcel(parcel);
                        }
                        long presetApps = setPresetApps(createFromParcel2, createFromParcel3, backgroundConfig3, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(presetApps);
                        return true;
                    case 83:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            backgroundConfig2 = BackgroundConfig.CREATOR.createFromParcel(parcel);
                        }
                        long backgroundImageConfig = setBackgroundImageConfig(backgroundConfig2, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(backgroundImageConfig);
                        return true;
                    case 84:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            appNotificationFilterSettings2 = AppNotificationFilterSettings.CREATOR.createFromParcel(parcel);
                        }
                        long notificationFilterSettings = setNotificationFilterSettings(appNotificationFilterSettings2, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(notificationFilterSettings);
                        return true;
                    case 85:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            replyMessageMappingGroup = ReplyMessageMappingGroup.CREATOR.createFromParcel(parcel);
                        }
                        long replyMessageMappingSetting = setReplyMessageMappingSetting(replyMessageMappingGroup, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(replyMessageMappingSetting);
                        return true;
                    case 86:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            notificationBaseObj = NotificationBaseObj.CREATOR.createFromParcel(parcel);
                        }
                        long notifyNotificationEvent = notifyNotificationEvent(notificationBaseObj, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(notifyNotificationEvent);
                        return true;
                    case 87:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            watchAppMappingSettings = WatchAppMappingSettings.CREATOR.createFromParcel(parcel);
                        }
                        setAutoWatchAppSettings(watchAppMappingSettings, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 88:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            backgroundConfig = BackgroundConfig.CREATOR.createFromParcel(parcel);
                        }
                        setAutoBackgroundImageConfig(backgroundConfig, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 89:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            appNotificationFilterSettings = AppNotificationFilterSettings.CREATOR.createFromParcel(parcel);
                        }
                        setAutoNotificationFilterSettings(appNotificationFilterSettings, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 90:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            userDisplayUnit = UserDisplayUnit.CREATOR.createFromParcel(parcel);
                        }
                        setImplicitDisplayUnitSettings(userDisplayUnit, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 91:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            userProfile2 = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        setImplicitDeviceConfig(userProfile2, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 92:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            userProfile = UserProfile.CREATOR.createFromParcel(parcel);
                        }
                        setAutoUserBiometricData(userProfile);
                        parcel2.writeNoException();
                        return true;
                    case 93:
                        parcel.enforceInterface(DESCRIPTOR);
                        long readCurrentWorkoutSession = readCurrentWorkoutSession(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(readCurrentWorkoutSession);
                        return true;
                    case 94:
                        parcel.enforceInterface(DESCRIPTOR);
                        long stopCurrentWorkoutSession = stopCurrentWorkoutSession(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(stopCurrentWorkoutSession);
                        return true;
                    case 95:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString15 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            location = (Location) Location.CREATOR.createFromParcel(parcel);
                        }
                        long workoutGPSDataSession = setWorkoutGPSDataSession(readString15, location);
                        parcel2.writeNoException();
                        parcel2.writeLong(workoutGPSDataSession);
                        return true;
                    case 96:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            workoutConfigData = WorkoutConfigData.CREATOR.createFromParcel(parcel);
                        }
                        setWorkoutConfig(workoutConfigData, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 97:
                        parcel.enforceInterface(DESCRIPTOR);
                        long minimumStepThresholdSession = setMinimumStepThresholdSession(parcel.readLong(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(minimumStepThresholdSession);
                        return true;
                    case 98:
                        parcel.enforceInterface(DESCRIPTOR);
                        byte[] createByteArray = parcel.createByteArray();
                        String readString16 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z8 = true;
                        }
                        long sendingEncryptedDataSession = sendingEncryptedDataSession(createByteArray, readString16, z8);
                        parcel2.writeNoException();
                        parcel2.writeLong(sendingEncryptedDataSession);
                        return true;
                    case 99:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString17 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            inactiveNudgeData = InactiveNudgeData.CREATOR.createFromParcel(parcel);
                        }
                        long deviceSetInactiveNudgeConfig = deviceSetInactiveNudgeConfig(readString17, inactiveNudgeData);
                        parcel2.writeNoException();
                        parcel2.writeLong(deviceSetInactiveNudgeConfig);
                        return true;
                    case 100:
                        parcel.enforceInterface(DESCRIPTOR);
                        resetDeviceSettingToDefault(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 101:
                        parcel.enforceInterface(DESCRIPTOR);
                        long heartRateMode = setHeartRateMode(parcel.readString(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeLong(heartRateMode);
                        return true;
                    case 102:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString18 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z7 = true;
                        }
                        long frontLightEnable = setFrontLightEnable(readString18, z7);
                        parcel2.writeNoException();
                        parcel2.writeLong(frontLightEnable);
                        return true;
                    case 103:
                        parcel.enforceInterface(DESCRIPTOR);
                        long sendServerSecretKey = sendServerSecretKey(parcel.readString(), parcel.readString(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeLong(sendServerSecretKey);
                        return true;
                    case 104:
                        parcel.enforceInterface(DESCRIPTOR);
                        long sendCurrentSecretKey = sendCurrentSecretKey(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(sendCurrentSecretKey);
                        return true;
                    case 105:
                        parcel.enforceInterface(DESCRIPTOR);
                        long onPing = onPing(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(onPing);
                        return true;
                    case 106:
                        parcel.enforceInterface(DESCRIPTOR);
                        long sendRandomKey = sendRandomKey(parcel.readString(), parcel.readString(), parcel.readInt());
                        parcel2.writeNoException();
                        parcel2.writeLong(sendRandomKey);
                        return true;
                    case 107:
                        parcel.enforceInterface(DESCRIPTOR);
                        setSecretKey(parcel.readString(), parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 108:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            localizationData = LocalizationData.CREATOR.createFromParcel(parcel);
                        }
                        setLocalizationData(localizationData, parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 109:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            workoutDetectionSetting = WorkoutDetectionSetting.CREATOR.createFromParcel(parcel);
                        }
                        long workoutDetectionSetting2 = setWorkoutDetectionSetting(workoutDetectionSetting, parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(workoutDetectionSetting2);
                        return true;
                    case 110:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString19 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            customRequest = CustomRequest.CREATOR.createFromParcel(parcel);
                        }
                        sendCustomCommand(readString19, customRequest);
                        parcel2.writeNoException();
                        return true;
                    case 111:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString20 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z6 = true;
                        }
                        confirmStopWorkout(readString20, z6);
                        parcel2.writeNoException();
                        return true;
                    case 112:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString21 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z5 = true;
                        }
                        if (parcel.readInt() != 0) {
                            watchParamsFileMapping = WatchParamsFileMapping.CREATOR.createFromParcel(parcel);
                        }
                        onSetWatchParamResponse(readString21, z5, watchParamsFileMapping);
                        parcel2.writeNoException();
                        return true;
                    case 113:
                        parcel.enforceInterface(DESCRIPTOR);
                        interruptCurrentSession(parcel.readString());
                        parcel2.writeNoException();
                        return true;
                    case 114:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString22 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z4 = true;
                        }
                        sendPushSecretKeyResponse(readString22, z4);
                        parcel2.writeNoException();
                        return true;
                    case 115:
                        parcel.enforceInterface(DESCRIPTOR);
                        long verifySecretKeySession = verifySecretKeySession(parcel.readString());
                        parcel2.writeNoException();
                        parcel2.writeLong(verifySecretKeySession);
                        return true;
                    case 116:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString23 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            z3 = true;
                        }
                        long notifyWatchAppFilesReady = notifyWatchAppFilesReady(readString23, z3);
                        parcel2.writeNoException();
                        parcel2.writeLong(notifyWatchAppFilesReady);
                        return true;
                    case 117:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            z2 = true;
                        }
                        confirmBCStatus(z2);
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    @DexIgnore
    void addLog(int i, String str, String str2) throws RemoteException;

    @DexIgnore
    void cancelPairDevice(String str) throws RemoteException;

    @DexIgnore
    void changePendingLogKey(int i, String str, int i2, String str2) throws RemoteException;

    @DexIgnore
    void confirmBCStatus(boolean z) throws RemoteException;

    @DexIgnore
    void confirmStopWorkout(String str, boolean z) throws RemoteException;

    @DexIgnore
    void connectAllButton() throws RemoteException;

    @DexIgnore
    void deleteDataFiles(String str) throws RemoteException;

    @DexIgnore
    void deleteHeartRateFiles(List<String> list, String str) throws RemoteException;

    @DexIgnore
    long deviceCancelCalibration(String str) throws RemoteException;

    @DexIgnore
    long deviceClearMapping(String str) throws RemoteException;

    @DexIgnore
    long deviceCompleteCalibration(String str) throws RemoteException;

    @DexIgnore
    void deviceDisconnect(String str) throws RemoteException;

    @DexIgnore
    long deviceForceReconnect(String str, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    long deviceGetBatteryLevel(String str) throws RemoteException;

    @DexIgnore
    long deviceGetCountDown(String str) throws RemoteException;

    @DexIgnore
    long deviceGetRssi(String str) throws RemoteException;

    @DexIgnore
    void deviceMovingHand(String str, HandCalibrationObj handCalibrationObj) throws RemoteException;

    @DexIgnore
    long deviceOta(String str, FirmwareData firmwareData, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    long devicePlayAnimation(String str) throws RemoteException;

    @DexIgnore
    long deviceReadRealTimeStep(String str) throws RemoteException;

    @DexIgnore
    long deviceSendNotification(String str, NotificationBaseObj notificationBaseObj) throws RemoteException;

    @DexIgnore
    void deviceSetAutoCountdownSetting(long j, long j2) throws RemoteException;

    @DexIgnore
    void deviceSetAutoListAlarm(List<Alarm> list) throws RemoteException;

    @DexIgnore
    void deviceSetAutoSecondTimezone(String str) throws RemoteException;

    @DexIgnore
    long deviceSetDisableCountDown(String str) throws RemoteException;

    @DexIgnore
    long deviceSetEnableCountDown(String str, long j, long j2) throws RemoteException;

    @DexIgnore
    long deviceSetInactiveNudgeConfig(String str, InactiveNudgeData inactiveNudgeData) throws RemoteException;

    @DexIgnore
    long deviceSetListAlarm(String str, List<Alarm> list) throws RemoteException;

    @DexIgnore
    long deviceSetMapping(String str, List<BLEMapping> list) throws RemoteException;

    @DexIgnore
    long deviceSetSecondTimeZone(String str, String str2) throws RemoteException;

    @DexIgnore
    long deviceSetVibrationStrength(String str, VibrationStrengthObj vibrationStrengthObj) throws RemoteException;

    @DexIgnore
    long deviceStartCalibration(String str) throws RemoteException;

    @DexIgnore
    void deviceStartScan() throws RemoteException;

    @DexIgnore
    long deviceStartSync(String str, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    void deviceStopScan() throws RemoteException;

    @DexIgnore
    long deviceUnlink(String str) throws RemoteException;

    @DexIgnore
    long deviceUpdateActivityGoals(String str, int i, int i2, int i3, boolean z) throws RemoteException;

    @DexIgnore
    long disableHeartRateNotification(String str) throws RemoteException;

    @DexIgnore
    long enableHeartRateNotification(String str) throws RemoteException;

    @DexIgnore
    int endLog(int i, String str) throws RemoteException;

    @DexIgnore
    boolean forceSwitchDeviceWithoutErase(String str) throws RemoteException;

    @DexIgnore
    void forceUpdateDeviceData(DeviceAppResponse deviceAppResponse, String str) throws RemoteException;

    @DexIgnore
    List<String> getActiveSerial() throws RemoteException;

    @DexIgnore
    List<BLEMapping> getAutoMapping(String str) throws RemoteException;

    @DexIgnore
    int getCommunicatorModeBySerial(String str) throws RemoteException;

    @DexIgnore
    MisfitDeviceProfile getDeviceProfile(String str) throws RemoteException;

    @DexIgnore
    int getGattState(String str) throws RemoteException;

    @DexIgnore
    int getHIDState(String str) throws RemoteException;

    @DexIgnore
    int[] getListActiveCommunicator() throws RemoteException;

    @DexIgnore
    List<MisfitDeviceProfile> getPairedDevice() throws RemoteException;

    @DexIgnore
    List<String> getPairedSerial() throws RemoteException;

    @DexIgnore
    List<FitnessData> getSyncData(String str) throws RemoteException;

    @DexIgnore
    void init(String str, String str2, String str3, char c, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig) throws RemoteException;

    @DexIgnore
    void interrupt(String str) throws RemoteException;

    @DexIgnore
    void interruptCurrentSession(String str) throws RemoteException;

    @DexIgnore
    boolean isLinking(String str) throws RemoteException;

    @DexIgnore
    boolean isSyncing(String str) throws RemoteException;

    @DexIgnore
    boolean isUpdatingFirmware(String str) throws RemoteException;

    @DexIgnore
    void logOut() throws RemoteException;

    @DexIgnore
    long notifyNotificationEvent(NotificationBaseObj notificationBaseObj, String str) throws RemoteException;

    @DexIgnore
    long notifyWatchAppFilesReady(String str, boolean z) throws RemoteException;

    @DexIgnore
    long onPing(String str) throws RemoteException;

    @DexIgnore
    void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) throws RemoteException;

    @DexIgnore
    long pairDevice(String str, String str2, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    long pairDeviceResponse(String str, PairingResponse pairingResponse) throws RemoteException;

    @DexIgnore
    long playVibration(String str, int i, int i2, boolean z) throws RemoteException;

    @DexIgnore
    long readCurrentWorkoutSession(String str) throws RemoteException;

    @DexIgnore
    void removeActiveSerial(String str) throws RemoteException;

    @DexIgnore
    void removePairedSerial(String str) throws RemoteException;

    @DexIgnore
    void resetDeviceSettingToDefault(String str) throws RemoteException;

    @DexIgnore
    long resetHandsToZeroDegree(String str) throws RemoteException;

    @DexIgnore
    long sendCurrentSecretKey(String str, String str2) throws RemoteException;

    @DexIgnore
    void sendCustomCommand(String str, CustomRequest customRequest) throws RemoteException;

    @DexIgnore
    void sendDeviceAppResponse(DeviceAppResponse deviceAppResponse, String str) throws RemoteException;

    @DexIgnore
    void sendMicroAppRemoteActivity(String str, DeviceAppResponse deviceAppResponse) throws RemoteException;

    @DexIgnore
    void sendMusicAppResponse(MusicResponse musicResponse, String str) throws RemoteException;

    @DexIgnore
    void sendPushSecretKeyResponse(String str, boolean z) throws RemoteException;

    @DexIgnore
    long sendRandomKey(String str, String str2, int i) throws RemoteException;

    @DexIgnore
    long sendServerSecretKey(String str, String str2, int i) throws RemoteException;

    @DexIgnore
    long sendingEncryptedDataSession(byte[] bArr, String str, boolean z) throws RemoteException;

    @DexIgnore
    void setActiveSerial(String str, String str2) throws RemoteException;

    @DexIgnore
    void setAutoBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException;

    @DexIgnore
    void setAutoComplicationAppSettings(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException;

    @DexIgnore
    void setAutoMapping(String str, List<BLEMapping> list) throws RemoteException;

    @DexIgnore
    void setAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException;

    @DexIgnore
    void setAutoUserBiometricData(UserProfile userProfile) throws RemoteException;

    @DexIgnore
    void setAutoWatchAppSettings(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException;

    @DexIgnore
    long setBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException;

    @DexIgnore
    long setComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException;

    @DexIgnore
    long setFrontLightEnable(String str, boolean z) throws RemoteException;

    @DexIgnore
    long setHeartRateMode(String str, int i) throws RemoteException;

    @DexIgnore
    void setImplicitDeviceConfig(UserProfile userProfile, String str) throws RemoteException;

    @DexIgnore
    void setImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit, String str) throws RemoteException;

    @DexIgnore
    void setLocalizationData(LocalizationData localizationData, String str) throws RemoteException;

    @DexIgnore
    long setMinimumStepThresholdSession(long j, String str) throws RemoteException;

    @DexIgnore
    long setNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException;

    @DexIgnore
    void setPairedSerial(String str, String str2) throws RemoteException;

    @DexIgnore
    long setPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) throws RemoteException;

    @DexIgnore
    long setReplyMessageMappingSetting(ReplyMessageMappingGroup replyMessageMappingGroup, String str) throws RemoteException;

    @DexIgnore
    void setSecretKey(String str, String str2) throws RemoteException;

    @DexIgnore
    long setWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException;

    @DexIgnore
    void setWorkoutConfig(WorkoutConfigData workoutConfigData, String str) throws RemoteException;

    @DexIgnore
    long setWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting, String str) throws RemoteException;

    @DexIgnore
    long setWorkoutGPSDataSession(String str, Location location) throws RemoteException;

    @DexIgnore
    void simulateDisconnection(String str, int i, int i2, int i3, int i4) throws RemoteException;

    @DexIgnore
    void simulatePusherEvent(String str, int i, int i2, int i3, int i4, int i5) throws RemoteException;

    @DexIgnore
    int startLog(int i, String str) throws RemoteException;

    @DexIgnore
    long stopCurrentWorkoutSession(String str) throws RemoteException;

    @DexIgnore
    void stopLogService(int i) throws RemoteException;

    @DexIgnore
    boolean switchActiveDevice(String str, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    long switchDeviceResponse(String str, boolean z, int i) throws RemoteException;

    @DexIgnore
    void updateActiveDeviceInfoLog(ActiveDeviceInfo activeDeviceInfo) throws RemoteException;

    @DexIgnore
    void updateAppInfo(String str) throws RemoteException;

    @DexIgnore
    void updateAppLogInfo(AppLogInfo appLogInfo) throws RemoteException;

    @DexIgnore
    void updatePercentageGoalProgress(String str, boolean z, UserProfile userProfile) throws RemoteException;

    @DexIgnore
    void updateUserId(String str) throws RemoteException;

    @DexIgnore
    long verifySecretKeySession(String str) throws RemoteException;
}
