package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Injection {
    @DexIgnore
    public static /* final */ Injection INSTANCE; // = new Injection();

    @DexIgnore
    public final FirmwareFileRepository provideFilesRepository(Context context) {
        ee7.b(context, "context");
        return FirmwareFileRepository.Companion.getInstance(context, new FirmwareFileLocalSource());
    }
}
