package com.misfit.frameworks.buttonservice.source;

import com.fossil.fb7;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface FirmwareFileSource {
    @DexIgnore
    Object downloadFirmware(String str, String str2, String str3, fb7<? super File> fb7);

    @DexIgnore
    String getFirmwareFilePath(String str);

    @DexIgnore
    boolean isDownloaded(String str, String str2);

    @DexIgnore
    byte[] readFirmware(String str);
}
