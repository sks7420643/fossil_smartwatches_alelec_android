package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.facebook.internal.Utility;
import com.fossil.ee7;
import com.fossil.ot7;
import com.fossil.x87;
import com.fossil.xs7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import io.flutter.plugin.common.StandardMessageCodec;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FirmwareUtils {
    @DexIgnore
    public static /* final */ FirmwareUtils INSTANCE; // = new FirmwareUtils();
    @DexIgnore
    public static /* final */ String TAG; // = TAG;

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        int length = bArr.length;
        String str = "";
        int i = 0;
        while (i < length) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            String num = Integer.toString((bArr[i] & 255) + StandardMessageCodec.NULL, 16);
            ee7.a((Object) num, "Integer.toString((input[\u2026() and 0xff) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                str = sb.toString();
                i++;
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        if (str != null) {
            String lowerCase = str.toLowerCase();
            ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean isLatestFirmware(String str, String str2) {
        ee7.b(str, "oldFw");
        ee7.b(str2, "latestFw");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return true;
        }
        if (new ot7(str2).compareTo(new ot7(str)) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] readFirmware(com.misfit.frameworks.buttonservice.model.FirmwareData r4, android.content.Context r5) {
        /*
            r3 = this;
            java.lang.String r0 = "firmwareData"
            com.fossil.ee7.b(r4, r0)
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = r4.getFirmwareVersion()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x0054
            boolean r1 = r4.isEmbedded()
            if (r1 == 0) goto L_0x0049
            android.content.res.Resources r5 = r5.getResources()     // Catch:{ Exception -> 0x002b }
            int r4 = r4.getRawBundleResource()     // Catch:{ Exception -> 0x002b }
            java.io.InputStream r4 = r5.openRawResource(r4)     // Catch:{ Exception -> 0x002b }
            byte[] r4 = com.fossil.rs7.b(r4)     // Catch:{ Exception -> 0x002b }
            goto L_0x0055
        L_0x002b:
            r4 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r0 = com.misfit.frameworks.buttonservice.utils.FirmwareUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "readFirmware() - e="
            r1.append(r2)
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            r5.e(r0, r4)
            goto L_0x0054
        L_0x0049:
            com.misfit.frameworks.buttonservice.source.Injection r4 = com.misfit.frameworks.buttonservice.source.Injection.INSTANCE
            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r4 = r4.provideFilesRepository(r5)
            byte[] r4 = r4.readFirmware(r0)
            goto L_0x0055
        L_0x0054:
            r4 = 0
        L_0x0055:
            if (r4 == 0) goto L_0x0058
            goto L_0x005b
        L_0x0058:
            r4 = 0
            byte[] r4 = new byte[r4]
        L_0x005b:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.utils.FirmwareUtils.readFirmware(com.misfit.frameworks.buttonservice.model.FirmwareData, android.content.Context):byte[]");
    }

    @DexIgnore
    public final boolean verifyFirmware(byte[] bArr, String str) {
        ee7.b(str, "checksum");
        if (!xs7.b(str) && bArr != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                instance.update(bArr);
                byte[] digest = instance.digest();
                ee7.a((Object) digest, "md5");
                String bytesToString = bytesToString(digest);
                String lowerCase = str.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return ee7.a((Object) lowerCase, (Object) bytesToString);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.e(str2, "Error inside " + TAG + ".verifyFirmware - e=" + e);
            }
        }
        return false;
    }
}
