package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.facebook.internal.Utility;
import com.fossil.ee7;
import com.fossil.h90;
import com.fossil.we7;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConversionUtils {
    @DexIgnore
    public static /* final */ ConversionUtils INSTANCE; // = new ConversionUtils();

    @DexIgnore
    public final String SHA1(String str) {
        ee7.b(str, "value");
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            Charset forName = Charset.forName("UTF-8");
            ee7.a((Object) forName, "Charset.forName(charsetName)");
            byte[] bytes = str.getBytes(forName);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            instance.update(bytes);
            byte[] digest = instance.digest();
            ee7.a((Object) digest, "messageDigest.digest()");
            return byteArrayToString(digest);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final String byteArrayToString(byte[] bArr) {
        ee7.b(bArr, "bytes");
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%02x", Arrays.copyOf(new Object[]{Byte.valueOf(b)}, 1));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
        }
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final float convertByteToMegaBytes(long j) {
        return ((float) (j / ((long) 1024))) / ((float) 1024);
    }

    @DexIgnore
    public final h90 getTimeFormat(Context context) {
        ee7.b(context, "context");
        if (DateFormat.is24HourFormat(context)) {
            return h90.TWENTY_FOUR;
        }
        return h90.TWELVE;
    }

    @DexIgnore
    public final int getTimezoneRawOffsetById(String str) {
        ee7.b(str, "timezoneId");
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        if (timeZone.inDaylightTime(instance.getTime())) {
            ee7.a((Object) timeZone, "timeZone");
            return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 60000;
        }
        ee7.a((Object) timeZone, "timeZone");
        return timeZone.getRawOffset() / 60000;
    }
}
