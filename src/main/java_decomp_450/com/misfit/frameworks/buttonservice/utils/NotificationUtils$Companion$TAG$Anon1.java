package com.misfit.frameworks.buttonservice.utils;

import com.fossil.bg7;
import com.fossil.me7;
import com.fossil.tc7;
import com.fossil.te7;
import com.fossil.uf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationUtils$Companion$TAG$Anon1 extends me7 {
    @DexIgnore
    public static /* final */ bg7 INSTANCE; // = new NotificationUtils$Companion$TAG$Anon1();

    @DexIgnore
    @Override // com.fossil.bg7
    public Object get(Object obj) {
        return tc7.a((NotificationUtils) obj);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public uf7 getOwner() {
        return te7.a(tc7.class, "buttonservice_release");
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
