package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import com.fossil.ee7;
import com.fossil.hg0;
import com.fossil.nh7;
import com.fossil.rs7;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppUtils {
    @DexIgnore
    public static /* final */ WatchAppUtils INSTANCE; // = new WatchAppUtils();
    @DexIgnore
    public static /* final */ String TAG;

    /*
    static {
        String name = WatchAppUtils.class.getName();
        ee7.a((Object) name, "WatchAppUtils::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public final hg0[] getWatchAppFiles(Context context) {
        ee7.b(context, "context");
        ArrayList arrayList = new ArrayList();
        File[] listFiles = new File(FileUtils.getDirectory(context, FileType.WATCH_APP)).listFiles();
        Boolean bool = SharePreferencesUtils.getInstance(context).getBoolean(SharePreferencesUtils.BC_STATUS, true);
        if (listFiles != null) {
            for (File file : listFiles) {
                FileInputStream fileInputStream = null;
                try {
                    FileInputStream fileInputStream2 = new FileInputStream(file);
                    try {
                        byte[] b = rs7.b(fileInputStream2);
                        ee7.a((Object) b, "byteArray");
                        hg0 hg0 = new hg0(b);
                        ee7.a((Object) bool, "isBCOn");
                        if (bool.booleanValue()) {
                            arrayList.add(hg0);
                        } else if (!nh7.a((CharSequence) hg0.getBundleId(), (CharSequence) "buddyChallengeApp", true)) {
                            arrayList.add(hg0);
                        }
                        rs7.a((InputStream) fileInputStream2);
                    } catch (Exception e) {
                        e = e;
                        fileInputStream = fileInputStream2;
                        try {
                            FLogger.INSTANCE.getLocal().e(TAG, "getWatchAppFiles, exception=" + e);
                            rs7.a((InputStream) fileInputStream);
                        } catch (Throwable th) {
                            th = th;
                            rs7.a((InputStream) fileInputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileInputStream = fileInputStream2;
                        rs7.a((InputStream) fileInputStream);
                        throw th;
                    }
                } catch (Exception e2) {
                    e = e2;
                    FLogger.INSTANCE.getLocal().e(TAG, "getWatchAppFiles, exception=" + e);
                    rs7.a((InputStream) fileInputStream);
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "file list=" + arrayList);
        Object[] array = arrayList.toArray(new hg0[0]);
        if (array != null) {
            return (hg0[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
