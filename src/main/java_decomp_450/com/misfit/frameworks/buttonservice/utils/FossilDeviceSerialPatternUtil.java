package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilDeviceSerialPatternUtil {

    @DexIgnore
    public enum BRAND {
        CHAPS('C'),
        EA('E'),
        DIESEL('D'),
        FOSSIL('F'),
        KARL('L'),
        MICHAEL_KORS('M'),
        KATE_SPADE('K'),
        SKAGEN('S'),
        ARMANI_EXCHANGE('X'),
        TORY_BURCH('T'),
        RELIC('R'),
        MARC_JACOBS('J'),
        DKNY('Y'),
        MICHELE('H'),
        UNIVERSAL('U'),
        CITIZEN('Z'),
        UNKNOWN('A');
        
        @DexIgnore
        public char prefix;

        @DexIgnore
        public BRAND(char c) {
            this.prefix = c;
        }

        @DexIgnore
        public static BRAND fromPrefix(char c) {
            BRAND[] values = values();
            for (BRAND brand : values) {
                if (brand.getPrefix() == c) {
                    return brand;
                }
            }
            return UNKNOWN;
        }

        @DexIgnore
        public char getPrefix() {
            return this.prefix;
        }
    }

    @DexIgnore
    public enum DEVICE {
        RMM('C'),
        SAM('W'),
        Q_MOTION('B'),
        UNKNOWN('U'),
        FAKE_SAM('S'),
        SAM_SLIM('L'),
        SAM_MINI('M'),
        SE0('Z'),
        DIANA('D'),
        IVY('V');
        
        @DexIgnore
        public char prefix;

        @DexIgnore
        public DEVICE(char c) {
            this.prefix = c;
        }

        @DexIgnore
        public static DEVICE fromPrefix(char c) {
            DEVICE[] values = values();
            for (DEVICE device : values) {
                if (device.getPrefix() == c) {
                    return device;
                }
            }
            return UNKNOWN;
        }

        @DexIgnore
        public char getPrefix() {
            return this.prefix;
        }
    }

    @DexIgnore
    public static BRAND getBrandBySerial(String str) {
        char c;
        if (TextUtils.isEmpty(str)) {
            return BRAND.UNKNOWN;
        }
        if (isQMotion(str)) {
            c = str.charAt(1);
        } else {
            c = str.charAt(2);
        }
        return BRAND.fromPrefix(c);
    }

    @DexIgnore
    public static DEVICE getDeviceBySerial(String str) {
        if (TextUtils.isEmpty(str)) {
            return DEVICE.UNKNOWN;
        }
        return DEVICE.fromPrefix(str.toUpperCase().charAt(0));
    }

    @DexIgnore
    public static String getStyleCode(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 5) {
            return "";
        }
        if (isQMotion(str)) {
            return str.substring(3, 5);
        }
        return str.substring(3, 6);
    }

    @DexIgnore
    public static boolean isDianaDevice(String str) {
        DEVICE deviceBySerial = getDeviceBySerial(str);
        return deviceBySerial == DEVICE.DIANA || deviceBySerial == DEVICE.IVY;
    }

    @DexIgnore
    public static boolean isGen1Device(String str) {
        return !TextUtils.isEmpty(str) && str.length() > 10;
    }

    @DexIgnore
    public static boolean isHybridSmartWatchDevice(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        DEVICE deviceBySerial = getDeviceBySerial(str);
        if (deviceBySerial == DEVICE.SAM || deviceBySerial == DEVICE.FAKE_SAM || deviceBySerial == DEVICE.SAM_SLIM || deviceBySerial == DEVICE.SAM_MINI || deviceBySerial == DEVICE.SE0 || deviceBySerial == DEVICE.DIANA || deviceBySerial == DEVICE.IVY) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isQMotion(String str) {
        if (!TextUtils.isEmpty(str) && getDeviceBySerial(str) == DEVICE.Q_MOTION) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isRmmDevice(String str) {
        if (!TextUtils.isEmpty(str) && getDeviceBySerial(str) == DEVICE.RMM) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isSamDevice(String str) {
        if (!TextUtils.isEmpty(str) && getDeviceBySerial(str) == DEVICE.SAM) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isSamMiniDevice(String str) {
        return !TextUtils.isEmpty(str) && getDeviceBySerial(str) == DEVICE.SAM_MINI;
    }

    @DexIgnore
    public static boolean isSamSlimDevice(String str) {
        if (!TextUtils.isEmpty(str) && getDeviceBySerial(str) == DEVICE.SAM_SLIM) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isSamSlimOrMiniDevice(String str) {
        DEVICE deviceBySerial = getDeviceBySerial(str);
        return deviceBySerial == DEVICE.SAM_SLIM || deviceBySerial == DEVICE.SAM_MINI;
    }

    @DexIgnore
    public static boolean isTrackerDevice(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        DEVICE deviceBySerial = getDeviceBySerial(str);
        if (deviceBySerial == DEVICE.RMM || deviceBySerial == DEVICE.Q_MOTION) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean isVibeSupportDevice(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        DEVICE deviceBySerial = getDeviceBySerial(str);
        if (deviceBySerial == DEVICE.SAM || deviceBySerial == DEVICE.SE0 || deviceBySerial == DEVICE.FAKE_SAM || deviceBySerial == DEVICE.Q_MOTION || deviceBySerial == DEVICE.SAM_MINI) {
            return true;
        }
        return false;
    }
}
