package com.misfit.frameworks.buttonservice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import com.fossil.ee7;
import com.fossil.n87;
import com.fossil.o6;
import com.fossil.o87;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE; // = "com.fossil.wearables.fossil.channel.defaultService";
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE_NAME; // = "Sync service";
    @DexIgnore
    public static /* final */ int DEVICE_STATUS_NOTIFICATION_ID; // = 1;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ n87 instance$delegate; // = o87.a(NotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static o6.e mNotificationBuilder;
    @DexIgnore
    public String mLastContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final NotificationUtils getInstance() {
            n87 access$getInstance$cp = NotificationUtils.instance$delegate;
            Companion companion = NotificationUtils.Companion;
            return (NotificationUtils) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Singleton {
        @DexIgnore
        public static /* final */ Singleton INSTANCE; // = new Singleton();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ NotificationUtils f0INSTANCE; // = new NotificationUtils(null);

        @DexIgnore
        public final NotificationUtils getINSTANCE() {
            return f0INSTANCE;
        }
    }

    /*
    static {
        String simpleName = NotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        ee7.a((Object) simpleName, "NotificationUtils::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public NotificationUtils() {
        this.mLastContent = "";
    }

    @DexIgnore
    private final Notification createDeviceStatusNotification(Context context, String str) {
        ArrayList<o6.a> arrayList;
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new o6.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        o6.e eVar = mNotificationBuilder;
        if (!(eVar == null || (arrayList = eVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            o6.e eVar2 = mNotificationBuilder;
            if (eVar2 != null) {
                eVar2.b((CharSequence) context.getResources().getString(R.string.app_name));
                eVar2.a(Constants.SERVICE);
                eVar2.c(false);
                eVar2.a((CharSequence) str);
                eVar2.a((o6.f) null);
                eVar2.f(R.drawable.ic_launcher_transparent);
                eVar2.d(false);
                eVar2.e(-2);
                Notification a = eVar2.a();
                ee7.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            ee7.a();
            throw null;
        }
        o6.e eVar3 = mNotificationBuilder;
        if (eVar3 != null) {
            eVar3.f(R.drawable.ic_launcher_transparent);
            eVar3.a(Constants.SERVICE);
            eVar3.b((CharSequence) str);
            eVar3.a((o6.f) null);
            eVar3.c(false);
            eVar3.d(false);
            eVar3.e(-2);
            Notification a2 = eVar3.a();
            ee7.a((Object) a2, "builder.build()");
            return a2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    private final Notification createDeviceStatusWithRichTextNotification(Context context, String str, String str2) {
        ArrayList<o6.a> arrayList;
        if (!(str2.length() == 0)) {
            this.mLastContent = str2;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new o6.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        o6.e eVar = mNotificationBuilder;
        if (!(eVar == null || (arrayList = eVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            o6.e eVar2 = mNotificationBuilder;
            if (eVar2 != null) {
                eVar2.b((CharSequence) context.getResources().getString(R.string.app_name));
                eVar2.a(Constants.SERVICE);
                o6.c cVar = new o6.c();
                cVar.a(str2);
                eVar2.a(cVar);
                eVar2.c(false);
                eVar2.a((CharSequence) str);
                eVar2.f(R.drawable.ic_launcher_transparent);
                eVar2.d(false);
                eVar2.e(-2);
                Notification a = eVar2.a();
                ee7.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            ee7.a();
            throw null;
        }
        o6.e eVar3 = mNotificationBuilder;
        if (eVar3 != null) {
            eVar3.f(R.drawable.ic_launcher_transparent);
            eVar3.a(Constants.SERVICE);
            o6.c cVar2 = new o6.c();
            cVar2.a(str2);
            eVar3.a(cVar2);
            eVar3.b((CharSequence) str);
            eVar3.c(false);
            eVar3.d(false);
            eVar3.e(-2);
            Notification a2 = eVar3.a();
            ee7.a((Object) a2, "builder.build()");
            return a2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public static final NotificationUtils getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    private final void riseNotification(NotificationManager notificationManager, int i, Notification notification) {
        try {
            notificationManager.notify(i, notification);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    public static /* synthetic */ void updateNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        notificationUtils.updateNotification(context, i, str, (i2 & 8) != 0 ? null : pendingIntent, (i2 & 16) != 0 ? null : pendingIntent2, z);
    }

    @DexIgnore
    public final Notification createDefaultDeviceStatusNotification(Context context, String str, String str2) {
        ee7.b(context, "context");
        ee7.b(str, "content");
        ee7.b(str2, "subText");
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        o6.e eVar = new o6.e(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        ArrayList<o6.a> arrayList = eVar.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            eVar.b((CharSequence) context.getResources().getString(R.string.app_name));
            eVar.a((CharSequence) str2);
            eVar.f(R.drawable.ic_launcher_transparent);
            eVar.a(Constants.SERVICE);
            eVar.c(false);
            eVar.d(false);
            eVar.a((o6.f) null);
            eVar.e(-2);
            Notification a = eVar.a();
            ee7.a((Object) a, "builder\n                \u2026                 .build()");
            return a;
        }
        eVar.f(R.drawable.ic_launcher_transparent);
        eVar.a(Constants.SERVICE);
        eVar.b((CharSequence) str2);
        eVar.a((o6.f) null);
        eVar.c(false);
        eVar.d(false);
        eVar.e(-2);
        Notification a2 = eVar.a();
        ee7.a((Object) a2, "builder\n                \u2026                 .build()");
        return a2;
    }

    @DexIgnore
    public final NotificationManager createNotificationChannels(Context context) {
        ee7.b(context, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "createNotificationChannels - context=" + context);
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "createNotificationChannels - currentChannel=" + notificationChannel);
                if (notificationChannel == null) {
                    NotificationChannel notificationChannel2 = new NotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE, DEFAULT_CHANNEL_WATCH_SERVICE_NAME, 1);
                    notificationChannel2.setShowBadge(false);
                    notificationManager.createNotificationChannel(notificationChannel2);
                }
            }
            return notificationManager;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void startForegroundNotification(Context context, Service service, String str, String str2, boolean z) {
        ee7.b(context, "context");
        ee7.b(service, Constants.SERVICE);
        ee7.b(str, "content");
        ee7.b(str2, "subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "Service Tracking - startForegroundNotification - context=" + context + ',' + " service=" + service + ", content=" + str + ", subText=" + str2 + ", isStopForeground = " + z);
        createNotificationChannels(context);
        service.startForeground(1, createDefaultDeviceStatusNotification(context, str, str2));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Service Tracking - startForegroundNotification done bring to foreground for service ");
        sb.append(service);
        local2.d(str4, sb.toString());
        if (z) {
            FLogger.INSTANCE.getLocal().d(TAG, "Service Tracking - startForegroundNotification() - stop foreground service and remove notification");
            service.stopForeground(true);
        }
    }

    @DexIgnore
    public final void updateNotification(Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "updateNotification - context=" + context + ", content=" + str + " notificationId=" + i + ", isReset=" + z);
        if (!(str.length() == 0) || z) {
            this.mLastContent = str;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusNotification(context, str));
    }

    @DexIgnore
    public final void updateRichTextNotification(Context context, int i, String str, String str2, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "title");
        ee7.b(str2, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "updateNotification - context=" + context + ", title=" + str + ", content=" + str2 + ", notificationId=" + i + ", isReset=" + z);
        if ((str2.length() > 0) || z) {
            this.mLastContent = str2;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusWithRichTextNotification(context, str, str2));
    }

    @DexIgnore
    public /* synthetic */ NotificationUtils(zd7 zd7) {
        this();
    }
}
