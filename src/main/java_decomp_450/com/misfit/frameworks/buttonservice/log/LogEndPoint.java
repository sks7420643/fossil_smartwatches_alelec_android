package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.mh7;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPoint {
    @DexIgnore
    public static /* final */ LogEndPoint INSTANCE; // = new LogEndPoint();
    @DexIgnore
    public static LogApiService logApiService;

    @DexIgnore
    public final LogApiService getLogApiService() {
        return logApiService;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4) {
        ee7.b(str, "logBrandName");
        ee7.b(str2, "logBaseUrl");
        ee7.b(str3, "accessKey");
        ee7.b(str4, "secretKey");
        if ((!mh7.a((CharSequence) str2)) && (!mh7.a((CharSequence) str3)) && (!mh7.a((CharSequence) str4))) {
            if (!mh7.a(str2, "/", false, 2, null)) {
                str2 = str2 + "/";
            }
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(LogEndPoint$init$httpLogInterceptor$Anon1.INSTANCE);
            httpLoggingInterceptor.a(ee7.a("release", "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
            Retrofit.b bVar = new Retrofit.b();
            bVar.a(str2);
            bVar.a(GsonConverterFactory.a());
            OkHttpClient.b bVar2 = new OkHttpClient.b();
            bVar2.a(new LogInterceptor(str, str3, str4));
            bVar2.a(httpLoggingInterceptor);
            bVar.a(bVar2.a());
            logApiService = (LogApiService) bVar.a().a(LogApiService.class);
        }
    }

    @DexIgnore
    public final void setLogApiService(LogApiService logApiService2) {
        logApiService = logApiService2;
    }
}
