package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.mo7;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class RepoResponse<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final <T> Failure<T> create(Throwable th) {
            ee7.b(th, "error");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("create=");
            th.printStackTrace();
            sb.append(i97.a);
            local.d("RepoResponse", sb.toString());
            if (th instanceof SocketTimeoutException) {
                return new Failure<>(MFNetworkReturnCode.CLIENT_TIMEOUT, null, th, null, 8, null);
            }
            return new Failure<>(601, null, th, null, 8, null);
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final <T> RepoResponse<T> create(fv7<T> fv7) {
            String str;
            String str2;
            String str3;
            ee7.b(fv7, "response");
            if (fv7.d()) {
                return new Success(fv7.a());
            }
            int b = fv7.b();
            if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(b));
                mo7 c = fv7.c();
                if (c == null || (str = c.string()) == null) {
                    str = fv7.e();
                }
                serverError.setMessage(str);
                return new Failure(b, serverError, null, null, 8, null);
            }
            mo7 c2 = fv7.c();
            if (c2 == null || (str2 = c2.string()) == null) {
                str2 = fv7.e();
            }
            try {
                ServerError serverError2 = (ServerError) new Gson().a(str2, (Class) ServerError.class);
                if (serverError2 != null) {
                    Integer code = serverError2.getCode();
                    if (code != null) {
                        if (code.intValue() == 0) {
                        }
                    }
                    return new Failure(fv7.b(), serverError2, null, null, 8, null);
                }
                return new Failure(fv7.b(), null, null, str2);
            } catch (Exception unused) {
                mo7 c3 = fv7.c();
                if (c3 == null || (str3 = c3.string()) == null) {
                    str3 = fv7.e();
                }
                return new Failure(fv7.b(), new ServerError(b, str3), null, null, 8, null);
            }
        }
    }

    @DexIgnore
    public RepoResponse() {
    }

    @DexIgnore
    public /* synthetic */ RepoResponse(zd7 zd7) {
        this();
    }
}
