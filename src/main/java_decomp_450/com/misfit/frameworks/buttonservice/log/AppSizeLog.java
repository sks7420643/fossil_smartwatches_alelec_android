package com.misfit.frameworks.buttonservice.log;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppSizeLog {
    @DexIgnore
    @te4("app")
    public AppLog app;
    @DexIgnore
    @te4("sdk")
    public SDKLog sdk;

    @DexIgnore
    public AppSizeLog() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public AppSizeLog(SDKLog sDKLog, AppLog appLog) {
        ee7.b(sDKLog, "sdk");
        ee7.b(appLog, "app");
        this.sdk = sDKLog;
        this.app = appLog;
    }

    @DexIgnore
    public static /* synthetic */ AppSizeLog copy$default(AppSizeLog appSizeLog, SDKLog sDKLog, AppLog appLog, int i, Object obj) {
        if ((i & 1) != 0) {
            sDKLog = appSizeLog.sdk;
        }
        if ((i & 2) != 0) {
            appLog = appSizeLog.app;
        }
        return appSizeLog.copy(sDKLog, appLog);
    }

    @DexIgnore
    public final SDKLog component1() {
        return this.sdk;
    }

    @DexIgnore
    public final AppLog component2() {
        return this.app;
    }

    @DexIgnore
    public final AppSizeLog copy(SDKLog sDKLog, AppLog appLog) {
        ee7.b(sDKLog, "sdk");
        ee7.b(appLog, "app");
        return new AppSizeLog(sDKLog, appLog);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AppSizeLog)) {
            return false;
        }
        AppSizeLog appSizeLog = (AppSizeLog) obj;
        return ee7.a(this.sdk, appSizeLog.sdk) && ee7.a(this.app, appSizeLog.app);
    }

    @DexIgnore
    public final AppLog getApp() {
        return this.app;
    }

    @DexIgnore
    public final SDKLog getSdk() {
        return this.sdk;
    }

    @DexIgnore
    public int hashCode() {
        SDKLog sDKLog = this.sdk;
        int i = 0;
        int hashCode = (sDKLog != null ? sDKLog.hashCode() : 0) * 31;
        AppLog appLog = this.app;
        if (appLog != null) {
            i = appLog.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setApp(AppLog appLog) {
        ee7.b(appLog, "<set-?>");
        this.app = appLog;
    }

    @DexIgnore
    public final void setSdk(SDKLog sDKLog) {
        ee7.b(sDKLog, "<set-?>");
        this.sdk = sDKLog;
    }

    @DexIgnore
    public String toString() {
        return "AppSizeLog(sdk=" + this.sdk + ", app=" + this.app + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AppSizeLog(SDKLog sDKLog, AppLog appLog, int i, zd7 zd7) {
        this((i & 1) != 0 ? new SDKLog(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 15, null) : sDKLog, (i & 2) != 0 ? new AppLog(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 7, null) : appLog);
    }
}
