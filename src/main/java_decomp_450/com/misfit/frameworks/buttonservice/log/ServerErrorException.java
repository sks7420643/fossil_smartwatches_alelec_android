package com.misfit.frameworks.buttonservice.log;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerErrorException extends IOException {
    @DexIgnore
    public /* final */ ServerError mServerError;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ServerErrorException(com.misfit.frameworks.buttonservice.log.ServerError r2) {
        /*
            r1 = this;
            java.lang.String r0 = "mServerError"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = r2.getUserMessage()
            if (r0 == 0) goto L_0x000c
            goto L_0x0010
        L_0x000c:
            java.lang.String r0 = r2.getMessage()
        L_0x0010:
            if (r0 == 0) goto L_0x0013
            goto L_0x0015
        L_0x0013:
            java.lang.String r0 = ""
        L_0x0015:
            r1.<init>(r0)
            r1.mServerError = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.ServerErrorException.<init>(com.misfit.frameworks.buttonservice.log.ServerError):void");
    }

    @DexIgnore
    public final ServerError getServerError() {
        return this.mServerError;
    }
}
