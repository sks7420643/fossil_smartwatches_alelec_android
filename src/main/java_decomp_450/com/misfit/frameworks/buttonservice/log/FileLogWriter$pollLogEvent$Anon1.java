package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.FileLogWriter$pollLogEvent$1", f = "FileLogWriter.kt", l = {133}, m = "invokeSuspend")
public final class FileLogWriter$pollLogEvent$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FileLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileLogWriter$pollLogEvent$Anon1(FileLogWriter fileLogWriter, fb7 fb7) {
        super(2, fb7);
        this.this$0 = fileLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        FileLogWriter$pollLogEvent$Anon1 fileLogWriter$pollLogEvent$Anon1 = new FileLogWriter$pollLogEvent$Anon1(this.this$0, fb7);
        fileLogWriter$pollLogEvent$Anon1.p$ = (yi7) obj;
        return fileLogWriter$pollLogEvent$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((FileLogWriter$pollLogEvent$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0115, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0116, code lost:
        com.fossil.hc7.a(r5, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0119, code lost:
        throw r1;
     */
    @DexIgnore
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r9.label
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0020
            if (r1 != r2) goto L_0x0018
            java.lang.Object r0 = r9.L$1
            com.fossil.kn7 r0 = (com.fossil.kn7) r0
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x0039
        L_0x0018:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0020:
            com.fossil.t87.a(r10)
            com.fossil.yi7 r10 = r9.p$
            com.misfit.frameworks.buttonservice.log.FileLogWriter r1 = r9.this$0
            com.fossil.kn7 r1 = r1.mFileLogWriterMutex
            r9.L$0 = r10
            r9.L$1 = r1
            r9.label = r2
            java.lang.Object r10 = r1.a(r3, r9)
            if (r10 != r0) goto L_0x0038
            return r0
        L_0x0038:
            r0 = r1
        L_0x0039:
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0     // Catch:{ all -> 0x0130 }
            java.lang.String r10 = r10.directoryPath     // Catch:{ all -> 0x0130 }
            boolean r10 = com.fossil.mh7.a(r10)     // Catch:{ all -> 0x0130 }
            if (r10 != 0) goto L_0x0128
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0     // Catch:{ Exception -> 0x011e }
            com.fossil.we7 r1 = com.fossil.we7.a     // Catch:{ Exception -> 0x011e }
            java.lang.String r1 = "app_log_%s.txt"
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x011e }
            r5 = 0
            java.lang.Integer r6 = com.fossil.pb7.a(r5)     // Catch:{ Exception -> 0x011e }
            r4[r5] = r6     // Catch:{ Exception -> 0x011e }
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r4, r2)     // Catch:{ Exception -> 0x011e }
            java.lang.String r1 = java.lang.String.format(r1, r4)     // Catch:{ Exception -> 0x011e }
            java.lang.String r4 = "java.lang.String.format(format, *args)"
            com.fossil.ee7.a(r1, r4)     // Catch:{ Exception -> 0x011e }
            java.lang.String r10 = r10.getFilePath(r1)     // Catch:{ Exception -> 0x011e }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x011e }
            r1.<init>(r10)     // Catch:{ Exception -> 0x011e }
            java.io.File r10 = r1.getParentFile()     // Catch:{ Exception -> 0x011e }
            if (r10 == 0) goto L_0x0079
            boolean r10 = r10.exists()     // Catch:{ Exception -> 0x011e }
            java.lang.Boolean r10 = com.fossil.pb7.a(r10)     // Catch:{ Exception -> 0x011e }
            goto L_0x007a
        L_0x0079:
            r10 = r3
        L_0x007a:
            if (r10 == 0) goto L_0x011a
            boolean r10 = r10.booleanValue()     // Catch:{ Exception -> 0x011e }
            if (r10 != 0) goto L_0x0090
            java.io.File r10 = r1.getParentFile()     // Catch:{ Exception -> 0x011e }
            if (r10 == 0) goto L_0x008c
            r10.mkdirs()     // Catch:{ Exception -> 0x011e }
            goto L_0x0090
        L_0x008c:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x011e }
            throw r3
        L_0x0090:
            boolean r10 = r1.exists()
            r4 = 5242880(0x500000, float:7.34684E-39)
            if (r10 != 0) goto L_0x009c
            r1.createNewFile()
            goto L_0x00aa
        L_0x009c:
            long r5 = r1.length()
            long r7 = (long) r4
            int r10 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r10 < 0) goto L_0x00aa
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            r10.rotateFiles()
        L_0x00aa:
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue r10 = r10.logEventQueue
            java.lang.Object r10 = r10.poll()
            java.lang.String r10 = (java.lang.String) r10
            if (r10 == 0) goto L_0x0128
            java.io.FileOutputStream r5 = new java.io.FileOutputStream
            r5.<init>(r1, r2)
            java.nio.charset.Charset r2 = com.fossil.sg7.a     // Catch:{ all -> 0x0113 }
            byte[] r10 = r10.getBytes(r2)     // Catch:{ all -> 0x0113 }
            java.lang.String r2 = "(this as java.lang.String).getBytes(charset)"
            com.fossil.ee7.a(r10, r2)     // Catch:{ all -> 0x0113 }
            r5.write(r10)     // Catch:{ all -> 0x0113 }
            r5.flush()     // Catch:{ all -> 0x0113 }
            com.fossil.i97 r10 = com.fossil.i97.a     // Catch:{ all -> 0x0113 }
            com.fossil.hc7.a(r5, r3)
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            com.misfit.frameworks.buttonservice.log.FileDebugOption r10 = r10.debugOption
            if (r10 == 0) goto L_0x00e9
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            int r2 = r10.mCount
            int r5 = r2 + 1
            r10.mCount = r5
            com.fossil.pb7.a(r2)
        L_0x00e9:
            long r1 = r1.length()
            long r4 = (long) r4
            int r10 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r10 < 0) goto L_0x00f7
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            r10.rotateFiles()
        L_0x00f7:
            com.misfit.frameworks.buttonservice.log.FileLogWriter r10 = r9.this$0
            com.misfit.frameworks.buttonservice.log.FileDebugOption r10 = r10.debugOption
            if (r10 == 0) goto L_0x0128
            com.misfit.frameworks.buttonservice.log.FileLogWriter r1 = r9.this$0
            int r1 = r1.mCount
            int r2 = r10.getMaximumLog()
            if (r1 != r2) goto L_0x0128
            com.misfit.frameworks.buttonservice.log.IFinishCallback r10 = r10.getCallback()
            r10.onFinish()
            goto L_0x0128
        L_0x0113:
            r10 = move-exception
            throw r10     // Catch:{ all -> 0x0115 }
        L_0x0115:
            r1 = move-exception
            com.fossil.hc7.a(r5, r10)
            throw r1
        L_0x011a:
            com.fossil.ee7.a()
            throw r3
        L_0x011e:
            r10 = move-exception
            java.lang.String r10 = r10.toString()
            java.io.PrintStream r1 = java.lang.System.out
            r1.print(r10)
        L_0x0128:
            com.fossil.i97 r10 = com.fossil.i97.a
            r0.a(r3)
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        L_0x0130:
            r10 = move-exception
            r0.a(r3)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.FileLogWriter$pollLogEvent$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
