package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.ik7;
import com.fossil.nb7;
import com.fossil.nh7;
import com.fossil.qj7;
import com.fossil.te4;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.OtaDetailLog;
import com.misfit.frameworks.buttonservice.log.model.RemoveDeviceLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.PinObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteFLogger implements IRemoteFLogger, BufferLogWriter.IBufferLogCallback, DBLogWriter.IDBLogWriterCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String LOG_MESSAGE_INTENT_ACTION; // = "fossil.log.action.messages";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_END_SESSION; // = "action:end_session";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_ERROR_RECORDED; // = "action:error_recorded";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FLUSH; // = "action:flush_data";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FULL_BUFFER; // = "action:full_buffer";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_KEY; // = "action";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_START_SESSION; // = "action:start_session";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_ERROR_CODE; // = "param:error";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SERIAL; // = "param:serial";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SUMMARY_KEY; // = "param:summary_key";
    @DexIgnore
    public static /* final */ String MESSAGE_SENDER_KEY; // = "sender";
    @DexIgnore
    public static /* final */ String MESSAGE_TARGET_KEY; // = "target";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveDeviceInfo activeDeviceInfo; // = new ActiveDeviceInfo("", "", "");
    @DexIgnore
    public AppLogInfo appLogInfo; // = new AppLogInfo("", "", "", "", "", "", "");
    @DexIgnore
    public BufferLogWriter bufferLogWriter;
    @DexIgnore
    public Context context;
    @DexIgnore
    public DBLogWriter dbLogWriter;
    @DexIgnore
    public String floggerName; // = "";
    @DexIgnore
    public int flushLogTimeThreshold; // = LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public volatile boolean isFlushing;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public boolean isPrintToConsole; // = true;
    @DexIgnore
    public long lastSyncTime;
    @DexIgnore
    public /* final */ BroadcastReceiver mMessageBroadcastReceiver; // = new RemoteFLogger$mMessageBroadcastReceiver$Anon1(this);
    @DexIgnore
    public IRemoteLogWriter remoteLogWriter;
    @DexIgnore
    public SessionDetailInfo sessionDetailInfo; // = new SessionDetailInfo(-1, 0, 0);
    @DexIgnore
    public /* final */ HashMap<String, SessionSummary> summarySessionMap; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ErrorEvent {
        @DexIgnore
        public /* final */ FLogger.Component component;
        @DexIgnore
        public /* final */ String error;
        @DexIgnore
        public /* final */ String errorCode;
        @DexIgnore
        public /* final */ String step;

        @DexIgnore
        public ErrorEvent(String str, String str2, FLogger.Component component2, String str3) {
            ee7.b(str, "errorCode");
            ee7.b(str2, "step");
            ee7.b(component2, "component");
            ee7.b(str3, "error");
            this.errorCode = str;
            this.step = str2;
            this.component = component2;
            this.error = str3;
        }

        @DexIgnore
        public final FLogger.Component getComponent() {
            return this.component;
        }

        @DexIgnore
        public final String getError() {
            return this.error;
        }

        @DexIgnore
        public final String getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getStep() {
            return this.step;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = new Gson().a(this);
            ee7.a((Object) a, "Gson().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }
    }

    @DexIgnore
    public enum MessageTarget {
        TO_ALL_FLOGGER(1),
        TO_MAIN_FLOGGER(2),
        TO_ALL_EXCEPT_MAIN_FLOGGER(3);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MessageTarget fromValue(int i) {
                MessageTarget messageTarget;
                MessageTarget[] values = MessageTarget.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        messageTarget = null;
                        break;
                    }
                    messageTarget = values[i2];
                    if (messageTarget.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return messageTarget != null ? messageTarget : MessageTarget.TO_ALL_FLOGGER;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public MessageTarget(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SessionSummary {
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        @te4("details")
        public Object details;
        @DexIgnore
        @te4("end_timestamp")
        public long endTimeStamp;
        @DexIgnore
        @te4("errors")
        public /* final */ List<String> errors; // = new ArrayList();
        @DexIgnore
        @te4("failure_reason")
        public String failureReason; // = "";
        @DexIgnore
        @te4("is_success")
        public boolean isSuccess; // = true;
        @DexIgnore
        @te4("start_timestamp")
        public long startTimeStamp;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final String getSummaryKey(String str, FLogger.Session session) {
                ee7.b(str, "serial");
                ee7.b(session, Constants.SESSION);
                return str + ':' + session;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public SessionSummary(long j, long j2) {
            this.startTimeStamp = j;
            this.endTimeStamp = j2;
        }

        @DexIgnore
        public final Object getDetails() {
            return this.details;
        }

        @DexIgnore
        public final long getEndTimeStamp() {
            return this.endTimeStamp;
        }

        @DexIgnore
        public final List<String> getErrors() {
            return this.errors;
        }

        @DexIgnore
        public final String getFailureReason() {
            return this.failureReason;
        }

        @DexIgnore
        public final long getStartTimeStamp() {
            return this.startTimeStamp;
        }

        @DexIgnore
        public final boolean isSuccess() {
            return this.isSuccess;
        }

        @DexIgnore
        public final void setDetails(Object obj) {
            this.details = obj;
        }

        @DexIgnore
        public final void setEndTimeStamp(long j) {
            this.endTimeStamp = j;
        }

        @DexIgnore
        public final void setStartTimeStamp(long j) {
            this.startTimeStamp = j;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = FLogUtils.INSTANCE.getGsonForLogEvent().a(this);
            ee7.a((Object) a, "FLogUtils.getGsonForLogEvent().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }

        @DexIgnore
        public final void update(int i) {
            this.isSuccess = i == 0;
            this.failureReason = true ^ this.errors.isEmpty() ? (String) ea7.f((List) this.errors) : "";
            if (!this.isSuccess && this.errors.isEmpty()) {
                this.errors.add(String.valueOf(i));
                this.failureReason = (String) ea7.f((List) this.errors);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = RemoteFLogger.TAG;
            local.d(access$getTAG$cp, "failureReason " + this.failureReason + " errors " + this.errors + " finalCode " + i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;

        /*
        static {
            int[] iArr = new int[MessageTarget.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MessageTarget.TO_MAIN_FLOGGER.ordinal()] = 1;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_FLOGGER.ordinal()] = 2;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER.ordinal()] = 3;
            int[] iArr2 = new int[FLogger.LogLevel.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[FLogger.LogLevel.DEBUG.ordinal()] = 1;
            $EnumSwitchMapping$1[FLogger.LogLevel.INFO.ordinal()] = 2;
            $EnumSwitchMapping$1[FLogger.LogLevel.ERROR.ordinal()] = 3;
            $EnumSwitchMapping$1[FLogger.LogLevel.SUMMARY.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        String name = RemoteFLogger.class.getName();
        ee7.a((Object) name, "RemoteFLogger::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getInternalIntentAction() {
        Context context2 = this.context;
        return ee7.a(context2 != null ? context2.getPackageName() : null, (Object) LOG_MESSAGE_INTENT_ACTION);
    }

    @DexIgnore
    private final Object getSessionDetails(String str, String str2) {
        if (nh7.a((CharSequence) str, (CharSequence) FLogger.Session.SYNC.name(), false, 2, (Object) null)) {
            return this.sessionDetailInfo;
        }
        if (nh7.a((CharSequence) str, (CharSequence) FLogger.Session.OTA.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        if (nh7.a((CharSequence) str, (CharSequence) FLogger.Session.PAIR.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        return nh7.a(str, FLogger.Session.REMOVE_DEVICE.name(), false, 2, null) ? new RemoveDeviceLog(str2) : "";
    }

    @DexIgnore
    private final void registerBroadcastMessages(Context context2) {
        context2.registerReceiver(this.mMessageBroadcastReceiver, new IntentFilter(getInternalIntentAction()));
    }

    @DexIgnore
    private final void sendInternalMessage(String str, String str2, MessageTarget messageTarget, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(getInternalIntentAction());
        intent.putExtra(MESSAGE_SENDER_KEY, str2);
        intent.putExtra("action", str);
        intent.putExtra("target", messageTarget.getValue());
        intent.putExtras(bundle);
        Context context2 = this.context;
        if (context2 != null) {
            context2.sendBroadcast(intent);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void d(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        ee7.b(component, "component");
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        ee7.b(str3, "message");
        if (this.isInitialized && this.isDebuggable) {
            log(FLogger.LogLevel.DEBUG, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3, ErrorCodeBuilder.Step step, String str4) {
        List<String> errors;
        ee7.b(component, "component");
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        ee7.b(str3, "errorCode");
        ee7.b(step, "step");
        ee7.b(str4, "errorMessage");
        if (!this.isInitialized) {
            return;
        }
        if (log(FLogger.LogLevel.ERROR, component, session, str, str2, new ErrorEvent(str3, step.getNameValue(), component, str4).toJsonString())) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (!(sessionSummary == null || (errors = sessionSummary.getErrors()) == null)) {
                errors.add(str3);
            }
            String str5 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_ERROR_CODE, str3);
            sendInternalMessage(MESSAGE_ACTION_ERROR_RECORDED, str5, messageTarget, bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public List<File> exportAppLogs() {
        List<File> list;
        ArrayList arrayList = new ArrayList();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 == null || (list = bufferLogWriter2.exportLogs()) == null) {
            list = new ArrayList<>();
        }
        arrayList.addAll(list);
        return arrayList;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void flush() {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new RemoteFLogger$flush$Anon1(this, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object flushBuffer(fb7<? super i97> fb7) {
        i97 i97;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        this.lastSyncTime = instance.getTimeInMillis();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.forceFlushBuffer();
            i97 = i97.a;
        } else {
            i97 = null;
        }
        if (i97 == nb7.a()) {
            return i97;
        }
        return i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object flushDB(com.fossil.fb7<? super com.fossil.i97> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1 r0 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1 r0 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r1 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r1 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r1
            java.lang.Object r0 = r0.L$0
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger) r0
            com.fossil.t87.a(r5)
            goto L_0x0055
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r5)
            boolean r5 = r4.isMainFLogger
            if (r5 == 0) goto L_0x0055
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r5 = r4.remoteLogWriter
            if (r5 == 0) goto L_0x0055
            com.misfit.frameworks.buttonservice.log.DBLogWriter r2 = r4.dbLogWriter
            if (r2 == 0) goto L_0x0055
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r5 = r2.flushTo(r5, r0)
            if (r5 != r1) goto L_0x0055
            return r1
        L_0x0055:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.RemoteFLogger.flushDB(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void i(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        ee7.b(component, "component");
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        ee7.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.INFO, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void init(String str, AppLogInfo appLogInfo2, ActiveDeviceInfo activeDeviceInfo2, CloudLogConfig cloudLogConfig, Context context2, boolean z, boolean z2) {
        ee7.b(str, "name");
        ee7.b(appLogInfo2, "appLogInfo");
        ee7.b(activeDeviceInfo2, "activeDeviceInfo");
        ee7.b(cloudLogConfig, "cloudLogConfig");
        ee7.b(context2, "context");
        int i = z2 ? 10 : 100;
        int i2 = z2 ? 50 : 500;
        this.flushLogTimeThreshold = z2 ? 1800000 : LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
        this.floggerName = str;
        this.appLogInfo = appLogInfo2;
        this.activeDeviceInfo = activeDeviceInfo2;
        this.context = context2;
        BufferLogWriter bufferLogWriter2 = new BufferLogWriter(str, i);
        this.bufferLogWriter = bufferLogWriter2;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.setCallback(this);
        }
        BufferLogWriter bufferLogWriter3 = this.bufferLogWriter;
        if (bufferLogWriter3 != null) {
            String file = context2.getFilesDir().toString();
            ee7.a((Object) file, "context.filesDir.toString()");
            bufferLogWriter3.startWriter(file, z, this);
        }
        this.dbLogWriter = new DBLogWriter(context2, i2, this);
        updateCloudLogConfig(cloudLogConfig);
        registerBroadcastMessages(context2);
        this.isMainFLogger = z;
        this.isDebuggable = z2;
        this.isInitialized = true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean log(com.misfit.frameworks.buttonservice.log.FLogger.LogLevel r28, com.misfit.frameworks.buttonservice.log.FLogger.Component r29, com.misfit.frameworks.buttonservice.log.FLogger.Session r30, java.lang.String r31, java.lang.String r32, java.lang.String r33) {
        /*
            r27 = this;
            r0 = r27
            r2 = r28
            r14 = r29
            r15 = r30
            r13 = r31
            r1 = r32
            r3 = r33
            java.lang.String r4 = "logLevel"
            com.fossil.ee7.b(r2, r4)
            java.lang.String r4 = "component"
            com.fossil.ee7.b(r14, r4)
            java.lang.String r4 = "session"
            com.fossil.ee7.b(r15, r4)
            java.lang.String r4 = "serial"
            com.fossil.ee7.b(r13, r4)
            java.lang.String r4 = "className"
            com.fossil.ee7.b(r1, r4)
            java.lang.String r4 = "message"
            com.fossil.ee7.b(r3, r4)
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r4 = r0.appLogInfo
            java.lang.String r4 = r4.getUserId()
            boolean r4 = com.fossil.mh7.a(r4)
            r11 = 1
            r4 = r4 ^ r11
            java.lang.String r12 = "Calendar.getInstance()"
            r18 = 0
            r10 = 0
            if (r4 == 0) goto L_0x01b8
            boolean r4 = r0.isPrintToConsole
            r5 = 3
            if (r4 == 0) goto L_0x00a6
            int[] r4 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.WhenMappings.$EnumSwitchMapping$1
            int r6 = r28.ordinal()
            r4 = r4[r6]
            java.lang.String r6 = "REMOTE - "
            if (r4 == r11) goto L_0x0094
            r7 = 2
            if (r4 == r7) goto L_0x0081
            if (r4 == r5) goto L_0x006e
            r6 = 4
            if (r4 == r6) goto L_0x0059
            goto L_0x00a6
        L_0x0059:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "SUMMARY - "
            r4.append(r6)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            android.util.Log.i(r1, r3)
            goto L_0x00a6
        L_0x006e:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r6)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            android.util.Log.e(r1, r3)
            goto L_0x00a6
        L_0x0081:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r6)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            android.util.Log.i(r1, r3)
            goto L_0x00a6
        L_0x0094:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r6)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            android.util.Log.d(r1, r3)
        L_0x00a6:
            com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo r1 = r0.activeDeviceInfo
            java.lang.String r1 = r1.getDeviceSerial()
            boolean r1 = com.fossil.mh7.b(r1, r13, r11)
            java.lang.String r4 = ""
            if (r1 == 0) goto L_0x00c5
            com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo r1 = r0.activeDeviceInfo
            java.lang.String r1 = r1.getFwVersion()
            com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo r4 = r0.activeDeviceInfo
            java.lang.String r4 = r4.getDeviceModel()
            r16 = r1
            r17 = r4
            goto L_0x00c9
        L_0x00c5:
            r16 = r4
            r17 = r16
        L_0x00c9:
            com.misfit.frameworks.buttonservice.log.AppSizeLog r1 = new com.misfit.frameworks.buttonservice.log.AppSizeLog
            r1.<init>(r10, r10, r5, r10)
            com.misfit.frameworks.buttonservice.log.FLogger$LogLevel r4 = com.misfit.frameworks.buttonservice.log.FLogger.LogLevel.SUMMARY
            if (r2 != r4) goto L_0x015a
            com.misfit.frameworks.buttonservice.log.FLogger$Session r4 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
            if (r15 != r4) goto L_0x015a
            com.misfit.frameworks.buttonservice.log.FLogger$Component r4 = com.misfit.frameworks.buttonservice.log.FLogger.Component.BLE
            if (r14 != r4) goto L_0x015a
            com.misfit.frameworks.buttonservice.utils.ConversionUtils r4 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE
            com.fossil.zs1 r5 = com.fossil.zs1.b
            com.fossil.bg0 r6 = com.fossil.bg0.HARDWARE_RAW_LOG
            long r5 = r5.a(r6)
            float r4 = r4.convertByteToMegaBytes(r5)
            com.misfit.frameworks.buttonservice.utils.ConversionUtils r5 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE
            com.fossil.zs1 r6 = com.fossil.zs1.b
            com.fossil.bg0 r7 = com.fossil.bg0.ACTIVITY_RAW_LOG
            long r6 = r6.a(r7)
            float r5 = r5.convertByteToMegaBytes(r6)
            com.misfit.frameworks.buttonservice.utils.ConversionUtils r6 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE
            com.fossil.zs1 r7 = com.fossil.zs1.b
            com.fossil.bg0 r8 = com.fossil.bg0.SDK_LOG
            long r7 = r7.a(r8)
            float r6 = r6.convertByteToMegaBytes(r7)
            com.misfit.frameworks.buttonservice.utils.ConversionUtils r7 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE
            com.fossil.zs1 r8 = com.fossil.zs1.b
            com.fossil.bg0 r9 = com.fossil.bg0.GPS_LOG
            long r8 = r8.a(r9)
            float r7 = r7.convertByteToMegaBytes(r8)
            com.misfit.frameworks.buttonservice.log.SDKLog r8 = new com.misfit.frameworks.buttonservice.log.SDKLog
            r8.<init>(r4, r5, r6, r7)
            r1.setSdk(r8)
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r4 = r0.bufferLogWriter
            if (r4 == 0) goto L_0x0123
            float r4 = r4.getLogFileSize()
            goto L_0x0124
        L_0x0123:
            r4 = 0
        L_0x0124:
            com.misfit.frameworks.buttonservice.log.AppLog r5 = new com.misfit.frameworks.buttonservice.log.AppLog
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 7
            r24 = 0
            r19 = r5
            r19.<init>(r20, r21, r22, r23, r24)
            r5.setAppLogSize(r4)
            r1.setApp(r5)
            com.google.gson.Gson r4 = new com.google.gson.Gson
            r4.<init>()
            java.lang.String r1 = r4.a(r1)
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>(r3)
            java.lang.String r3 = "app_size"
            r4.put(r3, r1)
            java.lang.String r1 = r4.toString()
            java.lang.String r3 = "logMessageJson.toString()"
            com.fossil.ee7.a(r1, r3)
            r19 = r1
            goto L_0x015c
        L_0x015a:
            r19 = r3
        L_0x015c:
            com.misfit.frameworks.buttonservice.log.LogEvent r9 = new com.misfit.frameworks.buttonservice.log.LogEvent
            r1 = r9
            java.util.Calendar r3 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r3, r12)
            long r3 = r3.getTimeInMillis()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r5 = r0.appLogInfo
            java.lang.String r5 = r5.getUserId()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r6 = r0.appLogInfo
            java.lang.String r6 = r6.getPhoneID()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r7 = r0.appLogInfo
            java.lang.String r7 = r7.getAppVersion()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r8 = r0.appLogInfo
            java.lang.String r8 = r8.getPlatform()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r10 = r0.appLogInfo
            java.lang.String r10 = r10.getPlatformVersion()
            r25 = r9
            r9 = r10
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r10 = r0.appLogInfo
            java.lang.String r10 = r10.getPhoneModel()
            com.misfit.frameworks.buttonservice.log.model.AppLogInfo r11 = r0.appLogInfo
            java.lang.String r11 = r11.getSdkVersion()
            r26 = r12
            r12 = r11
            r2 = r28
            r20 = 1
            r11 = r16
            r13 = r17
            r14 = r29
            r15 = r30
            r16 = r31
            r17 = r19
            r1.<init>(r2, r3, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r1 = r0.bufferLogWriter
            if (r1 == 0) goto L_0x01ba
            r2 = r25
            r1.writeLog(r2)
            r11 = 1
            goto L_0x01bb
        L_0x01b8:
            r26 = r12
        L_0x01ba:
            r11 = 0
        L_0x01bb:
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            r2 = r26
            com.fossil.ee7.a(r1, r2)
            long r1 = r1.getTimeInMillis()
            long r3 = r0.lastSyncTime
            long r1 = r1 - r3
            int r3 = r0.flushLogTimeThreshold
            long r3 = (long) r3
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x01f3
            com.fossil.ti7 r1 = com.fossil.qj7.a()
            com.fossil.yi7 r1 = com.fossil.zi7.a(r1)
            r2 = 0
            r3 = 0
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$log$Anon1 r4 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$log$Anon1
            r5 = 0
            r4.<init>(r0, r5)
            r5 = 3
            r6 = 0
            r28 = r1
            r29 = r2
            r30 = r3
            r31 = r4
            r32 = r5
            r33 = r6
            com.fossil.ik7 unused = com.fossil.xh7.b(r28, r29, r30, r31, r32, r33)
        L_0x01f3:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.RemoteFLogger.log(com.misfit.frameworks.buttonservice.log.FLogger$LogLevel, com.misfit.frameworks.buttonservice.log.FLogger$Component, com.misfit.frameworks.buttonservice.log.FLogger$Session, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.DBLogWriter.IDBLogWriterCallback
    public void onDeleteLogs() {
        ie4 ie4 = new ie4();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        ie4.a("app_log", String.valueOf(bufferLogWriter2 != null ? bufferLogWriter2.getLogFileSize() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        ie4.a(UserDataStore.DATE_OF_BIRTH, "");
        ie4.a("cache", "");
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String deviceSerial = this.activeDeviceInfo.getDeviceSerial();
        String str = TAG;
        String jsonElement = ie4.toString();
        ee7.a((Object) jsonElement, "jsonObject.toString()");
        i(component, session, deviceSerial, str, jsonElement);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback
    public void onFullBuffer(List<LogEvent> list, boolean z) {
        ee7.b(list, "logLines");
        ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new RemoteFLogger$onFullBuffer$Anon1(this, list, z, null), 3, null);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.DBLogWriter.IDBLogWriterCallback
    public void onReachDBThreshold() {
        if (this.isMainFLogger) {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new RemoteFLogger$onReachDBThreshold$Anon1(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback
    public void onWrittenSummaryLog(LogEvent logEvent) {
        ee7.b(logEvent, "logEvent");
        if (this.isDebuggable) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".onWrittenSummaryLog(), logEvent=" + logEvent);
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new RemoteFLogger$onWrittenSummaryLog$Anon1(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void setPrintToConsole(boolean z) {
        this.isPrintToConsole = z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void startSession(FLogger.Session session, String str, String str2) {
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            startSession(summaryKey, str);
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_SERIAL, str);
            sendInternalMessage(MESSAGE_ACTION_START_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void summary(int i, FLogger.Component component, FLogger.Session session, String str, String str2) {
        ee7.b(component, "component");
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            long currentTimeMillis = System.currentTimeMillis();
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (sessionSummary != null) {
                sessionSummary.update(i);
                sessionSummary.setEndTimeStamp(currentTimeMillis);
                if (this.isInitialized) {
                    log(FLogger.LogLevel.SUMMARY, component, session, str, str2, sessionSummary.toJsonString());
                }
                this.summarySessionMap.remove(summaryKey);
            }
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            sendInternalMessage(MESSAGE_ACTION_END_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public final void unregisterBroadcastMessage(Context context2) {
        ee7.b(context2, "context");
        context2.unregisterReceiver(this.mMessageBroadcastReceiver);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo2) {
        ee7.b(activeDeviceInfo2, "activeDeviceInfo");
        this.activeDeviceInfo = activeDeviceInfo2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateAppLogInfo(AppLogInfo appLogInfo2) {
        ee7.b(appLogInfo2, "appLogInfo");
        this.appLogInfo = appLogInfo2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateCloudLogConfig(CloudLogConfig cloudLogConfig) {
        ee7.b(cloudLogConfig, "cloudLogConfig");
        LogEndPoint.INSTANCE.init(cloudLogConfig.getLogBrandName(), cloudLogConfig.getEndPointBaseUrl(), cloudLogConfig.getAccessKey(), cloudLogConfig.getSecretKey());
        LogApiService logApiService = LogEndPoint.INSTANCE.getLogApiService();
        if (logApiService != null) {
            this.remoteLogWriter = new CloudLogWriter(logApiService);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo2) {
        ee7.b(sessionDetailInfo2, "sessionDetailInfo");
        this.sessionDetailInfo = sessionDetailInfo2;
    }

    @DexIgnore
    private final void startSession(String str, String str2) {
        SessionSummary sessionSummary = this.summarySessionMap.get(str);
        if (sessionSummary != null) {
            sessionSummary.setStartTimeStamp(System.currentTimeMillis());
            sessionSummary.setEndTimeStamp(-1);
            sessionSummary.getErrors().clear();
            SessionSummary sessionSummary2 = this.summarySessionMap.get(str);
            if (sessionSummary2 != null) {
                sessionSummary2.setDetails(getSessionDetails(str, str2));
                return;
            }
            return;
        }
        this.summarySessionMap.put(str, new SessionSummary(System.currentTimeMillis(), -1));
        SessionSummary sessionSummary3 = this.summarySessionMap.get(str);
        if (sessionSummary3 != null) {
            sessionSummary3.setDetails(getSessionDetails(str, str2));
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        ee7.b(component, "component");
        ee7.b(session, Constants.SESSION);
        ee7.b(str, "serial");
        ee7.b(str2, PinObject.COLUMN_CLASS_NAME);
        ee7.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.ERROR, component, session, str, str2, str3);
        }
    }
}
