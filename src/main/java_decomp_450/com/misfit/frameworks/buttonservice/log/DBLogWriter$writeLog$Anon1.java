package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter$writeLog$Anon1 extends fe7 implements gd7<LogEvent, Log> {
    @DexIgnore
    public static /* final */ DBLogWriter$writeLog$Anon1 INSTANCE; // = new DBLogWriter$writeLog$Anon1();

    @DexIgnore
    public DBLogWriter$writeLog$Anon1() {
        super(1);
    }

    @DexIgnore
    public final Log invoke(LogEvent logEvent) {
        ee7.b(logEvent, "it");
        return new Log(logEvent.getTimestamp(), logEvent.toString(), Log.Flag.ADD);
    }
}
