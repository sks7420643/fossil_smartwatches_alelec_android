package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.ee7;
import com.fossil.te4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoveDeviceLog {
    @DexIgnore
    @te4("current_device")
    public String currentDevice;

    @DexIgnore
    public RemoveDeviceLog(String str) {
        ee7.b(str, "currentDevice");
        this.currentDevice = str;
    }

    @DexIgnore
    public final String getCurrentDevice() {
        return this.currentDevice;
    }

    @DexIgnore
    public final void setCurrentDevice(String str) {
        ee7.b(str, "<set-?>");
        this.currentDevice = str;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }
}
