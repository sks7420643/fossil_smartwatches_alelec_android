package com.misfit.frameworks.buttonservice.log.db;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.si;
import com.fossil.vh;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDao_Impl implements LogDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<Log> __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ LogFlagConverter __logFlagConverter; // = new LogFlagConverter();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<Log> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `log` (`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, Log log) {
            ajVar.bindLong(1, (long) log.getId());
            ajVar.bindLong(2, log.getTimeStamp());
            if (log.getContent() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, log.getContent());
            }
            String logFlagEnumToString = LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, logFlagEnumToString);
            }
        }
    }

    @DexIgnore
    public LogDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfLog = new Anon1(ciVar);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int countExcept(Log.Flag flag) {
        fi b = fi.b("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int delete(List<Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = si.a();
        a.append("DELETE FROM log WHERE id IN (");
        si.a(a, list.size());
        a.append(")");
        aj compileStatement = this.__db.compileStatement(a.toString());
        int i = 1;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public List<Log> getAllLogEventsExcept(Log.Flag flag) {
        fi b = fi.b("SELECT * FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "timeStamp");
            int b4 = oi.b(a, "content");
            int b5 = oi.b(a, "cloudFlag");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                Log log = new Log(a.getLong(b3), a.getString(b4), this.__logFlagConverter.stringToLogFlag(a.getString(b5)));
                log.setId(a.getInt(b2));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void insertLogEvent(List<Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void updateCloudFlagByIds(List<Integer> list, Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder a = si.a();
        a.append("UPDATE log SET cloudFlag = ");
        a.append("?");
        a.append(" WHERE id IN (");
        si.a(a, list.size());
        a.append(")");
        aj compileStatement = this.__db.compileStatement(a.toString());
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.bindNull(1);
        } else {
            compileStatement.bindString(1, logFlagEnumToString);
        }
        int i = 2;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
