package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaDetailLog {
    @DexIgnore
    @te4("battery_levels")
    public int batteryLevel;

    @DexIgnore
    public OtaDetailLog() {
        this(0, 1, null);
    }

    @DexIgnore
    public OtaDetailLog(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ OtaDetailLog(int i, int i2, zd7 zd7) {
        this((i2 & 1) != 0 ? 0 : i);
    }
}
