package com.misfit.frameworks.buttonservice.log;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$deleteLogs$Anon2 extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $logIds;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$deleteLogs$Anon2(DBLogWriter dBLogWriter, List list, fb7 fb7) {
        super(2, fb7);
        this.this$0 = dBLogWriter;
        this.$logIds = list;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        DBLogWriter$deleteLogs$Anon2 dBLogWriter$deleteLogs$Anon2 = new DBLogWriter$deleteLogs$Anon2(this.this$0, this.$logIds, fb7);
        dBLogWriter$deleteLogs$Anon2.p$ = (yi7) obj;
        return dBLogWriter$deleteLogs$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
        return ((DBLogWriter$deleteLogs$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            int i = 0;
            for (List list : ea7.b(this.$logIds, 500)) {
                i += this.this$0.logDao.delete(ea7.d((Collection) list));
            }
            DBLogWriter.IDBLogWriterCallback access$getCallback$p = this.this$0.callback;
            if (access$getCallback$p != null) {
                access$getCallback$p.onDeleteLogs();
            }
            return pb7.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
