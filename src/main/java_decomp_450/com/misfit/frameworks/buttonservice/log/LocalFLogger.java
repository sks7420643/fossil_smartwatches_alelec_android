package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import android.util.Log;
import com.fossil.ee7;
import com.fossil.we7;
import com.fossil.zd7;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalFLogger implements ILocalFLogger {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String LOG_PATTERN; // = "%s %s: %s\n";
    @DexIgnore
    public FileLogWriter fileLogWriter;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public String prefix; // = "";
    @DexIgnore
    public /* final */ SimpleDateFormat sdf; // = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS", Locale.US);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    private final boolean getCanWriteLog() {
        return this.isInitialized && this.isDebuggable;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public void d(String str, String str2) {
        ee7.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.d(this.prefix + " - " + str, str2);
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(new Object[]{this.sdf.format(new Date()), this.prefix + " - " + str + " /D", str2}, 3));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public void e(String str, String str2) {
        ee7.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.e(this.prefix + " - " + str, str2);
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(new Object[]{this.sdf.format(new Date()), this.prefix + " - " + str + " /E", str2}, 3));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public List<File> exportAppLogs() {
        List<File> exportLogs;
        FileLogWriter fileLogWriter2 = this.fileLogWriter;
        return (fileLogWriter2 == null || (exportLogs = fileLogWriter2.exportLogs()) == null) ? new ArrayList() : exportLogs;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public void i(String str, String str2) {
        ee7.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.i(this.prefix + " - " + str, str2);
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(new Object[]{this.sdf.format(new Date()), this.prefix + " - " + str + " /I", str2}, 3));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public void init(Context context, String str, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "prefix");
        this.prefix = str;
        FileLogWriter fileLogWriter2 = new FileLogWriter();
        this.fileLogWriter = fileLogWriter2;
        if (fileLogWriter2 != null) {
            String file = context.getFilesDir().toString();
            ee7.a((Object) file, "context.filesDir.toString()");
            fileLogWriter2.startWriter(file);
        }
        this.isDebuggable = z;
        this.isInitialized = true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.ILocalFLogger
    public void v(String str, String str2) {
        ee7.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.v(this.prefix + " - " + str, str2);
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(new Object[]{this.sdf.format(new Date()), this.prefix + " - " + str + " /V", str2}, 3));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }
}
