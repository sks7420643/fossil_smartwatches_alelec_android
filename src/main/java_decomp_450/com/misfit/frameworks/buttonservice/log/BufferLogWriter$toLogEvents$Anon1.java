package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$toLogEvents$Anon1 extends fe7 implements gd7<String, LogEvent> {
    @DexIgnore
    public /* final */ /* synthetic */ Gson $gson;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$toLogEvents$Anon1(Gson gson) {
        super(1);
        this.$gson = gson;
    }

    @DexIgnore
    public final LogEvent invoke(String str) {
        ee7.b(str, "it");
        try {
            return (LogEvent) this.$gson.a(str, LogEvent.class);
        } catch (Exception unused) {
            return null;
        }
    }
}
