package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LogEvent $logEvent;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter$pollEventQueue$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2(LogEvent logEvent, fb7 fb7, BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1) {
        super(2, fb7);
        this.$logEvent = logEvent;
        this.this$0 = bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 = new BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2(this.$logEvent, fb7, this.this$0);
        bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2.p$ = (yi7) obj;
        return bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            BufferLogWriter.IBufferLogCallback callback = this.this$0.this$0.getCallback();
            if (callback == null) {
                return null;
            }
            callback.onWrittenSummaryLog(this.$logEvent);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
