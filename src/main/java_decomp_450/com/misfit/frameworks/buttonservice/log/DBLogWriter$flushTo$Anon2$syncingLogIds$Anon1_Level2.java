package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2 extends fe7 implements gd7<LogEvent, Integer> {
    @DexIgnore
    public static /* final */ DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2 INSTANCE; // = new DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2();

    @DexIgnore
    public DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2() {
        super(1);
    }

    @DexIgnore
    public final Integer invoke(LogEvent logEvent) {
        ee7.b(logEvent, "it");
        Object tag = logEvent.getTag();
        if (!(tag instanceof Integer)) {
            tag = null;
        }
        return (Integer) tag;
    }
}
