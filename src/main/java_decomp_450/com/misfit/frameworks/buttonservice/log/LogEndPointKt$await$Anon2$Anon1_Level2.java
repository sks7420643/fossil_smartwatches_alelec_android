package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ru7;
import com.fossil.s87;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt$await$Anon2$Anon1_Level2 implements ru7<T> {
    @DexIgnore
    public /* final */ /* synthetic */ fb7 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1_Level2(fb7 fb7) {
        this.$continuation = fb7;
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onFailure(Call<T> call, Throwable th) {
        ee7.b(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        fb7 fb7 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        s87.a aVar = s87.Companion;
        fb7.resumeWith(s87.m60constructorimpl(create));
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onResponse(Call<T> call, fv7<T> fv7) {
        ee7.b(fv7, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + fv7);
        fb7 fb7 = this.$continuation;
        RepoResponse create = RepoResponse.Companion.create(fv7);
        s87.a aVar = s87.Companion;
        fb7.resumeWith(s87.m60constructorimpl(create));
    }
}
