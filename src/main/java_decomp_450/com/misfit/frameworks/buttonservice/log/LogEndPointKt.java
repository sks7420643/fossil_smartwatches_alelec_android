package com.misfit.frameworks.buttonservice.log;

import com.fossil.fb7;
import com.fossil.kb7;
import com.fossil.mb7;
import com.fossil.nb7;
import com.fossil.vb7;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, fb7<? super RepoResponse<T>> fb7) {
        kb7 kb7 = new kb7(mb7.a(fb7));
        call.a(new LogEndPointKt$await$Anon2$Anon1_Level2(kb7));
        Object a = kb7.a();
        if (a == nb7.a()) {
            vb7.c(fb7);
        }
        return a;
    }
}
