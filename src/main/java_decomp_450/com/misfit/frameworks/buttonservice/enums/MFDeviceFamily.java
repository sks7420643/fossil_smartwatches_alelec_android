package com.misfit.frameworks.buttonservice.enums;

import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;

public enum MFDeviceFamily {
    UNKNOWN(AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, 0),
    INTEL("Intel", 1),
    DEVICE_FAMILY_Q_MOTION("Q_MOTION", 2),
    DEVICE_FAMILY_RMM("RMM", 3),
    DEVICE_FAMILY_SAM("SAM", 4),
    DEVICE_FAMILY_SAM_SLIM("SAM_SLIM", 5),
    DEVICE_FAMILY_SAM_MINI("MINI", 6),
    DEVICE_FAMILY_SE0("SE0", 7),
    DEVICE_FAMILY_DIANA("HYBRID HR", 8);
    
    public String serverName;
    public int value;

    public static /* synthetic */ class Anon1 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE;

        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE[] r0 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE = r0
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.RMM     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x001d }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SE0     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x003e }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.DIANA     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.Anon1.<clinit>():void");
        }
        */
    }

    public MFDeviceFamily(String str, int i) {
        this.value = i;
        this.serverName = str;
    }

    public static MFDeviceFamily fromInt(int i) {
        MFDeviceFamily[] values = values();
        for (MFDeviceFamily mFDeviceFamily : values) {
            if (mFDeviceFamily.getValue() == i) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    public static MFDeviceFamily fromServerName(String str) {
        MFDeviceFamily[] values = values();
        for (MFDeviceFamily mFDeviceFamily : values) {
            if (mFDeviceFamily.getServerName().equalsIgnoreCase(str)) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return DEVICE_FAMILY_RMM;
            case 2:
            case 3:
                return DEVICE_FAMILY_SAM;
            case 4:
                return DEVICE_FAMILY_SE0;
            case 5:
                return DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return DEVICE_FAMILY_SAM_MINI;
            case 7:
                return DEVICE_FAMILY_Q_MOTION;
            case 8:
                return DEVICE_FAMILY_DIANA;
            default:
                return UNKNOWN;
        }
    }

    public static boolean isHybridSmartWatchFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM || mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI || mFDeviceFamily == DEVICE_FAMILY_SE0;
    }

    public static boolean isSlimOrMiniFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI;
    }

    public String getServerName() {
        return this.serverName;
    }

    public int getValue() {
        return this.value;
    }
}
