package com.misfit.frameworks.buttonservice.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum DeviceSettings {
    SECOND_TIMEZONE,
    SINGLE_ALARM,
    MULTI_ALARM,
    MAPPINGS,
    GOAL_TRACKING,
    COUNT_DOWN,
    COMPLICATION_APPS,
    WATCH_APPS,
    BIOMETRIC,
    BACKGROUND_IMAGE,
    NOTIFICATION_FILTERS,
    LOCALIZATION_DATA,
    WATCH_PARAMS
}
