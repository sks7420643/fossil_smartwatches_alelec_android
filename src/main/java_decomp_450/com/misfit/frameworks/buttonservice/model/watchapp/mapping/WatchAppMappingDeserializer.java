package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import com.fossil.be4;
import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.je4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingDeserializer implements fe4<WatchAppMapping> {
    @DexIgnore
    @Override // com.fossil.fe4
    public WatchAppMapping deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        ee7.b(jsonElement, "json");
        ee7.b(type, "typeOfT");
        ee7.b(ee4, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = WatchAppMappingDeserializer.class.getName();
        ee7.a((Object) name, "WatchAppMappingDeserializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(WatchAppMapping.Companion.getFIELD_TYPE());
        ee7.a((Object) a, "jsonType");
        int b = a.b();
        be4 be4 = new be4();
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS()) {
            return (WatchAppMapping) be4.a().a(jsonElement, DiagnosticsWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD()) {
            return (WatchAppMapping) be4.a().a(jsonElement, WellnessWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT()) {
            return (WatchAppMapping) be4.a().a(jsonElement, WorkoutWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY()) {
            return (WatchAppMapping) be4.a().a(jsonElement, NoneWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getMUSIC()) {
            return (WatchAppMapping) be4.a().a(jsonElement, MusicWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getNOTIFICATION_PANEL()) {
            return (WatchAppMapping) be4.a().a(jsonElement, NotificationPanelWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH()) {
            return (WatchAppMapping) be4.a().a(jsonElement, StopWatchWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER()) {
            return (WatchAppMapping) be4.a().a(jsonElement, TimerWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER()) {
            return (WatchAppMapping) be4.a().a(jsonElement, WeatherWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME()) {
            return (WatchAppMapping) be4.a().a(jsonElement, CommuteTimeWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getBUDDY_CHALLENGE()) {
            return (WatchAppMapping) be4.a().a(jsonElement, BuddyChallengeMapping.class);
        }
        return null;
    }
}
