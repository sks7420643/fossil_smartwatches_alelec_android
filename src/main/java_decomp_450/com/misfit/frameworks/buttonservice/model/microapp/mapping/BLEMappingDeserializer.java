package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.be4;
import com.fossil.ee4;
import com.fossil.fe4;
import com.fossil.je4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingDeserializer implements fe4<BLEMapping> {
    @DexIgnore
    @Override // com.fossil.fe4
    public BLEMapping deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().a("mType").b();
        be4 be4 = new be4();
        be4.a(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) be4.a().a(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) be4.a().a(jsonElement, MicroAppMapping.class);
    }
}
