package com.misfit.frameworks.buttonservice.model.alarm;

import com.fossil.c70;
import com.fossil.d70;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.f70;
import com.fossil.j90;
import com.fossil.p87;
import com.fossil.u60;
import com.fossil.x60;
import com.fossil.x97;
import com.fossil.y60;
import com.fossil.z60;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmSetting {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HashSet<AlarmDay> alarmDays;
    @DexIgnore
    public /* final */ int hour;
    @DexIgnore
    public /* final */ boolean isRepeat;
    @DexIgnore
    public /* final */ String message;
    @DexIgnore
    public /* final */ int minute;
    @DexIgnore
    public /* final */ String title;

    @DexIgnore
    public enum AlarmDay {
        SUNDAY(1),
        MONDAY(2),
        TUESDAY(3),
        WEDNESDAY(4),
        THURSDAY(5),
        FRIDAY(6),
        SATURDAY(7);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public AlarmDay(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[AlarmDay.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[AlarmDay.SUNDAY.ordinal()] = 1;
            $EnumSwitchMapping$0[AlarmDay.MONDAY.ordinal()] = 2;
            $EnumSwitchMapping$0[AlarmDay.TUESDAY.ordinal()] = 3;
            $EnumSwitchMapping$0[AlarmDay.WEDNESDAY.ordinal()] = 4;
            $EnumSwitchMapping$0[AlarmDay.THURSDAY.ordinal()] = 5;
            $EnumSwitchMapping$0[AlarmDay.FRIDAY.ordinal()] = 6;
            $EnumSwitchMapping$0[AlarmDay.SATURDAY.ordinal()] = 7;
        }
        */
    }

    /*
    static {
        String name = AlarmSetting.class.getName();
        ee7.a((Object) name, "AlarmSetting::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public AlarmSetting(String str, String str2, int i, int i2, boolean z, HashSet<AlarmDay> hashSet) {
        this.title = str;
        this.message = str2;
        this.hour = i;
        this.minute = i2;
        this.isRepeat = z;
        this.alarmDays = hashSet;
    }

    @DexIgnore
    private final j90 convertCalendarDayToSDKV2Day(AlarmDay alarmDay) {
        switch (WhenMappings.$EnumSwitchMapping$0[alarmDay.ordinal()]) {
            case 1:
                return j90.SUNDAY;
            case 2:
                return j90.MONDAY;
            case 3:
                return j90.TUESDAY;
            case 4:
                return j90.WEDNESDAY;
            case 5:
                return j90.THURSDAY;
            case 6:
                return j90.FRIDAY;
            case 7:
                return j90.SATURDAY;
            default:
                throw new p87();
        }
    }

    @DexIgnore
    public final HashSet<AlarmDay> getAlarmDays() {
        return this.alarmDays;
    }

    @DexIgnore
    public final int getAlarmDaysAsInt() {
        HashSet<AlarmDay> hashSet = this.alarmDays;
        int i = 0;
        if (hashSet != null) {
            Iterator<T> it = hashSet.iterator();
            while (it.hasNext()) {
                i |= it.next().getValue();
            }
        }
        return i;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final boolean isRepeat() {
        return this.isRepeat;
    }

    @DexIgnore
    public final u60 toSDKV2Setting() {
        y60 y60;
        String str = this.title;
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        f70 f70 = new f70(str);
        String str3 = this.message;
        if (str3 != null) {
            str2 = str3;
        }
        x60 x60 = new x60(str2);
        HashSet<AlarmDay> hashSet = this.alarmDays;
        j90 j90 = null;
        if (hashSet == null) {
            y60 = new y60(new z60((byte) this.hour, (byte) this.minute, null), f70, x60);
        } else if (hashSet.isEmpty()) {
            y60 = new y60(new z60((byte) this.hour, (byte) this.minute, null), f70, x60);
        } else if (this.isRepeat) {
            HashSet<AlarmDay> hashSet2 = this.alarmDays;
            ArrayList arrayList = new ArrayList(x97.a(hashSet2, 10));
            Iterator<T> it = hashSet2.iterator();
            while (it.hasNext()) {
                arrayList.add(convertCalendarDayToSDKV2Day(it.next()));
            }
            return new c70(new d70((byte) this.hour, (byte) this.minute, ea7.q(arrayList)), f70, x60);
        } else if (this.alarmDays.size() > 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local.e(str4, ".toSDKV2Setting(), the alarm setting is wrong format, alarmSetting=" + this);
            return null;
        } else {
            if (!this.alarmDays.isEmpty()) {
                j90 = convertCalendarDayToSDKV2Day((AlarmDay) ea7.n(this.alarmDays).get(0));
            }
            y60 = new y60(new z60((byte) this.hour, (byte) this.minute, j90), f70, x60);
        }
        return y60;
    }

    @DexIgnore
    public String toString() {
        return "AlarmSetting{alarmHour=" + this.hour + ", alarmMinute=" + this.minute + ", alarmDays=" + this.alarmDays + ", alarmRepeat=" + this.isRepeat + "}";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public AlarmSetting(String str, String str2, int i, int i2) {
        this(str, str2, i, i2, false, new HashSet());
        ee7.b(str, "title");
        ee7.b(str2, "message");
    }
}
