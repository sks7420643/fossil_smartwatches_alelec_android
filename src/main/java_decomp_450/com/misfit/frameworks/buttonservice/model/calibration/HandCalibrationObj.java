package com.misfit.frameworks.buttonservice.model.calibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.ee7;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HandCalibrationObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<HandCalibrationObj> CREATOR; // = new HandCalibrationObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ CalibrationEnums.Direction direction;
    @DexIgnore
    public /* final */ CalibrationEnums.HandId handId;
    @DexIgnore
    public /* final */ CalibrationEnums.MovingType movingType;
    @DexIgnore
    public /* final */ CalibrationEnums.Speed speed;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public HandCalibrationObj(CalibrationEnums.HandId handId2, CalibrationEnums.MovingType movingType2, CalibrationEnums.Direction direction2, CalibrationEnums.Speed speed2, int i) {
        ee7.b(handId2, "handId");
        ee7.b(movingType2, "movingType");
        ee7.b(direction2, "direction");
        ee7.b(speed2, PlaceManager.PARAM_SPEED);
        this.handId = handId2;
        this.movingType = movingType2;
        this.direction = direction2;
        this.speed = speed2;
        this.degree = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final CalibrationEnums.Direction getDirection() {
        return this.direction;
    }

    @DexIgnore
    public final CalibrationEnums.HandId getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final CalibrationEnums.MovingType getMovingType() {
        return this.movingType;
    }

    @DexIgnore
    public final CalibrationEnums.Speed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(HandCalibrationObj.class.getName());
        parcel.writeInt(this.handId.getValue());
        parcel.writeInt(this.movingType.getValue());
        parcel.writeInt(this.direction.getValue());
        parcel.writeInt(this.speed.getValue());
        parcel.writeInt(this.degree);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HandCalibrationObj(android.os.Parcel r8) {
        /*
            r7 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r8, r0)
            int r0 = r8.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$HandId r2 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.HandId.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.HandId.\u2026omValue(parcel.readInt())"
            com.fossil.ee7.a(r2, r0)
            int r0 = r8.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$MovingType r3 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.MovingType.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.MovingT\u2026omValue(parcel.readInt())"
            com.fossil.ee7.a(r3, r0)
            int r0 = r8.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$Direction r4 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.Direction.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.Directi\u2026omValue(parcel.readInt())"
            com.fossil.ee7.a(r4, r0)
            int r0 = r8.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$Speed r5 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.Speed.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.Speed.fromValue(parcel.readInt())"
            com.fossil.ee7.a(r5, r0)
            int r6 = r8.readInt()
            r1 = r7
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj.<init>(android.os.Parcel):void");
    }
}
