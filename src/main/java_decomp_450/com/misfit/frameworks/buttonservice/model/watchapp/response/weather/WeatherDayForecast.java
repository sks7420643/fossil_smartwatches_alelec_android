package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.j90;
import com.fossil.p87;
import com.fossil.wa0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherDayForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ WeatherWeekDay weekDay;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new WeatherDayForecast(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public enum WeatherWeekDay {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherWeekDay.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherWeekDay.SUNDAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherWeekDay.MONDAY.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherWeekDay.TUESDAY.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherWeekDay.WEDNESDAY.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherWeekDay.THURSDAY.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherWeekDay.FRIDAY.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherWeekDay.SATURDAY.ordinal()] = 7;
            }
            */
        }

        @DexIgnore
        public final j90 toSDKWeekDay() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return j90.SUNDAY;
                case 2:
                    return j90.MONDAY;
                case 3:
                    return j90.TUESDAY;
                case 4:
                    return j90.WEDNESDAY;
                case 5:
                    return j90.THURSDAY;
                case 6:
                    return j90.FRIDAY;
                case 7:
                    return j90.SATURDAY;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore
    public WeatherDayForecast(float f, float f2, WeatherComplicationAppInfo.WeatherCondition weatherCondition2, WeatherWeekDay weatherWeekDay) {
        ee7.b(weatherCondition2, "weatherCondition");
        ee7.b(weatherWeekDay, "weekDay");
        this.highTemperature = f;
        this.lowTemperature = f2;
        this.weatherCondition = weatherCondition2;
        this.weekDay = weatherWeekDay;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final wa0 toSDKWeatherDayForecast() {
        return new wa0(this.weekDay.toSDKWeekDay(), this.highTemperature, this.lowTemperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeInt(this.weekDay.ordinal());
    }

    @DexIgnore
    public WeatherDayForecast(Parcel parcel) {
        ee7.b(parcel, "parcel");
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.weekDay = WeatherWeekDay.values()[parcel.readInt()];
    }
}
