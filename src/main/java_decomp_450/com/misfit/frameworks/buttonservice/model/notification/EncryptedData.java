package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.lb0;
import com.fossil.vf0;
import com.fossil.wf0;
import com.fossil.yf0;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EncryptedData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ EncryptMethod encryptMethod;
    @DexIgnore
    public /* final */ byte[] encryptedData;
    @DexIgnore
    public /* final */ EncryptedDataType encryptedDataType;
    @DexIgnore
    public /* final */ KeyType keyType;
    @DexIgnore
    public /* final */ short sequence;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<EncryptedData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new EncryptedData(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData[] newArray(int i) {
            return new EncryptedData[i];
        }
    }

    @DexIgnore
    public enum EncryptMethod {
        NONE,
        AES_CTR,
        AES_CBC_PKCS5;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[vf0.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[vf0.AES_CBC_PKCS5.ordinal()] = 1;
                    $EnumSwitchMapping$0[vf0.AES_CTR.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final EncryptMethod fromSDKEncryptMethod(vf0 vf0) {
                ee7.b(vf0, "sdkEncryptMethod");
                int i = WhenMappings.$EnumSwitchMapping$0[vf0.ordinal()];
                if (i == 1) {
                    return EncryptMethod.AES_CBC_PKCS5;
                }
                if (i != 2) {
                    return EncryptMethod.NONE;
                }
                return EncryptMethod.AES_CTR;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }
    }

    @DexIgnore
    public enum EncryptedDataType {
        BUDDY_CHALLENGE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[wf0.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[wf0.BUDDY_CHALLENGE.ordinal()] = 1;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final EncryptedDataType fromSDKEncryptedDataType(wf0 wf0) {
                ee7.b(wf0, "sdkEncryptedDataType");
                if (WhenMappings.$EnumSwitchMapping$0[wf0.ordinal()] != 1) {
                    return EncryptedDataType.BUDDY_CHALLENGE;
                }
                return EncryptedDataType.BUDDY_CHALLENGE;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }
    }

    @DexIgnore
    public enum KeyType {
        DEFAULT,
        PRE_SHARED_KEY,
        SHARED_SECRET_KEY;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[yf0.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[yf0.PRE_SHARED_KEY.ordinal()] = 1;
                    $EnumSwitchMapping$0[yf0.SHARED_SECRET_KEY.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final KeyType fromSDKKeyType(yf0 yf0) {
                ee7.b(yf0, "sdkKeyType");
                int i = WhenMappings.$EnumSwitchMapping$0[yf0.ordinal()];
                if (i == 1) {
                    return KeyType.PRE_SHARED_KEY;
                }
                if (i != 2) {
                    return KeyType.DEFAULT;
                }
                return KeyType.SHARED_SECRET_KEY;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }
    }

    @DexIgnore
    public /* synthetic */ EncryptedData(Parcel parcel, zd7 zd7) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final EncryptMethod getEncryptMethod() {
        return this.encryptMethod;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.encryptedData;
    }

    @DexIgnore
    public final EncryptedDataType getEncryptedDataType() {
        return this.encryptedDataType;
    }

    @DexIgnore
    public final KeyType getKeyType() {
        return this.keyType;
    }

    @DexIgnore
    public final short getSequence() {
        return this.sequence;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeInt(this.encryptMethod.ordinal());
        parcel.writeInt(this.encryptedData.length);
        parcel.writeByteArray(this.encryptedData);
        parcel.writeInt(this.encryptedDataType.ordinal());
        parcel.writeInt(this.keyType.ordinal());
        parcel.writeInt(this.sequence);
    }

    @DexIgnore
    public EncryptedData(lb0 lb0) {
        ee7.b(lb0, "encryptedDataNotification");
        this.encryptMethod = EncryptMethod.Companion.fromSDKEncryptMethod(lb0.getEncryptMethod());
        this.encryptedData = lb0.getEncryptedData();
        this.encryptedDataType = EncryptedDataType.Companion.fromSDKEncryptedDataType(lb0.getEncryptedDataType());
        this.keyType = KeyType.Companion.fromSDKKeyType(lb0.getKeyType());
        this.sequence = lb0.getSequence();
    }

    @DexIgnore
    public EncryptedData(Parcel parcel) {
        this.encryptMethod = EncryptMethod.values()[parcel.readInt()];
        byte[] bArr = new byte[parcel.readInt()];
        this.encryptedData = bArr;
        parcel.readByteArray(bArr);
        this.encryptedDataType = EncryptedDataType.values()[parcel.readInt()];
        this.keyType = KeyType.values()[parcel.readInt()];
        this.sequence = (short) parcel.readInt();
    }
}
