package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShineDevice$Companion$CREATOR$Anon1 implements Parcelable.Creator<ShineDevice> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ShineDevice createFromParcel(Parcel parcel) {
        ee7.b(parcel, "parcel");
        return new ShineDevice(parcel);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ShineDevice[] newArray(int i) {
        return new ShineDevice[i];
    }
}
