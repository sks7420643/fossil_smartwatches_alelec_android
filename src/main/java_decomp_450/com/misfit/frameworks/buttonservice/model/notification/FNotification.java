package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FNotification implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String iconFwPath;
    @DexIgnore
    public /* final */ NotificationBaseObj.ANotificationType notificationType;
    @DexIgnore
    public /* final */ String packageName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<FNotification> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FNotification createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new FNotification(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public FNotification[] newArray(int i) {
            return new FNotification[i];
        }
    }

    @DexIgnore
    public FNotification(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        ee7.b(str, "appName");
        ee7.b(str2, "packageName");
        ee7.b(str3, "iconFwPath");
        ee7.b(aNotificationType, "notificationType");
        this.appName = str;
        this.packageName = str2;
        this.iconFwPath = str3;
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public static /* synthetic */ FNotification copy$default(FNotification fNotification, String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType, int i, Object obj) {
        if ((i & 1) != 0) {
            str = fNotification.appName;
        }
        if ((i & 2) != 0) {
            str2 = fNotification.packageName;
        }
        if ((i & 4) != 0) {
            str3 = fNotification.iconFwPath;
        }
        if ((i & 8) != 0) {
            aNotificationType = fNotification.notificationType;
        }
        return fNotification.copy(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public final String component1() {
        return this.appName;
    }

    @DexIgnore
    public final String component2() {
        return this.packageName;
    }

    @DexIgnore
    public final String component3() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType component4() {
        return this.notificationType;
    }

    @DexIgnore
    public final FNotification copy(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        ee7.b(str, "appName");
        ee7.b(str2, "packageName");
        ee7.b(str3, "iconFwPath");
        ee7.b(aNotificationType, "notificationType");
        return new FNotification(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FNotification)) {
            return false;
        }
        FNotification fNotification = (FNotification) obj;
        return ee7.a(this.appName, fNotification.appName) && ee7.a(this.packageName, fNotification.packageName) && ee7.a(this.iconFwPath, fNotification.iconFwPath) && ee7.a(this.notificationType, fNotification.notificationType);
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType getNotificationType() {
        return this.notificationType;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.appName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.packageName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.iconFwPath;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        NotificationBaseObj.ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            i = aNotificationType.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "FNotification(appName=" + this.appName + ", packageName=" + this.packageName + ", iconFwPath=" + this.iconFwPath + ", notificationType=" + this.notificationType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.appName);
        parcel.writeString(this.packageName);
        parcel.writeString(this.iconFwPath);
        parcel.writeInt(this.notificationType.ordinal());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FNotification(android.os.Parcel r5) {
        /*
            r4 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = r5.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r2 = r5.readString()
            if (r2 == 0) goto L_0x0016
            goto L_0x0017
        L_0x0016:
            r2 = r1
        L_0x0017:
            java.lang.String r3 = r5.readString()
            if (r3 == 0) goto L_0x001e
            r1 = r3
        L_0x001e:
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType[] r3 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.values()
            int r5 = r5.readInt()
            r5 = r3[r5]
            r4.<init>(r0, r2, r1, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.notification.FNotification.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public FNotification() {
        this("", "", "", NotificationBaseObj.ANotificationType.values()[1]);
    }
}
