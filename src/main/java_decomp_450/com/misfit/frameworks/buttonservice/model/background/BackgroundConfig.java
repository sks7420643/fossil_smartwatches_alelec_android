package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.g70;
import com.fossil.h70;
import com.fossil.mh7;
import com.fossil.nh7;
import com.fossil.ps7;
import com.fossil.wg0;
import com.fossil.xg0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String TAG; // = "BackgroundConfig";
    @DexIgnore
    public BackgroundImgData bottomComplicationBackground;
    @DexIgnore
    public BackgroundImgData leftComplicationBackground;
    @DexIgnore
    public BackgroundImgData mainBackground;
    @DexIgnore
    public BackgroundImgData rightComplicationBackground;
    @DexIgnore
    public /* final */ long timestamp;
    @DexIgnore
    public BackgroundImgData topComplicationBackground;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundConfig createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new BackgroundConfig(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundConfig[] newArray(int i) {
            return new BackgroundConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundConfig(Parcel parcel, zd7 zd7) {
        this(parcel);
    }

    @DexIgnore
    private final String getHash() {
        return this.mainBackground.getHash() + ':' + this.topComplicationBackground.getHash() + ':' + this.rightComplicationBackground.getHash() + ':' + this.bottomComplicationBackground.getHash() + ':' + this.leftComplicationBackground.getHash();
    }

    @DexIgnore
    private final g70 getMainBackgroundImage(String str) {
        String imgData = this.mainBackground.getImgData();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "imageData = " + imgData);
        if (imgData.length() == 0) {
            return new g70(this.mainBackground.getImgName(), new byte[0], null, 4, null);
        }
        if (!nh7.a((CharSequence) imgData, (CharSequence) str, false, 2, (Object) null) || !mh7.a(imgData, Constants.PHOTO_BINARY_NAME_SUFFIX, true)) {
            return this.mainBackground.toSDKBackgroundImage();
        }
        try {
            byte[] b = ps7.b(new File(this.mainBackground.getImgData()));
            String imgName = this.mainBackground.getImgName();
            ee7.a((Object) b, "data");
            return new g70(imgName, b, null, 4, null);
        } catch (Exception e) {
            e.printStackTrace();
            return this.mainBackground.toSDKBackgroundImage();
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BackgroundConfig)) {
            return false;
        }
        return mh7.b(getHash(), ((BackgroundConfig) obj).getHash(), true);
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final wg0 toSDKBackgroundImageConfig(String str) {
        g70 g70;
        ee7.b(str, "directory");
        if (ee7.a((Object) this.mainBackground.getImgName(), (Object) Constants.MAIN_BACKGROUND_NAME)) {
            g70 = getMainBackgroundImage(str);
        } else {
            g70 = this.mainBackground.toSDKBackgroundImage();
        }
        return new wg0(new xg0[]{new h70(g70, this.topComplicationBackground.toSDKBackgroundImage(), this.rightComplicationBackground.toSDKBackgroundImage(), this.bottomComplicationBackground.toSDKBackgroundImage(), this.leftComplicationBackground.toSDKBackgroundImage())});
    }

    @DexIgnore
    public String toString() {
        return "{timestamp: " + this.timestamp + ", " + "mainBackground: " + this.mainBackground + ", " + "topComplicationBackground: " + this.topComplicationBackground + ", " + "rightComplicationBackground: " + this.rightComplicationBackground + ", bottomComplicationBackground: " + this.bottomComplicationBackground + ", " + "leftComplicationBackground: " + this.leftComplicationBackground + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeLong(this.timestamp);
        this.mainBackground.writeToParcel(parcel, i);
        this.topComplicationBackground.writeToParcel(parcel, i);
        this.rightComplicationBackground.writeToParcel(parcel, i);
        this.bottomComplicationBackground.writeToParcel(parcel, i);
        this.leftComplicationBackground.writeToParcel(parcel, i);
    }

    @DexIgnore
    public BackgroundConfig(long j, BackgroundImgData backgroundImgData, BackgroundImgData backgroundImgData2, BackgroundImgData backgroundImgData3, BackgroundImgData backgroundImgData4, BackgroundImgData backgroundImgData5) {
        ee7.b(backgroundImgData, "mainBackground");
        ee7.b(backgroundImgData2, "topComplicationBackground");
        ee7.b(backgroundImgData3, "rightComplicationBackground");
        ee7.b(backgroundImgData4, "bottomComplicationBackground");
        ee7.b(backgroundImgData5, "leftComplicationBackground");
        this.timestamp = j;
        this.mainBackground = backgroundImgData;
        this.topComplicationBackground = backgroundImgData2;
        this.rightComplicationBackground = backgroundImgData3;
        this.bottomComplicationBackground = backgroundImgData4;
        this.leftComplicationBackground = backgroundImgData5;
    }

    @DexIgnore
    public BackgroundConfig(Parcel parcel) {
        this.timestamp = parcel.readLong();
        this.mainBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.topComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.rightComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.bottomComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.leftComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
    }
}
