package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface WeatherTemperatureScale {
    public static final int CELSIUS = 0;
    public static final int FAHRENHEIT = 1;
}
