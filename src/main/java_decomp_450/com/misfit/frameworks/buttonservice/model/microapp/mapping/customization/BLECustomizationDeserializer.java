package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.util.Log;
import com.fossil.ee4;
import com.fossil.fe4;
import com.fossil.je4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLECustomizationDeserializer implements fe4<BLECustomization> {
    @DexIgnore
    @Override // com.fossil.fe4
    public BLECustomization deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        Log.d(BLECustomizationDeserializer.class.getName(), jsonElement.toString());
        if (jsonElement.d().a("type").b() != 1) {
            return new BLENonCustomization();
        }
        return (BLECustomization) new Gson().a(jsonElement, BLEGoalTrackingCustomization.class);
    }
}
