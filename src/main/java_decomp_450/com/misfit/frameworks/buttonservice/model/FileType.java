package com.misfit.frameworks.buttonservice.model;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum FileType {
    WATCH_APP("WATCH_APP"),
    WATCH_FACE("WATCH_FACE"),
    WORKOUT_SCREEN_SHOT("WORKOUT_SCREEN_SHOT"),
    LABEL("LABEL"),
    UNKNOWN("UNKNOWN");
    
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final FileType from(String str) {
            FileType fileType;
            ee7.b(str, "value");
            FileType[] values = FileType.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    fileType = null;
                    break;
                }
                fileType = values[i];
                if (ee7.a((Object) fileType.getMValue(), (Object) str)) {
                    break;
                }
                i++;
            }
            return fileType != null ? fileType : FileType.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public FileType(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
