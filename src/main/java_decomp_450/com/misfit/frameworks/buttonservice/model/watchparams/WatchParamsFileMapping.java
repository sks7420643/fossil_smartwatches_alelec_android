package com.misfit.frameworks.buttonservice.model.watchparams;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.ig0;
import com.fossil.pt7;
import com.fossil.sg7;
import com.fossil.x87;
import com.fossil.zd7;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParamsFileMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String dataContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WatchParamsFileMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WatchParamsFileMapping createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    ee7.a((Object) cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(Parcel.class);
                    ee7.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(parcel);
                    if (newInstance != null) {
                        return (WatchParamsFileMapping) newInstance;
                    }
                    throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WatchParamsFileMapping[] newArray(int i) {
            return new WatchParamsFileMapping[i];
        }
    }

    @DexIgnore
    public WatchParamsFileMapping(String str) {
        ee7.b(str, "dataContent");
        this.dataContent = str;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void initialize() {
        String str = this.dataContent;
        Charset charset = sg7.a;
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            this.data = pt7.a(bytes);
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final ig0 toSDKSetting() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new ig0(bArr);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(WatchParamsFileMapping.class.getName());
        parcel.writeString(this.dataContent);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WatchParamsFileMapping(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r2 = r2.readString()
            if (r2 == 0) goto L_0x000c
            goto L_0x000e
        L_0x000c:
            java.lang.String r2 = ""
        L_0x000e:
            r1.<init>(r2)
            r1.initialize()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping.<init>(android.os.Parcel):void");
    }
}
