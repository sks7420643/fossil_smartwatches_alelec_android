package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.fg0;
import com.fossil.x87;
import com.fossil.zd7;
import com.google.gson.Gson;
import io.flutter.plugin.common.StandardMessageCodec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalizationData implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<LocalizationData> CREATOR; // = new LocalizationData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String filePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        private final String bytesToString(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & 255) + StandardMessageCodec.NULL, 16);
                ee7.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLocalizationDataCheckSum(LocalizationData localizationData) {
            ee7.b(localizationData, "localizationData");
            if (ee7.a((Object) localizationData.getCheckSum(), (Object) "")) {
                return bytesToString(localizationData.toLocalizationFile().getData());
            }
            return localizationData.getCheckSum();
        }

        @DexIgnore
        public final boolean isTheSameFile(LocalizationData localizationData, LocalizationData localizationData2) {
            ee7.b(localizationData2, "newLocalizationData");
            if (localizationData == null) {
                return false;
            }
            String localizationDataCheckSum = getLocalizationDataCheckSum(localizationData2);
            if (!ee7.a((Object) localizationData.getFilePath(), (Object) localizationData2.getFilePath()) || !ee7.a((Object) localizationData.getCheckSum(), (Object) localizationDataCheckSum)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public LocalizationData(String str, String str2) {
        ee7.b(str, "filePath");
        ee7.b(str2, "checkSum");
        this.filePath = str;
        this.checkSum = str2;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getFilePath() {
        return this.filePath;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        com.fossil.hc7.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        r2 = move-exception;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void initialize() {
        /*
            r3 = this;
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x001b }
            java.lang.String r1 = r3.filePath     // Catch:{ Exception -> 0x001b }
            r0.<init>(r1)     // Catch:{ Exception -> 0x001b }
            r1 = 0
            byte[] r2 = com.fossil.gc7.a(r0)     // Catch:{ all -> 0x0014 }
            r3.data = r2     // Catch:{ all -> 0x0014 }
            com.fossil.i97 r2 = com.fossil.i97.a     // Catch:{ all -> 0x0014 }
            com.fossil.hc7.a(r0, r1)
            goto L_0x0030
        L_0x0014:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0016 }
        L_0x0016:
            r2 = move-exception
            com.fossil.hc7.a(r0, r1)
            throw r2
        L_0x001b:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            r0.printStackTrace()
            com.fossil.i97 r0 = com.fossil.i97.a
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r2 = "LocalizationData"
            r1.e(r2, r0)
        L_0x0030:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.LocalizationData.initialize():void");
    }

    @DexIgnore
    public final boolean isDataValid() {
        return this.data != null;
    }

    @DexIgnore
    public final void setCheckSum(String str) {
        ee7.b(str, "<set-?>");
        this.checkSum = str;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public final fg0 toLocalizationFile() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new fg0(bArr);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(LocalizationData.class.getName());
        parcel.writeString(this.filePath);
        parcel.writeString(this.checkSum);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ LocalizationData(String str, String str2, int i, zd7 zd7) {
        this(str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LocalizationData(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r3, r0)
            java.lang.String r0 = r3.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r3 = r3.readString()
            if (r3 == 0) goto L_0x0016
            r1 = r3
        L_0x0016:
            r2.<init>(r0, r1)
            r2.initialize()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.LocalizationData.<init>(android.os.Parcel):void");
    }
}
