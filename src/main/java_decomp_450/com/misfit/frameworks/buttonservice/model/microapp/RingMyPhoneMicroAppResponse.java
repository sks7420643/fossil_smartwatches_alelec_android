package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cc0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.if0;
import com.fossil.r60;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingMyPhoneMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public RingMyPhoneMicroAppResponse() {
        super(cb0.RING_MY_PHONE_MICRO_APP);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof cc0) || r60 == null) {
            return null;
        }
        return new if0((cc0) yb0, r60);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
    }
}
