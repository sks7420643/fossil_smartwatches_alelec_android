package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.oe0;
import com.fossil.r60;
import com.fossil.sb0;
import com.fossil.x87;
import com.fossil.xe0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCListChallengeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public List<oe0> challenges;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(List<oe0> list) {
        super(cb0.BUDDY_CHALLENGE_LIST_CHALLENGES);
        ee7.b(list, "challenges");
        this.challenges = new ArrayList();
        this.challenges = list;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof sb0)) {
            return null;
        }
        sb0 sb0 = (sb0) yb0;
        Object[] array = this.challenges.toArray(new oe0[0]);
        if (array != null) {
            return new xe0(sb0, (oe0[]) array);
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeList(this.challenges);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.challenges = arrayList;
        parcel.readList(arrayList, oe0.class.getClassLoader());
    }
}
