package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import com.fossil.be4;
import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.je4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingDeserializer implements fe4<ComplicationAppMapping> {
    @DexIgnore
    @Override // com.fossil.fe4
    public ComplicationAppMapping deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        ee7.b(jsonElement, "json");
        ee7.b(type, "typeOfT");
        ee7.b(ee4, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = ComplicationAppMappingDeserializer.class.getName();
        ee7.a((Object) name, "ComplicationAppMappingDe\u2026rializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(ComplicationAppMapping.Companion.getFIELD_TYPE());
        ee7.a((Object) a, "jsonType");
        int b = a.b();
        be4 be4 = new be4();
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, WeatherComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getHEART_RATE_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, HeartRateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getSTEPS_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, StepsComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getDATE_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, DateComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getEMPTY_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, NoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCHANCE_OF_RAIN()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, ChanceOfRainComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, SecondTimezoneComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getACTIVE_MINUTES()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, ActiveMinutesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getCALORIES()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, CaloriesComplicationAppMapping.class);
        }
        if (b == ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getBATTERY()) {
            return (ComplicationAppMapping) be4.a().a(jsonElement, BatteryComplicationAppMapping.class);
        }
        return null;
    }
}
