package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReplyMessageMappingGroup implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String iconFwPath;
    @DexIgnore
    public List<ReplyMessageMapping> replyMessageList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ReplyMessageMappingGroup> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMappingGroup createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new ReplyMessageMappingGroup(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMappingGroup[] newArray(int i) {
            return new ReplyMessageMappingGroup[i];
        }
    }

    @DexIgnore
    public ReplyMessageMappingGroup(List<ReplyMessageMapping> list, String str) {
        ee7.b(list, "replyMessageList");
        ee7.b(str, "iconFwPath");
        this.replyMessageList = list;
        this.iconFwPath = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final List<ReplyMessageMapping> getReplyMessageList() {
        return this.replyMessageList;
    }

    @DexIgnore
    public final void setIconFwPath(String str) {
        ee7.b(str, "<set-?>");
        this.iconFwPath = str;
    }

    @DexIgnore
    public final void setReplyMessageList(List<ReplyMessageMapping> list) {
        ee7.b(list, "<set-?>");
        this.replyMessageList = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedList(this.replyMessageList);
            parcel.writeString(this.iconFwPath);
        }
    }

    @DexIgnore
    public ReplyMessageMappingGroup(Parcel parcel) {
        ee7.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.replyMessageList = arrayList;
        parcel.readTypedList(arrayList, ReplyMessageMapping.CREATOR);
        String readString = parcel.readString();
        this.iconFwPath = readString == null ? "" : readString;
    }
}
