package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.af0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.r60;
import com.fossil.wb0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeTravelMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mTravelTimeInMinute;

    @DexIgnore
    public CommuteTimeTravelMicroAppResponse(int i) {
        super(cb0.COMMUTE_TIME_TRAVEL_MICRO_APP);
        this.mTravelTimeInMinute = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof wb0) || r60 == null) {
            return null;
        }
        return new af0((wb0) yb0, r60, this.mTravelTimeInMinute);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mTravelTimeInMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.mTravelTimeInMinute = parcel.readInt();
    }
}
