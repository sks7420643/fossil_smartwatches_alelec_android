package com.misfit.frameworks.buttonservice.model.notification;

import com.fossil.le7;
import com.fossil.te7;
import com.fossil.uf7;
import com.fossil.vd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationBaseObj$toSilentNotification$Anon1 extends le7 {
    @DexIgnore
    public NotificationBaseObj$toSilentNotification$Anon1(NotificationBaseObj notificationBaseObj) {
        super(notificationBaseObj);
    }

    @DexIgnore
    @Override // com.fossil.ag7
    public Object get() {
        return ((NotificationBaseObj) ((vd7) this).receiver).getNotificationFlags();
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public String getName() {
        return "notificationFlags";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public uf7 getOwner() {
        return te7.a(NotificationBaseObj.class);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public String getSignature() {
        return "getNotificationFlags()Ljava/util/List;";
    }
}
