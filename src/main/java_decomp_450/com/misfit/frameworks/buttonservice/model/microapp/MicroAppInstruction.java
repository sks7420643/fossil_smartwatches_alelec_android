package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.pg0;
import com.fossil.qg0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppInstruction implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MicroAppInstruction> CREATOR; // = new MicroAppInstruction$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public pg0 declarationID;
    @DexIgnore
    public qg0 variantID;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Parcelable.Creator<MicroAppInstruction> getCREATOR() {
            return MicroAppInstruction.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<MicroAppInstruction> creator) {
            MicroAppInstruction.CREATOR = creator;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum MicroAppID {
        UAPP_HID_MEDIA_CONTROL_MUSIC("music-control", true, false),
        UAPP_HID_MEDIA_VOL_UP_ID("music-volumn-up", true, false),
        UAPP_HID_MEDIA_VOL_DOWN_ID("music-volumn-down", true, false),
        UAPP_ACTIVITY_TAGGING_ID(Constants.ACTIVITY, false, false),
        UAPP_GOAL_TRACKING_ID("goal-tracking", false, false),
        UAPP_DATE_ID("date", false, false),
        UAPP_TIME2_ID("second-time-zone", false, false),
        UAPP_ALERT_ID("alert", false, false),
        UAPP_ALARM_ID(Alarm.TABLE_NAME, false, false),
        UAPP_PROGRESS_ID(Constants.ACTIVITY, false, false),
        UAPP_WEATHER_STANDARD("weather", false, true),
        UAPP_COMMUTE_TIME("commute-time", false, true),
        UAPP_TOGGLE_MODE("sequence", false, false),
        UAPP_RING_PHONE("ring-my-phone", false, true),
        UAPP_SELFIE(Constants.SELFIE, true, false),
        UAPP_STOPWATCH("stopwatch", false, false),
        UAPP_UNKNOWN("unknown", false, false);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ boolean isNeedHID;
        @DexIgnore
        public /* final */ boolean isNeedStreaming;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MicroAppID getMicroAppId(String str) {
                ee7.b(str, "value");
                MicroAppID[] values = MicroAppID.values();
                int length = values.length;
                for (int i = 0; i < length; i++) {
                    if (ee7.a((Object) values[i].getValue(), (Object) str)) {
                        return values[i];
                    }
                }
                return MicroAppID.UAPP_TOGGLE_MODE;
            }

            @DexIgnore
            public final MicroAppID getMicroAppIdFromDeviceEventId(int i) {
                if (i == cb0.RING_MY_PHONE_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_RING_PHONE;
                }
                if (i == cb0.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == cb0.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_COMMUTE_TIME;
                }
                return MicroAppID.UAPP_UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public MicroAppID(String str, boolean z, boolean z2) {
            this.value = str;
            this.isNeedHID = z;
            this.isNeedStreaming = z2;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final boolean isNeedHID() {
            return this.isNeedHID;
        }

        @DexIgnore
        public final boolean isNeedStreaming() {
            return this.isNeedStreaming;
        }
    }

    @DexIgnore
    public MicroAppInstruction(pg0 pg0, qg0 qg0) {
        ee7.b(pg0, "declarationID");
        ee7.b(qg0, "variantID");
        String name = MicroAppInstruction.class.getName();
        ee7.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = pg0;
        this.variantID = qg0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final pg0 getDeclarationID() {
        return this.declarationID;
    }

    @DexIgnore
    public abstract cf0 getMicroAppData();

    @DexIgnore
    public final qg0 getVariantID() {
        return this.variantID;
    }

    @DexIgnore
    public final void setDeclarationID(pg0 pg0) {
        this.declarationID = pg0;
    }

    @DexIgnore
    public final void setVariantID(qg0 qg0) {
        this.variantID = qg0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.className);
        pg0 pg0 = this.declarationID;
        if (pg0 != null) {
            parcel.writeInt(pg0.ordinal());
            qg0 qg0 = this.variantID;
            if (qg0 != null) {
                parcel.writeInt(qg0.ordinal());
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public MicroAppInstruction(Parcel parcel) {
        ee7.b(parcel, "in");
        String name = MicroAppInstruction.class.getName();
        ee7.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = pg0.values()[parcel.readInt()];
        this.variantID = qg0.values()[parcel.readInt()];
    }
}
