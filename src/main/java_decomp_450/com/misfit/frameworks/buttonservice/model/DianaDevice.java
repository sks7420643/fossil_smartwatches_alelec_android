package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DianaDevice extends Device {
    @DexIgnore
    public static /* final */ int DIANA_OTA_RESET_TIME; // = 30000;

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.Device
    public int getOTAResetTime() {
        return 30000;
    }
}
