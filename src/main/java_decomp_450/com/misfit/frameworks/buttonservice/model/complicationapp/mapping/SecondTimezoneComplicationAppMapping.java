package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.facebook.places.model.PlaceFields;
import com.fossil.ee7;
import com.fossil.s70;
import com.fossil.te0;
import com.fossil.z70;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public String location;
    @DexIgnore
    public int utcOffsetInMinutes;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(String str, int i) {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE());
        ee7.b(str, PlaceFields.LOCATION);
        this.location = str;
        this.utcOffsetInMinutes = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public String getHash() {
        String str = getMType() + ":" + this.location + ":" + this.utcOffsetInMinutes;
        ee7.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public s70 toSDKSetting(boolean z) {
        return new z70(new te0(this.location, this.utcOffsetInMinutes));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.location);
        parcel.writeInt(this.utcOffsetInMinutes);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        this.utcOffsetInMinutes = parcel.readInt();
    }
}
