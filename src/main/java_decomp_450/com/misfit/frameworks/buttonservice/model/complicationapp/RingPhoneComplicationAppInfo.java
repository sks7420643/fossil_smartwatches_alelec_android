package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.ag0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.dc0;
import com.fossil.ee7;
import com.fossil.jf0;
import com.fossil.r60;
import com.fossil.yb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingPhoneComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public ag0 mRingPhoneState; // = ag0.OFF;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(ag0 ag0) {
        super(cb0.RING_PHONE);
        ee7.b(ag0, "ringPhoneState");
        this.mRingPhoneState = ag0;
    }

    @DexIgnore
    public final ag0 getMRingPhoneState() {
        return this.mRingPhoneState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return new jf0(this.mRingPhoneState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (yb0 instanceof dc0) {
            return new jf0((dc0) yb0, this.mRingPhoneState);
        }
        return null;
    }

    @DexIgnore
    public final void setMRingPhoneState(ag0 ag0) {
        ee7.b(ag0, "<set-?>");
        this.mRingPhoneState = ag0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mRingPhoneState.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.mRingPhoneState = ag0.values()[parcel.readInt()];
    }
}
