package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.facebook.share.internal.ShareConstants;
import com.fossil.bf0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.n70;
import com.fossil.r60;
import com.fossil.xb0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public int commuteTimeInMinute;
    @DexIgnore
    public String destination;
    @DexIgnore
    public String traffic;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(String str, int i, String str2) {
        super(cb0.COMMUTE_TIME_WATCH_APP);
        ee7.b(str, ShareConstants.DESTINATION);
        ee7.b(str2, "traffic");
        this.destination = "";
        this.traffic = "";
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return new bf0(new n70(this.destination, this.commuteTimeInMinute, this.traffic));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (yb0 instanceof xb0) {
            return new bf0((xb0) yb0, new n70(this.destination, this.commuteTimeInMinute, this.traffic));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.destination);
        parcel.writeInt(this.commuteTimeInMinute);
        parcel.writeString(this.traffic);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String str = "";
        this.destination = str;
        this.traffic = str;
        String readString = parcel.readString();
        this.destination = readString == null ? str : readString;
        this.commuteTimeInMinute = parcel.readInt();
        String readString2 = parcel.readString();
        this.traffic = readString2 != null ? readString2 : str;
    }
}
