package com.misfit.frameworks.buttonservice.model.watchapp.response.workout;

import android.os.Parcel;
import com.fossil.bc0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.df0;
import com.fossil.ee7;
import com.fossil.hf0;
import com.fossil.r60;
import com.fossil.uf0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResumeWorkoutInfoData extends DeviceAppResponse {
    @DexIgnore
    public uf0 deviceMessageType;
    @DexIgnore
    public bc0 mResumeRequest;
    @DexIgnore
    public String message;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ResumeWorkoutInfoData(bc0 bc0, String str, uf0 uf0) {
        super(cb0.WORKOUT_RESUME);
        ee7.b(bc0, "resumeWorkoutRequest");
        ee7.b(str, "message");
        ee7.b(uf0, "deviceMessageType");
        this.message = "";
        this.deviceMessageType = uf0.SUCCESS;
        this.message = str;
        this.deviceMessageType = uf0;
        this.mResumeRequest = bc0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (yb0 instanceof bc0) {
            return new hf0((bc0) yb0, new df0(this.message, this.deviceMessageType));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
        parcel.writeParcelable(this.mResumeRequest, i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ResumeWorkoutInfoData(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String str = "";
        this.message = str;
        this.deviceMessageType = uf0.SUCCESS;
        String readString = parcel.readString();
        this.message = readString != null ? readString : str;
        this.deviceMessageType = uf0.values()[parcel.readInt()];
        this.mResumeRequest = (bc0) parcel.readParcelable(bc0.class.getClassLoader());
    }
}
