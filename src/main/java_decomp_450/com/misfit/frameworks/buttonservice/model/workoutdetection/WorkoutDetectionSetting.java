package com.misfit.frameworks.buttonservice.model.workoutdetection;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public List<WorkoutDetectionItem> workoutDetectionList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WorkoutDetectionSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionSetting createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new WorkoutDetectionSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionSetting[] newArray(int i) {
            return new WorkoutDetectionSetting[i];
        }
    }

    @DexIgnore
    public WorkoutDetectionSetting(Parcel parcel) {
        ee7.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.workoutDetectionList = arrayList;
        parcel.readTypedList(arrayList, WorkoutDetectionItem.CREATOR);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<WorkoutDetectionItem> getWorkoutDetectionList() {
        return this.workoutDetectionList;
    }

    @DexIgnore
    public final void setWorkoutDetectionList(List<WorkoutDetectionItem> list) {
        ee7.b(list, "<set-?>");
        this.workoutDetectionList = list;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeTypedList(this.workoutDetectionList);
    }

    @DexIgnore
    public WorkoutDetectionSetting(List<WorkoutDetectionItem> list) {
        ee7.b(list, "workoutDetectionList");
        this.workoutDetectionList = list;
    }
}
