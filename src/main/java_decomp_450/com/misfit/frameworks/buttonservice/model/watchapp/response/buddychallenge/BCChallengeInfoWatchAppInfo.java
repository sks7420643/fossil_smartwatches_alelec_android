package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.ne0;
import com.fossil.r60;
import com.fossil.rb0;
import com.fossil.we0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCChallengeInfoWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public ne0 challenge;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(ne0 ne0) {
        super(cb0.BUDDY_CHALLENGE_GET_INFO);
        ee7.b(ne0, "challenge");
        this.challenge = ne0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof rb0)) {
            return null;
        }
        rb0 rb0 = (rb0) yb0;
        ne0 ne0 = this.challenge;
        if (ne0 != null) {
            return new we0(rb0, ne0);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.challenge, i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.challenge = (ne0) parcel.readParcelable(ne0.class.getClassLoader());
    }
}
