package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.r60;
import com.fossil.yb0;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeTimeObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceAppResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<DeviceAppResponse> CREATOR; // = new DeviceAppResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ long LIFE_TIME; // = 8000;
    @DexIgnore
    public cb0 deviceEventId;
    @DexIgnore
    public boolean isForceUpdate;
    @DexIgnore
    public /* final */ LifeTimeObject lifeTimeObject;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public DeviceAppResponse(cb0 cb0) {
        ee7.b(cb0, "deviceEventId");
        this.deviceEventId = cb0;
        this.lifeTimeObject = new LifeTimeObject(LIFE_TIME);
    }

    @DexIgnore
    public static /* synthetic */ cf0 getSDKDeviceResponse$default(DeviceAppResponse deviceAppResponse, yb0 yb0, r60 r60, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                r60 = null;
            }
            return deviceAppResponse.getSDKDeviceResponse(yb0, r60);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSDKDeviceResponse");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final cb0 getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final LifeTimeObject getLifeTimeObject() {
        return this.lifeTimeObject;
    }

    @DexIgnore
    public abstract cf0 getSDKDeviceData();

    @DexIgnore
    public abstract cf0 getSDKDeviceResponse(yb0 yb0, r60 r60);

    @DexIgnore
    public final boolean isForceUpdate() {
        return this.isForceUpdate;
    }

    @DexIgnore
    public final void setDeviceEventId(cb0 cb0) {
        ee7.b(cb0, "<set-?>");
        this.deviceEventId = cb0;
    }

    @DexIgnore
    public final void setForceUpdate(boolean z) {
        this.isForceUpdate = z;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.deviceEventId.ordinal());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeviceAppResponse(Parcel parcel) {
        this(cb0.values()[parcel.readInt()]);
        ee7.b(parcel, "parcel");
    }
}
