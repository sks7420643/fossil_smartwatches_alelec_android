package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<ComplicationAppMappingSettings> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ComplicationAppMappingSettings createFromParcel(Parcel parcel) {
        ee7.b(parcel, "parcel");
        return new ComplicationAppMappingSettings(parcel, null);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ComplicationAppMappingSettings[] newArray(int i) {
        return new ComplicationAppMappingSettings[i];
    }
}
