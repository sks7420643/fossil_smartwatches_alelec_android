package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.a80;
import com.fossil.ee7;
import com.fossil.s70;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public WeatherComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getWEATHER_TYPE());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public s70 toSDKSetting(boolean z) {
        return new a80();
    }

    @DexIgnore
    public WeatherComplicationAppMapping(Parcel parcel) {
        super(parcel);
    }
}
