package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.gc0;
import com.fossil.of0;
import com.fossil.r60;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.ya0;
import com.fossil.yb0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherInfoWatchAppResponse extends DeviceAppResponse {
    @DexIgnore
    public /* final */ List<WeatherWatchAppInfo> listOfWeatherInfo;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3, int i, zd7 zd7) {
        this(weatherWatchAppInfo, (i & 2) != 0 ? null : weatherWatchAppInfo2, (i & 4) != 0 ? null : weatherWatchAppInfo3);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherAppInfo());
        }
        Object[] array = arrayList.toArray(new ya0[0]);
        if (array != null) {
            return new of0((ya0[]) array);
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof gc0)) {
            return null;
        }
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherAppInfo());
        }
        gc0 gc0 = (gc0) yb0;
        Object[] array = arrayList.toArray(new ya0[0]);
        if (array != null) {
            return new of0(gc0, (ya0[]) array);
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.listOfWeatherInfo);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3) {
        super(cb0.WEATHER_WATCH_APP);
        ee7.b(weatherWatchAppInfo, "firstWeatherWatchAppInfo");
        ArrayList arrayList = new ArrayList();
        this.listOfWeatherInfo = arrayList;
        arrayList.add(weatherWatchAppInfo);
        if (weatherWatchAppInfo2 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo2);
        }
        if (weatherWatchAppInfo3 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo3);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.listOfWeatherInfo = arrayList;
        parcel.readTypedList(arrayList, WeatherWatchAppInfo.CREATOR);
    }
}
