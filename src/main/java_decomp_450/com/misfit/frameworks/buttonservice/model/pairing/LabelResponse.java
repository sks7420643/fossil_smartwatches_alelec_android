package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelResponse extends PairingResponse {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_VERSION; // = "0.0";
    @DexIgnore
    public int code;
    @DexIgnore
    public String version;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public LabelResponse(String str, int i) {
        ee7.b(str, "version");
        this.version = "0.0";
        this.version = str;
        this.code = i;
    }

    @DexIgnore
    public final int getCode() {
        return this.code;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public final void setCode(int i) {
        this.code = i;
    }

    @DexIgnore
    public final void setVersion(String str) {
        ee7.b(str, "<set-?>");
        this.version = str;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.pairing.PairingResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.version);
        parcel.writeInt(this.code);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LabelResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String str = "0.0";
        this.version = str;
        String readString = parcel.readString();
        this.version = readString != null ? readString : str;
        this.code = parcel.readInt();
    }
}
