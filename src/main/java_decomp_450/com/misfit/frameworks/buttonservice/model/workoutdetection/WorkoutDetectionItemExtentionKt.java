package com.misfit.frameworks.buttonservice.model.workoutdetection;

import com.fossil.ee7;
import com.fossil.x80;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.z80;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionItemExtentionKt {
    @DexIgnore
    public static final z80[] toAutoWorkoutDetectionItemConfig(List<WorkoutDetectionItem> list) {
        ee7.b(list, "$this$toAutoWorkoutDetectionItemConfig");
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        for (T t : list) {
            arrayList.add(new z80(t.getType(), t.isEnable(), t.getUserConfirmation(), new x80((short) t.getStartLatency(), (short) t.getPauseLatency(), (short) t.getResumeLatency(), (short) t.getStopLatency())));
        }
        Object[] array = arrayList.toArray(new z80[0]);
        if (array != null) {
            return (z80[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
