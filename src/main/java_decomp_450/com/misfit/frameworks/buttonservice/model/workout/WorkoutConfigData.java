package com.misfit.frameworks.buttonservice.model.workout;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WorkoutConfigData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutConfigData> CREATOR; // = new Anon1();
    @DexIgnore
    public int chainring;
    @DexIgnore
    public int cog;
    @DexIgnore
    public int diameterInMillimeter;
    @DexIgnore
    public int tireSizeInMillimeter;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<WorkoutConfigData> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutConfigData createFromParcel(Parcel parcel) {
            return new WorkoutConfigData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutConfigData[] newArray(int i) {
            return new WorkoutConfigData[i];
        }
    }

    @DexIgnore
    public WorkoutConfigData(int i, int i2, int i3, int i4) {
        this.diameterInMillimeter = i;
        this.tireSizeInMillimeter = i2;
        this.chainring = i3;
        this.cog = i4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.diameterInMillimeter);
        parcel.writeInt(this.tireSizeInMillimeter);
        parcel.writeInt(this.chainring);
        parcel.writeInt(this.cog);
    }

    @DexIgnore
    public WorkoutConfigData(Parcel parcel) {
        this.diameterInMillimeter = parcel.readInt();
        this.tireSizeInMillimeter = parcel.readInt();
        this.chainring = parcel.readInt();
        this.cog = parcel.readInt();
    }
}
