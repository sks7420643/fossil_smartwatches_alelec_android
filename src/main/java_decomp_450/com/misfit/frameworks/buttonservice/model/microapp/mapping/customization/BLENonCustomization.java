package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.lg0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BLENonCustomization extends BLECustomization {
    @DexIgnore
    public BLENonCustomization() {
        super(0);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public lg0 getCustomizationFrame() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public String getHash() {
        return getType() + ":non";
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLENonCustomization(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "in");
    }
}
