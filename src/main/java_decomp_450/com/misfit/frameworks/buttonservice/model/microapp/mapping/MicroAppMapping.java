package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import android.os.Parcel;
import android.util.Base64;
import com.fossil.jg0;
import com.fossil.mg0;
import com.fossil.ng0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.common.enums.Gesture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MicroAppMapping extends BLEMapping implements Comparable {
    public static final String TAG = "MicroAppMapping";
    public BLECustomization customization;
    public String[] declarationFiles;
    public Gesture gesture;
    public String microAppId;

    public static /* synthetic */ class Anon1 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture;

        /* JADX WARNING: Can't wrap try/catch for region: R(42:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|(3:41|42|44)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(44:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|44) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0084 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0090 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00a8 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b4 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c0 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00cc */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00d8 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00e4 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00f0 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.misfit.frameworks.common.enums.Gesture[] r0 = com.misfit.frameworks.common.enums.Gesture.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture = r0
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_PRESSED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x001d }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x003e }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_PRESSED     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x006c }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x0090 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x0090 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0090 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0090 }
            L_0x0090:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x009c }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x009c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009c }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009c }
            L_0x009c:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00a8 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00a8 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a8 }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00a8 }
            L_0x00a8:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00b4 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_PRESSED     // Catch:{ NoSuchFieldError -> 0x00b4 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b4 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b4 }
            L_0x00b4:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00c0 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_SINGLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00c0 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c0 }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00c0 }
            L_0x00c0:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00cc }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00cc }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00cc }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00cc }
            L_0x00cc:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00d8 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_DOUBLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00d8 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00d8 }
                r2 = 18
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00d8 }
            L_0x00d8:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00e4 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00e4 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e4 }
                r2 = 19
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00e4 }
            L_0x00e4:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00f0 }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_TRIPLE_PRESS     // Catch:{ NoSuchFieldError -> 0x00f0 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00f0 }
                r2 = 20
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00f0 }
            L_0x00f0:
                int[] r0 = com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture     // Catch:{ NoSuchFieldError -> 0x00fc }
                com.misfit.frameworks.common.enums.Gesture r1 = com.misfit.frameworks.common.enums.Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD     // Catch:{ NoSuchFieldError -> 0x00fc }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00fc }
                r2 = 21
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00fc }
            L_0x00fc:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping.Anon1.<clinit>():void");
        }
        */
    }

    public MicroAppMapping(Gesture gesture2, String str, String str2, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        String[] strArr = new String[1];
        this.declarationFiles = strArr;
        strArr[0] = str2;
        this.customization = new BLENonCustomization();
    }

    public static List<BLEMapping> convertToBLEMapping(List<MicroAppMapping> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        return arrayList;
    }

    public static List<MicroAppMapping> convertToMicroAppMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((MicroAppMapping) it.next());
        }
        return arrayList;
    }

    public static List<jg0> convertToSDKMapping(List<BLEMapping> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<BLEMapping> it = list.iterator();
        while (it.hasNext()) {
            MicroAppMapping microAppMapping = (MicroAppMapping) it.next();
            ArrayList arrayList2 = new ArrayList();
            byte[] bArr = new byte[0];
            String[] strArr = microAppMapping.declarationFiles;
            for (String str : strArr) {
                if (str != null) {
                    bArr = Base64.decode(str, 0);
                }
                FLogger.INSTANCE.getLocal().d(TAG, "convertToMicroAppMapping appId is " + microAppMapping.microAppId + " and extraInfo=" + str);
                arrayList2.add(new mg0(bArr, microAppMapping.customization.getCustomizationFrame()));
            }
            arrayList.add(new jg0(microAppMapping.getButton(), (mg0[]) arrayList2.toArray(new mg0[0])));
        }
        return arrayList;
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        if (obj != null && (obj instanceof MicroAppMapping)) {
            MicroAppMapping microAppMapping = (MicroAppMapping) obj;
            if (microAppMapping.getGesture() == getGesture()) {
                return 0;
            }
            if (microAppMapping.getGesture().getValue() > getGesture().getValue()) {
                return -1;
            }
        }
        return 1;
    }

    public ng0 getButton() {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[this.gesture.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return ng0.TOP;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return ng0.MIDDLE;
            default:
                return ng0.BOTTOM;
        }
    }

    public BLECustomization getCustomization() {
        return this.customization;
    }

    public String[] getDeclarationFiles() {
        return this.declarationFiles;
    }

    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public Gesture getGesture() {
        return this.gesture;
    }

    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(((BLEMapping) this).mType);
        sb.append(":");
        sb.append(this.gesture);
        sb.append(":");
        sb.append(this.microAppId);
        sb.append(":");
        for (String str : this.declarationFiles) {
            sb.append(str);
            sb.append(":");
        }
        sb.append(this.customization.getHash());
        return sb.toString();
    }

    public String getMicroAppId() {
        return this.microAppId;
    }

    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedHID() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedHID();
    }

    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedStreaming() {
        return MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.microAppId).isNeedStreaming();
    }

    public void setCustomization(BLECustomization bLECustomization) {
        this.customization = bLECustomization;
    }

    public void setGesture(Gesture gesture2) {
        this.gesture = gesture2;
    }

    public void setMicroAppId(String str) {
        this.microAppId = str;
    }

    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.gesture.getValue());
        parcel.writeString(this.microAppId);
        parcel.writeInt(this.declarationFiles.length);
        for (String str : this.declarationFiles) {
            parcel.writeString(str);
        }
        this.customization.writeToParcel(parcel, i);
    }

    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, Long l) {
        super(2, l.longValue());
        this.gesture = gesture2;
        this.microAppId = str;
        this.declarationFiles = strArr;
        this.customization = new BLENonCustomization();
    }

    public MicroAppMapping(Gesture gesture2, String str, String[] strArr, BLECustomization bLECustomization, Long l) {
        this(gesture2, str, strArr, l);
        this.customization = bLECustomization;
    }

    public MicroAppMapping(Parcel parcel) {
        super(parcel);
        this.gesture = Gesture.fromInt(parcel.readInt());
        this.microAppId = parcel.readString();
        int readInt = parcel.readInt();
        this.declarationFiles = new String[readInt];
        for (int i = 0; i < readInt; i++) {
            this.declarationFiles[i] = parcel.readString();
        }
        this.customization = BLECustomization.Companion.getCREATOR().createFromParcel(parcel);
    }
}
