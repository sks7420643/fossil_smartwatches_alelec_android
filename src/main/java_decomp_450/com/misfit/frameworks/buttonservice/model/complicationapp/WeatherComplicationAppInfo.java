package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.fc0;
import com.fossil.g90;
import com.fossil.i90;
import com.fossil.nf0;
import com.fossil.p87;
import com.fossil.r60;
import com.fossil.ua0;
import com.fossil.yb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public float temperature;
    @DexIgnore
    public TemperatureUnit temperatureUnit;
    @DexIgnore
    public WeatherCondition weatherCondition;

    @DexIgnore
    public enum TemperatureUnit {
        C,
        F;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public final g90 toSdkTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return g90.C;
            }
            if (i == 2) {
                return g90.F;
            }
            throw new p87();
        }
    }

    @DexIgnore
    public enum WeatherCondition {
        CLEAR_DAY,
        CLEAR_NIGHT,
        RAIN,
        SNOW,
        SLEET,
        WIND,
        FOG,
        CLOUDY,
        PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherCondition.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherCondition.CLEAR_DAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherCondition.CLEAR_NIGHT.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherCondition.RAIN.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherCondition.SNOW.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherCondition.SLEET.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherCondition.WIND.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherCondition.FOG.ordinal()] = 7;
                $EnumSwitchMapping$0[WeatherCondition.CLOUDY.ordinal()] = 8;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_DAY.ordinal()] = 9;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_NIGHT.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public final i90 toSdkWeatherCondition() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return i90.CLEAR_DAY;
                case 2:
                    return i90.CLEAR_NIGHT;
                case 3:
                    return i90.RAIN;
                case 4:
                    return i90.SNOW;
                case 5:
                    return i90.SLEET;
                case 6:
                    return i90.WIND;
                case 7:
                    return i90.FOG;
                case 8:
                    return i90.CLOUDY;
                case 9:
                    return i90.PARTLY_CLOUDY_DAY;
                case 10:
                    return i90.PARTLY_CLOUDY_NIGHT;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(float f, TemperatureUnit temperatureUnit2, WeatherCondition weatherCondition2, long j) {
        super(cb0.WEATHER_COMPLICATION);
        ee7.b(temperatureUnit2, "temperatureUnit");
        ee7.b(weatherCondition2, "weatherCondition");
        this.temperature = f;
        this.temperatureUnit = temperatureUnit2;
        this.weatherCondition = weatherCondition2;
        this.expiredAt = j;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return new nf0(new ua0(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof fc0)) {
            return null;
        }
        return new nf0((fc0) yb0, new ua0(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.temperatureUnit.ordinal());
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.temperature = parcel.readFloat();
        this.temperatureUnit = TemperatureUnit.values()[parcel.readInt()];
        this.weatherCondition = WeatherCondition.values()[parcel.readInt()];
        this.expiredAt = parcel.readLong();
    }
}
