package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.xa0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherHourForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ int hourIn24Format;
    @DexIgnore
    public /* final */ float temperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherHourForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherHourForecast createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new WeatherHourForecast(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherHourForecast[] newArray(int i) {
            return new WeatherHourForecast[i];
        }
    }

    @DexIgnore
    public WeatherHourForecast(int i, float f, WeatherComplicationAppInfo.WeatherCondition weatherCondition2) {
        ee7.b(weatherCondition2, "weatherCondition");
        this.hourIn24Format = (i < 0 || 24 <= i) ? 0 : i;
        this.temperature = f;
        this.weatherCondition = weatherCondition2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final xa0 toSDKWeatherHourForecast() {
        return new xa0(this.hourIn24Format, this.temperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeInt(this.hourIn24Format);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.weatherCondition.ordinal());
    }

    @DexIgnore
    public WeatherHourForecast(Parcel parcel) {
        ee7.b(parcel, "parcel");
        this.hourIn24Format = parcel.readInt();
        this.temperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
    }
}
