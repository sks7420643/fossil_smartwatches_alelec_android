package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingConcrete extends BLEMapping {
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public Gesture getGesture() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public String getHash() {
        return ((BLEMapping) this).mType + "";
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedHID() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping
    public boolean isNeedStreaming() {
        return false;
    }
}
