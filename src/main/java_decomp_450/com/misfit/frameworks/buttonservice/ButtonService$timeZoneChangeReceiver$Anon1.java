package com.misfit.frameworks.buttonservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonService$timeZoneChangeReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public ButtonService$timeZoneChangeReceiver$Anon1(ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ee7.b(context, "context");
        ee7.b(intent, "intent");
        String action = intent.getAction();
        if (ee7.a((Object) "android.intent.action.TIME_SET", (Object) action) || ee7.a((Object) "android.intent.action.TIMEZONE_CHANGED", (Object) action)) {
            FLogger.INSTANCE.getLocal().d(ButtonService.TAG, "Inside .timeZoneChangeReceiver");
            for (String str : DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                ButtonService buttonService = this.this$0;
                ee7.a((Object) str, "serial");
                long unused = buttonService.doUpdateTime(str);
            }
        } else if (ee7.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
            TimeZone timeZone = TimeZone.getDefault();
            try {
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "calendar");
                Calendar instance2 = Calendar.getInstance();
                ee7.a((Object) instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                Calendar instance3 = Calendar.getInstance();
                ee7.a((Object) instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    FLogger.INSTANCE.getLocal().d(ButtonService.TAG, "Inside .timeZoneChangeReceiver - DST change");
                    for (String str2 : DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                        ButtonService buttonService2 = this.this$0;
                        ee7.a((Object) str2, "serial");
                        long unused2 = buttonService2.doUpdateTime(str2);
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = ButtonService.TAG;
                local.e(access$getTAG$cp, ".timeZoneChangeReceiver - ex=" + e.toString());
            }
        }
    }
}
