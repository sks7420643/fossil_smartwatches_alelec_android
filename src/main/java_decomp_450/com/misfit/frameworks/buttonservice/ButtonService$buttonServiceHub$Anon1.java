package com.misfit.frameworks.buttonservice;

import android.location.Location;
import android.os.RemoteException;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import com.fossil.dh0;
import com.fossil.ee7;
import com.fossil.fitness.FitnessData;
import com.fossil.mh7;
import com.fossil.zs1;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.AppInfo;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonService$buttonServiceHub$Anon1 extends IButtonConnectivity.Stub {
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public ButtonService$buttonServiceHub$Anon1(ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void addLog(int i, String str, String str2) {
        ee7.b(str, "serial");
        ee7.b(str2, "message");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.addLog(i, str, str2);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void cancelPairDevice(String str) {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.cancelPairDevice(str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void changePendingLogKey(int i, String str, int i2, String str2) {
        ee7.b(str, "curSerial");
        ee7.b(str2, "newSerial");
        if (this.this$0.isBleSupported(str2)) {
            this.this$0.changePendingLogKey(i, str, i2, str2);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void confirmBCStatus(boolean z) throws RemoteException {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$cp = ButtonService.TAG;
        local.e(access$getTAG$cp, "confirmBCStatus - bcStatus: " + z);
        SharePreferencesUtils.getInstance(this.this$0).setBoolean(SharePreferencesUtils.BC_STATUS, z);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void confirmStopWorkout(String str, boolean z) {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported()) {
            this.this$0.confirmStopWorkout(str, z);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void connectAllButton() throws RemoteException {
        if (this.this$0.isBleSupported()) {
            this.this$0.connectAllButton();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deleteDataFiles(String str) {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.deleteDataFiles(str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deleteHeartRateFiles(List<String> list, String str) throws RemoteException {
        ee7.b(list, "fileIds");
        ee7.b(str, "serial");
        this.this$0.deleteHeartRateFiles(list);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceCancelCalibration(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.cancelCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXIT_CALIBRATION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceClearMapping(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceClearMapping(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.CLEAN_LINK_MAPPINGS, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceCompleteCalibration(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceCompleteCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.APPLY_HAND_POSITION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceDisconnect(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.DISCONNECT, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.deviceDisconnect(str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceForceReconnect(String str, UserProfile userProfile) {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.forceConnect(str, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.FORCE_CONNECT, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceGetBatteryLevel(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceGetBatteryLevel(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceGetCountDown(String str) {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.GET_COUNTDOWN, ServiceActionResult.FAILED, null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceGetRssi(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceGetRssi(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceMovingHand(String str, HandCalibrationObj handCalibrationObj) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(handCalibrationObj, "handCalibrationObj");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.MOVE_HAND, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.deviceMovingHand(str, handCalibrationObj);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceOta(String str, FirmwareData firmwareData, UserProfile userProfile) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str) && !TextUtils.isEmpty(str) && firmwareData != null && userProfile != null) {
            return this.this$0.deviceOta(str, firmwareData, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.OTA, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long devicePlayAnimation(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.devicePlayAnimation(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceReadRealTimeStep(String str) {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceReadRealTimeStep(str) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSendNotification(String str, NotificationBaseObj notificationBaseObj) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(notificationBaseObj, "newNotification");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSendNotification(str, notificationBaseObj);
        }
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceSetAutoCountdownSetting(long j, long j2) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceSetAutoListAlarm(List<? extends Alarm> list) {
        ee7.b(list, "alarmList");
        this.this$0.deviceSetAutoListAlarm(list);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceSetAutoSecondTimezone(String str) {
        ee7.b(str, "secondTimezoneId");
        this.this$0.deviceSetAutoSecondTimezone(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetDisableCountDown(String str) {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COUNTDOWN, ServiceActionResult.FAILED, null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetEnableCountDown(String str, long j, long j2) {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COUNTDOWN, ServiceActionResult.FAILED, null);
        }
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetInactiveNudgeConfig(String str, InactiveNudgeData inactiveNudgeData) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(inactiveNudgeData, "inactiveNudgeData");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetListAlarm(String str, List<? extends Alarm> list) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(list, "alarms");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetListAlarm(str, list);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LIST_ALARM, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetMapping(String str, List<? extends BLEMapping> list) {
        ee7.b(str, "serial");
        ee7.b(list, "mappings");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetMapping(str, list);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_LINK_MAPPING, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetSecondTimeZone(String str, String str2) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(str2, "secondTimezoneId");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetSecondTimeZone(str, str2);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_SECOND_TIMEZONE, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceSetVibrationStrength(String str, VibrationStrengthObj vibrationStrengthObj) {
        ee7.b(str, "serial");
        ee7.b(vibrationStrengthObj, "vibrationStrengthLevelObj");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetVibrationStrength(str, vibrationStrengthObj);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_VIBRATION_STRENGTH, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceStartCalibration(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceStartCalibration(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.ENTER_CALIBRATION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceStartScan() throws RemoteException {
        if (this.this$0.isBleSupported()) {
            ButtonService.access$getScanServiceInstance$p(this.this$0).startScan();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceStartSync(String str, UserProfile userProfile) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(userProfile, "profile");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceStartSync(str, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SYNC, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void deviceStopScan() throws RemoteException {
        if (this.this$0.isBleSupported()) {
            ButtonService.access$getScanServiceInstance$p(this.this$0).stopScan();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceUnlink(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceUnlink(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.UNLINK, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long deviceUpdateActivityGoals(String str, int i, int i2, int i3, boolean z) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.deviceUpdateActivityGoals(str, i, i2, i3, z) : ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long disableHeartRateNotification(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceDisableHeartRate(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.DISABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long enableHeartRateNotification(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceEnableHeartRate(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.ENABLE_HEART_RATE_NOTIFICATION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int endLog(int i, String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.endLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public boolean forceSwitchDeviceWithoutErase(String str) throws RemoteException {
        ee7.b(str, "newActiveDeviceSerial");
        if (!this.this$0.isBleSupported(str)) {
            return false;
        }
        this.this$0.forceSwitchDeviceWithoutErase(str);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void forceUpdateDeviceData(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
        ee7.b(deviceAppResponse, "deviceAppResponse");
        ee7.b(str, "serial");
        this.this$0.sendDeviceAppResponse(deviceAppResponse, str, true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public List<String> getActiveSerial() throws RemoteException {
        List<String> allActiveButtonSerial = DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0);
        ee7.a((Object) allActiveButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allActiveButtonSerial;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public List<BLEMapping> getAutoMapping(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getAutoMapping(str);
        }
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int getCommunicatorModeBySerial(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getActiveCommunicatorBySerial(str);
        }
        return CommunicateMode.IDLE.getValue();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public MisfitDeviceProfile getDeviceProfile(String str) {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getDeviceProfile(str);
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int getGattState(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getGattState(str);
        }
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int getHIDState(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getHIDState(str);
        }
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int[] getListActiveCommunicator() throws RemoteException {
        return this.this$0.getActiveListCommunicator();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public List<MisfitDeviceProfile> getPairedDevice() throws RemoteException {
        if (this.this$0.isBleSupported()) {
            return this.this$0.getPairedDevice();
        }
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public List<String> getPairedSerial() throws RemoteException {
        List<String> allPairedButtonSerial = DevicePreferenceUtils.getAllPairedButtonSerial(this.this$0);
        ee7.a((Object) allPairedButtonSerial, "DevicePreferenceUtils.ge\u2026erial(this@ButtonService)");
        return allPairedButtonSerial;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public List<FitnessData> getSyncData(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.getSyncData(str);
        }
        FLogger.INSTANCE.getLocal().d(ButtonService.TAG, ".getSyncData(), device is not support");
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void init(String str, String str2, String str3, char c, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig) throws RemoteException {
        ee7.b(str, "sdkLogV2EndPoint");
        ee7.b(str2, "sdkLogV2AccessKey");
        ee7.b(str3, "sdkLogV2SecretKey");
        ee7.b(appLogInfo, "appLogInfo");
        ee7.b(activeDeviceInfo, "activeDeviceInfo");
        ee7.b(cloudLogConfig, "cloudLogConfig");
        ButtonService.fossilBrand = FossilDeviceSerialPatternUtil.BRAND.fromPrefix(c);
        ButtonService.Companion.setAppInfo(new AppInfo(appLogInfo.getAppVersion(), appLogInfo.getPlatformVersion()));
        FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
        FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
        FLogger.INSTANCE.updateCloudLogConfig(cloudLogConfig);
        if ((!mh7.a((CharSequence) str)) && (!mh7.a((CharSequence) str2)) && (!mh7.a((CharSequence) str3))) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, "init sdk end point " + str);
            zs1.b.a(new dh0(str, str2, str3));
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void interrupt(String str) {
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.INTERRUPT, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.interrupt(str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void interruptCurrentSession(String str) throws RemoteException {
        ee7.b(str, "serial");
        this.this$0.interruptCurrentSession(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public boolean isLinking(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) && this.this$0.isLinking(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public boolean isSyncing(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isSyncing(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public boolean isUpdatingFirmware(String str) throws RemoteException {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) && this.this$0.isUpdatingFirmware(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void logOut() throws RemoteException {
        this.this$0.logOut();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long notifyNotificationEvent(NotificationBaseObj notificationBaseObj, String str) throws RemoteException {
        ee7.b(notificationBaseObj, "newNotification");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.notifyNotificationEvent(notificationBaseObj, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.NOTIFY_NOTIFICATION_EVENT, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long notifyWatchAppFilesReady(String str, boolean z) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.notifyWatchAppFilesReady(str, z);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WATCH_APP_FILE_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long onPing(String str) {
        ee7.b(str, "serial");
        return this.this$0.isBleSupported(str) ? this.this$0.onPing(str) : System.currentTimeMillis();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) throws RemoteException {
        ee7.b(str, "serial");
        this.this$0.onSetWatchParamResponse(str, z, watchParamsFileMapping);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long pairDevice(String str, String str2, UserProfile userProfile) {
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        ee7.b(userProfile, "userProfile");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.pairDevice(str, str2, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long pairDeviceResponse(String str, PairingResponse pairingResponse) {
        ee7.b(str, "serial");
        ee7.b(pairingResponse, "response");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.pairDeviceResponse(str, pairingResponse);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.LINK, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long playVibration(String str, int i, int i2, boolean z) {
        ee7.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(ButtonService.TAG, "playVibration");
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long readCurrentWorkoutSession(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.readCurrentWorkoutSession(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void removeActiveSerial(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            DevicePreferenceUtils.removeActiveButtonSerial(this.this$0, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void removePairedSerial(String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            DevicePreferenceUtils.removePairedButtonSerial(this.this$0, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void resetDeviceSettingToDefault(String str) throws RemoteException {
        ee7.b(str, "serial");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long resetHandsToZeroDegree(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceResetHandsToZeroDegree(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.RESET_HAND, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long sendCurrentSecretKey(String str, String str2) {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveCurrentSecretKey(str, str2);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void sendCustomCommand(String str, CustomRequest customRequest) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(customRequest, Constants.COMMAND);
        System.currentTimeMillis();
        if (!this.this$0.isBleSupported(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = ButtonService.TAG;
            local.d(access$getTAG$cp, ".sendCustomRequest() with " + str + " is not supported");
            return;
        }
        this.this$0.sendCustomCommand(str, customRequest);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void sendDeviceAppResponse(DeviceAppResponse deviceAppResponse, String str) throws RemoteException {
        ee7.b(deviceAppResponse, "deviceAppResponse");
        ee7.b(str, "serial");
        ButtonService.sendDeviceAppResponse$default(this.this$0, deviceAppResponse, str, false, 4, null);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void sendMicroAppRemoteActivity(String str, DeviceAppResponse deviceAppResponse) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(deviceAppResponse, "deviceAppResponse");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.sendMicroAppRemoteActivity(str, deviceAppResponse);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void sendMusicAppResponse(MusicResponse musicResponse, String str) throws RemoteException {
        ee7.b(musicResponse, "musicResponse");
        ee7.b(str, "serial");
        this.this$0.sendMusicAppResponse(musicResponse, str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void sendPushSecretKeyResponse(String str, boolean z) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported()) {
            this.this$0.receivePushSecretKeyResponse(str, z);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long sendRandomKey(String str, String str2, int i) {
        ee7.b(str, "serial");
        ee7.b(str2, "randomKey");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveRandomKey(str, str2, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long sendServerSecretKey(String str, String str2, int i) {
        ee7.b(str, "serial");
        ee7.b(str2, "serverSecretKey");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.receiveServerSecretKey(str, str2, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.EXCHANGE_SECRET_KEY, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long sendingEncryptedDataSession(byte[] bArr, String str, boolean z) throws RemoteException {
        ee7.b(bArr, "encryptedData");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.sendingEncryptedDataSession(bArr, str, z);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SENDING_ENCRYPTED_DATA_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setActiveSerial(String str, String str2) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.setActiveSerial(str, str2);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
        ee7.b(backgroundConfig, "backgroundConfig");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setAutoBackgroundImageConfig(backgroundConfig, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoComplicationAppSettings(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_COMPLICATION_APPS, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setAutoComplicationApps(complicationAppMappingSettings, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoMapping(String str, List<? extends BLEMapping> list) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(list, "mappings");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_MAPPING, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setAutoMapping(str, list);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoUserBiometricData(UserProfile userProfile) throws RemoteException {
        ee7.b(userProfile, "userProfile");
        this.this$0.setAutoUserBiometricData(userProfile);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setAutoWatchAppSettings(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_AUTO_WATCH_APPS, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setAutoWatchApps(watchAppMappingSettings, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setBackgroundImageConfig(BackgroundConfig backgroundConfig, String str) throws RemoteException {
        ee7.b(backgroundConfig, "backgroundConfig");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setBackgroundImageConfig(backgroundConfig, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_BACKGROUND_IMAGE_CONFIG, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings, String str) throws RemoteException {
        ee7.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setCompilationApps(complicationAppMappingSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_COMPLICATION_APPS, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setFrontLightEnable(String str, boolean z) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setFrontLightEnable(str, z);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_FRONT_LIGHT_ENABLE, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setHeartRateMode(String str, int i) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        HeartRateMode fromValue = HeartRateMode.Companion.fromValue(i);
        if (this.this$0.isBleSupported(str) && fromValue != HeartRateMode.NONE) {
            return this.this$0.setHeartRateMode(str, fromValue);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_HEART_RATE_MODE, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setImplicitDeviceConfig(UserProfile userProfile, String str) throws RemoteException {
        ee7.b(userProfile, "userProfile");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setImplicitDeviceConfig(userProfile, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit, String str) throws RemoteException {
        ee7.b(userDisplayUnit, "userDisplayUnit");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setImplicitDisplayUnitSettings(userDisplayUnit, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setLocalizationData(LocalizationData localizationData, String str) throws RemoteException {
        ee7.b(localizationData, "localizationData");
        ee7.b(str, "serial");
        this.this$0.setLocalizationData(localizationData, str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setMinimumStepThresholdSession(long j, String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setMinimumStepThresholdSession(j, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_MINIMUM_STEP_THRESHOLD_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings, String str) throws RemoteException {
        ee7.b(appNotificationFilterSettings, "notificationFilterSettings");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setNotificationFilterSettings(appNotificationFilterSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_NOTIFICATION_FILTERS, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setPairedSerial(String str, String str2) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.setPairedDevice(str, str2);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_PRESET_APPS_DATA, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setReplyMessageMappingSetting(ReplyMessageMappingGroup replyMessageMappingGroup, String str) throws RemoteException {
        ee7.b(replyMessageMappingGroup, "messageGroup");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_REPLY_MESSAGE_MAPPING, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setSecretKey(String str, String str2) throws RemoteException {
        ee7.b(str, "serial");
        this.this$0.setSecretKey(str, str2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setWatchApps(WatchAppMappingSettings watchAppMappingSettings, String str) throws RemoteException {
        ee7.b(watchAppMappingSettings, "watchAppMappingSettings");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setWatchApps(watchAppMappingSettings, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WATCH_APPS, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void setWorkoutConfig(WorkoutConfigData workoutConfigData, String str) throws RemoteException {
        ee7.b(workoutConfigData, "workoutConfigData");
        ee7.b(str, "serial");
        if (!this.this$0.isBleSupported(str)) {
            this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WORKOUT_CONFIG_SESSION, ServiceActionResult.FAILED, null);
        } else {
            this.this$0.setWorkoutConfig(workoutConfigData, str);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting, String str) throws RemoteException {
        ee7.b(workoutDetectionSetting, "workoutDetectionSetting");
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.deviceSetWorkoutDetection(workoutDetectionSetting, str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WORKOUT_DETECTION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long setWorkoutGPSDataSession(String str, Location location) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(location, PlaceFields.LOCATION);
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.setWorkoutGPSDataSession(str, location);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SET_WORKOUT_GPS_DATA_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void simulateDisconnection(String str, int i, int i2, int i3, int i4) {
        ee7.b(str, "serial");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void simulatePusherEvent(String str, int i, int i2, int i3, int i4, int i5) throws RemoteException {
        ee7.b(str, "serial");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public int startLog(int i, String str) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.startLog(i, str);
        }
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long stopCurrentWorkoutSession(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.stopCurrentWorkoutSession(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.STOP_CURRENT_WORKOUT_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void stopLogService(int i) {
        this.this$0.stopLogService(i);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public boolean switchActiveDevice(String str, UserProfile userProfile) throws RemoteException {
        ee7.b(str, "newActiveDeviceSerial");
        ee7.b(userProfile, "userProfile");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.switchActiveDevice(str, userProfile);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.FAILED, null);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long switchDeviceResponse(String str, boolean z, int i) throws RemoteException {
        ee7.b(str, "serial");
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.switchDeviceResponse(str, z, i);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.SWITCH_DEVICE, ServiceActionResult.FAILED, null);
        return ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void updateActiveDeviceInfoLog(ActiveDeviceInfo activeDeviceInfo) throws RemoteException {
        ee7.b(activeDeviceInfo, "activeDeviceInfo");
        FLogger.INSTANCE.updateActiveDeviceInfo(activeDeviceInfo);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void updateAppInfo(String str) {
        ee7.b(str, "appInfoJson");
        this.this$0.updateAppInfo(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void updateAppLogInfo(AppLogInfo appLogInfo) throws RemoteException {
        ee7.b(appLogInfo, "appLogInfo");
        FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void updatePercentageGoalProgress(String str, boolean z, UserProfile userProfile) throws RemoteException {
        ee7.b(str, "serial");
        ee7.b(userProfile, "userProfile");
        if (this.this$0.isBleSupported(str)) {
            this.this$0.updatePercentageGoalProgress(str, z, userProfile);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public void updateUserId(String str) {
        ee7.b(str, ButtonService.USER_ID);
        this.this$0.updateUserId(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.IButtonConnectivity
    public long verifySecretKeySession(String str) throws RemoteException {
        ee7.b(str, "serial");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.this$0.isBleSupported(str)) {
            return this.this$0.verifySecreteKey(str);
        }
        this.this$0.broadcastServiceBlePhaseEvent(str, CommunicateMode.VERIFY_SECRET_KEY_SESSION, ServiceActionResult.FAILED, null);
        return currentTimeMillis;
    }
}
