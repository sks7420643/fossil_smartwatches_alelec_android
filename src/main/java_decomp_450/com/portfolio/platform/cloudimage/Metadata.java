package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Metadata {
    @DexIgnore
    public /* final */ String checksum;
    @DexIgnore
    public /* final */ String feature;
    @DexIgnore
    public /* final */ String platform;
    @DexIgnore
    public /* final */ String resolution;
    @DexIgnore
    public /* final */ String serialNumber;

    @DexIgnore
    public Metadata() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    public Metadata(String str, String str2, String str3, String str4, String str5) {
        this.checksum = str;
        this.feature = str2;
        this.platform = str3;
        this.resolution = str4;
        this.serialNumber = str5;
    }

    @DexIgnore
    public static /* synthetic */ Metadata copy$default(Metadata metadata, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metadata.checksum;
        }
        if ((i & 2) != 0) {
            str2 = metadata.feature;
        }
        if ((i & 4) != 0) {
            str3 = metadata.platform;
        }
        if ((i & 8) != 0) {
            str4 = metadata.resolution;
        }
        if ((i & 16) != 0) {
            str5 = metadata.serialNumber;
        }
        return metadata.copy(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public final String component1() {
        return this.checksum;
    }

    @DexIgnore
    public final String component2() {
        return this.feature;
    }

    @DexIgnore
    public final String component3() {
        return this.platform;
    }

    @DexIgnore
    public final String component4() {
        return this.resolution;
    }

    @DexIgnore
    public final String component5() {
        return this.serialNumber;
    }

    @DexIgnore
    public final Metadata copy(String str, String str2, String str3, String str4, String str5) {
        return new Metadata(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Metadata)) {
            return false;
        }
        Metadata metadata = (Metadata) obj;
        return ee7.a(this.checksum, metadata.checksum) && ee7.a(this.feature, metadata.feature) && ee7.a(this.platform, metadata.platform) && ee7.a(this.resolution, metadata.resolution) && ee7.a(this.serialNumber, metadata.serialNumber);
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final String getFeature() {
        return this.feature;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.checksum;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.feature;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.platform;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.resolution;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.serialNumber;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "Metadata(checksum=" + this.checksum + ", feature=" + this.feature + ", platform=" + this.platform + ", resolution=" + this.resolution + ", serialNumber=" + this.serialNumber + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Metadata(String str, String str2, String str3, String str4, String str5, int i, zd7 zd7) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5);
    }
}
