package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$2", f = "URLRequestTaskHelper.kt", l = {}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ AssetsDeviceResponse $assetsDeviceResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, AssetsDeviceResponse assetsDeviceResponse, fb7 fb7) {
        super(2, fb7);
        this.this$0 = uRLRequestTaskHelper;
        this.$assetsDeviceResponse = assetsDeviceResponse;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(this.this$0, this.$assetsDeviceResponse, fb7);
        uRLRequestTaskHelper$execute$Anon2.p$ = (yi7) obj;
        return uRLRequestTaskHelper$execute$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((URLRequestTaskHelper$execute$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            URLRequestTaskHelper.OnNextTaskListener listener$app_fossilRelease = this.this$0.getListener$app_fossilRelease();
            if (listener$app_fossilRelease == null) {
                return null;
            }
            String zipFilePath$app_fossilRelease = this.this$0.getZipFilePath$app_fossilRelease();
            String destinationUnzipPath$app_fossilRelease = this.this$0.getDestinationUnzipPath$app_fossilRelease();
            AssetsDeviceResponse assetsDeviceResponse = this.$assetsDeviceResponse;
            ee7.a((Object) assetsDeviceResponse, "assetsDeviceResponse");
            listener$app_fossilRelease.downloadFile(zipFilePath$app_fossilRelease, destinationUnzipPath$app_fossilRelease, assetsDeviceResponse);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
