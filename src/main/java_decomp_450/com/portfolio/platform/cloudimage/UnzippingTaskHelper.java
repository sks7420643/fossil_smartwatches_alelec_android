package com.portfolio.platform.cloudimage;

import android.os.AsyncTask;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.ShareConstants;
import com.fossil.ee7;
import com.fossil.qe7;
import com.fossil.se7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UnzippingTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + UnzippingTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public OnUnzipFinishListener listener;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class UnzipTask extends AsyncTask<Void, Void, Boolean> {
            @DexIgnore
            public /* final */ String destinationUnzipPath;
            @DexIgnore
            public /* final */ OnUnzipFinishListener listener;
            @DexIgnore
            public /* final */ String zipFilePath;

            @DexIgnore
            public UnzipTask(String str, String str2, OnUnzipFinishListener onUnzipFinishListener) {
                ee7.b(str, "zipFilePath");
                ee7.b(str2, "destinationUnzipPath");
                this.zipFilePath = str;
                this.destinationUnzipPath = str2;
                this.listener = onUnzipFinishListener;
            }

            @DexIgnore
            public Boolean doInBackground(Void... voidArr) {
                FileInputStream fileInputStream;
                ZipInputStream zipInputStream;
                ee7.b(voidArr, NativeProtocol.WEB_DIALOG_PARAMS);
                if (!new File(this.zipFilePath).exists()) {
                    return false;
                }
                byte[] bArr = new byte[2048];
                UnzippingTaskHelper.Companion.createDirectory$app_fossilRelease(this.destinationUnzipPath);
                fileInputStream = new FileInputStream(this.zipFilePath);
                zipInputStream = new ZipInputStream(fileInputStream);
                try {
                    se7 se7 = new se7();
                    while (true) {
                        T t = (T) zipInputStream.getNextEntry();
                        se7.element = t;
                        if (t != null) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String tAG$app_fossilRelease = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                            StringBuilder sb = new StringBuilder();
                            sb.append("Unzipping ");
                            T t2 = se7.element;
                            if (t2 != null) {
                                sb.append(t2.getName());
                                local.d(tAG$app_fossilRelease, sb.toString());
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(this.destinationUnzipPath);
                                sb2.append("/");
                                T t3 = se7.element;
                                if (t3 != null) {
                                    sb2.append(t3.getName());
                                    FileOutputStream fileOutputStream = new FileOutputStream(sb2.toString());
                                    qe7 qe7 = new qe7();
                                    while (true) {
                                        try {
                                            int read = zipInputStream.read(bArr);
                                            qe7.element = read;
                                            if (!(read > 0)) {
                                                break;
                                            }
                                            fileOutputStream.write(bArr, 0, qe7.element);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            zipInputStream.closeEntry();
                                        } catch (Throwable th) {
                                            zipInputStream.closeEntry();
                                            fileOutputStream.close();
                                            throw th;
                                        }
                                    }
                                    zipInputStream.closeEntry();
                                    fileOutputStream.close();
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String tAG$app_fossilRelease2 = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                            local2.d(tAG$app_fossilRelease2, "Unzipping completed, path = " + this.destinationUnzipPath);
                            zipInputStream.close();
                            fileInputStream.close();
                            return true;
                        }
                    }
                } catch (Exception e2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tAG$app_fossilRelease3 = UnzippingTaskHelper.Companion.getTAG$app_fossilRelease();
                    local3.e(tAG$app_fossilRelease3, "Unzipping failed, ex = " + e2);
                } catch (Throwable unused) {
                }
                zipInputStream.close();
                fileInputStream.close();
                return false;
            }

            @DexIgnore
            public void onPostExecute(Boolean bool) {
                super.onPostExecute((Object) bool);
                if (bool == null) {
                    ee7.a();
                    throw null;
                } else if (bool.booleanValue()) {
                    OnUnzipFinishListener onUnzipFinishListener = this.listener;
                    if (onUnzipFinishListener != null) {
                        onUnzipFinishListener.onUnzipSuccess(this.zipFilePath, this.destinationUnzipPath);
                    }
                } else {
                    OnUnzipFinishListener onUnzipFinishListener2 = this.listener;
                    if (onUnzipFinishListener2 != null) {
                        onUnzipFinishListener2.onUnzipFail(this.zipFilePath, this.destinationUnzipPath);
                    }
                }
            }
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final void createDirectory$app_fossilRelease(String str) {
            ee7.b(str, ShareConstants.DESTINATION);
            File file = new File(str);
            if (!file.isDirectory()) {
                file.mkdirs();
            }
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return UnzippingTaskHelper.TAG;
        }

        @DexIgnore
        public final UnzippingTaskHelper newInstance() {
            return new UnzippingTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface OnUnzipFinishListener {
        @DexIgnore
        void onUnzipFail(String str, String str2);

        @DexIgnore
        void onUnzipSuccess(String str, String str2);
    }

    @DexIgnore
    public static final UnzippingTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    public final void execute() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "execute() called with destinationUnzipPath = [" + this.destinationUnzipPath + ']');
        String str2 = this.zipFilePath;
        if (str2 != null) {
            String str3 = this.destinationUnzipPath;
            if (str3 != null) {
                new Companion.UnzipTask(str2, str3, this.listener).execute(new Void[0]);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void init(String str, String str2) {
        ee7.b(str, "zipFilePath");
        ee7.b(str2, "destinationUnzipPath");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
    }

    @DexIgnore
    public final void setOnUnzipFinishListener(OnUnzipFinishListener onUnzipFinishListener) {
        ee7.b(onUnzipFinishListener, "listener");
        this.listener = onUnzipFinishListener;
    }
}
