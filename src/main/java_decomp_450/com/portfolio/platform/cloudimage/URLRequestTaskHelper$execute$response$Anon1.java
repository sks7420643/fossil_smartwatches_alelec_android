package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.x87;
import com.fossil.zb7;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$response$1", f = "URLRequestTaskHelper.kt", l = {60}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$response$Anon1 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<ie4>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$response$Anon1(URLRequestTaskHelper uRLRequestTaskHelper, fb7 fb7) {
        super(1, fb7);
        this.this$0 = uRLRequestTaskHelper;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new URLRequestTaskHelper$execute$response$Anon1(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<ie4>>> fb7) {
        return ((URLRequestTaskHelper$execute$response$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 mApiService = this.this$0.getMApiService();
            String feature$app_fossilRelease = this.this$0.getFeature$app_fossilRelease();
            String str = null;
            if (feature$app_fossilRelease != null) {
                String access$getResolution$p = this.this$0.resolution;
                if (access$getResolution$p != null) {
                    String fastPairId$app_fossilRelease = this.this$0.getFastPairId$app_fossilRelease();
                    if (fastPairId$app_fossilRelease != null) {
                        if (fastPairId$app_fossilRelease != null) {
                            str = fastPairId$app_fossilRelease.toLowerCase();
                            ee7.a((Object) str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    this.label = 1;
                    obj = mApiService.getDeviceAssets(20, 0, "", feature$app_fossilRelease, access$getResolution$p, "ANDROID", str, this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
