package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetsDeviceResponse {
    @DexIgnore
    public /* final */ String category;
    @DexIgnore
    public /* final */ Data data;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Metadata metadata;
    @DexIgnore
    public /* final */ String sku;

    @DexIgnore
    public AssetsDeviceResponse() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    public AssetsDeviceResponse(String str, Data data2, Metadata metadata2, String str2, String str3) {
        this.category = str;
        this.data = data2;
        this.metadata = metadata2;
        this.sku = str2;
        this.id = str3;
    }

    @DexIgnore
    public static /* synthetic */ AssetsDeviceResponse copy$default(AssetsDeviceResponse assetsDeviceResponse, String str, Data data2, Metadata metadata2, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = assetsDeviceResponse.category;
        }
        if ((i & 2) != 0) {
            data2 = assetsDeviceResponse.data;
        }
        if ((i & 4) != 0) {
            metadata2 = assetsDeviceResponse.metadata;
        }
        if ((i & 8) != 0) {
            str2 = assetsDeviceResponse.sku;
        }
        if ((i & 16) != 0) {
            str3 = assetsDeviceResponse.id;
        }
        return assetsDeviceResponse.copy(str, data2, metadata2, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.category;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final Metadata component3() {
        return this.metadata;
    }

    @DexIgnore
    public final String component4() {
        return this.sku;
    }

    @DexIgnore
    public final String component5() {
        return this.id;
    }

    @DexIgnore
    public final AssetsDeviceResponse copy(String str, Data data2, Metadata metadata2, String str2, String str3) {
        return new AssetsDeviceResponse(str, data2, metadata2, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AssetsDeviceResponse)) {
            return false;
        }
        AssetsDeviceResponse assetsDeviceResponse = (AssetsDeviceResponse) obj;
        return ee7.a(this.category, assetsDeviceResponse.category) && ee7.a(this.data, assetsDeviceResponse.data) && ee7.a(this.metadata, assetsDeviceResponse.metadata) && ee7.a(this.sku, assetsDeviceResponse.sku) && ee7.a(this.id, assetsDeviceResponse.id);
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Metadata getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.category;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Data data2 = this.data;
        int hashCode2 = (hashCode + (data2 != null ? data2.hashCode() : 0)) * 31;
        Metadata metadata2 = this.metadata;
        int hashCode3 = (hashCode2 + (metadata2 != null ? metadata2.hashCode() : 0)) * 31;
        String str2 = this.sku;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.id;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "AssetsDeviceResponse(category=" + this.category + ", data=" + this.data + ", metadata=" + this.metadata + ", sku=" + this.sku + ", id=" + this.id + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AssetsDeviceResponse(String str, Data data2, Metadata metadata2, String str2, String str3, int i, zd7 zd7) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : data2, (i & 4) != 0 ? null : metadata2, (i & 8) != 0 ? null : str2, (i & 16) != 0 ? null : str3);
    }
}
