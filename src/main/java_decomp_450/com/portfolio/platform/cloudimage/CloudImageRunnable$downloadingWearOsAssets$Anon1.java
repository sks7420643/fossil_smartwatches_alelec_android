package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$downloadingWearOsAssets$1", f = "CloudImageRunnable.kt", l = {87}, m = "invokeSuspend")
public final class CloudImageRunnable$downloadingWearOsAssets$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public /* final */ /* synthetic */ String $feature;
    @DexIgnore
    public /* final */ /* synthetic */ String $zipFilePath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$downloadingWearOsAssets$Anon1(CloudImageRunnable cloudImageRunnable, String str, String str2, String str3, fb7 fb7) {
        super(2, fb7);
        this.this$0 = cloudImageRunnable;
        this.$zipFilePath = str;
        this.$destinationUnzipPath = str2;
        this.$feature = str3;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        CloudImageRunnable$downloadingWearOsAssets$Anon1 cloudImageRunnable$downloadingWearOsAssets$Anon1 = new CloudImageRunnable$downloadingWearOsAssets$Anon1(this.this$0, this.$zipFilePath, this.$destinationUnzipPath, this.$feature, fb7);
        cloudImageRunnable$downloadingWearOsAssets$Anon1.p$ = (yi7) obj;
        return cloudImageRunnable$downloadingWearOsAssets$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((CloudImageRunnable$downloadingWearOsAssets$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            URLRequestTaskHelper access$prepareURLRequestTask = this.this$0.prepareURLRequestTask();
            access$prepareURLRequestTask.init(this.$zipFilePath, this.$destinationUnzipPath, "", this.$feature, CloudImageRunnable.access$getResolution$p(this.this$0), CloudImageRunnable.access$getFastPairId$p(this.this$0));
            this.L$0 = yi7;
            this.L$1 = access$prepareURLRequestTask;
            this.label = 1;
            if (access$prepareURLRequestTask.execute(this) == a) {
                return a;
            }
        } else if (i == 1) {
            URLRequestTaskHelper uRLRequestTaskHelper = (URLRequestTaskHelper) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return i97.a;
    }
}
