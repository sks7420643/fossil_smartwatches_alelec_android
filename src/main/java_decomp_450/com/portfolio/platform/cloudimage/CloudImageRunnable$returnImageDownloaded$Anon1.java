package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.ge7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.te7;
import com.fossil.tk7;
import com.fossil.uf7;
import com.fossil.vd7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1", f = "CloudImageRunnable.kt", l = {151, 162}, m = "invokeSuspend")
public final class CloudImageRunnable$returnImageDownloaded$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzipPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class Anon1_Level3 extends ge7 {
            @DexIgnore
            public Anon1_Level3(CloudImageRunnable cloudImageRunnable) {
                super(cloudImageRunnable);
            }

            @DexIgnore
            @Override // com.fossil.ag7
            public Object get() {
                return CloudImageRunnable.access$getSerialNumber$p((CloudImageRunnable) ((vd7) this).receiver);
            }

            @DexIgnore
            @Override // com.fossil.sf7, com.fossil.vd7
            public String getName() {
                return "serialNumber";
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public uf7 getOwner() {
                return te7.a(CloudImageRunnable.class);
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public String getSignature() {
                return "getSerialNumber()Ljava/lang/String;";
            }

            @DexIgnore
            public void set(Object obj) {
                ((CloudImageRunnable) ((vd7) this).receiver).serialNumber = (String) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.this$0.this$0.serialNumber != null) {
                    CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                    if (access$getMListener$p == null) {
                        return null;
                    }
                    access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                    return i97.a;
                }
                CloudImageHelper.OnImageCallbackListener access$getMListener$p2 = this.this$0.this$0.mListener;
                if (access$getMListener$p2 == null) {
                    return null;
                }
                access$getMListener$p2.onImageCallback(CloudImageRunnable.access$getFastPairId$p(this.this$0.this$0), this.$filePath1);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnImageDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnImageDownloaded$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class Anon1_Level3 extends ge7 {
            @DexIgnore
            public Anon1_Level3(CloudImageRunnable cloudImageRunnable) {
                super(cloudImageRunnable);
            }

            @DexIgnore
            @Override // com.fossil.ag7
            public Object get() {
                return CloudImageRunnable.access$getSerialNumber$p((CloudImageRunnable) ((vd7) this).receiver);
            }

            @DexIgnore
            @Override // com.fossil.sf7, com.fossil.vd7
            public String getName() {
                return "serialNumber";
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public uf7 getOwner() {
                return te7.a(CloudImageRunnable.class);
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public String getSignature() {
                return "getSerialNumber()Ljava/lang/String;";
            }

            @DexIgnore
            public void set(Object obj) {
                ((CloudImageRunnable) ((vd7) this).receiver).serialNumber = (String) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageRunnable$returnImageDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, fb7);
            anon2_Level2.p$ = (yi7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon2_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.this$0.this$0.serialNumber != null) {
                    CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                    if (access$getMListener$p == null) {
                        return null;
                    }
                    access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                    return i97.a;
                }
                CloudImageHelper.OnImageCallbackListener access$getMListener$p2 = this.this$0.this$0.mListener;
                if (access$getMListener$p2 == null) {
                    return null;
                }
                access$getMListener$p2.onImageCallback(CloudImageRunnable.access$getFastPairId$p(this.this$0.this$0), this.$filePath2);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnImageDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, fb7 fb7) {
        super(2, fb7);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzipPath = str;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        CloudImageRunnable$returnImageDownloaded$Anon1 cloudImageRunnable$returnImageDownloaded$Anon1 = new CloudImageRunnable$returnImageDownloaded$Anon1(this.this$0, this.$destinationUnzipPath, fb7);
        cloudImageRunnable$returnImageDownloaded$Anon1.p$ = (yi7) obj;
        return cloudImageRunnable$returnImageDownloaded$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((CloudImageRunnable$returnImageDownloaded$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            String str = this.$destinationUnzipPath + '/' + this.this$0.type + ".webp";
            FLogger.INSTANCE.getLocal().d(CloudImageRunnable.TAG, "returnImageDownloaded(), filePath1=" + str);
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                tk7 c = qj7.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.label = 1;
                if (vh7.a(c, anon1_Level2, this) == a) {
                    return a;
                }
            } else {
                String str2 = this.$destinationUnzipPath + '/' + this.this$0.type + ".png";
                if (!AssetUtil.INSTANCE.checkFileExist(str2)) {
                    return i97.a;
                }
                tk7 c2 = qj7.c();
                Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 2;
                if (vh7.a(c2, anon2_Level2, this) == a) {
                    return a;
                }
                return i97.a;
            }
        } else if (i == 1) {
            String str3 = (String) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            String str4 = (String) this.L$2;
            String str5 = (String) this.L$1;
            yi7 yi73 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return i97.a;
    }
}
