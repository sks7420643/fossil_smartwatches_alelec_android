package com.portfolio.platform.cloudimage;

import android.os.AsyncTask;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDownloadTaskHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + FileDownloadTaskHelper.class.getSimpleName());
    @DexIgnore
    public String destinationUnzipPath;
    @DexIgnore
    public OnDownloadFinishListener listener;
    @DexIgnore
    public AssetsDeviceResponse response;
    @DexIgnore
    public String zipFilePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class FileDownloadTask extends AsyncTask<Void, Void, Boolean> {
            @DexIgnore
            public /* final */ String destinationUnzipPath;
            @DexIgnore
            public OnDownloadFinishListener listener;
            @DexIgnore
            public /* final */ AssetsDeviceResponse objectResponse;
            @DexIgnore
            public /* final */ String zipFilePath;

            @DexIgnore
            public FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener) {
                ee7.b(str, "zipFilePath");
                ee7.b(str2, "destinationUnzipPath");
                ee7.b(assetsDeviceResponse, "objectResponse");
                this.zipFilePath = str;
                this.destinationUnzipPath = str2;
                this.objectResponse = assetsDeviceResponse;
                this.listener = onDownloadFinishListener;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ FileDownloadTask(String str, String str2, AssetsDeviceResponse assetsDeviceResponse, OnDownloadFinishListener onDownloadFinishListener, int i, zd7 zd7) {
                this(str, str2, assetsDeviceResponse, (i & 8) != 0 ? null : onDownloadFinishListener);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
                if (r4 != null) goto L_0x006a;
             */
            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:62:0x0111  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Boolean doInBackground(java.lang.Void... r9) {
                /*
                    r8 = this;
                    java.lang.String r0 = "params"
                    com.fossil.ee7.b(r9, r0)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r9 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Data r9 = r9.getData()
                    r0 = 0
                    if (r9 == 0) goto L_0x0013
                    java.lang.String r9 = r9.getUrl()
                    goto L_0x0014
                L_0x0013:
                    r9 = r0
                L_0x0014:
                    boolean r9 = android.text.TextUtils.isEmpty(r9)
                    java.lang.String r1 = "], feature = ["
                    r2 = 0
                    if (r9 != 0) goto L_0x0115
                    java.io.FileOutputStream r9 = new java.io.FileOutputStream
                    java.lang.String r3 = r8.zipFilePath
                    r9.<init>(r3)
                    java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r4 = r8.objectResponse     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    com.portfolio.platform.cloudimage.Data r4 = r4.getData()     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    if (r4 == 0) goto L_0x0033
                    java.lang.String r4 = r4.getUrl()     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    goto L_0x0034
                L_0x0033:
                    r4 = r0
                L_0x0034:
                    r3.<init>(r4)     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    java.net.URLConnection r3 = r3.openConnection()     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    r3.connect()     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    r4.<init>(r3)     // Catch:{ Exception -> 0x0073, all -> 0x0070 }
                    r3 = 1024(0x400, float:1.435E-42)
                    byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x006e }
                L_0x004b:
                    int r5 = r4.read(r3)     // Catch:{ Exception -> 0x006e }
                    r6 = -1
                    r7 = 1
                    if (r5 == r6) goto L_0x0055
                    r6 = 1
                    goto L_0x0056
                L_0x0055:
                    r6 = 0
                L_0x0056:
                    if (r6 == 0) goto L_0x0063
                    boolean r6 = r8.isCancelled()     // Catch:{ Exception -> 0x006e }
                    if (r6 == 0) goto L_0x005f
                    goto L_0x0064
                L_0x005f:
                    r9.write(r3, r2, r5)     // Catch:{ Exception -> 0x006e }
                    goto L_0x004b
                L_0x0063:
                    r2 = 1
                L_0x0064:
                    r9.flush()
                    r9.close()
                L_0x006a:
                    r4.close()
                    goto L_0x0081
                L_0x006e:
                    r3 = move-exception
                    goto L_0x0075
                L_0x0070:
                    r1 = move-exception
                    goto L_0x0109
                L_0x0073:
                    r3 = move-exception
                    r4 = r0
                L_0x0075:
                    r3.printStackTrace()     // Catch:{ all -> 0x0107 }
                    r9.flush()
                    r9.close()
                    if (r4 == 0) goto L_0x0081
                    goto L_0x006a
                L_0x0081:
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r9 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r9 = r9.getMetadata()
                    if (r9 == 0) goto L_0x008e
                    java.lang.String r9 = r9.getChecksum()
                    goto L_0x008f
                L_0x008e:
                    r9 = r0
                L_0x008f:
                    boolean r9 = android.text.TextUtils.isEmpty(r9)
                    if (r9 != 0) goto L_0x00bd
                    com.portfolio.platform.cloudimage.ChecksumUtil r9 = com.portfolio.platform.cloudimage.ChecksumUtil.INSTANCE
                    java.lang.String r1 = r8.zipFilePath
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r3 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r3 = r3.getMetadata()
                    if (r3 == 0) goto L_0x00a5
                    java.lang.String r0 = r3.getChecksum()
                L_0x00a5:
                    boolean r9 = r9.verifyDownloadFile(r1, r0)
                    if (r9 != 0) goto L_0x0102
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    com.portfolio.platform.cloudimage.FileDownloadTaskHelper$Companion r0 = com.portfolio.platform.cloudimage.FileDownloadTaskHelper.Companion
                    java.lang.String r0 = r0.getTAG$app_fossilRelease()
                    java.lang.String r1 = "Inconsistent checksum, retry download?"
                    r9.e(r0, r1)
                    goto L_0x0102
                L_0x00bd:
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    com.portfolio.platform.cloudimage.FileDownloadTaskHelper$Companion r3 = com.portfolio.platform.cloudimage.FileDownloadTaskHelper.Companion
                    java.lang.String r3 = r3.getTAG$app_fossilRelease()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "Download assets completed for serialNumber = ["
                    r4.append(r5)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r5 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r5 = r5.getMetadata()
                    if (r5 == 0) goto L_0x00e0
                    java.lang.String r5 = r5.getSerialNumber()
                    goto L_0x00e1
                L_0x00e0:
                    r5 = r0
                L_0x00e1:
                    r4.append(r5)
                    r4.append(r1)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r1 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r1 = r1.getMetadata()
                    if (r1 == 0) goto L_0x00f3
                    java.lang.String r0 = r1.getFeature()
                L_0x00f3:
                    r4.append(r0)
                    java.lang.String r0 = "] with risk cause by empty checksum."
                    r4.append(r0)
                    java.lang.String r0 = r4.toString()
                    r9.e(r3, r0)
                L_0x0102:
                    java.lang.Boolean r9 = java.lang.Boolean.valueOf(r2)
                    return r9
                L_0x0107:
                    r1 = move-exception
                    r0 = r4
                L_0x0109:
                    r9.flush()
                    r9.close()
                    if (r0 == 0) goto L_0x0114
                    r0.close()
                L_0x0114:
                    throw r1
                L_0x0115:
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    com.portfolio.platform.cloudimage.FileDownloadTaskHelper$Companion r3 = com.portfolio.platform.cloudimage.FileDownloadTaskHelper.Companion
                    java.lang.String r3 = r3.getTAG$app_fossilRelease()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "Download file failed for serialNumber = ["
                    r4.append(r5)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r5 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r5 = r5.getMetadata()
                    if (r5 == 0) goto L_0x0138
                    java.lang.String r5 = r5.getSerialNumber()
                    goto L_0x0139
                L_0x0138:
                    r5 = r0
                L_0x0139:
                    r4.append(r5)
                    r4.append(r1)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r1 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Metadata r1 = r1.getMetadata()
                    if (r1 == 0) goto L_0x014c
                    java.lang.String r1 = r1.getFeature()
                    goto L_0x014d
                L_0x014c:
                    r1 = r0
                L_0x014d:
                    r4.append(r1)
                    java.lang.String r1 = "], downloadUrl = ["
                    r4.append(r1)
                    com.portfolio.platform.cloudimage.AssetsDeviceResponse r1 = r8.objectResponse
                    com.portfolio.platform.cloudimage.Data r1 = r1.getData()
                    if (r1 == 0) goto L_0x0161
                    java.lang.String r0 = r1.getUrl()
                L_0x0161:
                    r4.append(r0)
                    java.lang.String r0 = "]"
                    r4.append(r0)
                    java.lang.String r0 = r4.toString()
                    r9.e(r3, r0)
                    java.lang.Boolean r9 = java.lang.Boolean.valueOf(r2)
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.FileDownloadTaskHelper.Companion.FileDownloadTask.doInBackground(java.lang.Void[]):java.lang.Boolean");
            }

            @DexIgnore
            public void onPostExecute(Boolean bool) {
                super.onPostExecute((Object) bool);
                if (bool == null) {
                    ee7.a();
                    throw null;
                } else if (bool.booleanValue()) {
                    OnDownloadFinishListener onDownloadFinishListener = this.listener;
                    if (onDownloadFinishListener != null) {
                        onDownloadFinishListener.onDownloadSuccess(this.zipFilePath, this.destinationUnzipPath);
                    }
                } else {
                    OnDownloadFinishListener onDownloadFinishListener2 = this.listener;
                    if (onDownloadFinishListener2 != null) {
                        onDownloadFinishListener2.onDownloadFail(this.zipFilePath, this.destinationUnzipPath, this.objectResponse);
                    }
                }
            }
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return FileDownloadTaskHelper.TAG;
        }

        @DexIgnore
        public final FileDownloadTaskHelper newInstance() {
            return new FileDownloadTaskHelper();
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface OnDownloadFinishListener {
        @DexIgnore
        void onDownloadFail(String str, String str2, AssetsDeviceResponse assetsDeviceResponse);

        @DexIgnore
        void onDownloadSuccess(String str, String str2);
    }

    @DexIgnore
    public static final FileDownloadTaskHelper newInstance() {
        return Companion.newInstance();
    }

    @DexIgnore
    public final void execute() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("execute() called with serialNumber = [");
        AssetsDeviceResponse assetsDeviceResponse = this.response;
        if (assetsDeviceResponse != null) {
            Metadata metadata = assetsDeviceResponse.getMetadata();
            sb.append(metadata != null ? metadata.getSerialNumber() : null);
            sb.append("], feature = [");
            AssetsDeviceResponse assetsDeviceResponse2 = this.response;
            if (assetsDeviceResponse2 != null) {
                Metadata metadata2 = assetsDeviceResponse2.getMetadata();
                sb.append(metadata2 != null ? metadata2.getFeature() : null);
                sb.append("]");
                local.d(str, sb.toString());
                String str2 = this.zipFilePath;
                if (str2 != null) {
                    String str3 = this.destinationUnzipPath;
                    if (str3 != null) {
                        AssetsDeviceResponse assetsDeviceResponse3 = this.response;
                        if (assetsDeviceResponse3 != null) {
                            new Companion.FileDownloadTask(str2, str3, assetsDeviceResponse3, this.listener).execute(new Void[0]);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void init(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        ee7.b(str, "zipFilePath");
        ee7.b(str2, "destinationUnzipPath");
        ee7.b(assetsDeviceResponse, "response");
        this.zipFilePath = str;
        this.destinationUnzipPath = str2;
        this.response = assetsDeviceResponse;
    }

    @DexIgnore
    public final void setOnDownloadFinishListener(OnDownloadFinishListener onDownloadFinishListener) {
        ee7.b(onDownloadFinishListener, "listener");
        this.listener = onDownloadFinishListener;
    }
}
