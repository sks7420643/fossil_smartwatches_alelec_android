package com.portfolio.platform.cloudimage;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.MembersInjector;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class URLRequestTaskHelper_MembersInjector implements MembersInjector<URLRequestTaskHelper> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public URLRequestTaskHelper_MembersInjector(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static MembersInjector<URLRequestTaskHelper> create(Provider<ApiServiceV2> provider) {
        return new URLRequestTaskHelper_MembersInjector(provider);
    }

    @DexIgnore
    public static void injectMApiService(URLRequestTaskHelper uRLRequestTaskHelper, ApiServiceV2 apiServiceV2) {
        uRLRequestTaskHelper.mApiService = apiServiceV2;
    }

    @DexIgnore
    public void injectMembers(URLRequestTaskHelper uRLRequestTaskHelper) {
        injectMApiService(uRLRequestTaskHelper, this.mApiServiceProvider.get());
    }
}
