package com.portfolio.platform.cloudimage;

import com.facebook.internal.Utility;
import com.fossil.ee7;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import io.flutter.plugin.common.StandardMessageCodec;
import java.io.FileInputStream;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChecksumUtil {
    @DexIgnore
    public static /* final */ ChecksumUtil INSTANCE; // = new ChecksumUtil();
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + ChecksumUtil.class.getSimpleName());

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            String num = Integer.toString(((byte) (bArr[i] & ((byte) 255))) + StandardMessageCodec.NULL, 16);
            ee7.a((Object) num, "Integer.toString((input[\u2026ff.toByte()) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                i++;
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "ret.toString()");
        if (sb2 != null) {
            String lowerCase = sb2.toLowerCase();
            ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean verifyDownloadFile(String str, String str2) {
        ee7.b(str, "filePath");
        if (str2 == null) {
            return true;
        }
        MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
        FileInputStream fileInputStream = new FileInputStream(str);
        byte[] bArr = new byte[2014];
        while (true) {
            try {
                int read = fileInputStream.read(bArr);
                if (!(read != -1)) {
                    byte[] digest = instance.digest();
                    ee7.a((Object) digest, "md5");
                    String bytesToString = bytesToString(digest);
                    String lowerCase = str2.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    boolean a = ee7.a((Object) lowerCase, (Object) bytesToString);
                    fileInputStream.close();
                    return a;
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.e(str3, "VerifyDownloadFileFailed - ex=" + e);
                fileInputStream.close();
                return false;
            } catch (Throwable unused) {
                fileInputStream.close();
                return false;
            }
        }
    }
}
