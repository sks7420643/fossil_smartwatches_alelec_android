package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetUtil {
    @DexIgnore
    public static /* final */ AssetUtil INSTANCE; // = new AssetUtil();
    @DexIgnore
    public static /* final */ String TAG; // = "AssetUtil";

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0200  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0265  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x02c5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAssetExist(java.io.File r22, java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.String r26, java.lang.String r27, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r28, com.fossil.fb7<? super java.lang.Boolean> r29) {
        /*
            r21 = this;
            r0 = r21
            r1 = r24
            r2 = r25
            r3 = r26
            r4 = r27
            r5 = r29
            boolean r6 = r5 instanceof com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon1
            if (r6 == 0) goto L_0x001f
            r6 = r5
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon1 r6 = (com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon1) r6
            int r7 = r6.label
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            r9 = r7 & r8
            if (r9 == 0) goto L_0x001f
            int r7 = r7 - r8
            r6.label = r7
            goto L_0x0024
        L_0x001f:
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon1 r6 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon1
            r6.<init>(r0, r5)
        L_0x0024:
            java.lang.Object r5 = r6.result
            java.lang.Object r7 = com.fossil.nb7.a()
            int r8 = r6.label
            java.lang.String r9 = " serialNumber="
            r10 = 4
            r11 = 3
            r12 = 2
            java.lang.String r13 = "AssetUtil"
            r15 = 1
            if (r8 == 0) goto L_0x0126
            if (r8 == r15) goto L_0x00f7
            if (r8 == r12) goto L_0x00c4
            if (r8 == r11) goto L_0x007f
            if (r8 != r10) goto L_0x0077
            boolean r1 = r6.Z$1
            java.lang.Object r1 = r6.L$10
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r6.Z$0
            java.lang.Object r2 = r6.L$9
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$8
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$7
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r2 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r2
            java.lang.Object r2 = r6.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$5
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$4
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$3
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$2
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r6.L$1
            java.io.File r3 = (java.io.File) r3
            java.lang.Object r3 = r6.L$0
            com.portfolio.platform.cloudimage.AssetUtil r3 = (com.portfolio.platform.cloudimage.AssetUtil) r3
            com.fossil.t87.a(r5)
            r18 = r9
            r17 = r13
            goto L_0x029b
        L_0x0077:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x007f:
            java.lang.Object r1 = r6.L$10
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r6.Z$0
            java.lang.Object r3 = r6.L$9
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r4 = r6.L$8
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r6.L$7
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r8 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r8
            java.lang.Object r11 = r6.L$6
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r6.L$5
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r14 = r6.L$4
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r10 = r6.L$3
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r15 = r6.L$2
            java.lang.String r15 = (java.lang.String) r15
            r22 = r1
            java.lang.Object r1 = r6.L$1
            java.io.File r1 = (java.io.File) r1
            r23 = r1
            java.lang.Object r1 = r6.L$0
            com.portfolio.platform.cloudimage.AssetUtil r1 = (com.portfolio.platform.cloudimage.AssetUtil) r1
            com.fossil.t87.a(r5)
            r0 = r1
            r18 = r9
            r17 = r13
            r1 = r22
            r9 = r7
            r13 = r12
            r12 = r23
            r7 = r5
            r5 = r2
            r2 = r15
            goto L_0x025d
        L_0x00c4:
            boolean r1 = r6.Z$0
            java.lang.Object r1 = r6.L$9
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r6.L$8
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$7
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r2 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r2
            java.lang.Object r2 = r6.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$5
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$4
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$3
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r6.L$2
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r6.L$1
            java.io.File r3 = (java.io.File) r3
            java.lang.Object r3 = r6.L$0
            com.portfolio.platform.cloudimage.AssetUtil r3 = (com.portfolio.platform.cloudimage.AssetUtil) r3
            com.fossil.t87.a(r5)
            r18 = r9
            r17 = r13
            goto L_0x01d6
        L_0x00f7:
            java.lang.Object r1 = r6.L$9
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r6.L$8
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r6.L$7
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r3 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r3
            java.lang.Object r4 = r6.L$6
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r6.L$5
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r10 = r6.L$4
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r15 = r6.L$3
            java.lang.String r15 = (java.lang.String) r15
            java.lang.Object r11 = r6.L$2
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r6.L$1
            java.io.File r12 = (java.io.File) r12
            java.lang.Object r14 = r6.L$0
            com.portfolio.platform.cloudimage.AssetUtil r14 = (com.portfolio.platform.cloudimage.AssetUtil) r14
            com.fossil.t87.a(r5)
            r0 = r1
            r1 = r14
            goto L_0x019e
        L_0x0126:
            com.fossil.t87.a(r5)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = r22.getAbsolutePath()
            r5.append(r8)
            r8 = 47
            r5.append(r8)
            r5.append(r1)
            r10 = 45
            r5.append(r10)
            r5.append(r2)
            r5.append(r10)
            r5.append(r3)
            java.lang.String r5 = r5.toString()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r5)
            r10.append(r8)
            r10.append(r4)
            java.lang.String r8 = ".webp"
            r10.append(r8)
            java.lang.String r8 = r10.toString()
            com.fossil.ti7 r10 = com.fossil.qj7.b()
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 r11 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$Anon1
            r12 = 0
            r11.<init>(r8, r12)
            r6.L$0 = r0
            r12 = r22
            r6.L$1 = r12
            r14 = r23
            r6.L$2 = r14
            r6.L$3 = r1
            r6.L$4 = r2
            r6.L$5 = r3
            r6.L$6 = r4
            r15 = r28
            r6.L$7 = r15
            r6.L$8 = r5
            r6.L$9 = r8
            r0 = 1
            r6.label = r0
            java.lang.Object r0 = com.fossil.vh7.a(r10, r11, r6)
            if (r0 != r7) goto L_0x0194
            return r7
        L_0x0194:
            r10 = r2
            r2 = r5
            r11 = r14
            r5 = r0
            r0 = r8
            r8 = r3
            r3 = r15
            r15 = r1
            r1 = r21
        L_0x019e:
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x0200
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            r17 = r13
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon2 r13 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon2
            r18 = r9
            r9 = 0
            r13.<init>(r3, r11, r0, r9)
            r6.L$0 = r1
            r6.L$1 = r12
            r6.L$2 = r11
            r6.L$3 = r15
            r6.L$4 = r10
            r6.L$5 = r8
            r6.L$6 = r4
            r6.L$7 = r3
            r6.L$8 = r2
            r6.L$9 = r0
            r6.Z$0 = r5
            r1 = 2
            r6.label = r1
            java.lang.Object r1 = com.fossil.vh7.a(r14, r13, r6)
            if (r1 != r7) goto L_0x01d4
            return r7
        L_0x01d4:
            r1 = r0
            r2 = r11
        L_0x01d6:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "filePath1="
            r3.append(r4)
            r3.append(r1)
            r9 = r18
            r3.append(r9)
            r3.append(r2)
            java.lang.String r1 = r3.toString()
            r13 = r17
            r0.d(r13, r1)
            r0 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        L_0x0200:
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r2)
            r17 = r13
            r13 = 47
            r14.append(r13)
            r14.append(r4)
            java.lang.String r13 = ".png"
            r14.append(r13)
            java.lang.String r13 = r14.toString()
            com.fossil.ti7 r14 = com.fossil.qj7.b()
            r18 = r9
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath2Exist$Anon1 r9 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath2Exist$Anon1
            r16 = r7
            r7 = 0
            r9.<init>(r13, r7)
            r6.L$0 = r1
            r6.L$1 = r12
            r6.L$2 = r11
            r6.L$3 = r15
            r6.L$4 = r10
            r6.L$5 = r8
            r6.L$6 = r4
            r6.L$7 = r3
            r6.L$8 = r2
            r6.L$9 = r0
            r6.Z$0 = r5
            r6.L$10 = r13
            r7 = 3
            r6.label = r7
            java.lang.Object r7 = com.fossil.vh7.a(r14, r9, r6)
            r9 = r16
            if (r7 != r9) goto L_0x024d
            return r9
        L_0x024d:
            r14 = r10
            r10 = r15
            r19 = r3
            r3 = r0
            r0 = r1
            r1 = r13
            r13 = r8
            r8 = r19
            r20 = r4
            r4 = r2
            r2 = r11
            r11 = r20
        L_0x025d:
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x02c5
            com.fossil.tk7 r15 = com.fossil.qj7.c()
            r16 = r9
            com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon3 r9 = new com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$Anon3
            r22 = r15
            r15 = 0
            r9.<init>(r8, r2, r1, r15)
            r6.L$0 = r0
            r6.L$1 = r12
            r6.L$2 = r2
            r6.L$3 = r10
            r6.L$4 = r14
            r6.L$5 = r13
            r6.L$6 = r11
            r6.L$7 = r8
            r6.L$8 = r4
            r6.L$9 = r3
            r6.Z$0 = r5
            r6.L$10 = r1
            r6.Z$1 = r7
            r0 = 4
            r6.label = r0
            r0 = r22
            java.lang.Object r0 = com.fossil.vh7.a(r0, r9, r6)
            r3 = r16
            if (r0 != r3) goto L_0x029b
            return r3
        L_0x029b:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "filePath2="
            r3.append(r4)
            r3.append(r1)
            r1 = r18
            r3.append(r1)
            r3.append(r2)
            java.lang.String r1 = r3.toString()
            r3 = r17
            r0.d(r3, r1)
            r0 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        L_0x02c5:
            r3 = r17
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "file is not exist serialNumber = "
            r1.append(r4)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r3, r1)
            r0 = 0
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkAssetExist(java.io.File, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final boolean checkFileExist(String str) {
        ee7.b(str, "filePath");
        return new File(str).exists();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkWearOSAssetExist(java.io.File r20, java.lang.String r21, java.lang.String r22, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r23, com.fossil.fb7<? super java.lang.Boolean> r24) {
        /*
            r19 = this;
            r0 = r19
            r1 = r21
            r2 = r22
            r3 = r24
            boolean r4 = r3 instanceof com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon1
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon1 r4 = (com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon1) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon1 r4 = new com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon1
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            java.lang.String r7 = " fastPairId="
            r8 = 4
            r9 = 3
            r10 = 2
            java.lang.String r11 = "AssetUtil"
            r12 = 47
            r14 = 1
            if (r6 == 0) goto L_0x00fb
            if (r6 == r14) goto L_0x00cd
            if (r6 == r10) goto L_0x00a6
            if (r6 == r9) goto L_0x0075
            if (r6 != r8) goto L_0x006d
            boolean r1 = r4.Z$1
            java.lang.Object r1 = r4.L$8
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r4.Z$0
            java.lang.Object r2 = r4.L$7
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$5
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$4
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r2 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r2
            java.lang.Object r2 = r4.L$3
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r5 = r4.L$1
            java.io.File r5 = (java.io.File) r5
            java.lang.Object r4 = r4.L$0
            com.portfolio.platform.cloudimage.AssetUtil r4 = (com.portfolio.platform.cloudimage.AssetUtil) r4
            com.fossil.t87.a(r3)
            r16 = r7
            r17 = r11
            goto L_0x0239
        L_0x006d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0075:
            java.lang.Object r1 = r4.L$8
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r4.Z$0
            java.lang.Object r6 = r4.L$7
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r9 = r4.L$6
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = r4.L$5
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r12 = r4.L$4
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r12 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r12
            java.lang.Object r15 = r4.L$3
            java.lang.String r15 = (java.lang.String) r15
            java.lang.Object r8 = r4.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r14 = r4.L$1
            java.io.File r14 = (java.io.File) r14
            java.lang.Object r13 = r4.L$0
            com.portfolio.platform.cloudimage.AssetUtil r13 = (com.portfolio.platform.cloudimage.AssetUtil) r13
            com.fossil.t87.a(r3)
            r0 = r10
            r17 = r11
            r10 = r3
            r3 = r2
            r2 = r8
            goto L_0x0205
        L_0x00a6:
            boolean r1 = r4.Z$0
            java.lang.Object r1 = r4.L$7
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$5
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$4
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r2 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r2
            java.lang.Object r2 = r4.L$3
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r5 = r4.L$1
            java.io.File r5 = (java.io.File) r5
            java.lang.Object r4 = r4.L$0
            com.portfolio.platform.cloudimage.AssetUtil r4 = (com.portfolio.platform.cloudimage.AssetUtil) r4
            com.fossil.t87.a(r3)
            goto L_0x0193
        L_0x00cd:
            java.lang.Object r1 = r4.L$7
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$6
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r6 = r4.L$5
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r8 = r4.L$4
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r8 = (com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener) r8
            java.lang.Object r13 = r4.L$3
            java.lang.String r13 = (java.lang.String) r13
            java.lang.Object r14 = r4.L$2
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r15 = r4.L$1
            java.io.File r15 = (java.io.File) r15
            java.lang.Object r9 = r4.L$0
            com.portfolio.platform.cloudimage.AssetUtil r9 = (com.portfolio.platform.cloudimage.AssetUtil) r9
            com.fossil.t87.a(r3)
            r18 = r6
            r6 = r1
            r1 = r14
            r14 = r15
            r15 = r8
            r8 = r9
            r9 = r2
            r2 = r18
            goto L_0x0163
        L_0x00fb:
            com.fossil.t87.a(r3)
            java.lang.String r3 = "WearOS"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = r20.getAbsolutePath()
            r6.append(r8)
            r6.append(r12)
            r6.append(r1)
            r8 = 45
            r6.append(r8)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r6)
            r8.append(r12)
            r8.append(r3)
            java.lang.String r9 = ".webp"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            com.fossil.ti7 r9 = com.fossil.qj7.b()
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1 r13 = new com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$isFilePath1Exist$Anon1
            r14 = 0
            r13.<init>(r8, r14)
            r4.L$0 = r0
            r14 = r20
            r4.L$1 = r14
            r4.L$2 = r1
            r4.L$3 = r2
            r15 = r23
            r4.L$4 = r15
            r4.L$5 = r3
            r4.L$6 = r6
            r4.L$7 = r8
            r12 = 1
            r4.label = r12
            java.lang.Object r9 = com.fossil.vh7.a(r9, r13, r4)
            if (r9 != r5) goto L_0x015d
            return r5
        L_0x015d:
            r13 = r2
            r2 = r3
            r3 = r9
            r9 = r6
            r6 = r8
            r8 = r0
        L_0x0163:
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x01b9
            com.fossil.tk7 r12 = com.fossil.qj7.c()
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon2 r10 = new com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon2
            r0 = 0
            r10.<init>(r15, r1, r6, r0)
            r4.L$0 = r8
            r4.L$1 = r14
            r4.L$2 = r1
            r4.L$3 = r13
            r4.L$4 = r15
            r4.L$5 = r2
            r4.L$6 = r9
            r4.L$7 = r6
            r4.Z$0 = r3
            r0 = 2
            r4.label = r0
            java.lang.Object r0 = com.fossil.vh7.a(r12, r10, r4)
            if (r0 != r5) goto L_0x0191
            return r5
        L_0x0191:
            r2 = r1
            r1 = r6
        L_0x0193:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "filePath1="
            r3.append(r4)
            r3.append(r1)
            r3.append(r7)
            r3.append(r2)
            java.lang.String r1 = r3.toString()
            r0.d(r11, r1)
            r0 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        L_0x01b9:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r9)
            r10 = 47
            r0.append(r10)
            r0.append(r2)
            java.lang.String r10 = ".png"
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.fossil.ti7 r10 = com.fossil.qj7.b()
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1 r12 = new com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$isFilePath2Exist$Anon1
            r17 = r11
            r11 = 0
            r12.<init>(r0, r11)
            r4.L$0 = r8
            r4.L$1 = r14
            r4.L$2 = r1
            r4.L$3 = r13
            r4.L$4 = r15
            r4.L$5 = r2
            r4.L$6 = r9
            r4.L$7 = r6
            r4.Z$0 = r3
            r4.L$8 = r0
            r11 = 3
            r4.label = r11
            java.lang.Object r10 = com.fossil.vh7.a(r10, r12, r4)
            if (r10 != r5) goto L_0x01fc
            return r5
        L_0x01fc:
            r12 = r15
            r15 = r13
            r13 = r8
            r18 = r1
            r1 = r0
            r0 = r2
            r2 = r18
        L_0x0205:
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 == 0) goto L_0x0263
            com.fossil.tk7 r10 = com.fossil.qj7.c()
            com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon3 r11 = new com.portfolio.platform.cloudimage.AssetUtil$checkWearOSAssetExist$Anon3
            r16 = r7
            r7 = 0
            r11.<init>(r12, r2, r1, r7)
            r4.L$0 = r13
            r4.L$1 = r14
            r4.L$2 = r2
            r4.L$3 = r15
            r4.L$4 = r12
            r4.L$5 = r0
            r4.L$6 = r9
            r4.L$7 = r6
            r4.Z$0 = r3
            r4.L$8 = r1
            r4.Z$1 = r8
            r0 = 4
            r4.label = r0
            java.lang.Object r0 = com.fossil.vh7.a(r10, r11, r4)
            if (r0 != r5) goto L_0x0239
            return r5
        L_0x0239:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "filePath2="
            r3.append(r4)
            r3.append(r1)
            r1 = r16
            r3.append(r1)
            r3.append(r2)
            java.lang.String r1 = r3.toString()
            r3 = r17
            r0.d(r3, r1)
            r0 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        L_0x0263:
            r3 = r17
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "file is not exist fastPairId = "
            r1.append(r4)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r3, r1)
            r0 = 0
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkWearOSAssetExist(java.io.File, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.fossil.fb7):java.lang.Object");
    }
}
