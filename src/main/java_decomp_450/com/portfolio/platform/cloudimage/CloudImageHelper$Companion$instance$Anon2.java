package com.portfolio.platform.cloudimage;

import com.fossil.fe7;
import com.fossil.vc7;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper$Companion$instance$Anon2 extends fe7 implements vc7<CloudImageHelper> {
    @DexIgnore
    public static /* final */ CloudImageHelper$Companion$instance$Anon2 INSTANCE; // = new CloudImageHelper$Companion$instance$Anon2();

    @DexIgnore
    public CloudImageHelper$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public final CloudImageHelper invoke() {
        return CloudImageHelper.Holder.INSTANCE.getINSTANCE();
    }
}
