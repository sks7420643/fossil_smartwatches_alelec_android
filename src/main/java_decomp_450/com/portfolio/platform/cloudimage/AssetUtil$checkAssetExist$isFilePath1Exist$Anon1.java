package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(String str, fb7 fb7) {
        super(2, fb7);
        this.$filePath1 = str;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 assetUtil$checkAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(this.$filePath1, fb7);
        assetUtil$checkAssetExist$isFilePath1Exist$Anon1.p$ = (yi7) obj;
        return assetUtil$checkAssetExist$isFilePath1Exist$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
        return ((AssetUtil$checkAssetExist$isFilePath1Exist$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            return pb7.a(AssetUtil.INSTANCE.checkFileExist(this.$filePath1));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
