package com.portfolio.platform.cloudimage;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.tk7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1", f = "CloudImageRunnable.kt", l = {183, 186, 189}, m = "invokeSuspend")
public final class CloudImageRunnable$returnLogoDownloaded$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzippedPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, fb7);
            anon2_Level2.p$ = (yi7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon2_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$3", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, fb7 fb7) {
            super(2, fb7);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, fb7);
            anon3_Level2.p$ = (yi7) obj;
            return anon3_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon3_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), "");
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnLogoDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, fb7 fb7) {
        super(2, fb7);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzippedPath = str;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1 = new CloudImageRunnable$returnLogoDownloaded$Anon1(this.this$0, this.$destinationUnzippedPath, fb7);
        cloudImageRunnable$returnLogoDownloaded$Anon1.p$ = (yi7) obj;
        return cloudImageRunnable$returnLogoDownloaded$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((CloudImageRunnable$returnLogoDownloaded$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            String str = this.$destinationUnzippedPath + '/' + this.this$0.type + ".webp";
            String str2 = this.$destinationUnzippedPath + '/' + this.this$0.type + ".png";
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                tk7 c = qj7.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 1;
                if (vh7.a(c, anon1_Level2, this) == a) {
                    return a;
                }
            } else if (AssetUtil.INSTANCE.checkFileExist(str2)) {
                tk7 c2 = qj7.c();
                Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 2;
                if (vh7.a(c2, anon2_Level2, this) == a) {
                    return a;
                }
            } else {
                tk7 c3 = qj7.c();
                Anon3_Level2 anon3_Level2 = new Anon3_Level2(this, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 3;
                if (vh7.a(c3, anon3_Level2, this) == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2 || i == 3) {
            String str3 = (String) this.L$2;
            String str4 = (String) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return i97.a;
    }
}
