package com.portfolio.platform.cloudimage;

import com.fossil.pj4;
import com.portfolio.platform.PortfolioApp;
import dagger.MembersInjector;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper_MembersInjector implements MembersInjector<CloudImageHelper> {
    @DexIgnore
    public /* final */ Provider<pj4> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mAppProvider;

    @DexIgnore
    public CloudImageHelper_MembersInjector(Provider<pj4> provider, Provider<PortfolioApp> provider2) {
        this.mAppExecutorsProvider = provider;
        this.mAppProvider = provider2;
    }

    @DexIgnore
    public static MembersInjector<CloudImageHelper> create(Provider<pj4> provider, Provider<PortfolioApp> provider2) {
        return new CloudImageHelper_MembersInjector(provider, provider2);
    }

    @DexIgnore
    public static void injectMApp(CloudImageHelper cloudImageHelper, PortfolioApp portfolioApp) {
        cloudImageHelper.mApp = portfolioApp;
    }

    @DexIgnore
    public static void injectMAppExecutors(CloudImageHelper cloudImageHelper, pj4 pj4) {
        cloudImageHelper.mAppExecutors = pj4;
    }

    @DexIgnore
    public void injectMembers(CloudImageHelper cloudImageHelper) {
        injectMAppExecutors(cloudImageHelper, this.mAppExecutorsProvider.get());
        injectMApp(cloudImageHelper, this.mAppProvider.get());
    }
}
