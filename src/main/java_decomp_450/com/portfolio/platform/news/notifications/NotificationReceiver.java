package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ad5;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationReceiver extends BroadcastReceiver {
    @DexIgnore
    public ch5 a;
    @DexIgnore
    public ad5 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public /* final */ yi7 d; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.news.notifications.NotificationReceiver$onReceive$1", f = "NotificationReceiver.kt", l = {52}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NotificationReceiver notificationReceiver, Intent intent, fb7 fb7) {
            super(2, fb7);
            this.this$0 = notificationReceiver;
            this.$intent = intent;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$intent, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Integer a;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                UserRepository b = this.this$0.b();
                this.L$0 = yi7;
                this.label = 1;
                obj = b.getCurrentUser(this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) obj) == null) {
                FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
                return i97.a;
            }
            Intent intent = this.$intent;
            int intValue = (intent == null || (a = pb7.a(intent.getIntExtra("ACTION_EVENT", 0))) == null) ? 0 : a.intValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("NotificationReceiver", "onReceive - actionEvent=" + intValue);
            if (intValue == 1) {
                PortfolioApp.g0.c().a(this.this$0.a(), false, 10);
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final ad5 a() {
        ad5 ad5 = this.b;
        if (ad5 != null) {
            return ad5;
        }
        ee7.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final UserRepository b() {
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        ik7 unused = xh7.b(this.d, null, null, new b(this, intent, null), 3, null);
    }
}
