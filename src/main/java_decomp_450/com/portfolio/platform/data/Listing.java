package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.qf;
import com.fossil.vc7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<qf<T>> pagedList;
    @DexIgnore
    public /* final */ vc7<i97> refresh;
    @DexIgnore
    public /* final */ vc7<i97> retry;

    @DexIgnore
    public Listing(LiveData<qf<T>> liveData, LiveData<NetworkState> liveData2, vc7<i97> vc7, vc7<i97> vc72) {
        ee7.b(liveData, "pagedList");
        ee7.b(liveData2, "networkState");
        ee7.b(vc7, "refresh");
        ee7.b(vc72, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = vc7;
        this.retry = vc72;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.portfolio.platform.data.Listing */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Listing copy$default(Listing listing, LiveData liveData, LiveData liveData2, vc7 vc7, vc7 vc72, int i, Object obj) {
        if ((i & 1) != 0) {
            liveData = listing.pagedList;
        }
        if ((i & 2) != 0) {
            liveData2 = listing.networkState;
        }
        if ((i & 4) != 0) {
            vc7 = listing.refresh;
        }
        if ((i & 8) != 0) {
            vc72 = listing.retry;
        }
        return listing.copy(liveData, liveData2, vc7, vc72);
    }

    @DexIgnore
    public final LiveData<qf<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final vc7<i97> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final vc7<i97> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<qf<T>> liveData, LiveData<NetworkState> liveData2, vc7<i97> vc7, vc7<i97> vc72) {
        ee7.b(liveData, "pagedList");
        ee7.b(liveData2, "networkState");
        ee7.b(vc7, "refresh");
        ee7.b(vc72, "retry");
        return new Listing<>(liveData, liveData2, vc7, vc72);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Listing)) {
            return false;
        }
        Listing listing = (Listing) obj;
        return ee7.a(this.pagedList, listing.pagedList) && ee7.a(this.networkState, listing.networkState) && ee7.a(this.refresh, listing.refresh) && ee7.a(this.retry, listing.retry);
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<qf<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final vc7<i97> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final vc7<i97> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        LiveData<qf<T>> liveData = this.pagedList;
        int i = 0;
        int hashCode = (liveData != null ? liveData.hashCode() : 0) * 31;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = (hashCode + (liveData2 != null ? liveData2.hashCode() : 0)) * 31;
        vc7<i97> vc7 = this.refresh;
        int hashCode3 = (hashCode2 + (vc7 != null ? vc7.hashCode() : 0)) * 31;
        vc7<i97> vc72 = this.retry;
        if (vc72 != null) {
            i = vc72.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
