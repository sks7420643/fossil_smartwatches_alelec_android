package com.portfolio.platform.data;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Access {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ String l;
    @DexIgnore
    public /* final */ String m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ String o;

    @DexIgnore
    public Access(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14) {
        ee7.b(str, "a");
        ee7.b(str2, "b");
        ee7.b(str3, "c");
        ee7.b(str4, "d");
        ee7.b(str5, "e");
        ee7.b(str6, "f");
        ee7.b(str7, "g");
        ee7.b(str8, "h");
        ee7.b(str9, "i");
        ee7.b(str10, "k");
        ee7.b(str11, "l");
        ee7.b(str12, "m");
        ee7.b(str13, "n");
        ee7.b(str14, "o");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
        this.h = str8;
        this.i = str9;
        this.k = str10;
        this.l = str11;
        this.m = str12;
        this.n = str13;
        this.o = str14;
    }

    @DexIgnore
    public static /* synthetic */ Access copy$default(Access access, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, int i2, Object obj) {
        return access.copy((i2 & 1) != 0 ? access.a : str, (i2 & 2) != 0 ? access.b : str2, (i2 & 4) != 0 ? access.c : str3, (i2 & 8) != 0 ? access.d : str4, (i2 & 16) != 0 ? access.e : str5, (i2 & 32) != 0 ? access.f : str6, (i2 & 64) != 0 ? access.g : str7, (i2 & 128) != 0 ? access.h : str8, (i2 & 256) != 0 ? access.i : str9, (i2 & 512) != 0 ? access.k : str10, (i2 & 1024) != 0 ? access.l : str11, (i2 & 2048) != 0 ? access.m : str12, (i2 & 4096) != 0 ? access.n : str13, (i2 & 8192) != 0 ? access.o : str14);
    }

    @DexIgnore
    public final String component1() {
        return this.a;
    }

    @DexIgnore
    public final String component10() {
        return this.k;
    }

    @DexIgnore
    public final String component11() {
        return this.l;
    }

    @DexIgnore
    public final String component12() {
        return this.m;
    }

    @DexIgnore
    public final String component13() {
        return this.n;
    }

    @DexIgnore
    public final String component14() {
        return this.o;
    }

    @DexIgnore
    public final String component2() {
        return this.b;
    }

    @DexIgnore
    public final String component3() {
        return this.c;
    }

    @DexIgnore
    public final String component4() {
        return this.d;
    }

    @DexIgnore
    public final String component5() {
        return this.e;
    }

    @DexIgnore
    public final String component6() {
        return this.f;
    }

    @DexIgnore
    public final String component7() {
        return this.g;
    }

    @DexIgnore
    public final String component8() {
        return this.h;
    }

    @DexIgnore
    public final String component9() {
        return this.i;
    }

    @DexIgnore
    public final Access copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14) {
        ee7.b(str, "a");
        ee7.b(str2, "b");
        ee7.b(str3, "c");
        ee7.b(str4, "d");
        ee7.b(str5, "e");
        ee7.b(str6, "f");
        ee7.b(str7, "g");
        ee7.b(str8, "h");
        ee7.b(str9, "i");
        ee7.b(str10, "k");
        ee7.b(str11, "l");
        ee7.b(str12, "m");
        ee7.b(str13, "n");
        ee7.b(str14, "o");
        return new Access(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Access)) {
            return false;
        }
        Access access = (Access) obj;
        return ee7.a(this.a, access.a) && ee7.a(this.b, access.b) && ee7.a(this.c, access.c) && ee7.a(this.d, access.d) && ee7.a(this.e, access.e) && ee7.a(this.f, access.f) && ee7.a(this.g, access.g) && ee7.a(this.h, access.h) && ee7.a(this.i, access.i) && ee7.a(this.k, access.k) && ee7.a(this.l, access.l) && ee7.a(this.m, access.m) && ee7.a(this.n, access.n) && ee7.a(this.o, access.o);
    }

    @DexIgnore
    public final String getA() {
        return this.a;
    }

    @DexIgnore
    public final String getB() {
        return this.b;
    }

    @DexIgnore
    public final String getC() {
        return this.c;
    }

    @DexIgnore
    public final String getD() {
        return this.d;
    }

    @DexIgnore
    public final String getE() {
        return this.e;
    }

    @DexIgnore
    public final String getF() {
        return this.f;
    }

    @DexIgnore
    public final String getG() {
        return this.g;
    }

    @DexIgnore
    public final String getH() {
        return this.h;
    }

    @DexIgnore
    public final String getI() {
        return this.i;
    }

    @DexIgnore
    public final String getK() {
        return this.k;
    }

    @DexIgnore
    public final String getL() {
        return this.l;
    }

    @DexIgnore
    public final String getM() {
        return this.m;
    }

    @DexIgnore
    public final String getN() {
        return this.n;
    }

    @DexIgnore
    public final String getO() {
        return this.o;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.f;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.g;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.h;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.i;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.k;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.l;
        int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.m;
        int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.n;
        int hashCode13 = (hashCode12 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.o;
        if (str14 != null) {
            i2 = str14.hashCode();
        }
        return hashCode13 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Access(a=" + this.a + ", b=" + this.b + ", c=" + this.c + ", d=" + this.d + ", e=" + this.e + ", f=" + this.f + ", g=" + this.g + ", h=" + this.h + ", i=" + this.i + ", k=" + this.k + ", l=" + this.l + ", m=" + this.m + ", n=" + this.n + ", o=" + this.o + ")";
    }
}
