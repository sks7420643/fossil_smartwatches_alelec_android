package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.zd7;
import com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.StressWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerFitnessData {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveMinuteWrapper activeMinute;
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public List<RestingWrapper> restings;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public List<SleepSessionWrapper> sleeps;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public StressWrapper stress;
    @DexIgnore
    public DateTime syncTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public List<WorkoutSessionWrapper> workouts;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerFitnessData.class.getSimpleName();
        ee7.a((Object) simpleName, "ServerFitnessData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerFitnessData(DateTime dateTime, DateTime dateTime2, DateTime dateTime3, int i, String str) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime3, "syncTime");
        ee7.b(str, "serialNumber");
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.syncTime = dateTime3;
        this.timezoneOffsetInSecond = i;
        this.serialNumber = str;
        this.restings = new ArrayList();
        this.sleeps = new ArrayList();
        this.workouts = new ArrayList();
    }

    @DexIgnore
    public final ActiveMinuteWrapper getActiveMinute() {
        ActiveMinuteWrapper activeMinuteWrapper = this.activeMinute;
        if (activeMinuteWrapper != null) {
            return activeMinuteWrapper;
        }
        ee7.d("activeMinute");
        throw null;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        CalorieWrapper calorieWrapper = this.calorie;
        if (calorieWrapper != null) {
            return calorieWrapper;
        }
        ee7.d("calorie");
        throw null;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final List<RestingWrapper> getRestings() {
        return this.restings;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        StepWrapper stepWrapper = this.step;
        if (stepWrapper != null) {
            return stepWrapper;
        }
        ee7.d("step");
        throw null;
    }

    @DexIgnore
    public final StressWrapper getStress() {
        return this.stress;
    }

    @DexIgnore
    public final DateTime getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final void setActiveMinute(ActiveMinuteWrapper activeMinuteWrapper) {
        ee7.b(activeMinuteWrapper, "<set-?>");
        this.activeMinute = activeMinuteWrapper;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        ee7.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setRestings(List<RestingWrapper> list) {
        ee7.b(list, "<set-?>");
        this.restings = list;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        ee7.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setStress(StressWrapper stressWrapper) {
        this.stress = stressWrapper;
    }

    @DexIgnore
    public final void setSyncTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.syncTime = dateTime;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ServerFitnessData(FitnessDataWrapper fitnessDataWrapper) {
        this(fitnessDataWrapper.getStartTimeTZ(), fitnessDataWrapper.getEndTimeTZ(), fitnessDataWrapper.getSyncTime(), fitnessDataWrapper.getTimezoneOffsetInSecond(), fitnessDataWrapper.getSerialNumber());
        ee7.b(fitnessDataWrapper, "fitnessData");
        this.step = fitnessDataWrapper.getStep();
        this.activeMinute = fitnessDataWrapper.getActiveMinute();
        this.calorie = fitnessDataWrapper.getCalorie();
        this.distance = fitnessDataWrapper.getDistance();
        if (fitnessDataWrapper.getStress() != null) {
            this.stress = fitnessDataWrapper.getStress();
        }
        this.restings.addAll(fitnessDataWrapper.getResting());
        this.heartRate = fitnessDataWrapper.getHeartRate();
        this.sleeps.addAll(fitnessDataWrapper.getSleeps());
        this.workouts.addAll(fitnessDataWrapper.getWorkouts());
    }
}
