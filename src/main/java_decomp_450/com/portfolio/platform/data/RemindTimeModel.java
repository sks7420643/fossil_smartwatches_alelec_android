package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.re4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeModel {
    @DexIgnore
    @re4
    public int minutes;
    @DexIgnore
    @re4
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        ee7.b(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        ee7.b(str, "<set-?>");
        this.remindTimeName = str;
    }
}
