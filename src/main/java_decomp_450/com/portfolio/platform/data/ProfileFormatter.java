package com.portfolio.platform.data;

import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.we7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileFormatter implements NumberPickerLarge.f, RulerValuePicker.a, Serializable {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int UNIT_DEFAULT; // = -1;
    @DexIgnore
    public static /* final */ int UNIT_DOT; // = 2;
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_FEET_INCHES; // = 3;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public static /* final */ int UNIT_WEIGHT; // = 4;
    @DexIgnore
    public /* final */ int mUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public ProfileFormatter(int i) {
        this.mUnit = i;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.NumberPickerLarge.f, com.portfolio.platform.view.ruler.RulerValuePicker.a
    public String format(int i) {
        int i2 = this.mUnit;
        if (i2 == 0) {
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String a = ig5.a(PortfolioApp.g0.c(), 2131887366);
            ee7.a((Object) a, "LanguageHelper.getString\u2026ce, R.string.feet_format)");
            String format = String.format(locale, a, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            return format;
        } else if (i2 == 1) {
            we7 we72 = we7.a;
            Locale locale2 = Locale.US;
            ee7.a((Object) locale2, "Locale.US");
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131887390);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026, R.string.inches_format)");
            String format2 = String.format(locale2, a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            return format2;
        } else if (i2 == 2) {
            we7 we73 = we7.a;
            Locale locale3 = Locale.US;
            ee7.a((Object) locale3, "Locale.US");
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131887354);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026nce, R.string.dot_format)");
            String format3 = String.format(locale3, a3, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            return format3;
        } else if (i2 == 3) {
            we7 we74 = we7.a;
            Locale locale4 = Locale.US;
            ee7.a((Object) locale4, "Locale.US");
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131887367);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026tring.feet_inches_format)");
            String format4 = String.format(locale4, a4, Arrays.copyOf(new Object[]{Integer.valueOf(i / 12), Integer.valueOf(i % 12)}, 2));
            ee7.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            return format4;
        } else if (i2 != 4) {
            we7 we75 = we7.a;
            Locale locale5 = Locale.US;
            ee7.a((Object) locale5, "Locale.US");
            String a5 = ig5.a(PortfolioApp.g0.c(), 2131887448);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026, R.string.normal_format)");
            String format5 = String.format(locale5, a5, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format5, "java.lang.String.format(locale, format, *args)");
            return format5;
        } else {
            we7 we76 = we7.a;
            Locale locale6 = Locale.US;
            ee7.a((Object) locale6, "Locale.US");
            String a6 = ig5.a(PortfolioApp.g0.c(), 2131887572);
            ee7.a((Object) a6, "LanguageHelper.getString\u2026, R.string.weight_format)");
            String format6 = String.format(locale6, a6, Arrays.copyOf(new Object[]{Integer.valueOf(i / 10), Integer.valueOf(i % 10)}, 2));
            ee7.a((Object) format6, "java.lang.String.format(locale, format, *args)");
            return format6;
        }
    }

    @DexIgnore
    public final int getMUnit() {
        return this.mUnit;
    }
}
