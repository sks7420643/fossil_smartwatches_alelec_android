package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.ai7;
import com.fossil.b53;
import com.fossil.c53;
import com.fossil.ee7;
import com.fossil.s87;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends c53 {
    @DexIgnore
    public /* final */ /* synthetic */ ai7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ b53 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(ai7 ai7, b53 b53, LocationRequest locationRequest) {
        this.$continuation = ai7;
        this.$this_requestLocationUpdates$inlined = b53;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    @Override // com.fossil.c53
    public void onLocationResult(LocationResult locationResult) {
        ee7.b(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        Location e = locationResult.e();
        if (e != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "requestLocationUpdates lastLocation=" + e);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "requestLocationUpdates lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.a((c53) this);
        if (this.$continuation.isActive()) {
            ai7 ai7 = this.$continuation;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(e));
        }
    }
}
