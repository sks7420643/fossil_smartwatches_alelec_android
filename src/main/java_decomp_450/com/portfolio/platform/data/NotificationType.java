package com.portfolio.platform.data;

import android.text.TextUtils;
import com.fossil.ig5;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationType {
    CONTACT(2131887456),
    CODE_WORD(2131887455),
    CALL(2131887225),
    SMS(2131887226),
    EMAIL(2131887356),
    APP_FILTER(2131887454),
    FITNESS_GOAL_ACHIEVED(2131887457),
    APP_MODE(2131887452),
    OTHER(2131887450),
    CONTACT_EMPTY(2131887453),
    APP_EMPTY(2131887451);
    
    @DexIgnore
    public /* final */ int sectionTitleResId;

    @DexIgnore
    public NotificationType(int i) {
        this.sectionTitleResId = i;
    }

    @DexIgnore
    public static NotificationType find(String str) {
        if (!TextUtils.isEmpty(str)) {
            return valueOf(str);
        }
        return null;
    }

    @DexIgnore
    public int getSectionTitleResId() {
        return this.sectionTitleResId;
    }

    @DexIgnore
    public String getSectionTitleString() {
        return ig5.a(PortfolioApp.c0, this.sectionTitleResId);
    }
}
