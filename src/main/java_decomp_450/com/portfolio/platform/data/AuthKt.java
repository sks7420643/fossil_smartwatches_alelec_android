package com.portfolio.platform.data;

import com.portfolio.platform.data.model.MFUser;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AuthKt {
    @DexIgnore
    public static final MFUser.Auth toAuth(Auth auth) {
        String date;
        if (auth == null) {
            return null;
        }
        String accessToken = auth.getAccessToken();
        String str = "";
        if (accessToken == null) {
            accessToken = str;
        }
        String refreshToken = auth.getRefreshToken();
        if (refreshToken == null) {
            refreshToken = str;
        }
        Date accessTokenExpiresAt = auth.getAccessTokenExpiresAt();
        if (!(accessTokenExpiresAt == null || (date = accessTokenExpiresAt.toString()) == null)) {
            str = date;
        }
        Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
        return new MFUser.Auth(accessToken, refreshToken, str, accessTokenExpiresIn != null ? accessTokenExpiresIn.intValue() : 0);
    }
}
