package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.lx6;
import com.fossil.qx6;
import com.fossil.t3;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon2$result$Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2 this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level3 extends lx6<ActivitySummary, ie4> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2$result$Anon1_Level2 this$0;

        @DexIgnore
        public Anon1_Level3(SummariesRepository$getSummary$Anon2$result$Anon1_Level2 summariesRepository$getSummary$Anon2$result$Anon1_Level2, List list) {
            this.this$0 = summariesRepository$getSummary$Anon2$result$Anon1_Level2;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public Object createCall(fb7<? super fv7<ie4>> fb7) {
            Date q = zd5.q(this.this$0.this$0.$date);
            Date l = zd5.l(this.this$0.this$0.$date);
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.this$0.mApiServiceV2;
            String g = zd5.g(q);
            ee7.a((Object) g, "DateHelper.formatShortDate(startDate)");
            String g2 = zd5.g(l);
            ee7.a((Object) g2, "DateHelper.formatShortDate(endDate)");
            return access$getMApiServiceV2$p.getSummaries(g, g2, 0, 100, fb7);
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public Object loadFromDb(fb7<? super LiveData<ActivitySummary>> fb7) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(this.this$0.this$0.$date);
            LiveData<ActivitySummary> activitySummaryLiveData = this.this$0.$fitnessDatabase.activitySummaryDao().getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!zd5.w(this.this$0.this$0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            LiveData a = ge.a(activitySummaryLiveData, new SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(this));
            ee7.a((Object) a, "Transformations.map(acti\u2026ary\n                    }");
            return a;
        }

        @DexIgnore
        @Override // com.fossil.lx6
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
        @Override // com.fossil.lx6
        public /* bridge */ /* synthetic */ Object saveCallResult(ie4 ie4, fb7 fb7) {
            return saveCallResult(ie4, (fb7<? super i97>) fb7);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0162 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object saveCallResult(com.fossil.ie4 r13, com.fossil.fb7<? super com.fossil.i97> r14) {
            /*
                r12 = this;
                boolean r0 = r14 instanceof com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                if (r0 == 0) goto L_0x0013
                r0 = r14
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                int r1 = r0.label
                r2 = -2147483648(0xffffffff80000000, float:-0.0)
                r3 = r1 & r2
                if (r3 == 0) goto L_0x0013
                int r1 = r1 - r2
                r0.label = r1
                goto L_0x0018
            L_0x0013:
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                r0.<init>(r12, r14)
            L_0x0018:
                java.lang.Object r14 = r0.result
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 0
                r4 = 2
                r5 = 1
                java.lang.String r6 = "SummariesRepository"
                if (r2 == 0) goto L_0x0059
                if (r2 == r5) goto L_0x0048
                if (r2 != r4) goto L_0x0040
                java.lang.Object r13 = r0.L$3
                java.util.List r13 = (java.util.List) r13
                java.lang.Object r13 = r0.L$2
                java.util.ArrayList r13 = (java.util.ArrayList) r13
                java.lang.Object r13 = r0.L$1
                com.fossil.ie4 r13 = (com.fossil.ie4) r13
                java.lang.Object r13 = r0.L$0
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3 r13 = (com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3) r13
                com.fossil.t87.a(r14)     // Catch:{ Exception -> 0x0163 }
                goto L_0x0183
            L_0x0040:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r14 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r14)
                throw r13
            L_0x0048:
                java.lang.Object r13 = r0.L$2
                java.util.ArrayList r13 = (java.util.ArrayList) r13
                java.lang.Object r2 = r0.L$1
                com.fossil.ie4 r2 = (com.fossil.ie4) r2
                java.lang.Object r7 = r0.L$0
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3 r7 = (com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3) r7
                com.fossil.t87.a(r14)
                goto L_0x00fb
            L_0x0059:
                com.fossil.t87.a(r14)
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r7 = "getSummary - saveCallResult -- date="
                r2.append(r7)
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2 r7 = r12.this$0
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2 r7 = r7.this$0
                java.util.Date r7 = r7.$date
                r2.append(r7)
                java.lang.String r7 = ", item="
                r2.append(r7)
                r2.append(r13)
                java.lang.String r2 = r2.toString()
                r14.d(r6, r2)
                java.util.ArrayList r14 = new java.util.ArrayList
                r14.<init>()
                com.fossil.be4 r2 = new com.fossil.be4
                r2.<init>()
                java.lang.Class<java.util.Date> r7 = java.util.Date.class
                com.portfolio.platform.helper.GsonConvertDate r8 = new com.portfolio.platform.helper.GsonConvertDate
                r8.<init>()
                r2.a(r7, r8)
                java.lang.Class<org.joda.time.DateTime> r7 = org.joda.time.DateTime.class
                com.portfolio.platform.helper.GsonConvertDateTime r8 = new com.portfolio.platform.helper.GsonConvertDateTime
                r8.<init>()
                r2.a(r7, r8)
                com.google.gson.Gson r2 = r2.a()
                java.lang.String r7 = r13.toString()
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r8 = new com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4
                r8.<init>()
                java.lang.reflect.Type r8 = r8.getType()
                java.lang.Object r2 = r2.a(r7, r8)
                com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
                if (r2 == 0) goto L_0x00de
                java.util.List r2 = r2.get_items()
                if (r2 == 0) goto L_0x00de
                java.util.Iterator r2 = r2.iterator()
            L_0x00c5:
                boolean r7 = r2.hasNext()
                if (r7 == 0) goto L_0x00de
                java.lang.Object r7 = r2.next()
                com.portfolio.platform.data.model.FitnessDayData r7 = (com.portfolio.platform.data.model.FitnessDayData) r7
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r7 = r7.toActivitySummary()
                java.lang.String r8 = "it.toActivitySummary()"
                com.fossil.ee7.a(r7, r8)
                r14.add(r7)
                goto L_0x00c5
            L_0x00de:
                com.fossil.ti7 r2 = com.fossil.qj7.b()
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 r7 = new com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4
                r7.<init>(r12, r3)
                r0.L$0 = r12
                r0.L$1 = r13
                r0.L$2 = r14
                r0.label = r5
                java.lang.Object r2 = com.fossil.vh7.a(r2, r7, r0)
                if (r2 != r1) goto L_0x00f6
                return r1
            L_0x00f6:
                r7 = r12
                r11 = r2
                r2 = r13
                r13 = r14
                r14 = r11
            L_0x00fb:
                java.util.List r14 = (java.util.List) r14
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "fitnessDatasSize "
                r9.append(r10)
                int r10 = r14.size()
                r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.d(r6, r9)
                boolean r8 = r13.isEmpty()
                r5 = r5 ^ r8
                if (r5 == 0) goto L_0x0183
                boolean r5 = r14.isEmpty()
                if (r5 == 0) goto L_0x0183
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "upsert "
                r8.append(r9)
                r9 = 0
                java.lang.Object r9 = r13.get(r9)
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r9 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r9
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                r5.d(r6, r8)
                com.fossil.ti7 r5 = com.fossil.qj7.b()
                com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon4_Level4 r8 = new com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$saveCallResult$Anon4_Level4
                r8.<init>(r7, r13, r3)
                r0.L$0 = r7
                r0.L$1 = r2
                r0.L$2 = r13
                r0.L$3 = r14
                r0.label = r4
                java.lang.Object r13 = com.fossil.vh7.a(r5, r8, r0)
                if (r13 != r1) goto L_0x0183
                return r1
            L_0x0163:
                r13 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "getSummary - saveCallResult -- e="
                r0.append(r1)
                r13.printStackTrace()
                com.fossil.i97 r13 = com.fossil.i97.a
                r0.append(r13)
                java.lang.String r13 = r0.toString()
                r14.e(r6, r13)
            L_0x0183:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.ie4, com.fossil.fb7):java.lang.Object");
        }

        @DexIgnore
        public boolean shouldFetch(ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$Anon2$result$Anon1_Level2(SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2, FitnessDatabase fitnessDatabase) {
        this.this$0 = summariesRepository$getSummary$Anon2;
        this.$fitnessDatabase = fitnessDatabase;
    }

    @DexIgnore
    public final LiveData<qx6<ActivitySummary>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - date=" + this.this$0.$date + " fitnessDataList=" + list.size());
        return new Anon1_Level3(this, list).asLiveData();
    }
}
