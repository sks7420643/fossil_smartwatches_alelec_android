package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSettingDao_Impl implements WatchAppLastSettingDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WatchAppLastSetting> __insertionAdapterOfWatchAppLastSetting;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteWatchAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WatchAppLastSetting> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppLastSetting` (`watchAppId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WatchAppLastSetting watchAppLastSetting) {
            if (watchAppLastSetting.getWatchAppId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, watchAppLastSetting.getWatchAppId());
            }
            if (watchAppLastSetting.getUpdatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, watchAppLastSetting.getUpdatedAt());
            }
            if (watchAppLastSetting.getSetting() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, watchAppLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting WHERE watchAppId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WatchAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchAppLastSetting> call() throws Exception {
            Cursor a = pi.a(WatchAppLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "watchAppId");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppLastSettingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWatchAppLastSetting = new Anon1(ciVar);
        this.__preparedStmtOfDeleteWatchAppLastSettingById = new Anon2(ciVar);
        this.__preparedStmtOfCleanUp = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void deleteWatchAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteWatchAppLastSettingById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public List<WatchAppLastSetting> getAllWatchAppLastSetting() {
        fi b = fi.b("SELECT * FROM watchAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "watchAppId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchAppLastSetting"}, false, (Callable) new Anon4(fi.b("SELECT * FROM watchAppLastSetting", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public WatchAppLastSetting getWatchAppLastSetting(String str) {
        fi b = fi.b("SELECT * FROM watchAppLastSetting WHERE watchAppId=? ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchAppLastSetting watchAppLastSetting = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "watchAppId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                watchAppLastSetting = new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return watchAppLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao
    public void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppLastSetting.insert(watchAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
