package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qe7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$2", f = "HeartRateSampleRepository.kt", l = {39}, m = "invokeSuspend")
public final class HeartRateSampleRepository$getHeartRateSamples$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<HeartRateSample>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository$getHeartRateSamples$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<HeartRateSample>, ApiResponse<HeartRate>> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, qe7 qe7, int i, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$offset = qe7;
                this.$limit = i;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<HeartRate>>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiService$p.getHeartRateSamples(g, g2, this.$offset.element, this.$limit, fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
            @Override // com.fossil.lx6
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>> r7) {
                /*
                    r6 = this;
                    boolean r0 = r7 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r7
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r6, r7)
                L_0x0018:
                    java.lang.Object r7 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    if (r2 == 0) goto L_0x0035
                    if (r2 != r3) goto L_0x002d
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r7)
                    goto L_0x006a
                L_0x002d:
                    java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r7.<init>(r0)
                    throw r7
                L_0x0035:
                    com.fossil.t87.a(r7)
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    java.lang.String r2 = com.portfolio.platform.data.source.HeartRateSampleRepository.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getHeartRateSamples loadFromDb isNotToday day = "
                    r4.append(r5)
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r5 = r6.this$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$end
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r7.d(r2, r4)
                    com.fossil.pg5 r7 = com.fossil.pg5.i
                    r0.L$0 = r6
                    r0.label = r3
                    java.lang.Object r7 = r7.b(r0)
                    if (r7 != r1) goto L_0x0069
                    return r1
                L_0x0069:
                    r0 = r6
                L_0x006a:
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r7
                    com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao r7 = r7.getHeartRateDao()
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r1 = r0.this$0
                    java.util.Date r1 = r1.$startDate
                    java.lang.String r2 = "startDate"
                    com.fossil.ee7.a(r1, r2)
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r0 = r0.this$0
                    java.util.Date r0 = r0.$endDate
                    java.lang.String r2 = "endDate"
                    com.fossil.ee7.a(r0, r2)
                    androidx.lifecycle.LiveData r7 = r7.getHeartRateSamples(r1, r0)
                    return r7
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.TAG, "getHeartRateSamples onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<HeartRate> apiResponse, fb7 fb7) {
                return processContinueFetching(apiResponse, (fb7<? super Boolean>) fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<HeartRate> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<HeartRate> apiResponse, fb7<? super Boolean> fb7) {
                Boolean a;
                Range range = apiResponse.get_range();
                if (range == null || (a = pb7.a(range.isHasNext())) == null || !a.booleanValue()) {
                    return pb7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.TAG, "getHeartRateSamples processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return pb7.a(true);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.diana.heartrate.HeartRate> r8, com.fossil.fb7<? super com.fossil.i97> r9) {
                /*
                    r7 = this;
                    boolean r0 = r9 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r9
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r7, r9)
                L_0x0018:
                    java.lang.Object r9 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    if (r2 == 0) goto L_0x003e
                    if (r2 != r3) goto L_0x0036
                    java.lang.Object r8 = r0.L$2
                    java.util.List r8 = (java.util.List) r8
                    java.lang.Object r1 = r0.L$1
                    com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r9)
                    goto L_0x00b6
                L_0x0036:
                    java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                    java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
                    r8.<init>(r9)
                    throw r8
                L_0x003e:
                    com.fossil.t87.a(r9)
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    java.lang.String r2 = com.portfolio.platform.data.source.HeartRateSampleRepository.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getHeartRateSamples saveCallResult onResponse: response = "
                    r4.append(r5)
                    java.util.List r5 = r8.get_items()
                    int r5 = r5.size()
                    r4.append(r5)
                    java.lang.String r5 = " hasNext="
                    r4.append(r5)
                    com.portfolio.platform.data.model.Range r5 = r8.get_range()
                    if (r5 == 0) goto L_0x0074
                    boolean r5 = r5.isHasNext()
                    java.lang.Boolean r5 = com.fossil.pb7.a(r5)
                    goto L_0x0075
                L_0x0074:
                    r5 = 0
                L_0x0075:
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r9.d(r2, r4)
                    java.util.ArrayList r9 = new java.util.ArrayList
                    r9.<init>()
                    java.util.List r2 = r8.get_items()
                    java.util.Iterator r2 = r2.iterator()
                L_0x008c:
                    boolean r4 = r2.hasNext()
                    if (r4 == 0) goto L_0x00a2
                    java.lang.Object r4 = r2.next()
                    com.portfolio.platform.data.model.diana.heartrate.HeartRate r4 = (com.portfolio.platform.data.model.diana.heartrate.HeartRate) r4
                    com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r4 = r4.toHeartRateSample()
                    if (r4 == 0) goto L_0x008c
                    r9.add(r4)
                    goto L_0x008c
                L_0x00a2:
                    com.fossil.pg5 r2 = com.fossil.pg5.i
                    r0.L$0 = r7
                    r0.L$1 = r8
                    r0.L$2 = r9
                    r0.label = r3
                    java.lang.Object r8 = r2.b(r0)
                    if (r8 != r1) goto L_0x00b3
                    return r1
                L_0x00b3:
                    r6 = r9
                    r9 = r8
                    r8 = r6
                L_0x00b6:
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r9 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r9
                    com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao r9 = r9.getHeartRateDao()
                    r9.upsertHeartRateSampleList(r8)
                    com.fossil.i97 r8 = com.fossil.i97.a
                    return r8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse, com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            public boolean shouldFetch(List<HeartRateSample> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(HeartRateSampleRepository$getHeartRateSamples$Anon2 heartRateSampleRepository$getHeartRateSamples$Anon2, Date date, Date date2) {
            this.this$0 = heartRateSampleRepository$getHeartRateSamples$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
        }

        @DexIgnore
        public final LiveData<qx6<List<HeartRateSample>>> apply(List<FitnessDataWrapper> list) {
            qe7 qe7 = new qe7();
            qe7.element = 0;
            ee7.a((Object) list, "fitnessDataList");
            Date date = this.$startDate;
            ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, qe7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$getHeartRateSamples$Anon2(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = heartRateSampleRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        HeartRateSampleRepository$getHeartRateSamples$Anon2 heartRateSampleRepository$getHeartRateSamples$Anon2 = new HeartRateSampleRepository$getHeartRateSamples$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, fb7);
        heartRateSampleRepository$getHeartRateSamples$Anon2.p$ = (yi7) obj;
        return heartRateSampleRepository$getHeartRateSamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<HeartRateSample>>>> fb7) {
        return ((HeartRateSampleRepository$getHeartRateSamples$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            date = zd5.q(this.$start);
            Date l = zd5.l(this.$end);
            ti7 b = qj7.b();
            HeartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2 heartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2 = new HeartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.L$1 = date;
            this.L$2 = l;
            this.label = 1;
            obj = vh7.a(b, heartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
            date2 = l;
        } else if (i == 1) {
            date2 = (Date) this.L$2;
            date = (Date) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDataDao fitnessDataDao = ((FitnessDatabase) obj).getFitnessDataDao();
        ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
        ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
        return ge.b(fitnessDataDao.getFitnessDataLiveData(date, date2), new Anon1_Level2(this, date, date2));
    }
}
