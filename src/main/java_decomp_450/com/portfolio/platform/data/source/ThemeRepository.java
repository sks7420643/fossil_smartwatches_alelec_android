package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.local.ThemeDao;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_THEME_URI; // = "theme/default/config";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ ThemeDao mThemeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return ThemeRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ThemeRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "ThemeRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ThemeRepository(ThemeDao themeDao, PortfolioApp portfolioApp) {
        ee7.b(themeDao, "mThemeDao");
        ee7.b(portfolioApp, "mApp");
        this.mThemeDao = themeDao;
        this.mApp = portfolioApp;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007f, code lost:
        com.fossil.hc7.a(r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0082, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void loadTheme(java.lang.String r6) {
        /*
            r5 = this;
            java.lang.Class<com.portfolio.platform.data.model.Theme> r0 = com.portfolio.platform.data.model.Theme.class
            com.fossil.be4 r1 = new com.fossil.be4
            r1.<init>()
            com.portfolio.platform.gson.ThemeDeserializer r2 = new com.portfolio.platform.gson.ThemeDeserializer
            r2.<init>()
            r1.a(r0, r2)
            com.google.gson.Gson r1 = r1.a()
            r2 = 0
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0085 }
            com.portfolio.platform.PortfolioApp r4 = r5.mApp     // Catch:{ Exception -> 0x0085 }
            android.content.res.AssetManager r4 = r4.getAssets()     // Catch:{ Exception -> 0x0085 }
            java.io.InputStream r6 = r4.open(r6)     // Catch:{ Exception -> 0x0085 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0085 }
            java.nio.charset.Charset r6 = com.fossil.sg7.a     // Catch:{ Exception -> 0x0085 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0085 }
            r4.<init>(r3, r6)     // Catch:{ Exception -> 0x0085 }
            r6 = 8192(0x2000, float:1.14794E-41)
            boolean r3 = r4 instanceof java.io.BufferedReader     // Catch:{ Exception -> 0x0085 }
            if (r3 == 0) goto L_0x0033
            java.io.BufferedReader r4 = (java.io.BufferedReader) r4     // Catch:{ Exception -> 0x0085 }
            goto L_0x0039
        L_0x0033:
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0085 }
            r3.<init>(r4, r6)     // Catch:{ Exception -> 0x0085 }
            r4 = r3
        L_0x0039:
            java.lang.String r6 = com.fossil.sc7.a(r4)     // Catch:{ all -> 0x007c }
            com.fossil.hc7.a(r4, r2)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            boolean r2 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            if (r2 != 0) goto L_0x0072
            java.lang.Object r6 = r1.a(r6, r0)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            com.portfolio.platform.data.model.Theme r6 = (com.portfolio.platform.data.model.Theme) r6     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            com.portfolio.platform.data.source.local.ThemeDao r0 = r5.mThemeDao     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.String r1 = "theme"
            com.fossil.ee7.a(r6, r1)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r0.upsertTheme(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.String r1 = com.portfolio.platform.data.source.ThemeRepository.TAG     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r2.<init>()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.String r3 = "initializeLocalTheme - add theme "
            r2.append(r3)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r2.append(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.String r6 = r2.toString()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r0.d(r1, r6)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
        L_0x0072:
            r4.close()
            goto L_0x00a7
        L_0x0076:
            r6 = move-exception
            r2 = r4
            goto L_0x00a8
        L_0x0079:
            r6 = move-exception
            r2 = r4
            goto L_0x0086
        L_0x007c:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x007e }
        L_0x007e:
            r0 = move-exception
            com.fossil.hc7.a(r4, r6)
            throw r0
        L_0x0083:
            r6 = move-exception
            goto L_0x00a8
        L_0x0085:
            r6 = move-exception
        L_0x0086:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0083 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0083 }
            java.lang.String r1 = com.portfolio.platform.data.source.ThemeRepository.TAG     // Catch:{ all -> 0x0083 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0083 }
            r3.<init>()     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = "Exception when parse theme file "
            r3.append(r4)     // Catch:{ all -> 0x0083 }
            r3.append(r6)     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x0083 }
            r0.d(r1, r6)     // Catch:{ all -> 0x0083 }
            if (r2 == 0) goto L_0x00a7
            r2.close()
        L_0x00a7:
            return
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            r2.close()
        L_0x00ad:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThemeRepository.loadTheme(java.lang.String):void");
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThemeRepository$cleanUp$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object deleteTheme(Theme theme, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThemeRepository$deleteTheme$Anon2(this, theme, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object getCurrentTheme(fb7<? super Theme> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getCurrentTheme$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getCurrentThemeId(fb7<? super String> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getCurrentThemeId$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getListStyleById(String str, fb7<? super ArrayList<Style>> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getListStyleById$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object getListTheme(fb7<? super List<Theme>> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getListTheme$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getListThemeId(fb7<? super List<String>> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getListThemeId$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getListThemeIdByType(String str, fb7<? super List<String>> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getListThemeIdByType$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object getNameById(String str, fb7<? super String> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getNameById$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object getThemeById(String str, fb7<? super Theme> fb7) {
        return vh7.a(qj7.b(), new ThemeRepository$getThemeById$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object initializeLocalTheme(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThemeRepository$initializeLocalTheme$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object setCurrentThemeId(String str, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThemeRepository$setCurrentThemeId$Anon2(this, str, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object upsertUserTheme(Theme theme, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new ThemeRepository$upsertUserTheme$Anon2(this, theme, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }
}
