package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends li {
    @DexIgnore
    public WorkoutSettingDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        ee7.b(wiVar, "database");
        FLogger.INSTANCE.getLocal().d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 Start");
        wiVar.beginTransaction();
        try {
            wiVar.execSQL("UPDATE `workoutSetting` SET `enable` = 1, `askMeFirst` = 1, `pinType` = 2");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 with exception: " + e.getMessage());
            wiVar.execSQL("DROP TABLE IF EXISTS `workoutSetting`");
            wiVar.execSQL("CREATE TABLE `workoutSetting` (`type` TEXT NOT NULL, `mode` TEXT, `enable` INTEGER NOT NULL, `askMeFirst` INTEGER NOT NULL, `startLatency` INTEGER NOT NULL, `pauseLatency` INTEGER NOT NULL, `resumeLatency` INTEGER NOT NULL, `stopLatency` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`type`))");
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
        FLogger.INSTANCE.getLocal().d(WorkoutSettingDatabase.TAG, "Migration 1 to 2 Done");
    }
}
