package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.av4;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.iv4;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.si;
import com.fossil.vh;
import com.fossil.vt7;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppDao_Impl implements MicroAppDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<DeclarationFile> __insertionAdapterOfDeclarationFile;
    @DexIgnore
    public /* final */ vh<MicroApp> __insertionAdapterOfMicroApp;
    @DexIgnore
    public /* final */ vh<MicroAppSetting> __insertionAdapterOfMicroAppSetting;
    @DexIgnore
    public /* final */ vh<MicroAppVariant> __insertionAdapterOfMicroAppVariant;
    @DexIgnore
    public /* final */ av4 __millisecondDateConverter; // = new av4();
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllDeclarationFileTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllMicroAppGalleryTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllMicroAppSettingTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllMicroAppVariantTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteMicroAppsBySerial;
    @DexIgnore
    public /* final */ iv4 __stringArrayConverter; // = new iv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<MicroApp> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microApp` (`id`,`name`,`nameKey`,`serialNumber`,`categories`,`description`,`descriptionKey`,`icon`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MicroApp microApp) {
            if (microApp.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, microApp.getId());
            }
            if (microApp.getName() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, microApp.getName());
            }
            if (microApp.getNameKey() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, microApp.getNameKey());
            }
            if (microApp.getSerialNumber() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, microApp.getSerialNumber());
            }
            String a = MicroAppDao_Impl.this.__stringArrayConverter.a(microApp.getCategories());
            if (a == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a);
            }
            if (microApp.getDescription() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, microApp.getDescription());
            }
            if (microApp.getDescriptionKey() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, microApp.getDescriptionKey());
            }
            if (microApp.getIcon() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, microApp.getIcon());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<MicroAppVariant> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppVariant` (`id`,`appId`,`name`,`description`,`createdAt`,`updatedAt`,`majorNumber`,`minorNumber`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MicroAppVariant microAppVariant) {
            if (microAppVariant.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, microAppVariant.getId());
            }
            if (microAppVariant.getAppId() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, microAppVariant.getAppId());
            }
            if (microAppVariant.getName() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, microAppVariant.getName());
            }
            if (microAppVariant.getDescription() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, microAppVariant.getDescription());
            }
            ajVar.bindLong(5, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getCreatedAt()));
            ajVar.bindLong(6, MicroAppDao_Impl.this.__millisecondDateConverter.a(microAppVariant.getUpdatedAt()));
            ajVar.bindLong(7, (long) microAppVariant.getMajorNumber());
            ajVar.bindLong(8, (long) microAppVariant.getMinorNumber());
            if (microAppVariant.getSerialNumber() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, microAppVariant.getSerialNumber());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh<MicroAppSetting> {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppSetting` (`id`,`appId`,`setting`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MicroAppSetting microAppSetting) {
            if (microAppSetting.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, microAppSetting.getId());
            }
            if (microAppSetting.getAppId() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, microAppSetting.getAppId());
            }
            if (microAppSetting.getSetting() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, microAppSetting.getSetting());
            }
            if (microAppSetting.getCreatedAt() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, microAppSetting.getCreatedAt());
            }
            if (microAppSetting.getUpdatedAt() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, microAppSetting.getUpdatedAt());
            }
            ajVar.bindLong(6, (long) microAppSetting.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh<DeclarationFile> {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `declarationFile` (`appId`,`serialNumber`,`variantName`,`id`,`description`,`content`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DeclarationFile declarationFile) {
            String str = declarationFile.appId;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            String str2 = declarationFile.serialNumber;
            if (str2 == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, str2);
            }
            String str3 = declarationFile.variantName;
            if (str3 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, str3);
            }
            if (declarationFile.getId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, declarationFile.getId());
            }
            if (declarationFile.getDescription() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, declarationFile.getDescription());
            }
            if (declarationFile.getContent() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, declarationFile.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microApp";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends ji {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microAppSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends ji {
        @DexIgnore
        public Anon7(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microAppVariant";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends ji {
        @DexIgnore
        public Anon8(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM declarationFile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends ji {
        @DexIgnore
        public Anon9(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microApp WHERE serialNumber = ?";
        }
    }

    @DexIgnore
    public MicroAppDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfMicroApp = new Anon1(ciVar);
        this.__insertionAdapterOfMicroAppVariant = new Anon2(ciVar);
        this.__insertionAdapterOfMicroAppSetting = new Anon3(ciVar);
        this.__insertionAdapterOfDeclarationFile = new Anon4(ciVar);
        this.__preparedStmtOfClearAllMicroAppGalleryTable = new Anon5(ciVar);
        this.__preparedStmtOfClearAllMicroAppSettingTable = new Anon6(ciVar);
        this.__preparedStmtOfClearAllMicroAppVariantTable = new Anon7(ciVar);
        this.__preparedStmtOfClearAllDeclarationFileTable = new Anon8(ciVar);
        this.__preparedStmtOfDeleteMicroAppsBySerial = new Anon9(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllDeclarationFileTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllDeclarationFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllDeclarationFileTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroApp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppGalleryTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppSettingTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllMicroAppSettingTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppSettingTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppVariantTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllMicroAppVariantTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppVariantTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void deleteMicroAppsBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteMicroAppsBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppsBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<DeclarationFile> getAllDeclarationFile() {
        fi b = fi.b("SELECT*FROM declarationFile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "appId");
            int b3 = oi.b(a, "serialNumber");
            int b4 = oi.b(a, "variantName");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "description");
            int b7 = oi.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariant(String str, int i) {
        fi b = fi.b("SELECT * FROM microAppVariant WHERE serialNumber = ? AND majorNumber = ?", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        b.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            int b8 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = oi.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial() {
        fi b = fi.b("SELECT * FROM microAppVariant GROUP BY serialNumber", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            int b8 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = oi.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariants() {
        fi b = fi.b("SELECT*FROM microAppVariant", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            int b8 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = oi.b(a, "serialNumber");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10)));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getAllMicroApps() {
        fi b = fi.b("SELECT*FROM microApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "serialNumber");
            int b6 = oi.b(a, "categories");
            int b7 = oi.b(a, "description");
            int b8 = oi.b(a, "descriptionKey");
            int b9 = oi.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<DeclarationFile> getDeclarationFiles(String str, String str2, String str3) {
        fi b = fi.b("SELECT * FROM declarationFile WHERE appId = ? AND serialNumber = ? AND variantName = ? ORDER BY rowid ASC", 3);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        if (str3 == null) {
            b.bindNull(3);
        } else {
            b.bindString(3, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "appId");
            int b3 = oi.b(a, "serialNumber");
            int b4 = oi.b(a, "variantName");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "description");
            int b7 = oi.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(a.getString(b5), a.getString(b6), a.getString(b7));
                declarationFile.appId = a.getString(b2);
                declarationFile.serialNumber = a.getString(b3);
                declarationFile.variantName = a.getString(b4);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getListMicroApp(String str) {
        fi b = fi.b("SELECT * FROM microApp WHERE serialNumber = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "serialNumber");
            int b6 = oi.b(a, "categories");
            int b7 = oi.b(a, "description");
            int b8 = oi.b(a, "descriptionKey");
            int b9 = oi.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppSetting> getListMicroAppSetting() {
        fi b = fi.b("SELECT * FROM microAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = oi.b(a, "createdAt");
            int b6 = oi.b(a, "updatedAt");
            int b7 = oi.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroApp getMicroApp(String str, String str2) {
        fi b = fi.b("SELECT * FROM microApp WHERE serialNumber = ? AND id = ?", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroApp microApp = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "serialNumber");
            int b6 = oi.b(a, "categories");
            int b7 = oi.b(a, "description");
            int b8 = oi.b(a, "descriptionKey");
            int b9 = oi.b(a, "icon");
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroApp getMicroAppById(String str, String str2) {
        fi b = fi.b("SELECT * FROM microApp WHERE id=? AND serialNumber=?", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroApp microApp = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "serialNumber");
            int b6 = oi.b(a, "categories");
            int b7 = oi.b(a, "description");
            int b8 = oi.b(a, "descriptionKey");
            int b9 = oi.b(a, "icon");
            if (a.moveToFirst()) {
                microApp = new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9));
            }
            return microApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        StringBuilder a = si.a();
        a.append("SELECT ");
        a.append(vt7.ANY_MARKER);
        a.append(" FROM microApp WHERE id IN (");
        int size = list.size();
        si.a(a, size);
        a.append(") AND serialNumber=");
        a.append("?");
        int i = 1;
        int i2 = size + 1;
        fi b = fi.b(a.toString(), i2);
        for (String str2 : list) {
            if (str2 == null) {
                b.bindNull(i);
            } else {
                b.bindString(i, str2);
            }
            i++;
        }
        if (str == null) {
            b.bindNull(i2);
        } else {
            b.bindString(i2, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "id");
            int b3 = oi.b(a2, "name");
            int b4 = oi.b(a2, "nameKey");
            int b5 = oi.b(a2, "serialNumber");
            int b6 = oi.b(a2, "categories");
            int b7 = oi.b(a2, "description");
            int b8 = oi.b(a2, "descriptionKey");
            int b9 = oi.b(a2, "icon");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new MicroApp(a2.getString(b2), a2.getString(b3), a2.getString(b4), a2.getString(b5), this.__stringArrayConverter.a(a2.getString(b6)), a2.getString(b7), a2.getString(b8), a2.getString(b9)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppSetting getMicroAppSetting(String str) {
        fi b = fi.b("SELECT * FROM microAppSetting WHERE appId= ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroAppSetting microAppSetting = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = oi.b(a, "createdAt");
            int b6 = oi.b(a, "updatedAt");
            int b7 = oi.b(a, "pinType");
            if (a.moveToFirst()) {
                microAppSetting = new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7));
            }
            return microAppSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3) {
        fi b = fi.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ? AND name = ?", 4);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        b.bindLong(3, (long) i);
        if (str3 == null) {
            b.bindNull(4);
        } else {
            b.bindString(4, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroAppVariant microAppVariant = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            int b8 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = oi.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        fi b = fi.b("SELECT * FROM microAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int b5 = oi.b(a, "createdAt");
            int b6 = oi.b(a, "updatedAt");
            int b7 = oi.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppSetting(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> queryMicroAppByName(String str, String str2) {
        fi b = fi.b("SELECT * FROM microApp WHERE name LIKE '%' || ? || '%' AND serialNumber=?", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "serialNumber");
            int b6 = oi.b(a, "categories");
            int b7 = oi.b(a, "description");
            int b8 = oi.b(a, "descriptionKey");
            int b9 = oi.b(a, "icon");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertDeclarationFileList(List<DeclarationFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDeclarationFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertListMicroApp(List<MicroApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroApp(MicroApp microApp) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(microApp);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppSetting(MicroAppSetting microAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(microAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppSettingList(List<MicroAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppVariant(MicroAppVariant microAppVariant) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(microAppVariant);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppVariantList(List<MicroAppVariant> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        fi b = fi.b("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ?", 3);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        b.bindLong(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        MicroAppVariant microAppVariant = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "appId");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "updatedAt");
            int b8 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int b9 = oi.b(a, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int b10 = oi.b(a, "serialNumber");
            if (a.moveToFirst()) {
                microAppVariant = new MicroAppVariant(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), this.__millisecondDateConverter.a(a.getLong(b6)), this.__millisecondDateConverter.a(a.getLong(b7)), a.getInt(b8), a.getInt(b9), a.getString(b10));
            }
            return microAppVariant;
        } finally {
            a.close();
            b.c();
        }
    }
}
