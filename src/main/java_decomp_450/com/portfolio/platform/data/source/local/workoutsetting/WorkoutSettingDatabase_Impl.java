package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDatabase_Impl extends WorkoutSettingDatabase {
    @DexIgnore
    public volatile WorkoutSettingDao _workoutSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `workoutSetting` (`type` TEXT NOT NULL, `mode` TEXT, `enable` INTEGER NOT NULL, `askMeFirst` INTEGER NOT NULL, `startLatency` INTEGER NOT NULL, `pauseLatency` INTEGER NOT NULL, `resumeLatency` INTEGER NOT NULL, `stopLatency` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`type`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '8ebd946c2b78fdb442012d32eb1b6201')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `workoutSetting`");
            if (((ci) WorkoutSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) WorkoutSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) WorkoutSettingDatabase_Impl.this).mDatabase = wiVar;
            WorkoutSettingDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) WorkoutSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) WorkoutSettingDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("type", new ti.a("type", "TEXT", true, 1, null, 1));
            hashMap.put("mode", new ti.a("mode", "TEXT", false, 0, null, 1));
            hashMap.put("enable", new ti.a("enable", "INTEGER", true, 0, null, 1));
            hashMap.put("askMeFirst", new ti.a("askMeFirst", "INTEGER", true, 0, null, 1));
            hashMap.put("startLatency", new ti.a("startLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("pauseLatency", new ti.a("pauseLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("resumeLatency", new ti.a("resumeLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("stopLatency", new ti.a("stopLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("workoutSetting", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "workoutSetting");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "workoutSetting(com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `workoutSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "workoutSetting");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(2), "8ebd946c2b78fdb442012d32eb1b6201", "2baade49018ab002e0858caff419b24c");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase
    public WorkoutSettingDao workoutSettingDao() {
        WorkoutSettingDao workoutSettingDao;
        if (this._workoutSettingDao != null) {
            return this._workoutSettingDao;
        }
        synchronized (this) {
            if (this._workoutSettingDao == null) {
                this._workoutSettingDao = new WorkoutSettingDao_Impl(this);
            }
            workoutSettingDao = this._workoutSettingDao;
        }
        return workoutSettingDao;
    }
}
