package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1", f = "ServerSettingRepository.kt", l = {}, m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository$getServerSettingList$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(ServerSettingRepository$getServerSettingList$Anon1 serverSettingRepository$getServerSettingList$Anon1, ServerSettingList serverSettingList, fb7 fb7) {
        super(2, fb7);
        this.this$0 = serverSettingRepository$getServerSettingList$Anon1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 = new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this.this$0, this.$serverSettingList, fb7);
        serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2.p$ = (yi7) obj;
        return serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                List<ServerSetting> serverSettings = serverSettingList.getServerSettings();
                Boolean a = serverSettings != null ? pb7.a(!serverSettings.isEmpty()) : null;
                if (a == null) {
                    ee7.a();
                    throw null;
                } else if (a.booleanValue()) {
                    this.this$0.this$0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
