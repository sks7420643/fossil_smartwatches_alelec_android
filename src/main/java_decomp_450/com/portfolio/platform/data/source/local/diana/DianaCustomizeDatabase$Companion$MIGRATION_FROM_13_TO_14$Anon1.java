package com.portfolio.platform.data.source.local.diana;

import com.fossil.ee7;
import com.fossil.li;
import com.fossil.qb5;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1 extends li {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        ee7.b(wiVar, "database");
        FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - start");
        wiVar.beginTransaction();
        try {
            FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - start");
            wiVar.execSQL("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            wiVar.execSQL("CREATE TABLE `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("DROP TABLE IF EXISTS `watch_face_temp`");
            wiVar.execSQL("CREATE TABLE `watch_face_temp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("INSERT INTO watch_face_temp(id,name, ringStyleItems, background, previewUrl, serial) SELECT id, name, ringStyleItems, background, previewUrl, serial FROM watch_face");
            int value = qb5.BACKGROUND.getValue();
            wiVar.execSQL("ALTER TABLE `watch_face_temp` ADD `watchFaceType` INTEGER NOT NULL CONSTRAINT DefaultTypeValue DEFAULT(" + value + ')');
            wiVar.execSQL("DROP TABLE `watch_face`");
            wiVar.execSQL("ALTER TABLE watch_face_temp RENAME TO watch_face");
            FLogger.INSTANCE.getLocal().d(DianaCustomizeDatabase.TAG, "Migrate WatchFace - end");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_13_TO_14 - end with exception -- e=" + e);
            wiVar.execSQL("DROP TABLE IF EXISTS `watch_face`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watch_face_temp`");
            wiVar.execSQL("CREATE TABLE `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
    }
}
