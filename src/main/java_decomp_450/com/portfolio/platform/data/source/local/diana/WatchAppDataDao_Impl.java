package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.nu4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.diana.WatchAppData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataDao_Impl implements WatchAppDataDao {
    @DexIgnore
    public /* final */ nu4 __dateTimeConverter; // = new nu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WatchAppData> __insertionAdapterOfWatchAppData;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteWatchAppDataById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WatchAppData> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppData` (`id`,`type`,`maxAppVersion`,`maxFirmwareOSVersion`,`minAppVersion`,`minFirmwareOSVersion`,`version`,`executableBinaryDataUrl`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WatchAppData watchAppData) {
            if (watchAppData.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, watchAppData.getId());
            }
            if (watchAppData.getType() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, watchAppData.getType());
            }
            if (watchAppData.getMaxAppVersion() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, watchAppData.getMaxAppVersion());
            }
            if (watchAppData.getMaxFirmwareOSVersion() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, watchAppData.getMaxFirmwareOSVersion());
            }
            if (watchAppData.getMinAppVersion() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, watchAppData.getMinAppVersion());
            }
            if (watchAppData.getMinFirmwareOSVersion() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, watchAppData.getMinFirmwareOSVersion());
            }
            if (watchAppData.getVersion() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, watchAppData.getVersion());
            }
            if (watchAppData.getExecutableBinaryDataUrl() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, watchAppData.getExecutableBinaryDataUrl());
            }
            ajVar.bindLong(9, WatchAppDataDao_Impl.this.__dateTimeConverter.a(watchAppData.getUpdatedAt()));
            ajVar.bindLong(10, WatchAppDataDao_Impl.this.__dateTimeConverter.a(watchAppData.getCreatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchAppData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchAppData WHERE id=?";
        }
    }

    @DexIgnore
    public WatchAppDataDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWatchAppData = new Anon1(ciVar);
        this.__preparedStmtOfClearAll = new Anon2(ciVar);
        this.__preparedStmtOfDeleteWatchAppDataById = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void deleteWatchAppDataById(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteWatchAppDataById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppDataById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public List<WatchAppData> getAllWatchAppData() {
        fi b = fi.b("SELECT * FROM watchAppData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "maxAppVersion");
            int b5 = oi.b(a, "maxFirmwareOSVersion");
            int b6 = oi.b(a, "minAppVersion");
            int b7 = oi.b(a, "minFirmwareOSVersion");
            int b8 = oi.b(a, "version");
            int b9 = oi.b(a, "executableBinaryDataUrl");
            int b10 = oi.b(a, "updatedAt");
            int b11 = oi.b(a, "createdAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchAppData(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), this.__dateTimeConverter.a(a.getLong(b10)), this.__dateTimeConverter.a(a.getLong(b11))));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppData(WatchAppData watchAppData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert(watchAppData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppDataList(List<WatchAppData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
