package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ee7;
import com.fossil.lf;
import com.fossil.pj4;
import com.fossil.te5;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataSourceFactory extends lf.b<Long, GoalTrackingData> {
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public GoalTrackingDataLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ te5.a mListener;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingDataLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, pj4 pj4, te5.a aVar) {
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        ee7.b(date, "currentDate");
        ee7.b(pj4, "mAppExecutors");
        ee7.b(aVar, "mListener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.currentDate = date;
        this.mAppExecutors = pj4;
        this.mListener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.lf.b
    public lf<Long, GoalTrackingData> create() {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = new GoalTrackingDataLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDatabase, this.currentDate, this.mAppExecutors, this.mListener);
        this.localDataSource = goalTrackingDataLocalDataSource;
        this.sourceLiveData.a(goalTrackingDataLocalDataSource);
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource2 = this.localDataSource;
        if (goalTrackingDataLocalDataSource2 != null) {
            return goalTrackingDataLocalDataSource2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDataLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingDataLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.localDataSource = goalTrackingDataLocalDataSource;
    }
}
