package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.zd7;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ QuickResponseMessageDao mQRMessageDao;
    @DexIgnore
    public /* final */ QuickResponseSenderDao mQuickResponseSenderDao;
    @DexIgnore
    public LiveData<List<QuickResponseMessage>> mResponseMessages;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return QuickResponseRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = QuickResponseRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "QuickResponseRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public QuickResponseRepository(QuickResponseMessageDao quickResponseMessageDao, QuickResponseSenderDao quickResponseSenderDao) {
        ee7.b(quickResponseMessageDao, "mQRMessageDao");
        ee7.b(quickResponseSenderDao, "mQuickResponseSenderDao");
        this.mQRMessageDao = quickResponseMessageDao;
        this.mQuickResponseSenderDao = quickResponseSenderDao;
        this.mResponseMessages = quickResponseMessageDao.getAllResponse();
    }

    @DexIgnore
    public final List<QuickResponseMessage> getAllQuickResponse() {
        return this.mQRMessageDao.getAllRawResponse();
    }

    @DexIgnore
    public final LiveData<List<QuickResponseMessage>> getAllQuickResponseLiveData() {
        return this.mResponseMessages;
    }

    @DexIgnore
    public final QuickResponseSender getQuickResponseSender(int i) {
        return this.mQuickResponseSenderDao.getQuickResponseSenderById(i);
    }

    @DexIgnore
    public final void insertQR(QuickResponseMessage quickResponseMessage) {
        ee7.b(quickResponseMessage, "qr");
        this.mQRMessageDao.insertResponse(quickResponseMessage);
    }

    @DexIgnore
    public final void insertQRs(List<QuickResponseMessage> list) {
        ee7.b(list, "qrs");
        this.mQRMessageDao.insertResponses(list);
    }

    @DexIgnore
    public final void removeAll() {
        this.mQRMessageDao.removeAll();
    }

    @DexIgnore
    public final void removeQRById(int i) {
        this.mQRMessageDao.removeById(i);
    }

    @DexIgnore
    public final void updateQR(QuickResponseMessage quickResponseMessage) {
        ee7.b(quickResponseMessage, "qr");
        this.mQRMessageDao.update(quickResponseMessage);
    }

    @DexIgnore
    public final long upsertQuickResponseSender(QuickResponseSender quickResponseSender) {
        ee7.b(quickResponseSender, "quickResponseSender");
        return this.mQuickResponseSenderDao.insertQuickResponseSender(quickResponseSender);
    }

    @DexIgnore
    public final void updateQR(List<QuickResponseMessage> list) {
        ee7.b(list, "qr");
        this.mQRMessageDao.update(list);
    }
}
