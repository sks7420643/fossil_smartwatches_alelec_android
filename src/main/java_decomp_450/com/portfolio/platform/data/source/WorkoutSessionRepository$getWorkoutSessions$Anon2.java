package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.pg5;
import com.fossil.qe7;
import com.fossil.qx6;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$2", f = "WorkoutSessionRepository.kt", l = {53}, m = "invokeSuspend")
public final class WorkoutSessionRepository$getWorkoutSessions$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<WorkoutSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository$getWorkoutSessions$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<WorkoutSession>, ApiResponse<ServerWorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ List $fitnessDataList;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, qe7 qe7, int i, List list) {
                this.this$0 = anon1_Level2;
                this.$offset = qe7;
                this.$limit = i;
                this.$fitnessDataList = list;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<ServerWorkoutSession>>> fb7) {
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                String g = zd5.g(this.this$0.$startDate);
                ee7.a((Object) g, "DateHelper.formatShortDate(startDate)");
                String g2 = zd5.g(this.this$0.$endDate);
                ee7.a((Object) g2, "DateHelper.formatShortDate(endDate)");
                return access$getMApiService$p.getWorkoutSessions(g, g2, this.$offset.element, this.$limit, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<List<WorkoutSession>>> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$0.this$0.$end + ", startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
                WorkoutDao workoutDao = this.this$0.$fitnessDatabase.getWorkoutDao();
                Date date = this.this$0.$startDate;
                ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
                Date date2 = this.this$0.$endDate;
                ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
                return workoutDao.getWorkoutSessionsDesc(date, date2);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, fb7 fb7) {
                return processContinueFetching(apiResponse, (fb7<? super Boolean>) fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, fb7<? super Boolean> fb7) {
                Boolean a;
                Range range = apiResponse.get_range();
                if (range == null || (a = pb7.a(range.isHasNext())) == null || !a.booleanValue()) {
                    return pb7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
                this.$offset.element += this.$limit;
                return pb7.a(true);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, fb7<? super i97> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getWorkoutSessions - saveCallResult -- item.size=");
                sb.append(apiResponse.get_items().size());
                sb.append(", hasNext=");
                Range range = apiResponse.get_range();
                sb.append(range != null ? pb7.a(range.isHasNext()) : null);
                local.d(tAG$app_fossilRelease, sb.toString());
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = apiResponse.get_items().iterator();
                while (it.hasNext()) {
                    WorkoutSession workoutSession = it.next().toWorkoutSession();
                    if (workoutSession != null) {
                        arrayList.add(workoutSession);
                    }
                }
                if (!apiResponse.get_items().isEmpty()) {
                    this.this$0.$fitnessDatabase.getWorkoutDao().upsertListWorkoutSession(arrayList);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(List<WorkoutSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$fitnessDataList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2, FitnessDatabase fitnessDatabase, Date date, Date date2) {
            this.this$0 = workoutSessionRepository$getWorkoutSessions$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
            this.$startDate = date;
            this.$endDate = date2;
        }

        @DexIgnore
        public final LiveData<qx6<List<WorkoutSession>>> apply(List<FitnessDataWrapper> list) {
            qe7 qe7 = new qe7();
            qe7.element = 0;
            return new Anon1_Level3(this, qe7, 100, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessions$Anon2(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2 = new WorkoutSessionRepository$getWorkoutSessions$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, fb7);
        workoutSessionRepository$getWorkoutSessions$Anon2.p$ = (yi7) obj;
        return workoutSessionRepository$getWorkoutSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<WorkoutSession>>>> fb7) {
        return ((WorkoutSessionRepository$getWorkoutSessions$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            date = zd5.q(this.$start);
            Date l = zd5.l(this.$end);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.L$1 = date;
            this.L$2 = l;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
            date2 = l;
        } else if (i == 1) {
            date2 = (Date) this.L$2;
            date = (Date) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) obj;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
        ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
        return ge.b(fitnessDataDao.getFitnessDataLiveData(date, date2), new Anon1_Level2(this, fitnessDatabase, date, date2));
    }
}
