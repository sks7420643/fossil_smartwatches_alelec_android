package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.nh7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ MicroAppDao mMicroAppDao;
    @DexIgnore
    public /* final */ MicroAppRemoteDataSource mMicroAppRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        ee7.b(microAppDao, "mMicroAppDao");
        ee7.b(microAppRemoteDataSource, "mMicroAppRemoteDataSource");
        ee7.b(portfolioApp, "mPortfolioApp");
        this.mMicroAppDao = microAppDao;
        this.mMicroAppRemoteDataSource = microAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppDao.clearAllDeclarationFileTable();
        this.mMicroAppDao.clearAllMicroAppGalleryTable();
        this.mMicroAppDao.clearAllMicroAppSettingTable();
        this.mMicroAppDao.clearAllMicroAppVariantTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadAllMicroApp(java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroApp>>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.MicroAppRepository$downloadAllMicroApp$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.MicroAppRepository$downloadAllMicroApp$Anon1 r0 = (com.portfolio.platform.data.source.MicroAppRepository$downloadAllMicroApp$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.MicroAppRepository$downloadAllMicroApp$Anon1 r0 = new com.portfolio.platform.data.source.MicroAppRepository$downloadAllMicroApp$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "downloadAllMicroApp - serial="
            r4 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r4) goto L_0x0033
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.MicroAppRepository r0 = (com.portfolio.platform.data.source.MicroAppRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x0068
        L_0x0033:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003b:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r3)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource r12 = r10.mMicroAppRemoteDataSource
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r4
            java.lang.Object r12 = r12.getAllMicroApp(r11, r0)
            if (r12 != r1) goto L_0x0067
            return r1
        L_0x0067:
            r0 = r10
        L_0x0068:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r1 = r12 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0102
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r3)
            r6.append(r11)
            java.lang.String r3 = " success isFromCache "
            r6.append(r3)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            boolean r3 = r12.b()
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            r1.d(r5, r3)
            java.lang.Object r1 = r12.a()
            if (r1 == 0) goto L_0x00fe
            java.util.List r1 = (java.util.List) r1
            com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao r3 = r0.mMicroAppDao
            java.util.List r11 = r3.getListMicroApp(r11)
            boolean r12 = r12.b()
            r3 = 2
            r5 = 0
            if (r12 != 0) goto L_0x00b4
            boolean r12 = r1.isEmpty()
            r12 = r12 ^ r4
            if (r12 != 0) goto L_0x00bb
        L_0x00b4:
            boolean r11 = com.fossil.ee7.a(r11, r1)
            r11 = r11 ^ r4
            if (r11 == 0) goto L_0x00f8
        L_0x00bb:
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r12 = r1.iterator()
        L_0x00c4:
            boolean r6 = r12.hasNext()
            if (r6 == 0) goto L_0x00ee
            java.lang.Object r6 = r12.next()
            r7 = r6
            com.portfolio.platform.data.model.room.microapp.MicroApp r7 = (com.portfolio.platform.data.model.room.microapp.MicroApp) r7
            com.fossil.ol4$a r8 = com.fossil.ol4.A
            java.lang.String r8 = r8.t()
            java.lang.String r7 = r7.getId()
            boolean r7 = com.fossil.nh7.a(r8, r7, r5, r3, r2)
            r7 = r7 ^ r4
            java.lang.Boolean r7 = com.fossil.pb7.a(r7)
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x00c4
            r11.add(r6)
            goto L_0x00c4
        L_0x00ee:
            com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao r12 = r0.mMicroAppDao
            r12.clearAllMicroApp()
            com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao r12 = r0.mMicroAppDao
            r12.upsertListMicroApp(r11)
        L_0x00f8:
            com.fossil.bj5 r11 = new com.fossil.bj5
            r11.<init>(r1, r5, r3, r2)
            goto L_0x0157
        L_0x00fe:
            com.fossil.ee7.a()
            throw r2
        L_0x0102:
            boolean r0 = r12 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0158
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            r4.append(r11)
            java.lang.String r11 = " failed!!! "
            r4.append(r11)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r11 = r12.a()
            r4.append(r11)
            java.lang.String r11 = " serverError="
            r4.append(r11)
            com.portfolio.platform.data.model.ServerError r11 = r12.c()
            if (r11 == 0) goto L_0x0136
            java.lang.Integer r2 = r11.getCode()
        L_0x0136:
            r4.append(r2)
            java.lang.String r11 = r4.toString()
            r0.d(r1, r11)
            com.fossil.yi5 r11 = new com.fossil.yi5
            int r3 = r12.a()
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            java.lang.Throwable r5 = r12.d()
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x0157:
            return r11
        L_0x0158:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.MicroAppRepository.downloadAllMicroApp(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadMicroAppVariant(java.lang.String r9, java.lang.String r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroAppVariant>>> r12) {
        /*
            r8 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.MicroAppRepository$downloadMicroAppVariant$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.MicroAppRepository$downloadMicroAppVariant$Anon1 r0 = (com.portfolio.platform.data.source.MicroAppRepository$downloadMicroAppVariant$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.MicroAppRepository$downloadMicroAppVariant$Anon1 r0 = new com.portfolio.platform.data.source.MicroAppRepository$downloadMicroAppVariant$Anon1
            r0.<init>(r8, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            java.lang.String r4 = "downloadMicroAppVariant - serial="
            if (r2 == 0) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r9 = r0.L$3
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.MicroAppRepository r10 = (com.portfolio.platform.data.source.MicroAppRepository) r10
            com.fossil.t87.a(r12)
            goto L_0x0084
        L_0x003b:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0043:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r4)
            r5.append(r9)
            java.lang.String r6 = " major "
            r5.append(r6)
            r5.append(r10)
            java.lang.String r6 = " minor "
            r5.append(r6)
            r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.d(r2, r5)
            com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource r12 = r8.mMicroAppRemoteDataSource
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.label = r3
            java.lang.Object r12 = r12.getAllMicroAppVariant(r9, r10, r11, r0)
            if (r12 != r1) goto L_0x0083
            return r1
        L_0x0083:
            r10 = r8
        L_0x0084:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r11 = r12 instanceof com.fossil.bj5
            r0 = 0
            if (r11 == 0) goto L_0x00f8
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r4)
            r2.append(r9)
            java.lang.String r9 = " success isFromCache "
            r2.append(r9)
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            boolean r9 = r12.b()
            r2.append(r9)
            java.lang.String r9 = r2.toString()
            r11.d(r1, r9)
            java.lang.Object r9 = r12.a()
            if (r9 == 0) goto L_0x00f4
            java.util.List r9 = (java.util.List) r9
            boolean r11 = r12.b()
            if (r11 != 0) goto L_0x00ec
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r12 = r9.iterator()
        L_0x00ca:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x00e2
            java.lang.Object r1 = r12.next()
            com.portfolio.platform.data.model.room.microapp.MicroAppVariant r1 = (com.portfolio.platform.data.model.room.microapp.MicroAppVariant) r1
            java.util.ArrayList r1 = r1.getDeclarationFileList()
            boolean r1 = r11.addAll(r1)
            com.fossil.pb7.a(r1)
            goto L_0x00ca
        L_0x00e2:
            com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao r12 = r10.mMicroAppDao
            r12.upsertMicroAppVariantList(r9)
            com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao r10 = r10.mMicroAppDao
            r10.upsertDeclarationFileList(r11)
        L_0x00ec:
            com.fossil.bj5 r10 = new com.fossil.bj5
            r11 = 0
            r12 = 2
            r10.<init>(r9, r11, r12, r0)
            goto L_0x014d
        L_0x00f4:
            com.fossil.ee7.a()
            throw r0
        L_0x00f8:
            boolean r10 = r12 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x014e
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.MicroAppRepository.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r4)
            r1.append(r9)
            java.lang.String r9 = " failed!!! "
            r1.append(r9)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r9 = r12.a()
            r1.append(r9)
            java.lang.String r9 = " serverError="
            r1.append(r9)
            com.portfolio.platform.data.model.ServerError r9 = r12.c()
            if (r9 == 0) goto L_0x012c
            java.lang.Integer r0 = r9.getCode()
        L_0x012c:
            r1.append(r0)
            java.lang.String r9 = r1.toString()
            r10.d(r11, r9)
            com.fossil.yi5 r10 = new com.fossil.yi5
            int r1 = r12.a()
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            java.lang.Throwable r3 = r12.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x014d:
            return r10
        L_0x014e:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.MicroAppRepository.downloadMicroAppVariant(java.lang.String, java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<MicroApp> getAllMicroApp(String str) {
        ee7.b(str, "serialNumber");
        return this.mMicroAppDao.getListMicroApp(str);
    }

    @DexIgnore
    public final List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        ee7.b(list, "ids");
        ee7.b(str, "serialNumber");
        return this.mMicroAppDao.getMicroAppByIds(list, str);
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, String str3, int i) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "microAppId");
        ee7.b(str3, "variantName");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    public final List<MicroApp> queryMicroAppByName(String str, String str2) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ee7.b(str2, "serialNumber");
        ArrayList arrayList = new ArrayList();
        for (MicroApp microApp : this.mMicroAppDao.getListMicroApp(str2)) {
            String normalize = Normalizer.normalize(ig5.a(this.mPortfolioApp, microApp.getNameKey(), microApp.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            ee7.a((Object) normalize, "name");
            ee7.a((Object) normalize2, "searchQuery");
            if (nh7.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(microApp);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "microAppId");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }
}
