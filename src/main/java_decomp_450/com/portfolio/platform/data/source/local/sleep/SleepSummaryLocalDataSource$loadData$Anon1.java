package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.yi7;
import com.fossil.zb7;
import java.util.Date;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1", f = "SleepSummaryLocalDataSource.kt", l = {DateTimeConstants.HOURS_PER_WEEK, 179, 179}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ te5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ te5.d $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, Date date, Date date2, te5.d dVar, te5.b.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = dVar;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, fb7);
        sleepSummaryLocalDataSource$loadData$Anon1.p$ = (yi7) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x010c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x010d  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r14.label
            r2 = 3
            r3 = 2
            r4 = 1
            if (r1 == 0) goto L_0x0067
            if (r1 == r4) goto L_0x005f
            if (r1 == r3) goto L_0x003e
            if (r1 != r2) goto L_0x0036
            java.lang.Object r0 = r14.L$7
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r14.L$6
            com.fossil.te5$d r1 = (com.fossil.te5.d) r1
            java.lang.Object r2 = r14.L$5
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource r2 = (com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource) r2
            java.lang.Object r3 = r14.L$4
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r3 = r14.L$3
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r3 = r14.L$2
            com.fossil.r87 r3 = (com.fossil.r87) r3
            java.lang.Object r3 = r14.L$1
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r3 = r14.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r15)
            goto L_0x0110
        L_0x0036:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r15.<init>(r0)
            throw r15
        L_0x003e:
            java.lang.Object r1 = r14.L$6
            com.fossil.te5$d r1 = (com.fossil.te5.d) r1
            java.lang.Object r3 = r14.L$5
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource r3 = (com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource) r3
            java.lang.Object r4 = r14.L$4
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r5 = r14.L$3
            com.fossil.hj7 r5 = (com.fossil.hj7) r5
            java.lang.Object r6 = r14.L$2
            com.fossil.r87 r6 = (com.fossil.r87) r6
            java.lang.Object r7 = r14.L$1
            java.util.List r7 = (java.util.List) r7
            java.lang.Object r8 = r14.L$0
            com.fossil.yi7 r8 = (com.fossil.yi7) r8
            com.fossil.t87.a(r15)
            goto L_0x00f2
        L_0x005f:
            java.lang.Object r1 = r14.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r15)
            goto L_0x00ac
        L_0x0067:
            com.fossil.t87.a(r15)
            com.fossil.yi7 r15 = r14.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "loadData start="
            r5.append(r6)
            java.util.Date r6 = r14.$startDate
            r5.append(r6)
            java.lang.String r6 = ", end="
            r5.append(r6)
            java.util.Date r6 = r14.$endDate
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "SleepSummaryLocalDataSource"
            r1.d(r6, r5)
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource r1 = r14.this$0
            com.portfolio.platform.data.source.FitnessDataRepository r1 = r1.mFitnessDataRepository
            java.util.Date r5 = r14.$startDate
            java.util.Date r6 = r14.$endDate
            r14.L$0 = r15
            r14.label = r4
            java.lang.Object r1 = r1.getFitnessData(r5, r6, r14)
            if (r1 != r0) goto L_0x00a9
            return r0
        L_0x00a9:
            r13 = r1
            r1 = r15
            r15 = r13
        L_0x00ac:
            java.util.List r15 = (java.util.List) r15
            java.util.Date r4 = r14.$startDate
            java.util.Date r5 = r14.$endDate
            com.fossil.r87 r10 = com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(r15, r4, r5)
            if (r10 == 0) goto L_0x0118
            r5 = 0
            r6 = 0
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2 r7 = new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2
            r11 = 0
            r7.<init>(r14, r10, r11)
            r8 = 3
            r9 = 0
            r4 = r1
            com.fossil.hj7 r12 = com.fossil.xh7.a(r4, r5, r6, r7, r8, r9)
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 r7 = new com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2
            r7.<init>(r14, r10, r11)
            com.fossil.hj7 r4 = com.fossil.xh7.a(r4, r5, r6, r7, r8, r9)
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource r5 = r14.this$0
            com.fossil.te5$d r6 = r14.$requestType
            r14.L$0 = r1
            r14.L$1 = r15
            r14.L$2 = r10
            r14.L$3 = r12
            r14.L$4 = r4
            r14.L$5 = r5
            r14.L$6 = r6
            r14.label = r3
            java.lang.Object r3 = r12.c(r14)
            if (r3 != r0) goto L_0x00eb
            return r0
        L_0x00eb:
            r7 = r15
            r8 = r1
            r15 = r3
            r3 = r5
            r1 = r6
            r6 = r10
            r5 = r12
        L_0x00f2:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            r14.L$0 = r8
            r14.L$1 = r7
            r14.L$2 = r6
            r14.L$3 = r5
            r14.L$4 = r4
            r14.L$5 = r3
            r14.L$6 = r1
            r14.L$7 = r15
            r14.label = r2
            java.lang.Object r2 = r4.c(r14)
            if (r2 != r0) goto L_0x010d
            return r0
        L_0x010d:
            r0 = r15
            r15 = r2
            r2 = r3
        L_0x0110:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            com.fossil.te5$b$a r3 = r14.$helperCallback
            r2.combineData(r1, r0, r15, r3)
            goto L_0x011d
        L_0x0118:
            com.fossil.te5$b$a r15 = r14.$helperCallback
            r15.a()
        L_0x011d:
            com.fossil.i97 r15 = com.fossil.i97.a
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
