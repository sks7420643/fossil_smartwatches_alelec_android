package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.ThirdPartyRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$pushPendingData$2", f = "ThirdPartyRepository.kt", l = {86}, m = "invokeSuspend")
public final class ThirdPartyRepository$pushPendingData$Anon2 extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$pushPendingData$Anon2(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, fb7 fb7) {
        super(2, fb7);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ThirdPartyRepository$pushPendingData$Anon2 thirdPartyRepository$pushPendingData$Anon2 = new ThirdPartyRepository$pushPendingData$Anon2(this.this$0, this.$pushPendingThirdPartyDataCallback, fb7);
        thirdPartyRepository$pushPendingData$Anon2.p$ = (yi7) obj;
        return thirdPartyRepository$pushPendingData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
        return ((ThirdPartyRepository$pushPendingData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ThirdPartyRepository thirdPartyRepository = this.this$0;
            ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback = this.$pushPendingThirdPartyDataCallback;
            this.L$0 = yi7;
            this.label = 1;
            obj = thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
