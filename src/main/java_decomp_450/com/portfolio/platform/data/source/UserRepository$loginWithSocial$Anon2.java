package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.za5;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.AuthKt;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$loginWithSocial$2", f = "UserRepository.kt", l = {166, 180}, m = "invokeSuspend")
public final class UserRepository$loginWithSocial$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser.Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $clientId;
    @DexIgnore
    public /* final */ /* synthetic */ String $service;
    @DexIgnore
    public /* final */ /* synthetic */ String $token;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$loginWithSocial$Anon2(UserRepository userRepository, String str, String str2, String str3, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
        this.$service = str;
        this.$token = str2;
        this.$clientId = str3;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$loginWithSocial$Anon2 userRepository$loginWithSocial$Anon2 = new UserRepository$loginWithSocial$Anon2(this.this$0, this.$service, this.$token, this.$clientId, fb7);
        userRepository$loginWithSocial$Anon2.p$ = (yi7) obj;
        return userRepository$loginWithSocial$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return ((UserRepository$loginWithSocial$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Auth auth;
        Object obj2;
        MFUser mFUser;
        Object obj3;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            UserRemoteDataSource access$getMUserRemoteDataSource$p = this.this$0.mUserRemoteDataSource;
            String str2 = this.$service;
            String str3 = this.$token;
            String str4 = this.$clientId;
            this.L$0 = yi7;
            this.label = 1;
            obj3 = access$getMUserRemoteDataSource$p.loginWithSocial(str2, str3, str4, this);
            if (obj3 == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
            obj3 = obj;
        } else if (i == 2) {
            mFUser = (MFUser) this.L$3;
            zi5 zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            auth = (Auth) this.L$2;
            obj2 = obj;
            ((UserDatabase) obj2).userDao().insertUser(mFUser);
            this.this$0.mSharedPreferencesManager.y(auth.getAccessToken());
            this.this$0.mSharedPreferencesManager.a(System.currentTimeMillis());
            return new bj5(mFUser.getAuth(), false, 2, null);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj3;
        if (zi52 instanceof bj5) {
            auth = (Auth) ((bj5) zi52).a();
            if (auth != null) {
                String uid = auth.getUid();
                if (uid != null) {
                    MFUser mFUser2 = new MFUser("", uid);
                    String str5 = this.$service;
                    switch (str5.hashCode()) {
                        case -1240244679:
                            if (str5.equals("google")) {
                                String value = za5.GOOGLE.getValue();
                                ee7.a((Object) value, "AuthType.GOOGLE.value");
                                mFUser2.setAuthType(value);
                                break;
                            }
                            break;
                        case -791770330:
                            if (str5.equals("wechat")) {
                                String value2 = za5.WECHAT.getValue();
                                ee7.a((Object) value2, "AuthType.WECHAT.value");
                                mFUser2.setAuthType(value2);
                                break;
                            }
                            break;
                        case 113011944:
                            if (str5.equals("weibo")) {
                                String value3 = za5.WEIBO.getValue();
                                ee7.a((Object) value3, "AuthType.WEIBO.value");
                                mFUser2.setAuthType(value3);
                                break;
                            }
                            break;
                        case 497130182:
                            if (str5.equals(Constants.FACEBOOK)) {
                                String value4 = za5.FACEBOOK.getValue();
                                ee7.a((Object) value4, "AuthType.FACEBOOK.value");
                                mFUser2.setAuthType(value4);
                                break;
                            }
                            break;
                    }
                    MFUser.Auth auth2 = AuthKt.toAuth(auth);
                    if (auth2 != null) {
                        mFUser2.setAuth(auth2);
                        String uid2 = auth.getUid();
                        if (uid2 != null) {
                            mFUser2.setUserId(uid2);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String access$getTAG$cp = UserRepository.TAG;
                            local.d(access$getTAG$cp, "loginSocial user " + mFUser2);
                            pg5 pg5 = pg5.i;
                            this.L$0 = yi7;
                            this.L$1 = zi52;
                            this.L$2 = auth;
                            this.L$3 = mFUser2;
                            this.label = 2;
                            obj2 = pg5.f(this);
                            if (obj2 == a) {
                                return a;
                            }
                            mFUser = mFUser2;
                            ((UserDatabase) obj2).userDao().insertUser(mFUser);
                            this.this$0.mSharedPreferencesManager.y(auth.getAccessToken());
                            this.this$0.mSharedPreferencesManager.a(System.currentTimeMillis());
                            return new bj5(mFUser.getAuth(), false, 2, null);
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        } else if (!(zi52 instanceof yi5)) {
            return new yi5(600, new ServerError(), null, null, null, 24, null);
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp2 = UserRepository.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("loginWithSocial Failure error=");
            yi5 yi5 = (yi5) zi52;
            sb.append(yi5.a());
            sb.append(" message=");
            ServerError c = yi5.c();
            if (c != null) {
                str = c.getMessage();
            }
            sb.append(str);
            local2.d(access$getTAG$cp2, sb.toString());
            return new yi5(yi5.a(), yi5.c(), yi5.d(), null, null, 24, null);
        }
    }
}
