package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public List<SleepSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = SleepSummariesRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "SleepSummariesRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSummariesRepository(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SleepSummariesRepository$cleanUp$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object fetchLastSleepGoal(fb7<? super zi5<MFSleepSettings>> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$fetchLastSleepGoal$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object fetchSleepSummaries(Date date, Date date2, fb7<? super zi5<ie4>> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$fetchSleepSummaries$Anon2(this, date, date2, null), fb7);
    }

    @DexIgnore
    public final Object getCurrentLastSleepGoal(fb7<? super Integer> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fd A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLastSleepGoal(com.fossil.fb7<? super java.lang.Integer> r12) {
        /*
            r11 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$Anon1
            r0.<init>(r11, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L_0x0055
            if (r2 == r5) goto L_0x004d
            if (r2 == r4) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r1 = r0.L$1
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00fe
        L_0x0039:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0041:
            java.lang.Object r2 = r0.L$1
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r4 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r4 = (com.portfolio.platform.data.source.SleepSummariesRepository) r4
            com.fossil.t87.a(r12)
            goto L_0x00af
        L_0x004d:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r2 = (com.portfolio.platform.data.source.SleepSummariesRepository) r2
            com.fossil.t87.a(r12)
            goto L_0x0069
        L_0x0055:
            com.fossil.t87.a(r12)
            com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$response$Anon1 r12 = new com.portfolio.platform.data.source.SleepSummariesRepository$getLastSleepGoal$response$Anon1
            r12.<init>(r11, r6)
            r0.L$0 = r11
            r0.label = r5
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0068
            return r1
        L_0x0068:
            r2 = r11
        L_0x0069:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r5 = r12 instanceof com.fossil.bj5
            if (r5 == 0) goto L_0x00b6
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "getLastSleepGoal success: "
            r8.append(r9)
            r9 = r12
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r10 = r9.a()
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r10 = (com.portfolio.platform.data.model.room.sleep.MFSleepSettings) r10
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r5.d(r7, r8)
            java.lang.Object r5 = r9.a()
            if (r5 == 0) goto L_0x00b2
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r5 = (com.portfolio.platform.data.model.room.sleep.MFSleepSettings) r5
            int r5 = r5.getSleepGoal()
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r4
            java.lang.Object r4 = r2.saveSleepSettingToDB(r5, r0)
            if (r4 != r1) goto L_0x00ad
            return r1
        L_0x00ad:
            r4 = r2
            r2 = r12
        L_0x00af:
            r12 = r2
            r2 = r4
            goto L_0x00ef
        L_0x00b2:
            com.fossil.ee7.a()
            throw r6
        L_0x00b6:
            boolean r4 = r12 instanceof com.fossil.yi5
            if (r4 == 0) goto L_0x00ef
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "getLastSleepGoal fail: code="
            r7.append(r8)
            r8 = r12
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r9 = r8.a()
            r7.append(r9)
            java.lang.String r9 = " message="
            r7.append(r9)
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00e5
            java.lang.String r6 = r8.getMessage()
        L_0x00e5:
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            r4.d(r5, r6)
        L_0x00ef:
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r3
            java.lang.Object r12 = r4.d(r0)
            if (r12 != r1) goto L_0x00fe
            return r1
        L_0x00fe:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r12 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r12
            com.portfolio.platform.data.source.local.sleep.SleepDao r12 = r12.sleepDao()
            java.lang.Integer r12 = r12.getLastSleepGoal()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getLastSleepGoal(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSleepStatistic(boolean z, fb7<? super LiveData<qx6<SleepStatistic>>> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepStatistic - shouldFetch=" + z);
        return new SleepSummariesRepository$getSleepStatistic$Anon2(this, z).asLiveData();
    }

    @DexIgnore
    public final Object getSleepStatisticAwait(fb7<? super SleepStatistic> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$getSleepStatisticAwait$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final /* synthetic */ Object getSleepStatisticDB(fb7<? super SleepStatistic> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$getSleepStatisticDB$Anon2(null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r11 = (com.portfolio.platform.data.source.SleepSummariesRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSleepSummaries(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSummary(java.util.Date r6, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<com.portfolio.platform.data.model.room.sleep.MFSleepDay>>> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon1
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.util.Date r6 = (java.util.Date) r6
            java.lang.Object r6 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r6 = (com.portfolio.platform.data.source.SleepSummariesRepository) r6
            com.fossil.t87.a(r7)
            goto L_0x0053
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.tk7 r7 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$Anon2
            r4 = 0
            r2.<init>(r5, r6, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x0053
            return r1
        L_0x0053:
            java.lang.String r6 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r7, r6)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSleepSummary(java.util.Date, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSleepSummaryFromDb(Date date, fb7<? super MFSleepDay> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$getSleepSummaryFromDb$Anon2(date, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummariesPaging(com.portfolio.platform.data.source.SleepSessionsRepository r19, com.portfolio.platform.data.source.FitnessDataRepository r20, java.util.Date r21, com.fossil.pj4 r22, com.fossil.te5.a r23, com.fossil.fb7<? super com.portfolio.platform.data.Listing<com.portfolio.platform.data.SleepSummary>> r24) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r24
            boolean r5 = r4 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon1
            if (r5 == 0) goto L_0x001d
            r5 = r4
            com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon1 r5 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon1) r5
            int r6 = r5.label
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = r6 & r7
            if (r8 == 0) goto L_0x001d
            int r6 = r6 - r7
            r5.label = r6
            goto L_0x0022
        L_0x001d:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon1 r5 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon1
            r5.<init>(r0, r4)
        L_0x0022:
            java.lang.Object r4 = r5.result
            java.lang.Object r6 = com.fossil.nb7.a()
            int r7 = r5.label
            java.lang.String r8 = "calendar"
            r9 = 1
            if (r7 == 0) goto L_0x0071
            if (r7 != r9) goto L_0x0069
            java.lang.Object r1 = r5.L$10
            com.portfolio.platform.data.source.FitnessDataRepository r1 = (com.portfolio.platform.data.source.FitnessDataRepository) r1
            java.lang.Object r2 = r5.L$9
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = (com.portfolio.platform.data.source.SleepSessionsRepository) r2
            java.lang.Object r3 = r5.L$8
            com.portfolio.platform.data.source.SleepSummariesRepository r3 = (com.portfolio.platform.data.source.SleepSummariesRepository) r3
            java.lang.Object r6 = r5.L$7
            java.util.Calendar r6 = (java.util.Calendar) r6
            java.lang.Object r7 = r5.L$6
            java.util.Date r7 = (java.util.Date) r7
            java.lang.Object r7 = r5.L$5
            com.fossil.te5$a r7 = (com.fossil.te5.a) r7
            java.lang.Object r9 = r5.L$4
            com.fossil.pj4 r9 = (com.fossil.pj4) r9
            java.lang.Object r10 = r5.L$3
            java.util.Date r10 = (java.util.Date) r10
            java.lang.Object r11 = r5.L$2
            com.portfolio.platform.data.source.FitnessDataRepository r11 = (com.portfolio.platform.data.source.FitnessDataRepository) r11
            java.lang.Object r11 = r5.L$1
            com.portfolio.platform.data.source.SleepSessionsRepository r11 = (com.portfolio.platform.data.source.SleepSessionsRepository) r11
            java.lang.Object r5 = r5.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r5 = (com.portfolio.platform.data.source.SleepSummariesRepository) r5
            com.fossil.t87.a(r4)
            r12 = r1
            r11 = r2
            r16 = r7
            r15 = r9
            r14 = r10
            r10 = r3
            goto L_0x00df
        L_0x0069:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0071:
            com.fossil.t87.a(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "getSummariesPaging - createdDate="
            r10.append(r11)
            r10.append(r3)
            java.lang.String r10 = r10.toString()
            r4.d(r7, r10)
            com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$Companion r4 = com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource.Companion
            java.util.Calendar r7 = java.util.Calendar.getInstance()
            java.lang.String r10 = "Calendar.getInstance()"
            com.fossil.ee7.a(r7, r10)
            java.util.Date r7 = r7.getTime()
            java.lang.String r10 = "Calendar.getInstance().time"
            com.fossil.ee7.a(r7, r10)
            java.util.Date r4 = r4.calculateNextKey(r7, r3)
            java.util.Calendar r7 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r7, r8)
            r7.setTime(r4)
            com.fossil.pg5 r10 = com.fossil.pg5.i
            r5.L$0 = r0
            r5.L$1 = r1
            r5.L$2 = r2
            r5.L$3 = r3
            r11 = r22
            r5.L$4 = r11
            r12 = r23
            r5.L$5 = r12
            r5.L$6 = r4
            r5.L$7 = r7
            r5.L$8 = r0
            r5.L$9 = r1
            r5.L$10 = r2
            r5.label = r9
            java.lang.Object r4 = r10.d(r5)
            if (r4 != r6) goto L_0x00d7
            return r6
        L_0x00d7:
            r10 = r0
            r14 = r3
            r6 = r7
            r15 = r11
            r16 = r12
            r11 = r1
            r12 = r2
        L_0x00df:
            r13 = r4
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r13 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r13
            com.fossil.ee7.a(r6, r8)
            com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory r1 = new com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory
            r9 = r1
            r17 = r6
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            com.fossil.qf$f$a r2 = new com.fossil.qf$f$a
            r2.<init>()
            r3 = 30
            r2.a(r3)
            r4 = 0
            r2.a(r4)
            r2.b(r3)
            r3 = 5
            r2.c(r3)
            com.fossil.qf$f r2 = r2.a()
            java.lang.String r3 = "PagedList.Config.Builder\u2026\n                .build()"
            com.fossil.ee7.a(r2, r3)
            com.fossil.nf r3 = new com.fossil.nf
            r3.<init>(r1, r2)
            androidx.lifecycle.LiveData r2 = r3.a()
            java.lang.String r3 = "LivePagedListBuilder(sou\u2026eFactory, config).build()"
            com.fossil.ee7.a(r2, r3)
            com.portfolio.platform.data.Listing r3 = new com.portfolio.platform.data.Listing
            androidx.lifecycle.MutableLiveData r4 = r1.getSourceLiveData()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon2 r5 = com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon2.INSTANCE
            androidx.lifecycle.LiveData r4 = com.fossil.ge.b(r4, r5)
            java.lang.String r5 = "Transformations.switchMa\u2026rkState\n                }"
            com.fossil.ee7.a(r4, r5)
            com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon3 r5 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon3
            r5.<init>(r1)
            com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon4 r6 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSummariesPaging$Anon4
            r6.<init>(r1)
            r3.<init>(r2, r4, r5, r6)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.getSummariesPaging(com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.FitnessDataRepository, java.util.Date, com.fossil.pj4, com.fossil.te5$a, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushLastSleepGoalToServer(int i, fb7<? super zi5<MFSleepSettings>> fb7) {
        return vh7.a(qj7.b(), new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this, i, null), fb7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory : this.mSourceFactoryList) {
            SleepSummaryLocalDataSource localSource = sleepSummaryDataSourceFactory.getLocalSource();
            if (localSource != null) {
                localSource.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSettingToDB(int r12, com.fossil.fb7<? super com.fossil.i97> r13) {
        /*
            r11 = this;
            boolean r0 = r13 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSettingToDB$Anon1
            r0.<init>(r11, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0037
            if (r2 != r3) goto L_0x002f
            int r12 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.t87.a(r13)
            goto L_0x0065
        L_0x002f:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0037:
            com.fossil.t87.a(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "save sleep goal "
            r4.append(r5)
            r4.append(r12)
            java.lang.String r4 = r4.toString()
            r13.d(r2, r4)
            com.fossil.pg5 r13 = com.fossil.pg5.i
            r0.L$0 = r11
            r0.I$0 = r12
            r0.label = r3
            java.lang.Object r13 = r13.d(r0)
            if (r13 != r1) goto L_0x0065
            return r1
        L_0x0065:
            r6 = r12
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r13 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r13
            com.portfolio.platform.data.source.local.sleep.SleepDao r12 = r13.sleepDao()
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r13 = r12.getSleepSettings()
            if (r13 != 0) goto L_0x007b
            com.portfolio.platform.data.model.room.sleep.MFSleepSettings r13 = new com.portfolio.platform.data.model.room.sleep.MFSleepSettings
            r13.<init>(r6)
            r13.setId(r3)
            goto L_0x007e
        L_0x007b:
            r13.setSleepGoal(r6)
        L_0x007e:
            r12.upsertSleepSettings(r13)
            java.util.Date r13 = new java.util.Date
            r13.<init>()
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r13 = r12.getSleepDay(r13)
            if (r13 != 0) goto L_0x00c1
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r8 = new com.portfolio.platform.data.model.room.sleep.SleepDistribution
            r13 = 0
            r8.<init>(r13, r13, r13)
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r13 = new com.portfolio.platform.data.model.room.sleep.MFSleepDay
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            r7 = 0
            org.joda.time.DateTime r9 = new org.joda.time.DateTime
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r0, r1)
            long r2 = r0.getTimeInMillis()
            r9.<init>(r2)
            org.joda.time.DateTime r10 = new org.joda.time.DateTime
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r0, r1)
            long r0 = r0.getTimeInMillis()
            r10.<init>(r0)
            r4 = r13
            r4.<init>(r5, r6, r7, r8, r9, r10)
            goto L_0x00c4
        L_0x00c1:
            r13.setGoalMinutes(r6)
        L_0x00c4:
            r12.upsertSleepDay(r13)
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSettingToDB(int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSummaries(com.fossil.ie4 r11, java.util.Date r12, java.util.Date r13, com.fossil.r87<? extends java.util.Date, ? extends java.util.Date> r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r10 = this;
            boolean r0 = r15 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon1
            r0.<init>(r10, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0073
            if (r2 == r4) goto L_0x0051
            if (r2 != r3) goto L_0x0049
            java.lang.Object r11 = r0.L$6
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$5
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            java.lang.Object r12 = r0.L$4
            com.fossil.r87 r12 = (com.fossil.r87) r12
            java.lang.Object r12 = r0.L$3
            java.util.Date r12 = (java.util.Date) r12
            java.lang.Object r12 = r0.L$2
            java.util.Date r12 = (java.util.Date) r12
            java.lang.Object r12 = r0.L$1
            com.fossil.ie4 r12 = (com.fossil.ie4) r12
            java.lang.Object r12 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r12 = (com.portfolio.platform.data.source.SleepSummariesRepository) r12
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x013f }
            goto L_0x0135
        L_0x0049:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0051:
            java.lang.Object r11 = r0.L$5
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            java.lang.Object r12 = r0.L$4
            r14 = r12
            com.fossil.r87 r14 = (com.fossil.r87) r14
            java.lang.Object r12 = r0.L$3
            r13 = r12
            java.util.Date r13 = (java.util.Date) r13
            java.lang.Object r12 = r0.L$2
            java.util.Date r12 = (java.util.Date) r12
            java.lang.Object r2 = r0.L$1
            com.fossil.ie4 r2 = (com.fossil.ie4) r2
            java.lang.Object r4 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r4 = (com.portfolio.platform.data.source.SleepSummariesRepository) r4
            com.fossil.t87.a(r15)
            r9 = r15
            r15 = r11
            r11 = r2
            r2 = r9
            goto L_0x00d1
        L_0x0073:
            com.fossil.t87.a(r15)
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.google.gson.Gson r2 = r2.a()
            java.lang.String r5 = r11.toString()
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon2 r6 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummaries$Anon2
            r6.<init>()
            java.lang.reflect.Type r6 = r6.getType()
            java.lang.Object r2 = r2.a(r5, r6)
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            if (r2 == 0) goto L_0x00b9
            java.util.List r2 = r2.get_items()
            if (r2 == 0) goto L_0x00b9
            java.util.Iterator r2 = r2.iterator()
        L_0x00a3:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x00b9
            java.lang.Object r5 = r2.next()
            com.portfolio.platform.response.sleep.SleepDayParse r5 = (com.portfolio.platform.response.sleep.SleepDayParse) r5
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r5 = r5.getMFSleepBySleepDayParse()
            if (r5 == 0) goto L_0x00a3
            r15.add(r5)
            goto L_0x00a3
        L_0x00b9:
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r13
            r0.L$4 = r14
            r0.L$5 = r15
            r0.label = r4
            java.lang.Object r2 = r2.b(r0)
            if (r2 != r1) goto L_0x00d0
            return r1
        L_0x00d0:
            r4 = r10
        L_0x00d1:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.FitnessDataDao r2 = r2.getFitnessDataDao()
            if (r14 == 0) goto L_0x00e2
            java.lang.Object r5 = r14.getFirst()
            java.util.Date r5 = (java.util.Date) r5
            if (r5 == 0) goto L_0x00e2
            goto L_0x00e3
        L_0x00e2:
            r5 = r12
        L_0x00e3:
            if (r14 == 0) goto L_0x00ee
            java.lang.Object r6 = r14.getSecond()
            java.util.Date r6 = (java.util.Date) r6
            if (r6 == 0) goto L_0x00ee
            goto L_0x00ef
        L_0x00ee:
            r6 = r13
        L_0x00ef:
            java.util.List r2 = r2.getFitnessData(r5, r6)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fitnessDatasSize "
            r7.append(r8)
            int r8 = r2.size()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            boolean r5 = r2.isEmpty()
            if (r5 == 0) goto L_0x0161
            com.fossil.pg5 r5 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r13
            r0.L$4 = r14
            r0.L$5 = r15
            r0.L$6 = r2
            r0.label = r3
            java.lang.Object r11 = r5.d(r0)
            if (r11 != r1) goto L_0x0132
            return r1
        L_0x0132:
            r9 = r15
            r15 = r11
            r11 = r9
        L_0x0135:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r15 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r15
            com.portfolio.platform.data.source.local.sleep.SleepDao r12 = r15.sleepDao()
            r12.upsertSleepDays(r11)
            goto L_0x0161
        L_0x013f:
            r11 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "getSleepSummaries saveCallResult exception="
            r14.append(r15)
            r11.printStackTrace()
            com.fossil.i97 r11 = com.fossil.i97.a
            r14.append(r11)
            java.lang.String r11 = r14.toString()
            r12.e(r13, r11)
        L_0x0161:
            com.fossil.i97 r11 = com.fossil.i97.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSummaries(com.fossil.ie4, java.util.Date, java.util.Date, com.fossil.r87, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0106 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSummary(com.fossil.ie4 r12, java.util.Date r13, com.fossil.fb7<? super com.fossil.i97> r14) {
        /*
            r11 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon1
            r0.<init>(r11, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x005d
            if (r2 == r4) goto L_0x0049
            if (r2 != r3) goto L_0x0041
            java.lang.Object r12 = r0.L$4
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r12 = r0.L$3
            java.util.ArrayList r12 = (java.util.ArrayList) r12
            java.lang.Object r13 = r0.L$2
            java.util.Date r13 = (java.util.Date) r13
            java.lang.Object r13 = r0.L$1
            com.fossil.ie4 r13 = (com.fossil.ie4) r13
            java.lang.Object r13 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            com.fossil.t87.a(r14)     // Catch:{ Exception -> 0x011d }
            goto L_0x0107
        L_0x0041:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0049:
            java.lang.Object r12 = r0.L$3
            java.util.ArrayList r12 = (java.util.ArrayList) r12
            java.lang.Object r13 = r0.L$2
            java.util.Date r13 = (java.util.Date) r13
            java.lang.Object r2 = r0.L$1
            com.fossil.ie4 r2 = (com.fossil.ie4) r2
            java.lang.Object r5 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r5 = (com.portfolio.platform.data.source.SleepSummariesRepository) r5
            com.fossil.t87.a(r14)
            goto L_0x00bb
        L_0x005d:
            com.fossil.t87.a(r14)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.google.gson.Gson r2 = r2.a()
            java.lang.String r5 = r12.toString()
            com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon2 r6 = new com.portfolio.platform.data.source.SleepSummariesRepository$saveSleepSummary$Anon2
            r6.<init>()
            java.lang.reflect.Type r6 = r6.getType()
            java.lang.Object r2 = r2.a(r5, r6)
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            if (r2 == 0) goto L_0x00a3
            java.util.List r2 = r2.get_items()
            if (r2 == 0) goto L_0x00a3
            java.util.Iterator r2 = r2.iterator()
        L_0x008d:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x00a3
            java.lang.Object r5 = r2.next()
            com.portfolio.platform.response.sleep.SleepDayParse r5 = (com.portfolio.platform.response.sleep.SleepDayParse) r5
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r5 = r5.getMFSleepBySleepDayParse()
            if (r5 == 0) goto L_0x008d
            r14.add(r5)
            goto L_0x008d
        L_0x00a3:
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.L$3 = r14
            r0.label = r4
            java.lang.Object r2 = r2.b(r0)
            if (r2 != r1) goto L_0x00b6
            return r1
        L_0x00b6:
            r5 = r11
            r10 = r2
            r2 = r12
            r12 = r14
            r14 = r10
        L_0x00bb:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r14 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r14
            com.portfolio.platform.data.source.local.FitnessDataDao r14 = r14.getFitnessDataDao()
            java.util.List r14 = r14.getFitnessData(r13, r13)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "fitnessDatasSize "
            r8.append(r9)
            int r9 = r14.size()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            boolean r6 = r12.isEmpty()
            r4 = r4 ^ r6
            if (r4 == 0) goto L_0x013f
            boolean r4 = r14.isEmpty()
            if (r4 == 0) goto L_0x013f
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r0.L$0 = r5
            r0.L$1 = r2
            r0.L$2 = r13
            r0.L$3 = r12
            r0.L$4 = r14
            r0.label = r3
            java.lang.Object r14 = r4.d(r0)
            if (r14 != r1) goto L_0x0107
            return r1
        L_0x0107:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r14 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r14
            com.portfolio.platform.data.source.local.sleep.SleepDao r13 = r14.sleepDao()
            r14 = 0
            java.lang.Object r12 = r12.get(r14)
            java.lang.String r14 = "summaryList[0]"
            com.fossil.ee7.a(r12, r14)
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r12 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r12
            r13.upsertSleepDay(r12)
            goto L_0x013f
        L_0x011d:
            r12 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "getSleepSummary exception="
            r0.append(r1)
            r12.printStackTrace()
            com.fossil.i97 r12 = com.fossil.i97.a
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            r13.d(r14, r12)
        L_0x013f:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.saveSleepSummary(com.fossil.ie4, java.util.Date, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateLastSleepGoal(int r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.room.sleep.MFSleepSettings>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x0056
            if (r2 == r5) goto L_0x0044
            if (r2 != r4) goto L_0x003c
            java.lang.Object r11 = r0.L$2
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            java.lang.Object r1 = r0.L$1
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            int r1 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r0 = (com.portfolio.platform.data.source.SleepSummariesRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00e4
        L_0x003c:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0044:
            java.lang.Object r11 = r0.L$1
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            int r2 = r0.I$0
            java.lang.Object r5 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository r5 = (com.portfolio.platform.data.source.SleepSummariesRepository) r5
            com.fossil.t87.a(r12)
            r9 = r12
            r12 = r11
            r11 = r2
            r2 = r9
            goto L_0x00b0
        L_0x0056:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "updateLastSleepGoal sleepGoal="
            r6.append(r7)
            r6.append(r11)
            java.lang.String r6 = r6.toString()
            r12.d(r2, r6)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.Integer r2 = com.fossil.pb7.a(r11)
            java.lang.String r6 = "currentGoalMinutes"
            r12.a(r6, r2)
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()
            java.lang.String r6 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r2, r6)
            int r2 = r2.getRawOffset()
            int r2 = r2 / 1000
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            java.lang.String r6 = "timezoneOffset"
            r12.a(r6, r2)
            com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$response$Anon1 r2 = new com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$response$Anon1
            r2.<init>(r10, r12, r3)
            r0.L$0 = r10
            r0.I$0 = r11
            r0.L$1 = r12
            r0.label = r5
            java.lang.Object r2 = com.fossil.aj5.a(r2, r0)
            if (r2 != r1) goto L_0x00af
            return r1
        L_0x00af:
            r5 = r10
        L_0x00b0:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r6 = r2 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x00e6
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "updateLastSleepGoal response = "
            r7.append(r8)
            r7.append(r2)
            java.lang.String r7 = r7.toString()
            r3.d(r6, r7)
            r0.L$0 = r5
            r0.I$0 = r11
            r0.L$1 = r12
            r0.L$2 = r2
            r0.label = r4
            java.lang.Object r11 = r5.saveSleepSettingToDB(r11, r0)
            if (r11 != r1) goto L_0x00e3
            return r1
        L_0x00e3:
            r11 = r2
        L_0x00e4:
            r2 = r11
            goto L_0x011f
        L_0x00e6:
            boolean r11 = r2 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x011f
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "updateLastSleepGoal code="
            r0.append(r1)
            r1 = r2
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r4 = r1.a()
            r0.append(r4)
            java.lang.String r4 = " message="
            r0.append(r4)
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x0115
            java.lang.String r3 = r1.getMessage()
        L_0x0115:
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r11.d(r12, r0)
        L_0x011f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository.updateLastSleepGoal(int, com.fossil.fb7):java.lang.Object");
    }
}
