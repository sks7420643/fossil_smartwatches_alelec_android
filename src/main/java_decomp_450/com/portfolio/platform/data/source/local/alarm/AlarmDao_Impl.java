package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.ju4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDao_Impl implements AlarmDao {
    @DexIgnore
    public /* final */ ju4 __alarmConverter; // = new ju4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<Alarm> __insertionAdapterOfAlarm;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveAlarm;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<Alarm> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `alarm` (`id`,`uri`,`title`,`message`,`hour`,`minute`,`days`,`isActive`,`isRepeated`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, Alarm alarm) {
            if (alarm.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, alarm.getId());
            }
            if (alarm.getUri() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, alarm.getUri());
            }
            if (alarm.getTitle() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, alarm.getTitle());
            }
            if (alarm.getMessage() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, alarm.getMessage());
            }
            ajVar.bindLong(5, (long) alarm.getHour());
            ajVar.bindLong(6, (long) alarm.getMinute());
            String a = AlarmDao_Impl.this.__alarmConverter.a(alarm.getDays());
            if (a == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a);
            }
            ajVar.bindLong(8, alarm.isActive() ? 1 : 0);
            ajVar.bindLong(9, alarm.isRepeated() ? 1 : 0);
            if (alarm.getCreatedAt() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, alarm.getCreatedAt());
            }
            if (alarm.getUpdatedAt() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, alarm.getUpdatedAt());
            }
            ajVar.bindLong(12, (long) alarm.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM alarm WHERE uri =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM alarm";
        }
    }

    @DexIgnore
    public AlarmDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfAlarm = new Anon1(ciVar);
        this.__preparedStmtOfRemoveAlarm = new Anon2(ciVar);
        this.__preparedStmtOfCleanUp = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getActiveAlarms() {
        fi fiVar;
        fi b = fi.b("SELECT *FROM alarm WHERE isActive = 1 and pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            fiVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getAlarmWithUri(String str) {
        Alarm alarm;
        fi b = fi.b("SELECT * FROM alarm WHERE uri =?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarms() {
        fi fiVar;
        fi b = fi.b("SELECT * FROM alarm", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            fiVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarmsIgnoreDeleted() {
        fi fiVar;
        fi b = fi.b("SELECT * FROM alarm WHERE pinType <> 3 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            fiVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getInComingActiveAlarm(int i) {
        Alarm alarm;
        fi b = fi.b("SELECT * FROM alarm WHERE isActive = 1 and minute + hour * 60 > ? ORDER BY hour, minute ASC LIMIT 1", 1);
        b.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getInComingActiveAlarms() {
        fi fiVar;
        fi b = fi.b("SELECT * FROM alarm WHERE isActive = 1 and pinType <> 3 ORDER BY hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            fiVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getPendingAlarms() {
        fi fiVar;
        fi b = fi.b("SELECT*FROM alarm where pinType <> 0 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uri");
            int b4 = oi.b(a, "title");
            int b5 = oi.b(a, "message");
            int b6 = oi.b(a, AppFilter.COLUMN_HOUR);
            int b7 = oi.b(a, MFSleepGoal.COLUMN_MINUTE);
            int b8 = oi.b(a, Alarm.COLUMN_DAYS);
            int b9 = oi.b(a, "isActive");
            int b10 = oi.b(a, "isRepeated");
            int b11 = oi.b(a, "createdAt");
            int b12 = oi.b(a, "updatedAt");
            int b13 = oi.b(a, "pinType");
            fiVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = b2;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public long insertAlarm(Alarm alarm) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfAlarm.insertAndReturnId(alarm);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Long[] insertAlarms(List<Alarm> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfAlarm.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public int removeAlarm(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveAlarm.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAlarm.release(acquire);
        }
    }
}
