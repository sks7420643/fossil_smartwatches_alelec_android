package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ee7;
import com.fossil.lf;
import com.fossil.pj4;
import com.fossil.te5;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryDataSourceFactory extends lf.b<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ te5.a mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        ee7.b(date, "mCreatedDate");
        ee7.b(pj4, "mAppExecutors");
        ee7.b(aVar, "mListener");
        ee7.b(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = pj4;
        this.mListener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.lf.b
    public lf<Date, GoalTrackingSummary> create() {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mCreatedDate, this.mGoalTrackingDatabase, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localDataSource = goalTrackingSummaryLocalDataSource;
        this.sourceLiveData.a(goalTrackingSummaryLocalDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource2 != null) {
            return goalTrackingSummaryLocalDataSource2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
