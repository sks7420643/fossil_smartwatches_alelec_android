package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticAwait$2", f = "SummariesRepository.kt", l = {435, 435}, m = "invokeSuspend")
public final class SummariesRepository$getActivityStatisticAwait$Anon2 extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getActivityStatisticAwait$Anon2(SummariesRepository summariesRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getActivityStatisticAwait$Anon2 summariesRepository$getActivityStatisticAwait$Anon2 = new SummariesRepository$getActivityStatisticAwait$Anon2(this.this$0, fb7);
        summariesRepository$getActivityStatisticAwait$Anon2.p$ = (yi7) obj;
        return summariesRepository$getActivityStatisticAwait$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
        return ((SummariesRepository$getActivityStatisticAwait$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivityStatisticAwait");
            SummariesRepository summariesRepository = this.this$0;
            this.L$0 = yi7;
            this.label = 1;
            obj = summariesRepository.getActivityStatisticDB(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return (ActivityStatistic) obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivityStatistic activityStatistic = (ActivityStatistic) obj;
        if (activityStatistic != null) {
            return activityStatistic;
        }
        SummariesRepository summariesRepository2 = this.this$0;
        this.L$0 = yi7;
        this.label = 2;
        obj = summariesRepository2.fetchActivityStatistic(this);
        if (obj == a) {
            return a;
        }
        return (ActivityStatistic) obj;
    }
}
