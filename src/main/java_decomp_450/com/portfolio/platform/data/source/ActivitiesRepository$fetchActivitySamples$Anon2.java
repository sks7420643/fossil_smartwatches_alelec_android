package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$2", f = "ActivitiesRepository.kt", l = {145, 146, 158}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon2(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2 = new ActivitiesRepository$fetchActivitySamples$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, fb7);
        activitiesRepository$fetchActivitySamples$Anon2.p$ = (yi7) obj;
        return activitiesRepository$fetchActivitySamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<Activity>>> fb7) {
        return ((ActivitiesRepository$fetchActivitySamples$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0160  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r13.label
            r2 = 3
            r3 = 2
            r4 = 1
            r5 = 0
            if (r1 == 0) goto L_0x0046
            if (r1 == r4) goto L_0x003e
            if (r1 == r3) goto L_0x0032
            if (r1 != r2) goto L_0x002a
            java.lang.Object r0 = r13.L$3
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r13.L$2
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r13.L$1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r1 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r1
            java.lang.Object r1 = r13.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r14)     // Catch:{ Exception -> 0x0027 }
            goto L_0x012d
        L_0x0027:
            r14 = move-exception
            goto L_0x0139
        L_0x002a:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r0)
            throw r14
        L_0x0032:
            java.lang.Object r1 = r13.L$1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r1 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r1
            java.lang.Object r3 = r13.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r14)
            goto L_0x009f
        L_0x003e:
            java.lang.Object r1 = r13.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r14)
            goto L_0x0087
        L_0x0046:
            com.fossil.t87.a(r14)
            com.fossil.yi7 r14 = r13.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r6 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r6 = r6.getTAG$app_fossilRelease()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fetchActivitySamples: start = "
            r7.append(r8)
            java.util.Date r8 = r13.$start
            r7.append(r8)
            java.lang.String r8 = ", end = "
            r7.append(r8)
            java.util.Date r8 = r13.$end
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r1.d(r6, r7)
            com.fossil.pg5 r1 = com.fossil.pg5.i
            r13.L$0 = r14
            r13.label = r4
            java.lang.Object r1 = r1.b(r13)
            if (r1 != r0) goto L_0x0084
            return r0
        L_0x0084:
            r12 = r1
            r1 = r14
            r14 = r12
        L_0x0087:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r14 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r14
            com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2 r4 = new com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2
            r4.<init>(r13, r5)
            r13.L$0 = r1
            r13.L$1 = r14
            r13.label = r3
            java.lang.Object r3 = com.fossil.aj5.a(r4, r13)
            if (r3 != r0) goto L_0x009b
            return r0
        L_0x009b:
            r12 = r1
            r1 = r14
            r14 = r3
            r3 = r12
        L_0x009f:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r4 = r14 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x0160
            r4 = r14
            com.fossil.bj5 r4 = (com.fossil.bj5) r4
            java.lang.Object r4 = r4.a()
            if (r4 == 0) goto L_0x015f
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0135 }
            r4.<init>()     // Catch:{ Exception -> 0x0135 }
            r6 = r14
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x0135 }
            java.lang.Object r6 = r6.a()     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6     // Catch:{ Exception -> 0x0135 }
            java.util.List r6 = r6.get_items()     // Catch:{ Exception -> 0x0135 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ Exception -> 0x0135 }
        L_0x00c4:
            boolean r7 = r6.hasNext()     // Catch:{ Exception -> 0x0135 }
            if (r7 == 0) goto L_0x00d8
            java.lang.Object r7 = r6.next()     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.Activity r7 = (com.portfolio.platform.data.Activity) r7     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r7 = r7.toActivitySample()     // Catch:{ Exception -> 0x0135 }
            r4.add(r7)     // Catch:{ Exception -> 0x0135 }
            goto L_0x00c4
        L_0x00d8:
            r6 = r14
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x0135 }
            boolean r6 = r6.b()     // Catch:{ Exception -> 0x0135 }
            if (r6 != 0) goto L_0x00e8
            com.portfolio.platform.data.source.local.fitness.ActivitySampleDao r6 = r1.activitySampleDao()     // Catch:{ Exception -> 0x0135 }
            r6.upsertListActivitySample(r4)     // Catch:{ Exception -> 0x0135 }
        L_0x00e8:
            r6 = r14
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x0135 }
            java.lang.Object r6 = r6.a()     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.model.Range r6 = r6.get_range()     // Catch:{ Exception -> 0x0135 }
            if (r6 == 0) goto L_0x0134
            r6 = r14
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x0135 }
            java.lang.Object r6 = r6.a()     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6     // Catch:{ Exception -> 0x0135 }
            com.portfolio.platform.data.model.Range r6 = r6.get_range()     // Catch:{ Exception -> 0x0135 }
            if (r6 == 0) goto L_0x0130
            boolean r5 = r6.isHasNext()     // Catch:{ Exception -> 0x0135 }
            if (r5 == 0) goto L_0x0134
            com.portfolio.platform.data.source.ActivitiesRepository r6 = r13.this$0     // Catch:{ Exception -> 0x0135 }
            java.util.Date r7 = r13.$start     // Catch:{ Exception -> 0x0135 }
            java.util.Date r8 = r13.$end     // Catch:{ Exception -> 0x0135 }
            int r5 = r13.$offset     // Catch:{ Exception -> 0x0135 }
            int r9 = r13.$limit     // Catch:{ Exception -> 0x0135 }
            int r9 = r9 + r5
            int r10 = r13.$limit     // Catch:{ Exception -> 0x0135 }
            r13.L$0 = r3     // Catch:{ Exception -> 0x0135 }
            r13.L$1 = r1     // Catch:{ Exception -> 0x0135 }
            r13.L$2 = r14     // Catch:{ Exception -> 0x0135 }
            r13.L$3 = r4     // Catch:{ Exception -> 0x0135 }
            r13.label = r2     // Catch:{ Exception -> 0x0135 }
            r11 = r13
            java.lang.Object r1 = r6.fetchActivitySamples(r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0135 }
            if (r1 != r0) goto L_0x012b
            return r0
        L_0x012b:
            r0 = r14
            r14 = r1
        L_0x012d:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            goto L_0x0134
        L_0x0130:
            com.fossil.ee7.a()
            throw r5
        L_0x0134:
            return r14
        L_0x0135:
            r0 = move-exception
            r12 = r0
            r0 = r14
            r14 = r12
        L_0x0139:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r2 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r2 = r2.getTAG$app_fossilRelease()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchActivitySamples exception="
            r3.append(r4)
            r14.printStackTrace()
            com.fossil.i97 r14 = com.fossil.i97.a
            r3.append(r14)
            java.lang.String r14 = r3.toString()
            r1.d(r2, r14)
            return r0
        L_0x015f:
            return r14
        L_0x0160:
            boolean r0 = r14 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01b0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.ActivitiesRepository$Companion r1 = com.portfolio.platform.data.source.ActivitiesRepository.Companion
            java.lang.String r1 = r1.getTAG$app_fossilRelease()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchActivitySamples Failure code="
            r2.append(r3)
            r3 = r14
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r4 = r3.a()
            r2.append(r4)
            java.lang.String r4 = " message="
            r2.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r3.c()
            if (r4 == 0) goto L_0x0197
            java.lang.String r4 = r4.getMessage()
            if (r4 == 0) goto L_0x0197
            r5 = r4
            goto L_0x01a1
        L_0x0197:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x01a1
            java.lang.String r5 = r3.getUserMessage()
        L_0x01a1:
            if (r5 == 0) goto L_0x01a4
            goto L_0x01a6
        L_0x01a4:
            java.lang.String r5 = ""
        L_0x01a6:
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x01b0:
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
