package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.jv4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.uh;
import com.fossil.vh;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDao_Impl extends ThemeDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<Theme> __deletionAdapterOfTheme;
    @DexIgnore
    public /* final */ vh<Theme> __insertionAdapterOfTheme;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearThemeTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfResetAllCurrentTheme;
    @DexIgnore
    public /* final */ ji __preparedStmtOfSetCurrentThemeId;
    @DexIgnore
    public /* final */ jv4 __themeConverter; // = new jv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<Theme> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `theme` (`isCurrentTheme`,`type`,`id`,`name`,`styles`) VALUES (?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, Theme theme) {
            ajVar.bindLong(1, theme.isCurrentTheme() ? 1 : 0);
            if (theme.getType() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, theme.getType());
            }
            if (theme.getId() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, theme.getId());
            }
            if (theme.getName() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, theme.getName());
            }
            String a = ThemeDao_Impl.this.__themeConverter.a(theme.getStyles());
            if (a == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<Theme> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `theme` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, Theme theme) {
            if (theme.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, theme.getId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM theme";
        }
    }

    @DexIgnore
    public ThemeDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfTheme = new Anon1(ciVar);
        this.__deletionAdapterOfTheme = new Anon2(ciVar);
        this.__preparedStmtOfResetAllCurrentTheme = new Anon3(ciVar);
        this.__preparedStmtOfSetCurrentThemeId = new Anon4(ciVar);
        this.__preparedStmtOfClearThemeTable = new Anon5(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void clearThemeTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearThemeTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearThemeTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void deleteTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfTheme.handle(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getCurrentTheme() {
        boolean z = false;
        fi b = fi.b("SELECT * FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Theme theme = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "isCurrentTheme");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "id");
            int b5 = oi.b(a, "name");
            int b6 = oi.b(a, "styles");
            if (a.moveToFirst()) {
                Theme theme2 = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                if (a.getInt(b2) != 0) {
                    z = true;
                }
                theme2.setCurrentTheme(z);
                theme2.setType(a.getString(b3));
                theme = theme2;
            }
            return theme;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getCurrentThemeId() {
        fi b = fi.b("SELECT id FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        String str = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                str = a.getString(0);
            }
            return str;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<Theme> getListTheme() {
        fi b = fi.b("SELECT * FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "isCurrentTheme");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "id");
            int b5 = oi.b(a, "name");
            int b6 = oi.b(a, "styles");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                Theme theme = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                theme.setCurrentTheme(a.getInt(b2) != 0);
                theme.setType(a.getString(b3));
                arrayList.add(theme);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeId() {
        fi b = fi.b("SELECT id FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeIdByType(String str) {
        fi b = fi.b("SELECT id FROM theme WHERE type=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getNameById(String str) {
        fi b = fi.b("SELECT name FROM theme WHERE id=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        String str2 = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                str2 = a.getString(0);
            }
            return str2;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getThemeById(String str) {
        boolean z = true;
        fi b = fi.b("SELECT * FROM theme WHERE id=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Theme theme = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "isCurrentTheme");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "id");
            int b5 = oi.b(a, "name");
            int b6 = oi.b(a, "styles");
            if (a.moveToFirst()) {
                Theme theme2 = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                if (a.getInt(b2) == 0) {
                    z = false;
                }
                theme2.setCurrentTheme(z);
                theme2.setType(a.getString(b3));
                theme = theme2;
            }
            return theme;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void insertListTheme(List<Theme> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void resetAllCurrentTheme() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfResetAllCurrentTheme.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetAllCurrentTheme.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void setCurrentThemeId(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfSetCurrentThemeId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetCurrentThemeId.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void upsertTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
