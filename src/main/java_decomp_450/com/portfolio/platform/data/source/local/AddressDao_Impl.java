package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDao_Impl implements AddressDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<AddressOfWeather> __insertionAdapterOfAddressOfWeather;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<AddressOfWeather> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `addressOfWeather` (`id`,`lat`,`lng`,`address`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, AddressOfWeather addressOfWeather) {
            ajVar.bindLong(1, (long) addressOfWeather.getId());
            ajVar.bindDouble(2, addressOfWeather.getLat());
            ajVar.bindDouble(3, addressOfWeather.getLng());
            if (addressOfWeather.getAddress() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, addressOfWeather.getAddress());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM addressOfWeather";
        }
    }

    @DexIgnore
    public AddressDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfAddressOfWeather = new Anon1(ciVar);
        this.__preparedStmtOfClearData = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public List<AddressOfWeather> getAllSavedAddress() {
        fi b = fi.b("SELECT * FROM addressOfWeather", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, Constants.LAT);
            int b4 = oi.b(a, "lng");
            int b5 = oi.b(a, "address");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                AddressOfWeather addressOfWeather = new AddressOfWeather(a.getDouble(b3), a.getDouble(b4), a.getString(b5));
                addressOfWeather.setId(a.getInt(b2));
                arrayList.add(addressOfWeather);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void saveAddress(AddressOfWeather addressOfWeather) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfAddressOfWeather.insert(addressOfWeather);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
