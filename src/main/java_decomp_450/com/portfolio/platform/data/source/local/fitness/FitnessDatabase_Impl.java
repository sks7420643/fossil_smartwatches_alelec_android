package com.portfolio.platform.data.source.local.fitness;

import com.facebook.places.PlaceManager;
import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wearables.fsl.fitness.ActivitySettings;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.FitnessDataDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao_Impl;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase_Impl extends FitnessDatabase {
    @DexIgnore
    public volatile ActivitySampleDao _activitySampleDao;
    @DexIgnore
    public volatile ActivitySummaryDao _activitySummaryDao;
    @DexIgnore
    public volatile FitnessDataDao _fitnessDataDao;
    @DexIgnore
    public volatile HeartRateDailySummaryDao _heartRateDailySummaryDao;
    @DexIgnore
    public volatile HeartRateSampleDao _heartRateSampleDao;
    @DexIgnore
    public volatile SampleRawDao _sampleRawDao;
    @DexIgnore
    public volatile WorkoutDao _workoutDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sampleraw` (`id` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `uaPinType` INTEGER NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `sourceId` TEXT NOT NULL, `sourceTypeValue` TEXT NOT NULL, `movementTypeValue` TEXT, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneID` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `activity_sample` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneOffsetInSecond` INTEGER NOT NULL, `sourceId` TEXT NOT NULL, `syncTime` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sampleday` (`createdAt` INTEGER, `updatedAt` INTEGER, `pinType` INTEGER NOT NULL, `year` INTEGER NOT NULL, `month` INTEGER NOT NULL, `day` INTEGER NOT NULL, `timezoneName` TEXT, `dstOffset` INTEGER, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `intensities` TEXT NOT NULL, `activeTime` INTEGER NOT NULL, `stepGoal` INTEGER NOT NULL, `caloriesGoal` INTEGER NOT NULL, `activeTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`year`, `month`, `day`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `activitySettings` (`id` INTEGER NOT NULL, `currentStepGoal` INTEGER NOT NULL, `currentCaloriesGoal` INTEGER NOT NULL, `currentActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `activityRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedStepsGoal` INTEGER NOT NULL, `recommendedCaloriesGoal` INTEGER NOT NULL, `recommendedActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `workout_session` (`speed` TEXT, `screenShotUri` TEXT, `states` TEXT NOT NULL, `id` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `deviceSerialNumber` TEXT, `step` TEXT, `calorie` TEXT, `distance` TEXT, `heartRate` TEXT, `sourceType` TEXT, `workoutType` TEXT, `timezoneOffset` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `workoutGpsPoints` TEXT, `gpsDataPoints` TEXT, `mode` TEXT, `pace` TEXT, `cadence` TEXT, `editedStartTime` TEXT NOT NULL, `editedEndTime` TEXT NOT NULL, `editedType` TEXT, `editedMode` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `fitness_data` (`step` TEXT NOT NULL, `activeMinute` TEXT NOT NULL, `calorie` TEXT NOT NULL, `distance` TEXT NOT NULL, `stress` TEXT, `resting` TEXT NOT NULL, `heartRate` TEXT, `sleeps` TEXT NOT NULL, `workouts` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `syncTime` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`startTime`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `heart_rate_sample` (`id` TEXT NOT NULL, `average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `endTime` TEXT NOT NULL, `startTime` TEXT NOT NULL, `timezoneOffset` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `daily_heart_rate_summary` (`average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`date`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `activity_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `activeTimeBestDay` TEXT, `activeTimeBestStreak` TEXT, `caloriesBestDay` TEXT, `caloriesBestStreak` TEXT, `stepsBestDay` TEXT, `stepsBestStreak` TEXT, `totalActiveTime` INTEGER NOT NULL, `totalCalories` REAL NOT NULL, `totalDays` INTEGER NOT NULL, `totalDistance` REAL NOT NULL, `totalSteps` INTEGER NOT NULL, `totalIntensityDistInStep` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'abe564b531b636e9dfd52068284844ef')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `sampleraw`");
            wiVar.execSQL("DROP TABLE IF EXISTS `activity_sample`");
            wiVar.execSQL("DROP TABLE IF EXISTS `sampleday`");
            wiVar.execSQL("DROP TABLE IF EXISTS `activitySettings`");
            wiVar.execSQL("DROP TABLE IF EXISTS `activityRecommendedGoals`");
            wiVar.execSQL("DROP TABLE IF EXISTS `workout_session`");
            wiVar.execSQL("DROP TABLE IF EXISTS `fitness_data`");
            wiVar.execSQL("DROP TABLE IF EXISTS `heart_rate_sample`");
            wiVar.execSQL("DROP TABLE IF EXISTS `daily_heart_rate_summary`");
            wiVar.execSQL("DROP TABLE IF EXISTS `activity_statistic`");
            if (((ci) FitnessDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FitnessDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FitnessDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) FitnessDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FitnessDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FitnessDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) FitnessDatabase_Impl.this).mDatabase = wiVar;
            FitnessDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) FitnessDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FitnessDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FitnessDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(14);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_UA_PIN_TYPE, new ti.a(SampleRaw.COLUMN_UA_PIN_TYPE, "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_SOURCE_ID, new ti.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, new ti.a(SampleRaw.COLUMN_SOURCE_TYPE_VALUE, "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, new ti.a(SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE, "TEXT", false, 0, null, 1));
            hashMap.put("steps", new ti.a("steps", "REAL", true, 0, null, 1));
            hashMap.put("calories", new ti.a("calories", "REAL", true, 0, null, 1));
            hashMap.put("distance", new ti.a("distance", "REAL", true, 0, null, 1));
            hashMap.put(SampleDay.COLUMN_ACTIVE_TIME, new ti.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put("intensityDistInSteps", new ti.a("intensityDistInSteps", "TEXT", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_TIMEZONE_ID, new ti.a(SampleRaw.COLUMN_TIMEZONE_ID, "TEXT", false, 0, null, 1));
            ti tiVar = new ti(SampleRaw.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, SampleRaw.TABLE_NAME);
            if (!tiVar.equals(a)) {
                return new ei.b(false, "sampleraw(com.portfolio.platform.data.model.room.fitness.SampleRaw).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(15);
            hashMap2.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap2.put("uid", new ti.a("uid", "TEXT", true, 0, null, 1));
            hashMap2.put("date", new ti.a("date", "TEXT", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap2.put("steps", new ti.a("steps", "REAL", true, 0, null, 1));
            hashMap2.put("calories", new ti.a("calories", "REAL", true, 0, null, 1));
            hashMap2.put("distance", new ti.a("distance", "REAL", true, 0, null, 1));
            hashMap2.put(SampleDay.COLUMN_ACTIVE_TIME, new ti.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap2.put("intensityDistInSteps", new ti.a("intensityDistInSteps", "TEXT", true, 0, null, 1));
            hashMap2.put("timeZoneOffsetInSecond", new ti.a("timeZoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap2.put(SampleRaw.COLUMN_SOURCE_ID, new ti.a(SampleRaw.COLUMN_SOURCE_ID, "TEXT", true, 0, null, 1));
            hashMap2.put("syncTime", new ti.a("syncTime", "INTEGER", true, 0, null, 1));
            hashMap2.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            ti tiVar2 = new ti(ActivitySample.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, ActivitySample.TABLE_NAME);
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "activity_sample(com.portfolio.platform.data.model.room.fitness.ActivitySample).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(16);
            hashMap3.put("createdAt", new ti.a("createdAt", "INTEGER", false, 0, null, 1));
            hashMap3.put("updatedAt", new ti.a("updatedAt", "INTEGER", false, 0, null, 1));
            hashMap3.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap3.put("year", new ti.a("year", "INTEGER", true, 1, null, 1));
            hashMap3.put("month", new ti.a("month", "INTEGER", true, 2, null, 1));
            hashMap3.put("day", new ti.a("day", "INTEGER", true, 3, null, 1));
            hashMap3.put(SampleDay.COLUMN_TIMEZONE_NAME, new ti.a(SampleDay.COLUMN_TIMEZONE_NAME, "TEXT", false, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_DST_OFFSET, new ti.a(SampleDay.COLUMN_DST_OFFSET, "INTEGER", false, 0, null, 1));
            hashMap3.put("steps", new ti.a("steps", "REAL", true, 0, null, 1));
            hashMap3.put("calories", new ti.a("calories", "REAL", true, 0, null, 1));
            hashMap3.put("distance", new ti.a("distance", "REAL", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_INTENSITIES, new ti.a(SampleDay.COLUMN_INTENSITIES, "TEXT", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME, new ti.a(SampleDay.COLUMN_ACTIVE_TIME, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_STEP_GOAL, new ti.a(SampleDay.COLUMN_STEP_GOAL, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_CALORIES_GOAL, new ti.a(SampleDay.COLUMN_CALORIES_GOAL, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleDay.COLUMN_ACTIVE_TIME_GOAL, new ti.a(SampleDay.COLUMN_ACTIVE_TIME_GOAL, "INTEGER", true, 0, null, 1));
            ti tiVar3 = new ti(SampleDay.TABLE_NAME, hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, SampleDay.TABLE_NAME);
            if (!tiVar3.equals(a3)) {
                return new ei.b(false, "sampleday(com.portfolio.platform.data.model.room.fitness.ActivitySummary).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(4);
            hashMap4.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, new ti.a(ActivitySettings.COLUMN_CURRENT_STEP_GOAL, "INTEGER", true, 0, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, new ti.a(ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL, "INTEGER", true, 0, null, 1));
            hashMap4.put(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, new ti.a(ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL, "INTEGER", true, 0, null, 1));
            ti tiVar4 = new ti(ActivitySettings.TABLE_NAME, hashMap4, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, ActivitySettings.TABLE_NAME);
            if (!tiVar4.equals(a4)) {
                return new ei.b(false, "activitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings).\n Expected:\n" + tiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(4);
            hashMap5.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("recommendedStepsGoal", new ti.a("recommendedStepsGoal", "INTEGER", true, 0, null, 1));
            hashMap5.put("recommendedCaloriesGoal", new ti.a("recommendedCaloriesGoal", "INTEGER", true, 0, null, 1));
            hashMap5.put("recommendedActiveTimeGoal", new ti.a("recommendedActiveTimeGoal", "INTEGER", true, 0, null, 1));
            ti tiVar5 = new ti("activityRecommendedGoals", hashMap5, new HashSet(0), new HashSet(0));
            ti a5 = ti.a(wiVar, "activityRecommendedGoals");
            if (!tiVar5.equals(a5)) {
                return new ei.b(false, "activityRecommendedGoals(com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals).\n Expected:\n" + tiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(27);
            hashMap6.put(PlaceManager.PARAM_SPEED, new ti.a(PlaceManager.PARAM_SPEED, "TEXT", false, 0, null, 1));
            hashMap6.put("screenShotUri", new ti.a("screenShotUri", "TEXT", false, 0, null, 1));
            hashMap6.put("states", new ti.a("states", "TEXT", true, 0, null, 1));
            hashMap6.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("date", new ti.a("date", "TEXT", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new ti.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0, null, 1));
            hashMap6.put("step", new ti.a("step", "TEXT", false, 0, null, 1));
            hashMap6.put("calorie", new ti.a("calorie", "TEXT", false, 0, null, 1));
            hashMap6.put("distance", new ti.a("distance", "TEXT", false, 0, null, 1));
            hashMap6.put("heartRate", new ti.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap6.put("sourceType", new ti.a("sourceType", "TEXT", false, 0, null, 1));
            hashMap6.put("workoutType", new ti.a("workoutType", "TEXT", false, 0, null, 1));
            hashMap6.put("timezoneOffset", new ti.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap6.put("duration", new ti.a("duration", "INTEGER", true, 0, null, 1));
            hashMap6.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap6.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap6.put("workoutGpsPoints", new ti.a("workoutGpsPoints", "TEXT", false, 0, null, 1));
            hashMap6.put("gpsDataPoints", new ti.a("gpsDataPoints", "TEXT", false, 0, null, 1));
            hashMap6.put("mode", new ti.a("mode", "TEXT", false, 0, null, 1));
            hashMap6.put("pace", new ti.a("pace", "TEXT", false, 0, null, 1));
            hashMap6.put("cadence", new ti.a("cadence", "TEXT", false, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_EDITED_START_TIME, new ti.a(MFSleepSession.COLUMN_EDITED_START_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put(MFSleepSession.COLUMN_EDITED_END_TIME, new ti.a(MFSleepSession.COLUMN_EDITED_END_TIME, "TEXT", true, 0, null, 1));
            hashMap6.put("editedType", new ti.a("editedType", "TEXT", false, 0, null, 1));
            hashMap6.put("editedMode", new ti.a("editedMode", "TEXT", false, 0, null, 1));
            ti tiVar6 = new ti("workout_session", hashMap6, new HashSet(0), new HashSet(0));
            ti a6 = ti.a(wiVar, "workout_session");
            if (!tiVar6.equals(a6)) {
                return new ei.b(false, "workout_session(com.portfolio.platform.data.model.diana.workout.WorkoutSession).\n Expected:\n" + tiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(14);
            hashMap7.put("step", new ti.a("step", "TEXT", true, 0, null, 1));
            hashMap7.put("activeMinute", new ti.a("activeMinute", "TEXT", true, 0, null, 1));
            hashMap7.put("calorie", new ti.a("calorie", "TEXT", true, 0, null, 1));
            hashMap7.put("distance", new ti.a("distance", "TEXT", true, 0, null, 1));
            hashMap7.put("stress", new ti.a("stress", "TEXT", false, 0, null, 1));
            hashMap7.put("resting", new ti.a("resting", "TEXT", true, 0, null, 1));
            hashMap7.put("heartRate", new ti.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap7.put("sleeps", new ti.a("sleeps", "TEXT", true, 0, null, 1));
            hashMap7.put("workouts", new ti.a("workouts", "TEXT", true, 0, null, 1));
            hashMap7.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 1, null, 1));
            hashMap7.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap7.put("syncTime", new ti.a("syncTime", "TEXT", true, 0, null, 1));
            hashMap7.put("timezoneOffsetInSecond", new ti.a("timezoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap7.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 0, null, 1));
            ti tiVar7 = new ti("fitness_data", hashMap7, new HashSet(0), new HashSet(0));
            ti a7 = ti.a(wiVar, "fitness_data");
            if (!tiVar7.equals(a7)) {
                return new ei.b(false, "fitness_data(com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper).\n Expected:\n" + tiVar7 + "\n Found:\n" + a7);
            }
            HashMap hashMap8 = new HashMap(12);
            hashMap8.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap8.put(GoalTrackingSummary.COLUMN_AVERAGE, new ti.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0, null, 1));
            hashMap8.put("date", new ti.a("date", "TEXT", true, 0, null, 1));
            hashMap8.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap8.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", true, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", true, 0, null, 1));
            hashMap8.put("timezoneOffset", new ti.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap8.put("min", new ti.a("min", "INTEGER", true, 0, null, 1));
            hashMap8.put("max", new ti.a("max", "INTEGER", true, 0, null, 1));
            hashMap8.put("minuteCount", new ti.a("minuteCount", "INTEGER", true, 0, null, 1));
            hashMap8.put("resting", new ti.a("resting", "TEXT", false, 0, null, 1));
            ti tiVar8 = new ti("heart_rate_sample", hashMap8, new HashSet(0), new HashSet(0));
            ti a8 = ti.a(wiVar, "heart_rate_sample");
            if (!tiVar8.equals(a8)) {
                return new ei.b(false, "heart_rate_sample(com.portfolio.platform.data.model.diana.heartrate.HeartRateSample).\n Expected:\n" + tiVar8 + "\n Found:\n" + a8);
            }
            HashMap hashMap9 = new HashMap(8);
            hashMap9.put(GoalTrackingSummary.COLUMN_AVERAGE, new ti.a(GoalTrackingSummary.COLUMN_AVERAGE, "REAL", true, 0, null, 1));
            hashMap9.put("date", new ti.a("date", "TEXT", true, 1, null, 1));
            hashMap9.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("min", new ti.a("min", "INTEGER", true, 0, null, 1));
            hashMap9.put("max", new ti.a("max", "INTEGER", true, 0, null, 1));
            hashMap9.put("minuteCount", new ti.a("minuteCount", "INTEGER", true, 0, null, 1));
            hashMap9.put("resting", new ti.a("resting", "TEXT", false, 0, null, 1));
            ti tiVar9 = new ti("daily_heart_rate_summary", hashMap9, new HashSet(0), new HashSet(0));
            ti a9 = ti.a(wiVar, "daily_heart_rate_summary");
            if (!tiVar9.equals(a9)) {
                return new ei.b(false, "daily_heart_rate_summary(com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary).\n Expected:\n" + tiVar9 + "\n Found:\n" + a9);
            }
            HashMap hashMap10 = new HashMap(16);
            hashMap10.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap10.put("uid", new ti.a("uid", "TEXT", true, 0, null, 1));
            hashMap10.put("activeTimeBestDay", new ti.a("activeTimeBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("activeTimeBestStreak", new ti.a("activeTimeBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("caloriesBestDay", new ti.a("caloriesBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("caloriesBestStreak", new ti.a("caloriesBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("stepsBestDay", new ti.a("stepsBestDay", "TEXT", false, 0, null, 1));
            hashMap10.put("stepsBestStreak", new ti.a("stepsBestStreak", "TEXT", false, 0, null, 1));
            hashMap10.put("totalActiveTime", new ti.a("totalActiveTime", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalCalories", new ti.a("totalCalories", "REAL", true, 0, null, 1));
            hashMap10.put("totalDays", new ti.a("totalDays", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalDistance", new ti.a("totalDistance", "REAL", true, 0, null, 1));
            hashMap10.put("totalSteps", new ti.a("totalSteps", "INTEGER", true, 0, null, 1));
            hashMap10.put("totalIntensityDistInStep", new ti.a("totalIntensityDistInStep", "TEXT", true, 0, null, 1));
            hashMap10.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap10.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            ti tiVar10 = new ti(ActivityStatistic.TABLE_NAME, hashMap10, new HashSet(0), new HashSet(0));
            ti a10 = ti.a(wiVar, ActivityStatistic.TABLE_NAME);
            if (tiVar10.equals(a10)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "activity_statistic(com.portfolio.platform.data.ActivityStatistic).\n Expected:\n" + tiVar10 + "\n Found:\n" + a10);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public ActivitySampleDao activitySampleDao() {
        ActivitySampleDao activitySampleDao;
        if (this._activitySampleDao != null) {
            return this._activitySampleDao;
        }
        synchronized (this) {
            if (this._activitySampleDao == null) {
                this._activitySampleDao = new ActivitySampleDao_Impl(this);
            }
            activitySampleDao = this._activitySampleDao;
        }
        return activitySampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public ActivitySummaryDao activitySummaryDao() {
        ActivitySummaryDao activitySummaryDao;
        if (this._activitySummaryDao != null) {
            return this._activitySummaryDao;
        }
        synchronized (this) {
            if (this._activitySummaryDao == null) {
                this._activitySummaryDao = new ActivitySummaryDao_Impl(this);
            }
            activitySummaryDao = this._activitySummaryDao;
        }
        return activitySummaryDao;
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `sampleraw`");
            writableDatabase.execSQL("DELETE FROM `activity_sample`");
            writableDatabase.execSQL("DELETE FROM `sampleday`");
            writableDatabase.execSQL("DELETE FROM `activitySettings`");
            writableDatabase.execSQL("DELETE FROM `activityRecommendedGoals`");
            writableDatabase.execSQL("DELETE FROM `workout_session`");
            writableDatabase.execSQL("DELETE FROM `fitness_data`");
            writableDatabase.execSQL("DELETE FROM `heart_rate_sample`");
            writableDatabase.execSQL("DELETE FROM `daily_heart_rate_summary`");
            writableDatabase.execSQL("DELETE FROM `activity_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), SampleRaw.TABLE_NAME, ActivitySample.TABLE_NAME, SampleDay.TABLE_NAME, ActivitySettings.TABLE_NAME, "activityRecommendedGoals", "workout_session", "fitness_data", "heart_rate_sample", "daily_heart_rate_summary", ActivityStatistic.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(22), "abe564b531b636e9dfd52068284844ef", "6ab7daea4220f579f89a1c1e9992b5e3");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public FitnessDataDao getFitnessDataDao() {
        FitnessDataDao fitnessDataDao;
        if (this._fitnessDataDao != null) {
            return this._fitnessDataDao;
        }
        synchronized (this) {
            if (this._fitnessDataDao == null) {
                this._fitnessDataDao = new FitnessDataDao_Impl(this);
            }
            fitnessDataDao = this._fitnessDataDao;
        }
        return fitnessDataDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public HeartRateDailySummaryDao getHeartRateDailySummaryDao() {
        HeartRateDailySummaryDao heartRateDailySummaryDao;
        if (this._heartRateDailySummaryDao != null) {
            return this._heartRateDailySummaryDao;
        }
        synchronized (this) {
            if (this._heartRateDailySummaryDao == null) {
                this._heartRateDailySummaryDao = new HeartRateDailySummaryDao_Impl(this);
            }
            heartRateDailySummaryDao = this._heartRateDailySummaryDao;
        }
        return heartRateDailySummaryDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public HeartRateSampleDao getHeartRateDao() {
        HeartRateSampleDao heartRateSampleDao;
        if (this._heartRateSampleDao != null) {
            return this._heartRateSampleDao;
        }
        synchronized (this) {
            if (this._heartRateSampleDao == null) {
                this._heartRateSampleDao = new HeartRateSampleDao_Impl(this);
            }
            heartRateSampleDao = this._heartRateSampleDao;
        }
        return heartRateSampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public WorkoutDao getWorkoutDao() {
        WorkoutDao workoutDao;
        if (this._workoutDao != null) {
            return this._workoutDao;
        }
        synchronized (this) {
            if (this._workoutDao == null) {
                this._workoutDao = new WorkoutDao_Impl(this);
            }
            workoutDao = this._workoutDao;
        }
        return workoutDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.FitnessDatabase
    public SampleRawDao sampleRawDao() {
        SampleRawDao sampleRawDao;
        if (this._sampleRawDao != null) {
            return this._sampleRawDao;
        }
        synchronized (this) {
            if (this._sampleRawDao == null) {
                this._sampleRawDao = new SampleRawDao_Impl(this);
            }
            sampleRawDao = this._sampleRawDao;
        }
        return sampleRawDao;
    }
}
