package com.portfolio.platform.data.source.local.fitness;

import com.fossil.cb5;
import com.fossil.db5;
import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.workers.PushPendingDataWorker;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$Anon1 extends li {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        String str;
        String str2;
        String str3;
        ee7.b(wiVar, "database");
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration 2 to 13 start");
        wiVar.beginTransaction();
        DateTime dateTime = new DateTime();
        TimeZone timeZone = dateTime.getZone().toTimeZone();
        int year = dateTime.getYear();
        int monthOfYear = dateTime.getMonthOfYear();
        int dayOfMonth = dateTime.getDayOfMonth();
        ee7.a((Object) timeZone, "timeZone");
        String id = timeZone.getID();
        int dSTSavings = timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0;
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table");
            wiVar.execSQL("CREATE TABLE sampleraw_new (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT, " + "steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + db5.Device.getValue() + "'," + " pinType INTEGER NOT NULL DEFAULT 0, uaPinType INTEGER NOT NULL DEFAULT 0, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, " + "intensityDistInSteps TEXT NOT NULL DEFAULT '')");
            wiVar.execSQL("INSERT INTO sampleraw_new (id, startTime, endTime, sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, timeZoneID) SELECT id, substr(startTime, 1, 23), substr(endTime, 1, 23), sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, timeZoneID  FROM sampleraw");
            wiVar.execSQL("DROP TABLE sampleraw");
            wiVar.execSQL("ALTER TABLE sampleraw_new RENAME TO sampleraw");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table success");
            str2 = ", 0, 1)";
            str = ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, 140, 30, ";
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            str2 = ", 0, 1)";
            StringBuilder sb = new StringBuilder();
            str = ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, 140, 30, ";
            sb.append("Migrate sample raw table fail ");
            sb.append(e);
            local.d(FitnessDatabase.TAG, sb.toString());
            wiVar.execSQL("DROP TABLE IF EXISTS sampleraw_new");
            wiVar.execSQL("DROP TABLE IF EXISTS sampleraw");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS sampleraw (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT NOT NULL DEFAULT '" + cb5.WALKING.getValue() + "', steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + db5.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
        }
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw");
            wiVar.execSQL("CREATE TABLE intensities (min INTEGER NOT NULL, max INTEGER NOT NULL, intensity TEXT NOT NULL DEFAULT '')");
            wiVar.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (0, 70, 'light')");
            wiVar.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (70, 140, 'moderate')");
            wiVar.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (140, 2147483647, 'intense')");
            wiVar.execSQL("UPDATE sampleraw SET intensityDistInSteps=( SELECT '{\"' || intensity || '\":' || sampleraw.steps || '}' FROM intensities WHERE sampleraw.steps >= intensities.min AND sampleraw.steps < intensities.max)");
            wiVar.execSQL("DROP TABLE intensities");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw success");
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw " + e2);
            wiVar.execSQL("DROP TABLE IF EXISTS intensities");
        }
        try {
            wiVar.execSQL("CREATE TABLE sampleday_new (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 0, PRIMARY KEY (year, month, day))");
            wiVar.execSQL("INSERT INTO sampleday_new (year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt) SELECT year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt FROM sampleday");
            wiVar.execSQL("DROP TABLE sampleday");
            wiVar.execSQL("ALTER TABLE sampleday_new RENAME TO sampleday");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)");
            sb2.append(" VALUES(");
            sb2.append(year);
            sb2.append(", ");
            sb2.append(monthOfYear);
            sb2.append(", ");
            sb2.append(dayOfMonth);
            sb2.append(", '");
            sb2.append(id);
            sb2.append("', ");
            sb2.append(dSTSavings);
            try {
                sb2.append(str);
                str = str;
                sb2.append(System.currentTimeMillis());
                sb2.append(", ");
                sb2.append(System.currentTimeMillis());
                str3 = str2;
                try {
                    sb2.append(str3);
                    wiVar.execSQL(sb2.toString());
                    wiVar.execSQL("UPDATE sampleday SET stepGoal=(SELECT steps FROM dailygoal) WHERE sampleday.year = year AND sampleday.month = month AND sampleday.day = day");
                    FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample day success");
                } catch (Exception e3) {
                    e = e3;
                }
            } catch (Exception e4) {
                e = e4;
                str = str;
                str3 = str2;
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(FitnessDatabase.TAG, "Migrate sample day fail " + e);
                wiVar.execSQL("DROP TABLE IF EXISTS sampleday_new");
                wiVar.execSQL("DROP TABLE IF EXISTS sampleday");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
                wiVar.execSQL("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + str + System.currentTimeMillis() + ", " + System.currentTimeMillis() + str3);
                wiVar.execSQL("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
                wiVar.execSQL("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
                wiVar.execSQL("DROP TABLE dailygoal");
                FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration activitySetting success");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
                wiVar.execSQL("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
                wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
                wiVar.setTransactionSuccessful();
                wiVar.endTransaction();
                FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
                PushPendingDataWorker.G.a();
            }
        } catch (Exception e5) {
            e = e5;
            str3 = str2;
            ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
            local32.d(FitnessDatabase.TAG, "Migrate sample day fail " + e);
            wiVar.execSQL("DROP TABLE IF EXISTS sampleday_new");
            wiVar.execSQL("DROP TABLE IF EXISTS sampleday");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
            wiVar.execSQL("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType)" + " VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + str + System.currentTimeMillis() + ", " + System.currentTimeMillis() + str3);
            wiVar.execSQL("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            wiVar.execSQL("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
            wiVar.execSQL("DROP TABLE dailygoal");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration activitySetting success");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
            wiVar.execSQL("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            wiVar.setTransactionSuccessful();
            wiVar.endTransaction();
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
            PushPendingDataWorker.G.a();
        }
        try {
            wiVar.execSQL("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            wiVar.execSQL("INSERT INTO activitySettings (currentStepGoal) SELECT steps FROM dailygoal WHERE dailygoal.year=" + year + " AND dailygoal.month=" + monthOfYear + " AND dailygoal.day=" + dayOfMonth);
            wiVar.execSQL("DROP TABLE dailygoal");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration activitySetting success");
        } catch (Exception e6) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d(FitnessDatabase.TAG, "Migration activitySetting fail " + e6);
            wiVar.execSQL("DROP TABLE IF EXISTS dailygoal");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
        }
        wiVar.execSQL("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        wiVar.execSQL("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT, heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
        wiVar.execSQL("CREATE TABLE activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        wiVar.execSQL("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        wiVar.execSQL("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
        try {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (Exception e7) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            local5.e(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 - ActivityStatistic -- e=" + e7);
            e7.printStackTrace();
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 done, start schedule push pending data from 3.0");
        PushPendingDataWorker.G.a();
    }
}
