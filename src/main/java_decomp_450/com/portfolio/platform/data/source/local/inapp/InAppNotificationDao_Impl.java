package com.portfolio.platform.data.source.local.inapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.uh;
import com.fossil.vh;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDao_Impl extends InAppNotificationDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<InAppNotification> __deletionAdapterOfInAppNotification;
    @DexIgnore
    public /* final */ vh<InAppNotification> __insertionAdapterOfInAppNotification;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<InAppNotification> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inAppNotification` (`id`,`title`,`content`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, inAppNotification.getId());
            }
            if (inAppNotification.getTitle() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, inAppNotification.getTitle());
            }
            if (inAppNotification.getContent() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, inAppNotification.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<InAppNotification> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `inAppNotification` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, inAppNotification.getId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InAppNotification>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<InAppNotification> call() throws Exception {
            Cursor a = pi.a(InAppNotificationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "title");
                int b3 = oi.b(a, "content");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InAppNotification(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InAppNotificationDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfInAppNotification = new Anon1(ciVar);
        this.__deletionAdapterOfInAppNotification = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void delete(InAppNotification inAppNotification) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfInAppNotification.handle(inAppNotification);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public LiveData<List<InAppNotification>> getAllInAppNotification() {
        return this.__db.getInvalidationTracker().a(new String[]{"inAppNotification"}, false, (Callable) new Anon3(fi.b("SELECT * FROM inAppNotification", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void insertListInAppNotification(List<InAppNotification> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInAppNotification.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
