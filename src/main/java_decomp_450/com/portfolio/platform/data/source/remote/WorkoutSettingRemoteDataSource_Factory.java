package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRemoteDataSource_Factory implements Factory<WorkoutSettingRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public WorkoutSettingRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static WorkoutSettingRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new WorkoutSettingRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static WorkoutSettingRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new WorkoutSettingRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WorkoutSettingRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
