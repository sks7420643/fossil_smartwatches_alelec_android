package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.yi7;
import com.fossil.zb7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$1", f = "HeartRateSummaryLocalDataSource.kt", l = {154, 159}, m = "invokeSuspend")
public final class HeartRateSummaryLocalDataSource$loadData$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ te5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryLocalDataSource$loadData$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, Date date, Date date2, te5.b.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = heartRateSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        HeartRateSummaryLocalDataSource$loadData$Anon1 heartRateSummaryLocalDataSource$loadData$Anon1 = new HeartRateSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$helperCallback, fb7);
        heartRateSummaryLocalDataSource$loadData$Anon1.p$ = (yi7) obj;
        return heartRateSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((HeartRateSummaryLocalDataSource$loadData$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c5  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r8.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L_0x002f
            if (r1 == r3) goto L_0x0027
            if (r1 != r2) goto L_0x001f
            java.lang.Object r0 = r8.L$2
            com.fossil.r87 r0 = (com.fossil.r87) r0
            java.lang.Object r0 = r8.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r8.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r9)
            goto L_0x00a2
        L_0x001f:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0027:
            java.lang.Object r1 = r8.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r9)
            goto L_0x0075
        L_0x002f:
            com.fossil.t87.a(r9)
            com.fossil.yi7 r1 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$Companion r4 = com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource.Companion
            java.lang.String r4 = r4.getTAG$app_fossilRelease()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "loadData start="
            r5.append(r6)
            java.util.Date r6 = r8.$startDate
            r5.append(r6)
            java.lang.String r6 = ", end="
            r5.append(r6)
            java.util.Date r6 = r8.$endDate
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r9.d(r4, r5)
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource r9 = r8.this$0
            com.portfolio.platform.data.source.FitnessDataRepository r9 = r9.mFitnessDataRepository
            java.util.Date r4 = r8.$startDate
            java.util.Date r5 = r8.$endDate
            r8.L$0 = r1
            r8.label = r3
            java.lang.Object r9 = r9.getFitnessData(r4, r5, r8)
            if (r9 != r0) goto L_0x0075
            return r0
        L_0x0075:
            java.util.List r9 = (java.util.List) r9
            java.util.Date r4 = r8.$startDate
            java.util.Date r5 = r8.$endDate
            com.fossil.r87 r4 = com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.calculateRangeDownload(r9, r4, r5)
            if (r4 == 0) goto L_0x0100
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource r5 = r8.this$0
            com.portfolio.platform.data.source.HeartRateSummaryRepository r5 = r5.mSummariesRepository
            java.lang.Object r6 = r4.getFirst()
            java.util.Date r6 = (java.util.Date) r6
            java.lang.Object r7 = r4.getSecond()
            java.util.Date r7 = (java.util.Date) r7
            r8.L$0 = r1
            r8.L$1 = r9
            r8.L$2 = r4
            r8.label = r2
            java.lang.Object r9 = r5.loadSummaries(r6, r7, r8)
            if (r9 != r0) goto L_0x00a2
            return r0
        L_0x00a2:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r0 = r9 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x00c5
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource r9 = r8.this$0
            java.util.List r9 = r9.mRequestAfterQueue
            boolean r9 = r9.isEmpty()
            r9 = r9 ^ r3
            if (r9 == 0) goto L_0x00bf
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource r9 = r8.this$0
            java.util.List r9 = r9.mRequestAfterQueue
            r0 = 0
            r9.remove(r0)
        L_0x00bf:
            com.fossil.te5$b$a r9 = r8.$helperCallback
            r9.a()
            goto L_0x0105
        L_0x00c5:
            boolean r0 = r9 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0105
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            java.lang.Throwable r0 = r9.d()
            if (r0 == 0) goto L_0x00db
            com.fossil.te5$b$a r0 = r8.$helperCallback
            java.lang.Throwable r9 = r9.d()
            r0.a(r9)
            goto L_0x0105
        L_0x00db:
            com.portfolio.platform.data.model.ServerError r0 = r9.c()
            if (r0 == 0) goto L_0x0105
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            com.fossil.te5$b$a r0 = r8.$helperCallback
            java.lang.Throwable r1 = new java.lang.Throwable
            java.lang.String r2 = r9.getUserMessage()
            if (r2 == 0) goto L_0x00f0
            goto L_0x00f4
        L_0x00f0:
            java.lang.String r2 = r9.getMessage()
        L_0x00f4:
            if (r2 == 0) goto L_0x00f7
            goto L_0x00f9
        L_0x00f7:
            java.lang.String r2 = ""
        L_0x00f9:
            r1.<init>(r2)
            r0.a(r1)
            goto L_0x0105
        L_0x0100:
            com.fossil.te5$b$a r9 = r8.$helperCallback
            r9.a()
        L_0x0105:
            com.fossil.i97 r9 = com.fossil.i97.a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
