package com.portfolio.platform.data.source.local.diana;

import com.fossil.ci;
import com.fossil.li;
import com.fossil.zd7;
import com.portfolio.platform.data.source.local.RingStyleDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DianaCustomizeDatabase extends ci {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_13_TO_14; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_13_TO_14$Anon1(13, 14);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_14_TO_15; // = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1(14, 15);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaCustomizeDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final li getMIGRATION_FROM_13_TO_14() {
            return DianaCustomizeDatabase.MIGRATION_FROM_13_TO_14;
        }

        @DexIgnore
        public final li getMIGRATION_FROM_14_TO_15() {
            return DianaCustomizeDatabase.MIGRATION_FROM_14_TO_15;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public abstract ComplicationDao getComplicationDao();

    @DexIgnore
    public abstract ComplicationLastSettingDao getComplicationSettingDao();

    @DexIgnore
    public abstract DianaPresetDao getPresetDao();

    @DexIgnore
    public abstract RingStyleDao getRingStyleDao();

    @DexIgnore
    public abstract WatchAppDao getWatchAppDao();

    @DexIgnore
    public abstract WatchAppDataDao getWatchAppDataDao();

    @DexIgnore
    public abstract WatchAppLastSettingDao getWatchAppSettingDao();

    @DexIgnore
    public abstract WatchFaceDao getWatchFaceDao();
}
