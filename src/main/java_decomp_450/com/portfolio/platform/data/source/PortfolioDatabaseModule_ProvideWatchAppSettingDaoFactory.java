package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory implements Factory<WatchAppLastSettingDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WatchAppLastSettingDao provideWatchAppSettingDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        WatchAppLastSettingDao provideWatchAppSettingDao = portfolioDatabaseModule.provideWatchAppSettingDao(dianaCustomizeDatabase);
        c87.a(provideWatchAppSettingDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideWatchAppSettingDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppLastSettingDao get() {
        return provideWatchAppSettingDao(this.module, this.dbProvider.get());
    }
}
