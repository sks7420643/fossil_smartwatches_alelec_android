package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.dv4;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.lf;
import com.fossil.mi;
import com.fossil.mu4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDailySummaryDao_Impl extends HeartRateDailySummaryDao {
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<DailyHeartRateSummary> __insertionAdapterOfDailyHeartRateSummary;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllHeartRateSummaries;
    @DexIgnore
    public /* final */ dv4 __restingConverter; // = new dv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<DailyHeartRateSummary> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `daily_heart_rate_summary` (`average`,`date`,`createdAt`,`updatedAt`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DailyHeartRateSummary dailyHeartRateSummary) {
            ajVar.bindDouble(1, (double) dailyHeartRateSummary.getAverage());
            String a = HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(dailyHeartRateSummary.getDate());
            if (a == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a);
            }
            ajVar.bindLong(3, dailyHeartRateSummary.getCreatedAt());
            ajVar.bindLong(4, dailyHeartRateSummary.getUpdatedAt());
            ajVar.bindLong(5, (long) dailyHeartRateSummary.getMin());
            ajVar.bindLong(6, (long) dailyHeartRateSummary.getMax());
            ajVar.bindLong(7, (long) dailyHeartRateSummary.getMinuteCount());
            String a2 = HeartRateDailySummaryDao_Impl.this.__restingConverter.a(dailyHeartRateSummary.getResting());
            if (a2 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM daily_heart_rate_summary";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DailyHeartRateSummary> call() throws Exception {
            Cursor a = pi.a(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = oi.b(a, "date");
                int b3 = oi.b(a, "createdAt");
                int b4 = oi.b(a, "updatedAt");
                int b5 = oi.b(a, "min");
                int b6 = oi.b(a, "max");
                int b7 = oi.b(a, "minuteCount");
                int b8 = oi.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(a.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getLong(b3), a.getLong(b4), a.getInt(b5), a.getInt(b6), a.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(a.getString(b8))));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends lf.b<Integer, DailyHeartRateSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends mi<DailyHeartRateSummary> {
            @DexIgnore
            public Anon1_Level2(ci ciVar, fi fiVar, boolean z, String... strArr) {
                super(ciVar, fiVar, z, strArr);
            }

            @DexIgnore
            @Override // com.fossil.mi
            public List<DailyHeartRateSummary> convertRows(Cursor cursor) {
                int b = oi.b(cursor, GoalTrackingSummary.COLUMN_AVERAGE);
                int b2 = oi.b(cursor, "date");
                int b3 = oi.b(cursor, "createdAt");
                int b4 = oi.b(cursor, "updatedAt");
                int b5 = oi.b(cursor, "min");
                int b6 = oi.b(cursor, "max");
                int b7 = oi.b(cursor, "minuteCount");
                int b8 = oi.b(cursor, "resting");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(cursor.getFloat(b), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(cursor.getString(b2)), cursor.getLong(b3), cursor.getLong(b4), cursor.getInt(b5), cursor.getInt(b6), cursor.getInt(b7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(cursor.getString(b8))));
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.mi<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>' to match base method */
        @Override // com.fossil.lf.b
        public lf<Integer, DailyHeartRateSummary> create() {
            return new Anon1_Level2(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, "daily_heart_rate_summary");
        }
    }

    @DexIgnore
    public HeartRateDailySummaryDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfDailyHeartRateSummary = new Anon1(ciVar);
        this.__preparedStmtOfDeleteAllHeartRateSummaries = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void deleteAllHeartRateSummaries() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllHeartRateSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a3, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = oi.b(a3, "date");
            int b4 = oi.b(a3, "createdAt");
            int b5 = oi.b(a3, "updatedAt");
            int b6 = oi.b(a3, "min");
            int b7 = oi.b(a3, "max");
            int b8 = oi.b(a3, "minuteCount");
            int b9 = oi.b(a3, "resting");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                arrayList.add(new DailyHeartRateSummary(a3.getFloat(b2), this.__dateShortStringConverter.a(a3.getString(b3)), a3.getLong(b4), a3.getLong(b5), a3.getInt(b6), a3.getInt(b7), a3.getInt(b8), this.__restingConverter.a(a3.getString(b9))));
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"daily_heart_rate_summary"}, false, (Callable) new Anon3(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public DailyHeartRateSummary getDailyHeartRateSummary(Date date) {
        fi b = fi.b("SELECT * FROM daily_heart_rate_summary WHERE date = ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        DailyHeartRateSummary dailyHeartRateSummary = null;
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, GoalTrackingSummary.COLUMN_AVERAGE);
            int b3 = oi.b(a2, "date");
            int b4 = oi.b(a2, "createdAt");
            int b5 = oi.b(a2, "updatedAt");
            int b6 = oi.b(a2, "min");
            int b7 = oi.b(a2, "max");
            int b8 = oi.b(a2, "minuteCount");
            int b9 = oi.b(a2, "resting");
            if (a2.moveToFirst()) {
                dailyHeartRateSummary = new DailyHeartRateSummary(a2.getFloat(b2), this.__dateShortStringConverter.a(a2.getString(b3)), a2.getLong(b4), a2.getLong(b5), a2.getInt(b6), a2.getInt(b7), a2.getInt(b8), this.__restingConverter.a(a2.getString(b9)));
            }
            return dailyHeartRateSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public Date getLastDate() {
        fi b = fi.b("SELECT date FROM daily_heart_rate_summary ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Date date = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                date = this.__dateShortStringConverter.a(a.getString(0));
            }
            return date;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public lf.b<Integer, DailyHeartRateSummary> getSummariesDataSource() {
        return new Anon4(fi.b("SELECT * FROM daily_heart_rate_summary ORDER BY date DESC", 0));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(dailyHeartRateSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
