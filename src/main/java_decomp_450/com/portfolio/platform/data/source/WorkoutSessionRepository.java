package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.te5;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public List<WorkoutSessionDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return WorkoutSessionRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutSessionRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionRepository(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchWorkoutSessions$default(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, fb7 fb7, int i3, Object obj) {
        return workoutSessionRepository.fetchWorkoutSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, fb7);
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new WorkoutSessionRepository$cleanUp$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object fetchWorkoutSessions(Date date, Date date2, int i, int i2, fb7<? super zi5<ApiResponse<ServerWorkoutSession>>> fb7) {
        return vh7.a(qj7.b(), new WorkoutSessionRepository$fetchWorkoutSessions$Anon2(this, date, date2, i, i2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWorkoutSessionById(java.lang.String r5, com.fossil.fb7<? super com.portfolio.platform.data.model.diana.workout.WorkoutSession> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r0
            com.fossil.t87.a(r6)
            goto L_0x004b
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r6)
            com.fossil.pg5 r6 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r6.b(r0)
            if (r6 != r1) goto L_0x004b
            return r1
        L_0x004b:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r6 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r6
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r6 = r6.getWorkoutDao()
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r5 = r6.getWorkoutSessionById(r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.getWorkoutSessionById(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWorkoutSessions(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r11 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.ti7 r14 = com.fossil.qj7.b()
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon2 r2 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.getWorkoutSessions(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getWorkoutSessionsPaging(Date date, WorkoutSessionRepository workoutSessionRepository, pj4 pj4, te5.a aVar, fb7<? super Listing<WorkoutSession>> fb7) {
        return vh7.a(qj7.c(), new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(this, workoutSessionRepository, date, pj4, aVar, null), fb7);
    }

    @DexIgnore
    public final Object insertFromDevice(List<WorkoutSession> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new WorkoutSessionRepository$insertFromDevice$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object insertWorkoutTetherScreenShot(String str, String str2, fb7<? super i97> fb7) {
        return vh7.a(qj7.b(), new WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(str, str2, null), fb7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory : this.mSourceDataFactoryList) {
            WorkoutSessionLocalDataSource localDataSource = workoutSessionDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v18, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateWorkoutSession(java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper> r18, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$Anon1 r3 = (com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$Anon1 r3 = new com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 2
            r7 = 1
            r8 = 0
            if (r5 == 0) goto L_0x006f
            if (r5 == r7) goto L_0x0054
            if (r5 != r6) goto L_0x004c
            java.lang.Object r1 = r3.L$5
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r4 = r3.L$4
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r4 = r3.L$3
            com.fossil.ie4 r4 = (com.fossil.ie4) r4
            java.lang.Object r4 = r3.L$2
            com.google.gson.Gson r4 = (com.google.gson.Gson) r4
            java.lang.Object r4 = r3.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r3 = r3.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r3 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r3
            com.fossil.t87.a(r2)
            goto L_0x0166
        L_0x004c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0054:
            java.lang.Object r1 = r3.L$3
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r5 = r3.L$2
            com.google.gson.Gson r5 = (com.google.gson.Gson) r5
            java.lang.Object r9 = r3.L$1
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r3.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r10 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r10
            com.fossil.t87.a(r2)
            r16 = r2
            r2 = r1
            r1 = r9
            r9 = r16
            goto L_0x00e9
        L_0x006f:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.WorkoutSessionRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "updateWorkoutSession: workoutSessionUpdateWrappers = "
            r9.append(r10)
            r9.append(r1)
            java.lang.String r9 = r9.toString()
            r2.d(r5, r9)
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.fossil.od5 r5 = new com.fossil.od5
            r5.<init>()
            r2.b(r5)
            com.google.gson.Gson r5 = r2.a()
            com.fossil.ie4 r2 = new com.fossil.ie4
            r2.<init>()
            r9 = 0
            com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper[] r9 = new com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper[r9]
            java.lang.Object[] r9 = r1.toArray(r9)
            if (r9 == 0) goto L_0x01f2
            com.google.gson.JsonElement r9 = r5.b(r9)
            java.lang.String r10 = "_items"
            r2.a(r10, r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.WorkoutSessionRepository.TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "updateWorkoutSession jsonObject "
            r11.append(r12)
            r11.append(r2)
            java.lang.String r11 = r11.toString()
            r9.d(r10, r11)
            com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$repoResponse$Anon1 r9 = new com.portfolio.platform.data.source.WorkoutSessionRepository$updateWorkoutSession$repoResponse$Anon1
            r9.<init>(r0, r2, r8)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r5
            r3.L$3 = r2
            r3.label = r7
            java.lang.Object r9 = com.fossil.aj5.a(r9, r3)
            if (r9 != r4) goto L_0x00e8
            return r4
        L_0x00e8:
            r10 = r0
        L_0x00e9:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r11 = r9 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x0184
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r12 = r9
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r13 = r12.a()
            if (r13 == 0) goto L_0x017d
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.portfolio.platform.data.source.WorkoutSessionRepository.TAG
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r8 = "updateWorkoutSession - Success -- items="
            r15.append(r8)
            java.lang.Object r8 = r12.a()
            com.portfolio.platform.data.source.remote.ApiResponse r8 = (com.portfolio.platform.data.source.remote.ApiResponse) r8
            java.util.List r8 = r8.get_items()
            r15.append(r8)
            java.lang.String r8 = r15.toString()
            r13.d(r14, r8)
            java.lang.Object r8 = r12.a()
            com.portfolio.platform.data.source.remote.ApiResponse r8 = (com.portfolio.platform.data.source.remote.ApiResponse) r8
            java.util.List r8 = r8.get_items()
            java.util.Iterator r8 = r8.iterator()
        L_0x0131:
            boolean r12 = r8.hasNext()
            if (r12 == 0) goto L_0x0147
            java.lang.Object r12 = r8.next()
            com.portfolio.platform.data.ServerWorkoutSession r12 = (com.portfolio.platform.data.ServerWorkoutSession) r12
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r12 = r12.toWorkoutSession()
            if (r12 == 0) goto L_0x0131
            r11.add(r12)
            goto L_0x0131
        L_0x0147:
            boolean r8 = r11.isEmpty()
            r7 = r7 ^ r8
            if (r7 == 0) goto L_0x0170
            com.fossil.pg5 r7 = com.fossil.pg5.i
            r3.L$0 = r10
            r3.L$1 = r1
            r3.L$2 = r5
            r3.L$3 = r2
            r3.L$4 = r9
            r3.L$5 = r11
            r3.label = r6
            java.lang.Object r2 = r7.b(r3)
            if (r2 != r4) goto L_0x0165
            return r4
        L_0x0165:
            r1 = r11
        L_0x0166:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r2 = r2.getWorkoutDao()
            r2.upsertListWorkoutSession(r1)
            r11 = r1
        L_0x0170:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.WorkoutSessionRepository.TAG
            java.lang.String r3 = "updateWorkoutSession - saveCallResult -- DONE!!!"
            r1.d(r2, r3)
        L_0x017d:
            com.fossil.ko4 r1 = new com.fossil.ko4
            r2 = 0
            r1.<init>(r11, r2, r6, r2)
            return r1
        L_0x0184:
            r2 = r8
            boolean r1 = r9 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x01ec
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.WorkoutSessionRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateWorkoutSession - Failure -- code="
            r4.append(r5)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r5 = r9.a()
            r4.append(r5)
            java.lang.String r5 = ", message="
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r9.c()
            if (r5 == 0) goto L_0x01b6
            java.lang.String r5 = r5.getMessage()
            if (r5 == 0) goto L_0x01b6
            goto L_0x01c2
        L_0x01b6:
            com.portfolio.platform.data.model.ServerError r5 = r9.c()
            if (r5 == 0) goto L_0x01c1
            java.lang.String r5 = r5.getUserMessage()
            goto L_0x01c2
        L_0x01c1:
            r5 = r2
        L_0x01c2:
            if (r5 == 0) goto L_0x01c5
            goto L_0x01c7
        L_0x01c5:
            java.lang.String r5 = ""
        L_0x01c7:
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r3, r4)
            com.fossil.ko4 r1 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            int r4 = r9.a()
            com.portfolio.platform.data.model.ServerError r5 = r9.c()
            if (r5 == 0) goto L_0x01e4
            java.lang.String r8 = r5.getMessage()
            goto L_0x01e5
        L_0x01e4:
            r8 = r2
        L_0x01e5:
            r3.<init>(r4, r8)
            r1.<init>(r3)
            return r1
        L_0x01ec:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        L_0x01f2:
            com.fossil.x87 r1 = new com.fossil.x87
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.Array<T>"
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.updateWorkoutSession(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
