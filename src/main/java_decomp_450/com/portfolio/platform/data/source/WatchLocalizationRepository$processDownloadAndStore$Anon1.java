package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.bj5;
import com.fossil.ee5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.mo7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1", f = "WatchLocalizationRepository.kt", l = {65}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1 extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, String str2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1(this.this$0, this.$url, this.$path, fb7);
        watchLocalizationRepository$processDownloadAndStore$Anon1.p$ = (yi7) obj;
        return watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
        return ((WatchLocalizationRepository$processDownloadAndStore$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 = new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi5 = (zi5) obj;
        if (zi5 instanceof bj5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$p = this.this$0.TAG;
            local.d(access$getTAG$p, "start storing for url: " + this.$url + " to path: " + this.$path);
            ee5 ee5 = ee5.a;
            Object a2 = ((bj5) zi5).a();
            if (a2 == null) {
                ee7.a();
                throw null;
            } else if (ee5.a((mo7) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$p2 = this.this$0.TAG;
            local2.d(access$getTAG$p2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
