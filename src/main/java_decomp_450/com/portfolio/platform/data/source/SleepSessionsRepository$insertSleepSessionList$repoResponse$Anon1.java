package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$repoResponse$1", f = "SleepSessionsRepository.kt", l = {351}, m = "invokeSuspend")
public final class SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ie4 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1(SleepSessionsRepository sleepSessionsRepository, ie4 ie4, fb7 fb7) {
        super(1, fb7);
        this.this$0 = sleepSessionsRepository;
        this.$jsonObject = ie4;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1(this.this$0, this.$jsonObject, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ie4>> fb7) {
        return ((SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            ie4 ie4 = this.$jsonObject;
            this.label = 1;
            obj = access$getMApiService$p.insertSleepSessions(ie4, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
