package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1", f = "GoalTrackingSummaryLocalDataSource.kt", l = {144, 148, 152, 157}, m = "invokeSuspend")
public final class GoalTrackingSummaryLocalDataSource$loadData$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ te5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ te5.d $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$1", f = "GoalTrackingSummaryLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1, fb7 fb7) {
            super(2, fb7);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.$helperCallback.a();
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$2", f = "GoalTrackingSummaryLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ zi5 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1, zi5 zi5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$Anon1;
            this.$data = zi5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, fb7);
            anon2_Level2.p$ = (yi7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon2_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (((yi5) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((yi5) this.$data).d());
                } else if (((yi5) this.$data).c() != null) {
                    ServerError c = ((yi5) this.$data).c();
                    te5.b.a aVar = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingSummaryLocalDataSource$loadData$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, Date date, Date date2, te5.d dVar, te5.b.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = dVar;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1 = new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, fb7);
        goalTrackingSummaryLocalDataSource$loadData$Anon1.p$ = (yi7) obj;
        return goalTrackingSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((GoalTrackingSummaryLocalDataSource$loadData$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00fe  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r11.label
            r2 = 4
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L_0x004c
            if (r1 == r5) goto L_0x0043
            if (r1 == r4) goto L_0x0032
            if (r1 == r3) goto L_0x001d
            if (r1 != r2) goto L_0x0015
            goto L_0x001d
        L_0x0015:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x001d:
            java.lang.Object r0 = r11.L$3
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r0 = r11.L$2
            com.fossil.r87 r0 = (com.fossil.r87) r0
            java.lang.Object r0 = r11.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r11.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r12)
            goto L_0x0121
        L_0x0032:
            java.lang.Object r1 = r11.L$2
            com.fossil.r87 r1 = (com.fossil.r87) r1
            java.lang.Object r4 = r11.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r6 = r11.L$0
            com.fossil.yi7 r6 = (com.fossil.yi7) r6
            com.fossil.t87.a(r12)
            goto L_0x00c0
        L_0x0043:
            java.lang.Object r1 = r11.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r12)
            r6 = r1
            goto L_0x0090
        L_0x004c:
            com.fossil.t87.a(r12)
            com.fossil.yi7 r12 = r11.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "loadData start="
            r6.append(r7)
            java.util.Date r7 = r11.$startDate
            r6.append(r7)
            java.lang.String r7 = ", end="
            r6.append(r7)
            java.util.Date r7 = r11.$endDate
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = "GoalTrackingSummaryLocalDataSource"
            r1.d(r7, r6)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource r1 = r11.this$0
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = r1.mGoalTrackingRepository
            java.util.Date r6 = r11.$startDate
            java.util.Date r7 = r11.$endDate
            r11.L$0 = r12
            r11.label = r5
            java.lang.Object r1 = r1.getPendingGoalTrackingDataList(r6, r7, r11)
            if (r1 != r0) goto L_0x008e
            return r0
        L_0x008e:
            r6 = r12
            r12 = r1
        L_0x0090:
            java.util.List r12 = (java.util.List) r12
            java.util.Date r1 = r11.$startDate
            java.util.Date r7 = r11.$endDate
            com.fossil.r87 r1 = com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt.calculateRangeDownload(r12, r1, r7)
            if (r1 == 0) goto L_0x011c
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource r7 = r11.this$0
            com.portfolio.platform.data.source.GoalTrackingRepository r7 = r7.mGoalTrackingRepository
            java.lang.Object r8 = r1.getFirst()
            java.util.Date r8 = (java.util.Date) r8
            java.lang.Object r9 = r1.getSecond()
            java.util.Date r9 = (java.util.Date) r9
            r11.L$0 = r6
            r11.L$1 = r12
            r11.L$2 = r1
            r11.label = r4
            java.lang.Object r4 = r7.loadSummaries(r8, r9, r11)
            if (r4 != r0) goto L_0x00bd
            return r0
        L_0x00bd:
            r10 = r4
            r4 = r12
            r12 = r10
        L_0x00c0:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r7 = r12 instanceof com.fossil.bj5
            r8 = 0
            if (r7 == 0) goto L_0x00fe
            com.fossil.te5$d r2 = r11.$requestType
            com.fossil.te5$d r7 = com.fossil.te5.d.AFTER
            if (r2 != r7) goto L_0x00e4
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource r2 = r11.this$0
            java.util.List r2 = r2.mRequestAfterQueue
            boolean r2 = r2.isEmpty()
            r2 = r2 ^ r5
            if (r2 == 0) goto L_0x00e4
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource r2 = r11.this$0
            java.util.List r2 = r2.mRequestAfterQueue
            r5 = 0
            r2.remove(r5)
        L_0x00e4:
            com.fossil.tk7 r2 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1$Anon1_Level2 r5 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1$Anon1_Level2
            r5.<init>(r11, r8)
            r11.L$0 = r6
            r11.L$1 = r4
            r11.L$2 = r1
            r11.L$3 = r12
            r11.label = r3
            java.lang.Object r12 = com.fossil.vh7.a(r2, r5, r11)
            if (r12 != r0) goto L_0x0121
            return r0
        L_0x00fe:
            boolean r3 = r12 instanceof com.fossil.yi5
            if (r3 == 0) goto L_0x0121
            com.fossil.tk7 r3 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1$Anon2_Level2 r5 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1$Anon2_Level2
            r5.<init>(r11, r12, r8)
            r11.L$0 = r6
            r11.L$1 = r4
            r11.L$2 = r1
            r11.L$3 = r12
            r11.label = r2
            java.lang.Object r12 = com.fossil.vh7.a(r3, r5, r11)
            if (r12 != r0) goto L_0x0121
            return r0
        L_0x011c:
            com.fossil.te5$b$a r12 = r11.$helperCallback
            r12.a()
        L_0x0121:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
