package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.mf;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource extends mf<Long, GoalTrackingData> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public DateTime latestTrackedTime;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ Date mCurrentDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver; // = new Anon1(this, "goalTrackingRaw", new String[0]);
    @DexIgnore
    public int mOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingDataLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingDataLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = GoalTrackingDataLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "GoalTrackingDataLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingDataLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, pj4 pj4, te5.a aVar) {
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        ee7.b(date, "mCurrentDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCurrentDate = date;
        this.listener = aVar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<GoalTrackingData> getDataInDatabase(int i) {
        return this.mGoalTrackingDatabase.getGoalTrackingDao().getGoalTrackingDataListInitInDate(this.mCurrentDate, i);
    }

    @DexIgnore
    private final ik7 loadData(te5.d dVar, te5.b.a aVar, int i) {
        return xh7.b(zi7.a(qj7.b()), null, null, new GoalTrackingDataLocalDataSource$loadData$Anon1(this, i, aVar, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ ik7 loadData$default(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, te5.d dVar, te5.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return goalTrackingDataLocalDataSource.loadData(dVar, aVar, i);
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadAfter(mf.f<Long> fVar, mf.a<GoalTrackingData> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - currentDate = " + this.mCurrentDate);
        GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
        Date date = this.mCurrentDate;
        DateTime dateTime = this.latestTrackedTime;
        if (dateTime != null) {
            Key key = fVar.a;
            ee7.a((Object) key, "params.key");
            List<GoalTrackingData> goalTrackingDataListAfterInDate = goalTrackingDao.getGoalTrackingDataListAfterInDate(date, dateTime, key.longValue(), fVar.b);
            if (!goalTrackingDataListAfterInDate.isEmpty()) {
                this.latestTrackedTime = ((GoalTrackingData) ea7.f((List) goalTrackingDataListAfterInDate)).getTrackedAt();
            }
            aVar.a(goalTrackingDataListAfterInDate);
            this.mHelper.a(te5.d.AFTER, new GoalTrackingDataLocalDataSource$loadAfter$Anon1(this));
            return;
        }
        ee7.d("latestTrackedTime");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadBefore(mf.f<Long> fVar, mf.a<GoalTrackingData> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadInitial(mf.e<Long> eVar, mf.c<GoalTrackingData> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - mCurrentDate = " + this.mCurrentDate + ' ');
        List<GoalTrackingData> dataInDatabase = getDataInDatabase(eVar.b);
        if (!dataInDatabase.isEmpty()) {
            this.latestTrackedTime = ((GoalTrackingData) ea7.f((List) dataInDatabase)).getTrackedAt();
        }
        cVar.a(dataInDatabase);
        this.mHelper.a(te5.d.INITIAL, new GoalTrackingDataLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(GoalTrackingData goalTrackingData) {
        ee7.b(goalTrackingData, "item");
        return Long.valueOf(goalTrackingData.getUpdatedAt());
    }
}
