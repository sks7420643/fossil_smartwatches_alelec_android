package com.portfolio.platform.data.source.remote;

import com.fossil.de4;
import com.fossil.ee7;
import com.fossil.x87;
import com.fossil.zd7;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRemoteDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "AlarmsRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final de4 intArray2JsonArray(int[] iArr) {
        de4 de4 = new de4();
        if (iArr == null) {
            iArr = new int[0];
        }
        Calendar instance = Calendar.getInstance();
        int length = iArr.length;
        int i = 0;
        while (i < length) {
            instance.set(7, iArr[i]);
            String displayName = instance.getDisplayName(7, 2, Locale.US);
            ee7.a((Object) displayName, "calendar.getDisplayName(\u2026Calendar.LONG, Locale.US)");
            if (displayName != null) {
                String substring = displayName.substring(0, 3);
                ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (substring != null) {
                    String lowerCase = substring.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    de4.a(lowerCase);
                    i++;
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        return de4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteAlarm(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 r10 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1
            r10.<init>(r8, r9, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "deleteAlarm onResponse: response = "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r1 = r1.toString()
            r9.d(r0, r1)
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0079
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x0096
        L_0x0079:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0097
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0096:
            return r9
        L_0x0097:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.deleteAlarm(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteAlarms(java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r9 = r0.L$3
            com.fossil.de4 r9 = (com.fossil.de4) r9
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x0082
        L_0x003a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0042:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            com.fossil.de4 r2 = new com.fossil.de4
            r2.<init>()
            java.util.Iterator r5 = r9.iterator()
        L_0x0053:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0067
            java.lang.Object r6 = r5.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r6 = (com.portfolio.platform.data.source.local.alarm.Alarm) r6
            java.lang.String r6 = r6.getId()
            r2.a(r6)
            goto L_0x0053
        L_0x0067:
            java.lang.String r5 = "_ids"
            r10.a(r5, r2)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 r5 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1
            r5.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r5, r0)
            if (r10 != r1) goto L_0x0082
            return r1
        L_0x0082:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "deleteAlarms onResponse: response = "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r1 = r1.toString()
            r9.d(r0, r1)
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00ac
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x00c9
        L_0x00ac:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00ca
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00c9:
            return r9
        L_0x00ca:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.deleteAlarms(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAlarms(com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm>>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r0
            com.fossil.t87.a(r10)
            goto L_0x0049
        L_0x002e:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0036:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1 r10 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1
            r10.<init>(r9, r3)
            r0.L$0 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "getAlarms onResponse: response = "
            r2.append(r4)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0083
            com.fossil.bj5 r0 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r1 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            if (r1 == 0) goto L_0x007b
            java.util.List r3 = r1.get_items()
        L_0x007b:
            boolean r10 = r10.b()
            r0.<init>(r3, r10)
            goto L_0x00a0
        L_0x0083:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00a1
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r3 = r10.c()
            java.lang.Throwable r4 = r10.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x00a0:
            return r0
        L_0x00a1:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.getAlarms(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertAlarms(java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> r10, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm>>> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$Anon1
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r10 = r0.L$3
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            java.lang.Object r10 = r0.L$2
            com.fossil.de4 r10 = (com.fossil.de4) r10
            java.lang.Object r10 = r0.L$1
            java.util.List r10 = (java.util.List) r10
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r10 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r10
            com.fossil.t87.a(r11)
            goto L_0x00fc
        L_0x003b:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0043:
            com.fossil.t87.a(r11)
            com.fossil.de4 r11 = new com.fossil.de4
            r11.<init>()
            java.util.Iterator r2 = r10.iterator()
        L_0x004f:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x00c0
            java.lang.Object r5 = r2.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r5 = (com.portfolio.platform.data.source.local.alarm.Alarm) r5
            com.fossil.ie4 r6 = new com.fossil.ie4
            r6.<init>()
            int r7 = r5.getHour()
            java.lang.Integer r7 = com.fossil.pb7.a(r7)
            java.lang.String r8 = "hour"
            r6.a(r8, r7)
            int r7 = r5.getMinute()
            java.lang.Integer r7 = com.fossil.pb7.a(r7)
            java.lang.String r8 = "minute"
            r6.a(r8, r7)
            java.lang.String r7 = r5.getUri()
            java.lang.String r8 = "id"
            r6.a(r8, r7)
            boolean r7 = r5.isRepeated()
            java.lang.Boolean r7 = com.fossil.pb7.a(r7)
            java.lang.String r8 = "isRepeated"
            r6.a(r8, r7)
            boolean r7 = r5.isActive()
            java.lang.Boolean r7 = com.fossil.pb7.a(r7)
            java.lang.String r8 = "isActive"
            r6.a(r8, r7)
            java.lang.String r7 = r5.getTitle()
            java.lang.String r8 = "title"
            r6.a(r8, r7)
            java.lang.String r7 = r5.getMessage()
            java.lang.String r8 = "message"
            r6.a(r8, r7)
            int[] r5 = r5.getDays()
            com.fossil.de4 r5 = r9.intArray2JsonArray(r5)
            java.lang.String r7 = "days"
            r6.a(r7, r5)
            r11.a(r6)
            goto L_0x004f
        L_0x00c0:
            com.fossil.ie4 r2 = new com.fossil.ie4
            r2.<init>()
            java.lang.String r5 = "_items"
            r2.a(r5, r11)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "upsertAlarms: body "
            r7.append(r8)
            r7.append(r2)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1 r5 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$upsertAlarms$repoResponse$Anon1
            r5.<init>(r9, r2, r4)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r5, r0)
            if (r11 != r1) goto L_0x00fc
            return r1
        L_0x00fc:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "upsertAlarms onResponse: response = "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r1 = r1.toString()
            r10.d(r0, r1)
            boolean r10 = r11 instanceof com.fossil.bj5
            if (r10 == 0) goto L_0x0136
            com.fossil.bj5 r10 = new com.fossil.bj5
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r11 = r11.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            if (r11 == 0) goto L_0x012f
            java.util.List r11 = r11.get_items()
            goto L_0x0130
        L_0x012f:
            r11 = r4
        L_0x0130:
            r0 = 0
            r1 = 2
            r10.<init>(r11, r0, r1, r4)
            return r10
        L_0x0136:
            boolean r10 = r11 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x0154
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r1 = r11.a()
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return r10
        L_0x0154:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.upsertAlarms(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
