package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory implements Factory<ComplicationLastSettingDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ComplicationLastSettingDao provideComplicationSettingDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        ComplicationLastSettingDao provideComplicationSettingDao = portfolioDatabaseModule.provideComplicationSettingDao(dianaCustomizeDatabase);
        c87.a(provideComplicationSettingDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideComplicationSettingDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ComplicationLastSettingDao get() {
        return provideComplicationSettingDao(this.module, this.dbProvider.get());
    }
}
