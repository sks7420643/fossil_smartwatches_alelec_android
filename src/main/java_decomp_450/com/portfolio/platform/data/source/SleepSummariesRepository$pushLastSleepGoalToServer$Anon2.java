package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
public final class SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<MFSleepSettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $sleepGoal;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2$1", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements gd7<fb7<? super fv7<MFSleepSettings>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new Anon1_Level2(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<MFSleepSettings>> fb7) {
            return ((Anon1_Level2) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = access$getMApiService$p.setSleepSetting(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(SleepSummariesRepository sleepSummariesRepository, int i, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
        this.$sleepGoal = i;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2 = new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this.this$0, this.$sleepGoal, fb7);
        sleepSummariesRepository$pushLastSleepGoalToServer$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<MFSleepSettings>> fb7) {
        return ((SleepSummariesRepository$pushLastSleepGoalToServer$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = SleepSummariesRepository.TAG;
            local.d(access$getTAG$cp, "pushLastSleepGoalToServer sleepGoal=" + this.$sleepGoal);
            ie4 ie4 = new ie4();
            try {
                ie4.a("currentGoalMinutes", pb7.a(this.$sleepGoal));
                TimeZone timeZone = TimeZone.getDefault();
                ee7.a((Object) timeZone, "TimeZone.getDefault()");
                ie4.a("timezoneOffset", pb7.a(timeZone.getRawOffset() / 1000));
            } catch (Exception unused) {
            }
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, ie4, null);
            this.L$0 = yi7;
            this.L$1 = ie4;
            this.label = 1;
            obj = aj5.a(anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            ie4 ie42 = (ie4) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
