package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmsRemoteDataSource mAlarmRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "AlarmsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRepository(AlarmsRemoteDataSource alarmsRemoteDataSource) {
        ee7.b(alarmsRemoteDataSource, "mAlarmRemoteDataSource");
        this.mAlarmRemoteDataSource = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new AlarmsRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object deleteAlarm(Alarm alarm, fb7<? super zi5<? extends Void>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$deleteAlarm$Anon2(this, alarm, null), fb7);
    }

    @DexIgnore
    public final Object downloadAlarms(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new AlarmsRepository$downloadAlarms$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final synchronized Object executePendingRequest(fb7<? super Boolean> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$executePendingRequest$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object findIncomingActiveAlarm(fb7<? super Alarm> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$findIncomingActiveAlarm$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object findNextActiveAlarm(fb7<? super Alarm> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$findNextActiveAlarm$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object getActiveAlarms(fb7<? super List<Alarm>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$getActiveAlarms$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object getAlarmById(String str, fb7<? super Alarm> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$getAlarmById$Anon2(str, null), fb7);
    }

    @DexIgnore
    public final Object getAllAlarmIgnoreDeletePinType(fb7<? super List<Alarm>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object getInComingActiveAlarms(fb7<? super List<Alarm>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$getInComingActiveAlarms$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object insertAlarm(Alarm alarm, fb7<? super zi5<Alarm>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$insertAlarm$Anon2(this, alarm, null), fb7);
    }

    @DexIgnore
    public final Object updateAlarm(Alarm alarm, fb7<? super zi5<Alarm>> fb7) {
        return vh7.a(qj7.b(), new AlarmsRepository$updateAlarm$Anon2(this, alarm, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm r13, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.local.alarm.Alarm>> r14) {
        /*
            r12 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon1 r0 = (com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon1 r0 = new com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon1
            r0.<init>(r12, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            r5 = 0
            r6 = 2
            if (r2 == 0) goto L_0x0062
            if (r2 == r3) goto L_0x004e
            if (r2 != r6) goto L_0x0046
            java.lang.Object r13 = r0.L$5
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r13 = r0.L$4
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r1 = r0.L$3
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r1 = r0.L$2
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r1 = r0.L$1
            com.portfolio.platform.data.source.local.alarm.Alarm r1 = (com.portfolio.platform.data.source.local.alarm.Alarm) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.AlarmsRepository r0 = (com.portfolio.platform.data.source.AlarmsRepository) r0
            com.fossil.t87.a(r14)
            goto L_0x00af
        L_0x0046:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r14 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r14)
            throw r13
        L_0x004e:
            java.lang.Object r13 = r0.L$2
            java.util.ArrayList r13 = (java.util.ArrayList) r13
            java.lang.Object r2 = r0.L$1
            com.portfolio.platform.data.source.local.alarm.Alarm r2 = (com.portfolio.platform.data.source.local.alarm.Alarm) r2
            java.lang.Object r3 = r0.L$0
            com.portfolio.platform.data.source.AlarmsRepository r3 = (com.portfolio.platform.data.source.AlarmsRepository) r3
            com.fossil.t87.a(r14)
            r11 = r14
            r14 = r13
            r13 = r2
            r2 = r11
            goto L_0x007f
        L_0x0062:
            com.fossil.t87.a(r14)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            r14.add(r13)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r2 = r12.mAlarmRemoteDataSource
            r0.L$0 = r12
            r0.L$1 = r13
            r0.L$2 = r14
            r0.label = r3
            java.lang.Object r2 = r2.upsertAlarms(r14, r0)
            if (r2 != r1) goto L_0x007e
            return r1
        L_0x007e:
            r3 = r12
        L_0x007f:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r7 = r2 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x00bf
            r7 = r2
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            java.util.List r7 = (java.util.List) r7
            if (r7 == 0) goto L_0x00b9
            com.fossil.ti7 r8 = com.fossil.qj7.b()
            com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon2$Anon1_Level2 r9 = new com.portfolio.platform.data.source.AlarmsRepository$upsertAlarm$Anon2$Anon1_Level2
            r9.<init>(r7, r4)
            r0.L$0 = r3
            r0.L$1 = r13
            r0.L$2 = r14
            r0.L$3 = r2
            r0.L$4 = r7
            r0.L$5 = r7
            r0.label = r6
            java.lang.Object r13 = com.fossil.vh7.a(r8, r9, r0)
            if (r13 != r1) goto L_0x00ae
            return r1
        L_0x00ae:
            r13 = r7
        L_0x00af:
            com.fossil.bj5 r14 = new com.fossil.bj5
            java.lang.Object r13 = r13.get(r5)
            r14.<init>(r13, r5, r6, r4)
            goto L_0x00dc
        L_0x00b9:
            com.fossil.bj5 r14 = new com.fossil.bj5
            r14.<init>(r13, r5, r6, r4)
            goto L_0x00dc
        L_0x00bf:
            boolean r13 = r2 instanceof com.fossil.yi5
            if (r13 == 0) goto L_0x00dd
            com.fossil.yi5 r14 = new com.fossil.yi5
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            com.portfolio.platform.data.model.ServerError r5 = r2.c()
            java.lang.Throwable r6 = r2.d()
            r7 = 0
            r8 = 0
            r9 = 24
            r10 = 0
            r3 = r14
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
        L_0x00dc:
            return r14
        L_0x00dd:
            com.fossil.p87 r13 = new com.fossil.p87
            r13.<init>()
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository.upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm, com.fossil.fb7):java.lang.Object");
    }
}
