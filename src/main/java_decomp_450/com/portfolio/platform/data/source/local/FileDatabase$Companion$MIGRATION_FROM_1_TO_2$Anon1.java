package com.portfolio.platform.data.source.local;

import com.fossil.li;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends li {
    @DexIgnore
    public FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006f A[Catch:{ Exception -> 0x00d8, all -> 0x00d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b7 A[SYNTHETIC] */
    @Override // com.fossil.li
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void migrate(com.fossil.wi r10) {
        /*
            r9 = this;
            java.lang.String r0 = "MIGRATION_FROM_1_TO_2 - END"
            java.lang.String r1 = "database"
            com.fossil.ee7.b(r10, r1)
            java.lang.String r1 = "FileDatabase"
            java.lang.String r2 = "MIGRATION_FROM_1_TO_2 - START"
            android.util.Log.d(r1, r2)
            r10.beginTransaction()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x00d8 }
            com.portfolio.platform.PortfolioApp r2 = r2.c()     // Catch:{ Exception -> 0x00d8 }
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ Exception -> 0x00d8 }
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r2 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r2, r3)     // Catch:{ Exception -> 0x00d8 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00d8 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00d8 }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x00d8 }
            if (r4 != 0) goto L_0x002f
            r3.mkdirs()     // Catch:{ Exception -> 0x00d8 }
        L_0x002f:
            java.lang.String r3 = "SELECT * FROM localfile"
            android.database.Cursor r3 = r10.query(r3)     // Catch:{ Exception -> 0x00d8 }
            r3.moveToFirst()     // Catch:{ Exception -> 0x00d8 }
        L_0x0038:
            java.lang.String r4 = "cursor"
            com.fossil.ee7.a(r3, r4)     // Catch:{ Exception -> 0x00d8 }
            boolean r4 = r3.isAfterLast()     // Catch:{ Exception -> 0x00d8 }
            if (r4 != 0) goto L_0x00bc
            java.lang.String r4 = "fileName"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r5 = "localUri"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r5 = r3.getString(r5)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r6 = "remoteUrl"
            int r6 = r3.getColumnIndex(r6)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r6 = r3.getString(r6)     // Catch:{ Exception -> 0x00d8 }
            if (r5 == 0) goto L_0x006c
            boolean r7 = com.fossil.mh7.a(r5)     // Catch:{ Exception -> 0x00d8 }
            if (r7 == 0) goto L_0x006a
            goto L_0x006c
        L_0x006a:
            r7 = 0
            goto L_0x006d
        L_0x006c:
            r7 = 1
        L_0x006d:
            if (r7 != 0) goto L_0x00b7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d8 }
            r7.<init>()     // Catch:{ Exception -> 0x00d8 }
            r7.append(r2)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r8 = java.io.File.separator     // Catch:{ Exception -> 0x00d8 }
            r7.append(r8)     // Catch:{ Exception -> 0x00d8 }
            r7.append(r4)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r4 = r7.toString()     // Catch:{ Exception -> 0x00d8 }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00d8 }
            r7.<init>(r5)     // Catch:{ Exception -> 0x00d8 }
            boolean r5 = r7.exists()     // Catch:{ Exception -> 0x00d8 }
            if (r5 == 0) goto L_0x0096
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00d8 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x00d8 }
            r7.renameTo(r5)     // Catch:{ Exception -> 0x00d8 }
        L_0x0096:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d8 }
            r5.<init>()     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r7 = "UPDATE `localfile` SET  `localUri` = '"
            r5.append(r7)     // Catch:{ Exception -> 0x00d8 }
            r5.append(r4)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r4 = "' WHERE `remoteUrl` = '"
            r5.append(r4)     // Catch:{ Exception -> 0x00d8 }
            r5.append(r6)     // Catch:{ Exception -> 0x00d8 }
            r4 = 39
            r5.append(r4)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x00d8 }
            r10.execSQL(r4)     // Catch:{ Exception -> 0x00d8 }
        L_0x00b7:
            r3.moveToNext()     // Catch:{ Exception -> 0x00d8 }
            goto L_0x0038
        L_0x00bc:
            r3.close()     // Catch:{ Exception -> 0x00d8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d8 }
            r2.<init>()     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r3 = "ALTER TABLE `localfile` ADD COLUMN `type` TEXT NOT NULL DEFAULT "
            r2.append(r3)     // Catch:{ Exception -> 0x00d8 }
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE     // Catch:{ Exception -> 0x00d8 }
            r2.append(r3)     // Catch:{ Exception -> 0x00d8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00d8 }
            r10.execSQL(r2)     // Catch:{ Exception -> 0x00d8 }
            goto L_0x00f4
        L_0x00d6:
            r2 = move-exception
            goto L_0x00fe
        L_0x00d8:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00d6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d6 }
            r3.<init>()     // Catch:{ all -> 0x00d6 }
            java.lang.String r4 = "MIGRATION_FROM_1_TO_2 - ERROR: "
            r3.append(r4)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x00d6 }
            r3.append(r2)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x00d6 }
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x00d6 }
        L_0x00f4:
            r10.setTransactionSuccessful()
            r10.endTransaction()
            android.util.Log.d(r1, r0)
            return
        L_0x00fe:
            r10.setTransactionSuccessful()
            r10.endTransaction()
            android.util.Log.d(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1.migrate(com.fossil.wi):void");
    }
}
