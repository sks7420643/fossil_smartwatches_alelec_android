package com.portfolio.platform.data.source.remote;

import com.fossil.ao4;
import com.fossil.bw7;
import com.fossil.dw7;
import com.fossil.ew7;
import com.fossil.fb7;
import com.fossil.fo4;
import com.fossil.fv7;
import com.fossil.hn4;
import com.fossil.hw7;
import com.fossil.ie4;
import com.fossil.iw7;
import com.fossil.jn4;
import com.fossil.jo4;
import com.fossil.jw7;
import com.fossil.mn4;
import com.fossil.mo7;
import com.fossil.mw7;
import com.fossil.nw7;
import com.fossil.rw7;
import com.fossil.un4;
import com.fossil.xv7;
import com.fossil.yn4;
import com.fossil.yv7;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.UserWrapper;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppData;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ApiServiceV2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getDeviceAssets$default(ApiServiceV2 apiServiceV2, int i, int i2, String str, String str2, String str3, String str4, String str5, fb7 fb7, int i3, Object obj) {
            if (obj == null) {
                return apiServiceV2.getDeviceAssets(i, i2, str, str2, str3, str4, (i3 & 64) != 0 ? null : str5, fb7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getDeviceAssets");
        }
    }

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@xv7 ie4 ie4, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@xv7 ie4 ie4, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/block-user")
    Object block(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/cancel-current-user-friend-request")
    Object cancelFriendRequest(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/users/me/challenges")
    Object createChallenge(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @yv7("users/me/alarms/{id}")
    Object deleteAlarm(@mw7("id") String str, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<ie4>>> fb7);

    @DexIgnore
    @yv7("/v2/users/me/challenges/{id}")
    Object deleteChallenge(@mw7("id") String str, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @yv7("users/me/devices/{deviceId}")
    Object deleteDevice(@mw7("deviceId") String str, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@xv7 ie4 ie4, fb7<? super fv7<GoalEvent>> fb7);

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "/v2/users/me/notifications")
    Object deleteNotification(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @yv7("users/me")
    Object deleteUser(fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @bw7
    Object downloadFile(@rw7 String str, fb7<? super fv7<mo7>> fb7);

    @DexIgnore
    @hw7("/v2/users/me/challenges/{id}")
    Object editChallenge(@mw7("id") String str, @xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("rpc/feature-flag/evaluate-flags")
    Object featureFlag(@ew7("X-Active-Device") String str, @ew7("User-Agent") String str2, @xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/list-available-challenges")
    Object fetchAvailableChallenges(fb7<? super fv7<ApiResponse<mn4>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/challenges")
    Object fetchChallengesWithStatus(@nw7("status") String[] strArr, @nw7("limit") Integer num, fb7<? super fv7<ApiResponse<mn4>>> fb7);

    @DexIgnore
    @bw7("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/challenge-histories")
    Object fetchHistoryChallenges(@nw7("limit") int i, @nw7("offset") int i2, fb7<? super fv7<ApiResponse<yn4>>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/list-pending-invitations")
    Object fetchPendingChallenges(fb7<? super fv7<ApiResponse<mn4>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/sent-friend-requests")
    Object fetchReceivedRequestFriends(@nw7("limit") Integer num, fb7<? super fv7<ApiResponse<un4>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/received-friend-requests")
    Object fetchSentRequestFriends(@nw7("limit") Integer num, fb7<? super fv7<ApiResponse<un4>>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/social/search-user-social-profiles")
    Object findFriend(@nw7("keyword") String str, fb7<? super fv7<ApiResponse<un4>>> fb7);

    @DexIgnore
    @iw7("rpc/device/generate-pairing-key")
    Object generatePairingKey(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/users/me/social-profile")
    Object generateSocialProfile(fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/activities")
    Object getActivities(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<Activity>>> fb7);

    @DexIgnore
    @bw7("users/me/activity-settings")
    Object getActivitySetting(fb7<? super fv7<ActivitySettings>> fb7);

    @DexIgnore
    @bw7("users/me/activity-statistic")
    Object getActivityStatistic(fb7<? super fv7<ActivityStatistic>> fb7);

    @DexIgnore
    @bw7("users/me/alarms")
    Object getAlarms(@nw7("limit") int i, fb7<? super fv7<ApiResponse<Alarm>>> fb7);

    @DexIgnore
    @bw7("diana-complication-apps")
    Object getAllComplication(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<Complication>>> fb7);

    @DexIgnore
    @bw7("hybrid-apps")
    Object getAllMicroApp(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<MicroApp>>> fb7);

    @DexIgnore
    @bw7("hybrid-app-variants")
    Object getAllMicroAppVariant(@nw7("serialNumber") String str, @nw7("majorNumber") String str2, @nw7("minorNumber") String str3, fb7<? super fv7<ApiResponse<MicroAppVariant>>> fb7);

    @DexIgnore
    @bw7("diana-pusher-apps")
    Object getAllWatchApp(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<WatchApp>>> fb7);

    @DexIgnore
    @bw7("diana-watch-apps")
    Object getAllWatchAppData(@nw7("appVersion") String str, @nw7("firmwareOSVersion") String str2, fb7<? super fv7<ApiResponse<WatchAppData>>> fb7);

    @DexIgnore
    @bw7("app-categories")
    Object getCategories(fb7<? super fv7<ApiResponse<Category>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/challenges/{id}")
    Object getChallengeById(@mw7("id") String str, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<ie4>>> fb7);

    @DexIgnore
    @bw7("users/me/devices/{deviceId}")
    Object getDevice(@mw7("deviceId") String str, fb7<? super fv7<Device>> fb7);

    @DexIgnore
    @bw7("assets/app-sku-images")
    Object getDeviceAssets(@nw7("size") int i, @nw7("offset") int i2, @nw7("metadata.serialNumber") String str, @nw7("metadata.feature") String str2, @nw7("metadata.resolution") String str3, @nw7("metadata.platform") String str4, @nw7("metadata.fastPairId") String str5, fb7<? super fv7<ApiResponse<ie4>>> fb7);

    @DexIgnore
    @bw7("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@mw7("id") String str, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/devices")
    Object getDevices(fb7<? super fv7<ApiResponse<Device>>> fb7);

    @DexIgnore
    @bw7("/v2/assets/diana-complication-ringstyles")
    Object getDianaComplicationRingStyles(@nw7("metadata.isDefault") boolean z, @nw7("metadata.serialNumber") String str, fb7<? super fv7<ApiResponse<DianaComplicationRingStyle>>> fb7);

    @DexIgnore
    @bw7("users/me/diana-presets")
    Object getDianaPresetList(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<DianaPreset>>> fb7);

    @DexIgnore
    @bw7("diana-recommended-presets")
    Object getDianaRecommendPresetList(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<DianaRecommendPreset>>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/display-players")
    Object getDisplayPlayers(@nw7("challengeIds") List<String> list, fb7<? super fv7<ApiResponse<hn4>>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/list-compact-players-current-user-challenges")
    Object getFocusedPlayers(@nw7("challengeIds") List<String> list, @nw7("numberOfTopPlayers") int i, @nw7("numberOfNearPlayers") int i2, fb7<? super fv7<ApiResponse<hn4>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/friends")
    Object getFriends(@nw7("status") String str, fb7<? super fv7<ApiResponse<un4>>> fb7);

    @DexIgnore
    @bw7("users/me/goal-settings")
    Object getGoalSetting(fb7<? super fv7<GoalSetting>> fb7);

    @DexIgnore
    @bw7("users/me/goal-events")
    Object getGoalTrackingDataList(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<GoalEvent>>> fb7);

    @DexIgnore
    @bw7("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<GoalDailySummary>>> fb7);

    @DexIgnore
    @bw7("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@mw7("date") String str, fb7<? super fv7<GoalDailySummary>> fb7);

    @DexIgnore
    @bw7("users/me/heart-rates")
    Object getHeartRateSamples(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<HeartRate>>> fb7);

    @DexIgnore
    @bw7("users/me/hybrid-presets")
    Object getHybridPresetList(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<HybridPreset>>> fb7);

    @DexIgnore
    @bw7("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<HybridRecommendPreset>>> fb7);

    @DexIgnore
    @bw7("assets/e-label")
    Object getLabel(@nw7("metadata.serialNumber") String str, fb7<? super fv7<ApiResponse<Label>>> fb7);

    @DexIgnore
    @bw7("users/me/devices/latest-active")
    Object getLastActiveDevice(fb7<? super fv7<Device>> fb7);

    @DexIgnore
    @bw7("assets/watch-params")
    Object getLatestWatchParams(@nw7("metadata.serialNumber") String str, @nw7("metadata.version.major") int i, @nw7("sortBy") String str2, @nw7("offset") int i2, @nw7("limit") int i3, fb7<? super fv7<ApiResponse<WatchParameterResponse>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/social-profile")
    Object getMySocialProfile(fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/challenges/{id}/pending-players")
    Object getPendingPlayers(@mw7("id") String str, @nw7("limit") Integer num, fb7<? super fv7<ApiResponse<un4>>> fb7);

    @DexIgnore
    @bw7("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@nw7("age") int i, @nw7("weightInGrams") int i2, @nw7("heightInCentimeters") int i3, @nw7("gender") String str, fb7<? super fv7<ActivityRecommendedGoals>> fb7);

    @DexIgnore
    @bw7("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@nw7("age") int i, @nw7("weightInGrams") int i2, @nw7("heightInCentimeters") int i3, @nw7("gender") String str, fb7<? super fv7<SleepRecommendedGoal>> fb7);

    @DexIgnore
    @bw7("server-settings")
    Object getServerSettingList(@nw7("limit") int i, @nw7("offset") int i2, fb7<? super fv7<ServerSettingList>> fb7);

    @DexIgnore
    @bw7("skus")
    Object getSkus(@nw7("limit") int i, @nw7("offset") int i2, fb7<? super fv7<ApiResponse<SKUModel>>> fb7);

    @DexIgnore
    @bw7("users/me/sleep-sessions")
    Object getSleepSessions(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/sleep-settings")
    Object getSleepSetting(fb7<? super fv7<MFSleepSettings>> fb7);

    @DexIgnore
    @bw7("users/me/sleep-statistic")
    Object getSleepStatistic(fb7<? super fv7<SleepStatistic>> fb7);

    @DexIgnore
    @bw7("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/activity-daily-summaries")
    Object getSummaries(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/get-current-user-sync-status-data")
    Object getSyncStatusData(fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@xv7 TrafficRequest trafficRequest, fb7<? super fv7<TrafficResponse>> fb7);

    @DexIgnore
    @bw7("users/me/profile")
    Object getUser(fb7<? super fv7<UserWrapper>> fb7);

    @DexIgnore
    @bw7("users/me/settings")
    Object getUserSettings(fb7<? super fv7<UserSettings>> fb7);

    @DexIgnore
    @bw7("/v2/diana-watch-faces")
    Object getWatchFaces(@nw7("serialNumber") String str, fb7<? super fv7<ApiResponse<WatchFace>>> fb7);

    @DexIgnore
    @bw7("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@nw7("metadata.locale") String str, fb7<? super fv7<ApiResponse<WatchLocalization>>> fb7);

    @DexIgnore
    @bw7("weather-info")
    Object getWeather(@nw7("lat") String str, @nw7("lng") String str2, @nw7("temperatureUnit") String str3, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("users/me/workout-sessions")
    Object getWorkoutSessions(@nw7("startDate") String str, @nw7("endDate") String str2, @nw7("offset") int i, @nw7("limit") int i2, fb7<? super fv7<ApiResponse<ServerWorkoutSession>>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/workout-settings")
    Object getWorkoutSettingList(fb7<? super fv7<RemoteWorkoutSetting>> fb7);

    @DexIgnore
    @iw7("users/me/activities")
    Object insertActivities(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<Activity>>> fb7);

    @DexIgnore
    @iw7("users/me/fitness-files")
    Object insertFitnessDataFiles(@xv7 ApiResponse<ServerFitnessDataWrapper> apiResponse, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("users/me/goal-events")
    Object insertGoalTrackingDataList(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @jw7("users/me/installations")
    Object insertInstallation(@xv7 Installation installation, fb7<? super fv7<Installation>> fb7);

    @DexIgnore
    @iw7("users/me/sleep-sessions")
    Object insertSleepSessions(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/challenges/{id}/players")
    Object invitedPlayers(@mw7("id") String str, @nw7("status") String[] strArr, @nw7("limit") Integer num, fb7<? super fv7<ApiResponse<jn4>>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/join-challenge")
    Object joinChallenge(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/leave-challenge")
    Object leaveChallenge(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @jw7("users/me/devices")
    Object linkDevice(@xv7 Device device, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @bw7("/v2/users/me/notifications")
    Object notifications(@nw7("limit") Integer num, fb7<? super fv7<ApiResponse<ao4>>> fb7);

    @DexIgnore
    @iw7("users/me/notification-clients")
    Object pushFCMToken(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/player/push-current-user-step-data")
    Object pushStepData(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("/v2/rpc/challenges/list-pending-or-available-challenges")
    Object recommendedChallenges(fb7<? super fv7<ApiResponse<jo4>>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/rematch-challenge")
    Object rematchChallenge(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @dw7(hasBody = true, method = "DELETE", path = "users/me/notification-clients")
    Object removeFCMToken(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @jw7("users/me/diana-presets")
    Object replaceDianaPresetList(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<DianaPreset>>> fb7);

    @DexIgnore
    @jw7("users/me/hybrid-presets")
    Object replaceHybridPresetList(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<HybridPreset>>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/update-current-user-friend-request")
    Object respondFriendRequest(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/respond-invitation")
    Object respondInvitation(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/invite")
    Object sendInvitation(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/add-current-user-friend")
    Object sendRequest(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @hw7("users/me/sleep-settings")
    Object setSleepSetting(@xv7 ie4 ie4, fb7<? super fv7<MFSleepSettings>> fb7);

    @DexIgnore
    @jw7("/v2/users/me/social-profile")
    Object socialId(@xv7 ie4 ie4, fb7<? super fv7<fo4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/challenges/start-challenge")
    Object startChallenge(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/unfriend-current-user")
    Object unFriend(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @iw7("/v2/rpc/social/friend/unblock-user")
    Object unblock(@xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @hw7("users/me/activity-settings")
    Object updateActivitySetting(@xv7 ie4 ie4, fb7<? super fv7<ActivitySettings>> fb7);

    @DexIgnore
    @hw7("users/me/devices/{deviceId}")
    Object updateDevice(@mw7("deviceId") String str, @xv7 Device device, fb7<? super fv7<Void>> fb7);

    @DexIgnore
    @hw7("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@mw7("id") String str, @xv7 ie4 ie4, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @hw7("users/me/profile")
    Object updateUser(@xv7 ie4 ie4, fb7<? super fv7<UserWrapper>> fb7);

    @DexIgnore
    @hw7("users/me/settings")
    Object updateUserSetting(@xv7 ie4 ie4, fb7<? super fv7<UserSettings>> fb7);

    @DexIgnore
    @hw7("users/me/workout-sessions")
    Object updateWorkoutSessions(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<ServerWorkoutSession>>> fb7);

    @DexIgnore
    @hw7("users/me/alarms")
    Object upsertAlarms(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<Alarm>>> fb7);

    @DexIgnore
    @hw7("users/me/diana-presets")
    Object upsertDianaPresetList(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<DianaPreset>>> fb7);

    @DexIgnore
    @hw7("users/me/goal-settings")
    Object upsertGoalSetting(@xv7 ie4 ie4, fb7<? super fv7<GoalSetting>> fb7);

    @DexIgnore
    @hw7("users/me/hybrid-presets")
    Object upsertHybridPresetList(@xv7 ie4 ie4, fb7<? super fv7<ApiResponse<HybridPreset>>> fb7);

    @DexIgnore
    @jw7("/v2/users/me/workout-settings")
    Object upsertWorkoutSetting(@xv7 ie4 ie4, fb7<? super fv7<RemoteWorkoutSetting>> fb7);
}
