package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge5;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.te5;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "SummariesRepository";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ ge5 mFitnessHelper;
    @DexIgnore
    public List<ActivitySummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public SummariesRepository(ApiServiceV2 apiServiceV2, ge5 ge5) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        ee7.b(ge5, "mFitnessHelper");
        this.mApiServiceV2 = apiServiceV2;
        this.mFitnessHelper = ge5;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SummariesRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, fb7<? super zi5<ActivityRecommendedGoals>> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$downloadRecommendedGoals$Anon2(this, i, i2, i3, str, null), fb7);
    }

    @DexIgnore
    public final Object fetchActivitySettings(fb7<? super zi5<ActivitySettings>> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$fetchActivitySettings$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object fetchActivityStatistic(fb7<? super ActivityStatistic> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$fetchActivityStatistic$Anon2(this, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getActivitySettings(com.fossil.fb7<? super com.portfolio.platform.data.model.room.fitness.ActivitySettings> r12) {
        /*
            r11 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$Anon1
            r0.<init>(r11, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L_0x0055
            if (r2 == r5) goto L_0x004d
            if (r2 == r4) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r1 = r0.L$1
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r0 = (com.portfolio.platform.data.source.SummariesRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00fd
        L_0x0039:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0041:
            java.lang.Object r2 = r0.L$1
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r4 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r4 = (com.portfolio.platform.data.source.SummariesRepository) r4
            com.fossil.t87.a(r12)
            goto L_0x00b0
        L_0x004d:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r2 = (com.portfolio.platform.data.source.SummariesRepository) r2
            com.fossil.t87.a(r12)
            goto L_0x0069
        L_0x0055:
            com.fossil.t87.a(r12)
            com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$response$Anon1 r12 = new com.portfolio.platform.data.source.SummariesRepository$getActivitySettings$response$Anon1
            r12.<init>(r11, r6)
            r0.L$0 = r11
            r0.label = r5
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0068
            return r1
        L_0x0068:
            r2 = r11
        L_0x0069:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r5 = r12 instanceof com.fossil.bj5
            java.lang.String r7 = "SummariesRepository"
            if (r5 == 0) goto L_0x00b7
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "getActivitySettings success: "
            r8.append(r9)
            r9 = r12
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r10 = r9.a()
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r10 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r10
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r5.d(r7, r8)
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            java.lang.Object r7 = r9.a()
            if (r7 == 0) goto L_0x00b3
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r7 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r7
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r4
            java.lang.Object r4 = r2.saveActivitySettingsToDB(r5, r7, r0)
            if (r4 != r1) goto L_0x00ae
            return r1
        L_0x00ae:
            r4 = r2
            r2 = r12
        L_0x00b0:
            r12 = r2
            r2 = r4
            goto L_0x00ee
        L_0x00b3:
            com.fossil.ee7.a()
            throw r6
        L_0x00b7:
            boolean r4 = r12 instanceof com.fossil.yi5
            if (r4 == 0) goto L_0x00ee
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "getActivitySettings fail: code="
            r5.append(r8)
            r8 = r12
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r9 = r8.a()
            r5.append(r9)
            java.lang.String r9 = " message="
            r5.append(r9)
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00e4
            java.lang.String r6 = r8.getMessage()
        L_0x00e4:
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.d(r7, r5)
        L_0x00ee:
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r3
            java.lang.Object r12 = r4.b(r0)
            if (r12 != r1) goto L_0x00fd
            return r1
        L_0x00fd:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r12 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r12
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r12 = r12.activitySummaryDao()
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r12 = r12.getActivitySetting()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getActivitySettings(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<qx6<ActivityStatistic>> getActivityStatistic(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getActivityStatistic - shouldFetch=" + z);
        return new SummariesRepository$getActivityStatistic$Anon1(this, z).asLiveData();
    }

    @DexIgnore
    public final Object getActivityStatisticAwait(fb7<? super ActivityStatistic> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$getActivityStatisticAwait$Anon2(this, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object getActivityStatisticDB(com.fossil.fb7<? super com.portfolio.platform.data.ActivityStatistic> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticDB$Anon1
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r0 = (com.portfolio.platform.data.source.SummariesRepository) r0
            com.fossil.t87.a(r6)
            goto L_0x0052
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = "SummariesRepository"
            java.lang.String r4 = "getActivityStatisticDB"
            r6.d(r2, r4)
            com.fossil.pg5 r6 = com.fossil.pg5.i
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.b(r0)
            if (r6 != r1) goto L_0x0052
            return r1
        L_0x0052:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r6 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r6
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r6 = r6.activitySummaryDao()
            com.portfolio.platform.data.ActivityStatistic r6 = r6.getActivityStatistic()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getActivityStatisticDB(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getCurrentActivitySettings(fb7<? super ActivitySettings> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$getCurrentActivitySettings$Anon2(null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r11 = (com.portfolio.platform.data.source.SummariesRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2 r2 = new com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSummariesPaging(FitnessDataRepository fitnessDataRepository, Date date, pj4 pj4, te5.a aVar, fb7<? super Listing<ActivitySummary>> fb7) {
        return vh7.a(qj7.c(), new SummariesRepository$getSummariesPaging$Anon2(this, date, fitnessDataRepository, pj4, aVar, null), fb7);
    }

    @DexIgnore
    public final Object getSummary(Date date, fb7<? super LiveData<qx6<ActivitySummary>>> fb7) {
        return vh7.a(qj7.c(), new SummariesRepository$getSummary$Anon2(this, date, null), fb7);
    }

    @DexIgnore
    public final Object insertFromDevice(List<ActivitySummary> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SummariesRepository$insertFromDevice$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object loadSummaries(Date date, Date date2, fb7<? super zi5<ie4>> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$loadSummaries$Anon2(this, date, date2, null), fb7);
    }

    @DexIgnore
    public final Object pushActivitySettingsToServer(ActivitySettings activitySettings, fb7<? super fv7<ActivitySettings>> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$pushActivitySettingsToServer$Anon2(this, activitySettings, null), fb7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (ActivitySummaryDataSourceFactory activitySummaryDataSourceFactory : this.mSourceFactoryList) {
            ActivitySummaryLocalDataSource localDataSource = activitySummaryDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0170 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0193 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveActivitySettingsToDB(java.util.Date r32, com.portfolio.platform.data.model.room.fitness.ActivitySettings r33, com.fossil.fb7<? super com.fossil.i97> r34) {
        /*
            r31 = this;
            r0 = r31
            r1 = r32
            r2 = r34
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.SummariesRepository$saveActivitySettingsToDB$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.SummariesRepository$saveActivitySettingsToDB$Anon1 r3 = (com.portfolio.platform.data.source.SummariesRepository$saveActivitySettingsToDB$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.SummariesRepository$saveActivitySettingsToDB$Anon1 r3 = new com.portfolio.platform.data.source.SummariesRepository$saveActivitySettingsToDB$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 3
            r7 = 2
            r8 = 1
            if (r5 == 0) goto L_0x0078
            if (r5 == r8) goto L_0x0063
            if (r5 == r7) goto L_0x004e
            if (r5 != r6) goto L_0x0046
            java.lang.Object r1 = r3.L$3
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r1
            java.lang.Object r4 = r3.L$2
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r4 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r4
            java.lang.Object r4 = r3.L$1
            java.util.Date r4 = (java.util.Date) r4
            java.lang.Object r3 = r3.L$0
            com.portfolio.platform.data.source.SummariesRepository r3 = (com.portfolio.platform.data.source.SummariesRepository) r3
            com.fossil.t87.a(r2)
            goto L_0x0194
        L_0x0046:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004e:
            java.lang.Object r1 = r3.L$3
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r1
            java.lang.Object r5 = r3.L$2
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r5 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r5
            java.lang.Object r7 = r3.L$1
            java.util.Date r7 = (java.util.Date) r7
            java.lang.Object r8 = r3.L$0
            com.portfolio.platform.data.source.SummariesRepository r8 = (com.portfolio.platform.data.source.SummariesRepository) r8
            com.fossil.t87.a(r2)
            goto L_0x0178
        L_0x0063:
            java.lang.Object r1 = r3.L$2
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r1
            java.lang.Object r5 = r3.L$1
            java.util.Date r5 = (java.util.Date) r5
            java.lang.Object r9 = r3.L$0
            com.portfolio.platform.data.source.SummariesRepository r9 = (com.portfolio.platform.data.source.SummariesRepository) r9
            com.fossil.t87.a(r2)
            r30 = r5
            r5 = r1
            r1 = r30
            goto L_0x00d4
        L_0x0078:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r9 = "saveActivitySettingsToDB - date="
            r5.append(r9)
            r5.append(r1)
            java.lang.String r9 = ", stepGoal="
            r5.append(r9)
            int r9 = r33.getCurrentStepGoal()
            r5.append(r9)
            java.lang.String r9 = ", "
            r5.append(r9)
            java.lang.String r9 = "caloriesGoal="
            r5.append(r9)
            int r9 = r33.getCurrentCaloriesGoal()
            r5.append(r9)
            java.lang.String r9 = ", activeTimeGoal="
            r5.append(r9)
            int r9 = r33.getCurrentActiveTimeGoal()
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            java.lang.String r9 = "SummariesRepository"
            r2.d(r9, r5)
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r3.L$0 = r0
            r3.L$1 = r1
            r5 = r33
            r3.L$2 = r5
            r3.label = r8
            java.lang.Object r2 = r2.b(r3)
            if (r2 != r4) goto L_0x00d3
            return r4
        L_0x00d3:
            r9 = r0
        L_0x00d4:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r2 = r2.activitySummaryDao()
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r2 = r2.getActivitySummary(r1)
            if (r2 != 0) goto L_0x0149
            org.joda.time.DateTime r2 = new org.joda.time.DateTime
            r2.<init>()
            org.joda.time.DateTimeZone r10 = r2.getZone()
            java.util.TimeZone r10 = r10.toTimeZone()
            int r12 = r2.getYear()
            int r13 = r2.getMonthOfYear()
            int r14 = r2.getDayOfMonth()
            java.lang.String r11 = "timeZone"
            com.fossil.ee7.a(r10, r11)
            java.lang.String r15 = r10.getID()
            java.util.Date r2 = r2.toDate()
            boolean r2 = r10.inDaylightTime(r2)
            r23 = 0
            if (r2 == 0) goto L_0x0113
            int r2 = r10.getDSTSavings()
            goto L_0x0114
        L_0x0113:
            r2 = 0
        L_0x0114:
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r10 = new com.portfolio.platform.data.model.room.fitness.ActivitySummary
            r11 = r10
            java.lang.Integer r16 = com.fossil.pb7.a(r2)
            r17 = 0
            r19 = 0
            r21 = 0
            java.lang.Integer[] r2 = new java.lang.Integer[r6]
            java.lang.Integer r24 = com.fossil.pb7.a(r23)
            r2[r23] = r24
            java.lang.Integer r24 = com.fossil.pb7.a(r23)
            r2[r8] = r24
            java.lang.Integer r8 = com.fossil.pb7.a(r23)
            r2[r7] = r8
            java.util.List r23 = com.fossil.w97.d(r2)
            r24 = 0
            r25 = 0
            r26 = 0
            r27 = 0
            r28 = 7680(0x1e00, float:1.0762E-41)
            r29 = 0
            r11.<init>(r12, r13, r14, r15, r16, r17, r19, r21, r23, r24, r25, r26, r27, r28, r29)
            r2 = r10
        L_0x0149:
            int r8 = r5.getCurrentStepGoal()
            r2.setStepGoal(r8)
            int r8 = r5.getCurrentCaloriesGoal()
            r2.setCaloriesGoal(r8)
            int r8 = r5.getCurrentActiveTimeGoal()
            r2.setActiveTimeGoal(r8)
            com.fossil.pg5 r8 = com.fossil.pg5.i
            r3.L$0 = r9
            r3.L$1 = r1
            r3.L$2 = r5
            r3.L$3 = r2
            r3.label = r7
            java.lang.Object r7 = r8.b(r3)
            if (r7 != r4) goto L_0x0171
            return r4
        L_0x0171:
            r8 = r9
            r30 = r7
            r7 = r1
            r1 = r2
            r2 = r30
        L_0x0178:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r2 = r2.activitySummaryDao()
            r2.upsertActivitySettings(r5)
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r3.L$0 = r8
            r3.L$1 = r7
            r3.L$2 = r5
            r3.L$3 = r1
            r3.label = r6
            java.lang.Object r2 = r2.b(r3)
            if (r2 != r4) goto L_0x0194
            return r4
        L_0x0194:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r2 = r2.activitySummaryDao()
            r2.upsertActivitySummary(r1)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.saveActivitySettingsToDB(java.util.Date, com.portfolio.platform.data.model.room.fitness.ActivitySettings, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateActivitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.room.fitness.ActivitySettings>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1 r0 = new com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 2
            r5 = 1
            java.lang.String r6 = "SummariesRepository"
            if (r2 == 0) goto L_0x005c
            if (r2 == r5) goto L_0x0048
            if (r2 != r4) goto L_0x0040
            java.lang.Object r11 = r0.L$3
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            java.lang.Object r1 = r0.L$2
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r0.L$1
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r0 = (com.portfolio.platform.data.source.SummariesRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00e3
        L_0x0040:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0048:
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r2 = r0.L$1
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r2 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r2
            java.lang.Object r5 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository r5 = (com.portfolio.platform.data.source.SummariesRepository) r5
            com.fossil.t87.a(r12)
            r9 = r12
            r12 = r11
            r11 = r2
            r2 = r9
            goto L_0x00ac
        L_0x005c:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r7 = "updateActivitySettings - settings="
            r2.append(r7)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            r12.d(r6, r2)
            com.fossil.be4 r12 = new com.fossil.be4
            r12.<init>()
            com.fossil.od5 r2 = new com.fossil.od5
            r2.<init>()
            r12.b(r2)
            com.google.gson.Gson r12 = r12.a()
            com.google.gson.JsonElement r12 = r12.b(r11)
            java.lang.String r2 = "GsonBuilder()\n          \u2026sonTree(activitySettings)"
            com.fossil.ee7.a(r12, r2)
            com.fossil.ie4 r12 = r12.d()
            com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$response$Anon1 r2 = new com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$response$Anon1
            r2.<init>(r10, r12, r3)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r5
            java.lang.Object r2 = com.fossil.aj5.a(r2, r0)
            if (r2 != r1) goto L_0x00ab
            return r1
        L_0x00ab:
            r5 = r10
        L_0x00ac:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r7 = r2 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x00e5
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "updateActivitySettings response = "
            r7.append(r8)
            r7.append(r2)
            java.lang.String r7 = r7.toString()
            r3.d(r6, r7)
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            r0.L$0 = r5
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r2
            r0.label = r4
            java.lang.Object r11 = r5.saveActivitySettingsToDB(r3, r11, r0)
            if (r11 != r1) goto L_0x00e2
            return r1
        L_0x00e2:
            r11 = r2
        L_0x00e3:
            r2 = r11
            goto L_0x011c
        L_0x00e5:
            boolean r11 = r2 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x011c
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "updateActivitySettings code="
            r12.append(r0)
            r0 = r2
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r1 = r0.a()
            r12.append(r1)
            java.lang.String r1 = " message="
            r12.append(r1)
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x0112
            java.lang.String r3 = r0.getMessage()
        L_0x0112:
            r12.append(r3)
            java.lang.String r12 = r12.toString()
            r11.d(r6, r12)
        L_0x011c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.updateActivitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object upsertRecommendGoals(ActivityRecommendedGoals activityRecommendedGoals, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SummariesRepository$upsertRecommendGoals$Anon2(activityRecommendedGoals, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object getSummary(Calendar calendar, fb7<? super ActivitySummary> fb7) {
        return vh7.a(qj7.b(), new SummariesRepository$getSummary$Anon4(calendar, null), fb7);
    }
}
