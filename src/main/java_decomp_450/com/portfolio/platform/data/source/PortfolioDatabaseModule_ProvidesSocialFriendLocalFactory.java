package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.vn4;
import com.fossil.vo4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory implements Factory<vo4> {
    @DexIgnore
    public /* final */ Provider<vn4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<vn4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<vn4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static vo4 providesSocialFriendLocal(PortfolioDatabaseModule portfolioDatabaseModule, vn4 vn4) {
        vo4 providesSocialFriendLocal = portfolioDatabaseModule.providesSocialFriendLocal(vn4);
        c87.a(providesSocialFriendLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vo4 get() {
        return providesSocialFriendLocal(this.module, this.daoProvider.get());
    }
}
