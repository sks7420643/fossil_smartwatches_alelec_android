package com.portfolio.platform.data.source.remote;

import com.fossil.bw7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ie4;
import com.fossil.nw7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GoogleApiService {
    @DexIgnore
    @bw7("geocode/json")
    Object getAddress(@nw7("latlng") String str, @nw7("language") String str2, fb7<? super fv7<ie4>> fb7);

    @DexIgnore
    @bw7("geocode/json")
    Object getAddressWithType(@nw7("latlng") String str, @nw7("result_type") String str2, fb7<? super fv7<ie4>> fb7);
}
