package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllWatchApp(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.diana.WatchApp>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$Anon1 r0 = (com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$Anon1 r0 = new com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0031:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0039:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource$getAllWatchApp$response$Anon1
            r2 = 0
            r10.<init>(r8, r9, r2)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x007b
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x0071
            java.util.List r0 = r0.get_items()
            if (r0 == 0) goto L_0x0071
            boolean r0 = r9.addAll(r0)
            com.fossil.pb7.a(r0)
        L_0x0071:
            com.fossil.bj5 r0 = new com.fossil.bj5
            boolean r10 = r10.b()
            r0.<init>(r9, r10)
            goto L_0x0098
        L_0x007b:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0099
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0098:
            return r0
        L_0x0099:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource.getAllWatchApp(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
