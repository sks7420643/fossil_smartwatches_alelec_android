package com.portfolio.platform.data.source;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDatabase_Impl extends UserSettingDatabase {
    @DexIgnore
    public volatile UserSettingDao _userSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `userSettings` (`isShowGoalRing` INTEGER NOT NULL, `acceptedLocationDataSharing` TEXT NOT NULL, `acceptedPrivacies` TEXT NOT NULL, `acceptedTermsOfService` TEXT NOT NULL, `uid` TEXT NOT NULL, `id` TEXT, `isLatestLocationDataSharingAccepted` INTEGER NOT NULL, `isLatestPrivacyAccepted` INTEGER NOT NULL, `isLatestTermsOfServiceAccepted` INTEGER NOT NULL, `latestLocationDataSharingVersion` TEXT, `latestPrivacyVersion` TEXT, `latestTermsOfServiceVersion` TEXT, `startDayOfWeek` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uid`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'acf19a46886838b5899188682e5ab582')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `userSettings`");
            if (((ci) UserSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserSettingDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) UserSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserSettingDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) UserSettingDatabase_Impl.this).mDatabase = wiVar;
            UserSettingDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) UserSettingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserSettingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserSettingDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("isShowGoalRing", new ti.a("isShowGoalRing", "INTEGER", true, 0, null, 1));
            hashMap.put("acceptedLocationDataSharing", new ti.a("acceptedLocationDataSharing", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedPrivacies", new ti.a("acceptedPrivacies", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedTermsOfService", new ti.a("acceptedTermsOfService", "TEXT", true, 0, null, 1));
            hashMap.put("uid", new ti.a("uid", "TEXT", true, 1, null, 1));
            hashMap.put("id", new ti.a("id", "TEXT", false, 0, null, 1));
            hashMap.put("isLatestLocationDataSharingAccepted", new ti.a("isLatestLocationDataSharingAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestPrivacyAccepted", new ti.a("isLatestPrivacyAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestTermsOfServiceAccepted", new ti.a("isLatestTermsOfServiceAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("latestLocationDataSharingVersion", new ti.a("latestLocationDataSharingVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestPrivacyVersion", new ti.a("latestPrivacyVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestTermsOfServiceVersion", new ti.a("latestTermsOfServiceVersion", "TEXT", false, 0, null, 1));
            hashMap.put("startDayOfWeek", new ti.a("startDayOfWeek", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("userSettings", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "userSettings");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "userSettings(com.portfolio.platform.data.model.UserSettings).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `userSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "userSettings");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "acf19a46886838b5899188682e5ab582", "c5f82b465e65adc1cedba3c9f8395495");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDatabase
    public UserSettingDao userSettingDao() {
        UserSettingDao userSettingDao;
        if (this._userSettingDao != null) {
            return this._userSettingDao;
        }
        synchronized (this) {
            if (this._userSettingDao == null) {
                this._userSettingDao = new UserSettingDao_Impl(this);
            }
            userSettingDao = this._userSettingDao;
        }
        return userSettingDao;
    }
}
