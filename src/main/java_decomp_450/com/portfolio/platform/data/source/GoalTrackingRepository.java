package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ch5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.te5;
import com.fossil.vh7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zd7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ ch5 mSharedPreferencesManager;
    @DexIgnore
    public List<GoalTrackingDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public List<GoalTrackingSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingGoalTrackingDataListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<GoalTrackingData> list);
    }

    /*
    static {
        String simpleName = GoalTrackingRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "GoalTrackingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingRepository(UserRepository userRepository, ch5 ch5, ApiServiceV2 apiServiceV2) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mUserRepository = userRepository;
        this.mSharedPreferencesManager = ch5;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object loadGoalTrackingDataList$default(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, fb7 fb7, int i3, Object obj) {
        return goalTrackingRepository.loadGoalTrackingDataList(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, fb7);
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new GoalTrackingRepository$cleanUp$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object delete(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r17, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r18) {
        /*
            r16 = this;
            r1 = r16
            r0 = r18
            boolean r2 = r0 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$delete$Anon1
            if (r2 == 0) goto L_0x0017
            r2 = r0
            com.portfolio.platform.data.source.GoalTrackingRepository$delete$Anon1 r2 = (com.portfolio.platform.data.source.GoalTrackingRepository$delete$Anon1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.GoalTrackingRepository$delete$Anon1 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$delete$Anon1
            r2.<init>(r1, r0)
        L_0x001c:
            java.lang.Object r0 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 3
            r6 = 0
            r7 = 0
            r8 = 2
            r9 = 1
            if (r4 == 0) goto L_0x0088
            if (r4 == r9) goto L_0x0073
            if (r4 == r8) goto L_0x005a
            if (r4 != r5) goto L_0x0052
            java.lang.Object r3 = r2.L$6
            java.lang.reflect.Type r3 = (java.lang.reflect.Type) r3
            java.lang.Object r3 = r2.L$5
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r3 = r2.L$4
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r4 = r2.L$3
            com.fossil.ie4 r4 = (com.fossil.ie4) r4
            java.lang.Object r4 = r2.L$2
            com.fossil.de4 r4 = (com.fossil.de4) r4
            java.lang.Object r4 = r2.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r0)
            goto L_0x022f
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x005a:
            java.lang.Object r3 = r2.L$4
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r3 = r2.L$3
            com.fossil.ie4 r3 = (com.fossil.ie4) r3
            java.lang.Object r3 = r2.L$2
            com.fossil.de4 r3 = (com.fossil.de4) r3
            java.lang.Object r3 = r2.L$1
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r0)
            goto L_0x0144
        L_0x0073:
            java.lang.Object r4 = r2.L$3
            com.fossil.ie4 r4 = (com.fossil.ie4) r4
            java.lang.Object r10 = r2.L$2
            com.fossil.de4 r10 = (com.fossil.de4) r10
            java.lang.Object r11 = r2.L$1
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r12 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r12 = (com.portfolio.platform.data.source.GoalTrackingRepository) r12
            com.fossil.t87.a(r0)
            goto L_0x010d
        L_0x0088:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "deleteGoalTrackingDataList: sampleRawList ="
            r10.append(r11)
            int r11 = r17.size()
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r0.d(r4, r10)
            com.fossil.de4 r10 = new com.fossil.de4
            r10.<init>()
            java.util.Iterator r4 = r17.iterator()
        L_0x00b4:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00ea
            java.lang.Object r0 = r4.next()
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r0 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r0
            java.lang.String r0 = r0.component1()
            r10.a(r0)     // Catch:{ Exception -> 0x00c8 }
            goto L_0x00b4
        L_0x00c8:
            r0 = move-exception
            r11 = r0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "updateGoalSetting exception="
            r13.append(r14)
            r13.append(r11)
            java.lang.String r13 = r13.toString()
            r0.e(r12, r13)
            r11.printStackTrace()
            goto L_0x00b4
        L_0x00ea:
            com.fossil.ie4 r4 = new com.fossil.ie4
            r4.<init>()
            java.lang.String r0 = "_ids"
            r4.a(r0, r10)
            com.portfolio.platform.data.source.GoalTrackingRepository$delete$repoResponse$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$delete$repoResponse$Anon1
            r0.<init>(r1, r4, r6)
            r2.L$0 = r1
            r11 = r17
            r2.L$1 = r11
            r2.L$2 = r10
            r2.L$3 = r4
            r2.label = r9
            java.lang.Object r0 = com.fossil.aj5.a(r0, r2)
            if (r0 != r3) goto L_0x010c
            return r3
        L_0x010c:
            r12 = r1
        L_0x010d:
            r13 = r0
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r0 = r13 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x014b
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r14 = "deleteGoalTrackingDataList onResponse: response = "
            r9.append(r14)
            r9.append(r13)
            java.lang.String r9 = r9.toString()
            r0.d(r5, r9)
            r2.L$0 = r12
            r2.L$1 = r11
            r2.L$2 = r10
            r2.L$3 = r4
            r2.L$4 = r13
            r2.label = r8
            java.lang.Object r0 = r12.removeDeletedGoalTrackingList(r11, r2)
            if (r0 != r3) goto L_0x0143
            return r3
        L_0x0143:
            r3 = r11
        L_0x0144:
            com.fossil.bj5 r0 = new com.fossil.bj5
            r0.<init>(r3, r7, r8, r6)
            goto L_0x0254
        L_0x014b:
            boolean r0 = r13 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0255
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r14 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r6 = "deleteGoalTrackingDataList Failure code="
            r15.append(r6)
            r6 = r13
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r8 = r6.a()
            r15.append(r8)
            java.lang.String r8 = " message="
            r15.append(r8)
            com.portfolio.platform.data.model.ServerError r8 = r6.c()
            if (r8 == 0) goto L_0x017b
            java.lang.String r8 = r8.getMessage()
            goto L_0x017c
        L_0x017b:
            r8 = 0
        L_0x017c:
            r15.append(r8)
            java.lang.String r8 = r15.toString()
            r0.d(r14, r8)
            int r0 = r6.a()
            r8 = 422(0x1a6, float:5.91E-43)
            if (r0 != r8) goto L_0x0238
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.String r0 = r6.b()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0231
            com.portfolio.platform.data.source.GoalTrackingRepository$delete$type$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$delete$type$Anon1
            r0.<init>()
            java.lang.reflect.Type r6 = r0.getType()
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x01f5 }
            r0.<init>()     // Catch:{ Exception -> 0x01f5 }
            r14 = r13
            com.fossil.yi5 r14 = (com.fossil.yi5) r14     // Catch:{ Exception -> 0x01f5 }
            java.lang.String r14 = r14.b()     // Catch:{ Exception -> 0x01f5 }
            java.lang.Object r0 = r0.a(r14, r6)     // Catch:{ Exception -> 0x01f5 }
            java.lang.String r14 = "Gson().fromJson(repoResponse.errorItems, type)"
            com.fossil.ee7.a(r0, r14)     // Catch:{ Exception -> 0x01f5 }
            com.portfolio.platform.data.source.remote.UpsertApiResponse r0 = (com.portfolio.platform.data.source.remote.UpsertApiResponse) r0     // Catch:{ Exception -> 0x01f5 }
            java.util.List r0 = r0.get_items()     // Catch:{ Exception -> 0x01f5 }
            boolean r14 = r0.isEmpty()     // Catch:{ Exception -> 0x01f5 }
            r9 = r9 ^ r14
            if (r9 == 0) goto L_0x0215
            int r9 = r0.size()     // Catch:{ Exception -> 0x01f5 }
            r14 = 0
        L_0x01cd:
            if (r14 >= r9) goto L_0x0215
            java.lang.Object r15 = r0.get(r14)     // Catch:{ Exception -> 0x01f5 }
            com.portfolio.platform.data.model.ServerError r15 = (com.portfolio.platform.data.model.ServerError) r15     // Catch:{ Exception -> 0x01f5 }
            java.lang.Integer r15 = r15.getCode()     // Catch:{ Exception -> 0x01f5 }
            r5 = 404001(0x62a21, float:5.66126E-40)
            if (r15 != 0) goto L_0x01df
            goto L_0x01f1
        L_0x01df:
            int r15 = r15.intValue()     // Catch:{ Exception -> 0x01f5 }
            if (r15 != r5) goto L_0x01f1
            java.lang.Object r5 = r11.get(r14)     // Catch:{ Exception -> 0x01f5 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r5 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r5     // Catch:{ Exception -> 0x01f5 }
            r5.setPinType(r7)     // Catch:{ Exception -> 0x01f5 }
            r8.add(r5)     // Catch:{ Exception -> 0x01f5 }
        L_0x01f1:
            int r14 = r14 + 1
            r5 = 3
            goto L_0x01cd
        L_0x01f5:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r9 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "insertGoalTrackingDataList ex="
            r14.append(r15)
            r14.append(r0)
            java.lang.String r14 = r14.toString()
            r5.e(r9, r14)
            r0.printStackTrace()
        L_0x0215:
            r2.L$0 = r12
            r2.L$1 = r11
            r2.L$2 = r10
            r2.L$3 = r4
            r2.L$4 = r13
            r2.L$5 = r8
            r2.L$6 = r6
            r4 = 3
            r2.label = r4
            java.lang.Object r0 = r12.removeDeletedGoalTrackingList(r8, r2)
            if (r0 != r3) goto L_0x022d
            return r3
        L_0x022d:
            r4 = r11
            r3 = r13
        L_0x022f:
            r13 = r3
            r11 = r4
        L_0x0231:
            com.fossil.bj5 r0 = new com.fossil.bj5
            r2 = 0
            r3 = 2
            r0.<init>(r11, r7, r3, r2)
        L_0x0238:
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            int r3 = r13.a()
            com.portfolio.platform.data.model.ServerError r4 = r13.c()
            java.lang.Throwable r5 = r13.d()
            java.lang.String r6 = r13.b()
            r7 = 0
            r8 = 16
            r9 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x0254:
            return r0
        L_0x0255:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.delete(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object deleteGoalTracking(GoalTrackingData goalTrackingData, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new GoalTrackingRepository$deleteGoalTracking$Anon2(this, goalTrackingData, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object fetchGoalSetting(fb7<? super zi5<GoalSetting>> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$fetchGoalSetting$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getGoalTrackingDataInDate(Date date, fb7<? super List<GoalTrackingData>> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$getGoalTrackingDataInDate$Anon2(date, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getGoalTrackingDataList(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r11 = (com.portfolio.platform.data.source.GoalTrackingRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getGoalTrackingDataPaging(Date date, pj4 pj4, te5.a aVar, fb7<? super Listing<GoalTrackingData>> fb7) {
        return vh7.a(qj7.c(), new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(this, date, pj4, aVar, null), fb7);
    }

    @DexIgnore
    public final List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2, GoalTrackingDao goalTrackingDao) {
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        ee7.b(goalTrackingDao, "goalTrackingDao");
        return goalTrackingDao.getGoalTrackingSummaries(date, date2);
    }

    @DexIgnore
    public final Object getLastGoalSettingLiveData(fb7<? super LiveData<qx6<Integer>>> fb7) {
        return vh7.a(qj7.c(), new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(this, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f9 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLastGoalSettings(com.fossil.fb7<? super java.lang.Integer> r12) {
        /*
            r11 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$Anon1
            r0.<init>(r11, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L_0x0055
            if (r2 == r5) goto L_0x004d
            if (r2 == r4) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r1 = r0.L$1
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00fa
        L_0x0039:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0041:
            java.lang.Object r2 = r0.L$1
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r4 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r4 = (com.portfolio.platform.data.source.GoalTrackingRepository) r4
            com.fossil.t87.a(r12)
            goto L_0x00ab
        L_0x004d:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r12)
            goto L_0x0069
        L_0x0055:
            com.fossil.t87.a(r12)
            com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$response$Anon1 r12 = new com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSettings$response$Anon1
            r12.<init>(r11, r6)
            r0.L$0 = r11
            r0.label = r5
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0068
            return r1
        L_0x0068:
            r2 = r11
        L_0x0069:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r5 = r12 instanceof com.fossil.bj5
            if (r5 == 0) goto L_0x00b2
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "getLastGoalSettings success: "
            r8.append(r9)
            r9 = r12
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r10 = r9.a()
            com.portfolio.platform.data.model.GoalSetting r10 = (com.portfolio.platform.data.model.GoalSetting) r10
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r5.d(r7, r8)
            java.lang.Object r5 = r9.a()
            if (r5 == 0) goto L_0x00ae
            com.portfolio.platform.data.model.GoalSetting r5 = (com.portfolio.platform.data.model.GoalSetting) r5
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r4
            java.lang.Object r4 = r2.saveSettingToDB(r5, r0)
            if (r4 != r1) goto L_0x00a9
            return r1
        L_0x00a9:
            r4 = r2
            r2 = r12
        L_0x00ab:
            r12 = r2
            r2 = r4
            goto L_0x00eb
        L_0x00ae:
            com.fossil.ee7.a()
            throw r6
        L_0x00b2:
            boolean r4 = r12 instanceof com.fossil.yi5
            if (r4 == 0) goto L_0x00eb
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "getLastGoalSettings fail: code="
            r7.append(r8)
            r8 = r12
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r9 = r8.a()
            r7.append(r9)
            java.lang.String r9 = " message="
            r7.append(r9)
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00e1
            java.lang.String r6 = r8.getMessage()
        L_0x00e1:
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            r4.d(r5, r6)
        L_0x00eb:
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r0.L$0 = r2
            r0.L$1 = r12
            r0.label = r3
            java.lang.Object r12 = r4.c(r0)
            if (r12 != r1) goto L_0x00fa
            return r1
        L_0x00fa:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r12 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r12
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r12 = r12.getGoalTrackingDao()
            java.lang.Integer r12 = r12.getLastGoalSetting()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getLastGoalSettings(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getPendingGoalTrackingDataList(Date date, Date date2, fb7<? super List<GoalTrackingData>> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2(date, date2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPendingGoalTrackingDataListLiveData(java.util.Date r5, java.util.Date r6, com.fossil.fb7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r7) {
        /*
            r4 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1
            r0.<init>(r4, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r5 = r0.L$2
            r6 = r5
            java.util.Date r6 = (java.util.Date) r6
            java.lang.Object r5 = r0.L$1
            java.util.Date r5 = (java.util.Date) r5
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.t87.a(r7)
            goto L_0x0052
        L_0x0036:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003e:
            com.fossil.t87.a(r7)
            com.fossil.pg5 r7 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.label = r3
            java.lang.Object r7 = r7.c(r0)
            if (r7 != r1) goto L_0x0052
            return r1
        L_0x0052:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r7 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r7
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r7 = r7.getGoalTrackingDao()
            androidx.lifecycle.LiveData r5 = r7.getPendingGoalTrackingDataListLiveData(r5, r6)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getPendingGoalTrackingDataListLiveData(java.util.Date, java.util.Date, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r11 = (com.portfolio.platform.data.source.GoalTrackingRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummaries(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getSummariesPaging(Date date, pj4 pj4, te5.a aVar, fb7<? super Listing<GoalTrackingSummary>> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$getSummariesPaging$Anon2(this, date, pj4, aVar, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummary(java.util.Date r6, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>>> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.util.Date r6 = (java.util.Date) r6
            java.lang.Object r6 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r6 = (com.portfolio.platform.data.source.GoalTrackingRepository) r6
            com.fossil.t87.a(r7)
            goto L_0x0053
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.tk7 r7 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2
            r4 = 0
            r2.<init>(r5, r6, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x0053
            return r1
        L_0x0053:
            java.lang.String r6 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r7, r6)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummary(java.util.Date, com.fossil.fb7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:68:0x025e, code lost:
        if (r17.intValue() != 409000) goto L_0x0260;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r20, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r21) {
        /*
            r19 = this;
            r1 = r19
            r0 = r21
            boolean r2 = r0 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$insert$Anon1
            if (r2 == 0) goto L_0x0017
            r2 = r0
            com.portfolio.platform.data.source.GoalTrackingRepository$insert$Anon1 r2 = (com.portfolio.platform.data.source.GoalTrackingRepository$insert$Anon1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.GoalTrackingRepository$insert$Anon1 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$insert$Anon1
            r2.<init>(r1, r0)
        L_0x001c:
            java.lang.Object r0 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 4
            r6 = 3
            r7 = 2
            r8 = 1
            r9 = 0
            if (r4 == 0) goto L_0x00a5
            if (r4 == r8) goto L_0x0098
            if (r4 == r7) goto L_0x007e
            if (r4 == r6) goto L_0x0060
            if (r4 != r5) goto L_0x0058
            java.lang.Object r3 = r2.L$7
            java.lang.reflect.Type r3 = (java.lang.reflect.Type) r3
            java.lang.Object r3 = r2.L$6
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r4 = r2.L$5
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r5 = r2.L$4
            com.fossil.ie4 r5 = (com.fossil.ie4) r5
            java.lang.Object r5 = r2.L$3
            com.fossil.de4 r5 = (com.fossil.de4) r5
            java.lang.Object r5 = r2.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r2.L$1
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r0)
            goto L_0x02c6
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0060:
            java.lang.Object r3 = r2.L$5
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r3 = r2.L$4
            com.fossil.ie4 r3 = (com.fossil.ie4) r3
            java.lang.Object r3 = r2.L$3
            com.fossil.de4 r3 = (com.fossil.de4) r3
            java.lang.Object r3 = r2.L$2
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$1
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r0)
            r4 = 0
            goto L_0x01be
        L_0x007e:
            java.lang.Object r4 = r2.L$4
            com.fossil.ie4 r4 = (com.fossil.ie4) r4
            java.lang.Object r11 = r2.L$3
            com.fossil.de4 r11 = (com.fossil.de4) r11
            java.lang.Object r12 = r2.L$2
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r13 = r2.L$1
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r14 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r14 = (com.portfolio.platform.data.source.GoalTrackingRepository) r14
            com.fossil.t87.a(r0)
            r5 = r13
            goto L_0x0184
        L_0x0098:
            java.lang.Object r4 = r2.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r11 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r11 = (com.portfolio.platform.data.source.GoalTrackingRepository) r11
            com.fossil.t87.a(r0)
            r14 = r11
            goto L_0x00da
        L_0x00a5:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "insertGoalTrackingDataList: sampleRawList ="
            r11.append(r12)
            int r12 = r20.size()
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            r0.d(r4, r11)
            com.portfolio.platform.data.source.UserRepository r0 = r1.mUserRepository
            r2.L$0 = r1
            r4 = r20
            r2.L$1 = r4
            r2.label = r8
            java.lang.Object r0 = r0.getCurrentUser(r2)
            if (r0 != r3) goto L_0x00d9
            return r3
        L_0x00d9:
            r14 = r1
        L_0x00da:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x00e4
            java.lang.String r0 = r0.getUserId()
            r12 = r0
            goto L_0x00e5
        L_0x00e4:
            r12 = r9
        L_0x00e5:
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            if (r0 != 0) goto L_0x02fc
            com.fossil.de4 r11 = new com.fossil.de4
            r11.<init>()
            java.util.Iterator r13 = r4.iterator()
        L_0x00f4:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x015c
            java.lang.Object r0 = r13.next()
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r0 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r0
            java.lang.String r15 = r0.component1()
            org.joda.time.DateTime r16 = r0.component2()
            int r17 = r0.component3()
            java.util.Date r0 = r0.component4()
            com.fossil.ie4 r5 = new com.fossil.ie4     // Catch:{ Exception -> 0x0139 }
            r5.<init>()     // Catch:{ Exception -> 0x0139 }
            java.lang.String r8 = "id"
            r5.a(r8, r15)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r8 = "date"
            java.lang.String r0 = com.fossil.zd5.h(r0)     // Catch:{ Exception -> 0x0139 }
            r5.a(r8, r0)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r0 = "trackedAt"
            java.lang.String r8 = com.fossil.zd5.b(r16)     // Catch:{ Exception -> 0x0139 }
            r5.a(r0, r8)     // Catch:{ Exception -> 0x0139 }
            java.lang.String r0 = "timezoneOffset"
            java.lang.Integer r8 = com.fossil.pb7.a(r17)     // Catch:{ Exception -> 0x0139 }
            r5.a(r0, r8)     // Catch:{ Exception -> 0x0139 }
            r11.a(r5)     // Catch:{ Exception -> 0x0139 }
            goto L_0x0159
        L_0x0139:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r10 = "updateGoalSetting exception="
            r15.append(r10)
            r15.append(r0)
            java.lang.String r10 = r15.toString()
            r5.e(r8, r10)
            r0.printStackTrace()
        L_0x0159:
            r5 = 4
            r8 = 1
            goto L_0x00f4
        L_0x015c:
            com.fossil.ie4 r0 = new com.fossil.ie4
            r0.<init>()
            java.lang.String r5 = "_items"
            r0.a(r5, r11)
            com.portfolio.platform.data.source.GoalTrackingRepository$insert$repoResponse$Anon1 r5 = new com.portfolio.platform.data.source.GoalTrackingRepository$insert$repoResponse$Anon1
            r5.<init>(r14, r0, r9)
            r2.L$0 = r14
            r2.L$1 = r4
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r0
            r2.label = r7
            java.lang.Object r5 = com.fossil.aj5.a(r5, r2)
            if (r5 != r3) goto L_0x017e
            return r3
        L_0x017e:
            r18 = r4
            r4 = r0
            r0 = r5
            r5 = r18
        L_0x0184:
            r8 = r0
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r0 = r8 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x01c5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = "insertGoalTrackingDataList onResponse: response = "
            r13.append(r15)
            r13.append(r8)
            java.lang.String r13 = r13.toString()
            r0.d(r10, r13)
            r2.L$0 = r14
            r2.L$1 = r5
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r4
            r2.L$5 = r8
            r2.label = r6
            r4 = 0
            java.lang.Object r0 = r14.updateGoalTrackingPinType(r5, r4, r2)
            if (r0 != r3) goto L_0x01bd
            return r3
        L_0x01bd:
            r3 = r5
        L_0x01be:
            com.fossil.bj5 r0 = new com.fossil.bj5
            r0.<init>(r3, r4, r7, r9)
            goto L_0x02f5
        L_0x01c5:
            boolean r0 = r8 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x02f6
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "insertGoalTrackingDataList Failure code="
            r10.append(r13)
            r13 = r8
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            int r15 = r13.a()
            r10.append(r15)
            java.lang.String r15 = " message="
            r10.append(r15)
            com.portfolio.platform.data.model.ServerError r15 = r13.c()
            if (r15 == 0) goto L_0x01f5
            java.lang.String r15 = r15.getMessage()
            goto L_0x01f6
        L_0x01f5:
            r15 = r9
        L_0x01f6:
            r10.append(r15)
            java.lang.String r10 = r10.toString()
            r0.d(r6, r10)
            int r0 = r13.a()
            r6 = 422(0x1a6, float:5.91E-43)
            if (r0 != r6) goto L_0x02d8
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.String r0 = r13.b()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x02d0
            com.portfolio.platform.data.source.GoalTrackingRepository$insert$type$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$insert$type$Anon1
            r0.<init>()
            java.lang.reflect.Type r10 = r0.getType()
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0288 }
            r0.<init>()     // Catch:{ Exception -> 0x0288 }
            r13 = r8
            com.fossil.yi5 r13 = (com.fossil.yi5) r13     // Catch:{ Exception -> 0x0288 }
            java.lang.String r13 = r13.b()     // Catch:{ Exception -> 0x0288 }
            java.lang.Object r0 = r0.a(r13, r10)     // Catch:{ Exception -> 0x0288 }
            java.lang.String r13 = "Gson().fromJson(repoResponse.errorItems, type)"
            com.fossil.ee7.a(r0, r13)     // Catch:{ Exception -> 0x0288 }
            com.portfolio.platform.data.source.remote.UpsertApiResponse r0 = (com.portfolio.platform.data.source.remote.UpsertApiResponse) r0     // Catch:{ Exception -> 0x0288 }
            java.util.List r0 = r0.get_items()     // Catch:{ Exception -> 0x0288 }
            boolean r13 = r0.isEmpty()     // Catch:{ Exception -> 0x0288 }
            r15 = 1
            r13 = r13 ^ r15
            if (r13 == 0) goto L_0x02a8
            int r13 = r0.size()     // Catch:{ Exception -> 0x0288 }
            r15 = 0
        L_0x0248:
            if (r15 >= r13) goto L_0x02a8
            java.lang.Object r17 = r0.get(r15)     // Catch:{ Exception -> 0x0288 }
            com.portfolio.platform.data.model.ServerError r17 = (com.portfolio.platform.data.model.ServerError) r17     // Catch:{ Exception -> 0x0288 }
            java.lang.Integer r17 = r17.getCode()     // Catch:{ Exception -> 0x0288 }
            r7 = 409000(0x63da8, float:5.73131E-40)
            if (r17 != 0) goto L_0x025a
            goto L_0x0260
        L_0x025a:
            int r9 = r17.intValue()     // Catch:{ Exception -> 0x0288 }
            if (r9 == r7) goto L_0x0276
        L_0x0260:
            java.lang.Object r7 = r0.get(r15)     // Catch:{ Exception -> 0x0288 }
            com.portfolio.platform.data.model.ServerError r7 = (com.portfolio.platform.data.model.ServerError) r7     // Catch:{ Exception -> 0x0288 }
            java.lang.Integer r7 = r7.getCode()     // Catch:{ Exception -> 0x0288 }
            r9 = 409001(0x63da9, float:5.73132E-40)
            if (r7 != 0) goto L_0x0270
            goto L_0x0283
        L_0x0270:
            int r7 = r7.intValue()     // Catch:{ Exception -> 0x0288 }
            if (r7 != r9) goto L_0x0283
        L_0x0276:
            java.lang.Object r7 = r5.get(r15)     // Catch:{ Exception -> 0x0288 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r7 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r7     // Catch:{ Exception -> 0x0288 }
            r9 = 0
            r7.setPinType(r9)     // Catch:{ Exception -> 0x0288 }
            r6.add(r7)     // Catch:{ Exception -> 0x0288 }
        L_0x0283:
            int r15 = r15 + 1
            r7 = 2
            r9 = 0
            goto L_0x0248
        L_0x0288:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r9 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = "insertGoalTrackingDataList ex="
            r13.append(r15)
            r13.append(r0)
            java.lang.String r13 = r13.toString()
            r7.e(r9, r13)
            r0.printStackTrace()
        L_0x02a8:
            com.fossil.pg5 r0 = com.fossil.pg5.i
            r2.L$0 = r14
            r2.L$1 = r5
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r4
            r2.L$5 = r8
            r2.L$6 = r6
            r2.L$7 = r10
            r4 = 4
            r2.label = r4
            java.lang.Object r0 = r0.c(r2)
            if (r0 != r3) goto L_0x02c4
            return r3
        L_0x02c4:
            r3 = r6
            r4 = r8
        L_0x02c6:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            r0.upsertListGoalTrackingData(r3)
            r8 = r4
        L_0x02d0:
            com.fossil.bj5 r0 = new com.fossil.bj5
            r2 = 2
            r3 = 0
            r4 = 0
            r0.<init>(r5, r4, r2, r3)
        L_0x02d8:
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r10 = r8.a()
            com.portfolio.platform.data.model.ServerError r11 = r8.c()
            java.lang.Throwable r12 = r8.d()
            java.lang.String r13 = r8.b()
            r14 = 0
            r15 = 16
            r16 = 0
            r9 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
        L_0x02f5:
            return r0
        L_0x02f6:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        L_0x02fc:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.String r3 = "insertGoalTrackingDataList userId is null"
            r0.d(r2, r3)
            com.fossil.yi5 r0 = new com.fossil.yi5
            r5 = 600(0x258, float:8.41E-43)
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            java.lang.String r3 = ""
            r6.<init>(r2, r3)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 24
            r11 = 0
            r4 = r0
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.insert(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<GoalTrackingData> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new GoalTrackingRepository$insertFromDevice$Anon2(this, list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object loadGoalTrackingDataList(Date date, Date date2, int i, int i2, fb7<? super zi5<ApiResponse<GoalEvent>>> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$loadGoalTrackingDataList$Anon2(this, date, date2, i, i2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loadSummaries(java.util.Date r10, java.util.Date r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>>> r12) {
        /*
            r9 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$Anon1
            r0.<init>(r9, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 0
            r5 = 1
            if (r2 == 0) goto L_0x005e
            if (r2 == r5) goto L_0x004d
            if (r2 != r3) goto L_0x0045
            java.lang.Object r10 = r0.L$4
            java.util.List r10 = (java.util.List) r10
            java.lang.Object r11 = r0.L$3
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            java.lang.Object r1 = r0.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r0.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.t87.a(r12)     // Catch:{ Exception -> 0x0042 }
            goto L_0x00fe
        L_0x0042:
            r10 = move-exception
            goto L_0x0154
        L_0x0045:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x004d:
            java.lang.Object r10 = r0.L$2
            r11 = r10
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r10 = r0.L$1
            java.util.Date r10 = (java.util.Date) r10
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r12)
            goto L_0x009a
        L_0x005e:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "loadSummaries startDate="
            r6.append(r7)
            r6.append(r10)
            java.lang.String r7 = ", endDate="
            r6.append(r7)
            r6.append(r11)
            java.lang.String r6 = r6.toString()
            r12.d(r2, r6)
            com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$response$Anon1 r12 = new com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$response$Anon1
            r12.<init>(r9, r10, r11, r4)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r5
            java.lang.Object r12 = com.fossil.aj5.a(r12, r0)
            if (r12 != r1) goto L_0x0099
            return r1
        L_0x0099:
            r2 = r9
        L_0x009a:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r6 = r12 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x0175
            r6 = r12
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r7 = r6.a()
            if (r7 == 0) goto L_0x01c1
            boolean r6 = r6.b()
            if (r6 != 0) goto L_0x01c1
            r6 = r12
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x0152 }
            java.lang.Object r6 = r6.a()     // Catch:{ Exception -> 0x0152 }
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6     // Catch:{ Exception -> 0x0152 }
            java.util.List r6 = r6.get_items()     // Catch:{ Exception -> 0x0152 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ Exception -> 0x0152 }
            r8 = 10
            int r8 = com.fossil.x97.a(r6, r8)     // Catch:{ Exception -> 0x0152 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0152 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ Exception -> 0x0152 }
        L_0x00cb:
            boolean r8 = r6.hasNext()     // Catch:{ Exception -> 0x0152 }
            if (r8 == 0) goto L_0x00e5
            java.lang.Object r8 = r6.next()     // Catch:{ Exception -> 0x0152 }
            com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r8 = (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary) r8     // Catch:{ Exception -> 0x0152 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r8 = r8.toGoalTrackingSummary()     // Catch:{ Exception -> 0x0152 }
            if (r8 == 0) goto L_0x00e1
            r7.add(r8)     // Catch:{ Exception -> 0x0152 }
            goto L_0x00cb
        L_0x00e1:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x0152 }
            throw r4
        L_0x00e5:
            com.fossil.pg5 r4 = com.fossil.pg5.i
            r0.L$0 = r2
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.L$4 = r7
            r0.label = r3
            java.lang.Object r10 = r4.c(r0)
            if (r10 != r1) goto L_0x00fa
            return r1
        L_0x00fa:
            r11 = r12
            r0 = r2
            r12 = r10
            r10 = r7
        L_0x00fe:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r12 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r12
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r12 = r12.getGoalTrackingDao()
            java.util.List r10 = com.fossil.ea7.d(r10)
            r12.upsertGoalTrackingSummaries(r10)
            r10 = r11
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r10 = (com.portfolio.platform.data.source.remote.ApiResponse) r10
            java.util.List r10 = r10.get_items()
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            java.util.Iterator r10 = r10.iterator()
        L_0x0121:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x0145
            java.lang.Object r1 = r10.next()
            r2 = r1
            com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r2 = (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary) r2
            int r2 = r2.getMTotalTracked()
            if (r2 <= 0) goto L_0x0136
            r2 = 1
            goto L_0x0137
        L_0x0136:
            r2 = 0
        L_0x0137:
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x0121
            r12.add(r1)
            goto L_0x0121
        L_0x0145:
            boolean r10 = r12.isEmpty()
            r10 = r10 ^ r5
            if (r10 == 0) goto L_0x0173
            com.fossil.ch5 r10 = r0.mSharedPreferencesManager
            r10.j(r5)
            goto L_0x0173
        L_0x0152:
            r10 = move-exception
            r11 = r12
        L_0x0154:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "loadSummaries exception="
            r1.append(r2)
            r1.append(r10)
            java.lang.String r1 = r1.toString()
            r12.e(r0, r1)
            r10.printStackTrace()
        L_0x0173:
            r12 = r11
            goto L_0x01c1
        L_0x0175:
            boolean r10 = r12 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x01c1
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "loadSummaries Failure code="
            r0.append(r1)
            r1 = r12
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r2 = r1.a()
            r0.append(r2)
            java.lang.String r2 = " message="
            r0.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r1.c()
            if (r2 == 0) goto L_0x01a8
            java.lang.String r2 = r2.getMessage()
            if (r2 == 0) goto L_0x01a8
            r4 = r2
            goto L_0x01b2
        L_0x01a8:
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x01b2
            java.lang.String r4 = r1.getUserMessage()
        L_0x01b2:
            if (r4 == 0) goto L_0x01b5
            goto L_0x01b7
        L_0x01b5:
            java.lang.String r4 = ""
        L_0x01b7:
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r10.d(r11, r0)
        L_0x01c1:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.loadSummaries(java.util.Date, java.util.Date, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingGoalTrackingDataList(PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, fb7<? super i97> fb7) {
        return vh7.a(qj7.b(), new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(this, pushPendingGoalTrackingDataListCallback, null), fb7);
    }

    @DexIgnore
    public final /* synthetic */ Object removeDeletedGoalTrackingList(List<GoalTrackingData> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new GoalTrackingRepository$removeDeletedGoalTrackingList$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (GoalTrackingDataSourceFactory goalTrackingDataSourceFactory : this.mSourceDataFactoryList) {
            GoalTrackingDataLocalDataSource localDataSource = goalTrackingDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
        for (GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory : this.mSourceFactoryList) {
            GoalTrackingSummaryLocalDataSource localDataSource2 = goalTrackingSummaryDataSourceFactory.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ea  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x026b  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0271  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveGoalTrackingDataListToServer(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r20, com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback r21, com.fossil.fb7<? super com.fossil.i97> r22) {
        /*
            r19 = this;
            r0 = r22
            boolean r1 = r0 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 r1 = (com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            r2 = r19
            goto L_0x001e
        L_0x0017:
            com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 r1 = new com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1
            r2 = r19
            r1.<init>(r2, r0)
        L_0x001e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r1.label
            java.lang.String r6 = "startIndex="
            java.lang.String r8 = "saveGoalTrackingDataListToServer failed, errorCode="
            java.lang.String r9 = "saveGoalTrackingDataListToServer success, bravo!!! startIndex="
            r10 = 2
            java.lang.String r11 = " endIndex="
            r12 = 1
            if (r4 == 0) goto L_0x008c
            if (r4 == r12) goto L_0x0065
            if (r4 != r10) goto L_0x005d
            java.lang.Object r4 = r1.L$6
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r4 = r1.L$5
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r4 = r1.L$4
            java.util.List r4 = (java.util.List) r4
            int r4 = r1.I$1
            java.lang.Object r13 = r1.L$3
            java.util.List r13 = (java.util.List) r13
            int r14 = r1.I$0
            java.lang.Object r15 = r1.L$2
            com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback r15 = (com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback) r15
            java.lang.Object r10 = r1.L$1
            java.util.List r10 = (java.util.List) r10
            java.lang.Object r7 = r1.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r7 = (com.portfolio.platform.data.source.GoalTrackingRepository) r7
            com.fossil.t87.a(r0)
            r2 = r0
            r0 = 2
            goto L_0x01e4
        L_0x005d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0065:
            java.lang.Object r4 = r1.L$6
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r7 = r1.L$5
            java.util.List r7 = (java.util.List) r7
            java.lang.Object r10 = r1.L$4
            java.util.List r10 = (java.util.List) r10
            int r13 = r1.I$1
            java.lang.Object r14 = r1.L$3
            java.util.List r14 = (java.util.List) r14
            int r15 = r1.I$0
            java.lang.Object r5 = r1.L$2
            com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback r5 = (com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback) r5
            java.lang.Object r12 = r1.L$1
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r2 = r1.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.t87.a(r0)
            r17 = r6
            goto L_0x0123
        L_0x008c:
            com.fossil.t87.a(r0)
            r0 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0 = r20
            r14 = r2
            r4 = r3
            r15 = 0
            r3 = r19
            r2 = r1
            r1 = r21
        L_0x009f:
            int r5 = r0.size()
            if (r15 >= r5) goto L_0x027e
            int r5 = r15 + 100
            int r7 = r0.size()
            if (r5 <= r7) goto L_0x00b1
            int r5 = r0.size()
        L_0x00b1:
            r13 = r5
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r12 = "saveGoalTrackingDataListToServer startIndex="
            r10.append(r12)
            r10.append(r15)
            r10.append(r11)
            r10.append(r13)
            java.lang.String r10 = r10.toString()
            r5.d(r7, r10)
            java.util.List r10 = r0.subList(r15, r13)
            com.fossil.hg7 r5 = com.fossil.ea7.b(r10)
            com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1 r7 = com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1.INSTANCE
            com.fossil.hg7 r5 = com.fossil.og7.a(r5, r7)
            java.util.List r7 = com.fossil.og7.g(r5)
            com.fossil.hg7 r5 = com.fossil.ea7.b(r10)
            com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1 r12 = com.portfolio.platform.data.source.GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1.INSTANCE
            com.fossil.hg7 r5 = com.fossil.og7.a(r5, r12)
            java.util.List r5 = com.fossil.og7.g(r5)
            boolean r12 = r7.isEmpty()
            r17 = r6
            r6 = 1
            r12 = r12 ^ r6
            if (r12 == 0) goto L_0x01af
            r2.L$0 = r3
            r2.L$1 = r0
            r2.L$2 = r1
            r2.I$0 = r15
            r2.L$3 = r14
            r2.I$1 = r13
            r2.L$4 = r10
            r2.L$5 = r7
            r2.L$6 = r5
            r2.label = r6
            java.lang.Object r6 = r3.insert(r7, r2)
            if (r6 != r4) goto L_0x0119
            return r4
        L_0x0119:
            r12 = r0
            r0 = r6
            r18 = r5
            r5 = r1
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r18
        L_0x0123:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            boolean r6 = r0 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x0160
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            r20 = r1
            java.lang.String r1 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            r21 = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r9)
            r2.append(r15)
            r2.append(r11)
            r2.append(r13)
            java.lang.String r2 = r2.toString()
            r6.d(r1, r2)
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x015b
            java.util.List r0 = (java.util.List) r0
            r14.addAll(r0)
            goto L_0x019c
        L_0x015b:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0160:
            r20 = r1
            r21 = r2
            boolean r1 = r0 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x019c
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r8)
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r0 = r0.a()
            r6.append(r0)
            r0 = 32
            r6.append(r0)
            r0 = r17
            r6.append(r0)
            r6.append(r15)
            r6.append(r11)
            r6.append(r13)
            java.lang.String r6 = r6.toString()
            r1.d(r2, r6)
            goto L_0x019e
        L_0x019c:
            r0 = r17
        L_0x019e:
            r1 = r20
            r6 = r0
            r0 = r7
            r2 = r10
            r10 = r12
            r7 = r21
            r18 = r5
            r5 = r4
            r4 = r13
            r13 = r14
            r14 = r15
            r15 = r18
            goto L_0x01be
        L_0x01af:
            r6 = r17
            r18 = r10
            r10 = r0
            r0 = r7
            r7 = r3
            r3 = r4
            r4 = r13
            r13 = r14
            r14 = r15
            r15 = r1
            r1 = r2
            r2 = r18
        L_0x01be:
            boolean r12 = r5.isEmpty()
            r16 = 1
            r12 = r12 ^ 1
            if (r12 == 0) goto L_0x0258
            r1.L$0 = r7
            r1.L$1 = r10
            r1.L$2 = r15
            r1.I$0 = r14
            r1.L$3 = r13
            r1.I$1 = r4
            r1.L$4 = r2
            r1.L$5 = r0
            r1.L$6 = r5
            r0 = 2
            r1.label = r0
            java.lang.Object r2 = r7.delete(r5, r1)
            if (r2 != r3) goto L_0x01e4
            return r3
        L_0x01e4:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r5 = r2 instanceof com.fossil.bj5
            if (r5 == 0) goto L_0x021d
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r9)
            r0.append(r14)
            r0.append(r11)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r5.d(r12, r0)
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r0 = r2.a()
            if (r0 == 0) goto L_0x0218
            java.util.List r0 = (java.util.List) r0
            r13.addAll(r0)
            goto L_0x0258
        L_0x0218:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x021d:
            r0 = 0
            boolean r5 = r2 instanceof com.fossil.yi5
            if (r5 == 0) goto L_0x0258
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r8)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r2 = r2.a()
            r0.append(r2)
            r2 = 32
            r0.append(r2)
            r0.append(r6)
            r0.append(r14)
            r0.append(r11)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r5.d(r12, r0)
        L_0x0253:
            r4 = r3
            r0 = r10
            r3 = r1
            r1 = r15
            goto L_0x025b
        L_0x0258:
            r2 = 32
            goto L_0x0253
        L_0x025b:
            int r15 = r14 + 100
            int r5 = r0.size()
            if (r15 < r5) goto L_0x0279
            boolean r0 = r13.isEmpty()
            r5 = 1
            r0 = r0 ^ r5
            if (r0 == 0) goto L_0x0271
            if (r1 == 0) goto L_0x027e
            r1.onSuccess(r13)
            goto L_0x027e
        L_0x0271:
            if (r1 == 0) goto L_0x027e
            r0 = 600(0x258, float:8.41E-43)
            r1.onFail(r0)
            goto L_0x027e
        L_0x0279:
            r2 = r3
            r3 = r7
            r14 = r13
            goto L_0x009f
        L_0x027e:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.saveGoalTrackingDataListToServer(java.util.List, com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object saveSettingToDB(GoalSetting goalSetting, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new GoalTrackingRepository$saveSettingToDB$Anon2(goalSetting, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateGoalSetting(com.portfolio.platform.data.model.GoalSetting r11, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.GoalSetting>> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x005a
            if (r2 == r5) goto L_0x0046
            if (r2 != r4) goto L_0x003e
            java.lang.Object r11 = r0.L$3
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            java.lang.Object r1 = r0.L$2
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r0.L$1
            com.portfolio.platform.data.model.GoalSetting r1 = (com.portfolio.platform.data.model.GoalSetting) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x00ec
        L_0x003e:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0046:
            java.lang.Object r11 = r0.L$2
            com.fossil.ie4 r11 = (com.fossil.ie4) r11
            java.lang.Object r2 = r0.L$1
            com.portfolio.platform.data.model.GoalSetting r2 = (com.portfolio.platform.data.model.GoalSetting) r2
            java.lang.Object r5 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r5 = (com.portfolio.platform.data.source.GoalTrackingRepository) r5
            com.fossil.t87.a(r12)
            r9 = r12
            r12 = r11
            r11 = r2
            r2 = r9
            goto L_0x00b8
        L_0x005a:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "updateGoalSetting goalSetting="
            r6.append(r7)
            r6.append(r11)
            java.lang.String r6 = r6.toString()
            r12.d(r2, r6)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            int r2 = r11.getCurrentTarget()
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            java.lang.String r6 = "currentTarget"
            r12.a(r6, r2)
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()
            java.lang.String r6 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r2, r6)
            int r2 = r2.getRawOffset()
            int r2 = r2 / 1000
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            java.lang.String r6 = "timezoneOffset"
            r12.a(r6, r2)
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$response$Anon1 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalSetting$response$Anon1
            r2.<init>(r10, r12, r3)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.label = r5
            java.lang.Object r2 = com.fossil.aj5.a(r2, r0)
            if (r2 != r1) goto L_0x00b7
            return r1
        L_0x00b7:
            r5 = r10
        L_0x00b8:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r6 = r2 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x00ee
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "updateGoalSetting onResponse: response = "
            r7.append(r8)
            r7.append(r2)
            java.lang.String r7 = r7.toString()
            r3.d(r6, r7)
            r0.L$0 = r5
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r2
            r0.label = r4
            java.lang.Object r11 = r5.saveSettingToDB(r11, r0)
            if (r11 != r1) goto L_0x00eb
            return r1
        L_0x00eb:
            r11 = r2
        L_0x00ec:
            r2 = r11
            goto L_0x0127
        L_0x00ee:
            boolean r11 = r2 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0127
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.GoalTrackingRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "updateGoalSetting Failure code="
            r0.append(r1)
            r1 = r2
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r4 = r1.a()
            r0.append(r4)
            java.lang.String r4 = " message="
            r0.append(r4)
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x011d
            java.lang.String r3 = r1.getMessage()
        L_0x011d:
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r11.d(r12, r0)
        L_0x0127:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.updateGoalSetting(com.portfolio.platform.data.model.GoalSetting, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object updateGoalTrackingPinType(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r5, int r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r4 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1
            r0.<init>(r4, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            int r5 = r0.I$0
            java.lang.Object r5 = r0.L$1
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r6 = r0.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r6 = (com.portfolio.platform.data.source.GoalTrackingRepository) r6
            com.fossil.t87.a(r7)
            goto L_0x0063
        L_0x0033:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003b:
            com.fossil.t87.a(r7)
            java.util.Iterator r7 = r5.iterator()
        L_0x0042:
            boolean r2 = r7.hasNext()
            if (r2 == 0) goto L_0x0052
            java.lang.Object r2 = r7.next()
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r2 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r2
            r2.setPinType(r6)
            goto L_0x0042
        L_0x0052:
            com.fossil.pg5 r7 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.L$1 = r5
            r0.I$0 = r6
            r0.label = r3
            java.lang.Object r7 = r7.c(r0)
            if (r7 != r1) goto L_0x0063
            return r1
        L_0x0063:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r7 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r7
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r6 = r7.getGoalTrackingDao()
            r6.upsertListGoalTrackingData(r5)
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.updateGoalTrackingPinType(java.util.List, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object pushPendingGoalTrackingDataList(fb7<? super i97> fb7) {
        FLogger.INSTANCE.getLocal().d(TAG, "pushPendingGoalTrackingDataList");
        Object pushPendingGoalTrackingDataList = pushPendingGoalTrackingDataList(new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4(this), fb7);
        if (pushPendingGoalTrackingDataList == nb7.a()) {
            return pushPendingGoalTrackingDataList;
        }
        return i97.a;
    }
}
