package com.portfolio.platform.data.source.remote;

import com.fossil.bw7;
import com.fossil.nw7;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ShortcutApiService {
    @DexIgnore
    @bw7("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@nw7("offset") int i, @nw7("size") int i2, @nw7("serialNumber") String str);

    @DexIgnore
    @bw7("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@nw7("page") int i, @nw7("size") int i2, @nw7("serialNumber") String str);
}
