package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.hu4;
import com.fossil.ji;
import com.fossil.lu4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ hu4 __activityIntensitiesConverter; // = new hu4();
    @DexIgnore
    public /* final */ lu4 __dateLongStringConverter; // = new lu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<SampleRaw> __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<SampleRaw> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw` (`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, sampleRaw.getId());
            }
            ajVar.bindLong(2, (long) sampleRaw.getPinType());
            ajVar.bindLong(3, (long) sampleRaw.getUaPinType());
            String a = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a);
            }
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            if (sampleRaw.getSourceId() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, sampleRaw.getMovementTypeValue());
            }
            ajVar.bindDouble(9, sampleRaw.getSteps());
            ajVar.bindDouble(10, sampleRaw.getCalories());
            ajVar.bindDouble(11, sampleRaw.getDistance());
            ajVar.bindLong(12, (long) sampleRaw.getActiveTime());
            String a3 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a3 == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, a3);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, sampleRaw.getTimeZoneID());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfSampleRaw = new Anon1(ciVar);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        fi fiVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        fi b = fi.b("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        b.bindLong(1, (long) i);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sampleRawDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "pinType");
            int b4 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = oi.b(a, "intensityDistInSteps");
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i2 = b4;
                int i3 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    sampleRaw.setPinType(a.getInt(i3));
                    sampleRaw.setUaPinType(a.getInt(i2));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i2 = i2;
                    b2 = b2;
                    i3 = i3;
                    b5 = b5;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples() {
        fi fiVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        fi b = fi.b("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sampleRawDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "pinType");
            int b4 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = oi.b(a, "intensityDistInSteps");
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9), a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), a.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a.getString(b14)), a.getString(b15));
                    sampleRaw.setId(a.getString(b2));
                    sampleRaw.setPinType(a.getInt(i2));
                    sampleRaw.setUaPinType(a.getInt(i));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i;
                    b2 = b2;
                    i2 = i2;
                    b5 = b5;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        fi fiVar;
        SampleRawDao_Impl sampleRawDao_Impl = this;
        fi b = fi.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a = sampleRawDao_Impl.__dateLongStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = sampleRawDao_Impl.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        sampleRawDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a3 = pi.a(sampleRawDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a3, "id");
            int b3 = oi.b(a3, "pinType");
            int b4 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int b5 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int b6 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int b7 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int b8 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int b9 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int b10 = oi.b(a3, "steps");
            int b11 = oi.b(a3, "calories");
            int b12 = oi.b(a3, "distance");
            int b13 = oi.b(a3, SampleDay.COLUMN_ACTIVE_TIME);
            int b14 = oi.b(a3, "intensityDistInSteps");
            fiVar = b;
            try {
                int b15 = oi.b(a3, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b4;
                int i2 = b3;
                ArrayList arrayList = new ArrayList(a3.getCount());
                while (a3.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b5)), sampleRawDao_Impl.__dateLongStringConverter.a(a3.getString(b6)), a3.getString(b7), a3.getString(b8), a3.getString(b9), a3.getDouble(b10), a3.getDouble(b11), a3.getDouble(b12), a3.getInt(b13), sampleRawDao_Impl.__activityIntensitiesConverter.a(a3.getString(b14)), a3.getString(b15));
                    sampleRaw.setId(a3.getString(b2));
                    sampleRaw.setPinType(a3.getInt(i2));
                    sampleRaw.setUaPinType(a3.getInt(i));
                    arrayList.add(sampleRaw);
                    sampleRawDao_Impl = this;
                    i = i;
                    b2 = b2;
                    i2 = i2;
                    b5 = b5;
                }
                a3.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a3.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a3.close();
            fiVar.c();
            throw th;
        }
    }
}
