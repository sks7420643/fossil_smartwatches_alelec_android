package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.be5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.ie4;
import com.fossil.mh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ DeviceDao mDeviceDao;
    @DexIgnore
    public /* final */ DeviceRemoteDataSource mDeviceRemoteDataSource;
    @DexIgnore
    public /* final */ SkuDao mSkuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return DeviceRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "DeviceRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRepository(DeviceDao deviceDao, SkuDao skuDao, DeviceRemoteDataSource deviceRemoteDataSource) {
        ee7.b(deviceDao, "mDeviceDao");
        ee7.b(skuDao, "mSkuDao");
        ee7.b(deviceRemoteDataSource, "mDeviceRemoteDataSource");
        this.mDeviceDao = deviceDao;
        this.mSkuDao = skuDao;
        this.mDeviceRemoteDataSource = deviceRemoteDataSource;
    }

    @DexIgnore
    public static /* synthetic */ Object downloadSupportedSku$default(DeviceRepository deviceRepository, int i, fb7 fb7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return deviceRepository.downloadSupportedSku(i, fb7);
    }

    @DexIgnore
    private final boolean isHasDevice(List<Device> list, String str) {
        if (list == null) {
            return false;
        }
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            if (mh7.b(it.next().getDeviceId(), str, true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mDeviceDao.cleanUp();
        this.mSkuDao.cleanUpSku();
        this.mSkuDao.cleanUpWatchParam();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadDeviceList(com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.Device>>> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0044
            if (r2 == r4) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            java.lang.Object r1 = r0.L$1
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x007c
        L_0x0034:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x003c:
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r2 = (com.portfolio.platform.data.source.DeviceRepository) r2
            com.fossil.t87.a(r8)
            goto L_0x0055
        L_0x0044:
            com.fossil.t87.a(r8)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r8 = r7.mDeviceRemoteDataSource
            r0.L$0 = r7
            r0.label = r4
            java.lang.Object r8 = r8.getAllDevice(r0)
            if (r8 != r1) goto L_0x0054
            return r1
        L_0x0054:
            r2 = r7
        L_0x0055:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r4 = r8 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x007d
            r4 = r8
            com.fossil.bj5 r4 = (com.fossil.bj5) r4
            boolean r4 = r4.b()
            if (r4 != 0) goto L_0x007d
            com.fossil.ti7 r4 = com.fossil.qj7.a()
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon2 r5 = new com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon2
            r6 = 0
            r5.<init>(r2, r8, r6)
            r0.L$0 = r2
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r0 = com.fossil.vh7.a(r4, r5, r0)
            if (r0 != r1) goto L_0x007b
            return r1
        L_0x007b:
            r1 = r8
        L_0x007c:
            r8 = r1
        L_0x007d:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.downloadDeviceList(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadSupportedSku(int r10, com.fossil.fb7<? super com.fossil.i97> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0049
            if (r2 == r4) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            java.lang.Object r10 = r0.L$1
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            int r10 = r0.I$0
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r10 = (com.portfolio.platform.data.source.DeviceRepository) r10
            com.fossil.t87.a(r11)
            goto L_0x00cb
        L_0x0037:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003f:
            int r10 = r0.I$0
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r2 = (com.portfolio.platform.data.source.DeviceRepository) r2
            com.fossil.t87.a(r11)
            goto L_0x005c
        L_0x0049:
            com.fossil.t87.a(r11)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r11 = r9.mDeviceRemoteDataSource
            r0.L$0 = r9
            r0.I$0 = r10
            r0.label = r4
            java.lang.Object r11 = r11.getSupportedSku(r10, r0)
            if (r11 != r1) goto L_0x005b
            return r1
        L_0x005b:
            r2 = r9
        L_0x005c:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "downloadSupportedSku() - offset = "
            r7.append(r8)
            r7.append(r10)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            boolean r5 = r11 instanceof com.fossil.bj5
            if (r5 == 0) goto L_0x00cb
            r5 = r11
            com.fossil.bj5 r5 = (com.fossil.bj5) r5
            boolean r6 = r5.b()
            if (r6 != 0) goto L_0x00cb
            com.portfolio.platform.data.source.SkuDao r6 = r2.mSkuDao
            java.lang.Object r7 = r5.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            r8 = 0
            if (r7 == 0) goto L_0x0097
            java.util.List r7 = r7.get_items()
            goto L_0x0098
        L_0x0097:
            r7 = r8
        L_0x0098:
            if (r7 == 0) goto L_0x00c7
            r6.addOrUpdateSkuList(r7)
            java.lang.Object r5 = r5.a()
            com.portfolio.platform.data.source.remote.ApiResponse r5 = (com.portfolio.platform.data.source.remote.ApiResponse) r5
            com.portfolio.platform.data.model.Range r5 = r5.get_range()
            if (r5 == 0) goto L_0x00cb
            boolean r5 = r5.isHasNext()
            if (r5 != r4) goto L_0x00cb
            com.fossil.ti7 r4 = com.fossil.qj7.b()
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2 r5 = new com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2
            r5.<init>(r2, r10, r8)
            r0.L$0 = r2
            r0.I$0 = r10
            r0.L$1 = r11
            r0.label = r3
            java.lang.Object r10 = com.fossil.vh7.a(r4, r5, r0)
            if (r10 != r1) goto L_0x00cb
            return r1
        L_0x00c7:
            com.fossil.ee7.a()
            throw r8
        L_0x00cb:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku(int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object forceLinkDevice(com.portfolio.platform.data.model.Device r10, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.DeviceRepository$forceLinkDevice$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.DeviceRepository$forceLinkDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$forceLinkDevice$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$forceLinkDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$forceLinkDevice$Anon1
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "Inside .forceLinkDevice device="
            r4 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r4) goto L_0x0033
            java.lang.Object r10 = r0.L$1
            com.portfolio.platform.data.model.Device r10 = (com.portfolio.platform.data.model.Device) r10
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.t87.a(r11)
            goto L_0x0096
        L_0x0033:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003b:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r3)
            r5.append(r10)
            java.lang.String r5 = r5.toString()
            r11.d(r2, r5)
            r10.setActive(r4)
            java.lang.String r11 = r10.getDeviceId()
            boolean r11 = android.text.TextUtils.isEmpty(r11)
            if (r11 == 0) goto L_0x0086
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.String r0 = "Inside .forceLinkDevice can't link device without serial number"
            r10.d(r11, r0)
            com.fossil.yi5 r10 = new com.fossil.yi5
            r2 = 408(0x198, float:5.72E-43)
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>()
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return r10
        L_0x0086:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r11 = r9.mDeviceRemoteDataSource
            r0.L$0 = r9
            r0.L$1 = r10
            r0.label = r4
            java.lang.Object r11 = r11.forceLinkDevice(r10, r0)
            if (r11 != r1) goto L_0x0095
            return r1
        L_0x0095:
            r0 = r9
        L_0x0096:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r1 = r11 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00c5
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            java.lang.String r3 = r10.getDeviceId()
            r4.append(r3)
            java.lang.String r3 = "on server success"
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r1.d(r2, r3)
            com.portfolio.platform.data.source.DeviceDao r0 = r0.mDeviceDao
            r0.addOrUpdateDevice(r10)
            goto L_0x00e8
        L_0x00c5:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            java.lang.String r10 = r10.getDeviceId()
            r2.append(r10)
            java.lang.String r10 = "on server fail"
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r0.d(r1, r10)
        L_0x00e8:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.forceLinkDevice(com.portfolio.platform.data.model.Device, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object generatePairingKey(String str, fb7<? super zi5<String>> fb7) {
        return this.mDeviceRemoteDataSource.generatePairingKey(str, fb7);
    }

    @DexIgnore
    public final List<Device> getAllDevice() {
        return this.mDeviceDao.getAllDevice();
    }

    @DexIgnore
    public final LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.mDeviceDao.getAllDeviceAsLiveData();
    }

    @DexIgnore
    public final Device getDeviceBySerial(String str) {
        ee7.b(str, "serial");
        return this.mDeviceDao.getDeviceByDeviceId(str);
    }

    @DexIgnore
    public final LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        ee7.b(str, "serial");
        return this.mDeviceDao.getDeviceBySerialAsLiveData(str);
    }

    @DexIgnore
    public final String getDeviceNameBySerial(String str) {
        ee7.b(str, "serial");
        SKUModel skuByDeviceIdPrefix = this.mSkuDao.getSkuByDeviceIdPrefix(be5.o.b(str));
        if (skuByDeviceIdPrefix != null && !TextUtils.isEmpty(skuByDeviceIdPrefix.getDeviceName())) {
            String deviceName = skuByDeviceIdPrefix.getDeviceName();
            if (deviceName != null) {
                return deviceName;
            }
            ee7.a();
            throw null;
        } else if (TextUtils.isEmpty(str) || str.length() < 6) {
            return "UNKNOWN";
        } else {
            return DeviceIdentityUtils.isDianaDevice(str) ? "Hybrid HR" : "Hybrid Smartwatch";
        }
    }

    @DexIgnore
    public final Object getDeviceSecretKey(String str, fb7<? super zi5<String>> fb7) {
        return this.mDeviceRemoteDataSource.getDeviceSecretKey(str, fb7);
    }

    @DexIgnore
    public final Object getLatestWatchParamFromServer(String str, int i, fb7<? super WatchParameterResponse> fb7) {
        return this.mDeviceRemoteDataSource.getLatestWatchParamFromServer(str, i, fb7);
    }

    @DexIgnore
    public final SKUModel getSkuModelBySerialPrefix(String str) {
        ee7.b(str, "prefix");
        return this.mSkuDao.getSkuByDeviceIdPrefix(str);
    }

    @DexIgnore
    public final List<SKUModel> getSupportedSku() {
        return this.mSkuDao.getAllSkus();
    }

    @DexIgnore
    public final WatchParam getWatchParamBySerialId(String str) {
        ee7.b(str, "serial");
        return this.mSkuDao.getWatchParamById(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object removeDevice(com.portfolio.platform.data.model.Device r7, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r7 = r0.L$1
            com.portfolio.platform.data.model.Device r7 = (com.portfolio.platform.data.model.Device) r7
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x006c
        L_0x0031:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x0039:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Inside .removeDevice deviceId="
            r4.append(r5)
            java.lang.String r5 = r7.getDeviceId()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r8.d(r2, r4)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r8 = r6.mDeviceRemoteDataSource
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r8 = r8.removeDevice(r7, r0)
            if (r8 != r1) goto L_0x006b
            return r1
        L_0x006b:
            r0 = r6
        L_0x006c:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            if (r1 != 0) goto L_0x0081
            boolean r1 = r8 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x008a
            r1 = r8
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r1 = r1.a()
            r2 = 404(0x194, float:5.66E-43)
            if (r1 != r2) goto L_0x008a
        L_0x0081:
            com.portfolio.platform.data.source.DeviceDao r0 = r0.mDeviceDao
            java.lang.String r7 = r7.getDeviceId()
            r0.removeDeviceByDeviceId(r7)
        L_0x008a:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.removeDevice(com.portfolio.platform.data.model.Device, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object removeStealDevice(java.util.List<com.portfolio.platform.data.model.Device> r12, com.fossil.fb7<? super com.fossil.i97> r13) {
        /*
            r11 = this;
            boolean r0 = r13 instanceof com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1
            r0.<init>(r11, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0046
            if (r2 != r3) goto L_0x003e
            java.lang.Object r12 = r0.L$4
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r12 = r0.L$3
            java.util.Iterator r12 = (java.util.Iterator) r12
            java.lang.Object r2 = r0.L$2
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r4 = r0.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r5 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r5 = (com.portfolio.platform.data.source.DeviceRepository) r5
            com.fossil.t87.a(r13)
            r13 = r4
            goto L_0x0058
        L_0x003e:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0046:
            com.fossil.t87.a(r13)
            com.portfolio.platform.data.source.DeviceDao r13 = r11.mDeviceDao
            java.util.List r13 = r13.getAllDevice()
            java.util.Iterator r2 = r13.iterator()
            r5 = r11
            r10 = r13
            r13 = r12
            r12 = r2
            r2 = r10
        L_0x0058:
            boolean r4 = r12.hasNext()
            if (r4 == 0) goto L_0x00c3
            java.lang.Object r4 = r12.next()
            com.portfolio.platform.data.model.Device r4 = (com.portfolio.platform.data.model.Device) r4
            java.lang.String r4 = r4.component1()
            boolean r6 = r5.isHasDevice(r13, r4)
            if (r6 != 0) goto L_0x0058
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Inside .removeStealDevice device="
            r8.append(r9)
            r8.append(r4)
            java.lang.String r9 = " was stealed, remove it on local"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            com.portfolio.platform.data.source.DeviceDao r6 = r5.mDeviceDao
            r6.removeDeviceByDeviceId(r4)
            boolean r6 = android.text.TextUtils.isEmpty(r4)
            if (r6 != 0) goto L_0x0058
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            java.lang.String r6 = r6.c()
            boolean r6 = com.fossil.mh7.b(r4, r6, r3)
            if (r6 == 0) goto L_0x0058
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r0.L$0 = r5
            r0.L$1 = r13
            r0.L$2 = r2
            r0.L$3 = r12
            r0.L$4 = r4
            r0.label = r3
            java.lang.Object r4 = r6.j(r0)
            if (r4 != r1) goto L_0x0058
            return r1
        L_0x00c3:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.removeStealDevice(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void saveWatchParamModel(WatchParam watchParam) {
        ee7.b(watchParam, "watchParam");
        this.mSkuDao.addOrUpdateWatchParam(watchParam);
    }

    @DexIgnore
    public final Object swapPairingKey(String str, String str2, fb7<? super zi5<String>> fb7) {
        return this.mDeviceRemoteDataSource.swapPairingKey(str, str2, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateDevice(com.portfolio.platform.data.model.Device r9, boolean r10, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            boolean r9 = r0.Z$0
            java.lang.Object r9 = r0.L$1
            com.portfolio.platform.data.model.Device r9 = (com.portfolio.platform.data.model.Device) r9
            java.lang.Object r10 = r0.L$0
            com.portfolio.platform.data.source.DeviceRepository r10 = (com.portfolio.platform.data.source.DeviceRepository) r10
            com.fossil.t87.a(r11)
            goto L_0x00a1
        L_0x0033:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003b:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateDevice "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r5 = " isRemote "
            r4.append(r5)
            r4.append(r10)
            java.lang.String r4 = r4.toString()
            r11.d(r2, r4)
            if (r10 == 0) goto L_0x00c3
            java.lang.String r11 = r9.getDeviceId()
            boolean r11 = android.text.TextUtils.isEmpty(r11)
            if (r11 == 0) goto L_0x008f
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.String r11 = "Inside .updateDevice can't update device without serial number"
            r9.d(r10, r11)
            com.fossil.yi5 r9 = new com.fossil.yi5
            r1 = 600(0x258, float:8.41E-43)
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return r9
        L_0x008f:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r11 = r8.mDeviceRemoteDataSource
            r0.L$0 = r8
            r0.L$1 = r9
            r0.Z$0 = r10
            r0.label = r3
            java.lang.Object r11 = r11.updateDevice(r9, r0)
            if (r11 != r1) goto L_0x00a0
            return r1
        L_0x00a0:
            r10 = r8
        L_0x00a1:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            com.portfolio.platform.data.source.DeviceDao r0 = r10.mDeviceDao
            java.lang.String r9 = r9.getDeviceId()
            com.portfolio.platform.data.model.Device r9 = r0.getDeviceByDeviceId(r9)
            if (r9 == 0) goto L_0x00b5
            com.portfolio.platform.data.source.DeviceDao r10 = r10.mDeviceDao
            r10.addOrUpdateDevice(r9)
            goto L_0x00c2
        L_0x00b5:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.String r0 = "device is removed, no need to update to DB"
            r9.d(r10, r0)
        L_0x00c2:
            return r11
        L_0x00c3:
            java.lang.String r10 = r9.getDeviceId()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x00d2
            com.portfolio.platform.data.source.DeviceDao r10 = r8.mDeviceDao
            r10.addOrUpdateDevice(r9)
        L_0x00d2:
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r11 = 2
            r0 = 0
            r9.<init>(r0, r10, r11, r0)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.updateDevice(com.portfolio.platform.data.model.Device, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object updateDeviceSecretKey(String str, String str2, fb7<? super zi5<ie4>> fb7) {
        return this.mDeviceRemoteDataSource.updateDeviceSecretKey(str, str2, fb7);
    }
}
