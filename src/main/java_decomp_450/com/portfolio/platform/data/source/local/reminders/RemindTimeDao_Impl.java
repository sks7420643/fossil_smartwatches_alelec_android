package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeDao_Impl implements RemindTimeDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<RemindTimeModel> __insertionAdapterOfRemindTimeModel;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<RemindTimeModel> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `remindTimeModel` (`remindTimeName`,`minutes`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, RemindTimeModel remindTimeModel) {
            if (remindTimeModel.getRemindTimeName() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, remindTimeModel.getRemindTimeName());
            }
            ajVar.bindLong(2, (long) remindTimeModel.getMinutes());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM remindTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public RemindTimeModel call() throws Exception {
            RemindTimeModel remindTimeModel = null;
            Cursor a = pi.a(RemindTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "remindTimeName");
                int b2 = oi.b(a, "minutes");
                if (a.moveToFirst()) {
                    remindTimeModel = new RemindTimeModel(a.getString(b), a.getInt(b2));
                }
                return remindTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public RemindTimeDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfRemindTimeModel = new Anon1(ciVar);
        this.__preparedStmtOfDelete = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public LiveData<RemindTimeModel> getRemindTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"remindTimeModel"}, false, (Callable) new Anon3(fi.b("SELECT * FROM remindTimeModel", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public RemindTimeModel getRemindTimeModel() {
        fi b = fi.b("SELECT * FROM remindTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        RemindTimeModel remindTimeModel = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "remindTimeName");
            int b3 = oi.b(a, "minutes");
            if (a.moveToFirst()) {
                remindTimeModel = new RemindTimeModel(a.getString(b2), a.getInt(b3));
            }
            return remindTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void upsertRemindTimeModel(RemindTimeModel remindTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfRemindTimeModel.insert(remindTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
