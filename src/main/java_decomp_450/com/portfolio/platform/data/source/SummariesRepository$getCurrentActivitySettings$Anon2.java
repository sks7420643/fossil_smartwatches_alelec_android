package com.portfolio.platform.data.source;

import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$getCurrentActivitySettings$2", f = "SummariesRepository.kt", l = {58}, m = "invokeSuspend")
public final class SummariesRepository$getCurrentActivitySettings$Anon2 extends zb7 implements kd7<yi7, fb7<? super ActivitySettings>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    public SummariesRepository$getCurrentActivitySettings$Anon2(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$getCurrentActivitySettings$Anon2 summariesRepository$getCurrentActivitySettings$Anon2 = new SummariesRepository$getCurrentActivitySettings$Anon2(fb7);
        summariesRepository$getCurrentActivitySettings$Anon2.p$ = (yi7) obj;
        return summariesRepository$getCurrentActivitySettings$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super ActivitySettings> fb7) {
        return ((SummariesRepository$getCurrentActivitySettings$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivitySettings activitySetting = ((FitnessDatabase) obj).activitySummaryDao().getActivitySetting();
        if (activitySetting == null) {
            return new ActivitySettings(VideoUploader.RETRY_DELAY_UNIT_MS, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 30);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getCurrentActivitySettings - " + "stepGoal=" + activitySetting.getCurrentStepGoal() + ", caloriesGoal=" + activitySetting.getCurrentCaloriesGoal() + ", " + "activeTimeGoal=" + activitySetting.getCurrentActiveTimeGoal());
        return new ActivitySettings(activitySetting.getCurrentStepGoal(), activitySetting.getCurrentCaloriesGoal(), activitySetting.getCurrentActiveTimeGoal());
    }
}
