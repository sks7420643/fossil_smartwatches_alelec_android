package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRemoteDataSource_Factory implements Factory<WatchAppRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public WatchAppRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static WatchAppRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new WatchAppRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static WatchAppRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new WatchAppRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
