package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$insertFromDevice$2", f = "WorkoutSessionRepository.kt", l = {134}, m = "invokeSuspend")
public final class WorkoutSessionRepository$insertFromDevice$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $workoutSessions;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$insertFromDevice$Anon2(List list, fb7 fb7) {
        super(2, fb7);
        this.$workoutSessions = list;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSessionRepository$insertFromDevice$Anon2 workoutSessionRepository$insertFromDevice$Anon2 = new WorkoutSessionRepository$insertFromDevice$Anon2(this.$workoutSessions, fb7);
        workoutSessionRepository$insertFromDevice$Anon2.p$ = (yi7) obj;
        return workoutSessionRepository$insertFromDevice$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((WorkoutSessionRepository$insertFromDevice$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "insertFromDevice: workoutSessions = " + this.$workoutSessions);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((FitnessDatabase) obj).getWorkoutDao().upsertListWorkoutSession(this.$workoutSessions);
        FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "insertFromDevice: workoutSessions = DONE!!!");
        return i97.a;
    }
}
