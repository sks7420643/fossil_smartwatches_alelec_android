package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.ev4;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleDao_Impl implements RingStyleDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<DianaComplicationRingStyle> __insertionAdapterOfDianaComplicationRingStyle;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllData;
    @DexIgnore
    public /* final */ ev4 __ringStyleConverter; // = new ev4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<DianaComplicationRingStyle> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaComplicationRingStyle` (`id`,`category`,`name`,`data`,`metaData`,`serial`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DianaComplicationRingStyle dianaComplicationRingStyle) {
            if (dianaComplicationRingStyle.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, dianaComplicationRingStyle.getId());
            }
            if (dianaComplicationRingStyle.getCategory() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, dianaComplicationRingStyle.getCategory());
            }
            if (dianaComplicationRingStyle.getName() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, dianaComplicationRingStyle.getName());
            }
            String a = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getData());
            if (a == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a);
            }
            String a2 = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getMetaData());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            if (dianaComplicationRingStyle.getSerial() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, dianaComplicationRingStyle.getSerial());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM DianaComplicationRingStyle";
        }
    }

    @DexIgnore
    public RingStyleDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfDianaComplicationRingStyle = new Anon1(ciVar);
        this.__preparedStmtOfClearAllData = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void clearAllData() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public List<DianaComplicationRingStyle> getRingStyles() {
        fi b = fi.b("SELECT * FROM DianaComplicationRingStyle", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "category");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "data");
            int b6 = oi.b(a, "metaData");
            int b7 = oi.b(a, "serial");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DianaComplicationRingStyle(a.getString(b2), a.getString(b3), a.getString(b4), this.__ringStyleConverter.a(a.getString(b5)), this.__ringStyleConverter.b(a.getString(b6)), a.getString(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public DianaComplicationRingStyle getRingStylesBySerial(String str) {
        fi b = fi.b("SELECT * FROM DianaComplicationRingStyle WHERE serial = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaComplicationRingStyle dianaComplicationRingStyle = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "category");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "data");
            int b6 = oi.b(a, "metaData");
            int b7 = oi.b(a, "serial");
            if (a.moveToFirst()) {
                dianaComplicationRingStyle = new DianaComplicationRingStyle(a.getString(b2), a.getString(b3), a.getString(b4), this.__ringStyleConverter.a(a.getString(b5)), this.__ringStyleConverter.b(a.getString(b6)), a.getString(b7));
            }
            return dianaComplicationRingStyle;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(dianaComplicationRingStyle);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyles(List<DianaComplicationRingStyle> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
