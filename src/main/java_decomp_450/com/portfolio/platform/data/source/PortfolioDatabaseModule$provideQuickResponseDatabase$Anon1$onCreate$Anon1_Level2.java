package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ig5;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.w97;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.PortfolioDatabaseModule$provideQuickResponseDatabase$1$onCreate$1", f = "PortfolioDatabaseModule.kt", l = {}, m = "invokeSuspend")
public final class PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2(PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1 portfolioDatabaseModule$provideQuickResponseDatabase$Anon1, fb7 fb7) {
        super(2, fb7);
        this.this$0 = portfolioDatabaseModule$provideQuickResponseDatabase$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2 = new PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2(this.this$0, fb7);
        portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2.p$ = (yi7) obj;
        return portfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1$onCreate$Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        QuickResponseMessageDao quickResponseMessageDao;
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            String a = ig5.a(PortfolioApp.g0.c(), 2131886131);
            ee7.a((Object) a, "LanguageHelper.getString\u2026an_Text__CanICallYouBack)");
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886132);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026_MessageOn_Text__OnMyWay)");
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886133);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026y_Text__SorryCantTalkNow)");
            List<QuickResponseMessage> c = w97.c(new QuickResponseMessage(a), new QuickResponseMessage(a2), new QuickResponseMessage(a3));
            T t = this.this$0.$dbInstance.element;
            if (!(t == null || (quickResponseMessageDao = t.quickResponseMessageDao()) == null)) {
                quickResponseMessageDao.insertResponses(c);
            }
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
