package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.be4;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.response.sleep.SleepDayParse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchSleepSummaries$2", f = "SleepSummariesRepository.kt", l = {217, 225}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchSleepSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ie4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends TypeToken<ApiResponse<SleepDayParse>> {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchSleepSummaries$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$fetchSleepSummaries$Anon2 sleepSummariesRepository$fetchSleepSummaries$Anon2 = new SleepSummariesRepository$fetchSleepSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, fb7);
        sleepSummariesRepository$fetchSleepSummaries$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$fetchSleepSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ie4>> fb7) {
        return ((SleepSummariesRepository$fetchSleepSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        zi5 zi5;
        Exception e;
        ArrayList arrayList;
        yi7 yi7;
        String message;
        List<SleepDayParse> list;
        Object a = nb7.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "loadSummaries");
            SleepSummariesRepository$fetchSleepSummaries$Anon2$response$Anon1_Level2 sleepSummariesRepository$fetchSleepSummaries$Anon2$response$Anon1_Level2 = new SleepSummariesRepository$fetchSleepSummaries$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(sleepSummariesRepository$fetchSleepSummaries$Anon2$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            arrayList = (ArrayList) this.L$2;
            zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            try {
                t87.a(obj);
                ((SleepDatabase) obj).sleepDao().upsertSleepDays(arrayList);
            } catch (Exception e2) {
                e = e2;
            }
            return zi5;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            bj5 bj5 = (bj5) zi52;
            if (bj5.a() == null || bj5.b()) {
                return zi52;
            }
            try {
                ArrayList arrayList2 = new ArrayList();
                ApiResponse apiResponse = (ApiResponse) new be4().a().a(((ie4) ((bj5) zi52).a()).toString(), new Anon1_Level2().getType());
                if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                    for (SleepDayParse sleepDayParse : list) {
                        MFSleepDay mFSleepBySleepDayParse = sleepDayParse.getMFSleepBySleepDayParse();
                        if (mFSleepBySleepDayParse != null) {
                            arrayList2.add(mFSleepBySleepDayParse);
                        }
                    }
                }
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.L$1 = zi52;
                this.L$2 = arrayList2;
                this.label = 2;
                Object d = pg5.d(this);
                if (d == a) {
                    return a;
                }
                arrayList = arrayList2;
                zi5 = zi52;
                obj = d;
                ((SleepDatabase) obj).sleepDao().upsertSleepDays(arrayList);
                return zi5;
            } catch (Exception e3) {
                zi5 = zi52;
                e = e3;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = SleepSummariesRepository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("loadSummaries exception=");
                e.printStackTrace();
                sb.append(i97.a);
                local.d(access$getTAG$cp, sb.toString());
                return zi5;
            }
        } else if (!(zi52 instanceof yi5)) {
            return zi52;
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp2 = SleepSummariesRepository.TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("loadSummaries Failure code=");
            yi5 yi5 = (yi5) zi52;
            sb2.append(yi5.a());
            sb2.append(" message=");
            ServerError c = yi5.c();
            if (c == null || (message = c.getMessage()) == null) {
                ServerError c2 = yi5.c();
                if (c2 != null) {
                    str = c2.getUserMessage();
                }
            } else {
                str = message;
            }
            if (str == null) {
                str = "";
            }
            sb2.append(str);
            local2.d(access$getTAG$cp2, sb2.toString());
            return zi52;
        }
    }
}
