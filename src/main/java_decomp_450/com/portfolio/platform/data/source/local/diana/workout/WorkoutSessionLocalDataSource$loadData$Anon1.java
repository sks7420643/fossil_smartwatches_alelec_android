package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.tk7;
import com.fossil.vh7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1", f = "WorkoutSessionLocalDataSource.kt", l = {80, 83, 88}, m = "invokeSuspend")
public final class WorkoutSessionLocalDataSource$loadData$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ te5.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$1", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, fb7 fb7) {
            super(2, fb7);
            this.this$0 = workoutSessionLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.$helperCallback.a();
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource$loadData$1$2", f = "WorkoutSessionLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ zi5 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1, zi5 zi5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = workoutSessionLocalDataSource$loadData$Anon1;
            this.$data = zi5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, fb7);
            anon2_Level2.p$ = (yi7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon2_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (((yi5) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((yi5) this.$data).d());
                } else if (((yi5) this.$data).c() != null) {
                    ServerError c = ((yi5) this.$data).c();
                    te5.b.a aVar = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionLocalDataSource$loadData$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, int i, te5.b.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = workoutSessionLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSessionLocalDataSource$loadData$Anon1 workoutSessionLocalDataSource$loadData$Anon1 = new WorkoutSessionLocalDataSource$loadData$Anon1(this.this$0, this.$offset, this.$helperCallback, fb7);
        workoutSessionLocalDataSource$loadData$Anon1.p$ = (yi7) obj;
        return workoutSessionLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((WorkoutSessionLocalDataSource$loadData$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        List<FitnessDataWrapper> list;
        yi7 yi7;
        Object obj2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi72 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = WorkoutSessionLocalDataSource.Companion.getTAG();
            local.d(tag, "loadData currentDate = " + this.this$0.currentDate + ", offset=" + this.$offset);
            list = this.this$0.mFitnessDatabase.getFitnessDataDao().getFitnessData(this.this$0.currentDate, this.this$0.currentDate);
            if (list.isEmpty()) {
                WorkoutSessionRepository access$getWorkoutSessionRepository$p = this.this$0.workoutSessionRepository;
                Date access$getCurrentDate$p = this.this$0.currentDate;
                Date access$getCurrentDate$p2 = this.this$0.currentDate;
                int i2 = this.$offset;
                this.L$0 = yi72;
                this.L$1 = list;
                this.label = 1;
                obj2 = WorkoutSessionRepository.fetchWorkoutSessions$default(access$getWorkoutSessionRepository$p, access$getCurrentDate$p, access$getCurrentDate$p2, i2, 0, this, 8, null);
                if (obj2 == a) {
                    return a;
                }
                yi7 = yi72;
            } else {
                this.$helperCallback.a();
                return i97.a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
            list = (List) this.L$1;
            obj2 = obj;
        } else if (i == 2 || i == 3) {
            zi5 zi5 = (zi5) this.L$2;
            List list2 = (List) this.L$1;
            yi7 yi73 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj2;
        if (zi52 instanceof bj5) {
            WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$0;
            workoutSessionLocalDataSource.mOffset = workoutSessionLocalDataSource.mOffset + 100;
            tk7 c = qj7.c();
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.L$1 = list;
            this.L$2 = zi52;
            this.label = 2;
            if (vh7.a(c, anon1_Level2, this) == a) {
                return a;
            }
        } else if (zi52 instanceof yi5) {
            tk7 c2 = qj7.c();
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, zi52, null);
            this.L$0 = yi7;
            this.L$1 = list;
            this.L$2 = zi52;
            this.label = 3;
            if (vh7.a(c2, anon2_Level2, this) == a) {
                return a;
            }
        }
        return i97.a;
    }
}
