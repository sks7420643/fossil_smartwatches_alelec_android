package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.QuickResponseMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseMessageDao_Impl extends QuickResponseMessageDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<QuickResponseMessage> __insertionAdapterOfQuickResponseMessage;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveAll;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<QuickResponseMessage> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseMessage` (`id`,`response`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(aj ajVar, QuickResponseMessage quickResponseMessage) {
            ajVar.bindLong(1, (long) quickResponseMessage.getId());
            if (quickResponseMessage.getResponse() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, quickResponseMessage.getResponse());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM quickResponseMessage WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM quickResponseMessage";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<QuickResponseMessage> call() throws Exception {
            Cursor a = pi.a(QuickResponseMessageDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "response");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(a.getString(b2));
                    quickResponseMessage.setId(a.getInt(b));
                    arrayList.add(quickResponseMessage);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public QuickResponseMessageDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfQuickResponseMessage = new Anon1(ciVar);
        this.__preparedStmtOfRemoveById = new Anon2(ciVar);
        this.__preparedStmtOfRemoveAll = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public List<QuickResponseMessage> getAllRawResponse() {
        fi b = fi.b("SELECT * FROM quickResponseMessage", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "response");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                QuickResponseMessage quickResponseMessage = new QuickResponseMessage(a.getString(b3));
                quickResponseMessage.setId(a.getInt(b2));
                arrayList.add(quickResponseMessage);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public LiveData<List<QuickResponseMessage>> getAllResponse() {
        return this.__db.getInvalidationTracker().a(new String[]{"quickResponseMessage"}, false, (Callable) new Anon4(fi.b("SELECT * FROM quickResponseMessage", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponse(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponses(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
