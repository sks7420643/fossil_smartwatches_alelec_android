package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wi;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends li {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        ee7.b(wiVar, "database");
        wiVar.beginTransaction();
        try {
            wiVar.execSQL("CREATE TABLE `watchParam` (`prefixSerial` TEXT, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXTPRIMARY KEY(`prefixSerial`))");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(DeviceDatabase.TAG, "migrate DeviceDatabase from 1->2 failed");
            wiVar.execSQL("DROP TABLE IF EXISTS watchParam");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS watchParam (prefixSerial TEXT PRIMARY KEY NOT NULL, versionMajor TEXT, versionMinor TEXT, data TEXT)");
        }
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
    }
}
