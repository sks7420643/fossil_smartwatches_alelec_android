package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.te5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource$loadAfter$Anon1 implements te5.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadAfter$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.te5.b
    public final void run(te5.b.a aVar) {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
        te5.d dVar = te5.d.AFTER;
        ee7.a((Object) aVar, "helperCallback");
        ik7 unused = goalTrackingDataLocalDataSource.loadData(dVar, aVar, this.this$0.mOffset);
    }
}
