package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummary$2", f = "SleepSummariesRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 141}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummary$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends MFSleepDay>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummary$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<MFSleepDay, ie4> {
            @DexIgnore
            public /* final */ /* synthetic */ List $fitnessDataList;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, List list) {
                this.this$0 = anon1_Level2;
                this.$fitnessDataList = list;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ie4>> fb7) {
                Date q = zd5.q(this.this$0.this$0.$date);
                Date l = zd5.l(this.this$0.this$0.$date);
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "calendar");
                instance.setTimeInMillis(0);
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                String g = zd5.g(q);
                ee7.a((Object) g, "DateHelper.formatShortDate(startDate)");
                String g2 = zd5.g(l);
                ee7.a((Object) g2, "DateHelper.formatShortDate(endDate)");
                return access$getMApiService$p.getSleepSummaries(g, g2, 0, 100, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<MFSleepDay>> fb7) {
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "calendar");
                instance.setTime(this.this$0.this$0.$date);
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                String g = zd5.g(this.this$0.this$0.$date);
                ee7.a((Object) g, "DateHelper.formatShortDate(date)");
                return sleepDao.getSleepDayLiveData(g);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "getActivityList onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ie4 ie4, fb7 fb7) {
                return saveCallResult(ie4, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object saveCallResult(ie4 ie4, fb7<? super i97> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = SleepSummariesRepository.TAG;
                local.d(access$getTAG$cp, "getSleepSummary saveCallResult onResponse: response = " + ie4);
                SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2 = this.this$0.this$0;
                Object saveSleepSummary = sleepSummariesRepository$getSleepSummary$Anon2.this$0.saveSleepSummary(ie4, sleepSummariesRepository$getSleepSummary$Anon2.$date, fb7);
                if (saveSleepSummary == nb7.a()) {
                    return saveSleepSummary;
                }
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(MFSleepDay mFSleepDay) {
                return this.$fitnessDataList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2, SleepDatabase sleepDatabase) {
            this.this$0 = sleepSummariesRepository$getSleepSummary$Anon2;
            this.$sleepDatabase = sleepDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<MFSleepDay>> apply(List<FitnessDataWrapper> list) {
            return new Anon1_Level3(this, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummary$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$getSleepSummary$Anon2 sleepSummariesRepository$getSleepSummary$Anon2 = new SleepSummariesRepository$getSleepSummary$Anon2(this.this$0, this.$date, fb7);
        sleepSummariesRepository$getSleepSummary$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$getSleepSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends MFSleepDay>>> fb7) {
        return ((SleepSummariesRepository$getSleepSummary$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        FitnessDatabase fitnessDatabase;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = SleepSummariesRepository.TAG;
            local.d(access$getTAG$cp, "getSleepSummary date=" + this.$date);
            ti7 b = qj7.b();
            SleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2 sleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2 = new SleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, sleepSummariesRepository$getSleepSummary$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            fitnessDatabase = (FitnessDatabase) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
            Date date = this.$date;
            return ge.b(fitnessDataDao.getFitnessDataLiveData(date, date), new Anon1_Level2(this, (SleepDatabase) obj));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase2 = (FitnessDatabase) obj;
        ti7 b2 = qj7.b();
        SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 sleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2 = new SleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2(null);
        this.L$0 = yi7;
        this.L$1 = fitnessDatabase2;
        this.label = 2;
        Object a2 = vh7.a(b2, sleepSummariesRepository$getSleepSummary$Anon2$sleepDatabase$Anon1_Level2, this);
        if (a2 == a) {
            return a;
        }
        fitnessDatabase = fitnessDatabase2;
        obj = a2;
        FitnessDataDao fitnessDataDao2 = fitnessDatabase.getFitnessDataDao();
        Date date2 = this.$date;
        return ge.b(fitnessDataDao2.getFitnessDataLiveData(date2, date2), new Anon1_Level2(this, (SleepDatabase) obj));
    }
}
