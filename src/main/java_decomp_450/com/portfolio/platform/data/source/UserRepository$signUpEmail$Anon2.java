package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb5;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.p87;
import com.fossil.pg5;
import com.fossil.r87;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ud5;
import com.fossil.xd5;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.za5;
import com.fossil.zb7;
import com.fossil.zd5;
import com.fossil.zi5;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.AuthKt;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$signUpEmail$2", f = "UserRepository.kt", l = {197, 217}, m = "invokeSuspend")
public final class UserRepository$signUpEmail$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser.Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpEmailAuth $emailAuth;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$signUpEmail$Anon2(UserRepository userRepository, SignUpEmailAuth signUpEmailAuth, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
        this.$emailAuth = signUpEmailAuth;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$signUpEmail$Anon2 userRepository$signUpEmail$Anon2 = new UserRepository$signUpEmail$Anon2(this.this$0, this.$emailAuth, fb7);
        userRepository$signUpEmail$Anon2.p$ = (yi7) obj;
        return userRepository$signUpEmail$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return ((UserRepository$signUpEmail$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            UserRemoteDataSource access$getMUserRemoteDataSource$p = this.this$0.mUserRemoteDataSource;
            SignUpEmailAuth signUpEmailAuth = this.$emailAuth;
            this.L$0 = yi7;
            this.label = 1;
            obj = access$getMUserRemoteDataSource$p.signUpEmail(signUpEmailAuth, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            r87 r87 = (r87) this.L$4;
            mFUser = (MFUser) this.L$3;
            Auth auth = (Auth) this.L$2;
            zi5 zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            ((UserDatabase) obj).userDao().insertUser(mFUser);
            this.this$0.mSharedPreferencesManager.a(System.currentTimeMillis());
            return new bj5(mFUser.getAuth(), false, 2, null);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            Auth auth2 = (Auth) ((bj5) zi52).a();
            String email = this.$emailAuth.getEmail();
            if (auth2 != null) {
                String uid = auth2.getUid();
                if (uid == null) {
                    uid = "";
                }
                MFUser mFUser2 = new MFUser(email, uid);
                r87<Integer, Integer> a2 = ud5.a.a(fb5.Companion.a(this.$emailAuth.getGender()), mFUser2.getAge(this.$emailAuth.getBirthday()));
                String value = za5.EMAIL.getValue();
                ee7.a((Object) value, "AuthType.EMAIL.value");
                mFUser2.setAuthType(value);
                MFUser.Auth auth3 = AuthKt.toAuth(auth2);
                if (auth3 != null) {
                    mFUser2.setAuth(auth3);
                    String uid2 = auth2.getUid();
                    if (uid2 != null) {
                        mFUser2.setUserId(uid2);
                        mFUser2.setFirstName(this.$emailAuth.getFirstName());
                        mFUser2.setLastName(this.$emailAuth.getLastName());
                        mFUser2.setGender(this.$emailAuth.getGender());
                        mFUser2.setBirthday(this.$emailAuth.getBirthday());
                        mFUser2.setDiagnosticEnabled(this.$emailAuth.getDiagnosticEnabled());
                        String y = zd5.y(new Date());
                        ee7.a((Object) y, "DateHelper.toJodaTime(Date())");
                        mFUser2.setCreatedAt(y);
                        mFUser2.setUpdatedAt(mFUser2.getCreatedAt());
                        mFUser2.setHeightInCentimeters(a2.getFirst().intValue());
                        mFUser2.setUseDefaultBiometric(true);
                        mFUser2.setUseDefaultGoals(true);
                        mFUser2.setWeightInGrams((int) xd5.h((float) a2.getSecond().intValue()));
                        pg5 pg5 = pg5.i;
                        this.L$0 = yi7;
                        this.L$1 = zi52;
                        this.L$2 = auth2;
                        this.L$3 = mFUser2;
                        this.L$4 = a2;
                        this.label = 2;
                        obj = pg5.f(this);
                        if (obj == a) {
                            return a;
                        }
                        mFUser = mFUser2;
                        ((UserDatabase) obj).userDao().insertUser(mFUser);
                        this.this$0.mSharedPreferencesManager.a(System.currentTimeMillis());
                        return new bj5(mFUser.getAuth(), false, 2, null);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        } else if (zi52 instanceof yi5) {
            yi5 yi5 = (yi5) zi52;
            return new yi5(yi5.a(), yi5.c(), yi5.d(), null, null, 24, null);
        } else {
            throw new p87();
        }
    }
}
