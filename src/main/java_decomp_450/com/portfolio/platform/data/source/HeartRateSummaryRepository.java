package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public List<HeartRateSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "HeartRateSummaryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryRepository(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new HeartRateSummaryRepository$cleanUp$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getHeartRateSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1 r0 = (com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1 r0 = new com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.HeartRateSummaryRepository r11 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon2 r2 = new com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSummaryRepository.getHeartRateSummaries(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummariesPaging(com.portfolio.platform.data.source.FitnessDataRepository r17, java.util.Date r18, com.fossil.pj4 r19, com.fossil.te5.a r20, com.fossil.fb7<? super com.portfolio.platform.data.Listing<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> r21) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r21
            boolean r4 = r3 instanceof com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon1
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon1 r4 = (com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon1) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon1 r4 = new com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon1
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            java.lang.String r7 = "calendar"
            r8 = 1
            if (r6 == 0) goto L_0x0065
            if (r6 != r8) goto L_0x005d
            java.lang.Object r1 = r4.L$8
            com.portfolio.platform.data.source.FitnessDataRepository r1 = (com.portfolio.platform.data.source.FitnessDataRepository) r1
            java.lang.Object r2 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r2 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r2
            java.lang.Object r5 = r4.L$6
            java.util.Calendar r5 = (java.util.Calendar) r5
            java.lang.Object r6 = r4.L$5
            java.util.Date r6 = (java.util.Date) r6
            java.lang.Object r6 = r4.L$4
            com.fossil.te5$a r6 = (com.fossil.te5.a) r6
            java.lang.Object r8 = r4.L$3
            com.fossil.pj4 r8 = (com.fossil.pj4) r8
            java.lang.Object r9 = r4.L$2
            java.util.Date r9 = (java.util.Date) r9
            java.lang.Object r10 = r4.L$1
            com.portfolio.platform.data.source.FitnessDataRepository r10 = (com.portfolio.platform.data.source.FitnessDataRepository) r10
            java.lang.Object r4 = r4.L$0
            com.portfolio.platform.data.source.HeartRateSummaryRepository r4 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r4
            com.fossil.t87.a(r3)
            r10 = r1
            r15 = r5
            r14 = r6
            r13 = r8
            r12 = r9
            r9 = r2
            goto L_0x00b1
        L_0x005d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0065:
            com.fossil.t87.a(r3)
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource$Companion r3 = com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource.Companion
            java.util.Calendar r6 = java.util.Calendar.getInstance()
            java.lang.String r9 = "Calendar.getInstance()"
            com.fossil.ee7.a(r6, r9)
            java.util.Date r6 = r6.getTime()
            java.lang.String r9 = "Calendar.getInstance().time"
            com.fossil.ee7.a(r6, r9)
            java.util.Date r3 = r3.calculateNextKey(r6, r2)
            java.util.Calendar r6 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r6, r7)
            r6.setTime(r3)
            com.fossil.pg5 r9 = com.fossil.pg5.i
            r4.L$0 = r0
            r4.L$1 = r1
            r4.L$2 = r2
            r10 = r19
            r4.L$3 = r10
            r11 = r20
            r4.L$4 = r11
            r4.L$5 = r3
            r4.L$6 = r6
            r4.L$7 = r0
            r4.L$8 = r1
            r4.label = r8
            java.lang.Object r3 = r9.b(r4)
            if (r3 != r5) goto L_0x00ab
            return r5
        L_0x00ab:
            r9 = r0
            r12 = r2
            r15 = r6
            r13 = r10
            r14 = r11
            r10 = r1
        L_0x00b1:
            r11 = r3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r11 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r11
            com.fossil.ee7.a(r15, r7)
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory r1 = new com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory
            r8 = r1
            r8.<init>(r9, r10, r11, r12, r13, r14, r15)
            com.fossil.qf$f$a r2 = new com.fossil.qf$f$a
            r2.<init>()
            r3 = 30
            r2.a(r3)
            r4 = 0
            r2.a(r4)
            r2.b(r3)
            r3 = 5
            r2.c(r3)
            com.fossil.qf$f r2 = r2.a()
            java.lang.String r3 = "PagedList.Config.Builder\u2026\n                .build()"
            com.fossil.ee7.a(r2, r3)
            com.fossil.nf r3 = new com.fossil.nf
            r3.<init>(r1, r2)
            androidx.lifecycle.LiveData r2 = r3.a()
            java.lang.String r3 = "LivePagedListBuilder(sou\u2026eFactory, config).build()"
            com.fossil.ee7.a(r2, r3)
            com.portfolio.platform.data.Listing r3 = new com.portfolio.platform.data.Listing
            androidx.lifecycle.MutableLiveData r4 = r1.getSourceLiveData()
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon2 r5 = com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon2.INSTANCE
            androidx.lifecycle.LiveData r4 = com.fossil.ge.b(r4, r5)
            java.lang.String r5 = "Transformations.switchMa\u2026rkState\n                }"
            com.fossil.ee7.a(r4, r5)
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon3 r5 = new com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon3
            r5.<init>(r1)
            com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon4 r6 = new com.portfolio.platform.data.source.HeartRateSummaryRepository$getSummariesPaging$Anon4
            r6.<init>(r1)
            r3.<init>(r2, r4, r5, r6)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSummaryRepository.getSummariesPaging(com.portfolio.platform.data.source.FitnessDataRepository, java.util.Date, com.fossil.pj4, com.fossil.te5$a, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<DailyHeartRateSummary> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new HeartRateSummaryRepository$insertFromDevice$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object loadSummaries(Date date, Date date2, fb7<? super zi5<ie4>> fb7) {
        return vh7.a(qj7.b(), new HeartRateSummaryRepository$loadSummaries$Anon2(this, date, date2, null), fb7);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory : this.mSourceFactoryList) {
            HeartRateSummaryLocalDataSource localDataSource = heartRateSummaryDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }
}
