package com.portfolio.platform.data.source.local.diana.heartrate;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.r87;
import com.fossil.te5;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource$loadAfter$Anon1 implements te5.b {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadAfter$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.te5.b
    public final void run(te5.b.a aVar) {
        r87 r87 = (r87) ea7.d(this.this$0.mRequestAfterQueue);
        ee7.a((Object) aVar, "helperCallback");
        ik7 unused = this.this$0.loadData((Date) r87.getFirst(), (Date) r87.getSecond(), aVar);
    }
}
