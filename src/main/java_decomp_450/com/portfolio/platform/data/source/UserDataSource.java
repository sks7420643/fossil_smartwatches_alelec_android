package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.pb7;
import com.fossil.zi5;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSetting$suspendImpl(UserDataSource userDataSource, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSettingFromServer$suspendImpl(UserDataSource userDataSource, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, fb7 fb7) {
        return new bj5(pb7.a(200), false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ Object sendUserSettingToServer$suspendImpl(UserDataSource userDataSource, UserSettings userSettings, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, fb7 fb7) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, fb7<? super zi5<Boolean>> fb7) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, fb7);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, fb7<? super zi5<Boolean>> fb7) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, fb7);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, fb7<? super Integer> fb7) {
        return pb7.a(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public Object getUserSetting(fb7<? super UserSettings> fb7) {
        return getUserSetting$suspendImpl(this, fb7);
    }

    @DexIgnore
    public Object getUserSettingFromServer(fb7<? super zi5<UserSettings>> fb7) {
        return getUserSettingFromServer$suspendImpl(this, fb7);
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object insertUserSetting(UserSettings userSettings, fb7<? super i97> fb7) {
        return i97.a;
    }

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, fb7<? super zi5<MFUser>> fb7) {
        return loadUserInfo$suspendImpl(this, mFUser, fb7);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, fb7<? super zi5<Auth>> fb7) {
        return loginEmail$suspendImpl(this, str, str2, fb7);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, fb7<? super zi5<Auth>> fb7) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, fb7);
    }

    @DexIgnore
    public Object logoutUser(fb7<? super Integer> fb7) {
        return pb7.a(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, fb7<? super zi5<Void>> fb7) {
        return requestEmailOtp$suspendImpl(this, str, fb7);
    }

    @DexIgnore
    public Object resetPassword(String str, fb7<? super zi5<Integer>> fb7) {
        return resetPassword$suspendImpl(this, str, fb7);
    }

    @DexIgnore
    public Object sendUserSettingToServer(UserSettings userSettings, fb7<? super zi5<UserSettings>> fb7) {
        return sendUserSettingToServer$suspendImpl(this, userSettings, fb7);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, fb7<? super zi5<Auth>> fb7) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, fb7);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, fb7<? super zi5<Auth>> fb7) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, fb7);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, fb7<? super zi5<MFUser>> fb7);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, fb7<? super zi5<Void>> fb7) {
        return verifyEmailOtp$suspendImpl(this, str, str2, fb7);
    }
}
