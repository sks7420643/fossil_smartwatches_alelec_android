package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$2", f = "HeartRateSampleRepository.kt", l = {95, 107, 110}, m = "invokeSuspend")
public final class HeartRateSampleRepository$fetchHeartRateSamples$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<HeartRate>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$fetchHeartRateSamples$Anon2(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = heartRateSampleRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        HeartRateSampleRepository$fetchHeartRateSamples$Anon2 heartRateSampleRepository$fetchHeartRateSamples$Anon2 = new HeartRateSampleRepository$fetchHeartRateSamples$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, fb7);
        heartRateSampleRepository$fetchHeartRateSamples$Anon2.p$ = (yi7) obj;
        return heartRateSampleRepository$fetchHeartRateSamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<HeartRate>>> fb7) {
        return ((HeartRateSampleRepository$fetchHeartRateSamples$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r11.label
            r2 = 3
            r3 = 2
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x0047
            if (r1 == r5) goto L_0x003f
            if (r1 == r3) goto L_0x002a
            if (r1 != r2) goto L_0x0022
            java.lang.Object r0 = r11.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r11.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r12)     // Catch:{ Exception -> 0x001f }
            goto L_0x0136
        L_0x001f:
            r12 = move-exception
            goto L_0x0141
        L_0x0022:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x002a:
            java.lang.Object r1 = r11.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r3 = r11.L$1
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r5 = r11.L$0
            com.fossil.yi7 r5 = (com.fossil.yi7) r5
            com.fossil.t87.a(r12)     // Catch:{ Exception -> 0x003b }
            goto L_0x00ea
        L_0x003b:
            r12 = move-exception
            r0 = r3
            goto L_0x0141
        L_0x003f:
            java.lang.Object r1 = r11.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r12)
            goto L_0x0089
        L_0x0047:
            com.fossil.t87.a(r12)
            com.fossil.yi7 r12 = r11.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.HeartRateSampleRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fetchHeartRateSamples: start = "
            r7.append(r8)
            java.util.Date r8 = r11.$start
            r7.append(r8)
            java.lang.String r8 = ", end = "
            r7.append(r8)
            java.util.Date r8 = r11.$end
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r1.d(r6, r7)
            com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$Anon2$repoResponse$Anon1_Level2 r1 = new com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$Anon2$repoResponse$Anon1_Level2
            r1.<init>(r11, r4)
            r11.L$0 = r12
            r11.label = r5
            java.lang.Object r1 = com.fossil.aj5.a(r1, r11)
            if (r1 != r0) goto L_0x0086
            return r0
        L_0x0086:
            r10 = r1
            r1 = r12
            r12 = r10
        L_0x0089:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r6 = r12 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x0166
            r6 = r12
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r6 = r6.a()
            if (r6 == 0) goto L_0x0165
            r6 = r12
            com.fossil.bj5 r6 = (com.fossil.bj5) r6     // Catch:{ Exception -> 0x013d }
            boolean r6 = r6.b()     // Catch:{ Exception -> 0x013d }
            if (r6 != 0) goto L_0x00f5
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ Exception -> 0x013d }
            r6.<init>()     // Catch:{ Exception -> 0x013d }
            r7 = r12
            com.fossil.bj5 r7 = (com.fossil.bj5) r7     // Catch:{ Exception -> 0x013d }
            java.lang.Object r7 = r7.a()     // Catch:{ Exception -> 0x013d }
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7     // Catch:{ Exception -> 0x013d }
            java.util.List r7 = r7.get_items()     // Catch:{ Exception -> 0x013d }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x013d }
        L_0x00b7:
            boolean r8 = r7.hasNext()     // Catch:{ Exception -> 0x013d }
            if (r8 == 0) goto L_0x00cd
            java.lang.Object r8 = r7.next()     // Catch:{ Exception -> 0x013d }
            com.portfolio.platform.data.model.diana.heartrate.HeartRate r8 = (com.portfolio.platform.data.model.diana.heartrate.HeartRate) r8     // Catch:{ Exception -> 0x013d }
            com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r8 = r8.toHeartRateSample()     // Catch:{ Exception -> 0x013d }
            if (r8 == 0) goto L_0x00b7
            r6.add(r8)     // Catch:{ Exception -> 0x013d }
            goto L_0x00b7
        L_0x00cd:
            boolean r7 = r6.isEmpty()     // Catch:{ Exception -> 0x013d }
            r5 = r5 ^ r7
            if (r5 == 0) goto L_0x00f5
            com.fossil.pg5 r5 = com.fossil.pg5.i     // Catch:{ Exception -> 0x013d }
            r11.L$0 = r1     // Catch:{ Exception -> 0x013d }
            r11.L$1 = r12     // Catch:{ Exception -> 0x013d }
            r11.L$2 = r6     // Catch:{ Exception -> 0x013d }
            r11.label = r3     // Catch:{ Exception -> 0x013d }
            java.lang.Object r3 = r5.b(r11)     // Catch:{ Exception -> 0x013d }
            if (r3 != r0) goto L_0x00e5
            return r0
        L_0x00e5:
            r5 = r1
            r1 = r6
            r10 = r3
            r3 = r12
            r12 = r10
        L_0x00ea:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r12 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r12
            com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao r12 = r12.getHeartRateDao()
            r12.upsertHeartRateSampleList(r1)
            r12 = r3
            r1 = r5
        L_0x00f5:
            r3 = r12
            com.fossil.bj5 r3 = (com.fossil.bj5) r3
            java.lang.Object r3 = r3.a()
            com.portfolio.platform.data.source.remote.ApiResponse r3 = (com.portfolio.platform.data.source.remote.ApiResponse) r3
            com.portfolio.platform.data.model.Range r3 = r3.get_range()
            if (r3 == 0) goto L_0x0165
            r3 = r12
            com.fossil.bj5 r3 = (com.fossil.bj5) r3
            java.lang.Object r3 = r3.a()
            com.portfolio.platform.data.source.remote.ApiResponse r3 = (com.portfolio.platform.data.source.remote.ApiResponse) r3
            com.portfolio.platform.data.model.Range r3 = r3.get_range()
            if (r3 == 0) goto L_0x0139
            boolean r3 = r3.isHasNext()
            if (r3 == 0) goto L_0x0165
            com.portfolio.platform.data.source.HeartRateSampleRepository r4 = r11.this$0
            java.util.Date r5 = r11.$start
            java.util.Date r6 = r11.$end
            int r3 = r11.$offset
            int r7 = r11.$limit
            int r7 = r7 + r3
            int r8 = r11.$limit
            r11.L$0 = r1
            r11.L$1 = r12
            r11.label = r2
            r9 = r11
            java.lang.Object r1 = r4.fetchHeartRateSamples(r5, r6, r7, r8, r9)
            if (r1 != r0) goto L_0x0134
            return r0
        L_0x0134:
            r0 = r12
            r12 = r1
        L_0x0136:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            goto L_0x0165
        L_0x0139:
            com.fossil.ee7.a()
            throw r4
        L_0x013d:
            r0 = move-exception
            r10 = r0
            r0 = r12
            r12 = r10
        L_0x0141:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HeartRateSampleRepository.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchHeartRateSamples exception="
            r3.append(r4)
            r12.printStackTrace()
            com.fossil.i97 r12 = com.fossil.i97.a
            r3.append(r12)
            java.lang.String r12 = r3.toString()
            r1.e(r2, r12)
            r12 = r0
        L_0x0165:
            return r12
        L_0x0166:
            boolean r0 = r12 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01b4
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.HeartRateSampleRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchHeartRateSamples Failure code="
            r2.append(r3)
            r3 = r12
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r5 = r3.a()
            r2.append(r5)
            java.lang.String r5 = " message="
            r2.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r3.c()
            if (r5 == 0) goto L_0x019b
            java.lang.String r5 = r5.getMessage()
            if (r5 == 0) goto L_0x019b
            r4 = r5
            goto L_0x01a5
        L_0x019b:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x01a5
            java.lang.String r4 = r3.getUserMessage()
        L_0x01a5:
            if (r4 == 0) goto L_0x01a8
            goto L_0x01aa
        L_0x01a8:
            java.lang.String r4 = ""
        L_0x01aa:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x01b4:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
