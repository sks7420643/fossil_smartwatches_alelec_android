package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.be4;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qe7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$2", f = "SleepSessionsRepository.kt", l = {68, 69}, m = "invokeSuspend")
public final class SleepSessionsRepository$getSleepSessionList$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<MFSleepSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository$getSleepSessionList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<MFSleepSession>, ie4> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, qe7 qe7, int i, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$offset = qe7;
                this.$limit = i;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ie4>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiService$p.getSleepSessions(g, g2, this.$offset.element, this.$limit, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<List<MFSleepSession>>> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = SleepSessionsRepository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("getSleepSessionList: startMilli = ");
                Date date = this.this$0.$startDate;
                ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
                sb.append(date.getTime());
                sb.append(", endMilli = ");
                Date date2 = this.this$0.$endDate;
                ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
                sb.append(date2.getTime());
                local.d(access$getTAG$cp, sb.toString());
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                Date date3 = this.this$0.$startDate;
                ee7.a((Object) date3, GoalPhase.COLUMN_START_DATE);
                long time = date3.getTime();
                Date date4 = this.this$0.$endDate;
                ee7.a((Object) date4, GoalPhase.COLUMN_END_DATE);
                return sleepDao.getSleepSessionsLiveData(time, date4.getTime());
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object processContinueFetching(ie4 ie4, fb7 fb7) {
                return processContinueFetching(ie4, (fb7<? super Boolean>) fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ie4 ie4, fb7 fb7) {
                return saveCallResult(ie4, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object processContinueFetching(ie4 ie4, fb7<? super Boolean> fb7) {
                Boolean a;
                be4 be4 = new be4();
                be4.a(DateTime.class, new GsonConvertDateTime());
                be4.a(Date.class, new GsonConverterShortDate());
                Range range = ((ApiResponse) be4.a().a(ie4.toString(), new SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$processContinueFetching$sleepSessionList$Anon1_Level4().getType())).get_range();
                if (range == null || (a = pb7.a(range.isHasNext())) == null || !a.booleanValue()) {
                    return pb7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList getActivityList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return pb7.a(true);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.fossil.ie4 r7, com.fossil.fb7<? super com.fossil.i97> r8) {
                /*
                    r6 = this;
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r8
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r6, r8)
                L_0x0018:
                    java.lang.Object r8 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    if (r2 == 0) goto L_0x003e
                    if (r2 != r3) goto L_0x0036
                    java.lang.Object r7 = r0.L$2
                    java.util.ArrayList r7 = (java.util.ArrayList) r7
                    java.lang.Object r7 = r0.L$1
                    com.fossil.ie4 r7 = (com.fossil.ie4) r7
                    java.lang.Object r7 = r0.L$0
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3 r7 = (com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList.Anon2.Anon1_Level2.Anon1_Level3) r7
                    com.fossil.t87.a(r8)     // Catch:{ Exception -> 0x00c9 }
                    goto L_0x00ed
                L_0x0036:
                    java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                    java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
                    r7.<init>(r8)
                    throw r7
                L_0x003e:
                    com.fossil.t87.a(r8)
                    com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                    java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getSleepSessionList saveCallResult onResponse: response = "
                    r4.append(r5)
                    r4.append(r7)
                    java.lang.String r4 = r4.toString()
                    r8.d(r2, r4)
                    java.util.ArrayList r8 = new java.util.ArrayList
                    r8.<init>()
                    com.fossil.be4 r2 = new com.fossil.be4
                    r2.<init>()
                    java.lang.Class<org.joda.time.DateTime> r4 = org.joda.time.DateTime.class
                    com.portfolio.platform.helper.GsonConvertDateTime r5 = new com.portfolio.platform.helper.GsonConvertDateTime
                    r5.<init>()
                    r2.a(r4, r5)
                    java.lang.Class<java.util.Date> r4 = java.util.Date.class
                    com.portfolio.platform.helper.GsonConverterShortDate r5 = new com.portfolio.platform.helper.GsonConverterShortDate
                    r5.<init>()
                    r2.a(r4, r5)
                    com.google.gson.Gson r2 = r2.a()
                    java.lang.String r4 = r7.toString()
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r5 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4
                    r5.<init>()
                    java.lang.reflect.Type r5 = r5.getType()
                    java.lang.Object r2 = r2.a(r4, r5)
                    com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
                    if (r2 == 0) goto L_0x00b4
                    java.util.List r2 = r2.get_items()
                    if (r2 == 0) goto L_0x00b4
                    java.util.Iterator r2 = r2.iterator()
                L_0x00a0:
                    boolean r4 = r2.hasNext()
                    if (r4 == 0) goto L_0x00b4
                    java.lang.Object r4 = r2.next()
                    com.portfolio.platform.response.sleep.SleepSessionParse r4 = (com.portfolio.platform.response.sleep.SleepSessionParse) r4
                    com.portfolio.platform.data.model.room.sleep.MFSleepSession r4 = r4.getMfSleepSessionBySleepSessionParse()
                    r8.add(r4)
                    goto L_0x00a0
                L_0x00b4:
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2 r2 = r6.this$0
                    com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2 r2 = r2.this$0
                    com.portfolio.platform.data.source.SleepSessionsRepository r2 = r2.this$0
                    r0.L$0 = r6
                    r0.L$1 = r7
                    r0.L$2 = r8
                    r0.label = r3
                    java.lang.Object r7 = r2.insert(r8, r0)
                    if (r7 != r1) goto L_0x00ed
                    return r1
                L_0x00c9:
                    r7 = move-exception
                    com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                    java.lang.String r0 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r2 = "getSleepSessionList saveCallResult exception="
                    r1.append(r2)
                    r7.printStackTrace()
                    com.fossil.i97 r7 = com.fossil.i97.a
                    r1.append(r7)
                    java.lang.String r7 = r1.toString()
                    r8.e(r0, r7)
                L_0x00ed:
                    com.fossil.i97 r7 = com.fossil.i97.a
                    return r7
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.fossil.ie4, com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2, Date date, Date date2, SleepDatabase sleepDatabase) {
            this.this$0 = sleepSessionsRepository$getSleepSessionList$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
            this.$sleepDatabase = sleepDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<List<MFSleepSession>>> apply(List<FitnessDataWrapper> list) {
            qe7 qe7 = new qe7();
            qe7.element = 0;
            ee7.a((Object) list, "fitnessDataList");
            Date date = this.$startDate;
            ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, qe7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$getSleepSessionList$Anon2(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2 = new SleepSessionsRepository$getSleepSessionList$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, fb7);
        sleepSessionsRepository$getSleepSessionList$Anon2.p$ = (yi7) obj;
        return sleepSessionsRepository$getSleepSessionList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<MFSleepSession>>>> fb7) {
        return ((SleepSessionsRepository$getSleepSessionList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        FitnessDatabase fitnessDatabase;
        yi7 yi7;
        Date date3;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            Date q = zd5.q(this.$start);
            date2 = zd5.l(this.$end);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = SleepSessionsRepository.TAG;
            local.d(access$getTAG$cp, "getSleepSessionList: start = " + q + ", end = " + date2);
            ti7 b = qj7.b();
            SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2 sleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2 = new SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.L$1 = q;
            this.L$2 = date2;
            this.label = 1;
            Object a2 = vh7.a(b, sleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (a2 == a) {
                return a;
            }
            date3 = q;
            obj = a2;
        } else if (i == 1) {
            date2 = (Date) this.L$2;
            date3 = (Date) this.L$1;
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            fitnessDatabase = (FitnessDatabase) this.L$3;
            date2 = (Date) this.L$2;
            date = (Date) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$start, this.$end), new Anon1_Level2(this, date, date2, (SleepDatabase) obj));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase2 = (FitnessDatabase) obj;
        ti7 b2 = qj7.b();
        SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 sleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 = new SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2(null);
        this.L$0 = yi7;
        this.L$1 = date3;
        this.L$2 = date2;
        this.L$3 = fitnessDatabase2;
        this.label = 2;
        Object a3 = vh7.a(b2, sleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2, this);
        if (a3 == a) {
            return a;
        }
        fitnessDatabase = fitnessDatabase2;
        obj = a3;
        date = date3;
        return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$start, this.$end), new Anon1_Level2(this, date, date2, (SleepDatabase) obj));
    }
}
