package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getPendingSleepSessions$2", f = "SleepSessionsRepository.kt", l = {252}, m = "invokeSuspend")
public final class SleepSessionsRepository$getPendingSleepSessions$Anon2 extends zb7 implements kd7<yi7, fb7<? super List<MFSleepSession>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$getPendingSleepSessions$Anon2(Date date, Date date2, fb7 fb7) {
        super(2, fb7);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$getPendingSleepSessions$Anon2 sleepSessionsRepository$getPendingSleepSessions$Anon2 = new SleepSessionsRepository$getPendingSleepSessions$Anon2(this.$startDate, this.$endDate, fb7);
        sleepSessionsRepository$getPendingSleepSessions$Anon2.p$ = (yi7) obj;
        return sleepSessionsRepository$getPendingSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super List<MFSleepSession>> fb7) {
        return ((SleepSessionsRepository$getPendingSleepSessions$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.d(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SleepDao sleepDao = ((SleepDatabase) obj).sleepDao();
        Date q = zd5.q(this.$startDate);
        ee7.a((Object) q, "DateHelper.getStartOfDay(startDate)");
        Date l = zd5.l(this.$endDate);
        ee7.a((Object) l, "DateHelper.getEndOfDay(endDate)");
        return sleepDao.getPendingSleepSessions(q, l);
    }
}
