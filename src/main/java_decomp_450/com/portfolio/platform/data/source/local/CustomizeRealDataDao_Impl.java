package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.CustomizeRealData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDao_Impl implements CustomizeRealDataDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<CustomizeRealData> __insertionAdapterOfCustomizeRealData;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<CustomizeRealData> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `customizeRealData` (`id`,`value`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, CustomizeRealData customizeRealData) {
            if (customizeRealData.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, customizeRealData.getId());
            }
            if (customizeRealData.getValue() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, customizeRealData.getValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM customizeRealData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<CustomizeRealData> call() throws Exception {
            Cursor a = pi.a(CustomizeRealDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "value");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new CustomizeRealData(a.getString(b), a.getString(b2)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public CustomizeRealDataDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfCustomizeRealData = new Anon1(ciVar);
        this.__preparedStmtOfCleanUp = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"customizeRealData"}, false, (Callable) new Anon3(fi.b("SELECT * FROM customizeRealData", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public List<CustomizeRealData> getAllRealDataRaw() {
        fi b = fi.b("SELECT * FROM customizeRealData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "value");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new CustomizeRealData(a.getString(b2), a.getString(b3)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public CustomizeRealData getRealData(String str) {
        fi b = fi.b("SELECT * FROM customizeRealData WHERE id=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        CustomizeRealData customizeRealData = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "value");
            if (a.moveToFirst()) {
                customizeRealData = new CustomizeRealData(a.getString(b2), a.getString(b3));
            }
            return customizeRealData;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void upsertRealData(CustomizeRealData customizeRealData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCustomizeRealData.insert(customizeRealData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
