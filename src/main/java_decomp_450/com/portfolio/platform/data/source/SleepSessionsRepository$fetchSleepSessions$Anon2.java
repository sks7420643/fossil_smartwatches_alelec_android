package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import java.util.Date;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$2", f = "SleepSessionsRepository.kt", l = {DateTimeConstants.HOURS_PER_WEEK, 179, 182}, m = "invokeSuspend")
public final class SleepSessionsRepository$fetchSleepSessions$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ie4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$fetchSleepSessions$Anon2(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$fetchSleepSessions$Anon2 sleepSessionsRepository$fetchSleepSessions$Anon2 = new SleepSessionsRepository$fetchSleepSessions$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, fb7);
        sleepSessionsRepository$fetchSleepSessions$Anon2.p$ = (yi7) obj;
        return sleepSessionsRepository$fetchSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ie4>> fb7) {
        return ((SleepSessionsRepository$fetchSleepSessions$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0126 A[Catch:{ Exception -> 0x0164 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x012b A[Catch:{ Exception -> 0x0164 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012e A[Catch:{ Exception -> 0x0164 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r14.label
            r2 = 3
            r3 = 2
            r4 = 1
            r5 = 0
            if (r1 == 0) goto L_0x0054
            if (r1 == r4) goto L_0x004b
            if (r1 == r3) goto L_0x0032
            if (r1 != r2) goto L_0x002a
            java.lang.Object r0 = r14.L$3
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            java.lang.Object r0 = r14.L$2
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.lang.Object r0 = r14.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r14.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x0027 }
            goto L_0x015c
        L_0x0027:
            r15 = move-exception
            goto L_0x0168
        L_0x002a:
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r15.<init>(r0)
            throw r15
        L_0x0032:
            java.lang.Object r1 = r14.L$3
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.lang.Object r3 = r14.L$2
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            java.lang.Object r4 = r14.L$1
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r6 = r14.L$0
            com.fossil.yi7 r6 = (com.fossil.yi7) r6
            com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x0047 }
            goto L_0x0121
        L_0x0047:
            r15 = move-exception
            r0 = r4
            goto L_0x0168
        L_0x004b:
            java.lang.Object r1 = r14.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r15)
            r6 = r1
            goto L_0x0095
        L_0x0054:
            com.fossil.t87.a(r15)
            com.fossil.yi7 r15 = r14.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fetchSleepSessions: start = "
            r7.append(r8)
            java.util.Date r8 = r14.$start
            r7.append(r8)
            java.lang.String r8 = ", end = "
            r7.append(r8)
            java.util.Date r8 = r14.$end
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r1.d(r6, r7)
            com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2 r1 = new com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2$repoResponse$Anon1_Level2
            r1.<init>(r14, r5)
            r14.L$0 = r15
            r14.label = r4
            java.lang.Object r1 = com.fossil.aj5.a(r1, r14)
            if (r1 != r0) goto L_0x0093
            return r0
        L_0x0093:
            r6 = r15
            r15 = r1
        L_0x0095:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            boolean r1 = r15 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x018d
            r1 = r15
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x018c
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.fossil.be4 r4 = new com.fossil.be4     // Catch:{ Exception -> 0x0164 }
            r4.<init>()     // Catch:{ Exception -> 0x0164 }
            java.lang.Class<org.joda.time.DateTime> r7 = org.joda.time.DateTime.class
            com.portfolio.platform.helper.GsonConvertDateTime r8 = new com.portfolio.platform.helper.GsonConvertDateTime     // Catch:{ Exception -> 0x0164 }
            r8.<init>()     // Catch:{ Exception -> 0x0164 }
            r4.a(r7, r8)     // Catch:{ Exception -> 0x0164 }
            java.lang.Class<java.util.Date> r7 = java.util.Date.class
            com.portfolio.platform.helper.GsonConverterShortDate r8 = new com.portfolio.platform.helper.GsonConverterShortDate     // Catch:{ Exception -> 0x0164 }
            r8.<init>()     // Catch:{ Exception -> 0x0164 }
            r4.a(r7, r8)     // Catch:{ Exception -> 0x0164 }
            com.google.gson.Gson r4 = r4.a()     // Catch:{ Exception -> 0x0164 }
            r7 = r15
            com.fossil.bj5 r7 = (com.fossil.bj5) r7     // Catch:{ Exception -> 0x0164 }
            java.lang.Object r7 = r7.a()     // Catch:{ Exception -> 0x0164 }
            com.fossil.ie4 r7 = (com.fossil.ie4) r7     // Catch:{ Exception -> 0x0164 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2$responseDate$Anon1_Level2 r8 = new com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2$responseDate$Anon1_Level2     // Catch:{ Exception -> 0x0164 }
            r8.<init>()     // Catch:{ Exception -> 0x0164 }
            java.lang.reflect.Type r8 = r8.getType()     // Catch:{ Exception -> 0x0164 }
            java.lang.Object r4 = r4.a(r7, r8)     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.source.remote.ApiResponse r4 = (com.portfolio.platform.data.source.remote.ApiResponse) r4     // Catch:{ Exception -> 0x0164 }
            if (r4 == 0) goto L_0x0102
            java.util.List r7 = r4.get_items()     // Catch:{ Exception -> 0x0164 }
            if (r7 == 0) goto L_0x0102
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x0164 }
        L_0x00ee:
            boolean r8 = r7.hasNext()     // Catch:{ Exception -> 0x0164 }
            if (r8 == 0) goto L_0x0102
            java.lang.Object r8 = r7.next()     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.response.sleep.SleepSessionParse r8 = (com.portfolio.platform.response.sleep.SleepSessionParse) r8     // Catch:{ Exception -> 0x0164 }
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r8 = r8.getMfSleepSessionBySleepSessionParse()     // Catch:{ Exception -> 0x0164 }
            r1.add(r8)     // Catch:{ Exception -> 0x0164 }
            goto L_0x00ee
        L_0x0102:
            r7 = r15
            com.fossil.bj5 r7 = (com.fossil.bj5) r7     // Catch:{ Exception -> 0x0164 }
            boolean r7 = r7.b()     // Catch:{ Exception -> 0x0164 }
            if (r7 != 0) goto L_0x0124
            com.portfolio.platform.data.source.SleepSessionsRepository r7 = r14.this$0     // Catch:{ Exception -> 0x0164 }
            r14.L$0 = r6     // Catch:{ Exception -> 0x0164 }
            r14.L$1 = r15     // Catch:{ Exception -> 0x0164 }
            r14.L$2 = r1     // Catch:{ Exception -> 0x0164 }
            r14.L$3 = r4     // Catch:{ Exception -> 0x0164 }
            r14.label = r3     // Catch:{ Exception -> 0x0164 }
            java.lang.Object r3 = r7.insert(r1, r14)     // Catch:{ Exception -> 0x0164 }
            if (r3 != r0) goto L_0x011e
            return r0
        L_0x011e:
            r3 = r1
            r1 = r4
            r4 = r15
        L_0x0121:
            r15 = r4
            r4 = r1
            r1 = r3
        L_0x0124:
            if (r4 == 0) goto L_0x012b
            com.portfolio.platform.data.model.Range r3 = r4.get_range()     // Catch:{ Exception -> 0x0164 }
            goto L_0x012c
        L_0x012b:
            r3 = r5
        L_0x012c:
            if (r3 == 0) goto L_0x0163
            com.portfolio.platform.data.model.Range r3 = r4.get_range()     // Catch:{ Exception -> 0x0164 }
            if (r3 == 0) goto L_0x015f
            boolean r3 = r3.isHasNext()     // Catch:{ Exception -> 0x0164 }
            if (r3 == 0) goto L_0x0163
            com.portfolio.platform.data.source.SleepSessionsRepository r7 = r14.this$0     // Catch:{ Exception -> 0x0164 }
            java.util.Date r8 = r14.$start     // Catch:{ Exception -> 0x0164 }
            java.util.Date r9 = r14.$end     // Catch:{ Exception -> 0x0164 }
            int r3 = r14.$offset     // Catch:{ Exception -> 0x0164 }
            int r5 = r14.$limit     // Catch:{ Exception -> 0x0164 }
            int r10 = r3 + r5
            int r11 = r14.$limit     // Catch:{ Exception -> 0x0164 }
            r14.L$0 = r6     // Catch:{ Exception -> 0x0164 }
            r14.L$1 = r15     // Catch:{ Exception -> 0x0164 }
            r14.L$2 = r1     // Catch:{ Exception -> 0x0164 }
            r14.L$3 = r4     // Catch:{ Exception -> 0x0164 }
            r14.label = r2     // Catch:{ Exception -> 0x0164 }
            r12 = r14
            java.lang.Object r1 = r7.fetchSleepSessions(r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x0164 }
            if (r1 != r0) goto L_0x015a
            return r0
        L_0x015a:
            r0 = r15
            r15 = r1
        L_0x015c:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            goto L_0x0163
        L_0x015f:
            com.fossil.ee7.a()
            throw r5
        L_0x0163:
            return r15
        L_0x0164:
            r0 = move-exception
            r13 = r0
            r0 = r15
            r15 = r13
        L_0x0168:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchSleepSessions exception="
            r3.append(r4)
            r15.printStackTrace()
            com.fossil.i97 r15 = com.fossil.i97.a
            r3.append(r15)
            java.lang.String r15 = r3.toString()
            r1.d(r2, r15)
            return r0
        L_0x018c:
            return r15
        L_0x018d:
            boolean r0 = r15 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01db
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchSleepSessions Failure code="
            r2.append(r3)
            r3 = r15
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r4 = r3.a()
            r2.append(r4)
            java.lang.String r4 = " message="
            r2.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r3.c()
            if (r4 == 0) goto L_0x01c2
            java.lang.String r4 = r4.getMessage()
            if (r4 == 0) goto L_0x01c2
            r5 = r4
            goto L_0x01cc
        L_0x01c2:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x01cc
            java.lang.String r5 = r3.getUserMessage()
        L_0x01cc:
            if (r5 == 0) goto L_0x01cf
            goto L_0x01d1
        L_0x01cf:
            java.lang.String r5 = ""
        L_0x01d1:
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x01db:
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
