package com.portfolio.platform.data.source.local.workoutsetting;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.mv4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDao_Impl implements WorkoutSettingDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WorkoutSetting> __insertionAdapterOfWorkoutSetting;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ mv4 __workoutTypeConverter; // = new mv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WorkoutSetting> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workoutSetting` (`type`,`mode`,`enable`,`askMeFirst`,`startLatency`,`pauseLatency`,`resumeLatency`,`stopLatency`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WorkoutSetting workoutSetting) {
            String a = WorkoutSettingDao_Impl.this.__workoutTypeConverter.a(workoutSetting.getType());
            if (a == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, a);
            }
            String a2 = WorkoutSettingDao_Impl.this.__workoutTypeConverter.a(workoutSetting.getMode());
            if (a2 == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a2);
            }
            ajVar.bindLong(3, workoutSetting.getEnable() ? 1 : 0);
            ajVar.bindLong(4, workoutSetting.getAskMeFirst() ? 1 : 0);
            ajVar.bindLong(5, (long) workoutSetting.getStartLatency());
            ajVar.bindLong(6, (long) workoutSetting.getPauseLatency());
            ajVar.bindLong(7, (long) workoutSetting.getResumeLatency());
            ajVar.bindLong(8, (long) workoutSetting.getStopLatency());
            ajVar.bindLong(9, workoutSetting.getCreatedAt());
            ajVar.bindLong(10, workoutSetting.getUpdatedAt());
            ajVar.bindLong(11, (long) workoutSetting.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM workoutSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WorkoutSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSetting> call() throws Exception {
            Cursor a = pi.a(WorkoutSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "type");
                int b2 = oi.b(a, "mode");
                int b3 = oi.b(a, "enable");
                int b4 = oi.b(a, "askMeFirst");
                int b5 = oi.b(a, "startLatency");
                int b6 = oi.b(a, "pauseLatency");
                int b7 = oi.b(a, "resumeLatency");
                int b8 = oi.b(a, "stopLatency");
                int b9 = oi.b(a, "createdAt");
                int b10 = oi.b(a, "updatedAt");
                int b11 = oi.b(a, "pinType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WorkoutSetting(WorkoutSettingDao_Impl.this.__workoutTypeConverter.l(a.getString(b)), WorkoutSettingDao_Impl.this.__workoutTypeConverter.f(a.getString(b2)), a.getInt(b3) != 0, a.getInt(b4) != 0, a.getInt(b5), a.getInt(b6), a.getInt(b7), a.getInt(b8), a.getLong(b9), a.getLong(b10), a.getInt(b11)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WorkoutSettingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWorkoutSetting = new Anon1(ciVar);
        this.__preparedStmtOfCleanUp = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getPendingWorkoutSettings() {
        fi b = fi.b("SELECT * FROM workoutSetting where pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "type");
            int b3 = oi.b(a, "mode");
            int b4 = oi.b(a, "enable");
            int b5 = oi.b(a, "askMeFirst");
            int b6 = oi.b(a, "startLatency");
            int b7 = oi.b(a, "pauseLatency");
            int b8 = oi.b(a, "resumeLatency");
            int b9 = oi.b(a, "stopLatency");
            int b10 = oi.b(a, "createdAt");
            int b11 = oi.b(a, "updatedAt");
            int b12 = oi.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.l(a.getString(b2)), this.__workoutTypeConverter.f(a.getString(b3)), a.getInt(b4) != 0, a.getInt(b5) != 0, a.getInt(b6), a.getInt(b7), a.getInt(b8), a.getInt(b9), a.getLong(b10), a.getLong(b11), a.getInt(b12)));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getWorkoutSettingList() {
        fi b = fi.b("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "type");
            int b3 = oi.b(a, "mode");
            int b4 = oi.b(a, "enable");
            int b5 = oi.b(a, "askMeFirst");
            int b6 = oi.b(a, "startLatency");
            int b7 = oi.b(a, "pauseLatency");
            int b8 = oi.b(a, "resumeLatency");
            int b9 = oi.b(a, "stopLatency");
            int b10 = oi.b(a, "createdAt");
            int b11 = oi.b(a, "updatedAt");
            int b12 = oi.b(a, "pinType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.l(a.getString(b2)), this.__workoutTypeConverter.f(a.getString(b3)), a.getInt(b4) != 0, a.getInt(b5) != 0, a.getInt(b6), a.getInt(b7), a.getInt(b8), a.getInt(b9), a.getLong(b10), a.getLong(b11), a.getInt(b12)));
                b2 = b2;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public LiveData<List<WorkoutSetting>> getWorkoutSettingListAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"workoutSetting"}, false, (Callable) new Anon3(fi.b("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSetting(WorkoutSetting workoutSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert(workoutSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSettingList(List<WorkoutSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
