package com.portfolio.platform.data.source;

import com.fossil.ci;
import com.fossil.li;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceDatabase extends ci {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_1_TO_2; // = new DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ li MIGRATION_FROM_2_TO_3; // = new DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1(2, 3);
    @DexIgnore
    public static /* final */ String TAG; // = "DeviceDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final li getMIGRATION_FROM_1_TO_2() {
            return DeviceDatabase.MIGRATION_FROM_1_TO_2;
        }

        @DexIgnore
        public final li getMIGRATION_FROM_2_TO_3() {
            return DeviceDatabase.MIGRATION_FROM_2_TO_3;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public abstract DeviceDao deviceDao();

    @DexIgnore
    public abstract SkuDao skuDao();
}
