package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.iv4;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.si;
import com.fossil.vh;
import com.fossil.vt7;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDao_Impl implements WatchAppDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<WatchApp> __insertionAdapterOfWatchApp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ iv4 __stringArrayConverter; // = new iv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<WatchApp> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchApp` (`watchappId`,`name`,`nameKey`,`description`,`descriptionKey`,`categories`,`icon`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WatchApp watchApp) {
            if (watchApp.getWatchappId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, watchApp.getWatchappId());
            }
            if (watchApp.getName() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, watchApp.getName());
            }
            if (watchApp.getNameKey() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, watchApp.getNameKey());
            }
            if (watchApp.getDescription() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, watchApp.getDescription());
            }
            if (watchApp.getDescriptionKey() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, watchApp.getDescriptionKey());
            }
            String a = WatchAppDao_Impl.this.__stringArrayConverter.a(watchApp.getCategories());
            if (a == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a);
            }
            if (watchApp.getIcon() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, watchApp.getIcon());
            }
            if (watchApp.getUpdatedAt() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, watchApp.getUpdatedAt());
            }
            if (watchApp.getCreatedAt() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, watchApp.getCreatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchApp";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchApp> call() throws Exception {
            Cursor a = pi.a(WatchAppDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "watchappId");
                int b2 = oi.b(a, "name");
                int b3 = oi.b(a, "nameKey");
                int b4 = oi.b(a, "description");
                int b5 = oi.b(a, "descriptionKey");
                int b6 = oi.b(a, "categories");
                int b7 = oi.b(a, "icon");
                int b8 = oi.b(a, "updatedAt");
                int b9 = oi.b(a, "createdAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchApp(a.getString(b), a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), WatchAppDao_Impl.this.__stringArrayConverter.a(a.getString(b6)), a.getString(b7), a.getString(b8), a.getString(b9)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfWatchApp = new Anon1(ciVar);
        this.__preparedStmtOfClearAll = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> getAllWatchApp() {
        fi b = fi.b("SELECT * FROM watchApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "watchappId");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "descriptionKey");
            int b7 = oi.b(a, "categories");
            int b8 = oi.b(a, "icon");
            int b9 = oi.b(a, "updatedAt");
            int b10 = oi.b(a, "createdAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public LiveData<List<WatchApp>> getAllWatchAppAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchApp"}, false, (Callable) new Anon3(fi.b("SELECT * FROM watchApp", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public WatchApp getWatchAppById(String str) {
        fi b = fi.b("SELECT * FROM watchApp WHERE watchappId=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchApp watchApp = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "watchappId");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "descriptionKey");
            int b7 = oi.b(a, "categories");
            int b8 = oi.b(a, "icon");
            int b9 = oi.b(a, "updatedAt");
            int b10 = oi.b(a, "createdAt");
            if (a.moveToFirst()) {
                watchApp = new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10));
            }
            return watchApp;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> getWatchAppByIds(List<String> list) {
        StringBuilder a = si.a();
        a.append("SELECT ");
        a.append(vt7.ANY_MARKER);
        a.append(" FROM watchApp WHERE watchappId IN (");
        int size = list.size();
        si.a(a, size);
        a.append(")");
        fi b = fi.b(a.toString(), size + 0);
        int i = 1;
        for (String str : list) {
            if (str == null) {
                b.bindNull(i);
            } else {
                b.bindString(i, str);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a2, "watchappId");
            int b3 = oi.b(a2, "name");
            int b4 = oi.b(a2, "nameKey");
            int b5 = oi.b(a2, "description");
            int b6 = oi.b(a2, "descriptionKey");
            int b7 = oi.b(a2, "categories");
            int b8 = oi.b(a2, "icon");
            int b9 = oi.b(a2, "updatedAt");
            int b10 = oi.b(a2, "createdAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(new WatchApp(a2.getString(b2), a2.getString(b3), a2.getString(b4), a2.getString(b5), a2.getString(b6), this.__stringArrayConverter.a(a2.getString(b7)), a2.getString(b8), a2.getString(b9), a2.getString(b10)));
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public List<WatchApp> queryWatchAppByName(String str) {
        fi b = fi.b("SELECT * FROM watchApp WHERE name LIKE '%' || ? || '%'", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "watchappId");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "nameKey");
            int b5 = oi.b(a, "description");
            int b6 = oi.b(a, "descriptionKey");
            int b7 = oi.b(a, "categories");
            int b8 = oi.b(a, "icon");
            int b9 = oi.b(a, "updatedAt");
            int b10 = oi.b(a, "createdAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchApp(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), this.__stringArrayConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDao
    public void upsertWatchAppList(List<WatchApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
