package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.hu4;
import com.fossil.ji;
import com.fossil.lu4;
import com.fossil.mu4;
import com.fossil.oi;
import com.fossil.ou4;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySampleDao_Impl extends ActivitySampleDao {
    @DexIgnore
    public /* final */ hu4 __activityIntensitiesConverter; // = new hu4();
    @DexIgnore
    public /* final */ lu4 __dateLongStringConverter; // = new lu4();
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ ou4 __dateTimeISOStringConverter; // = new ou4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<ActivitySample> __insertionAdapterOfActivitySample;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<ActivitySample> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_sample` (`id`,`uid`,`date`,`startTime`,`endTime`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneOffsetInSecond`,`sourceId`,`syncTime`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ActivitySample activitySample) {
            if (activitySample.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, activitySample.getId());
            }
            if (activitySample.getUid() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, activitySample.getUid());
            }
            String a = ActivitySampleDao_Impl.this.__dateShortStringConverter.a(activitySample.getDate());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            String a2 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getStartTime());
            if (a2 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a2);
            }
            String a3 = ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(activitySample.getEndTime());
            if (a3 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a3);
            }
            ajVar.bindDouble(6, activitySample.getSteps());
            ajVar.bindDouble(7, activitySample.getCalories());
            ajVar.bindDouble(8, activitySample.getDistance());
            ajVar.bindLong(9, (long) activitySample.getActiveTime());
            String a4 = ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(activitySample.getIntensityDistInSteps());
            if (a4 == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, a4);
            }
            ajVar.bindLong(11, (long) activitySample.getTimeZoneOffsetInSecond());
            if (activitySample.getSourceId() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, activitySample.getSourceId());
            }
            ajVar.bindLong(13, activitySample.getSyncTime());
            ajVar.bindLong(14, activitySample.getCreatedAt());
            ajVar.bindLong(15, activitySample.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM activity_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<ActivitySample>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ActivitySample> call() throws Exception {
            Anon3 anon3 = this;
            Cursor a = pi.a(ActivitySampleDao_Impl.this.__db, anon3.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "uid");
                int b3 = oi.b(a, "date");
                int b4 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b5 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b6 = oi.b(a, "steps");
                int b7 = oi.b(a, "calories");
                int b8 = oi.b(a, "distance");
                int b9 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b10 = oi.b(a, "intensityDistInSteps");
                int b11 = oi.b(a, "timeZoneOffsetInSecond");
                int b12 = oi.b(a, SampleRaw.COLUMN_SOURCE_ID);
                int b13 = oi.b(a, "syncTime");
                int b14 = oi.b(a, "createdAt");
                int i = b;
                int b15 = oi.b(a, "updatedAt");
                int i2 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    ActivitySample activitySample = new ActivitySample(a.getString(b2), ActivitySampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b5)), a.getDouble(b6), a.getDouble(b7), a.getDouble(b8), a.getInt(b9), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b10)), a.getInt(b11), a.getString(b12), a.getLong(b13), a.getLong(i2), a.getLong(b15));
                    i2 = i2;
                    activitySample.setId(a.getString(i));
                    arrayList.add(activitySample);
                    anon3 = this;
                    b15 = b15;
                    b3 = b3;
                    i = i;
                    b2 = b2;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<com.portfolio.platform.data.model.room.fitness.SampleRaw> call() throws Exception {
            Anon4 anon4 = this;
            Cursor a = pi.a(ActivitySampleDao_Impl.this.__db, anon4.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "pinType");
                int b3 = oi.b(a, SampleRaw.COLUMN_UA_PIN_TYPE);
                int b4 = oi.b(a, SampleRaw.COLUMN_START_TIME);
                int b5 = oi.b(a, SampleRaw.COLUMN_END_TIME);
                int b6 = oi.b(a, SampleRaw.COLUMN_SOURCE_ID);
                int b7 = oi.b(a, SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
                int b8 = oi.b(a, SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
                int b9 = oi.b(a, "steps");
                int b10 = oi.b(a, "calories");
                int b11 = oi.b(a, "distance");
                int b12 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b13 = oi.b(a, "intensityDistInSteps");
                int b14 = oi.b(a, SampleRaw.COLUMN_TIMEZONE_ID);
                int i = b3;
                int i2 = b2;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    com.portfolio.platform.data.model.room.fitness.SampleRaw sampleRaw = new com.portfolio.platform.data.model.room.fitness.SampleRaw(ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b4)), ActivitySampleDao_Impl.this.__dateLongStringConverter.a(a.getString(b5)), a.getString(b6), a.getString(b7), a.getString(b8), a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), a.getInt(b12), ActivitySampleDao_Impl.this.__activityIntensitiesConverter.a(a.getString(b13)), a.getString(b14));
                    sampleRaw.setId(a.getString(b));
                    sampleRaw.setPinType(a.getInt(i2));
                    sampleRaw.setUaPinType(a.getInt(i));
                    arrayList.add(sampleRaw);
                    anon4 = this;
                    i = i;
                    b = b;
                    i2 = i2;
                    b4 = b4;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySampleDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfActivitySample = new Anon1(ciVar);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public ActivitySample getActivitySample(String str) {
        fi fiVar;
        ActivitySample activitySample;
        fi b = fi.b("SELECT * FROM activity_sample WHERE id = ? LIMIT 1", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uid");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b6 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            int b7 = oi.b(a, "steps");
            int b8 = oi.b(a, "calories");
            int b9 = oi.b(a, "distance");
            int b10 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            int b11 = oi.b(a, "intensityDistInSteps");
            int b12 = oi.b(a, "timeZoneOffsetInSecond");
            int b13 = oi.b(a, SampleRaw.COLUMN_SOURCE_ID);
            int b14 = oi.b(a, "syncTime");
            fiVar = b;
            try {
                int b15 = oi.b(a, "createdAt");
                int b16 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activitySample = new ActivitySample(a.getString(b3), this.__dateShortStringConverter.a(a.getString(b4)), this.__dateTimeISOStringConverter.a(a.getString(b5)), this.__dateTimeISOStringConverter.a(a.getString(b6)), a.getDouble(b7), a.getDouble(b8), a.getDouble(b9), a.getInt(b10), this.__activityIntensitiesConverter.a(a.getString(b11)), a.getInt(b12), a.getString(b13), a.getLong(b14), a.getLong(b15), a.getLong(b16));
                    activitySample.setId(a.getString(b2));
                } else {
                    activitySample = null;
                }
                a.close();
                fiVar.c();
                return activitySample;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public LiveData<List<com.portfolio.platform.data.model.room.fitness.SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? ORDER BY startTime ASC", 2);
        String a = this.__dateLongStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateLongStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{SampleRaw.TABLE_NAME}, false, (Callable) new Anon4(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2) {
        fi b = fi.b("SELECT * FROM activity_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{ActivitySample.TABLE_NAME}, false, (Callable) new Anon3(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySampleDao
    public void upsertListActivitySample(List<ActivitySample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
