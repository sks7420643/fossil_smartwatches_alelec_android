package com.portfolio.platform.data.source.remote;

import com.fossil.aj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRemoteDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "DeviceRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object forceLinkDevice(Device device, fb7<? super zi5<Void>> fb7) {
        return aj5.a(new DeviceRemoteDataSource$forceLinkDevice$Anon2(this, device, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object generatePairingKey(java.lang.String r14, com.fossil.fb7<? super com.fossil.zi5<java.lang.String>> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$Anon1
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r14 = r0.L$2
            com.fossil.ie4 r14 = (com.fossil.ie4) r14
            java.lang.Object r14 = r0.L$1
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r0
            com.fossil.t87.a(r15)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x003e:
            com.fossil.t87.a(r15)
            com.fossil.ie4 r15 = new com.fossil.ie4
            r15.<init>()
            java.lang.String r2 = "serialNumber"
            r15.a(r2, r14)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$generatePairingKey$response$Anon1
            r2.<init>(r13, r15, r4)
            r0.L$0 = r13
            r0.L$1 = r14
            r0.L$2 = r15
            r0.label = r3
            java.lang.Object r15 = com.fossil.aj5.a(r2, r0)
            if (r15 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "generatePairingKey of "
            r2.append(r3)
            r2.append(r14)
            java.lang.String r14 = " response "
            r2.append(r14)
            r2.append(r15)
            java.lang.String r14 = r2.toString()
            r0.d(r1, r14)
            boolean r14 = r15 instanceof com.fossil.bj5
            if (r14 == 0) goto L_0x00ca
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r14 = r15.a()
            if (r14 == 0) goto L_0x00c6
            com.fossil.ie4 r14 = (com.fossil.ie4) r14
            java.lang.String r0 = "randomKey"
            boolean r14 = r14.d(r0)
            if (r14 == 0) goto L_0x00b6
            com.fossil.bj5 r14 = new com.fossil.bj5
            java.lang.Object r15 = r15.a()
            com.fossil.ie4 r15 = (com.fossil.ie4) r15
            com.google.gson.JsonElement r15 = r15.a(r0)
            java.lang.String r0 = "response.response.get(Co\u2026ants.JSON_KEY_RANDOM_KEY)"
            com.fossil.ee7.a(r15, r0)
            java.lang.String r15 = r15.f()
            r0 = 0
            r1 = 2
            r14.<init>(r15, r0, r1, r4)
            goto L_0x00ea
        L_0x00b6:
            com.fossil.yi5 r14 = new com.fossil.yi5
            r6 = 600(0x258, float:8.41E-43)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 16
            r12 = 0
            r5 = r14
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            goto L_0x00ea
        L_0x00c6:
            com.fossil.ee7.a()
            throw r4
        L_0x00ca:
            boolean r14 = r15 instanceof com.fossil.yi5
            if (r14 == 0) goto L_0x00eb
            com.fossil.yi5 r14 = new com.fossil.yi5
            com.fossil.yi5 r15 = (com.fossil.yi5) r15
            int r1 = r15.a()
            com.portfolio.platform.data.model.ServerError r2 = r15.c()
            java.lang.Throwable r3 = r15.d()
            java.lang.String r4 = r15.b()
            r5 = 0
            r6 = 16
            r7 = 0
            r0 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00ea:
            return r14
        L_0x00eb:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.generatePairingKey(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getAllDevice(fb7<? super zi5<ApiResponse<Device>>> fb7) {
        FLogger.INSTANCE.getLocal().d(TAG, "getAllDevice");
        return aj5.a(new DeviceRemoteDataSource$getAllDevice$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getDeviceBySerial(String str, fb7<? super zi5<Device>> fb7) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDeviceBySerial");
        return aj5.a(new DeviceRemoteDataSource$getDeviceBySerial$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getDeviceSecretKey(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.String>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r0
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1
            r10.<init>(r8, r9, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDeviceSecretKey of "
            r2.append(r3)
            r2.append(r9)
            java.lang.String r9 = " response "
            r2.append(r9)
            r2.append(r10)
            java.lang.String r9 = r2.toString()
            r0.d(r1, r9)
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00c5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r9 = r10.a()
            if (r9 == 0) goto L_0x00c1
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.String r0 = "secretKey"
            boolean r9 = r9.d(r0)
            if (r9 == 0) goto L_0x00b1
            java.lang.Object r9 = r10.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            com.google.gson.JsonElement r9 = r9.a(r0)
            boolean r10 = r9 instanceof com.fossil.he4
            r1 = 2
            r2 = 0
            if (r10 == 0) goto L_0x00a3
            com.fossil.bj5 r9 = new com.fossil.bj5
            java.lang.String r10 = ""
            r9.<init>(r10, r2, r1, r4)
            goto L_0x00e5
        L_0x00a3:
            com.fossil.bj5 r10 = new com.fossil.bj5
            com.fossil.ee7.a(r9, r0)
            java.lang.String r9 = r9.f()
            r10.<init>(r9, r2, r1, r4)
            r9 = r10
            goto L_0x00e5
        L_0x00b1:
            com.fossil.yi5 r9 = new com.fossil.yi5
            r1 = 600(0x258, float:8.41E-43)
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 16
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x00e5
        L_0x00c1:
            com.fossil.ee7.a()
            throw r4
        L_0x00c5:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00e6
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            java.lang.String r4 = r10.b()
            r5 = 0
            r6 = 16
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00e5:
            return r9
        L_0x00e6:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getDeviceSecretKey(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLatestWatchParamFromServer(java.lang.String r7, int r8, com.fossil.fb7<? super com.portfolio.platform.data.model.WatchParameterResponse> r9) {
        /*
            r6 = this;
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1
            r0.<init>(r6, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            int r8 = r0.I$0
            java.lang.Object r7 = r0.L$1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r0
            com.fossil.t87.a(r9)
            goto L_0x0053
        L_0x0034:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003c:
            com.fossil.t87.a(r9)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 r9 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1
            r9.<init>(r6, r7, r8, r4)
            r0.L$0 = r6
            r0.L$1 = r7
            r0.I$0 = r8
            r0.label = r3
            java.lang.Object r9 = com.fossil.aj5.a(r9, r0)
            if (r9 != r1) goto L_0x0053
            return r1
        L_0x0053:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "getLatestWatchParamFromServer, serial ="
            r2.append(r5)
            r2.append(r7)
            java.lang.String r7 = ", majorVersion= "
            r2.append(r7)
            r2.append(r8)
            java.lang.String r7 = r2.toString()
            r0.d(r1, r7)
            boolean r7 = r9 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x00c1
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r7 = r9.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            if (r7 == 0) goto L_0x008c
            java.util.List r7 = r7.get_items()
            goto L_0x008d
        L_0x008c:
            r7 = r4
        L_0x008d:
            if (r7 == 0) goto L_0x00c0
            boolean r8 = r7.isEmpty()
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x00c0
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "getLatestWatchParamFromServer success, response = "
            r0.append(r1)
            r1 = 0
            java.lang.Object r2 = r7.get(r1)
            com.portfolio.platform.data.model.WatchParameterResponse r2 = (com.portfolio.platform.data.model.WatchParameterResponse) r2
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r8.d(r9, r0)
            java.lang.Object r7 = r7.get(r1)
            r4 = r7
            com.portfolio.platform.data.model.WatchParameterResponse r4 = (com.portfolio.platform.data.model.WatchParameterResponse) r4
        L_0x00c0:
            return r4
        L_0x00c1:
            boolean r7 = r9 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x00e8
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "getLatestWatchParamFromServer failed, errorCode = "
            r0.append(r1)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r9 = r9.a()
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            r7.d(r8, r9)
            return r4
        L_0x00e8:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getLatestWatchParamFromServer(java.lang.String, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSupportedSku(int r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.SKUModel>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0038
            if (r2 != r3) goto L_0x0030
            int r9 = r0.I$0
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x005a
        L_0x0030:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0038:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.String r5 = "fetchSupportedSkus"
            r10.d(r2, r5)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getSupportedSku$response$Anon1
            r10.<init>(r8, r9, r4)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x005a
            return r1
        L_0x005a:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0074
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            if (r10 == 0) goto L_0x0070
            r0 = 0
            r1 = 2
            r9.<init>(r10, r0, r1, r4)
            goto L_0x008e
        L_0x0070:
            com.fossil.ee7.a()
            throw r4
        L_0x0074:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x008f
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 16
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x008e:
            return r9
        L_0x008f:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.getSupportedSku(int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object removeDevice(Device device, fb7<? super zi5<Void>> fb7) {
        FLogger.INSTANCE.getLocal().d(TAG, "removeDevice");
        return aj5.a(new DeviceRemoteDataSource$removeDevice$Anon2(this, device, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object swapPairingKey(java.lang.String r20, java.lang.String r21, com.fossil.fb7<? super com.fossil.zi5<java.lang.String>> r22) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            boolean r4 = r3 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$Anon1
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$Anon1 r4 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$Anon1) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$Anon1 r4 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$Anon1
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            r7 = 1
            r8 = 0
            java.lang.String r9 = "encryptedData"
            if (r6 == 0) goto L_0x0051
            if (r6 != r7) goto L_0x0049
            java.lang.Object r1 = r4.L$3
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r4.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r4 = r4.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r4 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r4
            com.fossil.t87.a(r3)
            r18 = r2
            r2 = r1
            r1 = r18
            goto L_0x0077
        L_0x0049:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0051:
            com.fossil.t87.a(r3)
            com.fossil.ie4 r3 = new com.fossil.ie4
            r3.<init>()
            java.lang.String r6 = "serialNumber"
            r3.a(r6, r2)
            r3.a(r9, r1)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$response$Anon1 r6 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$swapPairingKey$response$Anon1
            r6.<init>(r0, r3, r8)
            r4.L$0 = r0
            r4.L$1 = r1
            r4.L$2 = r2
            r4.L$3 = r3
            r4.label = r7
            java.lang.Object r3 = com.fossil.aj5.a(r6, r4)
            if (r3 != r5) goto L_0x0077
            return r5
        L_0x0077:
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "swapPairingKey "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r1 = " of "
            r6.append(r1)
            r6.append(r2)
            java.lang.String r1 = " response "
            r6.append(r1)
            r6.append(r3)
            java.lang.String r1 = r6.toString()
            r4.d(r5, r1)
            boolean r1 = r3 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00fd
            com.fossil.bj5 r3 = (com.fossil.bj5) r3
            java.lang.Object r1 = r3.a()
            if (r1 == 0) goto L_0x00f9
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            boolean r1 = r1.d(r9)
            if (r1 == 0) goto L_0x00e8
            java.lang.Object r1 = r3.a()
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            com.google.gson.JsonElement r1 = r1.a(r9)
            boolean r2 = r1 instanceof com.fossil.he4
            if (r2 == 0) goto L_0x00d8
            com.fossil.yi5 r1 = new com.fossil.yi5
            r11 = 600(0x258, float:8.41E-43)
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 16
            r17 = 0
            r10 = r1
            r10.<init>(r11, r12, r13, r14, r15, r16, r17)
            goto L_0x011d
        L_0x00d8:
            com.fossil.bj5 r2 = new com.fossil.bj5
            com.fossil.ee7.a(r1, r9)
            java.lang.String r1 = r1.f()
            r3 = 0
            r4 = 2
            r2.<init>(r1, r3, r4, r8)
            r1 = r2
            goto L_0x011d
        L_0x00e8:
            com.fossil.yi5 r1 = new com.fossil.yi5
            r10 = 600(0x258, float:8.41E-43)
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 16
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            goto L_0x011d
        L_0x00f9:
            com.fossil.ee7.a()
            throw r8
        L_0x00fd:
            boolean r1 = r3 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x011e
            com.fossil.yi5 r1 = new com.fossil.yi5
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r5 = r3.a()
            com.portfolio.platform.data.model.ServerError r6 = r3.c()
            java.lang.Throwable r7 = r3.d()
            java.lang.String r8 = r3.b()
            r9 = 0
            r10 = 16
            r11 = 0
            r4 = r1
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
        L_0x011d:
            return r1
        L_0x011e:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.swapPairingKey(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object updateDevice(Device device, fb7<? super zi5<Void>> fb7) {
        FLogger.INSTANCE.getLocal().d(TAG, "updateDevice");
        return aj5.a(new DeviceRemoteDataSource$updateDevice$Anon2(this, device, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateDeviceSecretKey(java.lang.String r6, java.lang.String r7, com.fossil.fb7<? super com.fossil.zi5<com.fossil.ie4>> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 r0 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$Anon1
            r0.<init>(r5, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r6 = r0.L$3
            com.fossil.ie4 r6 = (com.fossil.ie4) r6
            java.lang.Object r6 = r0.L$2
            r7 = r6
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DeviceRemoteDataSource) r0
            com.fossil.t87.a(r8)
            goto L_0x0066
        L_0x003a:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0042:
            com.fossil.t87.a(r8)
            com.fossil.ie4 r8 = new com.fossil.ie4
            r8.<init>()
            java.lang.String r2 = "secretKey"
            r8.a(r2, r7)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1
            r4 = 0
            r2.<init>(r5, r6, r8, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.L$2 = r7
            r0.L$3 = r8
            r0.label = r3
            java.lang.Object r8 = com.fossil.aj5.a(r2, r0)
            if (r8 != r1) goto L_0x0066
            return r1
        L_0x0066:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "updateDeviceSecretKey "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r7 = " of "
            r2.append(r7)
            r2.append(r6)
            java.lang.String r6 = " response "
            r2.append(r6)
            r2.append(r8)
            java.lang.String r6 = r2.toString()
            r0.d(r1, r6)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DeviceRemoteDataSource.updateDeviceSecretKey(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
