package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.qu4;
import com.fossil.uh;
import com.fossil.vh;
import com.portfolio.platform.data.model.LocalFile;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDao_Impl extends FileDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<LocalFile> __deletionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ qu4 __fileTypeConverter; // = new qu4();
    @DexIgnore
    public /* final */ vh<LocalFile> __insertionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearLocalFileTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteLocalFileByType;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteLocalFileByUri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<LocalFile> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `localfile` (`pinType`,`type`,`fileName`,`localUri`,`remoteUrl`,`checksum`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, LocalFile localFile) {
            ajVar.bindLong(1, (long) localFile.getPinType());
            String a = FileDao_Impl.this.__fileTypeConverter.a(localFile.getType());
            if (a == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, a);
            }
            if (localFile.getFileName() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, localFile.getFileName());
            }
            if (localFile.getLocalUri() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, localFile.getLocalUri());
            }
            if (localFile.getRemoteUrl() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, localFile.getRemoteUrl());
            }
            if (localFile.getChecksum() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, localFile.getChecksum());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<LocalFile> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `localfile` WHERE `remoteUrl` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, LocalFile localFile) {
            if (localFile.getRemoteUrl() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, localFile.getRemoteUrl());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM localfile WHERE localUri=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM localfile WHERE type=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM localfile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<LocalFile>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon6(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<LocalFile> call() throws Exception {
            Cursor a = pi.a(FileDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "type");
                int b3 = oi.b(a, "fileName");
                int b4 = oi.b(a, "localUri");
                int b5 = oi.b(a, "remoteUrl");
                int b6 = oi.b(a, "checksum");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    LocalFile localFile = new LocalFile(a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6));
                    localFile.setPinType(a.getInt(b));
                    localFile.setType(FileDao_Impl.this.__fileTypeConverter.a(a.getString(b2)));
                    arrayList.add(localFile);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public FileDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfLocalFile = new Anon1(ciVar);
        this.__deletionAdapterOfLocalFile = new Anon2(ciVar);
        this.__preparedStmtOfDeleteLocalFileByUri = new Anon3(ciVar);
        this.__preparedStmtOfDeleteLocalFileByType = new Anon4(ciVar);
        this.__preparedStmtOfClearLocalFileTable = new Anon5(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void clearLocalFileTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearLocalFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearLocalFileTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfLocalFile.handle(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByType(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteLocalFileByType.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByType.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByUri(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteLocalFileByUri.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByUri.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListLocalFile() {
        fi b = fi.b("SELECT * FROM localfile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "fileName");
            int b5 = oi.b(a, "localUri");
            int b6 = oi.b(a, "remoteUrl");
            int b7 = oi.b(a, "checksum");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                LocalFile localFile = new LocalFile(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7));
                localFile.setPinType(a.getInt(b2));
                localFile.setType(this.__fileTypeConverter.a(a.getString(b3)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListPendingFile() {
        fi b = fi.b("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "fileName");
            int b5 = oi.b(a, "localUri");
            int b6 = oi.b(a, "remoteUrl");
            int b7 = oi.b(a, "checksum");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                LocalFile localFile = new LocalFile(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7));
                localFile.setPinType(a.getInt(b2));
                localFile.setType(this.__fileTypeConverter.a(a.getString(b3)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LiveData<List<LocalFile>> getListPendingFileAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"localfile"}, false, (Callable) new Anon6(fi.b("SELECT * FROM localfile WHERE pinType  <> 0", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LocalFile getLocalFileByRemoteUrl(String str) {
        fi b = fi.b("SELECT * FROM localfile WHERE remoteUrl=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        LocalFile localFile = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "type");
            int b4 = oi.b(a, "fileName");
            int b5 = oi.b(a, "localUri");
            int b6 = oi.b(a, "remoteUrl");
            int b7 = oi.b(a, "checksum");
            if (a.moveToFirst()) {
                LocalFile localFile2 = new LocalFile(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7));
                localFile2.setPinType(a.getInt(b2));
                localFile2.setType(this.__fileTypeConverter.a(a.getString(b3)));
                localFile = localFile2;
            }
            return localFile;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void insertListLocalFile(List<LocalFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void upsertLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
