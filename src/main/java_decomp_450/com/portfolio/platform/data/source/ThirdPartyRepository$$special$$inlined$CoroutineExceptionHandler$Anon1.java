package com.portfolio.platform.data.source;

import com.fossil.cb7;
import com.fossil.ib7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1 extends cb7 implements CoroutineExceptionHandler {
    @DexIgnore
    public ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(ib7.c cVar) {
        super(cVar);
    }

    @DexIgnore
    @Override // kotlinx.coroutines.CoroutineExceptionHandler
    public void handleException(ib7 ib7, Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "exception when push third party database " + th);
    }
}
