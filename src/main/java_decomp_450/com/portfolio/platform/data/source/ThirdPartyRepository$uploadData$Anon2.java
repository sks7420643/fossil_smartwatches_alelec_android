package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2", f = "ThirdPartyRepository.kt", l = {92, 148, 149, 150, 151, 152, 153, 154}, m = "invokeSuspend")
public final class ThirdPartyRepository$uploadData$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$1", f = "ThirdPartyRepository.kt", l = {108}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSampleList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitSampleList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$gFitSampleList, this.$activeDeviceSerial, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitSample> list = this.$gFitSampleList;
                String str = this.$activeDeviceSerial;
                this.L$0 = yi7;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSampleToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$2", f = "ThirdPartyRepository.kt", l = {116}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitActiveTimeList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitActiveTimeList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$gFitActiveTimeList, this.$activeDeviceSerial, fb7);
            anon2_Level2.p$ = (yi7) obj;
            return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((Anon2_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitActiveTime> list = this.$gFitActiveTimeList;
                String str = this.$activeDeviceSerial;
                this.L$0 = yi7;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitActiveTimeToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$3", f = "ThirdPartyRepository.kt", l = {124}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitHeartRateList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitHeartRateList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, this.$gFitHeartRateList, this.$activeDeviceSerial, fb7);
            anon3_Level2.p$ = (yi7) obj;
            return anon3_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((Anon3_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitHeartRate> list = this.$gFitHeartRateList;
                String str = this.$activeDeviceSerial;
                this.L$0 = yi7;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitHeartRateToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$4", f = "ThirdPartyRepository.kt", l = {131}, m = "invokeSuspend")
    public static final class Anon4_Level2 extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitSleepList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitSleepList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon4_Level2 anon4_Level2 = new Anon4_Level2(this.this$0, this.$gFitSleepList, this.$activeDeviceSerial, fb7);
            anon4_Level2.p$ = (yi7) obj;
            return anon4_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((Anon4_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitSleep> list = this.$gFitSleepList;
                String str = this.$activeDeviceSerial;
                this.L$0 = yi7;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitSleepDataToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$2$5", f = "ThirdPartyRepository.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class Anon5_Level2 extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public /* final */ /* synthetic */ List $gFitWorkoutSessionList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$uploadData$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon5_Level2(ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2, List list, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = thirdPartyRepository$uploadData$Anon2;
            this.$gFitWorkoutSessionList = list;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon5_Level2 anon5_Level2 = new Anon5_Level2(this.this$0, this.$gFitWorkoutSessionList, this.$activeDeviceSerial, fb7);
            anon5_Level2.p$ = (yi7) obj;
            return anon5_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
            return ((Anon5_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ThirdPartyRepository thirdPartyRepository = this.this$0.this$0;
                List<GFitWorkoutSession> list = this.$gFitWorkoutSessionList;
                String str = this.$activeDeviceSerial;
                this.L$0 = yi7;
                this.label = 1;
                obj = thirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(list, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$uploadData$Anon2(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, fb7 fb7) {
        super(2, fb7);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ThirdPartyRepository$uploadData$Anon2 thirdPartyRepository$uploadData$Anon2 = new ThirdPartyRepository$uploadData$Anon2(this.this$0, this.$pushPendingThirdPartyDataCallback, fb7);
        thirdPartyRepository$uploadData$Anon2.p$ = (yi7) obj;
        return thirdPartyRepository$uploadData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ThirdPartyRepository$uploadData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x034b  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x036b  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x038b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x03ab  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x03cb  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x03f9  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r21) {
        /*
            r20 = this;
            r0 = r20
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            java.lang.String r5 = "ThirdPartyRepository"
            switch(r2) {
                case 0: goto L_0x0162;
                case 1: goto L_0x0152;
                case 2: goto L_0x0125;
                case 3: goto L_0x00f8;
                case 4: goto L_0x00cb;
                case 5: goto L_0x009e;
                case 6: goto L_0x0071;
                case 7: goto L_0x0044;
                case 8: goto L_0x0017;
                default: goto L_0x000f;
            }
        L_0x000f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0017:
            java.lang.Object r1 = r0.L$9
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$8
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$7
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$6
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$5
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$4
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$3
            com.fossil.hj7 r1 = (com.fossil.hj7) r1
            java.lang.Object r1 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r1 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r1
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r0.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r21)
            goto L_0x03ea
        L_0x0044:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x03c9
        L_0x0071:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x03a9
        L_0x009e:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x0389
        L_0x00cb:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x0369
        L_0x00f8:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x0349
        L_0x0125:
            java.lang.Object r2 = r0.L$9
            com.fossil.hj7 r2 = (com.fossil.hj7) r2
            java.lang.Object r3 = r0.L$8
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r4 = r0.L$7
            com.fossil.hj7 r4 = (com.fossil.hj7) r4
            java.lang.Object r6 = r0.L$6
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            java.lang.Object r7 = r0.L$5
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            java.lang.Object r8 = r0.L$4
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            java.lang.Object r9 = r0.L$3
            com.fossil.hj7 r9 = (com.fossil.hj7) r9
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r12 = r0.L$0
            com.fossil.yi7 r12 = (com.fossil.yi7) r12
            com.fossil.t87.a(r21)
            goto L_0x0329
        L_0x0152:
            java.lang.Object r2 = r0.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r6 = r0.L$0
            com.fossil.yi7 r6 = (com.fossil.yi7) r6
            com.fossil.t87.a(r21)
            r7 = r21
            r11 = r2
            r2 = r6
            goto L_0x018c
        L_0x0162:
            com.fossil.t87.a(r21)
            com.fossil.yi7 r2 = r0.p$
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = "Start uploadData"
            r6.d(r5, r7)
            com.portfolio.platform.data.source.ThirdPartyRepository r6 = r0.this$0
            com.portfolio.platform.PortfolioApp r6 = r6.getMPortfolioApp()
            java.lang.String r6 = r6.c()
            com.fossil.pg5 r7 = com.fossil.pg5.i
            r0.L$0 = r2
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.e(r0)
            if (r7 != r1) goto L_0x018b
            return r1
        L_0x018b:
            r11 = r6
        L_0x018c:
            r10 = r7
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r10 = (com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase) r10
            int r6 = r11.length()
            if (r6 <= 0) goto L_0x0197
            r6 = 1
            goto L_0x0198
        L_0x0197:
            r6 = 0
        L_0x0198:
            if (r6 == 0) goto L_0x03f5
            com.portfolio.platform.data.source.ThirdPartyRepository r6 = r0.this$0
            com.fossil.ie5 r6 = r6.mGoogleFitHelper
            boolean r6 = r6.e()
            if (r6 == 0) goto L_0x02f5
            com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao r6 = r10.getGFitSampleDao()
            java.util.List r6 = r6.getAllGFitSample()
            boolean r7 = r6.isEmpty()
            r7 = r7 ^ r3
            if (r7 == 0) goto L_0x01e0
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "saveGFitSampleToGoogleFit "
            r8.append(r9)
            r8.append(r6)
            java.lang.String r8 = r8.toString()
            r7.d(r5, r8)
            r13 = 0
            r14 = 0
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon1_Level2 r15 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon1_Level2
            r15.<init>(r0, r6, r11, r4)
            r16 = 3
            r17 = 0
            r12 = r2
            com.fossil.hj7 r6 = com.fossil.xh7.a(r12, r13, r14, r15, r16, r17)
            goto L_0x01ec
        L_0x01e0:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = "GFitSample is empty"
            r6.d(r5, r7)
            r6 = r4
        L_0x01ec:
            com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao r7 = r10.getGFitActiveTimeDao()
            java.util.List r7 = r7.getAllGFitActiveTime()
            boolean r8 = r7.isEmpty()
            r8 = r8 ^ r3
            if (r8 == 0) goto L_0x0226
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r12 = "saveGFitActiveTimeToGoogleFit "
            r9.append(r12)
            r9.append(r7)
            java.lang.String r9 = r9.toString()
            r8.d(r5, r9)
            r13 = 0
            r14 = 0
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon2_Level2 r15 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon2_Level2
            r15.<init>(r0, r7, r11, r4)
            r16 = 3
            r17 = 0
            r12 = r2
            com.fossil.hj7 r7 = com.fossil.xh7.a(r12, r13, r14, r15, r16, r17)
            goto L_0x0232
        L_0x0226:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = "GFitActiveTime is empty"
            r7.d(r5, r8)
            r7 = r4
        L_0x0232:
            com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao r8 = r10.getGFitHeartRateDao()
            java.util.List r8 = r8.getAllGFitHeartRate()
            boolean r9 = r8.isEmpty()
            r9 = r9 ^ r3
            if (r9 == 0) goto L_0x026c
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "saveGFitHeartRateToGoogleFit "
            r12.append(r13)
            r12.append(r8)
            java.lang.String r12 = r12.toString()
            r9.d(r5, r12)
            r13 = 0
            r14 = 0
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon3_Level2 r15 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon3_Level2
            r15.<init>(r0, r8, r11, r4)
            r16 = 3
            r17 = 0
            r12 = r2
            com.fossil.hj7 r8 = com.fossil.xh7.a(r12, r13, r14, r15, r16, r17)
            goto L_0x0278
        L_0x026c:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = "GFitHeartRate is empty"
            r8.d(r5, r9)
            r8 = r4
        L_0x0278:
            com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao r9 = r10.getGFitSleepDao()
            java.util.List r9 = r9.getAllGFitSleep()
            boolean r12 = r9.isEmpty()
            r12 = r12 ^ r3
            if (r12 == 0) goto L_0x0298
            r13 = 0
            r14 = 0
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon4_Level2 r15 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon4_Level2
            r15.<init>(r0, r9, r11, r4)
            r16 = 3
            r17 = 0
            r12 = r2
            com.fossil.hj7 r9 = com.fossil.xh7.a(r12, r13, r14, r15, r16, r17)
            goto L_0x02a4
        L_0x0298:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r12 = "GFitSleep is empty"
            r9.d(r5, r12)
            r9 = r4
        L_0x02a4:
            com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao r12 = r10.getGFitWorkoutSessionDao()
            java.util.List r12 = r12.getAllGFitWorkoutSession()
            boolean r13 = r12.isEmpty()
            r3 = r3 ^ r13
            if (r3 == 0) goto L_0x02de
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "saveGFitWorkoutSessionToGoogleFit "
            r13.append(r14)
            r13.append(r12)
            java.lang.String r13 = r13.toString()
            r3.d(r5, r13)
            r13 = 0
            r14 = 0
            com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon5_Level2 r15 = new com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2$Anon5_Level2
            r15.<init>(r0, r12, r11, r4)
            r16 = 3
            r17 = 0
            r12 = r2
            com.fossil.hj7 r3 = com.fossil.xh7.a(r12, r13, r14, r15, r16, r17)
            goto L_0x02ea
        L_0x02de:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r12 = "GFitWorkoutSession is empty"
            r3.d(r5, r12)
            r3 = r4
        L_0x02ea:
            r18 = r9
            r9 = r6
            r6 = r18
            r19 = r8
            r8 = r7
            r7 = r19
            goto L_0x0305
        L_0x02f5:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r6 = "Google Fit is not connected"
            r3.d(r5, r6)
            r3 = r4
            r6 = r3
            r7 = r6
            r8 = r7
            r9 = r8
        L_0x0305:
            if (r9 == 0) goto L_0x0325
            r0.L$0 = r2
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r3
            r0.L$8 = r4
            r0.L$9 = r4
            r12 = 2
            r0.label = r12
            java.lang.Object r12 = r9.c(r0)
            if (r12 != r1) goto L_0x0325
            return r1
        L_0x0325:
            r12 = r2
            r2 = r4
            r4 = r3
            r3 = r2
        L_0x0329:
            if (r8 == 0) goto L_0x0349
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r13 = 3
            r0.label = r13
            java.lang.Object r13 = r8.c(r0)
            if (r13 != r1) goto L_0x0349
            return r1
        L_0x0349:
            if (r7 == 0) goto L_0x0369
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r13 = 4
            r0.label = r13
            java.lang.Object r13 = r7.c(r0)
            if (r13 != r1) goto L_0x0369
            return r1
        L_0x0369:
            if (r6 == 0) goto L_0x0389
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r13 = 5
            r0.label = r13
            java.lang.Object r13 = r6.c(r0)
            if (r13 != r1) goto L_0x0389
            return r1
        L_0x0389:
            if (r4 == 0) goto L_0x03a9
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r13 = 6
            r0.label = r13
            java.lang.Object r13 = r4.c(r0)
            if (r13 != r1) goto L_0x03a9
            return r1
        L_0x03a9:
            if (r3 == 0) goto L_0x03c9
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r13 = 7
            r0.label = r13
            java.lang.Object r13 = r3.c(r0)
            if (r13 != r1) goto L_0x03c9
            return r1
        L_0x03c9:
            if (r2 == 0) goto L_0x03ea
            r0.L$0 = r12
            r0.L$1 = r11
            r0.L$2 = r10
            r0.L$3 = r9
            r0.L$4 = r8
            r0.L$5 = r7
            r0.L$6 = r6
            r0.L$7 = r4
            r0.L$8 = r3
            r0.L$9 = r2
            r3 = 8
            r0.label = r3
            java.lang.Object r2 = r2.c(r0)
            if (r2 != r1) goto L_0x03ea
            return r1
        L_0x03ea:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "End uploadData"
            r1.d(r5, r2)
        L_0x03f5:
            com.portfolio.platform.data.source.ThirdPartyRepository$PushPendingThirdPartyDataCallback r1 = r0.$pushPendingThirdPartyDataCallback
            if (r1 == 0) goto L_0x03fc
            r1.onComplete()
        L_0x03fc:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
            switch-data {0->0x0162, 1->0x0152, 2->0x0125, 3->0x00f8, 4->0x00cb, 5->0x009e, 6->0x0071, 7->0x0044, 8->0x0017, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThirdPartyRepository$uploadData$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
