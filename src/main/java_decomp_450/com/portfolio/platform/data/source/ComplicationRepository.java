package com.portfolio.platform.data.source;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.nh7;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "ComplicationRepository";
    @DexIgnore
    public /* final */ ComplicationDao mComplicationDao;
    @DexIgnore
    public /* final */ ComplicationRemoteDataSource mComplicationRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public ComplicationRepository(ComplicationDao complicationDao, ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        ee7.b(complicationDao, "mComplicationDao");
        ee7.b(complicationRemoteDataSource, "mComplicationRemoteDataSource");
        ee7.b(portfolioApp, "mPortfolioApp");
        this.mComplicationDao = complicationDao;
        this.mComplicationRemoteDataSource = complicationRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mComplicationDao.clearAll();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00bc, code lost:
        if ((!((java.util.Collection) r1).isEmpty()) == false) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00c7, code lost:
        if (r8.isEmpty() != false) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00c9, code lost:
        r0.mComplicationDao.clearAll();
        r8 = r0.mComplicationDao;
        r9 = r9.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d4, code lost:
        if (r9 == null) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d6, code lost:
        r8.upsertComplicationList((java.util.List) r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00dc, code lost:
        com.fossil.ee7.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00df, code lost:
        throw null;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadAllComplication(java.lang.String r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.ComplicationRepository$downloadAllComplication$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.portfolio.platform.data.source.ComplicationRepository$downloadAllComplication$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$downloadAllComplication$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.ComplicationRepository$downloadAllComplication$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$downloadAllComplication$Anon1
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "downloadAllComplication of "
            java.lang.String r4 = "ComplicationRepository"
            r5 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r5) goto L_0x0035
            java.lang.Object r8 = r0.L$1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.ComplicationRepository r0 = (com.portfolio.platform.data.source.ComplicationRepository) r0
            com.fossil.t87.a(r9)
            goto L_0x0068
        L_0x0035:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x003d:
            com.fossil.t87.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r9.d(r4, r2)
            com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource r9 = r7.mComplicationRemoteDataSource
            r0.L$0 = r7
            r0.L$1 = r8
            r0.label = r5
            java.lang.Object r9 = r9.getAllComplication(r8, r0)
            if (r9 != r1) goto L_0x0067
            return r1
        L_0x0067:
            r0 = r7
        L_0x0068:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r1 = r9 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00e0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r3)
            r6.append(r8)
            java.lang.String r8 = " success isFromCache "
            r6.append(r8)
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            boolean r8 = r9.b()
            r6.append(r8)
            java.lang.String r8 = " response "
            r6.append(r8)
            java.lang.Object r8 = r9.a()
            java.util.List r8 = (java.util.List) r8
            r6.append(r8)
            java.lang.String r8 = r6.toString()
            r1.d(r4, r8)
            com.portfolio.platform.data.source.local.diana.ComplicationDao r8 = r0.mComplicationDao
            java.util.List r8 = r8.getAllComplications()
            boolean r1 = r9.b()
            if (r1 != 0) goto L_0x00c3
            java.lang.Object r1 = r9.a()
            if (r1 == 0) goto L_0x00bf
            java.util.Collection r1 = (java.util.Collection) r1
            boolean r1 = r1.isEmpty()
            r1 = r1 ^ r5
            if (r1 != 0) goto L_0x00c9
            goto L_0x00c3
        L_0x00bf:
            com.fossil.ee7.a()
            throw r2
        L_0x00c3:
            boolean r8 = r8.isEmpty()
            if (r8 == 0) goto L_0x011c
        L_0x00c9:
            com.portfolio.platform.data.source.local.diana.ComplicationDao r8 = r0.mComplicationDao
            r8.clearAll()
            com.portfolio.platform.data.source.local.diana.ComplicationDao r8 = r0.mComplicationDao
            java.lang.Object r9 = r9.a()
            if (r9 == 0) goto L_0x00dc
            java.util.List r9 = (java.util.List) r9
            r8.upsertComplicationList(r9)
            goto L_0x011c
        L_0x00dc:
            com.fossil.ee7.a()
            throw r2
        L_0x00e0:
            boolean r0 = r9 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x011c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r3)
            r1.append(r8)
            java.lang.String r8 = " fail!!! error="
            r1.append(r8)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r8 = r9.a()
            r1.append(r8)
            java.lang.String r8 = " serverErrorCode="
            r1.append(r8)
            com.portfolio.platform.data.model.ServerError r8 = r9.c()
            if (r8 == 0) goto L_0x0112
            java.lang.Integer r2 = r8.getCode()
        L_0x0112:
            r1.append(r2)
            java.lang.String r8 = r1.toString()
            r0.d(r4, r8)
        L_0x011c:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.downloadAllComplication(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<Complication> getAllComplicationRaw() {
        return this.mComplicationDao.getAllComplications();
    }

    @DexIgnore
    public final List<Complication> getComplicationByIds(List<String> list) {
        ee7.b(list, "ids");
        if (!list.isEmpty()) {
            return ea7.a((Iterable) this.mComplicationDao.getComplicationByIds(list), (Comparator) new ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1(list));
        }
        return new ArrayList();
    }

    @DexIgnore
    public final List<Complication> queryComplicationByName(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ArrayList arrayList = new ArrayList();
        for (Complication complication : this.mComplicationDao.getAllComplications()) {
            String normalize = Normalizer.normalize(ig5.a(this.mPortfolioApp, complication.getNameKey(), complication.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            ee7.a((Object) normalize, "name");
            ee7.a((Object) normalize2, "searchQuery");
            if (nh7.a((CharSequence) normalize, (CharSequence) normalize2, true)) {
                arrayList.add(complication);
            }
        }
        return arrayList;
    }
}
