package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRemoteDataSource_Factory implements Factory<DeviceRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public DeviceRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static DeviceRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new DeviceRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static DeviceRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new DeviceRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DeviceRemoteDataSource get() {
        return newInstance(this.mApiServiceProvider.get());
    }
}
