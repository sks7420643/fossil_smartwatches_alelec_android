package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fe7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.nf;
import com.fossil.pg5;
import com.fossil.pj4;
import com.fossil.qf;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.te5;
import com.fossil.vc7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummariesPaging$2", f = "GoalTrackingRepository.kt", l = {286}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummariesPaging$Anon2 extends zb7 implements kd7<yi7, fb7<? super Listing<GoalTrackingSummary>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ pj4 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $createdDate;
    @DexIgnore
    public /* final */ /* synthetic */ te5.a $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
            return goalTrackingSummaryLocalDataSource.getMNetworkState();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            GoalTrackingSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
            if (a != null) {
                a.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            te5 mHelper;
            GoalTrackingSummaryLocalDataSource a = this.$sourceFactory.getSourceLiveData().a();
            if (a != null && (mHelper = a.getMHelper()) != null) {
                mHelper.b();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummariesPaging$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, pj4 pj4, te5.a aVar, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$createdDate = date;
        this.$appExecutors = pj4;
        this.$listener = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getSummariesPaging$Anon2 goalTrackingRepository$getSummariesPaging$Anon2 = new GoalTrackingRepository$getSummariesPaging$Anon2(this.this$0, this.$createdDate, this.$appExecutors, this.$listener, fb7);
        goalTrackingRepository$getSummariesPaging$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getSummariesPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Listing<GoalTrackingSummary>> fb7) {
        return ((GoalTrackingRepository$getSummariesPaging$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Calendar calendar;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getSummariesPaging");
            GoalTrackingSummaryLocalDataSource.Companion companion = GoalTrackingSummaryLocalDataSource.Companion;
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            Date time = instance.getTime();
            ee7.a((Object) time, "Calendar.getInstance().time");
            Date calculateNextKey = companion.calculateNextKey(time, this.$createdDate);
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "calendar");
            instance2.setTime(calculateNextKey);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.L$1 = calculateNextKey;
            this.L$2 = instance2;
            this.label = 1;
            obj = pg5.c(this);
            if (obj == a) {
                return a;
            }
            calendar = instance2;
        } else if (i == 1) {
            Date date = (Date) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            calendar = (Calendar) this.L$2;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingRepository goalTrackingRepository = this.this$0;
        Date date2 = this.$createdDate;
        pj4 pj4 = this.$appExecutors;
        te5.a aVar = this.$listener;
        ee7.a((Object) calendar, "calendar");
        GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory = new GoalTrackingSummaryDataSourceFactory(goalTrackingRepository, (GoalTrackingDatabase) obj, date2, pj4, aVar, calendar);
        this.this$0.mSourceFactoryList.add(goalTrackingSummaryDataSourceFactory);
        qf.f.a aVar2 = new qf.f.a();
        aVar2.a(30);
        aVar2.a(false);
        aVar2.b(30);
        aVar2.c(5);
        qf.f a2 = aVar2.a();
        ee7.a((Object) a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new nf(goalTrackingSummaryDataSourceFactory, a2).a();
        ee7.a((Object) a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = ge.b(goalTrackingSummaryDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        ee7.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, b, new Anon2_Level2(goalTrackingSummaryDataSourceFactory), new Anon3_Level2(goalTrackingSummaryDataSourceFactory));
    }
}
