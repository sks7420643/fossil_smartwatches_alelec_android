package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$loadSummaries$2", f = "SummariesRepository.kt", l = {SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, Action.Presenter.NEXT}, m = "invokeSuspend")
public final class SummariesRepository$loadSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ie4>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends TypeToken<ApiResponse<FitnessDayData>> {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$loadSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$loadSummaries$Anon2 summariesRepository$loadSummaries$Anon2 = new SummariesRepository$loadSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, fb7);
        summariesRepository$loadSummaries$Anon2.p$ = (yi7) obj;
        return summariesRepository$loadSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ie4>> fb7) {
        return ((SummariesRepository$loadSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x017a  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r8.label
            r2 = 0
            r3 = 2
            r4 = 1
            java.lang.String r5 = "SummariesRepository"
            if (r1 == 0) goto L_0x002d
            if (r1 == r4) goto L_0x0025
            if (r1 != r3) goto L_0x001d
            java.lang.Object r0 = r8.L$1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r8.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r9)
            goto L_0x007b
        L_0x001d:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0025:
            java.lang.Object r1 = r8.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r9)
            goto L_0x0065
        L_0x002d:
            com.fossil.t87.a(r9)
            com.fossil.yi7 r1 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "loadSummaries - startDate="
            r6.append(r7)
            java.util.Date r7 = r8.$startDate
            r6.append(r7)
            java.lang.String r7 = ", endDate="
            r6.append(r7)
            java.util.Date r7 = r8.$endDate
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r9.d(r5, r6)
            com.fossil.pg5 r9 = com.fossil.pg5.i
            r8.L$0 = r1
            r8.label = r4
            java.lang.Object r9 = r9.b(r8)
            if (r9 != r0) goto L_0x0065
            return r0
        L_0x0065:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r9 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r9
            com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2$response$Anon1_Level2
            r4.<init>(r8, r2)
            r8.L$0 = r1
            r8.L$1 = r9
            r8.label = r3
            java.lang.Object r1 = com.fossil.aj5.a(r4, r8)
            if (r1 != r0) goto L_0x0079
            return r0
        L_0x0079:
            r0 = r9
            r9 = r1
        L_0x007b:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r1 = r9 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x017a
            r1 = r9
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r2 = r1.a()
            if (r2 == 0) goto L_0x01c4
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x01c4
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0159 }
            r1.<init>()     // Catch:{ Exception -> 0x0159 }
            com.fossil.be4 r2 = new com.fossil.be4     // Catch:{ Exception -> 0x0159 }
            r2.<init>()     // Catch:{ Exception -> 0x0159 }
            java.lang.Class<java.util.Date> r3 = java.util.Date.class
            com.portfolio.platform.helper.GsonConvertDate r4 = new com.portfolio.platform.helper.GsonConvertDate     // Catch:{ Exception -> 0x0159 }
            r4.<init>()     // Catch:{ Exception -> 0x0159 }
            r2.a(r3, r4)     // Catch:{ Exception -> 0x0159 }
            java.lang.Class<org.joda.time.DateTime> r3 = org.joda.time.DateTime.class
            com.portfolio.platform.helper.GsonConvertDateTime r4 = new com.portfolio.platform.helper.GsonConvertDateTime     // Catch:{ Exception -> 0x0159 }
            r4.<init>()     // Catch:{ Exception -> 0x0159 }
            r2.a(r3, r4)     // Catch:{ Exception -> 0x0159 }
            com.google.gson.Gson r2 = r2.a()     // Catch:{ Exception -> 0x0159 }
            r3 = r9
            com.fossil.bj5 r3 = (com.fossil.bj5) r3     // Catch:{ Exception -> 0x0159 }
            java.lang.Object r3 = r3.a()     // Catch:{ Exception -> 0x0159 }
            com.fossil.ie4 r3 = (com.fossil.ie4) r3     // Catch:{ Exception -> 0x0159 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0159 }
            com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2$Anon1_Level2     // Catch:{ Exception -> 0x0159 }
            r4.<init>()     // Catch:{ Exception -> 0x0159 }
            java.lang.reflect.Type r4 = r4.getType()     // Catch:{ Exception -> 0x0159 }
            java.lang.Object r2 = r2.a(r3, r4)     // Catch:{ Exception -> 0x0159 }
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2     // Catch:{ Exception -> 0x0159 }
            if (r2 == 0) goto L_0x00f3
            java.util.List r2 = r2.get_items()     // Catch:{ Exception -> 0x0159 }
            if (r2 == 0) goto L_0x00f3
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0159 }
        L_0x00da:
            boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x0159 }
            if (r3 == 0) goto L_0x00f3
            java.lang.Object r3 = r2.next()     // Catch:{ Exception -> 0x0159 }
            com.portfolio.platform.data.model.FitnessDayData r3 = (com.portfolio.platform.data.model.FitnessDayData) r3     // Catch:{ Exception -> 0x0159 }
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r3 = r3.toActivitySummary()     // Catch:{ Exception -> 0x0159 }
            java.lang.String r4 = "it.toActivitySummary()"
            com.fossil.ee7.a(r3, r4)     // Catch:{ Exception -> 0x0159 }
            r1.add(r3)     // Catch:{ Exception -> 0x0159 }
            goto L_0x00da
        L_0x00f3:
            com.portfolio.platform.data.source.local.FitnessDataDao r2 = r0.getFitnessDataDao()     // Catch:{ Exception -> 0x0159 }
            java.util.Date r3 = r8.$startDate     // Catch:{ Exception -> 0x0159 }
            java.util.Date r4 = r8.$endDate     // Catch:{ Exception -> 0x0159 }
            java.util.List r2 = r2.getFitnessData(r3, r4)     // Catch:{ Exception -> 0x0159 }
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0159 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x0159 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0159 }
            r4.<init>()     // Catch:{ Exception -> 0x0159 }
            java.lang.String r6 = "fitnessDataSize "
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            int r6 = r2.size()     // Catch:{ Exception -> 0x0159 }
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r6 = " from "
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            java.util.Date r6 = r8.$startDate     // Catch:{ Exception -> 0x0159 }
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r6 = " to "
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            java.util.Date r6 = r8.$endDate     // Catch:{ Exception -> 0x0159 }
            r4.append(r6)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0159 }
            r3.d(r5, r4)     // Catch:{ Exception -> 0x0159 }
            boolean r2 = r2.isEmpty()     // Catch:{ Exception -> 0x0159 }
            if (r2 == 0) goto L_0x01c4
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r0 = r0.activitySummaryDao()     // Catch:{ Exception -> 0x0159 }
            r0.upsertActivitySummaries(r1)     // Catch:{ Exception -> 0x0159 }
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0159 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x0159 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0159 }
            r2.<init>()     // Catch:{ Exception -> 0x0159 }
            java.lang.String r3 = "XXX- Done upsert summaries from server(2) "
            r2.append(r3)     // Catch:{ Exception -> 0x0159 }
            r2.append(r1)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0159 }
            r0.d(r5, r1)     // Catch:{ Exception -> 0x0159 }
            goto L_0x01c4
        L_0x0159:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "loadSummaries - e="
            r2.append(r3)
            r0.printStackTrace()
            com.fossil.i97 r0 = com.fossil.i97.a
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.d(r5, r0)
            goto L_0x01c4
        L_0x017a:
            boolean r0 = r9 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x01c4
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "loadSummaries - Failure -- code="
            r1.append(r3)
            r3 = r9
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r4 = r3.a()
            r1.append(r4)
            java.lang.String r4 = ", message="
            r1.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r3.c()
            if (r4 == 0) goto L_0x01ab
            java.lang.String r4 = r4.getMessage()
            if (r4 == 0) goto L_0x01ab
            r2 = r4
            goto L_0x01b5
        L_0x01ab:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x01b5
            java.lang.String r2 = r3.getUserMessage()
        L_0x01b5:
            if (r2 == 0) goto L_0x01b8
            goto L_0x01ba
        L_0x01b8:
            java.lang.String r2 = ""
        L_0x01ba:
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r5, r1)
        L_0x01c4:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
