package com.portfolio.platform.data.source.local.thirdparty;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `gFitSample`");
            wiVar.execSQL("DROP TABLE IF EXISTS `gFitActiveTime`");
            wiVar.execSQL("DROP TABLE IF EXISTS `gFitHeartRate`");
            wiVar.execSQL("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            wiVar.execSQL("DROP TABLE IF EXISTS `uaSample`");
            wiVar.execSQL("DROP TABLE IF EXISTS `gFitSleep`");
            if (((ci) ThirdPartyDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) ThirdPartyDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) ThirdPartyDatabase_Impl.this).mDatabase = wiVar;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) ThirdPartyDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) ThirdPartyDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("step", new ti.a("step", "INTEGER", true, 0, null, 1));
            hashMap.put("distance", new ti.a("distance", "REAL", true, 0, null, 1));
            hashMap.put("calorie", new ti.a("calorie", "REAL", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "gFitSample");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap2.put("activeTimes", new ti.a("activeTimes", "TEXT", true, 0, null, 1));
            ti tiVar2 = new ti("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "gFitActiveTime");
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("value", new ti.a("value", "REAL", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ti tiVar3 = new ti("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "gFitHeartRate");
            if (!tiVar3.equals(a3)) {
                return new ei.b(false, "gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(8);
            hashMap4.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put("workoutType", new ti.a("workoutType", "INTEGER", true, 0, null, 1));
            hashMap4.put("steps", new ti.a("steps", "TEXT", true, 0, null, 1));
            hashMap4.put("calories", new ti.a("calories", "TEXT", true, 0, null, 1));
            hashMap4.put("distances", new ti.a("distances", "TEXT", true, 0, null, 1));
            hashMap4.put("heartRates", new ti.a("heartRates", "TEXT", true, 0, null, 1));
            ti tiVar4 = new ti("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, "gFitWorkoutSession");
            if (!tiVar4.equals(a4)) {
                return new ei.b(false, "gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + tiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(5);
            hashMap5.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("step", new ti.a("step", "INTEGER", true, 0, null, 1));
            hashMap5.put("distance", new ti.a("distance", "REAL", true, 0, null, 1));
            hashMap5.put("calorie", new ti.a("calorie", "REAL", true, 0, null, 1));
            hashMap5.put(LogBuilder.KEY_TIME, new ti.a(LogBuilder.KEY_TIME, "INTEGER", true, 0, null, 1));
            ti tiVar5 = new ti("uaSample", hashMap5, new HashSet(0), new HashSet(0));
            ti a5 = ti.a(wiVar, "uaSample");
            if (!tiVar5.equals(a5)) {
                return new ei.b(false, "uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + tiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap6.put("sleepMins", new ti.a("sleepMins", "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            ti tiVar6 = new ti("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
            ti a6 = ti.a(wiVar, "gFitSleep");
            if (tiVar6.equals(a6)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + tiVar6 + "\n Found:\n" + a6);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `gFitSample`");
            writableDatabase.execSQL("DELETE FROM `gFitActiveTime`");
            writableDatabase.execSQL("DELETE FROM `gFitHeartRate`");
            writableDatabase.execSQL("DELETE FROM `gFitWorkoutSession`");
            writableDatabase.execSQL("DELETE FROM `uaSample`");
            writableDatabase.execSQL("DELETE FROM `gFitSleep`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
