package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$2$1$1$loadFromDb$2", f = "ActivitiesRepository.kt", l = {}, m = "invokeSuspend")
public final class ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 extends zb7 implements kd7<yi7, fb7<? super LiveData<List<ActivitySample>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, fb7 fb7) {
        super(2, fb7);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 = new ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(this.this$0, fb7);
        activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4.p$ = (yi7) obj;
        return activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<List<ActivitySample>>> fb7) {
        return ((ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            ActivitySampleDao activitySampleDao = this.this$0.this$0.$fitnessDatabase.activitySampleDao();
            Date date = this.this$0.this$0.$startDate;
            ee7.a((Object) date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.this$0.this$0.$endDate;
            ee7.a((Object) date2, GoalPhase.COLUMN_END_DATE);
            return activitySampleDao.getActivitySamplesLiveData(date, date2);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
