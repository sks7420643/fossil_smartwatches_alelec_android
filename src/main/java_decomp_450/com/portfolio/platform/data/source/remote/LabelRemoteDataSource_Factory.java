package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRemoteDataSource_Factory implements Factory<LabelRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;

    @DexIgnore
    public LabelRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.apiProvider = provider;
    }

    @DexIgnore
    public static LabelRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new LabelRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static LabelRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new LabelRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public LabelRemoteDataSource get() {
        return newInstance(this.apiProvider.get());
    }
}
