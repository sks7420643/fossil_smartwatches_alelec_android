package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository_Factory implements Factory<ComplicationRepository> {
    @DexIgnore
    public /* final */ Provider<ComplicationDao> mComplicationDaoProvider;
    @DexIgnore
    public /* final */ Provider<ComplicationRemoteDataSource> mComplicationRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;

    @DexIgnore
    public ComplicationRepository_Factory(Provider<ComplicationDao> provider, Provider<ComplicationRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        this.mComplicationDaoProvider = provider;
        this.mComplicationRemoteDataSourceProvider = provider2;
        this.mPortfolioAppProvider = provider3;
    }

    @DexIgnore
    public static ComplicationRepository_Factory create(Provider<ComplicationDao> provider, Provider<ComplicationRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new ComplicationRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static ComplicationRepository newInstance(ComplicationDao complicationDao, ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        return new ComplicationRepository(complicationDao, complicationRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ComplicationRepository get() {
        return newInstance(this.mComplicationDaoProvider.get(), this.mComplicationRemoteDataSourceProvider.get(), this.mPortfolioAppProvider.get());
    }
}
