package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.uh;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitHeartRateDao_Impl implements GFitHeartRateDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<GFitHeartRate> __deletionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ vh<GFitHeartRate> __insertionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<GFitHeartRate> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitHeartRate` (`id`,`value`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitHeartRate gFitHeartRate) {
            ajVar.bindLong(1, (long) gFitHeartRate.getId());
            ajVar.bindDouble(2, (double) gFitHeartRate.getValue());
            ajVar.bindLong(3, gFitHeartRate.getStartTime());
            ajVar.bindLong(4, gFitHeartRate.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<GFitHeartRate> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `gFitHeartRate` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitHeartRate gFitHeartRate) {
            ajVar.bindLong(1, (long) gFitHeartRate.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM gFitHeartRate";
        }
    }

    @DexIgnore
    public GFitHeartRateDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfGFitHeartRate = new Anon1(ciVar);
        this.__deletionAdapterOfGFitHeartRate = new Anon2(ciVar);
        this.__preparedStmtOfClearAll = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void deleteListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitHeartRate.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public List<GFitHeartRate> getAllGFitHeartRate() {
        fi b = fi.b("SELECT * FROM gFitHeartRate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "value");
            int b4 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b5 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitHeartRate gFitHeartRate = new GFitHeartRate(a.getFloat(b3), a.getLong(b4), a.getLong(b5));
                gFitHeartRate.setId(a.getInt(b2));
                arrayList.add(gFitHeartRate);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertGFitHeartRate(GFitHeartRate gFitHeartRate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(gFitHeartRate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
