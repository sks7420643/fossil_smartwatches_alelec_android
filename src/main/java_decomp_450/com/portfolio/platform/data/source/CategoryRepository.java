package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.zd7;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "CategoryRepository";
    @DexIgnore
    public /* final */ CategoryDao mCategoryDao;
    @DexIgnore
    public /* final */ CategoryRemoteDataSource mCategoryRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public CategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        ee7.b(categoryDao, "mCategoryDao");
        ee7.b(categoryRemoteDataSource, "mCategoryRemoteDataSource");
        this.mCategoryDao = categoryDao;
        this.mCategoryRemoteDataSource = categoryRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadCategories(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.CategoryRepository$downloadCategories$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.CategoryRepository$downloadCategories$Anon1 r0 = (com.portfolio.platform.data.source.CategoryRepository$downloadCategories$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.CategoryRepository$downloadCategories$Anon1 r0 = new com.portfolio.platform.data.source.CategoryRepository$downloadCategories$Anon1
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "CategoryRepository"
            r4 = 1
            if (r2 == 0) goto L_0x0037
            if (r2 != r4) goto L_0x002f
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.CategoryRepository r0 = (com.portfolio.platform.data.source.CategoryRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x0053
        L_0x002f:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0037:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = "downloadCategories "
            r8.d(r3, r2)
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource r8 = r7.mCategoryRemoteDataSource
            r0.L$0 = r7
            r0.label = r4
            java.lang.Object r8 = r8.getAllCategory(r0)
            if (r8 != r1) goto L_0x0052
            return r1
        L_0x0052:
            r0 = r7
        L_0x0053:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00ad
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "downloadCategories success isFromCache "
            r5.append(r6)
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            boolean r6 = r8.b()
            r5.append(r6)
            java.lang.String r6 = " response "
            r5.append(r6)
            java.lang.Object r6 = r8.a()
            java.util.List r6 = (java.util.List) r6
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r1.d(r3, r5)
            boolean r1 = r8.b()
            if (r1 != 0) goto L_0x00e3
            java.lang.Object r1 = r8.a()
            if (r1 == 0) goto L_0x00a9
            java.util.Collection r1 = (java.util.Collection) r1
            boolean r1 = r1.isEmpty()
            r1 = r1 ^ r4
            if (r1 == 0) goto L_0x00e3
            com.portfolio.platform.data.source.local.CategoryDao r0 = r0.mCategoryDao
            java.lang.Object r8 = r8.a()
            java.util.List r8 = (java.util.List) r8
            r0.upsertCategoryList(r8)
            goto L_0x00e3
        L_0x00a9:
            com.fossil.ee7.a()
            throw r2
        L_0x00ad:
            boolean r0 = r8 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00e3
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "downloadCategories fail!!! error="
            r1.append(r4)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r4 = r8.a()
            r1.append(r4)
            java.lang.String r4 = " serverErrorCode="
            r1.append(r4)
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00d9
            java.lang.Integer r2 = r8.getCode()
        L_0x00d9:
            r1.append(r2)
            java.lang.String r8 = r1.toString()
            r0.d(r3, r8)
        L_0x00e3:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.CategoryRepository.downloadCategories(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<Category> getAllCategories() {
        return this.mCategoryDao.getAllCategory();
    }
}
