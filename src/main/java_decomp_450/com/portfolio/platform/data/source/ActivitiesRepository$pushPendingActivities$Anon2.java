package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$pushPendingActivities$2", f = "ActivitiesRepository.kt", l = {188, 191}, m = "invokeSuspend")
public final class ActivitiesRepository$pushPendingActivities$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository.PushPendingActivitiesCallback $pushPendingActivitiesCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$pushPendingActivities$Anon2(ActivitiesRepository activitiesRepository, ActivitiesRepository.PushPendingActivitiesCallback pushPendingActivitiesCallback, fb7 fb7) {
        super(2, fb7);
        this.this$0 = activitiesRepository;
        this.$pushPendingActivitiesCallback = pushPendingActivitiesCallback;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$pushPendingActivities$Anon2 activitiesRepository$pushPendingActivities$Anon2 = new ActivitiesRepository$pushPendingActivities$Anon2(this.this$0, this.$pushPendingActivitiesCallback, fb7);
        activitiesRepository$pushPendingActivities$Anon2.p$ = (yi7) obj;
        return activitiesRepository$pushPendingActivities$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ActivitiesRepository$pushPendingActivities$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<SampleRaw> pendingActivitySamples = ((FitnessDatabase) obj).sampleRawDao().getPendingActivitySamples();
        if (pendingActivitySamples.size() > 0) {
            ActivitiesRepository activitiesRepository = this.this$0;
            ActivitiesRepository.PushPendingActivitiesCallback pushPendingActivitiesCallback = this.$pushPendingActivitiesCallback;
            this.L$0 = yi7;
            this.L$1 = pendingActivitySamples;
            this.label = 2;
            if (activitiesRepository.saveActivitiesToServer(pendingActivitySamples, pushPendingActivitiesCallback, this) == a) {
                return a;
            }
            return i97.a;
        }
        ActivitiesRepository.PushPendingActivitiesCallback pushPendingActivitiesCallback2 = this.$pushPendingActivitiesCallback;
        if (pushPendingActivitiesCallback2 == null) {
            return null;
        }
        pushPendingActivitiesCallback2.onFail(404);
        return i97.a;
    }
}
