package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesRingStyleDaoFactory implements Factory<RingStyleDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesRingStyleDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesRingStyleDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesRingStyleDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static RingStyleDao providesRingStyleDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        RingStyleDao providesRingStyleDao = portfolioDatabaseModule.providesRingStyleDao(dianaCustomizeDatabase);
        c87.a(providesRingStyleDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesRingStyleDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public RingStyleDao get() {
        return providesRingStyleDao(this.module, this.dbProvider.get());
    }
}
