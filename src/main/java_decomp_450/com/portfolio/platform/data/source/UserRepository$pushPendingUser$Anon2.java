package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.p87;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$pushPendingUser$2", f = "UserRepository.kt", l = {69, 75}, m = "invokeSuspend")
public final class UserRepository$pushPendingUser$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<MFUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$pushPendingUser$Anon2(UserRepository userRepository, MFUser mFUser, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
        this.$user = mFUser;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$pushPendingUser$Anon2 userRepository$pushPendingUser$Anon2 = new UserRepository$pushPendingUser$Anon2(this.this$0, this.$user, fb7);
        userRepository$pushPendingUser$Anon2.p$ = (yi7) obj;
        return userRepository$pushPendingUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<MFUser>> fb7) {
        return ((UserRepository$pushPendingUser$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            if (ee7.a((Object) this.$user.getPinType(), (Object) String.valueOf(0))) {
                return new bj5(this.$user, false, 2, null);
            }
            UserRemoteDataSource access$getMUserRemoteDataSource$p = this.this$0.mUserRemoteDataSource;
            MFUser mFUser = this.$user;
            this.L$0 = yi7;
            this.label = 1;
            obj = access$getMUserRemoteDataSource$p.updateUser(mFUser, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            MFUser mFUser2 = (MFUser) this.L$2;
            zi5 zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            ((UserDatabase) obj).userDao().updateUser(this.$user);
            return new bj5(this.$user, false, 2, null);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "pushPendingUser success ");
            Object a2 = ((bj5) zi52).a();
            if (a2 != null) {
                MFUser mFUser3 = (MFUser) a2;
                this.$user.setUpdatedAt(mFUser3.getUpdatedAt());
                this.$user.setPinType(String.valueOf(0));
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.L$1 = zi52;
                this.L$2 = mFUser3;
                this.label = 2;
                obj = pg5.f(this);
                if (obj == a) {
                    return a;
                }
                ((UserDatabase) obj).userDao().updateUser(this.$user);
                return new bj5(this.$user, false, 2, null);
            }
            ee7.a();
            throw null;
        } else if (zi52 instanceof yi5) {
            FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "pushPendingUser fail");
            yi5 yi5 = (yi5) zi52;
            return new yi5(yi5.a(), yi5.c(), null, null, null, 28, null);
        } else {
            throw new p87();
        }
    }
}
