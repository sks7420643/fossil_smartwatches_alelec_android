package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.diana.WatchAppDataDao;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRepository_Factory implements Factory<WatchAppDataRepository> {
    @DexIgnore
    public /* final */ Provider<FileRepository> mFileRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppDataDao> mLocalSourceProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppDataRemoteDataSource> mRemoteSourceProvider;

    @DexIgnore
    public WatchAppDataRepository_Factory(Provider<WatchAppDataDao> provider, Provider<WatchAppDataRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        this.mLocalSourceProvider = provider;
        this.mRemoteSourceProvider = provider2;
        this.mFileRepositoryProvider = provider3;
    }

    @DexIgnore
    public static WatchAppDataRepository_Factory create(Provider<WatchAppDataDao> provider, Provider<WatchAppDataRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        return new WatchAppDataRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchAppDataRepository newInstance(WatchAppDataDao watchAppDataDao, WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, FileRepository fileRepository) {
        return new WatchAppDataRepository(watchAppDataDao, watchAppDataRemoteDataSource, fileRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppDataRepository get() {
        return newInstance(this.mLocalSourceProvider.get(), this.mRemoteSourceProvider.get(), this.mFileRepositoryProvider.get());
    }
}
