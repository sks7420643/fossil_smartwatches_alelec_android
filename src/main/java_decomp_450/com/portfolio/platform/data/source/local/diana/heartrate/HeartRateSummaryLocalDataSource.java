package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.of;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.r87;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.w97;
import com.fossil.xh7;
import com.fossil.zd5;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource extends of<Date, DailyHeartRateSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver;
    @DexIgnore
    public List<r87<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = heartRateSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            ee7.b(date, "date");
            ee7.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = zd5.c(instance);
            if (zd5.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            ee7.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "HeartRateSummaryLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryLocalDataSource(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, FitnessDatabase fitnessDatabase, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(heartRateSummaryRepository, "mSummariesRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(fitnessDatabase, "mFitnessDatabase");
        ee7.b(date, "mCreatedDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        ee7.b(calendar, "key");
        this.mSummariesRepository = heartRateSummaryRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "daily_heart_rate_summary", new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = zd5.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        ee7.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (zd5.b(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> calculateSummaries(List<DailyHeartRateSummary> list) {
        int value;
        Resting resting;
        int i = 1;
        if (!list.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) ea7.f((List) list);
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "endCalendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int value2 = (instance.get(7) == 1 || (resting = dailyHeartRateSummary.getResting()) == null) ? 0 : resting.getValue();
            Calendar instance2 = Calendar.getInstance();
            ee7.a((Object) instance2, "calendar");
            instance2.setTime(((DailyHeartRateSummary) ea7.d((List) list)).getDate());
            Calendar s = zd5.s(instance2.getTime());
            ee7.a((Object) s, "DateHelper.getStartOfWeek(calendar.time)");
            s.add(5, -1);
            Calendar instance3 = Calendar.getInstance();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            for (T t : list) {
                int i6 = i5 + 1;
                if (i5 >= 0) {
                    T t2 = t;
                    ee7.a((Object) instance3, "mSummaryCalendar");
                    instance3.setTime(t2.getDate());
                    if (instance3.get(5) == s.get(5)) {
                        DailyHeartRateSummary dailyHeartRateSummary2 = list.get(i2);
                        if (i4 <= 0) {
                            i4 = 1;
                        }
                        dailyHeartRateSummary2.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i4));
                        s.add(5, -7);
                        i2 = i5;
                        i3 = 0;
                        i4 = 0;
                    }
                    Resting resting2 = t2.getResting();
                    if (resting2 != null && (value = resting2.getValue()) > 0) {
                        i3 += value;
                        i4++;
                    }
                    if (i5 == list.size() - 1 && value2 > 0) {
                        i3 += value2;
                        i4++;
                    }
                    i5 = i6;
                } else {
                    w97.c();
                    throw null;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary3 = list.get(i2);
            if (i4 > 0) {
                i = i4;
            }
            dailyHeartRateSummary3.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i));
        }
        return list;
    }

    @DexIgnore
    private final DailyHeartRateSummary dummySummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary = new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, date, System.currentTimeMillis(), System.currentTimeMillis(), 0, 0, 0, null);
        dailyHeartRateSummary.setCreatedAt(System.currentTimeMillis());
        dailyHeartRateSummary.setUpdatedAt(System.currentTimeMillis());
        return dailyHeartRateSummary;
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<DailyHeartRateSummary> calculateSummaries = calculateSummaries(this.mFitnessDatabase.getHeartRateDailySummaryDao().getDailyHeartRateSummariesDesc(zd5.b(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mFitnessDatabase.getHeartRateDailySummaryDao().getLastDate();
        if (lastDate == null) {
            lastDate = date;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        if (!zd5.b(date, lastDate)) {
            date = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date);
        while (zd5.c(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (zd5.d(((DailyHeartRateSummary) obj).getDate(), date2)) {
                    break;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
            if (dailyHeartRateSummary == null) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(dailyHeartRateSummary);
                arrayList2.remove(dailyHeartRateSummary);
            }
            date2 = zd5.p(date2);
            ee7.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary2 = (DailyHeartRateSummary) ea7.d((List) arrayList);
            Boolean w = zd5.w(dailyHeartRateSummary2.getDate());
            ee7.a((Object) w, "DateHelper.isToday(todaySummary.date)");
            if (w.booleanValue()) {
                arrayList.add(0, new DailyHeartRateSummary(dailyHeartRateSummary2));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final ik7 loadData(Date date, Date date2, te5.b.a aVar) {
        return xh7.b(zi7.a(qj7.b()), null, null, new HeartRateSummaryLocalDataSource$loadData$Anon1(this, date, date2, aVar, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadAfter(of.f<Date> fVar, of.a<Date, DailyHeartRateSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fVar.a));
        if (zd5.b(fVar.a, this.mCreatedDate)) {
            Key key2 = fVar.a;
            ee7.a((Object) key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fVar.a;
            ee7.a((Object) key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date o = zd5.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : zd5.o(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + o + ", endQueryDate=" + ((Object) key3));
            ee7.a((Object) o, "startQueryDate");
            aVar.a(getDataInDatabase(o, key3), calculateNextKey);
            if (zd5.b(this.mStartDate, (Date) key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new r87<>(this.mStartDate, this.mEndDate));
                this.mHelper.a(te5.d.AFTER, new HeartRateSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadBefore(of.f<Date> fVar, of.a<Date, DailyHeartRateSummary> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadInitial(of.e<Date> eVar, of.c<Date, DailyHeartRateSummary> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date o = zd5.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : zd5.o(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + o + ", endQueryDate=" + date);
        ee7.a((Object) o, "startQueryDate");
        cVar.a(getDataInDatabase(o, date), null, this.key.getTime());
        this.mHelper.a(te5.d.INITIAL, new HeartRateSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
