package com.portfolio.platform.data.source;

import com.fossil.qf7;
import com.fossil.t3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4<I, O> implements t3<X, Y> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    public SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3 anon1_Level3) {
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    public final ActivitySummary apply(ActivitySummary activitySummary) {
        if (activitySummary != null) {
            activitySummary.setSteps(qf7.a((double) this.this$0.this$0.this$0.this$0.mFitnessHelper.a(new Date()), activitySummary.getSteps()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("XXX- getSummary - onDataChange -- date=");
        sb.append(this.this$0.this$0.this$0.$date);
        sb.append(", summary=");
        sb.append(activitySummary);
        sb.append(" maxStep ");
        sb.append(activitySummary != null ? Double.valueOf(activitySummary.getSteps()) : null);
        local.d(SummariesRepository.TAG, sb.toString());
        return activitySummary;
    }
}
