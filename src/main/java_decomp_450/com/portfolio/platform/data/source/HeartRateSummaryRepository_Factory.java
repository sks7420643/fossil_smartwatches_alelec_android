package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository_Factory implements Factory<HeartRateSummaryRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public HeartRateSummaryRepository_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static HeartRateSummaryRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new HeartRateSummaryRepository_Factory(provider);
    }

    @DexIgnore
    public static HeartRateSummaryRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new HeartRateSummaryRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public HeartRateSummaryRepository get() {
        return newInstance(this.mApiServiceProvider.get());
    }
}
