package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.qj7;
import com.fossil.se7;
import com.fossil.xh7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4(GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onFail(int i) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onSuccess(List<GoalTrackingData> list) {
        ee7.b(list, "goalTrackingList");
        se7 se7 = new se7();
        se7.element = (T) list.get(0).getDate();
        se7 se72 = new se7();
        se72.element = (T) list.get(0).getDate();
        for (GoalTrackingData goalTrackingData : list) {
            if (goalTrackingData.getDate().getTime() < se7.element.getTime()) {
                se7.element = (T) goalTrackingData.getDate();
            }
            if (goalTrackingData.getDate().getTime() > se72.element.getTime()) {
                se72.element = (T) goalTrackingData.getDate();
            }
        }
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4$onSuccess$Anon1_Level2(this, se7, se72, null), 3, null);
    }
}
