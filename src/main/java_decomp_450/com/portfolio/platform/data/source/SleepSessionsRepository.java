package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingSleepSessionsCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MFSleepSession> list);
    }

    /*
    static {
        String simpleName = SleepSessionsRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "SleepSessionsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSessionsRepository(UserRepository userRepository, ApiServiceV2 apiServiceV2) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(apiServiceV2, "mApiService");
        this.mUserRepository = userRepository;
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchSleepSessions$default(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, fb7 fb7, int i3, Object obj) {
        return sleepSessionsRepository.fetchSleepSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, fb7);
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SleepSessionsRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRecommendedGoals(int r17, int r18, int r19, java.lang.String r20, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal>> r21) {
        /*
            r16 = this;
            r7 = r16
            r0 = r21
            boolean r1 = r0 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1 r1 = (com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1 r1 = new com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$Anon1
            r1.<init>(r7, r0)
        L_0x001c:
            r8 = r1
            java.lang.Object r0 = r8.result
            java.lang.Object r9 = com.fossil.nb7.a()
            int r1 = r8.label
            r10 = 1
            if (r1 == 0) goto L_0x0044
            if (r1 != r10) goto L_0x003c
            java.lang.Object r1 = r8.L$1
            java.lang.String r1 = (java.lang.String) r1
            int r1 = r8.I$2
            int r1 = r8.I$1
            int r1 = r8.I$0
            java.lang.Object r1 = r8.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r1 = (com.portfolio.platform.data.source.SleepSessionsRepository) r1
            com.fossil.t87.a(r0)
            goto L_0x0073
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.t87.a(r0)
            com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$Anon1 r11 = new com.portfolio.platform.data.source.SleepSessionsRepository$downloadRecommendedGoals$response$Anon1
            r6 = 0
            r0 = r11
            r1 = r16
            r2 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r8.L$0 = r7
            r0 = r17
            r8.I$0 = r0
            r0 = r18
            r8.I$1 = r0
            r0 = r19
            r8.I$2 = r0
            r0 = r20
            r8.L$1 = r0
            r8.label = r10
            java.lang.Object r0 = com.fossil.aj5.a(r11, r8)
            if (r0 != r9) goto L_0x0073
            return r9
        L_0x0073:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            boolean r1 = r0 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x0090
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            r1 = 0
            if (r0 == 0) goto L_0x008c
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r0 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r0
            com.fossil.bj5 r2 = new com.fossil.bj5
            r3 = 0
            r4 = 2
            r2.<init>(r0, r3, r4, r1)
            goto L_0x00aa
        L_0x008c:
            com.fossil.ee7.a()
            throw r1
        L_0x0090:
            boolean r1 = r0 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00ab
            com.fossil.yi5 r2 = new com.fossil.yi5
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r9 = r0.a()
            com.portfolio.platform.data.model.ServerError r10 = r0.c()
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 16
            r15 = 0
            r8 = r2
            r8.<init>(r9, r10, r11, r12, r13, r14, r15)
        L_0x00aa:
            return r2
        L_0x00ab:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.downloadRecommendedGoals(int, int, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object fetchSleepSessions(Date date, Date date2, int i, int i2, fb7<? super zi5<ie4>> fb7) {
        return vh7.a(qj7.b(), new SleepSessionsRepository$fetchSleepSessions$Anon2(this, date, date2, i, i2, null), fb7);
    }

    @DexIgnore
    public final Object getPendingSleepSessions(Date date, Date date2, fb7<? super List<MFSleepSession>> fb7) {
        return vh7.a(qj7.b(), new SleepSessionsRepository$getPendingSleepSessions$Anon2(date, date2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSleepSessionList(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r11 = (com.portfolio.platform.data.source.SleepSessionsRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2 r2 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r9, com.fossil.fb7<? super com.fossil.i97> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$insert$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.SleepSessionsRepository$insert$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$insert$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSessionsRepository$insert$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$insert$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r9 = r0.L$1
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r0 = (com.portfolio.platform.data.source.SleepSessionsRepository) r0
            com.fossil.t87.a(r10)
            goto L_0x0067
        L_0x0031:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0039:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insert: sleepSessionList = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r4 = r4.toString()
            r10.d(r2, r4)
            com.fossil.pg5 r10 = com.fossil.pg5.i
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = r10.d(r0)
            if (r10 != r1) goto L_0x0067
            return r1
        L_0x0067:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r10 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r10
            com.portfolio.platform.data.source.local.sleep.SleepDao r10 = r10.sleepDao()
            java.util.Iterator r9 = r9.iterator()
        L_0x0071:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0170
            java.lang.Object r0 = r9.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r0 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r0
            long r1 = r0.getDate()
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x0071
            int r1 = r0.getRealEndTime()
            long r1 = (long) r1
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r1 = r10.getSleepSession(r1)
            if (r1 == 0) goto L_0x0097
            org.joda.time.DateTime r2 = r1.getUpdatedAt()
            goto L_0x0098
        L_0x0097:
            r2 = 0
        L_0x0098:
            java.lang.String r3 = ", sleepDistribution="
            if (r2 == 0) goto L_0x0110
            org.joda.time.DateTime r1 = r1.getUpdatedAt()
            long r1 = r1.getMillis()
            org.joda.time.DateTime r4 = r0.getUpdatedAt()
            long r4 = r4.getMillis()
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 >= 0) goto L_0x0071
            int r1 = r0.getRealSleepMinutes()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedSleepMinutes(r1)
            int r1 = r0.getRealStartTime()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedStartTime(r1)
            int r1 = r0.getRealEndTime()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedEndTime(r1)
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r1 = r0.getRealSleepStateDistInMinute()
            r0.setEditedSleepStateDistInMinute(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Edit sleep session - date="
            r4.append(r5)
            java.util.Date r5 = new java.util.Date
            long r6 = r0.getDate()
            r5.<init>(r6)
            java.lang.String r5 = com.fossil.zd5.g(r5)
            r4.append(r5)
            r4.append(r3)
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r3 = r0.getEditedSleepStateDistInMinute()
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r1.d(r2, r3)
            r10.upsertSleepSession(r0)
            goto L_0x0071
        L_0x0110:
            int r1 = r0.getRealSleepMinutes()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedSleepMinutes(r1)
            int r1 = r0.getRealStartTime()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedStartTime(r1)
            int r1 = r0.getRealEndTime()
            java.lang.Integer r1 = com.fossil.pb7.a(r1)
            r0.setEditedEndTime(r1)
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r1 = r0.getRealSleepStateDistInMinute()
            r0.setEditedSleepStateDistInMinute(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Insert sleep session - date="
            r4.append(r5)
            java.util.Date r5 = new java.util.Date
            long r6 = r0.getDate()
            r5.<init>(r6)
            java.lang.String r5 = com.fossil.zd5.g(r5)
            r4.append(r5)
            r4.append(r3)
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r3 = r0.getEditedSleepStateDistInMinute()
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r1.d(r2, r3)
            r10.upsertSleepSession(r0)
            goto L_0x0071
        L_0x0170:
            com.fossil.i97 r9 = com.fossil.i97.a
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.insert(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<MFSleepSession> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SleepSessionsRepository$insertFromDevice$Anon2(this, list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insertSleepSessionList(java.lang.String r12, java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r13, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>>> r14) {
        /*
            r11 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$Anon1
            r0.<init>(r11, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x004c
            if (r2 != r3) goto L_0x0044
            java.lang.Object r12 = r0.L$5
            com.fossil.ie4 r12 = (com.fossil.ie4) r12
            java.lang.Object r12 = r0.L$4
            com.google.gson.Gson r12 = (com.google.gson.Gson) r12
            java.lang.Object r12 = r0.L$3
            com.fossil.de4 r12 = (com.fossil.de4) r12
            java.lang.Object r12 = r0.L$2
            r13 = r12
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r12 = r0.L$1
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r12 = r0.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r12 = (com.portfolio.platform.data.source.SleepSessionsRepository) r12
            com.fossil.t87.a(r14)
            goto L_0x0105
        L_0x0044:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x004c:
            com.fossil.t87.a(r14)
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "insertSleepSessionList sleepSessionList="
            r5.append(r6)
            r5.append(r13)
            java.lang.String r5 = r5.toString()
            r14.d(r2, r5)
            com.fossil.de4 r14 = new com.fossil.de4
            r14.<init>()
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            java.lang.Class<java.util.Date> r5 = java.util.Date.class
            com.portfolio.platform.helper.GsonISOConvertDateTime r6 = new com.portfolio.platform.helper.GsonISOConvertDateTime
            r6.<init>()
            r2.a(r5, r6)
            com.google.gson.Gson r2 = r2.a()
            java.util.Iterator r5 = r13.iterator()
        L_0x0087:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00c5
            java.lang.Object r6 = r5.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r6 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r6
            com.portfolio.platform.data.SleepSession r7 = new com.portfolio.platform.data.SleepSession     // Catch:{ Exception -> 0x00a2 }
            r7.<init>(r12, r6)     // Catch:{ Exception -> 0x00a2 }
            java.lang.Class<com.portfolio.platform.data.SleepSession> r6 = com.portfolio.platform.data.SleepSession.class
            com.google.gson.JsonElement r6 = r2.b(r7, r6)     // Catch:{ Exception -> 0x00a2 }
            r14.a(r6)     // Catch:{ Exception -> 0x00a2 }
            goto L_0x0087
        L_0x00a2:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "insertSleepSessionList exception="
            r9.append(r10)
            r6.printStackTrace()
            com.fossil.i97 r6 = com.fossil.i97.a
            r9.append(r6)
            java.lang.String r6 = r9.toString()
            r7.e(r8, r6)
            goto L_0x0087
        L_0x00c5:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "insertSleepSessionList jsonArray="
            r7.append(r8)
            r7.append(r14)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            com.fossil.ie4 r5 = new com.fossil.ie4
            r5.<init>()
            java.lang.String r6 = "_items"
            r5.a(r6, r14)
            com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 r6 = new com.portfolio.platform.data.source.SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1
            r6.<init>(r11, r5, r4)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.L$3 = r14
            r0.L$4 = r2
            r0.L$5 = r5
            r0.label = r3
            java.lang.Object r14 = com.fossil.aj5.a(r6, r0)
            if (r14 != r1) goto L_0x0105
            return r1
        L_0x0105:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r12 = r14 instanceof com.fossil.bj5
            if (r12 == 0) goto L_0x012f
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "insertSleepSession onResponse: response = "
            r1.append(r2)
            r1.append(r14)
            java.lang.String r14 = r1.toString()
            r12.d(r0, r14)
            com.fossil.bj5 r12 = new com.fossil.bj5
            r14 = 0
            r0 = 2
            r12.<init>(r13, r14, r0, r4)
            goto L_0x0181
        L_0x012f:
            boolean r12 = r14 instanceof com.fossil.yi5
            if (r12 == 0) goto L_0x0182
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "insertSleepSession Failure code="
            r0.append(r1)
            com.fossil.yi5 r14 = (com.fossil.yi5) r14
            int r1 = r14.a()
            r0.append(r1)
            java.lang.String r1 = " message="
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r14.c()
            if (r1 == 0) goto L_0x015d
            java.lang.String r4 = r1.getMessage()
        L_0x015d:
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r12.e(r13, r0)
            com.fossil.yi5 r12 = new com.fossil.yi5
            int r2 = r14.a()
            com.portfolio.platform.data.model.ServerError r3 = r14.c()
            java.lang.Throwable r4 = r14.d()
            java.lang.String r5 = r14.b()
            r6 = 0
            r7 = 16
            r8 = 0
            r1 = r12
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0181:
            return r12
        L_0x0182:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.insertSleepSessionList(java.lang.String, java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object isExistsSleepSession(com.portfolio.platform.data.model.room.sleep.MFSleepSession r11, com.fossil.fb7<? super java.lang.Boolean> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1 r0 = (com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$isExistsSleepSession$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r11 = r0.L$1
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r11 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r11
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r0 = (com.portfolio.platform.data.source.SleepSessionsRepository) r0
            com.fossil.t87.a(r12)
            goto L_0x004b
        L_0x0031:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0039:
            com.fossil.t87.a(r12)
            com.fossil.pg5 r12 = com.fossil.pg5.i
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r3
            java.lang.Object r12 = r12.d(r0)
            if (r12 != r1) goto L_0x004b
            return r1
        L_0x004b:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r12 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r12
            com.portfolio.platform.data.source.local.sleep.SleepDao r12 = r12.sleepDao()
            java.util.Date r0 = r11.getDay()
            long r0 = r0.getTime()
            java.util.List r12 = r12.getSleepSessions(r0)
            int r0 = r11.getStartTime()
            long r0 = (long) r0
            int r11 = r11.getEndTime()
            long r4 = (long) r11
            java.util.Iterator r11 = r12.iterator()
        L_0x006b:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x00a7
            java.lang.Object r12 = r11.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r12 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r12
            int r2 = r12.getStartTime()
            long r6 = (long) r2
            int r12 = r12.getEndTime()
            long r8 = (long) r12
            int r12 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r12 > 0) goto L_0x0089
            int r12 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r12 >= 0) goto L_0x00a2
        L_0x0089:
            int r12 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r12 > 0) goto L_0x0091
            int r12 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r12 >= 0) goto L_0x00a2
        L_0x0091:
            int r12 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r12 > 0) goto L_0x0099
            int r12 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r12 <= 0) goto L_0x00a2
        L_0x0099:
            int r12 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r12 <= 0) goto L_0x009e
            goto L_0x006b
        L_0x009e:
            int r12 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r12 < 0) goto L_0x006b
        L_0x00a2:
            java.lang.Boolean r11 = com.fossil.pb7.a(r3)
            return r11
        L_0x00a7:
            r11 = 0
            java.lang.Boolean r11 = com.fossil.pb7.a(r11)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.isExistsSleepSession(com.portfolio.platform.data.model.room.sleep.MFSleepSession, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingSleepSessions(PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, fb7<? super i97> fb7) {
        return vh7.a(qj7.b(), new SleepSessionsRepository$pushPendingSleepSessions$Anon2(this, pushPendingSleepSessionsCallback, null), fb7);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v6, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x027b, code lost:
        if (r16.intValue() != 409000) goto L_0x027d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0291, code lost:
        if (r7.intValue() != 409001) goto L_0x0293;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0327  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0334  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveSleepSessionsToServer(java.lang.String r20, java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession> r21, com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback r22, com.fossil.fb7<? super com.fossil.i97> r23) {
        /*
            r19 = this;
            r0 = r23
            boolean r1 = r0 instanceof com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$Anon1
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$Anon1 r1 = (com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            r2 = r19
            goto L_0x001e
        L_0x0017:
            com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$Anon1 r1 = new com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$Anon1
            r2 = r19
            r1.<init>(r2, r0)
        L_0x001e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r1.label
            r5 = 3
            r6 = 2
            java.lang.String r7 = " endIndex="
            r9 = 1
            if (r4 == 0) goto L_0x00be
            if (r4 == r9) goto L_0x0098
            if (r4 == r6) goto L_0x006e
            if (r4 != r5) goto L_0x0066
            java.lang.Object r4 = r1.L$8
            java.lang.reflect.Type r4 = (java.lang.reflect.Type) r4
            java.lang.Object r4 = r1.L$7
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r10 = r1.L$6
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            java.lang.Object r11 = r1.L$5
            java.util.List r11 = (java.util.List) r11
            int r12 = r1.I$1
            java.lang.Object r12 = r1.L$4
            java.util.List r12 = (java.util.List) r12
            int r13 = r1.I$0
            java.lang.Object r14 = r1.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r14 = (com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback) r14
            java.lang.Object r15 = r1.L$2
            java.util.List r15 = (java.util.List) r15
            java.lang.Object r5 = r1.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r6 = r1.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r6 = (com.portfolio.platform.data.source.SleepSessionsRepository) r6
            com.fossil.t87.a(r0)
            r17 = r7
            r8 = r13
            r13 = r5
            r5 = r3
            r3 = 3
            goto L_0x030f
        L_0x0066:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006e:
            java.lang.Object r4 = r1.L$7
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r5 = r1.L$6
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            java.lang.Object r5 = r1.L$5
            java.util.List r5 = (java.util.List) r5
            int r5 = r1.I$1
            java.lang.Object r5 = r1.L$4
            java.util.List r5 = (java.util.List) r5
            int r6 = r1.I$0
            java.lang.Object r10 = r1.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r10 = (com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback) r10
            java.lang.Object r11 = r1.L$2
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r12 = r1.L$1
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r13 = r1.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r13 = (com.portfolio.platform.data.source.SleepSessionsRepository) r13
            com.fossil.t87.a(r0)
            r9 = 2
            goto L_0x01af
        L_0x0098:
            java.lang.Object r4 = r1.L$5
            java.util.List r4 = (java.util.List) r4
            int r5 = r1.I$1
            java.lang.Object r6 = r1.L$4
            java.util.List r6 = (java.util.List) r6
            int r10 = r1.I$0
            java.lang.Object r11 = r1.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r11 = (com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback) r11
            java.lang.Object r12 = r1.L$2
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r13 = r1.L$1
            java.lang.String r13 = (java.lang.String) r13
            java.lang.Object r14 = r1.L$0
            com.portfolio.platform.data.source.SleepSessionsRepository r14 = (com.portfolio.platform.data.source.SleepSessionsRepository) r14
            com.fossil.t87.a(r0)
            r15 = r12
            r12 = r6
            r6 = r14
            r14 = r11
            r11 = r4
            goto L_0x013c
        L_0x00be:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.String r5 = "saveSleepSessionsToServer"
            r0.d(r4, r5)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4 = r0
            r5 = r1
            r11 = r2
            r6 = r3
            r10 = 0
            r0 = r20
            r1 = r21
            r3 = r22
        L_0x00de:
            int r12 = r1.size()
            if (r10 >= r12) goto L_0x0353
            int r12 = r10 + 100
            int r13 = r1.size()
            if (r12 <= r13) goto L_0x00f0
            int r12 = r1.size()
        L_0x00f0:
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r8 = "saveSleepSessionsToServer startIndex="
            r15.append(r8)
            r15.append(r10)
            r15.append(r7)
            r15.append(r12)
            java.lang.String r8 = r15.toString()
            r13.d(r14, r8)
            java.util.List r8 = r1.subList(r10, r12)
            r5.L$0 = r11
            r5.L$1 = r0
            r5.L$2 = r1
            r5.L$3 = r3
            r5.I$0 = r10
            r5.L$4 = r4
            r5.I$1 = r12
            r5.L$5 = r8
            r5.label = r9
            java.lang.Object r13 = r11.insertSleepSessionList(r0, r8, r5)
            if (r13 != r6) goto L_0x012f
            return r6
        L_0x012f:
            r15 = r1
            r14 = r3
            r1 = r5
            r3 = r6
            r6 = r11
            r5 = r12
            r12 = r4
            r11 = r8
            r18 = r13
            r13 = r0
            r0 = r18
        L_0x013c:
            r4 = r0
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            int r8 = r10 + 100
            boolean r0 = r4 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x01da
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r2 = "saveSleepSessionsToServer success, bravo!!! startIndex="
            r9.append(r2)
            r9.append(r8)
            r9.append(r7)
            r9.append(r5)
            java.lang.String r2 = r9.toString()
            r0.d(r10, r2)
            r0 = r4
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x01d5
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x0176:
            boolean r9 = r2.hasNext()
            if (r9 == 0) goto L_0x0187
            java.lang.Object r9 = r2.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r9 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r9
            r10 = 0
            r9.setPinType(r10)
            goto L_0x0176
        L_0x0187:
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r1.L$0 = r6
            r1.L$1 = r13
            r1.L$2 = r15
            r1.L$3 = r14
            r1.I$0 = r8
            r1.L$4 = r12
            r1.I$1 = r5
            r1.L$5 = r11
            r1.L$6 = r4
            r1.L$7 = r0
            r9 = 2
            r1.label = r9
            java.lang.Object r2 = r2.d(r1)
            if (r2 != r3) goto L_0x01a7
            return r3
        L_0x01a7:
            r4 = r0
            r0 = r2
            r5 = r12
            r12 = r13
            r10 = r14
            r11 = r15
            r13 = r6
            r6 = r8
        L_0x01af:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            r0.upsertSleepSessionList(r4)
            r5.addAll(r4)
            int r0 = r11.size()
            if (r6 < r0) goto L_0x01c8
            if (r10 == 0) goto L_0x0353
            r10.onSuccess(r5)
            goto L_0x0353
        L_0x01c8:
            r4 = r5
            r17 = r7
            r14 = r10
            r0 = r12
            r5 = r1
            r10 = r6
            r1 = r11
            r11 = r13
            r6 = r3
            r3 = 3
            goto L_0x034b
        L_0x01d5:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x01da:
            r9 = 2
            boolean r0 = r4 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0340
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r9 = "saveSleepSessionsToServer failed, errorCode="
            r10.append(r9)
            r9 = r4
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            r20 = r3
            int r3 = r9.a()
            r10.append(r3)
            r3 = 32
            r10.append(r3)
            java.lang.String r3 = "startIndex="
            r10.append(r3)
            r10.append(r8)
            r10.append(r7)
            r10.append(r5)
            java.lang.String r3 = r10.toString()
            r0.d(r2, r3)
            int r0 = r9.a()
            r2 = 422(0x1a6, float:5.91E-43)
            if (r0 != r2) goto L_0x0329
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.lang.String r0 = r9.b()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0329
            com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$type$Anon1 r0 = new com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$type$Anon1
            r0.<init>()
            java.lang.reflect.Type r3 = r0.getType()
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x02c0 }
            r0.<init>()     // Catch:{ Exception -> 0x02c0 }
            r9 = r4
            com.fossil.yi5 r9 = (com.fossil.yi5) r9     // Catch:{ Exception -> 0x02c0 }
            java.lang.String r9 = r9.b()     // Catch:{ Exception -> 0x02c0 }
            java.lang.Object r0 = r0.a(r9, r3)     // Catch:{ Exception -> 0x02c0 }
            java.lang.String r9 = "Gson().fromJson(repoResponse.errorItems, type)"
            com.fossil.ee7.a(r0, r9)     // Catch:{ Exception -> 0x02c0 }
            com.portfolio.platform.data.source.remote.UpsertApiResponse r0 = (com.portfolio.platform.data.source.remote.UpsertApiResponse) r0     // Catch:{ Exception -> 0x02c0 }
            java.util.List r0 = r0.get_items()     // Catch:{ Exception -> 0x02c0 }
            boolean r9 = r0.isEmpty()     // Catch:{ Exception -> 0x02c0 }
            r10 = 1
            r9 = r9 ^ r10
            if (r9 == 0) goto L_0x02ba
            int r9 = r0.size()     // Catch:{ Exception -> 0x02c0 }
            r10 = 0
        L_0x025f:
            if (r10 >= r9) goto L_0x02ba
            java.lang.Object r16 = r0.get(r10)     // Catch:{ Exception -> 0x02c0 }
            com.portfolio.platform.data.SleepSession r16 = (com.portfolio.platform.data.SleepSession) r16     // Catch:{ Exception -> 0x02c0 }
            java.lang.Integer r16 = r16.getCode()     // Catch:{ Exception -> 0x02c0 }
            r17 = r7
            r7 = 409000(0x63da8, float:5.73131E-40)
            if (r16 != 0) goto L_0x0275
            r21 = r9
            goto L_0x027d
        L_0x0275:
            r21 = r9
            int r9 = r16.intValue()     // Catch:{ Exception -> 0x02b8 }
            if (r9 == r7) goto L_0x02a2
        L_0x027d:
            java.lang.Object r7 = r0.get(r10)     // Catch:{ Exception -> 0x02b8 }
            com.portfolio.platform.data.SleepSession r7 = (com.portfolio.platform.data.SleepSession) r7     // Catch:{ Exception -> 0x02b8 }
            java.lang.Integer r7 = r7.getCode()     // Catch:{ Exception -> 0x02b8 }
            r9 = 409001(0x63da9, float:5.73132E-40)
            if (r7 != 0) goto L_0x028d
            goto L_0x0293
        L_0x028d:
            int r7 = r7.intValue()     // Catch:{ Exception -> 0x02b8 }
            if (r7 == r9) goto L_0x02a2
        L_0x0293:
            java.lang.Object r7 = r0.get(r10)     // Catch:{ Exception -> 0x02b8 }
            com.portfolio.platform.data.SleepSession r7 = (com.portfolio.platform.data.SleepSession) r7     // Catch:{ Exception -> 0x02b8 }
            java.util.Date r7 = r7.getRealEndTime()     // Catch:{ Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x02a0
            goto L_0x02a2
        L_0x02a0:
            r9 = 0
            goto L_0x02af
        L_0x02a2:
            java.lang.Object r7 = r11.get(r10)     // Catch:{ Exception -> 0x02b8 }
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r7 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r7     // Catch:{ Exception -> 0x02b8 }
            r9 = 0
            r7.setPinType(r9)     // Catch:{ Exception -> 0x02b6 }
            r2.add(r7)     // Catch:{ Exception -> 0x02b6 }
        L_0x02af:
            int r10 = r10 + 1
            r9 = r21
            r7 = r17
            goto L_0x025f
        L_0x02b6:
            r0 = move-exception
            goto L_0x02c4
        L_0x02b8:
            r0 = move-exception
            goto L_0x02c3
        L_0x02ba:
            r17 = r7
            r9 = 0
            r21 = r3
            goto L_0x02e7
        L_0x02c0:
            r0 = move-exception
            r17 = r7
        L_0x02c3:
            r9 = 0
        L_0x02c4:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r10 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r21 = r3
            java.lang.String r3 = "saveSleepSessionsToServer ex="
            r9.append(r3)
            r0.printStackTrace()
            com.fossil.i97 r0 = com.fossil.i97.a
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            r7.e(r10, r0)
        L_0x02e7:
            com.fossil.pg5 r0 = com.fossil.pg5.i
            r1.L$0 = r6
            r1.L$1 = r13
            r1.L$2 = r15
            r1.L$3 = r14
            r1.I$0 = r8
            r1.L$4 = r12
            r1.I$1 = r5
            r1.L$5 = r11
            r1.L$6 = r4
            r1.L$7 = r2
            r3 = r21
            r1.L$8 = r3
            r3 = 3
            r1.label = r3
            java.lang.Object r0 = r0.d(r1)
            r5 = r20
            if (r0 != r5) goto L_0x030d
            return r5
        L_0x030d:
            r10 = r4
            r4 = r2
        L_0x030f:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            r0.upsertSleepSessionList(r4)
            r12.addAll(r11)
            int r0 = r15.size()
            if (r8 < r0) goto L_0x0327
            if (r14 == 0) goto L_0x0353
            r14.onSuccess(r12)
            goto L_0x0353
        L_0x0327:
            r4 = r10
            goto L_0x032e
        L_0x0329:
            r5 = r20
            r17 = r7
            r3 = 3
        L_0x032e:
            int r0 = r15.size()
            if (r8 < r0) goto L_0x0344
            if (r14 == 0) goto L_0x0353
            com.fossil.yi5 r4 = (com.fossil.yi5) r4
            int r0 = r4.a()
            r14.onFail(r0)
            goto L_0x0353
        L_0x0340:
            r5 = r3
            r17 = r7
            r3 = 3
        L_0x0344:
            r11 = r6
            r10 = r8
            r4 = r12
            r0 = r13
            r6 = r5
            r5 = r1
            r1 = r15
        L_0x034b:
            r9 = 1
            r2 = r19
            r3 = r14
            r7 = r17
            goto L_0x00de
        L_0x0353:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.saveSleepSessionsToServer(java.lang.String, java.util.List, com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object upsertRecommendedGoals(SleepRecommendedGoal sleepRecommendedGoal, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new SleepSessionsRepository$upsertRecommendedGoals$Anon2(sleepRecommendedGoal, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }
}
