package com.portfolio.platform.data.source.local.quickresponse;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseDatabase_Impl extends QuickResponseDatabase {
    @DexIgnore
    public volatile QuickResponseMessageDao _quickResponseMessageDao;
    @DexIgnore
    public volatile QuickResponseSenderDao _quickResponseSenderDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `quickResponseMessage` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `response` TEXT NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `quickResponseSender` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `content` TEXT NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'e3a41f0570eccd0330bf7f76279c817f')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `quickResponseMessage`");
            wiVar.execSQL("DROP TABLE IF EXISTS `quickResponseSender`");
            if (((ci) QuickResponseDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) QuickResponseDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) QuickResponseDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) QuickResponseDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) QuickResponseDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) QuickResponseDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) QuickResponseDatabase_Impl.this).mDatabase = wiVar;
            QuickResponseDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) QuickResponseDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) QuickResponseDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) QuickResponseDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("response", new ti.a("response", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("quickResponseMessage", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "quickResponseMessage");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "quickResponseMessage(com.portfolio.platform.data.model.QuickResponseMessage).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap2.put("content", new ti.a("content", "TEXT", true, 0, null, 1));
            ti tiVar2 = new ti("quickResponseSender", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "quickResponseSender");
            if (tiVar2.equals(a2)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "quickResponseSender(com.portfolio.platform.data.model.QuickResponseSender).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `quickResponseMessage`");
            writableDatabase.execSQL("DELETE FROM `quickResponseSender`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "quickResponseMessage", "quickResponseSender");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "e3a41f0570eccd0330bf7f76279c817f", "eb9908832be312d2d3b3e7c35c7e6720");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase
    public QuickResponseMessageDao quickResponseMessageDao() {
        QuickResponseMessageDao quickResponseMessageDao;
        if (this._quickResponseMessageDao != null) {
            return this._quickResponseMessageDao;
        }
        synchronized (this) {
            if (this._quickResponseMessageDao == null) {
                this._quickResponseMessageDao = new QuickResponseMessageDao_Impl(this);
            }
            quickResponseMessageDao = this._quickResponseMessageDao;
        }
        return quickResponseMessageDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase
    public QuickResponseSenderDao quickResponseSenderDao() {
        QuickResponseSenderDao quickResponseSenderDao;
        if (this._quickResponseSenderDao != null) {
            return this._quickResponseSenderDao;
        }
        synchronized (this) {
            if (this._quickResponseSenderDao == null) {
                this._quickResponseSenderDao = new QuickResponseSenderDao_Impl(this);
            }
            quickResponseSenderDao = this._quickResponseSenderDao;
        }
        return quickResponseSenderDao;
    }
}
