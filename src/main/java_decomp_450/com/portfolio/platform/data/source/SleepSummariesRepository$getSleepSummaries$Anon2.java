package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$2", f = "SleepSummariesRepository.kt", l = {180, 181}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<MFSleepDay>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ String $end;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ String $start;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<MFSleepDay>, ie4> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ie4>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiService$p.getSleepSummaries(g, g2, 0, 100, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<List<MFSleepDay>>> fb7) {
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                String str = this.this$0.$start;
                ee7.a((Object) str, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                String str2 = this.this$0.$end;
                ee7.a((Object) str2, "end");
                return sleepDao.getSleepDaysLiveData(str, str2);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(SleepSummariesRepository.TAG, "getActivityList onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ie4 ie4, fb7 fb7) {
                return saveCallResult(ie4, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object saveCallResult(ie4 ie4, fb7<? super i97> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = SleepSummariesRepository.TAG;
                local.d(access$getTAG$cp, "getSleepSummaries saveCallResult onResponse: response = " + ie4);
                SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0.this$0;
                Object saveSleepSummaries = sleepSummariesRepository$getSleepSummaries$Anon2.this$0.saveSleepSummaries(ie4, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate, this.$downloadingDate, fb7);
                if (saveSleepSummaries == nb7.a()) {
                    return saveSleepSummaries;
                }
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepDay> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2, SleepDatabase sleepDatabase, String str, String str2) {
            this.this$0 = sleepSummariesRepository$getSleepSummaries$Anon2;
            this.$sleepDatabase = sleepDatabase;
            this.$start = str;
            this.$end = str2;
        }

        @DexIgnore
        public final LiveData<qx6<List<MFSleepDay>>> apply(List<FitnessDataWrapper> list) {
            ee7.a((Object) list, "fitnessDataList");
            SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummaries$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = new SleepSummariesRepository$getSleepSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, fb7);
        sleepSummariesRepository$getSleepSummaries$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$getSleepSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<MFSleepDay>>>> fb7) {
        return ((SleepSummariesRepository$getSleepSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        String str;
        String str2;
        FitnessDatabase fitnessDatabase;
        yi7 yi7;
        String str3;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = SleepSummariesRepository.TAG;
            local.d(access$getTAG$cp, "getSleepSummaries: startDate = " + this.$startDate + ", endDate = " + this.$endDate);
            String g = zd5.g(this.$startDate);
            str2 = zd5.g(this.$endDate);
            ti7 b = qj7.b();
            SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2 sleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.L$1 = g;
            this.L$2 = str2;
            this.label = 1;
            Object a2 = vh7.a(b, sleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (a2 == a) {
                return a;
            }
            str3 = g;
            obj = a2;
        } else if (i == 1) {
            str2 = (String) this.L$2;
            str3 = (String) this.L$1;
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            fitnessDatabase = (FitnessDatabase) this.L$3;
            str2 = (String) this.L$2;
            str = (String) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, (SleepDatabase) obj, str, str2));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase2 = (FitnessDatabase) obj;
        ti7 b2 = qj7.b();
        SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2 sleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2 = new SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2(null);
        this.L$0 = yi7;
        this.L$1 = str3;
        this.L$2 = str2;
        this.L$3 = fitnessDatabase2;
        this.label = 2;
        Object a3 = vh7.a(b2, sleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2, this);
        if (a3 == a) {
            return a;
        }
        fitnessDatabase = fitnessDatabase2;
        obj = a3;
        str = str3;
        return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, (SleepDatabase) obj, str, str2));
    }
}
