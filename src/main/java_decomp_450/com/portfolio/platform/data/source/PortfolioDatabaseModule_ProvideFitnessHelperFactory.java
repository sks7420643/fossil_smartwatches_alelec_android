package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.ch5;
import com.fossil.ge5;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<ge5> {
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<ch5> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ch5> provider) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ch5> provider) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ge5 provideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, ch5 ch5) {
        ge5 provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(ch5);
        c87.a(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ge5 get() {
        return provideFitnessHelper(this.module, this.sharedPreferencesManagerProvider.get());
    }
}
