package com.portfolio.platform.data.source.local;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDatabase_Impl extends AddressDatabase {
    @DexIgnore
    public volatile AddressDao _addressDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `addressOfWeather` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `lat` REAL NOT NULL, `lng` REAL NOT NULL, `address` TEXT NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'a0a1d2ac0b494c20d42ebbc7bcd2b1a6')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `addressOfWeather`");
            if (((ci) AddressDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AddressDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AddressDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) AddressDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AddressDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AddressDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) AddressDatabase_Impl.this).mDatabase = wiVar;
            AddressDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) AddressDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AddressDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AddressDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put(Constants.LAT, new ti.a(Constants.LAT, "REAL", true, 0, null, 1));
            hashMap.put("lng", new ti.a("lng", "REAL", true, 0, null, 1));
            hashMap.put("address", new ti.a("address", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("addressOfWeather", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "addressOfWeather");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "addressOfWeather(com.portfolio.platform.data.model.microapp.weather.AddressOfWeather).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDatabase
    public AddressDao addressDao() {
        AddressDao addressDao;
        if (this._addressDao != null) {
            return this._addressDao;
        }
        synchronized (this) {
            if (this._addressDao == null) {
                this._addressDao = new AddressDao_Impl(this);
            }
            addressDao = this._addressDao;
        }
        return addressDao;
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `addressOfWeather`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "addressOfWeather");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "a0a1d2ac0b494c20d42ebbc7bcd2b1a6", "22c420623f1ce32408e58d9756808f61");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }
}
