package com.portfolio.platform.data.source.local.workoutsetting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ac5;
import com.fossil.c;
import com.fossil.ee7;
import com.fossil.ub5;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSetting implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new Creator();
    @DexIgnore
    public boolean askMeFirst;
    @DexIgnore
    public /* final */ long createdAt;
    @DexIgnore
    public boolean enable;
    @DexIgnore
    public /* final */ ub5 mode;
    @DexIgnore
    public /* final */ int pauseLatency;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public /* final */ int resumeLatency;
    @DexIgnore
    public /* final */ int startLatency;
    @DexIgnore
    public /* final */ int stopLatency;
    @DexIgnore
    public /* final */ ac5 type;
    @DexIgnore
    public /* final */ long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Creator implements Parcelable.Creator {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object createFromParcel(Parcel parcel) {
            ee7.b(parcel, "in");
            ac5 ac5 = (ac5) Enum.valueOf(ac5.class, parcel.readString());
            ub5 ub5 = parcel.readInt() != 0 ? (ub5) Enum.valueOf(ub5.class, parcel.readString()) : null;
            boolean z = true;
            boolean z2 = parcel.readInt() != 0;
            if (parcel.readInt() == 0) {
                z = false;
            }
            return new WorkoutSetting(ac5, ub5, z2, z, parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readLong(), parcel.readLong(), parcel.readInt());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object[] newArray(int i) {
            return new WorkoutSetting[i];
        }
    }

    @DexIgnore
    public WorkoutSetting(ac5 ac5, ub5 ub5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5) {
        ee7.b(ac5, "type");
        this.type = ac5;
        this.mode = ub5;
        this.enable = z;
        this.askMeFirst = z2;
        this.startLatency = i;
        this.pauseLatency = i2;
        this.resumeLatency = i3;
        this.stopLatency = i4;
        this.createdAt = j;
        this.updatedAt = j2;
        this.pinType = i5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSetting copy$default(WorkoutSetting workoutSetting, ac5 ac5, ub5 ub5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5, int i6, Object obj) {
        return workoutSetting.copy((i6 & 1) != 0 ? workoutSetting.type : ac5, (i6 & 2) != 0 ? workoutSetting.mode : ub5, (i6 & 4) != 0 ? workoutSetting.enable : z, (i6 & 8) != 0 ? workoutSetting.askMeFirst : z2, (i6 & 16) != 0 ? workoutSetting.startLatency : i, (i6 & 32) != 0 ? workoutSetting.pauseLatency : i2, (i6 & 64) != 0 ? workoutSetting.resumeLatency : i3, (i6 & 128) != 0 ? workoutSetting.stopLatency : i4, (i6 & 256) != 0 ? workoutSetting.createdAt : j, (i6 & 512) != 0 ? workoutSetting.updatedAt : j2, (i6 & 1024) != 0 ? workoutSetting.pinType : i5);
    }

    @DexIgnore
    public final ac5 component1() {
        return this.type;
    }

    @DexIgnore
    public final long component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component11() {
        return this.pinType;
    }

    @DexIgnore
    public final ub5 component2() {
        return this.mode;
    }

    @DexIgnore
    public final boolean component3() {
        return this.enable;
    }

    @DexIgnore
    public final boolean component4() {
        return this.askMeFirst;
    }

    @DexIgnore
    public final int component5() {
        return this.startLatency;
    }

    @DexIgnore
    public final int component6() {
        return this.pauseLatency;
    }

    @DexIgnore
    public final int component7() {
        return this.resumeLatency;
    }

    @DexIgnore
    public final int component8() {
        return this.stopLatency;
    }

    @DexIgnore
    public final long component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final WorkoutSetting copy(ac5 ac5, ub5 ub5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5) {
        ee7.b(ac5, "type");
        return new WorkoutSetting(ac5, ub5, z, z2, i, i2, i3, i4, j, j2, i5);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSetting)) {
            return false;
        }
        WorkoutSetting workoutSetting = (WorkoutSetting) obj;
        return ee7.a(this.type, workoutSetting.type) && ee7.a(this.mode, workoutSetting.mode) && this.enable == workoutSetting.enable && this.askMeFirst == workoutSetting.askMeFirst && this.startLatency == workoutSetting.startLatency && this.pauseLatency == workoutSetting.pauseLatency && this.resumeLatency == workoutSetting.resumeLatency && this.stopLatency == workoutSetting.stopLatency && this.createdAt == workoutSetting.createdAt && this.updatedAt == workoutSetting.updatedAt && this.pinType == workoutSetting.pinType;
    }

    @DexIgnore
    public final boolean getAskMeFirst() {
        return this.askMeFirst;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final boolean getEnable() {
        return this.enable;
    }

    @DexIgnore
    public final ub5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final int getPauseLatency() {
        return this.pauseLatency;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getResumeLatency() {
        return this.resumeLatency;
    }

    @DexIgnore
    public final int getStartLatency() {
        return this.startLatency;
    }

    @DexIgnore
    public final int getStopLatency() {
        return this.stopLatency;
    }

    @DexIgnore
    public final ac5 getType() {
        return this.type;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r2v3, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        ac5 ac5 = this.type;
        int i = 0;
        int hashCode = (ac5 != null ? ac5.hashCode() : 0) * 31;
        ub5 ub5 = this.mode;
        if (ub5 != null) {
            i = ub5.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z = this.enable;
        int i3 = 1;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z2 = this.askMeFirst;
        if (z2 == 0) {
            i3 = z2;
        }
        return ((((((((((((((i6 + i3) * 31) + this.startLatency) * 31) + this.pauseLatency) * 31) + this.resumeLatency) * 31) + this.stopLatency) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt)) * 31) + this.pinType;
    }

    @DexIgnore
    public final void setAskMeFirst(boolean z) {
        this.askMeFirst = z;
    }

    @DexIgnore
    public final void setEnable(boolean z) {
        this.enable = z;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSetting(type=" + this.type + ", mode=" + this.mode + ", enable=" + this.enable + ", askMeFirst=" + this.askMeFirst + ", startLatency=" + this.startLatency + ", pauseLatency=" + this.pauseLatency + ", resumeLatency=" + this.resumeLatency + ", stopLatency=" + this.stopLatency + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.type.name());
        ub5 ub5 = this.mode;
        if (ub5 != null) {
            parcel.writeInt(1);
            parcel.writeString(ub5.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.enable ? 1 : 0);
        parcel.writeInt(this.askMeFirst ? 1 : 0);
        parcel.writeInt(this.startLatency);
        parcel.writeInt(this.pauseLatency);
        parcel.writeInt(this.resumeLatency);
        parcel.writeInt(this.stopLatency);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutSetting(ac5 ac5, ub5 ub5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5, int i6, zd7 zd7) {
        this(ac5, (i6 & 2) != 0 ? null : ub5, z, z2, i, i2, i3, i4, j, j2, (i6 & 1024) != 0 ? 1 : i5);
    }
}
