package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.dp4;
import com.fossil.go4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory implements Factory<dp4> {
    @DexIgnore
    public /* final */ Provider<go4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<go4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<go4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static dp4 providesSocialProfileLocal(PortfolioDatabaseModule portfolioDatabaseModule, go4 go4) {
        dp4 providesSocialProfileLocal = portfolioDatabaseModule.providesSocialProfileLocal(go4);
        c87.a(providesSocialProfileLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialProfileLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public dp4 get() {
        return providesSocialProfileLocal(this.module, this.daoProvider.get());
    }
}
