package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppDataRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllWatchAppData(java.lang.String r9, java.lang.String r10, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.diana.WatchAppData>>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$Anon1 r0 = (com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$Anon1 r0 = new com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$Anon1
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource) r9
            com.fossil.t87.a(r11)
            goto L_0x0055
        L_0x0035:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003d:
            com.fossil.t87.a(r11)
            com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1 r11 = new com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource$getAllWatchAppData$response$Anon1
            r2 = 0
            r11.<init>(r8, r9, r10, r2)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r11, r0)
            if (r11 != r1) goto L_0x0055
            return r1
        L_0x0055:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0081
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r10 = r11.a()
            com.portfolio.platform.data.source.remote.ApiResponse r10 = (com.portfolio.platform.data.source.remote.ApiResponse) r10
            if (r10 == 0) goto L_0x0077
            java.util.List r10 = r10.get_items()
            if (r10 == 0) goto L_0x0077
            boolean r10 = r9.addAll(r10)
            com.fossil.pb7.a(r10)
        L_0x0077:
            com.fossil.bj5 r10 = new com.fossil.bj5
            boolean r11 = r11.b()
            r10.<init>(r9, r11)
            goto L_0x009e
        L_0x0081:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009f
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r1 = r11.a()
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x009e:
            return r10
        L_0x009f:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource.getAllWatchAppData(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
