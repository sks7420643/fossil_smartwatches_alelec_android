package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WorkoutSettingRepository$downloadWorkoutSettings$2", f = "WorkoutSettingRepository.kt", l = {38, 40, 47}, m = "invokeSuspend")
public final class WorkoutSettingRepository$downloadWorkoutSettings$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSettingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSettingRepository$downloadWorkoutSettings$Anon2(WorkoutSettingRepository workoutSettingRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSettingRepository$downloadWorkoutSettings$Anon2 workoutSettingRepository$downloadWorkoutSettings$Anon2 = new WorkoutSettingRepository$downloadWorkoutSettings$Anon2(this.this$0, fb7);
        workoutSettingRepository$downloadWorkoutSettings$Anon2.p$ = (yi7) obj;
        return workoutSettingRepository$downloadWorkoutSettings$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((WorkoutSettingRepository$downloadWorkoutSettings$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f0  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r9.label
            r2 = 3
            r3 = 2
            r4 = 1
            java.lang.String r5 = "WorkoutSettingRepository"
            if (r1 == 0) goto L_0x0038
            if (r1 == r4) goto L_0x0030
            if (r1 == r3) goto L_0x0028
            if (r1 != r2) goto L_0x0020
            java.lang.Object r0 = r9.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x00d2
        L_0x0020:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0028:
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x0067
        L_0x0030:
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x0056
        L_0x0038:
            com.fossil.t87.a(r10)
            com.fossil.yi7 r10 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r6 = "downloadWorkoutSetting()"
            r1.d(r5, r6)
            com.portfolio.platform.data.source.WorkoutSettingRepository r1 = r9.this$0
            r9.L$0 = r10
            r9.label = r4
            java.lang.Object r1 = r1.executePendingRequest(r9)
            if (r1 != r0) goto L_0x0055
            return r0
        L_0x0055:
            r1 = r10
        L_0x0056:
            com.portfolio.platform.data.source.WorkoutSettingRepository r10 = r9.this$0
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource r10 = r10.mWorkoutSettingRemoteDataSource
            r9.L$0 = r1
            r9.label = r3
            java.lang.Object r10 = r10.downloadWorkoutSettingList(r9)
            if (r10 != r0) goto L_0x0067
            return r0
        L_0x0067:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r3 = r10 instanceof com.fossil.bj5
            if (r3 == 0) goto L_0x00f0
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "downloadWorkoutSetting() - success - isFromCache "
            r6.append(r7)
            r7 = r10
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            boolean r8 = r7.b()
            r6.append(r8)
            java.lang.String r6 = r6.toString()
            r3.d(r5, r6)
            boolean r3 = r7.b()
            if (r3 != 0) goto L_0x00e4
            java.lang.Object r3 = r7.a()
            java.util.Collection r3 = (java.util.Collection) r3
            r6 = 0
            if (r3 == 0) goto L_0x00a5
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x00a4
            goto L_0x00a5
        L_0x00a4:
            r4 = 0
        L_0x00a5:
            if (r4 != 0) goto L_0x00e4
            java.lang.Object r3 = r7.a()
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            java.util.Iterator r3 = r3.iterator()
        L_0x00b1:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x00c1
            java.lang.Object r4 = r3.next()
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting r4 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting) r4
            r4.setPinType(r6)
            goto L_0x00b1
        L_0x00c1:
            com.fossil.pg5 r3 = com.fossil.pg5.i
            r9.L$0 = r1
            r9.L$1 = r10
            r9.label = r2
            java.lang.Object r1 = r3.g(r9)
            if (r1 != r0) goto L_0x00d0
            return r0
        L_0x00d0:
            r0 = r10
            r10 = r1
        L_0x00d2:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r10 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase) r10
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao r10 = r10.workoutSettingDao()
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            java.util.List r0 = (java.util.List) r0
            r10.upsertWorkoutSettingList(r0)
            goto L_0x0114
        L_0x00e4:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r0 = "downloadWorkoutSetting() - empty list"
            r10.d(r5, r0)
            goto L_0x0114
        L_0x00f0:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0114
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadWorkoutSetting() fail!! "
            r1.append(r2)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r10 = r10.a()
            r1.append(r10)
            java.lang.String r10 = r1.toString()
            r0.d(r5, r10)
        L_0x0114:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository$downloadWorkoutSettings$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
