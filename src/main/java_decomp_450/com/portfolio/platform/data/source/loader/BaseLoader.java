package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.fossil.ee7;
import com.fossil.me;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseLoader<T> extends me<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return BaseLoader.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = BaseLoader.class.getSimpleName();
        ee7.a((Object) simpleName, "BaseLoader::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseLoader(Context context) {
        super(context);
        if (context != null) {
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void deliverResult(T t) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .deliverResult data=" + ((Object) t) + ", isReset=" + isReset() + ". isStarted=" + isStarted());
        if (!isReset() && isStarted()) {
            super.deliverResult(t);
        }
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onReset() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onReset");
        onStopLoading();
        super.onReset();
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onStopLoading() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .onStopLoading");
        cancelLoad();
        super.onStopLoading();
    }
}
