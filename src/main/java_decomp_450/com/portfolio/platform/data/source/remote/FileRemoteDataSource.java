package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "FileRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public FileRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mService");
        this.mService = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadFileToStorage(java.lang.String r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.lang.Object>> r12) {
        /*
            r9 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.remote.FileRemoteDataSource$downloadFileToStorage$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.remote.FileRemoteDataSource$downloadFileToStorage$Anon1 r0 = (com.portfolio.platform.data.source.remote.FileRemoteDataSource$downloadFileToStorage$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.FileRemoteDataSource$downloadFileToStorage$Anon1 r0 = new com.portfolio.platform.data.source.remote.FileRemoteDataSource$downloadFileToStorage$Anon1
            r0.<init>(r9, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0045
            if (r2 != r3) goto L_0x003d
            java.lang.Object r10 = r0.L$4
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r11 = r0.L$3
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$2
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.remote.FileRemoteDataSource r11 = (com.portfolio.platform.data.source.remote.FileRemoteDataSource) r11
            com.fossil.t87.a(r12)
            goto L_0x0076
        L_0x003d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0045:
            com.fossil.t87.a(r12)
            java.lang.String r12 = com.fossil.mg5.a(r10)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r11)
            java.lang.String r4 = java.io.File.separator
            r2.append(r4)
            r2.append(r12)
            java.lang.String r2 = r2.toString()
            com.portfolio.platform.data.source.remote.ApiServiceV2 r4 = r9.mService
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.L$4 = r2
            r0.label = r3
            java.lang.Object r12 = r4.downloadFile(r10, r0)
            if (r12 != r1) goto L_0x0075
            return r1
        L_0x0075:
            r10 = r2
        L_0x0076:
            com.fossil.fv7 r12 = (com.fossil.fv7) r12
            boolean r11 = r12.d()
            java.lang.String r0 = "FileRemoteDataSource"
            if (r11 == 0) goto L_0x00ee
            java.lang.Object r11 = r12.a()
            if (r11 == 0) goto L_0x00d3
            com.fossil.ee5 r11 = com.fossil.ee5.a
            java.lang.Object r12 = r12.a()
            r1 = 0
            if (r12 == 0) goto L_0x00cf
            java.lang.String r2 = "response.body()!!"
            com.fossil.ee7.a(r12, r2)
            com.fossil.mo7 r12 = (com.fossil.mo7) r12
            boolean r10 = r11.a(r12, r10)
            if (r10 == 0) goto L_0x00b4
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = "saveRemoteFile() success"
            r10.e(r0, r11)
            com.fossil.bj5 r10 = new com.fossil.bj5
            java.lang.Object r11 = new java.lang.Object
            r11.<init>()
            r12 = 0
            r0 = 2
            r10.<init>(r11, r12, r0, r1)
            goto L_0x0108
        L_0x00b4:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = "saveRemoteFile() fail do to write file to local storage"
            r10.e(r0, r11)
            com.fossil.yi5 r10 = new com.fossil.yi5
            r2 = 500(0x1f4, float:7.0E-43)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 28
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0108
        L_0x00cf:
            com.fossil.ee7.a()
            throw r1
        L_0x00d3:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = "saveRemoteFile() fail due to empty content"
            r10.e(r0, r11)
            com.fossil.yi5 r10 = new com.fossil.yi5
            r2 = 204(0xcc, float:2.86E-43)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 28
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0108
        L_0x00ee:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = "saveRemoteFile() fail"
            r10.e(r0, r11)
            com.fossil.yi5 r10 = new com.fossil.yi5
            r2 = 500(0x1f4, float:7.0E-43)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 28
            r8 = 0
            r1 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0108:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.FileRemoteDataSource.downloadFileToStorage(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
