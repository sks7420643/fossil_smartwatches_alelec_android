package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.nn4;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesChallengeDaoFactory implements Factory<nn4> {
    @DexIgnore
    public /* final */ Provider<BuddyChallengeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesChallengeDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesChallengeDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesChallengeDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static nn4 providesChallengeDao(PortfolioDatabaseModule portfolioDatabaseModule, BuddyChallengeDatabase buddyChallengeDatabase) {
        nn4 providesChallengeDao = portfolioDatabaseModule.providesChallengeDao(buddyChallengeDatabase);
        c87.a(providesChallengeDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesChallengeDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nn4 get() {
        return providesChallengeDao(this.module, this.dbProvider.get());
    }
}
