package com.portfolio.platform.data.source;

import com.fossil.ai7;
import com.fossil.ee7;
import com.fossil.qg5;
import com.fossil.s87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.model.LocalFile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$downloadFromUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1 implements qg5.c {
    @DexIgnore
    public /* final */ /* synthetic */ String $checksum$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ai7 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ String $remoteUrl$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileType $type$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileRepository this$0;

    @DexIgnore
    public FileRepository$downloadFromUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1(ai7 ai7, FileRepository fileRepository, String str, String str2, FileType fileType) {
        this.$continuation = ai7;
        this.this$0 = fileRepository;
        this.$remoteUrl$inlined = str;
        this.$checksum$inlined = str2;
        this.$type$inlined = fileType;
    }

    @DexIgnore
    @Override // com.fossil.qg5.c
    public void onComplete(boolean z, LocalFile localFile) {
        ee7.b(localFile, "file");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = FileRepository.Companion.getTAG();
        local.d(tag, "downloadFromURL with remoteUrl " + this.$remoteUrl$inlined + " isSuccess " + z);
        if (z) {
            this.this$0.mFileDao.upsertLocalFile(localFile);
        }
        if (this.$continuation.isActive()) {
            ai7 ai7 = this.$continuation;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(Boolean.valueOf(z)));
        }
    }
}
