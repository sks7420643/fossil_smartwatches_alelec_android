package com.portfolio.platform.data.source.local.diana.workout;

import com.fossil.ee7;
import com.fossil.ng;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDifference extends ng.d<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        ee7.b(workoutSession, "oldItem");
        ee7.b(workoutSession2, "newItem");
        return ee7.a(workoutSession, workoutSession2) && ee7.a(workoutSession.getScreenShotUri(), workoutSession2.getScreenShotUri());
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        ee7.b(workoutSession, "oldItem");
        ee7.b(workoutSession2, "newItem");
        return ee7.a((Object) workoutSession.getId(), (Object) workoutSession2.getId());
    }
}
