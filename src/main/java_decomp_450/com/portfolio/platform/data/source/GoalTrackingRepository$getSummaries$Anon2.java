package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.x97;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$2", f = "GoalTrackingRepository.kt", l = {210, 211}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<GoalTrackingSummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $goalTrackingDb;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<GoalTrackingSummary>, ApiResponse<GoalDailySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<GoalDailySummary>>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.this$0.mApiServiceV2;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiServiceV2$p.getGoalTrackingSummaries(g, g2, 0, 100, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<List<GoalTrackingSummary>>> fb7) {
                GoalTrackingDao goalTrackingDao = this.this$0.$goalTrackingDb.getGoalTrackingDao();
                GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = this.this$0.this$0;
                return goalTrackingDao.getGoalTrackingSummariesLiveData(goalTrackingRepository$getSummaries$Anon2.$startDate, goalTrackingRepository$getSummaries$Anon2.$endDate);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummaries onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<GoalDailySummary> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<GoalDailySummary> apiResponse, fb7<? super i97> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = GoalTrackingRepository.Companion.getTAG();
                local.d(tag, "getSummaries startDate=" + this.this$0.this$0.$startDate + ", endDate=" + this.this$0.this$0.$endDate + " saveCallResult onResponse: response = " + apiResponse);
                try {
                    List<GoalDailySummary> list = apiResponse.get_items();
                    ArrayList arrayList = new ArrayList(x97.a(list, 10));
                    Iterator<T> it = list.iterator();
                    while (it.hasNext()) {
                        GoalTrackingSummary goalTrackingSummary = it.next().toGoalTrackingSummary();
                        if (goalTrackingSummary != null) {
                            arrayList.add(goalTrackingSummary);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    this.this$0.$goalTrackingDb.getGoalTrackingDao().upsertGoalTrackingSummaries(ea7.d((Collection) arrayList));
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = GoalTrackingRepository.Companion.getTAG();
                    local2.e(tag2, "getSummaries startDate=" + this.this$0.this$0.$startDate + ", endDate=" + this.this$0.this$0.$endDate + " exception=" + e + '}');
                    e.printStackTrace();
                }
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(List<GoalTrackingSummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getSummaries$Anon2;
            this.$goalTrackingDb = goalTrackingDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<List<GoalTrackingSummary>>> apply(List<GoalTrackingData> list) {
            ee7.a((Object) list, "pendingList");
            GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, GoalTrackingDataKt.calculateRangeDownload(list, goalTrackingRepository$getSummaries$Anon2.$startDate, goalTrackingRepository$getSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummaries$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getSummaries$Anon2 goalTrackingRepository$getSummaries$Anon2 = new GoalTrackingRepository$getSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, fb7);
        goalTrackingRepository$getSummaries$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<GoalTrackingSummary>>>> fb7) {
        return ((GoalTrackingRepository$getSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        GoalTrackingDatabase goalTrackingDatabase;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getSummaries startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            ti7 b = qj7.b();
            GoalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2 goalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2 = new GoalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, goalTrackingRepository$getSummaries$Anon2$goalTrackingDb$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            goalTrackingDatabase = (GoalTrackingDatabase) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return ge.b((LiveData) obj, new Anon1_Level2(this, goalTrackingDatabase));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDatabase goalTrackingDatabase2 = (GoalTrackingDatabase) obj;
        GoalTrackingRepository goalTrackingRepository = this.this$0;
        Date date = this.$startDate;
        Date date2 = this.$endDate;
        this.L$0 = yi7;
        this.L$1 = goalTrackingDatabase2;
        this.label = 2;
        Object pendingGoalTrackingDataListLiveData = goalTrackingRepository.getPendingGoalTrackingDataListLiveData(date, date2, this);
        if (pendingGoalTrackingDataListLiveData == a) {
            return a;
        }
        goalTrackingDatabase = goalTrackingDatabase2;
        obj = pendingGoalTrackingDataListLiveData;
        return ge.b((LiveData) obj, new Anon1_Level2(this, goalTrackingDatabase));
    }
}
