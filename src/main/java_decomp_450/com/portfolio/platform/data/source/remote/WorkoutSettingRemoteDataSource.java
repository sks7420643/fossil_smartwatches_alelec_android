package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WorkoutSettingRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public WorkoutSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadWorkoutSettingList(com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting>>> r14) {
        /*
            r13 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$Anon1 r0 = (com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$Anon1 r0 = new com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$Anon1
            r0.<init>(r13, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            java.lang.String r4 = "WorkoutSettingRemoteDataSource"
            if (r2 == 0) goto L_0x0037
            if (r2 != r3) goto L_0x002f
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource) r0
            com.fossil.t87.a(r14)
            goto L_0x0056
        L_0x002f:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r0)
            throw r14
        L_0x0037:
            com.fossil.t87.a(r14)
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r2 = "downloadWorkoutSettingList()"
            r14.d(r4, r2)
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1 r14 = new com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1
            r2 = 0
            r14.<init>(r13, r2)
            r0.L$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.aj5.a(r14, r0)
            if (r14 != r1) goto L_0x0056
            return r1
        L_0x0056:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r0 = r14 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x00e7
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadWorkoutSettingList() - success isFromCache "
            r1.append(r2)
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            boolean r2 = r14.b()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r4, r1)
            java.lang.Object r0 = r14.a()
            if (r0 == 0) goto L_0x00cc
            java.lang.Object r0 = r14.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r0 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r0
            java.util.List r0 = r0.getAutoDetectWorkout()
            if (r0 == 0) goto L_0x0096
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0095
            goto L_0x0096
        L_0x0095:
            r3 = 0
        L_0x0096:
            if (r3 != 0) goto L_0x00cc
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadWorkoutSettingList() - success, response = "
            r1.append(r2)
            java.lang.Object r2 = r14.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r2 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r2
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r4, r1)
            com.fossil.bj5 r0 = new com.fossil.bj5
            java.lang.Object r1 = r14.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r1 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r1
            java.util.List r1 = r1.getAutoDetectWorkout()
            boolean r14 = r14.b()
            r0.<init>(r1, r14)
            goto L_0x012b
        L_0x00cc:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "downloadWorkoutSettingList() - success, empty response"
            r0.d(r4, r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            boolean r14 = r14.b()
            com.fossil.bj5 r1 = new com.fossil.bj5
            r1.<init>(r0, r14)
            r0 = r1
            goto L_0x012b
        L_0x00e7:
            boolean r0 = r14 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x012c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadWorkoutSettingList() - fail code "
            r1.append(r2)
            com.fossil.yi5 r14 = (com.fossil.yi5) r14
            int r2 = r14.a()
            r1.append(r2)
            java.lang.String r2 = " serverError "
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r14.c()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.d(r4, r1)
            com.fossil.yi5 r0 = new com.fossil.yi5
            int r6 = r14.a()
            com.portfolio.platform.data.model.ServerError r7 = r14.c()
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 28
            r12 = 0
            r5 = r0
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
        L_0x012b:
            return r0
        L_0x012c:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource.downloadWorkoutSettingList(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertWorkoutSettingList(java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting> r18, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting>>> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$Anon1
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$Anon1 r3 = (com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$Anon1) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$Anon1 r3 = new com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$Anon1
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 0
            r7 = 1
            java.lang.String r8 = "WorkoutSettingRemoteDataSource"
            if (r5 == 0) goto L_0x004a
            if (r5 != r7) goto L_0x0042
            java.lang.Object r1 = r3.L$3
            com.fossil.ie4 r1 = (com.fossil.ie4) r1
            java.lang.Object r1 = r3.L$2
            com.google.gson.Gson r1 = (com.google.gson.Gson) r1
            java.lang.Object r1 = r3.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource r1 = (com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource) r1
            com.fossil.t87.a(r2)
            goto L_0x00a7
        L_0x0042:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004a:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = "upsertWorkoutSettingList()"
            r2.d(r8, r5)
            com.fossil.be4 r2 = new com.fossil.be4
            r2.<init>()
            com.fossil.od5 r5 = new com.fossil.od5
            r5.<init>()
            r2.b(r5)
            com.google.gson.Gson r2 = r2.a()
            com.fossil.ie4 r5 = new com.fossil.ie4
            r5.<init>()
            com.google.gson.JsonElement r9 = r2.b(r1)
            java.lang.String r10 = "adwItems"
            r5.a(r10, r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "upsertWorkoutSettingList() - jsonObject "
            r10.append(r11)
            r10.append(r5)
            java.lang.String r10 = r10.toString()
            r9.d(r8, r10)
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$response$Anon1 r9 = new com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$upsertWorkoutSettingList$response$Anon1
            r9.<init>(r0, r5, r6)
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.label = r7
            java.lang.Object r2 = com.fossil.aj5.a(r9, r3)
            if (r2 != r4) goto L_0x00a7
            return r4
        L_0x00a7:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r1 = r2 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x0114
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r1 = r2.a()
            r3 = 2
            r4 = 0
            if (r1 == 0) goto L_0x00fd
            java.lang.Object r1 = r2.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r1 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r1
            java.util.List r1 = r1.getAutoDetectWorkout()
            if (r1 == 0) goto L_0x00cb
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x00ca
            goto L_0x00cb
        L_0x00ca:
            r7 = 0
        L_0x00cb:
            if (r7 != 0) goto L_0x00fd
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "upsertWorkoutSettingList() success, "
            r5.append(r7)
            java.lang.Object r7 = r2.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r7 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r7
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            r1.d(r8, r5)
            com.fossil.bj5 r1 = new com.fossil.bj5
            java.lang.Object r2 = r2.a()
            com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting r2 = (com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting) r2
            java.util.List r2 = r2.getAutoDetectWorkout()
            r1.<init>(r2, r4, r3, r6)
            goto L_0x0159
        L_0x00fd:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "upsertWorkoutSettingList() - success, empty response"
            r1.d(r8, r2)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.fossil.bj5 r2 = new com.fossil.bj5
            r2.<init>(r1, r4, r3, r6)
            r1 = r2
            goto L_0x0159
        L_0x0114:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x015a
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "upsertWorkoutSettingList() - fail code "
            r3.append(r4)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r4 = r2.a()
            r3.append(r4)
            java.lang.String r4 = " serverError "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r4 = r2.c()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r8, r3)
            com.fossil.yi5 r1 = new com.fossil.yi5
            int r10 = r2.a()
            com.portfolio.platform.data.model.ServerError r11 = r2.c()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 28
            r16 = 0
            r9 = r1
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
        L_0x0159:
            return r1
        L_0x015a:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource.upsertWorkoutSettingList(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
