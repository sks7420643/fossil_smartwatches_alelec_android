package com.portfolio.platform.data.source;

import com.fossil.ch5;
import com.fossil.ee7;
import com.fossil.hj7;
import com.fossil.qj7;
import com.fossil.xh7;
import com.fossil.zi7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;
    @DexIgnore
    public /* final */ ch5 sharedPreferencesManager;

    @DexIgnore
    public WatchLocalizationRepository(ApiServiceV2 apiServiceV2, ch5 ch5) {
        ee7.b(apiServiceV2, "api");
        ee7.b(ch5, "sharedPreferencesManager");
        this.api = apiServiceV2;
        this.sharedPreferencesManager = ch5;
        String simpleName = WatchLocalizationRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchLocalizationRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final hj7<String> processDownloadAndStore(String str, String str2) {
        return xh7.a(zi7.a(qj7.b()), null, null, new WatchLocalizationRepository$processDownloadAndStore$Anon1(this, str, str2, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchLocalizationFromServer(boolean r21, com.fossil.fb7<? super java.lang.String> r22) {
        /*
            r20 = this;
            r0 = r20
            r1 = r22
            boolean r2 = r1 instanceof com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1 r2 = (com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1 r2 = new com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 2
            r6 = 1
            r7 = 0
            if (r4 == 0) goto L_0x0076
            if (r4 == r6) goto L_0x0068
            if (r4 != r5) goto L_0x0060
            java.lang.Object r3 = r2.L$10
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$9
            com.fossil.hj7 r3 = (com.fossil.hj7) r3
            java.lang.Object r3 = r2.L$8
            java.io.File r3 = (java.io.File) r3
            java.lang.Object r3 = r2.L$7
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$6
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$5
            com.portfolio.platform.data.model.setting.WatchLocalization r3 = (com.portfolio.platform.data.model.setting.WatchLocalization) r3
            java.lang.Object r3 = r2.L$4
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r3 = r2.L$3
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r3 = r2.L$2
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r3 = r2.L$1
            java.lang.String r3 = (java.lang.String) r3
            boolean r3 = r2.Z$0
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.WatchLocalizationRepository r2 = (com.portfolio.platform.data.source.WatchLocalizationRepository) r2
            com.fossil.t87.a(r1)
            goto L_0x01d0
        L_0x0060:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0068:
            java.lang.Object r4 = r2.L$1
            java.lang.String r4 = (java.lang.String) r4
            boolean r6 = r2.Z$0
            java.lang.Object r8 = r2.L$0
            com.portfolio.platform.data.source.WatchLocalizationRepository r8 = (com.portfolio.platform.data.source.WatchLocalizationRepository) r8
            com.fossil.t87.a(r1)
            goto L_0x009b
        L_0x0076:
            com.fossil.t87.a(r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r4 = r1.m()
            com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 r1 = new com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1
            r1.<init>(r0, r4, r7)
            r2.L$0 = r0
            r8 = r21
            r2.Z$0 = r8
            r2.L$1 = r4
            r2.label = r6
            java.lang.Object r1 = com.fossil.aj5.a(r1, r2)
            if (r1 != r3) goto L_0x0099
            return r3
        L_0x0099:
            r6 = r8
            r8 = r0
        L_0x009b:
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            boolean r9 = r1 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x01e0
            r9 = r1
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r10 = r9.a()
            com.portfolio.platform.data.source.remote.ApiResponse r10 = (com.portfolio.platform.data.source.remote.ApiResponse) r10
            if (r10 == 0) goto L_0x00b1
            java.util.List r10 = r10.get_items()
            goto L_0x00b2
        L_0x00b1:
            r10 = r7
        L_0x00b2:
            if (r10 == 0) goto L_0x01de
            r11 = 0
            java.lang.Object r11 = r10.get(r11)
            com.portfolio.platform.data.model.setting.WatchLocalization r11 = (com.portfolio.platform.data.model.setting.WatchLocalization) r11
            com.fossil.ch5 r12 = r8.sharedPreferencesManager
            java.lang.String r13 = r11.getName()
            r12.b(r13, r4)
            com.fossil.ch5 r12 = r8.sharedPreferencesManager
            com.portfolio.platform.data.model.setting.MetaData r13 = r11.getMetaData()
            com.misfit.frameworks.buttonservice.model.watchparams.Version r13 = r13.getVersion()
            java.lang.String r13 = r13.toString()
            r12.v(r13)
            com.portfolio.platform.data.model.setting.Data r12 = r11.getData()
            java.lang.String r12 = r12.getUrl()
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r13 = "localization_"
            r15.append(r13)
            r15.append(r4)
            r16 = 0
            r17 = 0
            r18 = 6
            r19 = 0
            java.lang.String r14 = "."
            r13 = r12
            r7 = r15
            r15 = r16
            r16 = r17
            r17 = r18
            r18 = r19
            int r13 = com.fossil.nh7.b(r13, r14, r15, r16, r17, r18)
            if (r12 == 0) goto L_0x01d6
            java.lang.String r13 = r12.substring(r13)
            java.lang.String r14 = "(this as java.lang.String).substring(startIndex)"
            com.fossil.ee7.a(r13, r14)
            r7.append(r13)
            java.lang.String r7 = r7.toString()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r14 = r14.c()
            java.io.File r14 = r14.getFilesDir()
            r13.append(r14)
            java.lang.String r14 = "/localization"
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.io.File r14 = new java.io.File
            r14.<init>(r13)
            boolean r15 = r14.exists()
            if (r15 != 0) goto L_0x0167
            boolean r15 = r14.mkdirs()
            com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r16.getLocal()
            java.lang.String r0 = r8.TAG
            r16 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r21 = r11
            java.lang.String r11 = "create "
            r3.append(r11)
            r3.append(r14)
            java.lang.String r11 = " -  "
            r3.append(r11)
            r3.append(r15)
            java.lang.String r3 = r3.toString()
            r5.d(r0, r3)
            goto L_0x016b
        L_0x0167:
            r16 = r3
            r21 = r11
        L_0x016b:
            boolean r0 = r9.b()
            if (r0 == 0) goto L_0x0190
            com.fossil.ee5 r0 = com.fossil.ee5.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r13)
            java.lang.String r5 = java.io.File.separator
            r3.append(r5)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            boolean r0 = r0.a(r3)
            if (r0 != 0) goto L_0x018e
            goto L_0x0190
        L_0x018e:
            r0 = 0
            goto L_0x01df
        L_0x0190:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r13)
            java.lang.String r3 = java.io.File.separator
            r0.append(r3)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            com.fossil.hj7 r0 = r8.processDownloadAndStore(r12, r0)
            if (r6 == 0) goto L_0x01d4
            r2.L$0 = r8
            r2.Z$0 = r6
            r2.L$1 = r4
            r2.L$2 = r1
            r2.L$3 = r10
            r2.L$4 = r10
            r11 = r21
            r2.L$5 = r11
            r2.L$6 = r12
            r2.L$7 = r13
            r2.L$8 = r14
            r2.L$9 = r0
            r2.L$10 = r7
            r1 = 2
            r2.label = r1
            java.lang.Object r1 = r0.c(r2)
            r0 = r16
            if (r1 != r0) goto L_0x01d0
            return r0
        L_0x01d0:
            r7 = r1
            java.lang.String r7 = (java.lang.String) r7
            goto L_0x01d5
        L_0x01d4:
            r7 = 0
        L_0x01d5:
            return r7
        L_0x01d6:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x01de:
            r0 = r7
        L_0x01df:
            return r0
        L_0x01e0:
            r0 = r7
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r8.TAG
            java.lang.String r3 = "getWatchLocalizationFromServer - FAILED"
            r1.d(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchLocalizationRepository.getWatchLocalizationFromServer(boolean, com.fossil.fb7):java.lang.Object");
    }
}
