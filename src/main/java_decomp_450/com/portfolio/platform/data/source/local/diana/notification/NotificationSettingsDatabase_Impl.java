package com.portfolio.platform.data.source.local.diana.notification;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDatabase_Impl extends NotificationSettingsDatabase {
    @DexIgnore
    public volatile NotificationSettingsDao _notificationSettingsDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `notificationSettings` (`settingsName` TEXT NOT NULL, `settingsType` INTEGER NOT NULL, `isCall` INTEGER NOT NULL, PRIMARY KEY(`settingsName`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '563d2dcf0170d812960699bb88bdb35d')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `notificationSettings`");
            if (((ci) NotificationSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) NotificationSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) NotificationSettingsDatabase_Impl.this).mDatabase = wiVar;
            NotificationSettingsDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) NotificationSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) NotificationSettingsDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("settingsName", new ti.a("settingsName", "TEXT", true, 1, null, 1));
            hashMap.put("settingsType", new ti.a("settingsType", "INTEGER", true, 0, null, 1));
            hashMap.put("isCall", new ti.a("isCall", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("notificationSettings", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "notificationSettings");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "notificationSettings(com.portfolio.platform.data.model.NotificationSettingsModel).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `notificationSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "notificationSettings");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "563d2dcf0170d812960699bb88bdb35d", "d29e13e7c48fbcf2227b41f4f512e273");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase
    public NotificationSettingsDao getNotificationSettingsDao() {
        NotificationSettingsDao notificationSettingsDao;
        if (this._notificationSettingsDao != null) {
            return this._notificationSettingsDao;
        }
        synchronized (this) {
            if (this._notificationSettingsDao == null) {
                this._notificationSettingsDao = new NotificationSettingsDao_Impl(this);
            }
            notificationSettingsDao = this._notificationSettingsDao;
        }
        return notificationSettingsDao;
    }
}
