package com.portfolio.platform.data.source.local.inapp;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDatabase_Impl extends InAppNotificationDatabase {
    @DexIgnore
    public volatile InAppNotificationDao _inAppNotificationDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `inAppNotification` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `content` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b9f81264aeec3206295ee3fe096e0c84')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `inAppNotification`");
            if (((ci) InAppNotificationDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) InAppNotificationDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) InAppNotificationDatabase_Impl.this).mDatabase = wiVar;
            InAppNotificationDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) InAppNotificationDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) InAppNotificationDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("title", new ti.a("title", "TEXT", true, 0, null, 1));
            hashMap.put("content", new ti.a("content", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("inAppNotification", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "inAppNotification");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "inAppNotification(com.portfolio.platform.data.InAppNotification).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `inAppNotification`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "inAppNotification");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(1), "b9f81264aeec3206295ee3fe096e0c84", "9dd4de0568e77b393b85859b42ea3ad5");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase
    public InAppNotificationDao inAppNotificationDao() {
        InAppNotificationDao inAppNotificationDao;
        if (this._inAppNotificationDao != null) {
            return this._inAppNotificationDao;
        }
        synchronized (this) {
            if (this._inAppNotificationDao == null) {
                this._inAppNotificationDao = new InAppNotificationDao_Impl(this);
            }
            inAppNotificationDao = this._inAppNotificationDao;
        }
        return inAppNotificationDao;
    }
}
