package com.portfolio.platform.data.source.local.fitness;

import android.database.sqlite.SQLiteConstraintException;
import androidx.lifecycle.LiveData;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.w97;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.x97;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ActivitySummaryDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryDao.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivitySummaryDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSummary(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        List<Integer> list;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSummary - currentSummary=" + activitySummary + ", newSummary=" + activitySummary2);
        double steps = activitySummary2.getSteps() + activitySummary.getSteps();
        double calories = activitySummary2.getCalories() + activitySummary.getCalories();
        double distance = activitySummary2.getDistance() + activitySummary.getDistance();
        int activeTime = activitySummary2.getActiveTime() + activitySummary.getActiveTime();
        List<Integer> intensities = activitySummary2.getIntensities();
        List<Integer> intensities2 = activitySummary.getIntensities();
        if (intensities.size() > intensities2.size()) {
            ArrayList arrayList = new ArrayList(x97.a(intensities, 10));
            int i = 0;
            for (T t : intensities) {
                int i2 = i + 1;
                if (i >= 0) {
                    arrayList.add(Integer.valueOf(t.intValue() + (i < intensities2.size() ? intensities2.get(i).intValue() : 0)));
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list = ea7.d((Collection) arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(x97.a(intensities2, 10));
            int i3 = 0;
            for (T t2 : intensities2) {
                int i4 = i3 + 1;
                if (i3 >= 0) {
                    arrayList2.add(Integer.valueOf(t2.intValue() + (i3 < intensities.size() ? intensities.get(i3).intValue() : 0)));
                    i3 = i4;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list = ea7.d((Collection) arrayList2);
        }
        activitySummary2.setSteps(steps);
        activitySummary2.setCalories(calories);
        activitySummary2.setDistance(distance);
        activitySummary2.setActiveTime(activeTime);
        activitySummary2.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary2.setIntensities(list);
        activitySummary2.setStepGoal(activitySummary.getStepGoal());
        activitySummary2.setCaloriesGoal(activitySummary.getCaloriesGoal());
        activitySummary2.setActiveTimeGoal(activitySummary.getActiveTimeGoal());
        if (activitySummary.getSteps() != activitySummary2.getSteps()) {
            activitySummary2.setUpdatedAt(new DateTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySummaries();

    @DexIgnore
    public abstract ActivitySettings getActivitySetting();

    @DexIgnore
    public abstract LiveData<ActivitySettings> getActivitySettingLiveData();

    @DexIgnore
    public abstract ActivityStatistic getActivityStatistic();

    @DexIgnore
    public abstract LiveData<ActivityStatistic> getActivityStatisticLiveData();

    @DexIgnore
    public abstract List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final List<ActivitySummary> getActivitySummariesDesc(Date date, Date date2) {
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        ee7.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesDesc(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final LiveData<List<ActivitySummary>> getActivitySummariesLiveData(Date date, Date date2) {
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        ee7.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getActivitySummariesLiveData(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract ActivitySummary getActivitySummary(int i, int i2, int i3);

    @DexIgnore
    public final ActivitySummary getActivitySummary(Date date) {
        ee7.b(date, "date");
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "dateCalendar");
        instance.setTime(date);
        return getActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    @DexIgnore
    public abstract LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3);

    @DexIgnore
    public final Date getLastDate() {
        Calendar instance = Calendar.getInstance();
        ActivitySummary lastSummary = getLastSummary();
        if (lastSummary == null) {
            return null;
        }
        instance.set(lastSummary.getYear(), lastSummary.getMonth() - 1, lastSummary.getDay());
        ee7.a((Object) instance, "lastDate");
        return instance.getTime();
    }

    @DexIgnore
    public abstract ActivitySummary getLastSummary();

    @DexIgnore
    public abstract ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3);

    @DexIgnore
    public abstract ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6);

    @DexIgnore
    public final ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(Date date, Date date2) {
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "startCalendar");
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        ee7.a((Object) instance2, "endCalendar");
        instance2.setTime(date2);
        return getTotalValuesOfWeek(instance.get(5), instance.get(2) + 1, instance.get(1), instance2.get(5), instance2.get(2) + 1, instance2.get(1));
    }

    @DexIgnore
    public abstract long insertActivitySettings(ActivitySettings activitySettings);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void insertActivitySummaries(java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary> r8) {
        /*
            r7 = this;
            java.lang.String r0 = "summaries"
            com.fossil.ee7.b(r8, r0)
            java.util.Iterator r0 = r8.iterator()
        L_0x0009:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0105
            java.lang.Object r1 = r0.next()
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "addActivitySummary summary="
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            int r2 = r1.getYear()
            int r3 = r1.getMonth()
            int r4 = r1.getDay()
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r2 = r7.getActivitySummary(r2, r3, r4)
            if (r2 == 0) goto L_0x0048
            r7.calculateSummary(r2, r1)
            goto L_0x00e7
        L_0x0048:
            java.util.Calendar r2 = java.util.Calendar.getInstance()
            int r3 = r1.getYear()
            int r4 = r1.getMonth()
            r5 = 1
            int r4 = r4 - r5
            int r6 = r1.getDay()
            r2.set(r3, r4, r6)
            int r3 = r2.get(r5)
            r4 = 2
            int r4 = r2.get(r4)
            r5 = 5
            int r2 = r2.get(r5)
            com.portfolio.platform.data.model.room.fitness.ActivitySummary r2 = r7.getNearestSampleDayFromDate(r3, r4, r2)
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r3 = r7.getActivitySetting()
            r4 = 0
            if (r2 == 0) goto L_0x007f
            int r5 = r2.getStepGoal()
        L_0x007a:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            goto L_0x0087
        L_0x007f:
            if (r3 == 0) goto L_0x0086
            int r5 = r3.getCurrentStepGoal()
            goto L_0x007a
        L_0x0086:
            r5 = r4
        L_0x0087:
            if (r5 == 0) goto L_0x008e
            int r5 = r5.intValue()
            goto L_0x0090
        L_0x008e:
            r5 = 5000(0x1388, float:7.006E-42)
        L_0x0090:
            r1.setStepGoal(r5)
            if (r2 == 0) goto L_0x009e
            int r5 = r2.getActiveTimeGoal()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            goto L_0x00aa
        L_0x009e:
            if (r3 == 0) goto L_0x00a9
            int r5 = r3.getCurrentActiveTimeGoal()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            goto L_0x00aa
        L_0x00a9:
            r5 = r4
        L_0x00aa:
            if (r5 == 0) goto L_0x00b1
            int r5 = r5.intValue()
            goto L_0x00b3
        L_0x00b1:
            r5 = 30
        L_0x00b3:
            r1.setActiveTimeGoal(r5)
            if (r2 == 0) goto L_0x00c1
            int r2 = r2.getCaloriesGoal()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            goto L_0x00cb
        L_0x00c1:
            if (r3 == 0) goto L_0x00cb
            int r2 = r3.getCurrentCaloriesGoal()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
        L_0x00cb:
            if (r4 == 0) goto L_0x00d2
            int r2 = r4.intValue()
            goto L_0x00d4
        L_0x00d2:
            r2 = 140(0x8c, float:1.96E-43)
        L_0x00d4:
            r1.setCaloriesGoal(r2)
            org.joda.time.DateTime r2 = new org.joda.time.DateTime
            r2.<init>()
            r1.setCreatedAt(r2)
            org.joda.time.DateTime r2 = new org.joda.time.DateTime
            r2.<init>()
            r1.setUpdatedAt(r2)
        L_0x00e7:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "XXX- insert summary - summary="
            r4.append(r5)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.d(r3, r1)
            goto L_0x0009
        L_0x0105:
            r7.upsertActivitySummaries(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao.insertActivitySummaries(java.util.List):void");
    }

    @DexIgnore
    public abstract void updateActivitySettings(int i, int i2, int i3);

    @DexIgnore
    public abstract void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals);

    @DexIgnore
    public final void upsertActivitySettings(ActivitySettings activitySettings) {
        ee7.b(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "upsertActivitySettings stepGoal=" + activitySettings.getCurrentStepGoal() + " caloriesGoal=" + activitySettings.getCurrentCaloriesGoal() + " activeTimeGoal=" + activitySettings.getCurrentActiveTimeGoal());
        try {
            insertActivitySettings(activitySettings);
        } catch (SQLiteConstraintException e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "upsertActivitySettings exception " + e);
            updateActivitySettings(activitySettings.getCurrentStepGoal(), activitySettings.getCurrentCaloriesGoal(), activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexIgnore
    public abstract long upsertActivityStatistic(ActivityStatistic activityStatistic);

    @DexIgnore
    public abstract void upsertActivitySummaries(List<ActivitySummary> list);

    @DexIgnore
    public abstract void upsertActivitySummary(ActivitySummary activitySummary);
}
