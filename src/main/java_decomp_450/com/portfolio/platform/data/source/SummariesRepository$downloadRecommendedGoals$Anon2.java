package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.p87;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$2", f = "SummariesRepository.kt", l = {386}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$Anon2(SummariesRepository summariesRepository, int i, int i2, int i3, String str, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2 = new SummariesRepository$downloadRecommendedGoals$Anon2(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, fb7);
        summariesRepository$downloadRecommendedGoals$Anon2.p$ = (yi7) obj;
        return summariesRepository$downloadRecommendedGoals$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ActivityRecommendedGoals>> fb7) {
        return ((SummariesRepository$downloadRecommendedGoals$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 = new SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi5 = (zi5) obj;
        if (zi5 instanceof bj5) {
            Object a2 = ((bj5) zi5).a();
            if (a2 != null) {
                return new bj5((ActivityRecommendedGoals) a2, false, 2, null);
            }
            ee7.a();
            throw null;
        } else if (zi5 instanceof yi5) {
            yi5 yi5 = (yi5) zi5;
            return new yi5(yi5.a(), yi5.c(), null, null, null, 16, null);
        } else {
            throw new p87();
        }
    }
}
