package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$findNextActiveAlarm$2", f = "AlarmsRepository.kt", l = {34}, m = "invokeSuspend")
public final class AlarmsRepository$findNextActiveAlarm$Anon2 extends zb7 implements kd7<yi7, fb7<? super Alarm>, Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    public AlarmsRepository$findNextActiveAlarm$Anon2(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AlarmsRepository$findNextActiveAlarm$Anon2 alarmsRepository$findNextActiveAlarm$Anon2 = new AlarmsRepository$findNextActiveAlarm$Anon2(fb7);
        alarmsRepository$findNextActiveAlarm$Anon2.p$ = (yi7) obj;
        return alarmsRepository$findNextActiveAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Alarm> fb7) {
        return ((AlarmsRepository$findNextActiveAlarm$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        int i;
        Object a = nb7.a();
        int i2 = this.label;
        if (i2 == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            Calendar instance = Calendar.getInstance();
            int i3 = (instance.get(11) * 60) + instance.get(12);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.L$1 = instance;
            this.I$0 = i3;
            this.label = 1;
            obj = pg5.a(this);
            if (obj == a) {
                return a;
            }
            i = i3;
        } else if (i2 == 1) {
            i = this.I$0;
            Calendar calendar = (Calendar) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((AlarmDatabase) obj).alarmDao().getInComingActiveAlarm(i);
    }
}
