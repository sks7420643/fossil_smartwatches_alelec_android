package com.portfolio.platform.data.source;

import com.fossil.ch5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ch5 mSharedPreferencesManager;
    @DexIgnore
    public /* final */ UserRemoteDataSource mUserRemoteDataSource;
    @DexIgnore
    public /* final */ UserSettingDao mUserSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "UserRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRepository(UserRemoteDataSource userRemoteDataSource, UserSettingDao userSettingDao, ch5 ch5) {
        ee7.b(userRemoteDataSource, "mUserRemoteDataSource");
        ee7.b(userSettingDao, "mUserSettingDao");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.mUserRemoteDataSource = userRemoteDataSource;
        this.mUserSettingDao = userSettingDao;
        this.mSharedPreferencesManager = ch5;
    }

    @DexIgnore
    public final Object checkAuthenticationEmailExisting(String str, fb7<? super zi5<Boolean>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$checkAuthenticationEmailExisting$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object checkAuthenticationSocialExisting(String str, String str2, fb7<? super zi5<Boolean>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$checkAuthenticationSocialExisting$Anon2(this, str, str2, null), fb7);
    }

    @DexIgnore
    public final Object clearAllUser(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new UserRepository$clearAllUser$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final void clearUserSettings() {
        this.mUserSettingDao.cleanUp();
    }

    @DexIgnore
    public final Object deleteUser(fb7<? super Integer> fb7) {
        return vh7.a(qj7.b(), new UserRepository$deleteUser$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object getCurrentUser(fb7<? super MFUser> fb7) {
        return vh7.a(qj7.b(), new UserRepository$getCurrentUser$Anon2(null), fb7);
    }

    @DexIgnore
    public final UserSettings getUserSetting() {
        return this.mUserSettingDao.getCurrentUserSetting();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getUserSettingFromServer(com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.model.UserSettings>> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.t87.a(r5)
            goto L_0x0046
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r5 = r4.mUserRemoteDataSource
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.getUserSettingFromServer(r0)
            if (r5 != r1) goto L_0x0045
            return r1
        L_0x0045:
            r0 = r4
        L_0x0046:
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            boolean r1 = r5 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x0061
            r1 = r5
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r2 = r1.a()
            if (r2 == 0) goto L_0x0063
            com.portfolio.platform.data.source.UserSettingDao r0 = r0.mUserSettingDao
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            r0.addOrUpdateUserSetting(r1)
            goto L_0x0063
        L_0x0061:
            boolean r0 = r5 instanceof com.fossil.yi5
        L_0x0063:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.getUserSettingFromServer(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertUser(MFUser mFUser, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new UserRepository$insertUser$Anon2(mFUser, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final void insertUserSetting(UserSettings userSettings) {
        ee7.b(userSettings, "userSettings");
        this.mUserSettingDao.addOrUpdateUserSetting(userSettings);
    }

    @DexIgnore
    public final Object loadUserInfo(fb7<? super zi5<? extends MFUser>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$loadUserInfo$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object loginEmail(String str, String str2, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$loginEmail$Anon2(this, str, str2, null), fb7);
    }

    @DexIgnore
    public final Object loginWithSocial(String str, String str2, String str3, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$loginWithSocial$Anon2(this, str, str2, str3, null), fb7);
    }

    @DexIgnore
    public final Object logoutUser(fb7<? super Integer> fb7) {
        return vh7.a(qj7.b(), new UserRepository$logoutUser$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object pushPendingUser(MFUser mFUser, fb7<? super zi5<MFUser>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$pushPendingUser$Anon2(this, mFUser, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object pushPendingUserSetting(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r1 = r0.L$1
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x006d
        L_0x0031:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0039:
            com.fossil.t87.a(r8)
            com.portfolio.platform.data.source.UserSettingDao r8 = r7.mUserSettingDao
            com.portfolio.platform.data.model.UserSettings r8 = r8.getPendingUserSetting()
            if (r8 == 0) goto L_0x006d
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "pushPendingUserSetting(), userSetting="
            r5.append(r6)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            r2.d(r4, r5)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r8 = r7.sendUserSettingToServer(r8, r0)
            if (r8 != r1) goto L_0x006d
            return r1
        L_0x006d:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.pushPendingUserSetting(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object requestEmailOtp(String str, fb7<? super zi5<Void>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$requestEmailOtp$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object resetPassword(String str, fb7<? super zi5<Integer>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$resetPassword$Anon2(this, str, null), fb7);
    }

    @DexIgnore
    public final Object sendUserSettingToServer(UserSettings userSettings, fb7<? super zi5<UserSettings>> fb7) {
        return this.mUserRemoteDataSource.sendUserSettingToServer(userSettings, fb7);
    }

    @DexIgnore
    public final Object signUpEmail(SignUpEmailAuth signUpEmailAuth, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$signUpEmail$Anon2(this, signUpEmailAuth, null), fb7);
    }

    @DexIgnore
    public final Object signUpSocial(SignUpSocialAuth signUpSocialAuth, fb7<? super zi5<? extends MFUser.Auth>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$signUpSocial$Anon2(this, signUpSocialAuth, null), fb7);
    }

    @DexIgnore
    public final Object updateUser(MFUser mFUser, boolean z, fb7<? super zi5<? extends MFUser>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$updateUser$Anon2(this, z, mFUser, null), fb7);
    }

    @DexIgnore
    public final Object verifyEmailOtp(String str, String str2, fb7<? super zi5<Void>> fb7) {
        return vh7.a(qj7.b(), new UserRepository$verifyEmailOtp$Anon2(this, str, str2, null), fb7);
    }
}
