package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getCurrentLastSleepGoal$2", f = "SleepSummariesRepository.kt", l = {62}, m = "invokeSuspend")
public final class SleepSummariesRepository$getCurrentLastSleepGoal$Anon2 extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    public SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(fb7 fb7) {
        super(2, fb7);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$getCurrentLastSleepGoal$Anon2 sleepSummariesRepository$getCurrentLastSleepGoal$Anon2 = new SleepSummariesRepository$getCurrentLastSleepGoal$Anon2(fb7);
        sleepSummariesRepository$getCurrentLastSleepGoal$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$getCurrentLastSleepGoal$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
        return ((SleepSummariesRepository$getCurrentLastSleepGoal$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Integer a;
        Object a2 = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.d(this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFSleepSettings sleepSettings = ((SleepDatabase) obj).sleepDao().getSleepSettings();
        return pb7.a((sleepSettings == null || (a = pb7.a(sleepSettings.getSleepGoal())) == null) ? 480 : a.intValue());
    }
}
