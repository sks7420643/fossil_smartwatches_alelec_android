package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSettingRepository_Factory implements Factory<WatchAppLastSettingRepository> {
    @DexIgnore
    public /* final */ Provider<WatchAppLastSettingDao> mWatchAppLastSettingDaoProvider;

    @DexIgnore
    public WatchAppLastSettingRepository_Factory(Provider<WatchAppLastSettingDao> provider) {
        this.mWatchAppLastSettingDaoProvider = provider;
    }

    @DexIgnore
    public static WatchAppLastSettingRepository_Factory create(Provider<WatchAppLastSettingDao> provider) {
        return new WatchAppLastSettingRepository_Factory(provider);
    }

    @DexIgnore
    public static WatchAppLastSettingRepository newInstance(WatchAppLastSettingDao watchAppLastSettingDao) {
        return new WatchAppLastSettingRepository(watchAppLastSettingDao);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppLastSettingRepository get() {
        return newInstance(this.mWatchAppLastSettingDaoProvider.get());
    }
}
