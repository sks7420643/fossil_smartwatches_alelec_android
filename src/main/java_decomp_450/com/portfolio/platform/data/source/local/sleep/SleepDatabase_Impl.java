package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `sleep_session`");
            wiVar.execSQL("DROP TABLE IF EXISTS `sleep_date`");
            wiVar.execSQL("DROP TABLE IF EXISTS `sleep_settings`");
            wiVar.execSQL("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            wiVar.execSQL("DROP TABLE IF EXISTS `sleep_statistic`");
            if (((ci) SleepDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SleepDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SleepDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) SleepDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SleepDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SleepDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) SleepDatabase_Impl.this).mDatabase = wiVar;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) SleepDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SleepDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SleepDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new ti.a("date", "INTEGER", true, 0, null, 1));
            hashMap.put("day", new ti.a("day", "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new ti.a(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0, null, 1));
            hashMap.put("syncTime", new ti.a("syncTime", "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_BOOKMARK_TIME, new ti.a(MFSleepSession.COLUMN_BOOKMARK_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, new ti.a(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, "REAL", true, 0, null, 1));
            hashMap.put("source", new ti.a("source", "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_START_TIME, new ti.a(MFSleepSession.COLUMN_REAL_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_END_TIME, new ti.a(MFSleepSession.COLUMN_REAL_END_TIME, "INTEGER", true, 1, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, new ti.a(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, new ti.a(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_START_TIME, new ti.a(MFSleepSession.COLUMN_EDITED_START_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_END_TIME, new ti.a(MFSleepSession.COLUMN_EDITED_END_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, new ti.a(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, new ti.a(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_SLEEP_STATES, new ti.a(MFSleepSession.COLUMN_SLEEP_STATES, "TEXT", true, 0, null, 1));
            hashMap.put("heartRate", new ti.a("heartRate", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("timezoneOffset", new ti.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti(MFSleepSession.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, MFSleepSession.TABLE_NAME);
            if (!tiVar.equals(a)) {
                return new ei.b(false, "sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(8);
            hashMap2.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("timezoneOffset", new ti.a("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new ti.a("date", "TEXT", true, 1, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_GOAL_MINUTES, new ti.a(MFSleepDay.COLUMN_GOAL_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_MINUTES, new ti.a(MFSleepDay.COLUMN_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, new ti.a(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap2.put("createdAt", new ti.a("createdAt", "INTEGER", false, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "INTEGER", false, 0, null, 1));
            ti tiVar2 = new ti(MFSleepDay.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, MFSleepDay.TABLE_NAME);
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("sleepGoal", new ti.a("sleepGoal", "INTEGER", true, 0, null, 1));
            ti tiVar3 = new ti("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "sleep_settings");
            if (!tiVar3.equals(a3)) {
                return new ei.b(false, "sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(2);
            hashMap4.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap4.put("recommendedSleepGoal", new ti.a("recommendedSleepGoal", "INTEGER", true, 0, null, 1));
            ti tiVar4 = new ti("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, "sleepRecommendedGoals");
            if (!tiVar4.equals(a4)) {
                return new ei.b(false, "sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + tiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap5.put("uid", new ti.a("uid", "TEXT", true, 0, null, 1));
            hashMap5.put("sleepTimeBestDay", new ti.a("sleepTimeBestDay", "TEXT", false, 0, null, 1));
            hashMap5.put("sleepTimeBestStreak", new ti.a("sleepTimeBestStreak", "TEXT", false, 0, null, 1));
            hashMap5.put("totalDays", new ti.a("totalDays", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleeps", new ti.a("totalSleeps", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepMinutes", new ti.a("totalSleepMinutes", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepStateDistInMinute", new ti.a("totalSleepStateDistInMinute", "TEXT", true, 0, null, 1));
            hashMap5.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap5.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            ti tiVar5 = new ti(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
            ti a5 = ti.a(wiVar, SleepStatistic.TABLE_NAME);
            if (tiVar5.equals(a5)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + tiVar5 + "\n Found:\n" + a5);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `sleep_session`");
            writableDatabase.execSQL("DELETE FROM `sleep_date`");
            writableDatabase.execSQL("DELETE FROM `sleep_settings`");
            writableDatabase.execSQL("DELETE FROM `sleepRecommendedGoals`");
            writableDatabase.execSQL("DELETE FROM `sleep_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), MFSleepSession.TABLE_NAME, MFSleepDay.TABLE_NAME, "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDatabase
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
