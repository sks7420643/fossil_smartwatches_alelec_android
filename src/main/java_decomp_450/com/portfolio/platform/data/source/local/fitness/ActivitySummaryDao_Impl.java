package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.iu4;
import com.fossil.ji;
import com.fossil.nu4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.yu4;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDao_Impl extends ActivitySummaryDao {
    @DexIgnore
    public /* final */ iu4 __activityStatisticConverter; // = new iu4();
    @DexIgnore
    public /* final */ nu4 __dateTimeConverter; // = new nu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<ActivityRecommendedGoals> __insertionAdapterOfActivityRecommendedGoals;
    @DexIgnore
    public /* final */ vh<ActivitySettings> __insertionAdapterOfActivitySettings;
    @DexIgnore
    public /* final */ vh<ActivityStatistic> __insertionAdapterOfActivityStatistic;
    @DexIgnore
    public /* final */ vh<ActivitySummary> __insertionAdapterOfActivitySummary;
    @DexIgnore
    public /* final */ yu4 __integerArrayConverter; // = new yu4();
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllActivitySummaries;
    @DexIgnore
    public /* final */ ji __preparedStmtOfUpdateActivitySettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<ActivitySummary> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleday` (`createdAt`,`updatedAt`,`pinType`,`year`,`month`,`day`,`timezoneName`,`dstOffset`,`steps`,`calories`,`distance`,`intensities`,`activeTime`,`stepGoal`,`caloriesGoal`,`activeTimeGoal`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ActivitySummary activitySummary) {
            ajVar.bindLong(1, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getCreatedAt()));
            ajVar.bindLong(2, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activitySummary.getUpdatedAt()));
            ajVar.bindLong(3, (long) activitySummary.getPinType());
            ajVar.bindLong(4, (long) activitySummary.getYear());
            ajVar.bindLong(5, (long) activitySummary.getMonth());
            ajVar.bindLong(6, (long) activitySummary.getDay());
            if (activitySummary.getTimezoneName() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, activitySummary.getTimezoneName());
            }
            if (activitySummary.getDstOffset() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindLong(8, (long) activitySummary.getDstOffset().intValue());
            }
            ajVar.bindDouble(9, activitySummary.getSteps());
            ajVar.bindDouble(10, activitySummary.getCalories());
            ajVar.bindDouble(11, activitySummary.getDistance());
            String a = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activitySummary.getIntensities());
            if (a == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a);
            }
            ajVar.bindLong(13, (long) activitySummary.getActiveTime());
            ajVar.bindLong(14, (long) activitySummary.getStepGoal());
            ajVar.bindLong(15, (long) activitySummary.getCaloriesGoal());
            ajVar.bindLong(16, (long) activitySummary.getActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<ActivityStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon10(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivityStatistic call() throws Exception {
            ActivityStatistic activityStatistic;
            Cursor a = pi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "uid");
                int b3 = oi.b(a, "activeTimeBestDay");
                int b4 = oi.b(a, "activeTimeBestStreak");
                int b5 = oi.b(a, "caloriesBestDay");
                int b6 = oi.b(a, "caloriesBestStreak");
                int b7 = oi.b(a, "stepsBestDay");
                int b8 = oi.b(a, "stepsBestStreak");
                int b9 = oi.b(a, "totalActiveTime");
                int b10 = oi.b(a, "totalCalories");
                int b11 = oi.b(a, "totalDays");
                int b12 = oi.b(a, "totalDistance");
                int b13 = oi.b(a, "totalSteps");
                int b14 = oi.b(a, "totalIntensityDistInStep");
                int b15 = oi.b(a, "createdAt");
                int b16 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b), a.getString(b2), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b3)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b4)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.b(a.getString(b5)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b6)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b7)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(a.getString(b8)), a.getInt(b9), a.getDouble(b10), a.getInt(b11), a.getDouble(b12), a.getInt(b13), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b14)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b15)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b16)));
                } else {
                    activityStatistic = null;
                }
                return activityStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<ActivitySettings> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activitySettings` (`id`,`currentStepGoal`,`currentCaloriesGoal`,`currentActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ActivitySettings activitySettings) {
            ajVar.bindLong(1, (long) activitySettings.getId());
            ajVar.bindLong(2, (long) activitySettings.getCurrentStepGoal());
            ajVar.bindLong(3, (long) activitySettings.getCurrentCaloriesGoal());
            ajVar.bindLong(4, (long) activitySettings.getCurrentActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh<ActivityRecommendedGoals> {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activityRecommendedGoals` (`id`,`recommendedStepsGoal`,`recommendedCaloriesGoal`,`recommendedActiveTimeGoal`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ActivityRecommendedGoals activityRecommendedGoals) {
            ajVar.bindLong(1, (long) activityRecommendedGoals.getId());
            ajVar.bindLong(2, (long) activityRecommendedGoals.getRecommendedStepsGoal());
            ajVar.bindLong(3, (long) activityRecommendedGoals.getRecommendedCaloriesGoal());
            ajVar.bindLong(4, (long) activityRecommendedGoals.getRecommendedActiveTimeGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh<ActivityStatistic> {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_statistic` (`id`,`uid`,`activeTimeBestDay`,`activeTimeBestStreak`,`caloriesBestDay`,`caloriesBestStreak`,`stepsBestDay`,`stepsBestStreak`,`totalActiveTime`,`totalCalories`,`totalDays`,`totalDistance`,`totalSteps`,`totalIntensityDistInStep`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, ActivityStatistic activityStatistic) {
            if (activityStatistic.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, activityStatistic.getId());
            }
            if (activityStatistic.getUid() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, activityStatistic.getUid());
            }
            String a = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestDay());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            String a2 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestStreak());
            if (a2 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a2);
            }
            String a3 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestDay());
            if (a3 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a3);
            }
            String a4 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestStreak());
            if (a4 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a4);
            }
            String a5 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestDay());
            if (a5 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a5);
            }
            String a6 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestStreak());
            if (a6 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a6);
            }
            ajVar.bindLong(9, (long) activityStatistic.getTotalActiveTime());
            ajVar.bindDouble(10, activityStatistic.getTotalCalories());
            ajVar.bindLong(11, (long) activityStatistic.getTotalDays());
            ajVar.bindDouble(12, activityStatistic.getTotalDistance());
            ajVar.bindLong(13, (long) activityStatistic.getTotalSteps());
            String a7 = ActivitySummaryDao_Impl.this.__integerArrayConverter.a(activityStatistic.getTotalIntensityDistInStep());
            if (a7 == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, a7);
            }
            ajVar.bindLong(15, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getCreatedAt()));
            ajVar.bindLong(16, ActivitySummaryDao_Impl.this.__dateTimeConverter.a(activityStatistic.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM sampleday";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends ji {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE activitySettings SET currentStepGoal = ?, currentCaloriesGoal = ?, currentActiveTimeGoal = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<ActivitySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon7(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySummary call() throws Exception {
            ActivitySummary activitySummary;
            Integer num;
            Cursor a = pi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "createdAt");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, "pinType");
                int b4 = oi.b(a, "year");
                int b5 = oi.b(a, "month");
                int b6 = oi.b(a, "day");
                int b7 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
                int b8 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
                int b9 = oi.b(a, "steps");
                int b10 = oi.b(a, "calories");
                int b11 = oi.b(a, "distance");
                int b12 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
                int b13 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b14 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int b15 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int b16 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    int i = a.getInt(b4);
                    int i2 = a.getInt(b5);
                    int i3 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (a.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b8));
                    }
                    activitySummary = new ActivitySummary(i, i2, i3, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(b16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b)));
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setPinType(a.getInt(b3));
                } else {
                    activitySummary = null;
                }
                return activitySummary;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon8(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ActivitySummary> call() throws Exception {
            Integer num;
            Cursor a = pi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "createdAt");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, "pinType");
                int b4 = oi.b(a, "year");
                int b5 = oi.b(a, "month");
                int b6 = oi.b(a, "day");
                int b7 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
                int b8 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
                int b9 = oi.b(a, "steps");
                int b10 = oi.b(a, "calories");
                int b11 = oi.b(a, "distance");
                int b12 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
                int b13 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
                int b14 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i = b3;
                int b15 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i2 = b2;
                int i3 = b;
                int b16 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i4 = a.getInt(b4);
                    int i5 = a.getInt(b5);
                    int i6 = a.getInt(b6);
                    String string = a.getString(b7);
                    if (a.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b8));
                    }
                    ActivitySummary activitySummary = new ActivitySummary(i4, i5, i6, string, num, a.getDouble(b9), a.getDouble(b10), a.getDouble(b11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(a.getString(b12)), a.getInt(b13), a.getInt(b14), a.getInt(b15), a.getInt(b16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(a.getLong(i3)));
                    long j = a.getLong(i2);
                    i2 = i2;
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(j));
                    activitySummary.setPinType(a.getInt(i));
                    arrayList.add(activitySummary);
                    i = i;
                    b15 = b15;
                    b14 = b14;
                    b16 = b16;
                    i3 = i3;
                    b4 = b4;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<ActivitySettings> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon9(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySettings call() throws Exception {
            ActivitySettings activitySettings = null;
            Cursor a = pi.a(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
                int b3 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
                int b4 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    ActivitySettings activitySettings2 = new ActivitySettings(a.getInt(b2), a.getInt(b3), a.getInt(b4));
                    activitySettings2.setId(a.getInt(b));
                    activitySettings = activitySettings2;
                }
                return activitySettings;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public ActivitySummaryDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfActivitySummary = new Anon1(ciVar);
        this.__insertionAdapterOfActivitySettings = new Anon2(ciVar);
        this.__insertionAdapterOfActivityRecommendedGoals = new Anon3(ciVar);
        this.__insertionAdapterOfActivityStatistic = new Anon4(ciVar);
        this.__preparedStmtOfDeleteAllActivitySummaries = new Anon5(ciVar);
        this.__preparedStmtOfUpdateActivitySettings = new Anon6(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void deleteAllActivitySummaries() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllActivitySummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySettings getActivitySetting() {
        fi b = fi.b("SELECT * FROM activitySettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        ActivitySettings activitySettings = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
            int b4 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
            int b5 = oi.b(a, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
            if (a.moveToFirst()) {
                ActivitySettings activitySettings2 = new ActivitySettings(a.getInt(b3), a.getInt(b4), a.getInt(b5));
                activitySettings2.setId(a.getInt(b2));
                activitySettings = activitySettings2;
            }
            return activitySettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySettings> getActivitySettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME}, false, (Callable) new Anon9(fi.b("SELECT * FROM activitySettings LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivityStatistic getActivityStatistic() {
        fi fiVar;
        ActivityStatistic activityStatistic;
        fi b = fi.b("SELECT * FROM activity_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uid");
            int b4 = oi.b(a, "activeTimeBestDay");
            int b5 = oi.b(a, "activeTimeBestStreak");
            int b6 = oi.b(a, "caloriesBestDay");
            int b7 = oi.b(a, "caloriesBestStreak");
            int b8 = oi.b(a, "stepsBestDay");
            int b9 = oi.b(a, "stepsBestStreak");
            int b10 = oi.b(a, "totalActiveTime");
            int b11 = oi.b(a, "totalCalories");
            int b12 = oi.b(a, "totalDays");
            int b13 = oi.b(a, "totalDistance");
            int b14 = oi.b(a, "totalSteps");
            fiVar = b;
            try {
                int b15 = oi.b(a, "totalIntensityDistInStep");
                int b16 = oi.b(a, "createdAt");
                int b17 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(a.getString(b2), a.getString(b3), this.__activityStatisticConverter.a(a.getString(b4)), this.__activityStatisticConverter.a(a.getString(b5)), this.__activityStatisticConverter.b(a.getString(b6)), this.__activityStatisticConverter.a(a.getString(b7)), this.__activityStatisticConverter.a(a.getString(b8)), this.__activityStatisticConverter.a(a.getString(b9)), a.getInt(b10), a.getDouble(b11), a.getInt(b12), a.getDouble(b13), a.getInt(b14), this.__integerArrayConverter.a(a.getString(b15)), this.__dateTimeConverter.a(a.getLong(b16)), this.__dateTimeConverter.a(a.getLong(b17)));
                } else {
                    activityStatistic = null;
                }
                a.close();
                fiVar.c();
                return activityStatistic;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivityStatistic> getActivityStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{ActivityStatistic.TABLE_NAME}, false, (Callable) new Anon10(fi.b("SELECT * FROM activity_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6) {
        fi fiVar;
        Integer num;
        fi b = fi.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year DESC, month DESC, day DESC\n    ", 12);
        long j = (long) i3;
        b.bindLong(1, j);
        b.bindLong(2, j);
        long j2 = (long) i2;
        b.bindLong(3, j2);
        b.bindLong(4, j);
        b.bindLong(5, j2);
        b.bindLong(6, (long) i);
        long j3 = (long) i6;
        b.bindLong(7, j3);
        b.bindLong(8, j3);
        long j4 = (long) i5;
        b.bindLong(9, j4);
        b.bindLong(10, j3);
        b.bindLong(11, j4);
        b.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "year");
            int b6 = oi.b(a, "month");
            int b7 = oi.b(a, "day");
            int b8 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int i7 = b4;
                int b16 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int i8 = b3;
                int i9 = b2;
                int b17 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i10 = a.getInt(b5);
                    int i11 = a.getInt(b6);
                    int i12 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    ActivitySummary activitySummary = new ActivitySummary(i10, i11, i12, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(i9)));
                    long j5 = a.getLong(i8);
                    i8 = i8;
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(j5));
                    activitySummary.setPinType(a.getInt(i7));
                    arrayList.add(activitySummary);
                    b16 = b16;
                    b5 = b5;
                    i7 = i7;
                    i9 = i9;
                    b15 = b15;
                    b17 = b17;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6) {
        fi b = fi.b("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year ASC, month ASC, day ASC\n    ", 12);
        long j = (long) i3;
        b.bindLong(1, j);
        b.bindLong(2, j);
        long j2 = (long) i2;
        b.bindLong(3, j2);
        b.bindLong(4, j);
        b.bindLong(5, j2);
        b.bindLong(6, (long) i);
        long j3 = (long) i6;
        b.bindLong(7, j3);
        b.bindLong(8, j3);
        long j4 = (long) i5;
        b.bindLong(9, j4);
        b.bindLong(10, j3);
        b.bindLong(11, j4);
        b.bindLong(12, (long) i4);
        return this.__db.getInvalidationTracker().a(new String[]{SampleDay.TABLE_NAME}, false, (Callable) new Anon8(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getActivitySummary(int i, int i2, int i3) {
        fi fiVar;
        ActivitySummary activitySummary;
        Integer num;
        fi b = fi.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ? LIMIT 1", 3);
        b.bindLong(1, (long) i);
        b.bindLong(2, (long) i2);
        b.bindLong(3, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "year");
            int b6 = oi.b(a, "month");
            int b7 = oi.b(a, "day");
            int b8 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int b16 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int b17 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    int i4 = a.getInt(b5);
                    int i5 = a.getInt(b6);
                    int i6 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i4, i5, i6, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(b3)));
                    activitySummary.setPinType(a.getInt(b4));
                } else {
                    activitySummary = null;
                }
                a.close();
                fiVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3) {
        fi b = fi.b("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ?", 3);
        b.bindLong(1, (long) i);
        b.bindLong(2, (long) i2);
        b.bindLong(3, (long) i3);
        return this.__db.getInvalidationTracker().a(new String[]{SampleDay.TABLE_NAME}, false, (Callable) new Anon7(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getLastSummary() {
        fi fiVar;
        ActivitySummary activitySummary;
        Integer num;
        fi b = fi.b("SELECT * FROM sampleday ORDER BY year ASC, month ASC, day ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "year");
            int b6 = oi.b(a, "month");
            int b7 = oi.b(a, "day");
            int b8 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int b16 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int b17 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    int i = a.getInt(b5);
                    int i2 = a.getInt(b6);
                    int i3 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i, i2, i3, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(b3)));
                    activitySummary.setPinType(a.getInt(b4));
                } else {
                    activitySummary = null;
                }
                a.close();
                fiVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3) {
        fi fiVar;
        ActivitySummary activitySummary;
        Integer num;
        fi b = fi.b("SELECT * FROM sampleday\n        WHERE year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?) ORDER BY year DESC, month DESC, day DESC LIMIT 1", 6);
        long j = (long) i;
        b.bindLong(1, j);
        b.bindLong(2, j);
        long j2 = (long) i2;
        b.bindLong(3, j2);
        b.bindLong(4, j);
        b.bindLong(5, j2);
        b.bindLong(6, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "pinType");
            int b5 = oi.b(a, "year");
            int b6 = oi.b(a, "month");
            int b7 = oi.b(a, "day");
            int b8 = oi.b(a, SampleDay.COLUMN_TIMEZONE_NAME);
            int b9 = oi.b(a, SampleDay.COLUMN_DST_OFFSET);
            int b10 = oi.b(a, "steps");
            int b11 = oi.b(a, "calories");
            int b12 = oi.b(a, "distance");
            int b13 = oi.b(a, SampleDay.COLUMN_INTENSITIES);
            int b14 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, SampleDay.COLUMN_STEP_GOAL);
                int b16 = oi.b(a, SampleDay.COLUMN_CALORIES_GOAL);
                int b17 = oi.b(a, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (a.moveToFirst()) {
                    int i4 = a.getInt(b5);
                    int i5 = a.getInt(b6);
                    int i6 = a.getInt(b7);
                    String string = a.getString(b8);
                    if (a.isNull(b9)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b9));
                    }
                    activitySummary = new ActivitySummary(i4, i5, i6, string, num, a.getDouble(b10), a.getDouble(b11), a.getDouble(b12), this.__integerArrayConverter.a(a.getString(b13)), a.getInt(b14), a.getInt(b15), a.getInt(b16), a.getInt(b17));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(a.getLong(b2)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(a.getLong(b3)));
                    activitySummary.setPinType(a.getInt(b4));
                } else {
                    activitySummary = null;
                }
                a.close();
                fiVar.c();
                return activitySummary;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6) {
        fi b = fi.b("SELECT SUM(steps) as totalStepsOfWeek,  SUM(calories) as totalCaloriesOfWeek,  SUM(activeTime) as totalActiveTimeOfWeek FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ", 12);
        long j = (long) i3;
        b.bindLong(1, j);
        b.bindLong(2, j);
        long j2 = (long) i2;
        b.bindLong(3, j2);
        b.bindLong(4, j);
        b.bindLong(5, j2);
        b.bindLong(6, (long) i);
        long j3 = (long) i6;
        b.bindLong(7, j3);
        b.bindLong(8, j3);
        long j4 = (long) i5;
        b.bindLong(9, j4);
        b.bindLong(10, j3);
        b.bindLong(11, j4);
        b.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "totalStepsOfWeek");
            int b3 = oi.b(a, "totalCaloriesOfWeek");
            int b4 = oi.b(a, "totalActiveTimeOfWeek");
            if (a.moveToFirst()) {
                totalValuesOfWeek = new ActivitySummary.TotalValuesOfWeek(a.getDouble(b2), a.getDouble(b3), a.getInt(b4));
            }
            return totalValuesOfWeek;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long insertActivitySettings(ActivitySettings activitySettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivitySettings.insertAndReturnId(activitySettings);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void updateActivitySettings(int i, int i2, int i3) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfUpdateActivitySettings.acquire();
        acquire.bindLong(1, (long) i);
        acquire.bindLong(2, (long) i2);
        acquire.bindLong(3, (long) i3);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateActivitySettings.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivityRecommendedGoals.insert(activityRecommendedGoals);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long upsertActivityStatistic(ActivityStatistic activityStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivityStatistic.insertAndReturnId(activityStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummaries(List<ActivitySummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummary(ActivitySummary activitySummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(activitySummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
