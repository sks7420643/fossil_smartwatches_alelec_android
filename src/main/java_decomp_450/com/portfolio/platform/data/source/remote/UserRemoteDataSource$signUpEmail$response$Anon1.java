package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$1", f = "UserRemoteDataSource.kt", l = {197}, m = "invokeSuspend")
public final class UserRemoteDataSource$signUpEmail$response$Anon1 extends zb7 implements gd7<fb7<? super fv7<Auth>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SignUpEmailAuth $emailAuth;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$signUpEmail$response$Anon1(UserRemoteDataSource userRemoteDataSource, SignUpEmailAuth signUpEmailAuth, fb7 fb7) {
        super(1, fb7);
        this.this$0 = userRemoteDataSource;
        this.$emailAuth = signUpEmailAuth;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new UserRemoteDataSource$signUpEmail$response$Anon1(this.this$0, this.$emailAuth, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<Auth>> fb7) {
        return ((UserRemoteDataSource$signUpEmail$response$Anon1) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            AuthApiGuestService access$getMAuthApiGuestService$p = this.this$0.mAuthApiGuestService;
            SignUpEmailAuth signUpEmailAuth = this.$emailAuth;
            this.label = 1;
            obj = access$getMAuthApiGuestService$p.registerEmail(signUpEmailAuth, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
