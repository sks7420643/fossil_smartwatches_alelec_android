package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.QuickResponseSender;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseSenderDao_Impl extends QuickResponseSenderDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<QuickResponseSender> __insertionAdapterOfQuickResponseSender;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<QuickResponseSender> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseSender` (`id`,`content`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(aj ajVar, QuickResponseSender quickResponseSender) {
            ajVar.bindLong(1, (long) quickResponseSender.getId());
            if (quickResponseSender.getContent() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, quickResponseSender.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM quickResponseSender WHERE id = ?";
        }
    }

    @DexIgnore
    public QuickResponseSenderDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfQuickResponseSender = new Anon1(ciVar);
        this.__preparedStmtOfRemoveById = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public List<QuickResponseSender> getAllQuickResponseSender() {
        fi b = fi.b("SELECT * FROM quickResponseSender", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "content");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                QuickResponseSender quickResponseSender = new QuickResponseSender(a.getString(b3));
                quickResponseSender.setId(a.getInt(b2));
                arrayList.add(quickResponseSender);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public QuickResponseSender getQuickResponseSenderById(int i) {
        fi b = fi.b("SELECT * FROM quickResponseSender WHERE id = ?", 1);
        b.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        QuickResponseSender quickResponseSender = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "content");
            if (a.moveToFirst()) {
                QuickResponseSender quickResponseSender2 = new QuickResponseSender(a.getString(b3));
                quickResponseSender2.setId(a.getInt(b2));
                quickResponseSender = quickResponseSender2;
            }
            return quickResponseSender;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public long insertQuickResponseSender(QuickResponseSender quickResponseSender) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfQuickResponseSender.insertAndReturnId(quickResponseSender);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }
}
