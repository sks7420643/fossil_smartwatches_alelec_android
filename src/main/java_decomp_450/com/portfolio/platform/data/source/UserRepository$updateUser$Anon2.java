package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$updateUser$2", f = "UserRepository.kt", l = {44, 49, 59}, m = "invokeSuspend")
public final class UserRepository$updateUser$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isRemote;
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $user;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$updateUser$Anon2(UserRepository userRepository, boolean z, MFUser mFUser, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
        this.$isRemote = z;
        this.$user = mFUser;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$updateUser$Anon2 userRepository$updateUser$Anon2 = new UserRepository$updateUser$Anon2(this.this$0, this.$isRemote, this.$user, fb7);
        userRepository$updateUser$Anon2.p$ = (yi7) obj;
        return userRepository$updateUser$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
        return ((UserRepository$updateUser$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        MFUser mFUser;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            if (this.$isRemote) {
                FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "Inside .updateUser updateUserInfo on server");
                UserRemoteDataSource access$getMUserRemoteDataSource$p = this.this$0.mUserRemoteDataSource;
                MFUser mFUser2 = this.$user;
                this.L$0 = yi7;
                this.label = 1;
                obj = access$getMUserRemoteDataSource$p.updateUser(mFUser2, this);
                if (obj == a) {
                    return a;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "Inside .updateUser updateUserInfo on local");
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.label = 3;
                obj = pg5.f(this);
                if (obj == a) {
                    return a;
                }
                ((UserDatabase) obj).userDao().updateUser(this.$user);
                return new bj5(this.$user, false, 2, null);
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            mFUser = (MFUser) this.L$2;
            zi5 zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            ((UserDatabase) obj).userDao().updateUser(mFUser);
            return new bj5(mFUser, false, 2, null);
        } else if (i == 3) {
            yi7 yi73 = (yi7) this.L$0;
            t87.a(obj);
            ((UserDatabase) obj).userDao().updateUser(this.$user);
            return new bj5(this.$user, false, 2, null);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            Object a2 = ((bj5) zi52).a();
            if (a2 != null) {
                MFUser mFUser3 = (MFUser) a2;
                mFUser3.setAuth(this.$user.getAuth());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = UserRepository.TAG;
                local.d(access$getTAG$cp, "Inside .updateUser updateUserInfo success on serve userRemote " + mFUser3);
                pg5 pg52 = pg5.i;
                this.L$0 = yi7;
                this.L$1 = zi52;
                this.L$2 = mFUser3;
                this.label = 2;
                obj = pg52.f(this);
                if (obj == a) {
                    return a;
                }
                mFUser = mFUser3;
                ((UserDatabase) obj).userDao().updateUser(mFUser);
                return new bj5(mFUser, false, 2, null);
            }
            ee7.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "Inside .updateUser updateUserInfo error on server");
        return zi52;
    }
}
