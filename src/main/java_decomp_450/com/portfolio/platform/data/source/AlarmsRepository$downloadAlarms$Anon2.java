package com.portfolio.platform.data.source;

import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pd5;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2", f = "AlarmsRepository.kt", l = {89, 93, 97}, m = "invokeSuspend")
public final class AlarmsRepository$downloadAlarms$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2$1", f = "AlarmsRepository.kt", l = {106}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ zi5 $alarmsResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(zi5 zi5, fb7 fb7) {
            super(2, fb7);
            this.$alarmsResponse = zi5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.$alarmsResponse, fb7);
            anon1_Level2.p$ = (yi7) obj;
            return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((Anon1_Level2) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List<Alarm> list;
            List<Alarm> list2;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (!((bj5) this.$alarmsResponse).b() && (list2 = (List) ((bj5) this.$alarmsResponse).a()) != null) {
                    for (T t : list2) {
                        if (pd5.d.a((Alarm) t)) {
                            t.setActive(false);
                            t.setPinType(2);
                        }
                    }
                    pg5 pg5 = pg5.i;
                    this.L$0 = yi7;
                    this.L$1 = list2;
                    this.label = 1;
                    obj = pg5.a(this);
                    if (obj == a) {
                        return a;
                    }
                    list = list2;
                }
                return i97.a;
            } else if (i == 1) {
                list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ((AlarmDatabase) obj).alarmDao().insertAlarms(list);
            return i97.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$downloadAlarms$Anon2(AlarmsRepository alarmsRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        AlarmsRepository$downloadAlarms$Anon2 alarmsRepository$downloadAlarms$Anon2 = new AlarmsRepository$downloadAlarms$Anon2(this.this$0, fb7);
        alarmsRepository$downloadAlarms$Anon2.p$ = (yi7) obj;
        return alarmsRepository$downloadAlarms$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((AlarmsRepository$downloadAlarms$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bf  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r9.label
            r2 = 3
            r3 = 2
            r4 = 1
            if (r1 == 0) goto L_0x0038
            if (r1 == r4) goto L_0x0030
            if (r1 == r3) goto L_0x0026
            if (r1 != r2) goto L_0x001e
            java.lang.Object r0 = r9.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r0 = r9.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r10)
            goto L_0x00f7
        L_0x001e:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0026:
            boolean r1 = r9.Z$0
            java.lang.Object r3 = r9.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r10)
            goto L_0x007b
        L_0x0030:
            java.lang.Object r1 = r9.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r10)
            goto L_0x005c
        L_0x0038:
            com.fossil.t87.a(r10)
            com.fossil.yi7 r10 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.String r6 = "start download"
            r1.d(r5, r6)
            com.portfolio.platform.data.source.AlarmsRepository r1 = r9.this$0
            r9.L$0 = r10
            r9.label = r4
            java.lang.Object r1 = r1.executePendingRequest(r9)
            if (r1 != r0) goto L_0x0059
            return r0
        L_0x0059:
            r8 = r1
            r1 = r10
            r10 = r8
        L_0x005c:
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r10 = r10.booleanValue()
            if (r10 != 0) goto L_0x00e8
            com.portfolio.platform.data.source.AlarmsRepository r4 = r9.this$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r4 = r4.mAlarmRemoteDataSource
            r9.L$0 = r1
            r9.Z$0 = r10
            r9.label = r3
            java.lang.Object r3 = r4.getAlarms(r9)
            if (r3 != r0) goto L_0x0077
            return r0
        L_0x0077:
            r8 = r1
            r1 = r10
            r10 = r3
            r3 = r8
        L_0x007b:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r4 = r10 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x00bf
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "downloadAlarms success isFromCache "
            r6.append(r7)
            r7 = r10
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            boolean r7 = r7.b()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.d(r5, r6)
            com.fossil.ti7 r4 = com.fossil.qj7.b()
            com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$Anon2$Anon1_Level2 r5 = new com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$Anon2$Anon1_Level2
            r6 = 0
            r5.<init>(r10, r6)
            r9.L$0 = r3
            r9.Z$0 = r1
            r9.L$1 = r10
            r9.label = r2
            java.lang.Object r10 = com.fossil.vh7.a(r4, r5, r9)
            if (r10 != r0) goto L_0x00f7
            return r0
        L_0x00bf:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00f7
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "downloadAlarms fail!! "
            r2.append(r3)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r10 = r10.a()
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r0.d(r1, r10)
            goto L_0x00f7
        L_0x00e8:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.AlarmsRepository.TAG
            java.lang.String r1 = "downloadAlarms not execute due to pending data"
            r10.d(r0, r1)
        L_0x00f7:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
