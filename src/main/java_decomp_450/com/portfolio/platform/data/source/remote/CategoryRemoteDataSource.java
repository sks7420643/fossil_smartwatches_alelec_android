package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public CategoryRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllCategory(com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.Category>>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1 r0 = (com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1 r0 = new com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.CategoryRemoteDataSource) r0
            com.fossil.t87.a(r10)
            goto L_0x0049
        L_0x002e:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0036:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$response$Anon1
            r10.<init>(r9, r3)
            r0.L$0 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0073
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r10 = (com.portfolio.platform.data.source.remote.ApiResponse) r10
            if (r10 == 0) goto L_0x006b
            java.util.List r10 = r10.get_items()
            if (r10 == 0) goto L_0x006b
            boolean r10 = r0.addAll(r10)
            com.fossil.pb7.a(r10)
        L_0x006b:
            com.fossil.bj5 r10 = new com.fossil.bj5
            r1 = 0
            r2 = 2
            r10.<init>(r0, r1, r2, r3)
            goto L_0x0091
        L_0x0073:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0092
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r3 = r10.c()
            java.lang.Throwable r4 = r10.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            r10 = r0
        L_0x0091:
            return r10
        L_0x0092:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.CategoryRemoteDataSource.getAllCategory(com.fossil.fb7):java.lang.Object");
    }
}
