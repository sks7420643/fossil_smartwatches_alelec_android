package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.fossil.xu4;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDao_Impl implements HybridPresetDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ xu4 __hybridAppSettingTypeConverter; // = new xu4();
    @DexIgnore
    public /* final */ vh<HybridPreset> __insertionAdapterOfHybridPreset;
    @DexIgnore
    public /* final */ vh<HybridRecommendPreset> __insertionAdapterOfHybridRecommendPreset;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllPresetTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllRecommendPresetTable;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveAllDeletePinTypePreset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<HybridRecommendPreset> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridRecommendPreset` (`id`,`name`,`serialNumber`,`buttons`,`isDefault`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, HybridRecommendPreset hybridRecommendPreset) {
            if (hybridRecommendPreset.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, hybridRecommendPreset.getId());
            }
            if (hybridRecommendPreset.getName() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, hybridRecommendPreset.getName());
            }
            if (hybridRecommendPreset.getSerialNumber() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, hybridRecommendPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridRecommendPreset.getButtons());
            if (a == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a);
            }
            ajVar.bindLong(5, hybridRecommendPreset.isDefault() ? 1 : 0);
            if (hybridRecommendPreset.getCreatedAt() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, hybridRecommendPreset.getCreatedAt());
            }
            if (hybridRecommendPreset.getUpdatedAt() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, hybridRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<HybridPreset> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridPreset` (`pinType`,`createdAt`,`updatedAt`,`id`,`name`,`serialNumber`,`buttons`,`isActive`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, HybridPreset hybridPreset) {
            ajVar.bindLong(1, (long) hybridPreset.getPinType());
            if (hybridPreset.getCreatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, hybridPreset.getCreatedAt());
            }
            if (hybridPreset.getUpdatedAt() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, hybridPreset.getUpdatedAt());
            }
            if (hybridPreset.getId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, hybridPreset.getId());
            }
            if (hybridPreset.getName() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, hybridPreset.getName());
            }
            if (hybridPreset.getSerialNumber() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, hybridPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridPreset.getButtons());
            if (a == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a);
            }
            ajVar.bindLong(8, hybridPreset.isActive() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM hybridPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends ji {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM hybridRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends ji {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends ji {
        @DexIgnore
        public Anon7(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon8(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HybridPreset> call() throws Exception {
            Cursor a = pi.a(HybridPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "createdAt");
                int b3 = oi.b(a, "updatedAt");
                int b4 = oi.b(a, "id");
                int b5 = oi.b(a, "name");
                int b6 = oi.b(a, "serialNumber");
                int b7 = oi.b(a, "buttons");
                int b8 = oi.b(a, "isActive");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    HybridPreset hybridPreset = new HybridPreset(a.getString(b4), a.getString(b5), a.getString(b6), HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(a.getString(b7)), a.getInt(b8) != 0);
                    hybridPreset.setPinType(a.getInt(b));
                    hybridPreset.setCreatedAt(a.getString(b2));
                    hybridPreset.setUpdatedAt(a.getString(b3));
                    arrayList.add(hybridPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HybridPresetDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfHybridRecommendPreset = new Anon1(ciVar);
        this.__insertionAdapterOfHybridPreset = new Anon2(ciVar);
        this.__preparedStmtOfClearAllPresetTable = new Anon3(ciVar);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon4(ciVar);
        this.__preparedStmtOfClearAllRecommendPresetTable = new Anon5(ciVar);
        this.__preparedStmtOfDeletePreset = new Anon6(ciVar);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getActivePresetBySerial(String str) {
        fi b = fi.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        HybridPreset hybridPreset = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "createdAt");
            int b4 = oi.b(a, "updatedAt");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "name");
            int b7 = oi.b(a, "serialNumber");
            int b8 = oi.b(a, "buttons");
            int b9 = oi.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPendingPreset(String str) {
        fi b = fi.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "createdAt");
            int b4 = oi.b(a, "updatedAt");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "name");
            int b7 = oi.b(a, "serialNumber");
            int b8 = oi.b(a, "buttons");
            int b9 = oi.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPreset(String str) {
        fi b = fi.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "createdAt");
            int b4 = oi.b(a, "updatedAt");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "name");
            int b7 = oi.b(a, "serialNumber");
            int b8 = oi.b(a, "buttons");
            int b9 = oi.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str) {
        fi b = fi.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"hybridPreset"}, false, (Callable) new Anon8(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getPresetById(String str) {
        fi b = fi.b("SELECT * FROM hybridPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        HybridPreset hybridPreset = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "createdAt");
            int b4 = oi.b(a, "updatedAt");
            int b5 = oi.b(a, "id");
            int b6 = oi.b(a, "name");
            int b7 = oi.b(a, "serialNumber");
            int b8 = oi.b(a, "buttons");
            int b9 = oi.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridRecommendPreset> getRecommendPresetList(String str) {
        fi b = fi.b("SELECT * FROM hybridRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "name");
            int b4 = oi.b(a, "serialNumber");
            int b5 = oi.b(a, "buttons");
            int b6 = oi.b(a, "isDefault");
            int b7 = oi.b(a, "createdAt");
            int b8 = oi.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new HybridRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), this.__hybridAppSettingTypeConverter.a(a.getString(b5)), a.getInt(b6) != 0, a.getString(b7), a.getString(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPreset(HybridPreset hybridPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(hybridPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPresetList(List<HybridPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertRecommendPresetList(List<HybridRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
