package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.hm4;
import com.fossil.jm4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory implements Factory<jm4> {
    @DexIgnore
    public /* final */ Provider<hm4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<hm4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<hm4> provider) {
        return new PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static jm4 providesFlagLocalSource(PortfolioDatabaseModule portfolioDatabaseModule, hm4 hm4) {
        jm4 providesFlagLocalSource = portfolioDatabaseModule.providesFlagLocalSource(hm4);
        c87.a(providesFlagLocalSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesFlagLocalSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public jm4 get() {
        return providesFlagLocalSource(this.module, this.daoProvider.get());
    }
}
