package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingDao_Impl implements MicroAppLastSettingDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<MicroAppLastSetting> __insertionAdapterOfMicroAppLastSetting;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteMicroAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<MicroAppLastSetting> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppLastSetting` (`appId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MicroAppLastSetting microAppLastSetting) {
            if (microAppLastSetting.getAppId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, microAppLastSetting.getAppId());
            }
            if (microAppLastSetting.getUpdatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, microAppLastSetting.getUpdatedAt());
            }
            if (microAppLastSetting.getSetting() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, microAppLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microAppLastSetting WHERE appId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM microAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<MicroAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MicroAppLastSetting> call() throws Exception {
            Cursor a = pi.a(MicroAppLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "appId");
                int b2 = oi.b(a, "updatedAt");
                int b3 = oi.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new MicroAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public MicroAppLastSettingDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfMicroAppLastSetting = new Anon1(ciVar);
        this.__preparedStmtOfDeleteMicroAppLastSettingById = new Anon2(ciVar);
        this.__preparedStmtOfCleanUp = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void deleteMicroAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteMicroAppLastSettingById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public List<MicroAppLastSetting> getAllMicroAppLastSetting() {
        fi b = fi.b("SELECT * FROM microAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "appId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"microAppLastSetting"}, false, (Callable) new Anon4(fi.b("SELECT * FROM microAppLastSetting", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public MicroAppLastSetting getMicroAppLastSetting(String str) {
        fi b = fi.b("SELECT * FROM microAppLastSetting WHERE appId=? ", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroAppLastSetting microAppLastSetting = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "appId");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                microAppLastSetting = new MicroAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return microAppLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(microAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
