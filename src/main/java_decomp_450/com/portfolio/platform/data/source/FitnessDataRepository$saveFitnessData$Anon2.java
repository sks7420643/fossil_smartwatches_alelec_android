package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.x97;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.service.MFDeviceService;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.FitnessDataRepository$saveFitnessData$2", f = "FitnessDataRepository.kt", l = {26}, m = "invokeSuspend")
public final class FitnessDataRepository$saveFitnessData$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $fitnessDataList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FitnessDataRepository$saveFitnessData$Anon2(List list, fb7 fb7) {
        super(2, fb7);
        this.$fitnessDataList = list;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        FitnessDataRepository$saveFitnessData$Anon2 fitnessDataRepository$saveFitnessData$Anon2 = new FitnessDataRepository$saveFitnessData$Anon2(this.$fitnessDataList, fb7);
        fitnessDataRepository$saveFitnessData$Anon2.p$ = (yi7) obj;
        return fitnessDataRepository$saveFitnessData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((FitnessDataRepository$saveFitnessData$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            List<FitnessDataWrapper> list = this.$fitnessDataList;
            ArrayList arrayList = new ArrayList(x97.a(list, 10));
            for (FitnessDataWrapper fitnessDataWrapper : list) {
                arrayList.add(fitnessDataWrapper.getStartTime());
            }
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.PUSH_FITNESS_FILE;
            String b = MFDeviceService.Z.b();
            remote.i(component, session, "", b, "[Save Fitness Data] Pending fitness file size " + this.$fitnessDataList.size() + ", startTimestamp " + arrayList);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.L$1 = arrayList;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            List list2 = (List) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((FitnessDatabase) obj).getFitnessDataDao().insertFitnessDataList(this.$fitnessDataList);
        return i97.a;
    }
}
