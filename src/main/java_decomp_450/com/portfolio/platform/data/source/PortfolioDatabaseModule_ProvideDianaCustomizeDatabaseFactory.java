package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory implements Factory<DianaCustomizeDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DianaCustomizeDatabase provideDianaCustomizeDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        DianaCustomizeDatabase provideDianaCustomizeDatabase = portfolioDatabaseModule.provideDianaCustomizeDatabase(portfolioApp);
        c87.a(provideDianaCustomizeDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideDianaCustomizeDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaCustomizeDatabase get() {
        return provideDianaCustomizeDatabase(this.module, this.appProvider.get());
    }
}
