package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HybridPresetDao mHybridPresetDao;
    @DexIgnore
    public /* final */ HybridPresetRemoteDataSource mHybridPresetRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return HybridPresetRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = HybridPresetRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "HybridPresetRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HybridPresetRepository(HybridPresetDao hybridPresetDao, HybridPresetRemoteDataSource hybridPresetRemoteDataSource) {
        ee7.b(hybridPresetDao, "mHybridPresetDao");
        ee7.b(hybridPresetRemoteDataSource, "mHybridPresetRemoteDataSource");
        this.mHybridPresetDao = hybridPresetDao;
        this.mHybridPresetRemoteDataSource = hybridPresetRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mHybridPresetDao.clearAllPresetTable();
        this.mHybridPresetDao.clearAllRecommendPresetTable();
    }

    @DexIgnore
    public final void deleteAllPresetBySerial(String str) {
        ee7.b(str, "serial");
        this.mHybridPresetDao.clearAllPresetBySerial(str);
        this.mHybridPresetDao.clearAllRecommendPresetTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deletePresetById(java.lang.String r10, com.fossil.fb7<? super com.fossil.i97> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.HybridPresetRepository$deletePresetById$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.data.source.HybridPresetRepository$deletePresetById$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$deletePresetById$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HybridPresetRepository$deletePresetById$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$deletePresetById$Anon1
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0043
            if (r2 != r4) goto L_0x003b
            java.lang.Object r10 = r0.L$3
            com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r10
            java.lang.Object r10 = r0.L$2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r10
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r0 = (com.portfolio.platform.data.source.HybridPresetRepository) r0
            com.fossil.t87.a(r11)
            goto L_0x00ac
        L_0x003b:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0043:
            com.fossil.t87.a(r11)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r11 = r9.mHybridPresetDao
            com.portfolio.platform.data.model.room.microapp.HybridPreset r11 = r11.getPresetById(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "deletePresetById "
            r6.append(r7)
            if (r11 == 0) goto L_0x0065
            java.lang.String r7 = r11.getName()
            goto L_0x0066
        L_0x0065:
            r7 = r3
        L_0x0066:
            r6.append(r7)
            java.lang.String r7 = " pinType "
            r6.append(r7)
            if (r11 == 0) goto L_0x0079
            int r7 = r11.getPinType()
            java.lang.Integer r7 = com.fossil.pb7.a(r7)
            goto L_0x007a
        L_0x0079:
            r7 = r3
        L_0x007a:
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r2.d(r5, r6)
            if (r11 == 0) goto L_0x0101
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r2 = r9.mHybridPresetDao
            java.lang.String r5 = r11.getId()
            r2.deletePreset(r5)
            int r2 = r11.getPinType()
            if (r2 == r4) goto L_0x0101
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r2 = r9.mHybridPresetRemoteDataSource
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r11
            r0.label = r4
            java.lang.Object r10 = r2.deletePreset(r11, r0)
            if (r10 != r1) goto L_0x00a8
            return r1
        L_0x00a8:
            r0 = r9
            r8 = r11
            r11 = r10
            r10 = r8
        L_0x00ac:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r1 = r11 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00c0
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.String r0 = "deletePreset success"
            r10.d(r11, r0)
            goto L_0x0101
        L_0x00c0:
            boolean r1 = r11 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0101
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "deletePreset fail!! "
            r4.append(r5)
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r5 = r11.a()
            r4.append(r5)
            java.lang.String r5 = " serverCode "
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x00ee
            java.lang.Integer r3 = r11.getCode()
        L_0x00ee:
            r4.append(r3)
            java.lang.String r11 = r4.toString()
            r1.d(r2, r11)
            r11 = 3
            r10.setPinType(r11)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r11 = r0.mHybridPresetDao
            r11.upsertPreset(r10)
        L_0x0101:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.deletePresetById(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadPresetList(java.lang.String r12, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r13) {
        /*
            r11 = this;
            boolean r0 = r13 instanceof com.portfolio.platform.data.source.HybridPresetRepository$downloadPresetList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.portfolio.platform.data.source.HybridPresetRepository$downloadPresetList$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$downloadPresetList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HybridPresetRepository$downloadPresetList$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$downloadPresetList$Anon1
            r0.<init>(r11, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x004c
            if (r2 == r4) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            java.lang.Object r12 = r0.L$1
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r12 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r12 = (com.portfolio.platform.data.source.HybridPresetRepository) r12
            com.fossil.t87.a(r13)
            goto L_0x0090
        L_0x0034:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x003c:
            java.lang.Object r12 = r0.L$1
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r2 = (com.portfolio.platform.data.source.HybridPresetRepository) r2
            com.fossil.t87.a(r13)
            r10 = r13
            r13 = r12
            r12 = r2
            r2 = r10
            goto L_0x007b
        L_0x004c:
            com.fossil.t87.a(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "downloadPresetList serial "
            r5.append(r6)
            r5.append(r12)
            java.lang.String r5 = r5.toString()
            r13.d(r2, r5)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.label = r4
            java.lang.Object r13 = r11.executePendingRequest(r12, r0)
            if (r13 != r1) goto L_0x0078
            return r1
        L_0x0078:
            r2 = r13
            r13 = r12
            r12 = r11
        L_0x007b:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r4 = r2 instanceof com.fossil.bj5
            if (r4 == 0) goto L_0x0132
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r2 = r12.mHybridPresetRemoteDataSource
            r0.L$0 = r12
            r0.L$1 = r13
            r0.label = r3
            java.lang.Object r13 = r2.downloadHybridPresetList(r13, r0)
            if (r13 != r1) goto L_0x0090
            return r1
        L_0x0090:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r0 = r13 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00df
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "downloadPresetList success isFromCache "
            r3.append(r4)
            com.fossil.bj5 r13 = (com.fossil.bj5) r13
            boolean r4 = r13.b()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            boolean r0 = r13.b()
            if (r0 != 0) goto L_0x00d1
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r12 = r12.mHybridPresetDao
            java.lang.Object r0 = r13.a()
            if (r0 == 0) goto L_0x00cd
            java.util.List r0 = (java.util.List) r0
            r12.upsertPresetList(r0)
            goto L_0x00d1
        L_0x00cd:
            com.fossil.ee7.a()
            throw r1
        L_0x00d1:
            com.fossil.bj5 r12 = new com.fossil.bj5
            java.lang.Object r0 = r13.a()
            boolean r13 = r13.b()
            r12.<init>(r0, r13)
            goto L_0x012b
        L_0x00df:
            boolean r12 = r13 instanceof com.fossil.yi5
            if (r12 == 0) goto L_0x012c
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "downloadPresetList fail!! "
            r2.append(r3)
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            int r3 = r13.a()
            r2.append(r3)
            java.lang.String r3 = " serverCode "
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r3 = r13.c()
            if (r3 == 0) goto L_0x010d
            java.lang.Integer r1 = r3.getCode()
        L_0x010d:
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            r12.d(r0, r1)
            com.fossil.yi5 r12 = new com.fossil.yi5
            int r3 = r13.a()
            com.portfolio.platform.data.model.ServerError r4 = r13.c()
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 28
            r9 = 0
            r2 = r12
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x012b:
            return r12
        L_0x012c:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        L_0x0132:
            boolean r12 = r2 instanceof com.fossil.yi5
            if (r12 == 0) goto L_0x0147
            com.fossil.yi5 r12 = new com.fossil.yi5
            r1 = 600001(0x927c1, float:8.4078E-40)
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return r12
        L_0x0147:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.downloadPresetList(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRecommendPresetList(java.lang.String r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.HybridPresetRepository$downloadRecommendPresetList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.HybridPresetRepository$downloadRecommendPresetList$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$downloadRecommendPresetList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HybridPresetRepository$downloadRecommendPresetList$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$downloadRecommendPresetList$Anon1
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            java.lang.String r4 = "downloadRecommendPresetList - serial="
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            java.lang.Object r7 = r0.L$1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r0 = (com.portfolio.platform.data.source.HybridPresetRepository) r0
            com.fossil.t87.a(r8)
            goto L_0x0068
        L_0x0033:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003b:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r4)
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            r8.d(r2, r5)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r8 = r6.mHybridPresetRemoteDataSource
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r8 = r8.downloadHybridRecommendPresetList(r7, r0)
            if (r8 != r1) goto L_0x0067
            return r1
        L_0x0067:
            r0 = r6
        L_0x0068:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00bb
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r4)
            r5.append(r7)
            java.lang.String r4 = " success isFromCache "
            r5.append(r4)
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            boolean r4 = r8.b()
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            r1.d(r3, r4)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r1 = r0.mHybridPresetDao
            java.util.List r7 = r1.getRecommendPresetList(r7)
            boolean r1 = r8.b()
            if (r1 == 0) goto L_0x00a9
            boolean r7 = r7.isEmpty()
            if (r7 == 0) goto L_0x00f9
        L_0x00a9:
            java.lang.Object r7 = r8.a()
            if (r7 == 0) goto L_0x00b7
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r8 = r0.mHybridPresetDao
            r8.upsertRecommendPresetList(r7)
            goto L_0x00f9
        L_0x00b7:
            com.fossil.ee7.a()
            throw r2
        L_0x00bb:
            boolean r0 = r8 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00f9
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r4)
            r3.append(r7)
            java.lang.String r7 = " failed!!! "
            r3.append(r7)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r7 = r8.a()
            r3.append(r7)
            java.lang.String r7 = " serverError="
            r3.append(r7)
            com.portfolio.platform.data.model.ServerError r7 = r8.c()
            if (r7 == 0) goto L_0x00ef
            java.lang.Integer r2 = r7.getCode()
        L_0x00ef:
            r3.append(r2)
            java.lang.String r7 = r3.toString()
            r0.d(r1, r7)
        L_0x00f9:
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.downloadRecommendPresetList(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.Object executePendingRequest(java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.HybridPresetRepository$executePendingRequest$Anon1     // Catch:{ all -> 0x0126 }
            if (r0 == 0) goto L_0x0015
            r0 = r12
            com.portfolio.platform.data.source.HybridPresetRepository$executePendingRequest$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$executePendingRequest$Anon1) r0     // Catch:{ all -> 0x0126 }
            int r1 = r0.label     // Catch:{ all -> 0x0126 }
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0015
            int r12 = r0.label     // Catch:{ all -> 0x0126 }
            int r12 = r12 - r2
            r0.label = r12     // Catch:{ all -> 0x0126 }
            goto L_0x001a
        L_0x0015:
            com.portfolio.platform.data.source.HybridPresetRepository$executePendingRequest$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$executePendingRequest$Anon1     // Catch:{ all -> 0x0126 }
            r0.<init>(r10, r12)     // Catch:{ all -> 0x0126 }
        L_0x001a:
            java.lang.Object r12 = r0.result     // Catch:{ all -> 0x0126 }
            java.lang.Object r1 = com.fossil.nb7.a()     // Catch:{ all -> 0x0126 }
            int r2 = r0.label     // Catch:{ all -> 0x0126 }
            r3 = 2
            r4 = 0
            r5 = 0
            r6 = 1
            if (r2 == 0) goto L_0x0046
            if (r2 != r6) goto L_0x003e
            java.lang.Object r11 = r0.L$3     // Catch:{ all -> 0x0126 }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x0126 }
            java.lang.Object r11 = r0.L$2     // Catch:{ all -> 0x0126 }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x0126 }
            java.lang.Object r11 = r0.L$1     // Catch:{ all -> 0x0126 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0126 }
            java.lang.Object r11 = r0.L$0     // Catch:{ all -> 0x0126 }
            com.portfolio.platform.data.source.HybridPresetRepository r11 = (com.portfolio.platform.data.source.HybridPresetRepository) r11     // Catch:{ all -> 0x0126 }
            com.fossil.t87.a(r12)     // Catch:{ all -> 0x0126 }
            goto L_0x008d
        L_0x003e:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0126 }
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)     // Catch:{ all -> 0x0126 }
            throw r11     // Catch:{ all -> 0x0126 }
        L_0x0046:
            com.fossil.t87.a(r12)     // Catch:{ all -> 0x0126 }
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r12 = r10.mHybridPresetDao     // Catch:{ all -> 0x0126 }
            java.util.List r12 = r12.getAllPendingPreset(r11)     // Catch:{ all -> 0x0126 }
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0126 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x0126 }
            java.lang.String r7 = com.portfolio.platform.data.source.HybridPresetRepository.TAG     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r8.<init>()     // Catch:{ all -> 0x0126 }
            java.lang.String r9 = "executePendingRequest pendingPreset="
            r8.append(r9)     // Catch:{ all -> 0x0126 }
            r8.append(r12)     // Catch:{ all -> 0x0126 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0126 }
            r2.d(r7, r8)     // Catch:{ all -> 0x0126 }
            boolean r2 = r12.isEmpty()     // Catch:{ all -> 0x0126 }
            r2 = r2 ^ r6
            if (r2 == 0) goto L_0x010d
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r2 = r10.mHybridPresetDao     // Catch:{ all -> 0x0126 }
            java.util.List r2 = r2.getAllPreset(r11)     // Catch:{ all -> 0x0126 }
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r7 = r10.mHybridPresetRemoteDataSource     // Catch:{ all -> 0x0126 }
            r0.L$0 = r10     // Catch:{ all -> 0x0126 }
            r0.L$1 = r11     // Catch:{ all -> 0x0126 }
            r0.L$2 = r12     // Catch:{ all -> 0x0126 }
            r0.L$3 = r2     // Catch:{ all -> 0x0126 }
            r0.label = r6     // Catch:{ all -> 0x0126 }
            java.lang.Object r12 = r7.replaceHybridPresetList(r2, r0)     // Catch:{ all -> 0x0126 }
            if (r12 != r1) goto L_0x008c
            monitor-exit(r10)
            return r1
        L_0x008c:
            r11 = r10
        L_0x008d:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r0 = r12 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x00c5
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r0 = r11.mHybridPresetDao
            r1 = r12
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x00c1
            java.util.List r1 = (java.util.List) r1
            r0.upsertPresetList(r1)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r11 = r11.mHybridPresetDao
            r11.removeAllDeletePinTypePreset()
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.String r1 = "executePendingRequest success sync with server"
            r11.d(r0, r1)
            com.fossil.bj5 r11 = new com.fossil.bj5
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r12 = r12.a()
            r11.<init>(r12, r4, r3, r5)
            goto L_0x0105
        L_0x00c1:
            com.fossil.ee7.a()
            throw r5
        L_0x00c5:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0107
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "executePendingRequest fail to sync with server "
            r1.append(r2)
            r2 = r12
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r2 = r2.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r11.d(r0, r1)
            com.fossil.yi5 r11 = new com.fossil.yi5
            r0 = r12
            com.fossil.yi5 r0 = (com.fossil.yi5) r0
            int r3 = r0.a()
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            com.portfolio.platform.data.model.ServerError r4 = r12.c()
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 28
            r9 = 0
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
        L_0x0105:
            monitor-exit(r10)
            return r11
        L_0x0107:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        L_0x010d:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.String r0 = "executePendingRequest success no pending"
            r11.d(r12, r0)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            com.fossil.bj5 r12 = new com.fossil.bj5
            r12.<init>(r11, r4, r3, r5)
            monitor-exit(r10)
            return r12
        L_0x0126:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.executePendingRequest(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final HybridPreset getActivePresetBySerial(String str) {
        ee7.b(str, "serial");
        return this.mHybridPresetDao.getActivePresetBySerial(str);
    }

    @DexIgnore
    public final List<HybridRecommendPreset> getHybridRecommendPresetList(String str) {
        ee7.b(str, "serial");
        return this.mHybridPresetDao.getRecommendPresetList(str);
    }

    @DexIgnore
    public final HybridPreset getPresetById(String str) {
        ee7.b(str, "id");
        return this.mHybridPresetDao.getPresetById(str);
    }

    @DexIgnore
    public final ArrayList<HybridPreset> getPresetList(String str) {
        ee7.b(str, "serial");
        List<HybridPreset> allPreset = this.mHybridPresetDao.getAllPreset(str);
        if (allPreset != null) {
            return (ArrayList) allPreset;
        }
        throw new x87("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>");
    }

    @DexIgnore
    public final LiveData<List<HybridPreset>> getPresetListAsLiveData(String str) {
        ee7.b(str, "serial");
        return this.mHybridPresetDao.getAllPresetAsLiveData(str);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v3, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset */
    /* JADX DEBUG: Multi-variable search result rejected for r5v4, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset */
    /* JADX DEBUG: Multi-variable search result rejected for r5v6, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset r11, com.fossil.fb7<? super com.fossil.i97> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPreset$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPreset$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPreset$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPreset$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPreset$Anon1
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 != r3) goto L_0x0042
            java.lang.Object r11 = r0.L$5
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            java.lang.Object r11 = r0.L$4
            com.portfolio.platform.data.model.room.microapp.HybridPreset r11 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r11
            java.lang.Object r11 = r0.L$3
            com.portfolio.platform.data.model.room.microapp.HybridPreset r11 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r11
            java.lang.Object r11 = r0.L$2
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            java.lang.Object r11 = r0.L$1
            com.portfolio.platform.data.model.room.microapp.HybridPreset r11 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r11 = (com.portfolio.platform.data.source.HybridPresetRepository) r11
            com.fossil.t87.a(r12)
            goto L_0x0165
        L_0x0042:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x004a:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "upsertHybridPreset "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r5 = " pinType "
            r4.append(r5)
            int r5 = r11.getPinType()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r12.d(r2, r4)
            java.util.Date r12 = new java.util.Date
            long r4 = java.lang.System.currentTimeMillis()
            r12.<init>(r4)
            java.lang.String r12 = com.fossil.zd5.y(r12)
            r11.setUpdatedAt(r12)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r12 = r10.mHybridPresetDao
            java.lang.String r2 = r11.getSerialNumber()
            java.util.List r12 = r12.getAllPreset(r2)
            if (r12 == 0) goto L_0x0168
            java.util.ArrayList r12 = (java.util.ArrayList) r12
            boolean r2 = r12.isEmpty()
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x0165
            java.util.Iterator r2 = r12.iterator()
        L_0x009e:
            boolean r4 = r2.hasNext()
            r5 = 0
            if (r4 == 0) goto L_0x00c3
            java.lang.Object r4 = r2.next()
            r6 = r4
            com.portfolio.platform.data.model.room.microapp.HybridPreset r6 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r6
            java.lang.String r6 = r6.getId()
            java.lang.String r7 = r11.getId()
            boolean r6 = com.fossil.ee7.a(r6, r7)
            java.lang.Boolean r6 = com.fossil.pb7.a(r6)
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x009e
            goto L_0x00c4
        L_0x00c3:
            r4 = r5
        L_0x00c4:
            com.portfolio.platform.data.model.room.microapp.HybridPreset r4 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r4
            java.util.Iterator r2 = r12.iterator()
        L_0x00ca:
            boolean r6 = r2.hasNext()
            if (r6 == 0) goto L_0x00e6
            java.lang.Object r6 = r2.next()
            r7 = r6
            com.portfolio.platform.data.model.room.microapp.HybridPreset r7 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r7
            boolean r7 = r7.isActive()
            java.lang.Boolean r7 = com.fossil.pb7.a(r7)
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x00ca
            r5 = r6
        L_0x00e6:
            com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r5
            r2 = 2
            if (r4 == 0) goto L_0x00f5
            int r6 = r4.getPinType()
            if (r6 == r3) goto L_0x00f5
            r11.setPinType(r2)
            goto L_0x00f8
        L_0x00f5:
            r11.setPinType(r3)
        L_0x00f8:
            if (r5 == 0) goto L_0x011c
            java.lang.String r6 = r5.getId()
            java.lang.String r7 = r11.getId()
            boolean r6 = com.fossil.ee7.a(r6, r7)
            r6 = r6 ^ r3
            if (r6 == 0) goto L_0x011c
            boolean r6 = r11.isActive()
            if (r6 == 0) goto L_0x011c
            r6 = 0
            r5.setActive(r6)
            int r6 = r5.getPinType()
            if (r6 == r3) goto L_0x011c
            r5.setPinType(r2)
        L_0x011c:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r6 = r12.iterator()
        L_0x0125:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x014d
            java.lang.Object r7 = r6.next()
            r8 = r7
            com.portfolio.platform.data.model.room.microapp.HybridPreset r8 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r8
            java.lang.String r8 = r8.getId()
            java.lang.String r9 = r11.getId()
            boolean r8 = com.fossil.ee7.a(r8, r9)
            r8 = r8 ^ r3
            java.lang.Boolean r8 = com.fossil.pb7.a(r8)
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x0125
            r2.add(r7)
            goto L_0x0125
        L_0x014d:
            r2.add(r11)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r4
            r0.L$4 = r5
            r0.L$5 = r2
            r0.label = r3
            java.lang.Object r11 = r10.upsertHybridPresetList(r2, r0)
            if (r11 != r1) goto L_0x0165
            return r1
        L_0x0165:
            com.fossil.i97 r11 = com.fossil.i97.a
            return r11
        L_0x0168:
            com.fossil.x87 r11 = new com.fossil.x87
            java.lang.String r12 = "null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>"
            r11.<init>(r12)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.upsertHybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.util.List r6 = (java.util.List) r6
            java.lang.Object r6 = r0.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r6 = (com.portfolio.platform.data.source.HybridPresetRepository) r6
            com.fossil.t87.a(r7)
            goto L_0x005e
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.String r4 = "upsertHybridPresetList"
            r7.d(r2, r4)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r7 = r5.mHybridPresetDao
            r7.upsertPresetList(r6)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r7 = r5.mHybridPresetRemoteDataSource
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.upsertHybridPresetList(r6, r0)
            if (r7 != r1) goto L_0x005d
            return r1
        L_0x005d:
            r6 = r5
        L_0x005e:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r0 = r7 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00a1
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "upsertHybridPresetList success "
            r3.append(r4)
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r4 = r7.a()
            if (r4 == 0) goto L_0x009d
            java.util.ArrayList r4 = (java.util.ArrayList) r4
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r6 = r6.mHybridPresetDao
            java.lang.Object r7 = r7.a()
            if (r7 == 0) goto L_0x0099
            java.util.List r7 = (java.util.List) r7
            r6.upsertPresetList(r7)
            goto L_0x00d9
        L_0x0099:
            com.fossil.ee7.a()
            throw r1
        L_0x009d:
            com.fossil.ee7.a()
            throw r1
        L_0x00a1:
            boolean r6 = r7 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x00d9
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upsertHybridPresetList fail!! "
            r2.append(r3)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r3 = r7.a()
            r2.append(r3)
            java.lang.String r3 = " serverCode "
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00cf
            java.lang.Integer r1 = r7.getCode()
        L_0x00cf:
            r2.append(r1)
            java.lang.String r7 = r2.toString()
            r6.d(r0, r7)
        L_0x00d9:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.upsertHybridPresetList(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
