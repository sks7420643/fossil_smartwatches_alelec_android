package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.go4;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory implements Factory<go4> {
    @DexIgnore
    public /* final */ Provider<BuddyChallengeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static go4 providesSocialProfileDao(PortfolioDatabaseModule portfolioDatabaseModule, BuddyChallengeDatabase buddyChallengeDatabase) {
        go4 providesSocialProfileDao = portfolioDatabaseModule.providesSocialProfileDao(buddyChallengeDatabase);
        c87.a(providesSocialProfileDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialProfileDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public go4 get() {
        return providesSocialProfileDao(this.module, this.dbProvider.get());
    }
}
