package com.portfolio.platform.data.source;

import com.fossil.ge5;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository_Factory implements Factory<SummariesRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<ge5> mFitnessHelperProvider;

    @DexIgnore
    public SummariesRepository_Factory(Provider<ApiServiceV2> provider, Provider<ge5> provider2) {
        this.mApiServiceV2Provider = provider;
        this.mFitnessHelperProvider = provider2;
    }

    @DexIgnore
    public static SummariesRepository_Factory create(Provider<ApiServiceV2> provider, Provider<ge5> provider2) {
        return new SummariesRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static SummariesRepository newInstance(ApiServiceV2 apiServiceV2, ge5 ge5) {
        return new SummariesRepository(apiServiceV2, ge5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public SummariesRepository get() {
        return newInstance(this.mApiServiceV2Provider.get(), this.mFitnessHelperProvider.get());
    }
}
