package com.portfolio.platform.data.source;

import com.fossil.be5;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2", f = "DeviceRepository.kt", l = {106}, m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ zi5 $response;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$Anon2(DeviceRepository deviceRepository, zi5 zi5, fb7 fb7) {
        super(2, fb7);
        this.this$0 = deviceRepository;
        this.$response = zi5;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(this.this$0, this.$response, fb7);
        deviceRepository$downloadDeviceList$Anon2.p$ = (yi7) obj;
        return deviceRepository$downloadDeviceList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((DeviceRepository$downloadDeviceList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        List<Device> list;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ApiResponse apiResponse = (ApiResponse) ((bj5) this.$response).a();
            List<Device> list2 = apiResponse != null ? apiResponse.get_items() : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = DeviceRepository.Companion.getTAG();
            local.d(tag, "downloadDeviceList() - isFromCache = false " + list2);
            DeviceRepository deviceRepository = this.this$0;
            this.L$0 = yi7;
            this.L$1 = list2;
            this.label = 1;
            if (deviceRepository.removeStealDevice(list2, this) == a) {
                return a;
            }
            list = list2;
        } else if (i == 1) {
            list = (List) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (list != null) {
            for (Device device : list) {
                if (be5.o.e(device.getDeviceId())) {
                    Device deviceByDeviceId = this.this$0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                    if (deviceByDeviceId != null) {
                        if (device.getBatteryLevel() <= 0) {
                            device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                        }
                        device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = DeviceRepository.Companion.getTAG();
                    local2.d(tag2, "update device: " + device);
                    this.this$0.mDeviceDao.addOrUpdateDevice(device);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tag3 = DeviceRepository.Companion.getTAG();
                    local3.d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                }
            }
            return i97.a;
        }
        ee7.a();
        throw null;
    }
}
