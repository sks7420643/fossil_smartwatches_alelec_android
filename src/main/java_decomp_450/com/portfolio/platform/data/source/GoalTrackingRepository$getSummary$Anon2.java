package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2", f = "GoalTrackingRepository.kt", l = {175, 177}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends GoalTrackingSummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $goalTrackingDb;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<GoalTrackingSummary, GoalDailySummary> {
            @DexIgnore
            public /* final */ /* synthetic */ List $pendingList;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, List list) {
                this.this$0 = anon1_Level2;
                this.$pendingList = list;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<GoalDailySummary>> fb7) {
                ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.this$0.mApiServiceV2;
                String g = zd5.g(this.this$0.this$0.$date);
                ee7.a((Object) g, "DateHelper.formatShortDate(date)");
                return access$getMApiServiceV2$p.getGoalTrackingSummary(g, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<GoalTrackingSummary>> fb7) {
                return this.this$0.$goalTrackingDb.getGoalTrackingDao().getGoalTrackingSummaryLiveData(this.this$0.this$0.$date);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(GoalDailySummary goalDailySummary, fb7 fb7) {
                return saveCallResult(goalDailySummary, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x003e  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r8, com.fossil.fb7<? super com.fossil.i97> r9) {
                /*
                    r7 = this;
                    boolean r0 = r9 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r9
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r7, r9)
                L_0x0018:
                    java.lang.Object r9 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    java.lang.String r3 = "getSummary date="
                    r4 = 1
                    if (r2 == 0) goto L_0x003e
                    if (r2 != r4) goto L_0x0036
                    java.lang.Object r8 = r0.L$1
                    com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r8 = (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary) r8
                    java.lang.Object r8 = r0.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3 r8 = (com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3) r8
                    com.fossil.t87.a(r9)     // Catch:{ Exception -> 0x0034 }
                    goto L_0x00b5
                L_0x0034:
                    r9 = move-exception
                    goto L_0x0086
                L_0x0036:
                    java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                    java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
                    r8.<init>(r9)
                    throw r8
                L_0x003e:
                    com.fossil.t87.a(r9)
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r2 = r2.getTAG()
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    r5.append(r3)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r6 = r7.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r6 = r6.this$0
                    java.util.Date r6 = r6.$date
                    r5.append(r6)
                    java.lang.String r6 = " saveCallResult onResponse: response = "
                    r5.append(r6)
                    r5.append(r8)
                    java.lang.String r5 = r5.toString()
                    r9.d(r2, r5)
                    com.fossil.ti7 r9 = com.fossil.qj7.b()     // Catch:{ Exception -> 0x0084 }
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4     // Catch:{ Exception -> 0x0084 }
                    r5 = 0
                    r2.<init>(r7, r8, r5)     // Catch:{ Exception -> 0x0084 }
                    r0.L$0 = r7     // Catch:{ Exception -> 0x0084 }
                    r0.L$1 = r8     // Catch:{ Exception -> 0x0084 }
                    r0.label = r4     // Catch:{ Exception -> 0x0084 }
                    java.lang.Object r8 = com.fossil.vh7.a(r9, r2, r0)     // Catch:{ Exception -> 0x0084 }
                    if (r8 != r1) goto L_0x00b5
                    return r1
                L_0x0084:
                    r9 = move-exception
                    r8 = r7
                L_0x0086:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r1 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r1 = r1.getTAG()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r2.append(r3)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r8 = r8.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r8 = r8.this$0
                    java.util.Date r8 = r8.$date
                    r2.append(r8)
                    java.lang.String r8 = " exception="
                    r2.append(r8)
                    r2.append(r9)
                    java.lang.String r8 = r2.toString()
                    r0.e(r1, r8)
                    r9.printStackTrace()
                L_0x00b5:
                    com.fossil.i97 r8 = com.fossil.i97.a
                    return r8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary, com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            public boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
                return this.$pendingList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getSummary$Anon2;
            this.$goalTrackingDb = goalTrackingDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<GoalTrackingSummary>> apply(List<GoalTrackingData> list) {
            return new Anon1_Level3(this, list).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2 = new GoalTrackingRepository$getSummary$Anon2(this.this$0, this.$date, fb7);
        goalTrackingRepository$getSummary$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends GoalTrackingSummary>>> fb7) {
        return ((GoalTrackingRepository$getSummary$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        GoalTrackingDatabase goalTrackingDatabase;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getSummary date=" + this.$date);
            ti7 b = qj7.b();
            GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2 goalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2 = new GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, goalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            goalTrackingDatabase = (GoalTrackingDatabase) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return ge.b((LiveData) obj, new Anon1_Level2(this, goalTrackingDatabase));
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDatabase goalTrackingDatabase2 = (GoalTrackingDatabase) obj;
        GoalTrackingRepository goalTrackingRepository = this.this$0;
        Date date = this.$date;
        this.L$0 = yi7;
        this.L$1 = goalTrackingDatabase2;
        this.label = 2;
        Object pendingGoalTrackingDataListLiveData = goalTrackingRepository.getPendingGoalTrackingDataListLiveData(date, date, this);
        if (pendingGoalTrackingDataListLiveData == a) {
            return a;
        }
        goalTrackingDatabase = goalTrackingDatabase2;
        obj = pendingGoalTrackingDataListLiveData;
        return ge.b((LiveData) obj, new Anon1_Level2(this, goalTrackingDatabase));
    }
}
