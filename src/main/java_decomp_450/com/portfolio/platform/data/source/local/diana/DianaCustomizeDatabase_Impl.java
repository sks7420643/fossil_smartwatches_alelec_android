package com.portfolio.platform.data.source.local.diana;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.RingStyleDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase_Impl extends DianaCustomizeDatabase {
    @DexIgnore
    public volatile ComplicationDao _complicationDao;
    @DexIgnore
    public volatile ComplicationLastSettingDao _complicationLastSettingDao;
    @DexIgnore
    public volatile DianaPresetDao _dianaPresetDao;
    @DexIgnore
    public volatile RingStyleDao _ringStyleDao;
    @DexIgnore
    public volatile WatchAppDao _watchAppDao;
    @DexIgnore
    public volatile WatchAppDataDao _watchAppDataDao;
    @DexIgnore
    public volatile WatchAppLastSettingDao _watchAppLastSettingDao;
    @DexIgnore
    public volatile WatchFaceDao _watchFaceDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `complication` (`complicationId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `watchApp` (`watchappId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `icon` TEXT, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, PRIMARY KEY(`watchappId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `complicationLastSetting` (`complicationId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `watchAppLastSetting` (`watchAppId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`watchAppId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `dianaPreset` (`createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `name` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `dianaRecommendPreset` (`serialNumber` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `watchAppData` (`id` TEXT NOT NULL, `type` TEXT NOT NULL, `maxAppVersion` TEXT NOT NULL, `maxFirmwareOSVersion` TEXT NOT NULL, `minAppVersion` TEXT NOT NULL, `minFirmwareOSVersion` TEXT NOT NULL, `version` TEXT NOT NULL, `executableBinaryDataUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '683f46ee26915405748ecf5b2347e36e')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `complication`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watchApp`");
            wiVar.execSQL("DROP TABLE IF EXISTS `complicationLastSetting`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watchAppLastSetting`");
            wiVar.execSQL("DROP TABLE IF EXISTS `dianaPreset`");
            wiVar.execSQL("DROP TABLE IF EXISTS `dianaRecommendPreset`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watch_face`");
            wiVar.execSQL("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watchAppData`");
            if (((ci) DianaCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) DianaCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) DianaCustomizeDatabase_Impl.this).mDatabase = wiVar;
            DianaCustomizeDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) DianaCustomizeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DianaCustomizeDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("complicationId", new ti.a("complicationId", "TEXT", true, 1, null, 1));
            hashMap.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap.put("nameKey", new ti.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap.put("categories", new ti.a("categories", "TEXT", true, 0, null, 1));
            hashMap.put("description", new ti.a("description", "TEXT", true, 0, null, 1));
            hashMap.put("descriptionKey", new ti.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap.put("icon", new ti.a("icon", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("complication", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "complication");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "complication(com.portfolio.platform.data.model.diana.Complication).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("watchappId", new ti.a("watchappId", "TEXT", true, 1, null, 1));
            hashMap2.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap2.put("nameKey", new ti.a("nameKey", "TEXT", true, 0, null, 1));
            hashMap2.put("description", new ti.a("description", "TEXT", true, 0, null, 1));
            hashMap2.put("descriptionKey", new ti.a("descriptionKey", "TEXT", true, 0, null, 1));
            hashMap2.put("categories", new ti.a("categories", "TEXT", true, 0, null, 1));
            hashMap2.put("icon", new ti.a("icon", "TEXT", false, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap2.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            ti tiVar2 = new ti("watchApp", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "watchApp");
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "watchApp(com.portfolio.platform.data.model.diana.WatchApp).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(3);
            hashMap3.put("complicationId", new ti.a("complicationId", "TEXT", true, 1, null, 1));
            hashMap3.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap3.put(MicroAppSetting.SETTING, new ti.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ti tiVar3 = new ti("complicationLastSetting", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "complicationLastSetting");
            if (!tiVar3.equals(a3)) {
                return new ei.b(false, "complicationLastSetting(com.portfolio.platform.data.model.diana.ComplicationLastSetting).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(3);
            hashMap4.put("watchAppId", new ti.a("watchAppId", "TEXT", true, 1, null, 1));
            hashMap4.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap4.put(MicroAppSetting.SETTING, new ti.a(MicroAppSetting.SETTING, "TEXT", true, 0, null, 1));
            ti tiVar4 = new ti("watchAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, "watchAppLastSetting");
            if (!tiVar4.equals(a4)) {
                return new ei.b(false, "watchAppLastSetting(com.portfolio.platform.data.model.diana.WatchAppLastSetting).\n Expected:\n" + tiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap5.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap5.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap5.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap5.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap5.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap5.put("isActive", new ti.a("isActive", "INTEGER", true, 0, null, 1));
            hashMap5.put("complications", new ti.a("complications", "TEXT", true, 0, null, 1));
            hashMap5.put("watchapps", new ti.a("watchapps", "TEXT", true, 0, null, 1));
            hashMap5.put("watchFaceId", new ti.a("watchFaceId", "TEXT", true, 0, null, 1));
            ti tiVar5 = new ti("dianaPreset", hashMap5, new HashSet(0), new HashSet(0));
            ti a5 = ti.a(wiVar, "dianaPreset");
            if (!tiVar5.equals(a5)) {
                return new ei.b(false, "dianaPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset).\n Expected:\n" + tiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(9);
            hashMap6.put("serialNumber", new ti.a("serialNumber", "TEXT", true, 0, null, 1));
            hashMap6.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap6.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap6.put("isDefault", new ti.a("isDefault", "INTEGER", true, 0, null, 1));
            hashMap6.put("complications", new ti.a("complications", "TEXT", true, 0, null, 1));
            hashMap6.put("watchapps", new ti.a("watchapps", "TEXT", true, 0, null, 1));
            hashMap6.put("watchFaceId", new ti.a("watchFaceId", "TEXT", true, 0, null, 1));
            hashMap6.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap6.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            ti tiVar6 = new ti("dianaRecommendPreset", hashMap6, new HashSet(0), new HashSet(0));
            ti a6 = ti.a(wiVar, "dianaRecommendPreset");
            if (!tiVar6.equals(a6)) {
                return new ei.b(false, "dianaRecommendPreset(com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset).\n Expected:\n" + tiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(7);
            hashMap7.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap7.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap7.put("ringStyleItems", new ti.a("ringStyleItems", "TEXT", false, 0, null, 1));
            hashMap7.put(Explore.COLUMN_BACKGROUND, new ti.a(Explore.COLUMN_BACKGROUND, "TEXT", true, 0, null, 1));
            hashMap7.put("previewUrl", new ti.a("previewUrl", "TEXT", true, 0, null, 1));
            hashMap7.put("serial", new ti.a("serial", "TEXT", true, 0, null, 1));
            hashMap7.put("watchFaceType", new ti.a("watchFaceType", "INTEGER", true, 0, null, 1));
            ti tiVar7 = new ti("watch_face", hashMap7, new HashSet(0), new HashSet(0));
            ti a7 = ti.a(wiVar, "watch_face");
            if (!tiVar7.equals(a7)) {
                return new ei.b(false, "watch_face(com.portfolio.platform.data.model.diana.preset.WatchFace).\n Expected:\n" + tiVar7 + "\n Found:\n" + a7);
            }
            HashMap hashMap8 = new HashMap(6);
            hashMap8.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap8.put("category", new ti.a("category", "TEXT", true, 0, null, 1));
            hashMap8.put("name", new ti.a("name", "TEXT", true, 0, null, 1));
            hashMap8.put("data", new ti.a("data", "TEXT", true, 0, null, 1));
            hashMap8.put("metaData", new ti.a("metaData", "TEXT", true, 0, null, 1));
            hashMap8.put("serial", new ti.a("serial", "TEXT", true, 0, null, 1));
            ti tiVar8 = new ti("DianaComplicationRingStyle", hashMap8, new HashSet(0), new HashSet(0));
            ti a8 = ti.a(wiVar, "DianaComplicationRingStyle");
            if (!tiVar8.equals(a8)) {
                return new ei.b(false, "DianaComplicationRingStyle(com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle).\n Expected:\n" + tiVar8 + "\n Found:\n" + a8);
            }
            HashMap hashMap9 = new HashMap(10);
            hashMap9.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap9.put("type", new ti.a("type", "TEXT", true, 0, null, 1));
            hashMap9.put("maxAppVersion", new ti.a("maxAppVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("maxFirmwareOSVersion", new ti.a("maxFirmwareOSVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("minAppVersion", new ti.a("minAppVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("minFirmwareOSVersion", new ti.a("minFirmwareOSVersion", "TEXT", true, 0, null, 1));
            hashMap9.put("version", new ti.a("version", "TEXT", true, 0, null, 1));
            hashMap9.put("executableBinaryDataUrl", new ti.a("executableBinaryDataUrl", "TEXT", true, 0, null, 1));
            hashMap9.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap9.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            ti tiVar9 = new ti("watchAppData", hashMap9, new HashSet(0), new HashSet(0));
            ti a9 = ti.a(wiVar, "watchAppData");
            if (tiVar9.equals(a9)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "watchAppData(com.portfolio.platform.data.model.diana.WatchAppData).\n Expected:\n" + tiVar9 + "\n Found:\n" + a9);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `complication`");
            writableDatabase.execSQL("DELETE FROM `watchApp`");
            writableDatabase.execSQL("DELETE FROM `complicationLastSetting`");
            writableDatabase.execSQL("DELETE FROM `watchAppLastSetting`");
            writableDatabase.execSQL("DELETE FROM `dianaPreset`");
            writableDatabase.execSQL("DELETE FROM `dianaRecommendPreset`");
            writableDatabase.execSQL("DELETE FROM `watch_face`");
            writableDatabase.execSQL("DELETE FROM `DianaComplicationRingStyle`");
            writableDatabase.execSQL("DELETE FROM `watchAppData`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "complication", "watchApp", "complicationLastSetting", "watchAppLastSetting", "dianaPreset", "dianaRecommendPreset", "watch_face", "DianaComplicationRingStyle", "watchAppData");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(15), "683f46ee26915405748ecf5b2347e36e", "d0cc30c9de9f14c69678c44a79a24e8e");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public ComplicationDao getComplicationDao() {
        ComplicationDao complicationDao;
        if (this._complicationDao != null) {
            return this._complicationDao;
        }
        synchronized (this) {
            if (this._complicationDao == null) {
                this._complicationDao = new ComplicationDao_Impl(this);
            }
            complicationDao = this._complicationDao;
        }
        return complicationDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public ComplicationLastSettingDao getComplicationSettingDao() {
        ComplicationLastSettingDao complicationLastSettingDao;
        if (this._complicationLastSettingDao != null) {
            return this._complicationLastSettingDao;
        }
        synchronized (this) {
            if (this._complicationLastSettingDao == null) {
                this._complicationLastSettingDao = new ComplicationLastSettingDao_Impl(this);
            }
            complicationLastSettingDao = this._complicationLastSettingDao;
        }
        return complicationLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public DianaPresetDao getPresetDao() {
        DianaPresetDao dianaPresetDao;
        if (this._dianaPresetDao != null) {
            return this._dianaPresetDao;
        }
        synchronized (this) {
            if (this._dianaPresetDao == null) {
                this._dianaPresetDao = new DianaPresetDao_Impl(this);
            }
            dianaPresetDao = this._dianaPresetDao;
        }
        return dianaPresetDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public RingStyleDao getRingStyleDao() {
        RingStyleDao ringStyleDao;
        if (this._ringStyleDao != null) {
            return this._ringStyleDao;
        }
        synchronized (this) {
            if (this._ringStyleDao == null) {
                this._ringStyleDao = new RingStyleDao_Impl(this);
            }
            ringStyleDao = this._ringStyleDao;
        }
        return ringStyleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppDao getWatchAppDao() {
        WatchAppDao watchAppDao;
        if (this._watchAppDao != null) {
            return this._watchAppDao;
        }
        synchronized (this) {
            if (this._watchAppDao == null) {
                this._watchAppDao = new WatchAppDao_Impl(this);
            }
            watchAppDao = this._watchAppDao;
        }
        return watchAppDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppDataDao getWatchAppDataDao() {
        WatchAppDataDao watchAppDataDao;
        if (this._watchAppDataDao != null) {
            return this._watchAppDataDao;
        }
        synchronized (this) {
            if (this._watchAppDataDao == null) {
                this._watchAppDataDao = new WatchAppDataDao_Impl(this);
            }
            watchAppDataDao = this._watchAppDataDao;
        }
        return watchAppDataDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchAppLastSettingDao getWatchAppSettingDao() {
        WatchAppLastSettingDao watchAppLastSettingDao;
        if (this._watchAppLastSettingDao != null) {
            return this._watchAppLastSettingDao;
        }
        synchronized (this) {
            if (this._watchAppLastSettingDao == null) {
                this._watchAppLastSettingDao = new WatchAppLastSettingDao_Impl(this);
            }
            watchAppLastSettingDao = this._watchAppLastSettingDao;
        }
        return watchAppLastSettingDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase
    public WatchFaceDao getWatchFaceDao() {
        WatchFaceDao watchFaceDao;
        if (this._watchFaceDao != null) {
            return this._watchFaceDao;
        }
        synchronized (this) {
            if (this._watchFaceDao == null) {
                this._watchFaceDao = new WatchFaceDao_Impl(this);
            }
            watchFaceDao = this._watchFaceDao;
        }
        return watchFaceDao;
    }
}
