package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.source.SleepSessionsRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$pushPendingSleepSessions$2", f = "SleepSessionsRepository.kt", l = {240, 241, 244}, m = "invokeSuspend")
public final class SleepSessionsRepository$pushPendingSleepSessions$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository.PushPendingSleepSessionsCallback $pushPendingSleepSessionsCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$pushPendingSleepSessions$Anon2(SleepSessionsRepository sleepSessionsRepository, SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSessionsRepository;
        this.$pushPendingSleepSessionsCallback = pushPendingSleepSessionsCallback;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$pushPendingSleepSessions$Anon2 sleepSessionsRepository$pushPendingSleepSessions$Anon2 = new SleepSessionsRepository$pushPendingSleepSessions$Anon2(this.this$0, this.$pushPendingSleepSessionsCallback, fb7);
        sleepSessionsRepository$pushPendingSleepSessions$Anon2.p$ = (yi7) obj;
        return sleepSessionsRepository$pushPendingSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((SleepSessionsRepository$pushPendingSleepSessions$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r8.label
            r2 = 3
            r3 = 2
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x003f
            if (r1 == r5) goto L_0x0037
            if (r1 == r3) goto L_0x002b
            if (r1 != r2) goto L_0x0023
            java.lang.Object r0 = r8.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r8.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r8.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r9)
            goto L_0x00a1
        L_0x0023:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x002b:
            java.lang.Object r1 = r8.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r3 = r8.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r9)
            goto L_0x0075
        L_0x0037:
            java.lang.Object r1 = r8.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r9)
            goto L_0x0054
        L_0x003f:
            com.fossil.t87.a(r9)
            com.fossil.yi7 r9 = r8.p$
            com.fossil.pg5 r1 = com.fossil.pg5.i
            r8.L$0 = r9
            r8.label = r5
            java.lang.Object r1 = r1.d(r8)
            if (r1 != r0) goto L_0x0051
            return r0
        L_0x0051:
            r7 = r1
            r1 = r9
            r9 = r7
        L_0x0054:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r9 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r9
            com.portfolio.platform.data.source.local.sleep.SleepDao r9 = r9.sleepDao()
            java.util.List r9 = r9.getPendingSleepSessions()
            com.portfolio.platform.data.source.SleepSessionsRepository r6 = r8.this$0
            com.portfolio.platform.data.source.UserRepository r6 = r6.mUserRepository
            r8.L$0 = r1
            r8.L$1 = r9
            r8.label = r3
            java.lang.Object r3 = r6.getCurrentUser(r8)
            if (r3 != r0) goto L_0x0071
            return r0
        L_0x0071:
            r7 = r1
            r1 = r9
            r9 = r3
            r3 = r7
        L_0x0075:
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            if (r9 == 0) goto L_0x007e
            java.lang.String r9 = r9.getUserId()
            goto L_0x007f
        L_0x007e:
            r9 = r4
        L_0x007f:
            boolean r6 = r1.isEmpty()
            r5 = r5 ^ r6
            if (r5 == 0) goto L_0x00a8
            boolean r5 = android.text.TextUtils.isEmpty(r9)
            if (r5 != 0) goto L_0x00a8
            com.portfolio.platform.data.source.SleepSessionsRepository r5 = r8.this$0
            if (r9 == 0) goto L_0x00a4
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r4 = r8.$pushPendingSleepSessionsCallback
            r8.L$0 = r3
            r8.L$1 = r1
            r8.L$2 = r9
            r8.label = r2
            java.lang.Object r9 = r5.saveSleepSessionsToServer(r9, r1, r4, r8)
            if (r9 != r0) goto L_0x00a1
            return r0
        L_0x00a1:
            com.fossil.i97 r4 = com.fossil.i97.a
            goto L_0x00b3
        L_0x00a4:
            com.fossil.ee7.a()
            throw r4
        L_0x00a8:
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r9 = r8.$pushPendingSleepSessionsCallback
            if (r9 == 0) goto L_0x00b3
            r0 = 404(0x194, float:5.66E-43)
            r9.onFail(r0)
            com.fossil.i97 r4 = com.fossil.i97.a
        L_0x00b3:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$pushPendingSleepSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
