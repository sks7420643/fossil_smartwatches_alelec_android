package com.portfolio.platform.data.source;

import com.fossil.fb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AlarmsDataSource {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    Object deleteAlarm(Alarm alarm, fb7<? super zi5<Alarm>> fb7);

    @DexIgnore
    Object findAlarm(String str, fb7<? super Alarm> fb7);

    @DexIgnore
    Object getActiveAlarms(fb7<? super List<Alarm>> fb7);

    @DexIgnore
    Object getAlarms(fb7<? super zi5<List<Alarm>>> fb7);

    @DexIgnore
    Object getAllAlarmIgnorePinType(fb7<? super List<Alarm>> fb7);

    @DexIgnore
    Object setAlarm(Alarm alarm, fb7<? super zi5<Alarm>> fb7);
}
