package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.be4;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.x97;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$2", f = "HeartRateSummaryRepository.kt", l = {53}, m = "invokeSuspend")
public final class HeartRateSummaryRepository$getHeartRateSummaries$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<DailyHeartRateSummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository$getHeartRateSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<DailyHeartRateSummary>, ApiResponse<ie4>> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<ie4>>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiService$p = this.this$0.this$0.this$0.mApiService;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiService$p.getDailyHeartRateSummaries(g, g2, 0, 100, fb7);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object loadFromDb(fb7<? super LiveData<List<DailyHeartRateSummary>>> fb7) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = HeartRateSummaryRepository.TAG;
                local.d(access$getTAG$cp, "getHeartRateSummaries isNotToday startDate = " + this.this$0.this$0.$startDate + ", endDate = " + this.this$0.this$0.$endDate);
                HeartRateDailySummaryDao heartRateDailySummaryDao = this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao();
                HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0.this$0;
                return heartRateDailySummaryDao.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate);
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, "getHeartRateSummaries onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<ie4> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<ie4> apiResponse, fb7<? super i97> fb7) {
                Date date;
                Date date2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = HeartRateSummaryRepository.TAG;
                local.d(access$getTAG$cp, "saveCallResult onResponse: response = " + apiResponse);
                try {
                    if (!apiResponse.get_items().isEmpty()) {
                        be4 be4 = new be4();
                        be4.a(Long.TYPE, new GsonConvertDateTimeToLong());
                        be4.a(DateTime.class, new GsonConvertDateTime());
                        be4.a(Date.class, new GsonConverterShortDate());
                        Gson a = be4.a();
                        List<ie4> list = apiResponse.get_items();
                        ArrayList arrayList = new ArrayList(x97.a(list, 10));
                        Iterator<T> it = list.iterator();
                        while (it.hasNext()) {
                            arrayList.add((DailyHeartRateSummary) a.a((JsonElement) it.next(), new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$summaries$Anon1_Level4$Anon1_Level5().getType()));
                        }
                        List<DailyHeartRateSummary> d = ea7.d((Collection) arrayList);
                        FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, String.valueOf(d));
                        this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(d);
                        FitnessDataDao fitnessDataDao = this.this$0.$fitnessDatabase.getFitnessDataDao();
                        r87 r87 = this.$downloadingDate;
                        if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                            date = this.this$0.this$0.$startDate;
                        }
                        r87 r872 = this.$downloadingDate;
                        if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                            date2 = this.this$0.this$0.$endDate;
                        }
                        List<FitnessDataWrapper> fitnessData = fitnessDataDao.getFitnessData(date, date2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String access$getTAG$cp2 = HeartRateSummaryRepository.TAG;
                        local2.d(access$getTAG$cp2, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                        if (fitnessData.isEmpty()) {
                            this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(d);
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String access$getTAG$cp3 = HeartRateSummaryRepository.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("saveCallResult exception=");
                    e.printStackTrace();
                    sb.append(i97.a);
                    local3.e(access$getTAG$cp3, sb.toString());
                }
                return i97.a;
            }

            @DexIgnore
            public boolean shouldFetch(List<DailyHeartRateSummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = heartRateSummaryRepository$getHeartRateSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<qx6<List<DailyHeartRateSummary>>> apply(List<FitnessDataWrapper> list) {
            ee7.a((Object) list, "fitnessDataList");
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate)).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getHeartRateSummaries$Anon2(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, fb7);
        heartRateSummaryRepository$getHeartRateSummaries$Anon2.p$ = (yi7) obj;
        return heartRateSummaryRepository$getHeartRateSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<DailyHeartRateSummary>>>> fb7) {
        return ((HeartRateSummaryRepository$getHeartRateSummaries$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = HeartRateSummaryRepository.TAG;
            local.d(access$getTAG$cp, "getHeartRateSummaries: startDate = " + this.$startDate + ", endDate = " + this.$endDate);
            ti7 b = qj7.b();
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = yi7;
            this.label = 1;
            obj = vh7.a(b, heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) obj;
        return ge.b(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
