package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkuDao_Impl implements SkuDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<SKUModel> __insertionAdapterOfSKUModel;
    @DexIgnore
    public /* final */ vh<WatchParam> __insertionAdapterOfWatchParam;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUpSku;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUpWatchParam;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<SKUModel> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SKU` (`createdAt`,`updatedAt`,`serialNumberPrefix`,`sku`,`deviceName`,`groupName`,`gender`,`deviceType`,`fastPairId`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, SKUModel sKUModel) {
            if (sKUModel.getCreatedAt() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, sKUModel.getCreatedAt());
            }
            if (sKUModel.getUpdatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, sKUModel.getUpdatedAt());
            }
            if (sKUModel.getSerialNumberPrefix() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, sKUModel.getSerialNumberPrefix());
            }
            if (sKUModel.getSku() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, sKUModel.getSku());
            }
            if (sKUModel.getDeviceName() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, sKUModel.getDeviceName());
            }
            if (sKUModel.getGroupName() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, sKUModel.getGroupName());
            }
            if (sKUModel.getGender() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, sKUModel.getGender());
            }
            if (sKUModel.getDeviceType() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, sKUModel.getDeviceType());
            }
            if (sKUModel.getFastPairId() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, sKUModel.getFastPairId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<WatchParam> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchParam` (`prefixSerial`,`versionMajor`,`versionMinor`,`data`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, WatchParam watchParam) {
            if (watchParam.getPrefixSerial() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, watchParam.getPrefixSerial());
            }
            if (watchParam.getVersionMajor() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, watchParam.getVersionMajor());
            }
            if (watchParam.getVersionMinor() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, watchParam.getVersionMinor());
            }
            if (watchParam.getData() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, watchParam.getData());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM SKU";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends ji {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM watchParam";
        }
    }

    @DexIgnore
    public SkuDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfSKUModel = new Anon1(ciVar);
        this.__insertionAdapterOfWatchParam = new Anon2(ciVar);
        this.__preparedStmtOfCleanUpSku = new Anon3(ciVar);
        this.__preparedStmtOfCleanUpWatchParam = new Anon4(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateSkuList(List<SKUModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSKUModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateWatchParam(WatchParam watchParam) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchParam.insert(watchParam);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpSku() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUpSku.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpSku.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpWatchParam() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUpWatchParam.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpWatchParam.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public List<SKUModel> getAllSkus() {
        fi b = fi.b("SELECT * FROM SKU", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "serialNumberPrefix");
            int b5 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = oi.b(a, "deviceName");
            int b7 = oi.b(a, "groupName");
            int b8 = oi.b(a, "gender");
            int b9 = oi.b(a, "deviceType");
            int b10 = oi.b(a, "fastPairId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                SKUModel sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
                arrayList.add(sKUModel);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public SKUModel getSkuByDeviceIdPrefix(String str) {
        fi b = fi.b("SELECT * FROM SKU WHERE serialNumberPrefix=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        SKUModel sKUModel = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "createdAt");
            int b3 = oi.b(a, "updatedAt");
            int b4 = oi.b(a, "serialNumberPrefix");
            int b5 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = oi.b(a, "deviceName");
            int b7 = oi.b(a, "groupName");
            int b8 = oi.b(a, "gender");
            int b9 = oi.b(a, "deviceType");
            int b10 = oi.b(a, "fastPairId");
            if (a.moveToFirst()) {
                sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
            }
            return sKUModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public WatchParam getWatchParamById(String str) {
        fi b = fi.b("SELECT * FROM watchParam WHERE prefixSerial =?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchParam watchParam = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "prefixSerial");
            int b3 = oi.b(a, "versionMajor");
            int b4 = oi.b(a, "versionMinor");
            int b5 = oi.b(a, "data");
            if (a.moveToFirst()) {
                watchParam = new WatchParam(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5));
            }
            return watchParam;
        } finally {
            a.close();
            b.c();
        }
    }
}
