package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `goalTrackingRaw`");
            wiVar.execSQL("DROP TABLE IF EXISTS `goalTrackingDay`");
            wiVar.execSQL("DROP TABLE IF EXISTS `goalSetting`");
            if (((ci) GoalTrackingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) GoalTrackingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) GoalTrackingDatabase_Impl.this).mDatabase = wiVar;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) GoalTrackingDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) GoalTrackingDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put(GoalTrackingEvent.COLUMN_TRACKED_AT, new ti.a(GoalTrackingEvent.COLUMN_TRACKED_AT, "TEXT", true, 0, null, 1));
            hashMap.put("timezoneOffsetInSecond", new ti.a("timezoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new ti.a("date", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "goalTrackingRaw");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new ti.a("date", "TEXT", true, 1, null, 1));
            hashMap2.put("totalTracked", new ti.a("totalTracked", "INTEGER", true, 0, null, 1));
            hashMap2.put("goalTarget", new ti.a("goalTarget", "INTEGER", true, 0, null, 1));
            hashMap2.put("createdAt", new ti.a("createdAt", "INTEGER", true, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "INTEGER", true, 0, null, 1));
            ti tiVar2 = new ti("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "goalTrackingDay");
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("currentTarget", new ti.a("currentTarget", "INTEGER", true, 0, null, 1));
            ti tiVar3 = new ti("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "goalSetting");
            if (tiVar3.equals(a3)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `goalTrackingRaw`");
            writableDatabase.execSQL("DELETE FROM `goalTrackingDay`");
            writableDatabase.execSQL("DELETE FROM `goalSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "goalTrackingRaw", "goalTrackingDay", "goalSetting");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
