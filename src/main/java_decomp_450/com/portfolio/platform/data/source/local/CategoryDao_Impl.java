package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDao_Impl implements CategoryDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<Category> __insertionAdapterOfCategory;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<Category> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `category` (`id`,`englishName`,`name`,`updatedAt`,`createdAt`,`priority`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, Category category) {
            if (category.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, category.getId());
            }
            if (category.getEnglishName() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, category.getEnglishName());
            }
            if (category.getName() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, category.getName());
            }
            if (category.getUpdatedAt() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, category.getUpdatedAt());
            }
            if (category.getCreatedAt() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, category.getCreatedAt());
            }
            ajVar.bindLong(6, (long) category.getPriority());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM category";
        }
    }

    @DexIgnore
    public CategoryDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfCategory = new Anon1(ciVar);
        this.__preparedStmtOfClearData = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public List<Category> getAllCategory() {
        fi b = fi.b("SELECT * FROM category ORDER BY priority DESC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "englishName");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "updatedAt");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "priority");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Category(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public Category getCategoryById(String str) {
        fi b = fi.b("SELECT * FROM category WHERE id=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Category category = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "englishName");
            int b4 = oi.b(a, "name");
            int b5 = oi.b(a, "updatedAt");
            int b6 = oi.b(a, "createdAt");
            int b7 = oi.b(a, "priority");
            if (a.moveToFirst()) {
                category = new Category(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7));
            }
            return category;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void upsertCategoryList(List<Category> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCategory.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
