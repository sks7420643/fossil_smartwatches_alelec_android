package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchLastSleepGoal$2", f = "SleepSummariesRepository.kt", l = {122, 126}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchLastSleepGoal$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<MFSleepSettings>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchLastSleepGoal$Anon2(SleepSummariesRepository sleepSummariesRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$fetchLastSleepGoal$Anon2 sleepSummariesRepository$fetchLastSleepGoal$Anon2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2(this.this$0, fb7);
        sleepSummariesRepository$fetchLastSleepGoal$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$fetchLastSleepGoal$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<MFSleepSettings>> fb7) {
        return ((SleepSummariesRepository$fetchLastSleepGoal$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        zi5 zi5;
        yi7 yi7;
        String message;
        Object a = nb7.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "fetchLastSleepGoal");
            SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return zi5;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            bj5 bj5 = (bj5) zi52;
            if (bj5.a() == null || bj5.b()) {
                return zi52;
            }
            SleepSummariesRepository sleepSummariesRepository = this.this$0;
            int sleepGoal = ((MFSleepSettings) bj5.a()).getSleepGoal();
            this.L$0 = yi7;
            this.L$1 = zi52;
            this.label = 2;
            if (sleepSummariesRepository.saveSleepSettingToDB(sleepGoal, this) == a) {
                return a;
            }
            zi5 = zi52;
            return zi5;
        } else if (!(zi52 instanceof yi5)) {
            return zi52;
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = SleepSummariesRepository.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("fetchActivitySettings Failure code=");
            yi5 yi5 = (yi5) zi52;
            sb.append(yi5.a());
            sb.append(" message=");
            ServerError c = yi5.c();
            if (c == null || (message = c.getMessage()) == null) {
                ServerError c2 = yi5.c();
                if (c2 != null) {
                    str = c2.getUserMessage();
                }
            } else {
                str = message;
            }
            if (str == null) {
                str = "";
            }
            sb.append(str);
            local.d(access$getTAG$cp, sb.toString());
            return zi52;
        }
    }
}
