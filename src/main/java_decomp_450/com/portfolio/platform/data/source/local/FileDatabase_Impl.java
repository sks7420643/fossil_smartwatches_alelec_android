package com.portfolio.platform.data.source.local;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase_Impl extends FileDatabase {
    @DexIgnore
    public volatile FileDao _fileDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `localfile` (`pinType` INTEGER NOT NULL, `type` TEXT NOT NULL, `fileName` TEXT NOT NULL, `localUri` TEXT NOT NULL, `remoteUrl` TEXT NOT NULL, `checksum` TEXT, PRIMARY KEY(`remoteUrl`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '415513f336004021a41b46790e07d643')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `localfile`");
            if (((ci) FileDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FileDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FileDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) FileDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FileDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FileDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) FileDatabase_Impl.this).mDatabase = wiVar;
            FileDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) FileDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) FileDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) FileDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("type", new ti.a("type", "TEXT", true, 0, null, 1));
            hashMap.put("fileName", new ti.a("fileName", "TEXT", true, 0, null, 1));
            hashMap.put("localUri", new ti.a("localUri", "TEXT", true, 0, null, 1));
            hashMap.put("remoteUrl", new ti.a("remoteUrl", "TEXT", true, 1, null, 1));
            hashMap.put("checksum", new ti.a("checksum", "TEXT", false, 0, null, 1));
            ti tiVar = new ti("localfile", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "localfile");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "localfile(com.portfolio.platform.data.model.LocalFile).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `localfile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "localfile");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(2), "415513f336004021a41b46790e07d643", "f5892aa2843b6abf4eab005d74132c1f");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDatabase
    public FileDao fileDao() {
        FileDao fileDao;
        if (this._fileDao != null) {
            return this._fileDao;
        }
        synchronized (this) {
            if (this._fileDao == null) {
                this._fileDao = new FileDao_Impl(this);
            }
            fileDao = this._fileDao;
        }
        return fileDao;
    }
}
