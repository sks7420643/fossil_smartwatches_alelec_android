package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.te5;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource$loadInitial$Anon1 implements te5.b {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.te5.b
    public final void run(te5.b.a aVar) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$0;
        te5.d dVar = te5.d.INITIAL;
        Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        ee7.a((Object) aVar, "helperCallback");
        ik7 unused = sleepSummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
