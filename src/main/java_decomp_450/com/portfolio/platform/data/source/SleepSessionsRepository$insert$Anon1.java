package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fb7;
import com.fossil.rb7;
import com.fossil.tb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository", f = "SleepSessionsRepository.kt", l = {132}, m = "insert")
public final class SleepSessionsRepository$insert$Anon1 extends rb7 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$insert$Anon1(SleepSessionsRepository sleepSessionsRepository, fb7 fb7) {
        super(fb7);
        this.this$0 = sleepSessionsRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.insert(null, this);
    }
}
