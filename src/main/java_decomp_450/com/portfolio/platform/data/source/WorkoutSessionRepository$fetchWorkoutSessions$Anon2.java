package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$2", f = "WorkoutSessionRepository.kt", l = {183, 194, 203}, m = "invokeSuspend")
public final class WorkoutSessionRepository$fetchWorkoutSessions$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<ServerWorkoutSession>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$fetchWorkoutSessions$Anon2(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        WorkoutSessionRepository$fetchWorkoutSessions$Anon2 workoutSessionRepository$fetchWorkoutSessions$Anon2 = new WorkoutSessionRepository$fetchWorkoutSessions$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, fb7);
        workoutSessionRepository$fetchWorkoutSessions$Anon2.p$ = (yi7) obj;
        return workoutSessionRepository$fetchWorkoutSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<ServerWorkoutSession>>> fb7) {
        return ((WorkoutSessionRepository$fetchWorkoutSessions$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
            r12 = this;
            java.lang.Object r6 = com.fossil.nb7.a()
            int r0 = r12.label
            r1 = 3
            r2 = 2
            r3 = 1
            r4 = 0
            if (r0 == 0) goto L_0x0048
            if (r0 == r3) goto L_0x003f
            if (r0 == r2) goto L_0x002c
            if (r0 != r1) goto L_0x0024
            java.lang.Object r0 = r12.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r12.L$1
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            java.lang.Object r0 = r12.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r13)
            r0 = r13
            goto L_0x01c0
        L_0x0024:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x002c:
            java.lang.Object r0 = r12.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r2 = r12.L$1
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r3 = r12.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r13)
            r5 = r2
            r2 = r13
            goto L_0x0151
        L_0x003f:
            java.lang.Object r0 = r12.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r13)
            r5 = r13
            goto L_0x0089
        L_0x0048:
            com.fossil.t87.a(r13)
            com.fossil.yi7 r0 = r12.p$
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r7 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r7 = r7.getTAG$app_fossilRelease()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "fetchWorkoutSessions - start="
            r8.append(r9)
            java.util.Date r9 = r12.$start
            r8.append(r9)
            java.lang.String r9 = ", end="
            r8.append(r9)
            java.util.Date r9 = r12.$end
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r5.d(r7, r8)
            com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2 r5 = new com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$Anon2$repoResponse$Anon1_Level2
            r5.<init>(r12, r4)
            r12.L$0 = r0
            r12.label = r3
            java.lang.Object r5 = com.fossil.aj5.a(r5, r12)
            if (r5 != r6) goto L_0x0089
            return r6
        L_0x0089:
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            boolean r7 = r5 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x01c9
            r7 = r5
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r8 = r7.a()
            if (r8 == 0) goto L_0x01c8
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r9 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r9 = r9.getTAG$app_fossilRelease()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "fetchWorkoutSessions - Success -- item.size="
            r10.append(r11)
            java.lang.Object r11 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            java.util.List r11 = r11.get_items()
            int r11 = r11.size()
            r10.append(r11)
            java.lang.String r11 = ", hasNext="
            r10.append(r11)
            java.lang.Object r11 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            com.portfolio.platform.data.model.Range r11 = r11.get_range()
            if (r11 == 0) goto L_0x00d9
            boolean r11 = r11.isHasNext()
            java.lang.Boolean r11 = com.fossil.pb7.a(r11)
            goto L_0x00da
        L_0x00d9:
            r11 = r4
        L_0x00da:
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r8.d(r9, r10)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r9 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r9 = r9.getTAG$app_fossilRelease()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "fetchWorkoutSessions - Success -- items="
            r10.append(r11)
            java.lang.Object r11 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            java.util.List r11 = r11.get_items()
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r8.d(r9, r10)
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.Object r7 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            java.util.List r7 = r7.get_items()
            java.util.Iterator r7 = r7.iterator()
        L_0x0121:
            boolean r9 = r7.hasNext()
            if (r9 == 0) goto L_0x0137
            java.lang.Object r9 = r7.next()
            com.portfolio.platform.data.ServerWorkoutSession r9 = (com.portfolio.platform.data.ServerWorkoutSession) r9
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r9 = r9.toWorkoutSession()
            if (r9 == 0) goto L_0x0121
            r8.add(r9)
            goto L_0x0121
        L_0x0137:
            boolean r7 = r8.isEmpty()
            r3 = r3 ^ r7
            if (r3 == 0) goto L_0x016e
            com.fossil.pg5 r3 = com.fossil.pg5.i
            r12.L$0 = r0
            r12.L$1 = r5
            r12.L$2 = r8
            r12.label = r2
            java.lang.Object r2 = r3.b(r12)
            if (r2 != r6) goto L_0x014f
            return r6
        L_0x014f:
            r3 = r0
            r0 = r8
        L_0x0151:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r2
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r2 = r2.getWorkoutDao()
            java.util.Date r7 = r12.$start
            java.util.Date r8 = r12.$end
            java.util.List r7 = r2.getWorkoutSessionsRaw(r7, r8)
            int r7 = r7.size()
            int r8 = r0.size()
            if (r7 == r8) goto L_0x016c
            r2.upsertListWorkoutSession(r0)
        L_0x016c:
            r8 = r0
            r0 = r3
        L_0x016e:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r3 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r3 = r3.getTAG$app_fossilRelease()
            java.lang.String r7 = "fetchWorkoutSessions - saveCallResult -- DONE!!!"
            r2.d(r3, r7)
            r2 = r5
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r3 = r2.a()
            com.portfolio.platform.data.source.remote.ApiResponse r3 = (com.portfolio.platform.data.source.remote.ApiResponse) r3
            com.portfolio.platform.data.model.Range r3 = r3.get_range()
            if (r3 == 0) goto L_0x01c8
            java.lang.Object r2 = r2.a()
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            com.portfolio.platform.data.model.Range r2 = r2.get_range()
            if (r2 == 0) goto L_0x01c4
            boolean r2 = r2.isHasNext()
            if (r2 == 0) goto L_0x01c8
            com.portfolio.platform.data.source.WorkoutSessionRepository r2 = r12.this$0
            java.util.Date r3 = r12.$start
            java.util.Date r4 = r12.$end
            int r7 = r12.$offset
            int r9 = r12.$limit
            int r7 = r7 + r9
            r12.L$0 = r0
            r12.L$1 = r5
            r12.L$2 = r8
            r12.label = r1
            r0 = r2
            r1 = r3
            r2 = r4
            r3 = r7
            r4 = r9
            r5 = r12
            java.lang.Object r0 = r0.fetchWorkoutSessions(r1, r2, r3, r4, r5)
            if (r0 != r6) goto L_0x01c0
            return r6
        L_0x01c0:
            r5 = r0
            com.fossil.zi5 r5 = (com.fossil.zi5) r5
            goto L_0x01c8
        L_0x01c4:
            com.fossil.ee7.a()
            throw r4
        L_0x01c8:
            return r5
        L_0x01c9:
            boolean r0 = r5 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0219
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r1 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r1 = r1.getTAG$app_fossilRelease()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchWorkoutSessions - Failure -- code="
            r2.append(r3)
            r3 = r5
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r6 = r3.a()
            r2.append(r6)
            java.lang.String r6 = ", message="
            r2.append(r6)
            com.portfolio.platform.data.model.ServerError r6 = r3.c()
            if (r6 == 0) goto L_0x0200
            java.lang.String r6 = r6.getMessage()
            if (r6 == 0) goto L_0x0200
            r4 = r6
            goto L_0x020a
        L_0x0200:
            com.portfolio.platform.data.model.ServerError r3 = r3.c()
            if (r3 == 0) goto L_0x020a
            java.lang.String r4 = r3.getUserMessage()
        L_0x020a:
            if (r4 == 0) goto L_0x020d
            goto L_0x020f
        L_0x020d:
            java.lang.String r4 = ""
        L_0x020f:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x0219:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository$fetchWorkoutSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
