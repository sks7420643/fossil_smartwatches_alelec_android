package com.portfolio.platform.data.source;

import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.lx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getActivityStatistic$Anon1 extends lx6<ActivityStatistic, ActivityStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$getActivityStatistic$Anon1(SummariesRepository summariesRepository, boolean z) {
        this.this$0 = summariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.lx6
    public Object createCall(fb7<? super fv7<ActivityStatistic>> fb7) {
        return this.this$0.mApiServiceV2.getActivityStatistic(fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.lx6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<com.portfolio.platform.data.ActivityStatistic>> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$loadFromDb$Anon1_Level2
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$loadFromDb$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$loadFromDb$Anon1_Level2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$loadFromDb$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$loadFromDb$Anon1_Level2
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1) r0
            com.fossil.t87.a(r5)
            goto L_0x0045
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.fossil.pg5 r5 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.b(r0)
            if (r5 != r1) goto L_0x0045
            return r1
        L_0x0045:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r5 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r5
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r5 = r5.activitySummaryDao()
            androidx.lifecycle.LiveData r5 = r5.getActivityStatisticLiveData()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1.loadFromDb(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.lx6
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivityStatistic - onFetchFailed");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
    @Override // com.fossil.lx6
    public /* bridge */ /* synthetic */ Object saveCallResult(ActivityStatistic activityStatistic, fb7 fb7) {
        return saveCallResult(activityStatistic, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object saveCallResult(com.portfolio.platform.data.ActivityStatistic r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            com.portfolio.platform.data.ActivityStatistic r6 = (com.portfolio.platform.data.ActivityStatistic) r6
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1 r0 = (com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1) r0
            com.fossil.t87.a(r7)
            goto L_0x0067
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "getActivityStatistic - saveCallResult -- item="
            r2.append(r4)
            r2.append(r6)
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = "SummariesRepository"
            r7.d(r4, r2)
            com.fossil.pg5 r7 = com.fossil.pg5.i
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.b(r0)
            if (r7 != r1) goto L_0x0067
            return r1
        L_0x0067:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r7
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r7 = r7.activitySummaryDao()
            r7.upsertActivityStatistic(r6)
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$Anon1.saveCallResult(com.portfolio.platform.data.ActivityStatistic, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public boolean shouldFetch(ActivityStatistic activityStatistic) {
        return this.$shouldFetch;
    }
}
