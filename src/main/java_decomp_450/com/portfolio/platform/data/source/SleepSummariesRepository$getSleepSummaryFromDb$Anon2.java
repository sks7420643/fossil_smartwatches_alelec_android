package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaryFromDb$2", f = "SleepSummariesRepository.kt", l = {58}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummaryFromDb$Anon2 extends zb7 implements kd7<yi7, fb7<? super MFSleepDay>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummaryFromDb$Anon2(Date date, fb7 fb7) {
        super(2, fb7);
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSummariesRepository$getSleepSummaryFromDb$Anon2 sleepSummariesRepository$getSleepSummaryFromDb$Anon2 = new SleepSummariesRepository$getSleepSummaryFromDb$Anon2(this.$date, fb7);
        sleepSummariesRepository$getSleepSummaryFromDb$Anon2.p$ = (yi7) obj;
        return sleepSummariesRepository$getSleepSummaryFromDb$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super MFSleepDay> fb7) {
        return ((SleepSummariesRepository$getSleepSummaryFromDb$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = SleepSummariesRepository.TAG;
                local.d(access$getTAG$cp, "getSummary: calendar = " + this.$date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.label = 1;
            obj = pg5.d(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((SleepDatabase) obj).sleepDao().getSleepDay(this.$date);
    }
}
