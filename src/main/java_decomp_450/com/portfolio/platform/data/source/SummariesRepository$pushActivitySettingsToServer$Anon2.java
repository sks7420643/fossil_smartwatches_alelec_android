package com.portfolio.platform.data.source;

import com.fossil.be4;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.od5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$pushActivitySettingsToServer$2", f = "SummariesRepository.kt", l = {137}, m = "invokeSuspend")
public final class SummariesRepository$pushActivitySettingsToServer$Anon2 extends zb7 implements kd7<yi7, fb7<? super fv7<ActivitySettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$pushActivitySettingsToServer$Anon2(SummariesRepository summariesRepository, ActivitySettings activitySettings, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$pushActivitySettingsToServer$Anon2 summariesRepository$pushActivitySettingsToServer$Anon2 = new SummariesRepository$pushActivitySettingsToServer$Anon2(this.this$0, this.$activitySettings, fb7);
        summariesRepository$pushActivitySettingsToServer$Anon2.p$ = (yi7) obj;
        return summariesRepository$pushActivitySettingsToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super fv7<ActivitySettings>> fb7) {
        return ((SummariesRepository$pushActivitySettingsToServer$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "pushActivitySettingsToServer - settings=" + this.$activitySettings);
            be4 be4 = new be4();
            be4.b(new od5());
            JsonElement b = be4.a().b(this.$activitySettings);
            ee7.a((Object) b, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
            ie4 d = b.d();
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            ee7.a((Object) d, "jsonObject");
            this.L$0 = yi7;
            this.L$1 = d;
            this.label = 1;
            obj = access$getMApiServiceV2$p.updateActivitySetting(d, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            ie4 ie4 = (ie4) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
