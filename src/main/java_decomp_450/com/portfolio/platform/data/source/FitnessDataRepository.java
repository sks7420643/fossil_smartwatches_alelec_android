package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "FitnessDataRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataRepository(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new FitnessDataRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object getFitnessData(Date date, Date date2, fb7<? super List<FitnessDataWrapper>> fb7) {
        return vh7.a(qj7.b(), new FitnessDataRepository$getFitnessData$Anon2(date, date2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r20, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r21) {
        /*
            r19 = this;
            r0 = r19
            r1 = r21
            boolean r2 = r1 instanceof com.portfolio.platform.data.source.FitnessDataRepository$insert$Anon1
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.portfolio.platform.data.source.FitnessDataRepository$insert$Anon1 r2 = (com.portfolio.platform.data.source.FitnessDataRepository$insert$Anon1) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.data.source.FitnessDataRepository$insert$Anon1 r2 = new com.portfolio.platform.data.source.FitnessDataRepository$insert$Anon1
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 1
            java.lang.String r6 = "[Push Fitness File] At "
            java.lang.String r7 = ""
            r8 = 0
            if (r4 == 0) goto L_0x0051
            if (r4 != r5) goto L_0x0049
            long r3 = r2.J$0
            java.lang.Object r5 = r2.L$4
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r5 = r2.L$3
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r5 = r2.L$2
            com.portfolio.platform.data.source.remote.ApiResponse r5 = (com.portfolio.platform.data.source.remote.ApiResponse) r5
            java.lang.Object r5 = r2.L$1
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.FitnessDataRepository r2 = (com.portfolio.platform.data.source.FitnessDataRepository) r2
            com.fossil.t87.a(r1)
            goto L_0x00ef
        L_0x0049:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0051:
            com.fossil.t87.a(r1)
            com.portfolio.platform.data.source.remote.ApiResponse r1 = new com.portfolio.platform.data.source.remote.ApiResponse
            r1.<init>()
            java.util.List r4 = com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt.toServerFitnessDataWrapperList(r20)
            java.util.ArrayList r9 = new java.util.ArrayList
            r10 = 10
            int r10 = com.fossil.x97.a(r4, r10)
            r9.<init>(r10)
            java.util.Iterator r10 = r4.iterator()
        L_0x006c:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x0080
            java.lang.Object r11 = r10.next()
            com.portfolio.platform.data.ServerFitnessDataWrapper r11 = (com.portfolio.platform.data.ServerFitnessDataWrapper) r11
            org.joda.time.DateTime r11 = r11.getStartTime()
            r9.add(r11)
            goto L_0x006c
        L_0x0080:
            long r10 = java.lang.System.currentTimeMillis()
            java.util.List r12 = r1.get_items()
            r12.addAll(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r13 = r12.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r14 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r15 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PUSH_FITNESS_FILE
            java.lang.Object r12 = com.fossil.ea7.e(r20)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r12 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r12
            if (r12 == 0) goto L_0x00a6
            java.lang.String r12 = r12.getSerialNumber()
            if (r12 == 0) goto L_0x00a6
            r16 = r12
            goto L_0x00a8
        L_0x00a6:
            r16 = r7
        L_0x00a8:
            java.lang.String r17 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            r12.append(r6)
            r12.append(r10)
            java.lang.String r5 = " Push fitness file size "
            r12.append(r5)
            int r5 = r4.size()
            r12.append(r5)
            java.lang.String r5 = " fitnessTimestamps "
            r12.append(r5)
            r12.append(r9)
            java.lang.String r18 = r12.toString()
            r13.i(r14, r15, r16, r17, r18)
            com.portfolio.platform.data.source.FitnessDataRepository$insert$repoResponse$Anon1 r5 = new com.portfolio.platform.data.source.FitnessDataRepository$insert$repoResponse$Anon1
            r5.<init>(r0, r1, r8)
            r2.L$0 = r0
            r12 = r20
            r2.L$1 = r12
            r2.L$2 = r1
            r2.L$3 = r4
            r2.L$4 = r9
            r2.J$0 = r10
            r1 = 1
            r2.label = r1
            java.lang.Object r1 = com.fossil.aj5.a(r5, r2)
            if (r1 != r3) goto L_0x00ed
            return r3
        L_0x00ed:
            r3 = r10
            r5 = r12
        L_0x00ef:
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            boolean r2 = r1 instanceof com.fossil.bj5
            if (r2 == 0) goto L_0x014e
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r9 = r2.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r10 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r11 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PUSH_FITNESS_FILE
            java.lang.Object r2 = com.fossil.ea7.e(r5)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r2 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r2
            if (r2 == 0) goto L_0x010f
            java.lang.String r2 = r2.getSerialNumber()
            if (r2 == 0) goto L_0x010f
            r12 = r2
            goto L_0x0110
        L_0x010f:
            r12 = r7
        L_0x0110:
            java.lang.String r13 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r2.append(r3)
            java.lang.String r3 = " success"
            r2.append(r3)
            java.lang.String r14 = r2.toString()
            r9.i(r10, r11, r12, r13, r14)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "insert onResponse: response = "
            r4.append(r6)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.d(r3, r1)
            com.fossil.bj5 r1 = new com.fossil.bj5
            r2 = 0
            r3 = 2
            r1.<init>(r5, r2, r3, r8)
            goto L_0x01f0
        L_0x014e:
            boolean r2 = r1 instanceof com.fossil.yi5
            if (r2 == 0) goto L_0x01f1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r9 = r2.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r10 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r11 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PUSH_FITNESS_FILE
            java.lang.Object r2 = com.fossil.ea7.e(r5)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r2 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r2
            if (r2 == 0) goto L_0x016c
            java.lang.String r2 = r2.getSerialNumber()
            if (r2 == 0) goto L_0x016c
            r12 = r2
            goto L_0x016d
        L_0x016c:
            r12 = r7
        L_0x016d:
            java.lang.String r13 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r2.append(r3)
            java.lang.String r3 = " failed code "
            r2.append(r3)
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            int r3 = r1.a()
            r2.append(r3)
            java.lang.String r3 = " message "
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r3 = r1.c()
            if (r3 == 0) goto L_0x0198
            java.lang.String r3 = r3.getMessage()
            goto L_0x0199
        L_0x0198:
            r3 = r8
        L_0x0199:
            r2.append(r3)
            java.lang.String r14 = r2.toString()
            r9.i(r10, r11, r12, r13, r14)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insert Failure code="
            r4.append(r5)
            int r5 = r1.a()
            r4.append(r5)
            java.lang.String r5 = " message="
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r1.c()
            if (r5 == 0) goto L_0x01cb
            java.lang.String r8 = r5.getMessage()
        L_0x01cb:
            r4.append(r8)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.fossil.yi5 r2 = new com.fossil.yi5
            int r6 = r1.a()
            com.portfolio.platform.data.model.ServerError r7 = r1.c()
            java.lang.Throwable r8 = r1.d()
            java.lang.String r9 = r1.b()
            r10 = 0
            r11 = 16
            r12 = 0
            r5 = r2
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            r1 = r2
        L_0x01f0:
            return r1
        L_0x01f1:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.insert(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object pushFitnessData(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r17, com.fossil.fb7<? super com.fossil.zi5<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r18) {
        /*
            r16 = this;
            r0 = r18
            boolean r1 = r0 instanceof com.portfolio.platform.data.source.FitnessDataRepository$pushFitnessData$Anon1
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.portfolio.platform.data.source.FitnessDataRepository$pushFitnessData$Anon1 r1 = (com.portfolio.platform.data.source.FitnessDataRepository$pushFitnessData$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            r2 = r16
            goto L_0x001e
        L_0x0017:
            com.portfolio.platform.data.source.FitnessDataRepository$pushFitnessData$Anon1 r1 = new com.portfolio.platform.data.source.FitnessDataRepository$pushFitnessData$Anon1
            r2 = r16
            r1.<init>(r2, r0)
        L_0x001e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r1.label
            r7 = 2
            r8 = 1
            java.lang.String r9 = " endIndex="
            if (r4 == 0) goto L_0x007e
            if (r4 == r8) goto L_0x005d
            if (r4 != r7) goto L_0x0055
            java.lang.Object r3 = r1.L$6
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r3 = r1.L$5
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r3 = r1.L$4
            java.util.List r3 = (java.util.List) r3
            int r3 = r1.I$1
            java.lang.Object r3 = r1.L$3
            java.util.List r3 = (java.util.List) r3
            int r4 = r1.I$0
            java.lang.Object r4 = r1.L$2
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r4 = r1.L$1
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.FitnessDataRepository r1 = (com.portfolio.platform.data.source.FitnessDataRepository) r1
            com.fossil.t87.a(r0)
            goto L_0x0164
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            java.lang.Object r4 = r1.L$4
            java.util.List r4 = (java.util.List) r4
            int r10 = r1.I$1
            java.lang.Object r11 = r1.L$3
            java.util.List r11 = (java.util.List) r11
            int r12 = r1.I$0
            java.lang.Object r13 = r1.L$2
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            java.lang.Object r14 = r1.L$1
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r15 = r1.L$0
            com.portfolio.platform.data.source.FitnessDataRepository r15 = (com.portfolio.platform.data.source.FitnessDataRepository) r15
            com.fossil.t87.a(r0)
            r5 = r4
            r4 = r3
            r3 = r1
            r1 = r11
            goto L_0x00eb
        L_0x007e:
            com.fossil.t87.a(r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r15 = r2
            r4 = r3
            r12 = 0
            r13 = 0
            r3 = r1
            r1 = r0
            r0 = r17
        L_0x008e:
            int r10 = r0.size()
            if (r12 >= r10) goto L_0x01c8
            int r10 = r12 + 3
            int r11 = r0.size()
            if (r10 <= r11) goto L_0x00a0
            int r10 = r0.size()
        L_0x00a0:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r14 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r5 = "pushFitnessData listSize="
            r6.append(r5)
            int r5 = r0.size()
            r6.append(r5)
            java.lang.String r5 = ", startIndex="
            r6.append(r5)
            r6.append(r12)
            r6.append(r9)
            r6.append(r10)
            java.lang.String r5 = r6.toString()
            r11.d(r14, r5)
            java.util.List r5 = r0.subList(r12, r10)
            r3.L$0 = r15
            r3.L$1 = r0
            r3.L$2 = r13
            r3.I$0 = r12
            r3.L$3 = r1
            r3.I$1 = r10
            r3.L$4 = r5
            r3.label = r8
            java.lang.Object r6 = r15.insert(r5, r3)
            if (r6 != r4) goto L_0x00e9
            return r4
        L_0x00e9:
            r14 = r0
            r0 = r6
        L_0x00eb:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            int r12 = r12 + 3
            boolean r6 = r0 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x017c
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r11 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r7 = "pushFitnessData success, bravo!!! startIndex="
            r8.append(r7)
            r8.append(r12)
            r8.append(r9)
            r8.append(r10)
            java.lang.String r7 = r8.toString()
            r6.d(r11, r7)
            r6 = r0
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r6 = r6.a()
            if (r6 == 0) goto L_0x0177
            java.util.List r6 = (java.util.List) r6
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r2 = "pushFitnessData success, new fitness data "
            r11.append(r2)
            r11.append(r6)
            java.lang.String r2 = r11.toString()
            r7.d(r8, r2)
            r1.addAll(r6)
            int r2 = r14.size()
            if (r12 < r2) goto L_0x0174
            com.fossil.pg5 r2 = com.fossil.pg5.i
            r3.L$0 = r15
            r3.L$1 = r14
            r3.L$2 = r13
            r3.I$0 = r12
            r3.L$3 = r1
            r3.I$1 = r10
            r3.L$4 = r5
            r3.L$5 = r0
            r3.L$6 = r6
            r5 = 2
            r3.label = r5
            java.lang.Object r0 = r2.b(r3)
            if (r0 != r4) goto L_0x0163
            return r4
        L_0x0163:
            r3 = r1
        L_0x0164:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r0 = r0.getFitnessDataDao()
            r0.deleteFitnessData(r3)
            com.fossil.bj5 r13 = new com.fossil.bj5
            r2 = 0
            r13.<init>(r3, r2)
            goto L_0x01c8
        L_0x0174:
            r5 = 2
            r6 = 0
            goto L_0x01c1
        L_0x0177:
            com.fossil.ee7.a()
            r6 = 0
            throw r6
        L_0x017c:
            r2 = 0
            r5 = 2
            r6 = 0
            boolean r7 = r0 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x01c1
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.FitnessDataRepository.TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r2 = "pushFitnessData failed, errorCode="
            r11.append(r2)
            r2 = r0
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r2 = r2.a()
            r11.append(r2)
            r2 = 32
            r11.append(r2)
            java.lang.String r2 = "startIndex="
            r11.append(r2)
            r11.append(r12)
            r11.append(r9)
            r11.append(r10)
            java.lang.String r2 = r11.toString()
            r7.d(r8, r2)
            int r2 = r14.size()
            if (r12 < r2) goto L_0x01c1
            r13 = r0
            goto L_0x01c8
        L_0x01c1:
            r0 = r14
            r7 = 2
            r8 = 1
            r2 = r16
            goto L_0x008e
        L_0x01c8:
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.pushFitnessData(java.util.List, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingFitnessData(fb7<? super zi5<List<FitnessDataWrapper>>> fb7) {
        return vh7.a(qj7.b(), new FitnessDataRepository$pushPendingFitnessData$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final Object saveFitnessData(List<FitnessDataWrapper> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new FitnessDataRepository$saveFitnessData$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }
}
