package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.mo7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource$downloadWatchFaceFile$2", f = "WatchFaceRemoteDataSource.kt", l = {33}, m = "invokeSuspend")
public final class WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2 extends zb7 implements gd7<fb7<? super fv7<mo7>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(WatchFaceRemoteDataSource watchFaceRemoteDataSource, String str, fb7 fb7) {
        super(1, fb7);
        this.this$0 = watchFaceRemoteDataSource;
        this.$url = str;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2(this.this$0, this.$url, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<mo7>> fb7) {
        return ((WatchFaceRemoteDataSource$downloadWatchFaceFile$Anon2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getApi$p = this.this$0.api;
            String str = this.$url;
            this.label = 1;
            obj = access$getApi$p.downloadFile(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
