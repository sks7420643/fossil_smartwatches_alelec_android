package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.UserRepository$loadUserInfo$2", f = "UserRepository.kt", l = {103, 106, 110, 118}, m = "invokeSuspend")
public final class UserRepository$loadUserInfo$Anon2 extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$loadUserInfo$Anon2(UserRepository userRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        UserRepository$loadUserInfo$Anon2 userRepository$loadUserInfo$Anon2 = new UserRepository$loadUserInfo$Anon2(this.this$0, fb7);
        userRepository$loadUserInfo$Anon2.p$ = (yi7) obj;
        return userRepository$loadUserInfo$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
        return ((UserRepository$loadUserInfo$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r13.label
            r2 = 0
            r3 = 4
            r4 = 3
            r5 = 1
            r6 = 2
            if (r1 == 0) goto L_0x005f
            if (r1 == r5) goto L_0x0057
            if (r1 == r6) goto L_0x0047
            if (r1 == r4) goto L_0x0036
            if (r1 != r3) goto L_0x002e
            java.lang.Object r0 = r13.L$4
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.lang.Object r1 = r13.L$3
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r2 = r13.L$2
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            java.lang.Object r2 = r13.L$1
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            java.lang.Object r2 = r13.L$0
            com.fossil.yi7 r2 = (com.fossil.yi7) r2
            com.fossil.t87.a(r14)
            goto L_0x014a
        L_0x002e:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r0)
            throw r14
        L_0x0036:
            java.lang.Object r1 = r13.L$2
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r4 = r13.L$1
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            java.lang.Object r5 = r13.L$0
            com.fossil.yi7 r5 = (com.fossil.yi7) r5
            com.fossil.t87.a(r14)
            goto L_0x00b6
        L_0x0047:
            java.lang.Object r1 = r13.L$2
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r5 = r13.L$1
            com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
            java.lang.Object r7 = r13.L$0
            com.fossil.yi7 r7 = (com.fossil.yi7) r7
            com.fossil.t87.a(r14)
            goto L_0x008a
        L_0x0057:
            java.lang.Object r1 = r13.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r14)
            goto L_0x0071
        L_0x005f:
            com.fossil.t87.a(r14)
            com.fossil.yi7 r1 = r13.p$
            com.portfolio.platform.data.source.UserRepository r14 = r13.this$0
            r13.L$0 = r1
            r13.label = r5
            java.lang.Object r14 = r14.getCurrentUser(r13)
            if (r14 != r0) goto L_0x0071
            return r0
        L_0x0071:
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            if (r14 == 0) goto L_0x0179
            com.portfolio.platform.data.source.UserRepository r5 = r13.this$0
            r13.L$0 = r1
            r13.L$1 = r14
            r13.L$2 = r14
            r13.label = r6
            java.lang.Object r5 = r5.pushPendingUser(r14, r13)
            if (r5 != r0) goto L_0x0086
            return r0
        L_0x0086:
            r7 = r1
            r1 = r14
            r14 = r5
            r5 = r1
        L_0x008a:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r8 = r14 instanceof com.fossil.bj5
            if (r8 == 0) goto L_0x0159
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r6 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.String r8 = "push pending user success, start load user info"
            r14.d(r6, r8)
            com.portfolio.platform.data.source.UserRepository r14 = r13.this$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r14 = r14.mUserRemoteDataSource
            r13.L$0 = r7
            r13.L$1 = r5
            r13.L$2 = r1
            r13.label = r4
            java.lang.Object r14 = r14.loadUserInfo(r1, r13)
            if (r14 != r0) goto L_0x00b4
            return r0
        L_0x00b4:
            r4 = r5
            r5 = r7
        L_0x00b6:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r6 = r14 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x0172
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "loadUserInfo success isFromCache "
            r8.append(r9)
            r10 = r14
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            boolean r11 = r10.b()
            r8.append(r11)
            java.lang.String r11 = " user "
            r8.append(r11)
            java.lang.Object r11 = r10.a()
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            r8.append(r11)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            boolean r6 = r10.b()
            if (r6 != 0) goto L_0x0172
            java.lang.Object r6 = r10.a()
            if (r6 == 0) goto L_0x0155
            r2 = r6
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            com.portfolio.platform.data.model.MFUser$Auth r6 = r4.getAuth()
            r2.setAuth(r6)
            java.lang.String r6 = r4.getUserId()
            r2.setUserId(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r9)
            boolean r9 = r10.b()
            r8.append(r9)
            r9 = 32
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            com.fossil.pg5 r6 = com.fossil.pg5.i
            r13.L$0 = r5
            r13.L$1 = r4
            r13.L$2 = r1
            r13.L$3 = r14
            r13.L$4 = r2
            r13.label = r3
            java.lang.Object r1 = r6.f(r13)
            if (r1 != r0) goto L_0x0146
            return r0
        L_0x0146:
            r0 = r2
            r12 = r1
            r1 = r14
            r14 = r12
        L_0x014a:
            com.portfolio.platform.data.model.room.UserDatabase r14 = (com.portfolio.platform.data.model.room.UserDatabase) r14
            com.portfolio.platform.data.model.room.UserDao r14 = r14.userDao()
            r14.updateUser(r0)
            r14 = r1
            goto L_0x0172
        L_0x0155:
            com.fossil.ee7.a()
            throw r2
        L_0x0159:
            boolean r14 = r14 instanceof com.fossil.yi5
            if (r14 == 0) goto L_0x0173
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.String r1 = "push pending user fail, return current user info"
            r14.d(r0, r1)
            com.fossil.bj5 r14 = new com.fossil.bj5
            r0 = 0
            r14.<init>(r5, r0, r6, r2)
        L_0x0172:
            return r14
        L_0x0173:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        L_0x0179:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.String r1 = "user is null"
            r14.e(r0, r1)
            com.fossil.yi5 r14 = new com.fossil.yi5
            r3 = 600(0x258, float:8.41E-43)
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            r2 = r14
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository$loadUserInfo$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
