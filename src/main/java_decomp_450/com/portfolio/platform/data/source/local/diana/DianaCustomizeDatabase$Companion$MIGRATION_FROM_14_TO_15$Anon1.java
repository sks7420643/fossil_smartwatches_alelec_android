package com.portfolio.platform.data.source.local.diana;

import com.fossil.ee7;
import com.fossil.li;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1 extends li {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00e1 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00e6 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f2 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0117 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011c A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0128 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0135 A[Catch:{ Exception -> 0x0230 }, LOOP:2: B:46:0x012f->B:48:0x0135, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0173 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0223 A[Catch:{ Exception -> 0x0230 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00ab A[SYNTHETIC] */
    @Override // com.fossil.li
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void migrate(com.fossil.wi r19) {
        /*
            r18 = this;
            r1 = r18
            r2 = r19
            java.lang.String r0 = "database"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r3 = "DianaCustomizeDatabase"
            java.lang.String r0 = "MIGRATION_FROM_14_TO_15 - START"
            android.util.Log.d(r3, r0)
            r19.beginTransaction()
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0230 }
            r0.<init>()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$ringStyleItemsType$Anon1_Level2 r4 = new com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$ringStyleItemsType$Anon1_Level2     // Catch:{ Exception -> 0x0230 }
            r4.<init>()     // Catch:{ Exception -> 0x0230 }
            java.lang.reflect.Type r4 = r4.getType()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$backgroundType$Anon1_Level2 r5 = new com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$backgroundType$Anon1_Level2     // Catch:{ Exception -> 0x0230 }
            r5.<init>()     // Catch:{ Exception -> 0x0230 }
            java.lang.reflect.Type r5 = r5.getType()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.PortfolioApp r6 = r6.c()     // Catch:{ Exception -> 0x0230 }
            android.content.Context r6 = r6.getApplicationContext()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r7 = "PortfolioApp.instance.applicationContext"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x0230 }
            java.io.File r6 = r6.getFilesDir()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.PortfolioApp r7 = r7.c()     // Catch:{ Exception -> 0x0230 }
            android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x0230 }
            com.misfit.frameworks.buttonservice.model.FileType r8 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE     // Catch:{ Exception -> 0x0230 }
            java.lang.String r7 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r7, r8)     // Catch:{ Exception -> 0x0230 }
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x0230 }
            r8.<init>(r7)     // Catch:{ Exception -> 0x0230 }
            boolean r9 = r8.exists()     // Catch:{ Exception -> 0x0230 }
            if (r9 != 0) goto L_0x005b
            r8.mkdirs()     // Catch:{ Exception -> 0x0230 }
        L_0x005b:
            java.lang.String r8 = "SELECT * FROM watch_face"
            android.database.Cursor r8 = r2.query(r8)     // Catch:{ Exception -> 0x0230 }
            r8.moveToFirst()     // Catch:{ Exception -> 0x0230 }
        L_0x0064:
            java.lang.String r9 = "cursor"
            com.fossil.ee7.a(r8, r9)     // Catch:{ Exception -> 0x0230 }
            boolean r9 = r8.isAfterLast()     // Catch:{ Exception -> 0x0230 }
            if (r9 != 0) goto L_0x022c
            java.lang.String r9 = "watchFaceType"
            int r9 = r8.getColumnIndex(r9)     // Catch:{ Exception -> 0x0230 }
            int r9 = r8.getInt(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r10 = "ringStyleItems"
            int r10 = r8.getColumnIndex(r10)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r10 = r8.getString(r10)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = "background"
            int r11 = r8.getColumnIndex(r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = r8.getString(r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = "previewUrl"
            int r12 = r8.getColumnIndex(r12)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = r8.getString(r12)     // Catch:{ Exception -> 0x0230 }
            java.util.ArrayList r13 = new java.util.ArrayList     // Catch:{ Exception -> 0x0230 }
            r13.<init>()     // Catch:{ Exception -> 0x0230 }
            java.lang.Object r10 = r0.a(r10, r4)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r14 = "gSon.fromJson(ringStyleValue, ringStyleItemsType)"
            com.fossil.ee7.a(r10, r14)     // Catch:{ Exception -> 0x0230 }
            java.util.List r10 = (java.util.List) r10     // Catch:{ Exception -> 0x0230 }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ Exception -> 0x0230 }
        L_0x00ab:
            boolean r14 = r10.hasNext()     // Catch:{ Exception -> 0x0230 }
            r16 = 1
            if (r14 == 0) goto L_0x00f6
            java.lang.Object r14 = r10.next()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.RingStyleItem r14 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r14     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.RingStyle r17 = r14.getRingStyle()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Data r17 = r17.getData()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r15 = r17.getPreviewUrl()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.RingStyle r14 = r14.getRingStyle()     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Data r14 = r14.getData()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r14 = r14.getUrl()     // Catch:{ Exception -> 0x0230 }
            if (r15 == 0) goto L_0x00dd
            boolean r17 = com.fossil.mh7.a(r15)     // Catch:{ Exception -> 0x0230 }
            if (r17 == 0) goto L_0x00da
            goto L_0x00dd
        L_0x00da:
            r17 = 0
            goto L_0x00df
        L_0x00dd:
            r17 = 1
        L_0x00df:
            if (r17 != 0) goto L_0x00e4
            r13.add(r15)     // Catch:{ Exception -> 0x0230 }
        L_0x00e4:
            if (r14 == 0) goto L_0x00ef
            boolean r15 = com.fossil.mh7.a(r14)     // Catch:{ Exception -> 0x0230 }
            if (r15 == 0) goto L_0x00ed
            goto L_0x00ef
        L_0x00ed:
            r15 = 0
            goto L_0x00f0
        L_0x00ef:
            r15 = 1
        L_0x00f0:
            if (r15 != 0) goto L_0x00ab
            r13.add(r14)     // Catch:{ Exception -> 0x0230 }
            goto L_0x00ab
        L_0x00f6:
            java.lang.Object r10 = r0.a(r11, r5)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = "gSon.fromJson(backgroundValue, backgroundType)"
            com.fossil.ee7.a(r10, r11)     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Background r10 = (com.portfolio.platform.data.model.diana.preset.Background) r10     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Data r11 = r10.getData()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = r11.getPreviewUrl()     // Catch:{ Exception -> 0x0230 }
            if (r11 == 0) goto L_0x0114
            boolean r14 = com.fossil.mh7.a(r11)     // Catch:{ Exception -> 0x0230 }
            if (r14 == 0) goto L_0x0112
            goto L_0x0114
        L_0x0112:
            r14 = 0
            goto L_0x0115
        L_0x0114:
            r14 = 1
        L_0x0115:
            if (r14 != 0) goto L_0x011a
            r13.add(r11)     // Catch:{ Exception -> 0x0230 }
        L_0x011a:
            if (r12 == 0) goto L_0x0125
            boolean r11 = com.fossil.mh7.a(r12)     // Catch:{ Exception -> 0x0230 }
            if (r11 == 0) goto L_0x0123
            goto L_0x0125
        L_0x0123:
            r15 = 0
            goto L_0x0126
        L_0x0125:
            r15 = 1
        L_0x0126:
            if (r15 != 0) goto L_0x012b
            r13.add(r12)     // Catch:{ Exception -> 0x0230 }
        L_0x012b:
            java.util.Iterator r11 = r13.iterator()     // Catch:{ Exception -> 0x0230 }
        L_0x012f:
            boolean r12 = r11.hasNext()     // Catch:{ Exception -> 0x0230 }
            if (r12 == 0) goto L_0x016b
            java.lang.Object r12 = r11.next()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = com.fossil.mg5.a(r12)     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r13.<init>()     // Catch:{ Exception -> 0x0230 }
            r13.append(r6)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r14 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r13.append(r14)     // Catch:{ Exception -> 0x0230 }
            r13.append(r12)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r14.<init>()     // Catch:{ Exception -> 0x0230 }
            r14.append(r7)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r15 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r14.append(r15)     // Catch:{ Exception -> 0x0230 }
            r14.append(r12)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = r14.toString()     // Catch:{ Exception -> 0x0230 }
            r1.moveFile(r13, r12)     // Catch:{ Exception -> 0x0230 }
            goto L_0x012f
        L_0x016b:
            com.fossil.qb5 r11 = com.fossil.qb5.PHOTO     // Catch:{ Exception -> 0x0230 }
            int r11 = r11.getValue()     // Catch:{ Exception -> 0x0230 }
            if (r9 != r11) goto L_0x0223
            java.lang.String r9 = "id"
            int r9 = r8.getColumnIndex(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r9 = r8.getString(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = "imageId"
            com.fossil.ee7.a(r9, r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = "_img"
            java.lang.String r13 = "_bin"
            r14 = 0
            r15 = 4
            r16 = 0
            r11 = r9
            java.lang.String r11 = com.fossil.mh7.a(r11, r12, r13, r14, r15, r16)     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r12.<init>()     // Catch:{ Exception -> 0x0230 }
            r12.append(r6)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r13 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r12.append(r13)     // Catch:{ Exception -> 0x0230 }
            r12.append(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r13.<init>()     // Catch:{ Exception -> 0x0230 }
            r13.append(r6)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r14 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r13.append(r14)     // Catch:{ Exception -> 0x0230 }
            r13.append(r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r14.<init>()     // Catch:{ Exception -> 0x0230 }
            r14.append(r7)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r15 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r14.append(r15)     // Catch:{ Exception -> 0x0230 }
            r14.append(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r15.<init>()     // Catch:{ Exception -> 0x0230 }
            r15.append(r7)     // Catch:{ Exception -> 0x0230 }
            r16 = r4
            java.lang.String r4 = java.io.File.separator     // Catch:{ Exception -> 0x0230 }
            r15.append(r4)     // Catch:{ Exception -> 0x0230 }
            r15.append(r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r4 = r15.toString()     // Catch:{ Exception -> 0x0230 }
            r1.moveFile(r12, r14)     // Catch:{ Exception -> 0x0230 }
            r1.moveFile(r13, r4)     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Data r11 = r10.getData()     // Catch:{ Exception -> 0x0230 }
            r11.setPreviewUrl(r14)     // Catch:{ Exception -> 0x0230 }
            com.portfolio.platform.data.model.diana.preset.Data r11 = r10.getData()     // Catch:{ Exception -> 0x0230 }
            r11.setUrl(r4)     // Catch:{ Exception -> 0x0230 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0230 }
            r4.<init>()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r11 = "UPDATE `watch_face` SET  `background` = '"
            r4.append(r11)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r10 = r0.a(r10)     // Catch:{ Exception -> 0x0230 }
            r4.append(r10)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r10 = "', `previewUrl` = '"
            r4.append(r10)     // Catch:{ Exception -> 0x0230 }
            r4.append(r14)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r10 = "' WHERE `id` = '"
            r4.append(r10)     // Catch:{ Exception -> 0x0230 }
            r4.append(r9)     // Catch:{ Exception -> 0x0230 }
            r9 = 39
            r4.append(r9)     // Catch:{ Exception -> 0x0230 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0230 }
            r2.execSQL(r4)     // Catch:{ Exception -> 0x0230 }
            goto L_0x0225
        L_0x0223:
            r16 = r4
        L_0x0225:
            r8.moveToNext()     // Catch:{ Exception -> 0x0230 }
            r4 = r16
            goto L_0x0064
        L_0x022c:
            r8.close()     // Catch:{ Exception -> 0x0230 }
            goto L_0x024c
        L_0x0230:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "MIGRATION_FROM_14_TO_15 - ERROR: "
            r4.append(r5)
            java.lang.String r0 = r0.getMessage()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            android.util.Log.e(r3, r0)
        L_0x024c:
            java.lang.String r0 = "CREATE TABLE IF NOT EXISTS `watchAppData` (`id` TEXT NOT NULL, `type` TEXT NOT NULL, `maxAppVersion` TEXT NOT NULL, `maxFirmwareOSVersion` TEXT NOT NULL, `minAppVersion` TEXT NOT NULL, `minFirmwareOSVersion` TEXT NOT NULL, `version` TEXT NOT NULL, `executableBinaryDataUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, PRIMARY KEY(`id`))"
            r2.execSQL(r0)
            r19.setTransactionSuccessful()
            r19.endTransaction()
            java.lang.String r0 = "MIGRATION_FROM_14_TO_15 - END"
            android.util.Log.d(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1.migrate(com.fossil.wi):void");
    }

    @DexIgnore
    public final void moveFile(String str, String str2) {
        ee7.b(str, "oldPath");
        ee7.b(str2, "newPath");
        File file = new File(str);
        if (file.exists()) {
            file.renameTo(new File(str2));
        }
    }
}
