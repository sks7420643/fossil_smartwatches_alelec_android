package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.uh;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSampleDao_Impl implements GFitSampleDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<GFitSample> __deletionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ vh<GFitSample> __insertionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<GFitSample> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSample` (`id`,`step`,`distance`,`calorie`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitSample gFitSample) {
            ajVar.bindLong(1, (long) gFitSample.getId());
            ajVar.bindLong(2, (long) gFitSample.getStep());
            ajVar.bindDouble(3, (double) gFitSample.getDistance());
            ajVar.bindDouble(4, (double) gFitSample.getCalorie());
            ajVar.bindLong(5, gFitSample.getStartTime());
            ajVar.bindLong(6, gFitSample.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<GFitSample> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `gFitSample` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitSample gFitSample) {
            ajVar.bindLong(1, (long) gFitSample.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM gFitSample";
        }
    }

    @DexIgnore
    public GFitSampleDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfGFitSample = new Anon1(ciVar);
        this.__deletionAdapterOfGFitSample = new Anon2(ciVar);
        this.__preparedStmtOfClearAll = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void deleteListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public List<GFitSample> getAllGFitSample() {
        fi b = fi.b("SELECT * FROM gFitSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "step");
            int b4 = oi.b(a, "distance");
            int b5 = oi.b(a, "calorie");
            int b6 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b7 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSample gFitSample = new GFitSample(a.getInt(b3), a.getFloat(b4), a.getFloat(b5), a.getLong(b6), a.getLong(b7));
                gFitSample.setId(a.getInt(b2));
                arrayList.add(gFitSample);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertGFitSample(GFitSample gFitSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(gFitSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
