package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2$1$1$saveCallResult$2", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalDailySummary $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, GoalDailySummary goalDailySummary, fb7 fb7) {
        super(2, fb7);
        this.this$0 = anon1_Level3;
        this.$item = goalDailySummary;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 = new GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(this.this$0, this.$item, fb7);
        goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4.p$ = (yi7) obj;
        return goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            GoalTrackingDao goalTrackingDao = this.this$0.this$0.$goalTrackingDb.getGoalTrackingDao();
            GoalTrackingSummary goalTrackingSummary = this.$item.toGoalTrackingSummary();
            if (goalTrackingSummary != null) {
                goalTrackingDao.upsertGoalTrackingSummary(goalTrackingSummary);
                return i97.a;
            }
            ee7.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
