package com.portfolio.platform.data.source;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, `fastPairId` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '5396f1b60549f2c34ac181bb35df2a0f')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `device`");
            wiVar.execSQL("DROP TABLE IF EXISTS `SKU`");
            wiVar.execSQL("DROP TABLE IF EXISTS `watchParam`");
            if (((ci) DeviceDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DeviceDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DeviceDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) DeviceDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DeviceDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DeviceDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) DeviceDatabase_Impl.this).mDatabase = wiVar;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) DeviceDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) DeviceDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) DeviceDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("major", new ti.a("major", "INTEGER", true, 0, null, 1));
            hashMap.put("minor", new ti.a("minor", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("owner", new ti.a("owner", "TEXT", false, 0, null, 1));
            hashMap.put("productDisplayName", new ti.a("productDisplayName", "TEXT", false, 0, null, 1));
            hashMap.put("manufacturer", new ti.a("manufacturer", "TEXT", false, 0, null, 1));
            hashMap.put("softwareRevision", new ti.a("softwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("hardwareRevision", new ti.a("hardwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("deviceId", new ti.a("deviceId", "TEXT", true, 1, null, 1));
            hashMap.put("macAddress", new ti.a("macAddress", "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new ti.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new ti.a(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new ti.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0, null, 1));
            hashMap.put("vibrationStrength", new ti.a("vibrationStrength", "INTEGER", false, 0, null, 1));
            hashMap.put("isActive", new ti.a("isActive", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("device", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "device");
            if (!tiVar.equals(a)) {
                return new ei.b(false, "device(com.portfolio.platform.data.model.Device).\n Expected:\n" + tiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap2.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap2.put("serialNumberPrefix", new ti.a("serialNumberPrefix", "TEXT", true, 1, null, 1));
            hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new ti.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap2.put("deviceName", new ti.a("deviceName", "TEXT", false, 0, null, 1));
            hashMap2.put("groupName", new ti.a("groupName", "TEXT", false, 0, null, 1));
            hashMap2.put("gender", new ti.a("gender", "TEXT", false, 0, null, 1));
            hashMap2.put("deviceType", new ti.a("deviceType", "TEXT", false, 0, null, 1));
            hashMap2.put("fastPairId", new ti.a("fastPairId", "TEXT", false, 0, null, 1));
            ti tiVar2 = new ti("SKU", hashMap2, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "SKU");
            if (!tiVar2.equals(a2)) {
                return new ei.b(false, "SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + tiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("prefixSerial", new ti.a("prefixSerial", "TEXT", true, 1, null, 1));
            hashMap3.put("versionMajor", new ti.a("versionMajor", "TEXT", false, 0, null, 1));
            hashMap3.put("versionMinor", new ti.a("versionMinor", "TEXT", false, 0, null, 1));
            hashMap3.put("data", new ti.a("data", "TEXT", false, 0, null, 1));
            ti tiVar3 = new ti("watchParam", hashMap3, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "watchParam");
            if (tiVar3.equals(a3)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + tiVar3 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `device`");
            writableDatabase.execSQL("DELETE FROM `SKU`");
            writableDatabase.execSQL("DELETE FROM `watchParam`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "device", "SKU", "watchParam");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(3), "5396f1b60549f2c34ac181bb35df2a0f", "8e8d5273d687d9dce08dcd027801930e");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
