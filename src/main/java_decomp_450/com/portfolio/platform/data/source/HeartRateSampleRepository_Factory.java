package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleRepository_Factory implements Factory<HeartRateSampleRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public HeartRateSampleRepository_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static HeartRateSampleRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new HeartRateSampleRepository_Factory(provider);
    }

    @DexIgnore
    public static HeartRateSampleRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new HeartRateSampleRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public HeartRateSampleRepository get() {
        return newInstance(this.mApiServiceProvider.get());
    }
}
