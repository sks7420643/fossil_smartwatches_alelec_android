package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SleepSessionsRepository$insertFromDevice$2", f = "SleepSessionsRepository.kt", l = {207, 209}, m = "invokeSuspend")
public final class SleepSessionsRepository$insertFromDevice$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $sleepSessionList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$insertFromDevice$Anon2(SleepSessionsRepository sleepSessionsRepository, List list, fb7 fb7) {
        super(2, fb7);
        this.this$0 = sleepSessionsRepository;
        this.$sleepSessionList = list;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SleepSessionsRepository$insertFromDevice$Anon2 sleepSessionsRepository$insertFromDevice$Anon2 = new SleepSessionsRepository$insertFromDevice$Anon2(this.this$0, this.$sleepSessionList, fb7);
        sleepSessionsRepository$insertFromDevice$Anon2.p$ = (yi7) obj;
        return sleepSessionsRepository$insertFromDevice$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((SleepSessionsRepository$insertFromDevice$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ba  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r11.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L_0x0039
            if (r1 == r3) goto L_0x0031
            if (r1 != r2) goto L_0x0029
            java.lang.Object r1 = r11.L$3
            java.util.Iterator r1 = (java.util.Iterator) r1
            java.lang.Object r3 = r11.L$2
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r3 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r3
            java.lang.Object r4 = r11.L$1
            com.portfolio.platform.data.source.local.sleep.SleepDao r4 = (com.portfolio.platform.data.source.local.sleep.SleepDao) r4
            java.lang.Object r5 = r11.L$0
            com.fossil.yi7 r5 = (com.fossil.yi7) r5
            com.fossil.t87.a(r12)
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r1
            r1 = r0
            r0 = r11
            goto L_0x00a2
        L_0x0029:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0031:
            java.lang.Object r1 = r11.L$0
            com.fossil.yi7 r1 = (com.fossil.yi7) r1
            com.fossil.t87.a(r12)
            goto L_0x006b
        L_0x0039:
            com.fossil.t87.a(r12)
            com.fossil.yi7 r1 = r11.p$
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "insertFromDevice: sleepSessionList = "
            r5.append(r6)
            java.util.List r6 = r11.$sleepSessionList
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r12.d(r4, r5)
            com.fossil.pg5 r12 = com.fossil.pg5.i
            r11.L$0 = r1
            r11.label = r3
            java.lang.Object r12 = r12.d(r11)
            if (r12 != r0) goto L_0x006b
            return r0
        L_0x006b:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r12 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r12
            com.portfolio.platform.data.source.local.sleep.SleepDao r12 = r12.sleepDao()
            java.util.List r3 = r11.$sleepSessionList
            java.util.Iterator r3 = r3.iterator()
            r4 = r12
            r5 = r1
            r1 = r3
            r12 = r11
        L_0x007b:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x00eb
            java.lang.Object r3 = r1.next()
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r3 = (com.portfolio.platform.data.model.room.sleep.MFSleepSession) r3
            com.portfolio.platform.data.source.SleepSessionsRepository r6 = r12.this$0
            r12.L$0 = r5
            r12.L$1 = r4
            r12.L$2 = r3
            r12.L$3 = r1
            r12.label = r2
            java.lang.Object r6 = r6.isExistsSleepSession(r3, r12)
            if (r6 != r0) goto L_0x009a
            return r0
        L_0x009a:
            r10 = r0
            r0 = r12
            r12 = r6
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r1
            r1 = r10
        L_0x00a2:
            java.lang.Boolean r12 = (java.lang.Boolean) r12
            boolean r12 = r12.booleanValue()
            java.lang.String r7 = ".saveSyncResult - Sleep session already existed"
            if (r12 == 0) goto L_0x00ba
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            r12.d(r4, r7)
            goto L_0x00e5
        L_0x00ba:
            int r12 = r4.getRealEndTime()
            long r8 = (long) r12
            com.portfolio.platform.data.model.room.sleep.MFSleepSession r12 = r5.getSleepSession(r8)
            if (r12 != 0) goto L_0x00d8
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r7 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            java.lang.String r8 = ".saveSyncResult - Saving sleep session to local database"
            r12.d(r7, r8)
            r5.addSleepSession(r4)
            goto L_0x00e5
        L_0x00d8:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.TAG
            r12.d(r4, r7)
        L_0x00e5:
            r12 = r0
            r0 = r1
            r1 = r3
            r4 = r5
            r5 = r6
            goto L_0x007b
        L_0x00eb:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$insertFromDevice$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
