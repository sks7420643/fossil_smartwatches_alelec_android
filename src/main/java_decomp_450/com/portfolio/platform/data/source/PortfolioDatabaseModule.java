package com.portfolio.platform.data.source;

import com.facebook.appevents.UserDataStore;
import com.fossil.ap4;
import com.fossil.bi;
import com.fossil.ch5;
import com.fossil.ci;
import com.fossil.co4;
import com.fossil.dp4;
import com.fossil.ee7;
import com.fossil.ep4;
import com.fossil.ge5;
import com.fossil.go4;
import com.fossil.hm4;
import com.fossil.jm4;
import com.fossil.km4;
import com.fossil.nn4;
import com.fossil.po4;
import com.fossil.qo4;
import com.fossil.se7;
import com.fossil.vn4;
import com.fossil.vo4;
import com.fossil.wo4;
import com.fossil.zo4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDataDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule {
    @DexIgnore
    public final AddressDao provideAddressDao(AddressDatabase addressDatabase) {
        ee7.b(addressDatabase, UserDataStore.DATE_OF_BIRTH);
        return addressDatabase.addressDao();
    }

    @DexIgnore
    public final synchronized AddressDatabase provideAddressDatabase(PortfolioApp portfolioApp) {
        ci b;
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, AddressDatabase.class, "address.db");
        a.d();
        b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (AddressDatabase) b;
    }

    @DexIgnore
    public final AppSettingsDatabase provideAppSettingsDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, AppSettingsDatabase.class, "app_settings_database.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (AppSettingsDatabase) b;
    }

    @DexIgnore
    public final CategoryDao provideCategoryDao(CategoryDatabase categoryDatabase) {
        ee7.b(categoryDatabase, UserDataStore.DATE_OF_BIRTH);
        return categoryDatabase.categoryDao();
    }

    @DexIgnore
    public final synchronized CategoryDatabase provideCategoryDatabase(PortfolioApp portfolioApp) {
        ci b;
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, CategoryDatabase.class, "category.db");
        a.d();
        b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (CategoryDatabase) b;
    }

    @DexIgnore
    public final ComplicationDao provideComplicationDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getComplicationDao();
    }

    @DexIgnore
    public final ComplicationLastSettingDao provideComplicationSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getComplicationSettingDao();
    }

    @DexIgnore
    public final CustomizeRealDataDao provideCustomizeRealDataDao(CustomizeRealDataDatabase customizeRealDataDatabase) {
        ee7.b(customizeRealDataDatabase, UserDataStore.DATE_OF_BIRTH);
        return customizeRealDataDatabase.realDataDao();
    }

    @DexIgnore
    public final CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, CustomizeRealDataDatabase.class, "customizeRealData.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (CustomizeRealDataDatabase) b;
    }

    @DexIgnore
    public final DNDSettingsDatabase provideDNDSettingsDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, DNDSettingsDatabase.class, "dndSettings.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DNDSettingsDatabase) b;
    }

    @DexIgnore
    public final DeviceDao provideDeviceDao(DeviceDatabase deviceDatabase) {
        ee7.b(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.deviceDao();
    }

    @DexIgnore
    public final DeviceDatabase provideDeviceDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, DeviceDatabase.class, "devices.db");
        a.a(DeviceDatabase.Companion.getMIGRATION_FROM_1_TO_2(), DeviceDatabase.Companion.getMIGRATION_FROM_2_TO_3());
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DeviceDatabase) b;
    }

    @DexIgnore
    public final DianaCustomizeDatabase provideDianaCustomizeDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, DianaCustomizeDatabase.class, "dianaCustomize.db");
        a.a(DianaCustomizeDatabase.Companion.getMIGRATION_FROM_13_TO_14(), DianaCustomizeDatabase.Companion.getMIGRATION_FROM_14_TO_15());
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return (DianaCustomizeDatabase) b;
    }

    @DexIgnore
    public final DianaPresetDao provideDianaPresetDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getPresetDao();
    }

    @DexIgnore
    public final FileDao provideFileDao(FileDatabase fileDatabase) {
        ee7.b(fileDatabase, UserDataStore.DATE_OF_BIRTH);
        return fileDatabase.fileDao();
    }

    @DexIgnore
    public final FileDatabase provideFileDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, FileDatabase.class, "file.db");
        a.a(FileDatabase.Companion.getMIGRATION_FROM_1_TO_2());
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (FileDatabase) b;
    }

    @DexIgnore
    public final ge5 provideFitnessHelper(ch5 ch5) {
        ee7.b(ch5, "sharedPreferencesManager");
        return new ge5(ch5);
    }

    @DexIgnore
    public final HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, HybridCustomizeDatabase.class, "hybridCustomize.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return (HybridCustomizeDatabase) b;
    }

    @DexIgnore
    public final InAppNotificationDao provideInAppNotificationDao(InAppNotificationDatabase inAppNotificationDatabase) {
        ee7.b(inAppNotificationDatabase, UserDataStore.DATE_OF_BIRTH);
        return inAppNotificationDatabase.inAppNotificationDao();
    }

    @DexIgnore
    public final InAppNotificationDatabase provideInAppNotificationDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, InAppNotificationDatabase.class, "inAppNotification.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (InAppNotificationDatabase) b;
    }

    @DexIgnore
    public final MicroAppDao provideMicroAppDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        ee7.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppDao();
    }

    @DexIgnore
    public final MicroAppLastSettingDao provideMicroAppLastSettingDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        ee7.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppLastSettingDao();
    }

    @DexIgnore
    public final NotificationSettingsDao provideNotificationSettingsDao(NotificationSettingsDatabase notificationSettingsDatabase) {
        ee7.b(notificationSettingsDatabase, UserDataStore.DATE_OF_BIRTH);
        return notificationSettingsDatabase.getNotificationSettingsDao();
    }

    @DexIgnore
    public final NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, NotificationSettingsDatabase.class, "notificationSettings.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (NotificationSettingsDatabase) b;
    }

    @DexIgnore
    public final HybridPresetDao providePresetDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        ee7.b(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.presetDao();
    }

    @DexIgnore
    public final QuickResponseDatabase provideQuickResponseDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        se7 se7 = new se7();
        se7.element = null;
        ci.a a = bi.a(portfolioApp.getApplicationContext(), QuickResponseDatabase.class, "quickResponse.db");
        a.d();
        a.a(new PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1(se7));
        T t = (T) ((QuickResponseDatabase) a.b());
        se7.element = t;
        return t;
    }

    @DexIgnore
    public final QuickResponseMessageDao provideQuickResponseMessageDao(QuickResponseDatabase quickResponseDatabase) {
        ee7.b(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseMessageDao();
    }

    @DexIgnore
    public final QuickResponseSenderDao provideQuickResponseSenderDao(QuickResponseDatabase quickResponseDatabase) {
        ee7.b(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseSenderDao();
    }

    @DexIgnore
    public final RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, RemindersSettingsDatabase.class, "remindersSettings.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (RemindersSettingsDatabase) b;
    }

    @DexIgnore
    public final SkuDao provideSkuDao(DeviceDatabase deviceDatabase) {
        ee7.b(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.skuDao();
    }

    @DexIgnore
    public final ThemeDao provideThemeDao(ThemeDatabase themeDatabase) {
        ee7.b(themeDatabase, UserDataStore.DATE_OF_BIRTH);
        return themeDatabase.themeDao();
    }

    @DexIgnore
    public final ThemeDatabase provideThemeDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, ThemeDatabase.class, "theme.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (ThemeDatabase) b;
    }

    @DexIgnore
    public final synchronized UserSettingDao provideUserSettingDao(UserSettingDatabase userSettingDatabase) {
        ee7.b(userSettingDatabase, UserDataStore.DATE_OF_BIRTH);
        return userSettingDatabase.userSettingDao();
    }

    @DexIgnore
    public final UserSettingDatabase provideUserSettingDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, UserSettingDatabase.class, "userSetting.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (UserSettingDatabase) b;
    }

    @DexIgnore
    public final WatchAppDao provideWatchAppDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchAppDao();
    }

    @DexIgnore
    public final WatchAppLastSettingDao provideWatchAppSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchAppSettingDao();
    }

    @DexIgnore
    public final BuddyChallengeDatabase providesBuddyChallengeDatabase(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "app");
        ci.a a = bi.a(portfolioApp, BuddyChallengeDatabase.class, "buddy_challenge.db");
        a.d();
        ci b = a.b();
        ee7.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return (BuddyChallengeDatabase) b;
    }

    @DexIgnore
    public final nn4 providesChallengeDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        ee7.b(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.a();
    }

    @DexIgnore
    public final po4 providesChallengeLocalDataSource(nn4 nn4) {
        ee7.b(nn4, "dao");
        return new po4(nn4);
    }

    @DexIgnore
    public final qo4 providesChallengeRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        return new qo4(apiServiceV2);
    }

    @DexIgnore
    public final hm4 providesFlagDao(AppSettingsDatabase appSettingsDatabase) {
        ee7.b(appSettingsDatabase, "appSettingsDatabase");
        return appSettingsDatabase.a();
    }

    @DexIgnore
    public final jm4 providesFlagLocalSource(hm4 hm4) {
        ee7.b(hm4, "dao");
        return new jm4(hm4);
    }

    @DexIgnore
    public final km4 providesFlagRemoteSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        return new km4(apiServiceV2);
    }

    @DexIgnore
    public final co4 providesNotificationDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        ee7.b(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.c();
    }

    @DexIgnore
    public final zo4 providesNotificationLocal(co4 co4) {
        ee7.b(co4, "dao");
        return new zo4(co4);
    }

    @DexIgnore
    public final ap4 providesNotificationRemote(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        return new ap4(apiServiceV2);
    }

    @DexIgnore
    public final RingStyleDao providesRingStyleDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getRingStyleDao();
    }

    @DexIgnore
    public final vn4 providesSocialFriendDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        ee7.b(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.b();
    }

    @DexIgnore
    public final vo4 providesSocialFriendLocal(vn4 vn4) {
        ee7.b(vn4, "dao");
        return new vo4(vn4);
    }

    @DexIgnore
    public final wo4 providesSocialFriendRemote(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        return new wo4(apiServiceV2);
    }

    @DexIgnore
    public final go4 providesSocialProfileDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        ee7.b(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.d();
    }

    @DexIgnore
    public final dp4 providesSocialProfileLocal(go4 go4) {
        ee7.b(go4, "dao");
        return new dp4(go4);
    }

    @DexIgnore
    public final ep4 providesSocialProfileRemote(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        return new ep4(apiServiceV2);
    }

    @DexIgnore
    public final WatchAppDataDao providesWatchAppDataDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchAppDataDao();
    }

    @DexIgnore
    public final WatchFaceDao providesWatchFaceDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        ee7.b(dianaCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return dianaCustomizeDatabase.getWatchFaceDao();
    }
}
