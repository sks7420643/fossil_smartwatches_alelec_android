package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.uh;
import com.fossil.vh;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSleepDao_Impl implements GFitSleepDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<GFitSleep> __deletionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ vh<GFitSleep> __insertionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<GFitSleep> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSleep` (`id`,`sleepMins`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitSleep gFitSleep) {
            ajVar.bindLong(1, (long) gFitSleep.getId());
            ajVar.bindLong(2, (long) gFitSleep.getSleepMins());
            ajVar.bindLong(3, gFitSleep.getStartTime());
            ajVar.bindLong(4, gFitSleep.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<GFitSleep> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `gFitSleep` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, GFitSleep gFitSleep) {
            ajVar.bindLong(1, (long) gFitSleep.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM GFitSleep";
        }
    }

    @DexIgnore
    public GFitSleepDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfGFitSleep = new Anon1(ciVar);
        this.__deletionAdapterOfGFitSleep = new Anon2(ciVar);
        this.__preparedStmtOfClearAll = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void deleteListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSleep.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public List<GFitSleep> getAllGFitSleep() {
        fi b = fi.b("SELECT * FROM gFitSleep", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "sleepMins");
            int b4 = oi.b(a, SampleRaw.COLUMN_START_TIME);
            int b5 = oi.b(a, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSleep gFitSleep = new GFitSleep(a.getInt(b3), a.getLong(b4), a.getLong(b5));
                gFitSleep.setId(a.getInt(b2));
                arrayList.add(gFitSleep);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertGFitSleep(GFitSleep gFitSleep) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(gFitSleep);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
