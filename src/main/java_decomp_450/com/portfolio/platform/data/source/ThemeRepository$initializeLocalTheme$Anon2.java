package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ThemeRepository$initializeLocalTheme$2", f = "ThemeRepository.kt", l = {44, 45}, m = "invokeSuspend")
public final class ThemeRepository$initializeLocalTheme$Anon2 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$initializeLocalTheme$Anon2(ThemeRepository themeRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ThemeRepository$initializeLocalTheme$Anon2 themeRepository$initializeLocalTheme$Anon2 = new ThemeRepository$initializeLocalTheme$Anon2(this.this$0, fb7);
        themeRepository$initializeLocalTheme$Anon2.p$ = (yi7) obj;
        return themeRepository$initializeLocalTheme$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((ThemeRepository$initializeLocalTheme$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            FLogger.INSTANCE.getLocal().d(ThemeRepository.Companion.getTAG(), "initializeLocalTheme");
            this.this$0.loadTheme(ThemeRepository.DEFAULT_THEME_URI);
            ThemeRepository themeRepository = this.this$0;
            String j = PortfolioApp.g0.c().j();
            this.L$0 = yi7;
            this.label = 1;
            if (themeRepository.setCurrentThemeId(j, this) == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            return i97.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        eh5 a2 = eh5.l.a();
        this.L$0 = yi7;
        this.label = 2;
        if (a2.a(this) == a) {
            return a;
        }
        return i97.a;
    }
}
