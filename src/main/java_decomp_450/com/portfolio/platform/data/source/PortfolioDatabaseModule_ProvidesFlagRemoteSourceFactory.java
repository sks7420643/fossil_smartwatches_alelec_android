package com.portfolio.platform.data.source;

import com.fossil.c87;
import com.fossil.km4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory implements Factory<km4> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static km4 providesFlagRemoteSource(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        km4 providesFlagRemoteSource = portfolioDatabaseModule.providesFlagRemoteSource(apiServiceV2);
        c87.a(providesFlagRemoteSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesFlagRemoteSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public km4 get() {
        return providesFlagRemoteSource(this.module, this.apiProvider.get());
    }
}
