package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WorkoutSettingRepository";
    @DexIgnore
    public /* final */ WorkoutSettingRemoteDataSource mWorkoutSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public WorkoutSettingRepository(WorkoutSettingRemoteDataSource workoutSettingRemoteDataSource) {
        ee7.b(workoutSettingRemoteDataSource, "mWorkoutSettingRemoteDataSource");
        this.mWorkoutSettingRemoteDataSource = workoutSettingRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new WorkoutSettingRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object downloadWorkoutSettings(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new WorkoutSettingRepository$downloadWorkoutSettings$Anon2(this, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.Object executePendingRequest(com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WorkoutSettingRepository$executePendingRequest$Anon1     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x0015
            r0 = r7
            com.portfolio.platform.data.source.WorkoutSettingRepository$executePendingRequest$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSettingRepository$executePendingRequest$Anon1) r0     // Catch:{ all -> 0x0122 }
            int r1 = r0.label     // Catch:{ all -> 0x0122 }
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0015
            int r7 = r0.label     // Catch:{ all -> 0x0122 }
            int r7 = r7 - r2
            r0.label = r7     // Catch:{ all -> 0x0122 }
            goto L_0x001a
        L_0x0015:
            com.portfolio.platform.data.source.WorkoutSettingRepository$executePendingRequest$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSettingRepository$executePendingRequest$Anon1     // Catch:{ all -> 0x0122 }
            r0.<init>(r6, r7)     // Catch:{ all -> 0x0122 }
        L_0x001a:
            java.lang.Object r7 = r0.result     // Catch:{ all -> 0x0122 }
            java.lang.Object r1 = com.fossil.nb7.a()     // Catch:{ all -> 0x0122 }
            int r2 = r0.label     // Catch:{ all -> 0x0122 }
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 == r4) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r1 = r0.L$2     // Catch:{ all -> 0x0122 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x0122 }
            java.lang.Object r1 = r0.L$1     // Catch:{ all -> 0x0122 }
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao r1 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao) r1     // Catch:{ all -> 0x0122 }
            java.lang.Object r0 = r0.L$0     // Catch:{ all -> 0x0122 }
            com.portfolio.platform.data.source.WorkoutSettingRepository r0 = (com.portfolio.platform.data.source.WorkoutSettingRepository) r0     // Catch:{ all -> 0x0122 }
            com.fossil.t87.a(r7)     // Catch:{ all -> 0x0122 }
            goto L_0x009e
        L_0x003a:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)     // Catch:{ all -> 0x0122 }
            throw r7     // Catch:{ all -> 0x0122 }
        L_0x0042:
            java.lang.Object r2 = r0.L$0     // Catch:{ all -> 0x0122 }
            com.portfolio.platform.data.source.WorkoutSettingRepository r2 = (com.portfolio.platform.data.source.WorkoutSettingRepository) r2     // Catch:{ all -> 0x0122 }
            com.fossil.t87.a(r7)     // Catch:{ all -> 0x0122 }
            goto L_0x0069
        L_0x004a:
            com.fossil.t87.a(r7)     // Catch:{ all -> 0x0122 }
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0122 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ all -> 0x0122 }
            java.lang.String r2 = "WorkoutSettingRepository"
            java.lang.String r5 = "executePendingRequest()"
            r7.d(r2, r5)     // Catch:{ all -> 0x0122 }
            com.fossil.pg5 r7 = com.fossil.pg5.i     // Catch:{ all -> 0x0122 }
            r0.L$0 = r6     // Catch:{ all -> 0x0122 }
            r0.label = r4     // Catch:{ all -> 0x0122 }
            java.lang.Object r7 = r7.g(r0)     // Catch:{ all -> 0x0122 }
            if (r7 != r1) goto L_0x0068
            monitor-exit(r6)
            return r1
        L_0x0068:
            r2 = r6
        L_0x0069:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r7 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase) r7
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao r7 = r7.workoutSettingDao()
            java.util.List r4 = r7.getPendingWorkoutSettings()
            boolean r5 = r4.isEmpty()
            if (r5 == 0) goto L_0x008a
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = "WorkoutSettingRepository"
            java.lang.String r1 = "executePendingRequest() - no pending items, not executed"
            r7.d(r0, r1)
            com.fossil.i97 r7 = com.fossil.i97.a
            monitor-exit(r6)
            return r7
        L_0x008a:
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource r5 = r2.mWorkoutSettingRemoteDataSource
            r0.L$0 = r2
            r0.L$1 = r7
            r0.L$2 = r4
            r0.label = r3
            java.lang.Object r0 = r5.upsertWorkoutSettingList(r4, r0)
            if (r0 != r1) goto L_0x009c
            monitor-exit(r6)
            return r1
        L_0x009c:
            r1 = r7
            r7 = r0
        L_0x009e:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r0 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r0 == 0) goto L_0x00e3
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = "WorkoutSettingRepository"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "executePendingRequest() - success "
            r4.append(r5)
            r5 = r7
            com.fossil.bj5 r5 = (com.fossil.bj5) r5
            java.lang.Object r5 = r5.a()
            if (r5 == 0) goto L_0x00df
            java.util.List r5 = (java.util.List) r5
            r4.append(r5)
            java.lang.String r2 = r4.toString()
            r0.d(r3, r2)
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            java.util.List r7 = (java.util.List) r7
            r0 = 0
            java.lang.Object r7 = r7.get(r0)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting r7 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting) r7
            r1.upsertWorkoutSetting(r7)
            goto L_0x011e
        L_0x00df:
            com.fossil.ee7.a()
            throw r2
        L_0x00e3:
            boolean r0 = r7 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x011e
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "WorkoutSettingRepository"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "executePendingRequest() - fail!! "
            r3.append(r4)
            r4 = r7
            com.fossil.yi5 r4 = (com.fossil.yi5) r4
            int r4 = r4.a()
            r3.append(r4)
            java.lang.String r4 = " serverCode "
            r3.append(r4)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x0114
            java.lang.Integer r2 = r7.getCode()
        L_0x0114:
            r3.append(r2)
            java.lang.String r7 = r3.toString()
            r0.d(r1, r7)
        L_0x011e:
            com.fossil.i97 r7 = com.fossil.i97.a
            monitor-exit(r6)
            return r7
        L_0x0122:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.executePendingRequest(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object getWorkoutSettingList(fb7<? super List<WorkoutSetting>> fb7) {
        return vh7.a(qj7.b(), new WorkoutSettingRepository$getWorkoutSettingList$Anon2(null), fb7);
    }

    @DexIgnore
    public final Object getWorkoutSettingListAsLiveData(fb7<? super LiveData<List<WorkoutSetting>>> fb7) {
        return vh7.a(qj7.c(), new WorkoutSettingRepository$getWorkoutSettingListAsLiveData$Anon2(null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0082 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertWorkoutSettingList(java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting> r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.WorkoutSettingRepository$upsertWorkoutSettingList$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.portfolio.platform.data.source.WorkoutSettingRepository$upsertWorkoutSettingList$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSettingRepository$upsertWorkoutSettingList$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.WorkoutSettingRepository$upsertWorkoutSettingList$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSettingRepository$upsertWorkoutSettingList$Anon1
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            java.lang.String r5 = "WorkoutSettingRepository"
            if (r2 == 0) goto L_0x004e
            if (r2 == r4) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r8 = r0.L$2
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao r8 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao) r8
            java.lang.Object r1 = r0.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.WorkoutSettingRepository r0 = (com.portfolio.platform.data.source.WorkoutSettingRepository) r0
            com.fossil.t87.a(r9)
            goto L_0x0086
        L_0x003a:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0042:
            java.lang.Object r8 = r0.L$1
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r2 = r0.L$0
            com.portfolio.platform.data.source.WorkoutSettingRepository r2 = (com.portfolio.platform.data.source.WorkoutSettingRepository) r2
            com.fossil.t87.a(r9)
            goto L_0x006c
        L_0x004e:
            com.fossil.t87.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r2 = "upsertWorkoutSettingList()"
            r9.d(r5, r2)
            com.fossil.pg5 r9 = com.fossil.pg5.i
            r0.L$0 = r7
            r0.L$1 = r8
            r0.label = r4
            java.lang.Object r9 = r9.g(r0)
            if (r9 != r1) goto L_0x006b
            return r1
        L_0x006b:
            r2 = r7
        L_0x006c:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r9 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase) r9
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao r9 = r9.workoutSettingDao()
            com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource r6 = r2.mWorkoutSettingRemoteDataSource
            r0.L$0 = r2
            r0.L$1 = r8
            r0.L$2 = r9
            r0.label = r3
            java.lang.Object r0 = r6.upsertWorkoutSettingList(r8, r0)
            if (r0 != r1) goto L_0x0083
            return r1
        L_0x0083:
            r1 = r8
            r8 = r9
            r9 = r0
        L_0x0086:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r0 = r9 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x00f1
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r0 = r9.a()
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = 0
            if (r0 == 0) goto L_0x009f
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x009e
            goto L_0x009f
        L_0x009e:
            r4 = 0
        L_0x009f:
            if (r4 != 0) goto L_0x00e5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upsertWorkoutSettingList() - success "
            r2.append(r3)
            java.lang.Object r3 = r9.a()
            java.util.List r3 = (java.util.List) r3
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r5, r2)
            java.lang.Object r0 = r9.a()
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x00cb:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00db
            java.lang.Object r2 = r0.next()
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting r2 = (com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting) r2
            r2.setPinType(r1)
            goto L_0x00cb
        L_0x00db:
            java.lang.Object r9 = r9.a()
            java.util.List r9 = (java.util.List) r9
            r8.upsertWorkoutSettingList(r9)
            goto L_0x012c
        L_0x00e5:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = "upsertWorkoutSettingList() - empty list"
            r8.d(r5, r9)
            goto L_0x012c
        L_0x00f1:
            boolean r0 = r9 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x012c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upsertWorkoutSettingList() - fail!! "
            r2.append(r3)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r3 = r9.a()
            r2.append(r3)
            java.lang.String r3 = " serverCode "
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x011e
            java.lang.Integer r9 = r9.getCode()
            goto L_0x011f
        L_0x011e:
            r9 = 0
        L_0x011f:
            r2.append(r9)
            java.lang.String r9 = r2.toString()
            r0.d(r5, r9)
            r8.upsertWorkoutSettingList(r1)
        L_0x012c:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.upsertWorkoutSettingList(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
