package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.mf;
import com.fossil.pj4;
import com.fossil.qj7;
import com.fossil.te5;
import com.fossil.ue5;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zh;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource extends mf<Long, WorkoutSession> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public te5 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ zh.c mObserver; // = new Anon1(this, "workout_session", new String[0]);
    @DexIgnore
    public int mOffset;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends zh.c {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = workoutSessionLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            ee7.b(set, "tables");
            FLogger.INSTANCE.getLocal().d(WorkoutSessionLocalDataSource.Companion.getTAG(), "XXX- invalidate workout session table");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return WorkoutSessionLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutSessionLocalDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionLocalDataSource(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, pj4 pj4, te5.a aVar) {
        ee7.b(workoutSessionRepository2, "workoutSessionRepository");
        ee7.b(fitnessDatabase, "mFitnessDatabase");
        ee7.b(date, "currentDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.mFitnessDatabase = fitnessDatabase;
        this.currentDate = date;
        this.listener = aVar;
        te5 te5 = new te5(pj4.a());
        this.mHelper = te5;
        this.mNetworkState = ue5.a(te5);
        this.mHelper.a(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<WorkoutSession> getDataInDatabase(int i) {
        return this.mFitnessDatabase.getWorkoutDao().getWorkoutSessionsInDateDesc(this.currentDate, i);
    }

    @DexIgnore
    private final ik7 loadData(te5.b.a aVar, int i) {
        return xh7.b(zi7.a(qj7.b()), null, null, new WorkoutSessionLocalDataSource$loadData$Anon1(this, i, aVar, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ ik7 loadData$default(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, te5.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return workoutSessionLocalDataSource.loadData(aVar, i);
    }

    @DexIgnore
    public final te5 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadAfter(mf.f<Long> fVar, mf.a<WorkoutSession> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - params = " + ((Object) fVar.a) + " & currentDate = " + this.currentDate);
        WorkoutDao workoutDao = this.mFitnessDatabase.getWorkoutDao();
        Date date = this.currentDate;
        Key key = fVar.a;
        ee7.a((Object) key, "params.key");
        aVar.a(workoutDao.getWorkoutSessionsInDateAfterDesc(date, key.longValue(), fVar.b));
        this.mHelper.a(te5.d.AFTER, new WorkoutSessionLocalDataSource$loadAfter$Anon1(this));
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadBefore(mf.f<Long> fVar, mf.a<WorkoutSession> aVar) {
        ee7.b(fVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadInitial(mf.e<Long> eVar, mf.c<WorkoutSession> cVar) {
        ee7.b(eVar, NativeProtocol.WEB_DIALOG_PARAMS);
        ee7.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - currentDate = " + this.currentDate);
        cVar.a(getDataInDatabase(eVar.b));
        this.mHelper.a(te5.d.INITIAL, new WorkoutSessionLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(te5 te5) {
        ee7.b(te5, "<set-?>");
        this.mHelper = te5;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        ee7.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(WorkoutSession workoutSession) {
        ee7.b(workoutSession, "item");
        return Long.valueOf(workoutSession.getCreatedAt());
    }
}
