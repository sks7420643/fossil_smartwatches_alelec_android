package com.portfolio.platform.data.source;

import com.facebook.share.internal.VideoUploader;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$getPendingActivities$2", f = "ActivitiesRepository.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
public final class ActivitiesRepository$getPendingActivities$Anon2 extends zb7 implements kd7<yi7, fb7<? super List<SampleRaw>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getPendingActivities$Anon2(Date date, Date date2, fb7 fb7) {
        super(2, fb7);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        ActivitiesRepository$getPendingActivities$Anon2 activitiesRepository$getPendingActivities$Anon2 = new ActivitiesRepository$getPendingActivities$Anon2(this.$startDate, this.$endDate, fb7);
        activitiesRepository$getPendingActivities$Anon2.p$ = (yi7) obj;
        return activitiesRepository$getPendingActivities$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super List<SampleRaw>> fb7) {
        return ((ActivitiesRepository$getPendingActivities$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Calendar calendar;
        Calendar calendar2;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            calendar = Calendar.getInstance();
            ee7.a((Object) calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            calendar.setTime(this.$startDate);
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "end");
            instance.setTime(this.$endDate);
            pg5 pg5 = pg5.i;
            this.L$0 = yi7;
            this.L$1 = calendar;
            this.L$2 = instance;
            this.label = 1;
            obj = pg5.b(this);
            if (obj == a) {
                return a;
            }
            calendar2 = instance;
        } else if (i == 1) {
            calendar2 = (Calendar) this.L$2;
            calendar = (Calendar) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SampleRawDao sampleRawDao = ((FitnessDatabase) obj).sampleRawDao();
        ee7.a((Object) calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date q = zd5.q(calendar.getTime());
        ee7.a((Object) q, "DateHelper.getStartOfDay(start.time)");
        ee7.a((Object) calendar2, "end");
        Date l = zd5.l(calendar2.getTime());
        ee7.a((Object) l, "DateHelper.getEndOfDay(end.time)");
        return sampleRawDao.getPendingActivitySamples(q, l);
    }
}
