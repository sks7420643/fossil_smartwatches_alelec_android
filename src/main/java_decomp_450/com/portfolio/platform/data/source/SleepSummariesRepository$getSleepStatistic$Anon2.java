package com.portfolio.platform.data.source;

import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.i97;
import com.fossil.lx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.SleepStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSleepStatistic$Anon2 extends lx6<SleepStatistic, SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$Anon2(SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.lx6
    public Object createCall(fb7<? super fv7<SleepStatistic>> fb7) {
        return this.this$0.mApiService.getSleepStatistic(fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.lx6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<com.portfolio.platform.data.SleepStatistic>> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2) r0
            com.fossil.t87.a(r5)
            goto L_0x0045
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.fossil.pg5 r5 = com.fossil.pg5.i
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.d(r0)
            if (r5 != r1) goto L_0x0045
            return r1
        L_0x0045:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r5 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r5
            com.portfolio.platform.data.source.local.sleep.SleepDao r5 = r5.sleepDao()
            androidx.lifecycle.LiveData r5 = r5.getSleepStatisticLiveData()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2.loadFromDb(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.lx6
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
    @Override // com.fossil.lx6
    public /* bridge */ /* synthetic */ Object saveCallResult(SleepStatistic sleepStatistic, fb7 fb7) {
        return saveCallResult(sleepStatistic, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object saveCallResult(com.portfolio.platform.data.SleepStatistic r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r7 = r0.L$1
            com.portfolio.platform.data.SleepStatistic r7 = (com.portfolio.platform.data.SleepStatistic) r7
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2) r0
            com.fossil.t87.a(r8)
            goto L_0x0069
        L_0x0031:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x0039:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.SleepSummariesRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSleepStatistic - saveCallResult -- item="
            r4.append(r5)
            r4.append(r7)
            java.lang.String r4 = r4.toString()
            r8.d(r2, r4)
            com.fossil.pg5 r8 = com.fossil.pg5.i
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r8 = r8.d(r0)
            if (r8 != r1) goto L_0x0069
            return r1
        L_0x0069:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r8 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r8
            com.portfolio.platform.data.source.local.sleep.SleepDao r8 = r8.sleepDao()
            r8.upsertSleepStatistic(r7)
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2.saveCallResult(com.portfolio.platform.data.SleepStatistic, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public boolean shouldFetch(SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }
}
