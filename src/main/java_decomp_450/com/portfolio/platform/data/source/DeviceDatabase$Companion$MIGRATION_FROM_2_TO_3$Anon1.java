package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.li;
import com.fossil.wi;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1 extends li {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.li
    public void migrate(wi wiVar) {
        ee7.b(wiVar, "database");
        wiVar.beginTransaction();
        wiVar.execSQL("ALTER TABLE SKU ADD COLUMN fastPairId TEXT");
        wiVar.setTransactionSuccessful();
        wiVar.endTransaction();
    }
}
