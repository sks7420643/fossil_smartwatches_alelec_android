package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSampleRepository.class.getSimpleName();
        ee7.a((Object) simpleName, "HeartRateSampleRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSampleRepository(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchHeartRateSamples$default(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, fb7 fb7, int i3, Object obj) {
        return heartRateSampleRepository.fetchHeartRateSamples(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, fb7);
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new HeartRateSampleRepository$cleanUp$Anon2(null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final Object fetchHeartRateSamples(Date date, Date date2, int i, int i2, fb7<? super zi5<ApiResponse<HeartRate>>> fb7) {
        return vh7.a(qj7.b(), new HeartRateSampleRepository$fetchHeartRateSamples$Anon2(this, date, date2, i, i2, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getHeartRateSamples(java.util.Date r11, java.util.Date r12, boolean r13, com.fossil.fb7<? super androidx.lifecycle.LiveData<com.fossil.qx6<java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r11 = r0.Z$0
            java.lang.Object r11 = r0.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$1
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r11 = r0.L$0
            com.portfolio.platform.data.source.HeartRateSampleRepository r11 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r11
            com.fossil.t87.a(r14)
            goto L_0x0062
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.tk7 r14 = com.fossil.qj7.c()
            com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2 r2 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2
            r9 = 0
            r4 = r2
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.Z$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)
            if (r14 != r1) goto L_0x0062
            return r1
        L_0x0062:
            java.lang.String r11 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.fossil.ee7.a(r14, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples(java.util.Date, java.util.Date, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<HeartRateSample> list, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.b(), new HeartRateSampleRepository$insertFromDevice$Anon2(list, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }
}
