package com.portfolio.platform.data.source.local.sleep;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.fv4;
import com.fossil.gv4;
import com.fossil.hv4;
import com.fossil.ji;
import com.fossil.mu4;
import com.fossil.n4;
import com.fossil.nu4;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.si;
import com.fossil.vh;
import com.fossil.yu4;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDao_Impl extends SleepDao {
    @DexIgnore
    public /* final */ mu4 __dateShortStringConverter; // = new mu4();
    @DexIgnore
    public /* final */ nu4 __dateTimeConverter; // = new nu4();
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<MFSleepDay> __insertionAdapterOfMFSleepDay;
    @DexIgnore
    public /* final */ vh<MFSleepSession> __insertionAdapterOfMFSleepSession;
    @DexIgnore
    public /* final */ vh<MFSleepSession> __insertionAdapterOfMFSleepSession_1;
    @DexIgnore
    public /* final */ vh<MFSleepSettings> __insertionAdapterOfMFSleepSettings;
    @DexIgnore
    public /* final */ vh<SleepRecommendedGoal> __insertionAdapterOfSleepRecommendedGoal;
    @DexIgnore
    public /* final */ vh<SleepStatistic> __insertionAdapterOfSleepStatistic;
    @DexIgnore
    public /* final */ yu4 __integerArrayConverter; // = new yu4();
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllSleepDays;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDeleteAllSleepSessions;
    @DexIgnore
    public /* final */ ji __preparedStmtOfUpdateSleepSettings;
    @DexIgnore
    public /* final */ fv4 __sleepDistributionConverter; // = new fv4();
    @DexIgnore
    public /* final */ gv4 __sleepSessionHeartRateConverter; // = new gv4();
    @DexIgnore
    public /* final */ hv4 __sleepStatisticConverter; // = new hv4();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<MFSleepSession> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MFSleepSession mFSleepSession) {
            ajVar.bindLong(1, (long) mFSleepSession.getPinType());
            ajVar.bindLong(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindLong(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            ajVar.bindDouble(7, mFSleepSession.getNormalizedSleepQuality());
            ajVar.bindLong(8, (long) mFSleepSession.getSource());
            ajVar.bindLong(9, (long) mFSleepSession.getRealStartTime());
            ajVar.bindLong(10, (long) mFSleepSession.getRealEndTime());
            ajVar.bindLong(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindLong(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindLong(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindLong(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                ajVar.bindNull(18);
            } else {
                ajVar.bindString(18, a4);
            }
            ajVar.bindLong(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            ajVar.bindLong(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            ajVar.bindLong(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<MFSleepSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon10(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MFSleepSession> call() throws Exception {
            Integer num;
            Integer num2;
            Integer num3;
            int i;
            Integer num4;
            int i2;
            Integer num5;
            int i3;
            Anon10 anon10 = this;
            Cursor a = pi.a(SleepDao_Impl.this.__db, anon10.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "date");
                int b3 = oi.b(a, "day");
                int b4 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int b5 = oi.b(a, "syncTime");
                int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                int b8 = oi.b(a, "source");
                int b9 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i4 = b;
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b18 = oi.b(a, "heartRate");
                int b19 = oi.b(a, "createdAt");
                int b20 = oi.b(a, "updatedAt");
                int b21 = oi.b(a, "timezoneOffset");
                int i5 = b14;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b2);
                    Date a2 = SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3));
                    String string = a.getString(b4);
                    if (a.isNull(b5)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b5));
                    }
                    if (a.isNull(b6)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b6));
                    }
                    double d = a.getDouble(b7);
                    int i6 = a.getInt(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    SleepDistribution a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b12));
                    if (a.isNull(b13)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b13));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b15;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b15;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        b15 = i2;
                        i3 = b16;
                        num5 = null;
                    } else {
                        i5 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i3 = b16;
                        b15 = i2;
                    }
                    b16 = i3;
                    SleepDistribution a4 = SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(i3));
                    String string2 = a.getString(b17);
                    b17 = b17;
                    String string3 = a.getString(b18);
                    b18 = b18;
                    SleepSessionHeartRate a5 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(string3);
                    DateTime a6 = SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b19));
                    long j2 = a.getLong(b20);
                    b20 = b20;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i6, i7, i8, i9, a3, num3, num4, num5, a4, string2, a5, a6, SleepDao_Impl.this.__dateTimeConverter.a(j2), a.getInt(b21));
                    mFSleepSession.setPinType(a.getInt(i4));
                    arrayList.add(mFSleepSession);
                    anon10 = this;
                    b21 = b21;
                    i4 = i4;
                    b3 = b3;
                    b2 = b2;
                    b19 = b19;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<MFSleepDay> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon11(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public MFSleepDay call() throws Exception {
            MFSleepDay mFSleepDay = null;
            Cursor a = pi.a(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "timezoneOffset");
                int b3 = oi.b(a, "date");
                int b4 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int b7 = oi.b(a, "createdAt");
                int b8 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                }
                return mFSleepDay;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon12(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MFSleepDay> call() throws Exception {
            Cursor a = pi.a(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "pinType");
                int b2 = oi.b(a, "timezoneOffset");
                int b3 = oi.b(a, "date");
                int b4 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int b7 = oi.b(a, "createdAt");
                int b8 = oi.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    MFSleepDay mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), SleepDao_Impl.this.__sleepDistributionConverter.a(a.getString(b6)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b7)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b8)));
                    mFSleepDay.setPinType(a.getInt(b));
                    mFSleepDay.setTimezoneOffset(a.getInt(b2));
                    arrayList.add(mFSleepDay);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<SleepRecommendedGoal> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon13(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public SleepRecommendedGoal call() throws Exception {
            SleepRecommendedGoal sleepRecommendedGoal = null;
            Cursor a = pi.a(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "recommendedSleepGoal");
                if (a.moveToFirst()) {
                    SleepRecommendedGoal sleepRecommendedGoal2 = new SleepRecommendedGoal(a.getInt(b2));
                    sleepRecommendedGoal2.setId(a.getInt(b));
                    sleepRecommendedGoal = sleepRecommendedGoal2;
                }
                return sleepRecommendedGoal;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<SleepStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon14(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public SleepStatistic call() throws Exception {
            SleepStatistic sleepStatistic = null;
            Cursor a = pi.a(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "id");
                int b2 = oi.b(a, "uid");
                int b3 = oi.b(a, "sleepTimeBestDay");
                int b4 = oi.b(a, "sleepTimeBestStreak");
                int b5 = oi.b(a, "totalDays");
                int b6 = oi.b(a, "totalSleeps");
                int b7 = oi.b(a, "totalSleepMinutes");
                int b8 = oi.b(a, "totalSleepStateDistInMinute");
                int b9 = oi.b(a, "createdAt");
                int b10 = oi.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    sleepStatistic = new SleepStatistic(a.getString(b), a.getString(b2), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b3)), SleepDao_Impl.this.__sleepStatisticConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), a.getInt(b7), SleepDao_Impl.this.__integerArrayConverter.a(a.getString(b8)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b9)), SleepDao_Impl.this.__dateTimeConverter.a(a.getLong(b10)));
                }
                return sleepStatistic;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh<MFSleepSession> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MFSleepSession mFSleepSession) {
            ajVar.bindLong(1, (long) mFSleepSession.getPinType());
            ajVar.bindLong(2, mFSleepSession.getDate());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindLong(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            ajVar.bindDouble(7, mFSleepSession.getNormalizedSleepQuality());
            ajVar.bindLong(8, (long) mFSleepSession.getSource());
            ajVar.bindLong(9, (long) mFSleepSession.getRealStartTime());
            ajVar.bindLong(10, (long) mFSleepSession.getRealEndTime());
            ajVar.bindLong(11, (long) mFSleepSession.getRealSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getRealSleepStateDistInMinute());
            if (a2 == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, a2);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindLong(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindLong(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindLong(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String a3 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepSession.getEditedSleepStateDistInMinute());
            if (a3 == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, a3);
            }
            if (mFSleepSession.getSleepStates() == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, mFSleepSession.getSleepStates());
            }
            String a4 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(mFSleepSession.getHeartRate());
            if (a4 == null) {
                ajVar.bindNull(18);
            } else {
                ajVar.bindString(18, a4);
            }
            ajVar.bindLong(19, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getCreatedAt()));
            ajVar.bindLong(20, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepSession.getUpdatedAt()));
            ajVar.bindLong(21, (long) mFSleepSession.getTimezoneOffset());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh<MFSleepDay> {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_date` (`pinType`,`timezoneOffset`,`date`,`goalMinutes`,`sleepMinutes`,`sleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MFSleepDay mFSleepDay) {
            ajVar.bindLong(1, (long) mFSleepDay.getPinType());
            ajVar.bindLong(2, (long) mFSleepDay.getTimezoneOffset());
            String a = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepDay.getDate());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            ajVar.bindLong(4, (long) mFSleepDay.getGoalMinutes());
            ajVar.bindLong(5, (long) mFSleepDay.getSleepMinutes());
            String a2 = SleepDao_Impl.this.__sleepDistributionConverter.a(mFSleepDay.getSleepStateDistInMinute());
            if (a2 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a2);
            }
            ajVar.bindLong(7, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getCreatedAt()));
            ajVar.bindLong(8, SleepDao_Impl.this.__dateTimeConverter.a(mFSleepDay.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh<MFSleepSettings> {
        @DexIgnore
        public Anon4(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR FAIL INTO `sleep_settings` (`id`,`sleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MFSleepSettings mFSleepSettings) {
            ajVar.bindLong(1, (long) mFSleepSettings.getId());
            ajVar.bindLong(2, (long) mFSleepSettings.getSleepGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh<SleepRecommendedGoal> {
        @DexIgnore
        public Anon5(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleepRecommendedGoals` (`id`,`recommendedSleepGoal`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, SleepRecommendedGoal sleepRecommendedGoal) {
            ajVar.bindLong(1, (long) sleepRecommendedGoal.getId());
            ajVar.bindLong(2, (long) sleepRecommendedGoal.getRecommendedSleepGoal());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh<SleepStatistic> {
        @DexIgnore
        public Anon6(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_statistic` (`id`,`uid`,`sleepTimeBestDay`,`sleepTimeBestStreak`,`totalDays`,`totalSleeps`,`totalSleepMinutes`,`totalSleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, SleepStatistic sleepStatistic) {
            if (sleepStatistic.getId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, sleepStatistic.getId());
            }
            if (sleepStatistic.getUid() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, sleepStatistic.getUid());
            }
            String a = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestDay());
            if (a == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a);
            }
            String a2 = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestStreak());
            if (a2 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a2);
            }
            ajVar.bindLong(5, (long) sleepStatistic.getTotalDays());
            ajVar.bindLong(6, (long) sleepStatistic.getTotalSleeps());
            ajVar.bindLong(7, (long) sleepStatistic.getTotalSleepMinutes());
            String a3 = SleepDao_Impl.this.__integerArrayConverter.a(sleepStatistic.getTotalSleepStateDistInMinute());
            if (a3 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a3);
            }
            ajVar.bindLong(9, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getCreatedAt()));
            ajVar.bindLong(10, SleepDao_Impl.this.__dateTimeConverter.a(sleepStatistic.getUpdatedAt()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends ji {
        @DexIgnore
        public Anon7(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM sleep_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends ji {
        @DexIgnore
        public Anon8(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM sleep_date";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends ji {
        @DexIgnore
        public Anon9(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE sleep_settings SET sleepGoal = ?";
        }
    }

    @DexIgnore
    public SleepDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfMFSleepSession = new Anon1(ciVar);
        this.__insertionAdapterOfMFSleepSession_1 = new Anon2(ciVar);
        this.__insertionAdapterOfMFSleepDay = new Anon3(ciVar);
        this.__insertionAdapterOfMFSleepSettings = new Anon4(ciVar);
        this.__insertionAdapterOfSleepRecommendedGoal = new Anon5(ciVar);
        this.__insertionAdapterOfSleepStatistic = new Anon6(ciVar);
        this.__preparedStmtOfDeleteAllSleepSessions = new Anon7(ciVar);
        this.__preparedStmtOfDeleteAllSleepDays = new Anon8(ciVar);
        this.__preparedStmtOfUpdateSleepSettings = new Anon9(ciVar);
    }

    @DexIgnore
    private void __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(n4<String, ArrayList<MFSleepSession>> n4Var) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        long j;
        Date date;
        String str;
        Integer num;
        Integer num2;
        double d;
        int i7;
        int i8;
        int i9;
        int i10;
        SleepDistribution sleepDistribution;
        int i11;
        int i12;
        Integer num3;
        int i13;
        Integer num4;
        int i14;
        Integer num5;
        int i15;
        SleepDistribution sleepDistribution2;
        int i16;
        String str2;
        int i17;
        SleepSessionHeartRate sleepSessionHeartRate;
        int i18;
        DateTime dateTime;
        int i19;
        DateTime dateTime2;
        int i20;
        int i21;
        int i22;
        n4<String, ArrayList<MFSleepSession>> n4Var2 = n4Var;
        Set<String> keySet = n4Var.keySet();
        if (!keySet.isEmpty()) {
            if (n4Var.size() > 999) {
                n4<String, ArrayList<MFSleepSession>> n4Var3 = new n4<>(999);
                int size = n4Var.size();
                int i23 = 0;
                loop0:
                while (true) {
                    i22 = 0;
                    while (i23 < size) {
                        n4Var3.put(n4Var2.c(i23), n4Var2.e(i23));
                        i23++;
                        i22++;
                        if (i22 == 999) {
                            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(n4Var3);
                            n4Var3 = new n4<>(999);
                        }
                    }
                    break loop0;
                }
                if (i22 > 0) {
                    __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(n4Var3);
                    return;
                }
                return;
            }
            StringBuilder a = si.a();
            a.append("SELECT `pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset` FROM `sleep_session` WHERE `day` IN (");
            int size2 = keySet.size();
            si.a(a, size2);
            a.append(")");
            fi b = fi.b(a.toString(), size2 + 0);
            int i24 = 1;
            for (String str3 : keySet) {
                if (str3 == null) {
                    b.bindNull(i24);
                } else {
                    b.bindString(i24, str3);
                }
                i24++;
            }
            Cursor a2 = pi.a(this.__db, b, false, null);
            try {
                int a3 = oi.a(a2, "day");
                if (a3 != -1) {
                    int a4 = oi.a(a2, "pinType");
                    int a5 = oi.a(a2, "date");
                    int a6 = oi.a(a2, "day");
                    int a7 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                    int a8 = oi.a(a2, "syncTime");
                    int a9 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                    int a10 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                    int a11 = oi.a(a2, "source");
                    int a12 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                    int a13 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                    int a14 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                    int a15 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                    int i25 = a4;
                    int a16 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                    int a17 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                    int a18 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                    int a19 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                    int a20 = oi.a(a2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                    int a21 = oi.a(a2, "heartRate");
                    int a22 = oi.a(a2, "createdAt");
                    int a23 = oi.a(a2, "updatedAt");
                    int a24 = oi.a(a2, "timezoneOffset");
                    while (a2.moveToNext()) {
                        if (!a2.isNull(a3)) {
                            int i26 = a24;
                            ArrayList<MFSleepSession> arrayList = n4Var2.get(a2.getString(a3));
                            if (arrayList != null) {
                                int i27 = -1;
                                if (a5 == -1) {
                                    j = 0;
                                } else {
                                    j = a2.getLong(a5);
                                }
                                if (a6 == -1) {
                                    i = a6;
                                    date = null;
                                } else {
                                    i = a6;
                                    date = this.__dateShortStringConverter.a(a2.getString(a6));
                                    i27 = -1;
                                }
                                if (a7 == i27) {
                                    str = null;
                                } else {
                                    str = a2.getString(a7);
                                }
                                if (a8 != i27 && !a2.isNull(a8)) {
                                    num = Integer.valueOf(a2.getInt(a8));
                                } else {
                                    num = null;
                                }
                                if (a9 != i27 && !a2.isNull(a9)) {
                                    num2 = Integer.valueOf(a2.getInt(a9));
                                } else {
                                    num2 = null;
                                }
                                if (a10 == i27) {
                                    d = 0.0d;
                                } else {
                                    d = a2.getDouble(a10);
                                }
                                if (a11 == i27) {
                                    i7 = 0;
                                } else {
                                    i7 = a2.getInt(a11);
                                }
                                if (a12 == i27) {
                                    i8 = 0;
                                } else {
                                    i8 = a2.getInt(a12);
                                }
                                if (a13 == i27) {
                                    i9 = 0;
                                } else {
                                    i9 = a2.getInt(a13);
                                }
                                if (a14 == i27) {
                                    i10 = 0;
                                } else {
                                    i10 = a2.getInt(a14);
                                }
                                if (a15 == i27) {
                                    i12 = a16;
                                    i11 = -1;
                                    sleepDistribution = null;
                                } else {
                                    sleepDistribution = this.__sleepDistributionConverter.a(a2.getString(a15));
                                    i12 = a16;
                                    i11 = -1;
                                }
                                if (i12 != i11 && !a2.isNull(i12)) {
                                    a16 = i12;
                                    num3 = Integer.valueOf(a2.getInt(i12));
                                    i13 = a17;
                                } else {
                                    a16 = i12;
                                    i13 = a17;
                                    num3 = null;
                                }
                                if (i13 != i11 && !a2.isNull(i13)) {
                                    a17 = i13;
                                    num4 = Integer.valueOf(a2.getInt(i13));
                                    i14 = a18;
                                } else {
                                    a17 = i13;
                                    i14 = a18;
                                    num4 = null;
                                }
                                if (i14 != i11 && !a2.isNull(i14)) {
                                    a18 = i14;
                                    num5 = Integer.valueOf(a2.getInt(i14));
                                    i15 = a19;
                                } else {
                                    a18 = i14;
                                    i15 = a19;
                                    num5 = null;
                                }
                                if (i15 == i11) {
                                    a19 = i15;
                                    i16 = a20;
                                    sleepDistribution2 = null;
                                } else {
                                    a19 = i15;
                                    sleepDistribution2 = this.__sleepDistributionConverter.a(a2.getString(i15));
                                    i16 = a20;
                                    i11 = -1;
                                }
                                if (i16 == i11) {
                                    a20 = i16;
                                    i17 = a21;
                                    str2 = null;
                                } else {
                                    a20 = i16;
                                    str2 = a2.getString(i16);
                                    i17 = a21;
                                }
                                if (i17 == i11) {
                                    a21 = i17;
                                    i18 = a22;
                                    sleepSessionHeartRate = null;
                                } else {
                                    a21 = i17;
                                    sleepSessionHeartRate = this.__sleepSessionHeartRateConverter.a(a2.getString(i17));
                                    i18 = a22;
                                    i11 = -1;
                                }
                                if (i18 == i11) {
                                    i5 = a3;
                                    i3 = a13;
                                    i2 = a23;
                                    i19 = -1;
                                    dateTime = null;
                                } else {
                                    i5 = a3;
                                    i3 = a13;
                                    dateTime = this.__dateTimeConverter.a(a2.getLong(i18));
                                    i2 = a23;
                                    i19 = -1;
                                }
                                if (i2 == i19) {
                                    i4 = i18;
                                    i20 = i26;
                                    dateTime2 = null;
                                } else {
                                    i4 = i18;
                                    dateTime2 = this.__dateTimeConverter.a(a2.getLong(i2));
                                    i20 = i26;
                                    i19 = -1;
                                }
                                if (i20 == i19) {
                                    i21 = 0;
                                } else {
                                    i21 = a2.getInt(i20);
                                }
                                MFSleepSession mFSleepSession = new MFSleepSession(j, date, str, num, num2, d, i7, i8, i9, i10, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i21);
                                i26 = i20;
                                i6 = i25;
                                if (i6 != -1) {
                                    mFSleepSession.setPinType(a2.getInt(i6));
                                }
                                arrayList.add(mFSleepSession);
                            } else {
                                i = a6;
                                i3 = a13;
                                i6 = i25;
                                i2 = a23;
                                i4 = a22;
                                i5 = a3;
                            }
                            n4Var2 = n4Var;
                            i25 = i6;
                            a3 = i5;
                            a22 = i4;
                            a24 = i26;
                            a13 = i3;
                        } else {
                            i = a6;
                            i2 = a23;
                            n4Var2 = n4Var;
                        }
                        a23 = i2;
                        a6 = i;
                    }
                    a2.close();
                }
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void deleteAllSleepDays() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllSleepDays.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepDays.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void deleteAllSleepSessions() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDeleteAllSleepSessions.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepSessions.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getLastSleepDay() {
        fi b = fi.b("SELECT * FROM sleep_date ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "timezoneOffset");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = oi.b(a, "createdAt");
            int b9 = oi.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public Integer getLastSleepGoal() {
        fi b = fi.b("SELECT sleepGoal FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Integer num = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                if (!a.isNull(0)) {
                    num = Integer.valueOf(a.getInt(0));
                }
            }
            return num;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getNearestSleepDayFromDate(String str) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date <= ? ORDER BY date ASC LIMIT 1", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "timezoneOffset");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = oi.b(a, "createdAt");
            int b9 = oi.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getPendingSleepSessions(long j, long j2) {
        fi fiVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer valueOf;
        SleepDao_Impl sleepDao_Impl = this;
        fi b = fi.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? AND pinType <> 0 ORDER BY editedStartTime ASC", 2);
        b.bindLong(1, j);
        b.bindLong(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sleepDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "day");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = oi.b(a, "syncTime");
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = oi.b(a, "source");
            int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i4 = b2;
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = oi.b(a, "heartRate");
                int b20 = oi.b(a, "createdAt");
                int b21 = oi.b(a, "updatedAt");
                int b22 = oi.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i6 = a.getInt(b9);
                    int i7 = a.getInt(b10);
                    int i8 = a.getInt(b11);
                    int i9 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        i3 = b14;
                        valueOf = null;
                    } else {
                        i3 = b14;
                        valueOf = Integer.valueOf(a.getInt(i2));
                        i5 = i;
                    }
                    String string2 = a.getString(b17);
                    b17 = b17;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(string2);
                    String string3 = a.getString(b18);
                    b18 = b18;
                    String string4 = a.getString(b19);
                    b19 = b19;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(string4);
                    DateTime a6 = sleepDao_Impl.__dateTimeConverter.a(a.getLong(b20));
                    long j4 = a.getLong(b21);
                    b21 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i6, i7, i8, i9, a3, num3, num4, valueOf, a4, string3, a5, a6, sleepDao_Impl.__dateTimeConverter.a(j4), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i4));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = b22;
                    i4 = i4;
                    b3 = b3;
                    b4 = b4;
                    b14 = i3;
                    b16 = i2;
                    b20 = b20;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getSleepDay(String str) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "timezoneOffset");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = oi.b(a, "createdAt");
            int b9 = oi.b(a, "updatedAt");
            if (a.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
            }
            return mFSleepDay;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<MFSleepDay> getSleepDayLiveData(String str) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, (Callable) new Anon11(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepDay> getSleepDays(String str, String str2) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "timezoneOffset");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = oi.b(a, "createdAt");
            int b9 = oi.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                MFSleepDay mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                mFSleepDay.setPinType(a.getInt(b2));
                mFSleepDay.setTimezoneOffset(a.getInt(b3));
                arrayList.add(mFSleepDay);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<List<MFSleepDay>> getSleepDaysLiveData(String str, String str2) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, (Callable) new Anon12(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<SleepRecommendedGoal> getSleepRecommendedGoalLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"sleepRecommendedGoals"}, false, (Callable) new Anon13(fi.b("SELECT * FROM sleepRecommendedGoals LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepSession getSleepSession(long j) {
        fi fiVar;
        MFSleepSession mFSleepSession;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        int i;
        Integer num5;
        int i2;
        fi b = fi.b("SELECT * FROM sleep_session WHERE realEndTime = ?", 1);
        b.bindLong(1, j);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "day");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = oi.b(a, "syncTime");
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = oi.b(a, "source");
            int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = oi.b(a, "heartRate");
                int b20 = oi.b(a, "createdAt");
                int b21 = oi.b(a, "updatedAt");
                int b22 = oi.b(a, "timezoneOffset");
                if (a.moveToFirst()) {
                    long j2 = a.getLong(b3);
                    Date a2 = this.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i3 = a.getInt(b9);
                    int i4 = a.getInt(b10);
                    int i5 = a.getInt(b11);
                    int i6 = a.getInt(b12);
                    SleepDistribution a3 = this.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                    }
                    if (a.isNull(b15)) {
                        i = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(b15));
                        i = b16;
                    }
                    if (a.isNull(i)) {
                        i2 = b17;
                        num5 = null;
                    } else {
                        num5 = Integer.valueOf(a.getInt(i));
                        i2 = b17;
                    }
                    mFSleepSession = new MFSleepSession(j2, a2, string, num, num2, d, i3, i4, i5, i6, a3, num3, num4, num5, this.__sleepDistributionConverter.a(a.getString(i2)), a.getString(b18), this.__sleepSessionHeartRateConverter.a(a.getString(b19)), this.__dateTimeConverter.a(a.getLong(b20)), this.__dateTimeConverter.a(a.getLong(b21)), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(b2));
                } else {
                    mFSleepSession = null;
                }
                a.close();
                fiVar.c();
                return mFSleepSession;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getSleepSessions(String str) {
        fi fiVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        int i4;
        SleepDao_Impl sleepDao_Impl = this;
        fi b = fi.b("SELECT * FROM sleep_session WHERE day = ?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sleepDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "day");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = oi.b(a, "syncTime");
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = oi.b(a, "source");
            int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i5 = b2;
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = oi.b(a, "heartRate");
                int b20 = oi.b(a, "createdAt");
                int b21 = oi.b(a, "updatedAt");
                int b22 = oi.b(a, "timezoneOffset");
                int i6 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i6;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i6;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i6 = i;
                        i3 = b14;
                        i4 = b17;
                        num5 = null;
                    } else {
                        i6 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i4 = b17;
                        i3 = b14;
                    }
                    b17 = i4;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i4));
                    String string2 = a.getString(b18);
                    b18 = b18;
                    String string3 = a.getString(b19);
                    b19 = b19;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(string3);
                    DateTime a6 = sleepDao_Impl.__dateTimeConverter.a(a.getLong(b20));
                    long j2 = a.getLong(b21);
                    b21 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, num5, a4, string2, a5, a6, sleepDao_Impl.__dateTimeConverter.a(j2), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i5));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = b22;
                    i5 = i5;
                    b14 = i3;
                    b16 = i2;
                    b3 = b3;
                    b20 = b20;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<List<MFSleepSession>> getSleepSessionsLiveData(long j, long j2) {
        fi b = fi.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.bindLong(1, j);
        b.bindLong(2, j2);
        return this.__db.getInvalidationTracker().a(new String[]{com.fossil.wearables.fsl.sleep.MFSleepSession.TABLE_NAME}, false, (Callable) new Anon10(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepSettings getSleepSettings() {
        fi b = fi.b("SELECT * FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        MFSleepSettings mFSleepSettings = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "sleepGoal");
            if (a.moveToFirst()) {
                MFSleepSettings mFSleepSettings2 = new MFSleepSettings(a.getInt(b3));
                mFSleepSettings2.setId(a.getInt(b2));
                mFSleepSettings = mFSleepSettings2;
            }
            return mFSleepSettings;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public SleepStatistic getSleepStatistic() {
        fi b = fi.b("SELECT * FROM sleep_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        SleepStatistic sleepStatistic = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "id");
            int b3 = oi.b(a, "uid");
            int b4 = oi.b(a, "sleepTimeBestDay");
            int b5 = oi.b(a, "sleepTimeBestStreak");
            int b6 = oi.b(a, "totalDays");
            int b7 = oi.b(a, "totalSleeps");
            int b8 = oi.b(a, "totalSleepMinutes");
            int b9 = oi.b(a, "totalSleepStateDistInMinute");
            int b10 = oi.b(a, "createdAt");
            int b11 = oi.b(a, "updatedAt");
            if (a.moveToFirst()) {
                sleepStatistic = new SleepStatistic(a.getString(b2), a.getString(b3), this.__sleepStatisticConverter.a(a.getString(b4)), this.__sleepStatisticConverter.a(a.getString(b5)), a.getInt(b6), a.getInt(b7), a.getInt(b8), this.__integerArrayConverter.a(a.getString(b9)), this.__dateTimeConverter.a(a.getLong(b10)), this.__dateTimeConverter.a(a.getLong(b11)));
            }
            return sleepStatistic;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<SleepStatistic> getSleepStatisticLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{SleepStatistic.TABLE_NAME}, false, (Callable) new Anon14(fi.b("SELECT * FROM sleep_statistic LIMIT 1", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<SleepSummary> getSleepSummariesDesc(String str, String str2) {
        fi b = fi.b("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        MFSleepDay mFSleepDay = null;
        Cursor a = pi.a(this.__db, b, true, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "timezoneOffset");
            int b4 = oi.b(a, "date");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int b6 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int b8 = oi.b(a, "createdAt");
            int b9 = oi.b(a, "updatedAt");
            n4<String, ArrayList<MFSleepSession>> n4Var = new n4<>();
            while (a.moveToNext()) {
                if (!a.isNull(b4)) {
                    String string = a.getString(b4);
                    if (n4Var.get(string) == null) {
                        n4Var.put(string, new ArrayList<>());
                    }
                }
            }
            a.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(n4Var);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                if (!a.isNull(b2) || !a.isNull(b3) || !a.isNull(b4) || !a.isNull(b5) || !a.isNull(b6) || !a.isNull(b7) || !a.isNull(b8) || !a.isNull(b9)) {
                    mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.a(a.getString(b4)), a.getInt(b5), a.getInt(b6), this.__sleepDistributionConverter.a(a.getString(b7)), this.__dateTimeConverter.a(a.getLong(b8)), this.__dateTimeConverter.a(a.getLong(b9)));
                    mFSleepDay.setPinType(a.getInt(b2));
                    mFSleepDay.setTimezoneOffset(a.getInt(b3));
                }
                ArrayList<MFSleepSession> arrayList2 = !a.isNull(b4) ? n4Var.get(a.getString(b4)) : null;
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList<>();
                }
                arrayList.add(new SleepSummary(mFSleepDay, arrayList2));
                mFSleepDay = null;
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0100 A[Catch:{ all -> 0x011f }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010d A[Catch:{ all -> 0x011f }] */
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.portfolio.platform.data.SleepSummary getSleepSummary(java.lang.String r23) {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            r2 = 1
            java.lang.String r3 = "SELECT * FROM sleep_date WHERE date == ?"
            com.fossil.fi r3 = com.fossil.fi.b(r3, r2)
            if (r0 != 0) goto L_0x0011
            r3.bindNull(r2)
            goto L_0x0014
        L_0x0011:
            r3.bindString(r2, r0)
        L_0x0014:
            com.fossil.ci r0 = r1.__db
            r0.assertNotSuspendingTransaction()
            com.fossil.ci r0 = r1.__db
            r4 = 0
            android.database.Cursor r2 = com.fossil.pi.a(r0, r3, r2, r4)
            java.lang.String r0 = "pinType"
            int r0 = com.fossil.oi.b(r2, r0)     // Catch:{ all -> 0x011f }
            java.lang.String r5 = "timezoneOffset"
            int r5 = com.fossil.oi.b(r2, r5)     // Catch:{ all -> 0x011f }
            java.lang.String r6 = "date"
            int r6 = com.fossil.oi.b(r2, r6)     // Catch:{ all -> 0x011f }
            java.lang.String r7 = "goalMinutes"
            int r7 = com.fossil.oi.b(r2, r7)     // Catch:{ all -> 0x011f }
            java.lang.String r8 = "sleepMinutes"
            int r8 = com.fossil.oi.b(r2, r8)     // Catch:{ all -> 0x011f }
            java.lang.String r9 = "sleepStateDistInMinute"
            int r9 = com.fossil.oi.b(r2, r9)     // Catch:{ all -> 0x011f }
            java.lang.String r10 = "createdAt"
            int r10 = com.fossil.oi.b(r2, r10)     // Catch:{ all -> 0x011f }
            java.lang.String r11 = "updatedAt"
            int r11 = com.fossil.oi.b(r2, r11)     // Catch:{ all -> 0x011f }
            com.fossil.n4 r12 = new com.fossil.n4     // Catch:{ all -> 0x011f }
            r12.<init>()     // Catch:{ all -> 0x011f }
        L_0x0055:
            boolean r13 = r2.moveToNext()     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x0076
            boolean r13 = r2.isNull(r6)     // Catch:{ all -> 0x011f }
            if (r13 != 0) goto L_0x0055
            java.lang.String r13 = r2.getString(r6)     // Catch:{ all -> 0x011f }
            java.lang.Object r14 = r12.get(r13)     // Catch:{ all -> 0x011f }
            java.util.ArrayList r14 = (java.util.ArrayList) r14     // Catch:{ all -> 0x011f }
            if (r14 != 0) goto L_0x0055
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ all -> 0x011f }
            r14.<init>()     // Catch:{ all -> 0x011f }
            r12.put(r13, r14)     // Catch:{ all -> 0x011f }
            goto L_0x0055
        L_0x0076:
            r13 = -1
            r2.moveToPosition(r13)     // Catch:{ all -> 0x011f }
            r1.__fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(r12)     // Catch:{ all -> 0x011f }
            boolean r13 = r2.moveToFirst()     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x0118
            boolean r13 = r2.isNull(r0)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r5)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r6)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r7)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r8)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r9)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r10)     // Catch:{ all -> 0x011f }
            if (r13 == 0) goto L_0x00b6
            boolean r13 = r2.isNull(r11)     // Catch:{ all -> 0x011f }
            if (r13 != 0) goto L_0x00b4
            goto L_0x00b6
        L_0x00b4:
            r7 = r4
            goto L_0x00fa
        L_0x00b6:
            java.lang.String r13 = r2.getString(r6)     // Catch:{ all -> 0x011f }
            com.fossil.mu4 r14 = r1.__dateShortStringConverter     // Catch:{ all -> 0x011f }
            java.util.Date r16 = r14.a(r13)     // Catch:{ all -> 0x011f }
            int r17 = r2.getInt(r7)     // Catch:{ all -> 0x011f }
            int r18 = r2.getInt(r8)     // Catch:{ all -> 0x011f }
            java.lang.String r7 = r2.getString(r9)     // Catch:{ all -> 0x011f }
            com.fossil.fv4 r8 = r1.__sleepDistributionConverter     // Catch:{ all -> 0x011f }
            com.portfolio.platform.data.model.room.sleep.SleepDistribution r19 = r8.a(r7)     // Catch:{ all -> 0x011f }
            long r7 = r2.getLong(r10)     // Catch:{ all -> 0x011f }
            com.fossil.nu4 r9 = r1.__dateTimeConverter     // Catch:{ all -> 0x011f }
            org.joda.time.DateTime r20 = r9.a(r7)     // Catch:{ all -> 0x011f }
            long r7 = r2.getLong(r11)     // Catch:{ all -> 0x011f }
            com.fossil.nu4 r9 = r1.__dateTimeConverter     // Catch:{ all -> 0x011f }
            org.joda.time.DateTime r21 = r9.a(r7)     // Catch:{ all -> 0x011f }
            com.portfolio.platform.data.model.room.sleep.MFSleepDay r7 = new com.portfolio.platform.data.model.room.sleep.MFSleepDay     // Catch:{ all -> 0x011f }
            r15 = r7
            r15.<init>(r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x011f }
            int r0 = r2.getInt(r0)     // Catch:{ all -> 0x011f }
            r7.setPinType(r0)     // Catch:{ all -> 0x011f }
            int r0 = r2.getInt(r5)     // Catch:{ all -> 0x011f }
            r7.setTimezoneOffset(r0)     // Catch:{ all -> 0x011f }
        L_0x00fa:
            boolean r0 = r2.isNull(r6)     // Catch:{ all -> 0x011f }
            if (r0 != 0) goto L_0x010b
            java.lang.String r0 = r2.getString(r6)     // Catch:{ all -> 0x011f }
            java.lang.Object r0 = r12.get(r0)     // Catch:{ all -> 0x011f }
            r4 = r0
            java.util.ArrayList r4 = (java.util.ArrayList) r4     // Catch:{ all -> 0x011f }
        L_0x010b:
            if (r4 != 0) goto L_0x0112
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x011f }
            r4.<init>()     // Catch:{ all -> 0x011f }
        L_0x0112:
            com.portfolio.platform.data.SleepSummary r0 = new com.portfolio.platform.data.SleepSummary     // Catch:{ all -> 0x011f }
            r0.<init>(r7, r4)     // Catch:{ all -> 0x011f }
            r4 = r0
        L_0x0118:
            r2.close()
            r3.c()
            return r4
        L_0x011f:
            r0 = move-exception
            r2.close()
            r3.c()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.sleep.SleepDao_Impl.getSleepSummary(java.lang.String):com.portfolio.platform.data.SleepSummary");
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public int getTotalSleep(String str, String str2) {
        fi b = fi.b("SELECT SUM(sleepMinutes) FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        if (str2 == null) {
            b.bindNull(2);
        } else {
            b.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        int i = 0;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            if (a.moveToFirst()) {
                i = a.getInt(0);
            }
            return i;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void insertSleepSettings(MFSleepSettings mFSleepSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSettings.insert(mFSleepSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void updateSleepSettings(int i) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfUpdateSleepSettings.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateSleepSettings.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepDay(MFSleepDay mFSleepDay) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(mFSleepDay);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepDays(List<MFSleepDay> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepRecommendedGoal(SleepRecommendedGoal sleepRecommendedGoal) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSleepRecommendedGoal.insert(sleepRecommendedGoal);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepSession(MFSleepSession mFSleepSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession.insert(mFSleepSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepSessionList(List<MFSleepSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public long upsertSleepStatistic(SleepStatistic sleepStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfSleepStatistic.insertAndReturnId(sleepStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getPendingSleepSessions() {
        fi fiVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        Integer num5;
        int i3;
        int i4;
        SleepDao_Impl sleepDao_Impl = this;
        fi b = fi.b("SELECT * FROM sleep_session WHERE pinType <> 0", 0);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sleepDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "day");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = oi.b(a, "syncTime");
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = oi.b(a, "source");
            int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i5 = b2;
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = oi.b(a, "heartRate");
                int b20 = oi.b(a, "createdAt");
                int b21 = oi.b(a, "updatedAt");
                int b22 = oi.b(a, "timezoneOffset");
                int i6 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j = a.getLong(b3);
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i7 = a.getInt(b9);
                    int i8 = a.getInt(b10);
                    int i9 = a.getInt(b11);
                    int i10 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i6;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i6;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i6 = i;
                        i3 = b14;
                        i4 = b17;
                        num5 = null;
                    } else {
                        i6 = i;
                        num5 = Integer.valueOf(a.getInt(i2));
                        i4 = b17;
                        i3 = b14;
                    }
                    b17 = i4;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(i4));
                    String string2 = a.getString(b18);
                    b18 = b18;
                    String string3 = a.getString(b19);
                    b19 = b19;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(string3);
                    DateTime a6 = sleepDao_Impl.__dateTimeConverter.a(a.getLong(b20));
                    long j2 = a.getLong(b21);
                    b21 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j, a2, string, num, num2, d, i7, i8, i9, i10, a3, num3, num4, num5, a4, string2, a5, a6, sleepDao_Impl.__dateTimeConverter.a(j2), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i5));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = b22;
                    i5 = i5;
                    b14 = i3;
                    b16 = i2;
                    b3 = b3;
                    b20 = b20;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getSleepSessions(long j, long j2) {
        fi fiVar;
        Integer num;
        Integer num2;
        Integer num3;
        int i;
        Integer num4;
        int i2;
        int i3;
        Integer valueOf;
        SleepDao_Impl sleepDao_Impl = this;
        fi b = fi.b("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        b.bindLong(1, j);
        b.bindLong(2, j2);
        sleepDao_Impl.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(sleepDao_Impl.__db, b, false, null);
        try {
            int b2 = oi.b(a, "pinType");
            int b3 = oi.b(a, "date");
            int b4 = oi.b(a, "day");
            int b5 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int b6 = oi.b(a, "syncTime");
            int b7 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int b8 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int b9 = oi.b(a, "source");
            int b10 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int b11 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int b12 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int b13 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int b14 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            fiVar = b;
            try {
                int b15 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int i4 = b2;
                int b16 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int b17 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int b18 = oi.b(a, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int b19 = oi.b(a, "heartRate");
                int b20 = oi.b(a, "createdAt");
                int b21 = oi.b(a, "updatedAt");
                int b22 = oi.b(a, "timezoneOffset");
                int i5 = b15;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    long j3 = a.getLong(b3);
                    Date a2 = sleepDao_Impl.__dateShortStringConverter.a(a.getString(b4));
                    String string = a.getString(b5);
                    if (a.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b6));
                    }
                    if (a.isNull(b7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a.getInt(b7));
                    }
                    double d = a.getDouble(b8);
                    int i6 = a.getInt(b9);
                    int i7 = a.getInt(b10);
                    int i8 = a.getInt(b11);
                    int i9 = a.getInt(b12);
                    SleepDistribution a3 = sleepDao_Impl.__sleepDistributionConverter.a(a.getString(b13));
                    if (a.isNull(b14)) {
                        i = i5;
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a.getInt(b14));
                        i = i5;
                    }
                    if (a.isNull(i)) {
                        i2 = b16;
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a.getInt(i));
                        i2 = b16;
                    }
                    if (a.isNull(i2)) {
                        i5 = i;
                        i3 = b14;
                        valueOf = null;
                    } else {
                        i3 = b14;
                        valueOf = Integer.valueOf(a.getInt(i2));
                        i5 = i;
                    }
                    String string2 = a.getString(b17);
                    b17 = b17;
                    SleepDistribution a4 = sleepDao_Impl.__sleepDistributionConverter.a(string2);
                    String string3 = a.getString(b18);
                    b18 = b18;
                    String string4 = a.getString(b19);
                    b19 = b19;
                    SleepSessionHeartRate a5 = sleepDao_Impl.__sleepSessionHeartRateConverter.a(string4);
                    DateTime a6 = sleepDao_Impl.__dateTimeConverter.a(a.getLong(b20));
                    long j4 = a.getLong(b21);
                    b21 = b21;
                    MFSleepSession mFSleepSession = new MFSleepSession(j3, a2, string, num, num2, d, i6, i7, i8, i9, a3, num3, num4, valueOf, a4, string3, a5, a6, sleepDao_Impl.__dateTimeConverter.a(j4), a.getInt(b22));
                    mFSleepSession.setPinType(a.getInt(i4));
                    arrayList.add(mFSleepSession);
                    sleepDao_Impl = this;
                    b22 = b22;
                    i4 = i4;
                    b3 = b3;
                    b4 = b4;
                    b14 = i3;
                    b16 = i2;
                    b20 = b20;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }
}
