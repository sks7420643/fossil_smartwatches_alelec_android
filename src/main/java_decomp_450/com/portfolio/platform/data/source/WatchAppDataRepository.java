package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.local.diana.WatchAppDataDao;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WatchAppDataRepository";
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ WatchAppDataDao mLocalSource;
    @DexIgnore
    public /* final */ WatchAppDataRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public WatchAppDataRepository(WatchAppDataDao watchAppDataDao, WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, FileRepository fileRepository) {
        ee7.b(watchAppDataDao, "mLocalSource");
        ee7.b(watchAppDataRemoteDataSource, "mRemoteSource");
        ee7.b(fileRepository, "mFileRepository");
        this.mLocalSource = watchAppDataDao;
        this.mRemoteSource = watchAppDataRemoteDataSource;
        this.mFileRepository = fileRepository;
    }

    @DexIgnore
    public final Object cleanUp(fb7<? super i97> fb7) {
        this.mFileRepository.deletedFilesByType(FileType.WATCH_APP);
        this.mLocalSource.clearAll();
        return i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0248  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x034b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadWatchAppData(java.lang.String r27, java.lang.String r28, com.fossil.fb7<? super java.lang.Boolean> r29) {
        /*
            r26 = this;
            r0 = r26
            r1 = r27
            r2 = r28
            r3 = r29
            boolean r4 = r3 instanceof com.portfolio.platform.data.source.WatchAppDataRepository$downloadWatchAppData$Anon1
            if (r4 == 0) goto L_0x001b
            r4 = r3
            com.portfolio.platform.data.source.WatchAppDataRepository$downloadWatchAppData$Anon1 r4 = (com.portfolio.platform.data.source.WatchAppDataRepository$downloadWatchAppData$Anon1) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.portfolio.platform.data.source.WatchAppDataRepository$downloadWatchAppData$Anon1 r4 = new com.portfolio.platform.data.source.WatchAppDataRepository$downloadWatchAppData$Anon1
            r4.<init>(r0, r3)
        L_0x0020:
            java.lang.Object r3 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            java.lang.String r8 = " url "
            r9 = 2
            r11 = 1
            java.lang.String r12 = "WatchAppDataRepository"
            if (r6 == 0) goto L_0x0094
            if (r6 == r11) goto L_0x007f
            if (r6 != r9) goto L_0x0077
            java.lang.Object r1 = r4.L$12
            com.portfolio.platform.data.model.diana.WatchAppData r1 = (com.portfolio.platform.data.model.diana.WatchAppData) r1
            java.lang.Object r2 = r4.L$11
            java.lang.Object r2 = r4.L$10
            java.util.Iterator r2 = (java.util.Iterator) r2
            java.lang.Object r6 = r4.L$9
            java.util.List r6 = (java.util.List) r6
            java.lang.Object r13 = r4.L$8
            java.lang.Iterable r13 = (java.lang.Iterable) r13
            java.lang.Object r14 = r4.L$7
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r15 = r4.L$6
            java.util.List r15 = (java.util.List) r15
            java.lang.Object r7 = r4.L$5
            java.util.List r7 = (java.util.List) r7
            java.lang.Object r9 = r4.L$4
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r4.L$3
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            java.lang.Object r11 = r4.L$2
            java.lang.String r11 = (java.lang.String) r11
            r27 = r1
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            r28 = r1
            java.lang.Object r1 = r4.L$0
            com.portfolio.platform.data.source.WatchAppDataRepository r1 = (com.portfolio.platform.data.source.WatchAppDataRepository) r1
            com.fossil.t87.a(r3)
            r0 = r1
            r16 = r15
            r1 = r27
            r15 = r2
            r2 = r28
            goto L_0x01d9
        L_0x0077:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x007f:
            java.lang.Object r1 = r4.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r6 = r4.L$0
            com.portfolio.platform.data.source.WatchAppDataRepository r6 = (com.portfolio.platform.data.source.WatchAppDataRepository) r6
            com.fossil.t87.a(r3)
            r25 = r2
            r2 = r1
            r1 = r25
            goto L_0x00cc
        L_0x0094:
            com.fossil.t87.a(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "downloadWatchAppData() - appVersion = "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r7 = ", firmwareVersion = "
            r6.append(r7)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            r3.d(r12, r6)
            com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource r3 = r0.mRemoteSource
            r4.L$0 = r0
            r4.L$1 = r1
            r4.L$2 = r2
            r6 = 1
            r4.label = r6
            java.lang.Object r3 = r3.getAllWatchAppData(r1, r2, r4)
            if (r3 != r5) goto L_0x00cb
            return r5
        L_0x00cb:
            r6 = r0
        L_0x00cc:
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            boolean r7 = r3 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x034b
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "downloadWatchAppData() - Success, isFromCache "
            r9.append(r10)
            r10 = r3
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            boolean r11 = r10.b()
            r9.append(r11)
            java.lang.String r11 = " response "
            r9.append(r11)
            java.lang.Object r11 = r10.a()
            java.util.List r11 = (java.util.List) r11
            r9.append(r11)
            java.lang.String r9 = r9.toString()
            r7.d(r12, r9)
            com.portfolio.platform.data.source.local.diana.WatchAppDataDao r7 = r6.mLocalSource
            java.util.List r7 = r7.getAllWatchAppData()
            java.lang.Object r9 = r10.a()
            java.util.List r9 = (java.util.List) r9
            if (r9 == 0) goto L_0x0383
            boolean r10 = r10.b()
            if (r10 == 0) goto L_0x011b
            r10 = 0
            java.lang.Boolean r1 = com.fossil.pb7.a(r10)
            return r1
        L_0x011b:
            java.util.List r10 = com.fossil.ea7.b(r9, r7)
            java.util.List r11 = com.fossil.ea7.b(r7, r9)
            boolean r13 = r10.isEmpty()
            if (r13 == 0) goto L_0x013c
            boolean r13 = r11.isEmpty()
            if (r13 == 0) goto L_0x013c
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "downloadWatchAppData() - local and remote list are the same"
            r1.d(r12, r2)
            goto L_0x0383
        L_0x013c:
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.Iterator r14 = r10.iterator()
            r15 = r10
            r10 = r3
            r3 = r11
            r11 = r2
            r2 = r1
            r1 = r6
            r6 = r13
            r13 = r15
            r25 = r9
            r9 = r7
            r7 = r25
        L_0x0152:
            boolean r16 = r14.hasNext()
            if (r16 == 0) goto L_0x023e
            java.lang.Object r0 = r14.next()
            r27 = r5
            r5 = r0
            com.portfolio.platform.data.model.diana.WatchAppData r5 = (com.portfolio.platform.data.model.diana.WatchAppData) r5
            com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            r28 = r0
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r16.getLocal()
            r23 = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r24 = r6
            java.lang.String r6 = "start download watch-app type "
            r14.append(r6)
            java.lang.String r6 = r5.getType()
            r14.append(r6)
            r14.append(r8)
            java.lang.String r6 = r5.getExecutableBinaryDataUrl()
            r14.append(r6)
            java.lang.String r6 = r14.toString()
            r0.d(r12, r6)
            com.portfolio.platform.data.source.FileRepository r0 = r1.mFileRepository
            java.lang.String r17 = r5.getExecutableBinaryDataUrl()
            com.misfit.frameworks.buttonservice.model.FileType r18 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_APP
            r19 = 0
            r21 = 4
            r22 = 0
            r4.L$0 = r1
            r4.L$1 = r2
            r4.L$2 = r11
            r4.L$3 = r10
            r4.L$4 = r9
            r4.L$5 = r7
            r4.L$6 = r15
            r4.L$7 = r3
            r4.L$8 = r13
            r6 = r24
            r4.L$9 = r6
            r14 = r23
            r4.L$10 = r14
            r23 = r2
            r2 = r28
            r4.L$11 = r2
            r4.L$12 = r5
            r2 = 2
            r4.label = r2
            r16 = r0
            r20 = r4
            java.lang.Object r0 = com.portfolio.platform.data.source.FileRepository.downloadFromUrl$default(r16, r17, r18, r19, r20, r21, r22)
            r2 = r27
            if (r0 != r2) goto L_0x01cf
            return r2
        L_0x01cf:
            r16 = r15
            r15 = r14
            r14 = r3
            r3 = r0
            r0 = r1
            r1 = r5
            r5 = r2
            r2 = r23
        L_0x01d9:
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            com.misfit.frameworks.buttonservice.log.FLogger r17 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            r27 = r2
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r17.getLocal()
            r28 = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r17 = r5
            java.lang.String r5 = "done download watch-app version "
            r4.append(r5)
            java.lang.String r5 = r1.getVersion()
            r4.append(r5)
            java.lang.String r5 = " type "
            r4.append(r5)
            java.lang.String r5 = r1.getType()
            r4.append(r5)
            r4.append(r8)
            java.lang.String r5 = r1.getExecutableBinaryDataUrl()
            r4.append(r5)
            java.lang.String r5 = " isSuccess "
            r4.append(r5)
            r4.append(r3)
            java.lang.String r4 = r4.toString()
            r2.d(r12, r4)
            if (r3 == 0) goto L_0x022f
            java.lang.String r2 = r1.getType()
            r6.add(r2)
            com.portfolio.platform.data.source.local.diana.WatchAppDataDao r2 = r0.mLocalSource
            r2.upsertWatchAppData(r1)
        L_0x022f:
            r2 = r27
            r4 = r28
            r1 = r0
            r3 = r14
            r14 = r15
            r15 = r16
            r5 = r17
            r0 = r26
            goto L_0x0152
        L_0x023e:
            java.util.Iterator r0 = r3.iterator()
        L_0x0242:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x033a
            java.lang.Object r2 = r0.next()
            com.portfolio.platform.data.model.diana.WatchAppData r2 = (com.portfolio.platform.data.model.diana.WatchAppData) r2
            java.util.Iterator r3 = r15.iterator()
        L_0x0252:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0276
            java.lang.Object r4 = r3.next()
            r5 = r4
            com.portfolio.platform.data.model.diana.WatchAppData r5 = (com.portfolio.platform.data.model.diana.WatchAppData) r5
            java.lang.String r7 = r2.getType()
            java.lang.String r5 = r5.getType()
            boolean r5 = com.fossil.ee7.a(r7, r5)
            java.lang.Boolean r5 = com.fossil.pb7.a(r5)
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x0252
            goto L_0x0277
        L_0x0276:
            r4 = 0
        L_0x0277:
            if (r4 == 0) goto L_0x027b
            r3 = 1
            goto L_0x027c
        L_0x027b:
            r3 = 0
        L_0x027c:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "downloadWatchAppData() - delete type "
            r5.append(r7)
            java.lang.String r7 = r2.getType()
            r5.append(r7)
            java.lang.String r7 = " isDeleteItemInInsertList "
            r5.append(r7)
            r5.append(r3)
            java.lang.String r5 = r5.toString()
            r4.d(r12, r5)
            if (r3 == 0) goto L_0x02d6
            if (r3 == 0) goto L_0x02b1
            java.lang.String r3 = r2.getType()
            boolean r3 = r6.contains(r3)
            if (r3 == 0) goto L_0x02b1
            goto L_0x02d6
        L_0x02b1:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "this latest version of this "
            r4.append(r5)
            java.lang.String r2 = r2.getType()
            r4.append(r2)
            java.lang.String r2 = " is not downloaded success yet, do no delete"
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r3.d(r12, r2)
            goto L_0x0242
        L_0x02d6:
            com.portfolio.platform.data.source.FileRepository r3 = r1.mFileRepository
            java.lang.String r4 = r2.getExecutableBinaryDataUrl()
            java.io.File r3 = r3.getFileByRemoteUrl(r4)
            if (r3 == 0) goto L_0x0315
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "downloadWatchAppData() - delete file "
            r5.append(r7)
            java.lang.String r7 = r3.getAbsolutePath()
            r5.append(r7)
            java.lang.String r7 = " success"
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            r4.d(r12, r5)
            com.portfolio.platform.data.source.FileRepository r4 = r1.mFileRepository
            java.lang.String r3 = r3.getName()
            java.lang.String r5 = "it.name"
            com.fossil.ee7.a(r3, r5)
            com.misfit.frameworks.buttonservice.model.FileType r5 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_APP
            r4.deleteFileByName(r3, r5)
        L_0x0315:
            com.portfolio.platform.data.source.local.diana.WatchAppDataDao r3 = r1.mLocalSource
            java.lang.String r4 = r2.getId()
            r3.deleteWatchAppDataById(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "downloadWatchAppData() - delete from DB success, watchApp = "
            r4.append(r5)
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r3.d(r12, r2)
            goto L_0x0242
        L_0x033a:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "downloadWatchAppData() - insert to DB success"
            r0.d(r12, r1)
            r0 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        L_0x034b:
            boolean r0 = r3 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0383
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadWatchAppData() - Fail!!! error="
            r1.append(r2)
            com.fossil.yi5 r3 = (com.fossil.yi5) r3
            int r2 = r3.a()
            r1.append(r2)
            java.lang.String r2 = " serverErrorCode="
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            if (r2 == 0) goto L_0x0378
            java.lang.Integer r7 = r2.getCode()
            goto L_0x0379
        L_0x0378:
            r7 = 0
        L_0x0379:
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            r0.d(r12, r1)
        L_0x0383:
            r0 = 0
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchAppDataRepository.downloadWatchAppData(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
