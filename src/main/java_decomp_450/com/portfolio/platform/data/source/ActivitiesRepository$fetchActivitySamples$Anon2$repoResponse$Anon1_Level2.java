package com.portfolio.platform.data.source;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$2$repoResponse$1", f = "ActivitiesRepository.kt", l = {146}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2 extends zb7 implements gd7<fb7<? super fv7<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$fetchActivitySamples$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2(ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2, fb7 fb7) {
        super(1, fb7);
        this.this$0 = activitiesRepository$fetchActivitySamples$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(fb7<?> fb7) {
        ee7.b(fb7, "completion");
        return new ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2(this.this$0, fb7);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public final Object invoke(fb7<? super fv7<ApiResponse<Activity>>> fb7) {
        return ((ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2) create(fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            String g = zd5.g(this.this$0.$start);
            ee7.a((Object) g, "DateHelper.formatShortDate(start)");
            String g2 = zd5.g(this.this$0.$end);
            ee7.a((Object) g2, "DateHelper.formatShortDate(end)");
            ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2 = this.this$0;
            int i2 = activitiesRepository$fetchActivitySamples$Anon2.$offset;
            int i3 = activitiesRepository$fetchActivitySamples$Anon2.$limit;
            this.label = 1;
            obj = access$getMApiService$p.getActivities(g, g2, i2, i3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
