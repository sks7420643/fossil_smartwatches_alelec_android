package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fv7;
import com.fossil.ge;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lx6;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qe7;
import com.fossil.qx6;
import com.fossil.r87;
import com.fossil.t3;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {352}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataList$Anon2 extends zb7 implements kd7<yi7, fb7<? super LiveData<qx6<? extends List<GoalTrackingData>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getGoalTrackingDataList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends lx6<List<GoalTrackingData>, ApiResponse<GoalEvent>> {
            @DexIgnore
            public /* final */ /* synthetic */ r87 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, qe7 qe7, int i, r87 r87) {
                this.this$0 = anon1_Level2;
                this.$offset = qe7;
                this.$limit = i;
                this.$downloadingDate = r87;
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public Object createCall(fb7<? super fv7<ApiResponse<GoalEvent>>> fb7) {
                Date date;
                Date date2;
                ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.this$0.mApiServiceV2;
                r87 r87 = this.$downloadingDate;
                if (r87 == null || (date = (Date) r87.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String g = zd5.g(date);
                ee7.a((Object) g, "DateHelper.formatShortDa\u2026            ?: startDate)");
                r87 r872 = this.$downloadingDate;
                if (r872 == null || (date2 = (Date) r872.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String g2 = zd5.g(date2);
                ee7.a((Object) g2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return access$getMApiServiceV2$p.getGoalTrackingDataList(g, g2, this.$offset.element, this.$limit, fb7);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
            @Override // com.fossil.lx6
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.fossil.fb7<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r5) {
                /*
                    r4 = this;
                    boolean r0 = r5 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r5
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r4, r5)
                L_0x0018:
                    java.lang.Object r5 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    r3 = 1
                    if (r2 == 0) goto L_0x0035
                    if (r2 != r3) goto L_0x002d
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r5)
                    goto L_0x0046
                L_0x002d:
                    java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r5.<init>(r0)
                    throw r5
                L_0x0035:
                    com.fossil.t87.a(r5)
                    com.fossil.pg5 r5 = com.fossil.pg5.i
                    r0.L$0 = r4
                    r0.label = r3
                    java.lang.Object r5 = r5.c(r0)
                    if (r5 != r1) goto L_0x0045
                    return r1
                L_0x0045:
                    r0 = r4
                L_0x0046:
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r5 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r5
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r5 = r5.getGoalTrackingDao()
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r0 = r0.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r0 = r0.this$0
                    java.util.Date r1 = r0.$startDate
                    java.util.Date r0 = r0.$endDate
                    androidx.lifecycle.LiveData r5 = r5.getGoalTrackingDataListLiveData(r1, r0)
                    return r5
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            @Override // com.fossil.lx6
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList onFetchFailed");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, fb7 fb7) {
                return processContinueFetching(apiResponse, (fb7<? super Boolean>) fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.fb7] */
            @Override // com.fossil.lx6
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<GoalEvent> apiResponse, fb7 fb7) {
                return saveCallResult(apiResponse, (fb7<? super i97>) fb7);
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, fb7<? super Boolean> fb7) {
                Boolean a;
                Range range = apiResponse.get_range();
                if (range == null || (a = pb7.a(range.isHasNext())) == null || !a.booleanValue()) {
                    return pb7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return pb7.a(true);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalEvent> r9, com.fossil.fb7<? super com.fossil.i97> r10) {
                /*
                    r8 = this;
                    boolean r0 = r10 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x0013
                    r0 = r10
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = -2147483648(0xffffffff80000000, float:-0.0)
                    r3 = r1 & r2
                    if (r3 == 0) goto L_0x0013
                    int r1 = r1 - r2
                    r0.label = r1
                    goto L_0x0018
                L_0x0013:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r8, r10)
                L_0x0018:
                    java.lang.Object r10 = r0.result
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    java.lang.String r3 = ", endDate="
                    java.lang.String r4 = "getGoalTrackingDataList startDate="
                    r5 = 1
                    if (r2 == 0) goto L_0x0045
                    if (r2 != r5) goto L_0x003d
                    java.lang.Object r9 = r0.L$2
                    java.util.List r9 = (java.util.List) r9
                    java.lang.Object r1 = r0.L$1
                    com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.t87.a(r10)     // Catch:{ Exception -> 0x003a }
                    goto L_0x00c1
                L_0x003a:
                    r9 = move-exception
                    goto L_0x00d1
                L_0x003d:
                    java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                    java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
                    r9.<init>(r10)
                    throw r9
                L_0x0045:
                    com.fossil.t87.a(r10)
                    com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r2 = r2.getTAG()
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder
                    r6.<init>()
                    r6.append(r4)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r7 = r8.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r7 = r7.this$0
                    java.util.Date r7 = r7.$startDate
                    r6.append(r7)
                    r6.append(r3)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r7 = r8.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r7 = r7.this$0
                    java.util.Date r7 = r7.$endDate
                    r6.append(r7)
                    java.lang.String r7 = " saveCallResult onResponse: response = "
                    r6.append(r7)
                    r6.append(r9)
                    java.lang.String r6 = r6.toString()
                    r10.d(r2, r6)
                    java.util.List r10 = r9.get_items()     // Catch:{ Exception -> 0x00cf }
                    java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x00cf }
                    r6 = 10
                    int r6 = com.fossil.x97.a(r10, r6)     // Catch:{ Exception -> 0x00cf }
                    r2.<init>(r6)     // Catch:{ Exception -> 0x00cf }
                    java.util.Iterator r10 = r10.iterator()     // Catch:{ Exception -> 0x00cf }
                L_0x0093:
                    boolean r6 = r10.hasNext()     // Catch:{ Exception -> 0x00cf }
                    if (r6 == 0) goto L_0x00ae
                    java.lang.Object r6 = r10.next()     // Catch:{ Exception -> 0x00cf }
                    com.portfolio.platform.data.model.goaltracking.response.GoalEvent r6 = (com.portfolio.platform.data.model.goaltracking.response.GoalEvent) r6     // Catch:{ Exception -> 0x00cf }
                    com.portfolio.platform.data.model.goaltracking.GoalTrackingData r6 = r6.toGoalTrackingData()     // Catch:{ Exception -> 0x00cf }
                    if (r6 == 0) goto L_0x00a9
                    r2.add(r6)     // Catch:{ Exception -> 0x00cf }
                    goto L_0x0093
                L_0x00a9:
                    com.fossil.ee7.a()     // Catch:{ Exception -> 0x00cf }
                    r9 = 0
                    throw r9
                L_0x00ae:
                    com.fossil.pg5 r10 = com.fossil.pg5.i
                    r0.L$0 = r8
                    r0.L$1 = r9
                    r0.L$2 = r2
                    r0.label = r5
                    java.lang.Object r10 = r10.c(r0)
                    if (r10 != r1) goto L_0x00bf
                    return r1
                L_0x00bf:
                    r0 = r8
                    r9 = r2
                L_0x00c1:
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r10 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r10
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r10 = r10.getGoalTrackingDao()
                    java.util.List r9 = com.fossil.ea7.d(r9)
                    r10.upsertGoalTrackingDataList(r9)
                    goto L_0x010c
                L_0x00cf:
                    r9 = move-exception
                    r0 = r8
                L_0x00d1:
                    com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r1 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r1 = r1.getTAG()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r2.append(r4)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r4 = r0.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r4 = r4.this$0
                    java.util.Date r4 = r4.$startDate
                    r2.append(r4)
                    r2.append(r3)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r0 = r0.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r0 = r0.this$0
                    java.util.Date r0 = r0.$endDate
                    r2.append(r0)
                    java.lang.String r0 = " exception="
                    r2.append(r0)
                    r2.append(r9)
                    java.lang.String r0 = r2.toString()
                    r10.e(r1, r0)
                    r9.printStackTrace()
                L_0x010c:
                    com.fossil.i97 r9 = com.fossil.i97.a
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse, com.fossil.fb7):java.lang.Object");
            }

            @DexIgnore
            public boolean shouldFetch(List<GoalTrackingData> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2) {
            this.this$0 = goalTrackingRepository$getGoalTrackingDataList$Anon2;
        }

        @DexIgnore
        public final LiveData<qx6<List<GoalTrackingData>>> apply(List<GoalTrackingData> list) {
            ee7.a((Object) list, "pendingList");
            GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = this.this$0;
            r87<Date, Date> calculateRangeDownload = GoalTrackingDataKt.calculateRangeDownload(list, goalTrackingRepository$getGoalTrackingDataList$Anon2.$startDate, goalTrackingRepository$getGoalTrackingDataList$Anon2.$endDate);
            qe7 qe7 = new qe7();
            qe7.element = 0;
            return new Anon1_Level3(this, qe7, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, calculateRangeDownload).asLiveData();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = new GoalTrackingRepository$getGoalTrackingDataList$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, fb7);
        goalTrackingRepository$getGoalTrackingDataList$Anon2.p$ = (yi7) obj;
        return goalTrackingRepository$getGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LiveData<qx6<? extends List<GoalTrackingData>>>> fb7) {
        return ((GoalTrackingRepository$getGoalTrackingDataList$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getGoalTrackingDataList startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            GoalTrackingRepository goalTrackingRepository = this.this$0;
            Date date = this.$startDate;
            Date date2 = this.$endDate;
            this.L$0 = yi7;
            this.label = 1;
            obj = goalTrackingRepository.getPendingGoalTrackingDataListLiveData(date, date2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ge.b((LiveData) obj, new Anon1_Level2(this));
    }
}
