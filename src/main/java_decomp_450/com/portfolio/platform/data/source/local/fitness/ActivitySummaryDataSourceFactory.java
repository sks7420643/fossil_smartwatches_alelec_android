package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ee7;
import com.fossil.ge5;
import com.fossil.lf;
import com.fossil.pj4;
import com.fossil.te5;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDataSourceFactory extends lf.b<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ pj4 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ ge5 fitnessHelper;
    @DexIgnore
    public /* final */ te5.a listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, ge5 ge5, FitnessDataRepository fitnessDataRepository2, FitnessDatabase fitnessDatabase2, Date date, pj4 pj4, te5.a aVar, Calendar calendar) {
        ee7.b(summariesRepository2, "summariesRepository");
        ee7.b(ge5, "fitnessHelper");
        ee7.b(fitnessDataRepository2, "fitnessDataRepository");
        ee7.b(fitnessDatabase2, "fitnessDatabase");
        ee7.b(date, "createdDate");
        ee7.b(pj4, "appExecutors");
        ee7.b(aVar, "listener");
        ee7.b(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = ge5;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = pj4;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.fossil.lf.b
    public lf<Date, ActivitySummary> create() {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.localDataSource = activitySummaryLocalDataSource;
        this.sourceLiveData.a(activitySummaryLocalDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.localDataSource;
        if (activitySummaryLocalDataSource2 != null) {
            return activitySummaryLocalDataSource2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
