package com.portfolio.platform.data.source.local;

import android.util.SparseArray;
import com.fossil.ah5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wh5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationLocalDataSource implements NotificationsDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "NotificationLocalDataSource";

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllNotificationSetting() {
        ah5.p.a().b().removeAllContactGroups();
        ah5.p.a().a().removeAllAppFilters();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllPhoneFavoritesContacts() {
        ah5.p.a().h().d();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return ah5.p.a().a().getAllAppFiltersWithHour(i, i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return ah5.p.a().a().getAllAppFilterVibration(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilters(int i) {
        return ah5.p.a().a().getAllAppFilters(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getAllContactGroups(int i) {
        return ah5.p.a().b().getAllContactGroups(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        SparseArray<List<BaseFeatureModel>> sparseArray = new SparseArray<>();
        ArrayList<BaseFeatureModel> arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(i);
        List<AppFilter> allAppFilters = ah5.p.a().a().getAllAppFilters(i);
        if (allContactGroups != null && !allContactGroups.isEmpty()) {
            arrayList.addAll(allContactGroups);
        }
        if (allAppFilters != null && !allAppFilters.isEmpty()) {
            arrayList.addAll(allAppFilters);
        }
        for (BaseFeatureModel baseFeatureModel : arrayList) {
            List<BaseFeatureModel> list = sparseArray.get(baseFeatureModel.getHour());
            if (list == null || list.isEmpty()) {
                list = new ArrayList<>();
            }
            list.add(baseFeatureModel);
            sparseArray.put(baseFeatureModel.getHour(), list);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Inside .getAllNotificationsByHour inLocal, result=" + sparseArray);
        return sparseArray;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public AppFilter getAppFilterByType(String str, int i) {
        return ah5.p.a().a().getAppFilterMatchingType(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public Contact getContactById(int i) {
        return ((wh5) ah5.p.a().b()).a(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getContactGroupId(List<Integer> list) {
        return ((wh5) ah5.p.a().b()).b(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return ah5.p.a().b().getContactGroupsMatchingEmail(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return ah5.p.a().b().getContactGroupsMatchingIncomingCall(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return ah5.p.a().b().getContactGroupsMatchingSms(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getLocalContactId() {
        return ((wh5) ah5.p.a().b()).f();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAllAppFilters() {
        ah5.p.a().a().removeAllAppFilters();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAppFilter(AppFilter appFilter) {
        ah5.p.a().a().removeAppFilter(appFilter);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContact(Contact contact) {
        ah5.p.a().b().removeContact(contact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroup(ContactGroup contactGroup) {
        ah5.p.a().b().removeContactGroup(contactGroup);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroupList(List<ContactGroup> list) {
        ContactProvider b = ah5.p.a().b();
        for (ContactGroup contactGroup : list) {
            b.removeContactGroup(contactGroup);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListAppFilter(List<AppFilter> list) {
        AppFilterProvider a = ah5.p.a().a();
        for (AppFilter appFilter : list) {
            a.removeAppFilter(appFilter);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListContact(List<Contact> list) {
        ContactProvider b = ah5.p.a().b();
        for (Contact contact : list) {
            b.removeContact(contact);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContact(List<Integer> list) {
        ((wh5) ah5.p.a().b()).c(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContactGroup(List<Integer> list) {
        ((wh5) ah5.p.a().b()).e(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        ah5.p.a().h().removePhoneFavoritesContact(phoneFavoritesContact.getPhoneNumber());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumber(List<Integer> list) {
        ((wh5) ah5.p.a().b()).d(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumberByContactGroupId(int i) {
        ((wh5) ah5.p.a().b()).b(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeRedundantContact() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveAppFilter(AppFilter appFilter) {
        ah5.p.a().a().saveAppFilter(appFilter);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContact(Contact contact) {
        ah5.p.a().b().saveContact(contact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroup(ContactGroup contactGroup) {
        ah5.p.a().b().saveContactGroup(contactGroup);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroupList(List<ContactGroup> list) {
        ContactProvider b = ah5.p.a().b();
        for (ContactGroup contactGroup : list) {
            b.saveContactGroup(contactGroup);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveEmailAddress(EmailAddress emailAddress) {
        ah5.p.a().b().saveEmailAddress(emailAddress);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListAppFilters(List<AppFilter> list) {
        AppFilterProvider a = ah5.p.a().a();
        for (AppFilter appFilter : list) {
            a.saveAppFilter(appFilter);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListContact(List<Contact> list) {
        ContactProvider b = ah5.p.a().b();
        for (Contact contact : list) {
            b.saveContact(contact);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListPhoneNumber(List<PhoneNumber> list) {
        ContactProvider b = ah5.p.a().b();
        for (PhoneNumber phoneNumber : list) {
            b.savePhoneNumber(phoneNumber);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        ah5.p.a().h().a(phoneFavoritesContact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        ah5.p.a().b().savePhoneNumber(phoneNumber);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(String str) {
        ah5.p.a().h().removePhoneFavoritesContact(str);
    }
}
