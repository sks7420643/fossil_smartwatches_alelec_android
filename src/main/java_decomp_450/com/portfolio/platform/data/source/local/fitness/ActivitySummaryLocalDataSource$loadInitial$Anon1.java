package com.portfolio.platform.data.source.local.fitness;

import com.fossil.ee7;
import com.fossil.te5;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadInitial$Anon1 implements te5.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.fossil.te5.b
    public final void run(te5.b.a aVar) {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$0;
        te5.d dVar = te5.d.INITIAL;
        Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        ee7.a((Object) aVar, "helperCallback");
        activitySummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
