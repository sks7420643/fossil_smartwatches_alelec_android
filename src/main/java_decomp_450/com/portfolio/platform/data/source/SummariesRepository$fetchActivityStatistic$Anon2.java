package com.portfolio.platform.data.source;

import com.fossil.aj5;
import com.fossil.bj5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pg5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi5;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.source.SummariesRepository$fetchActivityStatistic$2", f = "SummariesRepository.kt", l = {439, 442}, m = "invokeSuspend")
public final class SummariesRepository$fetchActivityStatistic$Anon2 extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$fetchActivityStatistic$Anon2(SummariesRepository summariesRepository, fb7 fb7) {
        super(2, fb7);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        SummariesRepository$fetchActivityStatistic$Anon2 summariesRepository$fetchActivityStatistic$Anon2 = new SummariesRepository$fetchActivityStatistic$Anon2(this.this$0, fb7);
        summariesRepository$fetchActivityStatistic$Anon2.p$ = (yi7) obj;
        return summariesRepository$fetchActivityStatistic$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
        return ((SummariesRepository$fetchActivityStatistic$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        zi5 zi5;
        yi7 yi7;
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 = this.p$;
            SummariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2 summariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2 = new SummariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = yi7;
            this.label = 1;
            obj = aj5.a(summariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 = (yi7) this.L$0;
            t87.a(obj);
        } else if (i == 2) {
            zi5 = (zi5) this.L$1;
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
            bj5 bj5 = (bj5) zi5;
            ((FitnessDatabase) obj).activitySummaryDao().upsertActivityStatistic((ActivityStatistic) bj5.a());
            return bj5.a();
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zi5 zi52 = (zi5) obj;
        if (zi52 instanceof bj5) {
            if (((bj5) zi52).a() != null) {
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.L$1 = zi52;
                this.label = 2;
                Object b = pg5.b(this);
                if (b == a) {
                    return a;
                }
                zi5 = zi52;
                obj = b;
                bj5 bj52 = (bj5) zi5;
                ((FitnessDatabase) obj).activitySummaryDao().upsertActivityStatistic((ActivityStatistic) bj52.a());
                return bj52.a();
            }
        } else if (zi52 instanceof yi5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getActivityStatisticAwait - Failure -- code=");
            yi5 yi5 = (yi5) zi52;
            sb.append(yi5.a());
            sb.append(", message=");
            ServerError c = yi5.c();
            sb.append(c != null ? c.getMessage() : null);
            local.e(SummariesRepository.TAG, sb.toString());
        }
        return null;
    }
}
