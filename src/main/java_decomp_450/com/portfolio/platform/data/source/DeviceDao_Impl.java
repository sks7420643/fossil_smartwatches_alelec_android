package com.portfolio.platform.data.source;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Device;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDao_Impl implements DeviceDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<Device> __insertionAdapterOfDevice;
    @DexIgnore
    public /* final */ ji __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ ji __preparedStmtOfRemoveDeviceByDeviceId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<Device> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `device` (`major`,`minor`,`createdAt`,`updatedAt`,`owner`,`productDisplayName`,`manufacturer`,`softwareRevision`,`hardwareRevision`,`deviceId`,`macAddress`,`sku`,`firmwareRevision`,`batteryLevel`,`vibrationStrength`,`isActive`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, Device device) {
            ajVar.bindLong(1, (long) device.getMajor());
            ajVar.bindLong(2, (long) device.getMinor());
            if (device.getCreatedAt() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, device.getCreatedAt());
            }
            if (device.getUpdatedAt() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, device.getUpdatedAt());
            }
            if (device.getOwner() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, device.getOwner());
            }
            if (device.getProductDisplayName() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, device.getProductDisplayName());
            }
            if (device.getManufacturer() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, device.getManufacturer());
            }
            if (device.getSoftwareRevision() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, device.getSoftwareRevision());
            }
            if (device.getHardwareRevision() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, device.getHardwareRevision());
            }
            if (device.getDeviceId() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindString(10, device.getDeviceId());
            }
            if (device.getMacAddress() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, device.getMacAddress());
            }
            if (device.getSku() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, device.getSku());
            }
            if (device.getFirmwareRevision() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, device.getFirmwareRevision());
            }
            ajVar.bindLong(14, (long) device.getBatteryLevel());
            if (device.getVibrationStrength() == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindLong(15, (long) device.getVibrationStrength().intValue());
            }
            ajVar.bindLong(16, device.isActive() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM device WHERE deviceId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM device";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<Device> call() throws Exception {
            Integer num;
            Cursor a = pi.a(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "major");
                int b2 = oi.b(a, "minor");
                int b3 = oi.b(a, "createdAt");
                int b4 = oi.b(a, "updatedAt");
                int b5 = oi.b(a, "owner");
                int b6 = oi.b(a, "productDisplayName");
                int b7 = oi.b(a, "manufacturer");
                int b8 = oi.b(a, "softwareRevision");
                int b9 = oi.b(a, "hardwareRevision");
                int b10 = oi.b(a, "deviceId");
                int b11 = oi.b(a, "macAddress");
                int b12 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = oi.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = oi.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = oi.b(a, "vibrationStrength");
                int i = b9;
                int b16 = oi.b(a, "isActive");
                int i2 = b8;
                int i3 = b7;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i4 = a.getInt(b14);
                    if (a.isNull(b15)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b15));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, num, a.getInt(b16) != 0);
                    device.setMajor(a.getInt(b));
                    device.setMinor(a.getInt(b2));
                    device.setCreatedAt(a.getString(b3));
                    device.setUpdatedAt(a.getString(b4));
                    device.setOwner(a.getString(b5));
                    device.setProductDisplayName(a.getString(b6));
                    device.setManufacturer(a.getString(i3));
                    device.setSoftwareRevision(a.getString(i2));
                    device.setHardwareRevision(a.getString(i));
                    arrayList.add(device);
                    b = b;
                    i3 = i3;
                    i2 = i2;
                    i = i;
                    b15 = b15;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon5(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Device call() throws Exception {
            Device device;
            Integer num;
            Cursor a = pi.a(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "major");
                int b2 = oi.b(a, "minor");
                int b3 = oi.b(a, "createdAt");
                int b4 = oi.b(a, "updatedAt");
                int b5 = oi.b(a, "owner");
                int b6 = oi.b(a, "productDisplayName");
                int b7 = oi.b(a, "manufacturer");
                int b8 = oi.b(a, "softwareRevision");
                int b9 = oi.b(a, "hardwareRevision");
                int b10 = oi.b(a, "deviceId");
                int b11 = oi.b(a, "macAddress");
                int b12 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = oi.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = oi.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = oi.b(a, "vibrationStrength");
                int b16 = oi.b(a, "isActive");
                if (a.moveToFirst()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i = a.getInt(b14);
                    if (a.isNull(b15)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b15));
                    }
                    device = new Device(string, string2, string3, string4, i, num, a.getInt(b16) != 0);
                    device.setMajor(a.getInt(b));
                    device.setMinor(a.getInt(b2));
                    device.setCreatedAt(a.getString(b3));
                    device.setUpdatedAt(a.getString(b4));
                    device.setOwner(a.getString(b5));
                    device.setProductDisplayName(a.getString(b6));
                    device.setManufacturer(a.getString(b7));
                    device.setSoftwareRevision(a.getString(b8));
                    device.setHardwareRevision(a.getString(b9));
                } else {
                    device = null;
                }
                return device;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DeviceDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfDevice = new Anon1(ciVar);
        this.__preparedStmtOfRemoveDeviceByDeviceId = new Anon2(ciVar);
        this.__preparedStmtOfCleanUp = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addAllDevice(List<Device> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addOrUpdateDevice(Device device) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(device);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public List<Device> getAllDevice() {
        fi fiVar;
        Integer num;
        fi b = fi.b("SELECT * FROM device", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "major");
            int b3 = oi.b(a, "minor");
            int b4 = oi.b(a, "createdAt");
            int b5 = oi.b(a, "updatedAt");
            int b6 = oi.b(a, "owner");
            int b7 = oi.b(a, "productDisplayName");
            int b8 = oi.b(a, "manufacturer");
            int b9 = oi.b(a, "softwareRevision");
            int b10 = oi.b(a, "hardwareRevision");
            int b11 = oi.b(a, "deviceId");
            int b12 = oi.b(a, "macAddress");
            int b13 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = oi.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = oi.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            fiVar = b;
            try {
                int b16 = oi.b(a, "vibrationStrength");
                int i = b10;
                int b17 = oi.b(a, "isActive");
                int i2 = b9;
                int i3 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i4 = a.getInt(b15);
                    if (a.isNull(b16)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b16));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, num, a.getInt(b17) != 0);
                    device.setMajor(a.getInt(b2));
                    device.setMinor(a.getInt(b3));
                    device.setCreatedAt(a.getString(b4));
                    device.setUpdatedAt(a.getString(b5));
                    device.setOwner(a.getString(b6));
                    device.setProductDisplayName(a.getString(b7));
                    device.setManufacturer(a.getString(i3));
                    device.setSoftwareRevision(a.getString(i2));
                    device.setHardwareRevision(a.getString(i));
                    arrayList.add(device);
                    b15 = b15;
                    i3 = i3;
                    i2 = i2;
                    i = i;
                    b13 = b13;
                }
                a.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, (Callable) new Anon4(fi.b("SELECT * FROM device", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public Device getDeviceByDeviceId(String str) {
        fi fiVar;
        Device device;
        Integer num;
        fi b = fi.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "major");
            int b3 = oi.b(a, "minor");
            int b4 = oi.b(a, "createdAt");
            int b5 = oi.b(a, "updatedAt");
            int b6 = oi.b(a, "owner");
            int b7 = oi.b(a, "productDisplayName");
            int b8 = oi.b(a, "manufacturer");
            int b9 = oi.b(a, "softwareRevision");
            int b10 = oi.b(a, "hardwareRevision");
            int b11 = oi.b(a, "deviceId");
            int b12 = oi.b(a, "macAddress");
            int b13 = oi.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = oi.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = oi.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            fiVar = b;
            try {
                int b16 = oi.b(a, "vibrationStrength");
                int b17 = oi.b(a, "isActive");
                if (a.moveToFirst()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i = a.getInt(b15);
                    if (a.isNull(b16)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b16));
                    }
                    device = new Device(string, string2, string3, string4, i, num, a.getInt(b17) != 0);
                    device.setMajor(a.getInt(b2));
                    device.setMinor(a.getInt(b3));
                    device.setCreatedAt(a.getString(b4));
                    device.setUpdatedAt(a.getString(b5));
                    device.setOwner(a.getString(b6));
                    device.setProductDisplayName(a.getString(b7));
                    device.setManufacturer(a.getString(b8));
                    device.setSoftwareRevision(a.getString(b9));
                    device.setHardwareRevision(a.getString(b10));
                } else {
                    device = null;
                }
                a.close();
                fiVar.c();
                return device;
            } catch (Throwable th) {
                th = th;
                a.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b;
            a.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        fi b = fi.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            b.bindNull(1);
        } else {
            b.bindString(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, (Callable) new Anon5(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void removeDeviceByDeviceId(String str) {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfRemoveDeviceByDeviceId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeviceByDeviceId.release(acquire);
        }
    }
}
