package com.portfolio.platform.data.source.local.dnd;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.aj;
import com.fossil.ci;
import com.fossil.fi;
import com.fossil.ji;
import com.fossil.oi;
import com.fossil.pi;
import com.fossil.vh;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDScheduledTimeDao_Impl implements DNDScheduledTimeDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ vh<DNDScheduledTimeModel> __insertionAdapterOfDNDScheduledTimeModel;
    @DexIgnore
    public /* final */ ji __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<DNDScheduledTimeModel> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dndScheduledTimeModel` (`scheduledTimeName`,`minutes`,`scheduledTimeType`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, DNDScheduledTimeModel dNDScheduledTimeModel) {
            if (dNDScheduledTimeModel.getScheduledTimeName() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, dNDScheduledTimeModel.getScheduledTimeName());
            }
            ajVar.bindLong(2, (long) dNDScheduledTimeModel.getMinutes());
            ajVar.bindLong(3, (long) dNDScheduledTimeModel.getScheduledTimeType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends ji {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM dndScheduledTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon3(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DNDScheduledTimeModel> call() throws Exception {
            Cursor a = pi.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "scheduledTimeName");
                int b2 = oi.b(a, "minutes");
                int b3 = oi.b(a, "scheduledTimeType");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new DNDScheduledTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<DNDScheduledTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ fi val$_statement;

        @DexIgnore
        public Anon4(fi fiVar) {
            this.val$_statement = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DNDScheduledTimeModel call() throws Exception {
            DNDScheduledTimeModel dNDScheduledTimeModel = null;
            Cursor a = pi.a(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int b = oi.b(a, "scheduledTimeName");
                int b2 = oi.b(a, "minutes");
                int b3 = oi.b(a, "scheduledTimeType");
                if (a.moveToFirst()) {
                    dNDScheduledTimeModel = new DNDScheduledTimeModel(a.getString(b), a.getInt(b2), a.getInt(b3));
                }
                return dNDScheduledTimeModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DNDScheduledTimeDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfDNDScheduledTimeModel = new Anon1(ciVar);
        this.__preparedStmtOfDelete = new Anon2(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i) {
        fi b = fi.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        DNDScheduledTimeModel dNDScheduledTimeModel = null;
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "scheduledTimeName");
            int b3 = oi.b(a, "minutes");
            int b4 = oi.b(a, "scheduledTimeType");
            if (a.moveToFirst()) {
                dNDScheduledTimeModel = new DNDScheduledTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4));
            }
            return dNDScheduledTimeModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i) {
        fi b = fi.b("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        b.bindLong(1, (long) i);
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, (Callable) new Anon4(b));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime() {
        return this.__db.getInvalidationTracker().a(new String[]{"dndScheduledTimeModel"}, false, (Callable) new Anon3(fi.b("SELECT * FROM dndScheduledTimeModel", 0)));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public List<DNDScheduledTimeModel> getListDNDScheduledTimeModel() {
        fi b = fi.b("SELECT * FROM dndScheduledTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = pi.a(this.__db, b, false, null);
        try {
            int b2 = oi.b(a, "scheduledTimeName");
            int b3 = oi.b(a, "minutes");
            int b4 = oi.b(a, "scheduledTimeType");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DNDScheduledTimeModel(a.getString(b2), a.getInt(b3), a.getInt(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(dNDScheduledTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
