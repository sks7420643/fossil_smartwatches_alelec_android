package com.portfolio.platform.data.source.remote;

import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LabelRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public LabelRemoteDataSource(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.api = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLabel(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.local.label.Label>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1 r0 = (com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1 r0 = new com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003a
            if (r2 != r4) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource r9 = (com.portfolio.platform.data.source.remote.LabelRemoteDataSource) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$response$Anon1 r10 = new com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$response$Anon1
            r10.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00b2
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "data: "
            r0.append(r1)
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r1 = r10.a()
            if (r1 == 0) goto L_0x00ae
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            r0.append(r1)
            java.lang.String r1 = " - isFromCache: "
            r0.append(r1)
            boolean r1 = r10.b()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "LabelRemoteDataSource"
            r9.d(r1, r0)
            java.lang.Object r9 = r10.a()
            com.portfolio.platform.data.source.remote.ApiResponse r9 = (com.portfolio.platform.data.source.remote.ApiResponse) r9
            java.util.List r9 = r9.get_items()
            boolean r0 = r9.isEmpty()
            r0 = r0 ^ r4
            if (r0 == 0) goto L_0x00a4
            r0 = 0
            java.lang.Object r9 = r9.get(r0)
            r3 = r9
            com.portfolio.platform.data.source.local.label.Label r3 = (com.portfolio.platform.data.source.local.label.Label) r3
        L_0x00a4:
            com.fossil.bj5 r9 = new com.fossil.bj5
            boolean r10 = r10.b()
            r9.<init>(r3, r10)
            goto L_0x00cc
        L_0x00ae:
            com.fossil.ee7.a()
            throw r3
        L_0x00b2:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00cd
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00cc:
            return r9
        L_0x00cd:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.LabelRemoteDataSource.getLabel(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
