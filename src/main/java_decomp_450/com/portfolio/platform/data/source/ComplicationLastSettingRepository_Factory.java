package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSettingRepository_Factory implements Factory<ComplicationLastSettingRepository> {
    @DexIgnore
    public /* final */ Provider<ComplicationLastSettingDao> mComplicationLastSettingDaoProvider;

    @DexIgnore
    public ComplicationLastSettingRepository_Factory(Provider<ComplicationLastSettingDao> provider) {
        this.mComplicationLastSettingDaoProvider = provider;
    }

    @DexIgnore
    public static ComplicationLastSettingRepository_Factory create(Provider<ComplicationLastSettingDao> provider) {
        return new ComplicationLastSettingRepository_Factory(provider);
    }

    @DexIgnore
    public static ComplicationLastSettingRepository newInstance(ComplicationLastSettingDao complicationLastSettingDao) {
        return new ComplicationLastSettingRepository(complicationLastSettingDao);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ComplicationLastSettingRepository get() {
        return newInstance(this.mComplicationLastSettingDaoProvider.get());
    }
}
