package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.te4;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import java.util.UUID;

@DatabaseTable(tableName = "microApp")
public class MicroApp implements Parcelable {
    public static final String COLUMN_APP_ID = "appId";
    public static final String COLUMN_APP_SETTING = "appSetting";
    public static final String COLUMN_CREATE_AT = "createdAt";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LIKE = "like";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PLATFORM = "platform";
    public static final String COLUMN_RELEASE_DATE = "releaseDate";
    public static final String COLUMN_UPDATE_AT = "updatedAt";
    public static final Parcelable.Creator<MicroApp> CREATOR = new Anon1();
    @DatabaseField(columnName = "appId")
    public String appId;
    @DatabaseField(columnName = COLUMN_APP_SETTING)
    public String appSettings;
    @DatabaseField(columnName = "createdAt")
    @te4("createdAt")
    public long createAt;
    @DatabaseField(columnName = "description")
    @te4("description")
    public String description;
    @DatabaseField(columnName = "icon")
    @te4("icon")
    public String iconUrl;
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DatabaseField(columnName = COLUMN_LIKE)
    @te4(COLUMN_LIKE)
    public int like;
    @DatabaseField(columnName = "name")
    @te4("name")
    public String name;
    @DatabaseField(columnName = "platform")
    @te4("platform")
    public String platform;
    @DatabaseField(columnName = COLUMN_RELEASE_DATE)
    @te4(COLUMN_RELEASE_DATE)
    public long releaseDate;
    @DatabaseField(columnName = "updatedAt")
    @te4("updatedAt")
    public long updateAt;

    public static class Anon1 implements Parcelable.Creator<MicroApp> {
        @Override // android.os.Parcelable.Creator
        public MicroApp createFromParcel(Parcel parcel) {
            return new MicroApp(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public MicroApp[] newArray(int i) {
            return new MicroApp[i];
        }
    }

    public static /* synthetic */ class Anon2 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID;

        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|32) */
        /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0084 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0090 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00a8 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID[] r0 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID = r0
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x001d }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_SELFIE     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_ALARM_ID     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x003e }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_ALERT_ID     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_DATE_ID     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x006c }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x0090 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID     // Catch:{ NoSuchFieldError -> 0x0090 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0090 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0090 }
            L_0x0090:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x009c }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD     // Catch:{ NoSuchFieldError -> 0x009c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009c }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009c }
            L_0x009c:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x00a8 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE     // Catch:{ NoSuchFieldError -> 0x00a8 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a8 }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00a8 }
            L_0x00a8:
                int[] r0 = com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID     // Catch:{ NoSuchFieldError -> 0x00b4 }
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_STOPWATCH     // Catch:{ NoSuchFieldError -> 0x00b4 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b4 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b4 }
            L_0x00b4:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.legacy.threedotzero.MicroApp.Anon2.<clinit>():void");
        }
        */
    }

    public MicroApp() {
        this.id = UUID.randomUUID().toString();
        this.appId = MicroAppInstruction.MicroAppID.UAPP_UNKNOWN.getValue();
        this.description = "";
        this.name = "";
        this.iconUrl = "";
    }

    public int describeContents() {
        return 0;
    }

    public String getAppId() {
        return this.appId;
    }

    public String getAppSettings() {
        return this.appSettings;
    }

    public long getCreateAt() {
        return this.createAt;
    }

    public int getDefaultIconId() {
        switch (Anon2.$SwitchMap$com$misfit$frameworks$buttonservice$model$microapp$MicroAppInstruction$MicroAppID[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(this.appId).ordinal()]) {
            case 2:
                return 2131231167;
            case 3:
                return 2131231159;
            case 4:
                return 2131231160;
            case 5:
                return 2131231165;
            case 6:
                return 2131231163;
            case 7:
                return 2131231162;
            case 8:
                return 2131231164;
            case 9:
                return 2131231166;
            case 10:
                return 2131231170;
            case 11:
                return 2131231171;
            case 12:
                return 2131231158;
            case 13:
                return 2131231172;
            case 14:
                return 2131231168;
            case 15:
                return 2131231169;
            default:
                return 2131231100;
        }
    }

    public String getDescription() {
        return this.description;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public String getId() {
        return this.id;
    }

    public int getLike() {
        return this.like;
    }

    public String getName() {
        return this.name;
    }

    public String getPlatform() {
        return this.platform;
    }

    public long getReleaseDate() {
        return this.releaseDate;
    }

    public long getUpdateAt() {
        return this.updateAt;
    }

    public void setAppId(String str) {
        this.appId = str;
    }

    public void setAppSettings(String str) {
        this.appSettings = str;
    }

    public void setCreateAt(long j) {
        this.createAt = j;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setIconUrl(String str) {
        this.iconUrl = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setLike(int i) {
        this.like = i;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPlatform(String str) {
        this.platform = str;
    }

    public void setReleaseDate(long j) {
        this.releaseDate = j;
    }

    public void setUpdateAt(long j) {
        this.updateAt = j;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.appId);
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeString(this.platform);
        parcel.writeInt(this.like);
        parcel.writeString(this.appSettings);
        parcel.writeString(this.iconUrl);
        parcel.writeLong(this.releaseDate);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
    }

    public MicroApp(Parcel parcel) {
        this.id = UUID.randomUUID().toString();
        this.appId = parcel.readString();
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.platform = parcel.readString();
        this.like = parcel.readInt();
        this.appSettings = parcel.readString();
        this.iconUrl = parcel.readString();
        this.releaseDate = parcel.readLong();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
    }
}
