package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ah5;
import com.fossil.ee7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.zd7;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryLocalDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            ee7.b(str, "<set-?>");
            MicroAppGalleryLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppGalleryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void deleteListMicroApp(String str) {
        ee7.b(str, "serial");
        String str2 = TAG;
        MFLogger.d(str2, "deleteListMicroApp serial=" + str);
        ah5.p.a().c().deleteMicroAppList(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ee7.b(str2, "microAppId");
        String str3 = TAG;
        MFLogger.d(str3, "getMicroApp deviceSerial=" + str + " microAppId=" + str2);
        MicroApp microApp = ah5.p.a().c().getMicroApp(str, str2);
        if (microApp != null) {
            MFLogger.d(TAG, "getMicroApp onSuccess");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onSuccess(microApp);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroApp onFail");
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        List<MicroApp> listMicroApp = ah5.p.a().c().getListMicroApp(str);
        if (listMicroApp == null || !(!listMicroApp.isEmpty())) {
            MFLogger.d(TAG, "getMicroAppGallery onFail");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroAppGallery onSuccess");
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(listMicroApp);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void updateListMicroApp(List<? extends MicroApp> list) {
        if (list != null && (!list.isEmpty())) {
            String str = TAG;
            MFLogger.d(str, "updateListMicroApp microAppListSize=" + list.size());
            ah5.p.a().c().updateListMicroApp(list);
        }
    }
}
