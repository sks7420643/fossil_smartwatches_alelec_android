package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.de4;
import com.fossil.ie4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = MicroAppSetting.TABLE_NAME)
public class MicroAppSetting {
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_FIRST_USED; // = "firstUsed";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATED_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String LIKE; // = "isLiked";
    @DexIgnore
    public static /* final */ String MICRO_APP_ID; // = "appId";
    @DexIgnore
    public static /* final */ String SETTING; // = "setting";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "microAppSetting";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long createdAt;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_FIRST_USED)
    public long firstUsed;
    @DexIgnore
    @DatabaseField(columnName = LIKE)
    public boolean like;
    @DexIgnore
    @DatabaseField(columnName = "appId", id = true)
    public String microAppId;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = SETTING)
    public String setting;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updatedAt;

    @DexIgnore
    public MicroAppSetting() {
        this.microAppId = "";
        this.setting = "";
        this.like = false;
        this.pinType = 0;
        this.createdAt = System.currentTimeMillis();
        this.updatedAt = System.currentTimeMillis();
    }

    @DexIgnore
    public long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public long getFirstUsed() {
        return this.firstUsed;
    }

    @DexIgnore
    public ie4 getJsonObject() {
        ie4 ie4 = new ie4();
        try {
            de4 de4 = new de4();
            ie4 ie42 = new ie4();
            ie42.a("appId", this.microAppId);
            if (this.setting == null || this.setting.isEmpty()) {
                ie42.a(SETTING, new ie4());
            } else {
                ie42.a(SETTING, (JsonElement) new Gson().a(this.setting, ie4.class));
            }
            ie42.a(LIKE, Boolean.valueOf(this.like));
            de4.a(ie42);
            ie4.a(CloudLogWriter.ITEMS_PARAM, de4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppSetting", "initJsonData - json: " + ie4);
        return ie4;
    }

    @DexIgnore
    public String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public boolean isLike() {
        return this.like;
    }

    @DexIgnore
    public void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public void setFirstUsed(long j) {
        this.firstUsed = j;
    }

    @DexIgnore
    public void setLike(boolean z) {
        this.like = z;
    }

    @DexIgnore
    public void setMicroAppId(String str) {
        this.microAppId = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setSetting(String str) {
        this.setting = str;
    }

    @DexIgnore
    public void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public MicroAppSetting(String str, String str2) {
        this.microAppId = str;
        this.setting = str2;
        this.like = false;
        this.pinType = 0;
        this.createdAt = System.currentTimeMillis();
        this.updatedAt = System.currentTimeMillis();
    }

    @DexIgnore
    public MicroAppSetting(String str) {
        this.microAppId = str;
        this.setting = "";
        this.like = false;
        this.pinType = 0;
        this.createdAt = System.currentTimeMillis();
        this.updatedAt = System.currentTimeMillis();
    }
}
