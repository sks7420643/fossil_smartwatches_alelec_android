package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.cj5;
import com.fossil.fv7;
import com.fossil.ie4;
import com.fossil.pj4;
import com.fossil.ru7;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppSettingRemoteDataSource implements MicroAppSettingDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ pj4 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements ru7<ie4> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppSettingDataSource.MicroAppSettingListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$microAppSettingList;

        @DexIgnore
        public Anon1(List list, MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
            this.val$microAppSettingList = list;
            this.val$callback = microAppSettingListCallback;
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onFailure(Call<ie4> call, Throwable th) {
            MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFail");
            if (this.val$callback == null) {
                return;
            }
            if (!this.val$microAppSettingList.isEmpty()) {
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFailure presetList not null");
                this.val$callback.onSuccess(this.val$microAppSettingList);
                return;
            }
            MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onFailure presetList is null");
            this.val$callback.onFail();
        }

        @DexIgnore
        @Override // com.fossil.ru7
        public void onResponse(Call<ie4> call, fv7<ie4> fv7) {
            if (fv7.d()) {
                ie4 a = fv7.a();
                String str = MicroAppSettingRemoteDataSource.TAG;
                MFLogger.d(str, "getMicroAppSettingList onSuccess response=" + a);
                cj5 cj5 = new cj5();
                cj5.a(a);
                this.val$microAppSettingList.addAll(cj5.a());
                Range b = cj5.b();
                if (b != null && b.isHasNext()) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onSuccess hasNext=true");
                    MicroAppSettingRemoteDataSource.this.getMicroAppSettingList(b.getOffset() + b.getLimit() + 1, b.getLimit(), this);
                } else if (this.val$callback != null) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList onSuccess hasNext=false");
                    if (!this.val$microAppSettingList.isEmpty()) {
                        this.val$callback.onSuccess(this.val$microAppSettingList);
                    } else {
                        this.val$callback.onFail();
                    }
                }
            } else {
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful");
                if (!this.val$microAppSettingList.isEmpty()) {
                    MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful presetList not null");
                    MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback = this.val$callback;
                    if (microAppSettingListCallback != null) {
                        microAppSettingListCallback.onSuccess(this.val$microAppSettingList);
                        return;
                    }
                    return;
                }
                MFLogger.d(MicroAppSettingRemoteDataSource.TAG, "getMicroAppSettingList !isSuccessful presetList is null");
                MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback2 = this.val$callback;
                if (microAppSettingListCallback2 != null) {
                    microAppSettingListCallback2.onFail();
                }
            }
        }
    }

    @DexIgnore
    public MicroAppSettingRemoteDataSource(ShortcutApiService shortcutApiService, pj4 pj4) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = pj4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateMicroAppSetting microAppSetting=" + microAppSetting.getSetting() + " microAppId=" + microAppSetting.getMicroAppId());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void clearData() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void getMicroAppSetting(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void getMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        MFLogger.d(TAG, "getMicroAppSettingList");
        getMicroAppSettingList(0, 100, new Anon1(new ArrayList(), microAppSettingListCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
    }

    @DexIgnore
    public void getMicroAppSettingList(int i, int i2, ru7<ie4> ru7) {
        String str = TAG;
        MFLogger.d(str, "getMicroAppSettingList offset=" + i + " size=" + i2);
    }
}
