package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.ah5;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantLocalDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            ee7.b(str, "<set-?>");
            MicroAppVariantLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantLocalDataSource.class.getSimpleName();
        ee7.a((Object) simpleName, "MicroAppVariantLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public final boolean addOrUpDateVariant(MicroAppVariant microAppVariant) {
        ee7.b(microAppVariant, "microAppVariant");
        String str = TAG;
        MFLogger.d(str, ".addOrUpDateVariant - serial=" + microAppVariant.getSerialNumbers() + ", major=" + microAppVariant.getMajorNumber() + " minor=" + microAppVariant.getMinorNumber() + " id=" + microAppVariant.getId());
        return ah5.p.a().c().addOrUpdateMicroAppVariant(microAppVariant);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, MicroAppVariantDataSource.AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        ee7.b(declarationFile, "declarationFile");
        ee7.b(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
        String str = TAG;
        MFLogger.d(str, "addOrUpdateDeclarationFile declarationFileId=" + declarationFile.getFileId());
        if (ah5.p.a().c().addOrUpdateDeclarationFile(declarationFile)) {
            addOrUpdateDeclarationFileCallback.onSuccess(declarationFile);
        } else {
            addOrUpdateDeclarationFileCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        ee7.b(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serial=" + str + " major=" + i + " minor=" + i2);
        List<MicroAppVariant> allMicroAppVariant = ah5.p.a().c().getAllMicroAppVariant(str, i);
        if (allMicroAppVariant == null || !(!allMicroAppVariant.isEmpty())) {
            String str3 = TAG;
            MFLogger.d(str3, "getAllMicroAppVariants onFail serial=" + str);
            if (getVariantListCallback != null) {
                getVariantListCallback.onFail(600);
                return;
            }
            return;
        }
        String str4 = TAG;
        MFLogger.d(str4, "getAllMicroAppVariants onSuccess serial=" + str);
        if (getVariantListCallback != null) {
            getVariantListCallback.onSuccess(allMicroAppVariant);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, MicroAppVariantDataSource.GetVariantCallback getVariantCallback) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "microAppId");
        ee7.b(str3, "variantName");
        String str4 = TAG;
        MFLogger.d(str4, "getMicroAppVariant serial=" + str + " microAppId=" + str2 + " major=" + i + " minor=" + i2);
        MicroAppVariant microAppVariant = ah5.p.a().c().getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            String str5 = TAG;
            MFLogger.d(str5, "getMicroAppVariant onSuccess serial=" + str + " microAppId=" + str2);
            if (getVariantCallback != null) {
                getVariantCallback.onSuccess(microAppVariant);
                return;
            }
            return;
        }
        String str6 = TAG;
        MFLogger.d(str6, "getMicroAppVariant onFail serial=" + str + " microAppId=" + str2);
        if (getVariantCallback != null) {
            getVariantCallback.onFail(600);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void removeMicroAppVariants(String str, int i, int i2) {
        ee7.b(str, "serialNumber");
        ah5.p.a().c().deleteMicroAppVariants(str, i, i2);
    }
}
