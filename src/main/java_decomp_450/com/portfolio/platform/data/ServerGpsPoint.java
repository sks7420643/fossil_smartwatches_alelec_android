package com.portfolio.platform.data;

import com.facebook.places.PlaceManager;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerGpsPoint {
    @DexIgnore
    @te4(PlaceManager.PARAM_ALTITUDE)
    public /* final */ Double altitude;
    @DexIgnore
    @te4(PlaceManager.PARAM_HEADING)
    public /* final */ Double heading;
    @DexIgnore
    @te4("horizontalAccuracy")
    public /* final */ Float horizontalAccuracy;
    @DexIgnore
    @te4("latitude")
    public double latitude;
    @DexIgnore
    @te4("longitude")
    public /* final */ double longitude;
    @DexIgnore
    @te4(PlaceManager.PARAM_SPEED)
    public /* final */ Double speed;
    @DexIgnore
    @te4("at")
    public /* final */ String time;
    @DexIgnore
    @te4("verticalAccuracy")
    public /* final */ Float verticalAccuracy;

    @DexIgnore
    public ServerGpsPoint(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5) {
        ee7.b(str, LogBuilder.KEY_TIME);
        this.horizontalAccuracy = f;
        this.latitude = d;
        this.longitude = d2;
        this.time = str;
        this.verticalAccuracy = f2;
        this.altitude = d3;
        this.speed = d4;
        this.heading = d5;
    }

    @DexIgnore
    public static /* synthetic */ ServerGpsPoint copy$default(ServerGpsPoint serverGpsPoint, Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5, int i, Object obj) {
        return serverGpsPoint.copy((i & 1) != 0 ? serverGpsPoint.horizontalAccuracy : f, (i & 2) != 0 ? serverGpsPoint.latitude : d, (i & 4) != 0 ? serverGpsPoint.longitude : d2, (i & 8) != 0 ? serverGpsPoint.time : str, (i & 16) != 0 ? serverGpsPoint.verticalAccuracy : f2, (i & 32) != 0 ? serverGpsPoint.altitude : d3, (i & 64) != 0 ? serverGpsPoint.speed : d4, (i & 128) != 0 ? serverGpsPoint.heading : d5);
    }

    @DexIgnore
    public final Float component1() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double component2() {
        return this.latitude;
    }

    @DexIgnore
    public final double component3() {
        return this.longitude;
    }

    @DexIgnore
    public final String component4() {
        return this.time;
    }

    @DexIgnore
    public final Float component5() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public final Double component6() {
        return this.altitude;
    }

    @DexIgnore
    public final Double component7() {
        return this.speed;
    }

    @DexIgnore
    public final Double component8() {
        return this.heading;
    }

    @DexIgnore
    public final ServerGpsPoint copy(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5) {
        ee7.b(str, LogBuilder.KEY_TIME);
        return new ServerGpsPoint(f, d, d2, str, f2, d3, d4, d5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ServerGpsPoint)) {
            return false;
        }
        ServerGpsPoint serverGpsPoint = (ServerGpsPoint) obj;
        return ee7.a(this.horizontalAccuracy, serverGpsPoint.horizontalAccuracy) && Double.compare(this.latitude, serverGpsPoint.latitude) == 0 && Double.compare(this.longitude, serverGpsPoint.longitude) == 0 && ee7.a(this.time, serverGpsPoint.time) && ee7.a(this.verticalAccuracy, serverGpsPoint.verticalAccuracy) && ee7.a(this.altitude, serverGpsPoint.altitude) && ee7.a(this.speed, serverGpsPoint.speed) && ee7.a(this.heading, serverGpsPoint.heading);
    }

    @DexIgnore
    public final Double getAltitude() {
        return this.altitude;
    }

    @DexIgnore
    public final Double getHeading() {
        return this.heading;
    }

    @DexIgnore
    public final Float getHorizontalAccuracy() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public final double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public final Double getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final String getTime() {
        return this.time;
    }

    @DexIgnore
    public final Float getVerticalAccuracy() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public int hashCode() {
        Float f = this.horizontalAccuracy;
        int i = 0;
        int hashCode = (((((f != null ? f.hashCode() : 0) * 31) + Double.doubleToLongBits(this.latitude)) * 31) + Double.doubleToLongBits(this.longitude)) * 31;
        String str = this.time;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Float f2 = this.verticalAccuracy;
        int hashCode3 = (hashCode2 + (f2 != null ? f2.hashCode() : 0)) * 31;
        Double d = this.altitude;
        int hashCode4 = (hashCode3 + (d != null ? d.hashCode() : 0)) * 31;
        Double d2 = this.speed;
        int hashCode5 = (hashCode4 + (d2 != null ? d2.hashCode() : 0)) * 31;
        Double d3 = this.heading;
        if (d3 != null) {
            i = d3.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public String toString() {
        return "ServerGpsPoint(horizontalAccuracy=" + this.horizontalAccuracy + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", time=" + this.time + ", verticalAccuracy=" + this.verticalAccuracy + ", altitude=" + this.altitude + ", speed=" + this.speed + ", heading=" + this.heading + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerGpsPoint(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5, int i, zd7 zd7) {
        this((i & 1) != 0 ? null : f, d, d2, str, (i & 16) != 0 ? null : f2, (i & 32) != 0 ? null : d3, (i & 64) != 0 ? null : d4, (i & 128) != 0 ? null : d5);
    }
}
