package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpEmailAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("acceptedLocationDataSharing")
    public ArrayList<String> acceptedLocationDataSharing;
    @DexIgnore
    @te4("acceptedPrivacies")
    public ArrayList<String> acceptedPrivacies;
    @DexIgnore
    @te4("acceptedTermsOfService")
    public ArrayList<String> acceptedTermsOfService;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BIRTHDAY)
    public String birthday;
    @DexIgnore
    @te4("clientId")
    public String clientId;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public boolean diagnosticEnabled;
    @DexIgnore
    @te4(Constants.EMAIL)
    public String email;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String firstName;
    @DexIgnore
    @te4("gender")
    public String gender;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String lastName;
    @DexIgnore
    @te4("password")
    public String password;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpEmailAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpEmailAuth createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new SignUpEmailAuth(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpEmailAuth[] newArray(int i) {
            return new SignUpEmailAuth[i];
        }
    }

    @DexIgnore
    public SignUpEmailAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        ee7.b(str, Constants.EMAIL);
        ee7.b(str2, "password");
        ee7.b(str3, "clientId");
        ee7.b(str4, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str5, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str6, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str7, "gender");
        ee7.b(arrayList, "acceptedLocationDataSharing");
        ee7.b(arrayList2, "acceptedPrivacies");
        ee7.b(arrayList3, "acceptedTermsOfService");
        this.email = str;
        this.password = str2;
        this.clientId = str3;
        this.firstName = str4;
        this.lastName = str5;
        this.birthday = str6;
        this.gender = str7;
        this.diagnosticEnabled = z;
        this.acceptedLocationDataSharing = arrayList;
        this.acceptedPrivacies = arrayList2;
        this.acceptedTermsOfService = arrayList3;
    }

    @DexIgnore
    public static /* synthetic */ SignUpEmailAuth copy$default(SignUpEmailAuth signUpEmailAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i, Object obj) {
        return signUpEmailAuth.copy((i & 1) != 0 ? signUpEmailAuth.email : str, (i & 2) != 0 ? signUpEmailAuth.password : str2, (i & 4) != 0 ? signUpEmailAuth.clientId : str3, (i & 8) != 0 ? signUpEmailAuth.firstName : str4, (i & 16) != 0 ? signUpEmailAuth.lastName : str5, (i & 32) != 0 ? signUpEmailAuth.birthday : str6, (i & 64) != 0 ? signUpEmailAuth.gender : str7, (i & 128) != 0 ? signUpEmailAuth.diagnosticEnabled : z, (i & 256) != 0 ? signUpEmailAuth.acceptedLocationDataSharing : arrayList, (i & 512) != 0 ? signUpEmailAuth.acceptedPrivacies : arrayList2, (i & 1024) != 0 ? signUpEmailAuth.acceptedTermsOfService : arrayList3);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final ArrayList<String> component10() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> component11() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String component2() {
        return this.password;
    }

    @DexIgnore
    public final String component3() {
        return this.clientId;
    }

    @DexIgnore
    public final String component4() {
        return this.firstName;
    }

    @DexIgnore
    public final String component5() {
        return this.lastName;
    }

    @DexIgnore
    public final String component6() {
        return this.birthday;
    }

    @DexIgnore
    public final String component7() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component8() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final ArrayList<String> component9() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final SignUpEmailAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        ee7.b(str, Constants.EMAIL);
        ee7.b(str2, "password");
        ee7.b(str3, "clientId");
        ee7.b(str4, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str5, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str6, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str7, "gender");
        ee7.b(arrayList, "acceptedLocationDataSharing");
        ee7.b(arrayList2, "acceptedPrivacies");
        ee7.b(arrayList3, "acceptedTermsOfService");
        return new SignUpEmailAuth(str, str2, str3, str4, str5, str6, str7, z, arrayList, arrayList2, arrayList3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SignUpEmailAuth)) {
            return false;
        }
        SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) obj;
        return ee7.a(this.email, signUpEmailAuth.email) && ee7.a(this.password, signUpEmailAuth.password) && ee7.a(this.clientId, signUpEmailAuth.clientId) && ee7.a(this.firstName, signUpEmailAuth.firstName) && ee7.a(this.lastName, signUpEmailAuth.lastName) && ee7.a(this.birthday, signUpEmailAuth.birthday) && ee7.a(this.gender, signUpEmailAuth.gender) && this.diagnosticEnabled == signUpEmailAuth.diagnosticEnabled && ee7.a(this.acceptedLocationDataSharing, signUpEmailAuth.acceptedLocationDataSharing) && ee7.a(this.acceptedPrivacies, signUpEmailAuth.acceptedPrivacies) && ee7.a(this.acceptedTermsOfService, signUpEmailAuth.acceptedTermsOfService);
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedLocationDataSharing() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getPassword() {
        return this.password;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.password;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.clientId;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.firstName;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.lastName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.birthday;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.gender;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode7 + i2) * 31;
        ArrayList<String> arrayList = this.acceptedLocationDataSharing;
        int hashCode8 = (i4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<String> arrayList2 = this.acceptedPrivacies;
        int hashCode9 = (hashCode8 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        ArrayList<String> arrayList3 = this.acceptedTermsOfService;
        if (arrayList3 != null) {
            i = arrayList3.hashCode();
        }
        return hashCode9 + i;
    }

    @DexIgnore
    public final void setAcceptedLocationDataSharing(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedLocationDataSharing = arrayList;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedPrivacies = arrayList;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedTermsOfService = arrayList;
    }

    @DexIgnore
    public final void setBirthday(String str) {
        ee7.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        ee7.b(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        ee7.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        ee7.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        ee7.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        ee7.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setPassword(String str) {
        ee7.b(str, "<set-?>");
        this.password = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpEmailAuth(email=" + this.email + ", password=" + this.password + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ", acceptedLocationDataSharing=" + this.acceptedLocationDataSharing + ", acceptedPrivacies=" + this.acceptedPrivacies + ", acceptedTermsOfService=" + this.acceptedTermsOfService + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.password);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
        parcel.writeStringList(this.acceptedLocationDataSharing);
        parcel.writeStringList(this.acceptedPrivacies);
        parcel.writeStringList(this.acceptedTermsOfService);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SignUpEmailAuth(android.os.Parcel r15) {
        /*
            r14 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = r15.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000f
            r3 = r0
            goto L_0x0010
        L_0x000f:
            r3 = r1
        L_0x0010:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x0018
            r4 = r0
            goto L_0x0019
        L_0x0018:
            r4 = r1
        L_0x0019:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x0021
            r5 = r0
            goto L_0x0022
        L_0x0021:
            r5 = r1
        L_0x0022:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x002a
            r6 = r0
            goto L_0x002b
        L_0x002a:
            r6 = r1
        L_0x002b:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x0033
            r7 = r0
            goto L_0x0034
        L_0x0033:
            r7 = r1
        L_0x0034:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x003c
            r8 = r0
            goto L_0x003d
        L_0x003c:
            r8 = r1
        L_0x003d:
            java.lang.String r0 = r15.readString()
            if (r0 == 0) goto L_0x0045
            r9 = r0
            goto L_0x0046
        L_0x0045:
            r9 = r1
        L_0x0046:
            byte r0 = r15.readByte()
            r1 = 0
            byte r2 = (byte) r1
            if (r0 == r2) goto L_0x0051
            r0 = 1
            r10 = 1
            goto L_0x0052
        L_0x0051:
            r10 = 0
        L_0x0052:
            java.util.ArrayList r11 = r15.createStringArrayList()
        */
        //  java.lang.String r0 = "null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */"
        /*
            if (r11 == 0) goto L_0x0077
            java.util.ArrayList r12 = r15.createStringArrayList()
            if (r12 == 0) goto L_0x0071
            java.util.ArrayList r13 = r15.createStringArrayList()
            if (r13 == 0) goto L_0x006b
            r2 = r14
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        L_0x006b:
            com.fossil.x87 r15 = new com.fossil.x87
            r15.<init>(r0)
            throw r15
        L_0x0071:
            com.fossil.x87 r15 = new com.fossil.x87
            r15.<init>(r0)
            throw r15
        L_0x0077:
            com.fossil.x87 r15 = new com.fossil.x87
            r15.<init>(r0)
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.SignUpEmailAuth.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public SignUpEmailAuth() {
        this("", "", "", "", "", "", "", false, new ArrayList(), new ArrayList(), new ArrayList());
    }
}
