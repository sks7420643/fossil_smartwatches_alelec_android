package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpSocialAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("acceptedLocationDataSharing")
    public ArrayList<String> acceptedLocationDataSharing;
    @DexIgnore
    @te4("acceptedPrivacies")
    public ArrayList<String> acceptedPrivacies;
    @DexIgnore
    @te4("acceptedTermsOfService")
    public ArrayList<String> acceptedTermsOfService;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BIRTHDAY)
    public String birthday;
    @DexIgnore
    @te4("clientId")
    public String clientId;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public boolean diagnosticEnabled;
    @DexIgnore
    @te4(Constants.EMAIL)
    public String email;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String firstName;
    @DexIgnore
    @te4("gender")
    public String gender;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String lastName;
    @DexIgnore
    @te4(Constants.SERVICE)
    public String service;
    @DexIgnore
    @te4("token")
    public String token;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpSocialAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpSocialAuth createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new SignUpSocialAuth(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SignUpSocialAuth[] newArray(int i) {
            return new SignUpSocialAuth[i];
        }
    }

    @DexIgnore
    public SignUpSocialAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        ee7.b(str, Constants.EMAIL);
        ee7.b(str2, Constants.SERVICE);
        ee7.b(str3, "token");
        ee7.b(str4, "clientId");
        ee7.b(str5, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str6, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str7, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str8, "gender");
        ee7.b(arrayList, "acceptedLocationDataSharing");
        ee7.b(arrayList2, "acceptedPrivacies");
        ee7.b(arrayList3, "acceptedTermsOfService");
        this.email = str;
        this.service = str2;
        this.token = str3;
        this.clientId = str4;
        this.firstName = str5;
        this.lastName = str6;
        this.birthday = str7;
        this.gender = str8;
        this.diagnosticEnabled = z;
        this.acceptedLocationDataSharing = arrayList;
        this.acceptedPrivacies = arrayList2;
        this.acceptedTermsOfService = arrayList3;
    }

    @DexIgnore
    public static /* synthetic */ SignUpSocialAuth copy$default(SignUpSocialAuth signUpSocialAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i, Object obj) {
        return signUpSocialAuth.copy((i & 1) != 0 ? signUpSocialAuth.email : str, (i & 2) != 0 ? signUpSocialAuth.service : str2, (i & 4) != 0 ? signUpSocialAuth.token : str3, (i & 8) != 0 ? signUpSocialAuth.clientId : str4, (i & 16) != 0 ? signUpSocialAuth.firstName : str5, (i & 32) != 0 ? signUpSocialAuth.lastName : str6, (i & 64) != 0 ? signUpSocialAuth.birthday : str7, (i & 128) != 0 ? signUpSocialAuth.gender : str8, (i & 256) != 0 ? signUpSocialAuth.diagnosticEnabled : z, (i & 512) != 0 ? signUpSocialAuth.acceptedLocationDataSharing : arrayList, (i & 1024) != 0 ? signUpSocialAuth.acceptedPrivacies : arrayList2, (i & 2048) != 0 ? signUpSocialAuth.acceptedTermsOfService : arrayList3);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final ArrayList<String> component10() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> component11() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> component12() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String component2() {
        return this.service;
    }

    @DexIgnore
    public final String component3() {
        return this.token;
    }

    @DexIgnore
    public final String component4() {
        return this.clientId;
    }

    @DexIgnore
    public final String component5() {
        return this.firstName;
    }

    @DexIgnore
    public final String component6() {
        return this.lastName;
    }

    @DexIgnore
    public final String component7() {
        return this.birthday;
    }

    @DexIgnore
    public final String component8() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component9() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final SignUpSocialAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        ee7.b(str, Constants.EMAIL);
        ee7.b(str2, Constants.SERVICE);
        ee7.b(str3, "token");
        ee7.b(str4, "clientId");
        ee7.b(str5, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str6, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str7, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str8, "gender");
        ee7.b(arrayList, "acceptedLocationDataSharing");
        ee7.b(arrayList2, "acceptedPrivacies");
        ee7.b(arrayList3, "acceptedTermsOfService");
        return new SignUpSocialAuth(str, str2, str3, str4, str5, str6, str7, str8, z, arrayList, arrayList2, arrayList3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SignUpSocialAuth)) {
            return false;
        }
        SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) obj;
        return ee7.a(this.email, signUpSocialAuth.email) && ee7.a(this.service, signUpSocialAuth.service) && ee7.a(this.token, signUpSocialAuth.token) && ee7.a(this.clientId, signUpSocialAuth.clientId) && ee7.a(this.firstName, signUpSocialAuth.firstName) && ee7.a(this.lastName, signUpSocialAuth.lastName) && ee7.a(this.birthday, signUpSocialAuth.birthday) && ee7.a(this.gender, signUpSocialAuth.gender) && this.diagnosticEnabled == signUpSocialAuth.diagnosticEnabled && ee7.a(this.acceptedLocationDataSharing, signUpSocialAuth.acceptedLocationDataSharing) && ee7.a(this.acceptedPrivacies, signUpSocialAuth.acceptedPrivacies) && ee7.a(this.acceptedTermsOfService, signUpSocialAuth.acceptedTermsOfService);
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedLocationDataSharing() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getService() {
        return this.service;
    }

    @DexIgnore
    public final String getToken() {
        return this.token;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.service;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.token;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.clientId;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.firstName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.lastName;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.birthday;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.gender;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode8 + i2) * 31;
        ArrayList<String> arrayList = this.acceptedLocationDataSharing;
        int hashCode9 = (i4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<String> arrayList2 = this.acceptedPrivacies;
        int hashCode10 = (hashCode9 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        ArrayList<String> arrayList3 = this.acceptedTermsOfService;
        if (arrayList3 != null) {
            i = arrayList3.hashCode();
        }
        return hashCode10 + i;
    }

    @DexIgnore
    public final void setAcceptedLocationDataSharing(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedLocationDataSharing = arrayList;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedPrivacies = arrayList;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.acceptedTermsOfService = arrayList;
    }

    @DexIgnore
    public final void setBirthday(String str) {
        ee7.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        ee7.b(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        ee7.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        ee7.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        ee7.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        ee7.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setService(String str) {
        ee7.b(str, "<set-?>");
        this.service = str;
    }

    @DexIgnore
    public final void setToken(String str) {
        ee7.b(str, "<set-?>");
        this.token = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpSocialAuth(email=" + this.email + ", service=" + this.service + ", token=" + this.token + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ", acceptedLocationDataSharing=" + this.acceptedLocationDataSharing + ", acceptedPrivacies=" + this.acceptedPrivacies + ", acceptedTermsOfService=" + this.acceptedTermsOfService + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.service);
        parcel.writeString(this.token);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
        parcel.writeStringList(this.acceptedLocationDataSharing);
        parcel.writeStringList(this.acceptedPrivacies);
        parcel.writeStringList(this.acceptedTermsOfService);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SignUpSocialAuth(android.os.Parcel r17) {
        /*
            r16 = this;
            java.lang.String r0 = "parcel"
            r1 = r17
            com.fossil.ee7.b(r1, r0)
            java.lang.String r0 = r17.readString()
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x0011
            r4 = r0
            goto L_0x0012
        L_0x0011:
            r4 = r2
        L_0x0012:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x001a
            r5 = r0
            goto L_0x001b
        L_0x001a:
            r5 = r2
        L_0x001b:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x0023
            r6 = r0
            goto L_0x0024
        L_0x0023:
            r6 = r2
        L_0x0024:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x002c
            r7 = r0
            goto L_0x002d
        L_0x002c:
            r7 = r2
        L_0x002d:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x0035
            r8 = r0
            goto L_0x0036
        L_0x0035:
            r8 = r2
        L_0x0036:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x003e
            r9 = r0
            goto L_0x003f
        L_0x003e:
            r9 = r2
        L_0x003f:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x0047
            r10 = r0
            goto L_0x0048
        L_0x0047:
            r10 = r2
        L_0x0048:
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x0050
            r11 = r0
            goto L_0x0051
        L_0x0050:
            r11 = r2
        L_0x0051:
            byte r0 = r17.readByte()
            r2 = 0
            byte r3 = (byte) r2
            if (r0 == r3) goto L_0x005c
            r0 = 1
            r12 = 1
            goto L_0x005d
        L_0x005c:
            r12 = 0
        L_0x005d:
            java.util.ArrayList r13 = r17.createStringArrayList()
        */
        //  java.lang.String r0 = "null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */"
        /*
            if (r13 == 0) goto L_0x0083
            java.util.ArrayList r14 = r17.createStringArrayList()
            if (r14 == 0) goto L_0x007d
            java.util.ArrayList r15 = r17.createStringArrayList()
            if (r15 == 0) goto L_0x0077
            r3 = r16
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        L_0x0077:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r0)
            throw r1
        L_0x007d:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r0)
            throw r1
        L_0x0083:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.SignUpSocialAuth.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public SignUpSocialAuth() {
        this("", "", "", "", "", "", "", "", false, new ArrayList(), new ArrayList(), new ArrayList());
    }
}
