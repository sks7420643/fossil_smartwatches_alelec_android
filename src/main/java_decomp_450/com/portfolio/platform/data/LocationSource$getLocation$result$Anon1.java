package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.portfolio.platform.data.LocationSource$getLocation$result$1", f = "LocationSource.kt", l = {71}, m = "invokeSuspend")
public final class LocationSource$getLocation$result$Anon1 extends zb7 implements kd7<yi7, fb7<? super LocationSource.Result>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocation$result$Anon1(LocationSource locationSource, Context context, fb7 fb7) {
        super(2, fb7);
        this.this$0 = locationSource;
        this.$context = context;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        LocationSource$getLocation$result$Anon1 locationSource$getLocation$result$Anon1 = new LocationSource$getLocation$result$Anon1(this.this$0, this.$context, fb7);
        locationSource$getLocation$result$Anon1.p$ = (yi7) obj;
        return locationSource$getLocation$result$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super LocationSource.Result> fb7) {
        return ((LocationSource$getLocation$result$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            LocationSource locationSource = this.this$0;
            Context context = this.$context;
            this.L$0 = yi7;
            this.label = 1;
            obj = locationSource.getLocationFromFuseLocationManager(context, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
