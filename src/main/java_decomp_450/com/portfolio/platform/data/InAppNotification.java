package com.portfolio.platform.data;

import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotification {
    @DexIgnore
    public String content;
    @DexIgnore
    public String id;
    @DexIgnore
    public String title;

    @DexIgnore
    public InAppNotification(String str, String str2, String str3) {
        ee7.b(str, "id");
        ee7.b(str2, "title");
        ee7.b(str3, "content");
        this.id = str;
        this.title = str2;
        this.content = str3;
    }

    @DexIgnore
    public static /* synthetic */ InAppNotification copy$default(InAppNotification inAppNotification, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = inAppNotification.id;
        }
        if ((i & 2) != 0) {
            str2 = inAppNotification.title;
        }
        if ((i & 4) != 0) {
            str3 = inAppNotification.content;
        }
        return inAppNotification.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.title;
    }

    @DexIgnore
    public final String component3() {
        return this.content;
    }

    @DexIgnore
    public final InAppNotification copy(String str, String str2, String str3) {
        ee7.b(str, "id");
        ee7.b(str2, "title");
        ee7.b(str3, "content");
        return new InAppNotification(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InAppNotification)) {
            return false;
        }
        InAppNotification inAppNotification = (InAppNotification) obj;
        return ee7.a(this.id, inAppNotification.id) && ee7.a(this.title, inAppNotification.title) && ee7.a(this.content, inAppNotification.content);
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.title;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.content;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setContent(String str) {
        ee7.b(str, "<set-?>");
        this.content = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public String toString() {
        return "InAppNotification(id=" + this.id + ", title=" + this.title + ", content=" + this.content + ")";
    }
}
