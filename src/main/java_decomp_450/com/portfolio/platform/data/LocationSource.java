package com.portfolio.platform.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.b53;
import com.fossil.bi7;
import com.fossil.c53;
import com.fossil.e53;
import com.fossil.ee7;
import com.fossil.f53;
import com.fossil.fb7;
import com.fossil.ik7;
import com.fossil.mb7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.s87;
import com.fossil.se7;
import com.fossil.v6;
import com.fossil.vb7;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.source.local.AddressDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource {
    @DexIgnore
    public static /* final */ int CACHE_THRESHOLD_TIME; // = 60000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ long LOCATION_WAITING_THRESHOLD_TIME; // = 5000;
    @DexIgnore
    public static /* final */ float SIGNIFICANT_DISPLACEMENT; // = 300.0f;
    @DexIgnore
    public static /* final */ float SMALLEST_DISPLACEMENT; // = 50.0f;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ int TWO_MINUTES; // = 120000;
    @DexIgnore
    public AddressDao addressDao;
    @DexIgnore
    public boolean isMonitoringObserved;
    @DexIgnore
    public boolean isSignificantObserved;
    @DexIgnore
    public long lastRetrievedTime; // = -1;
    @DexIgnore
    public Location locationCache;
    @DexIgnore
    public MonitoringLocationCallback mMonitoringCallback; // = new MonitoringLocationCallback();
    @DexIgnore
    public b53 mMonitoringClient;
    @DexIgnore
    public List<LocationListener> mMonitoringLocationListeners; // = new ArrayList();
    @DexIgnore
    public SignificantLocationCallback mSignificantCallback; // = new SignificantLocationCallback();
    @DexIgnore
    public b53 mSignificantClient;
    @DexIgnore
    public float mSignificantDistance; // = 300.0f;
    @DexIgnore
    public List<LocationListener> mSignificantLocationListeners; // = new ArrayList();
    @DexIgnore
    public float mSmallestDisplacement; // = 50.0f;
    @DexIgnore
    public int mTimeInterval; // = 120000;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return LocationSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum ErrorState {
        SUCCESS,
        LOCATION_PERMISSION_OFF,
        BACKGROUND_PERMISSION_OFF,
        LOCATION_SERVICE_OFF,
        UNKNOWN
    }

    @DexIgnore
    public interface LocationListener {
        @DexIgnore
        void onLocationResult(Location location);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MonitoringLocationCallback extends c53 {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public MonitoringLocationCallback() {
        }

        @DexIgnore
        @Override // com.fossil.c53
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            ee7.b(locationAvailability, "locationAvailability");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationAvailability isAvailable " + locationAvailability.e());
        }

        @DexIgnore
        @Override // com.fossil.c53
        public void onLocationResult(LocationResult locationResult) {
            Location e = locationResult != null ? locationResult.e() : null;
            if (e != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "MonitoringLocationCallback onLocationResult lastLocation=" + e);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = e;
                for (LocationListener locationListener : LocationSource.this.mMonitoringLocationListeners) {
                    if (locationListener != null) {
                        locationListener.onLocationResult(e);
                    }
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "MonitoringLocationCallback onLocationResult lastLocation is null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result {
        @DexIgnore
        public /* final */ ErrorState errorState;
        @DexIgnore
        public /* final */ boolean fromCache;
        @DexIgnore
        public /* final */ Location location;

        @DexIgnore
        public Result(Location location2, ErrorState errorState2, boolean z) {
            ee7.b(errorState2, "errorState");
            this.location = location2;
            this.errorState = errorState2;
            this.fromCache = z;
        }

        @DexIgnore
        public final ErrorState getErrorState() {
            return this.errorState;
        }

        @DexIgnore
        public final boolean getFromCache() {
            return this.fromCache;
        }

        @DexIgnore
        public final Location getLocation() {
            return this.location;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(Location location2, ErrorState errorState2, boolean z, int i, zd7 zd7) {
            this(location2, errorState2, (i & 4) != 0 ? false : z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SignificantLocationCallback extends c53 {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SignificantLocationCallback() {
        }

        @DexIgnore
        @Override // com.fossil.c53
        public void onLocationResult(LocationResult locationResult) {
            Location e = locationResult != null ? locationResult.e() : null;
            if (e != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "SignificantLocationCallback onLocationResult lastLocation=" + e);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = e;
                for (LocationListener locationListener : LocationSource.this.mSignificantLocationListeners) {
                    if (locationListener != null) {
                        locationListener.onLocationResult(e);
                    }
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "SignificantLocationCallback onLocationResult lastLocation is null");
        }
    }

    /*
    static {
        String simpleName = LocationSource.class.getSimpleName();
        ee7.a((Object) simpleName, "LocationSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public static /* synthetic */ Object getLocation$default(LocationSource locationSource, Context context, boolean z, float f, float f2, int i, fb7 fb7, int i2, Object obj) {
        return locationSource.getLocation(context, z, (i2 & 4) != 0 ? 50.0f : f, (i2 & 8) != 0 ? 300.0f : f2, (i2 & 16) != 0 ? 120000 : i, fb7);
    }

    @DexIgnore
    private final Location isBetterLocation(Location location, Location location2) {
        if (location == null && location2 == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation both newLocation null and currentBestLocation null");
            return null;
        } else if (location != null && location2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "isBetterLocation newLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
            long time = location.getTime() - location2.getTime();
            boolean z = true;
            boolean z2 = time > ((long) this.mTimeInterval);
            boolean z3 = time < ((long) (-this.mTimeInterval));
            boolean z4 = time > 0;
            if (z2) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyNewer");
                return location;
            } else if (z3) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyOlder");
                return location2;
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
                int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
                boolean z5 = accuracy > 0;
                boolean z6 = accuracy < 0;
                if (accuracy <= 200) {
                    z = false;
                }
                boolean isSameProvider = isSameProvider(location.getProvider(), location2.getProvider());
                if (z6) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isMoreAccurate");
                    return location;
                } else if (z4 && !z5) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isLessAccurate=false");
                    return location;
                } else if (!z4 || z || !isSameProvider) {
                    return location2;
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                    return location;
                }
            }
        } else if (location != null) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.d(str4, "isBetterLocation bestLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            return location;
        } else {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("isBetterLocation bestLocation long=");
            if (location2 != null) {
                sb.append(location2.getLongitude());
                sb.append(" lat=");
                sb.append(location2.getLatitude());
                sb.append(" time=");
                sb.append(location2.getTime());
                local5.d(str5, sb.toString());
                return location2;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    private final boolean isSameProvider(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(TAG, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return ee7.a((Object) str, (Object) str2);
    }

    @DexIgnore
    public static /* synthetic */ void observerLocation$default(LocationSource locationSource, Context context, LocationListener locationListener, boolean z, float f, int i, Object obj) {
        if ((i & 8) != 0) {
            f = 50.0f;
        }
        locationSource.observerLocation(context, locationListener, z, f);
    }

    @DexIgnore
    public final AddressDao getAddressDao$app_fossilRelease() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2;
        }
        ee7.d("addressDao");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x013d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.Object getLocation(android.content.Context r9, boolean r10, float r11, float r12, int r13, com.fossil.fb7<? super com.portfolio.platform.data.LocationSource.Result> r14) {
        /*
            r8 = this;
            monitor-enter(r8)
            boolean r0 = r14 instanceof com.portfolio.platform.data.LocationSource$getLocation$Anon1     // Catch:{ all -> 0x014b }
            if (r0 == 0) goto L_0x0015
            r0 = r14
            com.portfolio.platform.data.LocationSource$getLocation$Anon1 r0 = (com.portfolio.platform.data.LocationSource$getLocation$Anon1) r0     // Catch:{ all -> 0x014b }
            int r1 = r0.label     // Catch:{ all -> 0x014b }
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0015
            int r14 = r0.label     // Catch:{ all -> 0x014b }
            int r14 = r14 - r2
            r0.label = r14     // Catch:{ all -> 0x014b }
            goto L_0x001a
        L_0x0015:
            com.portfolio.platform.data.LocationSource$getLocation$Anon1 r0 = new com.portfolio.platform.data.LocationSource$getLocation$Anon1     // Catch:{ all -> 0x014b }
            r0.<init>(r8, r14)     // Catch:{ all -> 0x014b }
        L_0x001a:
            java.lang.Object r14 = r0.result     // Catch:{ all -> 0x014b }
            java.lang.Object r1 = com.fossil.nb7.a()     // Catch:{ all -> 0x014b }
            int r2 = r0.label     // Catch:{ all -> 0x014b }
            r3 = 1
            if (r2 == 0) goto L_0x0044
            if (r2 != r3) goto L_0x003c
            int r9 = r0.I$0     // Catch:{ all -> 0x014b }
            float r9 = r0.F$1     // Catch:{ all -> 0x014b }
            float r9 = r0.F$0     // Catch:{ all -> 0x014b }
            boolean r9 = r0.Z$0     // Catch:{ all -> 0x014b }
            java.lang.Object r9 = r0.L$1     // Catch:{ all -> 0x014b }
            android.content.Context r9 = (android.content.Context) r9     // Catch:{ all -> 0x014b }
            java.lang.Object r9 = r0.L$0     // Catch:{ all -> 0x014b }
            com.portfolio.platform.data.LocationSource r9 = (com.portfolio.platform.data.LocationSource) r9     // Catch:{ all -> 0x014b }
            com.fossil.t87.a(r14)     // Catch:{ all -> 0x014b }
            goto L_0x00d6
        L_0x003c:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ all -> 0x014b }
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)     // Catch:{ all -> 0x014b }
            throw r9     // Catch:{ all -> 0x014b }
        L_0x0044:
            com.fossil.t87.a(r14)     // Catch:{ all -> 0x014b }
            r8.mTimeInterval = r13     // Catch:{ all -> 0x014b }
            r8.mSignificantDistance = r12     // Catch:{ all -> 0x014b }
            r8.mSmallestDisplacement = r11     // Catch:{ all -> 0x014b }
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x014b }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()     // Catch:{ all -> 0x014b }
            java.lang.String r2 = com.portfolio.platform.data.LocationSource.TAG     // Catch:{ all -> 0x014b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x014b }
            r4.<init>()     // Catch:{ all -> 0x014b }
            java.lang.String r5 = "getLocation with timeInterval "
            r4.append(r5)     // Catch:{ all -> 0x014b }
            int r5 = r8.mTimeInterval     // Catch:{ all -> 0x014b }
            r4.append(r5)     // Catch:{ all -> 0x014b }
            java.lang.String r5 = " significantDistance "
            r4.append(r5)     // Catch:{ all -> 0x014b }
            float r5 = r8.mSignificantDistance     // Catch:{ all -> 0x014b }
            r4.append(r5)     // Catch:{ all -> 0x014b }
            java.lang.String r5 = " smallestDisplacement "
            r4.append(r5)     // Catch:{ all -> 0x014b }
            float r5 = r8.mSmallestDisplacement     // Catch:{ all -> 0x014b }
            r4.append(r5)     // Catch:{ all -> 0x014b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x014b }
            r14.d(r2, r4)     // Catch:{ all -> 0x014b }
            android.location.Location r14 = r8.locationCache     // Catch:{ all -> 0x014b }
            if (r14 == 0) goto L_0x00b5
            java.util.Calendar r14 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x014b }
            java.lang.String r2 = "Calendar.getInstance()"
            com.fossil.ee7.a(r14, r2)     // Catch:{ all -> 0x014b }
            long r4 = r14.getTimeInMillis()     // Catch:{ all -> 0x014b }
            long r6 = r8.lastRetrievedTime     // Catch:{ all -> 0x014b }
            long r4 = r4 - r6
            r14 = 60000(0xea60, float:8.4078E-41)
            long r6 = (long) r14     // Catch:{ all -> 0x014b }
            int r14 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r14 >= 0) goto L_0x00b5
            if (r10 != 0) goto L_0x00b5
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x014b }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()     // Catch:{ all -> 0x014b }
            java.lang.String r10 = com.portfolio.platform.data.LocationSource.TAG     // Catch:{ all -> 0x014b }
            java.lang.String r11 = "getLocation from cache"
            r9.d(r10, r11)     // Catch:{ all -> 0x014b }
            com.portfolio.platform.data.LocationSource$Result r9 = new com.portfolio.platform.data.LocationSource$Result     // Catch:{ all -> 0x014b }
            android.location.Location r10 = r8.locationCache     // Catch:{ all -> 0x014b }
            com.portfolio.platform.data.LocationSource$ErrorState r11 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS     // Catch:{ all -> 0x014b }
            r9.<init>(r10, r11, r3)     // Catch:{ all -> 0x014b }
            goto L_0x0149
        L_0x00b5:
            com.fossil.ti7 r14 = com.fossil.qj7.b()     // Catch:{ all -> 0x014b }
            com.portfolio.platform.data.LocationSource$getLocation$result$Anon1 r2 = new com.portfolio.platform.data.LocationSource$getLocation$result$Anon1     // Catch:{ all -> 0x014b }
            r4 = 0
            r2.<init>(r8, r9, r4)     // Catch:{ all -> 0x014b }
            r0.L$0 = r8     // Catch:{ all -> 0x014b }
            r0.L$1 = r9     // Catch:{ all -> 0x014b }
            r0.Z$0 = r10     // Catch:{ all -> 0x014b }
            r0.F$0 = r11     // Catch:{ all -> 0x014b }
            r0.F$1 = r12     // Catch:{ all -> 0x014b }
            r0.I$0 = r13     // Catch:{ all -> 0x014b }
            r0.label = r3     // Catch:{ all -> 0x014b }
            java.lang.Object r14 = com.fossil.vh7.a(r14, r2, r0)     // Catch:{ all -> 0x014b }
            if (r14 != r1) goto L_0x00d5
            monitor-exit(r8)
            return r1
        L_0x00d5:
            r9 = r8
        L_0x00d6:
            r10 = r14
            com.portfolio.platform.data.LocationSource$Result r10 = (com.portfolio.platform.data.LocationSource.Result) r10
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.portfolio.platform.data.LocationSource.TAG
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "getLocation result="
            r13.append(r14)
            com.portfolio.platform.data.LocationSource$ErrorState r14 = r10.getErrorState()
            r13.append(r14)
            java.lang.String r14 = " location="
            r13.append(r14)
            android.location.Location r14 = r10.getLocation()
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            com.portfolio.platform.data.LocationSource$ErrorState r11 = r10.getErrorState()
            com.portfolio.platform.data.LocationSource$ErrorState r12 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            if (r11 == r12) goto L_0x0114
            android.location.Location r11 = r9.locationCache
            if (r11 == 0) goto L_0x0112
            goto L_0x0114
        L_0x0112:
            r9 = r10
            goto L_0x0149
        L_0x0114:
            android.location.Location r10 = r10.getLocation()
            android.location.Location r11 = r9.locationCache
            android.location.Location r1 = r9.isBetterLocation(r10, r11)
            if (r1 == 0) goto L_0x013d
            java.util.Calendar r10 = java.util.Calendar.getInstance()
            java.lang.String r11 = "Calendar.getInstance()"
            com.fossil.ee7.a(r10, r11)
            long r10 = r10.getTimeInMillis()
            r9.lastRetrievedTime = r10
            r9.locationCache = r1
            com.portfolio.platform.data.LocationSource$Result r9 = new com.portfolio.platform.data.LocationSource$Result
            com.portfolio.platform.data.LocationSource$ErrorState r2 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            r3 = 0
            r4 = 4
            r5 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5)
            goto L_0x0149
        L_0x013d:
            com.portfolio.platform.data.LocationSource$Result r9 = new com.portfolio.platform.data.LocationSource$Result
            r1 = 0
            com.portfolio.platform.data.LocationSource$ErrorState r2 = com.portfolio.platform.data.LocationSource.ErrorState.UNKNOWN
            r3 = 0
            r4 = 4
            r5 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5)
        L_0x0149:
            monitor-exit(r8)
            return r9
        L_0x014b:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.LocationSource.getLocation(android.content.Context, boolean, float, float, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object getLocationFromFuseLocationManager(Context context, fb7<? super Result> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        FLogger.INSTANCE.getLocal().d(Companion.getTAG$app_fossilRelease(), "getLocationFromFuseLocationManager");
        se7 se7 = new se7();
        se7.element = null;
        b53 a = e53.a(context);
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.l(0);
        locationRequest.k(0);
        locationRequest.b(100);
        locationRequest.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        new f53.a().a(locationRequest);
        if (v6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || v6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            Result result = new Result(null, ErrorState.LOCATION_PERMISSION_OFF, false, 4, null);
            s87.a aVar = s87.Companion;
            bi7.resumeWith(s87.m60constructorimpl(result));
        } else if (Build.VERSION.SDK_INT >= 29 && v6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0) {
            Result result2 = new Result(null, ErrorState.BACKGROUND_PERMISSION_OFF, false, 4, null);
            s87.a aVar2 = s87.Companion;
            bi7.resumeWith(s87.m60constructorimpl(result2));
        } else if (!LocationUtils.isLocationEnable(context)) {
            Result result3 = new Result(null, ErrorState.LOCATION_SERVICE_OFF, false, 4, null);
            s87.a aVar3 = s87.Companion;
            bi7.resumeWith(s87.m60constructorimpl(result3));
        } else {
            try {
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(se7, a, locationRequest, bi7, null, this, context), 3, null);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = Companion.getTAG$app_fossilRelease();
                local.e(tAG$app_fossilRelease, ".getLocationFromFuseLocationManager(), e1=" + e);
            }
        }
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    public final List<AddressOfWeather> getWeatherAddressList() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2.getAllSavedAddress();
        }
        ee7.d("addressDao");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isObservingLocation(com.portfolio.platform.data.LocationSource.LocationListener r3, boolean r4) {
        /*
            r2 = this;
            java.lang.String r0 = "listener"
            com.fossil.ee7.b(r3, r0)
            r0 = 1
            r1 = 0
            if (r4 == 0) goto L_0x0016
            boolean r4 = r2.isSignificantObserved
            if (r4 == 0) goto L_0x0023
            java.util.List<com.portfolio.platform.data.LocationSource$LocationListener> r4 = r2.mSignificantLocationListeners
            boolean r3 = r4.contains(r3)
            if (r3 == 0) goto L_0x0023
            goto L_0x0024
        L_0x0016:
            boolean r4 = r2.isMonitoringObserved
            if (r4 == 0) goto L_0x0023
            java.util.List<com.portfolio.platform.data.LocationSource$LocationListener> r4 = r2.mMonitoringLocationListeners
            boolean r3 = r4.contains(r3)
            if (r3 == 0) goto L_0x0023
            goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.LocationSource.isObservingLocation(com.portfolio.platform.data.LocationSource$LocationListener, boolean):boolean");
    }

    @DexIgnore
    public final void observerLocation(Context context, LocationListener locationListener, boolean z, float f) {
        ee7.b(context, "context");
        ee7.b(locationListener, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "observeLocation listener " + locationListener + ", isSignificant " + z + " isSignificantObserved " + this.isSignificantObserved + " isMonitoringObserved " + this.isMonitoringObserved + " smallestDisplacement " + f);
        if (!z || !this.isSignificantObserved) {
            if (z || !this.isMonitoringObserved) {
                b53 a = e53.a(context);
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.l(1000);
                locationRequest.k(1000);
                locationRequest.b(100);
                if (z) {
                    f = this.mSignificantDistance;
                }
                locationRequest.a(f);
                new f53.a().a(locationRequest);
                if (v6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || v6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0 || (Build.VERSION.SDK_INT >= 29 && v6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation permission missing");
                } else if (!LocationUtils.isLocationEnable(context)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation location disable");
                } else if (z) {
                    try {
                        FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start significantObserved");
                        this.mSignificantLocationListeners.add(locationListener);
                        a.a(locationRequest, this.mSignificantCallback, Looper.getMainLooper());
                        this.isSignificantObserved = true;
                        this.mSignificantClient = a;
                    } catch (Exception e) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.e(str2, ".getLocationFromFuseLocationManager(), e1=" + e);
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start monitoringObserved");
                    this.mMonitoringLocationListeners.add(locationListener);
                    a.a(locationRequest, this.mMonitoringCallback, Looper.getMainLooper());
                    this.isMonitoringObserved = true;
                    this.mMonitoringClient = a;
                }
            } else if (!this.mMonitoringLocationListeners.contains(locationListener)) {
                this.mMonitoringLocationListeners.add(locationListener);
            }
        } else if (!this.mSignificantLocationListeners.contains(locationListener)) {
            this.mSignificantLocationListeners.add(locationListener);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Object requestLocationUpdates(b53 b53, LocationRequest locationRequest, fb7<? super Location> fb7) {
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        b53.a(locationRequest, new LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(bi7, b53, locationRequest), Looper.getMainLooper());
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    public final void saveWeatherAddress(AddressOfWeather addressOfWeather) {
        ee7.b(addressOfWeather, "addressOfWeather");
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            addressDao2.saveAddress(addressOfWeather);
        } else {
            ee7.d("addressDao");
            throw null;
        }
    }

    @DexIgnore
    public final void setAddressDao$app_fossilRelease(AddressDao addressDao2) {
        ee7.b(addressDao2, "<set-?>");
        this.addressDao = addressDao2;
    }

    @DexIgnore
    public final void unObserverLocation(LocationListener locationListener, boolean z) {
        ee7.b(locationListener, "listener");
        if (z) {
            this.mSignificantLocationListeners.remove(locationListener);
            if (this.isSignificantObserved && this.mSignificantLocationListeners.isEmpty()) {
                FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end significantObserved");
                b53 b53 = this.mSignificantClient;
                if (b53 != null) {
                    b53.a((c53) this.mSignificantCallback);
                }
                this.isSignificantObserved = false;
                return;
            }
            return;
        }
        this.mMonitoringLocationListeners.remove(locationListener);
        if (this.isMonitoringObserved && this.mMonitoringLocationListeners.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end monitoringObserved");
            b53 b532 = this.mMonitoringClient;
            if (b532 != null) {
                b532.a((c53) this.mMonitoringCallback);
            }
            this.isMonitoringObserved = false;
        }
    }
}
