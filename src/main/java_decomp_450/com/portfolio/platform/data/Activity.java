package com.portfolio.platform.data;

import com.fossil.be4;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Activity extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "Activity";
    @DexIgnore
    @te4(SampleDay.COLUMN_ACTIVE_TIME)
    public /* final */ int activeTime;
    @DexIgnore
    @te4("calories")
    public /* final */ double calories;
    @DexIgnore
    @te4("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @te4("date")
    public /* final */ Date date;
    @DexIgnore
    @te4("distance")
    public /* final */ double distance;
    @DexIgnore
    @te4(SampleRaw.COLUMN_END_TIME)
    public /* final */ DateTime endTime;
    @DexIgnore
    @te4("id")
    public /* final */ String id;
    @DexIgnore
    @te4("intensityDistInSteps")
    public /* final */ ActivityIntensities intensityDistInSteps;
    @DexIgnore
    @te4(SampleRaw.COLUMN_SOURCE_ID)
    public /* final */ String sourceId;
    @DexIgnore
    @te4(SampleRaw.COLUMN_START_TIME)
    public /* final */ DateTime startTime;
    @DexIgnore
    @te4("steps")
    public /* final */ int steps;
    @DexIgnore
    @te4("syncTime")
    public /* final */ DateTime syncTime;
    @DexIgnore
    @te4("timezoneOffset")
    public /* final */ int timezoneOffset;
    @DexIgnore
    @te4("uid")
    public /* final */ String uid;
    @DexIgnore
    @te4("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Gson gsonConverter() {
            be4 be4 = new be4();
            be4.a(Date.class, new GsonConverterShortDate());
            be4.a(DateTime.class, new GsonConvertDateTime());
            Gson a = be4.a();
            if (a != null) {
                return a;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public final Activity toActivity(String str, ActivitySample activitySample) {
            ee7.b(str, "uid");
            ee7.b(activitySample, "sample");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(Activity.TAG, "toActivity - sample=" + activitySample);
            DateTime dateTime = new DateTime(activitySample.getSyncTime(), DateTimeZone.UTC);
            DateTime dateTime2 = new DateTime(activitySample.getCreatedAt(), DateTimeZone.UTC);
            DateTime dateTime3 = new DateTime(activitySample.getUpdatedAt(), DateTimeZone.UTC);
            return new Activity(str + activitySample.getId(), str, activitySample.getDate(), activitySample.getStartTime(), activitySample.getEndTime(), (int) activitySample.getSteps(), activitySample.getCalories(), activitySample.getDistance(), activitySample.getActiveTime(), activitySample.getIntensityDistInSteps(), activitySample.getTimeZoneOffsetInSecond(), activitySample.getSourceId(), dateTime, dateTime2, dateTime3);
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public Activity(String str, String str2, Date date2, DateTime dateTime, DateTime dateTime2, int i, double d, double d2, int i2, ActivityIntensities activityIntensities, int i3, String str3, DateTime dateTime3, DateTime dateTime4, DateTime dateTime5) {
        ee7.b(str, "id");
        ee7.b(str2, "uid");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(activityIntensities, "intensityDistInSteps");
        ee7.b(str3, SampleRaw.COLUMN_SOURCE_ID);
        ee7.b(dateTime4, "createdAt");
        ee7.b(dateTime5, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.steps = i;
        this.calories = d;
        this.distance = d2;
        this.activeTime = i2;
        this.intensityDistInSteps = activityIntensities;
        this.timezoneOffset = i3;
        this.sourceId = str3;
        this.syncTime = dateTime3;
        this.createdAt = dateTime4;
        this.updatedAt = dateTime5;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        long j;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "toActivitySample - id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", syncTime=" + this.syncTime);
        DateTimeZone forOffsetMillis = DateTimeZone.forOffsetMillis(this.timezoneOffset * 1000);
        String str = this.uid;
        Date date2 = this.date;
        DateTime withZone = this.startTime.withZone(forOffsetMillis);
        ee7.a((Object) withZone, "startTime.withZone(timeZone)");
        DateTime withZone2 = this.endTime.withZone(forOffsetMillis);
        ee7.a((Object) withZone2, "endTime.withZone(timeZone)");
        double d = (double) this.steps;
        double d2 = this.calories;
        double d3 = this.distance;
        int i = this.activeTime;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int i2 = this.timezoneOffset;
        String str2 = this.sourceId;
        DateTime dateTime = this.syncTime;
        if (dateTime != null) {
            j = dateTime.getMillis();
        } else {
            j = this.createdAt.getMillis();
        }
        ActivitySample activitySample = new ActivitySample(str, date2, withZone, withZone2, d, d2, d3, i, activityIntensities, i2, str2, j, this.createdAt.getMillis(), this.updatedAt.getMillis());
        activitySample.setId(this.id);
        return activitySample;
    }

    @DexIgnore
    public final String toJsonString() {
        FLogger.INSTANCE.getLocal().d(TAG, "toJsonString");
        return toJsonString(Companion.gsonConverter());
    }

    @DexIgnore
    public String toString() {
        return "[Activity: id='" + this.id + "', uid='" + this.uid + "', date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", " + "steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", " + "intensityDistInSteps=" + this.intensityDistInSteps + ", timezoneOffset=" + this.timezoneOffset + ", " + "sourceId='" + this.sourceId + "', syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")]";
    }

    @DexIgnore
    public final synchronized String toJsonString(Gson gson) {
        String str;
        ee7.b(gson, "gson");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "toJsonString - gson=" + gson);
        str = "";
        try {
            String a = gson.a(this);
            ee7.a((Object) a, "gson.toJson(this)");
            str = a;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e(TAG, "toJsonString - e=" + e);
        }
        return str;
    }
}
