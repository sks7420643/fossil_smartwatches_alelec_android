package com.portfolio.platform.data.model.room;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDatabase_Impl extends UserDatabase {
    @DexIgnore
    public volatile UserDao _userDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends ei.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `user` (`pinType` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `uid` TEXT NOT NULL, `authType` TEXT NOT NULL, `birthday` TEXT NOT NULL, `brand` TEXT NOT NULL, `diagnosticEnabled` INTEGER NOT NULL, `email` TEXT NOT NULL, `emailOptIn` INTEGER NOT NULL, `emailVerified` INTEGER NOT NULL, `firstName` TEXT NOT NULL, `gender` TEXT NOT NULL, `heightInCentimeters` INTEGER NOT NULL, `integrations` TEXT NOT NULL, `lastName` TEXT NOT NULL, `profilePicture` TEXT NOT NULL, `registerDate` TEXT NOT NULL, `registrationComplete` INTEGER NOT NULL, `useDefaultBiometric` INTEGER NOT NULL, `isOnboardingComplete` INTEGER NOT NULL, `useDefaultGoals` INTEGER NOT NULL, `username` TEXT NOT NULL, `averageSleep` INTEGER NOT NULL, `averageStep` INTEGER NOT NULL, `activeDeviceId` TEXT NOT NULL, `weightInGrams` INTEGER NOT NULL, `userAccessToken` TEXT NOT NULL, `refreshToken` TEXT NOT NULL, `accessTokenExpiresAt` TEXT NOT NULL, `accessTokenExpiresIn` INTEGER NOT NULL, `home` TEXT NOT NULL, `work` TEXT NOT NULL, `distanceUnit` TEXT NOT NULL, `weightUnit` TEXT NOT NULL, `heightUnit` TEXT NOT NULL, `temperatureUnit` TEXT NOT NULL, PRIMARY KEY(`uid`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '086181bfc70c5b6f8463fdc232f2ef29')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `user`");
            if (((ci) UserDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) UserDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) UserDatabase_Impl.this).mDatabase = wiVar;
            UserDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) UserDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) UserDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) UserDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(37);
            hashMap.put("pinType", new ti.a("pinType", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("uid", new ti.a("uid", "TEXT", true, 1, null, 1));
            hashMap.put(Constants.PROFILE_KEY_AUTHTYPE, new ti.a(Constants.PROFILE_KEY_AUTHTYPE, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_BIRTHDAY, new ti.a(Constants.PROFILE_KEY_BIRTHDAY, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_BRAND, new ti.a(Constants.PROFILE_KEY_BRAND, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE, new ti.a(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE, "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.EMAIL, new ti.a(Constants.EMAIL, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_EMAIL_OPT_IN, new ti.a(Constants.PROFILE_KEY_EMAIL_OPT_IN, "INTEGER", true, 0, null, 1));
            hashMap.put("emailVerified", new ti.a("emailVerified", "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_FIRST_NAME, new ti.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", true, 0, null, 1));
            hashMap.put("gender", new ti.a("gender", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_HEIGHT_IN_CM, new ti.a(Constants.PROFILE_KEY_HEIGHT_IN_CM, "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_INTEGRATIONS, new ti.a(Constants.PROFILE_KEY_INTEGRATIONS, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_LAST_NAME, new ti.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_PROFILE_PIC, new ti.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REGISTER_DATE, new ti.a(Constants.PROFILE_KEY_REGISTER_DATE, "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REGISTRATION_COMPLETE, new ti.a(Constants.PROFILE_KEY_REGISTRATION_COMPLETE, "INTEGER", true, 0, null, 1));
            hashMap.put("useDefaultBiometric", new ti.a("useDefaultBiometric", "INTEGER", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE, new ti.a(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE, "INTEGER", true, 0, null, 1));
            hashMap.put("useDefaultGoals", new ti.a("useDefaultGoals", "INTEGER", true, 0, null, 1));
            hashMap.put("username", new ti.a("username", "TEXT", true, 0, null, 1));
            hashMap.put("averageSleep", new ti.a("averageSleep", "INTEGER", true, 0, null, 1));
            hashMap.put("averageStep", new ti.a("averageStep", "INTEGER", true, 0, null, 1));
            hashMap.put("activeDeviceId", new ti.a("activeDeviceId", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS, new ti.a(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS, "INTEGER", true, 0, null, 1));
            hashMap.put("userAccessToken", new ti.a("userAccessToken", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_REFRESH_TOKEN, new ti.a(Constants.PROFILE_KEY_REFRESH_TOKEN, "TEXT", true, 0, null, 1));
            hashMap.put("accessTokenExpiresAt", new ti.a("accessTokenExpiresAt", "TEXT", true, 0, null, 1));
            hashMap.put("accessTokenExpiresIn", new ti.a("accessTokenExpiresIn", "INTEGER", true, 0, null, 1));
            hashMap.put("home", new ti.a("home", "TEXT", true, 0, null, 1));
            hashMap.put("work", new ti.a("work", "TEXT", true, 0, null, 1));
            hashMap.put("distanceUnit", new ti.a("distanceUnit", "TEXT", true, 0, null, 1));
            hashMap.put("weightUnit", new ti.a("weightUnit", "TEXT", true, 0, null, 1));
            hashMap.put("heightUnit", new ti.a("heightUnit", "TEXT", true, 0, null, 1));
            hashMap.put("temperatureUnit", new ti.a("temperatureUnit", "TEXT", true, 0, null, 1));
            ti tiVar = new ti("user", hashMap, new HashSet(0), new HashSet(0));
            ti a = ti.a(wiVar, "user");
            if (tiVar.equals(a)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "user(com.portfolio.platform.data.model.MFUser).\n Expected:\n" + tiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `user`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "user");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new Anon1(5), "086181bfc70c5b6f8463fdc232f2ef29", "1bc666ada9d537e47c26bd62c6cc46c4");
        xi.b.a a = xi.b.a(thVar.b);
        a.a(thVar.c);
        a.a(eiVar);
        return thVar.a.create(a.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDatabase
    public UserDao userDao() {
        UserDao userDao;
        if (this._userDao != null) {
            return this._userDao;
        }
        synchronized (this) {
            if (this._userDao == null) {
                this._userDao = new UserDao_Impl(this);
            }
            userDao = this._userDao;
        }
        return userDao;
    }
}
