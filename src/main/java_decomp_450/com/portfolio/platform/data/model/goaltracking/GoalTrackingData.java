package com.portfolio.platform.data.model.goaltracking;

import com.fossil.c;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingData implements Serializable {
    @DexIgnore
    @te4("createdAt")
    public long createdAt;
    @DexIgnore
    @te4("date")
    public Date date;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @te4("timezoneOffset")
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    @te4(GoalTrackingEvent.COLUMN_TRACKED_AT)
    public /* final */ DateTime trackedAt;
    @DexIgnore
    @te4("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingData(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        ee7.b(str, "id");
        ee7.b(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        ee7.b(date2, "date");
        this.id = str;
        this.trackedAt = dateTime;
        this.timezoneOffsetInSecond = i;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingData copy$default(GoalTrackingData goalTrackingData, String str, DateTime dateTime, int i, Date date2, long j, long j2, int i2, Object obj) {
        return goalTrackingData.copy((i2 & 1) != 0 ? goalTrackingData.id : str, (i2 & 2) != 0 ? goalTrackingData.trackedAt : dateTime, (i2 & 4) != 0 ? goalTrackingData.timezoneOffsetInSecond : i, (i2 & 8) != 0 ? goalTrackingData.date : date2, (i2 & 16) != 0 ? goalTrackingData.createdAt : j, (i2 & 32) != 0 ? goalTrackingData.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.trackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.date;
    }

    @DexIgnore
    public final long component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingData copy(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        ee7.b(str, "id");
        ee7.b(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        ee7.b(date2, "date");
        return new GoalTrackingData(str, dateTime, i, date2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GoalTrackingData)) {
            return false;
        }
        GoalTrackingData goalTrackingData = (GoalTrackingData) obj;
        return ee7.a(this.id, goalTrackingData.id) && ee7.a(this.trackedAt, goalTrackingData.trackedAt) && this.timezoneOffsetInSecond == goalTrackingData.timezoneOffsetInSecond && ee7.a(this.date, goalTrackingData.date) && this.createdAt == goalTrackingData.createdAt && this.updatedAt == goalTrackingData.updatedAt;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        DateTime dateTime = this.trackedAt;
        int hashCode2 = (((hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31;
        Date date2 = this.date;
        if (date2 != null) {
            i = date2.hashCode();
        }
        return ((((hashCode2 + i) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt);
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        ee7.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingData(id=" + this.id + ", trackedAt=" + this.trackedAt + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
