package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.se4;
import com.portfolio.platform.gson.DateTimeSerializer;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GpsDataPointWrapper {
    @DexIgnore
    public /* final */ Double altitude;
    @DexIgnore
    @se4(DateTimeSerializer.class)
    public /* final */ DateTime at;
    @DexIgnore
    public /* final */ Double heading;
    @DexIgnore
    public /* final */ Float horizontalAccuracy;
    @DexIgnore
    public double latitude;
    @DexIgnore
    public /* final */ double longitude;
    @DexIgnore
    public /* final */ Double speed;
    @DexIgnore
    public /* final */ Float verticalAccuracy;

    @DexIgnore
    public GpsDataPointWrapper(Float f, Float f2, double d, double d2, Double d3, Double d4, Double d5, DateTime dateTime) {
        ee7.b(dateTime, "at");
        this.horizontalAccuracy = f;
        this.verticalAccuracy = f2;
        this.latitude = d;
        this.longitude = d2;
        this.altitude = d3;
        this.speed = d4;
        this.heading = d5;
        this.at = dateTime;
    }

    @DexIgnore
    public static /* synthetic */ GpsDataPointWrapper copy$default(GpsDataPointWrapper gpsDataPointWrapper, Float f, Float f2, double d, double d2, Double d3, Double d4, Double d5, DateTime dateTime, int i, Object obj) {
        return gpsDataPointWrapper.copy((i & 1) != 0 ? gpsDataPointWrapper.horizontalAccuracy : f, (i & 2) != 0 ? gpsDataPointWrapper.verticalAccuracy : f2, (i & 4) != 0 ? gpsDataPointWrapper.latitude : d, (i & 8) != 0 ? gpsDataPointWrapper.longitude : d2, (i & 16) != 0 ? gpsDataPointWrapper.altitude : d3, (i & 32) != 0 ? gpsDataPointWrapper.speed : d4, (i & 64) != 0 ? gpsDataPointWrapper.heading : d5, (i & 128) != 0 ? gpsDataPointWrapper.at : dateTime);
    }

    @DexIgnore
    public final Float component1() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final Float component2() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public final double component3() {
        return this.latitude;
    }

    @DexIgnore
    public final double component4() {
        return this.longitude;
    }

    @DexIgnore
    public final Double component5() {
        return this.altitude;
    }

    @DexIgnore
    public final Double component6() {
        return this.speed;
    }

    @DexIgnore
    public final Double component7() {
        return this.heading;
    }

    @DexIgnore
    public final DateTime component8() {
        return this.at;
    }

    @DexIgnore
    public final GpsDataPointWrapper copy(Float f, Float f2, double d, double d2, Double d3, Double d4, Double d5, DateTime dateTime) {
        ee7.b(dateTime, "at");
        return new GpsDataPointWrapper(f, f2, d, d2, d3, d4, d5, dateTime);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GpsDataPointWrapper)) {
            return false;
        }
        GpsDataPointWrapper gpsDataPointWrapper = (GpsDataPointWrapper) obj;
        return ee7.a(this.horizontalAccuracy, gpsDataPointWrapper.horizontalAccuracy) && ee7.a(this.verticalAccuracy, gpsDataPointWrapper.verticalAccuracy) && Double.compare(this.latitude, gpsDataPointWrapper.latitude) == 0 && Double.compare(this.longitude, gpsDataPointWrapper.longitude) == 0 && ee7.a(this.altitude, gpsDataPointWrapper.altitude) && ee7.a(this.speed, gpsDataPointWrapper.speed) && ee7.a(this.heading, gpsDataPointWrapper.heading) && ee7.a(this.at, gpsDataPointWrapper.at);
    }

    @DexIgnore
    public final Double getAltitude() {
        return this.altitude;
    }

    @DexIgnore
    public final DateTime getAt() {
        return this.at;
    }

    @DexIgnore
    public final Double getHeading() {
        return this.heading;
    }

    @DexIgnore
    public final Float getHorizontalAccuracy() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public final double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public final Double getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final Float getVerticalAccuracy() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public int hashCode() {
        Float f = this.horizontalAccuracy;
        int i = 0;
        int hashCode = (f != null ? f.hashCode() : 0) * 31;
        Float f2 = this.verticalAccuracy;
        int hashCode2 = (((((hashCode + (f2 != null ? f2.hashCode() : 0)) * 31) + Double.doubleToLongBits(this.latitude)) * 31) + Double.doubleToLongBits(this.longitude)) * 31;
        Double d = this.altitude;
        int hashCode3 = (hashCode2 + (d != null ? d.hashCode() : 0)) * 31;
        Double d2 = this.speed;
        int hashCode4 = (hashCode3 + (d2 != null ? d2.hashCode() : 0)) * 31;
        Double d3 = this.heading;
        int hashCode5 = (hashCode4 + (d3 != null ? d3.hashCode() : 0)) * 31;
        DateTime dateTime = this.at;
        if (dateTime != null) {
            i = dateTime.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public String toString() {
        return "GpsDataPointWrapper(horizontalAccuracy=" + this.horizontalAccuracy + ", verticalAccuracy=" + this.verticalAccuracy + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", altitude=" + this.altitude + ", speed=" + this.speed + ", heading=" + this.heading + ", at=" + this.at + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public GpsDataPointWrapper(GpsDataPoint gpsDataPoint, int i) {
        this(Float.valueOf(gpsDataPoint.getHorizontalAccuracy()), Float.valueOf(gpsDataPoint.getVerticalAccuracy()), gpsDataPoint.getLatitude(), gpsDataPoint.getLongitude(), Double.valueOf(gpsDataPoint.getAltitude()), Double.valueOf(gpsDataPoint.getSpeed()), Double.valueOf(gpsDataPoint.getHeading()), new DateTime(((long) gpsDataPoint.getTimestamp()) * 1000, DateTimeZone.forOffsetMillis(i * 1000)));
        ee7.b(gpsDataPoint, "gpsDataPoint");
    }
}
