package com.portfolio.platform.data.model.room.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ButtonMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("button")
    public String button;
    @DexIgnore
    @te4("appId")
    public String microAppId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ButtonMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ButtonMapping createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new ButtonMapping(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ButtonMapping[] newArray(int i) {
            return new ButtonMapping[i];
        }
    }

    @DexIgnore
    public ButtonMapping(String str, String str2) {
        ee7.b(str, "button");
        ee7.b(str2, "microAppId");
        this.button = str;
        this.microAppId = str2;
    }

    @DexIgnore
    public static /* synthetic */ ButtonMapping copy$default(ButtonMapping buttonMapping, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = buttonMapping.button;
        }
        if ((i & 2) != 0) {
            str2 = buttonMapping.microAppId;
        }
        return buttonMapping.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.button;
    }

    @DexIgnore
    public final String component2() {
        return this.microAppId;
    }

    @DexIgnore
    public final ButtonMapping copy(String str, String str2) {
        ee7.b(str, "button");
        ee7.b(str2, "microAppId");
        return new ButtonMapping(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ButtonMapping)) {
            return false;
        }
        ButtonMapping buttonMapping = (ButtonMapping) obj;
        return ee7.a(this.button, buttonMapping.button) && ee7.a(this.microAppId, buttonMapping.microAppId);
    }

    @DexIgnore
    public final String getButton() {
        return this.button;
    }

    @DexIgnore
    public final String getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.button;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.microAppId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setButton(String str) {
        ee7.b(str, "<set-?>");
        this.button = str;
    }

    @DexIgnore
    public final void setMicroAppId(String str) {
        ee7.b(str, "<set-?>");
        this.microAppId = str;
    }

    @DexIgnore
    public String toString() {
        return "ButtonMapping(button=" + this.button + ", microAppId=" + this.microAppId + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.button);
        parcel.writeString(this.microAppId);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ButtonMapping(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r3, r0)
            java.lang.String r0 = r3.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r3 = r3.readString()
            if (r3 == 0) goto L_0x0016
            r1 = r3
        L_0x0016:
            r2.<init>(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.microapp.ButtonMapping.<init>(android.os.Parcel):void");
    }
}
