package com.portfolio.platform.data.model.room.sleep;

import com.fossil.ee7;
import com.fossil.zd7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDistribution {
    @DexIgnore
    public int awake;
    @DexIgnore
    public int deep;
    @DexIgnore
    public int light;

    @DexIgnore
    public SleepDistribution() {
        this(0, 0, 0, 7, null);
    }

    @DexIgnore
    public SleepDistribution(int i, int i2, int i3) {
        this.awake = i;
        this.light = i2;
        this.deep = i3;
    }

    @DexIgnore
    public static /* synthetic */ SleepDistribution copy$default(SleepDistribution sleepDistribution, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = sleepDistribution.awake;
        }
        if ((i4 & 2) != 0) {
            i2 = sleepDistribution.light;
        }
        if ((i4 & 4) != 0) {
            i3 = sleepDistribution.deep;
        }
        return sleepDistribution.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.awake;
    }

    @DexIgnore
    public final int component2() {
        return this.light;
    }

    @DexIgnore
    public final int component3() {
        return this.deep;
    }

    @DexIgnore
    public final SleepDistribution copy(int i, int i2, int i3) {
        return new SleepDistribution(i, i2, i3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepDistribution)) {
            return false;
        }
        SleepDistribution sleepDistribution = (SleepDistribution) obj;
        return this.awake == sleepDistribution.awake && this.light == sleepDistribution.light && this.deep == sleepDistribution.deep;
    }

    @DexIgnore
    public final int[] getArrayDistribution() {
        return new int[]{this.awake, this.light, this.deep};
    }

    @DexIgnore
    public final int getAwake() {
        return this.awake;
    }

    @DexIgnore
    public final int getDeep() {
        return this.deep;
    }

    @DexIgnore
    public final int getLight() {
        return this.light;
    }

    @DexIgnore
    public final int getTotalMinuteBySleepDistribution() {
        return this.awake + this.light + this.deep;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.awake * 31) + this.light) * 31) + this.deep;
    }

    @DexIgnore
    public final void setAwake(int i) {
        this.awake = i;
    }

    @DexIgnore
    public final void setDeep(int i) {
        this.deep = i;
    }

    @DexIgnore
    public final void setLight(int i) {
        this.light = i;
    }

    @DexIgnore
    public String toString() {
        return "SleepDistribution(awake=" + this.awake + ", light=" + this.light + ", deep=" + this.deep + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SleepDistribution(int i, int i2, int i3, int i4, zd7 zd7) {
        this((i4 & 1) != 0 ? 0 : i, (i4 & 2) != 0 ? 0 : i2, (i4 & 4) != 0 ? 0 : i3);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDistribution(List<Integer> list) {
        this(list.get(0).intValue(), list.get(1).intValue(), list.get(2).intValue());
        ee7.b(list, "value");
    }
}
