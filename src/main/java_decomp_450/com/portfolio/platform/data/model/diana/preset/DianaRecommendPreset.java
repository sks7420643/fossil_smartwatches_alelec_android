package com.portfolio.platform.data.model.diana.preset;

import com.fossil.ee7;
import com.fossil.re4;
import com.fossil.se4;
import com.fossil.te4;
import com.fossil.xc5;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendPreset {
    @DexIgnore
    @re4
    @se4(DianaPresetComplicationSettingSerializer.class)
    @te4("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("isDefault")
    public boolean isDefault;
    @DexIgnore
    @re4
    @te4("name")
    public String name;
    @DexIgnore
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @te4("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @re4
    @se4(DianaPresetWatchAppSettingSerializer.class)
    @te4("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore
    public DianaRecommendPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "id");
        ee7.b(str3, "name");
        ee7.b(arrayList, "complications");
        ee7.b(arrayList2, "watchapps");
        ee7.b(str4, "watchFaceId");
        ee7.b(str5, "createdAt");
        ee7.b(str6, "updatedAt");
        this.serialNumber = str;
        this.id = str2;
        this.name = str3;
        this.isDefault = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
        this.createdAt = str5;
        this.updatedAt = str6;
    }

    @DexIgnore
    public static /* synthetic */ DianaRecommendPreset copy$default(DianaRecommendPreset dianaRecommendPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, String str5, String str6, int i, Object obj) {
        return dianaRecommendPreset.copy((i & 1) != 0 ? dianaRecommendPreset.serialNumber : str, (i & 2) != 0 ? dianaRecommendPreset.id : str2, (i & 4) != 0 ? dianaRecommendPreset.name : str3, (i & 8) != 0 ? dianaRecommendPreset.isDefault : z, (i & 16) != 0 ? dianaRecommendPreset.complications : arrayList, (i & 32) != 0 ? dianaRecommendPreset.watchapps : arrayList2, (i & 64) != 0 ? dianaRecommendPreset.watchFaceId : str4, (i & 128) != 0 ? dianaRecommendPreset.createdAt : str5, (i & 256) != 0 ? dianaRecommendPreset.updatedAt : str6);
    }

    @DexIgnore
    public final DianaRecommendPreset clone() {
        return new DianaRecommendPreset(this.serialNumber, this.id, this.name, this.isDefault, xc5.b(this.complications), xc5.c(this.watchapps), this.watchFaceId, this.createdAt, this.updatedAt);
    }

    @DexIgnore
    public final String component1() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isDefault;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final String component8() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DianaRecommendPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, String str5, String str6) {
        ee7.b(str, "serialNumber");
        ee7.b(str2, "id");
        ee7.b(str3, "name");
        ee7.b(arrayList, "complications");
        ee7.b(arrayList2, "watchapps");
        ee7.b(str4, "watchFaceId");
        ee7.b(str5, "createdAt");
        ee7.b(str6, "updatedAt");
        return new DianaRecommendPreset(str, str2, str3, z, arrayList, arrayList2, str4, str5, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaRecommendPreset)) {
            return false;
        }
        DianaRecommendPreset dianaRecommendPreset = (DianaRecommendPreset) obj;
        return ee7.a(this.serialNumber, dianaRecommendPreset.serialNumber) && ee7.a(this.id, dianaRecommendPreset.id) && ee7.a(this.name, dianaRecommendPreset.name) && this.isDefault == dianaRecommendPreset.isDefault && ee7.a(this.complications, dianaRecommendPreset.complications) && ee7.a(this.watchapps, dianaRecommendPreset.watchapps) && ee7.a(this.watchFaceId, dianaRecommendPreset.watchFaceId) && ee7.a(this.createdAt, dianaRecommendPreset.createdAt) && ee7.a(this.updatedAt, dianaRecommendPreset.updatedAt);
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serialNumber;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.id;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode3 + i2) * 31;
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = (i4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = (hashCode4 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        String str4 = this.watchFaceId;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.createdAt;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.updatedAt;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        ee7.b(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaRecommendPreset(serialNumber=" + this.serialNumber + ", id=" + this.id + ", name=" + this.name + ", isDefault=" + this.isDefault + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
