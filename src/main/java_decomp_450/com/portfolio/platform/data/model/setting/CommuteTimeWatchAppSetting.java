package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("addresses")
    public List<AddressWrapper> addresses;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeWatchAppSetting createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new CommuteTimeWatchAppSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeWatchAppSetting[] newArray(int i) {
            return new CommuteTimeWatchAppSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting() {
        this(null, 1, null);
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting(List<AddressWrapper> list) {
        ee7.b(list, "addresses");
        this.addresses = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CommuteTimeWatchAppSetting copy$default(CommuteTimeWatchAppSetting commuteTimeWatchAppSetting, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = commuteTimeWatchAppSetting.addresses;
        }
        return commuteTimeWatchAppSetting.copy(list);
    }

    @DexIgnore
    public final List<AddressWrapper> component1() {
        return this.addresses;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting copy(List<AddressWrapper> list) {
        ee7.b(list, "addresses");
        return new CommuteTimeWatchAppSetting(list);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof CommuteTimeWatchAppSetting) && ee7.a(this.addresses, ((CommuteTimeWatchAppSetting) obj).addresses);
        }
        return true;
    }

    @DexIgnore
    public final AddressWrapper getAddressByName(String str) {
        ee7.b(str, "name");
        for (AddressWrapper addressWrapper : this.addresses) {
            if (ee7.a((Object) addressWrapper.getName(), (Object) str)) {
                return addressWrapper;
            }
        }
        return null;
    }

    @DexIgnore
    public final List<AddressWrapper> getAddresses() {
        return this.addresses;
    }

    @DexIgnore
    public final ArrayList<String> getListAddressNameExceptOf(AddressWrapper addressWrapper) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (AddressWrapper addressWrapper2 : this.addresses) {
            if (!ee7.a((Object) addressWrapper2.getId(), (Object) (addressWrapper != null ? addressWrapper.getId() : null))) {
                arrayList.add(addressWrapper2.getName());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        List<AddressWrapper> list = this.addresses;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setAddresses(List<AddressWrapper> list) {
        ee7.b(list, "<set-?>");
        this.addresses = list;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeWatchAppSetting(addresses=" + this.addresses + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeList(this.addresses);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CommuteTimeWatchAppSetting(java.util.List r4, int r5, com.fossil.zd7 r6) {
        /*
            r3 = this;
            r6 = 1
            r5 = r5 & r6
            if (r5 == 0) goto L_0x002a
            r4 = 2
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper[] r4 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper[r4]
            r5 = 0
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r0 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r1 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.HOME
            java.lang.String r1 = r1.getValue()
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r2 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.HOME
            r0.<init>(r1, r2)
            r4[r5] = r0
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r5 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r0 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.WORK
            java.lang.String r0 = r0.getValue()
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r1 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.WORK
            r5.<init>(r0, r1)
            r4[r6] = r5
            java.util.List r4 = com.fossil.w97.d(r4)
        L_0x002a:
            r3.<init>(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.<init>(java.util.List, int, com.fossil.zd7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommuteTimeWatchAppSetting(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r3, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Class<com.portfolio.platform.data.model.diana.commutetime.AddressWrapper> r1 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            r3.readList(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.<init>(android.os.Parcel):void");
    }
}
