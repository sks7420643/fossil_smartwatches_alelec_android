package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("commuteAddress")
    public String address;
    @DexIgnore
    @te4("commuteAvoidTolls")
    public boolean avoidTolls;
    @DexIgnore
    @te4("commuteFormat")
    public String format;
    @DexIgnore
    @te4("commuteMovement")
    public String movement;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new CommuteTimeSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting[] newArray(int i) {
            return new CommuteTimeSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeSetting() {
        this(null, null, false, null, 15, null);
    }

    @DexIgnore
    public CommuteTimeSetting(String str, String str2, boolean z, String str3) {
        ee7.b(str, "address");
        ee7.b(str2, "format");
        ee7.b(str3, "movement");
        this.address = str;
        this.format = str2;
        this.avoidTolls = z;
        this.movement = str3;
    }

    @DexIgnore
    public static /* synthetic */ CommuteTimeSetting copy$default(CommuteTimeSetting commuteTimeSetting, String str, String str2, boolean z, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = commuteTimeSetting.address;
        }
        if ((i & 2) != 0) {
            str2 = commuteTimeSetting.format;
        }
        if ((i & 4) != 0) {
            z = commuteTimeSetting.avoidTolls;
        }
        if ((i & 8) != 0) {
            str3 = commuteTimeSetting.movement;
        }
        return commuteTimeSetting.copy(str, str2, z, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.address;
    }

    @DexIgnore
    public final String component2() {
        return this.format;
    }

    @DexIgnore
    public final boolean component3() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String component4() {
        return this.movement;
    }

    @DexIgnore
    public final CommuteTimeSetting copy(String str, String str2, boolean z, String str3) {
        ee7.b(str, "address");
        ee7.b(str2, "format");
        ee7.b(str3, "movement");
        return new CommuteTimeSetting(str, str2, z, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CommuteTimeSetting)) {
            return false;
        }
        CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) obj;
        return ee7.a(this.address, commuteTimeSetting.address) && ee7.a(this.format, commuteTimeSetting.format) && this.avoidTolls == commuteTimeSetting.avoidTolls && ee7.a(this.movement, commuteTimeSetting.movement);
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final boolean getAvoidTolls() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String getFormat() {
        return this.format;
    }

    @DexIgnore
    public final String getMovement() {
        return this.movement;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.address;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.format;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z = this.avoidTolls;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode2 + i2) * 31;
        String str3 = this.movement;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return i4 + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        ee7.b(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setAvoidTolls(boolean z) {
        this.avoidTolls = z;
    }

    @DexIgnore
    public final void setFormat(String str) {
        ee7.b(str, "<set-?>");
        this.format = str;
    }

    @DexIgnore
    public final void setMovement(String str) {
        ee7.b(str, "<set-?>");
        this.movement = str;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeSetting(address=" + this.address + ", format=" + this.format + ", avoidTolls=" + this.avoidTolls + ", movement=" + this.movement + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.address);
        parcel.writeString(this.format);
        parcel.writeByte(this.avoidTolls ? (byte) 1 : 0);
        parcel.writeString(this.movement);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CommuteTimeSetting(String str, String str2, boolean z, String str3, int i, zd7 zd7) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "travel" : str2, (i & 4) != 0 ? true : z, (i & 8) != 0 ? "car" : str3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommuteTimeSetting(android.os.Parcel r7) {
        /*
            r6 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r7, r0)
            java.lang.String r0 = r7.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r2 = r7.readString()
            if (r2 == 0) goto L_0x0016
            goto L_0x0017
        L_0x0016:
            r2 = r1
        L_0x0017:
            byte r3 = r7.readByte()
            r4 = 0
            byte r5 = (byte) r4
            if (r3 == r5) goto L_0x0020
            r4 = 1
        L_0x0020:
            java.lang.String r7 = r7.readString()
            if (r7 == 0) goto L_0x0027
            r1 = r7
        L_0x0027:
            r6.<init>(r0, r2, r4, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeSetting.<init>(android.os.Parcel):void");
    }
}
