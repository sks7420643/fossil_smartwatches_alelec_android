package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.fitness.Distance;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DistanceWrapperKt {
    @DexIgnore
    public static final DistanceWrapper toDistanceWrapper(Distance distance) {
        if (distance == null) {
            return null;
        }
        int resolutionInSecond = distance.getResolutionInSecond();
        ArrayList<Double> values = distance.getValues();
        ee7.a((Object) values, "this.values");
        return new DistanceWrapper(resolutionInSecond, values, distance.getTotal());
    }
}
