package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.r87;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.x97;
import com.fossil.zd5;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataWrapperKt {
    @DexIgnore
    public static final r87<Date, Date> calculateRangeDownload(List<FitnessDataWrapper> list, Date date, Date date2) {
        ee7.b(list, "$this$calculateRangeDownload");
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new r87<>(date, date2);
        }
        if (!zd5.d(((FitnessDataWrapper) ea7.f((List) list)).getStartTimeTZ().toDate(), date2)) {
            return new r87<>(((FitnessDataWrapper) ea7.f((List) list)).getStartTimeTZ().toDate(), date2);
        }
        if (zd5.d(((FitnessDataWrapper) ea7.d((List) list)).getStartTimeTZ().toDate(), date)) {
            return null;
        }
        return new r87<>(date, zd5.p(((FitnessDataWrapper) ea7.d((List) list)).getStartTimeTZ().toDate()));
    }

    @DexIgnore
    public static final List<ServerFitnessDataWrapper> toServerFitnessDataWrapperList(List<FitnessDataWrapper> list) {
        ee7.b(list, "$this$toServerFitnessDataWrapperList");
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        for (Iterator<T> it = list.iterator(); it.hasNext(); it = it) {
            T next = it.next();
            arrayList.add(new ServerFitnessDataWrapper(next.getStartTime(), next.getEndTime(), next.getSyncTime(), next.getTimezoneOffsetInSecond(), next.getSerialNumber(), next.getStep(), next.getActiveMinute(), next.getCalorie(), next.getDistance(), next.getStress(), next.getResting(), next.getHeartRate(), next.getSleeps(), WorkoutSessionWrapperKt.toServerWorkoutSessionWrapperList(next.getWorkouts()), next.getGoalTrackings()));
        }
        return arrayList;
    }
}
