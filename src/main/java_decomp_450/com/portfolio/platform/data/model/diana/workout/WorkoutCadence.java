package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import com.fossil.sb5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.fitnessdata.CadenceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutCadence {
    @DexIgnore
    public Integer average;
    @DexIgnore
    public Integer maximum;
    @DexIgnore
    public sb5 unit;

    @DexIgnore
    public WorkoutCadence(Integer num, Integer num2, sb5 sb5) {
        ee7.b(sb5, Constants.PROFILE_KEY_UNIT);
        this.average = num;
        this.maximum = num2;
        this.unit = sb5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutCadence copy$default(WorkoutCadence workoutCadence, Integer num, Integer num2, sb5 sb5, int i, Object obj) {
        if ((i & 1) != 0) {
            num = workoutCadence.average;
        }
        if ((i & 2) != 0) {
            num2 = workoutCadence.maximum;
        }
        if ((i & 4) != 0) {
            sb5 = workoutCadence.unit;
        }
        return workoutCadence.copy(num, num2, sb5);
    }

    @DexIgnore
    public final Integer component1() {
        return this.average;
    }

    @DexIgnore
    public final Integer component2() {
        return this.maximum;
    }

    @DexIgnore
    public final sb5 component3() {
        return this.unit;
    }

    @DexIgnore
    public final WorkoutCadence copy(Integer num, Integer num2, sb5 sb5) {
        ee7.b(sb5, Constants.PROFILE_KEY_UNIT);
        return new WorkoutCadence(num, num2, sb5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutCadence)) {
            return false;
        }
        WorkoutCadence workoutCadence = (WorkoutCadence) obj;
        return ee7.a(this.average, workoutCadence.average) && ee7.a(this.maximum, workoutCadence.maximum) && ee7.a(this.unit, workoutCadence.unit);
    }

    @DexIgnore
    public final Integer getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Integer getMaximum() {
        return this.maximum;
    }

    @DexIgnore
    public final sb5 getUnit() {
        return this.unit;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.average;
        int i = 0;
        int hashCode = (num != null ? num.hashCode() : 0) * 31;
        Integer num2 = this.maximum;
        int hashCode2 = (hashCode + (num2 != null ? num2.hashCode() : 0)) * 31;
        sb5 sb5 = this.unit;
        if (sb5 != null) {
            i = sb5.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setAverage(Integer num) {
        this.average = num;
    }

    @DexIgnore
    public final void setMaximum(Integer num) {
        this.maximum = num;
    }

    @DexIgnore
    public final void setUnit(sb5 sb5) {
        ee7.b(sb5, "<set-?>");
        this.unit = sb5;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutCadence(average=" + this.average + ", maximum=" + this.maximum + ", unit=" + this.unit + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutCadence(CadenceWrapper cadenceWrapper) {
        this(cadenceWrapper.getAverage(), cadenceWrapper.getMaximum(), sb5.Companion.a(Integer.valueOf(cadenceWrapper.getUnit())));
        ee7.b(cadenceWrapper, "cadence");
    }
}
