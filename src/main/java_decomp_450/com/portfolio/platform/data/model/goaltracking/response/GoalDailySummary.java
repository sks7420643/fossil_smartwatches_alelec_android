package com.portfolio.platform.data.model.goaltracking.response;

import com.fossil.ee7;
import com.fossil.te4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalDailySummary {
    @DexIgnore
    @te4("createdAt")
    public DateTime mCreatedAt;
    @DexIgnore
    @te4("date")
    public Date mDate;
    @DexIgnore
    @te4("goalTarget")
    public int mGoalTarget;
    @DexIgnore
    @te4("totalTracked")
    public int mTotalTracked;
    @DexIgnore
    @te4("updatedAt")
    public DateTime mUpdatedAt;

    @DexIgnore
    public GoalDailySummary(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        ee7.b(date, "mDate");
        ee7.b(dateTime, "mCreatedAt");
        ee7.b(dateTime2, "mUpdatedAt");
        this.mDate = date;
        this.mTotalTracked = i;
        this.mGoalTarget = i2;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ GoalDailySummary copy$default(GoalDailySummary goalDailySummary, Date date, int i, int i2, DateTime dateTime, DateTime dateTime2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            date = goalDailySummary.mDate;
        }
        if ((i3 & 2) != 0) {
            i = goalDailySummary.mTotalTracked;
        }
        if ((i3 & 4) != 0) {
            i2 = goalDailySummary.mGoalTarget;
        }
        if ((i3 & 8) != 0) {
            dateTime = goalDailySummary.mCreatedAt;
        }
        if ((i3 & 16) != 0) {
            dateTime2 = goalDailySummary.mUpdatedAt;
        }
        return goalDailySummary.copy(date, i, i2, dateTime, dateTime2);
    }

    @DexIgnore
    public final Date component1() {
        return this.mDate;
    }

    @DexIgnore
    public final int component2() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final int component3() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final GoalDailySummary copy(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        ee7.b(date, "mDate");
        ee7.b(dateTime, "mCreatedAt");
        ee7.b(dateTime2, "mUpdatedAt");
        return new GoalDailySummary(date, i, i2, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GoalDailySummary)) {
            return false;
        }
        GoalDailySummary goalDailySummary = (GoalDailySummary) obj;
        return ee7.a(this.mDate, goalDailySummary.mDate) && this.mTotalTracked == goalDailySummary.mTotalTracked && this.mGoalTarget == goalDailySummary.mGoalTarget && ee7.a(this.mCreatedAt, goalDailySummary.mCreatedAt) && ee7.a(this.mUpdatedAt, goalDailySummary.mUpdatedAt);
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final int getMGoalTarget() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final int getMTotalTracked() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.mDate;
        int i = 0;
        int hashCode = (((((date != null ? date.hashCode() : 0) * 31) + this.mTotalTracked) * 31) + this.mGoalTarget) * 31;
        DateTime dateTime = this.mCreatedAt;
        int hashCode2 = (hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.mUpdatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setMCreatedAt(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.mCreatedAt = dateTime;
    }

    @DexIgnore
    public final void setMDate(Date date) {
        ee7.b(date, "<set-?>");
        this.mDate = date;
    }

    @DexIgnore
    public final void setMGoalTarget(int i) {
        this.mGoalTarget = i;
    }

    @DexIgnore
    public final void setMTotalTracked(int i) {
        this.mTotalTracked = i;
    }

    @DexIgnore
    public final void setMUpdatedAt(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.mUpdatedAt = dateTime;
    }

    @DexIgnore
    public final GoalTrackingSummary toGoalTrackingSummary() {
        try {
            return new GoalTrackingSummary(this.mDate, this.mTotalTracked, this.mGoalTarget, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GoalDailySummary", "toGoalTrackingSummary exception=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return "GoalDailySummary(mDate=" + this.mDate + ", mTotalTracked=" + this.mTotalTracked + ", mGoalTarget=" + this.mGoalTarget + ", mCreatedAt=" + this.mCreatedAt + ", mUpdatedAt=" + this.mUpdatedAt + ")";
    }
}
