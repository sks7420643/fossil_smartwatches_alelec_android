package com.portfolio.platform.data.model.goaltracking;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.r87;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zd5;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataKt {
    @DexIgnore
    public static final r87<Date, Date> calculateRangeDownload(List<GoalTrackingData> list, Date date, Date date2) {
        ee7.b(list, "$this$calculateRangeDownload");
        ee7.b(date, GoalPhase.COLUMN_START_DATE);
        ee7.b(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new r87<>(date, date2);
        }
        if (!zd5.d(((GoalTrackingData) ea7.f((List) list)).getDate(), date2)) {
            return new r87<>(((GoalTrackingData) ea7.f((List) list)).getDate(), date2);
        }
        if (zd5.d(((GoalTrackingData) ea7.d((List) list)).getDate(), date)) {
            return null;
        }
        return new r87<>(date, zd5.p(((GoalTrackingData) ea7.d((List) list)).getDate()));
    }
}
