package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutHeartRate {
    @DexIgnore
    public float average;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public WorkoutHeartRate(int i, float f, int i2, int i3, List<Short> list) {
        ee7.b(list, "values");
        this.resolutionInSecond = i;
        this.average = f;
        this.max = i2;
        this.min = i3;
        this.values = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WorkoutHeartRate copy$default(WorkoutHeartRate workoutHeartRate, int i, float f, int i2, int i3, List list, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = workoutHeartRate.resolutionInSecond;
        }
        if ((i4 & 2) != 0) {
            f = workoutHeartRate.average;
        }
        if ((i4 & 4) != 0) {
            i2 = workoutHeartRate.max;
        }
        if ((i4 & 8) != 0) {
            i3 = workoutHeartRate.min;
        }
        if ((i4 & 16) != 0) {
            list = workoutHeartRate.values;
        }
        return workoutHeartRate.copy(i, f, i2, i3, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final float component2() {
        return this.average;
    }

    @DexIgnore
    public final int component3() {
        return this.max;
    }

    @DexIgnore
    public final int component4() {
        return this.min;
    }

    @DexIgnore
    public final List<Short> component5() {
        return this.values;
    }

    @DexIgnore
    public final WorkoutHeartRate copy(int i, float f, int i2, int i3, List<Short> list) {
        ee7.b(list, "values");
        return new WorkoutHeartRate(i, f, i2, i3, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutHeartRate)) {
            return false;
        }
        WorkoutHeartRate workoutHeartRate = (WorkoutHeartRate) obj;
        return this.resolutionInSecond == workoutHeartRate.resolutionInSecond && Float.compare(this.average, workoutHeartRate.average) == 0 && this.max == workoutHeartRate.max && this.min == workoutHeartRate.min && ee7.a(this.values, workoutHeartRate.values);
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = ((((((this.resolutionInSecond * 31) + Float.floatToIntBits(this.average)) * 31) + this.max) * 31) + this.min) * 31;
        List<Short> list = this.values;
        return floatToIntBits + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        ee7.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutHeartRate(resolutionInSecond=" + this.resolutionInSecond + ", average=" + this.average + ", max=" + this.max + ", min=" + this.min + ", values=" + this.values + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutHeartRate(com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r8) {
        /*
            r7 = this;
            java.lang.String r0 = "heartRate"
            com.fossil.ee7.b(r8, r0)
            int r2 = r8.getResolutionInSecond()
            short r0 = r8.getAverage()
            float r3 = (float) r0
            short r4 = r8.getMaximum()
            java.util.List r0 = r8.getValues()
            java.lang.Comparable r0 = com.fossil.ea7.i(r0)
            java.lang.Short r0 = (java.lang.Short) r0
            if (r0 == 0) goto L_0x0024
            short r0 = r0.shortValue()
            r5 = r0
            goto L_0x0026
        L_0x0024:
            r0 = 0
            r5 = 0
        L_0x0026:
            java.util.List r6 = r8.getValues()
            r1 = r7
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate.<init>(com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper):void");
    }
}
