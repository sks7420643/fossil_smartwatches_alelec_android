package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.c;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionWrapper {
    @DexIgnore
    public /* final */ CadenceWrapper cadence;
    @DexIgnore
    public CalorieWrapper calorie;
    @DexIgnore
    public DistanceWrapper distance;
    @DexIgnore
    public int duration;
    @DexIgnore
    @te4("gpsDataPoints")
    public String encodedGpsData;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public /* final */ List<GpsDataPointWrapper> gpsPoints;
    @DexIgnore
    public HeartRateWrapper heartRate;
    @DexIgnore
    public long id;
    @DexIgnore
    public int mode;
    @DexIgnore
    public PaceWrapper pace;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChangeWrapper> stateChanges;
    @DexIgnore
    public StepWrapper step;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public int type;

    @DexIgnore
    public WorkoutSessionWrapper(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<GpsDataPointWrapper> list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(stepWrapper, "step");
        ee7.b(calorieWrapper, "calorie");
        this.id = j;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.type = i3;
        this.mode = i4;
        this.pace = paceWrapper;
        this.cadence = cadenceWrapper;
        this.gpsPoints = list;
        this.step = stepWrapper;
        this.calorie = calorieWrapper;
        this.distance = distanceWrapper;
        this.heartRate = heartRateWrapper;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSessionWrapper copy$default(WorkoutSessionWrapper workoutSessionWrapper, long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper, int i5, Object obj) {
        return workoutSessionWrapper.copy((i5 & 1) != 0 ? workoutSessionWrapper.id : j, (i5 & 2) != 0 ? workoutSessionWrapper.startTime : dateTime, (i5 & 4) != 0 ? workoutSessionWrapper.endTime : dateTime2, (i5 & 8) != 0 ? workoutSessionWrapper.timezoneOffsetInSecond : i, (i5 & 16) != 0 ? workoutSessionWrapper.duration : i2, (i5 & 32) != 0 ? workoutSessionWrapper.type : i3, (i5 & 64) != 0 ? workoutSessionWrapper.mode : i4, (i5 & 128) != 0 ? workoutSessionWrapper.pace : paceWrapper, (i5 & 256) != 0 ? workoutSessionWrapper.cadence : cadenceWrapper, (i5 & 512) != 0 ? workoutSessionWrapper.gpsPoints : list, (i5 & 1024) != 0 ? workoutSessionWrapper.step : stepWrapper, (i5 & 2048) != 0 ? workoutSessionWrapper.calorie : calorieWrapper, (i5 & 4096) != 0 ? workoutSessionWrapper.distance : distanceWrapper, (i5 & 8192) != 0 ? workoutSessionWrapper.heartRate : heartRateWrapper);
    }

    @DexIgnore
    public final long component1() {
        return this.id;
    }

    @DexIgnore
    public final List<GpsDataPointWrapper> component10() {
        return this.gpsPoints;
    }

    @DexIgnore
    public final StepWrapper component11() {
        return this.step;
    }

    @DexIgnore
    public final CalorieWrapper component12() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper component13() {
        return this.distance;
    }

    @DexIgnore
    public final HeartRateWrapper component14() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.endTime;
    }

    @DexIgnore
    public final int component4() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component5() {
        return this.duration;
    }

    @DexIgnore
    public final int component6() {
        return this.type;
    }

    @DexIgnore
    public final int component7() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper component8() {
        return this.pace;
    }

    @DexIgnore
    public final CadenceWrapper component9() {
        return this.cadence;
    }

    @DexIgnore
    public final WorkoutSessionWrapper copy(long j, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, PaceWrapper paceWrapper, CadenceWrapper cadenceWrapper, List<GpsDataPointWrapper> list, StepWrapper stepWrapper, CalorieWrapper calorieWrapper, DistanceWrapper distanceWrapper, HeartRateWrapper heartRateWrapper) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        ee7.b(stepWrapper, "step");
        ee7.b(calorieWrapper, "calorie");
        return new WorkoutSessionWrapper(j, dateTime, dateTime2, i, i2, i3, i4, paceWrapper, cadenceWrapper, list, stepWrapper, calorieWrapper, distanceWrapper, heartRateWrapper);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutSessionWrapper)) {
            return false;
        }
        WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) obj;
        return this.id == workoutSessionWrapper.id && ee7.a(this.startTime, workoutSessionWrapper.startTime) && ee7.a(this.endTime, workoutSessionWrapper.endTime) && this.timezoneOffsetInSecond == workoutSessionWrapper.timezoneOffsetInSecond && this.duration == workoutSessionWrapper.duration && this.type == workoutSessionWrapper.type && this.mode == workoutSessionWrapper.mode && ee7.a(this.pace, workoutSessionWrapper.pace) && ee7.a(this.cadence, workoutSessionWrapper.cadence) && ee7.a(this.gpsPoints, workoutSessionWrapper.gpsPoints) && ee7.a(this.step, workoutSessionWrapper.step) && ee7.a(this.calorie, workoutSessionWrapper.calorie) && ee7.a(this.distance, workoutSessionWrapper.distance) && ee7.a(this.heartRate, workoutSessionWrapper.heartRate);
    }

    @DexIgnore
    public final CadenceWrapper getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final CalorieWrapper getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final DistanceWrapper getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final String getEncodedGpsData() {
        return this.encodedGpsData;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<GpsDataPointWrapper> getGpsPoints() {
        return this.gpsPoints;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final long getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMode() {
        return this.mode;
    }

    @DexIgnore
    public final PaceWrapper getPace() {
        return this.pace;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final StepWrapper getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int a = c.a(this.id) * 31;
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (a + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (((((((((hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31) + this.duration) * 31) + this.type) * 31) + this.mode) * 31;
        PaceWrapper paceWrapper = this.pace;
        int hashCode3 = (hashCode2 + (paceWrapper != null ? paceWrapper.hashCode() : 0)) * 31;
        CadenceWrapper cadenceWrapper = this.cadence;
        int hashCode4 = (hashCode3 + (cadenceWrapper != null ? cadenceWrapper.hashCode() : 0)) * 31;
        List<GpsDataPointWrapper> list = this.gpsPoints;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        StepWrapper stepWrapper = this.step;
        int hashCode6 = (hashCode5 + (stepWrapper != null ? stepWrapper.hashCode() : 0)) * 31;
        CalorieWrapper calorieWrapper = this.calorie;
        int hashCode7 = (hashCode6 + (calorieWrapper != null ? calorieWrapper.hashCode() : 0)) * 31;
        DistanceWrapper distanceWrapper = this.distance;
        int hashCode8 = (hashCode7 + (distanceWrapper != null ? distanceWrapper.hashCode() : 0)) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return hashCode8 + i;
    }

    @DexIgnore
    public final void setCalorie(CalorieWrapper calorieWrapper) {
        ee7.b(calorieWrapper, "<set-?>");
        this.calorie = calorieWrapper;
    }

    @DexIgnore
    public final void setDistance(DistanceWrapper distanceWrapper) {
        this.distance = distanceWrapper;
    }

    @DexIgnore
    public final void setDuration(int i) {
        this.duration = i;
    }

    @DexIgnore
    public final void setEncodedGpsData(String str) {
        this.encodedGpsData = str;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setHeartRate(HeartRateWrapper heartRateWrapper) {
        this.heartRate = heartRateWrapper;
    }

    @DexIgnore
    public final void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public final void setMode(int i) {
        this.mode = i;
    }

    @DexIgnore
    public final void setPace(PaceWrapper paceWrapper) {
        this.pace = paceWrapper;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStateChanges(List<WorkoutStateChangeWrapper> list) {
        ee7.b(list, "<set-?>");
        this.stateChanges = list;
    }

    @DexIgnore
    public final void setStep(StepWrapper stepWrapper) {
        ee7.b(stepWrapper, "<set-?>");
        this.step = stepWrapper;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSessionWrapper(id=" + this.id + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", type=" + this.type + ", mode=" + this.mode + ", pace=" + this.pace + ", cadence=" + this.cadence + ", gpsPoints=" + this.gpsPoints + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSessionWrapper(com.fossil.fitness.WorkoutSession r21) {
        /*
            r20 = this;
            r15 = r20
            java.lang.String r0 = "workoutSession"
            r14 = r21
            com.fossil.ee7.b(r14, r0)
            long r1 = r21.getId()
            org.joda.time.DateTime r3 = new org.joda.time.DateTime
            int r0 = r21.getStartTime()
            long r4 = (long) r0
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r6
            int r0 = r21.getTimezoneOffsetInSecond()
            int r0 = r0 * 1000
            org.joda.time.DateTimeZone r0 = org.joda.time.DateTimeZone.forOffsetMillis(r0)
            r3.<init>(r4, r0)
            org.joda.time.DateTime r4 = new org.joda.time.DateTime
            int r0 = r21.getEndTime()
            long r8 = (long) r0
            long r8 = r8 * r6
            int r0 = r21.getTimezoneOffsetInSecond()
            int r0 = r0 * 1000
            org.joda.time.DateTimeZone r0 = org.joda.time.DateTimeZone.forOffsetMillis(r0)
            r4.<init>(r8, r0)
            int r5 = r21.getTimezoneOffsetInSecond()
            int r6 = r21.getDuration()
            com.fossil.fitness.WorkoutType r0 = r21.getType()
            int r7 = r0.ordinal()
            com.fossil.fitness.WorkoutMode r0 = r21.getMode()
            int r8 = r0.ordinal()
            com.fossil.fitness.Pace r0 = r21.getPace()
            java.lang.String r13 = "it"
            if (r0 == 0) goto L_0x0064
            com.portfolio.platform.data.model.fitnessdata.PaceWrapper r10 = new com.portfolio.platform.data.model.fitnessdata.PaceWrapper
            com.fossil.ee7.a(r0, r13)
            r10.<init>(r0)
            goto L_0x0065
        L_0x0064:
            r10 = 0
        L_0x0065:
            com.fossil.fitness.Cadence r0 = r21.getCadence()
            if (r0 == 0) goto L_0x0074
            com.portfolio.platform.data.model.fitnessdata.CadenceWrapper r11 = new com.portfolio.platform.data.model.fitnessdata.CadenceWrapper
            com.fossil.ee7.a(r0, r13)
            r11.<init>(r0)
            goto L_0x0075
        L_0x0074:
            r11 = 0
        L_0x0075:
            java.util.ArrayList r0 = r21.getGpsDataPoints()
            java.lang.String r12 = "workoutSession.gpsDataPoints"
            com.fossil.ee7.a(r0, r12)
            java.util.ArrayList r12 = new java.util.ArrayList
            r9 = 10
            int r9 = com.fossil.x97.a(r0, r9)
            r12.<init>(r9)
            java.util.Iterator r0 = r0.iterator()
        L_0x008d:
            boolean r9 = r0.hasNext()
            if (r9 == 0) goto L_0x00b1
            java.lang.Object r9 = r0.next()
            com.fossil.fitness.GpsDataPoint r9 = (com.fossil.fitness.GpsDataPoint) r9
            r17 = r0
            com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper r0 = new com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper
            com.fossil.ee7.a(r9, r13)
            r18 = r13
            int r13 = r21.getTimezoneOffsetInSecond()
            r0.<init>(r9, r13)
            r12.add(r0)
            r0 = r17
            r13 = r18
            goto L_0x008d
        L_0x00b1:
            r18 = r13
            java.util.List r12 = com.fossil.ea7.d(r12)
            com.portfolio.platform.data.model.fitnessdata.StepWrapper r13 = new com.portfolio.platform.data.model.fitnessdata.StepWrapper
            com.fossil.fitness.Step r0 = r21.getStep()
            java.lang.String r9 = "workoutSession.step"
            com.fossil.ee7.a(r0, r9)
            r13.<init>(r0)
            com.portfolio.platform.data.model.fitnessdata.CalorieWrapper r9 = new com.portfolio.platform.data.model.fitnessdata.CalorieWrapper
            com.fossil.fitness.Calorie r0 = r21.getCalorie()
            java.lang.String r14 = "workoutSession.calorie"
            com.fossil.ee7.a(r0, r14)
            r9.<init>(r0)
            com.fossil.fitness.Distance r0 = r21.getDistance()
            com.portfolio.platform.data.model.fitnessdata.DistanceWrapper r14 = com.portfolio.platform.data.model.fitnessdata.DistanceWrapperKt.toDistanceWrapper(r0)
            com.fossil.fitness.HeartRate r0 = r21.getHeartrate()
            if (r0 == 0) goto L_0x00f9
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r0 = new com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper
            r17 = r9
            com.fossil.fitness.HeartRate r9 = r21.getHeartrate()
            if (r9 == 0) goto L_0x00f1
            r0.<init>(r9)
            r16 = r0
            goto L_0x00fd
        L_0x00f1:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type com.fossil.fitness.HeartRate"
            r0.<init>(r1)
            throw r0
        L_0x00f9:
            r17 = r9
            r16 = 0
        L_0x00fd:
            r0 = r20
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r19 = r18
            r13 = r17
            r15 = r16
            r0.<init>(r1, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            java.util.ArrayList r0 = r21.getStateChanges()
            java.lang.String r1 = "workoutSession.stateChanges"
            com.fossil.ee7.a(r0, r1)
            java.util.Iterator r0 = r0.iterator()
        L_0x0119:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x014d
            java.lang.Object r1 = r0.next()
            com.fossil.fitness.WorkoutStateChange r1 = (com.fossil.fitness.WorkoutStateChange) r1
            r2 = r20
            java.util.List<com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper> r3 = r2.stateChanges
            com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper r4 = new com.portfolio.platform.data.model.fitnessdata.WorkoutStateChangeWrapper
            com.fossil.yb5$a r5 = com.fossil.yb5.Companion
            r6 = r19
            com.fossil.ee7.a(r1, r6)
            com.fossil.fitness.WorkoutState r7 = r1.getState()
            int r7 = r7.ordinal()
            com.fossil.yb5 r5 = r5.a(r7)
            int r5 = r5.ordinal()
            int r1 = r1.getIndexInSecond()
            r4.<init>(r5, r1)
            r3.add(r4)
            goto L_0x0119
        L_0x014d:
            r2 = r20
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r3 = r0.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r4 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r5 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "[Calculate GPS Points] rawGpsPoint from SDK size "
            r0.append(r1)
            java.util.ArrayList r1 = r21.getGpsDataPoints()
            int r1 = r1.size()
            r0.append(r1)
            r1 = 32
            r0.append(r1)
            java.lang.String r8 = r0.toString()
            java.lang.String r6 = ""
            java.lang.String r7 = "WorkoutSessionWrapper"
            r3.i(r4, r5, r6, r7, r8)
            java.util.ArrayList r0 = r21.getGpsDataPoints()
            r3 = 0
            if (r0 == 0) goto L_0x018e
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x018c
            goto L_0x018e
        L_0x018c:
            r0 = 0
            goto L_0x018f
        L_0x018e:
            r0 = 1
        L_0x018f:
            if (r0 != 0) goto L_0x01d1
            java.util.ArrayList r0 = r21.getGpsDataPoints()
            byte[] r0 = com.fossil.fitness.GpsDataEncoder.encode(r0)
            java.lang.String r4 = "GpsDataEncoder.encode(wo\u2026outSession.gpsDataPoints)"
            com.fossil.ee7.a(r0, r4)
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r3)
            r2.encodedGpsData = r0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r4 = r0.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r5 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r6 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r7 = "[Calculate GPS Points] Encoded GPS data length "
            r0.append(r7)
            java.lang.String r7 = r2.encodedGpsData
            if (r7 == 0) goto L_0x01c0
            int r3 = r7.length()
        L_0x01c0:
            r0.append(r3)
            r0.append(r1)
            java.lang.String r9 = r0.toString()
            java.lang.String r7 = ""
            java.lang.String r8 = "WorkoutSessionWrapper"
            r4.i(r5, r6, r7, r8, r9)
        L_0x01d1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper.<init>(com.fossil.fitness.WorkoutSession):void");
    }
}
