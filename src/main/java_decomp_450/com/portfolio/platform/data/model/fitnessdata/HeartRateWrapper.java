package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateWrapper {
    @DexIgnore
    public short average;
    @DexIgnore
    public short maximum;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public HeartRateWrapper(int i, short s, short s2, List<Short> list) {
        ee7.b(list, "values");
        this.resolutionInSecond = i;
        this.average = s;
        this.maximum = s2;
        this.values = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HeartRateWrapper copy$default(HeartRateWrapper heartRateWrapper, int i, short s, short s2, List list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = heartRateWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            s = heartRateWrapper.average;
        }
        if ((i2 & 4) != 0) {
            s2 = heartRateWrapper.maximum;
        }
        if ((i2 & 8) != 0) {
            list = heartRateWrapper.values;
        }
        return heartRateWrapper.copy(i, s, s2, list);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final short component2() {
        return this.average;
    }

    @DexIgnore
    public final short component3() {
        return this.maximum;
    }

    @DexIgnore
    public final List<Short> component4() {
        return this.values;
    }

    @DexIgnore
    public final HeartRateWrapper copy(int i, short s, short s2, List<Short> list) {
        ee7.b(list, "values");
        return new HeartRateWrapper(i, s, s2, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HeartRateWrapper)) {
            return false;
        }
        HeartRateWrapper heartRateWrapper = (HeartRateWrapper) obj;
        return this.resolutionInSecond == heartRateWrapper.resolutionInSecond && this.average == heartRateWrapper.average && this.maximum == heartRateWrapper.maximum && ee7.a(this.values, heartRateWrapper.values);
    }

    @DexIgnore
    public final short getAverage() {
        return this.average;
    }

    @DexIgnore
    public final short getMaximum() {
        return this.maximum;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = ((((this.resolutionInSecond * 31) + this.average) * 31) + this.maximum) * 31;
        List<Short> list = this.values;
        return i + (list != null ? list.hashCode() : 0);
    }

    @DexIgnore
    public final void setAverage(short s) {
        this.average = s;
    }

    @DexIgnore
    public final void setMaximum(short s) {
        this.maximum = s;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        ee7.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateWrapper(resolutionInSecond=" + this.resolutionInSecond + ", average=" + ((int) this.average) + ", maximum=" + ((int) this.maximum) + ", values=" + this.values + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HeartRateWrapper(com.fossil.fitness.HeartRate r5) {
        /*
            r4 = this;
            java.lang.String r0 = "heartRate"
            com.fossil.ee7.b(r5, r0)
            int r0 = r5.getResolutionInSecond()
            short r1 = r5.getAverage()
            short r2 = r5.getMaximum()
            java.util.ArrayList r5 = r5.getValues()
            java.lang.String r3 = "heartRate.values"
            com.fossil.ee7.a(r5, r3)
            r4.<init>(r0, r1, r2, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper.<init>(com.fossil.fitness.HeartRate):void");
    }
}
