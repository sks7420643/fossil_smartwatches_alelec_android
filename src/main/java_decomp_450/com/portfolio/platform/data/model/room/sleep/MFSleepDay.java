package com.portfolio.platform.data.model.room.sleep;

import com.fossil.ee7;
import com.fossil.ze5;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFSleepDay implements Serializable {
    @DexIgnore
    public Double averageSleepOfWeek; // = Double.valueOf(0.0d);
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public int goalMinutes;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int sleepMinutes;
    @DexIgnore
    public SleepDistribution sleepStateDistInMinute;
    @DexIgnore
    public int timezoneOffset; // = ze5.a();
    @DexIgnore
    public DateTime updatedAt;

    @DexIgnore
    public MFSleepDay(Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2) {
        ee7.b(date2, "date");
        this.date = date2;
        this.goalMinutes = i;
        this.sleepMinutes = i2;
        this.sleepStateDistInMinute = sleepDistribution;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ MFSleepDay copy$default(MFSleepDay mFSleepDay, Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            date2 = mFSleepDay.date;
        }
        if ((i3 & 2) != 0) {
            i = mFSleepDay.goalMinutes;
        }
        if ((i3 & 4) != 0) {
            i2 = mFSleepDay.sleepMinutes;
        }
        if ((i3 & 8) != 0) {
            sleepDistribution = mFSleepDay.sleepStateDistInMinute;
        }
        if ((i3 & 16) != 0) {
            dateTime = mFSleepDay.createdAt;
        }
        if ((i3 & 32) != 0) {
            dateTime2 = mFSleepDay.updatedAt;
        }
        return mFSleepDay.copy(date2, i, i2, sleepDistribution, dateTime, dateTime2);
    }

    @DexIgnore
    public final Date component1() {
        return this.date;
    }

    @DexIgnore
    public final int component2() {
        return this.goalMinutes;
    }

    @DexIgnore
    public final int component3() {
        return this.sleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component4() {
        return this.sleepStateDistInMinute;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final MFSleepDay copy(Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2) {
        ee7.b(date2, "date");
        return new MFSleepDay(date2, i, i2, sleepDistribution, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MFSleepDay)) {
            return false;
        }
        MFSleepDay mFSleepDay = (MFSleepDay) obj;
        return ee7.a(this.date, mFSleepDay.date) && this.goalMinutes == mFSleepDay.goalMinutes && this.sleepMinutes == mFSleepDay.sleepMinutes && ee7.a(this.sleepStateDistInMinute, mFSleepDay.sleepStateDistInMinute) && ee7.a(this.createdAt, mFSleepDay.createdAt) && ee7.a(this.updatedAt, mFSleepDay.updatedAt);
    }

    @DexIgnore
    public final Double getAverageSleepOfWeek() {
        return this.averageSleepOfWeek;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getGoalMinutes() {
        return this.goalMinutes;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getSleepMinutes() {
        return this.sleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getSleepStateDistInMinute() {
        return this.sleepStateDistInMinute;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        Date date2 = this.date;
        int i = 0;
        int hashCode = (((((date2 != null ? date2.hashCode() : 0) * 31) + this.goalMinutes) * 31) + this.sleepMinutes) * 31;
        SleepDistribution sleepDistribution = this.sleepStateDistInMinute;
        int hashCode2 = (hashCode + (sleepDistribution != null ? sleepDistribution.hashCode() : 0)) * 31;
        DateTime dateTime = this.createdAt;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setAverageSleepOfWeek(Double d) {
        this.averageSleepOfWeek = d;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        ee7.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setGoalMinutes(int i) {
        this.goalMinutes = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSleepMinutes(int i) {
        this.sleepMinutes = i;
    }

    @DexIgnore
    public final void setSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        this.sleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepDay(date=" + this.date + ", goalMinutes=" + this.goalMinutes + ", sleepMinutes=" + this.sleepMinutes + ", sleepStateDistInMinute=" + this.sleepStateDistInMinute + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
