package com.portfolio.platform.data.model.room.microapp;

import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.nd5;
import com.fossil.se4;
import com.fossil.te4;
import com.fossil.xc5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.HybridPresetAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    @se4(HybridPresetAppSettingSerializer.class)
    @te4("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("isActive")
    public boolean isActive;
    @DexIgnore
    @te4("name")
    public String name;
    @DexIgnore
    @nd5
    public int pinType;
    @DexIgnore
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final HybridPreset cloneFrom(HybridPreset hybridPreset) {
            ee7.b(hybridPreset, "preset");
            String y = zd5.y(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            ee7.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset2 = new HybridPreset(uuid, ig5.a(PortfolioApp.g0.c(), 2131886533), hybridPreset.getSerialNumber(), hybridPreset.getButtons(), false);
            hybridPreset2.setCreatedAt(y);
            hybridPreset2.setUpdatedAt(y);
            return hybridPreset2;
        }

        @DexIgnore
        public final HybridPreset cloneFromDefault(HybridRecommendPreset hybridRecommendPreset) {
            ee7.b(hybridRecommendPreset, "preset");
            String y = zd5.y(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            ee7.a((Object) uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset = new HybridPreset(uuid, hybridRecommendPreset.getName(), hybridRecommendPreset.getSerialNumber(), hybridRecommendPreset.getButtons(), hybridRecommendPreset.isDefault());
            hybridPreset.setCreatedAt(y);
            hybridPreset.setUpdatedAt(y);
            Iterator<T> it = hybridPreset.getButtons().iterator();
            while (it.hasNext()) {
                ee7.a((Object) y, "timestamp");
                it.next().setLocalUpdateAt(y);
            }
            return hybridPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public HybridPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        ee7.b(str, "id");
        ee7.b(str3, "serialNumber");
        ee7.b(arrayList, "buttons");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isActive = z;
        this.pinType = 1;
        this.createdAt = "";
        this.updatedAt = "";
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPreset */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HybridPreset copy$default(HybridPreset hybridPreset, String str, String str2, String str3, ArrayList arrayList, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = hybridPreset.name;
        }
        if ((i & 4) != 0) {
            str3 = hybridPreset.serialNumber;
        }
        if ((i & 8) != 0) {
            arrayList = hybridPreset.buttons;
        }
        if ((i & 16) != 0) {
            z = hybridPreset.isActive;
        }
        return hybridPreset.copy(str, str2, str3, arrayList, z);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, xc5.a(this.buttons), this.isActive);
        hybridPreset.createdAt = this.createdAt;
        hybridPreset.updatedAt = this.updatedAt;
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isActive;
    }

    @DexIgnore
    public final HybridPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        ee7.b(str, "id");
        ee7.b(str3, "serialNumber");
        ee7.b(arrayList, "buttons");
        return new HybridPreset(str, str2, str3, arrayList, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HybridPreset)) {
            return false;
        }
        HybridPreset hybridPreset = (HybridPreset) obj;
        return ee7.a(this.id, hybridPreset.id) && ee7.a(this.name, hybridPreset.name) && ee7.a(this.serialNumber, hybridPreset.serialNumber) && ee7.a(this.buttons, hybridPreset.buttons) && this.isActive == hybridPreset.isActive;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.serialNumber;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        int i2 = (hashCode3 + i) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isActive=" + this.isActive + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, int i, zd7 zd7) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z);
    }
}
