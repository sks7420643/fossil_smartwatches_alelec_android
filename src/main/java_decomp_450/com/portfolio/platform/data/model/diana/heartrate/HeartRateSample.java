package com.portfolio.platform.data.model.diana.heartrate;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.c;
import com.fossil.ee7;
import com.fossil.nh7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float average;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int minuteCount;
    @DexIgnore
    public Resting resting;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<HeartRateSample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new HeartRateSample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample[] newArray(int i) {
            return new HeartRateSample[i];
        }
    }

    @DexIgnore
    public HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        ee7.b(str, "id");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_START_TIME);
        this.id = str;
        this.average = f;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.endTime = dateTime;
        this.startTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.min = i2;
        this.max = i3;
        this.minuteCount = i4;
        this.resting = resting2;
    }

    @DexIgnore
    public static /* synthetic */ HeartRateSample copy$default(HeartRateSample heartRateSample, String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, Object obj) {
        return heartRateSample.copy((i5 & 1) != 0 ? heartRateSample.id : str, (i5 & 2) != 0 ? heartRateSample.average : f, (i5 & 4) != 0 ? heartRateSample.date : date2, (i5 & 8) != 0 ? heartRateSample.createdAt : j, (i5 & 16) != 0 ? heartRateSample.updatedAt : j2, (i5 & 32) != 0 ? heartRateSample.endTime : dateTime, (i5 & 64) != 0 ? heartRateSample.startTime : dateTime2, (i5 & 128) != 0 ? heartRateSample.timezoneOffsetInSecond : i, (i5 & 256) != 0 ? heartRateSample.min : i2, (i5 & 512) != 0 ? heartRateSample.max : i3, (i5 & 1024) != 0 ? heartRateSample.minuteCount : i4, (i5 & 2048) != 0 ? heartRateSample.resting : resting2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final int component10() {
        return this.max;
    }

    @DexIgnore
    public final int component11() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting component12() {
        return this.resting;
    }

    @DexIgnore
    public final float component2() {
        return this.average;
    }

    @DexIgnore
    public final Date component3() {
        return this.date;
    }

    @DexIgnore
    public final long component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component7() {
        return this.startTime;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component9() {
        return this.min;
    }

    @DexIgnore
    public final HeartRateSample copy(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        ee7.b(str, "id");
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_START_TIME);
        return new HeartRateSample(str, f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, resting2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HeartRateSample)) {
            return false;
        }
        HeartRateSample heartRateSample = (HeartRateSample) obj;
        return ee7.a(this.id, heartRateSample.id) && Float.compare(this.average, heartRateSample.average) == 0 && ee7.a(this.date, heartRateSample.date) && this.createdAt == heartRateSample.createdAt && this.updatedAt == heartRateSample.updatedAt && ee7.a(this.endTime, heartRateSample.endTime) && ee7.a(this.startTime, heartRateSample.startTime) && this.timezoneOffsetInSecond == heartRateSample.timezoneOffsetInSecond && this.min == heartRateSample.min && this.max == heartRateSample.max && this.minuteCount == heartRateSample.minuteCount && ee7.a(this.resting, heartRateSample.resting);
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getMinuteCount() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting getResting() {
        return this.resting;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime getStartTimeId() {
        return this.startTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + Float.floatToIntBits(this.average)) * 31;
        Date date2 = this.date;
        int hashCode2 = (((((hashCode + (date2 != null ? date2.hashCode() : 0)) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt)) * 31;
        DateTime dateTime = this.endTime;
        int hashCode3 = (hashCode2 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.startTime;
        int hashCode4 = (((((((((hashCode3 + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + this.timezoneOffsetInSecond) * 31) + this.min) * 31) + this.max) * 31) + this.minuteCount) * 31;
        Resting resting2 = this.resting;
        if (resting2 != null) {
            i = resting2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        ee7.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setMinuteCount(int i) {
        this.minuteCount = i;
    }

    @DexIgnore
    public final void setResting(Resting resting2) {
        this.resting = resting2;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        ee7.b(dateTime, "value");
        this.startTime = dateTime;
        String str = this.id;
        int b = nh7.b((CharSequence) str, ':', 0, false, 6, (Object) null) + 1;
        if (str != null) {
            String substring = str.substring(0, b);
            ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            this.id = substring + (this.startTime.getMillis() / ((long) 1000));
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "id=" + this.id + ", average=" + this.average + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", endTime=" + this.endTime + ", startTime=" + this.startTime + ", " + "timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", min=" + this.min + ", max=" + this.max + ", minuteCount=" + this.minuteCount + ", resting=" + this.resting;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeFloat(this.average);
        parcel.writeSerializable(this.date);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeSerializable(this.endTime);
        parcel.writeSerializable(this.startTime);
        parcel.writeInt(this.timezoneOffsetInSecond);
        parcel.writeInt(this.min);
        parcel.writeInt(this.max);
        parcel.writeInt(this.minuteCount);
        Resting resting2 = this.resting;
        if (resting2 != null) {
            parcel.writeSerializable(resting2);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, zd7 zd7) {
        this(str, f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2, int i5, zd7 zd7) {
        this(f, date2, j, j2, dateTime, dateTime2, i, i2, i3, str, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2) {
        this(str + ":device:" + (dateTime2.getMillis() / ((long) 1000)), f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, resting2);
        ee7.b(date2, "date");
        ee7.b(dateTime, SampleRaw.COLUMN_END_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_START_TIME);
        ee7.b(str, ButtonService.USER_ID);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HeartRateSample(android.os.Parcel r17) {
        /*
            r16 = this;
            java.lang.String r0 = "parcel"
            r1 = r17
            com.fossil.ee7.b(r1, r0)
            java.lang.String r0 = r17.readString()
            if (r0 == 0) goto L_0x000e
            goto L_0x0010
        L_0x000e:
            java.lang.String r0 = ""
        L_0x0010:
            r2 = r0
            float r3 = r17.readFloat()
            java.io.Serializable r0 = r17.readSerializable()
            if (r0 == 0) goto L_0x0062
            r4 = r0
            java.util.Date r4 = (java.util.Date) r4
            long r5 = r17.readLong()
            long r7 = r17.readLong()
            java.io.Serializable r0 = r17.readSerializable()
            java.lang.String r9 = "null cannot be cast to non-null type org.joda.time.DateTime"
            if (r0 == 0) goto L_0x005c
            org.joda.time.DateTime r0 = (org.joda.time.DateTime) r0
            java.io.Serializable r10 = r17.readSerializable()
            if (r10 == 0) goto L_0x0056
            org.joda.time.DateTime r10 = (org.joda.time.DateTime) r10
            int r11 = r17.readInt()
            int r12 = r17.readInt()
            int r13 = r17.readInt()
            int r14 = r17.readInt()
            java.io.Serializable r1 = r17.readSerializable()
            r15 = r1
            com.portfolio.platform.data.model.diana.heartrate.Resting r15 = (com.portfolio.platform.data.model.diana.heartrate.Resting) r15
            r1 = r16
            r9 = r0
            r1.<init>(r2, r3, r4, r5, r7, r9, r10, r11, r12, r13, r14, r15)
            return
        L_0x0056:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r9)
            throw r0
        L_0x005c:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r9)
            throw r0
        L_0x0062:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type java.util.Date"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.heartrate.HeartRateSample.<init>(android.os.Parcel):void");
    }
}
