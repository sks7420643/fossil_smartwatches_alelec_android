package com.portfolio.platform.data.model.room.microapp;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSetting {
    @DexIgnore
    @te4("appId")
    public String appId;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @te4(Constants.USER_SETTING)
    public String setting;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;

    @DexIgnore
    public MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i) {
        ee7.b(str, "id");
        ee7.b(str2, "appId");
        ee7.b(str4, "createdAt");
        ee7.b(str5, "updatedAt");
        this.id = str;
        this.appId = str2;
        this.setting = str3;
        this.createdAt = str4;
        this.updatedAt = str5;
        this.pinType = i;
    }

    @DexIgnore
    public static /* synthetic */ MicroAppSetting copy$default(MicroAppSetting microAppSetting, String str, String str2, String str3, String str4, String str5, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = microAppSetting.id;
        }
        if ((i2 & 2) != 0) {
            str2 = microAppSetting.appId;
        }
        if ((i2 & 4) != 0) {
            str3 = microAppSetting.setting;
        }
        if ((i2 & 8) != 0) {
            str4 = microAppSetting.createdAt;
        }
        if ((i2 & 16) != 0) {
            str5 = microAppSetting.updatedAt;
        }
        if ((i2 & 32) != 0) {
            i = microAppSetting.pinType;
        }
        return microAppSetting.copy(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final String component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component6() {
        return this.pinType;
    }

    @DexIgnore
    public final MicroAppSetting copy(String str, String str2, String str3, String str4, String str5, int i) {
        ee7.b(str, "id");
        ee7.b(str2, "appId");
        ee7.b(str4, "createdAt");
        ee7.b(str5, "updatedAt");
        return new MicroAppSetting(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public final MicroAppSetting deepCopy() {
        return new MicroAppSetting(this.id, this.appId, this.setting, this.createdAt, this.updatedAt, 0, 32, null);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroAppSetting)) {
            return false;
        }
        MicroAppSetting microAppSetting = (MicroAppSetting) obj;
        return ee7.a(this.id, microAppSetting.id) && ee7.a(this.appId, microAppSetting.appId) && ee7.a(this.setting, microAppSetting.setting) && ee7.a(this.createdAt, microAppSetting.createdAt) && ee7.a(this.updatedAt, microAppSetting.updatedAt) && this.pinType == microAppSetting.pinType;
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.appId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.setting;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.createdAt;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return ((hashCode4 + i) * 31) + this.pinType;
    }

    @DexIgnore
    public final void setAppId(String str) {
        ee7.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSetting(String str) {
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppSetting(id=" + this.id + ", appId=" + this.appId + ", setting=" + this.setting + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i, int i2, zd7 zd7) {
        this(str, str2, str3, str4, str5, (i2 & 32) != 0 ? 1 : i);
    }
}
