package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWODistance {
    @DexIgnore
    public float distance;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;

    @DexIgnore
    public GFitWODistance(float f, long j, long j2) {
        this.distance = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitWODistance copy$default(GFitWODistance gFitWODistance, float f, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            f = gFitWODistance.distance;
        }
        if ((i & 2) != 0) {
            j = gFitWODistance.startTime;
        }
        if ((i & 4) != 0) {
            j2 = gFitWODistance.endTime;
        }
        return gFitWODistance.copy(f, j, j2);
    }

    @DexIgnore
    public final float component1() {
        return this.distance;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitWODistance copy(float f, long j, long j2) {
        return new GFitWODistance(f, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GFitWODistance)) {
            return false;
        }
        GFitWODistance gFitWODistance = (GFitWODistance) obj;
        return Float.compare(this.distance, gFitWODistance.distance) == 0 && this.startTime == gFitWODistance.startTime && this.endTime == gFitWODistance.endTime;
    }

    @DexIgnore
    public final float getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.distance) * 31) + c.a(this.startTime)) * 31) + c.a(this.endTime);
    }

    @DexIgnore
    public final void setDistance(float f) {
        this.distance = f;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public String toString() {
        return "GFitWODistance(distance=" + this.distance + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
