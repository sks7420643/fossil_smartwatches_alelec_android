package com.portfolio.platform.data.model.diana.preset;

import com.fossil.ee7;
import com.fossil.te4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Background {
    @DexIgnore
    @te4("data")
    public Data data;
    @DexIgnore
    @te4("id")
    public String id;

    @DexIgnore
    public Background(String str, Data data2) {
        ee7.b(str, "id");
        ee7.b(data2, "data");
        this.id = str;
        this.data = data2;
    }

    @DexIgnore
    public static /* synthetic */ Background copy$default(Background background, String str, Data data2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = background.id;
        }
        if ((i & 2) != 0) {
            data2 = background.data;
        }
        return background.copy(str, data2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final Background copy(String str, Data data2) {
        ee7.b(str, "id");
        ee7.b(data2, "data");
        return new Background(str, data2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Background)) {
            return false;
        }
        Background background = (Background) obj;
        return ee7.a(this.id, background.id) && ee7.a(this.data, background.data);
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Data data2 = this.data;
        if (data2 != null) {
            i = data2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setData(Data data2) {
        ee7.b(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public String toString() {
        return "Background(id=" + this.id + ", data=" + this.data + ")";
    }
}
