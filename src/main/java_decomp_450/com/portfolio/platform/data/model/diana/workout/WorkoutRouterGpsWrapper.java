package com.portfolio.platform.data.model.diana.workout;

import com.facebook.places.PlaceManager;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutRouterGpsWrapper implements Serializable {
    @DexIgnore
    @te4(Constants.LAT)
    public double lat;
    @DexIgnore
    @te4("lng")
    public /* final */ double lng;
    @DexIgnore
    @te4(PlaceManager.PARAM_SPEED)
    public double speed;
    @DexIgnore
    @te4("timeInterval")
    public /* final */ int timestamp;
    @DexIgnore
    @te4("type")
    public int type;

    @DexIgnore
    public WorkoutRouterGpsWrapper(double d, double d2, int i, double d3, int i2) {
        this.lat = d;
        this.lng = d2;
        this.timestamp = i;
        this.speed = d3;
        this.type = i2;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutRouterGpsWrapper copy$default(WorkoutRouterGpsWrapper workoutRouterGpsWrapper, double d, double d2, int i, double d3, int i2, int i3, Object obj) {
        return workoutRouterGpsWrapper.copy((i3 & 1) != 0 ? workoutRouterGpsWrapper.lat : d, (i3 & 2) != 0 ? workoutRouterGpsWrapper.lng : d2, (i3 & 4) != 0 ? workoutRouterGpsWrapper.timestamp : i, (i3 & 8) != 0 ? workoutRouterGpsWrapper.speed : d3, (i3 & 16) != 0 ? workoutRouterGpsWrapper.type : i2);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final int component3() {
        return this.timestamp;
    }

    @DexIgnore
    public final double component4() {
        return this.speed;
    }

    @DexIgnore
    public final int component5() {
        return this.type;
    }

    @DexIgnore
    public final WorkoutRouterGpsWrapper copy(double d, double d2, int i, double d3, int i2) {
        return new WorkoutRouterGpsWrapper(d, d2, i, d3, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutRouterGpsWrapper)) {
            return false;
        }
        WorkoutRouterGpsWrapper workoutRouterGpsWrapper = (WorkoutRouterGpsWrapper) obj;
        return Double.compare(this.lat, workoutRouterGpsWrapper.lat) == 0 && Double.compare(this.lng, workoutRouterGpsWrapper.lng) == 0 && this.timestamp == workoutRouterGpsWrapper.timestamp && Double.compare(this.speed, workoutRouterGpsWrapper.speed) == 0 && this.type == workoutRouterGpsWrapper.type;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public final double getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final int getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((Double.doubleToLongBits(this.lat) * 31) + Double.doubleToLongBits(this.lng)) * 31) + this.timestamp) * 31) + Double.doubleToLongBits(this.speed)) * 31) + this.type;
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setSpeed(double d) {
        this.speed = d;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutRouterGpsWrapper(lat=" + this.lat + ", lng=" + this.lng + ", timestamp=" + this.timestamp + ", speed=" + this.speed + ", type=" + this.type + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutRouterGpsWrapper(double d, double d2, int i, double d3, int i2, int i3, zd7 zd7) {
        this(d, d2, i, d3, (i3 & 16) != 0 ? 0 : i2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutRouterGpsWrapper(com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint r13) {
        /*
            r12 = this;
            java.lang.String r0 = "gpsDataPoint"
            com.fossil.ee7.b(r13, r0)
            double r2 = r13.getLatitude()
            double r4 = r13.getLongitude()
            org.joda.time.DateTime r0 = r13.getAt()
            long r0 = r0.getMillis()
            r6 = 1000(0x3e8, float:1.401E-42)
            long r6 = (long) r6
            long r0 = r0 / r6
            int r6 = (int) r0
            java.lang.Double r13 = r13.getSpeed()
            if (r13 == 0) goto L_0x0025
            double r0 = r13.doubleValue()
            goto L_0x0027
        L_0x0025:
            r0 = 0
        L_0x0027:
            r7 = r0
            r9 = 0
            r10 = 16
            r11 = 0
            r1 = r12
            r1.<init>(r2, r4, r6, r7, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper.<init>(com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint):void");
    }
}
