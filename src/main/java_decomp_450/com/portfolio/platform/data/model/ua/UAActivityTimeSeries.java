package com.portfolio.platform.data.model.ua;

import com.fossil.de4;
import com.fossil.ee7;
import com.fossil.ie4;
import com.fossil.le4;
import com.fossil.mh7;
import com.fossil.pa7;
import com.fossil.x87;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public ie4 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            ee7.b(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        ee7.b(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final ie4 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final ie4 getJsonData() {
        ie4 ie4 = new ie4();
        ie4.a("external_id", new le4(this.mExternalId));
        ie4 ie42 = new ie4();
        if (this.stepsMap != null) {
            ie4 ie43 = new ie4();
            ie43.a("interval", new le4((Number) 100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ie43.a("values", (JsonElement) gson.a(mh7.a(mh7.a(pa7.d((HashMap) map).toString(), "(", "[", false), ")", "]", false), de4.class));
                ie42.a("steps", ie43);
            } else {
                throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            ie4 ie44 = new ie4();
            ie44.a("interval", new le4((Number) 100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                ie44.a("values", (JsonElement) gson2.a(mh7.a(mh7.a(pa7.d((HashMap) map2).toString(), "(", "[", false), ")", "]", false), de4.class));
                ie42.a("distance", ie44);
            } else {
                throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            ie4 ie45 = new ie4();
            ie45.a("interval", new le4((Number) 100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                ie45.a("values", (JsonElement) gson3.a(mh7.a(mh7.a(pa7.d((HashMap) map3).toString(), "(", "[", false), ")", "]", false), de4.class));
                ie42.a("energy_expended", ie45);
            } else {
                throw new x87("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        ie4.a("time_series", ie42);
        if (this.extraJsonObject != null) {
            de4 de4 = new de4();
            de4.a(this.extraJsonObject);
            ie4 ie46 = new ie4();
            ie46.a("data_source", de4);
            ie4.a("_links", ie46);
        }
        return ie4;
    }

    @DexIgnore
    public final void setExtraJsonObject(ie4 ie4) {
        this.extraJsonObject = ie4;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UAActivityTimeSeries(com.portfolio.platform.data.model.ua.UAActivityTimeSeries.Builder r4) {
        /*
            r3 = this;
            java.lang.String r0 = "builder"
            com.fossil.ee7.b(r4, r0)
            java.lang.String r0 = r4.getMExternalId()
            if (r0 == 0) goto L_0x001b
            java.util.Map r1 = r4.getStepsMap()
            java.util.Map r2 = r4.getDistanceMap()
            java.util.Map r4 = r4.getCaloriesMap()
            r3.<init>(r0, r1, r2, r4)
            return
        L_0x001b:
            com.fossil.ee7.a()
            r4 = 0
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.ua.UAActivityTimeSeries.<init>(com.portfolio.platform.data.model.ua.UAActivityTimeSeries$Builder):void");
    }
}
