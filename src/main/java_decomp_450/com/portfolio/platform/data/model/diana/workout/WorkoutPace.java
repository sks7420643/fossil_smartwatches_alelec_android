package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import com.portfolio.platform.data.model.fitnessdata.PaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutPace {
    @DexIgnore
    public Float average;
    @DexIgnore
    public Float best;

    @DexIgnore
    public WorkoutPace(Float f, Float f2) {
        this.average = f;
        this.best = f2;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutPace copy$default(WorkoutPace workoutPace, Float f, Float f2, int i, Object obj) {
        if ((i & 1) != 0) {
            f = workoutPace.average;
        }
        if ((i & 2) != 0) {
            f2 = workoutPace.best;
        }
        return workoutPace.copy(f, f2);
    }

    @DexIgnore
    public final Float component1() {
        return this.average;
    }

    @DexIgnore
    public final Float component2() {
        return this.best;
    }

    @DexIgnore
    public final WorkoutPace copy(Float f, Float f2) {
        return new WorkoutPace(f, f2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutPace)) {
            return false;
        }
        WorkoutPace workoutPace = (WorkoutPace) obj;
        return ee7.a(this.average, workoutPace.average) && ee7.a(this.best, workoutPace.best);
    }

    @DexIgnore
    public final Float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Float getBest() {
        return this.best;
    }

    @DexIgnore
    public int hashCode() {
        Float f = this.average;
        int i = 0;
        int hashCode = (f != null ? f.hashCode() : 0) * 31;
        Float f2 = this.best;
        if (f2 != null) {
            i = f2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setAverage(Float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setBest(Float f) {
        this.best = f;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutPace(average=" + this.average + ", best=" + this.best + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutPace(PaceWrapper paceWrapper) {
        this(paceWrapper.getAverage(), paceWrapper.getBest());
        ee7.b(paceWrapper, "pace");
    }
}
