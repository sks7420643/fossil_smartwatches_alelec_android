package com.portfolio.platform.data.model.diana.workout;

import com.facebook.places.PlaceManager;
import com.fossil.ee7;
import com.fossil.se4;
import com.fossil.te4;
import com.fossil.zd7;
import com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper;
import com.portfolio.platform.gson.DateTimeSerializer;
import java.io.Serializable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutGpsPoint implements Serializable {
    @DexIgnore
    @te4(PlaceManager.PARAM_ALTITUDE)
    public /* final */ Double altitude;
    @DexIgnore
    @se4(DateTimeSerializer.class)
    @te4("at")
    public /* final */ DateTime at;
    @DexIgnore
    @te4(PlaceManager.PARAM_HEADING)
    public /* final */ Double heading;
    @DexIgnore
    @te4("horizontalAccuracy")
    public /* final */ Float horizontalAccuracy;
    @DexIgnore
    @te4("latitude")
    public double latitude;
    @DexIgnore
    @te4("longitude")
    public /* final */ double longitude;
    @DexIgnore
    @te4(PlaceManager.PARAM_SPEED)
    public /* final */ Double speed;
    @DexIgnore
    @te4("verticalAccuracy")
    public /* final */ Float verticalAccuracy;

    @DexIgnore
    public WorkoutGpsPoint(Float f, double d, double d2, DateTime dateTime, Float f2, Double d3, Double d4, Double d5) {
        ee7.b(dateTime, "at");
        this.horizontalAccuracy = f;
        this.latitude = d;
        this.longitude = d2;
        this.at = dateTime;
        this.verticalAccuracy = f2;
        this.altitude = d3;
        this.speed = d4;
        this.heading = d5;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutGpsPoint copy$default(WorkoutGpsPoint workoutGpsPoint, Float f, double d, double d2, DateTime dateTime, Float f2, Double d3, Double d4, Double d5, int i, Object obj) {
        return workoutGpsPoint.copy((i & 1) != 0 ? workoutGpsPoint.horizontalAccuracy : f, (i & 2) != 0 ? workoutGpsPoint.latitude : d, (i & 4) != 0 ? workoutGpsPoint.longitude : d2, (i & 8) != 0 ? workoutGpsPoint.at : dateTime, (i & 16) != 0 ? workoutGpsPoint.verticalAccuracy : f2, (i & 32) != 0 ? workoutGpsPoint.altitude : d3, (i & 64) != 0 ? workoutGpsPoint.speed : d4, (i & 128) != 0 ? workoutGpsPoint.heading : d5);
    }

    @DexIgnore
    public final Float component1() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double component2() {
        return this.latitude;
    }

    @DexIgnore
    public final double component3() {
        return this.longitude;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.at;
    }

    @DexIgnore
    public final Float component5() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public final Double component6() {
        return this.altitude;
    }

    @DexIgnore
    public final Double component7() {
        return this.speed;
    }

    @DexIgnore
    public final Double component8() {
        return this.heading;
    }

    @DexIgnore
    public final WorkoutGpsPoint copy(Float f, double d, double d2, DateTime dateTime, Float f2, Double d3, Double d4, Double d5) {
        ee7.b(dateTime, "at");
        return new WorkoutGpsPoint(f, d, d2, dateTime, f2, d3, d4, d5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutGpsPoint)) {
            return false;
        }
        WorkoutGpsPoint workoutGpsPoint = (WorkoutGpsPoint) obj;
        return ee7.a(this.horizontalAccuracy, workoutGpsPoint.horizontalAccuracy) && Double.compare(this.latitude, workoutGpsPoint.latitude) == 0 && Double.compare(this.longitude, workoutGpsPoint.longitude) == 0 && ee7.a(this.at, workoutGpsPoint.at) && ee7.a(this.verticalAccuracy, workoutGpsPoint.verticalAccuracy) && ee7.a(this.altitude, workoutGpsPoint.altitude) && ee7.a(this.speed, workoutGpsPoint.speed) && ee7.a(this.heading, workoutGpsPoint.heading);
    }

    @DexIgnore
    public final Double getAltitude() {
        return this.altitude;
    }

    @DexIgnore
    public final DateTime getAt() {
        return this.at;
    }

    @DexIgnore
    public final Double getHeading() {
        return this.heading;
    }

    @DexIgnore
    public final Float getHorizontalAccuracy() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public final double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public final Double getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final Float getVerticalAccuracy() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public int hashCode() {
        Float f = this.horizontalAccuracy;
        int i = 0;
        int hashCode = (((((f != null ? f.hashCode() : 0) * 31) + Double.doubleToLongBits(this.latitude)) * 31) + Double.doubleToLongBits(this.longitude)) * 31;
        DateTime dateTime = this.at;
        int hashCode2 = (hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        Float f2 = this.verticalAccuracy;
        int hashCode3 = (hashCode2 + (f2 != null ? f2.hashCode() : 0)) * 31;
        Double d = this.altitude;
        int hashCode4 = (hashCode3 + (d != null ? d.hashCode() : 0)) * 31;
        Double d2 = this.speed;
        int hashCode5 = (hashCode4 + (d2 != null ? d2.hashCode() : 0)) * 31;
        Double d3 = this.heading;
        if (d3 != null) {
            i = d3.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutGpsPoint(horizontalAccuracy=" + this.horizontalAccuracy + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", at=" + this.at + ", verticalAccuracy=" + this.verticalAccuracy + ", altitude=" + this.altitude + ", speed=" + this.speed + ", heading=" + this.heading + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutGpsPoint(Float f, double d, double d2, DateTime dateTime, Float f2, Double d3, Double d4, Double d5, int i, zd7 zd7) {
        this((i & 1) != 0 ? null : f, d, d2, dateTime, (i & 16) != 0 ? null : f2, (i & 32) != 0 ? null : d3, (i & 64) != 0 ? null : d4, (i & 128) != 0 ? null : d5);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutGpsPoint(GpsDataPointWrapper gpsDataPointWrapper) {
        this(gpsDataPointWrapper.getHorizontalAccuracy(), gpsDataPointWrapper.getLatitude(), gpsDataPointWrapper.getLongitude(), gpsDataPointWrapper.getAt(), gpsDataPointWrapper.getVerticalAccuracy(), gpsDataPointWrapper.getAltitude(), gpsDataPointWrapper.getSpeed(), gpsDataPointWrapper.getHeading());
        ee7.b(gpsDataPointWrapper, "gpsDataPoint");
    }
}
