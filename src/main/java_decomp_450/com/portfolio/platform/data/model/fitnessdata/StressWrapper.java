package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StressWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public List<Byte> values;

    @DexIgnore
    public StressWrapper(DateTime dateTime, int i, int i2, List<Byte> list) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(list, "values");
        this.startTime = dateTime;
        this.timezoneOffsetInSecond = i;
        this.resolutionInSecond = i2;
        this.values = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.portfolio.platform.data.model.fitnessdata.StressWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StressWrapper copy$default(StressWrapper stressWrapper, DateTime dateTime, int i, int i2, List list, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            dateTime = stressWrapper.startTime;
        }
        if ((i3 & 2) != 0) {
            i = stressWrapper.timezoneOffsetInSecond;
        }
        if ((i3 & 4) != 0) {
            i2 = stressWrapper.resolutionInSecond;
        }
        if ((i3 & 8) != 0) {
            list = stressWrapper.values;
        }
        return stressWrapper.copy(dateTime, i, i2, list);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final int component2() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component3() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Byte> component4() {
        return this.values;
    }

    @DexIgnore
    public final StressWrapper copy(DateTime dateTime, int i, int i2, List<Byte> list) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(list, "values");
        return new StressWrapper(dateTime, i, i2, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StressWrapper)) {
            return false;
        }
        StressWrapper stressWrapper = (StressWrapper) obj;
        return ee7.a(this.startTime, stressWrapper.startTime) && this.timezoneOffsetInSecond == stressWrapper.timezoneOffsetInSecond && this.resolutionInSecond == stressWrapper.resolutionInSecond && ee7.a(this.values, stressWrapper.values);
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final List<Byte> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (((((dateTime != null ? dateTime.hashCode() : 0) * 31) + this.timezoneOffsetInSecond) * 31) + this.resolutionInSecond) * 31;
        List<Byte> list = this.values;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        ee7.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setValues(List<Byte> list) {
        ee7.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "StressWrapper(startTime=" + this.startTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ")";
    }
}
