package com.portfolio.platform.data.model.setting;

import com.fossil.ee7;
import com.fossil.te4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalization {
    @DexIgnore
    @te4("category")
    public String category;
    @DexIgnore
    @te4("data")
    public Data data;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("metadata")
    public MetaData metaData;
    @DexIgnore
    @te4("name")
    public String name;

    @DexIgnore
    public WatchLocalization(String str, String str2, String str3, Data data2, MetaData metaData2) {
        ee7.b(str, "id");
        ee7.b(str2, "category");
        ee7.b(str3, "name");
        ee7.b(data2, "data");
        ee7.b(metaData2, "metaData");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
    }

    @DexIgnore
    public static /* synthetic */ WatchLocalization copy$default(WatchLocalization watchLocalization, String str, String str2, String str3, Data data2, MetaData metaData2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = watchLocalization.id;
        }
        if ((i & 2) != 0) {
            str2 = watchLocalization.category;
        }
        if ((i & 4) != 0) {
            str3 = watchLocalization.name;
        }
        if ((i & 8) != 0) {
            data2 = watchLocalization.data;
        }
        if ((i & 16) != 0) {
            metaData2 = watchLocalization.metaData;
        }
        return watchLocalization.copy(str, str2, str3, data2, metaData2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component5() {
        return this.metaData;
    }

    @DexIgnore
    public final WatchLocalization copy(String str, String str2, String str3, Data data2, MetaData metaData2) {
        ee7.b(str, "id");
        ee7.b(str2, "category");
        ee7.b(str3, "name");
        ee7.b(data2, "data");
        ee7.b(metaData2, "metaData");
        return new WatchLocalization(str, str2, str3, data2, metaData2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchLocalization)) {
            return false;
        }
        WatchLocalization watchLocalization = (WatchLocalization) obj;
        return ee7.a(this.id, watchLocalization.id) && ee7.a(this.category, watchLocalization.category) && ee7.a(this.name, watchLocalization.name) && ee7.a(this.data, watchLocalization.data) && ee7.a(this.metaData, watchLocalization.metaData);
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.category;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Data data2 = this.data;
        int hashCode4 = (hashCode3 + (data2 != null ? data2.hashCode() : 0)) * 31;
        MetaData metaData2 = this.metaData;
        if (metaData2 != null) {
            i = metaData2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final void setCategory(String str) {
        ee7.b(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        ee7.b(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        ee7.b(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchLocalization(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metaData=" + this.metaData + ")";
    }
}
