package com.portfolio.platform.data.model;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.ee7;
import com.fossil.nd5;
import com.fossil.nh7;
import com.fossil.te4;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.api.ImageObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFUser {
    @DexIgnore
    @te4("activeDeviceId")
    public String activeDeviceId;
    @DexIgnore
    @te4("addresses")
    public Address addresses;
    @DexIgnore
    public Auth auth;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_AUTHTYPE)
    public String authType;
    @DexIgnore
    @te4("averageSleep")
    public int averageSleep;
    @DexIgnore
    @te4("averageStep")
    public int averageStep;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BIRTHDAY)
    public String birthday;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_BRAND)
    public String brand;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE)
    public boolean diagnosticEnabled;
    @DexIgnore
    @te4(Constants.EMAIL)
    public String email;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_EMAIL_OPT_IN)
    public boolean emailOptIn;
    @DexIgnore
    @te4("emailVerified")
    public boolean emailVerified;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String firstName;
    @DexIgnore
    @te4("gender")
    public String gender;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_HEIGHT_IN_CM)
    public int heightInCentimeters;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_INTEGRATIONS)
    public String integrations;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE)
    public boolean isOnboardingComplete;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String lastName;
    @DexIgnore
    @nd5
    public String pinType;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String profilePicture;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_REGISTER_DATE)
    public String registerDate;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_REGISTRATION_COMPLETE)
    public boolean registrationComplete;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_UNIT_GROUP)
    public UnitGroup unitGroup;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;
    @DexIgnore
    @te4("useDefaultBiometric")
    public boolean useDefaultBiometric;
    @DexIgnore
    @te4("useDefaultGoals")
    public boolean useDefaultGoals;
    @DexIgnore
    @te4("uid")
    public String userId;
    @DexIgnore
    @te4("username")
    public String username;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_WEIGHT_IN_GRAMS)
    public int weightInGrams;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Address {
        @DexIgnore
        @te4("home")
        public String home;
        @DexIgnore
        @te4("work")
        public String work;

        @DexIgnore
        public Address(String str, String str2) {
            ee7.b(str, "home");
            ee7.b(str2, "work");
            this.home = str;
            this.work = str2;
        }

        @DexIgnore
        public static /* synthetic */ Address copy$default(Address address, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = address.home;
            }
            if ((i & 2) != 0) {
                str2 = address.work;
            }
            return address.copy(str, str2);
        }

        @DexIgnore
        public final String component1() {
            return this.home;
        }

        @DexIgnore
        public final String component2() {
            return this.work;
        }

        @DexIgnore
        public final Address copy(String str, String str2) {
            ee7.b(str, "home");
            ee7.b(str2, "work");
            return new Address(str, str2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Address)) {
                return false;
            }
            Address address = (Address) obj;
            return ee7.a(this.home, address.home) && ee7.a(this.work, address.work);
        }

        @DexIgnore
        public final String getHome() {
            return this.home;
        }

        @DexIgnore
        public final String getWork() {
            return this.work;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.home;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.work;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        @DexIgnore
        public final void setHome(String str) {
            ee7.b(str, "<set-?>");
            this.home = str;
        }

        @DexIgnore
        public final void setWork(String str) {
            ee7.b(str, "<set-?>");
            this.work = str;
        }

        @DexIgnore
        public String toString() {
            return "Address(home=" + this.home + ", work=" + this.work + ")";
        }

        @DexIgnore
        public Address() {
            this("", "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Auth {
        @DexIgnore
        @te4(Constants.PROFILE_KEY_ACCESS_TOKEN)
        public String accessToken;
        @DexIgnore
        @te4("accessTokenExpiresAt")
        public String accessTokenExpiresAt;
        @DexIgnore
        @te4("accessTokenExpiresIn")
        public int accessTokenExpiresIn;
        @DexIgnore
        @te4(Constants.PROFILE_KEY_REFRESH_TOKEN)
        public String refreshToken;

        @DexIgnore
        public Auth(String str, String str2, String str3, int i) {
            ee7.b(str, Constants.PROFILE_KEY_ACCESS_TOKEN);
            ee7.b(str2, Constants.PROFILE_KEY_REFRESH_TOKEN);
            ee7.b(str3, "accessTokenExpiresAt");
            this.accessToken = str;
            this.refreshToken = str2;
            this.accessTokenExpiresAt = str3;
            this.accessTokenExpiresIn = i;
        }

        @DexIgnore
        public static /* synthetic */ Auth copy$default(Auth auth, String str, String str2, String str3, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = auth.accessToken;
            }
            if ((i2 & 2) != 0) {
                str2 = auth.refreshToken;
            }
            if ((i2 & 4) != 0) {
                str3 = auth.accessTokenExpiresAt;
            }
            if ((i2 & 8) != 0) {
                i = auth.accessTokenExpiresIn;
            }
            return auth.copy(str, str2, str3, i);
        }

        @DexIgnore
        public final String component1() {
            return this.accessToken;
        }

        @DexIgnore
        public final String component2() {
            return this.refreshToken;
        }

        @DexIgnore
        public final String component3() {
            return this.accessTokenExpiresAt;
        }

        @DexIgnore
        public final int component4() {
            return this.accessTokenExpiresIn;
        }

        @DexIgnore
        public final Auth copy(String str, String str2, String str3, int i) {
            ee7.b(str, Constants.PROFILE_KEY_ACCESS_TOKEN);
            ee7.b(str2, Constants.PROFILE_KEY_REFRESH_TOKEN);
            ee7.b(str3, "accessTokenExpiresAt");
            return new Auth(str, str2, str3, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Auth)) {
                return false;
            }
            Auth auth = (Auth) obj;
            return ee7.a(this.accessToken, auth.accessToken) && ee7.a(this.refreshToken, auth.refreshToken) && ee7.a(this.accessTokenExpiresAt, auth.accessTokenExpiresAt) && this.accessTokenExpiresIn == auth.accessTokenExpiresIn;
        }

        @DexIgnore
        public final String getAccessToken() {
            return this.accessToken;
        }

        @DexIgnore
        public final String getAccessTokenExpiresAt() {
            return this.accessTokenExpiresAt;
        }

        @DexIgnore
        public final int getAccessTokenExpiresIn() {
            return this.accessTokenExpiresIn;
        }

        @DexIgnore
        public final String getRefreshToken() {
            return this.refreshToken;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.accessToken;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.refreshToken;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.accessTokenExpiresAt;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return ((hashCode2 + i) * 31) + this.accessTokenExpiresIn;
        }

        @DexIgnore
        public final void setAccessToken(String str) {
            ee7.b(str, "<set-?>");
            this.accessToken = str;
        }

        @DexIgnore
        public final void setAccessTokenExpiresAt(String str) {
            ee7.b(str, "<set-?>");
            this.accessTokenExpiresAt = str;
        }

        @DexIgnore
        public final void setAccessTokenExpiresIn(int i) {
            this.accessTokenExpiresIn = i;
        }

        @DexIgnore
        public final void setRefreshToken(String str) {
            ee7.b(str, "<set-?>");
            this.refreshToken = str;
        }

        @DexIgnore
        public String toString() {
            return "Auth(accessToken=" + this.accessToken + ", refreshToken=" + this.refreshToken + ", accessTokenExpiresAt=" + this.accessTokenExpiresAt + ", accessTokenExpiresIn=" + this.accessTokenExpiresIn + ")";
        }

        @DexIgnore
        public Auth() {
            this("", "", "", 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class UnitGroup {
        @DexIgnore
        @te4("distance")
        public String distance;
        @DexIgnore
        @te4("height")
        public String height;
        @DexIgnore
        @te4("temperature")
        public String temperature;
        @DexIgnore
        @te4(Constants.PROFILE_KEY_UNITS_WEIGHT)
        public String weight;

        @DexIgnore
        public UnitGroup(String str, String str2, String str3, String str4) {
            ee7.b(str, "distance");
            ee7.b(str2, Constants.PROFILE_KEY_UNITS_WEIGHT);
            ee7.b(str3, "height");
            ee7.b(str4, "temperature");
            this.distance = str;
            this.weight = str2;
            this.height = str3;
            this.temperature = str4;
        }

        @DexIgnore
        public static /* synthetic */ UnitGroup copy$default(UnitGroup unitGroup, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                str = unitGroup.distance;
            }
            if ((i & 2) != 0) {
                str2 = unitGroup.weight;
            }
            if ((i & 4) != 0) {
                str3 = unitGroup.height;
            }
            if ((i & 8) != 0) {
                str4 = unitGroup.temperature;
            }
            return unitGroup.copy(str, str2, str3, str4);
        }

        @DexIgnore
        public final String component1() {
            return this.distance;
        }

        @DexIgnore
        public final String component2() {
            return this.weight;
        }

        @DexIgnore
        public final String component3() {
            return this.height;
        }

        @DexIgnore
        public final String component4() {
            return this.temperature;
        }

        @DexIgnore
        public final UnitGroup copy(String str, String str2, String str3, String str4) {
            ee7.b(str, "distance");
            ee7.b(str2, Constants.PROFILE_KEY_UNITS_WEIGHT);
            ee7.b(str3, "height");
            ee7.b(str4, "temperature");
            return new UnitGroup(str, str2, str3, str4);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UnitGroup)) {
                return false;
            }
            UnitGroup unitGroup = (UnitGroup) obj;
            return ee7.a(this.distance, unitGroup.distance) && ee7.a(this.weight, unitGroup.weight) && ee7.a(this.height, unitGroup.height) && ee7.a(this.temperature, unitGroup.temperature);
        }

        @DexIgnore
        public final String getDistance() {
            return this.distance;
        }

        @DexIgnore
        public final String getHeight() {
            return this.height;
        }

        @DexIgnore
        public final String getTemperature() {
            return this.temperature;
        }

        @DexIgnore
        public final String getWeight() {
            return this.weight;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.distance;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.weight;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.height;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.temperature;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode3 + i;
        }

        @DexIgnore
        public final void setDistance(String str) {
            ee7.b(str, "<set-?>");
            this.distance = str;
        }

        @DexIgnore
        public final void setHeight(String str) {
            ee7.b(str, "<set-?>");
            this.height = str;
        }

        @DexIgnore
        public final void setTemperature(String str) {
            ee7.b(str, "<set-?>");
            this.temperature = str;
        }

        @DexIgnore
        public final void setWeight(String str) {
            ee7.b(str, "<set-?>");
            this.weight = str;
        }

        @DexIgnore
        public String toString() {
            return "UnitGroup(distance=" + this.distance + ", weight=" + this.weight + ", height=" + this.height + ", temperature=" + this.temperature + ")";
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public UnitGroup() {
            /*
                r5 = this;
                com.fossil.ob5 r0 = com.fossil.ob5.IMPERIAL
                java.lang.String r0 = r0.getValue()
                java.lang.String r1 = "Unit.IMPERIAL.value"
                com.fossil.ee7.a(r0, r1)
                com.fossil.ob5 r2 = com.fossil.ob5.IMPERIAL
                java.lang.String r2 = r2.getValue()
                com.fossil.ee7.a(r2, r1)
                com.fossil.ob5 r3 = com.fossil.ob5.IMPERIAL
                java.lang.String r3 = r3.getValue()
                com.fossil.ee7.a(r3, r1)
                com.fossil.ob5 r4 = com.fossil.ob5.IMPERIAL
                java.lang.String r4 = r4.getValue()
                com.fossil.ee7.a(r4, r1)
                r5.<init>(r0, r2, r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.MFUser.UnitGroup.<init>():void");
        }
    }

    @DexIgnore
    public MFUser(String str, Address address, String str2, String str3, String str4, boolean z, String str5, boolean z2, boolean z3, String str6, String str7, int i, String str8, String str9, String str10, String str11, boolean z4, UnitGroup unitGroup2, boolean z5, boolean z6, boolean z7, String str12, int i2, int i3, String str13, int i4) {
        ee7.b(str, ButtonService.USER_ID);
        ee7.b(address, "addresses");
        ee7.b(str2, Constants.PROFILE_KEY_AUTHTYPE);
        ee7.b(str3, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str4, Constants.PROFILE_KEY_BRAND);
        ee7.b(str5, Constants.EMAIL);
        ee7.b(str6, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str7, "gender");
        ee7.b(str8, Constants.PROFILE_KEY_INTEGRATIONS);
        ee7.b(str9, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str10, Constants.PROFILE_KEY_PROFILE_PIC);
        ee7.b(str11, Constants.PROFILE_KEY_REGISTER_DATE);
        ee7.b(unitGroup2, Constants.PROFILE_KEY_UNIT_GROUP);
        ee7.b(str12, "username");
        ee7.b(str13, "activeDeviceId");
        this.userId = str;
        this.addresses = address;
        this.authType = str2;
        this.birthday = str3;
        this.brand = str4;
        this.diagnosticEnabled = z;
        this.email = str5;
        this.emailOptIn = z2;
        this.emailVerified = z3;
        this.firstName = str6;
        this.gender = str7;
        this.heightInCentimeters = i;
        this.integrations = str8;
        this.lastName = str9;
        this.profilePicture = str10;
        this.registerDate = str11;
        this.registrationComplete = z4;
        this.unitGroup = unitGroup2;
        this.useDefaultBiometric = z5;
        this.isOnboardingComplete = z6;
        this.useDefaultGoals = z7;
        this.username = str12;
        this.averageSleep = i2;
        this.averageStep = i3;
        this.activeDeviceId = str13;
        this.weightInGrams = i4;
        this.pinType = String.valueOf(0);
        this.createdAt = "2016-01-01T01:01:01.001Z";
        this.updatedAt = "2016-01-01T01:01:01.001Z";
        this.auth = new Auth("", "", "", 1000);
    }

    @DexIgnore
    public static /* synthetic */ MFUser copy$default(MFUser mFUser, String str, Address address, String str2, String str3, String str4, boolean z, String str5, boolean z2, boolean z3, String str6, String str7, int i, String str8, String str9, String str10, String str11, boolean z4, UnitGroup unitGroup2, boolean z5, boolean z6, boolean z7, String str12, int i2, int i3, String str13, int i4, int i5, Object obj) {
        return mFUser.copy((i5 & 1) != 0 ? mFUser.userId : str, (i5 & 2) != 0 ? mFUser.addresses : address, (i5 & 4) != 0 ? mFUser.authType : str2, (i5 & 8) != 0 ? mFUser.birthday : str3, (i5 & 16) != 0 ? mFUser.brand : str4, (i5 & 32) != 0 ? mFUser.diagnosticEnabled : z, (i5 & 64) != 0 ? mFUser.email : str5, (i5 & 128) != 0 ? mFUser.emailOptIn : z2, (i5 & 256) != 0 ? mFUser.emailVerified : z3, (i5 & 512) != 0 ? mFUser.firstName : str6, (i5 & 1024) != 0 ? mFUser.gender : str7, (i5 & 2048) != 0 ? mFUser.heightInCentimeters : i, (i5 & 4096) != 0 ? mFUser.integrations : str8, (i5 & 8192) != 0 ? mFUser.lastName : str9, (i5 & 16384) != 0 ? mFUser.profilePicture : str10, (i5 & 32768) != 0 ? mFUser.registerDate : str11, (i5 & 65536) != 0 ? mFUser.registrationComplete : z4, (i5 & 131072) != 0 ? mFUser.unitGroup : unitGroup2, (i5 & 262144) != 0 ? mFUser.useDefaultBiometric : z5, (i5 & 524288) != 0 ? mFUser.isOnboardingComplete : z6, (i5 & 1048576) != 0 ? mFUser.useDefaultGoals : z7, (i5 & ImageObject.DATA_SIZE) != 0 ? mFUser.username : str12, (i5 & 4194304) != 0 ? mFUser.averageSleep : i2, (i5 & 8388608) != 0 ? mFUser.averageStep : i3, (i5 & 16777216) != 0 ? mFUser.activeDeviceId : str13, (i5 & 33554432) != 0 ? mFUser.weightInGrams : i4);
    }

    @DexIgnore
    public final String component1() {
        return this.userId;
    }

    @DexIgnore
    public final String component10() {
        return this.firstName;
    }

    @DexIgnore
    public final String component11() {
        return this.gender;
    }

    @DexIgnore
    public final int component12() {
        return this.heightInCentimeters;
    }

    @DexIgnore
    public final String component13() {
        return this.integrations;
    }

    @DexIgnore
    public final String component14() {
        return this.lastName;
    }

    @DexIgnore
    public final String component15() {
        return this.profilePicture;
    }

    @DexIgnore
    public final String component16() {
        return this.registerDate;
    }

    @DexIgnore
    public final boolean component17() {
        return this.registrationComplete;
    }

    @DexIgnore
    public final UnitGroup component18() {
        return this.unitGroup;
    }

    @DexIgnore
    public final boolean component19() {
        return this.useDefaultBiometric;
    }

    @DexIgnore
    public final Address component2() {
        return this.addresses;
    }

    @DexIgnore
    public final boolean component20() {
        return this.isOnboardingComplete;
    }

    @DexIgnore
    public final boolean component21() {
        return this.useDefaultGoals;
    }

    @DexIgnore
    public final String component22() {
        return this.username;
    }

    @DexIgnore
    public final int component23() {
        return this.averageSleep;
    }

    @DexIgnore
    public final int component24() {
        return this.averageStep;
    }

    @DexIgnore
    public final String component25() {
        return this.activeDeviceId;
    }

    @DexIgnore
    public final int component26() {
        return this.weightInGrams;
    }

    @DexIgnore
    public final String component3() {
        return this.authType;
    }

    @DexIgnore
    public final String component4() {
        return this.birthday;
    }

    @DexIgnore
    public final String component5() {
        return this.brand;
    }

    @DexIgnore
    public final boolean component6() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String component7() {
        return this.email;
    }

    @DexIgnore
    public final boolean component8() {
        return this.emailOptIn;
    }

    @DexIgnore
    public final boolean component9() {
        return this.emailVerified;
    }

    @DexIgnore
    public final MFUser copy(String str, Address address, String str2, String str3, String str4, boolean z, String str5, boolean z2, boolean z3, String str6, String str7, int i, String str8, String str9, String str10, String str11, boolean z4, UnitGroup unitGroup2, boolean z5, boolean z6, boolean z7, String str12, int i2, int i3, String str13, int i4) {
        ee7.b(str, ButtonService.USER_ID);
        ee7.b(address, "addresses");
        ee7.b(str2, Constants.PROFILE_KEY_AUTHTYPE);
        ee7.b(str3, Constants.PROFILE_KEY_BIRTHDAY);
        ee7.b(str4, Constants.PROFILE_KEY_BRAND);
        ee7.b(str5, Constants.EMAIL);
        ee7.b(str6, Constants.PROFILE_KEY_FIRST_NAME);
        ee7.b(str7, "gender");
        ee7.b(str8, Constants.PROFILE_KEY_INTEGRATIONS);
        ee7.b(str9, Constants.PROFILE_KEY_LAST_NAME);
        ee7.b(str10, Constants.PROFILE_KEY_PROFILE_PIC);
        ee7.b(str11, Constants.PROFILE_KEY_REGISTER_DATE);
        ee7.b(unitGroup2, Constants.PROFILE_KEY_UNIT_GROUP);
        ee7.b(str12, "username");
        ee7.b(str13, "activeDeviceId");
        return new MFUser(str, address, str2, str3, str4, z, str5, z2, z3, str6, str7, i, str8, str9, str10, str11, z4, unitGroup2, z5, z6, z7, str12, i2, i3, str13, i4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MFUser)) {
            return false;
        }
        MFUser mFUser = (MFUser) obj;
        return ee7.a(this.userId, mFUser.userId) && ee7.a(this.addresses, mFUser.addresses) && ee7.a(this.authType, mFUser.authType) && ee7.a(this.birthday, mFUser.birthday) && ee7.a(this.brand, mFUser.brand) && this.diagnosticEnabled == mFUser.diagnosticEnabled && ee7.a(this.email, mFUser.email) && this.emailOptIn == mFUser.emailOptIn && this.emailVerified == mFUser.emailVerified && ee7.a(this.firstName, mFUser.firstName) && ee7.a(this.gender, mFUser.gender) && this.heightInCentimeters == mFUser.heightInCentimeters && ee7.a(this.integrations, mFUser.integrations) && ee7.a(this.lastName, mFUser.lastName) && ee7.a(this.profilePicture, mFUser.profilePicture) && ee7.a(this.registerDate, mFUser.registerDate) && this.registrationComplete == mFUser.registrationComplete && ee7.a(this.unitGroup, mFUser.unitGroup) && this.useDefaultBiometric == mFUser.useDefaultBiometric && this.isOnboardingComplete == mFUser.isOnboardingComplete && this.useDefaultGoals == mFUser.useDefaultGoals && ee7.a(this.username, mFUser.username) && this.averageSleep == mFUser.averageSleep && this.averageStep == mFUser.averageStep && ee7.a(this.activeDeviceId, mFUser.activeDeviceId) && this.weightInGrams == mFUser.weightInGrams;
    }

    @DexIgnore
    public final String getActiveDeviceId() {
        return this.activeDeviceId;
    }

    @DexIgnore
    public final Address getAddresses() {
        return this.addresses;
    }

    @DexIgnore
    public final int getAge(String str) {
        ee7.b(str, Constants.PROFILE_KEY_BIRTHDAY);
        Calendar instance = Calendar.getInstance();
        try {
            ee7.a((Object) instance, "calendar");
            instance.setTime(zd5.e(str));
            Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(instance), LocalDate.now());
            ee7.a((Object) yearsBetween, "Years.yearsBetween(Local\u2026lendar), LocalDate.now())");
            return yearsBetween.getYears();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("MFUser", "getAge - e=" + e);
            return 18;
        }
    }

    @DexIgnore
    public final Auth getAuth() {
        return this.auth;
    }

    @DexIgnore
    public final String getAuthType() {
        return this.authType;
    }

    @DexIgnore
    public final int getAverageSleep() {
        return this.averageSleep;
    }

    @DexIgnore
    public final int getAverageStep() {
        return this.averageStep;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getBrand() {
        return this.brand;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final boolean getEmailOptIn() {
        return this.emailOptIn;
    }

    @DexIgnore
    public final boolean getEmailVerified() {
        return this.emailVerified;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final int getHeightInCentimeters() {
        return this.heightInCentimeters;
    }

    @DexIgnore
    public final String getIntegrations() {
        return this.integrations;
    }

    @DexIgnore
    public final List<String> getIntegrationsList() {
        if (TextUtils.isEmpty(this.integrations)) {
            return new ArrayList();
        }
        Object[] array = nh7.a((CharSequence) this.integrations, new String[]{LocaleConverter.LOCALE_DELIMITER}, false, 0, 6, (Object) null).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            return new ArrayList(w97.c((String[]) Arrays.copyOf(strArr, strArr.length)));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getProfilePicture() {
        return this.profilePicture;
    }

    @DexIgnore
    public final String getRegisterDate() {
        return this.registerDate;
    }

    @DexIgnore
    public final boolean getRegistrationComplete() {
        return this.registrationComplete;
    }

    @DexIgnore
    public final UnitGroup getUnitGroup() {
        return this.unitGroup;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean getUseDefaultBiometric() {
        return this.useDefaultBiometric;
    }

    @DexIgnore
    public final boolean getUseDefaultGoals() {
        return this.useDefaultGoals;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final String getUsername() {
        return this.username;
    }

    @DexIgnore
    public final int getWeightInGrams() {
        return this.weightInGrams;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r2v37, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        String str = this.userId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Address address = this.addresses;
        int hashCode2 = (hashCode + (address != null ? address.hashCode() : 0)) * 31;
        String str2 = this.authType;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.birthday;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.brand;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        boolean z = this.diagnosticEnabled;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode5 + i3) * 31;
        String str5 = this.email;
        int hashCode6 = (i5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        boolean z2 = this.emailOptIn;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = (hashCode6 + i6) * 31;
        boolean z3 = this.emailVerified;
        if (z3) {
            z3 = true;
        }
        int i9 = z3 ? 1 : 0;
        int i10 = z3 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        String str6 = this.firstName;
        int hashCode7 = (i11 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.gender;
        int hashCode8 = (((hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31) + this.heightInCentimeters) * 31;
        String str8 = this.integrations;
        int hashCode9 = (hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.lastName;
        int hashCode10 = (hashCode9 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.profilePicture;
        int hashCode11 = (hashCode10 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.registerDate;
        int hashCode12 = (hashCode11 + (str11 != null ? str11.hashCode() : 0)) * 31;
        boolean z4 = this.registrationComplete;
        if (z4) {
            z4 = true;
        }
        int i12 = z4 ? 1 : 0;
        int i13 = z4 ? 1 : 0;
        int i14 = (hashCode12 + i12) * 31;
        UnitGroup unitGroup2 = this.unitGroup;
        int hashCode13 = (i14 + (unitGroup2 != null ? unitGroup2.hashCode() : 0)) * 31;
        boolean z5 = this.useDefaultBiometric;
        if (z5) {
            z5 = true;
        }
        int i15 = z5 ? 1 : 0;
        int i16 = z5 ? 1 : 0;
        int i17 = (hashCode13 + i15) * 31;
        boolean z6 = this.isOnboardingComplete;
        if (z6) {
            z6 = true;
        }
        int i18 = z6 ? 1 : 0;
        int i19 = z6 ? 1 : 0;
        int i20 = (i17 + i18) * 31;
        boolean z7 = this.useDefaultGoals;
        if (z7 == 0) {
            i2 = z7;
        }
        int i21 = (i20 + i2) * 31;
        String str12 = this.username;
        int hashCode14 = (((((i21 + (str12 != null ? str12.hashCode() : 0)) * 31) + this.averageSleep) * 31) + this.averageStep) * 31;
        String str13 = this.activeDeviceId;
        if (str13 != null) {
            i = str13.hashCode();
        }
        return ((hashCode14 + i) * 31) + this.weightInGrams;
    }

    @DexIgnore
    public final boolean isOnboardingComplete() {
        return this.isOnboardingComplete;
    }

    @DexIgnore
    public final void setActiveDeviceId(String str) {
        ee7.b(str, "<set-?>");
        this.activeDeviceId = str;
    }

    @DexIgnore
    public final void setAddresses(Address address) {
        ee7.b(address, "<set-?>");
        this.addresses = address;
    }

    @DexIgnore
    public final void setAuth(Auth auth2) {
        ee7.b(auth2, "<set-?>");
        this.auth = auth2;
    }

    @DexIgnore
    public final void setAuthType(String str) {
        ee7.b(str, "<set-?>");
        this.authType = str;
    }

    @DexIgnore
    public final void setAverageSleep(int i) {
        this.averageSleep = i;
    }

    @DexIgnore
    public final void setAverageStep(int i) {
        this.averageStep = i;
    }

    @DexIgnore
    public final void setBirthday(String str) {
        ee7.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setBrand(String str) {
        ee7.b(str, "<set-?>");
        this.brand = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        ee7.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setEmailOptIn(boolean z) {
        this.emailOptIn = z;
    }

    @DexIgnore
    public final void setEmailVerified(boolean z) {
        this.emailVerified = z;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        ee7.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        ee7.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setHeightInCentimeters(int i) {
        this.heightInCentimeters = i;
    }

    @DexIgnore
    public final void setIntegrations(String str) {
        ee7.b(str, "<set-?>");
        this.integrations = str;
    }

    @DexIgnore
    public final void setIntegrationsList(List<String> list) {
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (String str : list) {
                sb.append(str);
                sb.append('_');
            }
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "data.toString()");
            this.integrations = sb2;
        }
    }

    @DexIgnore
    public final void setLastName(String str) {
        ee7.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setOnboardingComplete(boolean z) {
        this.isOnboardingComplete = z;
    }

    @DexIgnore
    public final void setPinType(String str) {
        ee7.b(str, "<set-?>");
        this.pinType = str;
    }

    @DexIgnore
    public final void setProfilePicture(String str) {
        ee7.b(str, "<set-?>");
        this.profilePicture = str;
    }

    @DexIgnore
    public final void setRegisterDate(String str) {
        ee7.b(str, "<set-?>");
        this.registerDate = str;
    }

    @DexIgnore
    public final void setRegistrationComplete(boolean z) {
        this.registrationComplete = z;
    }

    @DexIgnore
    public final void setUnitGroup(UnitGroup unitGroup2) {
        ee7.b(unitGroup2, "<set-?>");
        this.unitGroup = unitGroup2;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setUseDefaultBiometric(boolean z) {
        this.useDefaultBiometric = z;
    }

    @DexIgnore
    public final void setUseDefaultGoals(boolean z) {
        this.useDefaultGoals = z;
    }

    @DexIgnore
    public final void setUserId(String str) {
        ee7.b(str, "<set-?>");
        this.userId = str;
    }

    @DexIgnore
    public final void setUsername(String str) {
        ee7.b(str, "<set-?>");
        this.username = str;
    }

    @DexIgnore
    public final void setWeightInGrams(int i) {
        this.weightInGrams = i;
    }

    @DexIgnore
    public String toString() {
        return "MFUser(userId=" + this.userId + ", addresses=" + this.addresses + ", authType=" + this.authType + ", birthday=" + this.birthday + ", brand=" + this.brand + ", diagnosticEnabled=" + this.diagnosticEnabled + ", email=" + this.email + ", emailOptIn=" + this.emailOptIn + ", emailVerified=" + this.emailVerified + ", firstName=" + this.firstName + ", gender=" + this.gender + ", heightInCentimeters=" + this.heightInCentimeters + ", integrations=" + this.integrations + ", lastName=" + this.lastName + ", profilePicture=" + this.profilePicture + ", registerDate=" + this.registerDate + ", registrationComplete=" + this.registrationComplete + ", unitGroup=" + this.unitGroup + ", useDefaultBiometric=" + this.useDefaultBiometric + ", isOnboardingComplete=" + this.isOnboardingComplete + ", useDefaultGoals=" + this.useDefaultGoals + ", username=" + this.username + ", averageSleep=" + this.averageSleep + ", averageStep=" + this.averageStep + ", activeDeviceId=" + this.activeDeviceId + ", weightInGrams=" + this.weightInGrams + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MFUser() {
        /*
            r27 = this;
            r0 = r27
            com.portfolio.platform.data.model.MFUser$Address r1 = new com.portfolio.platform.data.model.MFUser$Address
            r2 = r1
            java.lang.String r3 = ""
            r1.<init>(r3, r3)
            com.fossil.za5 r1 = com.fossil.za5.EMAIL
            java.lang.String r1 = r1.getValue()
            r3 = r1
            java.lang.String r4 = "AuthType.EMAIL.value"
            com.fossil.ee7.a(r1, r4)
            com.fossil.fb5 r1 = com.fossil.fb5.MALE
            java.lang.String r11 = r1.getValue()
            com.portfolio.platform.data.model.MFUser$UnitGroup r1 = new com.portfolio.platform.data.model.MFUser$UnitGroup
            r18 = r1
            com.fossil.ob5 r4 = com.fossil.ob5.IMPERIAL
            java.lang.String r4 = r4.getValue()
            java.lang.String r5 = "Unit.IMPERIAL.value"
            com.fossil.ee7.a(r4, r5)
            com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
            java.lang.String r6 = r6.getValue()
            com.fossil.ee7.a(r6, r5)
            com.fossil.ob5 r7 = com.fossil.ob5.IMPERIAL
            java.lang.String r7 = r7.getValue()
            com.fossil.ee7.a(r7, r5)
            com.fossil.ob5 r8 = com.fossil.ob5.IMPERIAL
            java.lang.String r8 = r8.getValue()
            com.fossil.ee7.a(r8, r5)
            r1.<init>(r4, r6, r7, r8)
            java.lang.String r1 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            r6 = 0
            java.lang.String r7 = ""
            r8 = 0
            r9 = 0
            java.lang.String r10 = ""
            r12 = 0
            java.lang.String r13 = ""
            java.lang.String r14 = ""
            java.lang.String r15 = ""
            java.lang.String r16 = ""
            r17 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            java.lang.String r22 = ""
            r23 = 0
            r24 = 0
            java.lang.String r25 = ""
            r26 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.MFUser.<init>():void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MFUser(java.lang.String r28, java.lang.String r29) {
        /*
            r27 = this;
            r0 = r27
            r7 = r28
            r1 = r29
            java.lang.String r2 = "email"
            r3 = r28
            com.fossil.ee7.b(r3, r2)
            java.lang.String r2 = "uid"
            r3 = r29
            com.fossil.ee7.b(r3, r2)
            com.portfolio.platform.data.model.MFUser$Address r3 = new com.portfolio.platform.data.model.MFUser$Address
            r2 = r3
            java.lang.String r4 = ""
            r3.<init>(r4, r4)
            com.fossil.za5 r3 = com.fossil.za5.EMAIL
            java.lang.String r4 = r3.getValue()
            r3 = r4
            java.lang.String r5 = "AuthType.EMAIL.value"
            com.fossil.ee7.a(r4, r5)
            com.fossil.fb5 r4 = com.fossil.fb5.MALE
            java.lang.String r11 = r4.getValue()
            com.portfolio.platform.data.model.MFUser$UnitGroup r4 = new com.portfolio.platform.data.model.MFUser$UnitGroup
            r18 = r4
            com.fossil.ob5 r5 = com.fossil.ob5.IMPERIAL
            java.lang.String r5 = r5.getValue()
            java.lang.String r6 = "Unit.IMPERIAL.value"
            com.fossil.ee7.a(r5, r6)
            com.fossil.ob5 r8 = com.fossil.ob5.IMPERIAL
            java.lang.String r8 = r8.getValue()
            com.fossil.ee7.a(r8, r6)
            com.fossil.ob5 r9 = com.fossil.ob5.IMPERIAL
            java.lang.String r9 = r9.getValue()
            com.fossil.ee7.a(r9, r6)
            com.fossil.ob5 r10 = com.fossil.ob5.IMPERIAL
            java.lang.String r10 = r10.getValue()
            com.fossil.ee7.a(r10, r6)
            r4.<init>(r5, r8, r9, r10)
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            r6 = 0
            r8 = 0
            r9 = 0
            java.lang.String r10 = ""
            r12 = 0
            java.lang.String r13 = ""
            java.lang.String r14 = ""
            java.lang.String r15 = ""
            java.lang.String r16 = ""
            r17 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            java.lang.String r22 = ""
            r23 = 0
            r24 = 0
            java.lang.String r25 = ""
            r26 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.MFUser.<init>(java.lang.String, java.lang.String):void");
    }
}
