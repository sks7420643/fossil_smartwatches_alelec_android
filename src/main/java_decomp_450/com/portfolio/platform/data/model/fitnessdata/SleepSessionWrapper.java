package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.ee7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionWrapper {
    @DexIgnore
    public /* final */ int awakeMinutes;
    @DexIgnore
    public /* final */ int deepSleepMinutes;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ HeartRateWrapper heartRate;
    @DexIgnore
    public /* final */ int lightSleepMinutes;
    @DexIgnore
    public /* final */ int normalizedSleepQuality;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public /* final */ List<SleepStateChangeWrapper> stateChanges;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;

    @DexIgnore
    public SleepSessionWrapper(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.awakeMinutes = i;
        this.lightSleepMinutes = i2;
        this.deepSleepMinutes = i3;
        this.heartRate = heartRateWrapper;
        this.normalizedSleepQuality = i4;
        this.timezoneOffsetInSecond = i5;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionWrapper copy$default(SleepSessionWrapper sleepSessionWrapper, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5, int i6, Object obj) {
        return sleepSessionWrapper.copy((i6 & 1) != 0 ? sleepSessionWrapper.startTime : dateTime, (i6 & 2) != 0 ? sleepSessionWrapper.endTime : dateTime2, (i6 & 4) != 0 ? sleepSessionWrapper.awakeMinutes : i, (i6 & 8) != 0 ? sleepSessionWrapper.lightSleepMinutes : i2, (i6 & 16) != 0 ? sleepSessionWrapper.deepSleepMinutes : i3, (i6 & 32) != 0 ? sleepSessionWrapper.heartRate : heartRateWrapper, (i6 & 64) != 0 ? sleepSessionWrapper.normalizedSleepQuality : i4, (i6 & 128) != 0 ? sleepSessionWrapper.timezoneOffsetInSecond : i5);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int component4() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int component5() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final HeartRateWrapper component6() {
        return this.heartRate;
    }

    @DexIgnore
    public final int component7() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final SleepSessionWrapper copy(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        ee7.b(dateTime, SampleRaw.COLUMN_START_TIME);
        ee7.b(dateTime2, SampleRaw.COLUMN_END_TIME);
        return new SleepSessionWrapper(dateTime, dateTime2, i, i2, i3, heartRateWrapper, i4, i5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepSessionWrapper)) {
            return false;
        }
        SleepSessionWrapper sleepSessionWrapper = (SleepSessionWrapper) obj;
        return ee7.a(this.startTime, sleepSessionWrapper.startTime) && ee7.a(this.endTime, sleepSessionWrapper.endTime) && this.awakeMinutes == sleepSessionWrapper.awakeMinutes && this.lightSleepMinutes == sleepSessionWrapper.lightSleepMinutes && this.deepSleepMinutes == sleepSessionWrapper.deepSleepMinutes && ee7.a(this.heartRate, sleepSessionWrapper.heartRate) && this.normalizedSleepQuality == sleepSessionWrapper.normalizedSleepQuality && this.timezoneOffsetInSecond == sleepSessionWrapper.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getAwakeMinutes() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int getDeepSleepMinutes() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final int getLightSleepMinutes() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<SleepStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (dateTime != null ? dateTime.hashCode() : 0) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (((((((hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + this.awakeMinutes) * 31) + this.lightSleepMinutes) * 31) + this.deepSleepMinutes) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return ((((hashCode2 + i) * 31) + this.normalizedSleepQuality) * 31) + this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionWrapper(startTime=" + this.startTime + ", endTime=" + this.endTime + ", awakeMinutes=" + this.awakeMinutes + ", lightSleepMinutes=" + this.lightSleepMinutes + ", deepSleepMinutes=" + this.deepSleepMinutes + ", heartRate=" + this.heartRate + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SleepSessionWrapper(com.fossil.fitness.SleepSession r11) {
        /*
            r10 = this;
            java.lang.String r0 = "sleepSession"
            com.fossil.ee7.b(r11, r0)
            org.joda.time.DateTime r2 = new org.joda.time.DateTime
            int r0 = r11.getStartTime()
            long r0 = (long) r0
            r3 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r3
            int r5 = r11.getTimezoneOffsetInSecond()
            int r5 = r5 * 1000
            org.joda.time.DateTimeZone r5 = org.joda.time.DateTimeZone.forOffsetMillis(r5)
            r2.<init>(r0, r5)
            org.joda.time.DateTime r0 = new org.joda.time.DateTime
            int r1 = r11.getEndTime()
            long r5 = (long) r1
            long r5 = r5 * r3
            int r1 = r11.getTimezoneOffsetInSecond()
            int r1 = r1 * 1000
            org.joda.time.DateTimeZone r1 = org.joda.time.DateTimeZone.forOffsetMillis(r1)
            r0.<init>(r5, r1)
            int r4 = r11.getAwakeMinutes()
            int r5 = r11.getLightSleepMinutes()
            int r6 = r11.getDeepSleepMinutes()
            com.fossil.fitness.HeartRate r1 = r11.getHeartrate()
            r3 = 0
            if (r1 == 0) goto L_0x005c
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r1 = new com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper
            com.fossil.fitness.HeartRate r7 = r11.getHeartrate()
            if (r7 == 0) goto L_0x0058
            java.lang.String r3 = "sleepSession.heartrate!!"
            com.fossil.ee7.a(r7, r3)
            r1.<init>(r7)
            r7 = r1
            goto L_0x005d
        L_0x0058:
            com.fossil.ee7.a()
            throw r3
        L_0x005c:
            r7 = r3
        L_0x005d:
            int r8 = r11.getQuality()
            int r9 = r11.getTimezoneOffsetInSecond()
            r1 = r10
            r3 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.ArrayList r11 = r11.getStateChanges()
            java.lang.String r0 = "sleepSession.stateChanges"
            com.fossil.ee7.a(r11, r0)
            java.util.Iterator r11 = r11.iterator()
        L_0x0077:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0093
            java.lang.Object r0 = r11.next()
            com.fossil.fitness.SleepStateChange r0 = (com.fossil.fitness.SleepStateChange) r0
            java.util.List<com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper> r1 = r10.stateChanges
            com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper r2 = new com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper
            java.lang.String r3 = "it"
            com.fossil.ee7.a(r0, r3)
            r2.<init>(r0)
            r1.add(r2)
            goto L_0x0077
        L_0x0093:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper.<init>(com.fossil.fitness.SleepSession):void");
    }
}
