package com.portfolio.platform.data.model.diana.workout;

import com.fossil.ee7;
import com.fossil.yb5;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChange {
    @DexIgnore
    public /* final */ yb5 state;
    @DexIgnore
    public /* final */ Date time;

    @DexIgnore
    public WorkoutStateChange(yb5 yb5, Date date) {
        ee7.b(yb5, "state");
        ee7.b(date, LogBuilder.KEY_TIME);
        this.state = yb5;
        this.time = date;
    }

    @DexIgnore
    private final yb5 component1() {
        return this.state;
    }

    @DexIgnore
    private final Date component2() {
        return this.time;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChange copy$default(WorkoutStateChange workoutStateChange, yb5 yb5, Date date, int i, Object obj) {
        if ((i & 1) != 0) {
            yb5 = workoutStateChange.state;
        }
        if ((i & 2) != 0) {
            date = workoutStateChange.time;
        }
        return workoutStateChange.copy(yb5, date);
    }

    @DexIgnore
    public final WorkoutStateChange copy(yb5 yb5, Date date) {
        ee7.b(yb5, "state");
        ee7.b(date, LogBuilder.KEY_TIME);
        return new WorkoutStateChange(yb5, date);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutStateChange)) {
            return false;
        }
        WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
        return ee7.a(this.state, workoutStateChange.state) && ee7.a(this.time, workoutStateChange.time);
    }

    @DexIgnore
    public int hashCode() {
        yb5 yb5 = this.state;
        int i = 0;
        int hashCode = (yb5 != null ? yb5.hashCode() : 0) * 31;
        Date date = this.time;
        if (date != null) {
            i = date.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange(state=" + this.state + ", time=" + this.time + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStateChange(int i, com.fossil.fitness.WorkoutStateChange workoutStateChange) {
        this(yb5.Companion.a(workoutStateChange.getState().name()), new Date(((long) (i + workoutStateChange.getIndexInSecond())) * 1000));
        ee7.b(workoutStateChange, "workoutStageChange");
    }
}
