package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @te4("timezoneCityCode")
    public String cityCode;
    @DexIgnore
    @te4("timezoneId")
    public String timeZoneId;
    @DexIgnore
    @te4("timezoneCityName")
    public String timeZoneName;
    @DexIgnore
    @te4("timezoneOffset")
    public int timezoneOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SecondTimezoneSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneSetting createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new SecondTimezoneSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SecondTimezoneSetting[] newArray(int i) {
            return new SecondTimezoneSetting[i];
        }
    }

    @DexIgnore
    public SecondTimezoneSetting() {
        this(null, null, 0, null, 15, null);
    }

    @DexIgnore
    public SecondTimezoneSetting(String str, String str2, int i, String str3) {
        ee7.b(str, "timeZoneName");
        ee7.b(str2, "timeZoneId");
        ee7.b(str3, "cityCode");
        this.timeZoneName = str;
        this.timeZoneId = str2;
        this.timezoneOffset = i;
        this.cityCode = str3;
    }

    @DexIgnore
    public static /* synthetic */ SecondTimezoneSetting copy$default(SecondTimezoneSetting secondTimezoneSetting, String str, String str2, int i, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = secondTimezoneSetting.timeZoneName;
        }
        if ((i2 & 2) != 0) {
            str2 = secondTimezoneSetting.timeZoneId;
        }
        if ((i2 & 4) != 0) {
            i = secondTimezoneSetting.timezoneOffset;
        }
        if ((i2 & 8) != 0) {
            str3 = secondTimezoneSetting.cityCode;
        }
        return secondTimezoneSetting.copy(str, str2, i, str3);
    }

    @DexIgnore
    @Override // java.lang.Object
    public final SecondTimezoneSetting clone() {
        String str = this.timeZoneName;
        String str2 = this.timeZoneId;
        return new SecondTimezoneSetting(str, str2, ConversionUtils.INSTANCE.getTimezoneRawOffsetById(str2), this.cityCode);
    }

    @DexIgnore
    public final String component1() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final String component2() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component4() {
        return this.cityCode;
    }

    @DexIgnore
    public final SecondTimezoneSetting copy(String str, String str2, int i, String str3) {
        ee7.b(str, "timeZoneName");
        ee7.b(str2, "timeZoneId");
        ee7.b(str3, "cityCode");
        return new SecondTimezoneSetting(str, str2, i, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SecondTimezoneSetting)) {
            return false;
        }
        SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) obj;
        return ee7.a(this.timeZoneName, secondTimezoneSetting.timeZoneName) && ee7.a(this.timeZoneId, secondTimezoneSetting.timeZoneId) && this.timezoneOffset == secondTimezoneSetting.timezoneOffset && ee7.a(this.cityCode, secondTimezoneSetting.cityCode);
    }

    @DexIgnore
    public final String getCityCode() {
        return this.cityCode;
    }

    @DexIgnore
    public final String getTimeZoneId() {
        return this.timeZoneId;
    }

    @DexIgnore
    public final String getTimeZoneName() {
        return this.timeZoneName;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.timeZoneName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.timeZoneId;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.timezoneOffset) * 31;
        String str3 = this.cityCode;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setCityCode(String str) {
        ee7.b(str, "<set-?>");
        this.cityCode = str;
    }

    @DexIgnore
    public final void setTimeZoneId(String str) {
        ee7.b(str, "<set-?>");
        this.timeZoneId = str;
    }

    @DexIgnore
    public final void setTimeZoneName(String str) {
        ee7.b(str, "<set-?>");
        this.timeZoneName = str;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public String toString() {
        return "SecondTimezoneSetting(timeZoneName=" + this.timeZoneName + ", timeZoneId=" + this.timeZoneId + ", timezoneOffset=" + this.timezoneOffset + ", cityCode=" + this.cityCode + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.timeZoneName);
        parcel.writeString(this.timeZoneId);
        parcel.writeInt(this.timezoneOffset);
        parcel.writeString(this.cityCode);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SecondTimezoneSetting(String str, String str2, int i, String str3, int i2, zd7 zd7) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? "" : str3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SecondTimezoneSetting(android.os.Parcel r5) {
        /*
            r4 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = r5.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r2 = r5.readString()
            if (r2 == 0) goto L_0x0016
            goto L_0x0017
        L_0x0016:
            r2 = r1
        L_0x0017:
            int r3 = r5.readInt()
            java.lang.String r5 = r5.readString()
            if (r5 == 0) goto L_0x0022
            r1 = r5
        L_0x0022:
            r4.<init>(r0, r2, r3, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.SecondTimezoneSetting.<init>(android.os.Parcel):void");
    }
}
