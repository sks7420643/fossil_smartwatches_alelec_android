package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.ee7;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressOfWeather {
    @DexIgnore
    public /* final */ String address;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ double lat;
    @DexIgnore
    public /* final */ double lng;

    @DexIgnore
    public AddressOfWeather() {
        this(0.0d, 0.0d, null, 7, null);
    }

    @DexIgnore
    public AddressOfWeather(double d, double d2, String str) {
        ee7.b(str, "address");
        this.lat = d;
        this.lng = d2;
        this.address = str;
    }

    @DexIgnore
    public static /* synthetic */ AddressOfWeather copy$default(AddressOfWeather addressOfWeather, double d, double d2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            d = addressOfWeather.lat;
        }
        if ((i & 2) != 0) {
            d2 = addressOfWeather.lng;
        }
        if ((i & 4) != 0) {
            str = addressOfWeather.address;
        }
        return addressOfWeather.copy(d, d2, str);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final String component3() {
        return this.address;
    }

    @DexIgnore
    public final AddressOfWeather copy(double d, double d2, String str) {
        ee7.b(str, "address");
        return new AddressOfWeather(d, d2, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AddressOfWeather)) {
            return false;
        }
        AddressOfWeather addressOfWeather = (AddressOfWeather) obj;
        return Double.compare(this.lat, addressOfWeather.lat) == 0 && Double.compare(this.lng, addressOfWeather.lng) == 0 && ee7.a(this.address, addressOfWeather.address);
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public int hashCode() {
        int a = ((Double.doubleToLongBits(this.lat) * 31) + Double.doubleToLongBits(this.lng)) * 31;
        String str = this.address;
        return a + (str != null ? str.hashCode() : 0);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AddressOfWeather(double d, double d2, String str, int i, zd7 zd7) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) == 0 ? d2 : 0.0d, (i & 4) != 0 ? "" : str);
    }
}
