package com.portfolio.platform.data.model.diana.preset;

import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.nd5;
import com.fossil.re4;
import com.fossil.se4;
import com.fossil.te4;
import com.fossil.xc5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    @se4(DianaPresetComplicationSettingSerializer.class)
    @te4("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @te4("createdAt")
    public String createdAt; // = "";
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("isActive")
    public boolean isActive;
    @DexIgnore
    @re4
    @te4("name")
    public String name;
    @DexIgnore
    @nd5
    public int pinType; // = 1;
    @DexIgnore
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt; // = "";
    @DexIgnore
    @te4("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @re4
    @se4(DianaPresetWatchAppSettingSerializer.class)
    @te4("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final DianaPreset cloneFrom(DianaPreset dianaPreset) {
            ee7.b(dianaPreset, "preset");
            String y = zd5.y(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            ee7.a((Object) uuid, "UUID.randomUUID().toString()");
            String serialNumber = dianaPreset.getSerialNumber();
            String a = ig5.a(PortfolioApp.g0.c(), 2131886533);
            ee7.a((Object) a, "LanguageHelper.getString\u2026wPreset_Title__NewPreset)");
            DianaPreset dianaPreset2 = new DianaPreset(uuid, serialNumber, a, false, xc5.b(dianaPreset.getComplications()), xc5.c(dianaPreset.getWatchapps()), dianaPreset.getWatchFaceId());
            dianaPreset2.setCreatedAt(y);
            dianaPreset2.setUpdatedAt(y);
            return dianaPreset2;
        }

        @DexIgnore
        public final DianaPreset cloneFromDefaultPreset(DianaRecommendPreset dianaRecommendPreset) {
            ee7.b(dianaRecommendPreset, "recommendPreset");
            String y = zd5.y(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            ee7.a((Object) uuid, "UUID.randomUUID().toString()");
            DianaPreset dianaPreset = new DianaPreset(uuid, dianaRecommendPreset.getSerialNumber(), dianaRecommendPreset.getName(), dianaRecommendPreset.isDefault(), xc5.b(dianaRecommendPreset.getComplications()), xc5.c(dianaRecommendPreset.getWatchapps()), dianaRecommendPreset.getWatchFaceId());
            dianaPreset.setCreatedAt(y);
            dianaPreset.setUpdatedAt(y);
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (it.hasNext()) {
                ee7.a((Object) y, "timestamp");
                it.next().setLocalUpdateAt(y);
            }
            Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
            while (it2.hasNext()) {
                ee7.a((Object) y, "timestamp");
                it2.next().setLocalUpdateAt(y);
            }
            return dianaPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public DianaPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        ee7.b(str, "id");
        ee7.b(str2, "serialNumber");
        ee7.b(str3, "name");
        ee7.b(arrayList, "complications");
        ee7.b(arrayList2, "watchapps");
        ee7.b(str4, "watchFaceId");
        this.id = str;
        this.serialNumber = str2;
        this.name = str3;
        this.isActive = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.portfolio.platform.data.model.diana.preset.DianaPreset */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DianaPreset copy$default(DianaPreset dianaPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = dianaPreset.serialNumber;
        }
        if ((i & 4) != 0) {
            str3 = dianaPreset.name;
        }
        if ((i & 8) != 0) {
            z = dianaPreset.isActive;
        }
        if ((i & 16) != 0) {
            arrayList = dianaPreset.complications;
        }
        if ((i & 32) != 0) {
            arrayList2 = dianaPreset.watchapps;
        }
        if ((i & 64) != 0) {
            str4 = dianaPreset.watchFaceId;
        }
        return dianaPreset.copy(str, str2, str3, z, arrayList, arrayList2, str4);
    }

    @DexIgnore
    public final DianaPreset clone() {
        DianaPreset dianaPreset = new DianaPreset(this.id, this.serialNumber, this.name, this.isActive, xc5.b(this.complications), xc5.c(this.watchapps), this.watchFaceId);
        dianaPreset.updatedAt = this.updatedAt;
        dianaPreset.createdAt = this.createdAt;
        return dianaPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isActive;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final DianaPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        ee7.b(str, "id");
        ee7.b(str2, "serialNumber");
        ee7.b(str3, "name");
        ee7.b(arrayList, "complications");
        ee7.b(arrayList2, "watchapps");
        ee7.b(str4, "watchFaceId");
        return new DianaPreset(str, str2, str3, z, arrayList, arrayList2, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaPreset)) {
            return false;
        }
        DianaPreset dianaPreset = (DianaPreset) obj;
        return ee7.a(this.id, dianaPreset.id) && ee7.a(this.serialNumber, dianaPreset.serialNumber) && ee7.a(this.name, dianaPreset.name) && this.isActive == dianaPreset.isActive && ee7.a(this.complications, dianaPreset.complications) && ee7.a(this.watchapps, dianaPreset.watchapps) && ee7.a(this.watchFaceId, dianaPreset.watchFaceId);
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.serialNumber;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode3 + i2) * 31;
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = (i4 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = (hashCode4 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        String str4 = this.watchFaceId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        ee7.b(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.id + ", serialNumber=" + this.serialNumber + ", name=" + this.name + ", isActive=" + this.isActive + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ")";
    }
}
