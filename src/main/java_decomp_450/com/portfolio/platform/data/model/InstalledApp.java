package com.portfolio.platform.data.model;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class InstalledApp implements Serializable {
    @DexIgnore
    public int currentHandGroup;
    @DexIgnore
    public int dbRowId;
    @DexIgnore
    public String identifier;
    @DexIgnore
    public Boolean isSelected;
    @DexIgnore
    public String title;

    @DexIgnore
    public InstalledApp(String str, String str2, Boolean bool) {
        this.identifier = str;
        this.title = str2;
        this.isSelected = bool;
    }

    @DexIgnore
    public int getCurrentHandGroup() {
        return this.currentHandGroup;
    }

    @DexIgnore
    public int getDbRowId() {
        return this.dbRowId;
    }

    @DexIgnore
    public String getIdentifier() {
        return this.identifier;
    }

    @DexIgnore
    public String getTitle() {
        return this.title;
    }

    @DexIgnore
    public Boolean isSelected() {
        return this.isSelected;
    }

    @DexIgnore
    public void setCurrentHandGroup(int i) {
        this.currentHandGroup = i;
    }

    @DexIgnore
    public void setDbRowId(int i) {
        this.dbRowId = i;
    }

    @DexIgnore
    public void setIdentifier(String str) {
        this.identifier = str;
    }

    @DexIgnore
    public void setSelected(boolean z) {
        this.isSelected = Boolean.valueOf(z);
    }

    @DexIgnore
    public void setTitle(String str) {
        this.title = str;
    }
}
