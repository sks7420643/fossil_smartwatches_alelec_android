package com.portfolio.platform.data.model.room.microapp;

import com.fossil.ee7;
import com.fossil.te4;
import com.fossil.xc5;
import com.fossil.zd7;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridRecommendPreset {
    @DexIgnore
    @te4("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @te4("createdAt")
    public String createdAt;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4("isDefault")
    public boolean isDefault;
    @DexIgnore
    @te4("name")
    public String name;
    @DexIgnore
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @te4("updatedAt")
    public String updatedAt;

    @DexIgnore
    public HybridRecommendPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        ee7.b(str, "id");
        ee7.b(str3, "serialNumber");
        ee7.b(arrayList, "buttons");
        ee7.b(str4, "createdAt");
        ee7.b(str5, "updatedAt");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isDefault = z;
        this.createdAt = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HybridRecommendPreset copy$default(HybridRecommendPreset hybridRecommendPreset, String str, String str2, String str3, ArrayList arrayList, boolean z, String str4, String str5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridRecommendPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = hybridRecommendPreset.name;
        }
        if ((i & 4) != 0) {
            str3 = hybridRecommendPreset.serialNumber;
        }
        if ((i & 8) != 0) {
            arrayList = hybridRecommendPreset.buttons;
        }
        if ((i & 16) != 0) {
            z = hybridRecommendPreset.isDefault;
        }
        if ((i & 32) != 0) {
            str4 = hybridRecommendPreset.createdAt;
        }
        if ((i & 64) != 0) {
            str5 = hybridRecommendPreset.updatedAt;
        }
        return hybridRecommendPreset.copy(str, str2, str3, arrayList, z, str4, str5);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, xc5.a(this.buttons), this.isDefault);
        hybridPreset.setCreatedAt(this.createdAt);
        hybridPreset.setUpdatedAt(this.updatedAt);
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isDefault;
    }

    @DexIgnore
    public final String component6() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component7() {
        return this.updatedAt;
    }

    @DexIgnore
    public final HybridRecommendPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        ee7.b(str, "id");
        ee7.b(str3, "serialNumber");
        ee7.b(arrayList, "buttons");
        ee7.b(str4, "createdAt");
        ee7.b(str5, "updatedAt");
        return new HybridRecommendPreset(str, str2, str3, arrayList, z, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HybridRecommendPreset)) {
            return false;
        }
        HybridRecommendPreset hybridRecommendPreset = (HybridRecommendPreset) obj;
        return ee7.a(this.id, hybridRecommendPreset.id) && ee7.a(this.name, hybridRecommendPreset.name) && ee7.a(this.serialNumber, hybridRecommendPreset.serialNumber) && ee7.a(this.buttons, hybridRecommendPreset.buttons) && this.isDefault == hybridRecommendPreset.isDefault && ee7.a(this.createdAt, hybridRecommendPreset.createdAt) && ee7.a(this.updatedAt, hybridRecommendPreset.updatedAt);
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.serialNumber;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        int hashCode4 = (hashCode3 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode4 + i2) * 31;
        String str4 = this.createdAt;
        int hashCode5 = (i4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        ee7.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridRecommendPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isDefault=" + this.isDefault + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridRecommendPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, String str4, String str5, int i, zd7 zd7) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z, str4, str5);
    }
}
