package com.portfolio.platform.data.model.room.microapp;

import com.fossil.ee7;
import com.fossil.te4;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariant {
    @DexIgnore
    @te4("appId")
    public String appId;
    @DexIgnore
    @te4("createdAt")
    public Date createdAt;
    @DexIgnore
    @te4(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_DECLARATION_FILES)
    public ArrayList<DeclarationFile> declarationFileList; // = new ArrayList<>();
    @DexIgnore
    @te4("description")
    public String description;
    @DexIgnore
    @te4("id")
    public String id;
    @DexIgnore
    @te4(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER)
    public int majorNumber;
    @DexIgnore
    @te4(com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER)
    public int minorNumber;
    @DexIgnore
    @te4("name")
    public String name;
    @DexIgnore
    @te4("serialNumber")
    public String serialNumber;
    @DexIgnore
    @te4("updatedAt")
    public Date updatedAt;

    @DexIgnore
    public MicroAppVariant(String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5) {
        ee7.b(str, "id");
        ee7.b(str2, "appId");
        ee7.b(str3, "name");
        ee7.b(str4, "description");
        ee7.b(date, "createdAt");
        ee7.b(date2, "updatedAt");
        ee7.b(str5, "serialNumber");
        this.id = str;
        this.appId = str2;
        this.name = str3;
        this.description = str4;
        this.createdAt = date;
        this.updatedAt = date2;
        this.majorNumber = i;
        this.minorNumber = i2;
        this.serialNumber = str5;
    }

    @DexIgnore
    public static /* synthetic */ MicroAppVariant copy$default(MicroAppVariant microAppVariant, String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5, int i3, Object obj) {
        return microAppVariant.copy((i3 & 1) != 0 ? microAppVariant.id : str, (i3 & 2) != 0 ? microAppVariant.appId : str2, (i3 & 4) != 0 ? microAppVariant.name : str3, (i3 & 8) != 0 ? microAppVariant.description : str4, (i3 & 16) != 0 ? microAppVariant.createdAt : date, (i3 & 32) != 0 ? microAppVariant.updatedAt : date2, (i3 & 64) != 0 ? microAppVariant.majorNumber : i, (i3 & 128) != 0 ? microAppVariant.minorNumber : i2, (i3 & 256) != 0 ? microAppVariant.serialNumber : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.description;
    }

    @DexIgnore
    public final Date component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component7() {
        return this.majorNumber;
    }

    @DexIgnore
    public final int component8() {
        return this.minorNumber;
    }

    @DexIgnore
    public final String component9() {
        return this.serialNumber;
    }

    @DexIgnore
    public final MicroAppVariant copy(String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5) {
        ee7.b(str, "id");
        ee7.b(str2, "appId");
        ee7.b(str3, "name");
        ee7.b(str4, "description");
        ee7.b(date, "createdAt");
        ee7.b(date2, "updatedAt");
        ee7.b(str5, "serialNumber");
        return new MicroAppVariant(str, str2, str3, str4, date, date2, i, i2, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroAppVariant)) {
            return false;
        }
        MicroAppVariant microAppVariant = (MicroAppVariant) obj;
        return ee7.a(this.id, microAppVariant.id) && ee7.a(this.appId, microAppVariant.appId) && ee7.a(this.name, microAppVariant.name) && ee7.a(this.description, microAppVariant.description) && ee7.a(this.createdAt, microAppVariant.createdAt) && ee7.a(this.updatedAt, microAppVariant.updatedAt) && this.majorNumber == microAppVariant.majorNumber && this.minorNumber == microAppVariant.minorNumber && ee7.a(this.serialNumber, microAppVariant.serialNumber);
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final Date getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final ArrayList<DeclarationFile> getDeclarationFileList() {
        return this.declarationFileList;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMajorNumber() {
        return this.majorNumber;
    }

    @DexIgnore
    public final int getMinorNumber() {
        return this.minorNumber;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final Date getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.appId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.description;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Date date = this.createdAt;
        int hashCode5 = (hashCode4 + (date != null ? date.hashCode() : 0)) * 31;
        Date date2 = this.updatedAt;
        int hashCode6 = (((((hashCode5 + (date2 != null ? date2.hashCode() : 0)) * 31) + this.majorNumber) * 31) + this.minorNumber) * 31;
        String str5 = this.serialNumber;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        ee7.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCreatedAt(Date date) {
        ee7.b(date, "<set-?>");
        this.createdAt = date;
    }

    @DexIgnore
    public final void setDeclarationFileList(ArrayList<DeclarationFile> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.declarationFileList = arrayList;
    }

    @DexIgnore
    public final void setDescription(String str) {
        ee7.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMajorNumber(int i) {
        this.majorNumber = i;
    }

    @DexIgnore
    public final void setMinorNumber(int i) {
        this.minorNumber = i;
    }

    @DexIgnore
    public final void setName(String str) {
        ee7.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        ee7.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(Date date) {
        ee7.b(date, "<set-?>");
        this.updatedAt = date;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppVariant(id=" + this.id + ", appId=" + this.appId + ", name=" + this.name + ", description=" + this.description + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", majorNumber=" + this.majorNumber + ", minorNumber=" + this.minorNumber + ", serialNumber=" + this.serialNumber + ")";
    }
}
