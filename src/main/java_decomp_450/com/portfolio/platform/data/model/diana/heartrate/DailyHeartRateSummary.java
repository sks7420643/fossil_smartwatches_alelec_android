package com.portfolio.platform.data.model.diana.heartrate;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.c;
import com.fossil.ee7;
import com.fossil.zd7;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DailyHeartRateSummary implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float average;
    @DexIgnore
    public Integer avgRestingHeartRateOfWeek;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int minuteCount;
    @DexIgnore
    public Resting resting;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<HeartRateSample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new HeartRateSample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample[] newArray(int i) {
            return new HeartRateSample[i];
        }
    }

    @DexIgnore
    public DailyHeartRateSummary(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2) {
        ee7.b(date2, "date");
        this.average = f;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.min = i;
        this.max = i2;
        this.minuteCount = i3;
        this.resting = resting2;
    }

    @DexIgnore
    public static /* synthetic */ DailyHeartRateSummary copy$default(DailyHeartRateSummary dailyHeartRateSummary, float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2, int i4, Object obj) {
        return dailyHeartRateSummary.copy((i4 & 1) != 0 ? dailyHeartRateSummary.average : f, (i4 & 2) != 0 ? dailyHeartRateSummary.date : date2, (i4 & 4) != 0 ? dailyHeartRateSummary.createdAt : j, (i4 & 8) != 0 ? dailyHeartRateSummary.updatedAt : j2, (i4 & 16) != 0 ? dailyHeartRateSummary.min : i, (i4 & 32) != 0 ? dailyHeartRateSummary.max : i2, (i4 & 64) != 0 ? dailyHeartRateSummary.minuteCount : i3, (i4 & 128) != 0 ? dailyHeartRateSummary.resting : resting2);
    }

    @DexIgnore
    public final float component1() {
        return this.average;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final long component3() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component4() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component5() {
        return this.min;
    }

    @DexIgnore
    public final int component6() {
        return this.max;
    }

    @DexIgnore
    public final int component7() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting component8() {
        return this.resting;
    }

    @DexIgnore
    public final DailyHeartRateSummary copy(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2) {
        ee7.b(date2, "date");
        return new DailyHeartRateSummary(f, date2, j, j2, i, i2, i3, resting2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DailyHeartRateSummary)) {
            return false;
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
        return Float.compare(this.average, dailyHeartRateSummary.average) == 0 && ee7.a(this.date, dailyHeartRateSummary.date) && this.createdAt == dailyHeartRateSummary.createdAt && this.updatedAt == dailyHeartRateSummary.updatedAt && this.min == dailyHeartRateSummary.min && this.max == dailyHeartRateSummary.max && this.minuteCount == dailyHeartRateSummary.minuteCount && ee7.a(this.resting, dailyHeartRateSummary.resting);
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final Integer getAvgRestingHeartRateOfWeek() {
        return this.avgRestingHeartRateOfWeek;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getMinuteCount() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting getResting() {
        return this.resting;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int floatToIntBits = Float.floatToIntBits(this.average) * 31;
        Date date2 = this.date;
        int i = 0;
        int hashCode = (((((((((((floatToIntBits + (date2 != null ? date2.hashCode() : 0)) * 31) + c.a(this.createdAt)) * 31) + c.a(this.updatedAt)) * 31) + this.min) * 31) + this.max) * 31) + this.minuteCount) * 31;
        Resting resting2 = this.resting;
        if (resting2 != null) {
            i = resting2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setAvgRestingHeartRateOfWeek(Integer num) {
        this.avgRestingHeartRateOfWeek = num;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        ee7.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setMinuteCount(int i) {
        this.minuteCount = i;
    }

    @DexIgnore
    public final void setResting(Resting resting2) {
        this.resting = resting2;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "average=" + this.average + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", min=" + this.min + ", max=" + this.max + ", minuteCount=" + this.minuteCount + ", resting=" + this.resting;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeFloat(this.average);
        parcel.writeSerializable(this.date);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeInt(this.min);
        parcel.writeInt(this.max);
        parcel.writeInt(this.minuteCount);
        Resting resting2 = this.resting;
        if (resting2 != null) {
            parcel.writeSerializable(resting2);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DailyHeartRateSummary(float f, Date date2, long j, long j2, int i, int i2, int i3, Resting resting2, int i4, zd7 zd7) {
        this(f, date2, j, j2, i, i2, i3, (i4 & 128) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this(dailyHeartRateSummary.average, dailyHeartRateSummary.date, dailyHeartRateSummary.createdAt, dailyHeartRateSummary.updatedAt, dailyHeartRateSummary.min, dailyHeartRateSummary.max, dailyHeartRateSummary.minuteCount, dailyHeartRateSummary.resting);
        ee7.b(dailyHeartRateSummary, PlaceManager.PARAM_SUMMARY);
        this.avgRestingHeartRateOfWeek = dailyHeartRateSummary.avgRestingHeartRateOfWeek;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DailyHeartRateSummary(android.os.Parcel r13) {
        /*
            r12 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r13, r0)
            float r2 = r13.readFloat()
            java.io.Serializable r0 = r13.readSerializable()
            if (r0 == 0) goto L_0x0032
            r3 = r0
            java.util.Date r3 = (java.util.Date) r3
            long r4 = r13.readLong()
            long r6 = r13.readLong()
            int r8 = r13.readInt()
            int r9 = r13.readInt()
            int r10 = r13.readInt()
            java.io.Serializable r13 = r13.readSerializable()
            r11 = r13
            com.portfolio.platform.data.model.diana.heartrate.Resting r11 = (com.portfolio.platform.data.model.diana.heartrate.Resting) r11
            r1 = r12
            r1.<init>(r2, r3, r4, r6, r8, r9, r10, r11)
            return
        L_0x0032:
            com.fossil.x87 r13 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type java.util.Date"
            r13.<init>(r0)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary.<init>(android.os.Parcel):void");
    }
}
