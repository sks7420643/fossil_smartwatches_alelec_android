package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.ee7;
import com.fossil.w97;
import com.fossil.zd7;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTime {
    @DexIgnore
    public List<Long> activeTimes;
    @DexIgnore
    public int id;

    @DexIgnore
    public GFitActiveTime() {
        this(null, 1, null);
    }

    @DexIgnore
    public GFitActiveTime(List<Long> list) {
        ee7.b(list, "activeTimes");
        this.activeTimes = list;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GFitActiveTime copy$default(GFitActiveTime gFitActiveTime, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = gFitActiveTime.activeTimes;
        }
        return gFitActiveTime.copy(list);
    }

    @DexIgnore
    public final List<Long> component1() {
        return this.activeTimes;
    }

    @DexIgnore
    public final GFitActiveTime copy(List<Long> list) {
        ee7.b(list, "activeTimes");
        return new GFitActiveTime(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof GFitActiveTime) && ee7.a(this.activeTimes, ((GFitActiveTime) obj).activeTimes);
        }
        return true;
    }

    @DexIgnore
    public final List<Long> getActiveTimes() {
        return this.activeTimes;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        List<Long> list = this.activeTimes;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setActiveTimes(List<Long> list) {
        ee7.b(list, "<set-?>");
        this.activeTimes = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitActiveTime(activeTimes=" + this.activeTimes + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitActiveTime(List list, int i, zd7 zd7) {
        this((i & 1) != 0 ? w97.a() : list);
    }
}
