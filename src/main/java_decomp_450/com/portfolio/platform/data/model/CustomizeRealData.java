package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String id;
    @DexIgnore
    public /* final */ String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CustomizeRealData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CustomizeRealData createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new CustomizeRealData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CustomizeRealData[] newArray(int i) {
            return new CustomizeRealData[i];
        }
    }

    @DexIgnore
    public CustomizeRealData(String str, String str2) {
        ee7.b(str, "id");
        ee7.b(str2, "value");
        this.id = str;
        this.value = str2;
    }

    @DexIgnore
    public static /* synthetic */ CustomizeRealData copy$default(CustomizeRealData customizeRealData, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = customizeRealData.id;
        }
        if ((i & 2) != 0) {
            str2 = customizeRealData.value;
        }
        return customizeRealData.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.value;
    }

    @DexIgnore
    public final CustomizeRealData copy(String str, String str2) {
        ee7.b(str, "id");
        ee7.b(str2, "value");
        return new CustomizeRealData(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CustomizeRealData)) {
            return false;
        }
        CustomizeRealData customizeRealData = (CustomizeRealData) obj;
        return ee7.a(this.id, customizeRealData.id) && ee7.a(this.value, customizeRealData.value);
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.value;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setId(String str) {
        ee7.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public String toString() {
        return "CustomizeRealData(id=" + this.id + ", value=" + this.value + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.value);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CustomizeRealData(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r3, r0)
            java.lang.String r0 = r3.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000e
            goto L_0x000f
        L_0x000e:
            r0 = r1
        L_0x000f:
            java.lang.String r3 = r3.readString()
            if (r3 == 0) goto L_0x0016
            r1 = r3
        L_0x0016:
            r2.<init>(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.CustomizeRealData.<init>(android.os.Parcel):void");
    }
}
