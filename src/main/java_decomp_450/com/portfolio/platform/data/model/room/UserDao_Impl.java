package com.portfolio.platform.data.model.room;

import com.fossil.aj;
import com.fossil.ci;
import com.fossil.ji;
import com.fossil.uh;
import com.fossil.vh;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDao_Impl implements UserDao {
    @DexIgnore
    public /* final */ ci __db;
    @DexIgnore
    public /* final */ uh<MFUser> __deletionAdapterOfMFUser;
    @DexIgnore
    public /* final */ vh<MFUser> __insertionAdapterOfMFUser;
    @DexIgnore
    public /* final */ ji __preparedStmtOfClearAllUser;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends vh<MFUser> {
        @DexIgnore
        public Anon1(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `user` (`pinType`,`createdAt`,`updatedAt`,`uid`,`authType`,`birthday`,`brand`,`diagnosticEnabled`,`email`,`emailOptIn`,`emailVerified`,`firstName`,`gender`,`heightInCentimeters`,`integrations`,`lastName`,`profilePicture`,`registerDate`,`registrationComplete`,`useDefaultBiometric`,`isOnboardingComplete`,`useDefaultGoals`,`username`,`averageSleep`,`averageStep`,`activeDeviceId`,`weightInGrams`,`userAccessToken`,`refreshToken`,`accessTokenExpiresAt`,`accessTokenExpiresIn`,`home`,`work`,`distanceUnit`,`weightUnit`,`heightUnit`,`temperatureUnit`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(aj ajVar, MFUser mFUser) {
            if (mFUser.getPinType() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, mFUser.getPinType());
            }
            if (mFUser.getCreatedAt() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, mFUser.getCreatedAt());
            }
            if (mFUser.getUpdatedAt() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, mFUser.getUpdatedAt());
            }
            if (mFUser.getUserId() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, mFUser.getUserId());
            }
            if (mFUser.getAuthType() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, mFUser.getAuthType());
            }
            if (mFUser.getBirthday() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, mFUser.getBirthday());
            }
            if (mFUser.getBrand() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, mFUser.getBrand());
            }
            ajVar.bindLong(8, mFUser.getDiagnosticEnabled() ? 1 : 0);
            if (mFUser.getEmail() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, mFUser.getEmail());
            }
            ajVar.bindLong(10, mFUser.getEmailOptIn() ? 1 : 0);
            ajVar.bindLong(11, mFUser.getEmailVerified() ? 1 : 0);
            if (mFUser.getFirstName() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, mFUser.getFirstName());
            }
            if (mFUser.getGender() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, mFUser.getGender());
            }
            ajVar.bindLong(14, (long) mFUser.getHeightInCentimeters());
            if (mFUser.getIntegrations() == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindString(15, mFUser.getIntegrations());
            }
            if (mFUser.getLastName() == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, mFUser.getLastName());
            }
            if (mFUser.getProfilePicture() == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, mFUser.getProfilePicture());
            }
            if (mFUser.getRegisterDate() == null) {
                ajVar.bindNull(18);
            } else {
                ajVar.bindString(18, mFUser.getRegisterDate());
            }
            ajVar.bindLong(19, mFUser.getRegistrationComplete() ? 1 : 0);
            ajVar.bindLong(20, mFUser.getUseDefaultBiometric() ? 1 : 0);
            ajVar.bindLong(21, mFUser.isOnboardingComplete() ? 1 : 0);
            ajVar.bindLong(22, mFUser.getUseDefaultGoals() ? 1 : 0);
            if (mFUser.getUsername() == null) {
                ajVar.bindNull(23);
            } else {
                ajVar.bindString(23, mFUser.getUsername());
            }
            ajVar.bindLong(24, (long) mFUser.getAverageSleep());
            ajVar.bindLong(25, (long) mFUser.getAverageStep());
            if (mFUser.getActiveDeviceId() == null) {
                ajVar.bindNull(26);
            } else {
                ajVar.bindString(26, mFUser.getActiveDeviceId());
            }
            ajVar.bindLong(27, (long) mFUser.getWeightInGrams());
            MFUser.Auth auth = mFUser.getAuth();
            if (auth != null) {
                if (auth.getAccessToken() == null) {
                    ajVar.bindNull(28);
                } else {
                    ajVar.bindString(28, auth.getAccessToken());
                }
                if (auth.getRefreshToken() == null) {
                    ajVar.bindNull(29);
                } else {
                    ajVar.bindString(29, auth.getRefreshToken());
                }
                if (auth.getAccessTokenExpiresAt() == null) {
                    ajVar.bindNull(30);
                } else {
                    ajVar.bindString(30, auth.getAccessTokenExpiresAt());
                }
                ajVar.bindLong(31, (long) auth.getAccessTokenExpiresIn());
            } else {
                ajVar.bindNull(28);
                ajVar.bindNull(29);
                ajVar.bindNull(30);
                ajVar.bindNull(31);
            }
            MFUser.Address addresses = mFUser.getAddresses();
            if (addresses != null) {
                if (addresses.getHome() == null) {
                    ajVar.bindNull(32);
                } else {
                    ajVar.bindString(32, addresses.getHome());
                }
                if (addresses.getWork() == null) {
                    ajVar.bindNull(33);
                } else {
                    ajVar.bindString(33, addresses.getWork());
                }
            } else {
                ajVar.bindNull(32);
                ajVar.bindNull(33);
            }
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                if (unitGroup.getDistance() == null) {
                    ajVar.bindNull(34);
                } else {
                    ajVar.bindString(34, unitGroup.getDistance());
                }
                if (unitGroup.getWeight() == null) {
                    ajVar.bindNull(35);
                } else {
                    ajVar.bindString(35, unitGroup.getWeight());
                }
                if (unitGroup.getHeight() == null) {
                    ajVar.bindNull(36);
                } else {
                    ajVar.bindString(36, unitGroup.getHeight());
                }
                if (unitGroup.getTemperature() == null) {
                    ajVar.bindNull(37);
                } else {
                    ajVar.bindString(37, unitGroup.getTemperature());
                }
            } else {
                ajVar.bindNull(34);
                ajVar.bindNull(35);
                ajVar.bindNull(36);
                ajVar.bindNull(37);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends uh<MFUser> {
        @DexIgnore
        public Anon2(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "DELETE FROM `user` WHERE `uid` = ?";
        }

        @DexIgnore
        public void bind(aj ajVar, MFUser mFUser) {
            if (mFUser.getUserId() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, mFUser.getUserId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends ji {
        @DexIgnore
        public Anon3(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM user";
        }
    }

    @DexIgnore
    public UserDao_Impl(ci ciVar) {
        this.__db = ciVar;
        this.__insertionAdapterOfMFUser = new Anon1(ciVar);
        this.__deletionAdapterOfMFUser = new Anon2(ciVar);
        this.__preparedStmtOfClearAllUser = new Anon3(ciVar);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void clearAllUser() {
        this.__db.assertNotSuspendingTransaction();
        aj acquire = this.__preparedStmtOfClearAllUser.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllUser.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void deleteUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfMFUser.handle(mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:51:0x021d A[Catch:{ all -> 0x02a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x022b A[Catch:{ all -> 0x02a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0244 A[Catch:{ all -> 0x02a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x025e A[Catch:{ all -> 0x02a7 }] */
    @Override // com.portfolio.platform.data.model.room.UserDao
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.portfolio.platform.data.model.MFUser getCurrentUser() {
        /*
            r67 = this;
            r1 = r67
            r0 = 0
            java.lang.String r2 = "SELECT * FROM user"
            com.fossil.fi r2 = com.fossil.fi.b(r2, r0)
            com.fossil.ci r3 = r1.__db
            r3.assertNotSuspendingTransaction()
            com.fossil.ci r3 = r1.__db
            r4 = 0
            android.database.Cursor r3 = com.fossil.pi.a(r3, r2, r0, r4)
            java.lang.String r5 = "pinType"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a9 }
            java.lang.String r6 = "createdAt"
            int r6 = com.fossil.oi.b(r3, r6)     // Catch:{ all -> 0x02a9 }
            java.lang.String r7 = "updatedAt"
            int r7 = com.fossil.oi.b(r3, r7)     // Catch:{ all -> 0x02a9 }
            java.lang.String r8 = "uid"
            int r8 = com.fossil.oi.b(r3, r8)     // Catch:{ all -> 0x02a9 }
            java.lang.String r9 = "authType"
            int r9 = com.fossil.oi.b(r3, r9)     // Catch:{ all -> 0x02a9 }
            java.lang.String r10 = "birthday"
            int r10 = com.fossil.oi.b(r3, r10)     // Catch:{ all -> 0x02a9 }
            java.lang.String r11 = "brand"
            int r11 = com.fossil.oi.b(r3, r11)     // Catch:{ all -> 0x02a9 }
            java.lang.String r12 = "diagnosticEnabled"
            int r12 = com.fossil.oi.b(r3, r12)     // Catch:{ all -> 0x02a9 }
            java.lang.String r13 = "email"
            int r13 = com.fossil.oi.b(r3, r13)     // Catch:{ all -> 0x02a9 }
            java.lang.String r14 = "emailOptIn"
            int r14 = com.fossil.oi.b(r3, r14)     // Catch:{ all -> 0x02a9 }
            java.lang.String r15 = "emailVerified"
            int r15 = com.fossil.oi.b(r3, r15)     // Catch:{ all -> 0x02a9 }
            java.lang.String r0 = "firstName"
            int r0 = com.fossil.oi.b(r3, r0)     // Catch:{ all -> 0x02a9 }
            java.lang.String r4 = "gender"
            int r4 = com.fossil.oi.b(r3, r4)     // Catch:{ all -> 0x02a9 }
            java.lang.String r1 = "heightInCentimeters"
            int r1 = com.fossil.oi.b(r3, r1)     // Catch:{ all -> 0x02a9 }
            r16 = r2
            java.lang.String r2 = "integrations"
            int r2 = com.fossil.oi.b(r3, r2)     // Catch:{ all -> 0x02a7 }
            r17 = r7
            java.lang.String r7 = "lastName"
            int r7 = com.fossil.oi.b(r3, r7)     // Catch:{ all -> 0x02a7 }
            r18 = r6
            java.lang.String r6 = "profilePicture"
            int r6 = com.fossil.oi.b(r3, r6)     // Catch:{ all -> 0x02a7 }
            r19 = r5
            java.lang.String r5 = "registerDate"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r20 = r5
            java.lang.String r5 = "registrationComplete"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r21 = r5
            java.lang.String r5 = "useDefaultBiometric"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r22 = r5
            java.lang.String r5 = "isOnboardingComplete"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r23 = r5
            java.lang.String r5 = "useDefaultGoals"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r24 = r5
            java.lang.String r5 = "username"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r25 = r5
            java.lang.String r5 = "averageSleep"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r26 = r5
            java.lang.String r5 = "averageStep"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r27 = r5
            java.lang.String r5 = "activeDeviceId"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r28 = r5
            java.lang.String r5 = "weightInGrams"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r29 = r5
            java.lang.String r5 = "userAccessToken"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r30 = r5
            java.lang.String r5 = "refreshToken"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r31 = r5
            java.lang.String r5 = "accessTokenExpiresAt"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r32 = r5
            java.lang.String r5 = "accessTokenExpiresIn"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r33 = r5
            java.lang.String r5 = "home"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r34 = r5
            java.lang.String r5 = "work"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r35 = r5
            java.lang.String r5 = "distanceUnit"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r36 = r5
            java.lang.String r5 = "weightUnit"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r37 = r5
            java.lang.String r5 = "heightUnit"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            r38 = r5
            java.lang.String r5 = "temperatureUnit"
            int r5 = com.fossil.oi.b(r3, r5)     // Catch:{ all -> 0x02a7 }
            boolean r39 = r3.moveToFirst()     // Catch:{ all -> 0x02a7 }
            if (r39 == 0) goto L_0x029f
            java.lang.String r41 = r3.getString(r8)     // Catch:{ all -> 0x02a7 }
            java.lang.String r43 = r3.getString(r9)     // Catch:{ all -> 0x02a7 }
            java.lang.String r44 = r3.getString(r10)     // Catch:{ all -> 0x02a7 }
            java.lang.String r45 = r3.getString(r11)     // Catch:{ all -> 0x02a7 }
            int r8 = r3.getInt(r12)     // Catch:{ all -> 0x02a7 }
            r9 = 1
            if (r8 == 0) goto L_0x0141
            r46 = 1
            goto L_0x0143
        L_0x0141:
            r46 = 0
        L_0x0143:
            java.lang.String r47 = r3.getString(r13)     // Catch:{ all -> 0x02a7 }
            int r8 = r3.getInt(r14)     // Catch:{ all -> 0x02a7 }
            if (r8 == 0) goto L_0x0150
            r48 = 1
            goto L_0x0152
        L_0x0150:
            r48 = 0
        L_0x0152:
            int r8 = r3.getInt(r15)     // Catch:{ all -> 0x02a7 }
            if (r8 == 0) goto L_0x015b
            r49 = 1
            goto L_0x015d
        L_0x015b:
            r49 = 0
        L_0x015d:
            java.lang.String r50 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            java.lang.String r51 = r3.getString(r4)     // Catch:{ all -> 0x02a7 }
            int r52 = r3.getInt(r1)     // Catch:{ all -> 0x02a7 }
            java.lang.String r53 = r3.getString(r2)     // Catch:{ all -> 0x02a7 }
            java.lang.String r54 = r3.getString(r7)     // Catch:{ all -> 0x02a7 }
            java.lang.String r55 = r3.getString(r6)     // Catch:{ all -> 0x02a7 }
            r0 = r20
            java.lang.String r56 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r21
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x0188
            r0 = r22
            r57 = 1
            goto L_0x018c
        L_0x0188:
            r0 = r22
            r57 = 0
        L_0x018c:
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x0197
            r0 = r23
            r59 = 1
            goto L_0x019b
        L_0x0197:
            r0 = r23
            r59 = 0
        L_0x019b:
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x01a6
            r0 = r24
            r60 = 1
            goto L_0x01aa
        L_0x01a6:
            r0 = r24
            r60 = 0
        L_0x01aa:
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            if (r0 == 0) goto L_0x01b5
            r0 = r25
            r61 = 1
            goto L_0x01b9
        L_0x01b5:
            r0 = r25
            r61 = 0
        L_0x01b9:
            java.lang.String r62 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r26
            int r63 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r27
            int r64 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r28
            java.lang.String r65 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r29
            int r66 = r3.getInt(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r30
            boolean r1 = r3.isNull(r0)     // Catch:{ all -> 0x02a7 }
            if (r1 == 0) goto L_0x01fa
            r1 = r31
            boolean r2 = r3.isNull(r1)     // Catch:{ all -> 0x02a7 }
            if (r2 == 0) goto L_0x01fc
            r2 = r32
            boolean r4 = r3.isNull(r2)     // Catch:{ all -> 0x02a7 }
            if (r4 == 0) goto L_0x01fe
            r4 = r33
            boolean r6 = r3.isNull(r4)     // Catch:{ all -> 0x02a7 }
            if (r6 != 0) goto L_0x01f6
            goto L_0x0200
        L_0x01f6:
            r0 = r34
            r6 = 0
            goto L_0x0217
        L_0x01fa:
            r1 = r31
        L_0x01fc:
            r2 = r32
        L_0x01fe:
            r4 = r33
        L_0x0200:
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x02a7 }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x02a7 }
            int r4 = r3.getInt(r4)     // Catch:{ all -> 0x02a7 }
            com.portfolio.platform.data.model.MFUser$Auth r6 = new com.portfolio.platform.data.model.MFUser$Auth     // Catch:{ all -> 0x02a7 }
            r6.<init>(r0, r1, r2, r4)     // Catch:{ all -> 0x02a7 }
            r0 = r34
        L_0x0217:
            boolean r1 = r3.isNull(r0)     // Catch:{ all -> 0x02a7 }
            if (r1 == 0) goto L_0x022b
            r1 = r35
            boolean r2 = r3.isNull(r1)     // Catch:{ all -> 0x02a7 }
            if (r2 != 0) goto L_0x0226
            goto L_0x022d
        L_0x0226:
            r0 = r36
            r42 = 0
            goto L_0x023e
        L_0x022b:
            r1 = r35
        L_0x022d:
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x02a7 }
            com.portfolio.platform.data.model.MFUser$Address r2 = new com.portfolio.platform.data.model.MFUser$Address     // Catch:{ all -> 0x02a7 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x02a7 }
            r42 = r2
            r0 = r36
        L_0x023e:
            boolean r1 = r3.isNull(r0)     // Catch:{ all -> 0x02a7 }
            if (r1 == 0) goto L_0x025e
            r1 = r37
            boolean r2 = r3.isNull(r1)     // Catch:{ all -> 0x02a7 }
            if (r2 == 0) goto L_0x0260
            r2 = r38
            boolean r4 = r3.isNull(r2)     // Catch:{ all -> 0x02a7 }
            if (r4 == 0) goto L_0x0262
            boolean r4 = r3.isNull(r5)     // Catch:{ all -> 0x02a7 }
            if (r4 != 0) goto L_0x025b
            goto L_0x0262
        L_0x025b:
            r58 = 0
            goto L_0x0279
        L_0x025e:
            r1 = r37
        L_0x0260:
            r2 = r38
        L_0x0262:
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x02a7 }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x02a7 }
            java.lang.String r4 = r3.getString(r5)     // Catch:{ all -> 0x02a7 }
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = new com.portfolio.platform.data.model.MFUser$UnitGroup     // Catch:{ all -> 0x02a7 }
            r5.<init>(r0, r1, r2, r4)     // Catch:{ all -> 0x02a7 }
            r58 = r5
        L_0x0279:
            com.portfolio.platform.data.model.MFUser r4 = new com.portfolio.platform.data.model.MFUser     // Catch:{ all -> 0x02a7 }
            r40 = r4
            r40.<init>(r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66)     // Catch:{ all -> 0x02a7 }
            r0 = r19
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r4.setPinType(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r18
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r4.setCreatedAt(r0)     // Catch:{ all -> 0x02a7 }
            r0 = r17
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x02a7 }
            r4.setUpdatedAt(r0)     // Catch:{ all -> 0x02a7 }
            r4.setAuth(r6)     // Catch:{ all -> 0x02a7 }
            goto L_0x02a0
        L_0x029f:
            r4 = 0
        L_0x02a0:
            r3.close()
            r16.c()
            return r4
        L_0x02a7:
            r0 = move-exception
            goto L_0x02ac
        L_0x02a9:
            r0 = move-exception
            r16 = r2
        L_0x02ac:
            r3.close()
            r16.c()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.UserDao_Impl.getCurrentUser():com.portfolio.platform.data.model.MFUser");
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void insertUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert(mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void updateUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert(mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
