package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.xg5;
import com.fossil.zd7;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new Creator();
    @DexIgnore
    public ArrayList<String> androidPermissionSet;
    @DexIgnore
    public String externalLink;
    @DexIgnore
    public boolean isGranted;
    @DexIgnore
    public String longDescription;
    @DexIgnore
    public String permissionGroup;
    @DexIgnore
    public xg5.c settingPermissionId;
    @DexIgnore
    public String shortDescription;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Creator implements Parcelable.Creator {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object createFromParcel(Parcel parcel) {
            ee7.b(parcel, "in");
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            while (readInt != 0) {
                arrayList.add(parcel.readString());
                readInt--;
            }
            return new PermissionData(arrayList, parcel.readInt() != 0 ? (xg5.c) Enum.valueOf(xg5.c.class, parcel.readString()) : null, parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object[] newArray(int i) {
            return new PermissionData[i];
        }
    }

    @DexIgnore
    public PermissionData(ArrayList<String> arrayList, xg5.c cVar, String str, String str2, String str3, String str4, boolean z) {
        ee7.b(arrayList, "androidPermissionSet");
        ee7.b(str, "permissionGroup");
        ee7.b(str2, "shortDescription");
        ee7.b(str3, "longDescription");
        ee7.b(str4, "externalLink");
        this.androidPermissionSet = arrayList;
        this.settingPermissionId = cVar;
        this.permissionGroup = str;
        this.shortDescription = str2;
        this.longDescription = str3;
        this.externalLink = str4;
        this.isGranted = z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.portfolio.platform.data.model.PermissionData */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ PermissionData copy$default(PermissionData permissionData, ArrayList arrayList, xg5.c cVar, String str, String str2, String str3, String str4, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            arrayList = permissionData.androidPermissionSet;
        }
        if ((i & 2) != 0) {
            cVar = permissionData.settingPermissionId;
        }
        if ((i & 4) != 0) {
            str = permissionData.permissionGroup;
        }
        if ((i & 8) != 0) {
            str2 = permissionData.shortDescription;
        }
        if ((i & 16) != 0) {
            str3 = permissionData.longDescription;
        }
        if ((i & 32) != 0) {
            str4 = permissionData.externalLink;
        }
        if ((i & 64) != 0) {
            z = permissionData.isGranted;
        }
        return permissionData.copy(arrayList, cVar, str, str2, str3, str4, z);
    }

    @DexIgnore
    public final ArrayList<String> component1() {
        return this.androidPermissionSet;
    }

    @DexIgnore
    public final xg5.c component2() {
        return this.settingPermissionId;
    }

    @DexIgnore
    public final String component3() {
        return this.permissionGroup;
    }

    @DexIgnore
    public final String component4() {
        return this.shortDescription;
    }

    @DexIgnore
    public final String component5() {
        return this.longDescription;
    }

    @DexIgnore
    public final String component6() {
        return this.externalLink;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isGranted;
    }

    @DexIgnore
    public final PermissionData copy(ArrayList<String> arrayList, xg5.c cVar, String str, String str2, String str3, String str4, boolean z) {
        ee7.b(arrayList, "androidPermissionSet");
        ee7.b(str, "permissionGroup");
        ee7.b(str2, "shortDescription");
        ee7.b(str3, "longDescription");
        ee7.b(str4, "externalLink");
        return new PermissionData(arrayList, cVar, str, str2, str3, str4, z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PermissionData)) {
            return false;
        }
        PermissionData permissionData = (PermissionData) obj;
        return ee7.a(this.androidPermissionSet, permissionData.androidPermissionSet) && ee7.a(this.settingPermissionId, permissionData.settingPermissionId) && ee7.a(this.permissionGroup, permissionData.permissionGroup) && ee7.a(this.shortDescription, permissionData.shortDescription) && ee7.a(this.longDescription, permissionData.longDescription) && ee7.a(this.externalLink, permissionData.externalLink) && this.isGranted == permissionData.isGranted;
    }

    @DexIgnore
    public final ArrayList<String> getAndroidPermissionSet() {
        return this.androidPermissionSet;
    }

    @DexIgnore
    public final String getExternalLink() {
        return this.externalLink;
    }

    @DexIgnore
    public final String getLongDescription() {
        return this.longDescription;
    }

    @DexIgnore
    public final String getPermissionGroup() {
        return this.permissionGroup;
    }

    @DexIgnore
    public final xg5.c getSettingPermissionId() {
        return this.settingPermissionId;
    }

    @DexIgnore
    public final String getShortDescription() {
        return this.shortDescription;
    }

    @DexIgnore
    public int hashCode() {
        ArrayList<String> arrayList = this.androidPermissionSet;
        int i = 0;
        int hashCode = (arrayList != null ? arrayList.hashCode() : 0) * 31;
        xg5.c cVar = this.settingPermissionId;
        int hashCode2 = (hashCode + (cVar != null ? cVar.hashCode() : 0)) * 31;
        String str = this.permissionGroup;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.shortDescription;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.longDescription;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.externalLink;
        if (str4 != null) {
            i = str4.hashCode();
        }
        int i2 = (hashCode5 + i) * 31;
        boolean z = this.isGranted;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public final boolean isGranted() {
        return this.isGranted;
    }

    @DexIgnore
    public final void setAndroidPermissionSet(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.androidPermissionSet = arrayList;
    }

    @DexIgnore
    public final void setExternalLink(String str) {
        ee7.b(str, "<set-?>");
        this.externalLink = str;
    }

    @DexIgnore
    public final void setGranted(boolean z) {
        this.isGranted = z;
    }

    @DexIgnore
    public final void setLongDescription(String str) {
        ee7.b(str, "<set-?>");
        this.longDescription = str;
    }

    @DexIgnore
    public final void setPermissionGroup(String str) {
        ee7.b(str, "<set-?>");
        this.permissionGroup = str;
    }

    @DexIgnore
    public final void setSettingPermissionId(xg5.c cVar) {
        this.settingPermissionId = cVar;
    }

    @DexIgnore
    public final void setShortDescription(String str) {
        ee7.b(str, "<set-?>");
        this.shortDescription = str;
    }

    @DexIgnore
    public String toString() {
        return "PermissionData(androidPermissionSet=" + this.androidPermissionSet + ", settingPermissionId=" + this.settingPermissionId + ", permissionGroup=" + this.permissionGroup + ", shortDescription=" + this.shortDescription + ", longDescription=" + this.longDescription + ", externalLink=" + this.externalLink + ", isGranted=" + this.isGranted + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        ArrayList<String> arrayList = this.androidPermissionSet;
        parcel.writeInt(arrayList.size());
        for (String str : arrayList) {
            parcel.writeString(str);
        }
        xg5.c cVar = this.settingPermissionId;
        if (cVar != null) {
            parcel.writeInt(1);
            parcel.writeString(cVar.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(this.permissionGroup);
        parcel.writeString(this.shortDescription);
        parcel.writeString(this.longDescription);
        parcel.writeString(this.externalLink);
        parcel.writeInt(this.isGranted ? 1 : 0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PermissionData(ArrayList arrayList, xg5.c cVar, String str, String str2, String str3, String str4, boolean z, int i, zd7 zd7) {
        this(arrayList, cVar, str, (i & 8) != 0 ? "" : str2, (i & 16) != 0 ? "" : str3, str4, z);
    }
}
