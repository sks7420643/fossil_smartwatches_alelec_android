package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.ge5;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.zd7;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityIntensities implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public double intense;
    @DexIgnore
    public double light;
    @DexIgnore
    public double moderate;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ActivityIntensities> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivityIntensities createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new ActivityIntensities(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivityIntensities[] newArray(int i) {
            return new ActivityIntensities[i];
        }
    }

    @DexIgnore
    public ActivityIntensities(double d, double d2, double d3) {
        this.light = d;
        this.moderate = d2;
        this.intense = d3;
    }

    @DexIgnore
    public static /* synthetic */ ActivityIntensities copy$default(ActivityIntensities activityIntensities, double d, double d2, double d3, int i, Object obj) {
        if ((i & 1) != 0) {
            d = activityIntensities.light;
        }
        if ((i & 2) != 0) {
            d2 = activityIntensities.moderate;
        }
        if ((i & 4) != 0) {
            d3 = activityIntensities.intense;
        }
        return activityIntensities.copy(d, d2, d3);
    }

    @DexIgnore
    public final double component1() {
        return this.light;
    }

    @DexIgnore
    public final double component2() {
        return this.moderate;
    }

    @DexIgnore
    public final double component3() {
        return this.intense;
    }

    @DexIgnore
    public final ActivityIntensities copy(double d, double d2, double d3) {
        return new ActivityIntensities(d, d2, d3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityIntensities)) {
            return false;
        }
        ActivityIntensities activityIntensities = (ActivityIntensities) obj;
        return Double.compare(this.light, activityIntensities.light) == 0 && Double.compare(this.moderate, activityIntensities.moderate) == 0 && Double.compare(this.intense, activityIntensities.intense) == 0;
    }

    @DexIgnore
    public final double getIntense() {
        return this.intense;
    }

    @DexIgnore
    public final List<Integer> getIntensitiesInArray() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf((int) this.light));
        arrayList.add(Integer.valueOf((int) this.moderate));
        arrayList.add(Integer.valueOf((int) this.intense));
        return arrayList;
    }

    @DexIgnore
    public final double getLight() {
        return this.light;
    }

    @DexIgnore
    public final double getModerate() {
        return this.moderate;
    }

    @DexIgnore
    public int hashCode() {
        return (((Double.doubleToLongBits(this.light) * 31) + Double.doubleToLongBits(this.moderate)) * 31) + Double.doubleToLongBits(this.intense);
    }

    @DexIgnore
    public final void setIntense(double d) {
        this.intense = d;
    }

    @DexIgnore
    public final void setIntensity(double d) {
        int a = ge5.c.a((long) d);
        if (a == 0) {
            this.light += d;
        } else if (a == 1) {
            this.moderate += d;
        } else if (a == 2) {
            this.intense += d;
        }
    }

    @DexIgnore
    public final void setLight(double d) {
        this.light = d;
    }

    @DexIgnore
    public final void setModerate(double d) {
        this.moderate = d;
    }

    @DexIgnore
    public String toString() {
        return "ActivityIntensities(light=" + this.light + ", moderate=" + this.moderate + ", intense=" + this.intense + ")";
    }

    @DexIgnore
    public final void updateActivityIntensities(ActivityIntensities activityIntensities) {
        ee7.b(activityIntensities, SampleDay.COLUMN_INTENSITIES);
        this.light += activityIntensities.light;
        this.moderate += activityIntensities.moderate;
        this.intense += activityIntensities.intense;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeDouble(this.light);
        parcel.writeDouble(this.moderate);
        parcel.writeDouble(this.intense);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ActivityIntensities(double d, double d2, double d3, int i, zd7 zd7) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) != 0 ? 0.0d : d2, (i & 4) != 0 ? 0.0d : d3);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityIntensities(Parcel parcel) {
        this(parcel.readDouble(), parcel.readDouble(), parcel.readDouble());
        ee7.b(parcel, "parcel");
    }

    @DexIgnore
    public ActivityIntensities() {
        this(0.0d, 0.0d, 0.0d);
    }
}
