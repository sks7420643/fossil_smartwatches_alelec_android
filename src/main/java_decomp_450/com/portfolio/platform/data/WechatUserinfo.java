package com.portfolio.platform.data;

import com.facebook.appevents.UserDataStore;
import com.fossil.ee7;
import com.fossil.fb5;
import com.fossil.te4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatUserinfo {
    @DexIgnore
    @te4("city")
    public /* final */ String mCity;
    @DexIgnore
    @te4(UserDataStore.COUNTRY)
    public /* final */ String mCountry;
    @DexIgnore
    @te4("headimgurl")
    public /* final */ String mHeadimgurl;
    @DexIgnore
    @te4("nickname")
    public /* final */ String mNickname;
    @DexIgnore
    @te4("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @te4("privilege")
    public /* final */ List<String> mPrivilege;
    @DexIgnore
    @te4("province")
    public /* final */ String mProvince;
    @DexIgnore
    @te4("sex")
    public /* final */ int mSex;
    @DexIgnore
    @te4("unionId")
    public /* final */ String mUnionId;

    @DexIgnore
    public WechatUserinfo(String str, String str2, int i, String str3, String str4, String str5, String str6, List<String> list, String str7) {
        ee7.b(str, "mOpenid");
        ee7.b(str2, "mNickname");
        ee7.b(str3, "mCity");
        ee7.b(str4, "mProvince");
        ee7.b(str5, "mCountry");
        ee7.b(str6, "mHeadimgurl");
        ee7.b(list, "mPrivilege");
        ee7.b(str7, "mUnionId");
        this.mOpenid = str;
        this.mNickname = str2;
        this.mSex = i;
        this.mCity = str3;
        this.mProvince = str4;
        this.mCountry = str5;
        this.mHeadimgurl = str6;
        this.mPrivilege = list;
        this.mUnionId = str7;
    }

    @DexIgnore
    public final String getHeadimgurl() {
        return this.mHeadimgurl;
    }

    @DexIgnore
    public final String getSex() {
        int i = this.mSex;
        if (i == 1) {
            return fb5.MALE.toString();
        }
        if (i != 2) {
            return fb5.OTHER.toString();
        }
        return fb5.FEMALE.toString();
    }
}
