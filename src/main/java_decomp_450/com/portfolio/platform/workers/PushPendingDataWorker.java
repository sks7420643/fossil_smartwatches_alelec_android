package com.portfolio.platform.workers;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.CoroutineWorker;
import androidx.work.WorkerParameters;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.VideoUploader;
import com.fossil.a07;
import com.fossil.ai7;
import com.fossil.am;
import com.fossil.ch5;
import com.fossil.dm;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ie4;
import com.fossil.ik7;
import com.fossil.jm;
import com.fossil.kd7;
import com.fossil.km;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qj7;
import com.fossil.rb7;
import com.fossil.rm;
import com.fossil.ro4;
import com.fossil.s87;
import com.fossil.se7;
import com.fossil.sm;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.to4;
import com.fossil.vh7;
import com.fossil.xh7;
import com.fossil.xo4;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PushPendingDataWorker extends CoroutineWorker {
    @DexIgnore
    public static /* final */ a G; // = new a(null);
    @DexIgnore
    public /* final */ xo4 A;
    @DexIgnore
    public /* final */ FileRepository B;
    @DexIgnore
    public /* final */ UserRepository C;
    @DexIgnore
    public /* final */ WorkoutSettingRepository D;
    @DexIgnore
    public /* final */ to4 E;
    @DexIgnore
    public /* final */ PortfolioApp F;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ SleepSessionsRepository j;
    @DexIgnore
    public /* final */ SleepSummariesRepository p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;
    @DexIgnore
    public /* final */ HeartRateSampleRepository r;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository s;
    @DexIgnore
    public /* final */ FitnessDataRepository t;
    @DexIgnore
    public /* final */ AlarmsRepository u;
    @DexIgnore
    public /* final */ ch5 v;
    @DexIgnore
    public /* final */ DianaPresetRepository w;
    @DexIgnore
    public /* final */ HybridPresetRepository x;
    @DexIgnore
    public /* final */ ThirdPartyRepository y;
    @DexIgnore
    public /* final */ ro4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a() {
            km.a aVar = new km.a(PushPendingDataWorker.class);
            am.a aVar2 = new am.a();
            aVar2.a(jm.CONNECTED);
            am a = aVar2.a();
            ee7.a((Object) a, "Constraints.Builder().se\u2026rkType.CONNECTED).build()");
            aVar.a(a);
            sm a2 = aVar.a();
            ee7.a((Object) a2, "uploadBuilder.build()");
            km kmVar = (km) a2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "startScheduleUploadPendingData() - id = " + kmVar.a());
            rm.a(PortfolioApp.g0.c()).a("PushPendingDataWorker", dm.KEEP, kmVar);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements a07<PushPendingDataWorker> {
        @DexIgnore
        public /* final */ Provider<ActivitiesRepository> a;
        @DexIgnore
        public /* final */ Provider<SummariesRepository> b;
        @DexIgnore
        public /* final */ Provider<SleepSessionsRepository> c;
        @DexIgnore
        public /* final */ Provider<SleepSummariesRepository> d;
        @DexIgnore
        public /* final */ Provider<GoalTrackingRepository> e;
        @DexIgnore
        public /* final */ Provider<HeartRateSampleRepository> f;
        @DexIgnore
        public /* final */ Provider<HeartRateSummaryRepository> g;
        @DexIgnore
        public /* final */ Provider<FitnessDataRepository> h;
        @DexIgnore
        public /* final */ Provider<AlarmsRepository> i;
        @DexIgnore
        public /* final */ Provider<ch5> j;
        @DexIgnore
        public /* final */ Provider<DianaPresetRepository> k;
        @DexIgnore
        public /* final */ Provider<HybridPresetRepository> l;
        @DexIgnore
        public /* final */ Provider<ThirdPartyRepository> m;
        @DexIgnore
        public /* final */ Provider<ro4> n;
        @DexIgnore
        public /* final */ Provider<xo4> o;
        @DexIgnore
        public /* final */ Provider<FileRepository> p;
        @DexIgnore
        public /* final */ Provider<UserRepository> q;
        @DexIgnore
        public /* final */ Provider<WorkoutSettingRepository> r;
        @DexIgnore
        public /* final */ Provider<to4> s;
        @DexIgnore
        public /* final */ Provider<PortfolioApp> t;

        @DexIgnore
        public b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<ch5> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<ro4> provider14, Provider<xo4> provider15, Provider<FileRepository> provider16, Provider<UserRepository> provider17, Provider<WorkoutSettingRepository> provider18, Provider<to4> provider19, Provider<PortfolioApp> provider20) {
            ee7.b(provider, "mActivitiesRepository");
            ee7.b(provider2, "mSummariesRepository");
            ee7.b(provider3, "mSleepSessionRepository");
            ee7.b(provider4, "mSleepSummariesRepository");
            ee7.b(provider5, "mGoalTrackingRepository");
            ee7.b(provider6, "mHeartRateSampleRepository");
            ee7.b(provider7, "mHeartRateSummaryRepository");
            ee7.b(provider8, "mFitnessDataRepository");
            ee7.b(provider9, "mAlarmsRepository");
            ee7.b(provider10, "mSharedPreferencesManager");
            ee7.b(provider11, "mDianaPresetRepository");
            ee7.b(provider12, "mHybridPresetRepository");
            ee7.b(provider13, "mThirdPartyRepository");
            ee7.b(provider14, "mChallengeRepository");
            ee7.b(provider15, "friendRepository");
            ee7.b(provider16, "mFileRepository");
            ee7.b(provider17, "mUserRepository");
            ee7.b(provider18, "mWorkoutSettingRepository");
            ee7.b(provider19, "fcmRepository");
            ee7.b(provider20, "mApp");
            this.a = provider;
            this.b = provider2;
            this.c = provider3;
            this.d = provider4;
            this.e = provider5;
            this.f = provider6;
            this.g = provider7;
            this.h = provider8;
            this.i = provider9;
            this.j = provider10;
            this.k = provider11;
            this.l = provider12;
            this.m = provider13;
            this.n = provider14;
            this.o = provider15;
            this.p = provider16;
            this.q = provider17;
            this.r = provider18;
            this.s = provider19;
            this.t = provider20;
        }

        @DexIgnore
        @Override // com.fossil.a07
        public PushPendingDataWorker a(Context context, WorkerParameters workerParameters) {
            ee7.b(context, "context");
            ee7.b(workerParameters, "parameterName");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "Factory - create()");
            ActivitiesRepository activitiesRepository = this.a.get();
            ee7.a((Object) activitiesRepository, "mActivitiesRepository.get()");
            SummariesRepository summariesRepository = this.b.get();
            ee7.a((Object) summariesRepository, "mSummariesRepository.get()");
            SleepSessionsRepository sleepSessionsRepository = this.c.get();
            ee7.a((Object) sleepSessionsRepository, "mSleepSessionRepository.get()");
            SleepSummariesRepository sleepSummariesRepository = this.d.get();
            ee7.a((Object) sleepSummariesRepository, "mSleepSummariesRepository.get()");
            GoalTrackingRepository goalTrackingRepository = this.e.get();
            ee7.a((Object) goalTrackingRepository, "mGoalTrackingRepository.get()");
            HeartRateSampleRepository heartRateSampleRepository = this.f.get();
            ee7.a((Object) heartRateSampleRepository, "mHeartRateSampleRepository.get()");
            HeartRateSummaryRepository heartRateSummaryRepository = this.g.get();
            ee7.a((Object) heartRateSummaryRepository, "mHeartRateSummaryRepository.get()");
            FitnessDataRepository fitnessDataRepository = this.h.get();
            ee7.a((Object) fitnessDataRepository, "mFitnessDataRepository.get()");
            AlarmsRepository alarmsRepository = this.i.get();
            ee7.a((Object) alarmsRepository, "mAlarmsRepository.get()");
            ch5 ch5 = this.j.get();
            ee7.a((Object) ch5, "mSharedPreferencesManager.get()");
            DianaPresetRepository dianaPresetRepository = this.k.get();
            ee7.a((Object) dianaPresetRepository, "mDianaPresetRepository.get()");
            HybridPresetRepository hybridPresetRepository = this.l.get();
            ee7.a((Object) hybridPresetRepository, "mHybridPresetRepository.get()");
            ThirdPartyRepository thirdPartyRepository = this.m.get();
            ee7.a((Object) thirdPartyRepository, "mThirdPartyRepository.get()");
            ro4 ro4 = this.n.get();
            ee7.a((Object) ro4, "mChallengeRepository.get()");
            ro4 ro42 = ro4;
            xo4 xo4 = this.o.get();
            ee7.a((Object) xo4, "friendRepository.get()");
            xo4 xo42 = xo4;
            FileRepository fileRepository = this.p.get();
            ee7.a((Object) fileRepository, "mFileRepository.get()");
            FileRepository fileRepository2 = fileRepository;
            UserRepository userRepository = this.q.get();
            ee7.a((Object) userRepository, "mUserRepository.get()");
            UserRepository userRepository2 = userRepository;
            WorkoutSettingRepository workoutSettingRepository = this.r.get();
            ee7.a((Object) workoutSettingRepository, "mWorkoutSettingRepository.get()");
            WorkoutSettingRepository workoutSettingRepository2 = workoutSettingRepository;
            to4 to4 = this.s.get();
            ee7.a((Object) to4, "fcmRepository.get()");
            to4 to42 = to4;
            PortfolioApp portfolioApp = this.t.get();
            ee7.a((Object) portfolioApp, "mApp.get()");
            return new PushPendingDataWorker(workerParameters, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, goalTrackingRepository, heartRateSampleRepository, heartRateSummaryRepository, fitnessDataRepository, alarmsRepository, ch5, dianaPresetRepository, hybridPresetRepository, thirdPartyRepository, ro42, xo42, fileRepository2, userRepository2, workoutSettingRepository2, to42, portfolioApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {96}, m = "doWork")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PushPendingDataWorker pushPendingDataWorker, fb7 fb7) {
            super(fb7);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ActivitiesRepository.PushPendingActivitiesCallback {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$d$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$d$a$a  reason: collision with other inner class name */
            public static final class C0316a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ se7 $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ se7 $startDate;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0316a(a aVar, se7 se7, se7 se72, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$startDate = se7;
                    this.$endDate = se72;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0316a aVar = new C0316a(this.this$0, this.$startDate, this.$endDate, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0316a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        SummariesRepository g = this.this$0.a.this$0.i;
                        Date date = this.$startDate.element.toLocalDateTime().toDate();
                        ee7.a((Object) date, "startDate.toLocalDateTime().toDate()");
                        Date date2 = this.$endDate.element.toLocalDateTime().toDate();
                        ee7.a((Object) date2, "endDate.toLocalDateTime().toDate()");
                        this.L$0 = yi7;
                        this.label = 1;
                        if (g.loadSummaries(date, date2, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingActivities onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    ai7 ai7 = this.a.$cancellableContinuation;
                    s87.a aVar = s87.Companion;
                    ai7.resumeWith(s87.m60constructorimpl(false));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onSuccess(List<ActivitySample> list) {
                ee7.b(list, "activityList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingActivities onSuccess, go to next");
                if (!list.isEmpty()) {
                    se7 se7 = new se7();
                    se7.element = (T) list.get(0).getStartTime();
                    se7 se72 = new se7();
                    se72.element = (T) list.get(0).getEndTime();
                    for (ActivitySample activitySample : list) {
                        if (activitySample.getStartTime().getMillis() < se7.element.getMillis()) {
                            se7.element = (T) activitySample.getStartTime();
                        }
                        if (activitySample.getEndTime().getMillis() < se72.element.getMillis()) {
                            se72.element = (T) activitySample.getEndTime();
                        }
                    }
                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new C0316a(this, se7, se72, null), 3, null);
                }
                if (this.a.$cancellableContinuation.isActive()) {
                    ai7 ai7 = this.a.$cancellableContinuation;
                    s87.a aVar = s87.Companion;
                    ai7.resumeWith(s87.m60constructorimpl(true));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ai7 ai7, fb7 fb7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, fb7);
            this.$cancellableContinuation = ai7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.$cancellableContinuation, fb7, this.this$0);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ActivitiesRepository a3 = this.this$0.h;
                a aVar = new a(this);
                this.L$0 = yi7;
                this.label = 1;
                if (a3.pushPendingActivities(aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements SleepSessionsRepository.PushPendingSleepSessionsCallback {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a  reason: collision with other inner class name */
            public static final class C0317a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $sleepSessionList;
                @DexIgnore
                public int I$0;
                @DexIgnore
                public int I$1;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a  reason: collision with other inner class name */
                public static final class C0318a extends zb7 implements kd7<yi7, fb7<? super zi5<ie4>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $end;
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $start;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0317a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0318a(C0317a aVar, Calendar calendar, Calendar calendar2, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                        this.$start = calendar;
                        this.$end = calendar2;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0318a aVar = new C0318a(this.this$0, this.$start, this.$end, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super zi5<ie4>> fb7) {
                        return ((C0318a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            SleepSummariesRepository f = this.this$0.this$0.a.this$0.p;
                            Calendar calendar = this.$start;
                            ee7.a((Object) calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            Date time = calendar.getTime();
                            ee7.a((Object) time, "start.time");
                            Calendar calendar2 = this.$end;
                            ee7.a((Object) calendar2, "end");
                            Date time2 = calendar2.getTime();
                            ee7.a((Object) time2, "end.time");
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = f.fetchSleepSummaries(time, time2, this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0317a(a aVar, List list, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$sleepSessionList = list;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0317a aVar = new C0317a(this.this$0, this.$sleepSessionList, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0317a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        if (!this.$sleepSessionList.isEmpty()) {
                            int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                            int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                            for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                                if (mFSleepSession.getRealStartTime() < realStartTime) {
                                    realStartTime = mFSleepSession.getRealStartTime();
                                }
                                if (mFSleepSession.getRealEndTime() > realEndTime) {
                                    realEndTime = mFSleepSession.getRealEndTime();
                                }
                            }
                            Calendar instance = Calendar.getInstance();
                            ee7.a((Object) instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            instance.setTimeInMillis((long) realStartTime);
                            Calendar instance2 = Calendar.getInstance();
                            ee7.a((Object) instance2, "end");
                            instance2.setTimeInMillis((long) realEndTime);
                            ti7 b = qj7.b();
                            C0318a aVar = new C0318a(this, instance, instance2, null);
                            this.L$0 = yi7;
                            this.I$0 = realStartTime;
                            this.I$1 = realEndTime;
                            this.L$1 = instance;
                            this.L$2 = instance2;
                            this.label = 1;
                            if (vh7.a(b, aVar, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        Calendar calendar = (Calendar) this.L$2;
                        Calendar calendar2 = (Calendar) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        ai7 ai7 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = pb7.a(true);
                        s87.a aVar2 = s87.Companion;
                        ai7.resumeWith(s87.m60constructorimpl(a2));
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingSleepSessions onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    ai7 ai7 = this.a.$cancellableContinuation;
                    s87.a aVar = s87.Companion;
                    ai7.resumeWith(s87.m60constructorimpl(true));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onSuccess(List<MFSleepSession> list) {
                ee7.b(list, "sleepSessionList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingSleepSessions onSuccess, go to next");
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new C0317a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ai7 ai7, fb7 fb7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, fb7);
            this.$cancellableContinuation = ai7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.$cancellableContinuation, fb7, this.this$0);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                SleepSessionsRepository e = this.this$0.j;
                a aVar = new a(this);
                this.L$0 = yi7;
                this.label = 1;
                if (e.pushPendingSleepSessions(aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ai7 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a  reason: collision with other inner class name */
            public static final class C0319a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $goalTrackingList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a  reason: collision with other inner class name */
                public static final class C0320a extends zb7 implements kd7<yi7, fb7<? super zi5<ApiResponse<GoalDailySummary>>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ se7 $endDate;
                    @DexIgnore
                    public /* final */ /* synthetic */ se7 $startDate;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0319a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0320a(C0319a aVar, se7 se7, se7 se72, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                        this.$startDate = se7;
                        this.$endDate = se72;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0320a aVar = new C0320a(this.this$0, this.$startDate, this.$endDate, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super zi5<ApiResponse<GoalDailySummary>>> fb7) {
                        return ((C0320a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            this.L$0 = this.p$;
                            this.label = 1;
                            obj = this.this$0.this$0.a.this$0.q.loadSummaries(this.$startDate.element, this.$endDate.element, this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi7 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0319a(a aVar, List list, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$goalTrackingList = list;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0319a aVar = new C0319a(this.this$0, this.$goalTrackingList, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0319a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        se7 se7 = new se7();
                        se7.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        se7 se72 = new se7();
                        se72.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        for (GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                            if (goalTrackingData.getDate().getTime() < se7.element.getTime()) {
                                se7.element = (T) goalTrackingData.getDate();
                            }
                            if (goalTrackingData.getDate().getTime() > se72.element.getTime()) {
                                se72.element = (T) goalTrackingData.getDate();
                            }
                        }
                        ti7 b = qj7.b();
                        C0320a aVar = new C0320a(this, se7, se72, null);
                        this.L$0 = yi7;
                        this.L$1 = se7;
                        this.L$2 = se72;
                        this.label = 1;
                        if (vh7.a(b, aVar, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        se7 se73 = (se7) this.L$2;
                        se7 se74 = (se7) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        ai7 ai7 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = pb7.a(true);
                        s87.a aVar2 = s87.Companion;
                        ai7.resumeWith(s87.m60constructorimpl(a2));
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    ai7 ai7 = this.a.$cancellableContinuation;
                    s87.a aVar = s87.Companion;
                    ai7.resumeWith(s87.m60constructorimpl(false));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onSuccess(List<GoalTrackingData> list) {
                ee7.b(list, "goalTrackingList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onSuccess, go to next");
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new C0319a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ai7 ai7, fb7 fb7, PushPendingDataWorker pushPendingDataWorker) {
            super(2, fb7);
            this.$cancellableContinuation = ai7;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.$cancellableContinuation, fb7, this.this$0);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                GoalTrackingRepository b = this.this$0.q;
                a aVar = new a(this);
                this.L$0 = yi7;
                this.label = 1;
                if (b.pushPendingGoalTrackingDataList(aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {106, 290, 298, 184, 185, 187, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 192, 226, 229, 230, 306, 264, 266, 272}, m = VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE)
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PushPendingDataWorker pushPendingDataWorker, fb7 fb7) {
            super(fb7);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$4", f = "PushPendingDataWorker.kt", l = {208, 209, 211, 212, 214, 215, 216}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ se7 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $startDate;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PushPendingDataWorker pushPendingDataWorker, se7 se7, se7 se72, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pushPendingDataWorker;
            this.$startDate = se7;
            this.$endDate = se72;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$startDate, this.$endDate, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00b5 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00e7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0113 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0145 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0171 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0183 A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                java.lang.String r2 = "endDate.toDate()"
                java.lang.String r3 = "startDate.toDate()"
                switch(r1) {
                    case 0: goto L_0x0052;
                    case 1: goto L_0x004a;
                    case 2: goto L_0x0042;
                    case 3: goto L_0x0039;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0027;
                    case 6: goto L_0x001e;
                    case 7: goto L_0x0015;
                    default: goto L_0x000d;
                }
            L_0x000d:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0015:
                java.lang.Object r0 = r12.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r13)
                goto L_0x0184
            L_0x001e:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0172
            L_0x0027:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0146
            L_0x0030:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0114
            L_0x0039:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x00e8
            L_0x0042:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x00b6
            L_0x004a:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x008a
            L_0x0052:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.portfolio.platform.workers.PushPendingDataWorker r1 = r12.this$0
                com.portfolio.platform.data.source.ActivitiesRepository r4 = r1.h
                com.fossil.se7 r1 = r12.$startDate
                T r1 = r1.element
                org.joda.time.DateTime r1 = (org.joda.time.DateTime) r1
                java.util.Date r5 = r1.toDate()
                com.fossil.ee7.a(r5, r3)
                com.fossil.se7 r1 = r12.$endDate
                T r1 = r1.element
                org.joda.time.DateTime r1 = (org.joda.time.DateTime) r1
                java.util.Date r6 = r1.toDate()
                com.fossil.ee7.a(r6, r2)
                r7 = 0
                r8 = 0
                r10 = 12
                r11 = 0
                r12.L$0 = r13
                r1 = 1
                r12.label = r1
                r9 = r12
                java.lang.Object r1 = com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(r4, r5, r6, r7, r8, r9, r10, r11)
                if (r1 != r0) goto L_0x0089
                return r0
            L_0x0089:
                r1 = r13
            L_0x008a:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.SummariesRepository r13 = r13.i
                com.fossil.se7 r4 = r12.$startDate
                T r4 = r4.element
                org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                java.util.Date r4 = r4.toDate()
                com.fossil.ee7.a(r4, r3)
                com.fossil.se7 r5 = r12.$endDate
                T r5 = r5.element
                org.joda.time.DateTime r5 = (org.joda.time.DateTime) r5
                java.util.Date r5 = r5.toDate()
                com.fossil.ee7.a(r5, r2)
                r12.L$0 = r1
                r6 = 2
                r12.label = r6
                java.lang.Object r13 = r13.loadSummaries(r4, r5, r12)
                if (r13 != r0) goto L_0x00b6
                return r0
            L_0x00b6:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.SleepSessionsRepository r4 = r13.j
                com.fossil.se7 r13 = r12.$startDate
                T r13 = r13.element
                org.joda.time.DateTime r13 = (org.joda.time.DateTime) r13
                java.util.Date r5 = r13.toDate()
                com.fossil.ee7.a(r5, r3)
                com.fossil.se7 r13 = r12.$endDate
                T r13 = r13.element
                org.joda.time.DateTime r13 = (org.joda.time.DateTime) r13
                java.util.Date r6 = r13.toDate()
                com.fossil.ee7.a(r6, r2)
                r7 = 0
                r8 = 0
                r10 = 12
                r11 = 0
                r12.L$0 = r1
                r13 = 3
                r12.label = r13
                r9 = r12
                java.lang.Object r13 = com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r4, r5, r6, r7, r8, r9, r10, r11)
                if (r13 != r0) goto L_0x00e8
                return r0
            L_0x00e8:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r13 = r13.p
                com.fossil.se7 r4 = r12.$startDate
                T r4 = r4.element
                org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                java.util.Date r4 = r4.toDate()
                com.fossil.ee7.a(r4, r3)
                com.fossil.se7 r5 = r12.$endDate
                T r5 = r5.element
                org.joda.time.DateTime r5 = (org.joda.time.DateTime) r5
                java.util.Date r5 = r5.toDate()
                com.fossil.ee7.a(r5, r2)
                r12.L$0 = r1
                r6 = 4
                r12.label = r6
                java.lang.Object r13 = r13.fetchSleepSummaries(r4, r5, r12)
                if (r13 != r0) goto L_0x0114
                return r0
            L_0x0114:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.HeartRateSampleRepository r4 = r13.r
                com.fossil.se7 r13 = r12.$startDate
                T r13 = r13.element
                org.joda.time.DateTime r13 = (org.joda.time.DateTime) r13
                java.util.Date r5 = r13.toDate()
                com.fossil.ee7.a(r5, r3)
                com.fossil.se7 r13 = r12.$endDate
                T r13 = r13.element
                org.joda.time.DateTime r13 = (org.joda.time.DateTime) r13
                java.util.Date r6 = r13.toDate()
                com.fossil.ee7.a(r6, r2)
                r7 = 0
                r8 = 0
                r10 = 12
                r11 = 0
                r12.L$0 = r1
                r13 = 5
                r12.label = r13
                r9 = r12
                java.lang.Object r13 = com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r4, r5, r6, r7, r8, r9, r10, r11)
                if (r13 != r0) goto L_0x0146
                return r0
            L_0x0146:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.HeartRateSummaryRepository r13 = r13.s
                com.fossil.se7 r4 = r12.$startDate
                T r4 = r4.element
                org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                java.util.Date r4 = r4.toDate()
                com.fossil.ee7.a(r4, r3)
                com.fossil.se7 r3 = r12.$endDate
                T r3 = r3.element
                org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                java.util.Date r3 = r3.toDate()
                com.fossil.ee7.a(r3, r2)
                r12.L$0 = r1
                r2 = 6
                r12.label = r2
                java.lang.Object r13 = r13.loadSummaries(r4, r3, r12)
                if (r13 != r0) goto L_0x0172
                return r0
            L_0x0172:
                com.portfolio.platform.workers.PushPendingDataWorker r13 = r12.this$0
                com.portfolio.platform.data.source.SummariesRepository r13 = r13.i
                r12.L$0 = r1
                r1 = 7
                r12.label = r1
                java.lang.Object r13 = r13.fetchActivityStatistic(r12)
                if (r13 != r0) goto L_0x0184
                return r0
            L_0x0184:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
                switch-data {0->0x0052, 1->0x004a, 2->0x0042, 3->0x0039, 4->0x0030, 5->0x0027, 6->0x001e, 7->0x0015, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ThirdPartyRepository.PushPendingThirdPartyDataCallback {
        @DexIgnore
        @Override // com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback
        public void onComplete() {
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushThirdPartyPendingData - Complete");
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PushPendingDataWorker(androidx.work.WorkerParameters r17, com.portfolio.platform.data.source.ActivitiesRepository r18, com.portfolio.platform.data.source.SummariesRepository r19, com.portfolio.platform.data.source.SleepSessionsRepository r20, com.portfolio.platform.data.source.SleepSummariesRepository r21, com.portfolio.platform.data.source.GoalTrackingRepository r22, com.portfolio.platform.data.source.HeartRateSampleRepository r23, com.portfolio.platform.data.source.HeartRateSummaryRepository r24, com.portfolio.platform.data.source.FitnessDataRepository r25, com.portfolio.platform.data.source.AlarmsRepository r26, com.fossil.ch5 r27, com.portfolio.platform.data.source.DianaPresetRepository r28, com.portfolio.platform.data.source.HybridPresetRepository r29, com.portfolio.platform.data.source.ThirdPartyRepository r30, com.fossil.ro4 r31, com.fossil.xo4 r32, com.portfolio.platform.data.source.FileRepository r33, com.portfolio.platform.data.source.UserRepository r34, com.portfolio.platform.data.source.WorkoutSettingRepository r35, com.fossil.to4 r36, com.portfolio.platform.PortfolioApp r37) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r22
            r7 = r23
            r8 = r24
            r9 = r25
            r10 = r26
            r11 = r27
            r12 = r28
            r13 = r29
            r14 = r30
            r15 = r31
            r0 = r32
            java.lang.String r0 = "mWorkerParameters"
            com.fossil.ee7.b(r1, r0)
            java.lang.String r0 = "mActivitiesRepository"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = "mSummariesRepository"
            com.fossil.ee7.b(r3, r0)
            java.lang.String r0 = "mSleepSessionRepository"
            com.fossil.ee7.b(r4, r0)
            java.lang.String r0 = "mSleepSummariesRepository"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = "mGoalTrackingRepository"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r0 = "mHeartRateSampleRepository"
            com.fossil.ee7.b(r7, r0)
            java.lang.String r0 = "mHeartRateSummaryRepository"
            com.fossil.ee7.b(r8, r0)
            java.lang.String r0 = "mFitnessDataRepository"
            com.fossil.ee7.b(r9, r0)
            java.lang.String r0 = "mAlarmsRepository"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = "mSharedPreferencesManager"
            com.fossil.ee7.b(r11, r0)
            java.lang.String r0 = "mDianaPresetRepository"
            com.fossil.ee7.b(r12, r0)
            java.lang.String r0 = "mHybridPresetRepository"
            com.fossil.ee7.b(r13, r0)
            java.lang.String r0 = "mThirdPartyRepository"
            com.fossil.ee7.b(r14, r0)
            java.lang.String r0 = "mChallengeRepository"
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "friendRepository"
            r15 = r32
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "mFileRepository"
            r15 = r33
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "mUserRepository"
            r15 = r34
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "mWorkoutSettingRepository"
            r15 = r35
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "fcmRepository"
            r15 = r36
            com.fossil.ee7.b(r15, r0)
            java.lang.String r0 = "mApp"
            r15 = r37
            com.fossil.ee7.b(r15, r0)
            android.content.Context r0 = r37.getApplicationContext()
            java.lang.String r15 = "mApp.applicationContext"
            com.fossil.ee7.a(r0, r15)
            r15 = r16
            r15.<init>(r0, r1)
            r15.h = r2
            r15.i = r3
            r15.j = r4
            r15.p = r5
            r15.q = r6
            r15.r = r7
            r15.s = r8
            r15.t = r9
            r15.u = r10
            r15.v = r11
            r15.w = r12
            r15.x = r13
            r15.y = r14
            r0 = r31
            r15.z = r0
            r0 = r32
            r15.A = r0
            r0 = r33
            r1 = r34
            r15.B = r0
            r15.C = r1
            r0 = r35
            r1 = r36
            r15.D = r0
            r15.E = r1
            r0 = r37
            r15.F = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.<init>(androidx.work.WorkerParameters, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.HeartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.AlarmsRepository, com.fossil.ch5, com.portfolio.platform.data.source.DianaPresetRepository, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.fossil.ro4, com.fossil.xo4, com.portfolio.platform.data.source.FileRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.data.source.WorkoutSettingRepository, com.fossil.to4, com.portfolio.platform.PortfolioApp):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // androidx.work.CoroutineWorker
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.fb7<? super androidx.work.ListenableWorker.a> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.portfolio.platform.workers.PushPendingDataWorker.c
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = (com.portfolio.platform.workers.PushPendingDataWorker.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = new com.portfolio.platform.workers.PushPendingDataWorker$c
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r0 = (com.portfolio.platform.workers.PushPendingDataWorker) r0
            com.fossil.t87.a(r6)
            goto L_0x0063
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "doWork() - id = "
            r2.append(r4)
            java.util.UUID r4 = r5.c()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = "PushPendingDataWorker"
            r6.d(r4, r2)
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r5.b(r0)
            if (r6 != r1) goto L_0x0063
            return r1
        L_0x0063:
            androidx.work.ListenableWorker$a r6 = androidx.work.ListenableWorker.a.c()
            java.lang.String r0 = "Result.success()"
            com.fossil.ee7.a(r6, r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x03af A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x03b0  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0408 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0424 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0440 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0225 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0239 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x024d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0261 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x029a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02b1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0350  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0375 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x037e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.i97> r23) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            boolean r2 = r1 instanceof com.portfolio.platform.workers.PushPendingDataWorker.g
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.portfolio.platform.workers.PushPendingDataWorker$g r2 = (com.portfolio.platform.workers.PushPendingDataWorker.g) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.portfolio.platform.workers.PushPendingDataWorker$g r2 = new com.portfolio.platform.workers.PushPendingDataWorker$g
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 0
            java.lang.String r6 = "PushPendingDataWorker"
            r7 = 1
            switch(r4) {
                case 0: goto L_0x0158;
                case 1: goto L_0x014e;
                case 2: goto L_0x013b;
                case 3: goto L_0x0128;
                case 4: goto L_0x0115;
                case 5: goto L_0x0102;
                case 6: goto L_0x00eb;
                case 7: goto L_0x00d4;
                case 8: goto L_0x00bd;
                case 9: goto L_0x00a6;
                case 10: goto L_0x008f;
                case 11: goto L_0x0078;
                case 12: goto L_0x0061;
                case 13: goto L_0x0078;
                case 14: goto L_0x004a;
                case 15: goto L_0x0033;
                default: goto L_0x002b;
            }
        L_0x002b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0033:
            java.lang.Object r3 = r2.L$3
            com.fossil.ko4 r3 = (com.fossil.ko4) r3
            java.lang.Object r3 = r2.L$2
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r2.L$1
            com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
            boolean r3 = r2.Z$0
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r2 = (com.portfolio.platform.workers.PushPendingDataWorker) r2
            com.fossil.t87.a(r1)
            goto L_0x0441
        L_0x004a:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r5 = r2.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r7 = r2.L$1
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            boolean r8 = r2.Z$0
            java.lang.Object r9 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r9 = (com.portfolio.platform.workers.PushPendingDataWorker) r9
            com.fossil.t87.a(r1)
            goto L_0x0425
        L_0x0061:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r5 = r2.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r7 = r2.L$1
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            boolean r8 = r2.Z$0
            java.lang.Object r9 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r9 = (com.portfolio.platform.workers.PushPendingDataWorker) r9
            com.fossil.t87.a(r1)
            goto L_0x03f2
        L_0x0078:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r5 = r2.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r7 = r2.L$1
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            boolean r8 = r2.Z$0
            java.lang.Object r9 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r9 = (com.portfolio.platform.workers.PushPendingDataWorker) r9
            com.fossil.t87.a(r1)
            goto L_0x0409
        L_0x008f:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r5 = r2.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r7 = r2.L$1
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            boolean r8 = r2.Z$0
            java.lang.Object r9 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r9 = (com.portfolio.platform.workers.PushPendingDataWorker) r9
            com.fossil.t87.a(r1)
            goto L_0x0399
        L_0x00a6:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r8 = r2.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$1
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            boolean r10 = r2.Z$0
            java.lang.Object r11 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r11 = (com.portfolio.platform.workers.PushPendingDataWorker) r11
            com.fossil.t87.a(r1)
            goto L_0x0376
        L_0x00bd:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r8 = r2.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$1
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            boolean r10 = r2.Z$0
            java.lang.Object r11 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r11 = (com.portfolio.platform.workers.PushPendingDataWorker) r11
            com.fossil.t87.a(r1)
            goto L_0x02b2
        L_0x00d4:
            java.lang.Object r4 = r2.L$3
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            java.lang.Object r8 = r2.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r2.L$1
            com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
            boolean r10 = r2.Z$0
            java.lang.Object r11 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r11 = (com.portfolio.platform.workers.PushPendingDataWorker) r11
            com.fossil.t87.a(r1)
            goto L_0x029b
        L_0x00eb:
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            boolean r9 = r2.Z$0
            java.lang.Object r10 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r10 = (com.portfolio.platform.workers.PushPendingDataWorker) r10
            com.fossil.t87.a(r1)
        L_0x00fc:
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r4
            goto L_0x0262
        L_0x0102:
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            boolean r9 = r2.Z$0
            java.lang.Object r10 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r10 = (com.portfolio.platform.workers.PushPendingDataWorker) r10
            com.fossil.t87.a(r1)
            goto L_0x024e
        L_0x0115:
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            boolean r9 = r2.Z$0
            java.lang.Object r10 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r10 = (com.portfolio.platform.workers.PushPendingDataWorker) r10
            com.fossil.t87.a(r1)
            goto L_0x023a
        L_0x0128:
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            boolean r9 = r2.Z$0
            java.lang.Object r10 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r10 = (com.portfolio.platform.workers.PushPendingDataWorker) r10
            com.fossil.t87.a(r1)
            goto L_0x0226
        L_0x013b:
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            boolean r9 = r2.Z$0
            java.lang.Object r10 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r10 = (com.portfolio.platform.workers.PushPendingDataWorker) r10
            com.fossil.t87.a(r1)
            goto L_0x01ec
        L_0x014e:
            boolean r4 = r2.Z$0
            java.lang.Object r8 = r2.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r8 = (com.portfolio.platform.workers.PushPendingDataWorker) r8
            com.fossil.t87.a(r1)
            goto L_0x0196
        L_0x0158:
            com.fossil.t87.a(r1)
            com.fossil.ch5 r1 = r0.v
            com.portfolio.platform.PortfolioApp r4 = r0.F
            java.lang.String r4 = r4.g()
            boolean r4 = r1.l(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "pushPendingData, isMigrationComplete "
            r8.append(r9)
            r8.append(r4)
            java.lang.String r8 = r8.toString()
            r1.d(r6, r8)
            if (r4 != 0) goto L_0x0186
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0186:
            com.fossil.pg5 r1 = com.fossil.pg5.i
            r2.L$0 = r0
            r2.Z$0 = r4
            r2.label = r7
            java.lang.Object r1 = r1.f(r2)
            if (r1 != r3) goto L_0x0195
            return r3
        L_0x0195:
            r8 = r0
        L_0x0196:
            com.portfolio.platform.data.model.room.UserDatabase r1 = (com.portfolio.platform.data.model.room.UserDatabase) r1
            com.portfolio.platform.data.model.room.UserDao r1 = r1.userDao()
            com.portfolio.platform.data.model.MFUser r1 = r1.getCurrentUser()
            if (r1 != 0) goto L_0x01a5
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x01a5:
            com.portfolio.platform.PortfolioApp r9 = r8.F
            java.lang.String r9 = r9.c()
            r2.L$0 = r8
            r2.Z$0 = r4
            r2.L$1 = r1
            r2.L$2 = r9
            r10 = 2
            r2.label = r10
            com.fossil.bi7 r10 = new com.fossil.bi7
            com.fossil.fb7 r11 = com.fossil.mb7.a(r2)
            r10.<init>(r11, r7)
            com.fossil.ti7 r11 = com.fossil.qj7.b()
            com.fossil.yi7 r12 = com.fossil.zi7.a(r11)
            r13 = 0
            r14 = 0
            com.portfolio.platform.workers.PushPendingDataWorker$d r15 = new com.portfolio.platform.workers.PushPendingDataWorker$d
            r15.<init>(r10, r5, r8)
            r16 = 3
            r17 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r12, r13, r14, r15, r16, r17)
            java.lang.Object r10 = r10.g()
            java.lang.Object r11 = com.fossil.nb7.a()
            if (r10 != r11) goto L_0x01e2
            com.fossil.vb7.c(r2)
        L_0x01e2:
            if (r10 != r3) goto L_0x01e5
            return r3
        L_0x01e5:
            r10 = r8
            r8 = r1
            r21 = r9
            r9 = r4
            r4 = r21
        L_0x01ec:
            r2.L$0 = r10
            r2.Z$0 = r9
            r2.L$1 = r8
            r2.L$2 = r4
            r1 = 3
            r2.label = r1
            com.fossil.bi7 r1 = new com.fossil.bi7
            com.fossil.fb7 r11 = com.fossil.mb7.a(r2)
            r1.<init>(r11, r7)
            com.fossil.ti7 r11 = com.fossil.qj7.b()
            com.fossil.yi7 r12 = com.fossil.zi7.a(r11)
            r13 = 0
            r14 = 0
            com.portfolio.platform.workers.PushPendingDataWorker$e r15 = new com.portfolio.platform.workers.PushPendingDataWorker$e
            r15.<init>(r1, r5, r10)
            r16 = 3
            r17 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r12, r13, r14, r15, r16, r17)
            java.lang.Object r1 = r1.g()
            java.lang.Object r11 = com.fossil.nb7.a()
            if (r1 != r11) goto L_0x0223
            com.fossil.vb7.c(r2)
        L_0x0223:
            if (r1 != r3) goto L_0x0226
            return r3
        L_0x0226:
            com.fossil.ro4 r1 = r10.z
            r2.L$0 = r10
            r2.Z$0 = r9
            r2.L$1 = r8
            r2.L$2 = r4
            r11 = 4
            r2.label = r11
            java.lang.Object r1 = r1.d(r2)
            if (r1 != r3) goto L_0x023a
            return r3
        L_0x023a:
            com.fossil.xo4 r1 = r10.A
            r2.L$0 = r10
            r2.Z$0 = r9
            r2.L$1 = r8
            r2.L$2 = r4
            r11 = 5
            r2.label = r11
            java.lang.Object r1 = r1.a(r2)
            if (r1 != r3) goto L_0x024e
            return r3
        L_0x024e:
            com.fossil.to4 r1 = r10.E
            r2.L$0 = r10
            r2.Z$0 = r9
            r2.L$1 = r8
            r2.L$2 = r4
            r11 = 6
            r2.label = r11
            java.lang.Object r1 = r1.a(r2)
            if (r1 != r3) goto L_0x00fc
            return r3
        L_0x0262:
            r4 = r1
            com.fossil.ko4 r4 = (com.fossil.ko4) r4
            com.portfolio.platform.data.model.ServerError r1 = r4.a()
            if (r1 == 0) goto L_0x029b
            com.portfolio.platform.data.model.ServerError r1 = r4.a()
            java.lang.Integer r1 = r1.getCode()
            if (r1 != 0) goto L_0x0276
            goto L_0x029b
        L_0x0276:
            int r1 = r1.intValue()
            if (r1 != 0) goto L_0x029b
            com.fossil.ch5 r1 = r11.v
            r1.x(r7)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            r2.L$0 = r11
            r2.Z$0 = r10
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r4
            r12 = 7
            r2.label = r12
            java.lang.Object r1 = r1.b(r2)
            if (r1 != r3) goto L_0x029b
            return r3
        L_0x029b:
            com.portfolio.platform.data.source.FitnessDataRepository r1 = r11.t
            r2.L$0 = r11
            r2.Z$0 = r10
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r4
            r12 = 8
            r2.label = r12
            java.lang.Object r1 = r1.pushPendingFitnessData(r2)
            if (r1 != r3) goto L_0x02b2
            return r3
        L_0x02b2:
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            boolean r12 = r1 instanceof com.fossil.bj5
            if (r12 == 0) goto L_0x0350
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = "pushPendingFitnessData - Success"
            r12.d(r6, r13)
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x035f
            boolean r12 = r1.isEmpty()
            r12 = r12 ^ r7
            if (r12 == 0) goto L_0x035f
            com.fossil.se7 r12 = new com.fossil.se7
            r12.<init>()
            r13 = 0
            java.lang.Object r14 = r1.get(r13)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r14 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r14
            org.joda.time.DateTime r14 = r14.getStartTimeTZ()
            r12.element = r14
            com.fossil.se7 r14 = new com.fossil.se7
            r14.<init>()
            java.lang.Object r13 = r1.get(r13)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r13 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r13
            org.joda.time.DateTime r13 = r13.getEndTimeTZ()
            r14.element = r13
            java.util.Iterator r1 = r1.iterator()
        L_0x02fb:
            boolean r13 = r1.hasNext()
            if (r13 == 0) goto L_0x0335
            java.lang.Object r13 = r1.next()
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r13 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r13
            long r15 = r13.getStartLongTime()
            T r7 = r12.element
            org.joda.time.DateTime r7 = (org.joda.time.DateTime) r7
            long r17 = r7.getMillis()
            int r7 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
            if (r7 >= 0) goto L_0x031d
            org.joda.time.DateTime r7 = r13.getStartTimeTZ()
            r12.element = r7
        L_0x031d:
            long r15 = r13.getEndLongTime()
            T r7 = r14.element
            org.joda.time.DateTime r7 = (org.joda.time.DateTime) r7
            long r17 = r7.getMillis()
            int r7 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
            if (r7 <= 0) goto L_0x0333
            org.joda.time.DateTime r7 = r13.getEndTimeTZ()
            r14.element = r7
        L_0x0333:
            r7 = 1
            goto L_0x02fb
        L_0x0335:
            com.fossil.ti7 r1 = com.fossil.qj7.b()
            com.fossil.yi7 r15 = com.fossil.zi7.a(r1)
            r16 = 0
            r17 = 0
            com.portfolio.platform.workers.PushPendingDataWorker$h r1 = new com.portfolio.platform.workers.PushPendingDataWorker$h
            r1.<init>(r11, r12, r14, r5)
            r19 = 3
            r20 = 0
            r18 = r1
            com.fossil.ik7 unused = com.fossil.xh7.b(r15, r16, r17, r18, r19, r20)
            goto L_0x035f
        L_0x0350:
            boolean r1 = r1 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x035f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r7 = "SleepSessionRepository.pushPendingFitnessData - Failed "
            r1.d(r6, r7)
        L_0x035f:
            com.portfolio.platform.data.source.AlarmsRepository r1 = r11.u
            r2.L$0 = r11
            r2.Z$0 = r10
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r4
            r7 = 9
            r2.label = r7
            java.lang.Object r1 = r1.executePendingRequest(r2)
            if (r1 != r3) goto L_0x0376
            return r3
        L_0x0376:
            com.fossil.be5$a r1 = com.fossil.be5.o
            boolean r1 = r1.f(r8)
            if (r1 == 0) goto L_0x03b0
            com.portfolio.platform.data.source.WorkoutSettingRepository r1 = r11.D
            r2.L$0 = r11
            r2.Z$0 = r10
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r4
            r5 = 10
            r2.label = r5
            java.lang.Object r1 = r1.executePendingRequest(r2)
            if (r1 != r3) goto L_0x0395
            return r3
        L_0x0395:
            r5 = r8
            r7 = r9
            r8 = r10
            r9 = r11
        L_0x0399:
            com.portfolio.platform.data.source.DianaPresetRepository r1 = r9.w
            r2.L$0 = r9
            r2.Z$0 = r8
            r2.L$1 = r7
            r2.L$2 = r5
            r2.L$3 = r4
            r10 = 11
            r2.label = r10
            java.lang.Object r1 = r1.executePendingRequest(r5, r2)
            if (r1 != r3) goto L_0x0409
            return r3
        L_0x03b0:
            r2.L$0 = r11
            r2.Z$0 = r10
            r2.L$1 = r9
            r2.L$2 = r8
            r2.L$3 = r4
            r1 = 12
            r2.label = r1
            com.fossil.bi7 r1 = new com.fossil.bi7
            com.fossil.fb7 r7 = com.fossil.mb7.a(r2)
            r12 = 1
            r1.<init>(r7, r12)
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.yi7 r12 = com.fossil.zi7.a(r7)
            r13 = 0
            r14 = 0
            com.portfolio.platform.workers.PushPendingDataWorker$f r15 = new com.portfolio.platform.workers.PushPendingDataWorker$f
            r15.<init>(r1, r5, r11)
            r16 = 3
            r17 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r12, r13, r14, r15, r16, r17)
            java.lang.Object r1 = r1.g()
            java.lang.Object r5 = com.fossil.nb7.a()
            if (r1 != r5) goto L_0x03eb
            com.fossil.vb7.c(r2)
        L_0x03eb:
            if (r1 != r3) goto L_0x03ee
            return r3
        L_0x03ee:
            r5 = r8
            r7 = r9
            r8 = r10
            r9 = r11
        L_0x03f2:
            com.portfolio.platform.data.source.HybridPresetRepository r1 = r9.x
            r2.L$0 = r9
            r2.Z$0 = r8
            r2.L$1 = r7
            r2.L$2 = r5
            r2.L$3 = r4
            r10 = 13
            r2.label = r10
            java.lang.Object r1 = r1.executePendingRequest(r5, r2)
            if (r1 != r3) goto L_0x0409
            return r3
        L_0x0409:
            com.portfolio.platform.data.source.ThirdPartyRepository r1 = r9.y
            com.portfolio.platform.workers.PushPendingDataWorker$i r10 = new com.portfolio.platform.workers.PushPendingDataWorker$i
            r10.<init>()
            r2.L$0 = r9
            r2.Z$0 = r8
            r2.L$1 = r7
            r2.L$2 = r5
            r2.L$3 = r4
            r11 = 14
            r2.label = r11
            java.lang.Object r1 = r1.pushPendingData(r10, r2)
            if (r1 != r3) goto L_0x0425
            return r3
        L_0x0425:
            com.portfolio.platform.data.source.FileRepository r1 = r9.B
            r1.downloadPendingFile()
            com.portfolio.platform.data.source.UserRepository r1 = r9.C
            r2.L$0 = r9
            r2.Z$0 = r8
            r2.L$1 = r7
            r2.L$2 = r5
            r2.L$3 = r4
            r4 = 15
            r2.label = r4
            java.lang.Object r1 = r1.pushPendingUserSetting(r2)
            if (r1 != r3) goto L_0x0441
            return r3
        L_0x0441:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "Done - Stop worker"
            r1.d(r6, r2)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
            switch-data {0->0x0158, 1->0x014e, 2->0x013b, 3->0x0128, 4->0x0115, 5->0x0102, 6->0x00eb, 7->0x00d4, 8->0x00bd, 9->0x00a6, 10->0x008f, 11->0x0078, 12->0x0061, 13->0x0078, 14->0x004a, 15->0x0033, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.b(com.fossil.fb7):java.lang.Object");
    }
}
