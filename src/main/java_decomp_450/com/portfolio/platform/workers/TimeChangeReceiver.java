package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ch5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.pd5;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.microapp.CommuteTimeService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeChangeReceiver extends BroadcastReceiver {
    @DexIgnore
    public pd5 a;
    @DexIgnore
    public ch5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ TimeChangeReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fb7 fb7, TimeChangeReceiver timeChangeReceiver) {
            super(2, fb7);
            this.this$0 = timeChangeReceiver;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(fb7, this.this$0);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                String c = PortfolioApp.g0.c().c();
                if (c.length() == 0) {
                    return i97.a;
                }
                long e = this.this$0.b().e(c);
                long c2 = this.this$0.b().c(c);
                long currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + e + "; " + "lastReminderSyncTimeSuccess = " + c2);
                if (c2 > currentTimeMillis || e > currentTimeMillis) {
                    ch5 b = this.this$0.b();
                    long j = currentTimeMillis - ((long) CommuteTimeService.z);
                    b.c(j, c);
                    this.this$0.b().a(j, c);
                    return i97.a;
                }
                pd5 a = this.this$0.a();
                Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                a.e(applicationContext);
                pd5 a2 = this.this$0.a();
                Context applicationContext2 = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                a2.d(applicationContext2);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final pd5 a() {
        pd5 pd5 = this.a;
        if (pd5 != null) {
            return pd5;
        }
        ee7.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final ch5 b() {
        ch5 ch5 = this.b;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        if (intent != null && (action = intent.getAction()) != null) {
            if (ee7.a((Object) action, (Object) "android.intent.action.TIME_SET") || ee7.a((Object) action, (Object) "android.intent.action.TIMEZONE_CHANGED") || ee7.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(null, this), 3, null);
            }
        }
    }
}
