package com.portfolio.platform.uirenew.home.profile.theme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.mk6;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemesActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, ThemesActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((mk6) getSupportFragmentManager().b(2131362149)) == null) {
            a(mk6.q.a(), 2131362149);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onResume() {
        super.onResume();
        a(false);
    }
}
