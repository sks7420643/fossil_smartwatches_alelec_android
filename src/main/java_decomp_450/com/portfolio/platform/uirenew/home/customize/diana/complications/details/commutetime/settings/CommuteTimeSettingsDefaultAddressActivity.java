package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.b16;
import com.fossil.cl5;
import com.fossil.d16;
import com.fossil.ee7;
import com.fossil.q06;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public d16 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle) {
            ee7.b(fragment, "fragment");
            ee7.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 111);
        }

        @DexIgnore
        public final void b(Fragment fragment, Bundle bundle) {
            ee7.b(fragment, "fragment");
            ee7.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 112);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        q06 q06 = (q06) getSupportFragmentManager().b(2131362149);
        if (q06 == null) {
            q06 = q06.r.a();
            a(q06, q06.r.b(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (q06 != null) {
            f.a(new b16(q06)).a(this);
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS");
            if (bundleExtra != null) {
                d16 d16 = this.y;
                if (d16 != null) {
                    String string = bundleExtra.getString("AddressType");
                    if (string == null) {
                        string = "Home";
                    }
                    String string2 = bundleExtra.getString("KEY_DEFAULT_PLACE");
                    if (string2 == null) {
                        string2 = "";
                    }
                    d16.a(string, string2);
                    return;
                }
                ee7.d("mCommuteTimeSettingsDefaultAddressPresenter");
                throw null;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressContract.View");
    }
}
