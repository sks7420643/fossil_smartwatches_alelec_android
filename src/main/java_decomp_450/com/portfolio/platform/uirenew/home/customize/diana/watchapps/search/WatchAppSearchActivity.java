package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.t46;
import com.fossil.tj4;
import com.fossil.u46;
import com.fossil.w46;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSearchActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public w46 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            ee7.b(fragment, "fragment");
            ee7.b(str, "watchAppTop");
            ee7.b(str2, "watchAppMiddle");
            ee7.b(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), WatchAppSearchActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            fragment.startActivityForResult(intent, 101, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        t46 t46 = (t46) getSupportFragmentManager().b(2131362149);
        if (t46 == null) {
            t46 = t46.p.b();
            a(t46, t46.p.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (t46 != null) {
            f.a(new u46(t46)).a(this);
            Intent intent = getIntent();
            ee7.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            String str = "empty";
            if (extras != null) {
                w46 w46 = this.y;
                if (w46 != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = str;
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = str;
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = str;
                    }
                    w46.a(string, string2, string3);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                w46 w462 = this.y;
                if (w462 != null) {
                    String string4 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string4 == null) {
                        string4 = str;
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = str;
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 != null) {
                        str = string6;
                    }
                    w462.a(string4, string5, str);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        w46 w46 = this.y;
        if (w46 != null) {
            w46.a(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }
}
