package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.a86;
import com.fossil.ah;
import com.fossil.b86;
import com.fossil.be5;
import com.fossil.da;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.g86;
import com.fossil.go5;
import com.fossil.h86;
import com.fossil.ow4;
import com.fossil.p76;
import com.fossil.q76;
import com.fossil.qb;
import com.fossil.qw6;
import com.fossil.qz6;
import com.fossil.tj4;
import com.fossil.u76;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewFragment extends go5 {
    @DexIgnore
    public qw6<ow4> f;
    @DexIgnore
    public q76 g;
    @DexIgnore
    public h86 h;
    @DexIgnore
    public b86 i;
    @DexIgnore
    public p76 j;
    @DexIgnore
    public g86 p;
    @DexIgnore
    public a86 q;
    @DexIgnore
    public int r; // = 7;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment a;

        @DexIgnore
        public b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.a = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.a;
            qw6 a2 = activeTimeOverviewFragment.f;
            activeTimeOverviewFragment.a(7, a2 != null ? (ow4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment a;

        @DexIgnore
        public c(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.a = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.a;
            qw6 a2 = activeTimeOverviewFragment.f;
            activeTimeOverviewFragment.a(4, a2 != null ? (ow4) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewFragment a;

        @DexIgnore
        public d(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            this.a = activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeOverviewFragment activeTimeOverviewFragment = this.a;
            qw6 a2 = activeTimeOverviewFragment.f;
            activeTimeOverviewFragment.a(2, a2 != null ? (ow4) a2.a() : null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActiveTimeOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        String str;
        String str2;
        String str3;
        qw6<ow4> qw6;
        ow4 a2;
        qw6<ow4> qw62;
        ow4 a3;
        ConstraintLayout constraintLayout;
        qw6<ow4> qw63;
        ow4 a4;
        FlexibleTextView flexibleTextView;
        qw6<ow4> qw64;
        ow4 a5;
        be5.a aVar = be5.o;
        q76 q76 = this.g;
        if (q76 != null) {
            if (aVar.a(q76.h())) {
                str = eh5.l.a().b("dianaActiveMinutesTab");
            } else {
                str = eh5.l.a().b("hybridActiveMinutesTab");
            }
            be5.a aVar2 = be5.o;
            q76 q762 = this.g;
            if (q762 != null) {
                if (aVar2.a(q762.h())) {
                    str2 = eh5.l.a().b("onDianaActiveMinutesTab");
                } else {
                    str2 = eh5.l.a().b("onHybridActiveMinutesTab");
                }
                if (!(str == null || (qw64 = this.f) == null || (a5 = qw64.a()) == null)) {
                    a5.x.setBackgroundColor(Color.parseColor(str));
                    a5.y.setBackgroundColor(Color.parseColor(str));
                }
                if (!(str2 == null || (qw63 = this.f) == null || (a4 = qw63.a()) == null || (flexibleTextView = a4.t) == null)) {
                    flexibleTextView.setTextColor(Color.parseColor(str2));
                }
                be5.a aVar3 = be5.o;
                q76 q763 = this.g;
                if (q763 != null) {
                    if (aVar3.a(q763.h())) {
                        str3 = eh5.l.a().b("onDianaInactiveTab");
                    } else {
                        str3 = eh5.l.a().b("onHybridInactiveTab");
                    }
                    this.s = str3;
                    String b2 = eh5.l.a().b("nonBrandSurface");
                    this.t = eh5.l.a().b("primaryText");
                    if (!(b2 == null || (qw62 = this.f) == null || (a3 = qw62.a()) == null || (constraintLayout = a3.q) == null)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(b2));
                    }
                    if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                        FlexibleTextView flexibleTextView2 = a2.u;
                        ee7.a((Object) flexibleTextView2, "it.ftvToday");
                        if (flexibleTextView2.isSelected()) {
                            a2.u.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.u.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView3 = a2.r;
                        ee7.a((Object) flexibleTextView3, "it.ftv7Days");
                        if (flexibleTextView3.isSelected()) {
                            a2.r.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.r.setTextColor(Color.parseColor(this.s));
                        }
                        FlexibleTextView flexibleTextView4 = a2.s;
                        ee7.a((Object) flexibleTextView4, "it.ftvMonth");
                        if (flexibleTextView4.isSelected()) {
                            a2.s.setTextColor(Color.parseColor(this.t));
                        } else {
                            a2.s.setTextColor(Color.parseColor(this.s));
                        }
                    }
                } else {
                    ee7.d("mActiveTimeOverviewDayPresenter");
                    throw null;
                }
            } else {
                ee7.d("mActiveTimeOverviewDayPresenter");
                throw null;
            }
        } else {
            ee7.d("mActiveTimeOverviewDayPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ow4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "onCreateView");
        ow4 ow4 = (ow4) qb.a(layoutInflater, 2131558492, viewGroup, false, a1());
        da.d((View) ow4.w, false);
        if (bundle != null) {
            this.r = bundle.getInt("CURRENT_TAB", 7);
        }
        ee7.a((Object) ow4, "binding");
        a(ow4);
        this.f = new qw6<>(this, ow4);
        f1();
        qw6<ow4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.r);
    }

    @DexIgnore
    public final void a(ow4 ow4) {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewFragment", "initUI");
        this.j = (p76) getChildFragmentManager().b("ActiveTimeOverviewDayFragment");
        this.p = (g86) getChildFragmentManager().b("ActiveTimeOverviewWeekFragment");
        this.q = (a86) getChildFragmentManager().b("ActiveTimeOverviewMonthFragment");
        if (this.j == null) {
            this.j = new p76();
        }
        if (this.p == null) {
            this.p = new g86();
        }
        if (this.q == null) {
            this.q = new a86();
        }
        ArrayList arrayList = new ArrayList();
        p76 p76 = this.j;
        if (p76 != null) {
            arrayList.add(p76);
            g86 g86 = this.p;
            if (g86 != null) {
                arrayList.add(g86);
                a86 a86 = this.q;
                if (a86 != null) {
                    arrayList.add(a86);
                    RecyclerView recyclerView = ow4.w;
                    ee7.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new qz6(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new ActiveTimeOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ah().a(recyclerView);
                    a(this.r, ow4);
                    tj4 f2 = PortfolioApp.g0.c().f();
                    p76 p762 = this.j;
                    if (p762 != null) {
                        g86 g862 = this.p;
                        if (g862 != null) {
                            a86 a862 = this.q;
                            if (a862 != null) {
                                f2.a(new u76(p762, g862, a862)).a(this);
                                ow4.u.setOnClickListener(new b(this));
                                ow4.r.setOnClickListener(new c(this));
                                ow4.s.setOnClickListener(new d(this));
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i2, ow4 ow4) {
        qw6<ow4> qw6;
        ow4 a2;
        ow4 a3;
        RecyclerView recyclerView;
        ow4 a4;
        RecyclerView recyclerView2;
        ow4 a5;
        RecyclerView recyclerView3;
        ow4 a6;
        RecyclerView recyclerView4;
        if (ow4 != null) {
            FlexibleTextView flexibleTextView = ow4.u;
            ee7.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = ow4.r;
            ee7.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = ow4.s;
            ee7.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = ow4.u;
            ee7.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = ow4.r;
            ee7.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = ow4.s;
            ee7.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = ow4.s;
                ee7.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = ow4.s;
                ee7.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = ow4.r;
                ee7.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                qw6<ow4> qw62 = this.f;
                if (!(qw62 == null || (a3 = qw62.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = ow4.r;
                ee7.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = ow4.r;
                ee7.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = ow4.r;
                ee7.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                qw6<ow4> qw63 = this.f;
                if (!(qw63 == null || (a4 = qw63.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = ow4.u;
                ee7.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = ow4.u;
                ee7.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = ow4.r;
                ee7.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                qw6<ow4> qw64 = this.f;
                if (!(qw64 == null || (a6 = qw64.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = ow4.u;
                ee7.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = ow4.u;
                ee7.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = ow4.r;
                ee7.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                qw6<ow4> qw65 = this.f;
                if (!(qw65 == null || (a5 = qw65.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                FlexibleTextView flexibleTextView19 = a2.u;
                ee7.a((Object) flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView20 = a2.r;
                ee7.a((Object) flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView21 = a2.s;
                ee7.a((Object) flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.s));
                }
            }
        }
    }
}
