package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.rf6;
import com.fossil.sf6;
import com.fossil.uf6;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailActivity extends cl5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public uf6 y;
    @DexIgnore
    public Date z; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            ee7.b(date, "date");
            ee7.b(context, "context");
            Intent intent = new Intent(context, GoalTrackingDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        rf6 rf6 = (rf6) getSupportFragmentManager().b(2131362149);
        Intent intent = getIntent();
        if (intent != null) {
            this.z = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (rf6 == null) {
            rf6 = rf6.z.a(this.z);
            a(rf6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new sf6(rf6)).a(this);
    }
}
