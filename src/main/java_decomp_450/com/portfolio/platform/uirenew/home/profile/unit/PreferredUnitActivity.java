package com.portfolio.platform.uirenew.home.profile.unit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.jn6;
import com.fossil.ln6;
import com.fossil.nn6;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreferredUnitActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public nn6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, PreferredUnitActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        jn6 jn6 = (jn6) getSupportFragmentManager().b(2131362149);
        if (jn6 == null) {
            jn6 = jn6.w.b();
            a(jn6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new ln6(jn6)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onResume() {
        super.onResume();
        a(false);
    }
}
