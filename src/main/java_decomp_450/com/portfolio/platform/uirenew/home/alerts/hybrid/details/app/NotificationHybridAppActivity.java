package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.ay5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.tj4;
import com.fossil.wx5;
import com.fossil.x87;
import com.fossil.xx5;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridAppActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ay5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<String> arrayList) {
            ee7.b(fragment, "fragment");
            ee7.b(arrayList, "stringAppsSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridAppActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putStringArrayListExtra("LIST_APPS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 4567);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        ee7.a((Object) NotificationHybridAppActivity.class.getSimpleName(), "NotificationHybridAppAct\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        wx5 wx5 = (wx5) getSupportFragmentManager().b(2131362149);
        if (wx5 == null) {
            wx5 = wx5.j.b();
            a(wx5, wx5.j.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (wx5 != null) {
            f.a(new xx5(wx5, getIntent().getIntExtra("HAND_NUMBER", 0), getIntent().getStringArrayListExtra("LIST_APPS_SELECTED"))).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppContract.View");
    }
}
