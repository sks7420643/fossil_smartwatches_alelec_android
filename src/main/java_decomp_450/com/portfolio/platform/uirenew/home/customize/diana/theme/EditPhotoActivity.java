package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.fossil.a36;
import com.fossil.cl5;
import com.fossil.fz5;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoActivity extends cl5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((a36) getSupportFragmentManager().b(2131362149)) == null && (intent = getIntent()) != null) {
            ArrayList<fz5> arrayList = null;
            Uri uri = intent.hasExtra("IMAGE_URI_EXTRA") ? (Uri) intent.getParcelableExtra("IMAGE_URI_EXTRA") : null;
            if (intent.hasExtra("COMPLICATION_EXTRA")) {
                arrayList = intent.getParcelableArrayListExtra("COMPLICATION_EXTRA");
            }
            if (uri == null || arrayList == null) {
                FLogger.INSTANCE.getLocal().d(e(), "Can not start EditPhotoFragment with invalid params");
                return;
            }
            a36 a2 = a36.t.a(uri, arrayList);
            if (a2 != null) {
                a(a2, "EditPhotoFragment", 2131362149);
                return;
            }
            throw new x87("null cannot be cast to non-null type androidx.fragment.app.Fragment");
        }
    }
}
