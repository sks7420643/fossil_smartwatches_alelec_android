package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.a06;
import com.fossil.a9;
import com.fossil.bl5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.fz5;
import com.fossil.g6;
import com.fossil.h6;
import com.fossil.rj4;
import com.fossil.wz5;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeEditActivity extends cl5 {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public a06 A;
    @DexIgnore
    public wz5 y;
    @DexIgnore
    public rj4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, int i, String str2, String str3) {
            ee7.b(context, "context");
            ee7.b(str, "presetId");
            ee7.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "start - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(context, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<a9<View, String>> arrayList, List<? extends a9<CustomizeWidget, String>> list, CopyOnWriteArrayList<CustomizeRealData> copyOnWriteArrayList, int i, String str2, String str3) {
            ee7.b(fragmentActivity, "context");
            ee7.b(str, "presetId");
            ee7.b(arrayList, "views");
            ee7.b(list, "customizeWidgetViews");
            ee7.b(copyOnWriteArrayList, "customizeRealDataList");
            ee7.b(str2, "complicationPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", complicationPos=" + str2 + ", watchAppPos=" + str3);
            Intent intent = new Intent(fragmentActivity, DianaCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_CUSTOMIZE_TAB", i);
            intent.putExtra("KEY_PRESET_COMPLICATION_POS_SELECTED", str2);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str3);
            intent.putExtra("KEY_CUSTOMIZE_REAL_DATA_LIST", copyOnWriteArrayList);
            Iterator<? extends a9<CustomizeWidget, String>> it = list.iterator();
            while (it.hasNext()) {
                a9 a9Var = (a9) it.next();
                arrayList.add(new a9<>(a9Var.a, a9Var.b));
                Bundle bundle = new Bundle();
                bl5.a aVar = bl5.b;
                F f = a9Var.a;
                if (f != null) {
                    ee7.a((Object) f, "wcPair.first!!");
                    aVar.a(f, bundle);
                    F f2 = a9Var.a;
                    if (f2 != null) {
                        ee7.a((Object) f2, "wcPair.first!!");
                        intent.putExtra(f2.getTransitionName(), bundle);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new a9[0]);
            if (array != null) {
                a9[] a9VarArr = (a9[]) array;
                h6 a = h6.a(fragmentActivity, (a9[]) Arrays.copyOf(a9VarArr, a9VarArr.length));
                ee7.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                g6.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x017f  */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r20) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            java.lang.String r0 = "KEY_ORIGINAL_PRESET"
            java.lang.String r3 = "KEY_CURRENT_PRESET"
            java.lang.Class<com.portfolio.platform.data.model.diana.preset.DianaPreset> r4 = com.portfolio.platform.data.model.diana.preset.DianaPreset.class
            super.onCreate(r20)
            r5 = 2131558428(0x7f0d001c, float:1.8742172E38)
            r1.setContentView(r5)
            android.content.Intent r5 = r19.getIntent()
            java.lang.String r6 = "KEY_CUSTOMIZE_TAB"
            r7 = 1
            int r5 = r5.getIntExtra(r6, r7)
            android.content.Intent r6 = r19.getIntent()
            java.lang.String r7 = "KEY_PRESET_ID"
            java.lang.String r6 = r6.getStringExtra(r7)
            android.content.Intent r7 = r19.getIntent()
            java.lang.String r14 = "KEY_PRESET_COMPLICATION_POS_SELECTED"
            java.lang.String r7 = r7.getStringExtra(r14)
            android.content.Intent r8 = r19.getIntent()
            java.lang.String r15 = "KEY_PRESET_WATCH_APP_POS_SELECTED"
            java.lang.String r13 = r8.getStringExtra(r15)
            androidx.fragment.app.FragmentManager r8 = r19.getSupportFragmentManager()
            r9 = 2131362149(0x7f0a0165, float:1.834407E38)
            androidx.fragment.app.Fragment r8 = r8.b(r9)
            com.fossil.sz5 r8 = (com.fossil.sz5) r8
            if (r8 != 0) goto L_0x0055
            com.fossil.sz5 r8 = new com.fossil.sz5
            r8.<init>()
            java.lang.String r10 = "DianaCustomizeEditFragment"
            r1.a(r8, r10, r9)
        L_0x0055:
            r9 = r8
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            com.fossil.tj4 r12 = r8.f()
            com.fossil.uz5 r11 = new com.fossil.uz5
            java.lang.String r8 = "presetId"
            com.fossil.ee7.a(r6, r8)
            r8 = r11
            r10 = r6
            r16 = r15
            r15 = r11
            r11 = r5
            r17 = r14
            r14 = r12
            r12 = r7
            r18 = r13
            r8.<init>(r9, r10, r11, r12, r13)
            com.fossil.pz5 r8 = r14.a(r15)
            r8.a(r1)
            java.lang.String r8 = "mPresenter"
            r9 = 0
            if (r2 == 0) goto L_0x0091
            r10 = -1
            if (r5 == r10) goto L_0x0091
            com.fossil.wz5 r10 = r1.y
            if (r10 == 0) goto L_0x008d
            r10.b(r5)
            goto L_0x0091
        L_0x008d:
            com.fossil.ee7.d(r8)
            throw r9
        L_0x0091:
            android.content.Intent r5 = r19.getIntent()
            if (r5 == 0) goto L_0x00b4
            java.lang.String r10 = "KEY_CUSTOMIZE_REAL_DATA_LIST"
            boolean r11 = r5.hasExtra(r10)
            if (r11 == 0) goto L_0x00b4
            com.fossil.wz5 r11 = r1.y
            if (r11 == 0) goto L_0x00b0
            java.util.ArrayList r5 = r5.getParcelableArrayListExtra(r10)
            java.lang.String r8 = "it.getParcelableArrayLis\u2026CUSTOMIZE_REAL_DATA_LIST)"
            com.fossil.ee7.a(r5, r8)
            r11.a(r5)
            goto L_0x00b4
        L_0x00b0:
            com.fossil.ee7.d(r8)
            throw r9
        L_0x00b4:
            com.fossil.rj4 r5 = r1.z
            if (r5 == 0) goto L_0x0183
            androidx.lifecycle.ViewModelProvider r5 = com.fossil.je.a(r1, r5)
            java.lang.Class<com.fossil.a06> r8 = com.fossil.a06.class
            com.fossil.he r5 = r5.a(r8)
            java.lang.String r8 = "ViewModelProviders.of(th\u2026izeViewModel::class.java)"
            com.fossil.ee7.a(r5, r8)
            com.fossil.a06 r5 = (com.fossil.a06) r5
            r1.A = r5
            java.lang.String r5 = "mDianaCustomizeViewModel"
            if (r2 != 0) goto L_0x00ed
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r19.e()
            java.lang.String r3 = "init from initialize state"
            r0.d(r2, r3)
            com.fossil.a06 r0 = r1.A
            if (r0 == 0) goto L_0x00e9
            r8 = r18
            r0.a(r6, r7, r8)
            goto L_0x017e
        L_0x00e9:
            com.fossil.ee7.d(r5)
            throw r9
        L_0x00ed:
            r8 = r18
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = r19.e()
            java.lang.String r12 = "init from savedInstanceState"
            r10.d(r11, r12)
            com.fossil.be4 r10 = new com.fossil.be4
            r10.<init>()
            com.portfolio.platform.gson.DianaPresetDeserializer r11 = new com.portfolio.platform.gson.DianaPresetDeserializer
            r11.<init>()
            r10.a(r4, r11)
            com.google.gson.Gson r10 = r10.a()
            boolean r11 = r2.containsKey(r3)     // Catch:{ Exception -> 0x0136 }
            if (r11 == 0) goto L_0x0120
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x0136 }
            java.lang.Object r3 = r10.a(r3, r4)     // Catch:{ Exception -> 0x0136 }
            com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r3     // Catch:{ Exception -> 0x0136 }
            goto L_0x0121
        L_0x0120:
            r3 = r9
        L_0x0121:
            boolean r11 = r2.containsKey(r0)     // Catch:{ Exception -> 0x0134 }
            if (r11 == 0) goto L_0x0156
            java.lang.String r0 = r2.getString(r0)     // Catch:{ Exception -> 0x0134 }
            java.lang.Object r0 = r10.a(r0, r4)     // Catch:{ Exception -> 0x0134 }
            com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r0     // Catch:{ Exception -> 0x0134 }
            r10 = r0
            r11 = r3
            goto L_0x0158
        L_0x0134:
            r0 = move-exception
            goto L_0x0138
        L_0x0136:
            r0 = move-exception
            r3 = r9
        L_0x0138:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r10 = r19.e()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "exception when parse GSON when retrieve from saveInstanceState "
            r11.append(r12)
            r11.append(r0)
            java.lang.String r0 = r11.toString()
            r4.d(r10, r0)
        L_0x0156:
            r11 = r3
            r10 = r9
        L_0x0158:
            r3 = r17
            boolean r0 = r2.containsKey(r3)
            if (r0 == 0) goto L_0x0166
            java.lang.String r0 = r2.getString(r3)
            r12 = r0
            goto L_0x0167
        L_0x0166:
            r12 = r7
        L_0x0167:
            r3 = r16
            boolean r0 = r2.containsKey(r3)
            if (r0 == 0) goto L_0x0175
            java.lang.String r0 = r2.getString(r3)
            r13 = r0
            goto L_0x0176
        L_0x0175:
            r13 = r8
        L_0x0176:
            com.fossil.a06 r8 = r1.A
            if (r8 == 0) goto L_0x017f
            r9 = r6
            r8.a(r9, r10, r11, r12, r13)
        L_0x017e:
            return
        L_0x017f:
            com.fossil.ee7.d(r5)
            throw r9
        L_0x0183:
            java.lang.String r0 = "viewModelFactory"
            com.fossil.ee7.d(r0)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity.onCreate(android.os.Bundle):void");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        wz5 wz5 = this.y;
        if (wz5 != null) {
            wz5.a(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final ArrayList<fz5> q() {
        wz5 wz5 = this.y;
        if (wz5 == null) {
            return null;
        }
        if (wz5 != null) {
            return wz5.j();
        }
        ee7.d("mPresenter");
        throw null;
    }
}
