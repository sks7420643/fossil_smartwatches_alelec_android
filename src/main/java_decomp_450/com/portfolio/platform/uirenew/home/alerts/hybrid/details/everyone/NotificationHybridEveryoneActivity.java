package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.bt5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.tj4;
import com.fossil.vy5;
import com.fossil.wy5;
import com.fossil.x87;
import com.fossil.zd7;
import com.fossil.zy5;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridEveryoneActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public zy5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<bt5> arrayList) {
            ee7.b(fragment, "fragment");
            ee7.b(arrayList, "contactWrappersSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridEveryoneActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putExtra("LIST_CONTACTS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 6789);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        ee7.a((Object) NotificationHybridEveryoneActivity.class.getSimpleName(), "NotificationHybridEveryo\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        vy5 vy5 = (vy5) getSupportFragmentManager().b(2131362149);
        if (vy5 == null) {
            vy5 = vy5.j.b();
            a(vy5, vy5.j.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (vy5 != null) {
            f.a(new wy5(vy5, getIntent().getIntExtra("HAND_NUMBER", 0), (ArrayList) getIntent().getSerializableExtra("LIST_CONTACTS_SELECTED"))).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneContract.View");
    }
}
