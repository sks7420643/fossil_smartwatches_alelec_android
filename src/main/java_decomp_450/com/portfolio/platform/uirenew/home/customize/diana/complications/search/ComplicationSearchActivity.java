package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.i26;
import com.fossil.j26;
import com.fossil.l26;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public l26 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3, String str4) {
            ee7.b(fragment, "fragment");
            ee7.b(str, "topComplication");
            ee7.b(str2, "bottomComplication");
            ee7.b(str3, "leftComplication");
            ee7.b(str4, "rightComplication");
            Intent intent = new Intent(fragment.getContext(), ComplicationSearchActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("bottom", str2);
            intent.putExtra(ViewHierarchy.DIMENSION_LEFT_KEY, str3);
            intent.putExtra("right", str4);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        i26 i26 = (i26) getSupportFragmentManager().b(2131362149);
        if (i26 == null) {
            i26 = i26.p.b();
            a(i26, i26.p.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (i26 != null) {
            f.a(new j26(i26)).a(this);
            Intent intent = getIntent();
            ee7.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            String str = "empty";
            if (extras != null) {
                l26 l26 = this.y;
                if (l26 != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = str;
                    }
                    String string2 = extras.getString("bottom");
                    if (string2 == null) {
                        string2 = str;
                    }
                    String string3 = extras.getString("right");
                    if (string3 == null) {
                        string3 = str;
                    }
                    String string4 = extras.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string4 == null) {
                        string4 = str;
                    }
                    l26.a(string, string2, string3, string4);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                l26 l262 = this.y;
                if (l262 != null) {
                    String string5 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string5 == null) {
                        string5 = str;
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = str;
                    }
                    String string7 = bundle.getString("right");
                    if (string7 == null) {
                        string7 = str;
                    }
                    String string8 = bundle.getString(ViewHierarchy.DIMENSION_LEFT_KEY);
                    if (string8 != null) {
                        str = string8;
                    }
                    l262.a(string5, string6, string7, str);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        l26 l26 = this.y;
        if (l26 != null) {
            l26.a(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }
}
