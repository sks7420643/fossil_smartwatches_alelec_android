package com.portfolio.platform.uirenew.home.details.workout;

import android.content.Context;
import android.content.Intent;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.ex6;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.x87;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetailActivity extends FlutterActivity {
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public WorkoutSessionRepository a;
    @DexIgnore
    public MethodChannel b;
    @DexIgnore
    public boolean c; // = true;
    @DexIgnore
    public /* final */ yi7 d; // = zi7.a(qj7.c().plus(dl7.a(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            ee7.b(context, "context");
            ee7.b(str, "workoutSessionId");
            ee7.b(str2, "unitType");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutDetailActivity", "start with workout id " + str + " unitType " + str2);
            Intent intent = new Intent(context, WorkoutDetailActivity.class);
            intent.putExtra("EXTRA_WORKOUT_ID", str);
            intent.putExtra("EXTRA_UNIT_TYPE", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1", f = "WorkoutDetailActivity.kt", l = {94, 98}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutDetailActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {
            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutDetailActivity", "Unable to load data: " + str2);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "Unable to load data: notImplemented");
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "success to load data");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b")
        /* renamed from: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b  reason: collision with other inner class name */
        public static final class C0308b extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $unitType$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession $workoutSession$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0308b(fb7 fb7, b bVar, WorkoutSession workoutSession, String str) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$workoutSession$inlined = workoutSession;
                this.$unitType$inlined = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0308b bVar = new C0308b(fb7, this.this$0, this.$workoutSession$inlined, this.$unitType$inlined);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((C0308b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    WorkoutDetailActivity workoutDetailActivity = this.this$0.this$0;
                    WorkoutSession workoutSession = this.$workoutSession$inlined;
                    String str = this.$unitType$inlined;
                    ee7.a((Object) str, "unitType");
                    if (str != null) {
                        String lowerCase = str.toLowerCase();
                        ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        return workoutDetailActivity.a(workoutSession, lowerCase);
                    }
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements MethodChannel.MethodCallHandler {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(String str, b bVar, WorkoutSession workoutSession, String str2) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
            public final void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                ee7.b(methodCall, "call");
                ee7.b(result, "<anonymous parameter 1>");
                if (ee7.a((Object) methodCall.method, (Object) "dismissMap")) {
                    this.a.this$0.c = true;
                    FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "MethodCallHandler dismiss");
                    this.a.this$0.finish();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1$workoutSession$1", f = "WorkoutDetailActivity.kt", l = {94}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super WorkoutSession>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $workoutId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$workoutId = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, this.$workoutId, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super WorkoutSession> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    WorkoutSessionRepository a2 = this.this$0.this$0.a();
                    String str = this.$workoutId;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = a2.getWorkoutSessionById(str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutDetailActivity workoutDetailActivity, fb7 fb7) {
            super(2, fb7);
            this.this$0 = workoutDetailActivity;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00d1 A[SYNTHETIC, Splitter:B:26:0x00d1] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 0
                java.lang.String r3 = "WorkoutDetailActivity"
                r4 = 2
                r5 = 1
                if (r1 == 0) goto L_0x0042
                if (r1 == r5) goto L_0x0032
                if (r1 != r4) goto L_0x002a
                java.lang.Object r0 = r12.L$4
                com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = (com.portfolio.platform.data.model.diana.workout.WorkoutSession) r0
                java.lang.Object r0 = r12.L$3
                com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = (com.portfolio.platform.data.model.diana.workout.WorkoutSession) r0
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r2 = r12.L$1
                java.lang.String r2 = (java.lang.String) r2
                java.lang.Object r2 = r12.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r13)
                goto L_0x00cd
            L_0x002a:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0032:
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r6 = r12.L$1
                java.lang.String r6 = (java.lang.String) r6
                java.lang.Object r7 = r12.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r13)
                goto L_0x0089
            L_0x0042:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r7 = r12.p$
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity r13 = r12.this$0
                android.content.Intent r13 = r13.getIntent()
                java.lang.String r1 = "EXTRA_WORKOUT_ID"
                java.lang.String r13 = r13.getStringExtra(r1)
                if (r13 == 0) goto L_0x0056
                goto L_0x0058
            L_0x0056:
                java.lang.String r13 = ""
            L_0x0058:
                r6 = r13
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity r13 = r12.this$0
                android.content.Intent r13 = r13.getIntent()
                java.lang.String r1 = "EXTRA_UNIT_TYPE"
                java.lang.String r13 = r13.getStringExtra(r1)
                if (r13 == 0) goto L_0x0068
                goto L_0x006e
            L_0x0068:
                com.fossil.ob5 r13 = com.fossil.ob5.METRIC
                java.lang.String r13 = r13.getValue()
            L_0x006e:
                com.fossil.ti7 r1 = com.fossil.qj7.b()
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$d r8 = new com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$d
                r8.<init>(r12, r6, r2)
                r12.L$0 = r7
                r12.L$1 = r6
                r12.L$2 = r13
                r12.label = r5
                java.lang.Object r1 = com.fossil.vh7.a(r1, r8, r12)
                if (r1 != r0) goto L_0x0086
                return r0
            L_0x0086:
                r11 = r1
                r1 = r13
                r13 = r11
            L_0x0089:
                com.portfolio.platform.data.model.diana.workout.WorkoutSession r13 = (com.portfolio.platform.data.model.diana.workout.WorkoutSession) r13
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "loadData workoutId "
                r9.append(r10)
                r9.append(r6)
                java.lang.String r10 = " unitType "
                r9.append(r10)
                r9.append(r1)
                java.lang.String r9 = r9.toString()
                r8.d(r3, r9)
                if (r13 == 0) goto L_0x0119
                com.fossil.ti7 r8 = com.fossil.qj7.a()
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b r9 = new com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$b
                r9.<init>(r2, r12, r13, r1)
                r12.L$0 = r7
                r12.L$1 = r6
                r12.L$2 = r1
                r12.L$3 = r13
                r12.L$4 = r13
                r12.label = r4
                java.lang.Object r2 = com.fossil.vh7.a(r8, r9, r12)
                if (r2 != r0) goto L_0x00cb
                return r0
            L_0x00cb:
                r0 = r13
                r13 = r2
            L_0x00cd:
                java.lang.String r13 = (java.lang.String) r13
                if (r13 == 0) goto L_0x0119
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity r2 = r12.this$0     // Catch:{ Exception -> 0x00f5 }
                io.flutter.plugin.common.MethodChannel r2 = com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.a(r2)     // Catch:{ Exception -> 0x00f5 }
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$c r4 = new com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$c     // Catch:{ Exception -> 0x00f5 }
                r4.<init>(r13, r12, r0, r1)     // Catch:{ Exception -> 0x00f5 }
                r2.setMethodCallHandler(r4)     // Catch:{ Exception -> 0x00f5 }
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity r0 = r12.this$0     // Catch:{ Exception -> 0x00f5 }
                io.flutter.plugin.common.MethodChannel r0 = com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.a(r0)     // Catch:{ Exception -> 0x00f5 }
                java.lang.String r1 = "loadData"
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x00f5 }
                r2.<init>(r13)     // Catch:{ Exception -> 0x00f5 }
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$a r13 = new com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$b$a     // Catch:{ Exception -> 0x00f5 }
                r13.<init>()     // Catch:{ Exception -> 0x00f5 }
                r0.invokeMethod(r1, r2, r13)     // Catch:{ Exception -> 0x00f5 }
                goto L_0x0119
            L_0x00f5:
                r13 = move-exception
                com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity r0 = r12.this$0
                r0.c = r5
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Unable to load data: "
                r1.append(r2)
                java.lang.String r13 = r13.getMessage()
                r1.append(r13)
                java.lang.String r13 = r1.toString()
                r0.d(r3, r13)
            L_0x0119:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public WorkoutDetailActivity() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public static final /* synthetic */ MethodChannel a(WorkoutDetailActivity workoutDetailActivity) {
        MethodChannel methodChannel = workoutDetailActivity.b;
        if (methodChannel != null) {
            return methodChannel;
        }
        ee7.d("mChannel");
        throw null;
    }

    @DexIgnore
    public final ik7 b() {
        return xh7.b(this.d, null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineConfigurator, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        ee7.b(flutterEngine, "flutterEngine");
        DartExecutor dartExecutor = flutterEngine.getDartExecutor();
        ee7.a((Object) dartExecutor, "flutterEngine.dartExecutor");
        this.b = new MethodChannel(dartExecutor.getBinaryMessenger(), "detailTracking/map", JSONMethodCodec.INSTANCE);
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void onFlutterUiDisplayed() {
        super.onFlutterUiDisplayed();
        if (this.c) {
            this.c = false;
            b();
        }
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineProvider, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public FlutterEngine provideFlutterEngine(Context context) {
        ee7.b(context, "context");
        return ex6.c.a();
    }

    @DexIgnore
    public final WorkoutSessionRepository a() {
        WorkoutSessionRepository workoutSessionRepository = this.a;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        ee7.d("mWorkoutSessionRepository");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r0 != null) goto L_0x0033;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(com.portfolio.platform.data.model.diana.workout.WorkoutSession r20, java.lang.String r21) {
        /*
            r19 = this;
            com.fossil.ac5 r0 = r20.getEditedType()
            if (r0 == 0) goto L_0x000b
            com.fossil.ac5 r0 = r20.getEditedType()
            goto L_0x000f
        L_0x000b:
            com.fossil.ac5 r0 = r20.getWorkoutType()
        L_0x000f:
            if (r0 == 0) goto L_0x002d
            java.lang.String r0 = r0.name()
            if (r0 == 0) goto L_0x002d
            if (r0 == 0) goto L_0x0025
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r1 = "(this as java.lang.String).toLowerCase()"
            com.fossil.ee7.a(r0, r1)
            if (r0 == 0) goto L_0x002d
            goto L_0x0033
        L_0x0025:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x002d:
            com.fossil.ac5 r0 = com.fossil.ac5.UNKNOWN
            java.lang.String r0 = r0.name()
        L_0x0033:
            r3 = r0
            com.portfolio.platform.data.model.diana.workout.WorkoutDistance r0 = r20.getDistance()
            if (r0 == 0) goto L_0x0044
            double r4 = r0.getTotal()
            java.lang.Double r0 = java.lang.Double.valueOf(r4)
            r4 = r0
            goto L_0x0045
        L_0x0044:
            r4 = 0
        L_0x0045:
            int r0 = r20.getDuration()
            double r5 = (double) r0
            java.lang.Double r5 = java.lang.Double.valueOf(r5)
            com.portfolio.platform.data.model.diana.workout.WorkoutPace r0 = r20.getPace()
            if (r0 == 0) goto L_0x0065
            java.lang.Float r0 = r0.getAverage()
            if (r0 == 0) goto L_0x0065
            float r0 = r0.floatValue()
            double r6 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r6)
            r6 = r0
            goto L_0x0066
        L_0x0065:
            r6 = 0
        L_0x0066:
            com.portfolio.platform.data.model.diana.workout.WorkoutPace r0 = r20.getPace()
            if (r0 == 0) goto L_0x007d
            java.lang.Float r0 = r0.getBest()
            if (r0 == 0) goto L_0x007d
            float r0 = r0.floatValue()
            double r7 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r7)
            r7 = r0
            goto L_0x007e
        L_0x007d:
            r7 = 0
        L_0x007e:
            com.portfolio.platform.data.model.diana.workout.WorkoutCalorie r0 = r20.getCalorie()
            if (r0 == 0) goto L_0x008f
            float r0 = r0.getTotal()
            double r8 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r8)
            r8 = r0
            goto L_0x0090
        L_0x008f:
            r8 = 0
        L_0x0090:
            com.portfolio.platform.data.model.diana.workout.WorkoutCadence r0 = r20.getCadence()
            if (r0 == 0) goto L_0x00a7
            java.lang.Integer r0 = r0.getAverage()
            if (r0 == 0) goto L_0x00a7
            int r0 = r0.intValue()
            double r9 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r9)
            r9 = r0
            goto L_0x00a8
        L_0x00a7:
            r9 = 0
        L_0x00a8:
            com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate r0 = r20.getHeartRate()
            if (r0 == 0) goto L_0x00b9
            float r0 = r0.getAverage()
            double r10 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r10)
            r10 = r0
            goto L_0x00ba
        L_0x00b9:
            r10 = 0
        L_0x00ba:
            com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate r0 = r20.getHeartRate()
            if (r0 == 0) goto L_0x00cb
            int r0 = r0.getMax()
            double r11 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r11)
            r11 = r0
            goto L_0x00cc
        L_0x00cb:
            r11 = 0
        L_0x00cc:
            org.joda.time.DateTime r0 = r20.getEditedStartTime()
            long r12 = r0.getMillis()
            r0 = 1000(0x3e8, float:1.401E-42)
            long r14 = (long) r0
            long r12 = r12 / r14
            int r0 = (int) r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r0)
            org.joda.time.DateTime r0 = r20.getEditedEndTime()
            long r16 = r0.getMillis()
            long r13 = r16 / r14
            int r0 = (int) r13
            java.lang.Integer r13 = java.lang.Integer.valueOf(r0)
            int r0 = r20.getDuration()
            double r14 = (double) r0
            java.lang.Double r14 = java.lang.Double.valueOf(r14)
            int r0 = r20.getTimezoneOffsetInSecond()
            double r1 = (double) r0
            java.lang.Double r0 = java.lang.Double.valueOf(r1)
            java.util.List r1 = r20.getWorkoutGpsPoints()
            if (r1 == 0) goto L_0x012f
            java.util.ArrayList r2 = new java.util.ArrayList
            r15 = 10
            int r15 = com.fossil.x97.a(r1, r15)
            r2.<init>(r15)
            java.util.Iterator r1 = r1.iterator()
        L_0x0113:
            boolean r15 = r1.hasNext()
            if (r15 == 0) goto L_0x012c
            java.lang.Object r15 = r1.next()
            com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint r15 = (com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint) r15
            r20 = r1
            com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper r1 = new com.portfolio.platform.data.model.diana.workout.WorkoutRouterGpsWrapper
            r1.<init>(r15)
            r2.add(r1)
            r1 = r20
            goto L_0x0113
        L_0x012c:
            r16 = r2
            goto L_0x0131
        L_0x012f:
            r16 = 0
        L_0x0131:
            com.portfolio.platform.data.model.diana.workout.WorkoutMapDataWrapper r15 = new com.portfolio.platform.data.model.diana.workout.WorkoutMapDataWrapper
            r1 = r15
            r2 = r21
            r18 = r15
            r15 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            com.google.gson.Gson r0 = new com.google.gson.Gson
            r0.<init>()
            r1 = r18
            java.lang.String r0 = r0.a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.a(com.portfolio.platform.data.model.diana.workout.WorkoutSession, java.lang.String):java.lang.String");
    }
}
