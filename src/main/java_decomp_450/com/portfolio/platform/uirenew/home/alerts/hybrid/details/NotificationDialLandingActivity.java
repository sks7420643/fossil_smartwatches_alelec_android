package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.lx5;
import com.fossil.mx5;
import com.fossil.px5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDialLandingActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public px5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            ee7.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationDialLandingActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        ee7.a((Object) NotificationDialLandingActivity.class.getSimpleName(), "NotificationDialLandingA\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        lx5 lx5 = (lx5) getSupportFragmentManager().b(2131362149);
        if (lx5 == null) {
            lx5 = lx5.j.b();
            a(lx5, lx5.j.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (lx5 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            ee7.a((Object) supportLoaderManager, "supportLoaderManager");
            f.a(new mx5(lx5, supportLoaderManager)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingContract.View");
    }
}
