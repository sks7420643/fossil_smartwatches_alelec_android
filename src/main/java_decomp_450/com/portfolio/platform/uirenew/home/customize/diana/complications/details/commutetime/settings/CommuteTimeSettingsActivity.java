package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.g16;
import com.fossil.i16;
import com.fossil.r06;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public i16 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            ee7.b(fragment, "fragment");
            ee7.b(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 106);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(2131558428);
        r06 r06 = (r06) getSupportFragmentManager().b(2131362149);
        if (r06 == null) {
            r06 = r06.r.a();
            a(r06, r06.r.b(), 2131362149);
        }
        PortfolioApp.g0.c().f().a(new g16(r06)).a(this);
        if (getIntent() != null) {
            str = getIntent().getStringExtra(Constants.USER_SETTING);
            ee7.a((Object) str, "intent.getStringExtra(Constants.JSON_KEY_SETTINGS)");
        } else {
            str = "";
        }
        i16 i16 = this.y;
        if (i16 != null) {
            i16.c(str);
        } else {
            ee7.d("mCommuteTimeSettingsPresenter");
            throw null;
        }
    }
}
