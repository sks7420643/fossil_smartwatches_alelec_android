package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.bt5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.iy5;
import com.fossil.jy5;
import com.fossil.ny5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ny5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<bt5> arrayList) {
            ee7.b(fragment, "fragment");
            ee7.b(arrayList, "contactWrappersSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridContactActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putExtra("LIST_CONTACTS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 5678);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        ee7.a((Object) NotificationHybridContactActivity.class.getSimpleName(), "NotificationHybridContac\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        iy5 iy5 = (iy5) getSupportFragmentManager().b(2131362149);
        if (iy5 == null) {
            iy5 = iy5.p.b();
            a(iy5, iy5.p.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (iy5 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            ee7.a((Object) supportLoaderManager, "supportLoaderManager");
            f.a(new jy5(iy5, getIntent().getIntExtra("HAND_NUMBER", 0), (ArrayList) getIntent().getSerializableExtra("LIST_CONTACTS_SELECTED"), supportLoaderManager)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactContract.View");
    }
}
