package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.cl5;
import com.fossil.cu5;
import com.fossil.du5;
import com.fossil.ee7;
import com.fossil.gu5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public gu5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            ee7.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        Fragment b = getSupportFragmentManager().b(2131362149);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        cu5 cu5 = (cu5) getSupportFragmentManager().b(2131362149);
        if (cu5 == null) {
            cu5 = cu5.p.b();
            a(cu5, cu5.p.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (cu5 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            ee7.a((Object) supportLoaderManager, "supportLoaderManager");
            f.a(new du5(cu5, supportLoaderManager)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsContract.View");
    }
}
