package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ah;
import com.fossil.cb6;
import com.fossil.da;
import com.fossil.db6;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.go5;
import com.fossil.hb6;
import com.fossil.m15;
import com.fossil.nb6;
import com.fossil.ob6;
import com.fossil.qb;
import com.fossil.qw6;
import com.fossil.qz6;
import com.fossil.tb6;
import com.fossil.tj4;
import com.fossil.ub6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewFragment extends go5 {
    @DexIgnore
    public qw6<m15> f;
    @DexIgnore
    public db6 g;
    @DexIgnore
    public ub6 h;
    @DexIgnore
    public ob6 i;
    @DexIgnore
    public cb6 j;
    @DexIgnore
    public tb6 p;
    @DexIgnore
    public nb6 q;
    @DexIgnore
    public int r; // = 7;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            qw6 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(7, a2 != null ? (m15) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public c(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            qw6 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(4, a2 != null ? (m15) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public d(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            qw6 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(2, a2 != null ? (m15) a2.a() : null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "GoalTrackingOverviewFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        qw6<m15> qw6;
        m15 a2;
        qw6<m15> qw62;
        m15 a3;
        ConstraintLayout constraintLayout;
        qw6<m15> qw63;
        m15 a4;
        FlexibleTextView flexibleTextView;
        qw6<m15> qw64;
        m15 a5;
        String b2 = eh5.l.a().b("hybridGoalTrackingTab");
        String b3 = eh5.l.a().b("onHybridGoalTrackingTab");
        if (!(b2 == null || (qw64 = this.f) == null || (a5 = qw64.a()) == null)) {
            a5.x.setBackgroundColor(Color.parseColor(b2));
            a5.y.setBackgroundColor(Color.parseColor(b2));
        }
        if (!(b3 == null || (qw63 = this.f) == null || (a4 = qw63.a()) == null || (flexibleTextView = a4.t) == null)) {
            flexibleTextView.setTextColor(Color.parseColor(b3));
        }
        this.s = eh5.l.a().b("onHybridInactiveTab");
        String b4 = eh5.l.a().b("nonBrandSurface");
        this.t = eh5.l.a().b("primaryText");
        if (!(b4 == null || (qw62 = this.f) == null || (a3 = qw62.a()) == null || (constraintLayout = a3.q) == null)) {
            constraintLayout.setBackgroundColor(Color.parseColor(b4));
        }
        if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
            FlexibleTextView flexibleTextView2 = a2.u;
            ee7.a((Object) flexibleTextView2, "it.ftvToday");
            if (flexibleTextView2.isSelected()) {
                a2.u.setTextColor(Color.parseColor(this.t));
            } else {
                a2.u.setTextColor(Color.parseColor(this.s));
            }
            FlexibleTextView flexibleTextView3 = a2.r;
            ee7.a((Object) flexibleTextView3, "it.ftv7Days");
            if (flexibleTextView3.isSelected()) {
                a2.r.setTextColor(Color.parseColor(this.t));
            } else {
                a2.r.setTextColor(Color.parseColor(this.s));
            }
            FlexibleTextView flexibleTextView4 = a2.s;
            ee7.a((Object) flexibleTextView4, "it.ftvMonth");
            if (flexibleTextView4.isSelected()) {
                a2.s.setTextColor(Color.parseColor(this.t));
            } else {
                a2.s.setTextColor(Color.parseColor(this.s));
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onCreateView");
        m15 m15 = (m15) qb.a(layoutInflater, 2131558558, viewGroup, false, a1());
        da.d((View) m15.w, false);
        if (bundle != null) {
            this.r = bundle.getInt("CURRENT_TAB", 7);
        }
        ee7.a((Object) m15, "binding");
        a(m15);
        this.f = new qw6<>(this, m15);
        f1();
        qw6<m15> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.r);
    }

    @DexIgnore
    public final void a(m15 m15) {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "initUI");
        this.j = (cb6) getChildFragmentManager().b("GoalTrackingOverviewDayFragment");
        this.p = (tb6) getChildFragmentManager().b("GoalTrackingOverviewWeekFragment");
        this.q = (nb6) getChildFragmentManager().b("GoalTrackingOverviewMonthFragment");
        if (this.j == null) {
            this.j = new cb6();
        }
        if (this.p == null) {
            this.p = new tb6();
        }
        if (this.q == null) {
            this.q = new nb6();
        }
        ArrayList arrayList = new ArrayList();
        cb6 cb6 = this.j;
        if (cb6 != null) {
            arrayList.add(cb6);
            tb6 tb6 = this.p;
            if (tb6 != null) {
                arrayList.add(tb6);
                nb6 nb6 = this.q;
                if (nb6 != null) {
                    arrayList.add(nb6);
                    RecyclerView recyclerView = m15.w;
                    ee7.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new qz6(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new GoalTrackingOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new ah().a(recyclerView);
                    a(this.r, m15);
                    tj4 f2 = PortfolioApp.g0.c().f();
                    cb6 cb62 = this.j;
                    if (cb62 != null) {
                        tb6 tb62 = this.p;
                        if (tb62 != null) {
                            nb6 nb62 = this.q;
                            if (nb62 != null) {
                                f2.a(new hb6(cb62, tb62, nb62)).a(this);
                                m15.u.setOnClickListener(new b(this));
                                m15.r.setOnClickListener(new c(this));
                                m15.s.setOnClickListener(new d(this));
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(int i2, m15 m15) {
        qw6<m15> qw6;
        m15 a2;
        m15 a3;
        RecyclerView recyclerView;
        m15 a4;
        RecyclerView recyclerView2;
        m15 a5;
        RecyclerView recyclerView3;
        m15 a6;
        RecyclerView recyclerView4;
        if (m15 != null) {
            FlexibleTextView flexibleTextView = m15.u;
            ee7.a((Object) flexibleTextView, "it.ftvToday");
            flexibleTextView.setSelected(false);
            FlexibleTextView flexibleTextView2 = m15.r;
            ee7.a((Object) flexibleTextView2, "it.ftv7Days");
            flexibleTextView2.setSelected(false);
            FlexibleTextView flexibleTextView3 = m15.s;
            ee7.a((Object) flexibleTextView3, "it.ftvMonth");
            flexibleTextView3.setSelected(false);
            FlexibleTextView flexibleTextView4 = m15.u;
            ee7.a((Object) flexibleTextView4, "it.ftvToday");
            flexibleTextView4.setPaintFlags(0);
            FlexibleTextView flexibleTextView5 = m15.r;
            ee7.a((Object) flexibleTextView5, "it.ftv7Days");
            flexibleTextView5.setPaintFlags(0);
            FlexibleTextView flexibleTextView6 = m15.s;
            ee7.a((Object) flexibleTextView6, "it.ftvMonth");
            flexibleTextView6.setPaintFlags(0);
            if (i2 == 2) {
                FlexibleTextView flexibleTextView7 = m15.s;
                ee7.a((Object) flexibleTextView7, "it.ftvMonth");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = m15.s;
                ee7.a((Object) flexibleTextView8, "it.ftvMonth");
                FlexibleTextView flexibleTextView9 = m15.r;
                ee7.a((Object) flexibleTextView9, "it.ftv7Days");
                flexibleTextView8.setPaintFlags(flexibleTextView9.getPaintFlags() | 8 | 1);
                qw6<m15> qw62 = this.f;
                if (!(qw62 == null || (a3 = qw62.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                FlexibleTextView flexibleTextView10 = m15.r;
                ee7.a((Object) flexibleTextView10, "it.ftv7Days");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = m15.r;
                ee7.a((Object) flexibleTextView11, "it.ftv7Days");
                FlexibleTextView flexibleTextView12 = m15.r;
                ee7.a((Object) flexibleTextView12, "it.ftv7Days");
                flexibleTextView11.setPaintFlags(flexibleTextView12.getPaintFlags() | 8 | 1);
                qw6<m15> qw63 = this.f;
                if (!(qw63 == null || (a4 = qw63.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                FlexibleTextView flexibleTextView13 = m15.u;
                ee7.a((Object) flexibleTextView13, "it.ftvToday");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = m15.u;
                ee7.a((Object) flexibleTextView14, "it.ftvToday");
                FlexibleTextView flexibleTextView15 = m15.r;
                ee7.a((Object) flexibleTextView15, "it.ftv7Days");
                flexibleTextView14.setPaintFlags(flexibleTextView15.getPaintFlags() | 8 | 1);
                qw6<m15> qw64 = this.f;
                if (!(qw64 == null || (a6 = qw64.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                FlexibleTextView flexibleTextView16 = m15.u;
                ee7.a((Object) flexibleTextView16, "it.ftvToday");
                flexibleTextView16.setSelected(true);
                FlexibleTextView flexibleTextView17 = m15.u;
                ee7.a((Object) flexibleTextView17, "it.ftvToday");
                FlexibleTextView flexibleTextView18 = m15.r;
                ee7.a((Object) flexibleTextView18, "it.ftv7Days");
                flexibleTextView17.setPaintFlags(flexibleTextView18.getPaintFlags() | 8 | 1);
                qw6<m15> qw65 = this.f;
                if (!(qw65 == null || (a5 = qw65.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.s) && !TextUtils.isEmpty(this.t) && (qw6 = this.f) != null && (a2 = qw6.a()) != null) {
                FlexibleTextView flexibleTextView19 = a2.u;
                ee7.a((Object) flexibleTextView19, "it.ftvToday");
                if (flexibleTextView19.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView20 = a2.r;
                ee7.a((Object) flexibleTextView20, "it.ftv7Days");
                if (flexibleTextView20.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.s));
                }
                FlexibleTextView flexibleTextView21 = a2.s;
                ee7.a((Object) flexibleTextView21, "it.ftvMonth");
                if (flexibleTextView21.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.t));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.s));
                }
            }
        }
    }
}
