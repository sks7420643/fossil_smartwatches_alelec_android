package com.portfolio.platform.uirenew.connectedapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.kh6;
import com.fossil.tj4;
import com.fossil.vq5;
import com.fossil.x87;
import com.fossil.xq5;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsActivity extends cl5 {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public kh6 y;
    @DexIgnore
    public xq5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            Intent intent = new Intent(context, ConnectedAppsActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        kh6 kh6 = this.y;
        if (kh6 != null) {
            kh6.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        kh6 kh6 = (kh6) getSupportFragmentManager().b(2131362149);
        this.y = kh6;
        if (kh6 == null) {
            kh6 b = kh6.j.b();
            this.y = b;
            if (b != null) {
                a(b, kh6.j.a(), 2131362149);
            } else {
                ee7.a();
                throw null;
            }
        }
        tj4 f = PortfolioApp.g0.c().f();
        kh6 kh62 = this.y;
        if (kh62 != null) {
            f.a(new vq5(kh62)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.connectedapps.ConnectedAppsContract.View");
    }
}
