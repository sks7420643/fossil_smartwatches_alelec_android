package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.at6;
import com.fossil.bt6;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.et6;
import com.fossil.ih5;
import com.fossil.jh5;
import com.fossil.kh5;
import com.fossil.lh5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpActivity extends cl5 {
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public jh5 A;
    @DexIgnore
    public lh5 B;
    @DexIgnore
    public kh5 C;
    @DexIgnore
    public et6 y;
    @DexIgnore
    public ih5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            context.startActivity(new Intent(context, SignUpActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            ih5 ih5 = this.z;
            if (ih5 != null) {
                ih5.a(i, i2, intent);
                jh5 jh5 = this.A;
                if (jh5 != null) {
                    jh5.a(i, i2, intent);
                    lh5 lh5 = this.B;
                    if (lh5 != null) {
                        lh5.a(i, i2, intent);
                        Fragment b = getSupportFragmentManager().b(2131362149);
                        if (b != null) {
                            b.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    ee7.d("mLoginWeiboManager");
                    throw null;
                }
                ee7.d("mLoginGoogleManager");
                throw null;
            }
            ee7.d("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        at6 at6 = (at6) getSupportFragmentManager().b(2131362149);
        if (at6 == null) {
            at6 = at6.i.a();
            a(at6, e(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (at6 != null) {
            f.a(new bt6(this, at6)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.SignUpContract.View");
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        ee7.b(intent, "intent");
        super.onNewIntent(intent);
        kh5 kh5 = this.C;
        if (kh5 != null) {
            Intent intent2 = getIntent();
            ee7.a((Object) intent2, "getIntent()");
            kh5.a(intent2);
            return;
        }
        ee7.d("mLoginWechatManager");
        throw null;
    }
}
