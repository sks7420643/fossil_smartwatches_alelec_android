package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.nu6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingActivity extends cl5 {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchSettingActivity.y;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            ee7.b(context, "context");
            ee7.b(str, "serial");
            Intent intent = new Intent(context, WatchSettingActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchSettingActivity.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchSettingActivity::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        nu6 nu6 = (nu6) getSupportFragmentManager().b(2131362149);
        if (nu6 == null) {
            nu6 = nu6.s.b();
            a(nu6, nu6.s.a(), 2131362149);
        }
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = e();
            local.d(e, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("SERIAL", string);
                nu6.setArguments(bundle2);
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String e2 = e();
            local2.d(e2, "retrieve serial from intent " + stringExtra);
            Bundle bundle3 = new Bundle();
            bundle3.putString("SERIAL", stringExtra);
            nu6.setArguments(bundle3);
        }
    }
}
