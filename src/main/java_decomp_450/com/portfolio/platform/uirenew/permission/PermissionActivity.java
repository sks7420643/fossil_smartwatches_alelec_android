package com.portfolio.platform.uirenew.permission;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.os6;
import com.fossil.qs6;
import com.fossil.tj4;
import com.fossil.ts6;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PermissionData;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ts6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, ArrayList arrayList, boolean z, boolean z2, Integer num, int i, Object obj) {
            boolean z3 = (i & 4) != 0 ? false : z;
            boolean z4 = (i & 8) != 0 ? false : z2;
            if ((i & 16) != 0) {
                num = null;
            }
            aVar.a(context, arrayList, z3, z4, num);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, ArrayList<PermissionData> arrayList, boolean z, boolean z2, Integer num) {
            ee7.b(context, "context");
            ee7.b(arrayList, "listPermissions");
            Intent intent = new Intent(context, PermissionActivity.class);
            intent.putExtra("PERMISSION_SKIP_ABLE_FLAG", z);
            intent.putExtra("EXTRA_LIST_PERMISSIONS", arrayList);
            intent.setFlags(536870912);
            if (z2 || z) {
                int i = 111;
                if (num != null) {
                    i = num.intValue();
                }
                ((Activity) context).startActivityForResult(intent, i);
                return;
            }
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (Build.VERSION.SDK_INT < 26) {
            setRequestedOrientation(1);
        }
        boolean booleanExtra = getIntent().getBooleanExtra("PERMISSION_SKIP_ABLE_FLAG", false);
        ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("EXTRA_LIST_PERMISSIONS");
        if (parcelableArrayListExtra != null) {
            os6 os6 = (os6) getSupportFragmentManager().b(2131362149);
            if (booleanExtra) {
                if (os6 == null) {
                    os6 = os6.q.a(booleanExtra);
                    a(os6, os6.q.a(), 2131362149);
                }
            } else if (os6 == null) {
                os6 = os6.q.b();
                a(os6, os6.q.a(), 2131362149);
            }
            tj4 f = PortfolioApp.g0.c().f();
            if (os6 != null) {
                f.a(new qs6(os6, parcelableArrayListExtra)).a(this);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionContract.View");
        }
        ee7.a();
        throw null;
    }
}
