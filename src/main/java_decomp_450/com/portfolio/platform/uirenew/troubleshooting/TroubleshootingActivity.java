package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.be5;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.eu6;
import com.fossil.fu6;
import com.fossil.hu6;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TroubleshootingActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public hu6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, String str, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                str = "";
            }
            if ((i & 4) != 0) {
                z = false;
            }
            if ((i & 8) != 0) {
                z2 = false;
            }
            aVar.a(context, str, z, z2);
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z, boolean z2) {
            ee7.b(context, "context");
            ee7.b(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            if (TextUtils.isEmpty(str) || be5.o.f(str)) {
                intent.putExtra("DEVICE_TYPE", false);
            } else {
                intent.putExtra("DEVICE_TYPE", true);
            }
            intent.putExtra("IS_PAIRING_FLOW", z);
            intent.putExtra("IS_ONBOARDING_FLOW", z2);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        eu6 eu6 = (eu6) getSupportFragmentManager().b(2131362149);
        Intent intent = getIntent();
        ee7.a((Object) intent, "intent");
        boolean a2 = a(intent);
        Intent intent2 = getIntent();
        ee7.a((Object) intent2, "intent");
        Bundle extras = intent2.getExtras();
        boolean z2 = false;
        boolean z3 = extras != null ? extras.getBoolean("IS_PAIRING_FLOW", false) : false;
        Intent intent3 = getIntent();
        ee7.a((Object) intent3, "intent");
        Bundle extras2 = intent3.getExtras();
        if (extras2 != null) {
            z2 = extras2.getBoolean("IS_ONBOARDING_FLOW", false);
        }
        if (eu6 == null) {
            eu6 = eu6.t.a(a2, z3, z2);
            a(eu6, eu6.t.d(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (eu6 != null) {
            f.a(new fu6(eu6)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
