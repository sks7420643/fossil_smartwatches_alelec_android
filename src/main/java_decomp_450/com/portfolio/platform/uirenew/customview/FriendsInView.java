package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.ee7;
import com.fossil.fu4;
import com.fossil.hd5;
import com.fossil.ig5;
import com.fossil.oy6;
import com.fossil.rt4;
import com.fossil.st4;
import com.fossil.w97;
import com.fossil.we7;
import com.fossil.xe5;
import com.fossil.xn4;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FriendsInView extends ConstraintLayout {
    @DexIgnore
    public FlexibleTextView A;
    @DexIgnore
    public oy6.b B;
    @DexIgnore
    public st4 C;
    @DexIgnore
    public ImageView v;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public ImageView y;
    @DexIgnore
    public ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context) {
        super(context);
        ee7.b(context, "context");
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        oy6.b b = oy6.a().b();
        ee7.a((Object) b, "TextDrawable.builder().round()");
        this.B = b;
        this.C = st4.d.a();
        View inflate = View.inflate(context, 2131558644, this);
        View findViewById = inflate.findViewById(2131362588);
        ee7.a((Object) findViewById, "view.findViewById(R.id.img1)");
        this.v = (ImageView) findViewById;
        View findViewById2 = inflate.findViewById(2131362589);
        ee7.a((Object) findViewById2, "view.findViewById(R.id.img2)");
        this.w = (ImageView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362590);
        ee7.a((Object) findViewById3, "view.findViewById(R.id.img3)");
        this.x = (ImageView) findViewById3;
        View findViewById4 = inflate.findViewById(2131362591);
        ee7.a((Object) findViewById4, "view.findViewById(R.id.img4)");
        this.y = (ImageView) findViewById4;
        View findViewById5 = inflate.findViewById(2131362592);
        ee7.a((Object) findViewById5, "view.findViewById(R.id.img5)");
        this.z = (ImageView) findViewById5;
        View findViewById6 = inflate.findViewById(2131362378);
        ee7.a((Object) findViewById6, "view.findViewById(R.id.ftv_description)");
        this.A = (FlexibleTextView) findViewById6;
    }

    @DexIgnore
    public final void setData(List<xn4> list) {
        String e;
        String str;
        String e2;
        ee7.b(list, "friendsIn");
        if (list.isEmpty()) {
            setVisibility(8);
            return;
        }
        ee7.a((Object) hd5.a(getContext()), "GlideApp.with(context)");
        int size = list.size();
        Iterator<T> it = list.iterator();
        int i = 0;
        while (true) {
            boolean hasNext = it.hasNext();
            String str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            if (hasNext) {
                T next = it.next();
                int i2 = i + 1;
                if (i >= 0) {
                    T t = next;
                    fu4 fu4 = fu4.a;
                    String b = t != null ? t.b() : null;
                    String d = t != null ? t.d() : null;
                    if (!(t == null || (e2 = t.e()) == null)) {
                        str2 = e2;
                    }
                    String a = fu4.a(b, d, str2);
                    if (i == 0) {
                        ImageView imageView = this.v;
                        if (imageView != null) {
                            imageView.setVisibility(0);
                            ImageView imageView2 = this.v;
                            if (imageView2 != null) {
                                String a2 = t != null ? t.a() : null;
                                oy6.b bVar = this.B;
                                if (bVar != null) {
                                    st4 st4 = this.C;
                                    if (st4 != null) {
                                        rt4.a(imageView2, a2, a, bVar, st4);
                                    } else {
                                        ee7.d("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                ee7.d("img1");
                                throw null;
                            }
                        } else {
                            ee7.d("img1");
                            throw null;
                        }
                    } else if (i == 1) {
                        ImageView imageView3 = this.w;
                        if (imageView3 != null) {
                            imageView3.setVisibility(0);
                            ImageView imageView4 = this.w;
                            if (imageView4 != null) {
                                String a3 = t != null ? t.a() : null;
                                oy6.b bVar2 = this.B;
                                if (bVar2 != null) {
                                    st4 st42 = this.C;
                                    if (st42 != null) {
                                        rt4.a(imageView4, a3, a, bVar2, st42);
                                    } else {
                                        ee7.d("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                ee7.d("img2");
                                throw null;
                            }
                        } else {
                            ee7.d("img2");
                            throw null;
                        }
                    } else if (i == 2) {
                        ImageView imageView5 = this.x;
                        if (imageView5 != null) {
                            imageView5.setVisibility(0);
                            ImageView imageView6 = this.x;
                            if (imageView6 != null) {
                                String a4 = t != null ? t.a() : null;
                                oy6.b bVar3 = this.B;
                                if (bVar3 != null) {
                                    st4 st43 = this.C;
                                    if (st43 != null) {
                                        rt4.a(imageView6, a4, a, bVar3, st43);
                                    } else {
                                        ee7.d("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                ee7.d("img3");
                                throw null;
                            }
                        } else {
                            ee7.d("img3");
                            throw null;
                        }
                    } else if (i == 3) {
                        ImageView imageView7 = this.y;
                        if (imageView7 != null) {
                            imageView7.setVisibility(0);
                            ImageView imageView8 = this.y;
                            if (imageView8 != null) {
                                String a5 = t != null ? t.a() : null;
                                oy6.b bVar4 = this.B;
                                if (bVar4 != null) {
                                    st4 st44 = this.C;
                                    if (st44 != null) {
                                        rt4.a(imageView8, a5, a, bVar4, st44);
                                    } else {
                                        ee7.d("colorGenerator");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("drawableBuilder");
                                    throw null;
                                }
                            } else {
                                ee7.d("img4");
                                throw null;
                            }
                        } else {
                            ee7.d("img4");
                            throw null;
                        }
                    } else {
                        continue;
                    }
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            } else {
                if (size > 5) {
                    ImageView imageView9 = this.z;
                    if (imageView9 != null) {
                        imageView9.setVisibility(0);
                        ImageView imageView10 = this.z;
                        if (imageView10 != null) {
                            oy6.b bVar5 = this.B;
                            if (bVar5 != null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append('+');
                                sb.append(list.size() - 4);
                                String sb2 = sb.toString();
                                st4 st45 = this.C;
                                if (st45 != null) {
                                    imageView10.setImageDrawable(bVar5.b(sb2, st45.a()));
                                } else {
                                    ee7.d("colorGenerator");
                                    throw null;
                                }
                            } else {
                                ee7.d("drawableBuilder");
                                throw null;
                            }
                        } else {
                            ee7.d("img5");
                            throw null;
                        }
                    } else {
                        ee7.d("img5");
                        throw null;
                    }
                } else if (size == 5) {
                    ImageView imageView11 = this.z;
                    if (imageView11 != null) {
                        imageView11.setVisibility(0);
                        ImageView imageView12 = this.z;
                        if (imageView12 != null) {
                            xn4 xn4 = list.get(4);
                            String a6 = xn4 != null ? xn4.a() : null;
                            fu4 fu42 = fu4.a;
                            xn4 xn42 = list.get(4);
                            String b2 = xn42 != null ? xn42.b() : null;
                            xn4 xn43 = list.get(4);
                            String d2 = xn43 != null ? xn43.d() : null;
                            xn4 xn44 = list.get(4);
                            if (xn44 == null || (str = xn44.e()) == null) {
                                str = str2;
                            }
                            String a7 = fu42.a(b2, d2, str);
                            oy6.b bVar6 = this.B;
                            if (bVar6 != null) {
                                st4 st46 = this.C;
                                if (st46 != null) {
                                    rt4.a(imageView12, a6, a7, bVar6, st46);
                                } else {
                                    ee7.d("colorGenerator");
                                    throw null;
                                }
                            } else {
                                ee7.d("drawableBuilder");
                                throw null;
                            }
                        } else {
                            ee7.d("img5");
                            throw null;
                        }
                    } else {
                        ee7.d("img5");
                        throw null;
                    }
                }
                fu4 fu43 = fu4.a;
                xn4 xn45 = list.get(0);
                String b3 = xn45 != null ? xn45.b() : null;
                xn4 xn46 = list.get(0);
                String d3 = xn46 != null ? xn46.d() : null;
                xn4 xn47 = list.get(0);
                if (!(xn47 == null || (e = xn47.e()) == null)) {
                    str2 = e;
                }
                String a8 = fu43.a(b3, d3, str2);
                if (size > 1) {
                    we7 we7 = we7.a;
                    String a9 = ig5.a(getContext(), 2131886209);
                    ee7.a((Object) a9, "LanguageHelper.getString\u2026ameAndNumberOthersJoined)");
                    String format = String.format(a9, Arrays.copyOf(new Object[]{a8, String.valueOf(size - 1)}, 2));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    Spannable a10 = xe5.a(xe5.b, a8, format, 0, 4, null);
                    FlexibleTextView flexibleTextView = this.A;
                    if (flexibleTextView != null) {
                        flexibleTextView.setText(a10);
                        return;
                    } else {
                        ee7.d("ftvDes");
                        throw null;
                    }
                } else {
                    we7 we72 = we7.a;
                    String a11 = ig5.a(getContext(), 2131886210);
                    ee7.a((Object) a11, "LanguageHelper.getString\u2026ge_Label__UsernameJoined)");
                    String format2 = String.format(a11, Arrays.copyOf(new Object[]{a8}, 1));
                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    Spannable a12 = xe5.a(xe5.b, a8, format2, 0, 4, null);
                    FlexibleTextView flexibleTextView2 = this.A;
                    if (flexibleTextView2 != null) {
                        flexibleTextView2.setText(a12);
                        return;
                    } else {
                        ee7.d("ftvDes");
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        a(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FriendsInView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        a(context, attributeSet);
    }
}
