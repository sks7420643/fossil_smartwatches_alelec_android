package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ListView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ar5;
import com.fossil.da;
import com.fossil.hj;
import com.fossil.t9;
import com.fossil.u9;
import com.fossil.v6;
import com.fossil.x9;
import com.fossil.y9;
import com.fossil.ya;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class InterceptSwipe extends ViewGroup implements x9, t9 {
    @DexIgnore
    public static /* final */ String V; // = InterceptSwipe.class.getSimpleName();
    @DexIgnore
    public static /* final */ int[] W; // = {16842766};
    @DexIgnore
    public ar5 A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public float D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public hj H;
    @DexIgnore
    public Animation I;
    @DexIgnore
    public Animation J;
    @DexIgnore
    public Animation K;
    @DexIgnore
    public Animation L;
    @DexIgnore
    public Animation M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public int O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public i Q;
    @DexIgnore
    public Animation.AnimationListener R;
    @DexIgnore
    public Float S;
    @DexIgnore
    public /* final */ Animation T;
    @DexIgnore
    public /* final */ Animation U;
    @DexIgnore
    public String a;
    @DexIgnore
    public View b;
    @DexIgnore
    public j c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public /* final */ y9 h;
    @DexIgnore
    public /* final */ u9 i;
    @DexIgnore
    public /* final */ int[] j;
    @DexIgnore
    public /* final */ int[] p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ DecelerateInterpolator z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animation.AnimationListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            j jVar;
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            if (interceptSwipe.d) {
                interceptSwipe.H.setAlpha(255);
                InterceptSwipe.this.H.start();
                InterceptSwipe interceptSwipe2 = InterceptSwipe.this;
                if (interceptSwipe2.N && (jVar = interceptSwipe2.c) != null) {
                    jVar.a();
                }
                InterceptSwipe interceptSwipe3 = InterceptSwipe.this;
                interceptSwipe3.s = interceptSwipe3.A.getTop();
                return;
            }
            interceptSwipe.d();
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Animation {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.setAnimationProgress(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends Animation {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.setAnimationProgress(1.0f - f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends Animation {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public d(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            hj hjVar = InterceptSwipe.this.H;
            int i = this.a;
            hjVar.setAlpha((int) (((float) i) + (((float) (this.b - i)) * f)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Animation.AnimationListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            if (!interceptSwipe.x) {
                interceptSwipe.a((Animation.AnimationListener) null);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends Animation {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            int i;
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            if (!interceptSwipe.P) {
                i = interceptSwipe.F - Math.abs(interceptSwipe.E);
            } else {
                i = interceptSwipe.F;
            }
            InterceptSwipe interceptSwipe2 = InterceptSwipe.this;
            int i2 = interceptSwipe2.C;
            InterceptSwipe.this.setTargetOffsetTopAndBottom((i2 + ((int) (((float) (i - i2)) * f))) - interceptSwipe2.A.getTop());
            InterceptSwipe.this.H.a(1.0f - f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends Animation {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.c(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends Animation {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            float f2 = interceptSwipe.D;
            interceptSwipe.setAnimationProgress(f2 + ((-f2) * f));
            InterceptSwipe.this.c(f);
        }
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        boolean a(InterceptSwipe interceptSwipe, View view);
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public InterceptSwipe(Context context) {
        this(context, null);
    }

    @DexIgnore
    private void setColorViewAlpha(int i2) {
        this.A.getBackground().setAlpha(i2);
        this.H.setAlpha(i2);
    }

    @DexIgnore
    public void a(boolean z2, int i2, int i3) {
        this.x = z2;
        this.E = i2;
        this.F = i3;
        this.P = true;
        d();
        this.d = false;
    }

    @DexIgnore
    public final void b() {
        this.A = new ar5(getContext(), -328966);
        hj hjVar = new hj(getContext());
        this.H = hjVar;
        hjVar.a(1);
        this.A.setImageDrawable(this.H);
        this.A.setVisibility(8);
        addView(this.A);
    }

    @DexIgnore
    public final void c() {
        if (this.b == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.A)) {
                    this.b = childAt;
                    return;
                }
            }
        }
    }

    @DexIgnore
    public void d() {
        this.A.clearAnimation();
        this.H.stop();
        this.A.setVisibility(8);
        setColorViewAlpha(255);
        if (this.x) {
            setAnimationProgress(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else {
            setTargetOffsetTopAndBottom(this.E - this.s);
        }
        this.s = this.A.getTop();
    }

    @DexIgnore
    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.i.a(f2, f3, z2);
    }

    @DexIgnore
    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.i.a(f2, f3);
    }

    @DexIgnore
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.i.a(i2, i3, iArr, iArr2);
    }

    @DexIgnore
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.i.a(i2, i3, i4, i5, iArr);
    }

    @DexIgnore
    public final void e() {
        this.L = a(this.H.getAlpha(), 255);
    }

    @DexIgnore
    public final void f() {
        this.K = a(this.H.getAlpha(), 76);
    }

    @DexIgnore
    public int getChildDrawingOrder(int i2, int i3) {
        int i4 = this.B;
        if (i4 < 0) {
            return i3;
        }
        if (i3 == i2 - 1) {
            return i4;
        }
        return i3 >= i4 ? i3 + 1 : i3;
    }

    @DexIgnore
    public int getNestedScrollAxes() {
        return this.h.a();
    }

    @DexIgnore
    public int getProgressCircleDiameter() {
        return this.O;
    }

    @DexIgnore
    public int getProgressViewEndOffset() {
        return this.F;
    }

    @DexIgnore
    public int getProgressViewStartOffset() {
        return this.E;
    }

    @DexIgnore
    public boolean hasNestedScrollingParent() {
        return this.i.b();
    }

    @DexIgnore
    @Override // com.fossil.t9
    public boolean isNestedScrollingEnabled() {
        return this.i.c();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        d();
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        a(motionEvent, -1.0f);
        c();
        int actionMasked = motionEvent.getActionMasked();
        if (this.y && actionMasked == 0) {
            this.y = false;
        }
        if (!isEnabled() || this.y || a() || this.d || this.q) {
            return false;
        }
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int i2 = this.w;
                    if (i2 == -1) {
                        Log.e(V, "Got ACTION_MOVE event but don't have an active pointer id.");
                        return false;
                    }
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = this.a;
                    local.e(str, "onInterceptTouchEvent - ActionMove - mActivePointerId " + this.w);
                    if (findPointerIndex < 0) {
                        return false;
                    }
                    d(motionEvent.getY(findPointerIndex));
                } else if (actionMasked != 3) {
                    if (actionMasked == 6) {
                        a(motionEvent);
                    }
                }
            }
            this.v = false;
            this.w = -1;
        } else {
            setTargetOffsetTopAndBottom(this.E - this.A.getTop());
            int pointerId = motionEvent.getPointerId(0);
            this.w = pointerId;
            this.v = false;
            int findPointerIndex2 = motionEvent.findPointerIndex(pointerId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.e(str2, "onInterceptTouchEvent - ActionDown - pointerIndex " + findPointerIndex2);
            if (findPointerIndex2 < 0) {
                return false;
            }
            this.u = motionEvent.getY(findPointerIndex2);
        }
        return this.v;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.b == null) {
                c();
            }
            View view = this.b;
            if (view != null) {
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.A.getMeasuredWidth();
                int measuredHeight2 = this.A.getMeasuredHeight();
                int i6 = measuredWidth / 2;
                int i7 = measuredWidth2 / 2;
                int i8 = this.s;
                this.A.layout(i6 - i7, i8, i6 + i7, measuredHeight2 + i8);
            }
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.b == null) {
            c();
        }
        View view = this.b;
        if (view != null) {
            view.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.A.measure(View.MeasureSpec.makeMeasureSpec(this.O, 1073741824), View.MeasureSpec.makeMeasureSpec(this.O, 1073741824));
            this.B = -1;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                if (getChildAt(i4) == this.A) {
                    this.B = i4;
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.x9
    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        return dispatchNestedFling(f2, f3, z2);
    }

    @DexIgnore
    @Override // com.fossil.x9
    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    @DexIgnore
    @Override // com.fossil.x9
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0) {
            float f2 = this.g;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                float f3 = (float) i3;
                if (f3 > f2) {
                    iArr[1] = i3 - ((int) f2);
                    this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                } else {
                    this.g = f2 - f3;
                    iArr[1] = i3;
                }
                b(this.g);
            }
        }
        if (this.P && i3 > 0 && this.g == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && Math.abs(i3 - iArr[1]) > 0) {
            this.A.setVisibility(8);
        }
        int[] iArr2 = this.j;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr[1] + iArr2[1];
        }
    }

    @DexIgnore
    @Override // com.fossil.x9
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        dispatchNestedScroll(i2, i3, i4, i5, this.p);
        int i6 = i5 + this.p[1];
        if (i6 < 0 && !a()) {
            float abs = this.g + ((float) Math.abs(i6));
            this.g = abs;
            b(abs);
        }
    }

    @DexIgnore
    @Override // com.fossil.x9
    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.h.a(view, view2, i2);
        startNestedScroll(i2 & 2);
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.q = true;
    }

    @DexIgnore
    @Override // com.fossil.x9
    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return isEnabled() && !this.y && !this.d && (i2 & 2) != 0;
    }

    @DexIgnore
    @Override // com.fossil.x9
    public void onStopNestedScroll(View view) {
        this.h.a(view);
        this.q = false;
        float f2 = this.g;
        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            a(f2);
            this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        stopNestedScroll();
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (this.y && actionMasked == 0) {
            this.y = false;
        }
        if (!isEnabled() || this.y || a() || this.d || this.q) {
            return false;
        }
        if (actionMasked == 0) {
            FLogger.INSTANCE.getLocal().e(this.a, "onTouch - ActionDown");
            this.w = motionEvent.getPointerId(0);
            this.v = false;
        } else if (actionMasked == 1) {
            int findPointerIndex = motionEvent.findPointerIndex(this.w);
            if (findPointerIndex < 0) {
                Log.e(V, "Got ACTION_UP event but don't have an active pointer id.");
                return false;
            }
            if (this.v) {
                this.v = false;
                a((motionEvent.getY(findPointerIndex) - this.t) * 0.5f);
            }
            this.w = -1;
            return false;
        } else if (actionMasked == 2) {
            FLogger.INSTANCE.getLocal().e(this.a, "onTouch - ActionMove");
            int findPointerIndex2 = motionEvent.findPointerIndex(this.w);
            if (findPointerIndex2 < 0) {
                Log.e(V, "Got ACTION_MOVE event but have an invalid active pointer id.");
                return false;
            }
            float y2 = motionEvent.getY(findPointerIndex2);
            d(y2);
            if (this.v) {
                float f2 = (y2 - this.t) * 0.5f;
                if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return false;
                }
                b(f2);
                a(motionEvent, f2);
            } else {
                a(motionEvent, -1.0f);
            }
        } else if (actionMasked == 3) {
            return false;
        } else {
            if (actionMasked == 5) {
                int actionIndex = motionEvent.getActionIndex();
                if (actionIndex < 0) {
                    Log.e(V, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
                this.w = motionEvent.getPointerId(actionIndex);
            } else if (actionMasked == 6) {
                a(motionEvent);
            }
        }
        return true;
    }

    @DexIgnore
    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (Build.VERSION.SDK_INT >= 21 || !(this.b instanceof AbsListView)) {
            View view = this.b;
            if (view == null || da.H(view)) {
                super.requestDisallowInterceptTouchEvent(z2);
            }
        }
    }

    @DexIgnore
    public void setAnimationProgress(float f2) {
        this.A.setScaleX(f2);
        this.A.setScaleY(f2);
    }

    @DexIgnore
    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    @DexIgnore
    public void setColorSchemeColors(int... iArr) {
        c();
        this.H.a(iArr);
    }

    @DexIgnore
    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = v6.a(context, iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    @DexIgnore
    public void setDistanceToTriggerSync(int i2) {
        this.f = (float) i2;
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!z2) {
            d();
        }
    }

    @DexIgnore
    @Override // com.fossil.t9
    public void setNestedScrollingEnabled(boolean z2) {
        this.i.a(z2);
    }

    @DexIgnore
    public void setOnChildScrollUpCallback(i iVar) {
        this.Q = iVar;
    }

    @DexIgnore
    public void setOnRefreshListener(j jVar) {
        this.c = jVar;
    }

    @DexIgnore
    @Deprecated
    public void setProgressBackgroundColor(int i2) {
        setProgressBackgroundColorSchemeResource(i2);
    }

    @DexIgnore
    public void setProgressBackgroundColorSchemeColor(int i2) {
        this.A.setBackgroundColor(i2);
    }

    @DexIgnore
    public void setProgressBackgroundColorSchemeResource(int i2) {
        setProgressBackgroundColorSchemeColor(v6.a(getContext(), i2));
    }

    @DexIgnore
    public void setRefreshing(boolean z2) {
        int i2;
        if (!z2 || this.d == z2) {
            a(z2, false);
            return;
        }
        this.d = z2;
        if (!this.P) {
            i2 = this.F + this.E;
        } else {
            i2 = this.F;
        }
        setTargetOffsetTopAndBottom(i2 - this.s);
        this.N = false;
        b(this.R);
    }

    @DexIgnore
    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                this.O = (int) (displayMetrics.density * 56.0f);
            } else {
                this.O = (int) (displayMetrics.density * 40.0f);
            }
            this.A.setImageDrawable(null);
            this.H.a(i2);
            this.A.setImageDrawable(this.H);
        }
    }

    @DexIgnore
    public void setSlingshotDistance(int i2) {
        this.G = i2;
    }

    @DexIgnore
    public void setTargetOffsetTopAndBottom(int i2) {
        this.A.bringToFront();
        da.e(this.A, i2);
        this.s = this.A.getTop();
    }

    @DexIgnore
    public boolean startNestedScroll(int i2) {
        return this.i.c(i2);
    }

    @DexIgnore
    @Override // com.fossil.t9
    public void stopNestedScroll() {
        this.i.d();
    }

    @DexIgnore
    public InterceptSwipe(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = InterceptSwipe.class.getSimpleName();
        this.d = false;
        this.f = -1.0f;
        this.j = new int[2];
        this.p = new int[2];
        this.w = -1;
        this.B = -1;
        this.R = new a();
        this.S = Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.T = new f();
        this.U = new g();
        this.e = ViewConfiguration.get(context).getScaledTouchSlop();
        this.r = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.z = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.O = (int) (displayMetrics.density * 40.0f);
        b();
        setChildrenDrawingOrderEnabled(true);
        int i2 = (int) (displayMetrics.density * 64.0f);
        this.F = i2;
        this.f = (float) i2;
        this.h = new y9(this);
        this.i = new u9(this);
        setNestedScrollingEnabled(true);
        int i3 = -this.O;
        this.s = i3;
        this.E = i3;
        c(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, W);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public void c(float f2) {
        int i2 = this.C;
        setTargetOffsetTopAndBottom((i2 + ((int) (((float) (this.E - i2)) * f2))) - this.A.getTop());
    }

    @DexIgnore
    public void a(boolean z2, int i2) {
        this.F = i2;
        this.x = z2;
        this.A.invalidate();
    }

    @DexIgnore
    public final void b(Animation.AnimationListener animationListener) {
        this.A.setVisibility(0);
        this.H.setAlpha(255);
        b bVar = new b();
        this.I = bVar;
        bVar.setDuration((long) this.r);
        if (animationListener != null) {
            this.A.a(animationListener);
        }
        this.A.clearAnimation();
        this.A.startAnimation(this.I);
    }

    @DexIgnore
    public final void c(int i2, Animation.AnimationListener animationListener) {
        this.C = i2;
        this.D = this.A.getScaleX();
        h hVar = new h();
        this.M = hVar;
        hVar.setDuration(150);
        if (animationListener != null) {
            this.A.a(animationListener);
        }
        this.A.clearAnimation();
        this.A.startAnimation(this.M);
    }

    @DexIgnore
    public final void d(float f2) {
        float f3 = this.u;
        int i2 = this.e;
        if (f2 - f3 > ((float) i2) && !this.v) {
            this.t = f3 + ((float) i2);
            this.v = true;
            this.H.setAlpha(76);
        }
    }

    @DexIgnore
    public final void a(boolean z2, boolean z3) {
        if (this.d != z2) {
            this.N = z3;
            c();
            this.d = z2;
            if (z2) {
                a(this.s, this.R);
            } else {
                a(this.R);
            }
        }
    }

    @DexIgnore
    public final void b(float f2) {
        this.H.a(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.f));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - this.f;
        int i2 = this.G;
        if (i2 <= 0) {
            i2 = this.P ? this.F - this.E : this.F;
        }
        float f3 = (float) i2;
        double max2 = (double) (Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Math.min(abs, f3 * 2.0f) / f3) / 4.0f);
        float pow = ((float) (max2 - Math.pow(max2, 2.0d))) * 2.0f;
        int i3 = this.E + ((int) ((f3 * min) + (f3 * pow * 2.0f)));
        if (this.A.getVisibility() != 0) {
            this.A.setVisibility(0);
        }
        if (!this.x) {
            this.A.setScaleX(1.0f);
            this.A.setScaleY(1.0f);
        }
        if (this.x) {
            setAnimationProgress(Math.min(1.0f, f2 / this.f));
        }
        if (f2 < this.f) {
            if (this.H.getAlpha() > 76 && !a(this.K)) {
                f();
            }
        } else if (this.H.getAlpha() < 255 && !a(this.L)) {
            e();
        }
        this.H.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Math.min(0.8f, max * 0.8f));
        this.H.a(Math.min(1.0f, max));
        this.H.b((((max * 0.4f) - 16.0f) + (pow * 2.0f)) * 0.5f);
        setTargetOffsetTopAndBottom(i3 - this.s);
    }

    @DexIgnore
    public void a(Animation.AnimationListener animationListener) {
        c cVar = new c();
        this.J = cVar;
        cVar.setDuration(150);
        this.A.a(animationListener);
        this.A.clearAnimation();
        this.A.startAnimation(this.J);
    }

    @DexIgnore
    public final Animation a(int i2, int i3) {
        d dVar = new d(i2, i3);
        dVar.setDuration(300);
        this.A.a(null);
        this.A.clearAnimation();
        this.A.startAnimation(dVar);
        return dVar;
    }

    @DexIgnore
    public boolean a() {
        i iVar = this.Q;
        if (iVar != null) {
            return iVar.a(this, this.b);
        }
        View view = this.b;
        if (view instanceof ListView) {
            return ya.a((ListView) view, -1);
        }
        return view.canScrollVertically(-1);
    }

    @DexIgnore
    public final boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    @DexIgnore
    public final void a(float f2) {
        if (f2 > this.f) {
            a(true, true);
            return;
        }
        this.d = false;
        this.H.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        e eVar = null;
        if (!this.x) {
            eVar = new e();
        }
        b(this.s, eVar);
        this.H.a(false);
    }

    @DexIgnore
    public final void a(int i2, Animation.AnimationListener animationListener) {
        this.C = i2;
        this.T.reset();
        this.T.setDuration(200);
        this.T.setInterpolator(this.z);
        if (animationListener != null) {
            this.A.a(animationListener);
        }
        this.A.clearAnimation();
        this.A.startAnimation(this.T);
    }

    @DexIgnore
    public final void b(int i2, Animation.AnimationListener animationListener) {
        if (this.x) {
            c(i2, animationListener);
            return;
        }
        this.C = i2;
        this.U.reset();
        this.U.setDuration(200);
        this.U.setInterpolator(this.z);
        if (animationListener != null) {
            this.A.a(animationListener);
        }
        this.A.clearAnimation();
        this.A.startAnimation(this.U);
    }

    @DexIgnore
    public final void a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.w) {
            this.w = motionEvent.getPointerId(actionIndex == 0 ? 1 : 0);
        }
    }

    @DexIgnore
    public final void a(MotionEvent motionEvent, float f2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.e(str, "handleInterceptTouchEventWithParent event: " + motionEvent.getAction() + " overScroll: " + f2);
        int action = motionEvent.getAction();
        if (action == 0) {
            this.S = Float.valueOf(motionEvent.getX());
            getParent().requestDisallowInterceptTouchEvent(true);
        } else if (action == 2) {
            float abs = Math.abs(motionEvent.getX() - this.S.floatValue());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.e(str2, "dx: " + abs + " - touchSlope: " + this.e);
            if (abs > ((float) this.e)) {
                if (f2 != -1.0f) {
                    a(f2);
                }
                getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
    }
}
