package com.portfolio.platform.uirenew.customview.imagecropper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.jr5;
import com.fossil.lr5;
import com.fossil.mr5;
import com.fossil.nr5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CropOverlayView extends View {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public float C;
    @DexIgnore
    public CropImageView.d D;
    @DexIgnore
    public CropImageView.c E;
    @DexIgnore
    public /* final */ Rect F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public Integer H;
    @DexIgnore
    public ScaleGestureDetector a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ mr5 c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public Paint f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Path j;
    @DexIgnore
    public /* final */ float[] p;
    @DexIgnore
    public /* final */ RectF q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public nr5 y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @TargetApi(11)
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            RectF f = CropOverlayView.this.c.f();
            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();
            float currentSpanY = scaleGestureDetector.getCurrentSpanY() / 2.0f;
            float currentSpanX = scaleGestureDetector.getCurrentSpanX() / 2.0f;
            float f2 = focusY - currentSpanY;
            float f3 = focusX - currentSpanX;
            float f4 = focusX + currentSpanX;
            float f5 = focusY + currentSpanY;
            if (f3 >= f4 || f2 > f5 || f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f4 > CropOverlayView.this.c.c() || f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f5 > CropOverlayView.this.c.b()) {
                return true;
            }
            f.set(f3, f2, f4, f5);
            CropOverlayView.this.c.a(f);
            CropOverlayView.this.invalidate();
            return true;
        }
    }

    @DexIgnore
    public CropOverlayView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public boolean b(boolean z2) {
        if (this.b == z2) {
            return false;
        }
        this.b = z2;
        if (!z2 || this.a != null) {
            return true;
        }
        this.a = new ScaleGestureDetector(getContext(), new c());
        return true;
    }

    @DexIgnore
    public boolean c() {
        return this.z;
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        if (this.h != null) {
            Paint paint = this.f;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            RectF f2 = this.c.f();
            f2.inset(strokeWidth, strokeWidth);
            float width = f2.width() / 3.0f;
            float height = f2.height() / 3.0f;
            if (this.E == CropImageView.c.OVAL) {
                float width2 = (f2.width() / 2.0f) - strokeWidth;
                float height2 = (f2.height() / 2.0f) - strokeWidth;
                float f3 = f2.left + width;
                float f4 = f2.right - width;
                float sin = (float) (((double) height2) * Math.sin(Math.acos((double) ((width2 - width) / width2))));
                canvas.drawLine(f3, (f2.top + height2) - sin, f3, (f2.bottom - height2) + sin, this.h);
                canvas.drawLine(f4, (f2.top + height2) - sin, f4, (f2.bottom - height2) + sin, this.h);
                float f5 = f2.top + height;
                float f6 = f2.bottom - height;
                float cos = (float) (((double) width2) * Math.cos(Math.asin((double) ((height2 - height) / height2))));
                canvas.drawLine((f2.left + width2) - cos, f5, (f2.right - width2) + cos, f5, this.h);
                canvas.drawLine((f2.left + width2) - cos, f6, (f2.right - width2) + cos, f6, this.h);
                return;
            }
            float f7 = f2.left + width;
            float f8 = f2.right - width;
            canvas.drawLine(f7, f2.top, f7, f2.bottom, this.h);
            canvas.drawLine(f8, f2.top, f8, f2.bottom, this.h);
            float f9 = f2.top + height;
            float f10 = f2.bottom - height;
            canvas.drawLine(f2.left, f9, f2.right, f9, this.h);
            canvas.drawLine(f2.left, f10, f2.right, f10, this.h);
        }
    }

    @DexIgnore
    public final void e() {
        if (this.y != null) {
            this.y = null;
            a(false);
            invalidate();
        }
    }

    @DexIgnore
    public void f() {
        if (this.G) {
            setCropWindowRect(jr5.b);
            b();
            invalidate();
        }
    }

    @DexIgnore
    public int getAspectRatioX() {
        return this.A;
    }

    @DexIgnore
    public int getAspectRatioY() {
        return this.B;
    }

    @DexIgnore
    public CropImageView.c getCropShape() {
        return this.E;
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        return this.c.f();
    }

    @DexIgnore
    public CropImageView.d getGuidelines() {
        return this.D;
    }

    @DexIgnore
    public Rect getInitialCropWindowRect() {
        return this.F;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
        if (this.c.i()) {
            CropImageView.d dVar = this.D;
            if (dVar == CropImageView.d.ON) {
                d(canvas);
            } else if (dVar == CropImageView.d.ON_TOUCH && this.y != null) {
                d(canvas);
            }
        }
        b(canvas);
        c(canvas);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        if (this.b) {
            this.a.onTouchEvent(motionEvent);
        }
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    b(motionEvent.getX(), motionEvent.getY());
                    getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                } else if (action != 3) {
                    return false;
                }
            }
            getParent().requestDisallowInterceptTouchEvent(false);
            e();
            return true;
        }
        a(motionEvent.getX(), motionEvent.getY());
        return true;
    }

    @DexIgnore
    public void setAspectRatioX(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.A != i2) {
            this.A = i2;
            this.C = ((float) i2) / ((float) this.B);
            if (this.G) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setAspectRatioY(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.B != i2) {
            this.B = i2;
            this.C = ((float) this.A) / ((float) i2);
            if (this.G) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setCropShape(CropImageView.c cVar) {
        if (this.E != cVar) {
            this.E = cVar;
            if (Build.VERSION.SDK_INT <= 17) {
                if (cVar == CropImageView.c.OVAL) {
                    Integer valueOf = Integer.valueOf(getLayerType());
                    this.H = valueOf;
                    if (valueOf.intValue() != 1) {
                        setLayerType(1, null);
                    } else {
                        this.H = null;
                    }
                } else {
                    Integer num = this.H;
                    if (num != null) {
                        setLayerType(num.intValue(), null);
                        this.H = null;
                    }
                }
            }
            invalidate();
        }
    }

    @DexIgnore
    public void setCropWindowChangeListener(b bVar) {
        this.d = bVar;
    }

    @DexIgnore
    public void setCropWindowRect(RectF rectF) {
        this.c.a(rectF);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            if (this.G) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setGuidelines(CropImageView.d dVar) {
        if (this.D != dVar) {
            this.D = dVar;
            if (this.G) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setInitialAttributeValues(lr5 lr5) {
        this.c.a(lr5);
        setCropShape(lr5.a);
        this.x = lr5.b;
        setGuidelines(lr5.d);
        setFixedAspectRatio(lr5.q);
        setAspectRatioX(lr5.r);
        setAspectRatioY(lr5.s);
        b(lr5.i);
        this.w = lr5.c;
        this.v = lr5.p;
        this.f = a(lr5.t, lr5.u);
        this.t = lr5.w;
        this.u = lr5.x;
        this.g = a(lr5.v, lr5.y);
        this.h = a(lr5.z, lr5.A);
        this.i = a(lr5.B);
    }

    @DexIgnore
    public void setInitialCropWindowRect(Rect rect) {
        Rect rect2 = this.F;
        if (rect == null) {
            rect = jr5.a;
        }
        rect2.set(rect);
        if (this.G) {
            b();
            invalidate();
            a(false);
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        this.x = f2;
    }

    @DexIgnore
    public CropOverlayView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = new mr5();
        this.e = new RectF();
        this.j = new Path();
        this.p = new float[8];
        this.q = new RectF();
        this.C = ((float) this.A) / ((float) this.B);
        this.F = new Rect();
    }

    @DexIgnore
    public void a() {
        RectF cropWindowRect = getCropWindowRect();
        b(cropWindowRect);
        this.c.a(cropWindowRect);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        if (this.g != null) {
            Paint paint = this.f;
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float strokeWidth2 = this.g.getStrokeWidth();
            float f3 = strokeWidth2 / 2.0f;
            if (this.E == CropImageView.c.RECTANGLE) {
                f2 = this.t;
            }
            float f4 = f2 + f3;
            RectF f5 = this.c.f();
            f5.inset(f4, f4);
            float f6 = (strokeWidth2 - strokeWidth) / 2.0f;
            float f7 = f3 + f6;
            float f8 = f5.left;
            float f9 = f5.top;
            canvas.drawLine(f8 - f6, f9 - f7, f8 - f6, f9 + this.u, this.g);
            float f10 = f5.left;
            float f11 = f5.top;
            canvas.drawLine(f10 - f7, f11 - f6, f10 + this.u, f11 - f6, this.g);
            float f12 = f5.right;
            float f13 = f5.top;
            canvas.drawLine(f12 + f6, f13 - f7, f12 + f6, f13 + this.u, this.g);
            float f14 = f5.right;
            float f15 = f5.top;
            canvas.drawLine(f14 + f7, f15 - f6, f14 - this.u, f15 - f6, this.g);
            float f16 = f5.left;
            float f17 = f5.bottom;
            canvas.drawLine(f16 - f6, f17 + f7, f16 - f6, f17 - this.u, this.g);
            float f18 = f5.left;
            float f19 = f5.bottom;
            canvas.drawLine(f18 - f7, f19 + f6, f18 + this.u, f19 + f6, this.g);
            float f20 = f5.right;
            float f21 = f5.bottom;
            canvas.drawLine(f20 + f6, f21 + f7, f20 + f6, f21 - this.u, this.g);
            float f22 = f5.right;
            float f23 = f5.bottom;
            canvas.drawLine(f22 + f7, f23 + f6, f22 - this.u, f23 + f6, this.g);
        }
    }

    @DexIgnore
    public void a(float[] fArr, int i2, int i3) {
        if (fArr == null || !Arrays.equals(this.p, fArr)) {
            if (fArr == null) {
                Arrays.fill(this.p, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                System.arraycopy(fArr, 0, this.p, 0, fArr.length);
            }
            this.r = i2;
            this.s = i3;
            RectF f2 = this.c.f();
            if (f2.width() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2.height() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                b();
            }
        }
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.c.b(i2, i3);
    }

    @DexIgnore
    public final void b() {
        float max = Math.max(jr5.e(this.p), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float max2 = Math.max(jr5.g(this.p), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float min = Math.min(jr5.f(this.p), (float) getWidth());
        float min2 = Math.min(jr5.a(this.p), (float) getHeight());
        if (min > max && min2 > max2) {
            RectF rectF = new RectF();
            this.G = true;
            float f2 = this.v;
            float f3 = min - max;
            float f4 = f2 * f3;
            float f5 = min2 - max2;
            float f6 = f2 * f5;
            if (this.F.width() > 0 && this.F.height() > 0) {
                rectF.left = (((float) this.F.left) / this.c.h()) + max;
                rectF.top = (((float) this.F.top) / this.c.g()) + max2;
                rectF.right = rectF.left + (((float) this.F.width()) / this.c.h());
                rectF.bottom = rectF.top + (((float) this.F.height()) / this.c.g());
                rectF.left = Math.max(max, rectF.left);
                rectF.top = Math.max(max2, rectF.top);
                rectF.right = Math.min(min, rectF.right);
                rectF.bottom = Math.min(min2, rectF.bottom);
            } else if (!this.z || min <= max || min2 <= max2) {
                rectF.left = max + f4;
                rectF.top = max2 + f6;
                rectF.right = min - f4;
                rectF.bottom = min2 - f6;
            } else if (f3 / f5 > this.C) {
                rectF.top = max2 + f6;
                rectF.bottom = min2 - f6;
                float width = ((float) getWidth()) / 2.0f;
                this.C = ((float) this.A) / ((float) this.B);
                float max3 = Math.max(this.c.e(), rectF.height() * this.C) / 2.0f;
                rectF.left = width - max3;
                rectF.right = width + max3;
            } else {
                rectF.left = max + f4;
                rectF.right = min - f4;
                float height = ((float) getHeight()) / 2.0f;
                float max4 = Math.max(this.c.d(), rectF.width() / this.C) / 2.0f;
                rectF.top = height - max4;
                rectF.bottom = height + max4;
            }
            b(rectF);
            this.c.a(rectF);
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.c.a(i2, i3);
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5) {
        this.c.a(f2, f3, f4, f5);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        RectF f2 = this.c.f();
        float max = Math.max(jr5.e(this.p), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float max2 = Math.max(jr5.g(this.p), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float min = Math.min(jr5.f(this.p), (float) getWidth());
        float min2 = Math.min(jr5.a(this.p), (float) getHeight());
        if (this.E != CropImageView.c.RECTANGLE) {
            this.j.reset();
            if (Build.VERSION.SDK_INT > 17 || this.E != CropImageView.c.OVAL) {
                this.e.set(f2.left, f2.top, f2.right, f2.bottom);
            } else {
                this.e.set(f2.left + 2.0f, f2.top + 2.0f, f2.right - 2.0f, f2.bottom - 2.0f);
            }
            this.j.addOval(this.e, Path.Direction.CW);
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.j);
            } else {
                canvas.clipPath(this.j, Region.Op.XOR);
            }
            canvas.drawRect(max, max2, min, min2, this.i);
            canvas.restore();
        } else if (!d() || Build.VERSION.SDK_INT <= 17) {
            canvas.drawRect(max, max2, min, f2.top, this.i);
            canvas.drawRect(max, f2.bottom, min, min2, this.i);
            canvas.drawRect(max, f2.top, f2.left, f2.bottom, this.i);
            canvas.drawRect(f2.right, f2.top, min, f2.bottom, this.i);
        } else {
            this.j.reset();
            Path path = this.j;
            float[] fArr = this.p;
            path.moveTo(fArr[0], fArr[1]);
            Path path2 = this.j;
            float[] fArr2 = this.p;
            path2.lineTo(fArr2[2], fArr2[3]);
            Path path3 = this.j;
            float[] fArr3 = this.p;
            path3.lineTo(fArr3[4], fArr3[5]);
            Path path4 = this.j;
            float[] fArr4 = this.p;
            path4.lineTo(fArr4[6], fArr4[7]);
            this.j.close();
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.j);
            } else {
                canvas.clipPath(this.j, Region.Op.INTERSECT);
            }
            canvas.clipRect(f2, Region.Op.XOR);
            canvas.drawRect(max, max2, min, min2, this.i);
            canvas.restore();
        }
    }

    @DexIgnore
    public final boolean d() {
        float[] fArr = this.p;
        return (fArr[0] == fArr[6] || fArr[1] == fArr[7]) ? false : true;
    }

    @DexIgnore
    public final void b(RectF rectF) {
        if (rectF.width() < this.c.e()) {
            float e2 = (this.c.e() - rectF.width()) / 2.0f;
            rectF.left -= e2;
            rectF.right += e2;
        }
        if (rectF.height() < this.c.d()) {
            float d2 = (this.c.d() - rectF.height()) / 2.0f;
            rectF.top -= d2;
            rectF.bottom += d2;
        }
        if (rectF.width() > this.c.c()) {
            float width = (rectF.width() - this.c.c()) / 2.0f;
            rectF.left += width;
            rectF.right -= width;
        }
        if (rectF.height() > this.c.b()) {
            float height = (rectF.height() - this.c.b()) / 2.0f;
            rectF.top += height;
            rectF.bottom -= height;
        }
        a(rectF);
        if (this.q.width() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.q.height() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float max = Math.max(this.q.left, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float max2 = Math.max(this.q.top, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float min = Math.min(this.q.right, (float) getWidth());
            float min2 = Math.min(this.q.bottom, (float) getHeight());
            if (rectF.left < max) {
                rectF.left = max;
            }
            if (rectF.top < max2) {
                rectF.top = max2;
            }
            if (rectF.right > min) {
                rectF.right = min;
            }
            if (rectF.bottom > min2) {
                rectF.bottom = min2;
            }
        }
        if (this.z && ((double) Math.abs(rectF.width() - (rectF.height() * this.C))) > 0.1d) {
            if (rectF.width() > rectF.height() * this.C) {
                float abs = Math.abs((rectF.height() * this.C) - rectF.width()) / 2.0f;
                rectF.left += abs;
                rectF.right -= abs;
                return;
            }
            float abs2 = Math.abs((rectF.width() / this.C) - rectF.height()) / 2.0f;
            rectF.top += abs2;
            rectF.bottom -= abs2;
        }
    }

    @DexIgnore
    public static Paint a(int i2) {
        Paint paint = new Paint();
        paint.setColor(i2);
        return paint;
    }

    @DexIgnore
    public static Paint a(float f2, int i2) {
        if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return null;
        }
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        return paint;
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        nr5 a2 = this.c.a(f2, f3, this.w, this.E);
        this.y = a2;
        if (a2 != null) {
            invalidate();
        }
    }

    @DexIgnore
    public final boolean a(RectF rectF) {
        float e2 = jr5.e(this.p);
        float g2 = jr5.g(this.p);
        float f2 = jr5.f(this.p);
        float a2 = jr5.a(this.p);
        if (!d()) {
            this.q.set(e2, g2, f2, a2);
            return false;
        }
        float[] fArr = this.p;
        float f3 = fArr[0];
        float f4 = fArr[1];
        float f5 = fArr[4];
        float f6 = fArr[5];
        float f7 = fArr[6];
        float f8 = fArr[7];
        if (fArr[7] < fArr[1]) {
            if (fArr[1] < fArr[3]) {
                f3 = fArr[6];
                f4 = fArr[7];
                f5 = fArr[2];
                f6 = fArr[3];
                f7 = fArr[4];
                f8 = fArr[5];
            } else {
                f3 = fArr[4];
                f4 = fArr[5];
                f5 = fArr[0];
                f6 = fArr[1];
                f7 = fArr[2];
                f8 = fArr[3];
            }
        } else if (fArr[1] > fArr[3]) {
            f3 = fArr[2];
            f4 = fArr[3];
            f5 = fArr[6];
            f6 = fArr[7];
            f7 = fArr[0];
            f8 = fArr[1];
        }
        float f9 = (f8 - f4) / (f7 - f3);
        float f10 = -1.0f / f9;
        float f11 = f4 - (f9 * f3);
        float f12 = f4 - (f3 * f10);
        float f13 = f6 - (f9 * f5);
        float f14 = f6 - (f5 * f10);
        float centerY = rectF.centerY() - rectF.top;
        float centerX = rectF.centerX();
        float f15 = rectF.left;
        float f16 = centerY / (centerX - f15);
        float f17 = -f16;
        float f18 = rectF.top;
        float f19 = f18 - (f15 * f16);
        float f20 = rectF.right;
        float f21 = f18 - (f17 * f20);
        float f22 = f9 - f16;
        float f23 = (f19 - f11) / f22;
        float max = Math.max(e2, f23 < f20 ? f23 : e2);
        float f24 = (f19 - f12) / (f10 - f16);
        if (f24 >= rectF.right) {
            f24 = max;
        }
        float max2 = Math.max(max, f24);
        float f25 = f10 - f17;
        float f26 = (f21 - f14) / f25;
        if (f26 >= rectF.right) {
            f26 = max2;
        }
        float max3 = Math.max(max2, f26);
        float f27 = (f21 - f12) / f25;
        if (f27 <= rectF.left) {
            f27 = f2;
        }
        float min = Math.min(f2, f27);
        float f28 = (f21 - f13) / (f9 - f17);
        if (f28 <= rectF.left) {
            f28 = min;
        }
        float min2 = Math.min(min, f28);
        float f29 = (f19 - f13) / f22;
        if (f29 <= rectF.left) {
            f29 = min2;
        }
        float min3 = Math.min(min2, f29);
        float max4 = Math.max(g2, Math.max((f9 * max3) + f11, (f10 * min3) + f12));
        float min4 = Math.min(a2, Math.min((f10 * max3) + f14, (f9 * min3) + f13));
        RectF rectF2 = this.q;
        rectF2.left = max3;
        rectF2.top = max4;
        rectF2.right = min3;
        rectF2.bottom = min4;
        return true;
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        Paint paint = this.f;
        if (paint != null) {
            float strokeWidth = paint.getStrokeWidth();
            RectF f2 = this.c.f();
            float f3 = strokeWidth / 2.0f;
            f2.inset(f3, f3);
            if (this.E == CropImageView.c.RECTANGLE) {
                canvas.drawRect(f2, this.f);
            } else {
                canvas.drawOval(f2, this.f);
            }
        }
    }

    @DexIgnore
    public final void b(float f2, float f3) {
        if (this.y != null) {
            float f4 = this.x;
            RectF f5 = this.c.f();
            this.y.a(f5, f2, f3, this.q, this.r, this.s, a(f5) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f4, this.z, this.C);
            this.c.a(f5);
            a(true);
            invalidate();
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        try {
            if (this.d != null) {
                this.d.a(z2);
            }
        } catch (Exception e2) {
            Log.e("AIC", "Exception in crop window changed", e2);
        }
    }
}
