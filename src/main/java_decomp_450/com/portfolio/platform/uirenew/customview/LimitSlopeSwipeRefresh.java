package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LimitSlopeSwipeRefresh extends SwipeRefreshLayout {
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public Float W;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LimitSlopeSwipeRefresh(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        ee7.a((Object) viewConfiguration, "ViewConfiguration.get(context)");
        this.V = viewConfiguration.getScaledTouchSlop();
    }

    @DexIgnore
    @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Integer valueOf = motionEvent != null ? Integer.valueOf(motionEvent.getAction()) : null;
        if (valueOf != null && valueOf.intValue() == 0) {
            this.W = Float.valueOf(motionEvent.getX());
        } else if (valueOf != null && valueOf.intValue() == 2) {
            float x = motionEvent.getX();
            Float f = this.W;
            if (f == null) {
                ee7.a();
                throw null;
            } else if (Math.abs(x - f.floatValue()) > ((float) this.V)) {
                return false;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}
