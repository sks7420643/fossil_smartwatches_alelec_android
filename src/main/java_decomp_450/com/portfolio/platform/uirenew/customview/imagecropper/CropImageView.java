package com.portfolio.platform.uirenew.customview.imagecropper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hr5;
import com.fossil.imagefilters.FilterType;
import com.fossil.ir5;
import com.fossil.jr5;
import com.fossil.kr5;
import com.fossil.lr5;
import com.fossil.pl4;
import com.fossil.ub;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;
import java.lang.ref.WeakReference;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CropImageView extends FrameLayout {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public int B;
    @DexIgnore
    public g C;
    @DexIgnore
    public f D;
    @DexIgnore
    public h E;
    @DexIgnore
    public i F;
    @DexIgnore
    public e G;
    @DexIgnore
    public Uri H;
    @DexIgnore
    public int I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public RectF M;
    @DexIgnore
    public int N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public Uri P;
    @DexIgnore
    public WeakReference<ir5> Q;
    @DexIgnore
    public WeakReference<hr5> R;
    @DexIgnore
    public Bitmap S;
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public /* final */ CropOverlayView b;
    @DexIgnore
    public /* final */ Matrix c;
    @DexIgnore
    public /* final */ Matrix d;
    @DexIgnore
    public /* final */ float[] e;
    @DexIgnore
    public /* final */ float[] f;
    @DexIgnore
    public kr5 g;
    @DexIgnore
    public Bitmap h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public k v;
    @DexIgnore
    public FilterType w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CropOverlayView.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView.b
        public void a(boolean z) {
            CropImageView.this.a(z, true);
            g a2 = CropImageView.this.C;
            if (a2 != null && !z) {
                a2.a(CropImageView.this.getCropRect());
            }
            f b = CropImageView.this.D;
            if (b != null && z) {
                b.a(CropImageView.this.getCropRect());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ Uri b;
        @DexIgnore
        public /* final */ Bitmap c;
        @DexIgnore
        public /* final */ Uri d;
        @DexIgnore
        public /* final */ Rect e;
        @DexIgnore
        public /* final */ Rect f;

        @DexIgnore
        public b(Bitmap bitmap, Uri uri, Bitmap bitmap2, Uri uri2, Exception exc, float[] fArr, Rect rect, Rect rect2, int i, int i2) {
            this.a = bitmap;
            this.b = uri;
            this.c = bitmap2;
            this.d = uri2;
            this.e = rect;
            this.f = rect2;
        }
    }

    @DexIgnore
    public enum c {
        RECTANGLE,
        OVAL
    }

    @DexIgnore
    public enum d {
        OFF,
        ON_TOUCH,
        ON
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(CropImageView cropImageView, b bVar);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void a(CropImageView cropImageView, Uri uri, Exception exc);
    }

    @DexIgnore
    public enum j {
        NONE,
        SAMPLING,
        RESIZE_INSIDE,
        RESIZE_FIT,
        RESIZE_EXACT
    }

    @DexIgnore
    public enum k {
        FIT_CENTER,
        CENTER,
        CENTER_CROP,
        CENTER_INSIDE
    }

    @DexIgnore
    public CropImageView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public void c(int i2, int i3) {
        this.b.b(i2, i3);
    }

    @DexIgnore
    public boolean d() {
        return this.q;
    }

    @DexIgnore
    public boolean e() {
        return this.r;
    }

    @DexIgnore
    public final void f() {
        float[] fArr = this.e;
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = (float) this.h.getWidth();
        float[] fArr2 = this.e;
        fArr2[3] = 0.0f;
        fArr2[4] = (float) this.h.getWidth();
        this.e[5] = (float) this.h.getHeight();
        float[] fArr3 = this.e;
        fArr3[6] = 0.0f;
        fArr3[7] = (float) this.h.getHeight();
        this.c.mapPoints(this.e);
        float[] fArr4 = this.f;
        fArr4[0] = 0.0f;
        fArr4[1] = 0.0f;
        fArr4[2] = 100.0f;
        fArr4[3] = 0.0f;
        fArr4[4] = 100.0f;
        fArr4[5] = 100.0f;
        fArr4[6] = 0.0f;
        fArr4[7] = 100.0f;
        this.c.mapPoints(fArr4);
    }

    @DexIgnore
    public final void g() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView != null) {
            cropOverlayView.setVisibility((!this.y || this.h == null) ? 4 : 0);
        }
    }

    @DexIgnore
    public Pair<Integer, Integer> getAspectRatio() {
        return new Pair<>(Integer.valueOf(this.b.getAspectRatioX()), Integer.valueOf(this.b.getAspectRatioY()));
    }

    @DexIgnore
    public Bitmap getBitmap() {
        return this.h;
    }

    @DexIgnore
    public float[] getCropPoints() {
        RectF cropWindowRect = this.b.getCropWindowRect();
        float f2 = cropWindowRect.left;
        float f3 = cropWindowRect.top;
        float f4 = cropWindowRect.right;
        float f5 = cropWindowRect.bottom;
        float[] fArr = {f2, f3, f4, f3, f4, f5, f2, f5};
        this.c.invert(this.d);
        this.d.mapPoints(fArr);
        for (int i2 = 0; i2 < 8; i2++) {
            fArr[i2] = fArr[i2] * ((float) this.I);
        }
        return fArr;
    }

    @DexIgnore
    public Rect getCropRect() {
        int i2 = this.I;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return jr5.a(getCropPoints(), bitmap.getWidth() * i2, i2 * bitmap.getHeight(), this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY());
    }

    @DexIgnore
    public c getCropShape() {
        return this.b.getCropShape();
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView == null) {
            return null;
        }
        return cropOverlayView.getCropWindowRect();
    }

    @DexIgnore
    public Bitmap getCroppedImage() {
        return a(0, 0, j.NONE);
    }

    @DexIgnore
    public void getCroppedImageAsync() {
        b(0, 0, j.NONE);
    }

    @DexIgnore
    public d getGuidelines() {
        return this.b.getGuidelines();
    }

    @DexIgnore
    public int getImageResource() {
        return this.u;
    }

    @DexIgnore
    public Uri getImageUri() {
        return this.H;
    }

    @DexIgnore
    public Bitmap getInitializeBitmap() {
        return this.i;
    }

    @DexIgnore
    public int getLoadedSampleSize() {
        return this.I;
    }

    @DexIgnore
    public int getMaxZoom() {
        return this.B;
    }

    @DexIgnore
    public int getRotatedDegrees() {
        return this.p;
    }

    @DexIgnore
    public k getScaleType() {
        return this.v;
    }

    @DexIgnore
    public Rect getWholeImageRect() {
        int i2 = this.I;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return new Rect(0, 0, bitmap.getWidth() * i2, bitmap.getHeight() * i2);
    }

    @DexIgnore
    public final void h() {
        if (!this.z) {
            return;
        }
        if (this.h != null || this.Q == null) {
            WeakReference<hr5> weakReference = this.R;
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.s <= 0 || this.t <= 0) {
            a(true);
            return;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = this.s;
        layoutParams.height = this.t;
        setLayoutParams(layoutParams);
        if (this.h != null) {
            float f2 = (float) (i4 - i2);
            float f3 = (float) (i5 - i3);
            a(f2, f3, true, false);
            if (this.M != null) {
                int i6 = this.N;
                if (i6 != this.j) {
                    this.p = i6;
                    a(f2, f3, true, false);
                }
                this.c.mapRect(this.M);
                this.b.setCropWindowRect(this.M);
                a(false, false);
                this.b.a();
                this.M = null;
            } else if (this.O) {
                this.O = false;
                a(false, false);
            }
        } else {
            a(true);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            if (size2 == 0) {
                size2 = bitmap.getHeight();
            }
            double width = size < this.h.getWidth() ? ((double) size) / ((double) this.h.getWidth()) : Double.POSITIVE_INFINITY;
            double height = size2 < this.h.getHeight() ? ((double) size2) / ((double) this.h.getHeight()) : Double.POSITIVE_INFINITY;
            if (width == Double.POSITIVE_INFINITY && height == Double.POSITIVE_INFINITY) {
                i5 = this.h.getWidth();
                i4 = this.h.getHeight();
            } else if (width <= height) {
                i4 = (int) (((double) this.h.getHeight()) * width);
                i5 = size;
            } else {
                i5 = (int) (((double) this.h.getWidth()) * height);
                i4 = size2;
            }
            int a2 = a(mode, size, i5);
            int a3 = a(mode2, size2, i4);
            this.s = a2;
            this.t = a3;
            setMeasuredDimension(a2, a3);
            return;
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            if (this.Q == null && this.H == null && this.h == null && this.u == 0) {
                FilterType filterType = (FilterType) bundle.getSerializable("FILTER_TYPE");
                Uri uri = (Uri) bundle.getParcelable("LOADED_IMAGE_URI");
                if (uri != null) {
                    String string = bundle.getString("LOADED_IMAGE_STATE_BITMAP_KEY");
                    if (string != null) {
                        Pair<String, WeakReference<Bitmap>> pair = jr5.g;
                        Bitmap bitmap = (pair == null || !((String) pair.first).equals(string)) ? null : (Bitmap) ((WeakReference) jr5.g.second).get();
                        jr5.g = null;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            a(bitmap, 0, uri, bundle.getInt("LOADED_SAMPLE_SIZE"), 0);
                        }
                    }
                    if (this.H == null) {
                        a(uri, filterType);
                    }
                } else {
                    int i2 = bundle.getInt("LOADED_IMAGE_RESOURCE");
                    if (i2 > 0) {
                        setImageResource(i2);
                    } else {
                        Uri uri2 = (Uri) bundle.getParcelable("LOADING_IMAGE_URI");
                        if (uri2 != null) {
                            a(uri2, filterType);
                        }
                    }
                }
                int i3 = bundle.getInt("DEGREES_ROTATED");
                this.N = i3;
                this.p = i3;
                Rect rect = (Rect) bundle.getParcelable("INITIAL_CROP_RECT");
                if (rect != null && (rect.width() > 0 || rect.height() > 0)) {
                    this.b.setInitialCropWindowRect(rect);
                }
                RectF rectF = (RectF) bundle.getParcelable("CROP_WINDOW_RECT");
                if (rectF != null && (rectF.width() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || rectF.height() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                    this.M = rectF;
                }
                this.b.setCropShape(c.valueOf(bundle.getString("CROP_SHAPE")));
                this.A = bundle.getBoolean("CROP_AUTO_ZOOM_ENABLED");
                this.B = bundle.getInt("CROP_MAX_ZOOM");
                this.q = bundle.getBoolean("CROP_FLIP_HORIZONTALLY");
                this.r = bundle.getBoolean("CROP_FLIP_VERTICALLY");
            }
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        ir5 ir5;
        if (this.H == null && this.h == null && this.u < 1) {
            return super.onSaveInstanceState();
        }
        Bundle bundle = new Bundle();
        Uri uri = this.H;
        if (this.x && uri == null && this.u < 1) {
            uri = jr5.a(getContext(), this.h, this.P);
            this.P = uri;
        }
        if (!(uri == null || this.h == null)) {
            String uuid = UUID.randomUUID().toString();
            jr5.g = new Pair<>(uuid, new WeakReference(this.h));
            bundle.putString("LOADED_IMAGE_STATE_BITMAP_KEY", uuid);
        }
        WeakReference<ir5> weakReference = this.Q;
        if (!(weakReference == null || (ir5 = weakReference.get()) == null)) {
            bundle.putParcelable("LOADING_IMAGE_URI", ir5.a());
        }
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putParcelable("LOADED_IMAGE_URI", uri);
        bundle.putSerializable("FILTER_TYPE", this.w);
        bundle.putInt("LOADED_IMAGE_RESOURCE", this.u);
        bundle.putInt("LOADED_SAMPLE_SIZE", this.I);
        bundle.putInt("DEGREES_ROTATED", this.p);
        bundle.putParcelable("INITIAL_CROP_RECT", this.b.getInitialCropWindowRect());
        jr5.c.set(this.b.getCropWindowRect());
        this.c.invert(this.d);
        this.d.mapRect(jr5.c);
        bundle.putParcelable("CROP_WINDOW_RECT", jr5.c);
        bundle.putString("CROP_SHAPE", this.b.getCropShape().name());
        bundle.putBoolean("CROP_AUTO_ZOOM_ENABLED", this.A);
        bundle.putInt("CROP_MAX_ZOOM", this.B);
        bundle.putBoolean("CROP_FLIP_HORIZONTALLY", this.q);
        bundle.putBoolean("CROP_FLIP_VERTICALLY", this.r);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.O = i4 > 0 && i5 > 0;
    }

    @DexIgnore
    public void setAutoZoomEnabled(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setCropRect(Rect rect) {
        this.b.setInitialCropWindowRect(rect);
    }

    @DexIgnore
    public void setCropShape(c cVar) {
        this.b.setCropShape(cVar);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        this.b.setFixedAspectRatio(z2);
    }

    @DexIgnore
    public void setFlippedHorizontally(boolean z2) {
        if (this.q != z2) {
            this.q = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setFlippedVertically(boolean z2) {
        if (this.r != z2) {
            this.r = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setGuidelines(d dVar) {
        this.b.setGuidelines(dVar);
    }

    @DexIgnore
    public void setImageBitmap(Bitmap bitmap) {
        this.b.setInitialCropWindowRect(null);
        a(bitmap, 0, null, 1, 0);
    }

    @DexIgnore
    public void setImageResource(int i2) {
        if (i2 != 0) {
            this.b.setInitialCropWindowRect(null);
            a(BitmapFactory.decodeResource(getResources(), i2), i2, null, 1, 0);
        }
    }

    @DexIgnore
    public void setMaxZoom(int i2) {
        if (this.B != i2 && i2 > 0) {
            this.B = i2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setMultiTouchEnabled(boolean z2) {
        if (this.b.b(z2)) {
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setOnCropImageCompleteListener(e eVar) {
        this.G = eVar;
    }

    @DexIgnore
    public void setOnCropWindowChangedListener(h hVar) {
        this.E = hVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayMovedListener(f fVar) {
        this.D = fVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayReleasedListener(g gVar) {
        this.C = gVar;
    }

    @DexIgnore
    public void setOnSetImageUriCompleteListener(i iVar) {
        this.F = iVar;
    }

    @DexIgnore
    public void setRotatedDegrees(int i2) {
        int i3 = this.p;
        if (i3 != i2) {
            a(i2 - i3);
        }
    }

    @DexIgnore
    public void setSaveBitmapToInstanceState(boolean z2) {
        this.x = z2;
    }

    @DexIgnore
    public void setScaleType(k kVar) {
        if (kVar != this.v) {
            this.v = kVar;
            this.J = 1.0f;
            this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.K = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.b.f();
            requestLayout();
        }
    }

    @DexIgnore
    public void setShowCropOverlay(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
            g();
        }
    }

    @DexIgnore
    public void setShowProgressBar(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            h();
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b.setSnapRadius(f2);
        }
    }

    @DexIgnore
    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Bundle bundleExtra;
        this.c = new Matrix();
        this.d = new Matrix();
        this.e = new float[8];
        this.f = new float[8];
        this.w = FilterType.ATKINSON_DITHERING;
        this.x = false;
        this.y = true;
        this.z = true;
        this.A = true;
        this.I = 1;
        this.J = 1.0f;
        lr5 lr5 = null;
        Intent intent = context instanceof Activity ? ((Activity) context).getIntent() : null;
        if (!(intent == null || (bundleExtra = intent.getBundleExtra("CROP_IMAGE_EXTRA_BUNDLE")) == null)) {
            lr5 = (lr5) bundleExtra.getParcelable("CROP_IMAGE_EXTRA_OPTIONS");
        }
        if (lr5 == null) {
            lr5 = new lr5();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CropImageView, 0, 0);
                try {
                    lr5.q = obtainStyledAttributes.getBoolean(10, lr5.q);
                    lr5.r = obtainStyledAttributes.getInteger(0, lr5.r);
                    lr5.s = obtainStyledAttributes.getInteger(1, lr5.s);
                    lr5.e = k.values()[obtainStyledAttributes.getInt(26, lr5.e.ordinal())];
                    lr5.h = obtainStyledAttributes.getBoolean(2, lr5.h);
                    lr5.i = obtainStyledAttributes.getBoolean(24, lr5.i);
                    lr5.j = obtainStyledAttributes.getInteger(19, lr5.j);
                    lr5.a = c.values()[obtainStyledAttributes.getInt(27, lr5.a.ordinal())];
                    lr5.d = d.values()[obtainStyledAttributes.getInt(13, lr5.d.ordinal())];
                    lr5.b = obtainStyledAttributes.getDimension(30, lr5.b);
                    lr5.c = obtainStyledAttributes.getDimension(31, lr5.c);
                    lr5.p = obtainStyledAttributes.getFloat(16, lr5.p);
                    lr5.t = obtainStyledAttributes.getDimension(8, lr5.t);
                    lr5.u = obtainStyledAttributes.getInteger(9, lr5.u);
                    lr5.v = obtainStyledAttributes.getDimension(7, lr5.v);
                    lr5.w = obtainStyledAttributes.getDimension(6, lr5.w);
                    lr5.x = obtainStyledAttributes.getDimension(5, lr5.x);
                    lr5.y = obtainStyledAttributes.getInteger(4, lr5.y);
                    lr5.z = obtainStyledAttributes.getDimension(15, lr5.z);
                    lr5.A = obtainStyledAttributes.getInteger(14, lr5.A);
                    lr5.B = obtainStyledAttributes.getInteger(3, lr5.B);
                    lr5.f = obtainStyledAttributes.getBoolean(28, this.y);
                    lr5.g = obtainStyledAttributes.getBoolean(29, this.z);
                    lr5.v = obtainStyledAttributes.getDimension(7, lr5.v);
                    lr5.C = (int) obtainStyledAttributes.getDimension(23, (float) lr5.C);
                    lr5.D = (int) obtainStyledAttributes.getDimension(22, (float) lr5.D);
                    lr5.E = (int) obtainStyledAttributes.getFloat(21, (float) lr5.E);
                    lr5.F = (int) obtainStyledAttributes.getFloat(20, (float) lr5.F);
                    lr5.G = (int) obtainStyledAttributes.getFloat(18, (float) lr5.G);
                    lr5.H = (int) obtainStyledAttributes.getFloat(17, (float) lr5.H);
                    lr5.X = obtainStyledAttributes.getBoolean(11, lr5.X);
                    lr5.Y = obtainStyledAttributes.getBoolean(11, lr5.Y);
                    this.x = obtainStyledAttributes.getBoolean(25, this.x);
                    if (obtainStyledAttributes.hasValue(0) && obtainStyledAttributes.hasValue(0) && !obtainStyledAttributes.hasValue(10)) {
                        lr5.q = true;
                    }
                } finally {
                    obtainStyledAttributes.recycle();
                }
            }
        }
        lr5.a();
        this.v = lr5.e;
        this.A = lr5.h;
        this.B = lr5.j;
        this.y = lr5.f;
        this.z = lr5.g;
        this.q = lr5.X;
        this.r = lr5.Y;
        View inflate = LayoutInflater.from(context).inflate(2131558459, (ViewGroup) this, true);
        ImageView imageView = (ImageView) inflate.findViewById(2131361803);
        this.a = imageView;
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        CropOverlayView cropOverlayView = (CropOverlayView) inflate.findViewById(2131361798);
        this.b = cropOverlayView;
        cropOverlayView.setCropWindowChangeListener(new a());
        this.b.setInitialAttributeValues(lr5);
        h();
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.b.a(i2, i3);
    }

    @DexIgnore
    public boolean c() {
        return this.b.c();
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.b.setAspectRatioX(i2);
        this.b.setAspectRatioY(i3);
        setFixedAspectRatio(true);
    }

    @DexIgnore
    public void b(int i2, int i3, j jVar) {
        a(i2, i3, jVar, null, null, 0);
    }

    @DexIgnore
    public final void b() {
        if (this.h != null && (this.u > 0 || this.H != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.u = 0;
        this.H = null;
        this.I = 1;
        this.p = 0;
        this.J = 1.0f;
        this.K = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.L = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.c.reset();
        this.P = null;
        this.a.setImageBitmap(null);
        g();
    }

    @DexIgnore
    public Bitmap a(int i2, int i3, j jVar) {
        Bitmap bitmap;
        if (this.h == null) {
            return null;
        }
        this.a.clearAnimation();
        int i4 = 0;
        int i5 = jVar != j.NONE ? i2 : 0;
        if (jVar != j.NONE) {
            i4 = i3;
        }
        if (this.H == null || (this.I <= 1 && jVar != j.SAMPLING)) {
            bitmap = jr5.a(this.h, getCropPoints(), this.p, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), this.q, this.r).a;
        } else {
            bitmap = jr5.a(getContext(), this.H, getCropPoints(), this.p, this.h.getWidth() * this.I, this.h.getHeight() * this.I, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i4, this.q, this.r).a;
        }
        return jr5.a(bitmap, i5, i4, jVar);
    }

    @DexIgnore
    public void a() {
        this.E = null;
        this.D = null;
        this.C = null;
        this.G = null;
        this.F = null;
    }

    @DexIgnore
    public void a(Bitmap bitmap, ub ubVar) {
        int i2;
        Bitmap bitmap2;
        if (bitmap == null || ubVar == null) {
            bitmap2 = bitmap;
            i2 = 0;
        } else {
            jr5.b a2 = jr5.a(bitmap, ubVar);
            Bitmap bitmap3 = a2.a;
            int i3 = a2.b;
            this.j = i3;
            i2 = i3;
            bitmap2 = bitmap3;
        }
        this.b.setInitialCropWindowRect(null);
        a(bitmap2, 0, null, 1, i2);
    }

    @DexIgnore
    public void a(Uri uri, FilterType filterType) {
        if (uri != null) {
            this.w = filterType;
            WeakReference<ir5> weakReference = this.Q;
            ir5 ir5 = weakReference != null ? weakReference.get() : null;
            if (ir5 != null) {
                ir5.cancel(true);
            }
            b();
            this.M = null;
            this.N = 0;
            this.b.setInitialCropWindowRect(null);
            WeakReference<ir5> weakReference2 = new WeakReference<>(new ir5(this, uri, filterType));
            this.Q = weakReference2;
            weakReference2.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            h();
        }
    }

    @DexIgnore
    public void a(int i2) {
        int i3;
        if (this.h != null) {
            if (i2 < 0) {
                i3 = (i2 % 360) + 360;
            } else {
                i3 = i2 % 360;
            }
            boolean z2 = !this.b.c() && ((i3 > 45 && i3 < 135) || (i3 > 215 && i3 < 305));
            jr5.c.set(this.b.getCropWindowRect());
            RectF rectF = jr5.c;
            float height = (z2 ? rectF.height() : rectF.width()) / 2.0f;
            RectF rectF2 = jr5.c;
            float width = (z2 ? rectF2.width() : rectF2.height()) / 2.0f;
            if (z2) {
                boolean z3 = this.q;
                this.q = this.r;
                this.r = z3;
            }
            this.c.invert(this.d);
            jr5.d[0] = jr5.c.centerX();
            jr5.d[1] = jr5.c.centerY();
            float[] fArr = jr5.d;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 1.0f;
            fArr[5] = 0.0f;
            this.d.mapPoints(fArr);
            this.p = (this.p + i3) % 360;
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(jr5.e, jr5.d);
            float[] fArr2 = jr5.e;
            double pow = Math.pow((double) (fArr2[4] - fArr2[2]), 2.0d);
            float[] fArr3 = jr5.e;
            float sqrt = (float) (((double) this.J) / Math.sqrt(pow + Math.pow((double) (fArr3[5] - fArr3[3]), 2.0d)));
            this.J = sqrt;
            this.J = Math.max(sqrt, 1.0f);
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(jr5.e, jr5.d);
            float[] fArr4 = jr5.e;
            double pow2 = Math.pow((double) (fArr4[4] - fArr4[2]), 2.0d);
            float[] fArr5 = jr5.e;
            double sqrt2 = Math.sqrt(pow2 + Math.pow((double) (fArr5[5] - fArr5[3]), 2.0d));
            float f2 = (float) (((double) height) * sqrt2);
            float f3 = (float) (((double) width) * sqrt2);
            RectF rectF3 = jr5.c;
            float[] fArr6 = jr5.e;
            rectF3.set(fArr6[0] - f2, fArr6[1] - f3, fArr6[0] + f2, fArr6[1] + f3);
            this.b.f();
            this.b.setCropWindowRect(jr5.c);
            a((float) getWidth(), (float) getHeight(), true, false);
            a(false, false);
            this.b.a();
        }
    }

    @DexIgnore
    public void a(ir5.a aVar) {
        this.Q = null;
        h();
        if (aVar.e == null) {
            this.j = aVar.d;
            if (this.i == null) {
                Bitmap bitmap = aVar.b;
                this.i = bitmap.copy(bitmap.getConfig(), false);
            }
            a(aVar.f, 0, aVar.a, aVar.c, aVar.d);
        }
        i iVar = this.F;
        if (iVar != null) {
            iVar.a(this, aVar.a, aVar.e);
        }
    }

    @DexIgnore
    public void a(hr5.a aVar) {
        this.R = null;
        h();
        e eVar = this.G;
        if (eVar != null) {
            eVar.a(this, new b(this.h, this.H, aVar.a, aVar.b, aVar.c, getCropPoints(), getCropRect(), getWholeImageRect(), this.p, aVar.d));
        }
    }

    @DexIgnore
    public final void a(Bitmap bitmap, int i2, Uri uri, int i3, int i4) {
        Bitmap bitmap2 = this.h;
        if (bitmap2 == null || !bitmap2.equals(bitmap)) {
            this.a.clearAnimation();
            b();
            this.S = bitmap.copy(bitmap.getConfig(), true);
            this.h = bitmap;
            this.a.setImageBitmap(bitmap);
            this.H = uri;
            this.u = i2;
            this.I = i3;
            this.p = i4;
            a((float) getWidth(), (float) getHeight(), true, false);
            CropOverlayView cropOverlayView = this.b;
            if (cropOverlayView != null) {
                cropOverlayView.f();
                g();
            }
        }
    }

    @DexIgnore
    public void a(int i2, int i3, j jVar, Uri uri, Bitmap.CompressFormat compressFormat, int i4) {
        CropImageView cropImageView;
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            this.a.clearAnimation();
            WeakReference<hr5> weakReference = this.R;
            hr5 hr5 = weakReference != null ? weakReference.get() : null;
            if (hr5 != null) {
                hr5.cancel(true);
            }
            int i5 = jVar != j.NONE ? i2 : 0;
            int i6 = jVar != j.NONE ? i3 : 0;
            int width = bitmap.getWidth() * this.I;
            int height = bitmap.getHeight();
            int i7 = this.I;
            int i8 = height * i7;
            if (this.H == null || (i7 <= 1 && jVar != j.SAMPLING)) {
                cropImageView = this;
                cropImageView.R = new WeakReference<>(new hr5(this, bitmap, getCropPoints(), this.p, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.q, this.r, jVar, uri, compressFormat, i4));
            } else {
                this.R = new WeakReference<>(new hr5(this, this.H, getCropPoints(), this.p, width, i8, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.q, this.r, jVar, uri, compressFormat, i4));
                cropImageView = this;
            }
            cropImageView.R.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            h();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(boolean r12, boolean r13) {
        /*
            r11 = this;
            int r0 = r11.getWidth()
            int r1 = r11.getHeight()
            android.graphics.Bitmap r2 = r11.h
            if (r2 == 0) goto L_0x0102
            if (r0 <= 0) goto L_0x0102
            if (r1 <= 0) goto L_0x0102
            com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView r2 = r11.b
            android.graphics.RectF r2 = r2.getCropWindowRect()
            r3 = 0
            if (r12 == 0) goto L_0x003b
            float r13 = r2.left
            int r13 = (r13 > r3 ? 1 : (r13 == r3 ? 0 : -1))
            if (r13 < 0) goto L_0x0033
            float r13 = r2.top
            int r13 = (r13 > r3 ? 1 : (r13 == r3 ? 0 : -1))
            if (r13 < 0) goto L_0x0033
            float r13 = r2.right
            float r3 = (float) r0
            int r13 = (r13 > r3 ? 1 : (r13 == r3 ? 0 : -1))
            if (r13 > 0) goto L_0x0033
            float r13 = r2.bottom
            float r2 = (float) r1
            int r13 = (r13 > r2 ? 1 : (r13 == r2 ? 0 : -1))
            if (r13 <= 0) goto L_0x00f9
        L_0x0033:
            float r13 = (float) r0
            float r0 = (float) r1
            r1 = 0
            r11.a(r13, r0, r1, r1)
            goto L_0x00f9
        L_0x003b:
            boolean r4 = r11.A
            r5 = 1065353216(0x3f800000, float:1.0)
            if (r4 != 0) goto L_0x0047
            float r4 = r11.J
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L_0x00f9
        L_0x0047:
            float r4 = r11.J
            int r6 = r11.B
            float r6 = (float) r6
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x0089
            float r4 = r2.width()
            float r6 = (float) r0
            r7 = 1056964608(0x3f000000, float:0.5)
            float r8 = r6 * r7
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 >= 0) goto L_0x0089
            float r4 = r2.height()
            float r8 = (float) r1
            float r7 = r7 * r8
            int r4 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r4 >= 0) goto L_0x0089
            int r4 = r11.B
            float r4 = (float) r4
            float r7 = r2.width()
            float r9 = r11.J
            float r7 = r7 / r9
            r9 = 1059313418(0x3f23d70a, float:0.64)
            float r7 = r7 / r9
            float r6 = r6 / r7
            float r7 = r2.height()
            float r10 = r11.J
            float r7 = r7 / r10
            float r7 = r7 / r9
            float r8 = r8 / r7
            float r6 = java.lang.Math.min(r6, r8)
            float r4 = java.lang.Math.min(r4, r6)
            goto L_0x008a
        L_0x0089:
            r4 = 0
        L_0x008a:
            float r6 = r11.J
            int r6 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r6 <= 0) goto L_0x00c7
            float r6 = r2.width()
            float r7 = (float) r0
            r8 = 1059481190(0x3f266666, float:0.65)
            float r9 = r7 * r8
            int r6 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r6 > 0) goto L_0x00a9
            float r6 = r2.height()
            float r9 = (float) r1
            float r9 = r9 * r8
            int r6 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r6 <= 0) goto L_0x00c7
        L_0x00a9:
            float r4 = r2.width()
            float r6 = r11.J
            float r4 = r4 / r6
            r6 = 1057132380(0x3f028f5c, float:0.51)
            float r4 = r4 / r6
            float r7 = r7 / r4
            float r4 = (float) r1
            float r2 = r2.height()
            float r8 = r11.J
            float r2 = r2 / r8
            float r2 = r2 / r6
            float r4 = r4 / r2
            float r2 = java.lang.Math.min(r7, r4)
            float r4 = java.lang.Math.max(r5, r2)
        L_0x00c7:
            boolean r2 = r11.A
            if (r2 != 0) goto L_0x00cc
            goto L_0x00cd
        L_0x00cc:
            r5 = r4
        L_0x00cd:
            int r2 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x00f9
            float r2 = r11.J
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x00f9
            if (r13 == 0) goto L_0x00f1
            com.fossil.kr5 r2 = r11.g
            if (r2 != 0) goto L_0x00e8
            com.fossil.kr5 r2 = new com.fossil.kr5
            android.widget.ImageView r3 = r11.a
            com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView r4 = r11.b
            r2.<init>(r3, r4)
            r11.g = r2
        L_0x00e8:
            com.fossil.kr5 r2 = r11.g
            float[] r3 = r11.e
            android.graphics.Matrix r4 = r11.c
            r2.b(r3, r4)
        L_0x00f1:
            r11.J = r5
            float r0 = (float) r0
            float r1 = (float) r1
            r2 = 1
            r11.a(r0, r1, r2, r13)
        L_0x00f9:
            com.portfolio.platform.uirenew.customview.imagecropper.CropImageView$h r13 = r11.E
            if (r13 == 0) goto L_0x0102
            if (r12 != 0) goto L_0x0102
            r13.a()
        L_0x0102:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.a(boolean, boolean):void");
    }

    @DexIgnore
    public final void a(float f2, float f3, boolean z2, boolean z3) {
        float f4;
        if (this.h != null) {
            float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.c.invert(this.d);
                RectF cropWindowRect = this.b.getCropWindowRect();
                this.d.mapRect(cropWindowRect);
                this.c.reset();
                this.c.postTranslate((f2 - ((float) this.h.getWidth())) / 2.0f, (f3 - ((float) this.h.getHeight())) / 2.0f);
                f();
                int i2 = this.p;
                if (i2 > 0) {
                    this.c.postRotate((float) i2, jr5.b(this.e), jr5.c(this.e));
                    f();
                }
                float min = Math.min(f2 / jr5.h(this.e), f3 / jr5.d(this.e));
                k kVar = this.v;
                if (kVar == k.FIT_CENTER || ((kVar == k.CENTER_INSIDE && min < 1.0f) || (min > 1.0f && this.A))) {
                    this.c.postScale(min, min, jr5.b(this.e), jr5.c(this.e));
                    f();
                }
                float f6 = this.q ? -this.J : this.J;
                float f7 = this.r ? -this.J : this.J;
                this.c.postScale(f6, f7, jr5.b(this.e), jr5.c(this.e));
                f();
                this.c.mapRect(cropWindowRect);
                if (z2) {
                    if (f2 > jr5.h(this.e)) {
                        f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    } else {
                        f4 = Math.max(Math.min((f2 / 2.0f) - cropWindowRect.centerX(), -jr5.e(this.e)), ((float) getWidth()) - jr5.f(this.e)) / f6;
                    }
                    this.K = f4;
                    if (f3 <= jr5.d(this.e)) {
                        f5 = Math.max(Math.min((f3 / 2.0f) - cropWindowRect.centerY(), -jr5.g(this.e)), ((float) getHeight()) - jr5.a(this.e)) / f7;
                    }
                    this.L = f5;
                } else {
                    this.K = Math.min(Math.max(this.K * f6, -cropWindowRect.left), (-cropWindowRect.right) + f2) / f6;
                    this.L = Math.min(Math.max(this.L * f7, -cropWindowRect.top), (-cropWindowRect.bottom) + f3) / f7;
                }
                this.c.postTranslate(this.K * f6, this.L * f7);
                cropWindowRect.offset(this.K * f6, this.L * f7);
                this.b.setCropWindowRect(cropWindowRect);
                f();
                this.b.invalidate();
                if (z3) {
                    this.g.a(this.e, this.c);
                    this.a.startAnimation(this.g);
                } else {
                    this.a.setImageMatrix(this.c);
                }
                a(false);
            }
        }
    }

    @DexIgnore
    public static int a(int i2, int i3, int i4) {
        if (i2 == 1073741824) {
            return i3;
        }
        return i2 == Integer.MIN_VALUE ? Math.min(i4, i3) : i4;
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (this.h != null && !z2) {
            this.b.a((float) getWidth(), (float) getHeight(), (((float) this.I) * 100.0f) / jr5.h(this.f), (((float) this.I) * 100.0f) / jr5.d(this.f));
        }
        this.b.a(z2 ? null : this.e, getWidth(), getHeight());
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.clearAnimation();
        if (this.h != null && (this.u > 0 || this.H != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.h = bitmap;
        this.a.setImageBitmap(bitmap);
    }
}
