package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import com.fossil.br5;
import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.p87;
import com.fossil.ut4;
import com.fossil.vt4;
import com.fossil.we7;
import com.fossil.xe5;
import com.fossil.zd7;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Locale;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerTextView extends FlexibleTextView implements zt4 {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public int A; // = 2131099677;
    @DexIgnore
    public long u;
    @DexIgnore
    public int v;
    @DexIgnore
    public CountDownTimer w;
    @DexIgnore
    public TimerViewObserver x;
    @DexIgnore
    public ut4 y; // = ut4.TIME_LEFT;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ TimerTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(TimerTextView timerTextView, long j, long j2, long j3) {
            super(j2, j3);
            this.a = timerTextView;
        }

        @DexIgnore
        public void onFinish() {
            this.a.h();
        }

        @DexIgnore
        public void onTick(long j) {
            this.a.c(j);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = TimerTextView.class.getSimpleName();
        ee7.a((Object) simpleName, "TimerTextView::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context) {
        super(context);
        ee7.b(context, "context");
    }

    @DexIgnore
    private final void setDayText(int i) {
        we7 we7 = we7.a;
        Locale locale = Locale.US;
        ee7.a((Object) locale, "Locale.US");
        String a2 = ig5.a(getContext(), 2131886202);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Label__StartInDays)");
        String format = String.format(locale, a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
        we7 we72 = we7.a;
        Locale locale2 = Locale.US;
        ee7.a((Object) locale2, "Locale.US");
        String a3 = ig5.a(getContext(), 2131886207);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, a3, Arrays.copyOf(new Object[]{format}, 1));
        ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
        int i2 = br5.c[this.y.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(xe5.b.a(format, format2, this.A));
        }
    }

    @DexIgnore
    private final void setHourText(int i) {
        we7 we7 = we7.a;
        Locale locale = Locale.US;
        ee7.a((Object) locale, "Locale.US");
        String a2 = ig5.a(getContext(), 2131886203);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026enge_Label__StartInHours)");
        String format = String.format(locale, a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
        we7 we72 = we7.a;
        Locale locale2 = Locale.US;
        ee7.a((Object) locale2, "Locale.US");
        String a3 = ig5.a(getContext(), 2131886207);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, a3, Arrays.copyOf(new Object[]{format}, 1));
        ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
        int i2 = br5.d[this.y.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(xe5.b.a(format, format2, this.A));
        }
    }

    @DexIgnore
    private final void setMinuteText(int i) {
        we7 we7 = we7.a;
        Locale locale = Locale.US;
        ee7.a((Object) locale, "Locale.US");
        String a2 = ig5.a(getContext(), 2131886205);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Label__StartInMins)");
        String format = String.format(locale, a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
        we7 we72 = we7.a;
        Locale locale2 = Locale.US;
        ee7.a((Object) locale2, "Locale.US");
        String a3 = ig5.a(getContext(), 2131886207);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, a3, Arrays.copyOf(new Object[]{format}, 1));
        ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
        int i2 = br5.e[this.y.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(xe5.b.a(format, format2, this.A));
        }
    }

    @DexIgnore
    private final void setSecText(int i) {
        we7 we7 = we7.a;
        Locale locale = Locale.US;
        ee7.a((Object) locale, "Locale.US");
        String a2 = ig5.a(getContext(), 2131886206);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Label__StartInSecs)");
        String format = String.format(locale, a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
        we7 we72 = we7.a;
        Locale locale2 = Locale.US;
        ee7.a((Object) locale2, "Locale.US");
        String a3 = ig5.a(getContext(), 2131886207);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026lenge_Label__StartInTime)");
        String format2 = String.format(locale2, a3, Arrays.copyOf(new Object[]{format}, 1));
        ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
        int i2 = br5.f[this.y.ordinal()];
        if (i2 == 1) {
            setText(format2);
        } else if (i2 == 2) {
            setText(xe5.b.a(format, format2, this.A));
        }
    }

    @DexIgnore
    public final void b(long j) {
        long j2 = (long) DateTimeConstants.MILLIS_PER_DAY;
        if (j >= j2) {
            int i = (int) (j / j2);
            if (this.v != i) {
                this.v = i;
                setDayText(i);
                return;
            }
            return;
        }
        long j3 = (long) DateTimeConstants.MILLIS_PER_HOUR;
        if (j >= j3) {
            int i2 = (int) (j / j3);
            if (this.v != i2) {
                this.v = i2;
                setHourText(i2);
                return;
            }
            return;
        }
        long j4 = (long) 60000;
        if (j >= j4) {
            int i3 = (int) (j / j4);
            if (this.v != i3) {
                this.v = i3;
                setMinuteText(i3);
                return;
            }
            return;
        }
        setSecText((int) (j / ((long) 1000)));
    }

    @DexIgnore
    public final void c(long j) {
        int i = br5.a[this.y.ordinal()];
        if (i == 1 || i == 2) {
            b(j);
        } else if (i == 3) {
            setText(xe5.b.h((int) (j / ((long) 1000))));
        }
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().e(B, "clean");
        CountDownTimer countDownTimer = this.w;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        TimerViewObserver timerViewObserver = this.x;
        if (timerViewObserver != null) {
            timerViewObserver.a(g());
        }
    }

    @DexIgnore
    public final int g() {
        return System.identityHashCode(this);
    }

    @DexIgnore
    public final ut4 getDisplayType() {
        return this.y;
    }

    @DexIgnore
    public final String getEndingText() {
        return this.z;
    }

    @DexIgnore
    public final int getHighlightColour() {
        return this.A;
    }

    @DexIgnore
    public final void h() {
        CharSequence charSequence;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = B;
        local.e(str, "parseTextOnFinish - " + this.z + " - " + this.y);
        String str2 = this.z;
        if (str2 != null) {
            setText(str2);
            return;
        }
        int i = br5.b[this.y.ordinal()];
        if (i == 1) {
            charSequence = ig5.a(getContext(), 2131886208);
        } else if (i == 2) {
            String a2 = ig5.a(getContext(), 2131886208);
            xe5 xe5 = xe5.b;
            ee7.a((Object) a2, "starting");
            charSequence = xe5.a(a2, a2, this.A);
        } else if (i == 3) {
            charSequence = xe5.b.h(0);
        } else {
            throw new p87();
        }
        setText(charSequence);
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onPause() {
        FLogger.INSTANCE.getLocal().e(B, "onPause");
        CountDownTimer countDownTimer = this.w;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onResume() {
        FLogger.INSTANCE.getLocal().e(B, "onResume");
        a(this.u - vt4.a.b());
    }

    @DexIgnore
    public final void setDisplayType(ut4 ut4) {
        ee7.b(ut4, "<set-?>");
        this.y = ut4;
    }

    @DexIgnore
    public final void setEndingText(String str) {
        this.z = str;
    }

    @DexIgnore
    public final void setHighlightColour(int i) {
        this.A = i;
    }

    @DexIgnore
    public final void setObserver(TimerViewObserver timerViewObserver) {
        if (this.x == null) {
            this.x = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.a(this, g());
            }
        }
    }

    @DexIgnore
    public final void setTime(long j) {
        this.u = j;
        a(j - vt4.a.b());
    }

    @DexIgnore
    public final void a(TimerViewObserver timerViewObserver, int i) {
        if (this.x == null) {
            this.x = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.a(this, getId() + i);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
    }

    @DexIgnore
    public final void a(long j) {
        CountDownTimer countDownTimer = this.w;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.w = new b(this, j, j, 1000).start();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
    }
}
