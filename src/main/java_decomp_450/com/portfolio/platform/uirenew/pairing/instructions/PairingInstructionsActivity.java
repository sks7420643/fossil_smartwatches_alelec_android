package com.portfolio.platform.uirenew.pairing.instructions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.ar6;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.hr6;
import com.fossil.jr6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingInstructionsActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public jr6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            if ((i & 4) != 0) {
                z2 = false;
            }
            aVar.a(context, z, z2);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z, boolean z2) {
            ee7.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("", "start isOnboarding=" + z + ", isClearTop=" + z2);
            Intent intent = new Intent(context, PairingInstructionsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            if (!z2) {
                intent.setFlags(536870912);
            } else {
                intent.setFlags(67108864);
            }
            context.startActivity(intent);
        }
    }

    /*
    static {
        ee7.a((Object) PairingInstructionsActivity.class.getSimpleName(), "PairingInstructionsActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ar6 ar6 = (ar6) getSupportFragmentManager().b(2131362149);
        Intent intent = getIntent();
        boolean z2 = false;
        if (intent != null) {
            z2 = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (ar6 == null) {
            ar6 = ar6.p.a(z2);
            a(ar6, 2131362149);
        }
        PortfolioApp.g0.c().f().a(new hr6(ar6)).a(this);
    }
}
