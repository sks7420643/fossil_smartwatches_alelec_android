package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.bp6;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.ko5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.fossil.zo6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public bp6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            ee7.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ExploreWatchActivity", "start - isOnboarding=" + z);
            Intent intent = new Intent(context, ExploreWatchActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ko5 ko5 = (ko5) getSupportFragmentManager().b(2131362149);
        boolean z2 = false;
        if (getIntent() != null) {
            z2 = getIntent().getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (ko5 == null) {
            ko5 = ko5.s.a(z2);
            a(ko5, "ExploreWatchFragment", 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (ko5 != null) {
            f.a(new zo6(ko5)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchContract.View");
    }
}
