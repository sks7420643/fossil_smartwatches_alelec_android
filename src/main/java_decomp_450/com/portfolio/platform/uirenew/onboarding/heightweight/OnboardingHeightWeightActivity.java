package com.portfolio.platform.uirenew.onboarding.heightweight;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.mo5;
import com.fossil.rp6;
import com.fossil.tj4;
import com.fossil.tp6;
import com.fossil.x87;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public tp6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            context.startActivity(new Intent(context, OnboardingHeightWeightActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.fossil.cl5
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        mo5 mo5 = (mo5) getSupportFragmentManager().b(2131362149);
        if (mo5 == null) {
            mo5 = mo5.j.b();
            a(mo5, mo5.j.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (mo5 != null) {
            f.a(new rp6(mo5)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightContract.View");
    }
}
