package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.ip6;
import com.fossil.kp6;
import com.fossil.lo5;
import com.fossil.tj4;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForgotPasswordActivity extends cl5 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public kp6 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            ee7.b(context, "context");
            ee7.b(str, Constants.EMAIL);
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        lo5 lo5 = (lo5) getSupportFragmentManager().b(2131362149);
        if (lo5 == null) {
            lo5 = lo5.j.b();
            a(lo5, lo5.j.a(), 2131362149);
        }
        tj4 f = PortfolioApp.g0.c().f();
        if (lo5 != null) {
            f.a(new ip6(lo5)).a(this);
            String str = "";
            if (bundle == null) {
                kp6 kp6 = this.y;
                if (kp6 != null) {
                    if (stringExtra == null) {
                        stringExtra = str;
                    }
                    kp6.c(stringExtra);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                kp6 kp62 = this.y;
                if (kp62 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string != null) {
                        str = string;
                    }
                    kp62.c(str);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            kp6 kp6 = this.y;
            if (kp6 != null) {
                kp6.a("EMAIL", bundle);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        super.onSaveInstanceState(bundle, persistableBundle);
    }
}
