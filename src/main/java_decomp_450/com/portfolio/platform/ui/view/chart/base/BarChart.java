package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.annotation.Keep;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bb7;
import com.fossil.co5;
import com.fossil.do5;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.gb5;
import com.fossil.p87;
import com.fossil.pl4;
import com.fossil.r87;
import com.fossil.t97;
import com.fossil.tw6;
import com.fossil.w97;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BarChart extends BaseChart {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public PointF D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public ArrayList<PointF> H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public float J;
    @DexIgnore
    public int K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public String N;
    @DexIgnore
    public Path O;
    @DexIgnore
    public String P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public ArrayList<String> R;
    @DexIgnore
    public int S;
    @DexIgnore
    public float T;
    @DexIgnore
    public String U;
    @DexIgnore
    public int V;
    @DexIgnore
    public String W;
    @DexIgnore
    public ArrayList<r87<String, PointF>> a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public ArrayList<r87<Integer, PointF>> c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h0;
    @DexIgnore
    public float i0;
    @DexIgnore
    public float j0;
    @DexIgnore
    public int k0;
    @DexIgnore
    public int l0;
    @DexIgnore
    public int m0;
    @DexIgnore
    public boolean n0;
    @DexIgnore
    public String o0;
    @DexIgnore
    public c p0;
    @DexIgnore
    public gb5 q0;
    @DexIgnore
    public Paint r0;
    @DexIgnore
    public Paint s0;
    @DexIgnore
    public Paint t0;
    @DexIgnore
    public Paint u0;
    @DexIgnore
    public e v;
    @DexIgnore
    public Paint v0;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public ArrayList<ArrayList<b>> b;
        @DexIgnore
        public long c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a() {
            this(0, null, 0, false, 15, null);
        }

        @DexIgnore
        public a(int i, ArrayList<ArrayList<b>> arrayList, long j, boolean z) {
            ee7.b(arrayList, "mListOfBarPoints");
            this.a = i;
            this.b = arrayList;
            this.c = j;
            this.d = z;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final ArrayList<ArrayList<b>> c() {
            return this.b;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.c == aVar.c && this.d == aVar.d && a(aVar.b)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a * 31) + this.b.hashCode()) * 31) + com.fossil.c.a(this.c)) * 31) + com.fossil.a.a(this.d);
        }

        @DexIgnore
        public String toString() {
            return "{goal=" + this.a + ", index=" + this.c + ", points=" + this.b + '}';
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(int i, ArrayList arrayList, long j, boolean z, int i2, zd7 zd7) {
            this((i2 & 1) != 0 ? -1 : i, (i2 & 2) != 0 ? w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(0, null, 0, 0, null, 23, null)})}) : arrayList, (i2 & 4) != 0 ? -1 : j, (i2 & 8) == 0 ? z : false);
        }

        @DexIgnore
        public final boolean a(ArrayList<ArrayList<b>> arrayList) {
            if (this.b.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T t : this.b) {
                int i2 = i + 1;
                if (i >= 0) {
                    T t2 = t;
                    if (t2.size() != arrayList.get(i).size()) {
                        return false;
                    }
                    int i3 = 0;
                    for (Object obj : t2) {
                        int i4 = i3 + 1;
                        if (i3 < 0) {
                            w97.c();
                            throw null;
                        } else if (!ee7.a((b) obj, arrayList.get(i).get(i3))) {
                            return false;
                        } else {
                            i3 = i4;
                        }
                    }
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public f b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public RectF e;

        @DexIgnore
        public b() {
            this(0, null, 0, 0, null, 31, null);
        }

        @DexIgnore
        public b(int i, f fVar, int i2, int i3, RectF rectF) {
            ee7.b(fVar, "mState");
            ee7.b(rectF, "mBound");
            this.a = i;
            this.b = fVar;
            this.c = i2;
            this.d = i3;
            this.e = rectF;
        }

        @DexIgnore
        public final RectF a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final f c() {
            return this.b;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final int e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.d == bVar.d && this.c == bVar.c && this.a == bVar.a && this.b.getMValue() == bVar.b.getMValue()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a * 31) + this.b.hashCode()) * 31) + this.c) * 31) + this.d) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "{index=" + this.a + ", state=" + this.b + ", mTotal=" + this.c + ", value=" + this.d + ", bound=" + this.e + '}';
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(int i, f fVar, int i2, int i3, RectF rectF, int i4, zd7 zd7) {
            this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? f.DEFAULT : fVar, (i4 & 4) != 0 ? 0 : i2, (i4 & 8) == 0 ? i3 : 0, (i4 & 16) != 0 ? new RectF() : rectF);
        }

        @DexIgnore
        public final void a(RectF rectF) {
            ee7.b(rectF, "<set-?>");
            this.e = rectF;
        }

        @DexIgnore
        public final void b(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void c(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void a(f fVar) {
            ee7.b(fVar, "<set-?>");
            this.b = fVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends do5 {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public ArrayList<a> d;

        @DexIgnore
        public c() {
            this(0, 0, null, 7, null);
        }

        @DexIgnore
        public c(int i, int i2, ArrayList<a> arrayList) {
            ee7.b(arrayList, "mData");
            this.b = i;
            this.c = i2;
            this.d = arrayList;
        }

        @DexIgnore
        public final ArrayList<a> a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (this.b == cVar.b && this.c == cVar.c && a(cVar.d)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.b * 31) + this.c) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ChartModel:{max=" + this.b + ", goal=" + this.c + ", data=" + this.d + '}';
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, int i2, ArrayList arrayList, int i3, zd7 zd7) {
            this((i3 & 1) != 0 ? -1 : i, (i3 & 2) != 0 ? -1 : i2, (i3 & 4) != 0 ? new ArrayList() : arrayList);
        }

        @DexIgnore
        public final void a(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public final boolean a(ArrayList<a> arrayList) {
            if (this.d.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T t : this.d) {
                int i2 = i + 1;
                if (i < 0) {
                    w97.c();
                    throw null;
                } else if (!ee7.a((Object) t, (Object) arrayList.get(i))) {
                    return false;
                } else {
                    i = i2;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public /* synthetic */ d(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public enum f {
        LOWEST(0),
        DEFAULT(1),
        HIGHEST(2);
        
        @DexIgnore
        public static /* final */ a Companion; // = new a(null);
        @DexIgnore
        public int mValue;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public f(int i) {
            this.mValue = i;
        }

        @DexIgnore
        public final int getMValue() {
            return this.mValue;
        }

        @DexIgnore
        public final void setMValue(int i) {
            this.mValue = i;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ f(int i, int i2, zd7 zd7) {
            this((i2 & 1) != 0 ? 0 : i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t2.c(), t.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t2.c(), t.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    /*
    static {
        new d(null);
    }
    */

    @DexIgnore
    public BarChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        g();
        if (isInEditMode()) {
            this.p0 = new c(200, 200, w97.a((Object[]) new a[]{new a(200, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 40, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, null), new a(200, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 80, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, null), new a(150, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 60, new RectF()), new b(-1, f.LOWEST, 0, 100, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, null), new a(150, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 80, new RectF()), new b(-1, f.LOWEST, 0, 30, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, null), new a(150, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 90, new RectF()), new b(-1, f.HIGHEST, 0, 20, new RectF())})}), 0, false, 12, null), new a(180, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 40, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, null), new a(180, w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new b[]{new b(-1, f.DEFAULT, 0, 70, new RectF()), new b(-1, f.LOWEST, 0, 30, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, null)}));
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.b(canvas);
        d();
        e();
        f(canvas);
        e(canvas);
        h(canvas);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void d(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.d(canvas);
        f();
        i(canvas);
        j(canvas);
    }

    @DexIgnore
    public void e(Canvas canvas) {
        int i2;
        ee7.b(canvas, "canvas");
        Iterator<a> it = this.p0.a().iterator();
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = ea7.a((Iterable) arrayList, (Comparator) new i()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    b bVar = (b) it2.next();
                    if (bVar.e() != 0) {
                        Paint paint = this.r0;
                        if (paint != null) {
                            int i3 = co5.a[bVar.c().ordinal()];
                            if (i3 == 1) {
                                i2 = this.y;
                            } else if (i3 == 2) {
                                i2 = this.z;
                            } else if (i3 == 3) {
                                i2 = this.A;
                            } else {
                                throw new p87();
                            }
                            paint.setColor(i2);
                            float f2 = bVar.a().left;
                            float f3 = bVar.a().top;
                            float f4 = bVar.a().right;
                            float f5 = bVar.a().bottom;
                            float f6 = this.g0;
                            Paint paint2 = this.r0;
                            if (paint2 != null) {
                                tw6.a(canvas, f2, f3, f4, f5, f6, f6, true, true, false, false, paint2);
                            } else {
                                ee7.d("mGraphPaint");
                                throw null;
                            }
                        } else {
                            ee7.d("mGraphPaint");
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        ee7.b(canvas, "canvas");
        g(canvas);
        k(canvas);
    }

    @DexIgnore
    public final void g() {
        String b2;
        Typeface c2;
        String b3;
        Paint paint = new Paint(1);
        this.r0 = paint;
        if (paint != null) {
            paint.setAntiAlias(true);
            Paint paint2 = this.r0;
            if (paint2 != null) {
                paint2.setStyle(Paint.Style.FILL);
                Paint paint3 = this.r0;
                if (paint3 != null) {
                    Paint paint4 = new Paint(paint3);
                    this.s0 = paint4;
                    if (paint4 != null) {
                        paint4.setStrokeWidth(this.J);
                        Paint paint5 = this.s0;
                        if (paint5 != null) {
                            paint5.setColor(this.K);
                            Paint paint6 = this.s0;
                            if (paint6 != null) {
                                paint6.setStyle(Paint.Style.STROKE);
                                Paint paint7 = this.s0;
                                if (paint7 != null) {
                                    paint7.setStrokeCap(Paint.Cap.ROUND);
                                    Paint paint8 = this.s0;
                                    if (paint8 != null) {
                                        paint8.setStrokeJoin(Paint.Join.ROUND);
                                        Paint paint9 = this.s0;
                                        if (paint9 != null) {
                                            paint9.setPathEffect(new DashPathEffect(new float[]{this.L, this.M}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                                            Paint paint10 = new Paint(1);
                                            this.t0 = paint10;
                                            if (paint10 != null) {
                                                paint10.setColorFilter(new PorterDuffColorFilter(this.w, PorterDuff.Mode.SRC_IN));
                                                this.u0 = new Paint(1);
                                                if (!TextUtils.isEmpty(this.U) && (b3 = eh5.l.a().b(this.U)) != null) {
                                                    Paint paint11 = this.u0;
                                                    if (paint11 != null) {
                                                        paint11.setColor(Color.parseColor(b3));
                                                    } else {
                                                        ee7.d("mLegendPaint");
                                                        throw null;
                                                    }
                                                }
                                                Paint paint12 = this.u0;
                                                if (paint12 != null) {
                                                    paint12.setAntiAlias(true);
                                                    Paint paint13 = this.u0;
                                                    if (paint13 != null) {
                                                        paint13.setStyle(Paint.Style.FILL);
                                                        Paint paint14 = this.u0;
                                                        if (paint14 != null) {
                                                            paint14.setStrokeWidth(this.Q);
                                                            Paint paint15 = this.u0;
                                                            if (paint15 != null) {
                                                                paint15.setTextSize(this.T);
                                                                if (!TextUtils.isEmpty(this.W) && (c2 = eh5.l.a().c(this.W)) != null) {
                                                                    Paint paint16 = this.u0;
                                                                    if (paint16 != null) {
                                                                        paint16.setTypeface(c2);
                                                                    } else {
                                                                        ee7.d("mLegendPaint");
                                                                        throw null;
                                                                    }
                                                                }
                                                                Paint paint17 = this.u0;
                                                                if (paint17 != null) {
                                                                    this.v0 = new Paint(paint17);
                                                                    if (!TextUtils.isEmpty(this.P) && (b2 = eh5.l.a().b(this.P)) != null) {
                                                                        Paint paint18 = this.v0;
                                                                        if (paint18 != null) {
                                                                            paint18.setColor(Color.parseColor(b2));
                                                                        } else {
                                                                            ee7.d("mLegendLinePaint");
                                                                            throw null;
                                                                        }
                                                                    }
                                                                } else {
                                                                    ee7.d("mLegendPaint");
                                                                    throw null;
                                                                }
                                                            } else {
                                                                ee7.d("mLegendPaint");
                                                                throw null;
                                                            }
                                                        } else {
                                                            ee7.d("mLegendPaint");
                                                            throw null;
                                                        }
                                                    } else {
                                                        ee7.d("mLegendPaint");
                                                        throw null;
                                                    }
                                                } else {
                                                    ee7.d("mLegendPaint");
                                                    throw null;
                                                }
                                            } else {
                                                ee7.d("mGraphIconPaint");
                                                throw null;
                                            }
                                        } else {
                                            ee7.d("mGraphGoalLinePaint");
                                            throw null;
                                        }
                                    } else {
                                        ee7.d("mGraphGoalLinePaint");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("mGraphGoalLinePaint");
                                    throw null;
                                }
                            } else {
                                ee7.d("mGraphGoalLinePaint");
                                throw null;
                            }
                        } else {
                            ee7.d("mGraphGoalLinePaint");
                            throw null;
                        }
                    } else {
                        ee7.d("mGraphGoalLinePaint");
                        throw null;
                    }
                } else {
                    ee7.d("mGraphPaint");
                    throw null;
                }
            } else {
                ee7.d("mGraphPaint");
                throw null;
            }
        } else {
            ee7.d("mGraphPaint");
            throw null;
        }
    }

    @DexIgnore
    public final int getMActiveColor() {
        return this.w;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.o0;
    }

    @DexIgnore
    public final int getMBarAlpha() {
        return this.m0;
    }

    @DexIgnore
    public final float getMBarMargin() {
        return this.i0;
    }

    @DexIgnore
    public final float getMBarMarginEnd() {
        return this.j0;
    }

    @DexIgnore
    public final float getMBarRadius() {
        return this.g0;
    }

    @DexIgnore
    public final float getMBarSpace() {
        return this.h0;
    }

    @DexIgnore
    public final e getMBarTouchListener() {
        return this.v;
    }

    @DexIgnore
    public final float getMBarWidth() {
        return this.f0;
    }

    @DexIgnore
    public final c getMChartModel() {
        return this.p0;
    }

    @DexIgnore
    public final int getMDefaultColor() {
        return this.z;
    }

    @DexIgnore
    public final PointF getMGoalIconPoint() {
        return this.D;
    }

    @DexIgnore
    public final boolean getMGoalIconShow() {
        return this.E;
    }

    @DexIgnore
    public final int getMGoalIconSize() {
        return this.C;
    }

    @DexIgnore
    public final Path getMGoalLinePath() {
        return this.O;
    }

    @DexIgnore
    public final gb5 getMGoalType() {
        return this.q0;
    }

    @DexIgnore
    public final int getMGoalnonBrandLineColor() {
        return this.K;
    }

    @DexIgnore
    public final Paint getMGraphGoalLinePaint() {
        Paint paint = this.s0;
        if (paint != null) {
            return paint;
        }
        ee7.d("mGraphGoalLinePaint");
        throw null;
    }

    @DexIgnore
    public final float getMGraphLegendMargin() {
        return this.b0;
    }

    @DexIgnore
    public final ArrayList<r87<Integer, PointF>> getMGraphLegendPoint() {
        return this.c0;
    }

    @DexIgnore
    public final Paint getMGraphPaint() {
        Paint paint = this.r0;
        if (paint != null) {
            return paint;
        }
        ee7.d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final int getMHighestColor() {
        return this.A;
    }

    @DexIgnore
    public final int getMInActiveColor() {
        return this.x;
    }

    @DexIgnore
    public final boolean getMIsFlexibleSize() {
        return this.n0;
    }

    @DexIgnore
    public final int getMLegendIconRes() {
        return this.S;
    }

    @DexIgnore
    public final Paint getMLegendLinePaint() {
        Paint paint = this.v0;
        if (paint != null) {
            return paint;
        }
        ee7.d("mLegendLinePaint");
        throw null;
    }

    @DexIgnore
    public final Paint getMLegendPaint() {
        Paint paint = this.u0;
        if (paint != null) {
            return paint;
        }
        ee7.d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public final ArrayList<String> getMLegendTexts() {
        return this.R;
    }

    @DexIgnore
    public final int getMLowestColor() {
        return this.y;
    }

    @DexIgnore
    public final int getMMaxValue() {
        return this.l0;
    }

    @DexIgnore
    public final int getMNumberBar() {
        return this.k0;
    }

    @DexIgnore
    public final float getMSafeAreaHeight() {
        return this.d0;
    }

    @DexIgnore
    public final ArrayList<PointF> getMStarIconPoint() {
        return this.H;
    }

    @DexIgnore
    public final boolean getMStarIconShow() {
        return this.I;
    }

    @DexIgnore
    public final int getMStarIconSize() {
        return this.G;
    }

    @DexIgnore
    public final String getMTextColor() {
        return this.U;
    }

    @DexIgnore
    public final int getMTextMargin() {
        return this.V;
    }

    @DexIgnore
    public final ArrayList<r87<String, PointF>> getMTextPoint() {
        return this.a0;
    }

    @DexIgnore
    public final float getMTextSize() {
        return this.T;
    }

    @DexIgnore
    public void h(Canvas canvas) {
        ee7.b(canvas, "canvas");
        Iterator<r87<Integer, PointF>> it = this.c0.iterator();
        while (it.hasNext()) {
            r87<Integer, PointF> next = it.next();
            String valueOf = String.valueOf(next.getFirst().intValue());
            float f2 = next.getSecond().x;
            float f3 = next.getSecond().y;
            Paint paint = this.u0;
            if (paint != null) {
                canvas.drawText(valueOf, f2, f3, paint);
            } else {
                ee7.d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        ee7.b(canvas, "canvas");
        float f2 = this.Q * 0.5f;
        float width = (float) canvas.getWidth();
        float f3 = this.Q * 0.5f;
        Paint paint = this.v0;
        if (paint != null) {
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, width, f3, paint);
        } else {
            ee7.d("mLegendLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public void j(Canvas canvas) {
        ee7.b(canvas, "canvas");
        Iterator<r87<String, PointF>> it = this.a0.iterator();
        while (it.hasNext()) {
            r87<String, PointF> next = it.next();
            String first = next.getFirst();
            float f2 = next.getSecond().x;
            float f3 = next.getSecond().y;
            Paint paint = this.u0;
            if (paint != null) {
                canvas.drawText(first, f2, f3, paint);
            } else {
                ee7.d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public void k(Canvas canvas) {
        Bitmap a2;
        ee7.b(canvas, "canvas");
        if (this.I && (a2 = a(this.F, this.G)) != null) {
            for (T t : this.H) {
                float f2 = ((PointF) t).x;
                float f3 = ((PointF) t).y;
                Paint paint = this.t0;
                if (paint != null) {
                    canvas.drawBitmap(a2, f2, f3, paint);
                } else {
                    ee7.d("mGraphIconPaint");
                    throw null;
                }
            }
            a2.recycle();
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    @Keep
    public final void setBarAlpha(int i2) {
        this.m0 = i2;
        Paint paint = this.r0;
        if (paint != null) {
            paint.setAlpha(i2);
            Paint paint2 = this.s0;
            if (paint2 != null) {
                paint2.setAlpha(this.m0);
                Paint paint3 = this.t0;
                if (paint3 != null) {
                    paint3.setAlpha(this.m0);
                    c();
                    return;
                }
                ee7.d("mGraphIconPaint");
                throw null;
            }
            ee7.d("mGraphGoalLinePaint");
            throw null;
        }
        ee7.d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final void setMActiveColor(int i2) {
        this.w = i2;
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        ee7.b(str, "<set-?>");
        this.o0 = str;
    }

    @DexIgnore
    public final void setMBarAlpha(int i2) {
        this.m0 = i2;
    }

    @DexIgnore
    public final void setMBarMargin(float f2) {
        this.i0 = f2;
    }

    @DexIgnore
    public final void setMBarMarginEnd(float f2) {
        this.j0 = f2;
    }

    @DexIgnore
    public final void setMBarRadius(float f2) {
        this.g0 = f2;
    }

    @DexIgnore
    public final void setMBarSpace(float f2) {
        this.h0 = f2;
    }

    @DexIgnore
    public final void setMBarTouchListener(e eVar) {
        this.v = eVar;
    }

    @DexIgnore
    public final void setMBarWidth(float f2) {
        this.f0 = f2;
    }

    @DexIgnore
    public final void setMChartModel(c cVar) {
        ee7.b(cVar, "<set-?>");
        this.p0 = cVar;
    }

    @DexIgnore
    public final void setMDefaultColor(int i2) {
        this.z = i2;
    }

    @DexIgnore
    public final void setMGoalIconPoint(PointF pointF) {
        ee7.b(pointF, "<set-?>");
        this.D = pointF;
    }

    @DexIgnore
    public final void setMGoalIconShow(boolean z2) {
        this.E = z2;
    }

    @DexIgnore
    public final void setMGoalIconSize(int i2) {
        this.C = i2;
    }

    @DexIgnore
    public final void setMGoalLinePath(Path path) {
        ee7.b(path, "<set-?>");
        this.O = path;
    }

    @DexIgnore
    public final void setMGoalType(gb5 gb5) {
        ee7.b(gb5, "<set-?>");
        this.q0 = gb5;
    }

    @DexIgnore
    public final void setMGoalnonBrandLineColor(int i2) {
        this.K = i2;
    }

    @DexIgnore
    public final void setMGraphGoalLinePaint(Paint paint) {
        ee7.b(paint, "<set-?>");
        this.s0 = paint;
    }

    @DexIgnore
    public final void setMGraphLegendMargin(float f2) {
        this.b0 = f2;
    }

    @DexIgnore
    public final void setMGraphLegendPoint(ArrayList<r87<Integer, PointF>> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.c0 = arrayList;
    }

    @DexIgnore
    public final void setMGraphPaint(Paint paint) {
        ee7.b(paint, "<set-?>");
        this.r0 = paint;
    }

    @DexIgnore
    public final void setMHighestColor(int i2) {
        this.A = i2;
    }

    @DexIgnore
    public final void setMInActiveColor(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public final void setMIsFlexibleSize(boolean z2) {
        this.n0 = z2;
    }

    @DexIgnore
    public final void setMLegendIconRes(int i2) {
        this.S = i2;
    }

    @DexIgnore
    public final void setMLegendLinePaint(Paint paint) {
        ee7.b(paint, "<set-?>");
        this.v0 = paint;
    }

    @DexIgnore
    public final void setMLegendPaint(Paint paint) {
        ee7.b(paint, "<set-?>");
        this.u0 = paint;
    }

    @DexIgnore
    public final void setMLegendTexts(ArrayList<String> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.R = arrayList;
    }

    @DexIgnore
    public final void setMLowestColor(int i2) {
        this.y = i2;
    }

    @DexIgnore
    public final void setMMaxValue(int i2) {
        this.l0 = i2;
    }

    @DexIgnore
    public final void setMNumberBar(int i2) {
        this.k0 = i2;
    }

    @DexIgnore
    public final void setMSafeAreaHeight(float f2) {
        this.d0 = f2;
    }

    @DexIgnore
    public final void setMStarIconPoint(ArrayList<PointF> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.H = arrayList;
    }

    @DexIgnore
    public final void setMStarIconShow(boolean z2) {
        this.I = z2;
    }

    @DexIgnore
    public final void setMStarIconSize(int i2) {
        this.G = i2;
    }

    @DexIgnore
    public final void setMTextColor(String str) {
        ee7.b(str, "<set-?>");
        this.U = str;
    }

    @DexIgnore
    public final void setMTextMargin(int i2) {
        this.V = i2;
    }

    @DexIgnore
    public final void setMTextPoint(ArrayList<r87<String, PointF>> arrayList) {
        ee7.b(arrayList, "<set-?>");
        this.a0 = arrayList;
    }

    @DexIgnore
    public final void setMTextSize(float f2) {
        this.T = f2;
    }

    @DexIgnore
    @Keep
    public final void setMaxValue(int i2) {
        this.l0 = i2;
        c();
    }

    @DexIgnore
    public final void setOnTouchListener(e eVar) {
        ee7.b(eVar, "barTouchListener");
        this.v = eVar;
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public final void f() {
        RectF rectF = new RectF(this.i0, this.d0, ((float) getMGraphWidth()) - this.j0, (float) getMGraphHeight());
        this.a0.clear();
        a(rectF.left, rectF.right);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        String[] strArr;
        this.B = -1;
        this.C = 10;
        this.D = new PointF();
        this.F = -1;
        this.G = 10;
        this.H = new ArrayList<>();
        this.J = 2.0f;
        this.L = 2.0f;
        this.M = 2.0f;
        String str = "";
        this.N = str;
        this.O = new Path();
        this.P = str;
        this.Q = 2.0f;
        this.R = new ArrayList<>();
        this.S = -1;
        this.T = 14.0f;
        this.U = str;
        this.V = 10;
        this.W = str;
        this.a0 = new ArrayList<>();
        this.c0 = new ArrayList<>();
        this.d0 = 50.0f;
        this.f0 = 10.0f;
        this.g0 = 5.0f;
        this.h0 = 30.0f;
        this.i0 = 30.0f;
        this.j0 = 30.0f;
        this.k0 = -1;
        this.m0 = 255;
        this.o0 = str;
        this.p0 = new c(0, 0, null, 7, null);
        this.q0 = gb5.ACTIVE_TIME;
        if (!(attributeSet == null || context == null || (theme = context.getTheme()) == null || (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, pl4.BaseChart, 0, 0)) == null)) {
            try {
                this.w = obtainStyledAttributes.getColor(0, 0);
                this.x = obtainStyledAttributes.getColor(14, 0);
                this.y = obtainStyledAttributes.getColor(20, 0);
                this.z = obtainStyledAttributes.getColor(2, 0);
                this.A = obtainStyledAttributes.getColor(13, 0);
                this.B = obtainStyledAttributes.getResourceId(5, 10);
                this.C = obtainStyledAttributes.getDimensionPixelSize(6, 10);
                this.F = obtainStyledAttributes.getResourceId(27, 10);
                this.G = obtainStyledAttributes.getDimensionPixelSize(28, 10);
                this.J = (float) obtainStyledAttributes.getDimensionPixelSize(10, 2);
                this.K = obtainStyledAttributes.getColor(7, 0);
                this.L = (float) obtainStyledAttributes.getDimensionPixelSize(9, 2);
                this.M = (float) obtainStyledAttributes.getDimensionPixelSize(8, 2);
                String string = obtainStyledAttributes.getString(11);
                if (string == null) {
                    string = str;
                }
                this.N = string;
                this.T = (float) obtainStyledAttributes.getDimensionPixelSize(31, 14);
                String string2 = obtainStyledAttributes.getString(29);
                if (string2 == null) {
                    string2 = str;
                }
                this.U = string2;
                this.V = obtainStyledAttributes.getDimensionPixelSize(30, 10);
                this.S = obtainStyledAttributes.getResourceId(16, -1);
                String string3 = obtainStyledAttributes.getString(17);
                if (string3 == null) {
                    string3 = str;
                }
                this.P = string3;
                this.Q = (float) obtainStyledAttributes.getDimensionPixelSize(18, 2);
                this.b0 = (float) obtainStyledAttributes.getDimensionPixelSize(12, 0);
                obtainStyledAttributes.getDimensionPixelSize(25, -1);
                this.d0 = (float) obtainStyledAttributes.getDimensionPixelSize(24, 50);
                this.e0 = (float) obtainStyledAttributes.getDimensionPixelSize(32, 0);
                this.f0 = (float) obtainStyledAttributes.getDimensionPixelSize(33, 10);
                this.g0 = (float) obtainStyledAttributes.getDimensionPixelSize(23, 5);
                float dimensionPixelSize = (float) obtainStyledAttributes.getDimensionPixelSize(21, 30);
                this.i0 = dimensionPixelSize;
                this.j0 = (float) obtainStyledAttributes.getDimensionPixelSize(22, (int) dimensionPixelSize);
                this.h0 = (float) obtainStyledAttributes.getDimensionPixelSize(26, 30);
                setMLegendHeight(obtainStyledAttributes.getDimensionPixelSize(15, 30));
                this.n0 = obtainStyledAttributes.getBoolean(3, false);
                String string4 = obtainStyledAttributes.getString(4);
                if (string4 == null) {
                    string4 = str;
                }
                this.W = string4;
                String string5 = obtainStyledAttributes.getString(1);
                this.o0 = string5 != null ? string5 : str;
                int resourceId = obtainStyledAttributes.getResourceId(19, -1);
                if (resourceId != -1) {
                    Resources resources = getResources();
                    if (resources == null || (strArr = resources.getStringArray(resourceId)) == null) {
                        strArr = new String[0];
                    }
                    ee7.a((Object) strArr, "(resources?.getStringArr\u2026         ?: emptyArray())");
                    ArrayList<String> arrayList = new ArrayList<>();
                    t97.b((Object[]) strArr, (Collection) arrayList);
                    this.R = arrayList;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a();
    }

    @DexIgnore
    public void d() {
        ArrayList<a> a2 = this.p0.a();
        RectF rectF = new RectF(this.i0, this.d0, ((float) getMGraphWidth()) - this.j0, (float) getMGraphHeight());
        float height = rectF.height();
        float width = rectF.width();
        if (this.n0) {
            float f2 = this.h0;
            int i2 = this.k0;
            this.f0 = (width - (f2 * ((float) (i2 - 1)))) / ((float) i2);
        } else {
            this.h0 = a(width);
        }
        float f3 = this.e0;
        float f4 = this.h0;
        if (f3 > f4 * 0.5f) {
            this.e0 = f4 * 0.5f;
        }
        float f5 = rectF.left;
        float f6 = (float) 10;
        PointF pointF = new PointF(rectF.right + f6, rectF.top);
        PointF pointF2 = new PointF(rectF.right + f6, (rectF.top + rectF.bottom) * 0.5f);
        this.O = new Path();
        this.D = new PointF();
        int i3 = 0;
        this.E = false;
        this.H.clear();
        this.I = false;
        this.c0.clear();
        Iterator<a> it = a2.iterator();
        boolean z2 = true;
        boolean z3 = true;
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(i3);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List<b> a3 = ea7.a((Iterable) arrayList, (Comparator) new g());
            float f8 = this.f0 + f5;
            float f9 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (b bVar : a3) {
                f9 += (float) bVar.e();
                float f10 = (f9 * height) / ((float) this.l0);
                if (f10 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    float f11 = this.g0;
                    if (f10 < f11) {
                        f10 = f11;
                    }
                }
                float mGraphHeight = ((float) getMGraphHeight()) - f10;
                bVar.a(new RectF(f5, mGraphHeight, f8, (float) getMGraphHeight()));
                f7 = mGraphHeight;
            }
            if (f8 > pointF2.x && f7 < pointF2.y) {
                z2 = false;
            }
            if (f8 > pointF.x && f7 < pointF.y) {
                z3 = false;
            }
            f5 = f8 + this.h0;
            i3 = 0;
        }
        if (z2) {
            this.c0.add(new r87<>(Integer.valueOf(this.l0 / 2), pointF2));
        }
        if (z3) {
            this.c0.add(new r87<>(Integer.valueOf(this.l0), pointF));
            return;
        }
        pointF.set(pointF.x, this.d0 - 10.0f);
        this.c0.add(new r87<>(Integer.valueOf(this.l0), pointF));
    }

    @DexIgnore
    public void b(do5 do5) {
        ee7.b(do5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        c cVar = (c) do5;
        this.p0 = cVar;
        this.l0 = cVar.c();
        if (this.k0 == -1) {
            this.k0 = this.p0.a().size();
        }
    }

    @DexIgnore
    public void e() {
        ArrayList<a> a2 = this.p0.a();
        int b2 = this.p0.b();
        RectF rectF = new RectF(this.i0, this.d0, ((float) getMGraphWidth()) - this.j0, (float) getMGraphHeight());
        float height = rectF.height();
        float f2 = rectF.left;
        Iterator<a> it = a2.iterator();
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = false;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List<b> a3 = ea7.a((Iterable) arrayList, (Comparator) new h());
            float f5 = this.f0 + f2;
            float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (b bVar : a3) {
                f6 += (float) bVar.e();
                float f7 = (f6 * height) / ((float) this.l0);
                if (f7 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    float f8 = this.g0;
                    if (f7 < f8) {
                        f7 = f8;
                    }
                }
                float mGraphHeight = ((float) getMGraphHeight()) - f7;
                bVar.a(new RectF(f2, mGraphHeight, f5, (float) getMGraphHeight()));
                f4 = mGraphHeight;
            }
            f3 += f6;
            if (f3 >= ((float) b2) && !z2) {
                float f9 = (f2 + f5) * 0.5f;
                this.O.moveTo(f9, this.d0 - 10.0f);
                this.O.lineTo(f9, f4);
                this.D.set(f9 - (((float) this.C) * 0.5f), (this.d0 * 0.5f) - 20.0f);
                this.E = true;
                z2 = true;
            }
            f2 = this.h0 + f5;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        ee7.b(str, "reachGoal");
        ee7.b(str2, "nonBrandNonReachGoal");
        String b2 = eh5.l.a().b(str);
        String b3 = eh5.l.a().b(str2);
        if (b2 != null) {
            int parseColor = Color.parseColor(b2);
            this.z = parseColor;
            this.K = parseColor;
            this.w = parseColor;
            Paint paint = this.s0;
            if (paint != null) {
                paint.setColor(parseColor);
            } else {
                ee7.d("mGraphGoalLinePaint");
                throw null;
            }
        }
        if (b3 != null) {
            this.x = Color.parseColor(b3);
        }
    }

    @DexIgnore
    public void g(Canvas canvas) {
        ee7.b(canvas, "canvas");
        if (this.E) {
            Path path = this.O;
            Paint paint = this.s0;
            if (paint != null) {
                canvas.drawPath(path, paint);
                Bitmap a2 = a(this.B, this.C);
                if (a2 != null) {
                    PointF pointF = this.D;
                    float f2 = pointF.x;
                    float f3 = pointF.y;
                    Paint paint2 = this.t0;
                    if (paint2 != null) {
                        canvas.drawBitmap(a2, f2, f3, paint2);
                        a2.recycle();
                        return;
                    }
                    ee7.d("mGraphIconPaint");
                    throw null;
                }
                return;
            }
            ee7.d("mGraphGoalLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ Bitmap a(BarChart barChart, int i2, int i3, int i4, Object obj) {
        if (obj == null) {
            if ((i4 & 2) != 0) {
                i3 = -1;
            }
            return barChart.a(i2, i3);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createBitmapByRes");
    }

    @DexIgnore
    public Bitmap a(int i2, int i3) {
        int i4;
        if (i2 == -1) {
            return null;
        }
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i2);
            if (decodeResource == null) {
                return decodeResource;
            }
            if (i3 == -1) {
                i3 = decodeResource.getWidth();
                i4 = decodeResource.getHeight();
            } else {
                i4 = i3;
            }
            return Bitmap.createScaledBitmap(decodeResource, i3, i4, false);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "createBitmapByRes - e=" + e2);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public boolean a(MotionEvent motionEvent) {
        ee7.b(motionEvent, Constants.EVENT);
        PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onGraphOverlayTouchEvent - pointF=" + pointF);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                return false;
            }
            Iterator<a> it = this.p0.a().iterator();
            while (it.hasNext()) {
                a next = it.next();
                Iterator<ArrayList<b>> it2 = next.c().iterator();
                while (it2.hasNext()) {
                    ArrayList<b> next2 = it2.next();
                    if (this.q0 == gb5.TOTAL_SLEEP) {
                        if (BaseChart.u.a(pointF, a(new RectF(next2.get(0).a().left, next2.get(next2.size() - 1).a().top, next2.get(0).a().right, next2.get(0).a().bottom), this.e0))) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String tag2 = getTAG();
                            local2.d(tag2, "onGraphOverlayTouchEvent for total sleep - ACTION_UP = TRUE, index=" + next.b());
                            e eVar = this.v;
                            if (eVar != null) {
                                eVar.a(next.b());
                            }
                        }
                    } else {
                        Iterator<b> it3 = next2.iterator();
                        while (it3.hasNext()) {
                            if (BaseChart.u.a(pointF, a(it3.next().a(), this.e0))) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String tag3 = getTAG();
                                local3.d(tag3, "onGraphOverlayTouchEvent for normal chart item- ACTION_UP = TRUE, index=" + next.b());
                                e eVar2 = this.v;
                                if (eVar2 != null) {
                                    eVar2.a(next.b());
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    public float a(float f2) {
        float f3 = this.f0;
        int i2 = this.k0;
        return (f2 - (f3 * ((float) i2))) / ((float) (i2 - 1));
    }

    @DexIgnore
    public void a(float f2, float f3) {
        Rect rect = new Rect();
        Paint paint = this.u0;
        if (paint != null) {
            paint.getTextBounds("12 am", 0, 5, rect);
            float height = ((float) this.V) + ((float) rect.height());
            this.a0.add(new r87<>("12 am", new PointF(f2, height)));
            this.a0.add(new r87<>("12 am", new PointF(f3 - ((float) rect.width()), height)));
            Paint paint2 = this.u0;
            if (paint2 != null) {
                paint2.getTextBounds("12 pm", 0, 5, rect);
                this.a0.add(new r87<>("12 pm", new PointF(((f2 + f3) - ((float) rect.width())) * 0.5f, height)));
                return;
            }
            ee7.d("mLegendPaint");
            throw null;
        }
        ee7.d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public synchronized void a(do5 do5) {
        ee7.b(do5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "changeModel - model=" + do5);
        if (!ee7.a(this.p0, do5)) {
            b(do5);
            c();
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(BarChart barChart, int i2, int i3, int i4, int i5, String str, gb5 gb5, int i6, Object obj) {
        if (obj == null) {
            if ((i6 & 1) != 0) {
                i2 = -1;
            }
            if ((i6 & 2) != 0) {
                i3 = -1;
            }
            if ((i6 & 4) != 0) {
                i4 = -1;
            }
            if ((i6 & 8) != 0) {
                i5 = -1;
            }
            if ((i6 & 16) != 0) {
                str = null;
            }
            if ((i6 & 32) != 0) {
                gb5 = gb5.ACTIVE_TIME;
            }
            barChart.a(i2, i3, i4, i5, str, gb5);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setDataRes");
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5, String str, gb5 gb5) {
        String str2;
        ee7.b(gb5, "goalType");
        if (i2 == -1) {
            i2 = this.w;
        }
        this.w = i2;
        if (i3 == -1) {
            i3 = this.y;
        }
        this.y = i3;
        if (i4 == -1) {
            i4 = this.z;
        }
        this.z = i4;
        if (i5 == -1) {
            i5 = this.A;
        }
        this.A = i5;
        if (str != null) {
            str2 = str;
        } else {
            str2 = this.N;
        }
        this.N = str2;
        if (gb5 == gb5.ACTIVE_TIME) {
            gb5 = this.q0;
        }
        this.q0 = gb5;
        if (str == null) {
            str = this.N;
        }
        this.N = str;
    }

    @DexIgnore
    public static /* synthetic */ void a(BarChart barChart, ArrayList arrayList, boolean z2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                z2 = false;
            }
            barChart.a(arrayList, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setLegendTexts");
    }

    @DexIgnore
    public final void a(ArrayList<String> arrayList, boolean z2) {
        ee7.b(arrayList, "array");
        this.R.clear();
        this.R.addAll(arrayList);
        if (z2) {
            getMLegend().invalidate();
        }
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        return new RectF(rectF.left - f2, rectF.top - f2, rectF.right + f2, rectF.bottom + f2);
    }
}
