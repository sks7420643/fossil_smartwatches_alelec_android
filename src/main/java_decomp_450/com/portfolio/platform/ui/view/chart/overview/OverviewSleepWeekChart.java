package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.re5;
import com.fossil.we7;
import com.fossil.zd7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepWeekChart extends OverviewWeekChart {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public OverviewSleepWeekChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public float a(float f) {
        return ((f - (getMBarWidth() * ((float) getMNumberBar()))) - ((float) 20)) / ((float) (getMNumberBar() - 1));
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart
    public String a(int i) {
        we7 we7 = we7.a;
        String a2 = ig5.a(getContext(), 2131886601);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026eep7days_Label__NumberHr)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{re5.a(((float) i) / ((float) 60), 1).toString()}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
