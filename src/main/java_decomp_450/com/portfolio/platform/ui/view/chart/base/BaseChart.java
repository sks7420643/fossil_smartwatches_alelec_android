package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseChart extends ViewGroup {
    @DexIgnore
    public static /* final */ b u; // = new b(null);
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public c p;
    @DexIgnore
    public e q;
    @DexIgnore
    public a r;
    @DexIgnore
    public d s;
    @DexIgnore
    public /* final */ String t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseChart baseChart, Context context) {
            super(context);
            ee7.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            ee7.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.a(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.a(i, i2, i3, i4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final boolean a(PointF pointF, RectF rectF) {
            ee7.b(pointF, "point");
            ee7.b(rectF, "rect");
            float f = pointF.x;
            if (f <= rectF.right && f >= rectF.left) {
                float f2 = pointF.y;
                return f2 <= rectF.bottom && f2 >= rectF.top;
            }
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BaseChart baseChart, Context context) {
            super(context);
            ee7.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            ee7.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.b(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.setMGraphWidth(i);
            this.a.setMGraphHeight(i2);
            this.a.c(i, i2, i3, i4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(BaseChart baseChart, Context context) {
            super(context);
            ee7.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            ee7.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.c(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.b(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean onTouchEvent(MotionEvent motionEvent) {
            ee7.b(motionEvent, Constants.EVENT);
            return this.a.a(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(BaseChart baseChart, Context context) {
            super(context);
            ee7.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            ee7.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.d(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.setMLegendWidth(i);
            this.a.d(i, i2, i3, i4);
        }
    }

    @DexIgnore
    public BaseChart(Context context) {
        super(context);
        String simpleName = getClass().getSimpleName();
        ee7.a((Object) simpleName, "this::class.java.simpleName");
        this.t = simpleName;
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(this.t, "initGraph");
        Context context = getContext();
        ee7.a((Object) context, "context");
        c cVar = new c(this, context);
        this.p = cVar;
        if (cVar != null) {
            addView(cVar);
            Context context2 = getContext();
            ee7.a((Object) context2, "context");
            e eVar = new e(this, context2);
            this.q = eVar;
            if (eVar != null) {
                addView(eVar);
                Context context3 = getContext();
                ee7.a((Object) context3, "context");
                a aVar = new a(this, context3);
                this.r = aVar;
                if (aVar != null) {
                    addView(aVar);
                    Context context4 = getContext();
                    ee7.a((Object) context4, "context");
                    d dVar = new d(this, context4);
                    this.s = dVar;
                    if (dVar != null) {
                        addView(dVar);
                    } else {
                        ee7.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    ee7.d("mBackground");
                    throw null;
                }
            } else {
                ee7.d("mLegend");
                throw null;
            }
        } else {
            ee7.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void a(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public final void b() {
        c cVar = this.p;
        if (cVar != null) {
            cVar.invalidate();
            e eVar = this.q;
            if (eVar != null) {
                eVar.invalidate();
                a aVar = this.r;
                if (aVar != null) {
                    aVar.invalidate();
                    d dVar = this.s;
                    if (dVar != null) {
                        dVar.invalidate();
                    } else {
                        ee7.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    ee7.d("mBackground");
                    throw null;
                }
            } else {
                ee7.d("mLegend");
                throw null;
            }
        } else {
            ee7.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public final void c() {
        b();
    }

    @DexIgnore
    public void c(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void c(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public void d(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void d(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public final a getMBackground() {
        a aVar = this.r;
        if (aVar != null) {
            return aVar;
        }
        ee7.d("mBackground");
        throw null;
    }

    @DexIgnore
    public final int getMBottomPadding() {
        return this.f;
    }

    @DexIgnore
    public final c getMGraph() {
        c cVar = this.p;
        if (cVar != null) {
            return cVar;
        }
        ee7.d("mGraph");
        throw null;
    }

    @DexIgnore
    public final int getMGraphHeight() {
        return this.h;
    }

    @DexIgnore
    public final d getMGraphOverlay() {
        d dVar = this.s;
        if (dVar != null) {
            return dVar;
        }
        ee7.d("mGraphOverlay");
        throw null;
    }

    @DexIgnore
    public final int getMGraphWidth() {
        return this.g;
    }

    @DexIgnore
    public final int getMHeight() {
        return this.a;
    }

    @DexIgnore
    public final int getMLeftPadding() {
        return this.c;
    }

    @DexIgnore
    public final e getMLegend() {
        e eVar = this.q;
        if (eVar != null) {
            return eVar;
        }
        ee7.d("mLegend");
        throw null;
    }

    @DexIgnore
    public final int getMLegendHeight() {
        return this.j;
    }

    @DexIgnore
    public final int getMLegendWidth() {
        return this.i;
    }

    @DexIgnore
    public final int getMRightPadding() {
        return this.e;
    }

    @DexIgnore
    public final int getMTopPadding() {
        return this.d;
    }

    @DexIgnore
    public final int getMWidth() {
        return this.b;
    }

    @DexIgnore
    public final String getTAG() {
        return this.t;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.b = i2;
        this.a = i3;
        this.c = getPaddingLeft();
        this.d = getPaddingTop();
        this.e = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        this.f = paddingBottom;
        c cVar = this.p;
        if (cVar != null) {
            cVar.layout(this.c, this.d, i2 - this.e, (i3 - this.j) - paddingBottom);
            e eVar = this.q;
            if (eVar != null) {
                int i6 = this.c;
                int i7 = this.f;
                eVar.layout(i6, (i3 - this.j) - i7, i2 - this.e, i3 - i7);
                a aVar = this.r;
                if (aVar != null) {
                    aVar.layout(this.c, this.d, i2 - this.e, i3 - this.f);
                    d dVar = this.s;
                    if (dVar != null) {
                        dVar.layout(this.c, this.d, i2 - this.e, (i3 - this.j) - this.f);
                    } else {
                        ee7.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    ee7.d("mBackground");
                    throw null;
                }
            } else {
                ee7.d("mLegend");
                throw null;
            }
        } else {
            ee7.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public final void setMBackground(a aVar) {
        ee7.b(aVar, "<set-?>");
        this.r = aVar;
    }

    @DexIgnore
    public final void setMBottomPadding(int i2) {
        this.f = i2;
    }

    @DexIgnore
    public final void setMGraph(c cVar) {
        ee7.b(cVar, "<set-?>");
        this.p = cVar;
    }

    @DexIgnore
    public final void setMGraphHeight(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void setMGraphOverlay(d dVar) {
        ee7.b(dVar, "<set-?>");
        this.s = dVar;
    }

    @DexIgnore
    public final void setMGraphWidth(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final void setMHeight(int i2) {
        this.a = i2;
    }

    @DexIgnore
    public final void setMLeftPadding(int i2) {
        this.c = i2;
    }

    @DexIgnore
    public final void setMLegend(e eVar) {
        ee7.b(eVar, "<set-?>");
        this.q = eVar;
    }

    @DexIgnore
    public final void setMLegendHeight(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void setMLegendWidth(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void setMRightPadding(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final void setMTopPadding(int i2) {
        this.d = i2;
    }

    @DexIgnore
    public final void setMWidth(int i2) {
        this.b = i2;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String simpleName = getClass().getSimpleName();
        ee7.a((Object) simpleName, "this::class.java.simpleName");
        this.t = simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        String simpleName = getClass().getSimpleName();
        ee7.a((Object) simpleName, "this::class.java.simpleName");
        this.t = simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        String simpleName = getClass().getSimpleName();
        ee7.a((Object) simpleName, "this::class.java.simpleName");
        this.t = simpleName;
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        ee7.b(motionEvent, Constants.EVENT);
        return super.onTouchEvent(motionEvent);
    }
}
