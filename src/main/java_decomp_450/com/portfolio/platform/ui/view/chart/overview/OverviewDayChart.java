package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bb7;
import com.fossil.do5;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.nh7;
import com.fossil.r87;
import com.fossil.re5;
import com.fossil.vt7;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewDayChart extends BarChart {
    @DexIgnore
    public ObjectAnimator w0;
    @DexIgnore
    public ObjectAnimator x0;
    @DexIgnore
    public do5 y0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ OverviewDayChart a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(OverviewDayChart overviewDayChart) {
            this.a = overviewDayChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator b = this.a.w0;
            sb.append(b != null ? Boolean.valueOf(b.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewDayChart overviewDayChart = this.a;
            do5 c = overviewDayChart.y0;
            if (c != null) {
                overviewDayChart.b(c);
                ObjectAnimator a2 = this.a.x0;
                if (a2 != null) {
                    a2.start();
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public OverviewDayChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v10 */
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void e(Canvas canvas) {
        BarChart.b bVar;
        ee7.b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        int a2 = a(getMChartModel().a());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = ea7.a((Iterable) arrayList, (Comparator) new c());
            Paint mGraphPaint = getMGraphPaint();
            ee7.a((Object) next, "item");
            mGraphPaint.setAlpha(a(next, a2));
            Iterator it2 = a3.iterator();
            if (!it2.hasNext()) {
                bVar = null;
            } else {
                Object next2 = it2.next();
                if (!it2.hasNext()) {
                    bVar = next2;
                } else {
                    int e = ((BarChart.b) next2).e();
                    do {
                        Object next3 = it2.next();
                        int e2 = ((BarChart.b) next3).e();
                        if (e < e2) {
                            next2 = next3;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
                bVar = next2;
            }
            BarChart.b bVar2 = bVar;
            if (bVar2 != null) {
                canvas.drawRoundRect(bVar2.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator f(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i, i4 * i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        ee7.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void h(Canvas canvas) {
        ee7.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<r87<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            r87<Integer, PointF> next = it.next();
            Rect rect = new Rect();
            float intValue = (float) next.getFirst().intValue();
            String valueOf = String.valueOf((int) intValue);
            float f = (float) 1000;
            if (intValue >= f) {
                valueOf = re5.a(intValue / f, 1) + ig5.a(PortfolioApp.g0.c(), 2131886628);
            }
            getMLegendPaint().getTextBounds(valueOf, 0, valueOf.length(), rect);
            float f2 = next.getSecond().y;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, width, f2, getMLegendLinePaint());
            canvas.drawText(valueOf, (width - getMGraphLegendMargin()) - ((float) rect.width()), f2 + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void i(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public void setGraphPreviewColor(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor color=" + i);
        getMGraph().invalidate();
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        setMNumberBar(24);
        h();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void b(do5 do5) {
        int i;
        ee7.b(do5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        setMChartModel((BarChart.c) do5);
        setMMaxValue(getMChartModel().c());
        if (getMChartModel().a().size() <= 1) {
            i = 24;
        } else {
            i = getMChartModel().a().size();
        }
        setMNumberBar(i);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public synchronized void a(do5 do5) {
        ee7.b(do5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("changeModel - model=");
        sb.append(do5);
        sb.append(", mOutAnim.isRunning=");
        ObjectAnimator objectAnimator = this.w0;
        sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        ObjectAnimator objectAnimator2 = this.x0;
        sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.d(tag, sb.toString());
        ObjectAnimator objectAnimator3 = this.w0;
        Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
        ObjectAnimator objectAnimator4 = this.x0;
        Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
        int i = 1;
        if (!ee7.a((Object) valueOf, (Object) true)) {
            if (!ee7.a((Object) valueOf2, (Object) true)) {
                if (ee7.a(getMChartModel(), do5)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.y0 = do5;
                this.w0 = b(this, getMMaxValue(), 255, 0, 0, 8, null);
                do5 do52 = this.y0;
                if (do52 != null) {
                    this.x0 = a(this, ((BarChart.c) do52).c(), 0, 255, 0, 8, null);
                    ObjectAnimator objectAnimator5 = this.w0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new b(this));
                    }
                    ObjectAnimator objectAnimator6 = this.w0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        }
        if (ee7.a(do5, this.y0)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.y0 = do5;
        if (ee7.a((Object) valueOf, (Object) true)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            do5 do53 = this.y0;
            if (do53 != null) {
                sb2.append(((BarChart.c) do53).c());
                local2.d(tag2, sb2.toString());
                do5 do54 = this.y0;
                if (do54 != null) {
                    this.x0 = a(this, ((BarChart.c) do54).c(), 0, 255, 0, 8, null);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
            ObjectAnimator objectAnimator7 = this.x0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c2 = getMChartModel().c();
            int mBarAlpha = getMBarAlpha();
            if (c2 > 0) {
                i = c2;
            }
            int i2 = mMaxValue / i;
            do5 do55 = this.y0;
            if (do55 != null) {
                b(do55);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag3 = getTAG();
                local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().c());
                ObjectAnimator e = e(getMChartModel().c(), mBarAlpha, 255, i2);
                this.x0 = e;
                if (e != null) {
                    e.start();
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator b(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.f(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        d();
        e();
        h(canvas);
        e(canvas);
        h();
    }

    @DexIgnore
    public final void h() {
        if (!TextUtils.isEmpty(getMBackgroundColor())) {
            String b2 = eh5.l.a().b(getMBackgroundColor());
            if (!TextUtils.isEmpty(b2)) {
                setBackgroundColor(Color.parseColor(b2));
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator e(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i4 * i, i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        ee7.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator a(OverviewDayChart overviewDayChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewDayChart.e(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a(Canvas canvas) {
        Bitmap a2;
        ee7.b(canvas, "canvas");
        super.a(canvas);
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = ea7.a((Iterable) arrayList, (Comparator) new d());
            if ((!a3.isEmpty()) && next.d() && (a2 = BarChart.a(this, getMLegendIconRes(), 0, 2, (Object) null)) != null) {
                RectF a4 = ((BarChart.b) a3.get(0)).a();
                canvas.drawBitmap(a2, a4.left + ((a4.width() - ((float) a2.getWidth())) * 0.5f), a4.bottom + ((float) getMTextMargin()), new Paint(1));
                a2.recycle();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void a(float f, float f2) {
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        if (size < 1) {
            getMTextPoint().clear();
            return;
        }
        float f3 = size == 1 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (f2 - f) / ((float) (size - 1));
        float mBarWidth = f + (getMBarWidth() * 0.5f);
        int i = 0;
        for (T t : getMLegendTexts()) {
            int i2 = i + 1;
            if (i >= 0) {
                T t2 = t;
                if (nh7.a((CharSequence) t2, (CharSequence) vt7.ANY_NON_NULL_MARKER, false, 2, (Object) null)) {
                    String str = (String) nh7.a((CharSequence) t2, new String[]{vt7.ANY_NON_NULL_MARKER}, false, 0, 6, (Object) null).get(0);
                    getMLegendPaint().getTextBounds(str, 0, str.length(), rect);
                    getMTextPoint().add(new r87<>(t2, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                } else {
                    getMLegendPaint().getTextBounds((String) t2, 0, t2.length(), rect);
                    getMTextPoint().add(new r87<>(t2, new PointF(((((float) i) * f3) + mBarWidth) - (((float) rect.width()) * 0.5f), mLegendHeight)));
                }
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int a(List<BarChart.a> list) {
        if (list.isEmpty()) {
            return 0;
        }
        int i = RecyclerView.UNDEFINED_DURATION;
        for (T t : list) {
            if (!t.c().isEmpty()) {
                ArrayList<BarChart.b> arrayList = t.c().get(0);
                ee7.a((Object) arrayList, "model.mListOfBarPoints[0]");
                Iterator<T> it = arrayList.iterator();
                while (it.hasNext()) {
                    int e = it.next().e();
                    if (e > i) {
                        i = e;
                    }
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final int a(BarChart.a aVar, int i) {
        T t;
        if (i < 0) {
            return 255;
        }
        int i2 = 0;
        ArrayList<BarChart.b> arrayList = aVar.c().get(0);
        ee7.a((Object) arrayList, "barModel.mListOfBarPoints[0]");
        Iterator<T> it = arrayList.iterator();
        if (!it.hasNext()) {
            t = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t = next;
            } else {
                int e = next.e();
                do {
                    T next2 = it.next();
                    int e2 = next2.e();
                    if (e < e2) {
                        next = next2;
                        e = e2;
                    }
                } while (it.hasNext());
            }
            t = next;
        }
        T t2 = t;
        if (t2 != null) {
            i2 = t2.e();
        }
        double d2 = (double) (((float) i2) / ((float) i));
        if (d2 >= 0.75d) {
            return 255;
        }
        return (int) ((d2 < 0.5d || d2 >= 0.75d) ? (d2 < 0.25d || d2 >= 0.5d) ? 63.75d : 127.5d : 191.25d);
    }
}
