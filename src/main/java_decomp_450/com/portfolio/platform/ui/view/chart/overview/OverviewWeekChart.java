package com.portfolio.platform.ui.view.chart.overview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bb7;
import com.fossil.do5;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.fo5;
import com.fossil.ig5;
import com.fossil.p87;
import com.fossil.pe7;
import com.fossil.r87;
import com.fossil.re5;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OverviewWeekChart extends BarChart {
    @DexIgnore
    public PointF w0;
    @DexIgnore
    public ObjectAnimator x0;
    @DexIgnore
    public ObjectAnimator y0;
    @DexIgnore
    public do5 z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ OverviewWeekChart a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(OverviewWeekChart overviewWeekChart) {
            this.a = overviewWeekChart;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationCancel");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = this.a.getTAG();
            StringBuilder sb = new StringBuilder();
            sb.append("changeModel - onAnimationEnd -- isRunning=");
            ObjectAnimator b = this.a.x0;
            sb.append(b != null ? Boolean.valueOf(b.isRunning()) : null);
            local.d(tag, sb.toString());
            OverviewWeekChart overviewWeekChart = this.a;
            do5 c = overviewWeekChart.z0;
            if (c != null) {
                overviewWeekChart.b(c);
                ObjectAnimator a2 = this.a.y0;
                if (a2 != null) {
                    a2.start();
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationRepeat");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            FLogger.INSTANCE.getLocal().d(this.a.getTAG(), "changeModel - onAnimationStart");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.c(), t2.c());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public OverviewWeekChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void e(Canvas canvas) {
        int i;
        ee7.b(canvas, "canvas");
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = ea7.a((Iterable) arrayList, (Comparator) new c()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    BarChart.b bVar = (BarChart.b) it2.next();
                    if (bVar.e() != 0) {
                        Paint mGraphPaint = getMGraphPaint();
                        if (next.a() <= 0 || bVar.e() < next.a()) {
                            i = getMInActiveColor();
                        } else {
                            int i2 = fo5.a[bVar.c().ordinal()];
                            if (i2 == 1) {
                                i = getMLowestColor();
                            } else if (i2 == 2) {
                                i = getMDefaultColor();
                            } else if (i2 == 3) {
                                i = getMHighestColor();
                            } else {
                                throw new p87();
                            }
                        }
                        mGraphPaint.setColor(i);
                        canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    public final ObjectAnimator f(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i, i4 * i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        ee7.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026s, outMaxValue, outAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void g(Canvas canvas) {
        String b2;
        ee7.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        if (getMGoalIconShow()) {
            canvas.drawPath(getMGoalLinePath(), getMGraphGoalLinePaint());
            int size = getMChartModel().a().size();
            if (size > 0) {
                Rect rect = new Rect();
                String a2 = a(getMChartModel().a().get(size - 1).a());
                getMLegendPaint().getTextBounds(a2, 0, a2.length(), rect);
                getMLegendPaint().setColor(getMActiveColor());
                canvas.drawText(a2, (width - getMGraphLegendMargin()) - ((float) rect.width()), this.w0.y + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
                if (!TextUtils.isEmpty(getMTextColor()) && (b2 = eh5.l.a().b(getMTextColor())) != null) {
                    getMLegendPaint().setColor(Color.parseColor(b2));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void h(Canvas canvas) {
        ee7.b(canvas, "canvas");
        float width = (float) canvas.getWidth();
        Iterator<r87<Integer, PointF>> it = getMGraphLegendPoint().iterator();
        while (it.hasNext()) {
            r87<Integer, PointF> next = it.next();
            Rect rect = new Rect();
            String a2 = a(next.getFirst().intValue());
            getMLegendPaint().getTextBounds(a2, 0, a2.length(), rect);
            float f = next.getSecond().y;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f, width, f, getMLegendLinePaint());
            canvas.drawText(a2, (width - getMGraphLegendMargin()) - ((float) rect.width()), f + getMGraphLegendMargin() + ((float) rect.height()), getMLegendPaint());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void i(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a() {
        super.a();
        setMNumberBar(7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart, com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        d();
        e();
        h(canvas);
        e(canvas);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.w0 = new PointF();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void a(Canvas canvas) {
        Bitmap a2;
        ee7.b(canvas, "canvas");
        super.a(canvas);
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            BarChart.a next = it.next();
            ArrayList<BarChart.b> arrayList = next.c().get(0);
            ee7.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List a3 = ea7.a((Iterable) arrayList, (Comparator) new d());
            if ((!a3.isEmpty()) && next.d() && (a2 = BarChart.a(this, getMLegendIconRes(), 0, 2, (Object) null)) != null) {
                RectF a4 = ((BarChart.b) a3.get(0)).a();
                canvas.drawBitmap(a2, a4.left + ((a4.width() - ((float) a2.getWidth())) * 0.5f), a4.bottom + ((float) getMTextMargin()), new Paint(1));
                a2.recycle();
            }
        }
        f(canvas);
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator b(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.f(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createOutAnim");
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void e() {
        T t;
        ArrayList<BarChart.a> a2 = getMChartModel().a();
        RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMarginEnd(), (float) getMGraphHeight());
        float height = rectF.height();
        float f = rectF.left;
        pe7 pe7 = new pe7();
        pe7 pe72 = new pe7();
        pe7 pe73 = new pe7();
        Iterator<T> it = a2.iterator();
        int i = 0;
        if (!it.hasNext()) {
            t = null;
        } else {
            t = it.next();
            if (it.hasNext()) {
                ArrayList<BarChart.b> arrayList = t.c().get(0);
                ee7.a((Object) arrayList, "it.mListOfBarPoints[0]");
                Iterator<T> it2 = arrayList.iterator();
                int i2 = 0;
                while (it2.hasNext()) {
                    i2 += it2.next().e();
                }
                do {
                    T next = it.next();
                    ArrayList<BarChart.b> arrayList2 = next.c().get(0);
                    ee7.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                    Iterator<T> it3 = arrayList2.iterator();
                    int i3 = 0;
                    while (it3.hasNext()) {
                        i3 += it3.next().e();
                    }
                    if (i2 < i3) {
                        i2 = i3;
                        t = next;
                    }
                } while (it.hasNext());
            }
        }
        T t2 = t;
        int size = a2.size() - 1;
        Iterator<T> it4 = a2.iterator();
        int i4 = 0;
        boolean z = false;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (it4.hasNext()) {
            T next2 = it4.next();
            int i5 = i4 + 1;
            if (i4 >= 0) {
                T t3 = next2;
                ArrayList<BarChart.b> arrayList3 = t3.c().get(i);
                ee7.a((Object) arrayList3, "item.mListOfBarPoints[0]");
                float a3 = (float) t3.a();
                pe72.element = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                pe7.element = getMBarWidth() + f;
                for (Iterator<BarChart.b> it5 = arrayList3.iterator(); it5.hasNext(); it5 = it5) {
                    float e = pe72.element + ((float) it5.next().e());
                    pe72.element = e;
                    float mMaxValue = (e * height) / ((float) getMMaxValue());
                    if (mMaxValue != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && mMaxValue < getMBarRadius()) {
                        mMaxValue = getMBarRadius();
                    }
                    f3 = ((float) getMGraphHeight()) - mMaxValue;
                }
                setMGoalIconShow(true);
                float mGraphHeight = ((float) getMGraphHeight()) - ((a3 * height) / ((float) getMMaxValue()));
                float mBarSpace = pe7.element + (getMBarSpace() * 0.5f);
                pe73.element = mBarSpace;
                float f4 = rectF.right;
                if (mBarSpace > f4) {
                    pe73.element = f4 + (getMBarMargin() * 0.5f);
                }
                if (!z) {
                    getMGoalLinePath().moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, mGraphHeight);
                    getMGoalLinePath().lineTo(pe73.element, mGraphHeight);
                    z = true;
                } else {
                    getMGoalLinePath().lineTo(f2, mGraphHeight);
                    getMGoalLinePath().lineTo(pe73.element, mGraphHeight);
                    getMGoalIconPoint().set(pe73.element, mGraphHeight - (((float) getMGoalIconSize()) * 0.5f));
                }
                if (i4 == size) {
                    getMGoalLinePath().lineTo((float) getMGraphWidth(), mGraphHeight);
                    this.w0 = new PointF((float) getMGraphWidth(), mGraphHeight);
                }
                f2 = pe73.element;
                if (ee7.a((Object) t2, (Object) t3)) {
                    ArrayList<BarChart.b> arrayList4 = t3.c().get(0);
                    ee7.a((Object) arrayList4, "item.mListOfBarPoints[0]");
                    Iterator<T> it6 = arrayList4.iterator();
                    int i6 = 0;
                    while (it6.hasNext()) {
                        i6 += it6.next().e();
                    }
                    if (i6 >= t3.a()) {
                        getMStarIconPoint().add(new PointF(f - ((float) getMStarIconSize()), f3 - (((float) getMStarIconSize()) * 1.25f)));
                        float mGraphHeight2 = f3 + ((((float) getMGraphHeight()) - f3) * 0.5f);
                        getMStarIconPoint().add(new PointF(pe7.element + (((float) getMStarIconSize()) * 0.5f), mGraphHeight2 - (((float) getMStarIconSize()) * 2.0f)));
                        getMStarIconPoint().add(new PointF(f - ((float) getMStarIconSize()), mGraphHeight2));
                    }
                }
                f = getMBarSpace() + pe7.element;
                i4 = i5;
                it4 = it4;
                i = 0;
            } else {
                w97.c();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void a(float f, float f2) {
        Rect rect = new Rect();
        getMLegendPaint().getTextBounds("gh", 0, 2, rect);
        float mLegendHeight = ((float) (getMLegendHeight() + rect.height())) * 0.5f;
        int size = getMLegendTexts().size();
        for (int i = 0; i < 7; i++) {
            if (i < size) {
                String str = getMLegendTexts().get(i);
                ee7.a((Object) str, "mLegendTexts[i]");
                String str2 = str;
                getMLegendPaint().getTextBounds(str2, 0, str2.length(), rect);
                float mBarWidth = getMBarWidth() + f;
                getMTextPoint().add(new r87<>(str2, new PointF(((f + mBarWidth) - ((float) rect.width())) * 0.5f, mLegendHeight)));
                f = mBarWidth + getMBarSpace();
            }
        }
    }

    @DexIgnore
    public String a(int i) {
        float f = (float) i;
        String valueOf = String.valueOf((int) f);
        float f2 = (float) 1000;
        if (f < f2) {
            return valueOf;
        }
        return re5.a(f / f2, 1) + ig5.a(getContext(), 2131886628);
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public synchronized void a(do5 do5) {
        ee7.b(do5, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("changeModel - model=");
        sb.append(do5);
        sb.append(", mOutAnim.isRunning=");
        ObjectAnimator objectAnimator = this.x0;
        sb.append(objectAnimator != null ? Boolean.valueOf(objectAnimator.isRunning()) : null);
        sb.append(", mInAnim.isRunning=");
        ObjectAnimator objectAnimator2 = this.y0;
        sb.append(objectAnimator2 != null ? Boolean.valueOf(objectAnimator2.isRunning()) : null);
        local.d(tag, sb.toString());
        ObjectAnimator objectAnimator3 = this.x0;
        Boolean valueOf = objectAnimator3 != null ? Boolean.valueOf(objectAnimator3.isRunning()) : null;
        ObjectAnimator objectAnimator4 = this.y0;
        Boolean valueOf2 = objectAnimator4 != null ? Boolean.valueOf(objectAnimator4.isRunning()) : null;
        int i = 1;
        if (!ee7.a((Object) valueOf, (Object) true)) {
            if (!ee7.a((Object) valueOf2, (Object) true)) {
                if (ee7.a(getMChartModel(), do5)) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - mChartModel == model");
                    return;
                }
                this.z0 = do5;
                this.x0 = b(this, getMMaxValue(), 255, 0, 0, 8, null);
                do5 do52 = this.z0;
                if (do52 != null) {
                    this.y0 = a(this, ((BarChart.c) do52).c(), 0, 255, 0, 8, null);
                    ObjectAnimator objectAnimator5 = this.x0;
                    if (objectAnimator5 != null) {
                        objectAnimator5.addListener(new b(this));
                    }
                    ObjectAnimator objectAnimator6 = this.x0;
                    if (objectAnimator6 != null) {
                        objectAnimator6.start();
                    }
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        }
        if (ee7.a(do5, this.z0)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - model == mTempModel");
            return;
        }
        this.z0 = do5;
        if (ee7.a((Object) valueOf, (Object) true)) {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - outRunning == true");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("changeModel - outRunning == true - mMaxValue=");
            do5 do53 = this.z0;
            if (do53 != null) {
                sb2.append(((BarChart.c) do53).c());
                local2.d(tag2, sb2.toString());
                do5 do54 = this.z0;
                if (do54 != null) {
                    this.y0 = a(this, ((BarChart.c) do54).c(), 0, 255, 0, 8, null);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.view.chart.base.BarChart.ChartModel");
            }
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "changeModel - inRunning == true");
            ObjectAnimator objectAnimator7 = this.y0;
            if (objectAnimator7 != null) {
                objectAnimator7.cancel();
            }
            int mMaxValue = getMMaxValue();
            int c2 = getMChartModel().c();
            int mBarAlpha = getMBarAlpha();
            if (c2 > 0) {
                i = c2;
            }
            int i2 = mMaxValue / i;
            do5 do55 = this.z0;
            if (do55 != null) {
                b(do55);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag3 = getTAG();
                local3.d(tag3, "changeModel - inRunning == true -- tempStartMaxValue=" + mMaxValue + ", " + "tempEndMaxValue=" + mMaxValue + ", tempAlpha=" + mMaxValue + ", tempMaxRate=" + mMaxValue + ", newMaxValue=" + getMChartModel().c());
                ObjectAnimator e = e(getMChartModel().c(), mBarAlpha, 255, i2);
                this.y0 = e;
                if (e != null) {
                    e.start();
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ ObjectAnimator a(OverviewWeekChart overviewWeekChart, int i, int i2, int i3, int i4, int i5, Object obj) {
        if (obj == null) {
            if ((i5 & 8) != 0) {
                i4 = 10;
            }
            return overviewWeekChart.e(i, i2, i3, i4);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createInAnim");
    }

    @DexIgnore
    public final ObjectAnimator e(int i, int i2, int i3, int i4) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, PropertyValuesHolder.ofInt("maxValue", i4 * i, i), PropertyValuesHolder.ofInt("barAlpha", i2, i3));
        ee7.a((Object) ofPropertyValuesHolder, "ObjectAnimator.ofPropert\u2026his, inMaxValue, inAlpha)");
        ofPropertyValuesHolder.setDuration(200L);
        return ofPropertyValuesHolder;
    }
}
