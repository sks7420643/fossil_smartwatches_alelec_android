package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.pl4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepQualityChart extends View {
    @DexIgnore
    public Paint a; // = new Paint(1);
    @DexIgnore
    public float b; // = 10.0f;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public int g; // = 50;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public SleepQualityChart(Context context) {
        super(context, null);
    }

    @DexIgnore
    public final void a(float f2) {
        this.a.setDither(true);
        this.a.setStyle(Paint.Style.STROKE);
        this.a.setStrokeWidth(f2);
        this.a.setAntiAlias(true);
    }

    @DexIgnore
    public final void b(float f2) {
        this.a.setShader(new LinearGradient((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.c, this.d, Shader.TileMode.CLAMP));
        this.a.setPathEffect(new DashPathEffect(new float[]{f2 / ((float) 5), 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        b((float) getWidth());
        if (canvas != null) {
            Bitmap bitmap = this.f;
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (bitmap != null) {
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float width2 = ((float) (canvas.getWidth() * this.g)) / 100.0f;
                if (width2 + width > ((float) canvas.getWidth())) {
                    width2 = ((float) canvas.getWidth()) - width;
                }
                canvas.drawBitmap(bitmap, width2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.a);
                f2 = height;
            }
            float f3 = f2 + (this.b / ((float) 2)) + 2.0f;
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, (float) canvas.getWidth(), f3, this.a);
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        Bitmap bitmap = this.f;
        if (bitmap != null) {
            bitmap.getHeight();
        }
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        int i3 = 0;
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                Bitmap bitmap2 = this.f;
                if (bitmap2 != null) {
                    i3 = bitmap2.getHeight();
                }
                size2 = (((int) this.b) * 2) + i3;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public final void setPercent(int i) {
        this.g = i;
        invalidate();
    }

    @DexIgnore
    public SleepQualityChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Bitmap bitmap = null;
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, pl4.SleepQualityChart) : null;
        if (obtainStyledAttributes != null) {
            this.b = obtainStyledAttributes.getDimension(4, 10.0f);
            this.c = obtainStyledAttributes.getColor(1, 0);
            this.d = obtainStyledAttributes.getColor(0, 0);
            this.e = obtainStyledAttributes.getDrawable(3);
            this.g = obtainStyledAttributes.getInt(2, 0);
        }
        a(this.b);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) this.e;
        this.f = bitmapDrawable != null ? bitmapDrawable.getBitmap() : bitmap;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
