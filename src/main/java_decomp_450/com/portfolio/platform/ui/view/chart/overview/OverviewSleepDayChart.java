package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eo5;
import com.fossil.gb5;
import com.fossil.nh7;
import com.fossil.p87;
import com.fossil.pl4;
import com.fossil.r87;
import com.fossil.tw6;
import com.fossil.xe5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepDayChart extends BarChart {
    @DexIgnore
    public Integer A0;
    @DexIgnore
    public Integer B0;
    @DexIgnore
    public int C0;
    @DexIgnore
    public float w0;
    @DexIgnore
    public float x0;
    @DexIgnore
    public float y0;
    @DexIgnore
    public Integer z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public OverviewSleepDayChart(Context context) {
        this(context, null);
    }

    @DexIgnore
    public void a(String str, int i) {
        ee7.b(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " color=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1991609013) {
            if (hashCode != -1521618862) {
                if (hashCode == -219887775 && str.equals("lightSleep")) {
                    this.A0 = Integer.valueOf(i);
                }
            } else if (str.equals("awakeSleep")) {
                this.z0 = Integer.valueOf(i);
            }
        } else if (str.equals("deepSleep")) {
            this.B0 = Integer.valueOf(i);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void d() {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        ArrayList<BarChart.a> a2 = getMChartModel().a();
        if (a2.size() != 0) {
            setMGoalLinePath(new Path());
            setMGoalIconPoint(new PointF());
            setMGoalIconShow(false);
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            getMGraphHeight();
            float f11 = rectF.left;
            float f12 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                i += next.get(0).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                }
            }
            float f13 = (rectF.right - rectF.left) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList<BarChart.b> next2 = it2.next();
                float mGraphHeight = ((float) getMGraphHeight()) * this.w0;
                int size = next2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (i2 < next2.size() - 1) {
                        f12 = (((float) (next2.get(i2 + 1).b() - next2.get(i2).b())) * f13) + f11;
                        int i3 = eo5.b[next2.get(i2).c().ordinal()];
                        if (i3 != 1) {
                            if (i3 == 2) {
                                f10 = (float) getMGraphHeight();
                                f9 = (float) getMGraphHeight();
                                f8 = this.x0;
                            } else if (i3 == 3) {
                                f10 = (float) getMGraphHeight();
                                f9 = (float) getMGraphHeight();
                                f8 = this.y0;
                            } else {
                                throw new p87();
                            }
                            f6 = (f10 - (f9 * f8)) - mGraphHeight;
                        } else {
                            f6 = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.w0);
                        }
                        int i4 = eo5.c[next2.get(i2).c().ordinal()];
                        if (i4 == 1) {
                            f7 = (float) getMGraphHeight();
                        } else if (i4 == 2 || i4 == 3) {
                            f7 = ((float) getMGraphHeight()) - mGraphHeight;
                        } else {
                            throw new p87();
                        }
                        next2.get(i2).a(new RectF(f11, f6, f12, f7));
                        f11 = f12;
                    } else if (i2 == next2.size() - 1) {
                        f12 = (((float) (next2.get(i2).d() - next2.get(i2).b())) * f13) + f11;
                        int i5 = eo5.d[next2.get(i2).c().ordinal()];
                        if (i5 != 1) {
                            if (i5 == 2) {
                                f5 = (float) getMGraphHeight();
                                f4 = (float) getMGraphHeight();
                                f3 = this.x0;
                            } else if (i5 == 3) {
                                f5 = (float) getMGraphHeight();
                                f4 = (float) getMGraphHeight();
                                f3 = this.y0;
                            } else {
                                throw new p87();
                            }
                            f = (f5 - (f4 * f3)) - mGraphHeight;
                        } else {
                            f = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.w0);
                        }
                        int i6 = eo5.e[next2.get(i2).c().ordinal()];
                        if (i6 == 1) {
                            f2 = (float) getMGraphHeight();
                        } else if (i6 == 2 || i6 == 3) {
                            f2 = ((float) getMGraphHeight()) - mGraphHeight;
                        } else {
                            throw new p87();
                        }
                        next2.get(next2.size() - 1).a(new RectF(f11, f, f12, f2));
                    } else {
                        continue;
                    }
                }
                f12 += ((float) 10) * f13;
                f11 = f12;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void e(Canvas canvas) {
        ee7.b(canvas, "canvas");
        canvas.drawRect(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.w0), (float) getMGraphWidth(), (((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.w0)) + ((float) 2)), getMLegendLinePaint());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            Iterator<ArrayList<BarChart.b>> it2 = it.next().c().iterator();
            while (it2.hasNext()) {
                Iterator<BarChart.b> it3 = it2.next().iterator();
                while (it3.hasNext()) {
                    BarChart.b next = it3.next();
                    int i = eo5.a[next.c().ordinal()];
                    if (i == 1) {
                        Paint mGraphPaint = getMGraphPaint();
                        Integer num = this.z0;
                        mGraphPaint.setColor(num != null ? num.intValue() : getMLowestColor());
                        tw6.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), false, false, true, true, getMGraphPaint());
                    } else if (i == 2) {
                        Paint mGraphPaint2 = getMGraphPaint();
                        Integer num2 = this.A0;
                        mGraphPaint2.setColor(num2 != null ? num2.intValue() : getMDefaultColor());
                        tw6.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    } else if (i == 3) {
                        Paint mGraphPaint3 = getMGraphPaint();
                        Integer num3 = this.B0;
                        mGraphPaint3.setColor(num3 != null ? num3.intValue() : getMHighestColor());
                        tw6.a(canvas, next.a().left, next.a().top, next.a().right, next.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void i(Canvas canvas) {
        ee7.b(canvas, "canvas");
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.C0 = i;
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        this.C0 = -1;
        if (attributeSet != null && context != null && (theme = context.getTheme()) != null && (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, pl4.OverviewSleepDayChart, 0, 0)) != null) {
            try {
                this.w0 = obtainStyledAttributes.getFloat(0, 0.1f);
                this.x0 = obtainStyledAttributes.getFloat(2, 0.33f);
                this.y0 = obtainStyledAttributes.getFloat(1, 0.6f);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void a(float f, float f2) {
        if (getMChartModel().a().size() != 0) {
            ArrayList<ArrayList<BarChart.b>> c = getMChartModel().a().get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                BarChart.b bVar = next.get(0);
                ee7.a((Object) bVar, "session[0]");
                BarChart.b bVar2 = bVar;
                BarChart.b bVar3 = next.get(next.size() - 1);
                ee7.a((Object) bVar3, "session[session.size - 1]");
                BarChart.b bVar4 = bVar3;
                long j = (long) 1000;
                long e = ((long) bVar2.e()) * j;
                long d = ((((long) bVar4.d()) - 1) * ((long) 60) * j) + e;
                Rect rect = new Rect();
                int i = this.C0;
                String c2 = i != -1 ? zd5.c(e, i) : zd5.b(e);
                int i2 = this.C0;
                String c3 = i2 != -1 ? zd5.c(d, i2) : zd5.b(d);
                xe5 xe5 = xe5.b;
                ee7.a((Object) c2, "startTimeString");
                String b = xe5.b(c2);
                xe5 xe52 = xe5.b;
                ee7.a((Object) c3, "endTimeString");
                String b2 = xe52.b(c3);
                getMLegendPaint().getTextBounds(b, 0, nh7.c((CharSequence) b), rect);
                float mTextMargin = ((float) getMTextMargin()) + ((float) rect.height());
                if (bVar4.a().right - bVar2.a().left > ((float) (rect.right * 2))) {
                    getMTextPoint().add(new r87<>(b, new PointF(bVar2.a().left, mTextMargin)));
                    getMTextPoint().add(new r87<>(b2, new PointF((bVar4.a().right - f) - ((float) rect.right), mTextMargin)));
                } else if (c.indexOf(next) == c.size() - 1) {
                    getMTextPoint().add(new r87<>(b2, new PointF((bVar4.a().right - f) - ((float) rect.right), mTextMargin)));
                } else {
                    getMTextPoint().add(new r87<>(b, new PointF(bVar2.a().left, mTextMargin)));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BarChart
    public void e() {
        if (getMGoalType() == gb5.TOTAL_SLEEP && getMChartModel().a().size() != 0) {
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            ArrayList<BarChart.a> a2 = getMChartModel().a();
            int b = getMChartModel().b();
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList<BarChart.b> next = it.next();
                i += next.get(0).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                    b += 10;
                }
            }
            float f = (((float) b) * (rectF.right - rectF.left)) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList<BarChart.b> next2 = it2.next();
                if (f <= next2.get(next2.size() - 1).a().right) {
                    Iterator<BarChart.b> it3 = next2.iterator();
                    while (it3.hasNext()) {
                        BarChart.b next3 = it3.next();
                        if (f >= next3.a().left && f <= next3.a().right) {
                            getMGoalLinePath().moveTo(f, getMSafeAreaHeight() - 10.0f);
                            getMGoalLinePath().lineTo(f, next3.a().top);
                            getMGoalIconPoint().set(f - (((float) getMGoalIconSize()) * 0.5f), (getMSafeAreaHeight() * 0.5f) - 20.0f);
                            setMGoalIconShow(false);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }
}
