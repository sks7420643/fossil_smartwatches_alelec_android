package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ad5;
import com.fossil.be5;
import com.fossil.cb0;
import com.fossil.ch5;
import com.fossil.cl5;
import com.fossil.cy6;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fl4;
import com.fossil.fv7;
import com.fossil.gd7;
import com.fossil.he;
import com.fossil.i0;
import com.fossil.i97;
import com.fossil.ig5;
import com.fossil.ik7;
import com.fossil.jc5;
import com.fossil.je;
import com.fossil.jl5;
import com.fossil.kd7;
import com.fossil.ll5;
import com.fossil.ml5;
import com.fossil.nb7;
import com.fossil.nh7;
import com.fossil.nj5;
import com.fossil.nl5;
import com.fossil.ol5;
import com.fossil.om5;
import com.fossil.pl5;
import com.fossil.qj7;
import com.fossil.r87;
import com.fossil.rb7;
import com.fossil.sj5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.ti7;
import com.fossil.vh7;
import com.fossil.w97;
import com.fossil.we7;
import com.fossil.wz6;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.xe7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.DebugForceBackgroundRequestData;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DebugActivity extends cl5 implements jl5.b {
    @DexIgnore
    public static /* final */ a M; // = new a(null);
    @DexIgnore
    public FirmwareFileRepository A;
    @DexIgnore
    public GuestApiService B;
    @DexIgnore
    public DianaPresetRepository C;
    @DexIgnore
    public sj5 D;
    @DexIgnore
    public ad5 E;
    @DexIgnore
    public /* final */ jl5 F; // = new jl5();
    @DexIgnore
    public /* final */ ArrayList<pl5> G; // = new ArrayList<>();
    @DexIgnore
    public wz6 H;
    @DexIgnore
    public String I; // = "";
    @DexIgnore
    public HeartRateMode J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public /* final */ e L; // = new e(this);
    @DexIgnore
    public ch5 y;
    @DexIgnore
    public om5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            ee7.b(context, "context");
            context.startActivity(new Intent(context, DebugActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements InputFilter {
        @DexIgnore
        public double a;
        @DexIgnore
        public double b;

        @DexIgnore
        public b(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0018 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(double r4, double r6, double r8) {
            /*
                r3 = this;
                r0 = 1
                r1 = 0
                int r2 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
                if (r2 <= 0) goto L_0x000f
                int r2 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
                if (r2 < 0) goto L_0x0018
                int r4 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
                if (r4 > 0) goto L_0x0018
                goto L_0x0019
            L_0x000f:
                int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
                if (r2 < 0) goto L_0x0018
                int r6 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
                if (r6 > 0) goto L_0x0018
                goto L_0x0019
            L_0x0018:
                r0 = 0
            L_0x0019:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.b.a(double, double, double):boolean");
        }

        @DexIgnore
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            ee7.b(charSequence, "source");
            ee7.b(spanned, "dest");
            try {
                StringBuilder sb = new StringBuilder();
                String obj = spanned.toString();
                if (obj != null) {
                    String substring = obj.substring(0, i3);
                    ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    sb.append(substring);
                    String obj2 = spanned.toString();
                    int length = spanned.toString().length();
                    if (obj2 != null) {
                        String substring2 = obj2.substring(i4, length);
                        ee7.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        sb.append(substring2);
                        String sb2 = sb.toString();
                        StringBuilder sb3 = new StringBuilder();
                        if (sb2 != null) {
                            String substring3 = sb2.substring(0, i3);
                            ee7.a((Object) substring3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            sb3.append(substring3);
                            sb3.append(charSequence.toString());
                            int length2 = sb2.length();
                            if (sb2 != null) {
                                String substring4 = sb2.substring(i3, length2);
                                ee7.a((Object) substring4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                                sb3.append(substring4);
                                if (a(this.a, this.b, Double.parseDouble(sb3.toString()))) {
                                    return null;
                                }
                                return "";
                            }
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.debug.DebugActivity", f = "DebugActivity.kt", l = {268}, m = "loadFirmware")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DebugActivity debugActivity, fb7 fb7) {
            super(fb7);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.debug.DebugActivity$loadFirmware$repoResponse$1", f = "DebugActivity.kt", l = {268}, m = "invokeSuspend")
    public static final class d extends zb7 implements gd7<fb7<? super fv7<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DebugActivity debugActivity, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = debugActivity;
            this.$model = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.this$0, this.$model, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<Firmware>>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GuestApiService w = this.this$0.w();
                String g = PortfolioApp.g0.c().g();
                String str = this.$model;
                this.label = 1;
                obj = GuestApiService.DefaultImpls.getFirmwares$default(w, g, str, "android", false, this, 8, null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            T t;
            ArrayList<ll5> a2;
            T t2;
            ArrayList<ll5> a3;
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            ll5 ll5 = null;
            if (communicateMode == CommunicateMode.SET_HEART_RATE_MODE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.A().a(this.a.x());
                    Iterator<T> it = this.a.s().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it.next();
                        if (ee7.a((Object) t2.b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    T t3 = t2;
                    if (!(t3 == null || (a3 = t3.a()) == null)) {
                        Iterator<T> it2 = a3.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            T next = it2.next();
                            if (ee7.a((Object) next.a(), (Object) "SWITCH HEART RATE MODE")) {
                                ll5 = next;
                                break;
                            }
                        }
                        ll5 = ll5;
                    }
                    ol5 ol5 = (ol5) ll5;
                    if (ol5 != null) {
                        ol5.b(this.a.x().name());
                    }
                    this.a.q().notifyDataSetChanged();
                } else {
                    DebugActivity debugActivity = this.a;
                    HeartRateMode b = debugActivity.A().b(PortfolioApp.g0.c().c());
                    ee7.a((Object) b, "mSharedPreferencesManage\u2026tance.activeDeviceSerial)");
                    debugActivity.a(b);
                }
                this.a.g();
            } else if (communicateMode == CommunicateMode.SET_FRONT_LIGHT_ENABLE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.A().g(this.a.y());
                } else {
                    DebugActivity debugActivity2 = this.a;
                    debugActivity2.d(debugActivity2.A().k(PortfolioApp.g0.c().c()));
                    Iterator<T> it3 = this.a.s().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it3.next();
                        if (ee7.a((Object) t.b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    T t4 = t;
                    if (!(t4 == null || (a2 = t4.a()) == null)) {
                        Iterator<T> it4 = a2.iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                break;
                            }
                            T next2 = it4.next();
                            if (ee7.a((Object) next2.a(), (Object) "FRONT LIGHT ENABLE")) {
                                ll5 = next2;
                                break;
                            }
                        }
                        ll5 = ll5;
                    }
                    nl5 nl5 = (nl5) ll5;
                    if (nl5 != null) {
                        nl5.a(this.a.y());
                    }
                    this.a.q().notifyDataSetChanged();
                }
                this.a.g();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$4", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements gd7<fb7<? super List<? extends DebugFirmwareData>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, f fVar) {
                super(1, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(fb7<?> fb7) {
                ee7.b(fb7, "completion");
                return new a(fb7, this.this$0);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public final Object invoke(fb7<? super List<? extends DebugFirmwareData>> fb7) {
                return ((a) create(fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    DebugActivity debugActivity = this.this$0.this$0;
                    String a2 = debugActivity.I;
                    this.label = 1;
                    obj = debugActivity.a(a2, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DebugActivity debugActivity, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = debugActivity;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$activeDeviceSerial, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                Device deviceBySerial = this.this$0.b().getDeviceBySerial(this.$activeDeviceSerial);
                if (deviceBySerial != null) {
                    DebugActivity debugActivity = this.this$0;
                    String sku = deviceBySerial.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    debugActivity.I = sku;
                    Firmware a2 = this.this$0.A().a(this.this$0.I);
                    if (a2 != null) {
                        DebugActivity.b(this.this$0).a(a2);
                    }
                    if (!DebugActivity.b(this.this$0).c()) {
                        DebugActivity.b(this.this$0).a(new a(null, this));
                    }
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<Firmware> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public g(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Firmware firmware) {
            ll5 ll5;
            T t;
            ArrayList<ll5> a2;
            if (firmware != null) {
                Iterator<T> it = this.a.s().iterator();
                while (true) {
                    ll5 = null;
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.b(), (Object) "OTHER")) {
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (a2 = t2.a()) == null)) {
                    Iterator<T> it2 = a2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (ee7.a((Object) next.a(), (Object) "CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                            ll5 = next;
                            break;
                        }
                    }
                    ll5 = ll5;
                }
                if (ll5 != null) {
                    ll5.a("CONSIDER AS LATEST BUNDLE FIRMWARE: " + firmware.getVersionNumber());
                }
                this.a.q().notifyDataSetChanged();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<List<? extends DebugFirmwareData>> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public h(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DebugFirmwareData> list) {
            T t;
            ArrayList<ll5> a2;
            ArrayList<ll5> a3;
            if (list != null && (!list.isEmpty())) {
                Iterator<T> it = this.a.s().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.b(), (Object) "FIRMWARE")) {
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (a3 = t2.a()) == null)) {
                    a3.clear();
                }
                for (T t3 : list) {
                    Firmware firmware = t3.getFirmware();
                    int state = t3.getState();
                    String str = state != 1 ? state != 2 ? "" : "Downloaded" : "Downloading";
                    String versionNumber = firmware.getVersionNumber();
                    ee7.a((Object) versionNumber, "firmware.versionNumber");
                    ol5 ol5 = new ol5("FIRMWARE", versionNumber, str);
                    ol5.a((Object) t3);
                    if (!(t2 == null || (a2 = t2.a()) == null)) {
                        a2.add(ol5);
                    }
                }
                this.a.q().notifyDataSetChanged();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1", f = "DebugActivity.kt", l = {Action.Presenter.PREVIOUS, Action.Presenter.BLACKOUT, 304}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Toast.makeText(this.this$0.this$0, "Log will be sent after 1 second", 1).show();
                    this.this$0.this$0.finish();
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(DebugActivity debugActivity, fb7 fb7) {
            super(2, fb7);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x006b A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 3
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x0031
                if (r1 == r4) goto L_0x0029
                if (r1 == r3) goto L_0x0021
                if (r1 != r2) goto L_0x0019
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x006c
            L_0x0019:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0021:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0057
            L_0x0029:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x004a
            L_0x0031:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r7 = r6.p$
                com.portfolio.platform.ui.debug.DebugActivity r1 = r6.this$0
                com.fossil.sj5 r1 = r1.z()
                com.portfolio.platform.ui.debug.DebugActivity r5 = r6.this$0
                r6.L$0 = r7
                r6.label = r4
                java.lang.Object r1 = r1.a(r5, r6)
                if (r1 != r0) goto L_0x0049
                return r0
            L_0x0049:
                r1 = r7
            L_0x004a:
                r4 = 1000(0x3e8, double:4.94E-321)
                r6.L$0 = r1
                r6.label = r3
                java.lang.Object r7 = com.fossil.kj7.a(r4, r6)
                if (r7 != r0) goto L_0x0057
                return r0
            L_0x0057:
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.portfolio.platform.ui.debug.DebugActivity$i$a r3 = new com.portfolio.platform.ui.debug.DebugActivity$i$a
                r4 = 0
                r3.<init>(r6, r4)
                r6.L$0 = r1
                r6.label = r2
                java.lang.Object r7 = com.fossil.vh7.a(r7, r3, r6)
                if (r7 != r0) goto L_0x006c
                return r0
            L_0x006c:
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;
        @DexIgnore
        public /* final */ /* synthetic */ EditText c;
        @DexIgnore
        public /* final */ /* synthetic */ EditText d;
        @DexIgnore
        public /* final */ /* synthetic */ EditText e;
        @DexIgnore
        public /* final */ /* synthetic */ i0 f;

        @DexIgnore
        public j(DebugActivity debugActivity, EditText editText, EditText editText2, EditText editText3, EditText editText4, i0 i0Var) {
            this.a = debugActivity;
            this.b = editText;
            this.c = editText2;
            this.d = editText3;
            this.e = editText4;
            this.f = i0Var;
        }

        @DexIgnore
        public final void onClick(View view) {
            int i;
            int i2;
            int i3;
            int i4;
            EditText editText = this.b;
            ee7.a((Object) editText, "etDelay");
            String obj = editText.getText().toString();
            EditText editText2 = this.c;
            ee7.a((Object) editText2, "etDuration");
            String obj2 = editText2.getText().toString();
            EditText editText3 = this.d;
            ee7.a((Object) editText3, "etRepeat");
            String obj3 = editText3.getText().toString();
            EditText editText4 = this.e;
            ee7.a((Object) editText4, "etDelayEachTime");
            String obj4 = editText4.getText().toString();
            try {
                i = Integer.parseInt(obj);
            } catch (Exception unused) {
                i = 0;
            }
            try {
                i2 = Integer.parseInt(obj2);
            } catch (Exception unused2) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(obj3);
            } catch (Exception unused3) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(obj4);
            } catch (Exception unused4) {
                i4 = 0;
            }
            if (i > 65535 || i2 > 65535 || i3 > 65535 || i4 > 65535) {
                Toast.makeText(this.a, "Value should be in range [0, 65535]", 0).show();
                return;
            }
            PortfolioApp.g0.c().a(i, i2, i3, i4);
            this.f.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;

        @DexIgnore
        public k(DebugActivity debugActivity, EditText editText) {
            this.a = debugActivity;
            this.b = editText;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            String obj = this.b.getText().toString();
            if (obj != null) {
                Integer valueOf = Integer.valueOf(nh7.d((CharSequence) obj).toString());
                ee7.a((Object) valueOf, "Integer.valueOf(input.text.toString().trim())");
                int intValue = valueOf.intValue();
                this.a.A().e(intValue);
                for (T t : this.a.s()) {
                    if (ee7.a((Object) t.b(), (Object) "SIMULATION")) {
                        for (T t2 : t.a()) {
                            if (ee7.a((Object) t2.c(), (Object) "TRIGGER LOW BATTERY EVENT")) {
                                if (t2 != null) {
                                    we7 we7 = we7.a;
                                    String format = String.format("Battery level: %d", Arrays.copyOf(new Object[]{Integer.valueOf(intValue)}, 1));
                                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                    t2.b(format);
                                    this.a.q().notifyDataSetChanged();
                                    return;
                                }
                                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ i0 a;

        @DexIgnore
        public l(i0 i0Var) {
            this.a = i0Var;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            if (com.fossil.ee7.a(java.lang.Integer.valueOf(com.fossil.nh7.d((java.lang.CharSequence) r6).toString()).intValue(), 100) <= 0) goto L_0x006f;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void afterTextChanged(android.text.Editable r6) {
            /*
                r5 = this;
                java.lang.String r0 = "s"
                com.fossil.ee7.b(r6, r0)
                int r0 = r6.length()
                r1 = 1
                r2 = 0
                if (r0 != 0) goto L_0x000f
                r0 = 1
                goto L_0x0010
            L_0x000f:
                r0 = 0
            L_0x0010:
                java.lang.String r3 = "dialog.getButton(AlertDialog.BUTTON_POSITIVE)"
                r4 = -1
                if (r0 == 0) goto L_0x0022
                com.fossil.i0 r6 = r5.a
                android.widget.Button r6 = r6.b(r4)
                com.fossil.ee7.a(r6, r3)
                r6.setEnabled(r2)
                goto L_0x0072
            L_0x0022:
                com.fossil.i0 r0 = r5.a
                android.widget.Button r0 = r0.b(r4)
                com.fossil.ee7.a(r0, r3)
                java.lang.String r3 = r6.toString()
                java.lang.String r4 = "null cannot be cast to non-null type kotlin.CharSequence"
                if (r3 == 0) goto L_0x0073
                java.lang.CharSequence r3 = com.fossil.nh7.d(r3)
                java.lang.String r3 = r3.toString()
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                int r3 = r3.intValue()
                int r3 = com.fossil.ee7.a(r3, r1)
                if (r3 < 0) goto L_0x006e
                java.lang.String r6 = r6.toString()
                if (r6 == 0) goto L_0x0068
                java.lang.CharSequence r6 = com.fossil.nh7.d(r6)
                java.lang.String r6 = r6.toString()
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
                int r6 = r6.intValue()
                r3 = 100
                int r6 = com.fossil.ee7.a(r6, r3)
                if (r6 > 0) goto L_0x006e
                goto L_0x006f
            L_0x0068:
                com.fossil.x87 r6 = new com.fossil.x87
                r6.<init>(r4)
                throw r6
            L_0x006e:
                r1 = 0
            L_0x006f:
                r0.setEnabled(r1)
            L_0x0072:
                return
            L_0x0073:
                com.fossil.x87 r6 = new com.fossil.x87
                r6.<init>(r4)
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.l.afterTextChanged(android.text.Editable):void");
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Firmware a;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ DebugFirmwareData c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$m$a$a")
            /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$m$a$a  reason: collision with other inner class name */
            public static final class C0306a extends zb7 implements kd7<yi7, fb7<? super File>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0306a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0306a aVar = new C0306a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super File> fb7) {
                    return ((C0306a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        FirmwareFileRepository v = this.this$0.this$0.b.v();
                        String versionNumber = this.this$0.this$0.a.getVersionNumber();
                        ee7.a((Object) versionNumber, "firmware.versionNumber");
                        String downloadUrl = this.this$0.this$0.a.getDownloadUrl();
                        ee7.a((Object) downloadUrl, "firmware.downloadUrl");
                        String checksum = this.this$0.this$0.a.getChecksum();
                        ee7.a((Object) checksum, "firmware.checksum");
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = v.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = mVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                DebugFirmwareData debugFirmwareData;
                int i;
                Object a = nb7.a();
                int i2 = this.label;
                if (i2 == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = qj7.a();
                    C0306a aVar = new C0306a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((File) obj) != null) {
                    debugFirmwareData = this.this$0.c;
                    i = 2;
                } else {
                    debugFirmwareData = this.this$0.c;
                    i = 0;
                }
                debugFirmwareData.setState(i);
                DebugActivity.b(this.this$0.b).d();
                return i97.a;
            }
        }

        @DexIgnore
        public m(Firmware firmware, DebugActivity debugActivity, DebugFirmwareData debugFirmwareData) {
            this.a = firmware;
            this.b = debugActivity;
            this.c = debugFirmwareData;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, null), 3, null);
            this.c.setState(1);
            DebugActivity.b(this.b).d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ n a; // = new n();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware b;

        @DexIgnore
        public o(DebugActivity debugActivity, Firmware firmware) {
            this.a = debugActivity;
            this.b = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.a(this.b);
            dialogInterface.dismiss();
            this.a.finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ p a; // = new p();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware b;

        @DexIgnore
        public q(DebugActivity debugActivity, Firmware firmware) {
            this.a = debugActivity;
            this.b = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.A().a(this.b, this.a.I);
            DebugActivity.b(this.a).a(this.b);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ r a; // = new r();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static final /* synthetic */ wz6 b(DebugActivity debugActivity) {
        wz6 wz6 = debugActivity.H;
        if (wz6 != null) {
            return wz6;
        }
        ee7.d("mFirmwareViewModel");
        throw null;
    }

    @DexIgnore
    public final ch5 A() {
        ch5 ch5 = this.y;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void c(Firmware firmware) {
        i0.a aVar = new i0.a(this);
        aVar.b("Confirm Latest Firmware");
        aVar.a("Are you sure you want to use firmware " + firmware.getVersionNumber() + " as the bundle latest firmware?");
        aVar.b("Confirm", new q(this, firmware));
        aVar.a("Cancel", r.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public final void d(boolean z2) {
        this.K = z2;
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558430);
        PortfolioApp.g0.c().f().a(this);
        String c2 = PortfolioApp.g0.c().c();
        ch5 ch5 = this.y;
        if (ch5 != null) {
            HeartRateMode b2 = ch5.b(c2);
            ee7.a((Object) b2, "mSharedPreferencesManage\u2026eMode(activeDeviceSerial)");
            this.J = b2;
            ch5 ch52 = this.y;
            if (ch52 != null) {
                this.K = ch52.k(c2);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new ll5("VIEW LOG", "VIEW LOG"));
                arrayList.add(new ll5("SEND LOG", "SEND LOG"));
                arrayList.add(new ll5("RESET UAPP LOG FILES", "RESET UAPP LOG FILES"));
                this.G.add(new pl5("LOG", "LOG", arrayList, false, 8, null));
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(new ll5("CLEAR DATA", "CLEAR DATA"));
                arrayList2.add(new ll5("GENERATE PRESET DATA", "GENERATE PRESET DATA"));
                this.G.add(new pl5("DATA", "DATA", arrayList2, false, 8, null));
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new ll5("BLUETOOTH SETTING", "BLUETOOTH SETTING"));
                this.G.add(new pl5("ANDROID SETTINGS", "ANDROID SETTINGS", arrayList3, false, 8, null));
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(new ll5("START MINDFULNESS PRACTICE", "START MINDFULNESS PRACTICE"));
                arrayList4.add(new ll5("WATCH APP MUSIC CONTROL", "WATCH APP MUSIC CONTROL"));
                arrayList4.add(new ll5("SIMULATE DISCONNECTION", "SIMULATE DISCONNECTION"));
                we7 we7 = we7.a;
                Object[] objArr = new Object[1];
                ch5 ch53 = this.y;
                if (ch53 != null) {
                    objArr[0] = Integer.valueOf(ch53.z());
                    String format = String.format("Battery level: %d", Arrays.copyOf(objArr, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    arrayList4.add(new ol5("TRIGGER LOW BATTERY EVENT", "TRIGGER LOW BATTERY EVENT", format));
                    this.G.add(new pl5("SIMULATION", "SIMULATION", arrayList4, false, 8, null));
                    ArrayList arrayList5 = new ArrayList();
                    arrayList5.add(new ll5("FIRMWARE_LOADING", "FIRMWARE_LOADING"));
                    this.G.add(new pl5("FIRMWARE", "FIRMWARE", arrayList5, false, 8, null));
                    ArrayList arrayList6 = new ArrayList();
                    arrayList6.add(new ll5("SYNC", "SYNC"));
                    List<DebugForceBackgroundRequestData> d2 = w97.d(new DebugForceBackgroundRequestData("Notification Filter Background", ForceBackgroundRequest.BackgroundRequestType.SET_NOTIFICATION_FILTER), new DebugForceBackgroundRequestData("Alarms Background", ForceBackgroundRequest.BackgroundRequestType.SET_MULTI_ALARM), new DebugForceBackgroundRequestData("Device Configuration Background", ForceBackgroundRequest.BackgroundRequestType.SET_CONFIG_FILE));
                    ArrayList arrayList7 = new ArrayList(x97.a(d2, 10));
                    for (DebugForceBackgroundRequestData debugForceBackgroundRequestData : d2) {
                        arrayList7.add(debugForceBackgroundRequestData.getVisibleName());
                    }
                    ml5 ml5 = new ml5("FORCE_BACKGROUND_REQUEST", "FORCE_BACKGROUND_REQUEST", ea7.d((Collection) arrayList7), "Send");
                    ml5.a(d2);
                    i97 i97 = i97.a;
                    arrayList6.add(ml5);
                    arrayList6.add(new ll5("WEATHER_WATCH_APP_TAP", "WEATHER_WATCH_APP_TAP"));
                    arrayList6.add(new ll5("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT", "RESET DEVICE SETTING IN FIRMWARE TO DEFAULT"));
                    arrayList6.add(new ll5("RESET_DELAY_OTA_TIMESTAMP", "RESET_DELAY_OTA_TIMESTAMP"));
                    ch5 ch54 = this.y;
                    if (ch54 != null) {
                        arrayList6.add(new nl5("SKIP OTA", "SKIP OTA", ch54.a0()));
                        ch5 ch55 = this.y;
                        if (ch55 != null) {
                            arrayList6.add(new nl5("SHOW ALL DEVICES", "SHOW ALL DEVICES", ch55.Z()));
                            ch5 ch56 = this.y;
                            if (ch56 != null) {
                                arrayList6.add(new nl5("DISABLE HW_LOG SYNC", "DISABLE HW_LOG SYNC", !ch56.N()));
                                ch5 ch57 = this.y;
                                if (ch57 != null) {
                                    arrayList6.add(new nl5("DISABLE AUTO SYNC", "DISABLE AUTO SYNC", true ^ ch57.G()));
                                    ch5 ch58 = this.y;
                                    if (ch58 != null) {
                                        arrayList6.add(new nl5("SHOW DISPLAY DEVICE INFO", "SHOW DISPLAY DEVICE INFO", ch58.O()));
                                        ch5 ch59 = this.y;
                                        if (ch59 != null) {
                                            arrayList6.add(new nl5("CONSIDER AS LATEST BUNDLE FIRMWARE", "CONSIDER AS LATEST BUNDLE FIRMWARE:No Firmware", ch59.H()));
                                            ch5 ch510 = this.y;
                                            if (ch510 != null) {
                                                arrayList6.add(new nl5("APPLY NEW NOTIFICATION FILTER", "APPLY NEW NOTIFICATION FILTER", ch510.S()));
                                                if (FossilDeviceSerialPatternUtil.isDianaDevice(c2)) {
                                                    arrayList6.add(new nl5("FRONT LIGHT ENABLE", "FRONT LIGHT ENABLE", this.K));
                                                    HeartRateMode heartRateMode = this.J;
                                                    if (heartRateMode != null) {
                                                        arrayList6.add(new ol5("SWITCH HEART RATE MODE", "SWITCH HEART RATE MODE", heartRateMode.name()));
                                                    } else {
                                                        ee7.d("mHeartRateMode");
                                                        throw null;
                                                    }
                                                }
                                                this.G.add(new pl5("OTHER", "OTHER", arrayList6, false, 8, null));
                                                ArrayList arrayList8 = new ArrayList();
                                                arrayList8.add(new ll5("CUSTOMIZE THEME", "CUSTOMIZE THEME"));
                                                this.G.add(new pl5("THEME", "THEME", arrayList8, false, 8, null));
                                                jl5 jl5 = this.F;
                                                jl5.a(this.G);
                                                jl5.a(this);
                                                i97 i972 = i97.a;
                                                RecyclerView recyclerView = (RecyclerView) findViewById(2131362984);
                                                recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                                                recyclerView.setAdapter(this.F);
                                                RecyclerView.g adapter = recyclerView.getAdapter();
                                                if (adapter != null) {
                                                    adapter.notifyDataSetChanged();
                                                    i97 i973 = i97.a;
                                                    he a2 = je.a((FragmentActivity) this).a(wz6.class);
                                                    ee7.a((Object) a2, "ViewModelProviders.of(th\u2026bugViewModel::class.java)");
                                                    this.H = (wz6) a2;
                                                    h hVar = new h(this);
                                                    wz6 wz6 = this.H;
                                                    if (wz6 != null) {
                                                        wz6.a().a(this, hVar);
                                                        g gVar = new g(this);
                                                        wz6 wz62 = this.H;
                                                        if (wz62 != null) {
                                                            wz62.b().a(this, gVar);
                                                            if (!TextUtils.isEmpty(c2)) {
                                                                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new f(this, c2, null), 3, null);
                                                                return;
                                                            }
                                                            return;
                                                        }
                                                        ee7.d("mFirmwareViewModel");
                                                        throw null;
                                                    }
                                                    ee7.d("mFirmwareViewModel");
                                                    throw null;
                                                }
                                                ee7.a();
                                                throw null;
                                            }
                                            ee7.d("mSharedPreferencesManager");
                                            throw null;
                                        }
                                        ee7.d("mSharedPreferencesManager");
                                        throw null;
                                    }
                                    ee7.d("mSharedPreferencesManager");
                                    throw null;
                                }
                                ee7.d("mSharedPreferencesManager");
                                throw null;
                            }
                            ee7.d("mSharedPreferencesManager");
                            throw null;
                        }
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                    ee7.d("mSharedPreferencesManager");
                    throw null;
                }
                ee7.d("mSharedPreferencesManager");
                throw null;
            }
            ee7.d("mSharedPreferencesManager");
            throw null;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onPause() {
        super.onPause();
        nj5.d.b(this.L, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onResume() {
        super.onResume();
        nj5.d.a(this.L, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
        nj5.d.a(CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public final jl5 q() {
        return this.F;
    }

    @DexIgnore
    public final LinkedList<r87<String, Long>> r() {
        Calendar instance = Calendar.getInstance();
        LinkedList<r87<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 31; i2++) {
            ee7.a((Object) instance, "calendar");
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(5, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e2 = e();
            local.d(e2, "getListDay - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final ArrayList<pl5> s() {
        return this.G;
    }

    @DexIgnore
    public final LinkedList<r87<String, Long>> t() {
        Calendar instance = Calendar.getInstance();
        LinkedList<r87<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 12; i2++) {
            ee7.a((Object) instance, "calendar");
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(2, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e2 = e();
            local.d(e2, "getListMonth - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final LinkedList<r87<String, Long>> u() {
        Calendar instance = Calendar.getInstance();
        LinkedList<r87<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 52; i2++) {
            ee7.a((Object) instance, "calendar");
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            linkedList.add(b(time));
            instance.add(5, -7);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e2 = e();
            local.d(e2, "getListWeek - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final FirmwareFileRepository v() {
        FirmwareFileRepository firmwareFileRepository = this.A;
        if (firmwareFileRepository != null) {
            return firmwareFileRepository;
        }
        ee7.d("mFirmwareFileRepository");
        throw null;
    }

    @DexIgnore
    public final GuestApiService w() {
        GuestApiService guestApiService = this.B;
        if (guestApiService != null) {
            return guestApiService;
        }
        ee7.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final HeartRateMode x() {
        HeartRateMode heartRateMode = this.J;
        if (heartRateMode != null) {
            return heartRateMode;
        }
        ee7.d("mHeartRateMode");
        throw null;
    }

    @DexIgnore
    public final boolean y() {
        return this.K;
    }

    @DexIgnore
    public final sj5 z() {
        sj5 sj5 = this.D;
        if (sj5 != null) {
            return sj5;
        }
        ee7.d("mShakeFeedbackService");
        throw null;
    }

    @DexIgnore
    public final r87<String, Long> b(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        instance.set(7, instance.getFirstDayOfWeek());
        int i2 = instance.get(5);
        instance.set(7, instance.getFirstDayOfWeek() + 6);
        int i3 = instance.get(5);
        StringBuilder sb = new StringBuilder();
        sb.append(i2);
        sb.append('-');
        sb.append(i3);
        return new r87<>(sb.toString(), Long.valueOf(date.getTime()));
    }

    @DexIgnore
    public final void a(HeartRateMode heartRateMode) {
        ee7.b(heartRateMode, "<set-?>");
        this.J = heartRateMode;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r10, com.fossil.fb7<? super java.util.List<com.portfolio.platform.data.model.DebugFirmwareData>> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.portfolio.platform.ui.debug.DebugActivity.c
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.portfolio.platform.ui.debug.DebugActivity$c r0 = (com.portfolio.platform.ui.debug.DebugActivity.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.ui.debug.DebugActivity$c r0 = new com.portfolio.platform.ui.debug.DebugActivity$c
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L_0x003f
            if (r2 != r4) goto L_0x0037
            java.lang.Object r10 = r0.L$2
            java.util.ArrayList r10 = (java.util.ArrayList) r10
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.ui.debug.DebugActivity r0 = (com.portfolio.platform.ui.debug.DebugActivity) r0
            com.fossil.t87.a(r11)
            goto L_0x006a
        L_0x0037:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003f:
            com.fossil.t87.a(r11)
            int r11 = r10.length()
            if (r11 <= 0) goto L_0x004a
            r11 = 1
            goto L_0x004b
        L_0x004a:
            r11 = 0
        L_0x004b:
            if (r11 == 0) goto L_0x0119
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            com.portfolio.platform.ui.debug.DebugActivity$d r2 = new com.portfolio.platform.ui.debug.DebugActivity$d
            r2.<init>(r9, r10, r5)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x0066
            return r1
        L_0x0066:
            r0 = r9
            r8 = r11
            r11 = r10
            r10 = r8
        L_0x006a:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r0.e()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = ".loadFirmware(), response"
            r4.append(r6)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            boolean r1 = r11 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x00de
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r11 = r11.a()
            com.portfolio.platform.data.source.remote.ApiResponse r11 = (com.portfolio.platform.data.source.remote.ApiResponse) r11
            if (r11 == 0) goto L_0x009d
            java.util.List r11 = r11.get_items()
            goto L_0x009e
        L_0x009d:
            r11 = r5
        L_0x009e:
            if (r11 == 0) goto L_0x0118
            java.util.Iterator r11 = r11.iterator()
        L_0x00a4:
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L_0x0118
            java.lang.Object r1 = r11.next()
            com.portfolio.platform.data.model.Firmware r1 = (com.portfolio.platform.data.model.Firmware) r1
            com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r2 = r0.A
            if (r2 == 0) goto L_0x00d8
            java.lang.String r4 = r1.getVersionNumber()
            java.lang.String r6 = "it.versionNumber"
            com.fossil.ee7.a(r4, r6)
            java.lang.String r6 = r1.getChecksum()
            java.lang.String r7 = "it.checksum"
            com.fossil.ee7.a(r6, r7)
            boolean r2 = r2.isDownloaded(r4, r6)
            if (r2 == 0) goto L_0x00ce
            r2 = 2
            goto L_0x00cf
        L_0x00ce:
            r2 = 0
        L_0x00cf:
            com.portfolio.platform.data.model.DebugFirmwareData r4 = new com.portfolio.platform.data.model.DebugFirmwareData
            r4.<init>(r1, r2)
            r10.add(r4)
            goto L_0x00a4
        L_0x00d8:
            java.lang.String r10 = "mFirmwareFileRepository"
            com.fossil.ee7.d(r10)
            throw r5
        L_0x00de:
            boolean r1 = r11 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x0118
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.e()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ".loadFirmware(), fail with code="
            r2.append(r3)
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r3 = r11.a()
            r2.append(r3)
            java.lang.String r3 = ", message="
            r2.append(r3)
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x010e
            java.lang.String r5 = r11.getMessage()
        L_0x010e:
            r2.append(r5)
            java.lang.String r11 = r2.toString()
            r1.d(r0, r11)
        L_0x0118:
            return r10
        L_0x0119:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = r9.e()
            java.lang.String r0 = ".loadFirmware(), device model is empty. Return empty list"
            r10.d(r11, r0)
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.debug.DebugActivity.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.jl5.b
    public void b(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        ee7.b(str, "tagName");
        if (str.hashCode() == 227289531 && str.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
            FirmwareFileRepository firmwareFileRepository = this.A;
            if (firmwareFileRepository != null) {
                String versionNumber = debugFirmwareData.getFirmware().getVersionNumber();
                ee7.a((Object) versionNumber, "it.firmware.versionNumber");
                String checksum = debugFirmwareData.getFirmware().getChecksum();
                ee7.a((Object) checksum, "it.firmware.checksum");
                if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                    c(debugFirmwareData.getFirmware());
                } else {
                    FLogger.INSTANCE.getLocal().d(e(), "Firmware hasn't been downloaded. Could not be set as latest firmware.");
                }
            } else {
                ee7.d("mFirmwareFileRepository");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void b(Firmware firmware) {
        i0.a aVar = new i0.a(this);
        aVar.b("Confirm OTA");
        aVar.a("Are you sure you want OTA to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new o(this, firmware));
        aVar.a("Cancel", p.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v44, resolved type: com.portfolio.platform.data.model.DebugForceBackgroundRequestData */
    /* JADX DEBUG: Multi-variable search result rejected for r0v47, resolved type: com.portfolio.platform.data.model.DebugForceBackgroundRequestData */
    /* JADX DEBUG: Multi-variable search result rejected for r0v84, resolved type: com.portfolio.platform.data.model.DebugForceBackgroundRequestData */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.jl5.b
    public void a(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        ee7.b(str, "tagName");
        Object obj2 = null;
        switch (str.hashCode()) {
            case -2017027426:
                if (str.equals("BLUETOOTH SETTING")) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.BLUETOOTH_SETTINGS");
                    startActivity(intent);
                    return;
                }
                return;
            case -1935069302:
                if (str.equals("WEATHER_WATCH_APP_TAP")) {
                    PortfolioApp.a aVar = PortfolioApp.g0;
                    aVar.a(new jc5(aVar.c().c(), cb0.WEATHER_WATCH_APP.ordinal(), new Bundle()));
                    return;
                }
                return;
            case -1822588397:
                if (str.equals("TRIGGER LOW BATTERY EVENT")) {
                    i0.a aVar2 = new i0.a(this);
                    EditText editText = new EditText(this);
                    editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    editText.setInputType(2);
                    aVar2.b(editText);
                    aVar2.b("Custom dialog");
                    aVar2.a("Enter battery level between 1-100");
                    aVar2.b("Done", new k(this, editText));
                    i0 a2 = aVar2.a();
                    ee7.a((Object) a2, "dialogBuilder.create()");
                    a2.show();
                    Button b2 = a2.b(-1);
                    ee7.a((Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                    b2.setEnabled(false);
                    editText.addTextChangedListener(new l(a2));
                    return;
                }
                return;
            case -1367489317:
                if (str.equals("SKIP OTA")) {
                    ch5 ch5 = this.y;
                    if (ch5 != null) {
                        Boolean valueOf = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf != null) {
                            ch5.w(valueOf.booleanValue());
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1276486892:
                if (str.equals("DISABLE AUTO SYNC")) {
                    ch5 ch52 = this.y;
                    if (ch52 != null) {
                        Boolean valueOf2 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf2 != null) {
                            ch52.b(!valueOf2.booleanValue());
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -907588345:
                if (str.equals("CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                    ch5 ch53 = this.y;
                    if (ch53 != null) {
                        Boolean valueOf3 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf3 != null) {
                            ch53.c(valueOf3.booleanValue());
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -833549689:
                if (str.equals("RESET_DELAY_OTA_TIMESTAMP")) {
                    ch5 ch54 = this.y;
                    if (ch54 != null) {
                        ch54.a(PortfolioApp.g0.c().c(), -1);
                        return;
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -558521180:
                if (str.equals("FRONT LIGHT ENABLE") && bundle != null && bundle.containsKey("DEBUG_BUNDLE_IS_CHECKED")) {
                    boolean z2 = bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false);
                    o();
                    if (PortfolioApp.g0.c().d(PortfolioApp.g0.c().c(), z2) == ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD()) {
                        g();
                        return;
                    }
                    return;
                }
                return;
            case -276828739:
                if (str.equals("CLEAR DATA")) {
                    cy6.f fVar = new cy6.f(2131558479);
                    fVar.a(2131362358, "ARE YOU SURE?");
                    fVar.a(2131362357, "** This will NOT happen on production **");
                    fVar.a(2131362260, "OK");
                    fVar.a(2131362260);
                    cy6 a3 = fVar.a("CONFIRM_CLEAR_DATA");
                    ee7.a((Object) a3, "AlertDialogFragment.Buil\u2026Utils.CONFIRM_CLEAR_DATA)");
                    a3.setCancelable(true);
                    a3.setStyle(0, 2131951933);
                    a3.show(getSupportFragmentManager(), "CONFIRM_CLEAR_DATA");
                    return;
                }
                return;
            case -75588064:
                if (str.equals("GENERATE PRESET DATA")) {
                    r();
                    u();
                    t();
                    return;
                }
                return;
            case -62411653:
                if (str.equals("SHOW ALL DEVICES")) {
                    ch5 ch55 = this.y;
                    if (ch55 != null) {
                        Boolean valueOf4 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf4 != null) {
                            ch55.v(valueOf4.booleanValue());
                            be5.o.e().a();
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.d("mSharedPreferencesManager");
                    throw null;
                }
                return;
            case 2560667:
                if (str.equals("SYNC")) {
                    PortfolioApp c2 = PortfolioApp.g0.c();
                    ad5 ad5 = this.E;
                    if (ad5 != null) {
                        c2.a(ad5, false, 12);
                        return;
                    } else {
                        ee7.d("mDeviceSettingFactory");
                        throw null;
                    }
                } else {
                    return;
                }
            case 227289531:
                if (str.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
                    if (debugFirmwareData.getState() == 2) {
                        b(debugFirmwareData.getFirmware());
                        return;
                    } else if (debugFirmwareData.getState() == 0) {
                        a(debugFirmwareData);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 334768719:
                if (str.equals("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT")) {
                    PortfolioApp.g0.c().O();
                    return;
                }
                return;
            case 345524076:
                if (str.equals("CUSTOMIZE THEME")) {
                    ThemesActivity.y.a(this);
                    return;
                }
                return;
            case 437897810:
                if (str.equals("FORCE_BACKGROUND_REQUEST") && bundle != null && bundle.containsKey("DEBUG_BUNDLE_SPINNER_SELECTED_POS")) {
                    int i4 = bundle.getInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS");
                    List b3 = xe7.b(obj);
                    Object obj3 = b3 != null ? b3.get(i4) : null;
                    if (obj3 instanceof DebugForceBackgroundRequestData) {
                        obj2 = obj3;
                    }
                    DebugForceBackgroundRequestData debugForceBackgroundRequestData = (DebugForceBackgroundRequestData) obj2;
                    if (debugForceBackgroundRequestData != null) {
                        PortfolioApp.g0.c().a(PortfolioApp.g0.c().c(), new ForceBackgroundRequest(debugForceBackgroundRequestData.getBackgroundRequestType()));
                        return;
                    }
                    return;
                }
                return;
            case 596314607:
                if (str.equals("DISABLE HW_LOG SYNC")) {
                    ch5 ch56 = this.y;
                    if (ch56 != null) {
                        Boolean valueOf5 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf5 != null) {
                            ch56.i(!valueOf5.booleanValue());
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 967694544:
                if (str.equals("SIMULATE DISCONNECTION")) {
                    i0.a aVar3 = new i0.a(this);
                    Object systemService = getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558811, (ViewGroup) null);
                        aVar3.b(inflate);
                        i0 a4 = aVar3.a();
                        ee7.a((Object) a4, "builder.create()");
                        a4.show();
                        EditText editText2 = (EditText) inflate.findViewById(2131362220);
                        EditText editText3 = (EditText) inflate.findViewById(2131362222);
                        EditText editText4 = (EditText) inflate.findViewById(2131362232);
                        EditText editText5 = (EditText) inflate.findViewById(2131362221);
                        ee7.a((Object) editText2, "etDelay");
                        double d2 = (double) 0;
                        double d3 = (double) 65535;
                        editText2.setFilters(new InputFilter[]{new b(d2, d3)});
                        ee7.a((Object) editText3, "etDuration");
                        editText3.setFilters(new InputFilter[]{new b(d2, d3)});
                        ee7.a((Object) editText4, "etRepeat");
                        editText4.setFilters(new InputFilter[]{new b(d2, d3)});
                        ee7.a((Object) editText5, "etDelayEachTime");
                        editText5.setFilters(new InputFilter[]{new b(d2, (double) 4294967)});
                        ((FlexibleButton) inflate.findViewById(2131361966)).setOnClickListener(new j(this, editText2, editText3, editText4, editText5, a4));
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type android.view.LayoutInflater");
                }
                return;
            case 999402770:
                if (str.equals("WATCH APP MUSIC CONTROL")) {
                    finish();
                    return;
                }
                return;
            case 1053818587:
                if (str.equals("APPLY NEW NOTIFICATION FILTER")) {
                    ch5 ch57 = this.y;
                    if (ch57 != null) {
                        Boolean valueOf6 = bundle != null ? Boolean.valueOf(bundle.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf6 != null) {
                            ch57.q(valueOf6.booleanValue());
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1484036056:
                if (str.equals("RESET UAPP LOG FILES")) {
                    MicroAppEventLogger.resetLogFiles();
                    return;
                }
                return;
            case 1642625733:
                if (str.equals("GENERATE HEART RATE DATA")) {
                    finish();
                    return;
                }
                return;
            case 1977879337:
                if (str.equals("VIEW LOG")) {
                    LogcatActivity.a((Context) this);
                    return;
                }
                return;
            case 2029483660:
                if (str.equals("SEND LOG")) {
                    ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new i(this, null), 3, null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final r87<String, Long> a(Date date) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        long timeInMillis = instance.getTimeInMillis();
        switch (instance.get(7)) {
            case 1:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886719), Long.valueOf(timeInMillis));
            case 2:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886718), Long.valueOf(timeInMillis));
            case 3:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886721), Long.valueOf(timeInMillis));
            case 4:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886723), Long.valueOf(timeInMillis));
            case 5:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886722), Long.valueOf(timeInMillis));
            case 6:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886717), Long.valueOf(timeInMillis));
            case 7:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886720), Long.valueOf(timeInMillis));
            default:
                return new r87<>(ig5.a(PortfolioApp.g0.c(), 2131886719), Long.valueOf(timeInMillis));
        }
    }

    @DexIgnore
    public final void a(DebugFirmwareData debugFirmwareData) {
        i0.a aVar = new i0.a(this);
        Firmware firmware = debugFirmwareData.getFirmware();
        aVar.b("Confirm Download");
        aVar.a("Are you sure you want download to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new m(firmware, this, debugFirmwareData));
        aVar.a("Cancel", n.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        String c2 = PortfolioApp.g0.c().c();
        FirmwareData createFirmwareData = FirmwareFactory.getInstance().createFirmwareData(firmware.getVersionNumber(), firmware.getDeviceModel(), firmware.getChecksum());
        ee7.a((Object) createFirmwareData, "FirmwareFactory.getInsta\u2026Model, firmware.checksum)");
        om5.b bVar = new om5.b(c2, createFirmwareData);
        om5 om5 = this.z;
        if (om5 != null) {
            om5.a(bVar, (fl4.e) null);
        } else {
            ee7.d("mOTAToSpecificFirmwareUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (ee7.a((Object) str, (Object) "CONFIRM_CLEAR_DATA") && i2 == 2131362260) {
            DebugClearDataWarningActivity.a(this);
        } else if (ee7.a((Object) str, (Object) "SWITCH HEART RATE MODE") && i2 == 2131362260 && intent != null && intent.hasExtra("EXTRA_RADIO_GROUPS_RESULTS")) {
            HashMap hashMap = (HashMap) intent.getSerializableExtra("EXTRA_RADIO_GROUPS_RESULTS");
        }
    }
}
