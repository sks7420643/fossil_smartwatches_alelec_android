package com.portfolio.platform.helper;

import com.fossil.ee4;
import com.fossil.fe4;
import com.fossil.je4;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GsonConverterShortDateTime implements fe4<DateTime> {
    @DexIgnore
    @Override // com.fossil.fe4
    public DateTime deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        String f = jsonElement.f();
        if (f.isEmpty()) {
            return null;
        }
        return zd5.b(f);
    }
}
