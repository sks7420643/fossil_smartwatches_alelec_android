package com.portfolio.platform.helper;

import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.le4;
import com.fossil.me4;
import com.fossil.ne4;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDateTime implements fe4<DateTime>, ne4<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(DateTime dateTime, Type type, me4 me4) {
        String str;
        ee7.b(type, "typeOfSrc");
        ee7.b(me4, "context");
        if (dateTime == null) {
            str = "";
        } else {
            str = zd5.a(DateTimeZone.UTC, dateTime);
            ee7.a((Object) str, "DateHelper.printServerDa\u2026at(DateTimeZone.UTC, src)");
        }
        return new le4(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0039 */
    @Override // com.fossil.fe4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.joda.time.DateTime deserialize(com.google.gson.JsonElement r5, java.lang.reflect.Type r6, com.fossil.ee4 r7) {
        /*
            r4 = this;
            java.lang.String r0 = "json"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = "typeOfT"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r6 = "context"
            com.fossil.ee7.b(r7, r6)
            java.lang.String r6 = r5.f()
            java.lang.String r7 = "dateAsString"
            com.fossil.ee7.a(r6, r7)
            int r7 = r6.length()
            if (r7 != 0) goto L_0x0020
            r7 = 1
            goto L_0x0021
        L_0x0020:
            r7 = 0
        L_0x0021:
            r0 = 0
            if (r7 == 0) goto L_0x002b
            org.joda.time.DateTime r5 = new org.joda.time.DateTime
            r5.<init>(r0)
            return r5
        L_0x002b:
            org.joda.time.DateTimeZone r7 = org.joda.time.DateTimeZone.getDefault()     // Catch:{ Exception -> 0x0039 }
            org.joda.time.DateTime r6 = com.fossil.zd5.a(r7, r6)     // Catch:{ Exception -> 0x0039 }
            java.lang.String r7 = "DateHelper.getServerDate\u2026tDefault(), dateAsString)"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x0039 }
            goto L_0x0079
        L_0x0039:
            java.lang.String r6 = r5.f()     // Catch:{ Exception -> 0x0047 }
            org.joda.time.DateTime r6 = com.fossil.zd5.b(r6)     // Catch:{ Exception -> 0x0047 }
            java.lang.String r7 = "DateHelper.getLocalDateT\u2026DateFormat(json.asString)"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x0047 }
            goto L_0x0079
        L_0x0047:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "deserialize - json="
            r2.append(r3)
            java.lang.String r5 = r5.f()
            r2.append(r5)
            java.lang.String r5 = ", e="
            r2.append(r5)
            r2.append(r6)
            java.lang.String r5 = r2.toString()
            java.lang.String r2 = "GsonConvertDateTime"
            r7.e(r2, r5)
            r6.printStackTrace()
            org.joda.time.DateTime r5 = new org.joda.time.DateTime
            r5.<init>(r0)
            r6 = r5
        L_0x0079:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.GsonConvertDateTime.deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.fossil.ee4):org.joda.time.DateTime");
    }
}
