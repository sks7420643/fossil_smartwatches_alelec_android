package com.portfolio.platform.helper;

import com.fossil.fe4;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDate implements fe4<Date> {
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057  */
    @Override // com.fossil.fe4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Date deserialize(com.google.gson.JsonElement r5, java.lang.reflect.Type r6, com.fossil.ee4 r7) {
        /*
            r4 = this;
            java.lang.String r0 = "json"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = "typeOfT"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r6 = "context"
            com.fossil.ee7.b(r7, r6)
            java.lang.String r6 = r5.f()
            java.lang.String r7 = "dateAsString"
            com.fossil.ee7.a(r6, r7)
            int r7 = r6.length()
            if (r7 != 0) goto L_0x0020
            r7 = 1
            goto L_0x0021
        L_0x0020:
            r7 = 0
        L_0x0021:
            r0 = 0
            if (r7 == 0) goto L_0x002b
            java.util.Date r5 = new java.util.Date
            r5.<init>(r0)
            return r5
        L_0x002b:
            org.joda.time.DateTimeZone r7 = org.joda.time.DateTimeZone.getDefault()     // Catch:{ Exception -> 0x003d }
            org.joda.time.DateTime r6 = com.fossil.zd5.a(r7, r6)     // Catch:{ Exception -> 0x003d }
            java.util.Date r6 = r6.toDate()     // Catch:{ Exception -> 0x003d }
            java.lang.String r7 = "DateHelper.getServerDate\u2026), dateAsString).toDate()"
            com.fossil.ee7.a(r6, r7)     // Catch:{ Exception -> 0x003d }
            goto L_0x008d
        L_0x003d:
            java.lang.ThreadLocal<java.text.SimpleDateFormat> r6 = com.fossil.zd5.a     // Catch:{ Exception -> 0x005b }
            java.lang.Object r6 = r6.get()     // Catch:{ Exception -> 0x005b }
            r7 = 0
            if (r6 == 0) goto L_0x0057
            java.text.SimpleDateFormat r6 = (java.text.SimpleDateFormat) r6     // Catch:{ Exception -> 0x005b }
            java.lang.String r2 = r5.f()     // Catch:{ Exception -> 0x005b }
            java.util.Date r6 = r6.parse(r2)     // Catch:{ Exception -> 0x005b }
            if (r6 == 0) goto L_0x0053
            goto L_0x008d
        L_0x0053:
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x005b }
            throw r7
        L_0x0057:
            com.fossil.ee7.a()
            throw r7
        L_0x005b:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "deserialize - json="
            r2.append(r3)
            java.lang.String r5 = r5.f()
            r2.append(r5)
            java.lang.String r5 = ", e="
            r2.append(r5)
            r2.append(r6)
            java.lang.String r5 = r2.toString()
            java.lang.String r2 = "GsonConvertDate"
            r7.e(r2, r5)
            r6.printStackTrace()
            java.util.Date r5 = new java.util.Date
            r5.<init>(r0)
            r6 = r5
        L_0x008d:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.helper.GsonConvertDate.deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.fossil.ee4):java.util.Date");
    }
}
