package com.portfolio.platform.helper;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.i97;
import com.fossil.le4;
import com.fossil.me4;
import com.fossil.ne4;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConverterShortDate implements fe4<Date>, ne4<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, me4 me4) {
        String str;
        ee7.b(type, "typeOfSrc");
        ee7.b(me4, "context");
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = zd5.a.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                ee7.a((Object) str, "DateHelper.SHORT_DATE_FO\u2026ATTER.get()!!.format(src)");
            } else {
                ee7.a();
                throw null;
            }
        }
        return new le4(str);
    }

    @DexIgnore
    @Override // com.fossil.fe4
    public Date deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        ee7.b(jsonElement, "json");
        ee7.b(type, "typeOfT");
        ee7.b(ee4, "context");
        String f = jsonElement.f();
        if (f != null) {
            try {
                SimpleDateFormat simpleDateFormat = zd5.a.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    if (parse != null) {
                        return parse;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(f);
                sb.append(", e=");
                e.printStackTrace();
                sb.append(i97.a);
                local.e("GsonConverterShortDate", sb.toString());
            }
        }
        return new Date(0);
    }
}
