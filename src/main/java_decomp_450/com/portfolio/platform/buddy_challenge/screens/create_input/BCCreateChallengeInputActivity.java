package com.portfolio.platform.buddy_challenge.screens.create_input;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ip4;
import com.fossil.qn4;
import com.fossil.sn4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateChallengeInputActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, sn4 sn4) {
            ee7.b(fragment, "fragment");
            ee7.b(sn4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_template_extra", sn4);
            fragment.startActivityForResult(intent, 11);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, qn4 qn4) {
            ee7.b(fragment, "fragment");
            ee7.b(qn4, "draft");
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_draft_extra", qn4);
            fragment.startActivityForResult(intent, 14);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((ip4) getSupportFragmentManager().b(2131362149)) == null) {
            ip4 a2 = ip4.s.a((sn4) getIntent().getParcelableExtra("challenge_template_extra"), (qn4) getIntent().getParcelableExtra("challenge_draft_extra"));
            if (a2 != null) {
                a(a2, ip4.s.a(), 2131362149);
                i97 i97 = i97.a;
                return;
            }
            ee7.a();
            throw null;
        }
    }
}
