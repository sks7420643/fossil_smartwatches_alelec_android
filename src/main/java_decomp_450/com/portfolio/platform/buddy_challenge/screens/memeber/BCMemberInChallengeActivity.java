package com.portfolio.platform.buddy_challenge.screens.memeber;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.cl5;
import com.fossil.ee7;
import com.fossil.qq4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCMemberInChallengeActivity extends cl5 {
    @DexIgnore
    public static /* final */ a y; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, boolean z) {
            ee7.b(fragment, "fragment");
            ee7.b(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCMemberInChallengeActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_status_extra", z);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.fossil.cl5
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((qq4) getSupportFragmentManager().b(2131362149)) == null) {
            a(qq4.s.a(getIntent().getStringExtra("challenge_id_extra"), getIntent().getBooleanExtra("challenge_status_extra", true)), qq4.s.a(), 2131362149);
        }
    }
}
