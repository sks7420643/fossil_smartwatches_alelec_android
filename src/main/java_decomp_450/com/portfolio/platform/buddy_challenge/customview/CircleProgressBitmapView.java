package com.portfolio.platform.buddy_challenge.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.vt4;
import com.fossil.x87;
import com.fossil.zd7;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CircleProgressBitmapView extends View implements zt4 {
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public /* final */ short a; // = -90;
    @DexIgnore
    public /* final */ PorterDuffXfermode b; // = new PorterDuffXfermode(PorterDuff.Mode.DST_OUT);
    @DexIgnore
    public /* final */ PorterDuffXfermode c; // = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
    @DexIgnore
    public /* final */ SparseArray<WeakReference<Bitmap>> d; // = new SparseArray<>();
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Bitmap g;
    @DexIgnore
    public Bitmap h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    @Keep
    public float lackingAngle; // = -360.0f;
    @DexIgnore
    public /* final */ Rect p; // = new Rect();
    @DexIgnore
    public /* final */ Rect q; // = new Rect();
    @DexIgnore
    public /* final */ RectF r; // = new RectF();
    @DexIgnore
    public Paint s;
    @DexIgnore
    public int t;
    @DexIgnore
    public long u;
    @DexIgnore
    public long v;
    @DexIgnore
    public long w;
    @DexIgnore
    public CountDownTimer x;
    @DexIgnore
    public TimerViewObserver y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ CircleProgressBitmapView a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(CircleProgressBitmapView circleProgressBitmapView, long j, long j2, long j3, long j4) {
            super(j3, j4);
            this.a = circleProgressBitmapView;
            this.b = j;
        }

        @DexIgnore
        public void onFinish() {
            this.a.e(-1, 0);
        }

        @DexIgnore
        public void onTick(long j) {
            this.a.e(j, this.b);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = CircleProgressBitmapView.class.getSimpleName();
        ee7.a((Object) simpleName, "CircleProgressBitmapView::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context) {
        super(context);
        ee7.b(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        Paint paint = new Paint(1);
        this.s = paint;
        if (paint != null) {
            paint.setFilterBitmap(true);
            Paint paint2 = this.s;
            if (paint2 != null) {
                paint2.setColor(-16777216);
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CircleProgressView);
                int resourceId = obtainStyledAttributes.getResourceId(10, 2131231031);
                this.e = resourceId;
                this.g = a(context, resourceId);
                int resourceId2 = obtainStyledAttributes.getResourceId(1, 2131231031);
                this.f = resourceId2;
                this.h = a(context, resourceId2);
                obtainStyledAttributes.recycle();
                return;
            }
            ee7.d("paint");
            throw null;
        }
        ee7.d("paint");
        throw null;
    }

    @DexIgnore
    public final void b(long j2, long j3) {
        CountDownTimer countDownTimer = this.x;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        b bVar = new b(this, j2, j3, j3, this.w);
        this.x = bVar;
        if (bVar != null) {
            bVar.start();
        }
    }

    @DexIgnore
    public final float c(long j2, long j3) {
        return ((float) -360) * (((float) 1) - ((j2 <= 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : (j3 <= 0 || j2 >= j3) ? 100.0f : (((float) 100) * ((float) j2)) / ((float) j3)) / 100.0f));
    }

    @DexIgnore
    public final void d(long j2, long j3) {
        this.u = j2;
        this.v = j3;
        this.w = j2 <= 3600000 ? ButtonService.CONNECT_TIMEOUT : 30000;
        b(j2, this.v - vt4.a.b());
    }

    @DexIgnore
    public final void e(long j2, long j3) {
        if (j2 < 0) {
            j2 = 0;
        }
        if (j3 < 0) {
            j3 = 1000;
        }
        this.lackingAngle = c(j2, j3);
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.onDraw(canvas);
        if (this.j > 0) {
            int saveLayer = canvas.saveLayer(this.r, null, 31);
            Rect rect = this.p;
            Bitmap bitmap = this.g;
            if (bitmap != null) {
                int width = bitmap.getWidth();
                Bitmap bitmap2 = this.g;
                if (bitmap2 != null) {
                    rect.set(0, 0, width, bitmap2.getHeight());
                    Bitmap bitmap3 = this.g;
                    if (bitmap3 != null) {
                        Rect rect2 = this.p;
                        Rect rect3 = this.q;
                        Paint paint = this.s;
                        if (paint != null) {
                            canvas.drawBitmap(bitmap3, rect2, rect3, paint);
                            Paint paint2 = this.s;
                            if (paint2 != null) {
                                paint2.setColorFilter(null);
                                Paint paint3 = this.s;
                                if (paint3 != null) {
                                    paint3.setXfermode(this.b);
                                    RectF rectF = this.r;
                                    float f2 = (float) this.a;
                                    float f3 = this.lackingAngle;
                                    Paint paint4 = this.s;
                                    if (paint4 != null) {
                                        canvas.drawArc(rectF, f2, f3, true, paint4);
                                        Paint paint5 = this.s;
                                        if (paint5 != null) {
                                            paint5.setXfermode(this.c);
                                            Rect rect4 = this.p;
                                            Bitmap bitmap4 = this.h;
                                            if (bitmap4 != null) {
                                                int width2 = bitmap4.getWidth();
                                                Bitmap bitmap5 = this.h;
                                                if (bitmap5 != null) {
                                                    rect4.set(0, 0, width2, bitmap5.getHeight());
                                                    Bitmap bitmap6 = this.h;
                                                    if (bitmap6 != null) {
                                                        Rect rect5 = this.p;
                                                        Rect rect6 = this.q;
                                                        Paint paint6 = this.s;
                                                        if (paint6 != null) {
                                                            canvas.drawBitmap(bitmap6, rect5, rect6, paint6);
                                                            Paint paint7 = this.s;
                                                            if (paint7 != null) {
                                                                paint7.setXfermode(null);
                                                                canvas.restoreToCount(saveLayer);
                                                                return;
                                                            }
                                                            ee7.d("paint");
                                                            throw null;
                                                        }
                                                        ee7.d("paint");
                                                        throw null;
                                                    }
                                                    ee7.a();
                                                    throw null;
                                                }
                                                ee7.a();
                                                throw null;
                                            }
                                            ee7.a();
                                            throw null;
                                        }
                                        ee7.d("paint");
                                        throw null;
                                    }
                                    ee7.d("paint");
                                    throw null;
                                }
                                ee7.d("paint");
                                throw null;
                            }
                            ee7.d("paint");
                            throw null;
                        }
                        ee7.d("paint");
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onPause() {
        FLogger.INSTANCE.getLocal().e(z, "onPause");
        CountDownTimer countDownTimer = this.x;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onResume() {
        FLogger.INSTANCE.getLocal().e(z, "onResume");
        b(this.u, this.v - vt4.a.b());
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        int i6 = this.i * 2;
        if (i2 <= i6 || i3 <= i6) {
            this.j = 0;
            return;
        }
        this.j = i3 - i6;
        int width = getWidth();
        int i7 = this.j;
        int i8 = (width - i7) / 2;
        Rect rect = this.q;
        int i9 = this.i;
        rect.set(i8, i9, i8 + i7, i7 + i9);
        int i10 = this.i;
        int i11 = this.j;
        this.r.set((float) i8, (float) i10, (float) (i8 + i11), (float) (i10 + i11));
    }

    @DexIgnore
    public final void setBackgroundDrawable(int i2) {
        this.f = i2;
        Context context = getContext();
        ee7.a((Object) context, "context");
        this.h = a(context, this.f);
        invalidate();
    }

    @DexIgnore
    public final void setColour(int i2) {
        this.t = i2;
        new PorterDuffColorFilter(this.t, PorterDuff.Mode.SRC_IN);
        invalidate();
    }

    @DexIgnore
    public final void setObserver(TimerViewObserver timerViewObserver) {
        if (this.y == null) {
            this.y = timerViewObserver;
            if (timerViewObserver != null) {
                timerViewObserver.a(this, a());
            }
        }
    }

    @DexIgnore
    public final void setProgressDrawable(int i2) {
        this.e = i2;
        Context context = getContext();
        ee7.a((Object) context, "context");
        this.g = a(context, this.e);
        invalidate();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        a(context, attributeSet);
    }

    @DexIgnore
    public final Bitmap a(Context context, int i2) {
        WeakReference<Bitmap> weakReference = this.d.get(i2);
        if ((weakReference != null ? weakReference.get() : null) == null) {
            Drawable c2 = v6.c(context, i2);
            if (c2 != null) {
                weakReference = new WeakReference<>(((BitmapDrawable) c2).getBitmap());
                this.d.put(i2, weakReference);
            } else {
                throw new x87("null cannot be cast to non-null type android.graphics.drawable.BitmapDrawable");
            }
        }
        return weakReference.get();
    }

    @DexIgnore
    public final int a() {
        return System.identityHashCode(this);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressBitmapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        a(context, attributeSet);
    }
}
