package com.portfolio.platform.buddy_challenge.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.an4;
import com.fossil.ee7;
import com.fossil.en4;
import com.fossil.fn4;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.vt4;
import com.fossil.ym4;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CircleProgressView extends View implements en4, fn4 {
    @DexIgnore
    public float A;
    @DexIgnore
    public ym4 B;
    @DexIgnore
    public float C;
    @DexIgnore
    public int D;
    @DexIgnore
    public TimerViewObserver E;
    @DexIgnore
    public CountDownTimer F;
    @DexIgnore
    public int G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public String K;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Path b; // = new Path();
    @DexIgnore
    public /* final */ Paint c; // = new Paint(1);
    @DexIgnore
    public /* final */ Path d; // = new Path();
    @DexIgnore
    public /* final */ Paint e; // = new Paint(1);
    @DexIgnore
    public /* final */ Path f; // = new Path();
    @DexIgnore
    public /* final */ Paint g; // = new Paint(1);
    @DexIgnore
    public float h; // = 0.5f;
    @DexIgnore
    public float i; // = 60.0f;
    @DexIgnore
    public float j; // = 2.0f;
    @DexIgnore
    public float p; // = 270.0f;
    @DexIgnore
    public float q; // = 40.0f;
    @DexIgnore
    public /* final */ float r; // = 360.0f;
    @DexIgnore
    public /* final */ RectF s; // = new RectF();
    @DexIgnore
    public boolean t; // = true;
    @DexIgnore
    public int u; // = v6.a(getContext(), 2131099701);
    @DexIgnore
    public int v; // = v6.a(getContext(), 2131099702);
    @DexIgnore
    public int w; // = v6.a(getContext(), 2131099689);
    @DexIgnore
    public an4 x; // = an4.IDLE;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context) {
        super(context);
        ee7.b(context, "context");
        String simpleName = CircleProgressView.class.getSimpleName();
        ee7.a((Object) simpleName, "CircleProgressView::class.java.simpleName");
        this.a = simpleName;
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        this.B = new ym4(context2, this);
        this.C = 800.0f;
        this.D = 10;
        this.G = System.identityHashCode(this);
        this.K = this.a;
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    public void a(TimerViewObserver timerViewObserver, int i2) {
        fn4.a.a(this, timerViewObserver, i2);
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public void b(long j2, long j3) {
        fn4.a.a(this, j2, j3);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        this.f.reset();
        float f2 = this.p;
        while (f2 < this.p + getCurrentValue()) {
            this.f.addArc(this.s, f2, this.h);
            f2 += this.j;
        }
        if (canvas != null) {
            canvas.drawPath(this.f, this.g);
        }
    }

    @DexIgnore
    public final void d(long j2, long j3) {
        setTotalMillisecond(j2);
        setEndTime(j3);
        setInterval(j2 <= 3600000 ? ButtonService.CONNECT_TIMEOUT : 30000);
        b(j2, getEndTime() - vt4.a.b());
    }

    @DexIgnore
    public final void e(long j2, long j3) {
        float c2 = c(j3, j2);
        if (Math.abs(getCurrentValue() - c2) > 2.0f) {
            Message message = new Message();
            message.what = zm4.SET_VALUE_ANIMATED.ordinal();
            message.obj = new float[]{getCurrentValue(), c2};
            getHandler().sendMessage(message);
            return;
        }
        setCurrentValue(c2);
        invalidate();
    }

    @DexIgnore
    public final void f(long j2, long j3) {
        setCurrentValue(c(j3, j2));
        invalidate();
    }

    @DexIgnore
    @Override // com.fossil.en4
    public float getAnimationDuration() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public an4 getAnimationState() {
        return this.x;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public float getCurrentValue() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public long getEndTime() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public int getFrameDelay() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public float getFrom() {
        return this.y;
    }

    @DexIgnore
    public ym4 getHandler() {
        return this.B;
    }

    @DexIgnore
    public int getIdentity() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public long getInterval() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public String getTag() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public CountDownTimer getTimer() {
        return this.F;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public TimerViewObserver getTimerViewObserver() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public float getTo() {
        return this.z;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public long getTotalMillisecond() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public void onDone() {
        f(0, 1);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
        b(canvas);
        c(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int min = Math.min(View.getDefaultSize(getSuggestedMinimumWidth(), i2), View.getDefaultSize(getSuggestedMinimumHeight(), i3));
        RectF rectF = this.s;
        float f2 = this.q;
        float f3 = (float) min;
        rectF.set(f2, f2, f3 - f2, f3 - f2);
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onPause() {
        fn4.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.zt4
    public void onResume() {
        fn4.a.b(this);
    }

    @DexIgnore
    public void setAnimationDuration(float f2) {
        this.C = f2;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public void setAnimationState(an4 an4) {
        ee7.b(an4, "<set-?>");
        this.x = an4;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public void setCurrentValue(float f2) {
        this.A = f2;
    }

    @DexIgnore
    public void setEndTime(long j2) {
        this.I = j2;
    }

    @DexIgnore
    public void setFrameDelay(int i2) {
        this.D = i2;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public void setFrom(float f2) {
        this.y = f2;
    }

    @DexIgnore
    public void setHandler(ym4 ym4) {
        ee7.b(ym4, "<set-?>");
        this.B = ym4;
    }

    @DexIgnore
    public void setIdentity(int i2) {
        this.G = i2;
    }

    @DexIgnore
    public void setInterval(long j2) {
        this.J = j2;
    }

    @DexIgnore
    public final void setProgressColour(int i2) {
        this.w = i2;
        this.g.setColor(i2);
        invalidate();
    }

    @DexIgnore
    public void setTag(String str) {
        ee7.b(str, "<set-?>");
        this.K = str;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public void setTimer(CountDownTimer countDownTimer) {
        this.F = countDownTimer;
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public void setTimerViewObserver(TimerViewObserver timerViewObserver) {
        this.E = timerViewObserver;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public void setTo(float f2) {
        this.z = f2;
    }

    @DexIgnore
    public void setTotalMillisecond(long j2) {
        this.H = j2;
    }

    @DexIgnore
    @Override // com.fossil.en4
    public void a() {
        invalidate();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        this.d.reset();
        RectF rectF = new RectF();
        RectF rectF2 = this.s;
        float f2 = rectF2.left;
        float f3 = this.i;
        float f4 = (float) 2;
        rectF.set(f2 + (f3 / f4), rectF2.top + (f3 / f4), rectF2.right - (f3 / f4), rectF2.bottom - (f3 / f4));
        if (canvas != null) {
            canvas.drawArc(rectF, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.r, true, this.e);
        }
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CircleProgressView);
        this.h = obtainStyledAttributes.getFloat(3, 0.5f);
        this.i = obtainStyledAttributes.getDimension(2, 60.0f);
        this.t = obtainStyledAttributes.getBoolean(7, true);
        this.q = obtainStyledAttributes.getDimension(8, 40.0f);
        this.u = obtainStyledAttributes.getColor(0, v6.a(context, 2131099701));
        this.w = obtainStyledAttributes.getColor(9, v6.a(context, 2131099689));
        this.v = obtainStyledAttributes.getColor(0, v6.a(context, 2131099702));
        setCurrentValue(obtainStyledAttributes.getFloat(4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        obtainStyledAttributes.recycle();
        this.c.setColor(this.u);
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setStrokeWidth(this.i);
        this.e.setColor(this.v);
        this.e.setStyle(Paint.Style.FILL);
        this.g.setColor(this.w);
        this.g.setStyle(Paint.Style.STROKE);
        this.g.setStrokeWidth(this.i);
    }

    @DexIgnore
    public void b() {
        FLogger.INSTANCE.getLocal().e(this.a, "clean");
        CountDownTimer timer = getTimer();
        if (timer != null) {
            timer.cancel();
        }
        TimerViewObserver timerViewObserver = getTimerViewObserver();
        if (timerViewObserver != null) {
            timerViewObserver.a(getIdentity());
        }
    }

    @DexIgnore
    public final float c(long j2, long j3) {
        return (((float) j3) * this.r) / ((float) j2);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        if (this.t) {
            this.b.reset();
            float f2 = this.p;
            while (f2 < this.p + this.r) {
                this.b.addArc(this.s, f2, this.h);
                f2 += this.j;
            }
            if (canvas != null) {
                canvas.drawPath(this.b, this.c);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        String simpleName = CircleProgressView.class.getSimpleName();
        ee7.a((Object) simpleName, "CircleProgressView::class.java.simpleName");
        this.a = simpleName;
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        this.B = new ym4(context2, this);
        this.C = 800.0f;
        this.D = 10;
        this.G = System.identityHashCode(this);
        this.K = this.a;
        a(context, attributeSet);
    }

    @DexIgnore
    @Override // com.fossil.fn4
    public void a(long j2, long j3) {
        e(j2, j3);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attributeSet");
        String simpleName = CircleProgressView.class.getSimpleName();
        ee7.a((Object) simpleName, "CircleProgressView::class.java.simpleName");
        this.a = simpleName;
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        this.B = new ym4(context2, this);
        this.C = 800.0f;
        this.D = 10;
        this.G = System.identityHashCode(this);
        this.K = this.a;
        a(context, attributeSet);
    }
}
