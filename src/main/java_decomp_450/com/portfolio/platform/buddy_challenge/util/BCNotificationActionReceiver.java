package com.portfolio.platform.buddy_challenge.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.ko4;
import com.fossil.nb7;
import com.fossil.no4;
import com.fossil.pb7;
import com.fossil.qe5;
import com.fossil.qj7;
import com.fossil.ro4;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.um4;
import com.fossil.un4;
import com.fossil.vt4;
import com.fossil.xh7;
import com.fossil.xo4;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCNotificationActionReceiver extends BroadcastReceiver {
    @DexIgnore
    public xo4 a;
    @DexIgnore
    public ro4 b;
    @DexIgnore
    public ch5 c;
    @DexIgnore
    public /* final */ yi7 d; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$1", f = "BCNotificationActionReceiver.kt", l = {47}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BCNotificationActionReceiver bCNotificationActionReceiver, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$friend, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                xo4 b = this.this$0.b();
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(true, un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((ko4) obj).c() != null) {
                um4.a.b("bc_accept_friend_request", PortfolioApp.g0.c().w(), this.$friend.d(), PortfolioApp.g0.c());
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$2", f = "BCNotificationActionReceiver.kt", l = {59}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(BCNotificationActionReceiver bCNotificationActionReceiver, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$friend, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                xo4 b = this.this$0.b();
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(false, un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((ko4) obj).c() != null) {
                um4.a.b("bc_reject_friend_request", PortfolioApp.g0.c().w(), this.$friend.d(), PortfolioApp.g0.c());
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$3", f = "BCNotificationActionReceiver.kt", l = {76}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BCNotificationActionReceiver bCNotificationActionReceiver, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bCNotificationActionReceiver;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Long c = this.this$0.c().c();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = this.this$0.e;
                local.e(a2, "ACTION_SET_SYNC_DATA - endTimeOfChallenge: " + c + " - exact: " + vt4.a.b());
                if (c.longValue() > vt4.a.b()) {
                    ro4 a3 = this.this$0.a();
                    this.L$0 = yi7;
                    this.L$1 = c;
                    this.label = 1;
                    obj = a3.b(this);
                    if (obj == a) {
                        return a;
                    }
                } else {
                    this.this$0.c().a(pb7.a(0L));
                    this.this$0.c().a(pb7.a(false));
                    return i97.a;
                }
            } else if (i == 1) {
                Long l = (Long) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            no4 no4 = (no4) ((ko4) obj).c();
            if (no4 != null) {
                PortfolioApp.g0.c().n(no4.a());
            } else {
                qe5.c.d();
            }
            return i97.a;
        }
    }

    @DexIgnore
    public BCNotificationActionReceiver() {
        String simpleName = BCNotificationActionReceiver.class.getSimpleName();
        ee7.a((Object) simpleName, "BCNotificationActionRece\u2026er::class.java.simpleName");
        this.e = simpleName;
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final xo4 b() {
        xo4 xo4 = this.a;
        if (xo4 != null) {
            return xo4;
        }
        ee7.d("friendRepository");
        throw null;
    }

    @DexIgnore
    public final ch5 c() {
        ch5 ch5 = this.c;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("shared");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        un4 un4 = intent != null ? (un4) intent.getParcelableExtra("friend_extra") : null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != -1294772761) {
                if (hashCode != -1088442688) {
                    if (hashCode == 1236721143 && action.equals("com.buddy_challenge.friend.decline") && un4 != null) {
                        qe5.c.a(PortfolioApp.g0.c(), un4.d().hashCode());
                        ik7 unused = xh7.b(this.d, null, null, new b(this, un4, null), 3, null);
                    }
                } else if (action.equals("com.buddy_challenge.set_sync_data")) {
                    FLogger.INSTANCE.getLocal().e(this.e, "ACTION_SET_SYNC_DATA");
                    qe5.c.b();
                    ik7 unused2 = xh7.b(this.d, null, null, new c(this, null), 3, null);
                }
            } else if (action.equals("com.buddy_challenge.friend.accept") && un4 != null) {
                qe5.c.a(PortfolioApp.g0.c(), un4.d().hashCode());
                ik7 unused3 = xh7.b(this.d, null, null, new a(this, un4, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final ro4 a() {
        ro4 ro4 = this.b;
        if (ro4 != null) {
            return ro4;
        }
        ee7.d("challengeRepository");
        throw null;
    }
}
