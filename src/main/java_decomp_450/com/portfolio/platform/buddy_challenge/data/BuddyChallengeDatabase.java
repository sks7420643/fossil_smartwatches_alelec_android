package com.portfolio.platform.buddy_challenge.data;

import com.fossil.ci;
import com.fossil.co4;
import com.fossil.go4;
import com.fossil.nn4;
import com.fossil.vn4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BuddyChallengeDatabase extends ci {
    @DexIgnore
    public abstract nn4 a();

    @DexIgnore
    public abstract vn4 b();

    @DexIgnore
    public abstract co4 c();

    @DexIgnore
    public abstract go4 d();
}
