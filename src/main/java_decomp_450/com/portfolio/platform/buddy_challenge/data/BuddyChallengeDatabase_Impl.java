package com.portfolio.platform.buddy_challenge.data;

import com.facebook.share.internal.ShareConstants;
import com.fossil.ci;
import com.fossil.co4;
import com.fossil.do4;
import com.fossil.ei;
import com.fossil.go4;
import com.fossil.ho4;
import com.fossil.nn4;
import com.fossil.on4;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.vn4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wi;
import com.fossil.wn4;
import com.fossil.xi;
import com.fossil.zh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BuddyChallengeDatabase_Impl extends BuddyChallengeDatabase {
    @DexIgnore
    public volatile vn4 a;
    @DexIgnore
    public volatile go4 b;
    @DexIgnore
    public volatile nn4 c;
    @DexIgnore
    public volatile co4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ei.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `profile` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `createdAt` TEXT, `updatedAt` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `friend` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `confirmation` INTEGER NOT NULL, `pin` INTEGER NOT NULL, `friendType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `syncData` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `category` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `notification` (`id` TEXT NOT NULL, `titleKey` TEXT NOT NULL, `bodyKey` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `challengeData` TEXT, `profileData` TEXT, `confirmChallenge` INTEGER NOT NULL, `rank` INTEGER NOT NULL, `reachGoalAt` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `fitness` (`pinType` INTEGER NOT NULL, `id` INTEGER NOT NULL, `encryptMethod` INTEGER NOT NULL, `encryptedData` TEXT NOT NULL, `keyType` INTEGER NOT NULL, `sequence` TEXT NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `display_player` (`challengeId` TEXT NOT NULL, `numberOfPlayers` INTEGER, `topPlayers` TEXT, `nearPlayers` TEXT, PRIMARY KEY(`challengeId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `challenge_player` (`challengeId` TEXT NOT NULL, `challengeName` TEXT NOT NULL, `players` TEXT NOT NULL, PRIMARY KEY(`challengeId`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `history_challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `players` TEXT NOT NULL, `topPlayer` TEXT, `currentPlayer` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `completedAt` TEXT, `isFirst` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `players` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `isDeleted` INTEGER, `ranking` INTEGER, `status` TEXT, `stepsBase` INTEGER, `stepsOffset` INTEGER, `caloriesBase` INTEGER, `caloriesOffset` INTEGER, `profilePicture` TEXT, `lastSync` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b238af0aae08cbacb61a8a87455c3eee')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `profile`");
            wiVar.execSQL("DROP TABLE IF EXISTS `friend`");
            wiVar.execSQL("DROP TABLE IF EXISTS `challenge`");
            wiVar.execSQL("DROP TABLE IF EXISTS `notification`");
            wiVar.execSQL("DROP TABLE IF EXISTS `fitness`");
            wiVar.execSQL("DROP TABLE IF EXISTS `display_player`");
            wiVar.execSQL("DROP TABLE IF EXISTS `challenge_player`");
            wiVar.execSQL("DROP TABLE IF EXISTS `history_challenge`");
            wiVar.execSQL("DROP TABLE IF EXISTS `players`");
            if (((ci) BuddyChallengeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) BuddyChallengeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) BuddyChallengeDatabase_Impl.this).mDatabase = wiVar;
            BuddyChallengeDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) BuddyChallengeDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) BuddyChallengeDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("socialId", new ti.a("socialId", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_FIRST_NAME, new ti.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_LAST_NAME, new ti.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put("points", new ti.a("points", "INTEGER", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_PROFILE_PIC, new ti.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            ti tiVar = new ti("profile", hashMap, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "profile");
            if (!tiVar.equals(a2)) {
                return new ei.b(false, "profile(com.portfolio.platform.buddy_challenge.data.Profile).\n Expected:\n" + tiVar + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap2.put("socialId", new ti.a("socialId", "TEXT", true, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_FIRST_NAME, new ti.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_LAST_NAME, new ti.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put("points", new ti.a("points", "INTEGER", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_PROFILE_PIC, new ti.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap2.put("confirmation", new ti.a("confirmation", "INTEGER", true, 0, null, 1));
            hashMap2.put("pin", new ti.a("pin", "INTEGER", true, 0, null, 1));
            hashMap2.put("friendType", new ti.a("friendType", "INTEGER", true, 0, null, 1));
            ti tiVar2 = new ti("friend", hashMap2, new HashSet(0), new HashSet(0));
            ti a3 = ti.a(wiVar, "friend");
            if (!tiVar2.equals(a3)) {
                return new ei.b(false, "friend(com.portfolio.platform.buddy_challenge.data.Friend).\n Expected:\n" + tiVar2 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(17);
            hashMap3.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap3.put("type", new ti.a("type", "TEXT", false, 0, null, 1));
            hashMap3.put("name", new ti.a("name", "TEXT", false, 0, null, 1));
            hashMap3.put("des", new ti.a("des", "TEXT", false, 0, null, 1));
            hashMap3.put("owner", new ti.a("owner", "TEXT", false, 0, null, 1));
            hashMap3.put("numberOfPlayers", new ti.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put("target", new ti.a("target", "INTEGER", false, 0, null, 1));
            hashMap3.put("duration", new ti.a("duration", "INTEGER", false, 0, null, 1));
            hashMap3.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new ti.a(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap3.put("version", new ti.a("version", "TEXT", false, 0, null, 1));
            hashMap3.put("status", new ti.a("status", "TEXT", false, 0, null, 1));
            hashMap3.put("syncData", new ti.a("syncData", "TEXT", false, 0, null, 1));
            hashMap3.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap3.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap3.put("category", new ti.a("category", "TEXT", false, 0, null, 1));
            ti tiVar3 = new ti("challenge", hashMap3, new HashSet(0), new HashSet(0));
            ti a4 = ti.a(wiVar, "challenge");
            if (!tiVar3.equals(a4)) {
                return new ei.b(false, "challenge(com.portfolio.platform.buddy_challenge.data.Challenge).\n Expected:\n" + tiVar3 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(9);
            hashMap4.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap4.put("titleKey", new ti.a("titleKey", "TEXT", true, 0, null, 1));
            hashMap4.put("bodyKey", new ti.a("bodyKey", "TEXT", true, 0, null, 1));
            hashMap4.put("createdAt", new ti.a("createdAt", "TEXT", true, 0, null, 1));
            hashMap4.put("challengeData", new ti.a("challengeData", "TEXT", false, 0, null, 1));
            hashMap4.put("profileData", new ti.a("profileData", "TEXT", false, 0, null, 1));
            hashMap4.put("confirmChallenge", new ti.a("confirmChallenge", "INTEGER", true, 0, null, 1));
            hashMap4.put("rank", new ti.a("rank", "INTEGER", true, 0, null, 1));
            hashMap4.put("reachGoalAt", new ti.a("reachGoalAt", "TEXT", false, 0, null, 1));
            ti tiVar4 = new ti("notification", hashMap4, new HashSet(0), new HashSet(0));
            ti a5 = ti.a(wiVar, "notification");
            if (!tiVar4.equals(a5)) {
                return new ei.b(false, "notification(com.portfolio.platform.buddy_challenge.data.Notification).\n Expected:\n" + tiVar4 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("pinType", new ti.a("pinType", "INTEGER", true, 0, null, 1));
            hashMap5.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("encryptMethod", new ti.a("encryptMethod", "INTEGER", true, 0, null, 1));
            hashMap5.put("encryptedData", new ti.a("encryptedData", "TEXT", true, 0, null, 1));
            hashMap5.put("keyType", new ti.a("keyType", "INTEGER", true, 0, null, 1));
            hashMap5.put("sequence", new ti.a("sequence", "TEXT", true, 0, null, 1));
            ti tiVar5 = new ti("fitness", hashMap5, new HashSet(0), new HashSet(0));
            ti a6 = ti.a(wiVar, "fitness");
            if (!tiVar5.equals(a6)) {
                return new ei.b(false, "fitness(com.portfolio.platform.buddy_challenge.data.BCFitnessData).\n Expected:\n" + tiVar5 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("challengeId", new ti.a("challengeId", "TEXT", true, 1, null, 1));
            hashMap6.put("numberOfPlayers", new ti.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap6.put("topPlayers", new ti.a("topPlayers", "TEXT", false, 0, null, 1));
            hashMap6.put("nearPlayers", new ti.a("nearPlayers", "TEXT", false, 0, null, 1));
            ti tiVar6 = new ti("display_player", hashMap6, new HashSet(0), new HashSet(0));
            ti a7 = ti.a(wiVar, "display_player");
            if (!tiVar6.equals(a7)) {
                return new ei.b(false, "display_player(com.portfolio.platform.buddy_challenge.data.BCDisplayPlayer).\n Expected:\n" + tiVar6 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(3);
            hashMap7.put("challengeId", new ti.a("challengeId", "TEXT", true, 1, null, 1));
            hashMap7.put("challengeName", new ti.a("challengeName", "TEXT", true, 0, null, 1));
            hashMap7.put("players", new ti.a("players", "TEXT", true, 0, null, 1));
            ti tiVar7 = new ti("challenge_player", hashMap7, new HashSet(0), new HashSet(0));
            ti a8 = ti.a(wiVar, "challenge_player");
            if (!tiVar7.equals(a8)) {
                return new ei.b(false, "challenge_player(com.portfolio.platform.buddy_challenge.data.BCChallengePlayer).\n Expected:\n" + tiVar7 + "\n Found:\n" + a8);
            }
            HashMap hashMap8 = new HashMap(20);
            hashMap8.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap8.put("type", new ti.a("type", "TEXT", false, 0, null, 1));
            hashMap8.put("name", new ti.a("name", "TEXT", false, 0, null, 1));
            hashMap8.put("des", new ti.a("des", "TEXT", false, 0, null, 1));
            hashMap8.put("owner", new ti.a("owner", "TEXT", false, 0, null, 1));
            hashMap8.put("numberOfPlayers", new ti.a("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_START_TIME, new ti.a(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_END_TIME, new ti.a(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put("target", new ti.a("target", "INTEGER", false, 0, null, 1));
            hashMap8.put("duration", new ti.a("duration", "INTEGER", false, 0, null, 1));
            hashMap8.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new ti.a(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap8.put("version", new ti.a("version", "TEXT", false, 0, null, 1));
            hashMap8.put("status", new ti.a("status", "TEXT", false, 0, null, 1));
            hashMap8.put("players", new ti.a("players", "TEXT", true, 0, null, 1));
            hashMap8.put("topPlayer", new ti.a("topPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("currentPlayer", new ti.a("currentPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("createdAt", new ti.a("createdAt", "TEXT", false, 0, null, 1));
            hashMap8.put("updatedAt", new ti.a("updatedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("completedAt", new ti.a("completedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("isFirst", new ti.a("isFirst", "INTEGER", true, 0, null, 1));
            ti tiVar8 = new ti("history_challenge", hashMap8, new HashSet(0), new HashSet(0));
            ti a9 = ti.a(wiVar, "history_challenge");
            if (!tiVar8.equals(a9)) {
                return new ei.b(false, "history_challenge(com.portfolio.platform.buddy_challenge.data.HistoryChallenge).\n Expected:\n" + tiVar8 + "\n Found:\n" + a9);
            }
            HashMap hashMap9 = new HashMap(13);
            hashMap9.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap9.put("socialId", new ti.a("socialId", "TEXT", true, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_FIRST_NAME, new ti.a(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_LAST_NAME, new ti.a(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put("isDeleted", new ti.a("isDeleted", "INTEGER", false, 0, null, 1));
            hashMap9.put("ranking", new ti.a("ranking", "INTEGER", false, 0, null, 1));
            hashMap9.put("status", new ti.a("status", "TEXT", false, 0, null, 1));
            hashMap9.put("stepsBase", new ti.a("stepsBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("stepsOffset", new ti.a("stepsOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesBase", new ti.a("caloriesBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesOffset", new ti.a("caloriesOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_PROFILE_PIC, new ti.a(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap9.put("lastSync", new ti.a("lastSync", "TEXT", false, 0, null, 1));
            ti tiVar9 = new ti("players", hashMap9, new HashSet(0), new HashSet(0));
            ti a10 = ti.a(wiVar, "players");
            if (tiVar9.equals(a10)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "players(com.portfolio.platform.buddy_challenge.data.BCPlayer).\n Expected:\n" + tiVar9 + "\n Found:\n" + a10);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `profile`");
            writableDatabase.execSQL("DELETE FROM `friend`");
            writableDatabase.execSQL("DELETE FROM `challenge`");
            writableDatabase.execSQL("DELETE FROM `notification`");
            writableDatabase.execSQL("DELETE FROM `fitness`");
            writableDatabase.execSQL("DELETE FROM `display_player`");
            writableDatabase.execSQL("DELETE FROM `challenge_player`");
            writableDatabase.execSQL("DELETE FROM `history_challenge`");
            writableDatabase.execSQL("DELETE FROM `players`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "profile", "friend", "challenge", "notification", "fitness", "display_player", "challenge_player", "history_challenge", "players");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new a(1), "b238af0aae08cbacb61a8a87455c3eee", "5290fb2a20a9967b5a4b27ffa5f1bb99");
        xi.b.a a2 = xi.b.a(thVar.b);
        a2.a(thVar.c);
        a2.a(eiVar);
        return thVar.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public co4 c() {
        co4 co4;
        if (this.d != null) {
            return this.d;
        }
        synchronized (this) {
            if (this.d == null) {
                this.d = new do4(this);
            }
            co4 = this.d;
        }
        return co4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public go4 d() {
        go4 go4;
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b == null) {
                this.b = new ho4(this);
            }
            go4 = this.b;
        }
        return go4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public nn4 a() {
        nn4 nn4;
        if (this.c != null) {
            return this.c;
        }
        synchronized (this) {
            if (this.c == null) {
                this.c = new on4(this);
            }
            nn4 = this.c;
        }
        return nn4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public vn4 b() {
        vn4 vn4;
        if (this.a != null) {
            return this.a;
        }
        synchronized (this) {
            if (this.a == null) {
                this.a = new wn4(this);
            }
            vn4 = this.a;
        }
        return vn4;
    }
}
