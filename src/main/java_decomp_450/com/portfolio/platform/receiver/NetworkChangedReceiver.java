package com.portfolio.platform.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.workers.PushPendingDataWorker;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NetworkChangedReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public UserRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2", f = "NetworkChangedReceiver.kt", l = {52}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isInternetAvailable;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkChangedReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NetworkChangedReceiver networkChangedReceiver, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = networkChangedReceiver;
            this.$isInternetAvailable = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$isInternetAvailable, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                UserRepository a2 = this.this$0.a();
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.getCurrentUser(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) obj) != null && !this.$isInternetAvailable) {
                PushPendingDataWorker.G.a();
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = NetworkChangedReceiver.class.getSimpleName();
        ee7.a((Object) simpleName, "NetworkChangedReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public NetworkChangedReceiver() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final UserRepository a() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        ee7.b(context, "context");
        ee7.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(b, "onReceive");
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Constants.ACTIVITY);
        if (activityManager != null) {
            if (activityManager.getRunningAppProcesses() != null) {
                Iterator<ActivityManager.RunningAppProcessInfo> it = activityManager.getRunningAppProcesses().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    runningAppProcessInfo = it.next();
                    FLogger.INSTANCE.getLocal().d(b, "onReceive processId=" + runningAppProcessInfo.pid);
                    if (runningAppProcessInfo.pid == myPid) {
                        break;
                    }
                }
            } else {
                return;
            }
        }
        runningAppProcessInfo = null;
        if (runningAppProcessInfo != null) {
            if (!TextUtils.isEmpty(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null)) {
                if (ee7.a((Object) (runningAppProcessInfo != null ? runningAppProcessInfo.processName : null), (Object) PortfolioApp.g0.c().getPackageName())) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = b;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onReceive processName=");
                    sb.append(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null);
                    local.d(str, sb.toString());
                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, a(context), null), 3, null);
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }
}
