package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.pd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BootReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b; // = BootReceiver.class.getSimpleName();
    @DexIgnore
    public pd5 a;

    @DexIgnore
    public BootReceiver() {
        PortfolioApp.c0.f().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        pd5 pd5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".BootReceiver, start hwlog sync scheduler");
        if (!TextUtils.isEmpty(intent.getAction()) && intent.getAction().equalsIgnoreCase("android.intent.action.BOOT_COMPLETED") && (pd5 = this.a) != null) {
            pd5.c(context);
            this.a.e(context);
        }
    }
}
