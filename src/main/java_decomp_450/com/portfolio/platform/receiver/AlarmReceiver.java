package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ch5;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.pd5;
import com.fossil.qj7;
import com.fossil.tb7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmReceiver extends BroadcastReceiver {
    @DexIgnore
    public UserRepository a;
    @DexIgnore
    public ch5 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public pd5 d;
    @DexIgnore
    public AlarmsRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$1", f = "AlarmReceiver.kt", l = {56, 59, 104}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public long J$1;
        @DexIgnore
        public long J$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AlarmReceiver alarmReceiver, int i, Intent intent, Context context, fb7 fb7) {
            super(2, fb7);
            this.this$0 = alarmReceiver;
            this.$action = i;
            this.$intent = intent;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$action, this.$intent, this.$context, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:46:0x013e  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0143  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x0146  */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x028c  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x029a  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r33) {
            /*
                r32 = this;
                r1 = r32
                java.lang.Object r0 = com.fossil.nb7.a()
                int r2 = r1.label
                java.lang.String r3 = "onReceive - user=null||empty||equals=false"
                java.lang.String r4 = "DEF_ALARM_RECEIVER_USER_ID"
                r5 = 2
                r7 = 3
                r8 = 1
                java.lang.String r9 = "AlarmReceiver"
                if (r2 == 0) goto L_0x0050
                if (r2 == r8) goto L_0x0046
                if (r2 == r5) goto L_0x0037
                if (r2 != r7) goto L_0x002f
                java.lang.Object r0 = r1.L$2
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r2 = r1.L$1
                com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                java.lang.Object r2 = r1.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r33)     // Catch:{ Exception -> 0x002c }
                r2 = r33
                goto L_0x013a
            L_0x002c:
                r0 = move-exception
                goto L_0x01dc
            L_0x002f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0037:
                java.lang.Object r0 = r1.L$1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r2 = r1.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r33)
                r2 = r33
                goto L_0x0202
            L_0x0046:
                java.lang.Object r2 = r1.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r33)
                r10 = r33
                goto L_0x0066
            L_0x0050:
                com.fossil.t87.a(r33)
                com.fossil.yi7 r2 = r1.p$
                com.portfolio.platform.receiver.AlarmReceiver r10 = r1.this$0
                com.portfolio.platform.data.source.UserRepository r10 = r10.e()
                r1.L$0 = r2
                r1.label = r8
                java.lang.Object r10 = r10.getCurrentUser(r1)
                if (r10 != r0) goto L_0x0066
                return r0
            L_0x0066:
                com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
                int r11 = r1.$action
                if (r11 == 0) goto L_0x01ee
                if (r11 == r5) goto L_0x00b8
                if (r11 == r7) goto L_0x0072
                goto L_0x027c
            L_0x0072:
                android.content.Intent r0 = r1.$intent
                java.lang.String r0 = r0.getStringExtra(r4)
                if (r10 == 0) goto L_0x00a2
                boolean r2 = android.text.TextUtils.isEmpty(r0)
                if (r2 != 0) goto L_0x00a2
                java.lang.String r2 = r10.getUserId()
                boolean r0 = com.fossil.ee7.a(r2, r0)
                r0 = r0 ^ r8
                if (r0 == 0) goto L_0x008c
                goto L_0x00a2
            L_0x008c:
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                r0.a()
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                android.content.Context r2 = r1.$context
                r0.d(r2)
                goto L_0x027c
            L_0x00a2:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                r0.e(r9, r3)
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                android.content.Context r2 = r1.$context
                r0.a(r2)
                goto L_0x027c
            L_0x00b8:
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                java.lang.String r4 = "onReceive - ACTION_ALARM_REPEAT"
                r3.d(r9, r4)
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                java.lang.String r3 = r3.c()
                int r4 = r3.length()
                if (r4 != 0) goto L_0x00d5
                r4 = 1
                goto L_0x00d6
            L_0x00d5:
                r4 = 0
            L_0x00d6:
                if (r4 == 0) goto L_0x00db
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x00db:
                com.portfolio.platform.receiver.AlarmReceiver r4 = r1.this$0
                com.fossil.ch5 r4 = r4.d()
                long r4 = r4.e(r3)
                com.portfolio.platform.receiver.AlarmReceiver r11 = r1.this$0
                com.fossil.ch5 r11 = r11.d()
                long r11 = r11.c(r3)
                r13 = 432000000(0x19bfcc00, float:1.9831332E-23)
                long r14 = java.lang.System.currentTimeMillis()
                long r16 = r14 - r11
                long r6 = (long) r13
                int r18 = (r16 > r6 ? 1 : (r16 == r6 ? 0 : -1))
                if (r18 <= 0) goto L_0x027c
                long r16 = r14 - r4
                int r18 = (r16 > r6 ? 1 : (r16 == r6 ? 0 : -1))
                if (r18 <= 0) goto L_0x027c
                com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                java.lang.String r7 = "Remind to sync success"
                r6.d(r9, r7)
                com.portfolio.platform.receiver.AlarmReceiver r6 = r1.this$0
                com.fossil.ch5 r6 = r6.d()
                r6.a(r14, r3)
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                com.portfolio.platform.data.source.UserRepository r6 = r6.s()
                r1.L$0 = r2
                r1.L$1 = r10
                r1.L$2 = r3
                r1.J$0 = r4
                r1.J$1 = r11
                r1.I$0 = r13
                r1.J$2 = r14
                r2 = 3
                r1.label = r2
                java.lang.Object r2 = r6.getCurrentUser(r1)
                if (r2 != r0) goto L_0x0139
                return r0
            L_0x0139:
                r0 = r3
            L_0x013a:
                com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                if (r2 == 0) goto L_0x0143
                java.lang.String r2 = r2.getFirstName()
                goto L_0x0144
            L_0x0143:
                r2 = 0
            L_0x0144:
                if (r2 == 0) goto L_0x027c
                com.fossil.we7 r3 = com.fossil.we7.a
                android.content.Context r3 = r1.$context
                r4 = 2131887382(0x7f120516, float:1.940937E38)
                java.lang.String r3 = com.fossil.ig5.a(r3, r4)
                java.lang.String r4 = "LanguageHelper.getString\u2026ext, R.string.hello_user)"
                com.fossil.ee7.a(r3, r4)
                java.lang.Object[] r4 = new java.lang.Object[r8]
                r5 = 0
                r4[r5] = r2
                java.lang.Object[] r2 = java.util.Arrays.copyOf(r4, r8)
                java.lang.String r2 = java.lang.String.format(r3, r2)
                java.lang.String r3 = "java.lang.String.format(format, *args)"
                com.fossil.ee7.a(r2, r3)
                android.content.Context r3 = r1.$context
                r4 = 2131886832(0x7f1202f0, float:1.9408254E38)
                java.lang.String r3 = com.fossil.ig5.a(r3, r4)
                com.fossil.qh5$a r4 = com.fossil.qh5.c
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                android.content.Context r5 = r5.getApplicationContext()
                java.lang.String r6 = "PortfolioApp.instance.applicationContext"
                com.fossil.ee7.a(r5, r6)
                java.lang.String r6 = "message"
                com.fossil.ee7.a(r3, r6)
                r4.a(r5, r2, r3)
                boolean r2 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r0)
                if (r2 == 0) goto L_0x027c
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r4 = "Remind to sync send notificaiton to watch"
                r2.d(r9, r4)
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj r4 = new com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj
                r19 = 99
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r5 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r5 = r5.getFOSSIL()
                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r20 = r5.getNotificationType()
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r5 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
                com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r21 = r5.getFOSSIL()
                java.lang.String r22 = "Smart Watch"
                java.lang.String r23 = "Fossil"
                r24 = -1
                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationFlag[] r5 = new com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag[r8]
                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationFlag r6 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationFlag.IMPORTANT
                r7 = 0
                r5[r7] = r6
                java.util.List r26 = com.fossil.w97.d(r5)
                r27 = 0
                r28 = 0
                r29 = 0
                r30 = 1792(0x700, float:2.511E-42)
                r31 = 0
                r18 = r4
                r25 = r3
                r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31)
                r2.b(r0, r4)
                goto L_0x027c
            L_0x01dc:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r3 = r0.getMessage()
                r2.e(r9, r3)
                r0.printStackTrace()
                goto L_0x027c
            L_0x01ee:
                com.portfolio.platform.receiver.AlarmReceiver r6 = r1.this$0
                com.portfolio.platform.data.source.AlarmsRepository r6 = r6.b()
                r1.L$0 = r2
                r1.L$1 = r10
                r1.label = r5
                java.lang.Object r2 = r6.findNextActiveAlarm(r1)
                if (r2 != r0) goto L_0x0201
                return r0
            L_0x0201:
                r0 = r10
            L_0x0202:
                com.portfolio.platform.data.source.local.alarm.Alarm r2 = (com.portfolio.platform.data.source.local.alarm.Alarm) r2
                android.content.Intent r5 = r1.$intent
                java.lang.String r5 = r5.getStringExtra(r4)
                if (r0 == 0) goto L_0x0268
                boolean r6 = android.text.TextUtils.isEmpty(r5)
                if (r6 != 0) goto L_0x0268
                java.lang.String r6 = r0.getUserId()
                boolean r5 = com.fossil.ee7.a(r6, r5)
                r5 = r5 ^ r8
                if (r5 != 0) goto L_0x0268
                if (r2 != 0) goto L_0x0220
                goto L_0x0268
            L_0x0220:
                android.os.Bundle r3 = new android.os.Bundle
                r3.<init>()
                java.lang.String r5 = "DEF_ALARM_RECEIVER_ACTION"
                r6 = 3
                r3.putInt(r5, r6)
                java.lang.String r0 = r0.getUserId()
                r3.putString(r4, r0)
                android.content.Intent r0 = new android.content.Intent
                android.content.Context r4 = r1.$context
                java.lang.Class<com.portfolio.platform.receiver.AlarmReceiver> r5 = com.portfolio.platform.receiver.AlarmReceiver.class
                r0.<init>(r4, r5)
                java.lang.String r4 = "com.portfolio.platform.ALARM_RECEIVER"
                r0.setAction(r4)
                r0.putExtras(r3)
                android.content.Context r3 = r1.$context
                java.lang.String r4 = "alarm"
                java.lang.Object r3 = r3.getSystemService(r4)
                android.app.AlarmManager r3 = (android.app.AlarmManager) r3
                if (r3 == 0) goto L_0x027c
                com.fossil.pd5$a r4 = com.fossil.pd5.d
                long r5 = r2.getMillisecond()
                long r4 = r4.a(r5)
                android.content.Context r2 = r1.$context
                r6 = 101(0x65, float:1.42E-43)
                r7 = 134217728(0x8000000, float:3.85186E-34)
                android.app.PendingIntent r0 = android.app.PendingIntent.getBroadcast(r2, r6, r0, r7)
                r2 = 0
                r3.setExact(r2, r4, r0)
                goto L_0x027c
            L_0x0268:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                r0.e(r9, r3)
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                android.content.Context r2 = r1.$context
                r0.a(r2)
            L_0x027c:
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                java.lang.String r0 = r0.c()
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 == 0) goto L_0x029a
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = "onReceive - active device is EMPTY!!!"
                r0.d(r9, r2)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x029a:
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.portfolio.platform.data.source.DeviceRepository r0 = r0.c()
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                java.lang.String r2 = r2.c()
                com.portfolio.platform.data.model.Device r0 = r0.getDeviceBySerial(r2)
                if (r0 == 0) goto L_0x0311
                com.portfolio.platform.receiver.AlarmReceiver r2 = r1.this$0
                com.fossil.ch5 r2 = r2.d()
                int r2 = r2.z()
                android.content.Intent r3 = r1.$intent
                r4 = -1
                java.lang.String r5 = "REQUEST_CODE"
                int r3 = r3.getIntExtra(r5, r4)
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "onReceive - extraCode="
                r5.append(r6)
                r5.append(r3)
                java.lang.String r6 = ", lowBatteryLevel="
                r5.append(r6)
                r5.append(r2)
                java.lang.String r6 = ", deviceBattery="
                r5.append(r6)
                int r6 = r0.getBatteryLevel()
                r5.append(r6)
                java.lang.String r5 = r5.toString()
                r4.d(r9, r5)
                if (r3 == 0) goto L_0x02f4
                goto L_0x0311
            L_0x02f4:
                int r0 = r0.getBatteryLevel()
                if (r0 < r2) goto L_0x0306
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                android.content.Context r2 = r1.$context
                r0.f(r2)
                goto L_0x0311
            L_0x0306:
                com.portfolio.platform.receiver.AlarmReceiver r0 = r1.this$0
                com.fossil.pd5 r0 = r0.a()
                android.content.Context r2 = r1.$context
                r0.b(r2)
            L_0x0311:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.receiver.AlarmReceiver.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final pd5 a() {
        pd5 pd5 = this.d;
        if (pd5 != null) {
            return pd5;
        }
        ee7.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        ee7.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final ch5 d() {
        ch5 ch5 = this.b;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ee7.b(context, "context");
        PortfolioApp.g0.c().f().a(this);
        FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        if (intent != null) {
            int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmReceiver", "onReceive - action=" + intExtra);
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, intExtra, intent, context, null), 3, null);
        }
    }
}
