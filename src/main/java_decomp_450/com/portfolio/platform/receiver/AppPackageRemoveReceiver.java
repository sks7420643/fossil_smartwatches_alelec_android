package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.nx6;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public NotificationsRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1", f = "AppPackageRemoveReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageRemoveReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AppPackageRemoveReceiver appPackageRemoveReceiver, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = appPackageRemoveReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$packageName, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                AppFilter a = nx6.b.a(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_SAM);
                AppFilter a2 = nx6.b.a(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_DIANA);
                if (a != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = AppPackageRemoveReceiver.b;
                    local.d(b, "remove app " + this.$packageName);
                    this.this$0.a().removeAppFilter(a);
                }
                if (a2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String b2 = AppPackageRemoveReceiver.b;
                    local2.d(b2, "remove app " + this.$packageName);
                    this.this$0.a().removeAppFilter(a2);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        ee7.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        ee7.d("notificationsRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String str;
        ee7.b(context, "context");
        ee7.b(intent, "intent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "onReceive " + intent.getAction());
        if (ee7.a((Object) "android.intent.action.PACKAGE_FULLY_REMOVED", (Object) intent.getAction())) {
            Uri data = intent.getData();
            if (data == null || (str = data.getSchemeSpecificPart()) == null) {
                str = "";
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.d(str3, "package removed " + str);
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, str, null), 3, null);
        }
    }
}
