package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.ee7;
import com.fossil.rx6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RestartServiceReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        ee7.a((Object) simpleName, "RestartServiceReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        ee7.b(context, "context");
        ee7.b(intent, "intent");
        if (!(PortfolioApp.g0.c().c().length() == 0)) {
            FLogger.INSTANCE.getLocal().e(a, "Service Tracking - startForegroundService in RestartServiceReceiver");
            rx6.a.a(rx6.a, context, MFDeviceService.class, null, 4, null);
            rx6.a.a(rx6.a, context, ButtonService.class, null, 4, null);
        }
    }
}
