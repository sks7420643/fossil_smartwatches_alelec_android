package com.portfolio.platform.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.AppLinkData;
import com.fossil.be5;
import com.fossil.cb0;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.ek5;
import com.fossil.fb0;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.jc5;
import com.fossil.k07;
import com.fossil.kd7;
import com.fossil.kj7;
import com.fossil.lc5;
import com.fossil.mh7;
import com.fossil.mk5;
import com.fossil.nb7;
import com.fossil.nh7;
import com.fossil.oc5;
import com.fossil.pj4;
import com.fossil.pk5;
import com.fossil.px6;
import com.fossil.qj7;
import com.fossil.rb7;
import com.fossil.s90;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.fossil.zs7;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"OverrideAbstract"})
public final class FossilNotificationListenerService extends NotificationListenerService {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static boolean t;
    @DexIgnore
    public static /* final */ String[] u; // = {"android.title", "android.title.big", "android.text", "android.subText", "android.infoText", "android.summaryText", "android.bigText"};
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public /* final */ StringBuilder a; // = new StringBuilder();
    @DexIgnore
    public /* final */ SparseArray<b> b; // = new SparseArray<>();
    @DexIgnore
    public /* final */ yi7 c; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));
    @DexIgnore
    public /* final */ HashSet<String> d; // = new HashSet<>();
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ Runnable f; // = new d(this);
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<StatusBarNotification> g; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public ek5 h;
    @DexIgnore
    public mk5 i;
    @DexIgnore
    public pk5 j;
    @DexIgnore
    public pj4 p;
    @DexIgnore
    public ch5 q;
    @DexIgnore
    public UserRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return FossilNotificationListenerService.s;
        }

        @DexIgnore
        public final boolean b() {
            return FossilNotificationListenerService.t;
        }

        @DexIgnore
        public final void c() {
            FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService()");
            try {
                ComponentName componentName = new ComponentName(PortfolioApp.g0.c(), FossilNotificationListenerService.class);
                a(componentName);
                if (Build.VERSION.SDK_INT >= 24) {
                    FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService() - requestRebind");
                    NotificationListenerService.requestRebind(componentName);
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = a();
                local.d(a, "tryReconnectNotificationService() - ex = " + e);
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(ComponentName componentName) {
            FLogger.INSTANCE.getLocal().d(a(), "toggleNotificationListenerService()");
            PackageManager packageManager = PortfolioApp.g0.c().getPackageManager();
            packageManager.setComponentEnabledSetting(componentName, 2, 1);
            packageManager.setComponentEnabledSetting(componentName, 1, 1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a; // = -1;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ long e;

        @DexIgnore
        public b(String str, String str2, int i, long j) {
            ee7.b(str, "packageName");
            ee7.b(str2, "text");
            this.b = str;
            this.c = str2;
            this.d = i;
            this.e = j;
        }

        @DexIgnore
        public final long a() {
            return this.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                return ee7.a(this.b, bVar.b) && ee7.a(this.c, bVar.c) && this.d == bVar.d;
            }
        }

        @DexIgnore
        public int hashCode() {
            if (this.a == -1) {
                this.a = (this.b + this.d).hashCode();
            }
            return this.a;
        }

        @DexIgnore
        public String toString() {
            return this.d + " | " + this.b + " | " + this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.FossilNotificationListenerService$filterQueue$1", f = "FossilNotificationListenerService.kt", l = {265}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FossilNotificationListenerService fossilNotificationListenerService, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x013c  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x00a6  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 0
                r3 = 1
                if (r1 == 0) goto L_0x003a
                if (r1 != r3) goto L_0x0032
                java.lang.Object r1 = r14.L$4
                android.service.notification.StatusBarNotification r1 = (android.service.notification.StatusBarNotification) r1
                int r4 = r14.I$1
                int r5 = r14.I$0
                java.lang.Object r6 = r14.L$3
                java.util.HashSet r6 = (java.util.HashSet) r6
                java.lang.Object r7 = r14.L$2
                java.util.ArrayList r7 = (java.util.ArrayList) r7
                java.lang.Object r8 = r14.L$1
                java.util.ArrayList r8 = (java.util.ArrayList) r8
                java.lang.Object r9 = r14.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r15)
                r10 = r9
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r4
                r4 = r1
                r1 = r0
                r0 = r14
                goto L_0x0134
            L_0x0032:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x003a:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r15 = r14.p$
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r4 = com.portfolio.platform.service.FossilNotificationListenerService.v
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "filterQueue() - queue size = "
                r5.append(r6)
                com.portfolio.platform.service.FossilNotificationListenerService r6 = r14.this$0
                java.util.concurrent.ConcurrentLinkedQueue r6 = r6.g
                int r6 = r6.size()
                r5.append(r6)
                java.lang.String r6 = " items"
                r5.append(r6)
                java.lang.String r5 = r5.toString()
                r1.d(r4, r5)
                java.util.ArrayList r1 = new java.util.ArrayList
                com.portfolio.platform.service.FossilNotificationListenerService r4 = r14.this$0
                java.util.concurrent.ConcurrentLinkedQueue r4 = r4.g
                int r4 = r4.size()
                r1.<init>(r4)
                com.portfolio.platform.service.FossilNotificationListenerService r4 = r14.this$0
                java.util.concurrent.ConcurrentLinkedQueue r4 = r4.g
                r1.addAll(r4)
                java.util.ArrayList r4 = new java.util.ArrayList
                r4.<init>()
                com.portfolio.platform.service.FossilNotificationListenerService r5 = r14.this$0
                java.util.concurrent.ConcurrentLinkedQueue r5 = r5.g
                r5.clear()
                java.util.HashSet r5 = new java.util.HashSet
                r5.<init>()
                int r6 = r1.size()
                r9 = r15
                r8 = r1
                r7 = r4
                r4 = r6
                r15 = r14
                r6 = r5
                r5 = 0
            L_0x00a4:
                if (r5 >= r4) goto L_0x0160
                java.lang.Object r1 = r8.get(r5)
                java.lang.String r10 = "notifications[i]"
                com.fossil.ee7.a(r1, r10)
                android.service.notification.StatusBarNotification r1 = (android.service.notification.StatusBarNotification) r1
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r11 = com.portfolio.platform.service.FossilNotificationListenerService.v
                java.lang.String r11 = r11.a()
                java.lang.String r12 = "filterQueue() - Looking at:"
                r10.d(r11, r12)
                com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
                boolean r10 = r10.e()
                if (r10 == 0) goto L_0x00cf
                com.portfolio.platform.service.FossilNotificationListenerService r10 = r15.this$0
                r10.d(r1)
            L_0x00cf:
                com.portfolio.platform.service.FossilNotificationListenerService r10 = r15.this$0
                boolean r10 = r10.a(r1, r7)
                com.portfolio.platform.service.FossilNotificationListenerService r11 = r15.this$0
                boolean r11 = r11.b(r1, r7)
                if (r11 == 0) goto L_0x00de
                r10 = 0
            L_0x00de:
                java.lang.String r11 = r1.getPackageName()
                boolean r11 = r6.contains(r11)
                if (r11 == 0) goto L_0x00fa
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r11 = com.portfolio.platform.service.FossilNotificationListenerService.v
                java.lang.String r11 = r11.a()
                java.lang.String r12 = "filterQueue() - This notification is processing"
                r10.d(r11, r12)
                r10 = 0
            L_0x00fa:
                if (r10 == 0) goto L_0x014c
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r11 = com.portfolio.platform.service.FossilNotificationListenerService.v
                java.lang.String r11 = r11.a()
                java.lang.String r12 = "filterQueue() - Allowing notification to process..."
                r10.d(r11, r12)
                r7.add(r1)
                com.portfolio.platform.service.FossilNotificationListenerService r10 = r15.this$0
                r15.L$0 = r9
                r15.L$1 = r8
                r15.L$2 = r7
                r15.L$3 = r6
                r15.I$0 = r5
                r15.I$1 = r4
                r15.L$4 = r1
                r15.label = r3
                java.lang.Object r10 = r10.a(r1, r15)
                if (r10 != r0) goto L_0x0129
                return r0
            L_0x0129:
                r13 = r0
                r0 = r15
                r15 = r10
                r10 = r9
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r4
                r4 = r1
                r1 = r13
            L_0x0134:
                java.lang.Boolean r15 = (java.lang.Boolean) r15
                boolean r15 = r15.booleanValue()
                if (r15 == 0) goto L_0x0143
                java.lang.String r15 = r4.getPackageName()
                r7.add(r15)
            L_0x0143:
                r15 = r0
                r0 = r1
                r4 = r5
                r5 = r6
                r6 = r7
                r7 = r8
                r8 = r9
                r9 = r10
                goto L_0x015d
            L_0x014c:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.service.FossilNotificationListenerService$a r10 = com.portfolio.platform.service.FossilNotificationListenerService.v
                java.lang.String r10 = r10.a()
                java.lang.String r11 = "filterQueue() - Blocking notification from processing"
                r1.d(r10, r11)
            L_0x015d:
                int r5 = r5 + r3
                goto L_0x00a4
            L_0x0160:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService a;

        @DexIgnore
        public d(FossilNotificationListenerService fossilNotificationListenerService) {
            this.a = fossilNotificationListenerService;
        }

        @DexIgnore
        public final void run() {
            ik7 unused = this.a.b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.FossilNotificationListenerService$onCreate$1", f = "FossilNotificationListenerService.kt", l = {94, 97}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(FossilNotificationListenerService fossilNotificationListenerService, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                this.L$0 = yi7;
                this.label = 1;
                if (kj7.a(5000, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (PortfolioApp.g0.c().F()) {
                FLogger.INSTANCE.getLocal().d(FossilNotificationListenerService.v.a(), "onCreate() - enable Music Control Component.");
                ek5 c = this.this$0.c();
                this.L$0 = yi7;
                this.label = 2;
                if (c.a(this) == a) {
                    return a;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.FossilNotificationListenerService", f = "FossilNotificationListenerService.kt", l = {356}, m = "processNotification")
    public static final class f extends rb7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FossilNotificationListenerService fossilNotificationListenerService, fb7 fb7) {
            super(fb7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((StatusBarNotification) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.FossilNotificationListenerService", f = "FossilNotificationListenerService.kt", l = {420}, m = "pushLogToFireBase")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FossilNotificationListenerService fossilNotificationListenerService, fb7 fb7) {
            super(fb7);
            this.this$0 = fossilNotificationListenerService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b((StatusBarNotification) null, this);
        }
    }

    /*
    static {
        String simpleName = FossilNotificationListenerService.class.getSimpleName();
        ee7.a((Object) simpleName, "FossilNotificationListen\u2026ce::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public final ek5 c() {
        ek5 ek5 = this.h;
        if (ek5 != null) {
            return ek5;
        }
        ee7.d("mMusicControlComponent");
        throw null;
    }

    @DexIgnore
    public final boolean d() {
        ch5 ch5 = this.q;
        if (ch5 != null) {
            boolean I = ch5.I();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = s;
            local.d(str, "isBlockedByDND() = " + I);
            return I;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.g0.c().f().a(this);
        PortfolioApp.g0.b(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onCreate() - Notification Listener Service permission " + PortfolioApp.g0.c().F());
        ik7 unused = xh7.b(this.c, null, null, new e(this, null), 3, null);
        FLogger.INSTANCE.getLocal().d(s, "onCreate() - Notification Listener Service Created");
    }

    @DexIgnore
    public void onDestroy() {
        mk5 mk5 = this.i;
        if (mk5 != null) {
            mk5.m();
            PortfolioApp.g0.c(this);
            super.onDestroy();
            ek5 ek5 = this.h;
            if (ek5 != null) {
                ek5.d();
                FLogger.INSTANCE.getLocal().d(s, "onDestroy() - Notification Listener Service Destroyed");
                return;
            }
            ee7.d("mMusicControlComponent");
            throw null;
        }
        ee7.d("mDianaNotificationComponent");
        throw null;
    }

    @DexIgnore
    @k07
    public final void onDeviceAppEvent(jc5 jc5) {
        fb0 fb0;
        ee7.b(jc5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("onDeviceAppEvent", "onDeviceAppEvent = " + jc5);
        if (jc5.a() == cb0.APP_NOTIFICATION_CONTROL.ordinal() && (fb0 = (fb0) jc5.b().getParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA)) != null && fb0.getAction().getActionType() == s90.DISMISS_NOTIFICATION) {
            mk5 mk5 = this.i;
            if (mk5 != null) {
                mk5.b(fb0.getNotificationUid());
            } else {
                ee7.d("mDianaNotificationComponent");
                throw null;
            }
        }
    }

    @DexIgnore
    @k07
    public final void onDeviceConnectionStateChange(lc5 lc5) {
        ee7.b(lc5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onDeviceConnectionStateChange(), serial=" + lc5.a() + ", state=" + lc5.b());
        if (lc5.b() == ConnectionStateChange.GATT_ON.ordinal() && FossilDeviceSerialPatternUtil.isDianaDevice(lc5.a()) && PortfolioApp.g0.c().F()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local2.d(str2, "deviceChange, event = " + lc5);
            mk5 mk5 = this.i;
            if (mk5 != null) {
                mk5.k();
            } else {
                ee7.d("mDianaNotificationComponent");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onListenerConnected() {
        super.onListenerConnected();
        FLogger.INSTANCE.getLocal().d(s, "onListenerConnected()");
        t = true;
        mk5 mk5 = this.i;
        if (mk5 != null) {
            mk5.l();
        } else {
            ee7.d("mDianaNotificationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        FLogger.INSTANCE.getLocal().d(s, "onListenerDisconnected()");
        t = false;
    }

    @DexIgnore
    @k07
    public final void onNotificationEvent(oc5 oc5) {
        ee7.b(oc5, Constants.EVENT);
        FLogger.INSTANCE.getLocal().d(s, "onNotificationEvent");
        mk5 mk5 = this.i;
        if (mk5 != null) {
            mk5.a(oc5.a(), oc5.b());
        } else {
            ee7.d("mDianaNotificationComponent");
            throw null;
        }
    }

    @DexIgnore
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        ee7.b(statusBarNotification, "sbn");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onNotificationPosted() - Receive notification with permission enable is " + px6.a.e());
        String c2 = PortfolioApp.g0.c().c();
        if (!be5.o.d(c2)) {
            FLogger.INSTANCE.getLocal().d(s, "onNotificationPosted() - Device is not supported");
        } else if (FossilDeviceSerialPatternUtil.isDianaDevice(c2)) {
            d(statusBarNotification);
            c(statusBarNotification);
            mk5 mk5 = this.i;
            if (mk5 != null) {
                mk5.b(statusBarNotification);
            } else {
                ee7.d("mDianaNotificationComponent");
                throw null;
            }
        } else {
            ch5 ch5 = this.q;
            if (ch5 != null) {
                if (ch5.S() && !d()) {
                    pk5 pk5 = this.j;
                    if (pk5 != null) {
                        pk5.a(statusBarNotification);
                    } else {
                        ee7.d("mHybridMessageNotificationComponent");
                        throw null;
                    }
                }
                a(statusBarNotification);
                return;
            }
            ee7.d("mSharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(s, "onNotificationRemoved()");
        if (statusBarNotification != null) {
            try {
                super.onNotificationRemoved(statusBarNotification);
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.g0.c().c())) {
                    d(statusBarNotification);
                    c(statusBarNotification);
                    mk5 mk5 = this.i;
                    if (mk5 != null) {
                        mk5.a(statusBarNotification);
                    } else {
                        ee7.d("mDianaNotificationComponent");
                        throw null;
                    }
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = s;
                local.d(str, "onNotificationRemoved(): package = " + statusBarNotification.getPackageName() + " - notification = " + statusBarNotification.getNotification() + ", exception = " + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        ee7.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(s, "onStartCommand() - Notification Listener Service Started");
        return super.onStartCommand(intent, i2, i3);
    }

    @DexIgnore
    public final void c(StatusBarNotification statusBarNotification) {
        String packageName = statusBarNotification.getPackageName();
        Notification notification = statusBarNotification.getNotification();
        Bundle bundle = notification.extras;
        String obj = !TextUtils.isEmpty(notification.tickerText) ? notification.tickerText.toString() : "";
        CharSequence charSequence = bundle.getCharSequence("android.title");
        CharSequence charSequence2 = bundle.getCharSequence("android.text");
        CharSequence charSequence3 = bundle.getCharSequence("android.subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "printDebugInfo() - Notification info:\n  package name: " + packageName + "\n" + "  ticker: " + obj + "\n" + "  title: " + charSequence + "\n" + "  subtext: " + charSequence3 + "\n" + "  text: " + charSequence2);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (ee7.a((Object) statusBarNotification.getPackageName(), (Object) ((StatusBarNotification) list.get(i2)).getPackageName())) {
                FLogger.INSTANCE.getLocal().d(s, "isDuplicated() - Notification is duplicated");
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void a(StatusBarNotification statusBarNotification) {
        FLogger.INSTANCE.getLocal().d(s, "addNotificationToQueue()");
        if (statusBarNotification.getNotification().priority < -1) {
            FLogger.INSTANCE.getLocal().d(s, "addNotificationToQueue() - Ignoring Min priority notification");
            if (!PortfolioApp.g0.c().G()) {
                c(statusBarNotification);
                return;
            }
            return;
        }
        this.g.add(statusBarNotification);
        this.e.removeCallbacks(this.f);
        this.e.postDelayed(this.f, 500);
    }

    @DexIgnore
    public final void d(StatusBarNotification statusBarNotification) {
        Notification notification = statusBarNotification.getNotification();
        FLogger.INSTANCE.getLocal().d(s, "............");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "  Notification Posted: " + statusBarNotification.getPackageName());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local2.d(str2, "  Id: " + statusBarNotification.getId());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = s;
        local3.d(str3, "  Post Time: " + statusBarNotification.getPostTime());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = s;
        local4.d(str4, "  Tag: " + statusBarNotification.getTag());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = s;
        local5.d(str5, "  Content: " + notification.extras);
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = s;
        local6.d(str6, "  Notification Priority: " + notification.priority);
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = s;
        local7.d(str7, "  Notification Flags: " + Integer.toBinaryString(notification.flags));
        if (Build.VERSION.SDK_INT >= 21) {
            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
            String str8 = s;
            local8.d(str8, "  Group Key: " + statusBarNotification.getGroupKey());
            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
            String str9 = s;
            local9.d(str9, "  Key: " + statusBarNotification.getKey());
            ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
            String str10 = s;
            StringBuilder sb = new StringBuilder();
            sb.append("  Notification Group: ");
            ee7.a((Object) notification, "notification");
            sb.append(notification.getGroup());
            local10.d(str10, sb.toString());
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            String str11 = s;
            local11.d(str11, "  Notification Sort Key: " + notification.getSortKey());
        }
        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
        String str12 = s;
        local12.d(str12, "  isSummary = " + b(statusBarNotification));
        FLogger.INSTANCE.getLocal().d(s, "............");
    }

    @DexIgnore
    public final ik7 b() {
        return xh7.b(this.c, null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification) {
        return (statusBarNotification.getNotification().flags & 512) == 512;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(android.service.notification.StatusBarNotification r6, com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.portfolio.platform.service.FossilNotificationListenerService.g
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.portfolio.platform.service.FossilNotificationListenerService$g r0 = (com.portfolio.platform.service.FossilNotificationListenerService.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.portfolio.platform.service.FossilNotificationListenerService$g r0 = new com.portfolio.platform.service.FossilNotificationListenerService$g
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "statusBarNotification.packageName"
            r4 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r4) goto L_0x003a
            java.lang.Object r6 = r0.L$2
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r1 = r0.L$1
            android.service.notification.StatusBarNotification r1 = (android.service.notification.StatusBarNotification) r1
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.service.FossilNotificationListenerService r0 = (com.portfolio.platform.service.FossilNotificationListenerService) r0
            com.fossil.t87.a(r7)
            r0 = r7
            r7 = r6
            r6 = r1
            goto L_0x0072
        L_0x003a:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0042:
            com.fossil.t87.a(r7)
            com.fossil.nx6 r7 = com.fossil.nx6.b
            java.lang.String r2 = r6.getPackageName()
            com.fossil.ee7.a(r2, r3)
            boolean r7 = r7.a(r2)
            if (r7 == 0) goto L_0x00b5
            boolean r7 = com.fossil.yx6.b()
            if (r7 == 0) goto L_0x005d
            java.lang.String r7 = "DND"
            goto L_0x005f
        L_0x005d:
            java.lang.String r7 = "Silence"
        L_0x005f:
            com.portfolio.platform.data.source.UserRepository r2 = r5.r
            if (r2 == 0) goto L_0x00ae
            r0.L$0 = r5
            r0.L$1 = r6
            r0.L$2 = r7
            r0.label = r4
            java.lang.Object r0 = r2.getCurrentUser(r0)
            if (r0 != r1) goto L_0x0072
            return r1
        L_0x0072:
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            if (r0 == 0) goto L_0x0084
            java.lang.String r0 = r0.getUserId()
            java.lang.String r2 = "user_id"
            r1.put(r2, r0)
        L_0x0084:
            java.lang.String r0 = r6.getPackageName()
            com.fossil.ee7.a(r0, r3)
            java.lang.String r2 = "app_id"
            r1.put(r2, r0)
            long r2 = r6.getPostTime()
            java.lang.String r6 = java.lang.String.valueOf(r2)
            java.lang.String r0 = "post_time"
            r1.put(r0, r6)
            java.lang.String r6 = "blocked_by"
            r1.put(r6, r7)
            com.fossil.qd5$a r6 = com.fossil.qd5.f
            com.fossil.qd5 r6 = r6.c()
            java.lang.String r7 = "notification_blocked"
            r6.a(r7, r1)
            goto L_0x00b5
        L_0x00ae:
            java.lang.String r6 = "mUserRepository"
            com.fossil.ee7.d(r6)
            r6 = 0
            throw r6
        L_0x00b5:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.b(android.service.notification.StatusBarNotification, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final boolean a(StatusBarNotification statusBarNotification, List<? extends StatusBarNotification> list) {
        boolean z = true;
        if (b(statusBarNotification) && Build.VERSION.SDK_INT >= 21) {
            int size = list.size();
            boolean z2 = true;
            for (int i2 = 0; i2 < size; i2++) {
                String groupKey = statusBarNotification.getGroupKey();
                String groupKey2 = ((StatusBarNotification) list.get(i2)).getGroupKey();
                Notification notification = statusBarNotification.getNotification();
                ee7.a((Object) notification, "notification.notification");
                String group = notification.getGroup();
                Notification notification2 = ((StatusBarNotification) list.get(i2)).getNotification();
                ee7.a((Object) notification2, "filterList[j].notification");
                String group2 = notification2.getGroup();
                if (!(groupKey == null || groupKey2 == null || !mh7.b(groupKey, groupKey2, true))) {
                    z2 = false;
                }
                if (!(group == null || group2 == null || !mh7.b(group, group2, true))) {
                    z2 = false;
                }
            }
            z = z2;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "handleSummaryNotification() - isAlone = " + z);
        return z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(android.service.notification.StatusBarNotification r20, com.fossil.fb7<? super java.lang.Boolean> r21) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            r0 = r21
            boolean r3 = r0 instanceof com.portfolio.platform.service.FossilNotificationListenerService.f
            if (r3 == 0) goto L_0x0019
            r3 = r0
            com.portfolio.platform.service.FossilNotificationListenerService$f r3 = (com.portfolio.platform.service.FossilNotificationListenerService.f) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.service.FossilNotificationListenerService$f r3 = new com.portfolio.platform.service.FossilNotificationListenerService$f
            r3.<init>(r1, r0)
        L_0x001e:
            java.lang.Object r0 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 0
            r7 = 1
            if (r5 == 0) goto L_0x0057
            if (r5 != r7) goto L_0x004f
            java.lang.Object r2 = r3.L$6
            com.portfolio.platform.service.FossilNotificationListenerService$b r2 = (com.portfolio.platform.service.FossilNotificationListenerService.b) r2
            long r4 = r3.J$0
            java.lang.Object r2 = r3.L$5
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r3.L$4
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r3.L$3
            android.os.Bundle r2 = (android.os.Bundle) r2
            java.lang.Object r2 = r3.L$2
            android.app.Notification r2 = (android.app.Notification) r2
            java.lang.Object r2 = r3.L$1
            android.service.notification.StatusBarNotification r2 = (android.service.notification.StatusBarNotification) r2
            java.lang.Object r2 = r3.L$0
            com.portfolio.platform.service.FossilNotificationListenerService r2 = (com.portfolio.platform.service.FossilNotificationListenerService) r2
            com.fossil.t87.a(r0)
            goto L_0x014c
        L_0x004f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0057:
            com.fossil.t87.a(r0)
            if (r2 != 0) goto L_0x006e
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.service.FossilNotificationListenerService.s
            java.lang.String r3 = "processNotification() - sbn is null "
            r0.d(r2, r3)
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            return r0
        L_0x006e:
            android.app.Notification r0 = r20.getNotification()
            if (r0 != 0) goto L_0x0086
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.service.FossilNotificationListenerService.s
            java.lang.String r3 = "processNotification() - sbn.getNotification() is null "
            r0.d(r2, r3)
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            return r0
        L_0x0086:
            android.app.Notification r5 = r20.getNotification()
            android.os.Bundle r5 = r5.extras
            if (r5 == 0) goto L_0x01d2
            android.app.Notification r5 = r20.getNotification()
            android.os.Bundle r5 = r5.extras
            java.lang.String r8 = "android.progressMax"
            int r5 = r5.getInt(r8, r6)
            if (r5 == 0) goto L_0x009e
            goto L_0x01d2
        L_0x009e:
            r19.c(r20)
            android.os.Bundle r5 = r1.a(r0)
            java.lang.String r8 = ""
            java.lang.String r9 = "android.title"
            java.lang.CharSequence r9 = r5.getCharSequence(r9, r8)
            java.lang.String r12 = r9.toString()
            java.lang.StringBuilder r9 = r1.a
            java.lang.String r9 = r9.toString()
            java.lang.String r10 = "mStringBuilder.toString()"
            com.fossil.ee7.a(r9, r10)
            int r10 = r9.length()
            int r10 = r10 - r7
            r11 = 0
            r13 = 0
        L_0x00c3:
            if (r11 > r10) goto L_0x00f4
            if (r13 != 0) goto L_0x00c9
            r14 = r11
            goto L_0x00ca
        L_0x00c9:
            r14 = r10
        L_0x00ca:
            char r14 = r9.charAt(r14)
            java.lang.Character r14 = com.fossil.pb7.a(r14)
            char r14 = r14.charValue()
            r15 = 32
            if (r14 > r15) goto L_0x00dc
            r14 = 1
            goto L_0x00dd
        L_0x00dc:
            r14 = 0
        L_0x00dd:
            java.lang.Boolean r14 = com.fossil.pb7.a(r14)
            boolean r14 = r14.booleanValue()
            if (r13 != 0) goto L_0x00ee
            if (r14 != 0) goto L_0x00eb
            r13 = 1
            goto L_0x00c3
        L_0x00eb:
            int r11 = r11 + 1
            goto L_0x00c3
        L_0x00ee:
            if (r14 != 0) goto L_0x00f1
            goto L_0x00f4
        L_0x00f1:
            int r10 = r10 + -1
            goto L_0x00c3
        L_0x00f4:
            int r10 = r10 + r7
            java.lang.CharSequence r9 = r9.subSequence(r11, r10)
            java.lang.String r9 = r9.toString()
            long r10 = java.lang.System.currentTimeMillis()
            com.portfolio.platform.service.FossilNotificationListenerService$b r15 = new com.portfolio.platform.service.FossilNotificationListenerService$b
            java.lang.String r14 = r20.getPackageName()
            java.lang.String r13 = "sbn.packageName"
            com.fossil.ee7.a(r14, r13)
            int r16 = r20.getId()
            r13 = r15
            r7 = r15
            r15 = r9
            r17 = r10
            r13.<init>(r14, r15, r16, r17)
            boolean r13 = r1.a(r7)
            if (r13 == 0) goto L_0x0123
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            return r0
        L_0x0123:
            android.util.SparseArray<com.portfolio.platform.service.FossilNotificationListenerService$b> r13 = r1.b
            int r14 = r7.hashCode()
            r13.put(r14, r7)
            boolean r13 = r19.d()
            if (r13 == 0) goto L_0x0151
            r3.L$0 = r1
            r3.L$1 = r2
            r3.L$2 = r0
            r3.L$3 = r5
            r3.L$4 = r12
            r3.L$5 = r9
            r3.J$0 = r10
            r3.L$6 = r7
            r5 = 1
            r3.label = r5
            java.lang.Object r0 = r1.b(r2, r3)
            if (r0 != r4) goto L_0x014c
            return r4
        L_0x014c:
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            return r0
        L_0x0151:
            android.content.pm.PackageManager r0 = r19.getPackageManager()
            java.lang.String r3 = r20.getPackageName()     // Catch:{ Exception -> 0x0171 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r3 = r0.getApplicationInfo(r3, r4)     // Catch:{ Exception -> 0x0171 }
            java.lang.CharSequence r0 = r0.getApplicationLabel(r3)     // Catch:{ Exception -> 0x0171 }
            if (r0 == 0) goto L_0x0169
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0171 }
            r15 = r0
            goto L_0x018f
        L_0x0169:
            com.fossil.x87 r0 = new com.fossil.x87     // Catch:{ Exception -> 0x0171 }
            java.lang.String r3 = "null cannot be cast to non-null type kotlin.String"
            r0.<init>(r3)     // Catch:{ Exception -> 0x0171 }
            throw r0     // Catch:{ Exception -> 0x0171 }
        L_0x0171:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.service.FossilNotificationListenerService.s
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "processNotification() - Could not find app name - ex = "
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r3.e(r4, r0)
            r15 = r8
        L_0x018f:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = com.portfolio.platform.service.FossilNotificationListenerService.s
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "processNotification() - We are going to analyze this notification from: "
            r4.append(r5)
            r4.append(r15)
            java.lang.String r5 = ", string = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r4 = r4.toString()
            r0.d(r3, r4)
            com.portfolio.platform.data.model.NotificationInfo r0 = new com.portfolio.platform.data.model.NotificationInfo
            com.portfolio.platform.data.NotificationSource r11 = com.portfolio.platform.data.NotificationSource.OS
            java.lang.String r14 = r20.getPackageName()
            r10 = r0
            r13 = r9
            r10.<init>(r11, r12, r13, r14, r15)
            com.fossil.sg5$a r2 = com.fossil.sg5.i
            com.fossil.sg5 r2 = r2.a()
            r2.a(r0)
            r19.a()
            r2 = 1
            java.lang.Boolean r0 = com.fossil.pb7.a(r2)
            return r0
        L_0x01d2:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.service.FossilNotificationListenerService.s
            java.lang.String r3 = "processNotification() - Ignoring Progress notification"
            r0.d(r2, r3)
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.FossilNotificationListenerService.a(android.service.notification.StatusBarNotification, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Bundle a(Notification notification) {
        FLogger.INSTANCE.getLocal().d(s, "collectTextInfo()");
        Bundle bundle = notification.extras;
        this.a.setLength(0);
        this.d.clear();
        String[] strArr = u;
        for (String str : strArr) {
            Object obj = bundle.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = s;
            StringBuilder sb = new StringBuilder();
            sb.append("  Looking at ");
            sb.append(str);
            sb.append(" : ");
            sb.append(obj);
            sb.append(" : class ");
            sb.append(obj != null ? obj.getClass().getSimpleName() : "null");
            local.d(str2, sb.toString());
            if ((obj instanceof CharSequence) && !TextUtils.isEmpty((CharSequence) obj)) {
                FLogger.INSTANCE.getLocal().d(s, "  Adding " + str + " with value of " + obj);
                a(obj.toString());
            }
        }
        FLogger.INSTANCE.getLocal().d(s, "  Ticker Text = " + notification.tickerText);
        if (!TextUtils.isEmpty(notification.tickerText)) {
            a(notification.tickerText.toString());
        }
        ee7.a((Object) bundle, AppLinkData.ARGUMENTS_EXTRAS_KEY);
        return bundle;
    }

    @DexIgnore
    public final boolean a(b bVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "isBlockedByIntervalTime() - thisNotification.hashCode() = " + bVar.hashCode());
        b bVar2 = this.b.get(bVar.hashCode());
        if (bVar2 == null) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(s, "isBlockedByIntervalTime() - Have an old notification with the same hash");
        if (bVar.a() - bVar2.a() > ButtonService.CONNECT_TIMEOUT) {
            FLogger.INSTANCE.getLocal().d(s, "isBlockedByIntervalTime() - Older than 10000 milliseconds. Allow update to this notification");
            return false;
        }
        long abs = Math.abs(ButtonService.CONNECT_TIMEOUT - (bVar.a() - bVar2.a()));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local2.d(str2, "isBlockedByIntervalTime() - Block duplicate in " + zs7.a(abs) + ", notification: " + bVar);
        return true;
    }

    @DexIgnore
    public final void a() {
        long currentTimeMillis = System.currentTimeMillis();
        int size = this.b.size();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "cleanPastNotificationMap() - Size = " + size);
        LinkedList linkedList = new LinkedList();
        for (int i2 = 0; i2 < size; i2++) {
            int keyAt = this.b.keyAt(i2);
            if (this.b.get(keyAt) != null && currentTimeMillis - this.b.get(keyAt).a() > ButtonService.CONNECT_TIMEOUT) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = s;
                local2.d(str2, "cleanPastNotificationMap() - Adding key to remove - key = " + keyAt);
                linkedList.add(Integer.valueOf(keyAt));
            }
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            FLogger.INSTANCE.getLocal().d(s, "cleanPastNotificationMap() - Dumping old notification");
            SparseArray<b> sparseArray = this.b;
            ee7.a((Object) num, "keyInt");
            sparseArray.remove(num.intValue());
        }
    }

    @DexIgnore
    public final void a(String str) {
        if (!this.d.contains(str)) {
            Iterator<String> it = this.d.iterator();
            while (it.hasNext()) {
                String next = it.next();
                ee7.a((Object) next, "oldString");
                if (nh7.a((CharSequence) next, (CharSequence) str, false, 2, (Object) null)) {
                    return;
                }
            }
            this.a.append(' ');
            this.a.append(str);
            this.d.add(str);
        }
    }
}
