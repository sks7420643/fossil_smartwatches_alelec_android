package com.portfolio.platform.service.fcm;

import com.fossil.ch5;
import com.fossil.co6;
import com.fossil.dl7;
import com.fossil.ed4;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.lm4;
import com.fossil.nb7;
import com.fossil.pb7;
import com.fossil.qj7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.tn4;
import com.fossil.to4;
import com.fossil.uj5;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public co6 g;
    @DexIgnore
    public uj5 h;
    @DexIgnore
    public ch5 i;
    @DexIgnore
    public to4 j;
    @DexIgnore
    public lm4 p;
    @DexIgnore
    public yi7 q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$onMessageReceived$1", f = "FossilFirebaseMessagingService.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ed4 $remoteMessage;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends TypeToken<tn4> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FossilFirebaseMessagingService fossilFirebaseMessagingService, ed4 ed4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fossilFirebaseMessagingService;
            this.$remoteMessage = ed4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$remoteMessage, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                Boolean a2 = this.this$0.f().a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("FossilFirebaseMessagingService", "onMessageReceived - isBCOn : " + a2);
                ee7.a((Object) a2, "isBcOn");
                if (a2.booleanValue()) {
                    try {
                        String obj2 = this.$remoteMessage.e().toString();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.e("FossilFirebaseMessagingService", "dataStr: " + obj2);
                        tn4 tn4 = (tn4) new Gson().a(obj2, new a().getType());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("isActive: ");
                        yi7 a3 = this.this$0.q;
                        sb.append(a3 != null ? pb7.a(zi7.b(a3)) : null);
                        sb.append(" - notification: ");
                        sb.append(tn4);
                        local3.e("FossilFirebaseMessagingService", sb.toString());
                        if (tn4 != null) {
                            this.this$0.d().a(tn4);
                            this.this$0.e().a(tn4, this.this$0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$registerFCM$1", f = "FossilFirebaseMessagingService.kt", l = {131}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FossilFirebaseMessagingService fossilFirebaseMessagingService, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fossilFirebaseMessagingService;
            this.$token = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$token, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                to4 c = this.this$0.c();
                String str = this.$token;
                this.L$0 = yi7;
                this.label = 1;
                if (c.a(str, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final to4 c() {
        to4 to4 = this.j;
        if (to4 != null) {
            return to4;
        }
        ee7.d("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final uj5 d() {
        uj5 uj5 = this.h;
        if (uj5 != null) {
            return uj5;
        }
        ee7.d("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final co6 e() {
        co6 co6 = this.g;
        if (co6 != null) {
            return co6;
        }
        ee7.d("mInAppNotificationManager");
        throw null;
    }

    @DexIgnore
    public final ch5 f() {
        ch5 ch5 = this.i;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.q = zi7.a(zi7.a(qj7.b()), dl7.a(null, 1, null));
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    @Override // com.fossil.uc4
    public void onDestroy() {
        super.onDestroy();
        yi7 yi7 = this.q;
        if (yi7 != null) {
            zi7.a(yi7, null, 1, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void a(ed4 ed4) {
        yi7 yi7;
        ee7.b(ed4, "remoteMessage");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "onMessageReceived(), from:  " + ed4.g() + ' ' + "- priority: " + ed4.w() + " - notification: " + ed4.v() + " - messageData: " + ed4.e());
        Map<String, String> e = ed4.e();
        ee7.a((Object) e, "remoteMessage.data");
        if ((!e.isEmpty()) && (yi7 = this.q) != null) {
            ik7 unused = xh7.b(yi7, qj7.b(), null, new b(this, ed4, null), 2, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void d(String str) {
        ee7.b(str, "refreshedToken");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "Refreshed token: " + str);
        ch5 ch5 = this.i;
        if (ch5 != null) {
            String j2 = ch5.j();
            if (j2 == null || j2.length() == 0) {
                ch5 ch52 = this.i;
                if (ch52 != null) {
                    ch52.t(str);
                    ch5 ch53 = this.i;
                    if (ch53 == null) {
                        ee7.d("mSharedPrefs");
                        throw null;
                    } else if (ch53.f0()) {
                        e(str);
                    }
                } else {
                    ee7.d("mSharedPrefs");
                    throw null;
                }
            } else {
                ch5 ch54 = this.i;
                if (ch54 != null) {
                    ch54.t(str);
                    e(str);
                    return;
                }
                ee7.d("mSharedPrefs");
                throw null;
            }
        } else {
            ee7.d("mSharedPrefs");
            throw null;
        }
    }

    @DexIgnore
    public final void e(String str) {
        yi7 yi7 = this.q;
        if (yi7 != null) {
            ik7 unused = xh7.b(yi7, qj7.b(), null, new c(this, str, null), 2, null);
        }
    }
}
