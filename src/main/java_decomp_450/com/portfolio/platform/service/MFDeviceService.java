package com.portfolio.platform.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import com.fossil.ah5;
import com.fossil.bj5;
import com.fossil.ch5;
import com.fossil.cw6;
import com.fossil.eb5;
import com.fossil.ee7;
import com.fossil.ek5;
import com.fossil.fb7;
import com.fossil.ff5;
import com.fossil.fitness.FitnessData;
import com.fossil.fl4;
import com.fossil.ge5;
import com.fossil.gl4;
import com.fossil.he5;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.jc5;
import com.fossil.kc5;
import com.fossil.kd7;
import com.fossil.mh7;
import com.fossil.nb7;
import com.fossil.nj4;
import com.fossil.nj5;
import com.fossil.nw6;
import com.fossil.oc5;
import com.fossil.pb5;
import com.fossil.pb7;
import com.fossil.pc5;
import com.fossil.pj4;
import com.fossil.px6;
import com.fossil.qd5;
import com.fossil.qe;
import com.fossil.qh5;
import com.fossil.qj7;
import com.fossil.rx6;
import com.fossil.sh5;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.uj5;
import com.fossil.vg5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wk5;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi5;
import com.fossil.zi7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.receiver.RestartServiceReceiver;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFDeviceService extends Service {
    @DexIgnore
    public static /* final */ String X;
    @DexIgnore
    public static /* final */ String Y;
    @DexIgnore
    public static /* final */ a Z; // = new a(null);
    @DexIgnore
    public nw6 A;
    @DexIgnore
    public ge5 B;
    @DexIgnore
    public ff5 C;
    @DexIgnore
    public uj5 D;
    @DexIgnore
    public wk5 E;
    @DexIgnore
    public ek5 F;
    @DexIgnore
    public MisfitDeviceProfile G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public /* final */ sh5 I; // = new sh5();
    @DexIgnore
    public /* final */ Gson J; // = new Gson();
    @DexIgnore
    public /* final */ i K; // = new i(this);
    @DexIgnore
    public boolean L;
    @DexIgnore
    public Handler M;
    @DexIgnore
    public /* final */ Runnable N; // = new m(this);
    @DexIgnore
    public /* final */ d O; // = new d(this);
    @DexIgnore
    public /* final */ e P; // = new e(this);
    @DexIgnore
    public /* final */ k Q; // = new k();
    @DexIgnore
    public /* final */ o R; // = new o();
    @DexIgnore
    public /* final */ j S; // = new j();
    @DexIgnore
    public /* final */ f T; // = new f();
    @DexIgnore
    public /* final */ h U; // = new h();
    @DexIgnore
    public /* final */ n V; // = new n();
    @DexIgnore
    public /* final */ c W; // = new c(this);
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public ch5 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public ActivitiesRepository d;
    @DexIgnore
    public SummariesRepository e;
    @DexIgnore
    public SleepSessionsRepository f;
    @DexIgnore
    public SleepSummariesRepository g;
    @DexIgnore
    public UserRepository h;
    @DexIgnore
    public HybridPresetRepository i;
    @DexIgnore
    public MicroAppRepository j;
    @DexIgnore
    public HeartRateSampleRepository p;
    @DexIgnore
    public HeartRateSummaryRepository q;
    @DexIgnore
    public WorkoutSessionRepository r;
    @DexIgnore
    public FitnessDataRepository s;
    @DexIgnore
    public GoalTrackingRepository t;
    @DexIgnore
    public pj4 u;
    @DexIgnore
    public qd5 v;
    @DexIgnore
    public PortfolioApp w;
    @DexIgnore
    public vg5 x;
    @DexIgnore
    public ThirdPartyRepository y;
    @DexIgnore
    public cw6 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFDeviceService.Y;
        }

        @DexIgnore
        public final String b() {
            return MFDeviceService.X;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Binder {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final MFDeviceService a() {
            return MFDeviceService.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            if (intent != null) {
                String stringExtra = intent.getStringExtra("message");
                MFDeviceService mFDeviceService = this.a;
                ee7.a((Object) stringExtra, "message");
                mFDeviceService.a(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {239, 291, 292, 321, 331, 336, 342, 350, 356, 363, 383, 393, 438, 477, 522, 540}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public int I$4;
            @DexIgnore
            public int I$5;
            @DexIgnore
            public int I$6;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public long J$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$a")
            @tb7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$10", f = "MFDeviceService.kt", l = {635}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$a  reason: collision with other inner class name */
            public static final class C0302a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public /* final */ /* synthetic */ String $uiVersion;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0302a(a aVar, String str, String str2, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$uiVersion = str;
                    this.$serial = str2;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0302a aVar = new C0302a(this.this$0, this.$uiVersion, this.$serial, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0302a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    PortfolioApp portfolioApp;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        PortfolioApp c = this.this$0.this$0.a.c();
                        String str = this.$uiVersion;
                        this.L$0 = yi7;
                        this.L$1 = c;
                        this.label = 1;
                        obj = c.a(str, this);
                        if (obj == a) {
                            return a;
                        }
                        portfolioApp = c;
                    } else if (i == 1) {
                        portfolioApp = (PortfolioApp) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    boolean booleanValue = ((Boolean) obj).booleanValue();
                    String str2 = this.$serial;
                    ee7.a((Object) str2, "serial");
                    portfolioApp.b(str2, booleanValue);
                    return i97.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b implements fl4.e<nw6.d, nw6.c> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public b(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                /* renamed from: a */
                public void onSuccess(nw6.d dVar) {
                    ee7.b(dVar, "responseValue");
                    PortfolioApp c = this.a.this$0.a.c();
                    String str = this.b;
                    ee7.a((Object) str, "serial");
                    c.b(str, dVar.a());
                }

                @DexIgnore
                public void a(nw6.c cVar) {
                    ee7.b(cVar, "errorValue");
                    PortfolioApp c = this.a.this$0.a.c();
                    String str = this.b;
                    ee7.a((Object) str, "serial");
                    c.b(str, (String) null);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$8", f = "MFDeviceService.kt", l = {574}, m = "invokeSuspend")
            public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, String str, String str2, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, this.$serial, this.$secretKey, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        DeviceRepository e = this.this$0.this$0.a.e();
                        String str = this.$serial;
                        if (str != null) {
                            String str2 = this.$secretKey;
                            if (str2 != null) {
                                this.L$0 = yi7;
                                this.label = 1;
                                obj = e.updateDeviceSecretKey(str, str2, this);
                                if (obj == a) {
                                    return a;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (((zi5) obj) instanceof bj5) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String b = MFDeviceService.Z.b();
                        local.d(b, "update secret key " + this.$secretKey + " for " + this.$serial + " to server success");
                        PortfolioApp.g0.c().c(this.$serial, true);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.Z.b();
                        local2.d(b2, "update secret key " + this.$secretKey + " for " + this.$serial + " to server failed");
                        PortfolioApp.g0.c().c(this.$serial, false);
                    }
                    return i97.a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$d")
            @tb7(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$9", f = "MFDeviceService.kt", l = {599, 603}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$d  reason: collision with other inner class name */
            public static final class C0303d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0303d(a aVar, String str, String str2, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0303d dVar = new C0303d(this.this$0, this.$serial, this.$secretKey, fb7);
                    dVar.p$ = (yi7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0303d) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        UserRepository t = this.this$0.this$0.a.t();
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = t.getCurrentUser(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        String str = (String) this.L$3;
                        MFUser mFUser = (MFUser) this.L$2;
                        MFUser mFUser2 = (MFUser) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                        fl4.c cVar = (fl4.c) obj;
                        return i97.a;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    MFUser mFUser3 = (MFUser) obj;
                    if (mFUser3 != null) {
                        String str2 = mFUser3.getUserId() + ':' + this.$serial;
                        cw6 f = this.this$0.this$0.a.f();
                        pb5 pb5 = new pb5();
                        String str3 = this.$secretKey;
                        if (str3 != null) {
                            cw6.b bVar = new cw6.b(str2, pb5, str3);
                            this.L$0 = yi7;
                            this.L$1 = mFUser3;
                            this.L$2 = mFUser3;
                            this.L$3 = str2;
                            this.label = 2;
                            obj = gl4.a(f, bVar, this);
                            if (obj == a) {
                                return a;
                            }
                            fl4.c cVar2 = (fl4.c) obj;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    return i97.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class e extends zb7 implements kd7<yi7, fb7<? super zi5<Void>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ int $batteryLevel$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ Device $device$inlined;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public e(fb7 fb7, a aVar, int i, Device device) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$batteryLevel$inlined = i;
                    this.$device$inlined = device;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    e eVar = new e(fb7, this.this$0, this.$batteryLevel$inlined, this.$device$inlined);
                    eVar.p$ = (yi7) obj;
                    return eVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super zi5<Void>> fb7) {
                    return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        DeviceRepository e = this.this$0.this$0.a.e();
                        Device device = this.$device$inlined;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = e.updateDevice(device, true, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Intent intent, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$intent, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:179:0x0b0d  */
            /* JADX WARNING: Removed duplicated region for block: B:181:0x0b18  */
            /* JADX WARNING: Removed duplicated region for block: B:242:0x0e03 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:243:0x0e04  */
            /* JADX WARNING: Removed duplicated region for block: B:363:0x11d9 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:364:0x11da  */
            /* JADX WARNING: Removed duplicated region for block: B:498:0x1605 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:499:0x1606  */
            /* JADX WARNING: Removed duplicated region for block: B:558:0x1854  */
            /* JADX WARNING: Removed duplicated region for block: B:594:0x190b  */
            /* JADX WARNING: Removed duplicated region for block: B:659:0x1b2b A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:662:0x1b44  */
            /* JADX WARNING: Removed duplicated region for block: B:664:0x1bb4  */
            /* JADX WARNING: Removed duplicated region for block: B:666:0x1c05  */
            /* JADX WARNING: Removed duplicated region for block: B:669:0x1c9b A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:73:0x0749  */
            /* JADX WARNING: Removed duplicated region for block: B:74:0x0754  */
            /* JADX WARNING: Removed duplicated region for block: B:91:0x07f0  */
            /* JADX WARNING: Removed duplicated region for block: B:96:0x0810  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r61) {
                /*
                    r60 = this;
                    r15 = r60
                    java.lang.Object r14 = com.fossil.nb7.a()
                    int r0 = r15.label
                    java.lang.String r1 = "ota_session"
                    java.lang.String r2 = "OTA_RESULT"
                    java.lang.String r12 = "sync_session"
                    java.lang.String r11 = "Inside "
                    java.lang.String r6 = ""
                    java.lang.String r7 = "serial"
                    switch(r0) {
                        case 0: goto L_0x050c;
                        case 1: goto L_0x04eb;
                        case 2: goto L_0x0491;
                        case 3: goto L_0x0428;
                        case 4: goto L_0x0376;
                        case 5: goto L_0x02da;
                        case 6: goto L_0x024d;
                        case 7: goto L_0x01c3;
                        case 8: goto L_0x014c;
                        case 9: goto L_0x00fb;
                        case 10: goto L_0x00ce;
                        case 11: goto L_0x00d2;
                        case 12: goto L_0x00da;
                        case 13: goto L_0x00a9;
                        case 14: goto L_0x0068;
                        case 15: goto L_0x0046;
                        case 16: goto L_0x0020;
                        default: goto L_0x0017;
                    }
                L_0x0017:
                    r7 = r15
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r15.L$6
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
                    java.lang.Object r0 = r15.L$4
                    android.os.Bundle r0 = (android.os.Bundle) r0
                    java.lang.Object r0 = r15.L$3
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r1 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r1
                    java.lang.Object r1 = r15.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r61)
                    r5 = r0
                    r31 = r6
                    r0 = r61
                    goto L_0x07ca
                L_0x0046:
                    java.lang.Object r0 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
                    java.lang.Object r0 = r15.L$4
                    android.os.Bundle r0 = (android.os.Bundle) r0
                    java.lang.Object r0 = r15.L$3
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r1 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r1
                    java.lang.Object r1 = r15.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r61)
                    r5 = r0
                    r31 = r6
                    r0 = r61
                    goto L_0x0743
                L_0x0068:
                    java.lang.Object r0 = r15.L$12
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r15.L$11
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r15.L$10
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$9
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$8
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r3 = r15.L$7
                    com.fossil.if5 r3 = (com.fossil.if5) r3
                    java.lang.Object r4 = r15.L$6
                    android.content.Intent r4 = (android.content.Intent) r4
                    java.lang.Object r5 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r5 = (com.portfolio.platform.data.model.SKUModel) r5
                    java.lang.Object r5 = r15.L$4
                    android.os.Bundle r5 = (android.os.Bundle) r5
                    java.lang.Object r5 = r15.L$3
                    java.util.ArrayList r5 = (java.util.ArrayList) r5
                    java.lang.Object r5 = r15.L$2
                    java.lang.String r5 = (java.lang.String) r5
                    java.lang.Object r7 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r7 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r7
                    java.lang.Object r7 = r15.L$0
                    com.fossil.yi7 r7 = (com.fossil.yi7) r7
                    com.fossil.t87.a(r61)
                    r25 = r1
                    r24 = r2
                    r31 = r6
                    r1 = r61
                    goto L_0x0adb
                L_0x00a9:
                    java.lang.Object r0 = r15.L$7
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r15.L$6
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
                    java.lang.Object r0 = r15.L$4
                    android.os.Bundle r0 = (android.os.Bundle) r0
                    java.lang.Object r0 = r15.L$3
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r0
                    java.lang.Object r0 = r15.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r61)
                    goto L_0x0c02
                L_0x00ce:
                    java.lang.Object r0 = r15.L$9
                    java.lang.Exception r0 = (java.lang.Exception) r0
                L_0x00d2:
                    java.lang.Object r0 = r15.L$8
                    com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                    java.lang.Object r0 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
                L_0x00da:
                    java.lang.Object r0 = r15.L$6
                    com.fossil.if5 r0 = (com.fossil.if5) r0
                    java.lang.Object r0 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
                    java.lang.Object r0 = r15.L$4
                    android.os.Bundle r0 = (android.os.Bundle) r0
                    java.lang.Object r0 = r15.L$3
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r0
                    java.lang.Object r0 = r15.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r61)
                    goto L_0x019a
                L_0x00fb:
                    java.lang.Object r0 = r15.L$10
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r15.L$9
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r15.L$8
                    r1 = r0
                    com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                    java.lang.Object r0 = r15.L$7
                    r2 = r0
                    com.misfit.frameworks.buttonservice.model.UserProfile r2 = (com.misfit.frameworks.buttonservice.model.UserProfile) r2
                    long r3 = r15.J$1
                    long r5 = r15.J$0
                    java.lang.Object r0 = r15.L$6
                    r7 = r0
                    com.fossil.if5 r7 = (com.fossil.if5) r7
                    int r8 = r15.I$5
                    int r9 = r15.I$4
                    int r11 = r15.I$3
                    java.lang.Object r0 = r15.L$5
                    r16 = r0
                    com.portfolio.platform.data.model.SKUModel r16 = (com.portfolio.platform.data.model.SKUModel) r16
                    java.lang.Object r0 = r15.L$4
                    r17 = r0
                    android.os.Bundle r17 = (android.os.Bundle) r17
                    java.lang.Object r0 = r15.L$3
                    r18 = r0
                    java.util.ArrayList r18 = (java.util.ArrayList) r18
                    int r10 = r15.I$2
                    int r13 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r21 = r0
                    java.lang.String r21 = (java.lang.String) r21
                    java.lang.Object r0 = r15.L$1
                    r22 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r22 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r22
                    r23 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r24 = r0
                    com.fossil.yi7 r24 = (com.fossil.yi7) r24
                L_0x0148:
                    com.fossil.t87.a(r61)     // Catch:{ Exception -> 0x019f }
                    goto L_0x019a
                L_0x014c:
                    java.lang.Object r0 = r15.L$10
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r15.L$9
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r15.L$8
                    r1 = r0
                    com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                    java.lang.Object r0 = r15.L$7
                    r2 = r0
                    com.misfit.frameworks.buttonservice.model.UserProfile r2 = (com.misfit.frameworks.buttonservice.model.UserProfile) r2
                    long r3 = r15.J$1
                    long r5 = r15.J$0
                    java.lang.Object r0 = r15.L$6
                    r7 = r0
                    com.fossil.if5 r7 = (com.fossil.if5) r7
                    int r8 = r15.I$5
                    int r9 = r15.I$4
                    int r11 = r15.I$3
                    java.lang.Object r0 = r15.L$5
                    r16 = r0
                    com.portfolio.platform.data.model.SKUModel r16 = (com.portfolio.platform.data.model.SKUModel) r16
                    java.lang.Object r0 = r15.L$4
                    r17 = r0
                    android.os.Bundle r17 = (android.os.Bundle) r17
                    java.lang.Object r0 = r15.L$3
                    r18 = r0
                    java.util.ArrayList r18 = (java.util.ArrayList) r18
                    int r10 = r15.I$2
                    int r13 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r21 = r0
                    java.lang.String r21 = (java.lang.String) r21
                    java.lang.Object r0 = r15.L$1
                    r22 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r22 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r22
                    r23 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r24 = r0
                    com.fossil.yi7 r24 = (com.fossil.yi7) r24
                    goto L_0x0148
                L_0x019a:
                    r52 = r12
                    r7 = r15
                    goto L_0x1e42
                L_0x019f:
                    r0 = move-exception
                    r61 = r0
                    r0 = r10
                    r52 = r12
                    r53 = r14
                    r14 = r7
                    r10 = r8
                    r7 = r15
                    r8 = r18
                    r15 = r13
                    r12 = r5
                    r6 = r21
                    r5 = r24
                    r24 = r3
                    r3 = r17
                    r4 = r22
                    r56 = r2
                    r2 = r1
                    r1 = r16
                    r16 = r23
                    r23 = r56
                    goto L_0x1a70
                L_0x01c3:
                    java.lang.Object r0 = r15.L$12
                    com.fossil.sk5$a r0 = (com.fossil.sk5.a) r0
                    java.lang.Object r0 = r15.L$11
                    java.util.List r0 = (java.util.List) r0
                    int r0 = r15.I$6
                    java.lang.Object r1 = r15.L$10
                    java.util.List r1 = (java.util.List) r1
                    java.lang.Object r2 = r15.L$9
                    java.util.List r2 = (java.util.List) r2
                    java.lang.Object r3 = r15.L$8
                    com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
                    java.lang.Object r4 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r4 = (com.misfit.frameworks.buttonservice.model.UserProfile) r4
                    long r5 = r15.J$1
                    long r7 = r15.J$0
                    java.lang.Object r10 = r15.L$6
                    com.fossil.if5 r10 = (com.fossil.if5) r10
                    int r13 = r15.I$5
                    int r9 = r15.I$4
                    r17 = r1
                    int r1 = r15.I$3
                    r18 = r0
                    java.lang.Object r0 = r15.L$5
                    r21 = r0
                    com.portfolio.platform.data.model.SKUModel r21 = (com.portfolio.platform.data.model.SKUModel) r21
                    java.lang.Object r0 = r15.L$4
                    r22 = r0
                    android.os.Bundle r22 = (android.os.Bundle) r22
                    java.lang.Object r0 = r15.L$3
                    r23 = r0
                    java.util.ArrayList r23 = (java.util.ArrayList) r23
                    r24 = r1
                    int r1 = r15.I$2
                    r25 = r1
                    int r1 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r26 = r0
                    java.lang.String r26 = (java.lang.String) r26
                    java.lang.Object r0 = r15.L$1
                    r27 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r27 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r27
                    r28 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r29 = r0
                    com.fossil.yi7 r29 = (com.fossil.yi7) r29
                    com.fossil.t87.a(r61)     // Catch:{ Exception -> 0x0235 }
                    r51 = r11
                    r52 = r12
                    r11 = r14
                    r30 = r17
                    r0 = r18
                    r34 = r25
                    r56 = r24
                    r24 = r23
                    r23 = r56
                    goto L_0x1625
                L_0x0235:
                    r0 = move-exception
                    r61 = r0
                    r2 = r1
                    r16 = r3
                    r52 = r12
                    r53 = r14
                    r1 = r21
                    r3 = r22
                    r11 = r24
                    r0 = r25
                    r24 = r5
                    r14 = r10
                    r10 = r13
                    goto L_0x0366
                L_0x024d:
                    java.lang.Object r0 = r15.L$11
                    java.util.List r0 = (java.util.List) r0
                    int r1 = r15.I$6
                    java.lang.Object r2 = r15.L$10
                    java.util.List r2 = (java.util.List) r2
                    java.lang.Object r3 = r15.L$9
                    java.util.List r3 = (java.util.List) r3
                    java.lang.Object r4 = r15.L$8
                    com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
                    java.lang.Object r5 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r5 = (com.misfit.frameworks.buttonservice.model.UserProfile) r5
                    long r6 = r15.J$1
                    long r8 = r15.J$0
                    java.lang.Object r10 = r15.L$6
                    com.fossil.if5 r10 = (com.fossil.if5) r10
                    int r13 = r15.I$5
                    r17 = r1
                    int r1 = r15.I$4
                    r18 = r1
                    int r1 = r15.I$3
                    r21 = r0
                    java.lang.Object r0 = r15.L$5
                    r22 = r0
                    com.portfolio.platform.data.model.SKUModel r22 = (com.portfolio.platform.data.model.SKUModel) r22
                    java.lang.Object r0 = r15.L$4
                    r23 = r0
                    android.os.Bundle r23 = (android.os.Bundle) r23
                    java.lang.Object r0 = r15.L$3
                    r24 = r0
                    java.util.ArrayList r24 = (java.util.ArrayList) r24
                    r25 = r1
                    int r1 = r15.I$2
                    r26 = r1
                    int r1 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r27 = r0
                    java.lang.String r27 = (java.lang.String) r27
                    java.lang.Object r0 = r15.L$1
                    r28 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r28 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r28
                    r29 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r30 = r0
                    com.fossil.yi7 r30 = (com.fossil.yi7) r30
                    com.fossil.t87.a(r61)     // Catch:{ Exception -> 0x0400 }
                    r19 = r10
                    r51 = r11
                    r52 = r12
                    r20 = r13
                    r53 = r14
                    r11 = r15
                    r16 = r17
                    r0 = r21
                    r15 = r26
                    r61 = r27
                    r10 = r29
                    r14 = r30
                    r12 = r1
                    r17 = r2
                    r13 = r4
                    r21 = r18
                    r2 = r24
                    r1 = r28
                    r18 = r3
                    r3 = r23
                    r56 = r8
                    r9 = r5
                    r4 = r56
                    r8 = r22
                    r22 = r25
                    goto L_0x14e5
                L_0x02da:
                    java.lang.Object r0 = r15.L$12
                    com.fossil.rk5$a r0 = (com.fossil.rk5.a) r0
                    java.lang.Object r0 = r15.L$11
                    java.util.List r0 = (java.util.List) r0
                    int r0 = r15.I$6
                    java.lang.Object r1 = r15.L$10
                    java.util.List r1 = (java.util.List) r1
                    java.lang.Object r2 = r15.L$9
                    java.util.List r2 = (java.util.List) r2
                    java.lang.Object r3 = r15.L$8
                    com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
                    java.lang.Object r4 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r4 = (com.misfit.frameworks.buttonservice.model.UserProfile) r4
                    long r5 = r15.J$1
                    long r7 = r15.J$0
                    java.lang.Object r9 = r15.L$6
                    com.fossil.if5 r9 = (com.fossil.if5) r9
                    int r10 = r15.I$5
                    int r13 = r15.I$4
                    r17 = r1
                    int r1 = r15.I$3
                    r18 = r0
                    java.lang.Object r0 = r15.L$5
                    r21 = r0
                    com.portfolio.platform.data.model.SKUModel r21 = (com.portfolio.platform.data.model.SKUModel) r21
                    java.lang.Object r0 = r15.L$4
                    r22 = r0
                    android.os.Bundle r22 = (android.os.Bundle) r22
                    java.lang.Object r0 = r15.L$3
                    r23 = r0
                    java.util.ArrayList r23 = (java.util.ArrayList) r23
                    r24 = r1
                    int r1 = r15.I$2
                    r25 = r1
                    int r1 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r26 = r0
                    java.lang.String r26 = (java.lang.String) r26
                    java.lang.Object r0 = r15.L$1
                    r27 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r27 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r27
                    r28 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r29 = r0
                    com.fossil.yi7 r29 = (com.fossil.yi7) r29
                    com.fossil.t87.a(r61)     // Catch:{ Exception -> 0x0350 }
                    r51 = r11
                    r52 = r12
                    r15 = r14
                    r0 = r18
                    r19 = r28
                    r56 = r24
                    r24 = r22
                    r22 = r56
                    r57 = r25
                    r25 = r23
                    r23 = r57
                    goto L_0x11f7
                L_0x0350:
                    r0 = move-exception
                    r61 = r0
                    r2 = r1
                    r16 = r3
                    r52 = r12
                    r53 = r14
                    r1 = r21
                    r3 = r22
                    r11 = r24
                    r0 = r25
                    r24 = r5
                    r14 = r9
                    r9 = r13
                L_0x0366:
                    r6 = r26
                    r5 = r29
                    r12 = r7
                    r7 = r15
                    r8 = r23
                    r15 = r28
                    r23 = r4
                    r4 = r27
                    goto L_0x1a70
                L_0x0376:
                    java.lang.Object r0 = r15.L$11
                    java.util.List r0 = (java.util.List) r0
                    int r1 = r15.I$6
                    java.lang.Object r2 = r15.L$10
                    java.util.List r2 = (java.util.List) r2
                    java.lang.Object r3 = r15.L$9
                    java.util.List r3 = (java.util.List) r3
                    java.lang.Object r4 = r15.L$8
                    com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
                    java.lang.Object r5 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r5 = (com.misfit.frameworks.buttonservice.model.UserProfile) r5
                    long r6 = r15.J$1
                    long r8 = r15.J$0
                    java.lang.Object r10 = r15.L$6
                    com.fossil.if5 r10 = (com.fossil.if5) r10
                    int r13 = r15.I$5
                    r17 = r1
                    int r1 = r15.I$4
                    r18 = r1
                    int r1 = r15.I$3
                    r21 = r0
                    java.lang.Object r0 = r15.L$5
                    r22 = r0
                    com.portfolio.platform.data.model.SKUModel r22 = (com.portfolio.platform.data.model.SKUModel) r22
                    java.lang.Object r0 = r15.L$4
                    r23 = r0
                    android.os.Bundle r23 = (android.os.Bundle) r23
                    java.lang.Object r0 = r15.L$3
                    r24 = r0
                    java.util.ArrayList r24 = (java.util.ArrayList) r24
                    r25 = r1
                    int r1 = r15.I$2
                    r26 = r1
                    int r1 = r15.I$1
                    java.lang.Object r0 = r15.L$2
                    r27 = r0
                    java.lang.String r27 = (java.lang.String) r27
                    java.lang.Object r0 = r15.L$1
                    r28 = r0
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r28 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r28
                    r29 = r1
                    int r1 = r15.I$0
                    java.lang.Object r0 = r15.L$0
                    r30 = r0
                    com.fossil.yi7 r30 = (com.fossil.yi7) r30
                    com.fossil.t87.a(r61)
                    r20 = r14
                    r0 = r21
                    r61 = r27
                    r14 = r30
                    r21 = r3
                    r30 = r12
                    r3 = r23
                    r27 = r25
                    r12 = r1
                    r23 = r11
                    r25 = r18
                    r11 = r26
                    r1 = r28
                    r18 = r2
                    r2 = r24
                    r24 = r13
                    r13 = r4
                    r56 = r8
                    r9 = r5
                    r4 = r56
                    r8 = r22
                    r22 = r10
                    r10 = r29
                    goto L_0x10ab
                L_0x0400:
                    r0 = move-exception
                    r61 = r0
                    r2 = r1
                    r16 = r4
                    r52 = r12
                    r53 = r14
                    r1 = r22
                    r3 = r23
                    r11 = r25
                    r0 = r26
                    r4 = r28
                    r23 = r5
                    r14 = r10
                    r10 = r13
                    r5 = r30
                    r12 = r8
                    r9 = r18
                    r8 = r24
                    r24 = r6
                    r7 = r15
                    r6 = r27
                    r15 = r29
                    goto L_0x1a70
                L_0x0428:
                    java.lang.Object r0 = r15.L$7
                    com.misfit.frameworks.buttonservice.model.UserProfile r0 = (com.misfit.frameworks.buttonservice.model.UserProfile) r0
                    long r1 = r15.J$1
                    long r6 = r15.J$0
                    java.lang.Object r8 = r15.L$6
                    com.fossil.if5 r8 = (com.fossil.if5) r8
                    int r9 = r15.I$5
                    int r10 = r15.I$4
                    int r13 = r15.I$3
                    java.lang.Object r3 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r3 = (com.portfolio.platform.data.model.SKUModel) r3
                    java.lang.Object r4 = r15.L$4
                    android.os.Bundle r4 = (android.os.Bundle) r4
                    java.lang.Object r5 = r15.L$3
                    java.util.ArrayList r5 = (java.util.ArrayList) r5
                    r22 = r0
                    int r0 = r15.I$2
                    r23 = r0
                    int r0 = r15.I$1
                    r24 = r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    r25 = r0
                    java.lang.Object r0 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r0
                    r26 = r0
                    int r0 = r15.I$0
                    r27 = r0
                    java.lang.Object r0 = r15.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r61)
                    r28 = r10
                    r30 = r12
                    r20 = r14
                    r10 = r23
                    r12 = r3
                    r23 = r11
                    r11 = r13
                    r3 = r22
                    r13 = r24
                    r22 = r8
                    r8 = r26
                    r56 = r0
                    r0 = r61
                    r57 = r9
                    r9 = r56
                    r58 = r1
                    r1 = r4
                    r4 = r5
                    r5 = r25
                    r2 = r27
                    r24 = r58
                    r27 = r57
                    goto L_0x0e1d
                L_0x0491:
                    long r0 = r15.J$1
                    long r2 = r15.J$0
                    java.lang.Object r4 = r15.L$6
                    com.fossil.if5 r4 = (com.fossil.if5) r4
                    int r5 = r15.I$5
                    int r6 = r15.I$4
                    int r7 = r15.I$3
                    java.lang.Object r8 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r8 = (com.portfolio.platform.data.model.SKUModel) r8
                    java.lang.Object r9 = r15.L$4
                    android.os.Bundle r9 = (android.os.Bundle) r9
                    java.lang.Object r10 = r15.L$3
                    java.util.ArrayList r10 = (java.util.ArrayList) r10
                    int r13 = r15.I$2
                    r22 = r0
                    int r0 = r15.I$1
                    java.lang.Object r1 = r15.L$2
                    java.lang.String r1 = (java.lang.String) r1
                    r24 = r0
                    java.lang.Object r0 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r0
                    r25 = r0
                    int r0 = r15.I$0
                    r26 = r0
                    java.lang.Object r0 = r15.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r61)
                    r27 = r2
                    r3 = r8
                    r30 = r12
                    r20 = r14
                    r2 = r61
                    r8 = r4
                    r14 = r6
                    r4 = r9
                    r9 = r24
                    r6 = r1
                    r1 = r26
                    r56 = r10
                    r10 = r5
                    r5 = r56
                    r57 = r13
                    r13 = r7
                    r7 = r25
                    r24 = r22
                    r23 = r11
                    r11 = r57
                    goto L_0x0dc6
                L_0x04eb:
                    java.lang.Object r0 = r15.L$6
                    java.lang.Long r0 = (java.lang.Long) r0
                    java.lang.Object r0 = r15.L$5
                    com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
                    java.lang.Object r0 = r15.L$4
                    android.os.Bundle r0 = (android.os.Bundle) r0
                    java.lang.Object r0 = r15.L$3
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r0 = r15.L$2
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r15.L$1
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r0
                    java.lang.Object r0 = r15.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r61)
                    goto L_0x0c04
                L_0x050c:
                    com.fossil.t87.a(r61)
                    com.fossil.yi7 r0 = r15.p$
                    android.content.Intent r3 = r15.$intent
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r4 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r4 = r4.getSERVICE_BLE_PHASE()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r5 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.IDLE
                    int r5 = r5.ordinal()
                    int r3 = r3.getIntExtra(r4, r5)
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r4 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.values()
                    r4 = r4[r3]
                    android.content.Intent r5 = r15.$intent
                    java.lang.String r9 = "serial_number"
                    java.lang.String r5 = r5.getStringExtra(r9)
                    android.content.Intent r9 = r15.$intent
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r10 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r10 = r10.getSERVICE_ACTION_RESULT()
                    r13 = -1
                    int r9 = r9.getIntExtra(r10, r13)
                    android.content.Intent r10 = r15.$intent
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r13 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r13 = r13.getLAST_DEVICE_ERROR_STATE()
                    r8 = 9999(0x270f, float:1.4012E-41)
                    int r8 = r10.getIntExtra(r13, r8)
                    android.content.Intent r10 = r15.$intent
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r13 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r13 = r13.getLIST_PERMISSION_CODES()
                    java.util.ArrayList r10 = r10.getIntegerArrayListExtra(r13)
                    if (r10 != 0) goto L_0x055f
                    java.util.ArrayList r10 = new java.util.ArrayList
                    r10.<init>()
                L_0x055f:
                    android.content.Intent r13 = r15.$intent
                    android.os.Bundle r13 = r13.getExtras()
                    if (r13 == 0) goto L_0x0568
                    goto L_0x056d
                L_0x0568:
                    android.os.Bundle r13 = new android.os.Bundle
                    r13.<init>()
                L_0x056d:
                    r23 = r11
                    java.lang.String r11 = "intent.extras ?: Bundle()"
                    com.fossil.ee7.a(r13, r11)
                    com.portfolio.platform.service.MFDeviceService$d r11 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r11 = r11.a
                    com.portfolio.platform.data.source.DeviceRepository r11 = r11.e()
                    r30 = r12
                    com.fossil.be5$a r12 = com.fossil.be5.o
                    java.lang.String r12 = r12.b(r5)
                    com.portfolio.platform.data.model.SKUModel r11 = r11.getSkuModelBySerialPrefix(r12)
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r12 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r12 = r12.getSYNC_MODE()
                    r24 = r2
                    r2 = -1
                    int r12 = r13.getInt(r12, r2)
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r2 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r2 = r2.getORIGINAL_SYNC_MODE()
                    r25 = r1
                    r1 = 10
                    int r2 = r13.getInt(r2, r1)
                    com.misfit.frameworks.buttonservice.log.FLogger r20 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r20.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r20 = com.portfolio.platform.service.MFDeviceService.Z
                    r31 = r6
                    java.lang.String r6 = r20.b()
                    r20 = r14
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder
                    r14.<init>()
                    r61 = r2
                    java.lang.String r2 = "Inside .bleActionServiceReceiver phase "
                    r14.append(r2)
                    r14.append(r4)
                    java.lang.String r2 = " result "
                    r14.append(r2)
                    r14.append(r9)
                    java.lang.String r2 = " extra info "
                    r14.append(r2)
                    r14.append(r13)
                    java.lang.String r2 = r14.toString()
                    r1.d(r6, r2)
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r1 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r1 = r1.getLOG_ID()
                    r2 = -1
                    int r1 = r13.getInt(r1, r2)
                    int[] r2 = com.fossil.pj5.a
                    int r6 = r4.ordinal()
                    r2 = r2[r6]
                    java.lang.String r14 = "error_code"
                    java.lang.String r6 = "device"
                    switch(r2) {
                        case 1: goto L_0x1e66;
                        case 2: goto L_0x1e54;
                        case 3: goto L_0x0c07;
                        case 4: goto L_0x0b8a;
                        case 5: goto L_0x097b;
                        case 6: goto L_0x0964;
                        case 7: goto L_0x0942;
                        case 8: goto L_0x091f;
                        case 9: goto L_0x06e5;
                        case 10: goto L_0x06d1;
                        case 11: goto L_0x067b;
                        case 12: goto L_0x0603;
                        default: goto L_0x05f3;
                    }
                L_0x05f3:
                    r2 = r4
                    r7 = r15
                    com.fossil.nj5 r0 = com.fossil.nj5.d
                    com.fossil.nj5$a r1 = new com.fossil.nj5$a
                    android.content.Intent r3 = r7.$intent
                    r1.<init>(r2, r5, r3)
                    r0.a(r2, r1)
                    goto L_0x1ff6
                L_0x0603:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.String r2 = ".bleActionServiceReceiver() - SET_WATCH_APP_FILE_SESSION"
                    r0.d(r1, r2)
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_WATCH_APP_DATA
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x0c04
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r0 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r0 = r0.getWATCH_APP_MAJOR_VERSION()
                    byte r0 = r13.getByte(r0)
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r1 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r1 = r1.getWATCH_APP_MINOR_VERSION()
                    byte r1 = r13.getByte(r1)
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r2.append(r0)
                    r0 = 46
                    r2.append(r0)
                    r2.append(r1)
                    java.lang.String r0 = r2.toString()
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r2 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r2 = r2.b()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "Ask for WatchAppData, uiVersion = "
                    r3.append(r4)
                    r3.append(r0)
                    java.lang.String r3 = r3.toString()
                    r1.d(r2, r3)
                    com.fossil.ti7 r1 = com.fossil.qj7.b()
                    com.fossil.yi7 r6 = com.fossil.zi7.a(r1)
                    r7 = 0
                    r8 = 0
                    com.portfolio.platform.service.MFDeviceService$d$a$a r9 = new com.portfolio.platform.service.MFDeviceService$d$a$a
                    r1 = 0
                    r9.<init>(r15, r0, r5, r1)
                    r10 = 3
                    r11 = 0
                    com.fossil.ik7 unused = com.fossil.xh7.b(r6, r7, r8, r9, r10, r11)
                    goto L_0x0c04
                L_0x067b:
                    java.lang.String r0 = "CURRENT_WORKOUT_SESSION"
                    android.os.Parcelable r0 = r13.getParcelable(r0)
                    com.fossil.ab0 r0 = (com.fossil.ab0) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r2 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r2 = r2.b()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "onReadCurrentWorkout success, isRequireGPS "
                    r3.append(r4)
                    if (r0 == 0) goto L_0x06a4
                    boolean r4 = r0.isRequiredGPS()
                    java.lang.Boolean r8 = com.fossil.pb7.a(r4)
                    goto L_0x06a5
                L_0x06a4:
                    r8 = 0
                L_0x06a5:
                    r3.append(r8)
                    java.lang.String r3 = r3.toString()
                    r1.d(r2, r3)
                    if (r0 == 0) goto L_0x06c4
                    boolean r0 = r0.isRequiredGPS()
                    if (r0 == 0) goto L_0x06c4
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.wk5 r0 = r0.x()
                    r0.a()
                    goto L_0x0c04
                L_0x06c4:
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.wk5 r0 = r0.x()
                    r0.i()
                    goto L_0x0c04
                L_0x06d1:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_WATCH_PARAMS
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x0c04
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ee7.a(r5, r7)
                    r0.a(r5, r13)
                    goto L_0x0c04
                L_0x06e5:
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r6 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r6 = r6.b()
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder
                    r14.<init>()
                    r28 = r1
                    java.lang.String r1 = "onExchangeSecretKey result "
                    r14.append(r1)
                    r14.append(r9)
                    java.lang.String r1 = r14.toString()
                    r2.d(r6, r1)
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r1 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_RANDOM_KEY
                    int r1 = r1.ordinal()
                    if (r9 != r1) goto L_0x0776
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.portfolio.platform.data.source.DeviceRepository r1 = r1.e()
                    com.fossil.ee7.a(r5, r7)
                    r15.L$0 = r0
                    r15.I$0 = r3
                    r15.L$1 = r4
                    r15.L$2 = r5
                    r15.I$1 = r9
                    r15.I$2 = r8
                    r15.L$3 = r10
                    r15.L$4 = r13
                    r15.L$5 = r11
                    r15.I$3 = r12
                    r2 = r61
                    r15.I$4 = r2
                    r6 = r28
                    r15.I$5 = r6
                    r0 = 15
                    r15.label = r0
                    java.lang.Object r0 = r1.generatePairingKey(r5, r15)
                    r1 = r20
                    if (r0 != r1) goto L_0x0743
                    return r1
                L_0x0743:
                    com.fossil.zi5 r0 = (com.fossil.zi5) r0
                    boolean r1 = r0 instanceof com.fossil.bj5
                    if (r1 == 0) goto L_0x0754
                    com.fossil.bj5 r0 = (com.fossil.bj5) r0
                    java.lang.Object r0 = r0.a()
                    r6 = r0
                    java.lang.String r6 = (java.lang.String) r6
                    r9 = 0
                    goto L_0x0760
                L_0x0754:
                    boolean r1 = r0 instanceof com.fossil.yi5
                    if (r1 == 0) goto L_0x0770
                    com.fossil.yi5 r0 = (com.fossil.yi5) r0
                    int r9 = r0.a()
                    r6 = r31
                L_0x0760:
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.fossil.ee7.a(r5, r7)
                    r0.a(r5, r6, r9)
                    goto L_0x0c04
                L_0x0770:
                    com.fossil.p87 r0 = new com.fossil.p87
                    r0.<init>()
                    throw r0
                L_0x0776:
                    r2 = r61
                    r1 = r20
                    r6 = r28
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r14 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_SERVER_SECRET_KEY
                    int r14 = r14.ordinal()
                    if (r9 != r14) goto L_0x082c
                    java.lang.String r14 = "randomKeyFromDevice"
                    java.lang.String r14 = r13.getString(r14)
                    r20 = r1
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.portfolio.platform.data.source.DeviceRepository r1 = r1.e()
                    if (r14 == 0) goto L_0x079a
                    r61 = r1
                    r1 = r14
                    goto L_0x079e
                L_0x079a:
                    r61 = r1
                    r1 = r31
                L_0x079e:
                    com.fossil.ee7.a(r5, r7)
                    r15.L$0 = r0
                    r15.I$0 = r3
                    r15.L$1 = r4
                    r15.L$2 = r5
                    r15.I$1 = r9
                    r15.I$2 = r8
                    r15.L$3 = r10
                    r15.L$4 = r13
                    r15.L$5 = r11
                    r15.I$3 = r12
                    r15.I$4 = r2
                    r15.I$5 = r6
                    r15.L$6 = r14
                    r0 = 16
                    r15.label = r0
                    r0 = r61
                    java.lang.Object r0 = r0.swapPairingKey(r1, r5, r15)
                    r1 = r20
                    if (r0 != r1) goto L_0x07ca
                    return r1
                L_0x07ca:
                    com.fossil.zi5 r0 = (com.fossil.zi5) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r2 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r2 = r2.b()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "swap pairing key "
                    r3.append(r4)
                    r3.append(r0)
                    java.lang.String r3 = r3.toString()
                    r1.d(r2, r3)
                    boolean r1 = r0 instanceof com.fossil.bj5
                    if (r1 == 0) goto L_0x0810
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.portfolio.platform.PortfolioApp r1 = r1.c()
                    com.fossil.ee7.a(r5, r7)
                    com.fossil.bj5 r0 = (com.fossil.bj5) r0
                    java.lang.Object r0 = r0.a()
                    if (r0 == 0) goto L_0x080b
                    java.lang.String r0 = (java.lang.String) r0
                    r2 = 0
                    r1.b(r5, r0, r2)
                    goto L_0x0c04
                L_0x080b:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x0810:
                    boolean r1 = r0 instanceof com.fossil.yi5
                    if (r1 == 0) goto L_0x0c04
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.portfolio.platform.PortfolioApp r1 = r1.c()
                    com.fossil.ee7.a(r5, r7)
                    com.fossil.yi5 r0 = (com.fossil.yi5) r0
                    int r0 = r0.a()
                    r7 = r31
                    r1.b(r5, r7, r0)
                    goto L_0x0c04
                L_0x082c:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_CURRENT_SECRET_KEY
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x0859
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.fossil.ee7.a(r5, r7)
                    r0.h(r5)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.nw6 r0 = r0.u()
                    com.fossil.nw6$b r1 = new com.fossil.nw6$b
                    r1.<init>(r5)
                    com.portfolio.platform.service.MFDeviceService$d$a$b r2 = new com.portfolio.platform.service.MFDeviceService$d$a$b
                    r2.<init>(r15, r5)
                    r0.a(r1, r2)
                    goto L_0x0c04
                L_0x0859:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.REQUEST_PUSH_SECRET_KEY_TO_CLOUD
                    int r0 = r0.ordinal()
                    java.lang.String r1 = " for "
                    java.lang.String r2 = "deviceSecretKey"
                    if (r9 != r0) goto L_0x08ca
                    boolean r0 = r13.isEmpty()
                    if (r0 != 0) goto L_0x0c04
                    java.lang.String r0 = r13.getString(r2)
                    boolean r2 = android.text.TextUtils.isEmpty(r0)
                    if (r2 != 0) goto L_0x08b7
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r3 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r3 = r3.b()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r6 = "update secret key "
                    r4.append(r6)
                    r4.append(r0)
                    r4.append(r1)
                    r4.append(r5)
                    java.lang.String r1 = " to server"
                    r4.append(r1)
                    java.lang.String r1 = r4.toString()
                    r2.d(r3, r1)
                    com.fossil.ti7 r1 = com.fossil.qj7.b()
                    com.fossil.yi7 r6 = com.fossil.zi7.a(r1)
                    r7 = 0
                    r8 = 0
                    com.portfolio.platform.service.MFDeviceService$d$a$c r9 = new com.portfolio.platform.service.MFDeviceService$d$a$c
                    r1 = 0
                    r9.<init>(r15, r5, r0, r1)
                    r10 = 3
                    r11 = 0
                    com.fossil.ik7 unused = com.fossil.xh7.b(r6, r7, r8, r9, r10, r11)
                    goto L_0x0c04
                L_0x08b7:
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    if (r5 == 0) goto L_0x08c5
                    r1 = 0
                    r0.c(r5, r1)
                    goto L_0x0c04
                L_0x08c5:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x08ca:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x0c04
                    boolean r0 = r13.isEmpty()
                    if (r0 != 0) goto L_0x0c04
                    java.lang.String r0 = r13.getString(r2)
                    boolean r2 = android.text.TextUtils.isEmpty(r0)
                    if (r2 != 0) goto L_0x0c04
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r3 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r3 = r3.b()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r6 = "encrypt secret key "
                    r4.append(r6)
                    r4.append(r0)
                    r4.append(r1)
                    r4.append(r5)
                    java.lang.String r1 = r4.toString()
                    r2.d(r3, r1)
                    com.fossil.ti7 r1 = com.fossil.qj7.b()
                    com.fossil.yi7 r6 = com.fossil.zi7.a(r1)
                    r7 = 0
                    r8 = 0
                    com.portfolio.platform.service.MFDeviceService$d$a$d r9 = new com.portfolio.platform.service.MFDeviceService$d$a$d
                    r1 = 0
                    r9.<init>(r15, r5, r0, r1)
                    r10 = 3
                    r11 = 0
                    com.fossil.ik7 unused = com.fossil.xh7.b(r6, r7, r8, r9, r10, r11)
                    goto L_0x0c04
                L_0x091f:
                    boolean r0 = r13.isEmpty()
                    if (r0 != 0) goto L_0x0c04
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    android.os.Parcelable r1 = r13.getParcelable(r6)
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r1
                    r0.a(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = r0.a()
                    com.fossil.ee7.a(r5, r7)
                    r0.a(r1, r5)
                    goto L_0x0c04
                L_0x0942:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x0957
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                    com.fossil.dc5 r1 = new com.fossil.dc5
                    r2 = 1
                    r1.<init>(r5, r2)
                    r0.a(r1)
                    goto L_0x0c04
                L_0x0957:
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                    com.fossil.dc5 r1 = new com.fossil.dc5
                    r2 = 0
                    r1.<init>(r5, r2)
                    r0.a(r1)
                    goto L_0x0c04
                L_0x0964:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r0 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r0 = r0.ordinal()
                    if (r9 != r0) goto L_0x096e
                    r10 = 1
                    goto L_0x096f
                L_0x096e:
                    r10 = 0
                L_0x096f:
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                    com.fossil.ic5 r1 = new com.fossil.ic5
                    r1.<init>(r5, r10)
                    r0.a(r1)
                    goto L_0x0c04
                L_0x097b:
                    r2 = r61
                    r61 = r14
                    r7 = r31
                    r14 = r1
                    android.content.Intent r1 = new android.content.Intent
                    r28 = r14
                    java.lang.String r14 = "BROADCAST_OTA_COMPLETE"
                    r1.<init>(r14)
                    com.fossil.qd5$a r14 = com.fossil.qd5.f
                    r17 = r1
                    r1 = r25
                    com.fossil.if5 r14 = r14.c(r1)
                    if (r11 == 0) goto L_0x09c1
                    java.util.HashMap r1 = new java.util.HashMap
                    r1.<init>()
                    java.lang.String r18 = r11.getSku()
                    r29 = r2
                    if (r18 == 0) goto L_0x09ab
                    r56 = r18
                    r18 = r14
                    r14 = r56
                    goto L_0x09ae
                L_0x09ab:
                    r18 = r14
                    r14 = r7
                L_0x09ae:
                    java.lang.String r2 = "Style_Number"
                    r1.put(r2, r14)
                    java.lang.String r2 = r11.getDeviceName()
                    if (r2 == 0) goto L_0x09ba
                    goto L_0x09bb
                L_0x09ba:
                    r2 = r7
                L_0x09bb:
                    java.lang.String r14 = "Device_Name"
                    r1.put(r14, r2)
                    goto L_0x09c6
                L_0x09c1:
                    r29 = r2
                    r18 = r14
                    r1 = 0
                L_0x09c6:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r2 = r2.ordinal()
                    if (r9 != r2) goto L_0x0b20
                    boolean r2 = r13.isEmpty()
                    if (r2 != 0) goto L_0x09e1
                    com.portfolio.platform.service.MFDeviceService$d r2 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    android.os.Parcelable r6 = r13.getParcelable(r6)
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r6
                    r2.a(r6)
                L_0x09e1:
                    com.portfolio.platform.service.MFDeviceService$d r2 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r2 = r2.a()
                    if (r2 == 0) goto L_0x0a21
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r6 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r6 = r6.b()
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder
                    r14.<init>()
                    r21 = r1
                    java.lang.String r1 = "OTA complete, update fw version from device="
                    r14.append(r1)
                    r14.append(r7)
                    java.lang.String r1 = r14.toString()
                    r2.d(r6, r1)
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = r1.a()
                    if (r1 == 0) goto L_0x0a1c
                    java.lang.String r1 = r1.getFirmwareVersion()
                    goto L_0x0a4f
                L_0x0a1c:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x0a21:
                    r21 = r1
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r2 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r2 = r2.b()
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder
                    r6.<init>()
                    java.lang.String r14 = "OTA complete, update fw version from temp="
                    r6.append(r14)
                    r6.append(r7)
                    java.lang.String r6 = r6.toString()
                    r1.d(r2, r6)
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.fossil.ch5 r1 = r1.o()
                    java.lang.String r1 = r1.g(r5)
                L_0x0a4f:
                    boolean r2 = android.text.TextUtils.isEmpty(r1)
                    if (r2 != 0) goto L_0x0afb
                    com.portfolio.platform.service.MFDeviceService$d r2 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    com.portfolio.platform.data.source.DeviceRepository r2 = r2.e()
                    if (r5 == 0) goto L_0x0af6
                    com.portfolio.platform.data.model.Device r2 = r2.getDeviceBySerial(r5)
                    if (r2 == 0) goto L_0x0ae6
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r14 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r14 = r14.b()
                    r31 = r7
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    r32 = r12
                    java.lang.String r12 = "OTA complete, update fw version="
                    r7.append(r12)
                    if (r1 == 0) goto L_0x0ae1
                    r7.append(r1)
                    java.lang.String r7 = r7.toString()
                    r6.d(r14, r7)
                    r2.setFirmwareRevision(r1)
                    com.portfolio.platform.service.MFDeviceService$d r6 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    com.portfolio.platform.data.source.DeviceRepository r6 = r6.e()
                    r15.L$0 = r0
                    r15.I$0 = r3
                    r15.L$1 = r4
                    r15.L$2 = r5
                    r15.I$1 = r9
                    r15.I$2 = r8
                    r15.L$3 = r10
                    r15.L$4 = r13
                    r15.L$5 = r11
                    r12 = r32
                    r15.I$3 = r12
                    r14 = r29
                    r15.I$4 = r14
                    r0 = r28
                    r15.I$5 = r0
                    r0 = r17
                    r15.L$6 = r0
                    r3 = r18
                    r15.L$7 = r3
                    r4 = r21
                    r15.L$8 = r4
                    r15.L$9 = r1
                    r15.L$10 = r1
                    r15.L$11 = r2
                    r15.L$12 = r2
                    r1 = 14
                    r15.label = r1
                    r1 = 0
                    java.lang.Object r1 = r6.updateDevice(r2, r1, r15)
                    r2 = r20
                    if (r1 != r2) goto L_0x0ad6
                    return r2
                L_0x0ad6:
                    r56 = r4
                    r4 = r0
                    r0 = r56
                L_0x0adb:
                    com.fossil.zi5 r1 = (com.fossil.zi5) r1
                    r1 = r0
                    r14 = r3
                    r0 = r4
                    goto L_0x0af0
                L_0x0ae1:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x0ae6:
                    r31 = r7
                    r0 = r17
                    r3 = r18
                    r4 = r21
                    r14 = r3
                    r1 = r4
                L_0x0af0:
                    r4 = r1
                    r2 = r24
                    r7 = 1
                    r1 = r0
                    goto L_0x0b08
                L_0x0af6:
                    r0 = 0
                    com.fossil.ee7.a()
                    throw r0
                L_0x0afb:
                    r31 = r7
                    r0 = r17
                    r3 = r18
                    r4 = r21
                    r1 = r0
                    r14 = r3
                    r2 = r24
                    r7 = 1
                L_0x0b08:
                    r1.putExtra(r2, r7)
                    if (r4 == 0) goto L_0x0b16
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    java.lang.String r2 = "ota_success"
                    r0.a(r2, r4)
                L_0x0b16:
                    if (r14 == 0) goto L_0x0b64
                    r0 = r31
                    r14.a(r0)
                    com.fossil.i97 r0 = com.fossil.i97.a
                    goto L_0x0b64
                L_0x0b20:
                    r4 = r1
                    r0 = r17
                    r3 = r18
                    r2 = r24
                    if (r4 == 0) goto L_0x0b3c
                    java.lang.String r1 = java.lang.String.valueOf(r8)
                    r6 = r61
                    r4.put(r6, r1)
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    java.lang.String r7 = "ota_fail"
                    r1.a(r7, r4)
                    goto L_0x0b3e
                L_0x0b3c:
                    r6 = r61
                L_0x0b3e:
                    com.fossil.qd5$a r1 = com.fossil.qd5.f
                    java.lang.String r4 = "ota_session_optional_error"
                    com.fossil.gf5 r1 = r1.a(r4)
                    java.lang.String r4 = java.lang.String.valueOf(r8)
                    r1.a(r6, r4)
                    if (r3 == 0) goto L_0x0b54
                    r3.a(r1)
                    com.fossil.i97 r1 = com.fossil.i97.a
                L_0x0b54:
                    if (r3 == 0) goto L_0x0b5f
                    java.lang.String r1 = java.lang.String.valueOf(r8)
                    r3.a(r1)
                    com.fossil.i97 r1 = com.fossil.i97.a
                L_0x0b5f:
                    r1 = 0
                    r0.putExtra(r2, r1)
                    r1 = r0
                L_0x0b64:
                    com.fossil.qd5$a r0 = com.fossil.qd5.f
                    r2 = r25
                    r0.e(r2)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r2 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r2 = r2.b()
                    java.lang.String r3 = "Inside .OTA receiver, broadcast local in app"
                    r0.d(r2, r3)
                    com.fossil.nj5 r0 = com.fossil.nj5.d
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.OTA
                    com.fossil.nj5$a r3 = new com.fossil.nj5$a
                    r3.<init>(r2, r5, r1)
                    r0.a(r2, r3)
                    goto L_0x0c04
                L_0x0b8a:
                    r14 = r61
                    r2 = r20
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r6 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.SUCCEEDED
                    int r6 = r6.ordinal()
                    if (r9 != r6) goto L_0x0c04
                    boolean r6 = r13.isEmpty()
                    if (r6 != 0) goto L_0x0c04
                    java.lang.String r6 = "battery"
                    int r6 = r13.getInt(r6)
                    r20 = r2
                    com.portfolio.platform.service.MFDeviceService$d r2 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    com.portfolio.platform.data.source.DeviceRepository r2 = r2.e()
                    com.fossil.ee7.a(r5, r7)
                    com.portfolio.platform.data.model.Device r2 = r2.getDeviceBySerial(r5)
                    if (r2 == 0) goto L_0x0c04
                    if (r6 <= 0) goto L_0x0c02
                    r2.setBatteryLevel(r6)
                    int r7 = r2.getBatteryLevel()
                    r28 = r1
                    r1 = 100
                    if (r7 <= r1) goto L_0x0bc7
                    r2.setBatteryLevel(r1)
                L_0x0bc7:
                    com.fossil.ti7 r1 = com.fossil.qj7.b()
                    com.portfolio.platform.service.MFDeviceService$d$a$e r7 = new com.portfolio.platform.service.MFDeviceService$d$a$e
                    r61 = r1
                    r1 = 0
                    r7.<init>(r1, r15, r6, r2)
                    r15.L$0 = r0
                    r15.I$0 = r3
                    r15.L$1 = r4
                    r15.L$2 = r5
                    r15.I$1 = r9
                    r15.I$2 = r8
                    r15.L$3 = r10
                    r15.L$4 = r13
                    r15.L$5 = r11
                    r15.I$3 = r12
                    r15.I$4 = r14
                    r1 = r28
                    r15.I$5 = r1
                    r15.I$6 = r6
                    r15.L$6 = r2
                    r15.L$7 = r2
                    r0 = 13
                    r15.label = r0
                    r0 = r61
                    java.lang.Object r0 = com.fossil.vh7.a(r0, r7, r15)
                    r2 = r20
                    if (r0 != r2) goto L_0x0c02
                    return r2
                L_0x0c02:
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x0c04:
                    r7 = r15
                    goto L_0x1ff6
                L_0x0c07:
                    r7 = r14
                    r2 = r20
                    r33 = r31
                    r14 = r61
                    r61 = r6
                    com.fossil.qd5$a r6 = com.fossil.qd5.f
                    r2 = r30
                    com.fossil.if5 r6 = r6.c(r2)
                    com.portfolio.platform.service.MFDeviceService$d r2 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    com.fossil.ch5 r2 = r2.o()
                    r28 = r3
                    r29 = r4
                    long r3 = java.lang.System.currentTimeMillis()
                    r2.b(r3, r5)
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.ASK_FOR_STOP_WORKOUT
                    int r2 = r2.ordinal()
                    if (r9 != r2) goto L_0x0c5e
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.String r2 = "sync is pending because workout is working"
                    r0.d(r1, r2)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r24 = 5
                    java.lang.Integer r27 = com.fossil.pb7.a(r14)
                    r22 = r0
                    r23 = r5
                    r25 = r8
                    r26 = r10
                    r22.a(r23, r24, r25, r26, r27)
                L_0x0c59:
                    r7 = r15
                    r52 = r30
                    goto L_0x1e42
                L_0x0c5e:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.PROCESSING
                    int r2 = r2.ordinal()
                    if (r9 != r2) goto L_0x0c8f
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.String r2 = "sync status is SYNCING"
                    r0.d(r1, r2)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r24 = 0
                    r25 = 0
                    r26 = 0
                    r27 = 0
                    r28 = 24
                    r29 = 0
                    r22 = r0
                    r23 = r5
                    com.portfolio.platform.service.MFDeviceService.a(r22, r23, r24, r25, r26, r27, r28, r29)
                    goto L_0x0c59
                L_0x0c8f:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.UNALLOWED_ACTION
                    int r2 = r2.ordinal()
                    if (r9 != r2) goto L_0x0cb9
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r1 = 0
                    r0.c(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r24 = 4
                    java.lang.Integer r27 = com.fossil.pb7.a(r14)
                    r22 = r0
                    r23 = r5
                    r25 = r8
                    r26 = r10
                    r22.a(r23, r24, r25, r26, r27)
                    goto L_0x0c59
                L_0x0cb9:
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.FAILED
                    int r2 = r2.ordinal()
                    r3 = 2
                    if (r9 != r2) goto L_0x0d42
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r4 = 0
                    r0.c(r4)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r4 = "sync status is FAILED - current sync success number = "
                    r2.append(r4)
                    com.portfolio.platform.service.MFDeviceService$d r4 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r4 = r4.a
                    com.fossil.ch5 r4 = r4.o()
                    int r4 = r4.i()
                    r2.append(r4)
                    java.lang.String r2 = r2.toString()
                    r0.d(r1, r2)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r24 = 2
                    java.lang.Integer r27 = com.fossil.pb7.a(r14)
                    r22 = r0
                    r23 = r5
                    r25 = r8
                    r26 = r10
                    r22.a(r23, r24, r25, r26, r27)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r0.a(r11, r12, r3)
                    com.fossil.qd5$a r0 = com.fossil.qd5.f
                    java.lang.String r1 = "sync_session_optional_error"
                    com.fossil.gf5 r0 = r0.a(r1)
                    java.lang.String r1 = java.lang.String.valueOf(r8)
                    r0.a(r7, r1)
                    if (r6 == 0) goto L_0x0d2c
                    r6.a(r0)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x0d2c:
                    if (r6 == 0) goto L_0x0d37
                    java.lang.String r0 = java.lang.String.valueOf(r8)
                    r6.a(r0)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x0d37:
                    com.fossil.qh5$a r0 = com.fossil.qh5.c
                    com.portfolio.platform.service.MFDeviceService$d r1 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    r0.a(r1)
                    goto L_0x0c59
                L_0x0d42:
                    r4 = 0
                    com.misfit.frameworks.buttonservice.enums.ServiceActionResult r2 = com.misfit.frameworks.buttonservice.enums.ServiceActionResult.RECEIVED_DATA
                    int r2 = r2.ordinal()
                    if (r9 != r2) goto L_0x1d59
                    boolean r2 = r13.isEmpty()
                    if (r2 != 0) goto L_0x1c9c
                    if (r5 == 0) goto L_0x0d5c
                    boolean r2 = com.fossil.mh7.a(r5)
                    if (r2 == 0) goto L_0x0d5a
                    goto L_0x0d5c
                L_0x0d5a:
                    r2 = 0
                    goto L_0x0d5d
                L_0x0d5c:
                    r2 = 1
                L_0x0d5d:
                    if (r2 != 0) goto L_0x1c9c
                    r3 = -1
                    java.lang.String r7 = "com.misfit.fw.syncresult"
                    long r3 = r13.getLong(r7, r3)
                    com.misfit.frameworks.buttonservice.ButtonService$Companion r7 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                    java.lang.String r7 = r7.getREALTIME_STEPS()
                    r24 = r3
                    r2 = -1
                    long r2 = r13.getLong(r7, r2)
                    com.portfolio.platform.service.MFDeviceService$d r4 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r4 = r4.a
                    com.portfolio.platform.PortfolioApp r4 = r4.c()
                    r15.L$0 = r0
                    r7 = r28
                    r15.I$0 = r7
                    r7 = r29
                    r15.L$1 = r7
                    r15.L$2 = r5
                    r15.I$1 = r9
                    r15.I$2 = r8
                    r15.L$3 = r10
                    r15.L$4 = r13
                    r15.L$5 = r11
                    r15.I$3 = r12
                    r15.I$4 = r14
                    r15.I$5 = r1
                    r15.L$6 = r6
                    r31 = r11
                    r32 = r12
                    r11 = r24
                    r15.J$0 = r11
                    r15.J$1 = r2
                    r24 = r2
                    r2 = 2
                    r15.label = r2
                    java.lang.Object r2 = r4.d(r15)
                    r3 = r20
                    if (r2 != r3) goto L_0x0db3
                    return r3
                L_0x0db3:
                    r20 = r3
                    r4 = r13
                    r3 = r31
                    r13 = r32
                    r56 = r10
                    r10 = r1
                    r1 = r28
                    r27 = r11
                    r11 = r8
                    r8 = r6
                    r6 = r5
                    r5 = r56
                L_0x0dc6:
                    com.misfit.frameworks.buttonservice.model.UserProfile r2 = (com.misfit.frameworks.buttonservice.model.UserProfile) r2
                    com.portfolio.platform.service.MFDeviceService$d r12 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r12 = r12.a
                    com.portfolio.platform.data.source.UserRepository r12 = r12.t()
                    r15.L$0 = r0
                    r15.I$0 = r1
                    r15.L$1 = r7
                    r15.L$2 = r6
                    r15.I$1 = r9
                    r15.I$2 = r11
                    r15.L$3 = r5
                    r15.L$4 = r4
                    r15.L$5 = r3
                    r15.I$3 = r13
                    r15.I$4 = r14
                    r15.I$5 = r10
                    r15.L$6 = r8
                    r22 = r0
                    r29 = r1
                    r0 = r27
                    r15.J$0 = r0
                    r0 = r24
                    r15.J$1 = r0
                    r15.L$7 = r2
                    r0 = 3
                    r15.label = r0
                    java.lang.Object r0 = r12.getCurrentUser(r15)
                    r1 = r20
                    if (r0 != r1) goto L_0x0e04
                    return r1
                L_0x0e04:
                    r20 = r1
                    r12 = r3
                    r1 = r4
                    r4 = r5
                    r5 = r6
                    r3 = r2
                    r2 = r29
                    r56 = r8
                    r8 = r7
                    r6 = r27
                    r27 = r10
                    r10 = r11
                    r11 = r13
                    r28 = r14
                    r13 = r9
                    r9 = r22
                    r22 = r56
                L_0x0e1d:
                    r14 = r0
                    com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    r29 = r11
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r11 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    r61 = r12
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder
                    r12.<init>()
                    r37 = r1
                    java.lang.String r1 = "Sync time="
                    r12.append(r1)
                    r12.append(r6)
                    java.lang.String r1 = r12.toString()
                    r0.a(r11, r5, r1)
                    r0 = -1
                    long r0 = (long) r0
                    int r11 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
                    if (r11 <= 0) goto L_0x1b2c
                    if (r3 == 0) goto L_0x1b2c
                    if (r14 == 0) goto L_0x1b2c
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0     // Catch:{ Exception -> 0x1a42 }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x1a42 }
                    com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC     // Catch:{ Exception -> 0x1a42 }
                    java.lang.String r11 = "Save sync result to DB"
                    r0.a(r1, r5, r11)     // Catch:{ Exception -> 0x1a42 }
                    java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x1a42 }
                    r1.<init>()     // Catch:{ Exception -> 0x1a42 }
                    com.portfolio.platform.service.MFDeviceService$d r0 = r15.this$0     // Catch:{ Exception -> 0x1a42 }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.db.DataFileProvider r0 = com.misfit.frameworks.buttonservice.db.DataFileProvider.getInstance(r0)     // Catch:{ Exception -> 0x1a42 }
                    java.util.List r11 = r0.getAllDataFiles(r6, r5)     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r0.getRemote()     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x1a42 }
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC     // Catch:{ Exception -> 0x1a42 }
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x1a42 }
                    java.lang.String r35 = r0.b()     // Catch:{ Exception -> 0x1a42 }
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x1a42 }
                    r0.<init>()     // Catch:{ Exception -> 0x1a42 }
                    java.lang.String r12 = "[SYNC] Process sync data from BS dataSize "
                    r0.append(r12)     // Catch:{ Exception -> 0x1a42 }
                    int r12 = r11.size()     // Catch:{ Exception -> 0x1a42 }
                    r0.append(r12)     // Catch:{ Exception -> 0x1a42 }
                    java.lang.String r36 = r0.toString()     // Catch:{ Exception -> 0x1a42 }
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)     // Catch:{ Exception -> 0x1a42 }
                    java.lang.String r0 = "cacheData"
                    com.fossil.ee7.a(r11, r0)     // Catch:{ Exception -> 0x1a42 }
                    java.util.Iterator r12 = r11.iterator()     // Catch:{ Exception -> 0x1a42 }
                L_0x0ea1:
                    boolean r0 = r12.hasNext()     // Catch:{ Exception -> 0x1a42 }
                    if (r0 == 0) goto L_0x0f8a
                    java.lang.Object r0 = r12.next()     // Catch:{ Exception -> 0x0f5f }
                    com.misfit.frameworks.buttonservice.db.DataFile r0 = (com.misfit.frameworks.buttonservice.db.DataFile) r0     // Catch:{ Exception -> 0x0f5f }
                    r17 = r12
                    com.portfolio.platform.service.MFDeviceService$d r12 = r15.this$0     // Catch:{ Exception -> 0x0f06 }
                    com.portfolio.platform.service.MFDeviceService r12 = r12.a     // Catch:{ Exception -> 0x0f06 }
                    com.google.gson.Gson r12 = r12.J     // Catch:{ Exception -> 0x0f06 }
                    r18 = r11
                    java.lang.String r11 = "it"
                    com.fossil.ee7.a(r0, r11)     // Catch:{ Exception -> 0x0f02 }
                    java.lang.String r0 = r0.getDataFile()     // Catch:{ Exception -> 0x0f02 }
                    java.lang.Class<com.fossil.fitness.FitnessData> r11 = com.fossil.fitness.FitnessData.class
                    java.lang.Object r0 = r12.a(r0, r11)     // Catch:{ Exception -> 0x0f02 }
                    com.fossil.fitness.FitnessData r0 = (com.fossil.fitness.FitnessData) r0     // Catch:{ Exception -> 0x0f02 }
                    com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0f02 }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ Exception -> 0x0f02 }
                    com.portfolio.platform.service.MFDeviceService$a r12 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x0f02 }
                    java.lang.String r12 = r12.b()     // Catch:{ Exception -> 0x0f02 }
                    r21 = r14
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0efe }
                    r14.<init>()     // Catch:{ Exception -> 0x0efe }
                    r38 = r3
                    java.lang.String r3 = "[SYNC] Add fitness data "
                    r14.append(r3)     // Catch:{ Exception -> 0x0efc }
                    r14.append(r0)     // Catch:{ Exception -> 0x0efc }
                    java.lang.String r3 = " to sync data"
                    r14.append(r3)     // Catch:{ Exception -> 0x0efc }
                    java.lang.String r3 = r14.toString()     // Catch:{ Exception -> 0x0efc }
                    r11.d(r12, r3)     // Catch:{ Exception -> 0x0efc }
                    java.lang.String r3 = "fitnessData"
                    com.fossil.ee7.a(r0, r3)     // Catch:{ Exception -> 0x0efc }
                    r1.add(r0)     // Catch:{ Exception -> 0x0efc }
                    goto L_0x0f53
                L_0x0efc:
                    r0 = move-exception
                    goto L_0x0f0d
                L_0x0efe:
                    r0 = move-exception
                    r38 = r3
                    goto L_0x0f0d
                L_0x0f02:
                    r0 = move-exception
                    r38 = r3
                    goto L_0x0f0b
                L_0x0f06:
                    r0 = move-exception
                    r38 = r3
                    r18 = r11
                L_0x0f0b:
                    r21 = r14
                L_0x0f0d:
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0f5d }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x0f5d }
                    com.portfolio.platform.service.MFDeviceService$a r11 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r11 = r11.b()     // Catch:{ Exception -> 0x0f5d }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0f5d }
                    r12.<init>()     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r14 = "Parse fitnessData. exception="
                    r12.append(r14)     // Catch:{ Exception -> 0x0f5d }
                    r12.append(r0)     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0f5d }
                    r3.d(r11, r12)     // Catch:{ Exception -> 0x0f5d }
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0f5d }
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r3.getRemote()     // Catch:{ Exception -> 0x0f5d }
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x0f5d }
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC     // Catch:{ Exception -> 0x0f5d }
                    com.portfolio.platform.service.MFDeviceService$a r3 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r35 = r3.b()     // Catch:{ Exception -> 0x0f5d }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0f5d }
                    r3.<init>()     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r11 = "[Sync Data Received] Exception when parse data "
                    r3.append(r11)     // Catch:{ Exception -> 0x0f5d }
                    r3.append(r0)     // Catch:{ Exception -> 0x0f5d }
                    java.lang.String r36 = r3.toString()     // Catch:{ Exception -> 0x0f5d }
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)     // Catch:{ Exception -> 0x0f5d }
                L_0x0f53:
                    r12 = r17
                    r11 = r18
                    r14 = r21
                    r3 = r38
                    goto L_0x0ea1
                L_0x0f5d:
                    r0 = move-exception
                    goto L_0x0f64
                L_0x0f5f:
                    r0 = move-exception
                    r38 = r3
                    r21 = r14
                L_0x0f64:
                    r1 = r61
                    r61 = r0
                    r0 = r10
                    r53 = r20
                    r16 = r21
                    r14 = r22
                    r10 = r27
                    r11 = r29
                    r52 = r30
                    r3 = r37
                    r23 = r38
                    r56 = r8
                    r8 = r4
                    r4 = r56
                    r57 = r6
                    r6 = r5
                    r5 = r9
                    r7 = r15
                    r9 = r28
                    r15 = r13
                    r12 = r57
                    goto L_0x1a70
                L_0x0f8a:
                    r38 = r3
                    r18 = r11
                    r21 = r14
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x1a2d }
                    com.portfolio.platform.service.MFDeviceService$a r3 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r3 = r3.b()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x1a2d }
                    r11.<init>()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r12 = "[SYNC] Process data done,syncData size "
                    r11.append(r12)     // Catch:{ Exception -> 0x1a2d }
                    int r12 = r1.size()     // Catch:{ Exception -> 0x1a2d }
                    r11.append(r12)     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x1a2d }
                    r0.d(r3, r11)     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r0.getRemote()     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC     // Catch:{ Exception -> 0x1a2d }
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r35 = r0.b()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x1a2d }
                    r0.<init>()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r3 = "[Sync Data Received] After get data, size "
                    r0.append(r3)     // Catch:{ Exception -> 0x1a2d }
                    int r3 = r1.size()     // Catch:{ Exception -> 0x1a2d }
                    r0.append(r3)     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r36 = r0.toString()     // Catch:{ Exception -> 0x1a2d }
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)     // Catch:{ Exception -> 0x1a2d }
                    boolean r0 = r1.isEmpty()     // Catch:{ Exception -> 0x1a2d }
                    if (r0 != 0) goto L_0x0fe6
                    r0 = 1
                    goto L_0x0fe7
                L_0x0fe6:
                    r0 = 0
                L_0x0fe7:
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x1a2d }
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x1a2d }
                    com.portfolio.platform.service.MFDeviceService$a r11 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r11 = r11.b()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x1a2d }
                    r12.<init>()     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r14 = ".onReadDataFilesSuccess(), sdk = "
                    r12.append(r14)     // Catch:{ Exception -> 0x1a2d }
                    com.portfolio.platform.service.MFDeviceService$d r14 = r15.this$0     // Catch:{ Exception -> 0x1a2d }
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a     // Catch:{ Exception -> 0x1a2d }
                    com.google.gson.Gson r14 = r14.J     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r14 = r14.a(r1)     // Catch:{ Exception -> 0x1a2d }
                    r12.append(r14)     // Catch:{ Exception -> 0x1a2d }
                    java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x1a2d }
                    r3.d(r11, r12)     // Catch:{ Exception -> 0x1a2d }
                    boolean r3 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r5)     // Catch:{ Exception -> 0x1a2d }
                    if (r3 == 0) goto L_0x143b
                    if (r0 == 0) goto L_0x141f
                    com.portfolio.platform.service.MFDeviceService$d r3 = r15.this$0     // Catch:{ Exception -> 0x13f4 }
                    com.portfolio.platform.service.MFDeviceService r3 = r3.a     // Catch:{ Exception -> 0x13f4 }
                    org.joda.time.DateTime r11 = new org.joda.time.DateTime     // Catch:{ Exception -> 0x13f4 }
                    r11.<init>()     // Catch:{ Exception -> 0x13f4 }
                    java.util.List r3 = r3.a(r1, r5, r11)     // Catch:{ Exception -> 0x13f4 }
                    com.portfolio.platform.service.MFDeviceService$d r11 = r15.this$0     // Catch:{ Exception -> 0x13f4 }
                    com.portfolio.platform.service.MFDeviceService r11 = r11.a     // Catch:{ Exception -> 0x13f4 }
                    com.portfolio.platform.data.source.FitnessDataRepository r11 = r11.g()     // Catch:{ Exception -> 0x13f4 }
                    r15.L$0 = r9     // Catch:{ Exception -> 0x13f4 }
                    r15.I$0 = r2     // Catch:{ Exception -> 0x13f4 }
                    r15.L$1 = r8     // Catch:{ Exception -> 0x13f4 }
                    r15.L$2 = r5     // Catch:{ Exception -> 0x13f4 }
                    r15.I$1 = r13     // Catch:{ Exception -> 0x13f4 }
                    r15.I$2 = r10     // Catch:{ Exception -> 0x13f4 }
                    r15.L$3 = r4     // Catch:{ Exception -> 0x13f4 }
                    r12 = r37
                    r15.L$4 = r12     // Catch:{ Exception -> 0x13ec }
                    r14 = r61
                    r15.L$5 = r14     // Catch:{ Exception -> 0x13e6 }
                    r61 = r14
                    r14 = r29
                    r15.I$3 = r14     // Catch:{ Exception -> 0x13e0 }
                    r29 = r14
                    r14 = r28
                    r15.I$4 = r14     // Catch:{ Exception -> 0x13da }
                    r28 = r14
                    r14 = r27
                    r15.I$5 = r14     // Catch:{ Exception -> 0x13d4 }
                    r27 = r14
                    r14 = r22
                    r15.L$6 = r14     // Catch:{ Exception -> 0x13d0 }
                    r15.J$0 = r6     // Catch:{ Exception -> 0x13d0 }
                    r39 = r6
                    r6 = r24
                    r15.J$1 = r6     // Catch:{ Exception -> 0x13cc }
                    r24 = r6
                    r6 = r38
                    r15.L$7 = r6     // Catch:{ Exception -> 0x13c8 }
                    r7 = r21
                    r15.L$8 = r7     // Catch:{ Exception -> 0x13c4 }
                    r15.L$9 = r1     // Catch:{ Exception -> 0x13c4 }
                    r21 = r7
                    r7 = r18
                    r15.L$10 = r7
                    r15.I$6 = r0
                    r15.L$11 = r3
                    r18 = r7
                    r7 = 4
                    r15.label = r7
                    java.lang.Object r7 = r11.saveFitnessData(r3, r15)
                    r11 = r20
                    if (r7 != r11) goto L_0x108a
                    return r11
                L_0x108a:
                    r17 = r0
                    r0 = r3
                    r20 = r11
                    r3 = r12
                    r22 = r14
                    r12 = r2
                    r2 = r4
                    r14 = r9
                    r11 = r10
                    r10 = r13
                    r13 = r21
                    r21 = r1
                    r9 = r6
                    r1 = r8
                    r6 = r24
                    r24 = r27
                    r25 = r28
                    r27 = r29
                    r8 = r61
                    r61 = r5
                    r4 = r39
                L_0x10ab:
                    com.fossil.rk5 r41 = com.fossil.rk5.b     // Catch:{ Exception -> 0x1375 }
                    r28 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.PortfolioApp r50 = r8.c()     // Catch:{ Exception -> 0x1359 }
                    r42 = r61
                    r43 = r0
                    r44 = r13
                    r45 = r9
                    r46 = r4
                    r48 = r6
                    com.fossil.rk5$a r8 = r41.a(r42, r43, r44, r45, r46, r48, r50)     // Catch:{ Exception -> 0x1359 }
                    com.fossil.rk5 r29 = com.fossil.rk5.b     // Catch:{ Exception -> 0x1359 }
                    r31 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.SleepSessionsRepository r8 = r8.p()     // Catch:{ Exception -> 0x1359 }
                    r32 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.SummariesRepository r8 = r8.r()     // Catch:{ Exception -> 0x1359 }
                    r33 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.SleepSummariesRepository r8 = r8.q()     // Catch:{ Exception -> 0x1359 }
                    r34 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.HeartRateSampleRepository r8 = r8.j()     // Catch:{ Exception -> 0x1359 }
                    r35 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r8 = r8.k()     // Catch:{ Exception -> 0x1359 }
                    r36 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.WorkoutSessionRepository r37 = r8.w()     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.FitnessDataRepository r38 = r8.g()     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.ActivitiesRepository r39 = r8.b()     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.data.source.ThirdPartyRepository r40 = r8.s()     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.PortfolioApp r41 = r8.c()     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r15.this$0     // Catch:{ Exception -> 0x1359 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1359 }
                    com.fossil.ge5 r42 = r8.h()     // Catch:{ Exception -> 0x1359 }
                    r15.L$0 = r14     // Catch:{ Exception -> 0x1359 }
                    r15.I$0 = r12     // Catch:{ Exception -> 0x1359 }
                    r15.L$1 = r1     // Catch:{ Exception -> 0x1359 }
                    r8 = r61
                    r15.L$2 = r8     // Catch:{ Exception -> 0x134f }
                    r15.I$1 = r10     // Catch:{ Exception -> 0x134f }
                    r15.I$2 = r11     // Catch:{ Exception -> 0x134f }
                    r15.L$3 = r2     // Catch:{ Exception -> 0x134f }
                    r15.L$4 = r3     // Catch:{ Exception -> 0x134f }
                    r43 = r10
                    r10 = r28
                    r15.L$5 = r10     // Catch:{ Exception -> 0x1331 }
                    r28 = r14
                    r14 = r27
                    r15.I$3 = r14     // Catch:{ Exception -> 0x1308 }
                    r27 = r1
                    r1 = r25
                    r15.I$4 = r1     // Catch:{ Exception -> 0x12e2 }
                    r25 = r2
                    r2 = r24
                    r15.I$5 = r2     // Catch:{ Exception -> 0x12c0 }
                    r24 = r3
                    r3 = r22
                    r15.L$6 = r3     // Catch:{ Exception -> 0x1285 }
                    r15.J$0 = r4     // Catch:{ Exception -> 0x1285 }
                    r15.J$1 = r6     // Catch:{ Exception -> 0x1285 }
                    r15.L$7 = r9     // Catch:{ Exception -> 0x1285 }
                    r15.L$8 = r13     // Catch:{ Exception -> 0x1285 }
                    r61 = r9
                    r9 = r21
                    r15.L$9 = r9     // Catch:{ Exception -> 0x127b }
                    r21 = r10
                    r10 = r18
                    r15.L$10 = r10     // Catch:{ Exception -> 0x1257 }
                    r18 = r14
                    r14 = r17
                    r15.I$6 = r14     // Catch:{ Exception -> 0x1247 }
                    r15.L$11 = r0     // Catch:{ Exception -> 0x1247 }
                    r0 = r31
                    r15.L$12 = r0     // Catch:{ Exception -> 0x1247 }
                    r17 = r1
                    r1 = 5
                    r15.label = r1     // Catch:{ Exception -> 0x1222 }
                    r22 = r20
                    r26 = 10
                    r20 = r17
                    r17 = r27
                    r1 = r29
                    r27 = r2
                    r29 = r30
                    r2 = r0
                    r30 = r3
                    r3 = r8
                    r44 = r4
                    r16 = 0
                    r4 = r32
                    r5 = r33
                    r31 = r6
                    r6 = r34
                    r19 = 1
                    r7 = r35
                    r0 = r9
                    r33 = r21
                    r21 = r8
                    r8 = r36
                    r16 = r61
                    r9 = r37
                    r34 = r10
                    r19 = r43
                    r10 = r38
                    r51 = r23
                    r23 = r11
                    r11 = r39
                    r52 = r29
                    r29 = r12
                    r12 = r40
                    r26 = r13
                    r13 = r41
                    r53 = r22
                    r22 = r18
                    r18 = r28
                    r28 = r14
                    r14 = r42
                    r15 = r60
                    java.lang.Object r1 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x121b }
                    r15 = r53
                    if (r1 != r15) goto L_0x11da
                    return r15
                L_0x11da:
                    r2 = r0
                    r4 = r16
                    r13 = r20
                    r3 = r26
                    r10 = r27
                    r0 = r28
                    r1 = r29
                    r9 = r30
                    r5 = r31
                    r7 = r44
                    r27 = r17
                    r29 = r18
                    r26 = r21
                    r21 = r33
                    r17 = r34
                L_0x11f7:
                    r18 = r3
                    r14 = r9
                    r9 = r13
                    r53 = r15
                    r16 = r17
                    r11 = r19
                    r15 = r21
                    r13 = r22
                    r12 = r23
                    r3 = r25
                    r17 = r2
                    r25 = r4
                    r2 = r24
                    r4 = r26
                L_0x1211:
                    r56 = r5
                    r5 = r27
                    r26 = r56
                    r6 = r29
                    goto L_0x1852
                L_0x121b:
                    r0 = move-exception
                    r7 = r60
                    r61 = r0
                    goto L_0x12ae
                L_0x1222:
                    r0 = move-exception
                    r16 = r61
                    r44 = r4
                    r31 = r6
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r22 = r18
                    r15 = r20
                    r33 = r21
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r30 = r3
                    r21 = r8
                    r20 = r17
                    r17 = r27
                    r27 = r2
                    goto L_0x12a8
                L_0x1247:
                    r0 = move-exception
                    r16 = r61
                    r44 = r4
                    r31 = r6
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r22 = r18
                    goto L_0x1266
                L_0x1257:
                    r0 = move-exception
                    r16 = r61
                    r44 = r4
                    r31 = r6
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r22 = r14
                L_0x1266:
                    r15 = r20
                    r33 = r21
                    r17 = r27
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r20 = r1
                    r27 = r2
                    r30 = r3
                    r21 = r8
                    goto L_0x12a8
                L_0x127b:
                    r0 = move-exception
                    r16 = r61
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    goto L_0x128e
                L_0x1285:
                    r0 = move-exception
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    r16 = r9
                L_0x128e:
                    r33 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r22 = r14
                    r15 = r20
                    r17 = r27
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r20 = r1
                    r27 = r2
                    r30 = r3
                L_0x12a8:
                    r7 = r60
                    r61 = r0
                    r53 = r15
                L_0x12ae:
                    r4 = r17
                    r5 = r18
                    r15 = r19
                    r9 = r20
                    r6 = r21
                    r11 = r22
                    r0 = r23
                    r3 = r24
                    goto L_0x13b0
                L_0x12c0:
                    r0 = move-exception
                    r24 = r3
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    r16 = r9
                    r33 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r15 = r20
                    r17 = r27
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r20 = r1
                    r27 = r2
                    goto L_0x132b
                L_0x12e2:
                    r0 = move-exception
                    r25 = r2
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    r16 = r9
                    r33 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r15 = r20
                    r17 = r27
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r20 = r1
                    r30 = r22
                    r27 = r24
                    r24 = r3
                    goto L_0x132d
                L_0x1308:
                    r0 = move-exception
                    r17 = r1
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    r16 = r9
                    r33 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r15 = r20
                    r27 = r24
                    r20 = r25
                    r18 = r28
                    r52 = r30
                    r19 = r43
                    r25 = r2
                    r24 = r3
                L_0x132b:
                    r30 = r22
                L_0x132d:
                    r22 = r14
                    goto L_0x139c
                L_0x1331:
                    r0 = move-exception
                    r17 = r1
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    r16 = r9
                    r33 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r18 = r14
                    r15 = r20
                    r20 = r25
                    r52 = r30
                    r19 = r43
                    goto L_0x1392
                L_0x134f:
                    r0 = move-exception
                    r17 = r1
                    r44 = r4
                    r31 = r6
                    r21 = r8
                    goto L_0x1362
                L_0x1359:
                    r0 = move-exception
                    r21 = r61
                    r17 = r1
                    r44 = r4
                    r31 = r6
                L_0x1362:
                    r16 = r9
                    r19 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r18 = r14
                    r15 = r20
                    r20 = r25
                    r33 = r28
                    goto L_0x1390
                L_0x1375:
                    r0 = move-exception
                    r21 = r61
                    r17 = r1
                    r44 = r4
                    r31 = r6
                    r33 = r8
                    r16 = r9
                    r19 = r10
                    r23 = r11
                    r29 = r12
                    r26 = r13
                    r18 = r14
                    r15 = r20
                    r20 = r25
                L_0x1390:
                    r52 = r30
                L_0x1392:
                    r25 = r2
                    r30 = r22
                    r22 = r27
                    r27 = r24
                    r24 = r3
                L_0x139c:
                    r7 = r60
                    r61 = r0
                    r53 = r15
                    r4 = r17
                    r5 = r18
                    r15 = r19
                    r9 = r20
                    r6 = r21
                    r11 = r22
                    r0 = r23
                L_0x13b0:
                    r8 = r25
                    r10 = r27
                    r2 = r29
                    r14 = r30
                    r24 = r31
                    r1 = r33
                    r12 = r44
                    r23 = r16
                    r16 = r26
                    goto L_0x1a70
                L_0x13c4:
                    r0 = move-exception
                    r21 = r7
                    goto L_0x13c9
                L_0x13c8:
                    r0 = move-exception
                L_0x13c9:
                    r52 = r30
                    goto L_0x13ff
                L_0x13cc:
                    r0 = move-exception
                    r24 = r6
                    goto L_0x13f1
                L_0x13d0:
                    r0 = move-exception
                    r39 = r6
                    goto L_0x13f1
                L_0x13d4:
                    r0 = move-exception
                    r39 = r6
                    r27 = r14
                    goto L_0x13ef
                L_0x13da:
                    r0 = move-exception
                    r39 = r6
                    r28 = r14
                    goto L_0x13ef
                L_0x13e0:
                    r0 = move-exception
                    r39 = r6
                    r29 = r14
                    goto L_0x13ef
                L_0x13e6:
                    r0 = move-exception
                    r39 = r6
                    r61 = r14
                    goto L_0x13ef
                L_0x13ec:
                    r0 = move-exception
                    r39 = r6
                L_0x13ef:
                    r14 = r22
                L_0x13f1:
                    r52 = r30
                    goto L_0x13fd
                L_0x13f4:
                    r0 = move-exception
                    r39 = r6
                    r14 = r22
                    r52 = r30
                    r12 = r37
                L_0x13fd:
                    r6 = r38
                L_0x13ff:
                    r7 = r60
                    r1 = r61
                    r61 = r0
                    r23 = r6
                    r0 = r10
                    r3 = r12
                    r15 = r13
                    r53 = r20
                    r16 = r21
                    r10 = r27
                    r11 = r29
                    r12 = r39
                    r6 = r5
                    r5 = r9
                    r9 = r28
                    r56 = r8
                    r8 = r4
                    r4 = r56
                    goto L_0x1a70
                L_0x141f:
                    r39 = r6
                    r14 = r22
                    r51 = r23
                    r52 = r30
                    r6 = r38
                    r17 = r0
                    r19 = r8
                    r16 = r13
                    r11 = r20
                    r8 = r21
                    r12 = r39
                    r20 = r9
                    r9 = r18
                    goto L_0x182f
                L_0x143b:
                    r39 = r6
                    r15 = r20
                    r14 = r22
                    r51 = r23
                    r52 = r30
                    r12 = r37
                    r6 = r38
                    if (r0 == 0) goto L_0x181e
                    r11 = r60
                    com.portfolio.platform.service.MFDeviceService$d r3 = r11.this$0     // Catch:{ Exception -> 0x1804 }
                    com.portfolio.platform.service.MFDeviceService r3 = r3.a     // Catch:{ Exception -> 0x1804 }
                    org.joda.time.DateTime r7 = new org.joda.time.DateTime     // Catch:{ Exception -> 0x1804 }
                    r7.<init>()     // Catch:{ Exception -> 0x1804 }
                    java.util.List r3 = r3.a(r1, r5, r7)     // Catch:{ Exception -> 0x1804 }
                    com.portfolio.platform.service.MFDeviceService$d r7 = r11.this$0     // Catch:{ Exception -> 0x1804 }
                    com.portfolio.platform.service.MFDeviceService r7 = r7.a     // Catch:{ Exception -> 0x1804 }
                    com.portfolio.platform.data.source.FitnessDataRepository r7 = r7.g()     // Catch:{ Exception -> 0x1804 }
                    r11.L$0 = r9     // Catch:{ Exception -> 0x1804 }
                    r11.I$0 = r2     // Catch:{ Exception -> 0x1804 }
                    r11.L$1 = r8     // Catch:{ Exception -> 0x1804 }
                    r11.L$2 = r5     // Catch:{ Exception -> 0x1804 }
                    r11.I$1 = r13     // Catch:{ Exception -> 0x1804 }
                    r11.I$2 = r10     // Catch:{ Exception -> 0x1804 }
                    r11.L$3 = r4     // Catch:{ Exception -> 0x1804 }
                    r11.L$4 = r12     // Catch:{ Exception -> 0x1804 }
                    r37 = r12
                    r12 = r61
                    r11.L$5 = r12     // Catch:{ Exception -> 0x17fc }
                    r61 = r12
                    r12 = r29
                    r11.I$3 = r12     // Catch:{ Exception -> 0x17f4 }
                    r29 = r12
                    r12 = r28
                    r11.I$4 = r12     // Catch:{ Exception -> 0x17ec }
                    r28 = r12
                    r12 = r27
                    r11.I$5 = r12     // Catch:{ Exception -> 0x17e4 }
                    r11.L$6 = r14     // Catch:{ Exception -> 0x17e4 }
                    r27 = r12
                    r16 = r13
                    r12 = r39
                    r11.J$0 = r12     // Catch:{ Exception -> 0x17dc }
                    r19 = r8
                    r20 = r9
                    r8 = r24
                    r11.J$1 = r8     // Catch:{ Exception -> 0x17d8 }
                    r11.L$7 = r6     // Catch:{ Exception -> 0x17d8 }
                    r24 = r8
                    r8 = r21
                    r11.L$8 = r8     // Catch:{ Exception -> 0x17d6 }
                    r11.L$9 = r1     // Catch:{ Exception -> 0x17d6 }
                    r9 = r18
                    r11.L$10 = r9     // Catch:{ Exception -> 0x17d6 }
                    r11.I$6 = r0     // Catch:{ Exception -> 0x17d6 }
                    r11.L$11 = r3     // Catch:{ Exception -> 0x17d6 }
                    r17 = r0
                    r0 = 6
                    r11.label = r0     // Catch:{ Exception -> 0x17d6 }
                    java.lang.Object r0 = r7.saveFitnessData(r3, r11)     // Catch:{ Exception -> 0x17d6 }
                    if (r0 != r15) goto L_0x14ba
                    return r15
                L_0x14ba:
                    r18 = r1
                    r0 = r3
                    r53 = r15
                    r1 = r19
                    r21 = r28
                    r22 = r29
                    r3 = r37
                    r15 = r10
                    r19 = r14
                    r10 = r16
                    r16 = r17
                    r14 = r20
                    r20 = r27
                    r17 = r9
                    r9 = r6
                    r6 = r24
                    r56 = r8
                    r8 = r61
                    r61 = r5
                    r57 = r12
                    r12 = r2
                    r2 = r4
                    r13 = r56
                    r4 = r57
                L_0x14e5:
                    com.fossil.sk5 r38 = com.fossil.sk5.b     // Catch:{ Exception -> 0x178d }
                    r23 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.PortfolioApp r47 = r8.c()     // Catch:{ Exception -> 0x1769 }
                    r39 = r61
                    r40 = r0
                    r41 = r13
                    r42 = r9
                    r43 = r4
                    r45 = r6
                    com.fossil.sk5$a r8 = r38.a(r39, r40, r41, r42, r43, r45, r47)     // Catch:{ Exception -> 0x1769 }
                    com.fossil.sk5 r24 = com.fossil.sk5.b     // Catch:{ Exception -> 0x1769 }
                    r25 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.SleepSessionsRepository r8 = r8.p()     // Catch:{ Exception -> 0x1769 }
                    r26 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.SummariesRepository r8 = r8.r()     // Catch:{ Exception -> 0x1769 }
                    r27 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.SleepSummariesRepository r8 = r8.q()     // Catch:{ Exception -> 0x1769 }
                    r28 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.FitnessDataRepository r8 = r8.g()     // Catch:{ Exception -> 0x1769 }
                    r29 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.ActivitiesRepository r8 = r8.b()     // Catch:{ Exception -> 0x1769 }
                    r30 = r8
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.HybridPresetRepository r31 = r8.n()     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.GoalTrackingRepository r32 = r8.i()     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.ThirdPartyRepository r33 = r8.s()     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.data.source.UserRepository r34 = r8.t()     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.PortfolioApp r35 = r8.c()     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService$d r8 = r11.this$0     // Catch:{ Exception -> 0x1769 }
                    com.portfolio.platform.service.MFDeviceService r8 = r8.a     // Catch:{ Exception -> 0x1769 }
                    com.fossil.ge5 r36 = r8.h()     // Catch:{ Exception -> 0x1769 }
                    r11.L$0 = r14     // Catch:{ Exception -> 0x1769 }
                    r11.I$0 = r12     // Catch:{ Exception -> 0x1769 }
                    r11.L$1 = r1     // Catch:{ Exception -> 0x1769 }
                    r8 = r61
                    r11.L$2 = r8     // Catch:{ Exception -> 0x175f }
                    r11.I$1 = r10     // Catch:{ Exception -> 0x175f }
                    r11.I$2 = r15     // Catch:{ Exception -> 0x175f }
                    r11.L$3 = r2     // Catch:{ Exception -> 0x175f }
                    r11.L$4 = r3     // Catch:{ Exception -> 0x175f }
                    r37 = r10
                    r10 = r23
                    r11.L$5 = r10     // Catch:{ Exception -> 0x1743 }
                    r23 = r14
                    r14 = r22
                    r11.I$3 = r14     // Catch:{ Exception -> 0x1720 }
                    r22 = r1
                    r1 = r21
                    r11.I$4 = r1     // Catch:{ Exception -> 0x16f9 }
                    r21 = r2
                    r2 = r20
                    r11.I$5 = r2     // Catch:{ Exception -> 0x16db }
                    r20 = r3
                    r3 = r19
                    r11.L$6 = r3     // Catch:{ Exception -> 0x16aa }
                    r11.J$0 = r4     // Catch:{ Exception -> 0x16aa }
                    r11.J$1 = r6     // Catch:{ Exception -> 0x16aa }
                    r11.L$7 = r9     // Catch:{ Exception -> 0x16aa }
                    r11.L$8 = r13     // Catch:{ Exception -> 0x16aa }
                    r61 = r9
                    r9 = r18
                    r11.L$9 = r9     // Catch:{ Exception -> 0x169e }
                    r18 = r10
                    r10 = r17
                    r11.L$10 = r10     // Catch:{ Exception -> 0x1692 }
                    r17 = r14
                    r14 = r16
                    r11.I$6 = r14     // Catch:{ Exception -> 0x1670 }
                    r11.L$11 = r0     // Catch:{ Exception -> 0x1670 }
                    r0 = r25
                    r11.L$12 = r0     // Catch:{ Exception -> 0x1670 }
                    r16 = r1
                    r1 = 7
                    r11.label = r1     // Catch:{ Exception -> 0x166c }
                    r19 = r22
                    r1 = r24
                    r24 = r21
                    r21 = r2
                    r2 = r0
                    r22 = r3
                    r3 = r8
                    r38 = r4
                    r4 = r26
                    r5 = r27
                    r25 = r6
                    r6 = r28
                    r7 = r29
                    r27 = r8
                    r0 = r9
                    r8 = r30
                    r28 = r61
                    r9 = r31
                    r30 = r10
                    r29 = r37
                    r10 = r32
                    r11 = r33
                    r31 = r12
                    r12 = r34
                    r32 = r13
                    r13 = r35
                    r33 = r14
                    r56 = r23
                    r23 = r17
                    r17 = r56
                    r14 = r36
                    r34 = r15
                    r54 = r53
                    r15 = r60
                    java.lang.Object r1 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x1641 }
                    r11 = r54
                    if (r1 != r11) goto L_0x1606
                    return r11
                L_0x1606:
                    r2 = r0
                    r9 = r16
                    r13 = r21
                    r10 = r22
                    r5 = r25
                    r26 = r27
                    r4 = r28
                    r28 = r29
                    r1 = r31
                    r3 = r32
                    r0 = r33
                    r7 = r38
                    r29 = r17
                    r21 = r18
                    r27 = r19
                    r22 = r20
                L_0x1625:
                    r17 = r2
                    r18 = r3
                    r25 = r4
                    r14 = r10
                    r53 = r11
                    r10 = r13
                    r15 = r21
                    r2 = r22
                    r13 = r23
                    r3 = r24
                    r4 = r26
                    r11 = r28
                    r16 = r30
                    r12 = r34
                    goto L_0x1211
                L_0x1641:
                    r0 = move-exception
                    r7 = r60
                    r61 = r0
                    r9 = r16
                    r5 = r17
                    r1 = r18
                    r4 = r19
                    r3 = r20
                    r10 = r21
                    r14 = r22
                    r11 = r23
                    r8 = r24
                    r24 = r25
                    r6 = r27
                    r23 = r28
                    r15 = r29
                    r2 = r31
                    r16 = r32
                    r0 = r34
                    r12 = r38
                    r53 = r54
                    goto L_0x1a70
                L_0x166c:
                    r0 = move-exception
                    r28 = r61
                    goto L_0x1675
                L_0x1670:
                    r0 = move-exception
                    r28 = r61
                    r16 = r1
                L_0x1675:
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r31 = r12
                    r32 = r13
                    r34 = r15
                    r24 = r21
                    r19 = r22
                    r29 = r37
                    r21 = r2
                    r22 = r3
                    r56 = r23
                    r23 = r17
                    r17 = r56
                    goto L_0x16cb
                L_0x1692:
                    r0 = move-exception
                    r28 = r61
                    r16 = r1
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    goto L_0x16b7
                L_0x169e:
                    r0 = move-exception
                    r28 = r61
                    r16 = r1
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    goto L_0x16b5
                L_0x16aa:
                    r0 = move-exception
                    r16 = r1
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r28 = r9
                L_0x16b5:
                    r18 = r10
                L_0x16b7:
                    r31 = r12
                    r32 = r13
                    r34 = r15
                    r24 = r21
                    r19 = r22
                    r17 = r23
                    r29 = r37
                    r21 = r2
                    r22 = r3
                    r23 = r14
                L_0x16cb:
                    r7 = r60
                    r61 = r0
                    r9 = r16
                    r5 = r17
                    r1 = r18
                    r4 = r19
                    r3 = r20
                    goto L_0x17bc
                L_0x16db:
                    r0 = move-exception
                    r16 = r1
                    r20 = r3
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r28 = r9
                    r18 = r10
                    r31 = r12
                    r32 = r13
                    r34 = r15
                    r24 = r21
                    r17 = r23
                    r29 = r37
                    r21 = r2
                    goto L_0x1716
                L_0x16f9:
                    r0 = move-exception
                    r16 = r1
                    r24 = r2
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r28 = r9
                    r18 = r10
                    r31 = r12
                    r32 = r13
                    r34 = r15
                    r21 = r20
                    r17 = r23
                    r29 = r37
                    r20 = r3
                L_0x1716:
                    r23 = r14
                    r56 = r22
                    r22 = r19
                    r19 = r56
                    goto L_0x17b0
                L_0x1720:
                    r0 = move-exception
                    r24 = r2
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r28 = r9
                    r18 = r10
                    r31 = r12
                    r32 = r13
                    r34 = r15
                    r22 = r19
                    r16 = r21
                    r17 = r23
                    r29 = r37
                    r19 = r1
                    r23 = r14
                    r21 = r20
                    goto L_0x17ae
                L_0x1743:
                    r0 = move-exception
                    r24 = r2
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    r28 = r9
                    r18 = r10
                    r31 = r12
                    r32 = r13
                    r17 = r14
                    r34 = r15
                    r16 = r21
                    r23 = r22
                    r29 = r37
                    goto L_0x17a8
                L_0x175f:
                    r0 = move-exception
                    r24 = r2
                    r38 = r4
                    r25 = r6
                    r27 = r8
                    goto L_0x1772
                L_0x1769:
                    r0 = move-exception
                    r27 = r61
                    r24 = r2
                    r38 = r4
                    r25 = r6
                L_0x1772:
                    r28 = r9
                    r29 = r10
                    r31 = r12
                    r32 = r13
                    r17 = r14
                    r34 = r15
                    r16 = r21
                    r18 = r23
                    r21 = r20
                    r23 = r22
                    r20 = r3
                    r22 = r19
                    r19 = r1
                    goto L_0x17b0
                L_0x178d:
                    r0 = move-exception
                    r27 = r61
                    r24 = r2
                    r38 = r4
                    r25 = r6
                    r18 = r8
                    r28 = r9
                    r29 = r10
                    r31 = r12
                    r32 = r13
                    r17 = r14
                    r34 = r15
                    r16 = r21
                    r23 = r22
                L_0x17a8:
                    r22 = r19
                    r21 = r20
                    r19 = r1
                L_0x17ae:
                    r20 = r3
                L_0x17b0:
                    r7 = r60
                    r61 = r0
                    r9 = r16
                    r5 = r17
                    r1 = r18
                    r4 = r19
                L_0x17bc:
                    r10 = r21
                    r14 = r22
                    r11 = r23
                    r8 = r24
                    r24 = r25
                    r6 = r27
                    r23 = r28
                    r15 = r29
                    r2 = r31
                    r16 = r32
                    r0 = r34
                    r12 = r38
                    goto L_0x1a70
                L_0x17d6:
                    r0 = move-exception
                    goto L_0x1811
                L_0x17d8:
                    r0 = move-exception
                    r24 = r8
                    goto L_0x17e1
                L_0x17dc:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                L_0x17e1:
                    r8 = r21
                    goto L_0x1811
                L_0x17e4:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                    r27 = r12
                    goto L_0x180b
                L_0x17ec:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                    r28 = r12
                    goto L_0x180b
                L_0x17f4:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                    r29 = r12
                    goto L_0x180b
                L_0x17fc:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                    r61 = r12
                    goto L_0x180b
                L_0x1804:
                    r0 = move-exception
                    r19 = r8
                    r20 = r9
                    r37 = r12
                L_0x180b:
                    r16 = r13
                    r8 = r21
                    r12 = r39
                L_0x1811:
                    r7 = r60
                    r1 = r61
                    r61 = r0
                    r23 = r6
                    r0 = r10
                    r53 = r15
                    goto L_0x1a5e
                L_0x181e:
                    r17 = r0
                    r19 = r8
                    r20 = r9
                    r37 = r12
                    r16 = r13
                    r11 = r15
                    r9 = r18
                    r8 = r21
                    r12 = r39
                L_0x182f:
                    r15 = r61
                    r3 = r4
                    r4 = r5
                    r18 = r8
                    r53 = r11
                    r7 = r12
                    r11 = r16
                    r0 = r17
                    r5 = r19
                    r13 = r29
                    r17 = r1
                    r1 = r2
                    r16 = r9
                    r12 = r10
                    r10 = r27
                    r9 = r28
                    r2 = r37
                    r26 = r24
                    r25 = r6
                    r6 = r20
                L_0x1852:
                    if (r0 == 0) goto L_0x190b
                    r28 = r7
                    r7 = r60
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0     // Catch:{ Exception -> 0x18f5 }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x18f5 }
                    com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x18f5 }
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r8 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC     // Catch:{ Exception -> 0x18f5 }
                    r30 = r14
                    java.lang.String r14 = "Process data OK."
                    r0.a(r8, r4, r14)     // Catch:{ Exception -> 0x18ed }
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x18ed }
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r19 = r0.getRemote()     // Catch:{ Exception -> 0x18ed }
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r20 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x18ed }
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r21 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC     // Catch:{ Exception -> 0x18ed }
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x18ed }
                    java.lang.String r23 = r0.b()     // Catch:{ Exception -> 0x18ed }
                    java.lang.String r24 = "Process data OK."
                    r22 = r4
                    r19.i(r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x18ed }
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0     // Catch:{ Exception -> 0x18ed }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x18ed }
                    com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x18ed }
                    r7.L$0 = r6     // Catch:{ Exception -> 0x18ed }
                    r7.I$0 = r1     // Catch:{ Exception -> 0x18ed }
                    r7.L$1 = r5     // Catch:{ Exception -> 0x18ed }
                    r7.L$2 = r4     // Catch:{ Exception -> 0x18ed }
                    r7.I$1 = r11     // Catch:{ Exception -> 0x18ed }
                    r7.I$2 = r12     // Catch:{ Exception -> 0x18ed }
                    r7.L$3 = r3     // Catch:{ Exception -> 0x18ed }
                    r7.L$4 = r2     // Catch:{ Exception -> 0x18ed }
                    r7.L$5 = r15     // Catch:{ Exception -> 0x18ed }
                    r7.I$3 = r13     // Catch:{ Exception -> 0x18ed }
                    r7.I$4 = r9     // Catch:{ Exception -> 0x18ed }
                    r7.I$5 = r10     // Catch:{ Exception -> 0x18ed }
                    r14 = r30
                    r7.L$6 = r14
                    r30 = r9
                    r8 = r28
                    r7.J$0 = r8     // Catch:{ Exception -> 0x18dc }
                    r28 = r8
                    r8 = r26
                    r7.J$1 = r8     // Catch:{ Exception -> 0x18d8 }
                    r26 = r8
                    r8 = r25
                    r7.L$7 = r8     // Catch:{ Exception -> 0x18d6 }
                    r9 = r18
                    r7.L$8 = r9     // Catch:{ Exception -> 0x18d2 }
                    r18 = r9
                    r9 = r17
                    r7.L$9 = r9
                    r9 = r16
                    r7.L$10 = r9
                    r9 = 8
                    r7.label = r9
                    r9 = 0
                    java.lang.Object r0 = r0.a(r9, r7)
                    r9 = r53
                    if (r0 != r9) goto L_0x1e42
                    return r9
                L_0x18d2:
                    r0 = move-exception
                    r18 = r9
                    goto L_0x18e1
                L_0x18d6:
                    r0 = move-exception
                    goto L_0x18e1
                L_0x18d8:
                    r0 = move-exception
                    r26 = r8
                    goto L_0x18df
                L_0x18dc:
                    r0 = move-exception
                    r28 = r8
                L_0x18df:
                    r8 = r25
                L_0x18e1:
                    r61 = r0
                    r23 = r8
                    r0 = r12
                    r16 = r18
                    r24 = r26
                    r9 = r30
                    goto L_0x1903
                L_0x18ed:
                    r0 = move-exception
                    r8 = r25
                    r14 = r30
                    r30 = r9
                    goto L_0x18fa
                L_0x18f5:
                    r0 = move-exception
                    r30 = r9
                    r8 = r25
                L_0x18fa:
                    r61 = r0
                    r23 = r8
                    r0 = r12
                    r16 = r18
                    r24 = r26
                L_0x1903:
                    r8 = r3
                    r3 = r2
                    r2 = r1
                    r1 = r15
                    r15 = r11
                    r11 = r13
                    goto L_0x1a24
                L_0x190b:
                    r28 = r7
                    r30 = r9
                    r0 = r16
                    r9 = r17
                    r8 = r25
                    r55 = r53
                    r7 = r60
                    com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x1a05 }
                    r17 = r0
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r16.getLocal()     // Catch:{ Exception -> 0x1a05 }
                    com.portfolio.platform.service.MFDeviceService$a r16 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x1a05 }
                    r25 = r9
                    java.lang.String r9 = r16.b()     // Catch:{ Exception -> 0x1a05 }
                    r16 = r8
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x19fa }
                    r8.<init>()     // Catch:{ Exception -> 0x19fa }
                    r31 = r14
                    r14 = r51
                    r8.append(r14)     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService$a r14 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r14 = r14.b()     // Catch:{ Exception -> 0x19ed }
                    r8.append(r14)     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r14 = ".processBleResult - data is empty."
                    r8.append(r14)     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x19ed }
                    r0.d(r9, r8)     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x19ed }
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r8 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r9 = "data is empty."
                    r0.a(r8, r4, r9)     // Catch:{ Exception -> 0x19ed }
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x19ed }
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r19 = r0.getRemote()     // Catch:{ Exception -> 0x19ed }
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r20 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x19ed }
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r21 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r23 = r0.b()     // Catch:{ Exception -> 0x19ed }
                    java.lang.String r24 = "data is empty."
                    r22 = r4
                    r19.i(r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a     // Catch:{ Exception -> 0x19ed }
                    com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x19ed }
                    r7.L$0 = r6     // Catch:{ Exception -> 0x19ed }
                    r7.I$0 = r1     // Catch:{ Exception -> 0x19ed }
                    r7.L$1 = r5     // Catch:{ Exception -> 0x19ed }
                    r7.L$2 = r4     // Catch:{ Exception -> 0x19ed }
                    r7.I$1 = r11     // Catch:{ Exception -> 0x19ed }
                    r7.I$2 = r12     // Catch:{ Exception -> 0x19ed }
                    r7.L$3 = r3     // Catch:{ Exception -> 0x19ed }
                    r7.L$4 = r2     // Catch:{ Exception -> 0x19ed }
                    r7.L$5 = r15     // Catch:{ Exception -> 0x19ed }
                    r7.I$3 = r13     // Catch:{ Exception -> 0x19ed }
                    r9 = r30
                    r7.I$4 = r9     // Catch:{ Exception -> 0x19e4 }
                    r7.I$5 = r10     // Catch:{ Exception -> 0x19e4 }
                    r14 = r31
                    r7.L$6 = r14     // Catch:{ Exception -> 0x19db }
                    r8 = r1
                    r19 = r2
                    r1 = r28
                    r7.J$0 = r1     // Catch:{ Exception -> 0x19d7 }
                    r28 = r1
                    r1 = r26
                    r7.J$1 = r1     // Catch:{ Exception -> 0x19d3 }
                    r26 = r1
                    r1 = r16
                    r7.L$7 = r1     // Catch:{ Exception -> 0x19cf }
                    r2 = r18
                    r7.L$8 = r2     // Catch:{ Exception -> 0x19c9 }
                    r16 = r1
                    r1 = r25
                    r7.L$9 = r1     // Catch:{ Exception -> 0x19c7 }
                    r1 = r17
                    r7.L$10 = r1     // Catch:{ Exception -> 0x19c7 }
                    r1 = 9
                    r7.label = r1     // Catch:{ Exception -> 0x19c7 }
                    r1 = 1
                    java.lang.Object r0 = r0.a(r1, r7)     // Catch:{ Exception -> 0x19c7 }
                    r1 = r55
                    if (r0 != r1) goto L_0x1e42
                    return r1
                L_0x19c7:
                    r0 = move-exception
                    goto L_0x19cc
                L_0x19c9:
                    r0 = move-exception
                    r16 = r1
                L_0x19cc:
                    r17 = r3
                    goto L_0x1a11
                L_0x19cf:
                    r0 = move-exception
                    r16 = r1
                    goto L_0x19df
                L_0x19d3:
                    r0 = move-exception
                    r26 = r1
                    goto L_0x19df
                L_0x19d7:
                    r0 = move-exception
                    r28 = r1
                    goto L_0x19df
                L_0x19db:
                    r0 = move-exception
                    r8 = r1
                    r19 = r2
                L_0x19df:
                    r17 = r3
                    r2 = r18
                    goto L_0x1a11
                L_0x19e4:
                    r0 = move-exception
                    r8 = r1
                    r19 = r2
                    r17 = r3
                    r2 = r18
                    goto L_0x19f7
                L_0x19ed:
                    r0 = move-exception
                    r8 = r1
                    r19 = r2
                    r17 = r3
                    r2 = r18
                    r9 = r30
                L_0x19f7:
                    r14 = r31
                    goto L_0x1a11
                L_0x19fa:
                    r0 = move-exception
                    r8 = r1
                    r19 = r2
                    r17 = r3
                    r2 = r18
                    r9 = r30
                    goto L_0x1a11
                L_0x1a05:
                    r0 = move-exception
                    r19 = r2
                    r17 = r3
                    r16 = r8
                    r2 = r18
                    r9 = r30
                    r8 = r1
                L_0x1a11:
                    r61 = r0
                    r0 = r12
                    r1 = r15
                    r23 = r16
                    r3 = r19
                    r24 = r26
                    r53 = r55
                    r16 = r2
                    r2 = r8
                    r15 = r11
                    r11 = r13
                    r8 = r17
                L_0x1a24:
                    r12 = r28
                    r56 = r6
                    r6 = r4
                    r4 = r5
                    r5 = r56
                    goto L_0x1a70
                L_0x1a2d:
                    r0 = move-exception
                    r19 = r8
                    r16 = r13
                    r3 = r20
                    r8 = r21
                    r52 = r30
                    r1 = 1
                    r12 = r6
                    r20 = r9
                    r7 = r15
                    r9 = r22
                    r6 = r38
                    goto L_0x1a54
                L_0x1a42:
                    r0 = move-exception
                    r19 = r8
                    r16 = r13
                    r8 = r14
                    r52 = r30
                    r1 = 1
                    r12 = r6
                    r7 = r15
                    r6 = r3
                    r3 = r20
                    r20 = r9
                    r9 = r22
                L_0x1a54:
                    r1 = r61
                    r61 = r0
                    r53 = r3
                    r23 = r6
                    r14 = r9
                    r0 = r10
                L_0x1a5e:
                    r15 = r16
                    r10 = r27
                    r9 = r28
                    r11 = r29
                    r3 = r37
                    r6 = r5
                    r16 = r8
                    r5 = r20
                    r8 = r4
                    r4 = r19
                L_0x1a70:
                    com.misfit.frameworks.buttonservice.log.FLogger r17 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    r26 = r12
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r17.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r13 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r13 = r13.b()
                    r28 = r14
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder
                    r14.<init>()
                    r29 = r10
                    java.lang.String r10 = "Error while processing sync data - e="
                    r14.append(r10)
                    r61.printStackTrace()
                    com.fossil.i97 r10 = com.fossil.i97.a
                    r14.append(r10)
                    java.lang.String r10 = r14.toString()
                    r12.e(r13, r10)
                    r61.printStackTrace()
                    com.portfolio.platform.service.MFDeviceService$d r10 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r10 = r10.a
                    com.portfolio.platform.PortfolioApp r10 = r10.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r12 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    java.lang.StringBuilder r13 = new java.lang.StringBuilder
                    r13.<init>()
                    java.lang.String r14 = "Sync data result OK but has exception error e="
                    r13.append(r14)
                    r30 = r9
                    r9 = r61
                    r13.append(r9)
                    java.lang.String r13 = r13.toString()
                    r10.a(r12, r6, r13)
                    com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r17 = r10.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r18 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r19 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r10 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r21 = r10.b()
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder
                    r10.<init>()
                    r10.append(r14)
                    r10.append(r9)
                    java.lang.String r22 = r10.toString()
                    r20 = r6
                    r17.i(r18, r19, r20, r21, r22)
                    com.portfolio.platform.service.MFDeviceService$d r10 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r10 = r10.a
                    com.portfolio.platform.PortfolioApp r10 = r10.c()
                    r7.L$0 = r5
                    r7.I$0 = r2
                    r7.L$1 = r4
                    r7.L$2 = r6
                    r7.I$1 = r15
                    r7.I$2 = r0
                    r7.L$3 = r8
                    r7.L$4 = r3
                    r7.L$5 = r1
                    r7.I$3 = r11
                    r13 = r30
                    r7.I$4 = r13
                    r13 = r29
                    r7.I$5 = r13
                    r14 = r28
                    r7.L$6 = r14
                    r12 = r26
                    r7.J$0 = r12
                    r3 = r24
                    r7.J$1 = r3
                    r6 = r23
                    r7.L$7 = r6
                    r8 = r16
                    r7.L$8 = r8
                    r7.L$9 = r9
                    r1 = 10
                    r7.label = r1
                    r1 = 1
                    java.lang.Object r0 = r10.a(r1, r7)
                    r3 = r53
                    if (r0 != r3) goto L_0x1e42
                    return r3
                L_0x1b2c:
                    r19 = r8
                    r16 = r13
                    r8 = r14
                    r14 = r23
                    r52 = r30
                    r12 = r6
                    r7 = r15
                    r6 = r3
                    r3 = r20
                    r20 = r9
                    r9 = r22
                    r0 = 0
                    int r11 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1))
                    if (r11 >= 0) goto L_0x1bb2
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    r11.append(r14)
                    com.portfolio.platform.service.MFDeviceService$a r15 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r15 = r15.b()
                    r11.append(r15)
                    java.lang.String r15 = ".processBleResult - syncTime wrong: "
                    r11.append(r15)
                    r11.append(r12)
                    java.lang.String r11 = r11.toString()
                    r0.e(r1, r11)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    java.lang.String r15 = "syncTime value wrong: "
                    r11.append(r15)
                    r11.append(r12)
                    java.lang.String r11 = r11.toString()
                    r0.a(r1, r5, r11)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r0.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r35 = r0.b()
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>()
                    r0.append(r15)
                    r0.append(r12)
                    java.lang.String r36 = r0.toString()
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)
                L_0x1bb2:
                    if (r6 != 0) goto L_0x1c03
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    r11.append(r14)
                    com.portfolio.platform.service.MFDeviceService$a r15 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r15 = r15.b()
                    r11.append(r15)
                    java.lang.String r15 = ".processBleResult - userProfile empty"
                    r11.append(r15)
                    java.lang.String r11 = r11.toString()
                    r0.e(r1, r11)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    java.lang.String r11 = "userProfile is empty."
                    r0.a(r1, r5, r11)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r0.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r35 = r0.b()
                    java.lang.String r36 = "userProfile is empty."
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)
                L_0x1c03:
                    if (r8 != 0) goto L_0x1c54
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    r11.append(r14)
                    com.portfolio.platform.service.MFDeviceService$a r14 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r14 = r14.b()
                    r11.append(r14)
                    java.lang.String r14 = ".processBleResult - user data empty"
                    r11.append(r14)
                    java.lang.String r11 = r11.toString()
                    r0.e(r1, r11)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r1 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    java.lang.String r11 = "user data is empty."
                    r0.a(r1, r5, r11)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r31 = r0.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r32 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r33 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r35 = r0.b()
                    java.lang.String r36 = "user data is empty."
                    r34 = r5
                    r31.i(r32, r33, r34, r35, r36)
                L_0x1c54:
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    r1 = r20
                    r7.L$0 = r1
                    r7.I$0 = r2
                    r1 = r19
                    r7.L$1 = r1
                    r7.L$2 = r5
                    r1 = r16
                    r7.I$1 = r1
                    r7.I$2 = r10
                    r7.L$3 = r4
                    r4 = r37
                    r7.L$4 = r4
                    r1 = r61
                    r7.L$5 = r1
                    r1 = r29
                    r7.I$3 = r1
                    r14 = r28
                    r7.I$4 = r14
                    r10 = r27
                    r7.I$5 = r10
                    r7.L$6 = r9
                    r7.J$0 = r12
                    r1 = r24
                    r7.J$1 = r1
                    r7.L$7 = r6
                    r7.L$8 = r8
                    r1 = 11
                    r7.label = r1
                    r1 = 1
                    java.lang.Object r0 = r0.a(r1, r7)
                    if (r0 != r3) goto L_0x1e42
                    return r3
                L_0x1c9c:
                    r31 = r11
                    r32 = r12
                    r7 = r15
                    r3 = r20
                    r2 = r29
                    r52 = r30
                    r4 = 1
                    boolean r11 = r13.isEmpty()
                    if (r11 == 0) goto L_0x1cdc
                    com.portfolio.platform.service.MFDeviceService$d r11 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r11 = r11.a
                    com.portfolio.platform.PortfolioApp r11 = r11.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r12 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    if (r5 == 0) goto L_0x1cd7
                    java.lang.String r15 = "Sync data result OK but extra info is EMPTY"
                    r11.a(r12, r5, r15)
                    com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r22 = r11.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r23 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r24 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r11 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r26 = r11.b()
                    java.lang.String r27 = "Sync data result OK but extra info is EMPTY"
                    r25 = r5
                    r22.i(r23, r24, r25, r26, r27)
                    goto L_0x1d20
                L_0x1cd7:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x1cdc:
                    com.portfolio.platform.service.MFDeviceService$d r11 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r11 = r11.a
                    com.portfolio.platform.PortfolioApp r11 = r11.c()
                    com.misfit.frameworks.buttonservice.communite.CommunicateMode r12 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                    if (r5 == 0) goto L_0x1d54
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder
                    r15.<init>()
                    java.lang.String r4 = "Sync data result OK but serial is "
                    r15.append(r4)
                    r15.append(r5)
                    java.lang.String r15 = r15.toString()
                    r11.a(r12, r5, r15)
                    com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.IRemoteFLogger r22 = r11.getRemote()
                    com.misfit.frameworks.buttonservice.log.FLogger$Component r23 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                    com.misfit.frameworks.buttonservice.log.FLogger$Session r24 = com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC
                    com.portfolio.platform.service.MFDeviceService$a r11 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r26 = r11.b()
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    r11.append(r4)
                    r11.append(r5)
                    java.lang.String r27 = r11.toString()
                    r25 = r5
                    r22.i(r23, r24, r25, r26, r27)
                L_0x1d20:
                    com.portfolio.platform.service.MFDeviceService$d r4 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r4 = r4.a
                    com.portfolio.platform.PortfolioApp r4 = r4.c()
                    r7.L$0 = r0
                    r11 = r28
                    r7.I$0 = r11
                    r7.L$1 = r2
                    r7.L$2 = r5
                    r7.I$1 = r9
                    r7.I$2 = r8
                    r7.L$3 = r10
                    r7.L$4 = r13
                    r12 = r31
                    r7.L$5 = r12
                    r15 = r32
                    r7.I$3 = r15
                    r7.I$4 = r14
                    r7.I$5 = r1
                    r7.L$6 = r6
                    r0 = 12
                    r7.label = r0
                    r1 = 1
                    java.lang.Object r0 = r4.a(r1, r7)
                    if (r0 != r3) goto L_0x1e42
                    return r3
                L_0x1d54:
                    com.fossil.ee7.a()
                    r0 = 0
                    throw r0
                L_0x1d59:
                    r7 = r15
                    r52 = r30
                    r9 = 0
                    r15 = r12
                    r12 = r11
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    int r1 = r0.i()
                    r2 = 1
                    int r1 = r1 + r2
                    r0.c(r1)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "sync status is successful - current sync success number = "
                    r2.append(r3)
                    com.portfolio.platform.service.MFDeviceService$d r3 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r3 = r3.a
                    com.fossil.ch5 r3 = r3.o()
                    int r3 = r3.i()
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    r0.d(r1, r2)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    long r1 = java.lang.System.currentTimeMillis()
                    r0.c(r1, r5)
                    boolean r0 = r13.isEmpty()
                    if (r0 != 0) goto L_0x1e0a
                    if (r5 == 0) goto L_0x1e0a
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r1 = r61
                    android.os.Parcelable r1 = r13.getParcelable(r1)
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r1
                    r0.a(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = r0.a()
                    if (r0 == 0) goto L_0x1ddb
                    com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r1 = r1.c()
                    int r0 = r0.getBatteryLevel()
                    r1.a(r0)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x1ddb:
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = r0.a()
                    r0.a(r1, r5, r9)
                    r0 = 13
                    if (r15 != r0) goto L_0x1e0a
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    boolean r0 = r0.i(r5)
                    if (r0 != 0) goto L_0x1e0a
                    java.lang.String r0 = "syncId"
                    long r0 = r13.getLong(r0)
                    com.portfolio.platform.service.MFDeviceService$d r2 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r2 = r2.a
                    com.fossil.ch5 r2 = r2.o()
                    r3 = 1
                    r2.a(r5, r0, r3)
                L_0x1e0a:
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r24 = 1
                    r25 = 0
                    r26 = 0
                    r27 = 0
                    r28 = 24
                    r29 = 0
                    r22 = r0
                    r23 = r5
                    com.portfolio.platform.service.MFDeviceService.a(r22, r23, r24, r25, r26, r27, r28, r29)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r1 = 1
                    r0.a(r12, r15, r1)
                    if (r6 == 0) goto L_0x1e32
                    r4 = r33
                    r6.a(r4)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x1e32:
                    com.fossil.qh5$a r0 = com.fossil.qh5.c
                    com.portfolio.platform.service.MFDeviceService$d r1 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    r0.a(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r0.z()
                L_0x1e42:
                    com.fossil.qd5$a r0 = com.fossil.qd5.f
                    r1 = r52
                    r0.e(r1)
                    com.fossil.qh5$a r0 = com.fossil.qh5.c
                    com.portfolio.platform.service.MFDeviceService$d r1 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    r0.a(r1)
                    goto L_0x1ff6
                L_0x1e54:
                    r7 = r15
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    long r1 = java.lang.System.currentTimeMillis()
                    r0.b(r1)
                    goto L_0x1ff6
                L_0x1e66:
                    r14 = r61
                    r2 = r4
                    r7 = r15
                    r4 = r31
                    r15 = r12
                    r12 = r11
                    r11 = r3
                    r3 = r20
                    com.portfolio.platform.service.MFDeviceService$d r6 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    com.fossil.ch5 r6 = r6.o()
                    java.lang.Long r6 = r6.c()
                    if (r9 != 0) goto L_0x1ed1
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.String r2 = "SENDING_ENCRYPTED_DATA_SESSION - succeded"
                    r0.e(r1, r2)
                    com.fossil.qe5 r0 = com.fossil.qe5.c
                    r0.b()
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r1 = 0
                    r0.b(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    java.lang.Boolean r1 = com.fossil.pb7.a(r1)
                    r0.a(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r1 = 0
                    java.lang.Long r3 = com.fossil.pb7.a(r1)
                    r0.a(r3)
                    long r3 = r6.longValue()
                    int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
                    if (r0 <= 0) goto L_0x1ff6
                    com.fossil.fu4 r0 = com.fossil.fu4.a
                    r1 = 1
                    r0.a(r1)
                    goto L_0x1ff6
                L_0x1ed1:
                    com.misfit.frameworks.buttonservice.log.FLogger r16 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    r53 = r3
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r16.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r16 = com.portfolio.platform.service.MFDeviceService.Z
                    r31 = r4
                    java.lang.String r4 = r16.b()
                    r28 = r1
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    r61 = r14
                    java.lang.String r14 = "SENDING_ENCRYPTED_DATA_SESSION - failed - endTimeOfUnsetChallenge: "
                    r1.append(r14)
                    r1.append(r6)
                    java.lang.String r14 = " - exactTime: "
                    r1.append(r14)
                    com.fossil.vt4 r14 = com.fossil.vt4.a
                    r32 = r15
                    long r14 = r14.b()
                    r1.append(r14)
                    java.lang.String r1 = r1.toString()
                    r3.e(r4, r1)
                    long r3 = r6.longValue()
                    com.fossil.vt4 r1 = com.fossil.vt4.a
                    long r14 = r1.b()
                    int r1 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
                    if (r1 <= 0) goto L_0x1fb4
                    com.portfolio.platform.service.MFDeviceService$d r1 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.fossil.ch5 r1 = r1.o()
                    r3 = 1
                    java.lang.Boolean r4 = com.fossil.pb7.a(r3)
                    r1.a(r4)
                    com.portfolio.platform.service.MFDeviceService$d r1 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r1 = r1.a
                    com.fossil.ch5 r1 = r1.o()
                    int r1 = r1.b()
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r4 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r4 = r4.b()
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder
                    r14.<init>()
                    java.lang.String r15 = "retry after failed - time: "
                    r14.append(r15)
                    r14.append(r1)
                    java.lang.String r14 = r14.toString()
                    r3.e(r4, r14)
                    r3 = 1
                    if (r1 >= r3) goto L_0x1fa8
                    com.portfolio.platform.service.MFDeviceService$d r3 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r3 = r3.a
                    com.fossil.ch5 r3 = r3.o()
                    int r4 = r1 + 1
                    r3.b(r4)
                    com.portfolio.platform.service.MFDeviceService$d r3 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r3 = r3.a
                    com.fossil.uj5 r3 = r3.d()
                    if (r5 == 0) goto L_0x1f6f
                    r31 = r5
                L_0x1f6f:
                    r4 = 0
                    r14 = 2
                    r15 = 0
                    r7.L$0 = r0
                    r7.I$0 = r11
                    r7.L$1 = r2
                    r7.L$2 = r5
                    r7.I$1 = r9
                    r7.I$2 = r8
                    r7.L$3 = r10
                    r7.L$4 = r13
                    r7.L$5 = r12
                    r0 = r32
                    r7.I$3 = r0
                    r0 = r61
                    r7.I$4 = r0
                    r0 = r28
                    r7.I$5 = r0
                    r7.L$6 = r6
                    r7.I$6 = r1
                    r1 = 1
                    r7.label = r1
                    r1 = r3
                    r2 = r31
                    r8 = r53
                    r3 = r4
                    r4 = r60
                    r5 = r14
                    r6 = r15
                    java.lang.Object r0 = com.fossil.uj5.a(r1, r2, r3, r4, r5, r6)
                    if (r0 != r8) goto L_0x1ff6
                    return r8
                L_0x1fa8:
                    com.fossil.qe5 r0 = com.fossil.qe5.c
                    r0.d()
                    com.fossil.fu4 r0 = com.fossil.fu4.a
                    r1 = 0
                    r0.a(r1)
                    goto L_0x1ff6
                L_0x1fb4:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r1 = r1.b()
                    java.lang.String r2 = "SENDING_ENCRYPTED_DATA_SESSION - failed - clear all"
                    r0.e(r1, r2)
                    com.fossil.qe5 r0 = com.fossil.qe5.c
                    r0.b()
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r1 = 0
                    java.lang.Long r1 = com.fossil.pb7.a(r1)
                    r0.a(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    r1 = 0
                    r0.b(r1)
                    com.portfolio.platform.service.MFDeviceService$d r0 = r7.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    com.fossil.ch5 r0 = r0.o()
                    java.lang.Boolean r1 = com.fossil.pb7.a(r1)
                    r0.a(r1)
                L_0x1ff6:
                    com.fossil.i97 r0 = com.fossil.i97.a
                    return r0
                    switch-data {0->0x050c, 1->0x04eb, 2->0x0491, 3->0x0428, 4->0x0376, 5->0x02da, 6->0x024d, 7->0x01c3, 8->0x014c, 9->0x00fb, 10->0x00ce, 11->0x00d2, 12->0x00da, 13->0x00a9, 14->0x0068, 15->0x0046, 16->0x0020, }
                    switch-data {1->0x1e66, 2->0x1e54, 3->0x0c07, 4->0x0b8a, 5->0x097b, 6->0x0964, 7->0x0942, 8->0x091f, 9->0x06e5, 10->0x06d1, 11->0x067b, 12->0x0603, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {673}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$e$a$a")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$e$a$a  reason: collision with other inner class name */
            public static final class C0304a implements nj4.b {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public C0304a(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                @Override // com.fossil.nj4.b
                public void a(Location location, int i) {
                    MFDeviceService mFDeviceService = this.a.this$0.a;
                    String str = this.b;
                    ee7.a((Object) str, "serial");
                    mFDeviceService.a(str, location, i);
                    if (location != null) {
                        nj4.a(this.a.this$0.a.c()).b(this);
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$intent, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01b2  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                    r13 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r13.label
                    java.lang.String r2 = "serial"
                    r3 = 1
                    if (r1 == 0) goto L_0x0028
                    if (r1 != r3) goto L_0x0020
                    java.lang.Object r0 = r13.L$2
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r0
                    int r0 = r13.I$0
                    java.lang.Object r1 = r13.L$1
                    java.lang.String r1 = (java.lang.String) r1
                    java.lang.Object r3 = r13.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r14)
                    goto L_0x011a
                L_0x0020:
                    java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r14.<init>(r0)
                    throw r14
                L_0x0028:
                    com.fossil.t87.a(r14)
                    com.fossil.yi7 r14 = r13.p$
                    android.content.Intent r1 = r13.$intent
                    java.lang.String r4 = "serial_number"
                    java.lang.String r1 = r1.getStringExtra(r4)
                    android.content.Intent r4 = r13.$intent
                    r5 = -1
                    java.lang.String r6 = "com.misfit.fw.connectionstate"
                    int r4 = r4.getIntExtra(r6, r5)
                    com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r6 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r6 = r6.b()
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    java.lang.String r8 = "---Inside .connectionStateChangeReceiver "
                    r7.append(r8)
                    r7.append(r1)
                    java.lang.String r9 = " status "
                    r7.append(r9)
                    r7.append(r4)
                    java.lang.String r7 = r7.toString()
                    r5.d(r6, r7)
                    com.misfit.frameworks.buttonservice.enums.ConnectionStateChange r5 = com.misfit.frameworks.buttonservice.enums.ConnectionStateChange.GATT_ON
                    int r5 = r5.ordinal()
                    if (r4 != r5) goto L_0x0154
                    com.fossil.be5$a r5 = com.fossil.be5.o
                    com.fossil.be5 r5 = r5.e()
                    com.fossil.ee7.a(r1, r2)
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r5 = r5.a(r1)
                    com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r7 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r7 = r7.b()
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    r9.<init>()
                    r9.append(r8)
                    r9.append(r1)
                    java.lang.String r8 = " deviceProfile="
                    r9.append(r8)
                    r9.append(r5)
                    java.lang.String r8 = r9.toString()
                    r6.d(r7, r8)
                    if (r5 == 0) goto L_0x00b0
                    com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r6 = r6.c()
                    int r7 = r5.getBatteryLevel()
                    r6.a(r7)
                L_0x00b0:
                    com.portfolio.platform.service.MFDeviceService$e r6 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    r6.a(r5, r1, r3)
                    boolean r6 = android.text.TextUtils.isEmpty(r1)
                    if (r6 != 0) goto L_0x00d4
                    com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r6 = r6.c()
                    java.lang.String r6 = r6.c()
                    boolean r6 = com.fossil.ee7.a(r1, r6)
                    if (r6 == 0) goto L_0x00d4
                    com.portfolio.platform.service.MFDeviceService$e r6 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    r6.z()
                L_0x00d4:
                    com.portfolio.platform.service.MFDeviceService$e r6 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    r6.B()
                    com.portfolio.platform.service.MFDeviceService$e r6 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    com.fossil.ch5 r6 = r6.o()
                    java.lang.Boolean r6 = r6.a()
                    java.lang.String r7 = "mSharedPreferencesManager.bcStatus()"
                    com.fossil.ee7.a(r6, r7)
                    boolean r6 = r6.booleanValue()
                    if (r6 == 0) goto L_0x011b
                    com.portfolio.platform.service.MFDeviceService$e r6 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r6 = r6.a
                    com.fossil.uj5 r7 = r6.d()
                    com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r6 = r6.c()
                    java.lang.String r8 = r6.c()
                    r9 = 0
                    r11 = 2
                    r12 = 0
                    r13.L$0 = r14
                    r13.L$1 = r1
                    r13.I$0 = r4
                    r13.L$2 = r5
                    r13.label = r3
                    r10 = r13
                    java.lang.Object r14 = com.fossil.uj5.a(r7, r8, r9, r10, r11, r12)
                    if (r14 != r0) goto L_0x0119
                    return r0
                L_0x0119:
                    r0 = r4
                L_0x011a:
                    r4 = r0
                L_0x011b:
                    boolean r14 = android.text.TextUtils.isEmpty(r1)
                    if (r14 != 0) goto L_0x0190
                    com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r14 = r14.c()
                    java.lang.String r14 = r14.c()
                    boolean r14 = com.fossil.ee7.a(r1, r14)
                    if (r14 == 0) goto L_0x0190
                    com.fossil.be5$a r14 = com.fossil.be5.o
                    boolean r14 = r14.f(r1)
                    if (r14 == 0) goto L_0x0190
                    com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r0 = r0.b()
                    java.lang.String r3 = "GATT_ON read current workout to resume GPS if possible"
                    r14.d(r0, r3)
                    com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r14 = r14.c()
                    r14.k(r1)
                    goto L_0x0190
                L_0x0154:
                    com.misfit.frameworks.buttonservice.enums.ConnectionStateChange r14 = com.misfit.frameworks.buttonservice.enums.ConnectionStateChange.GATT_OFF
                    int r14 = r14.ordinal()
                    if (r4 != r14) goto L_0x0190
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.fossil.ek5 r14 = r14.m()
                    r0 = 0
                    r14.a(r0)
                    boolean r14 = android.text.TextUtils.isEmpty(r1)
                    if (r14 != 0) goto L_0x0185
                    com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r14 = r14.c()
                    java.lang.String r14 = r14.c()
                    boolean r14 = com.fossil.ee7.a(r1, r14)
                    if (r14 == 0) goto L_0x0185
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    r14.A()
                L_0x0185:
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.fossil.wk5 r14 = r14.x()
                    r14.f()
                L_0x0190:
                    com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                    com.fossil.lc5 r0 = new com.fossil.lc5
                    r0.<init>(r1, r4)
                    r14.a(r0)
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.fossil.ee7.a(r1, r2)
                    r14.c(r1)
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.fossil.ch5 r14 = r14.o()
                    boolean r14 = r14.K()
                    if (r14 == 0) goto L_0x01d3
                    com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                    com.portfolio.platform.service.MFDeviceService$a r0 = com.portfolio.platform.service.MFDeviceService.Z
                    java.lang.String r0 = r0.b()
                    java.lang.String r2 = "Collect location data only user enable locate feature"
                    r14.d(r0, r2)
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.fossil.nj4 r14 = com.fossil.nj4.a(r14)
                    com.portfolio.platform.service.MFDeviceService$e$a$a r0 = new com.portfolio.platform.service.MFDeviceService$e$a$a
                    r0.<init>(r13, r1)
                    r14.a(r0)
                L_0x01d3:
                    com.fossil.qh5$a r14 = com.fossil.qh5.c
                    com.portfolio.platform.service.MFDeviceService$e r0 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r0 = r0.a
                    r14.a(r0)
                    com.portfolio.platform.service.MFDeviceService$e r14 = r13.this$0
                    com.portfolio.platform.service.MFDeviceService r14 = r14.a
                    com.portfolio.platform.PortfolioApp r14 = r14.c()
                    com.fossil.qe r14 = com.fossil.qe.a(r14)
                    android.content.Intent r0 = r13.$intent
                    r14.a(r0)
                    com.fossil.i97 r14 = com.fossil.i97.a
                    return r14
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(this, intent, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), "Device app event receive");
            if (intent != null) {
                PortfolioApp.g0.a(new jc5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getExtras()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1", f = "MFDeviceService.kt", l = {795, 797, 799}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $currentWPVersion;
        @DexIgnore
        public /* final */ /* synthetic */ int $majorVersion;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(MFDeviceService mFDeviceService, String str, int i, float f, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$majorVersion = i;
            this.$currentWPVersion = f;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$majorVersion, this.$currentWPVersion, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                DeviceRepository e = this.this$0.e();
                String str = this.$serial;
                int i2 = this.$majorVersion;
                this.L$0 = yi7;
                this.label = 1;
                obj = e.getLatestWatchParamFromServer(str, i2, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2 || i == 3) {
                WatchParameterResponse watchParameterResponse = (WatchParameterResponse) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) obj;
            if (watchParameterResponse2 != null) {
                ff5 v = this.this$0.v();
                String str2 = this.$serial;
                float f = this.$currentWPVersion;
                this.L$0 = yi7;
                this.L$1 = watchParameterResponse2;
                this.label = 2;
                if (v.a(str2, f, watchParameterResponse2, this) == a) {
                    return a;
                }
            } else {
                ff5 v2 = this.this$0.v();
                String str3 = this.$serial;
                float f2 = this.$currentWPVersion;
                this.L$0 = yi7;
                this.L$1 = watchParameterResponse2;
                this.label = 3;
                if (v2.a(str3, f2, this) == a) {
                    return a;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            if (intent != null) {
                int intExtra = intent.getIntExtra(Constants.DAILY_STEPS, 0);
                int intExtra2 = intent.getIntExtra(Constants.DAILY_POINTS, 0);
                Calendar instance = Calendar.getInstance();
                ee7.a((Object) instance, "Calendar.getInstance()");
                long longExtra = intent.getLongExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
                FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), ".saveSyncResult - Update heartbeat step by heartbeat receiver");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.Z.b();
                local.i(b, "Heartbeat data received, dailySteps=" + intExtra + ", dailyPoints=" + intExtra2 + ", receivedTime=" + longExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1", f = "MFDeviceService.kt", l = {DateTimeConstants.HOURS_PER_WEEK}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ IBinder $service;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(IBinder iBinder, fb7 fb7) {
                super(2, fb7);
                this.$service = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$service, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp.a aVar = PortfolioApp.g0;
                    IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
                    ee7.a((Object) asInterface, "IButtonConnectivity.Stub.asInterface(service)");
                    this.L$0 = yi7;
                    this.label = 1;
                    if (aVar.a(asInterface, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ee7.b(componentName, "name");
            ee7.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), "Connection to the BLE has been established");
            this.a.L = true;
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(iBinder, null), 3, null);
            this.a.C();
            this.a.B();
            qh5.c.a(this.a);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            ee7.b(componentName, "name");
            this.a.L = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), "Micro app cancel event receive");
            if (intent != null) {
                PortfolioApp.g0.a(new kc5(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getIntExtra(Constants.VARIANT_ID, -1), intent.getIntExtra("gesture", -1)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), "Notification event receive");
            if (intent != null) {
                PortfolioApp.g0.a(new oc5(intent.getIntExtra(ButtonService.NOTIFICATION_ID, -1), intent.getBooleanExtra(ButtonService.NOTIFICATION_RESULT, false)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
    public static final class l extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceLocation $deviceLocation;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(DeviceLocation deviceLocation, fb7 fb7) {
            super(2, fb7);
            this.$deviceLocation = deviceLocation;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            l lVar = new l(this.$deviceLocation, fb7);
            lVar.p$ = (yi7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((l) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ah5.p.a().f().saveDeviceLocation(this.$deviceLocation);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        public m(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public final void run() {
            if (mh7.b("release", "debug", true) && ee7.a((Object) this.a.c().h(), (Object) eb5.PORTFOLIO.getName())) {
                this.a.c().J();
                this.a.z();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            intent.setAction("SCAN_DEVICE_FOUND");
            qe.a(context).a(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.Z.b(), "Streaming event receive");
            if (intent != null) {
                int intExtra = intent.getIntExtra("gesture", -1);
                PortfolioApp.g0.a(new pc5(intent.getStringExtra(Constants.SERIAL_NUMBER), intExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$1", f = "MFDeviceService.kt", l = {991, 1000}, m = "invokeSuspend")
    public static final class p extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $deviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(MFDeviceService mFDeviceService, String str, boolean z, MisfitDeviceProfile misfitDeviceProfile, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$isNeedUpdateRemote = z;
            this.$deviceProfile = misfitDeviceProfile;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            p pVar = new p(this.this$0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, fb7);
            pVar.p$ = (yi7) obj;
            return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((p) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0225 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0230  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x0049
                if (r1 == r4) goto L_0x0030
                if (r1 != r3) goto L_0x0028
                java.lang.Object r0 = r12.L$4
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r0 = (com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj) r0
                java.lang.Object r0 = r12.L$3
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r0
                java.lang.Object r0 = r12.L$2
                com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                java.lang.Object r0 = r12.L$1
                com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r13)
                goto L_0x0226
            L_0x0028:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0030:
                java.lang.Object r1 = r12.L$4
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r1 = (com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj) r1
                java.lang.Object r4 = r12.L$3
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r4 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r4
                java.lang.Object r5 = r12.L$2
                com.portfolio.platform.data.model.Device r5 = (com.portfolio.platform.data.model.Device) r5
                java.lang.Object r6 = r12.L$1
                com.portfolio.platform.data.model.Device r6 = (com.portfolio.platform.data.model.Device) r6
                java.lang.Object r7 = r12.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r13)
                goto L_0x01c7
            L_0x0049:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r7 = r12.p$
                com.portfolio.platform.service.MFDeviceService r13 = r12.this$0
                com.portfolio.platform.data.source.DeviceRepository r13 = r13.e()
                java.lang.String r1 = r12.$serial
                com.portfolio.platform.data.model.Device r5 = r13.getDeviceBySerial(r1)
                if (r5 == 0) goto L_0x023f
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                com.portfolio.platform.service.MFDeviceService$a r1 = com.portfolio.platform.service.MFDeviceService.Z
                java.lang.String r1 = r1.b()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r8 = "updateDeviceData localDevice "
                r6.append(r8)
                r6.append(r5)
                java.lang.String r8 = " forceUpdateRemote "
                r6.append(r8)
                boolean r8 = r12.$isNeedUpdateRemote
                r6.append(r8)
                java.lang.String r6 = r6.toString()
                r13.d(r1, r6)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r13 = r12.$deviceProfile
                if (r13 == 0) goto L_0x023f
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.service.MFDeviceService$a r6 = com.portfolio.platform.service.MFDeviceService.Z
                java.lang.String r6 = r6.b()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "updateDeviceData batteryLevel "
                r8.append(r9)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r12.$deviceProfile
                int r9 = r9.getBatteryLevel()
                r8.append(r9)
                java.lang.String r9 = " fwVersion "
                r8.append(r9)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r12.$deviceProfile
                java.lang.String r9 = r9.getFirmwareVersion()
                r8.append(r9)
                java.lang.String r9 = " major "
                r8.append(r9)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r12.$deviceProfile
                short r9 = r9.getMicroAppMajorVersion()
                r8.append(r9)
                java.lang.String r9 = " minor "
                r8.append(r9)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r12.$deviceProfile
                short r9 = r9.getMicroAppMinorVersion()
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                r1.d(r6, r8)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r1 = r12.$deviceProfile
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r1 = r1.getVibrationStrength()
                com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                com.portfolio.platform.service.MFDeviceService$a r8 = com.portfolio.platform.service.MFDeviceService.Z
                java.lang.String r8 = r8.b()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "updateDeviceData - isDefaultValue: "
                r9.append(r10)
                boolean r10 = r1.isDefaultValue()
                r9.append(r10)
                java.lang.String r9 = r9.toString()
                r6.d(r8, r9)
                boolean r6 = r1.isDefaultValue()
                if (r6 != 0) goto L_0x0141
                int r6 = r1.getVibrationStrengthLevel()
                int r6 = com.fossil.he5.b(r6)
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.portfolio.platform.service.MFDeviceService$a r9 = com.portfolio.platform.service.MFDeviceService.Z
                java.lang.String r9 = r9.b()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "updateDeviceData - newVibrationLvl: "
                r10.append(r11)
                r10.append(r6)
                java.lang.String r11 = " - device: "
                r10.append(r11)
                r10.append(r5)
                java.lang.String r10 = r10.toString()
                r8.d(r9, r10)
                java.lang.Integer r6 = com.fossil.pb7.a(r6)
                r5.setVibrationStrength(r6)
            L_0x0141:
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = r12.$deviceProfile
                int r6 = r6.getBatteryLevel()
                if (r6 <= 0) goto L_0x015d
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = r12.$deviceProfile
                int r6 = r6.getBatteryLevel()
                r5.setBatteryLevel(r6)
                int r6 = r5.getBatteryLevel()
                r8 = 100
                if (r6 <= r8) goto L_0x015d
                r5.setBatteryLevel(r8)
            L_0x015d:
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = r12.$deviceProfile
                java.lang.String r6 = r6.getFirmwareVersion()
                r5.setFirmwareRevision(r6)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = r12.$deviceProfile
                java.lang.String r6 = r6.getDeviceModel()
                r5.setSku(r6)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r6 = r12.$deviceProfile
                java.lang.String r6 = r6.getAddress()
                r5.setMacAddress(r6)
                int r6 = r5.getMajor()
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r8 = r12.$deviceProfile
                short r8 = r8.getMicroAppMajorVersion()
                if (r6 != r8) goto L_0x0193
                int r6 = r5.getMinor()
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r8 = r12.$deviceProfile
                short r8 = r8.getMicroAppMinorVersion()
                if (r6 == r8) goto L_0x0191
                goto L_0x0193
            L_0x0191:
                r6 = 0
                goto L_0x0194
            L_0x0193:
                r6 = 1
            L_0x0194:
                if (r6 == 0) goto L_0x01d8
                com.portfolio.platform.service.MFDeviceService r6 = r12.this$0
                com.portfolio.platform.data.source.MicroAppRepository r6 = r6.l()
                java.lang.String r8 = r12.$serial
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r12.$deviceProfile
                short r9 = r9.getMicroAppMajorVersion()
                java.lang.String r9 = java.lang.String.valueOf(r9)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r10 = r12.$deviceProfile
                short r10 = r10.getMicroAppMinorVersion()
                java.lang.String r10 = java.lang.String.valueOf(r10)
                r12.L$0 = r7
                r12.L$1 = r5
                r12.L$2 = r5
                r12.L$3 = r13
                r12.L$4 = r1
                r12.label = r4
                java.lang.Object r4 = r6.downloadMicroAppVariant(r8, r9, r10, r12)
                if (r4 != r0) goto L_0x01c5
                return r0
            L_0x01c5:
                r4 = r13
                r6 = r5
            L_0x01c7:
                com.portfolio.platform.service.MFDeviceService r13 = r12.this$0
                com.fossil.ch5 r13 = r13.o()
                java.lang.String r8 = r12.$serial
                r9 = 0
                r13.a(r8, r9, r2)
                r13 = r4
                r2 = r5
                r5 = r6
                goto L_0x01d9
            L_0x01d8:
                r2 = r5
            L_0x01d9:
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r4 = r12.$deviceProfile
                short r4 = r4.getMicroAppMajorVersion()
                r5.setMajor(r4)
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r4 = r12.$deviceProfile
                short r4 = r4.getMicroAppMinorVersion()
                r5.setMinor(r4)
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                com.portfolio.platform.service.MFDeviceService$a r6 = com.portfolio.platform.service.MFDeviceService.Z
                java.lang.String r6 = r6.b()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "update device data "
                r8.append(r9)
                r8.append(r5)
                java.lang.String r8 = r8.toString()
                r4.d(r6, r8)
                com.portfolio.platform.service.MFDeviceService r4 = r12.this$0
                com.portfolio.platform.data.source.DeviceRepository r4 = r4.e()
                boolean r6 = r12.$isNeedUpdateRemote
                r12.L$0 = r7
                r12.L$1 = r5
                r12.L$2 = r2
                r12.L$3 = r13
                r12.L$4 = r1
                r12.label = r3
                java.lang.Object r13 = r4.updateDevice(r5, r6, r12)
                if (r13 != r0) goto L_0x0226
                return r0
            L_0x0226:
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r13 = r12.$deviceProfile
                com.misfit.frameworks.buttonservice.enums.HeartRateMode r13 = r13.getHeartRateMode()
                com.misfit.frameworks.buttonservice.enums.HeartRateMode r0 = com.misfit.frameworks.buttonservice.enums.HeartRateMode.NONE
                if (r13 == r0) goto L_0x023f
                com.portfolio.platform.service.MFDeviceService r13 = r12.this$0
                com.fossil.ch5 r13 = r13.o()
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = r12.$deviceProfile
                com.misfit.frameworks.buttonservice.enums.HeartRateMode r0 = r0.getHeartRateMode()
                r13.a(r0)
            L_0x023f:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.MFDeviceService.p.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1", f = "MFDeviceService.kt", l = {906}, m = "invokeSuspend")
    public static final class q extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(MFDeviceService mFDeviceService, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mFDeviceService;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            q qVar = new q(this.this$0, fb7);
            qVar.p$ = (yi7) obj;
            return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((q) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List<Device> list;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                List<Device> allDevice = this.this$0.e().getAllDevice();
                if (!allDevice.isEmpty()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.Z.b();
                    local.d(b, "Get all device success devicesList=" + allDevice);
                    UserRepository t = this.this$0.t();
                    this.L$0 = yi7;
                    this.L$1 = allDevice;
                    this.label = 1;
                    obj = t.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                    list = allDevice;
                }
                return i97.a;
            } else if (i == 1) {
                list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) obj) != null) {
                for (Device device : list) {
                    this.this$0.c().d(device.component1(), device.component2());
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $this_run;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(MisfitDeviceProfile misfitDeviceProfile, fb7 fb7, MFDeviceService mFDeviceService, String str) {
            super(2, fb7);
            this.$this_run = misfitDeviceProfile;
            this.this$0 = mFDeviceService;
            this.$serial$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            r rVar = new r(this.$this_run, fb7, this.this$0, this.$serial$inlined);
            rVar.p$ = (yi7) obj;
            return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((r) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Integer vibrationStrength;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Device deviceBySerial = this.this$0.e().getDeviceBySerial(this.$serial$inlined);
                int b = he5.b(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = MFDeviceService.Z.b();
                local.d(b2, "newVibrationLvl: " + b + " - device: " + deviceBySerial);
                if (deviceBySerial != null && ((vibrationStrength = deviceBySerial.getVibrationStrength()) == null || vibrationStrength.intValue() != b)) {
                    deviceBySerial.setVibrationStrength(pb7.a(b));
                    DeviceRepository e = this.this$0.e();
                    this.L$0 = yi7;
                    this.L$1 = deviceBySerial;
                    this.I$0 = b;
                    this.label = 1;
                    if (e.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                Device device = (Device) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = MFDeviceService.class.getSimpleName();
        ee7.a((Object) simpleName, "MFDeviceService::class.java.simpleName");
        X = simpleName;
        StringBuilder sb = new StringBuilder();
        Package r2 = MFDeviceService.class.getPackage();
        if (r2 != null) {
            ee7.a((Object) r2, "MFDeviceService::class.java.`package`!!");
            sb.append(r2.getName());
            sb.append(".location_updated");
            Y = sb.toString();
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final void A() {
        if (this.M != null) {
            FLogger.INSTANCE.getLocal().d(X, "Stop reading realtime step timeoutHandler");
            Handler handler = this.M;
            if (handler != null) {
                handler.removeCallbacks(this.N);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void B() {
        boolean b2 = FossilNotificationListenerService.v.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = X;
        local.d(str, "triggerReconnectNotificationService() - isConnectedNotificationManager = " + b2);
        if (!b2 && px6.a.e()) {
            FossilNotificationListenerService.v.c();
        }
    }

    @DexIgnore
    public final ik7 C() {
        return xh7.b(zi7.a(qj7.b()), null, null, new q(this, null), 3, null);
    }

    @DexIgnore
    public final ActivitiesRepository b() {
        ActivitiesRepository activitiesRepository = this.d;
        if (activitiesRepository != null) {
            return activitiesRepository;
        }
        ee7.d("mActivitiesRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp c() {
        PortfolioApp portfolioApp = this.w;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        ee7.d("mApp");
        throw null;
    }

    @DexIgnore
    public final uj5 d() {
        uj5 uj5 = this.D;
        if (uj5 != null) {
            return uj5;
        }
        ee7.d("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository e() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final cw6 f() {
        cw6 cw6 = this.z;
        if (cw6 != null) {
            return cw6;
        }
        ee7.d("mEncryptUseCase");
        throw null;
    }

    @DexIgnore
    public final FitnessDataRepository g() {
        FitnessDataRepository fitnessDataRepository = this.s;
        if (fitnessDataRepository != null) {
            return fitnessDataRepository;
        }
        ee7.d("mFitnessDataRepository");
        throw null;
    }

    @DexIgnore
    public final ge5 h() {
        ge5 ge5 = this.B;
        if (ge5 != null) {
            return ge5;
        }
        ee7.d("mFitnessHelper");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository i() {
        GoalTrackingRepository goalTrackingRepository = this.t;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        ee7.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSampleRepository j() {
        HeartRateSampleRepository heartRateSampleRepository = this.p;
        if (heartRateSampleRepository != null) {
            return heartRateSampleRepository;
        }
        ee7.d("mHeartRateSampleRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryRepository k() {
        HeartRateSummaryRepository heartRateSummaryRepository = this.q;
        if (heartRateSummaryRepository != null) {
            return heartRateSummaryRepository;
        }
        ee7.d("mHeartRateSummaryRepository");
        throw null;
    }

    @DexIgnore
    public final MicroAppRepository l() {
        MicroAppRepository microAppRepository = this.j;
        if (microAppRepository != null) {
            return microAppRepository;
        }
        ee7.d("mMicroAppRepository");
        throw null;
    }

    @DexIgnore
    public final ek5 m() {
        ek5 ek5 = this.F;
        if (ek5 != null) {
            return ek5;
        }
        ee7.d("mMusicControlComponent");
        throw null;
    }

    @DexIgnore
    public final HybridPresetRepository n() {
        HybridPresetRepository hybridPresetRepository = this.i;
        if (hybridPresetRepository != null) {
            return hybridPresetRepository;
        }
        ee7.d("mPresetRepository");
        throw null;
    }

    @DexIgnore
    public final ch5 o() {
        ch5 ch5 = this.b;
        if (ch5 != null) {
            return ch5;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        FLogger.INSTANCE.getLocal().i(X, "on misfit service bind");
        return this.a;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.g0.c().f().a(this);
        y();
        FLogger.INSTANCE.getLocal().d(X, "Service TRacking - MFDeviceService onCreate");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = X;
        remote.i(component, session, "", str, "[MFDeviceService] onCreate " + System.currentTimeMillis());
    }

    @DexIgnore
    public void onDestroy() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = X;
        local.d(str, "Inside " + X + ".onDestroy");
        super.onDestroy();
        if (this.L) {
            rx6.a.a(this, this.K);
        }
        unregisterReceiver(this.O);
        unregisterReceiver(this.W);
        unregisterReceiver(this.R);
        unregisterReceiver(this.P);
        unregisterReceiver(this.V);
        unregisterReceiver(this.T);
        unregisterReceiver(this.S);
        unregisterReceiver(this.U);
        unregisterReceiver(this.I);
        unregisterReceiver(this.Q);
        this.H = false;
        uj5 uj5 = this.D;
        if (uj5 != null) {
            uj5.a();
        } else {
            ee7.d("mBuddyChallengeManager");
            throw null;
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        if (mh7.b(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, null)) {
            FLogger.INSTANCE.getLocal().d(X, "Service Tracking - onStartCommand() - Start to bring service to foreground");
            qh5.c.a((Context) this, (Service) this, false);
        } else {
            FLogger.INSTANCE.getLocal().d(X, "Service Tracking - onStartCommand() - Stop service");
            qh5.c.a((Context) this, (Service) this, true);
        }
        if (!this.L) {
            rx6.a.a(this, ButtonService.class, this.K, 1);
            FLogger.INSTANCE.getLocal().d(X, "Service Tracking - onStartCommand() - Bound to service");
        }
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.SERVICE;
        String str = X;
        remote.i(component, session, "", str, "[MFDeviceService] onStartCommand " + System.currentTimeMillis());
        return 1;
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(X, "onTaskRemoved()");
        stopSelf();
        sendBroadcast(new Intent(this, RestartServiceReceiver.class));
    }

    @DexIgnore
    public final SleepSessionsRepository p() {
        SleepSessionsRepository sleepSessionsRepository = this.f;
        if (sleepSessionsRepository != null) {
            return sleepSessionsRepository;
        }
        ee7.d("mSleepSessionsRepository");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository q() {
        SleepSummariesRepository sleepSummariesRepository = this.g;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        ee7.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository r() {
        SummariesRepository summariesRepository = this.e;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        ee7.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final ThirdPartyRepository s() {
        ThirdPartyRepository thirdPartyRepository = this.y;
        if (thirdPartyRepository != null) {
            return thirdPartyRepository;
        }
        ee7.d("mThirdPartyRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository t() {
        UserRepository userRepository = this.h;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final nw6 u() {
        nw6 nw6 = this.A;
        if (nw6 != null) {
            return nw6;
        }
        ee7.d("mVerifySecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final ff5 v() {
        ff5 ff5 = this.C;
        if (ff5 != null) {
            return ff5;
        }
        ee7.d("mWatchParamHelper");
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionRepository w() {
        WorkoutSessionRepository workoutSessionRepository = this.r;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        ee7.d("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final wk5 x() {
        wk5 wk5 = this.E;
        if (wk5 != null) {
            return wk5;
        }
        ee7.d("mWorkoutTetherGpsManager");
        throw null;
    }

    @DexIgnore
    public final void y() {
        if (this.H) {
            FLogger.INSTANCE.getLocal().e(X, "Return from device service register receiver");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = X;
        local.i(str, "Inside " + X + ".registerReceiver " + b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE()));
        registerReceiver(this.O, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE())));
        registerReceiver(this.W, new IntentFilter(b(ButtonService.Companion.getACTION_ANALYTIC_EVENT())));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = X;
        local2.i(str2, "Inside " + X + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT());
        registerReceiver(this.R, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT())));
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = X;
        local3.i(str3, "Inside " + X + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT());
        registerReceiver(this.S, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT())));
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = X;
        local4.i(str4, "Inside " + X + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT());
        registerReceiver(this.T, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT())));
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = X;
        local5.i(str5, "Inside " + X + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA());
        registerReceiver(this.U, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA())));
        registerReceiver(this.P, new IntentFilter(b(ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE())));
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = X;
        local6.i(str6, "Inside " + X + ".registerReceiver " + ButtonService.Companion.getACTION_NOTIFICATION_SENT());
        registerReceiver(this.Q, new IntentFilter(b(ButtonService.Companion.getACTION_NOTIFICATION_SENT())));
        registerReceiver(this.V, new IntentFilter(b(ButtonService.Companion.getACTION_SCAN_DEVICE_FOUND())));
        registerReceiver(this.I, new IntentFilter("android.intent.action.TIME_TICK"));
        this.H = true;
    }

    @DexIgnore
    public final void z() {
        A();
        FLogger.INSTANCE.getLocal().d(X, "Start reading realtime step timeoutHandler 30 mins");
        Handler handler = new Handler(getMainLooper());
        this.M = handler;
        if (handler != null) {
            handler.postDelayed(this.N, 1800000);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final String b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = X;
        local.d(str2, "Key=" + getPackageName() + str);
        return getPackageName() + str;
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "serial");
        LocationProvider f2 = ah5.p.a().f();
        DeviceLocation deviceLocation = f2.getDeviceLocation(str);
        double d2 = 0.0d;
        double latitude = deviceLocation != null ? deviceLocation.getLatitude() : 0.0d;
        if (deviceLocation != null) {
            d2 = deviceLocation.getLongitude();
        }
        f2.saveDeviceLocation(new DeviceLocation(str, latitude, d2, System.currentTimeMillis()));
    }

    @DexIgnore
    public final MisfitDeviceProfile a() {
        return this.G;
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile) {
        this.G = misfitDeviceProfile;
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new g(this, str, bundle.getInt(ButtonService.WATCH_PARAMS_MAJOR), bundle.getFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION), null), 3, null);
    }

    @DexIgnore
    public final List<FitnessDataWrapper> a(List<? extends FitnessData> list, String str, DateTime dateTime) {
        ee7.b(list, "syncData");
        ee7.b(str, "serial");
        ee7.b(dateTime, "syncTime");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = X;
            local.d(str2, "saveFitnessData=" + ((Object) t2));
            arrayList.add(new FitnessDataWrapper(t2, str, dateTime));
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.portfolio.platform.service.MFDeviceService */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void a(MFDeviceService mFDeviceService, String str, int i2, int i3, ArrayList arrayList, Integer num, int i4, Object obj) {
        if ((i4 & 8) != 0) {
            arrayList = new ArrayList(i3);
        }
        if ((i4 & 16) != 0) {
            num = 10;
        }
        mFDeviceService.a(str, i2, i3, arrayList, num);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, ArrayList<Integer> arrayList, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = X;
        local.i(str2, "Broadcast sync status=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra("LAST_ERROR_CODE", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), num);
        intent.putIntegerArrayListExtra("LIST_ERROR_CODE", arrayList);
        intent.putExtra("SERIAL", str);
        nj5 nj5 = nj5.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        nj5.a(communicateMode, new nj5.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile, String str) {
        ee7.b(str, "serial");
        if (misfitDeviceProfile != null) {
            boolean isDefaultValue = misfitDeviceProfile.getVibrationStrength().isDefaultValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = X;
            local.d(str2, "updateVibrationStrengthLevel - isDefaultValue: " + isDefaultValue);
            if (!isDefaultValue) {
                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new r(misfitDeviceProfile, null, this, str), 3, null);
            }
        }
    }

    @DexIgnore
    public final ik7 a(MisfitDeviceProfile misfitDeviceProfile, String str, boolean z2) {
        ee7.b(str, "serial");
        return xh7.b(zi7.a(qj7.a()), null, null, new p(this, str, z2, misfitDeviceProfile, null), 3, null);
    }

    @DexIgnore
    public final void a(String str, Location location, int i2) {
        ee7.b(str, "serial");
        if (i2 >= 0) {
            if (location != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = X;
                local.d(str2, "Inside " + X + ".onLocationUpdated - location=[lat:" + location.getLatitude() + ", lon:" + location.getLongitude() + ", accuracy:" + location.getAccuracy() + "]");
                if (location.getAccuracy() <= 500.0f) {
                    try {
                        DeviceLocation deviceLocation = new DeviceLocation(str, location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                        a(str, deviceLocation);
                        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new l(deviceLocation, null), 3, null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = X;
                        local2.e(str3, "Error inside " + X + ".onLocationUpdated - e=" + e2);
                    }
                }
            }
        } else if (i2 != -1) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = X;
            local3.e(str4, "Error inside " + X + ".onLocationUpdated - code=" + i2);
        }
    }

    @DexIgnore
    public final void a(String str, DeviceLocation deviceLocation) {
        Intent intent = new Intent();
        intent.putExtra("SERIAL", str);
        intent.putExtra("device_location", deviceLocation);
        intent.setAction(Y);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void a(SKUModel sKUModel, int i2, int i3) {
        if (sKUModel != null) {
            String str = "";
            if (i3 == 1) {
                qd5 qd5 = this.v;
                if (qd5 != null) {
                    String sku = sKUModel.getSku();
                    if (sku == null) {
                        sku = str;
                    }
                    String deviceName = sKUModel.getDeviceName();
                    if (deviceName != null) {
                        str = deviceName;
                    }
                    qd5.c(sku, str, i2);
                } else {
                    ee7.d("mAnalyticsHelper");
                    throw null;
                }
            } else if (i3 == 2) {
                qd5 qd52 = this.v;
                if (qd52 != null) {
                    String sku2 = sKUModel.getSku();
                    if (sku2 == null) {
                        sku2 = str;
                    }
                    String deviceName2 = sKUModel.getDeviceName();
                    if (deviceName2 != null) {
                        str = deviceName2;
                    }
                    qd52.b(sku2, str, i2);
                } else {
                    ee7.d("mAnalyticsHelper");
                    throw null;
                }
            }
            ch5 ch5 = this.b;
            if (ch5 != null) {
                ch5.c((Boolean) false);
            } else {
                ee7.d("mSharedPreferencesManager");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, Constants.EVENT);
        qd5.f.c().a(str);
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        ee7.b(str, Constants.EVENT);
        qd5.f.c().a(str, map);
    }
}
