package com.portfolio.platform;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.ad5;
import com.fossil.ae;
import com.fossil.ch5;
import com.fossil.dl7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fp4;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kd7;
import com.fossil.lm4;
import com.fossil.nb7;
import com.fossil.nh7;
import com.fossil.qd5;
import com.fossil.qj7;
import com.fossil.rb7;
import com.fossil.rd;
import com.fossil.ro4;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.to4;
import com.fossil.uj4;
import com.fossil.uj5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.x87;
import com.fossil.xg5;
import com.fossil.xh7;
import com.fossil.xo4;
import com.fossil.yi7;
import com.fossil.zb7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApplicationEventListener implements rd {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public static /* final */ a F; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutSettingRepository A;
    @DexIgnore
    public /* final */ lm4 B;
    @DexIgnore
    public /* final */ uj5 C;
    @DexIgnore
    public /* final */ ThemeRepository D;
    @DexIgnore
    public /* final */ yi7 a; // = zi7.a(qj7.b().plus(dl7.a(null, 1, null)));
    @DexIgnore
    public /* final */ PortfolioApp b;
    @DexIgnore
    public /* final */ ch5 c;
    @DexIgnore
    public /* final */ HybridPresetRepository d;
    @DexIgnore
    public /* final */ CategoryRepository e;
    @DexIgnore
    public /* final */ WatchAppRepository f;
    @DexIgnore
    public /* final */ ComplicationRepository g;
    @DexIgnore
    public /* final */ MicroAppRepository h;
    @DexIgnore
    public /* final */ DianaPresetRepository i;
    @DexIgnore
    public /* final */ RingStyleRepository j;
    @DexIgnore
    public /* final */ DeviceRepository p;
    @DexIgnore
    public /* final */ UserRepository q;
    @DexIgnore
    public /* final */ ad5 r;
    @DexIgnore
    public /* final */ AlarmsRepository s;
    @DexIgnore
    public /* final */ WatchFaceRepository t;
    @DexIgnore
    public /* final */ WatchLocalizationRepository u;
    @DexIgnore
    public /* final */ fp4 v;
    @DexIgnore
    public /* final */ xo4 w;
    @DexIgnore
    public /* final */ ro4 x;
    @DexIgnore
    public /* final */ FileRepository y;
    @DexIgnore
    public /* final */ to4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ApplicationEventListener.E;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ApplicationEventListener", f = "ApplicationEventListener.kt", l = {118, 119, 120, 121, 124, 125, 126, 129, 130, 131, 132, 133, 134, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 147, 152, 153, 158, 159, 160, 161, 162, 163, 164, 167, DateTimeConstants.HOURS_PER_WEEK, 169}, m = "downloadResources")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ApplicationEventListener applicationEventListener, fb7 fb7) {
            super(fb7);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$1", f = "ApplicationEventListener.kt", l = {82, 83}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ApplicationEventListener applicationEventListener, fb7 fb7) {
            super(2, fb7);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ThemeRepository d = this.this$0.D;
                this.L$0 = yi7;
                this.label = 1;
                if (d.initializeLocalTheme(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            PortfolioApp b = this.this$0.b;
            this.L$0 = yi7;
            this.label = 2;
            if (b.l(this) == a) {
                return a;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2", f = "ApplicationEventListener.kt", l = {91, 92, 93, 98, 109}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMigrationComplete;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ApplicationEventListener applicationEventListener, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = applicationEventListener;
            this.$isMigrationComplete = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$isMigrationComplete, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0071  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00ad A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00f8  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x00fb  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x014f  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 5
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r1 == 0) goto L_0x0054
                if (r1 == r6) goto L_0x004c
                if (r1 == r5) goto L_0x0044
                if (r1 == r4) goto L_0x003b
                if (r1 == r3) goto L_0x002c
                if (r1 != r2) goto L_0x0024
                java.lang.Object r0 = r12.L$1
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r13)
                goto L_0x0147
            L_0x0024:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x002c:
                java.lang.Object r1 = r12.L$1
                java.lang.String r1 = (java.lang.String) r1
                boolean r3 = r12.Z$0
                java.lang.Object r4 = r12.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r13)
                goto L_0x00ea
            L_0x003b:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
            L_0x0042:
                r4 = r1
                goto L_0x00ae
            L_0x0044:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x009d
            L_0x004c:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x006d
            L_0x0054:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.portfolio.platform.ApplicationEventListener r1 = r12.this$0
                com.portfolio.platform.data.source.UserRepository r1 = r1.q
                r12.L$0 = r13
                r12.label = r6
                java.lang.Object r1 = r1.getCurrentUser(r12)
                if (r1 != r0) goto L_0x006a
                return r0
            L_0x006a:
                r11 = r1
                r1 = r13
                r13 = r11
            L_0x006d:
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                if (r13 == 0) goto L_0x014f
                com.portfolio.platform.ApplicationEventListener r13 = r12.this$0
                com.fossil.lm4 r13 = r13.B
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                java.lang.String r7 = r7.c()
                com.fossil.xe5 r8 = com.fossil.xe5.b
                java.lang.String r8 = r8.a()
                java.lang.String[] r6 = new java.lang.String[r6]
                r9 = 0
                com.fossil.gm4 r10 = com.fossil.gm4.BUDDY_CHALLENGE
                java.lang.String r10 = r10.getStrType()
                r6[r9] = r10
                r12.L$0 = r1
                r12.label = r5
                java.lang.Object r13 = r13.a(r7, r8, r6, r12)
                if (r13 != r0) goto L_0x009d
                return r0
            L_0x009d:
                com.portfolio.platform.ApplicationEventListener r13 = r12.this$0
                com.fossil.lm4 r13 = r13.B
                r12.L$0 = r1
                r12.label = r4
                java.lang.Object r13 = r13.a(r12)
                if (r13 != r0) goto L_0x0042
                return r0
            L_0x00ae:
                java.lang.Boolean r13 = (java.lang.Boolean) r13
                boolean r13 = r13.booleanValue()
                com.portfolio.platform.ApplicationEventListener r1 = r12.this$0
                com.portfolio.platform.PortfolioApp r1 = r1.b
                java.lang.String r1 = r1.c()
                boolean r5 = android.text.TextUtils.isEmpty(r1)
                if (r5 != 0) goto L_0x00eb
                java.lang.Boolean r5 = com.fossil.pb7.a(r13)
                com.portfolio.platform.ApplicationEventListener r6 = r12.this$0
                com.fossil.ch5 r6 = r6.c
                java.lang.Boolean r6 = r6.a()
                boolean r5 = com.fossil.ee7.a(r5, r6)
                if (r5 == 0) goto L_0x00eb
                com.portfolio.platform.ApplicationEventListener r5 = r12.this$0
                r12.L$0 = r4
                r12.Z$0 = r13
                r12.L$1 = r1
                r12.label = r3
                java.lang.Object r3 = r5.a(r12)
                if (r3 != r0) goto L_0x00e9
                return r0
            L_0x00e9:
                r3 = r13
            L_0x00ea:
                r13 = r3
            L_0x00eb:
                r9 = r4
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                boolean r10 = r3.z()
                if (r10 != 0) goto L_0x00fb
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x00fb:
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r3 = r3.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r4 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                com.misfit.frameworks.buttonservice.log.FLogger$Session r5 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
                com.portfolio.platform.ApplicationEventListener$a r6 = com.portfolio.platform.ApplicationEventListener.F
                java.lang.String r7 = r6.a()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r8 = "[App Open] Is migrate complete "
                r6.append(r8)
                boolean r8 = r12.$isMigrationComplete
                r6.append(r8)
                java.lang.String r8 = " Is Notification Listener Enabled "
                r6.append(r8)
                com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r8 = r8.c()
                boolean r8 = r8.F()
                r6.append(r8)
                java.lang.String r8 = r6.toString()
                r6 = r1
                r3.i(r4, r5, r6, r7, r8)
                com.portfolio.platform.ApplicationEventListener r3 = r12.this$0
                r12.L$0 = r9
                r12.Z$0 = r13
                r12.L$1 = r1
                r12.Z$1 = r10
                r12.label = r2
                java.lang.Object r13 = r3.a(r1, r13, r12)
                if (r13 != r0) goto L_0x0147
                return r0
            L_0x0147:
                com.portfolio.platform.ApplicationEventListener r13 = r12.this$0
                r13.a()
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x014f:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = uj4.INSTANCE.getClass().getSimpleName();
        ee7.a((Object) simpleName, "ApplicationEventListener\u2026lass.javaClass.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    public ApplicationEventListener(PortfolioApp portfolioApp, ch5 ch5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, RingStyleRepository ringStyleRepository, DeviceRepository deviceRepository, UserRepository userRepository, ad5 ad5, AlarmsRepository alarmsRepository, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, fp4 fp4, xo4 xo4, ro4 ro4, FileRepository fileRepository, to4 to4, WorkoutSettingRepository workoutSettingRepository, lm4 lm4, uj5 uj5, ThemeRepository themeRepository) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(hybridPresetRepository, "mHybridPresetRepository");
        ee7.b(categoryRepository, "mCategoryRepository");
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(ringStyleRepository, "mRingStyleRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(alarmsRepository, "mAlarmRepository");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        ee7.b(watchLocalizationRepository, "watchLocalization");
        ee7.b(fp4, "profileRepository");
        ee7.b(xo4, "friendRepository");
        ee7.b(ro4, "challengeRepository");
        ee7.b(fileRepository, "mFileRepository");
        ee7.b(to4, "fcmRepository");
        ee7.b(workoutSettingRepository, "mWorkoutSettingRepository");
        ee7.b(lm4, "flagRepository");
        ee7.b(uj5, "buddyChallengeManager");
        ee7.b(themeRepository, "mThemeRepository");
        this.b = portfolioApp;
        this.c = ch5;
        this.d = hybridPresetRepository;
        this.e = categoryRepository;
        this.f = watchAppRepository;
        this.g = complicationRepository;
        this.h = microAppRepository;
        this.i = dianaPresetRepository;
        this.j = ringStyleRepository;
        this.p = deviceRepository;
        this.q = userRepository;
        this.r = ad5;
        this.s = alarmsRepository;
        this.t = watchFaceRepository;
        this.u = watchLocalizationRepository;
        this.v = fp4;
        this.w = xo4;
        this.x = ro4;
        this.y = fileRepository;
        this.z = to4;
        this.A = workoutSettingRepository;
        this.B = lm4;
        this.C = uj5;
        this.D = themeRepository;
    }

    @DexIgnore
    @ae(Lifecycle.a.ON_STOP)
    public final void onAppEnterBackground() {
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b.c(), E, "[App Close] User put app in background");
    }

    @DexIgnore
    @ae(Lifecycle.a.ON_START)
    public final void onAppEnterForeground() {
        Log.d(E, "ApplicationEventListener onAppEnterForeground");
        ik7 unused = xh7.b(this.a, null, null, new c(this, null), 3, null);
        boolean l = this.c.l(this.b.g());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "onAppEnterForeground isMigrationComplete " + l);
        if (l) {
            ik7 unused2 = xh7.b(this.a, null, null, new d(this, l, null), 3, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:104:0x03c4  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x03e9 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x03f1  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x040f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0422 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0435 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x044d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0460 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0473 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0486 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0499 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x04ac A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x04bf A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x04c3  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01ad  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x020c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x021e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0230 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0266 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0278 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0285  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02c0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02d3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x02e6 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0301 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0314 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0359 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0388  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r20, boolean r21, com.fossil.fb7<? super com.fossil.i97> r22) {
        /*
            r19 = this;
            r0 = r19
            r1 = r21
            r2 = r22
            boolean r3 = r2 instanceof com.portfolio.platform.ApplicationEventListener.b
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.portfolio.platform.ApplicationEventListener$b r3 = (com.portfolio.platform.ApplicationEventListener.b) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.portfolio.platform.ApplicationEventListener$b r3 = new com.portfolio.platform.ApplicationEventListener$b
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r10 = com.fossil.nb7.a()
            int r4 = r3.label
            r11 = 2
            r5 = 0
            r6 = 0
            r12 = 1
            switch(r4) {
                case 0: goto L_0x01c9;
                case 1: goto L_0x01bb;
                case 2: goto L_0x01ad;
                case 3: goto L_0x019f;
                case 4: goto L_0x0190;
                case 5: goto L_0x0181;
                case 6: goto L_0x0172;
                case 7: goto L_0x0163;
                case 8: goto L_0x0154;
                case 9: goto L_0x0145;
                case 10: goto L_0x0136;
                case 11: goto L_0x0127;
                case 12: goto L_0x0118;
                case 13: goto L_0x0106;
                case 14: goto L_0x00f3;
                case 15: goto L_0x00da;
                case 16: goto L_0x00cb;
                case 17: goto L_0x00bc;
                case 18: goto L_0x00ad;
                case 19: goto L_0x009e;
                case 20: goto L_0x008f;
                case 21: goto L_0x0080;
                case 22: goto L_0x0071;
                case 23: goto L_0x0062;
                case 24: goto L_0x0035;
                case 25: goto L_0x0053;
                case 26: goto L_0x0044;
                case 27: goto L_0x0035;
                default: goto L_0x002d;
            }
        L_0x002d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0035:
            boolean r1 = r3.Z$0
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r1 = (com.portfolio.platform.ApplicationEventListener) r1
            com.fossil.t87.a(r2)
            goto L_0x04c0
        L_0x0044:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0423
        L_0x0053:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0410
        L_0x0062:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x04ad
        L_0x0071:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x049a
        L_0x0080:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0487
        L_0x008f:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0474
        L_0x009e:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0461
        L_0x00ad:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x044e
        L_0x00bc:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x03ea
        L_0x00cb:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x03d7
        L_0x00da:
            boolean r1 = r3.Z$1
            java.lang.Object r1 = r3.L$3
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x03be
        L_0x00f3:
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            boolean r4 = r3.Z$0
            java.lang.Object r5 = r3.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x035a
        L_0x0106:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
        L_0x0113:
            r7 = r5
            r5 = r4
            r4 = r1
            goto L_0x0315
        L_0x0118:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0302
        L_0x0127:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x02e7
        L_0x0136:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x02d4
        L_0x0145:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x02c1
        L_0x0154:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x02ae
        L_0x0163:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r5 = (com.portfolio.platform.ApplicationEventListener) r5
            com.fossil.t87.a(r2)
            goto L_0x0299
        L_0x0172:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x0279
        L_0x0181:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x0267
        L_0x0190:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x0231
        L_0x019f:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x021f
        L_0x01ad:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x020d
        L_0x01bb:
            boolean r1 = r3.Z$0
            java.lang.Object r4 = r3.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r7 = r3.L$0
            com.portfolio.platform.ApplicationEventListener r7 = (com.portfolio.platform.ApplicationEventListener) r7
            com.fossil.t87.a(r2)
            goto L_0x01fc
        L_0x01c9:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.portfolio.platform.ApplicationEventListener.E
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "flag: "
            r7.append(r8)
            r7.append(r1)
            java.lang.String r7 = r7.toString()
            r2.e(r4, r7)
            com.portfolio.platform.data.source.WorkoutSettingRepository r2 = r0.A
            r3.L$0 = r0
            r4 = r20
            r3.L$1 = r4
            r3.Z$0 = r1
            r3.label = r12
            java.lang.Object r2 = r2.downloadWorkoutSettings(r3)
            if (r2 != r10) goto L_0x01fb
            return r10
        L_0x01fb:
            r7 = r0
        L_0x01fc:
            com.portfolio.platform.data.source.WatchLocalizationRepository r2 = r7.u
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r3.label = r11
            java.lang.Object r2 = r2.getWatchLocalizationFromServer(r6, r3)
            if (r2 != r10) goto L_0x020d
            return r10
        L_0x020d:
            com.portfolio.platform.data.source.DeviceRepository r2 = r7.p
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r8 = 3
            r3.label = r8
            java.lang.Object r2 = r2.downloadDeviceList(r3)
            if (r2 != r10) goto L_0x021f
            return r10
        L_0x021f:
            com.portfolio.platform.data.source.UserRepository r2 = r7.q
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r8 = 4
            r3.label = r8
            java.lang.Object r2 = r2.loadUserInfo(r3)
            if (r2 != r10) goto L_0x0231
            return r10
        L_0x0231:
            com.portfolio.platform.data.source.FileRepository r2 = r7.y
            r2.downloadPendingFile()
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r13 = r2.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r14 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r15 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r16 = r2.c()
            java.lang.String r17 = com.portfolio.platform.ApplicationEventListener.E
            java.lang.String r18 = "[App Open] Start download firmware"
            r13.i(r14, r15, r16, r17, r18)
            com.fossil.yw6$a r2 = com.fossil.yw6.h
            com.fossil.yw6 r2 = r2.a()
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r8 = 5
            r3.label = r8
            java.lang.Object r2 = r2.a(r3)
            if (r2 != r10) goto L_0x0267
            return r10
        L_0x0267:
            com.portfolio.platform.data.source.DeviceRepository r2 = r7.p
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r8 = 6
            r3.label = r8
            java.lang.Object r2 = com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r2, r6, r3, r12, r5)
            if (r2 != r10) goto L_0x0279
            return r10
        L_0x0279:
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            com.fossil.jg5 r2 = r2.p()
            if (r2 == 0) goto L_0x04c3
            r2.a(r5)
            r3.L$0 = r7
            r3.L$1 = r4
            r3.Z$0 = r1
            r5 = 7
            r3.label = r5
            java.lang.Object r2 = r2.a(r3)
            if (r2 != r10) goto L_0x0298
            return r10
        L_0x0298:
            r5 = r7
        L_0x0299:
            if (r1 == 0) goto L_0x03be
            com.fossil.fp4 r2 = r5.v
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r7 = 8
            r3.label = r7
            java.lang.Object r2 = r2.a(r3)
            if (r2 != r10) goto L_0x02ae
            return r10
        L_0x02ae:
            com.fossil.xo4 r2 = r5.w
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r7 = 9
            r3.label = r7
            java.lang.Object r2 = r2.e(r3)
            if (r2 != r10) goto L_0x02c1
            return r10
        L_0x02c1:
            com.fossil.xo4 r2 = r5.w
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r7 = 10
            r3.label = r7
            java.lang.Object r2 = r2.d(r3)
            if (r2 != r10) goto L_0x02d4
            return r10
        L_0x02d4:
            com.fossil.xo4 r2 = r5.w
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r7 = 11
            r3.label = r7
            java.lang.Object r2 = r2.c(r3)
            if (r2 != r10) goto L_0x02e7
            return r10
        L_0x02e7:
            com.fossil.ro4 r2 = r5.x
            java.lang.String r7 = "running"
            java.lang.String r8 = "waiting"
            java.lang.String[] r7 = new java.lang.String[]{r7, r8}
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r8 = 12
            r3.label = r8
            java.lang.Object r2 = r2.a(r7, r12, r3)
            if (r2 != r10) goto L_0x0302
            return r10
        L_0x0302:
            com.fossil.to4 r2 = r5.z
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r7 = 13
            r3.label = r7
            java.lang.Object r2 = r2.a(r3)
            if (r2 != r10) goto L_0x0113
            return r10
        L_0x0315:
            r1 = r2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            com.portfolio.platform.data.model.ServerError r2 = r1.a()
            if (r2 == 0) goto L_0x035a
            com.portfolio.platform.data.model.ServerError r2 = r1.a()
            java.lang.Integer r2 = r2.getCode()
            if (r2 != 0) goto L_0x0329
            goto L_0x035a
        L_0x0329:
            int r2 = r2.intValue()
            if (r2 != 0) goto L_0x035a
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r8 = com.portfolio.platform.ApplicationEventListener.E
            java.lang.String r9 = "reset device Id"
            r2.e(r8, r9)
            com.fossil.ch5 r2 = r7.c
            r2.x(r12)
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            r3.L$0 = r7
            r3.L$1 = r5
            r3.Z$0 = r4
            r3.L$2 = r1
            r8 = 14
            r3.label = r8
            java.lang.Object r2 = r2.b(r3)
            if (r2 != r10) goto L_0x035a
            return r10
        L_0x035a:
            r2 = r1
            r1 = r4
            r13 = r5
            r14 = r7
            com.portfolio.platform.PortfolioApp r4 = r14.b
            java.lang.String r5 = r4.c()
            com.portfolio.platform.PortfolioApp r4 = r14.b
            boolean r4 = r4.e()
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.ApplicationEventListener.E
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r15 = "Consider retry sync data for buddy challenge - activeStatus: "
            r9.append(r15)
            r9.append(r4)
            java.lang.String r9 = r9.toString()
            r7.d(r8, r9)
            if (r4 == 0) goto L_0x03bc
            int r7 = r5.length()
            if (r7 <= 0) goto L_0x038f
            r6 = 1
        L_0x038f:
            if (r6 == 0) goto L_0x03bc
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            boolean r6 = r6.g(r5)
            if (r6 != 0) goto L_0x03bc
            com.fossil.uj5 r6 = r14.C
            r7 = 0
            r8 = 2
            r9 = 0
            r3.L$0 = r14
            r3.L$1 = r13
            r3.Z$0 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.Z$1 = r4
            r2 = 15
            r3.label = r2
            r4 = r6
            r6 = r7
            r7 = r3
            java.lang.Object r2 = com.fossil.uj5.a(r4, r5, r6, r7, r8, r9)
            if (r2 != r10) goto L_0x03bc
            return r10
        L_0x03bc:
            r4 = r13
            r5 = r14
        L_0x03be:
            boolean r2 = android.text.TextUtils.isEmpty(r4)
            if (r2 != 0) goto L_0x04c0
            com.portfolio.platform.data.source.CategoryRepository r2 = r5.e
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 16
            r3.label = r6
            java.lang.Object r2 = r2.downloadCategories(r3)
            if (r2 != r10) goto L_0x03d7
            return r10
        L_0x03d7:
            com.portfolio.platform.data.source.AlarmsRepository r2 = r5.s
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 17
            r3.label = r6
            java.lang.Object r2 = r2.downloadAlarms(r3)
            if (r2 != r10) goto L_0x03ea
            return r10
        L_0x03ea:
            com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r2 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r4)
            if (r2 != 0) goto L_0x03f1
            goto L_0x03fd
        L_0x03f1:
            int[] r6 = com.fossil.vj4.a
            int r2 = r2.ordinal()
            r2 = r6[r2]
            if (r2 == r12) goto L_0x0436
            if (r2 == r11) goto L_0x0436
        L_0x03fd:
            com.portfolio.platform.data.source.HybridPresetRepository r2 = r5.d
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 25
            r3.label = r6
            java.lang.Object r2 = r2.downloadRecommendPresetList(r4, r3)
            if (r2 != r10) goto L_0x0410
            return r10
        L_0x0410:
            com.portfolio.platform.data.source.MicroAppRepository r2 = r5.h
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 26
            r3.label = r6
            java.lang.Object r2 = r2.downloadAllMicroApp(r4, r3)
            if (r2 != r10) goto L_0x0423
            return r10
        L_0x0423:
            com.portfolio.platform.data.source.HybridPresetRepository r2 = r5.d
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r1 = 27
            r3.label = r1
            java.lang.Object r1 = r2.downloadPresetList(r4, r3)
            if (r1 != r10) goto L_0x04c0
            return r10
        L_0x0436:
            com.portfolio.platform.PortfolioApp r2 = r5.b
            r2.k(r4)
            com.portfolio.platform.PortfolioApp r2 = r5.b
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 18
            r3.label = r6
            java.lang.Object r2 = r2.c(r3)
            if (r2 != r10) goto L_0x044e
            return r10
        L_0x044e:
            com.portfolio.platform.data.RingStyleRepository r2 = r5.j
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 19
            r3.label = r6
            java.lang.Object r2 = r2.downloadRingStyleList(r4, r12, r3)
            if (r2 != r10) goto L_0x0461
            return r10
        L_0x0461:
            com.portfolio.platform.data.source.WatchAppRepository r2 = r5.f
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 20
            r3.label = r6
            java.lang.Object r2 = r2.downloadWatchApp(r4, r3)
            if (r2 != r10) goto L_0x0474
            return r10
        L_0x0474:
            com.portfolio.platform.data.source.ComplicationRepository r2 = r5.g
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 21
            r3.label = r6
            java.lang.Object r2 = r2.downloadAllComplication(r4, r3)
            if (r2 != r10) goto L_0x0487
            return r10
        L_0x0487:
            com.portfolio.platform.data.source.DianaPresetRepository r2 = r5.i
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 22
            r3.label = r6
            java.lang.Object r2 = r2.downloadPresetList(r4, r3)
            if (r2 != r10) goto L_0x049a
            return r10
        L_0x049a:
            com.portfolio.platform.data.source.DianaPresetRepository r2 = r5.i
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r6 = 23
            r3.label = r6
            java.lang.Object r2 = r2.downloadRecommendPresetList(r4, r3)
            if (r2 != r10) goto L_0x04ad
            return r10
        L_0x04ad:
            com.portfolio.platform.data.source.WatchFaceRepository r2 = r5.t
            r3.L$0 = r5
            r3.L$1 = r4
            r3.Z$0 = r1
            r1 = 24
            r3.label = r1
            java.lang.Object r1 = r2.getWatchFacesFromServer(r4, r3)
            if (r1 != r10) goto L_0x04c0
            return r10
        L_0x04c0:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x04c3:
            com.fossil.ee7.a()
            throw r5
            switch-data {0->0x01c9, 1->0x01bb, 2->0x01ad, 3->0x019f, 4->0x0190, 5->0x0181, 6->0x0172, 7->0x0163, 8->0x0154, 9->0x0145, 10->0x0136, 11->0x0127, 12->0x0118, 13->0x0106, 14->0x00f3, 15->0x00da, 16->0x00cb, 17->0x00bc, 18->0x00ad, 19->0x009e, 20->0x008f, 21->0x0080, 22->0x0071, 23->0x0062, 24->0x0035, 25->0x0053, 26->0x0044, 27->0x0035, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.a(java.lang.String, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object a(fb7<? super i97> fb7) {
        FLogger.INSTANCE.getLocal().d(E, "Inside .autoSync");
        String c2 = this.b.c();
        if (TextUtils.isEmpty(c2)) {
            FLogger.INSTANCE.getLocal().d(E, "User has no active device, skip auto sync");
            return i97.a;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "Inside .autoSync lastSyncSuccess=" + this.c.d(this.b.c()));
        long currentTimeMillis = System.currentTimeMillis() - this.c.e(this.b.c());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = E;
        remote.i(component, session, c2, str2, "[App Open] Last sync OK interval " + currentTimeMillis);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = E;
        local2.d(str3, "Inside .autoSync, last sync interval " + currentTimeMillis);
        if (currentTimeMillis >= ((long) CommuteTimeService.z) || currentTimeMillis < 0) {
            PortfolioApp portfolioApp = this.b;
            if (portfolioApp.e(portfolioApp.c()) == CommunicateMode.OTA.getValue()) {
                FLogger.INSTANCE.getLocal().d(E, "Inside .autoSync, device is ota, wait for the next time");
            } else if (this.b.G() || this.c.G()) {
                FLogger.INSTANCE.getLocal().d(E, "Inside .autoSync, start auto-sync.");
                boolean a2 = xg5.a(xg5.b, (Context) this.b, xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 56, (Object) null);
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String str4 = E;
                remote2.i(component2, session2, c2, str4, "[App Open] [Sync Start] AUTO SYNC isBlueToothEnabled " + BluetoothUtils.isBluetoothEnable() + ' ');
                if (a2) {
                    this.b.a(this.r, false, 10);
                    return i97.a;
                }
                FLogger.INSTANCE.getLocal().d(E, "autoSync fail due to lack of permission");
            } else {
                FLogger.INSTANCE.getLocal().d(E, "Inside .autoSync, doesn't start auto-sync.");
            }
        }
        return i97.a;
    }

    @DexIgnore
    public final void a() {
        Object systemService = this.b.getSystemService("accessibility");
        if (systemService != null) {
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = ((AccessibilityManager) systemService).getEnabledAccessibilityServiceList(-1);
            StringBuilder sb = new StringBuilder();
            for (AccessibilityServiceInfo accessibilityServiceInfo : enabledAccessibilityServiceList) {
                ee7.a((Object) accessibilityServiceInfo, "accessibility");
                String id = accessibilityServiceInfo.getId();
                ee7.a((Object) id, "accessibility.id");
                sb.append((String) ea7.f(nh7.a((CharSequence) id, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null)));
                sb.append(" ");
            }
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "enabledAccessibilityStringBuilder.toString()");
            if (sb2 != null) {
                String obj = nh7.d((CharSequence) sb2).toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = E;
                local.d(str, "Enabled Accessibility: " + obj);
                qd5.f.c().a("accessibility_config_on_launch", obj);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new x87("null cannot be cast to non-null type android.view.accessibility.AccessibilityManager");
    }
}
