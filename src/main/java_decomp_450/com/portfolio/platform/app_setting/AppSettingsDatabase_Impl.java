package com.portfolio.platform.app_setting;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.hm4;
import com.fossil.im4;
import com.fossil.pi;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppSettingsDatabase_Impl extends AppSettingsDatabase {
    @DexIgnore
    public volatile hm4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ei.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `flag` (`id` TEXT NOT NULL, `variantKey` TEXT, PRIMARY KEY(`id`))");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '5b7a0d80ded41376004c43da846986b1')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `flag`");
            if (((ci) AppSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AppSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AppSettingsDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) AppSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AppSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AppSettingsDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) AppSettingsDatabase_Impl.this).mDatabase = wiVar;
            AppSettingsDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) AppSettingsDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) AppSettingsDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) AppSettingsDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new ti.a("id", "TEXT", true, 1, null, 1));
            hashMap.put("variantKey", new ti.a("variantKey", "TEXT", false, 0, null, 1));
            ti tiVar = new ti("flag", hashMap, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "flag");
            if (tiVar.equals(a2)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "flag(com.portfolio.platform.app_setting.flag.entity.Flag).\n Expected:\n" + tiVar + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `flag`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "flag");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new a(5), "5b7a0d80ded41376004c43da846986b1", "6101f2dcc2699026f4b3a325e90ce486");
        xi.b.a a2 = xi.b.a(thVar.b);
        a2.a(thVar.c);
        a2.a(eiVar);
        return thVar.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.app_setting.AppSettingsDatabase
    public hm4 a() {
        hm4 hm4;
        if (this.a != null) {
            return this.a;
        }
        synchronized (this) {
            if (this.a == null) {
                this.a = new im4(this);
            }
            hm4 = this.a;
        }
        return hm4;
    }
}
