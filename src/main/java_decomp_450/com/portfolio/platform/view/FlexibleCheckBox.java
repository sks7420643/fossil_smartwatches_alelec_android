package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleCheckBox extends AppCompatCheckBox {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context) {
        super(context);
        ee7.b(context, "context");
        a(null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleCheckBox);
            String string = obtainStyledAttributes.getString(0);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.d = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 != null) {
                str = string2;
            }
            this.e = str;
            obtainStyledAttributes.recycle();
        }
        b();
    }

    @DexIgnore
    public final void b() {
        if (!TextUtils.isEmpty(this.d) && !TextUtils.isEmpty(this.e)) {
            String b = eh5.l.a().b(this.e);
            String b2 = eh5.l.a().b(this.d);
            if (!TextUtils.isEmpty(b) && !TextUtils.isEmpty(b2)) {
                setButtonTintList(new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(b2), Color.parseColor(b)}));
            }
        }
    }

    @DexIgnore
    public final void setEnableColor(String str) {
        ee7.b(str, BaseFeatureModel.COLUMN_COLOR);
        this.e = str;
        b();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
