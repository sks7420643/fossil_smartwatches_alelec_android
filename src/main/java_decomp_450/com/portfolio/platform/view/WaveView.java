package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WaveView extends View {
    @DexIgnore
    public Paint a;
    @DexIgnore
    public int b;
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String p;
    @DexIgnore
    public long q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public WaveView(Context context) {
        this(context, null, 2, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.a = new Paint(1);
        this.b = 3;
        this.c = 374.0f;
        this.d = 1670;
        this.e = 330;
        this.f = 244.0f;
        this.g = 618.0f;
        this.h = 318.0f;
        this.i = 6.0f;
        this.j = 50;
        this.p = "nonBrandSurface";
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        Context context;
        TypedArray obtainStyledAttributes;
        this.a.setStyle(Paint.Style.STROKE);
        if (attributeSet != null && (context = getContext()) != null && (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.WaveView)) != null) {
            try {
                String string = obtainStyledAttributes.getString(0);
                if (string == null) {
                    string = "primaryText";
                }
                this.p = string;
                a();
                this.b = obtainStyledAttributes.getInteger(1, 3);
                this.d = obtainStyledAttributes.getInteger(2, 1670);
                this.e = obtainStyledAttributes.getInteger(3, 330);
                obtainStyledAttributes.getInteger(4, 417);
                this.f = (float) obtainStyledAttributes.getDimensionPixelSize(6, 244);
                float dimensionPixelSize = (float) obtainStyledAttributes.getDimensionPixelSize(5, 618);
                this.g = dimensionPixelSize;
                float f2 = dimensionPixelSize - this.f;
                this.c = f2;
                this.h = this.f + ((f2 * ((float) this.e)) / ((float) this.d));
                this.i = (float) obtainStyledAttributes.getDimensionPixelSize(8, 6);
                this.j = obtainStyledAttributes.getDimensionPixelSize(7, 50);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("WaveView", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        long j2;
        float f2;
        float f3;
        int i2;
        ee7.b(canvas, "canvas");
        super.dispatchDraw(canvas);
        long currentTimeMillis = System.currentTimeMillis();
        int width = getWidth() / 2;
        int height = getHeight() / 2;
        long j3 = this.q;
        if (j3 == 0) {
            this.q = currentTimeMillis;
            j2 = 0;
        } else {
            j2 = currentTimeMillis - j3;
        }
        if (j2 == 0) {
            f2 = this.f;
        } else {
            f2 = ((((float) j2) * this.c) / ((float) this.d)) + this.f;
        }
        if (f2 - ((float) ((this.b - 1) * this.j)) > this.g) {
            f2 = this.f;
            this.q = currentTimeMillis;
        }
        int i3 = 0;
        while (i3 < this.b) {
            float f4 = this.f;
            if (f2 < f4) {
                break;
            }
            float f5 = this.h;
            if (f2 < f5) {
                float f6 = (f2 - f4) / (f5 - f4);
                i2 = (int) (((float) 255) * f6);
                f3 = f6 * this.i;
            } else {
                float f7 = (f2 - f5) / (this.g - f5);
                int i4 = 255 - ((int) (((float) 255) * f7));
                if (i4 < 0) {
                    i4 = 0;
                }
                float f8 = this.i;
                f3 = f8 - (f7 * f8);
                if (f3 < ((float) 0)) {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                i2 = i4;
            }
            this.a.setAlpha(i2);
            this.a.setStrokeWidth(f3);
            canvas.drawCircle((float) width, (float) height, f2, this.a);
            i3++;
            f2 -= (float) this.j;
        }
        postInvalidateDelayed(0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WaveView(Context context, AttributeSet attributeSet, int i2, zd7 zd7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet);
    }

    @DexIgnore
    public final void a() {
        String b2 = eh5.l.a().b(this.p);
        if (b2 != null) {
            this.a.setColor(Color.parseColor(b2));
        }
    }
}
