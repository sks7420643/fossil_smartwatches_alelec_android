package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.nz6;
import com.fossil.pl4;
import com.fossil.we7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zd5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String L; // = L;
    @DexIgnore
    public static /* final */ int M; // = 7;
    @DexIgnore
    public View A;
    @DexIgnore
    public TextView B;
    @DexIgnore
    public ConstraintLayout C;
    @DexIgnore
    public String D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public String I;
    @DexIgnore
    public int J;
    @DexIgnore
    public Calendar K;
    @DexIgnore
    public GridLayoutManager v;
    @DexIgnore
    public nz6 w;
    @DexIgnore
    public c x;
    @DexIgnore
    public d y;
    @DexIgnore
    public View z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(RecyclerViewCalendar recyclerViewCalendar) {
            this.e = recyclerViewCalendar;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int a(int i) {
            int itemViewType = this.e.getMAdapter$app_fossilRelease().getItemViewType(i);
            return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView b;

        @DexIgnore
        public f(RecyclerViewCalendar recyclerViewCalendar, RecyclerView recyclerView) {
            this.a = recyclerViewCalendar;
            this.b = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            nz6 mAdapter$app_fossilRelease = this.a.getMAdapter$app_fossilRelease();
            RecyclerView recyclerView = this.b;
            ee7.a((Object) recyclerView, "recyclerView");
            mAdapter$app_fossilRelease.b(recyclerView.getMeasuredWidth() / 7);
            RecyclerView recyclerView2 = this.b;
            ee7.a((Object) recyclerView2, "recyclerView");
            recyclerView2.setAdapter(this.a.getMAdapter$app_fossilRelease());
            RecyclerView recyclerView3 = this.b;
            ee7.a((Object) recyclerView3, "recyclerView");
            recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public RecyclerViewCalendar(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        this.D = "#FFFFFF";
        this.E = "#A7A7A7";
        this.F = "#CCCCCC";
        this.G = "#242424";
        this.H = "#FFFFFF";
        this.I = "#FFFFFF";
        this.K = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.RecyclerViewCalendar);
            try {
                eh5 a2 = eh5.l.a();
                String string = obtainStyledAttributes.getString(1);
                String str = "onDianaStepsTab";
                String b2 = a2.b(string == null ? str : string);
                if (b2 != null) {
                    this.D = b2;
                }
                eh5 a3 = eh5.l.a();
                String string2 = obtainStyledAttributes.getString(2);
                String b3 = a3.b(string2 == null ? "secondaryText" : string2);
                if (b3 != null) {
                    this.E = b3;
                }
                eh5 a4 = eh5.l.a();
                String string3 = obtainStyledAttributes.getString(3);
                String b4 = a4.b(string3 == null ? "nonBrandDisableCalendarDay" : string3);
                if (b4 != null) {
                    this.F = b4;
                }
                eh5 a5 = eh5.l.a();
                String string4 = obtainStyledAttributes.getString(5);
                String b5 = a5.b(string4 == null ? "primaryText" : string4);
                if (b5 != null) {
                    this.G = b5;
                }
                eh5 a6 = eh5.l.a();
                String string5 = obtainStyledAttributes.getString(4);
                String b6 = a6.b(string5 != null ? string5 : str);
                if (b6 != null) {
                    this.H = b6;
                }
                eh5 a7 = eh5.l.a();
                String string6 = obtainStyledAttributes.getString(0);
                String b7 = a7.b(string6 == null ? "nonBrandSurface" : string6);
                if (b7 != null) {
                    this.I = b7;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = L;
                local.e(str2, "RecyclerViewCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    public final void a(Context context) {
        View inflate = View.inflate(context, 2131558831, this);
        this.C = (ConstraintLayout) inflate.findViewById(2131362116);
        this.B = (TextView) inflate.findViewById(2131362810);
        this.z = inflate.findViewById(2131362841);
        this.A = inflate.findViewById(2131362922);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(2131362188);
        nz6 nz6 = new nz6(context);
        this.w = nz6;
        if (nz6 != null) {
            nz6.a(Color.parseColor(this.D), Color.parseColor(this.E), Color.parseColor(this.F), Color.parseColor(this.G), Color.parseColor(this.H));
            ConstraintLayout constraintLayout = this.C;
            if (constraintLayout != null) {
                constraintLayout.setBackgroundColor(Color.parseColor(this.I));
                RecyclerViewCalendar$init$Anon1 recyclerViewCalendar$init$Anon1 = new RecyclerViewCalendar$init$Anon1(context, context, M, 0, true);
                this.v = recyclerViewCalendar$init$Anon1;
                if (recyclerViewCalendar$init$Anon1 != null) {
                    recyclerViewCalendar$init$Anon1.a(new e(this));
                    ee7.a((Object) recyclerView, "recyclerView");
                    recyclerView.setLayoutManager(this.v);
                    recyclerView.setItemAnimator(null);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new f(this, recyclerView));
                    View view = this.z;
                    if (view != null) {
                        view.setOnClickListener(this);
                        View view2 = this.A;
                        if (view2 != null) {
                            view2.setOnClickListener(this);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "reachGoal");
        String b2 = eh5.l.a().b(str);
        if (b2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = L;
            local.d(str2, "invalidateStyle reachGoal=" + str + " color=" + b2);
            nz6 nz6 = this.w;
            if (nz6 != null) {
                nz6.a(Color.parseColor(this.D), Color.parseColor(this.E), Color.parseColor(this.F), Color.parseColor(this.G), Color.parseColor(this.H));
                nz6 nz62 = this.w;
                if (nz62 != null) {
                    nz62.notifyDataSetChanged();
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.d("mAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c(int i) {
        d dVar = this.y;
        if (dVar == null) {
            return;
        }
        if (i != 2131362841) {
            if (i == 2131362922) {
                if (dVar != null) {
                    dVar.a();
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else if (dVar != null) {
            dVar.next();
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        nz6 nz6 = this.w;
        if (nz6 != null) {
            Calendar c2 = nz6.c();
            nz6 nz62 = this.w;
            if (nz62 != null) {
                Calendar d2 = nz62.d();
                c cVar = this.x;
                if (cVar != null) {
                    if (cVar != null) {
                        Calendar calendar = this.K;
                        ee7.a((Object) calendar, "chosenCalendar");
                        cVar.a(calendar);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                a(d2, c2);
                return;
            }
            ee7.d("mAdapter");
            throw null;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public final nz6 getMAdapter$app_fossilRelease() {
        nz6 nz6 = this.w;
        if (nz6 != null) {
            return nz6;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        ee7.b(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == 2131362841) {
            this.J++;
        } else if (id == 2131362922) {
            this.J--;
        }
        int i = this.J;
        nz6 nz6 = this.w;
        if (nz6 != null) {
            this.K = zd5.a(i, nz6.c());
            d();
            c(view.getId());
            return;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public final void setData(Map<Long, Float> map) {
        ee7.b(map, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = L;
        local.d(str, "setData dataSize=" + map.size());
        nz6 nz6 = this.w;
        if (nz6 != null) {
            nz6.a(map, this.K);
            nz6 nz62 = this.w;
            if (nz62 != null) {
                nz62.notifyDataSetChanged();
            } else {
                ee7.d("mAdapter");
                throw null;
            }
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(Boolean bool) {
        View view = this.z;
        if (view == null) {
            ee7.a();
            throw null;
        } else if (bool != null) {
            view.setEnabled(bool.booleanValue());
            View view2 = this.A;
            if (view2 != null) {
                view2.setEnabled(bool.booleanValue());
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        ee7.b(calendar, GoalPhase.COLUMN_END_DATE);
        this.K = zd5.a(this.J, calendar);
        nz6 nz6 = this.w;
        if (nz6 != null) {
            nz6.a(calendar);
            nz6 nz62 = this.w;
            if (nz62 != null) {
                nz62.notifyDataSetChanged();
                nz6 nz63 = this.w;
                if (nz63 != null) {
                    a(nz63.d(), calendar);
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.d("mAdapter");
                throw null;
            }
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(nz6 nz6) {
        ee7.b(nz6, "<set-?>");
        this.w = nz6;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(b bVar) {
        ee7.b(bVar, "listener");
        nz6 nz6 = this.w;
        if (nz6 != null) {
            nz6.a(bVar);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(c cVar) {
        this.x = cVar;
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        ee7.b(calendar, "currentCalendar");
        ee7.b(calendar2, "startCalendar");
        ee7.b(calendar3, "endCalendar");
        nz6 nz6 = this.w;
        if (nz6 != null) {
            nz6.c(calendar2);
            nz6 nz62 = this.w;
            if (nz62 != null) {
                nz62.a(calendar3);
                nz6 nz63 = this.w;
                if (nz63 != null) {
                    nz63.b(calendar);
                    a(calendar2, calendar3);
                    nz6 nz64 = this.w;
                    if (nz64 != null) {
                        nz64.notifyDataSetChanged();
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.d("mAdapter");
                throw null;
            }
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i, int i2, zd7 zd7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.K.get(2);
            int i2 = this.K.get(1);
            View view = this.A;
            if (view != null) {
                int i3 = 8;
                view.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
                View view2 = this.z;
                if (view2 != null) {
                    if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                        i3 = 0;
                    }
                    view2.setVisibility(i3);
                    TextView textView = this.B;
                    if (textView != null) {
                        we7 we7 = we7.a;
                        Calendar calendar3 = this.K;
                        ee7.a((Object) calendar3, "chosenCalendar");
                        String format = String.format("%s %s", Arrays.copyOf(new Object[]{a(calendar3), Integer.valueOf(i2)}, 2));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886803);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a2;
            case 1:
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886802);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return a3;
            case 2:
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886806);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return a4;
            case 3:
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886799);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return a5;
            case 4:
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886807);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return a6;
            case 5:
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886805);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return a7;
            case 6:
                String a8 = ig5.a(PortfolioApp.g0.c(), 2131886804);
                ee7.a((Object) a8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return a8;
            case 7:
                String a9 = ig5.a(PortfolioApp.g0.c(), 2131886800);
                ee7.a((Object) a9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return a9;
            case 8:
                String a10 = ig5.a(PortfolioApp.g0.c(), 2131886810);
                ee7.a((Object) a10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return a10;
            case 9:
                String a11 = ig5.a(PortfolioApp.g0.c(), 2131886809);
                ee7.a((Object) a11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return a11;
            case 10:
                String a12 = ig5.a(PortfolioApp.g0.c(), 2131886808);
                ee7.a((Object) a12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return a12;
            case 11:
                String a13 = ig5.a(PortfolioApp.g0.c(), 2131886801);
                ee7.a((Object) a13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return a13;
            default:
                String a14 = ig5.a(PortfolioApp.g0.c(), 2131886803);
                ee7.a((Object) a14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a14;
        }
    }
}
