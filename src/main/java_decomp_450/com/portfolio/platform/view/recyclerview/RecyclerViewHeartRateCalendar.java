package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.e7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.oz6;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.we7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.fossil.zd5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewHeartRateCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public View A;
    @DexIgnore
    public TextView B;
    @DexIgnore
    public ConstraintLayout C;
    @DexIgnore
    public String D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public String I;
    @DexIgnore
    public String J;
    @DexIgnore
    public Calendar K;
    @DexIgnore
    public int L;
    @DexIgnore
    public GridLayoutManager v;
    @DexIgnore
    public oz6 w;
    @DexIgnore
    public c x;
    @DexIgnore
    public d y;
    @DexIgnore
    public View z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
            this.e = recyclerViewHeartRateCalendar;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.GridLayoutManager.b
        public int a(int i) {
            oz6 mAdapter$app_fossilRelease = this.e.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                int itemViewType = mAdapter$app_fossilRelease.getItemViewType(i);
                return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView b;

        @DexIgnore
        public f(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar, RecyclerView recyclerView) {
            this.a = recyclerViewHeartRateCalendar;
            this.b = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            oz6 mAdapter$app_fossilRelease = this.a.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                RecyclerView recyclerView = this.b;
                ee7.a((Object) recyclerView, "recyclerView");
                mAdapter$app_fossilRelease.b(recyclerView.getMeasuredWidth() / 7);
                RecyclerView recyclerView2 = this.b;
                ee7.a((Object) recyclerView2, "recyclerView");
                recyclerView2.setAdapter(this.a.getMAdapter$app_fossilRelease());
                RecyclerView recyclerView3 = this.b;
                ee7.a((Object) recyclerView3, "recyclerView");
                recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    /*
    static {
        new a(null);
        Color.parseColor("#FFFF00");
    }
    */

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        this.D = "";
        this.E = "";
        this.F = "";
        this.G = "";
        this.H = "";
        this.I = "";
        this.J = "";
        this.K = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.RecyclerViewHeartRateCalendar);
            try {
                String string = obtainStyledAttributes.getString(4);
                if (string == null) {
                    string = "onDianaHeartRateTab";
                }
                this.D = string;
                String string2 = obtainStyledAttributes.getString(5);
                if (string2 == null) {
                    string2 = "nonBrandDisableCalendarDay";
                }
                this.E = string2;
                String string3 = obtainStyledAttributes.getString(6);
                if (string3 == null) {
                    string3 = "primaryText";
                }
                this.F = string3;
                String string4 = obtainStyledAttributes.getString(1);
                if (string4 == null) {
                    string4 = "averageRestingHeartRate";
                }
                this.G = string4;
                String string5 = obtainStyledAttributes.getString(0);
                if (string5 == null) {
                    string5 = "aboveAverageRestingHeartRate";
                }
                this.H = string5;
                String string6 = obtainStyledAttributes.getString(3);
                if (string6 == null) {
                    string6 = "maxHeartRate";
                }
                this.I = string6;
                String string7 = obtainStyledAttributes.getString(2);
                if (string7 == null) {
                    string7 = "nonBrandSurface";
                }
                this.J = string7;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("RecyclerViewHeartRateCalendar", "RecyclerViewHeartRateCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    public final void a(Context context) {
        View inflate = View.inflate(context, 2131558831, this);
        this.C = (ConstraintLayout) inflate.findViewById(2131362116);
        this.B = (TextView) inflate.findViewById(2131362810);
        this.z = inflate.findViewById(2131362841);
        this.A = inflate.findViewById(2131362922);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(2131362188);
        this.w = new oz6(context);
        int parseColor = Color.parseColor(eh5.l.a().b(this.H));
        int parseColor2 = Color.parseColor(eh5.l.a().b(this.G));
        int parseColor3 = Color.parseColor(eh5.l.a().b(this.D));
        int parseColor4 = Color.parseColor(eh5.l.a().b(this.E));
        int c2 = e7.c(Color.parseColor(eh5.l.a().b(this.I)), 20);
        int parseColor5 = Color.parseColor(eh5.l.a().b(this.F));
        oz6 oz6 = this.w;
        if (oz6 != null) {
            oz6.a(parseColor3, parseColor4, parseColor5, parseColor2, parseColor, c2);
            this.v = new RecyclerViewHeartRateCalendar$init$Anon1(context, context, 7, 0, true);
            String b2 = eh5.l.a().b(this.J);
            if (!TextUtils.isEmpty(b2)) {
                ConstraintLayout constraintLayout = this.C;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ConstraintLayout constraintLayout2 = this.C;
                if (constraintLayout2 != null) {
                    constraintLayout2.setBackgroundColor(v6.a(context, 2131099946));
                } else {
                    ee7.a();
                    throw null;
                }
            }
            GridLayoutManager gridLayoutManager = this.v;
            if (gridLayoutManager != null) {
                gridLayoutManager.a(new e(this));
                ee7.a((Object) recyclerView, "recyclerView");
                recyclerView.setLayoutManager(this.v);
                recyclerView.setItemAnimator(null);
                recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new f(this, recyclerView));
                View view = this.z;
                if (view != null) {
                    view.setOnClickListener(this);
                    View view2 = this.A;
                    if (view2 != null) {
                        view2.setOnClickListener(this);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(int i) {
        d dVar = this.y;
        if (dVar == null) {
            return;
        }
        if (i != 2131362841) {
            if (i == 2131362922) {
                if (dVar != null) {
                    dVar.a();
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else if (dVar != null) {
            dVar.next();
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        oz6 oz6 = this.w;
        Calendar calendar = null;
        Calendar d2 = oz6 != null ? oz6.d() : null;
        oz6 oz62 = this.w;
        if (oz62 != null) {
            calendar = oz62.p();
        }
        c cVar = this.x;
        if (cVar != null) {
            Calendar calendar2 = this.K;
            ee7.a((Object) calendar2, "chosenCalendar");
            cVar.a(calendar2);
        }
        a(calendar, d2);
    }

    @DexIgnore
    public final oz6 getMAdapter$app_fossilRelease() {
        return this.w;
    }

    @DexIgnore
    public void onClick(View view) {
        ee7.b(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == 2131362841) {
            this.L++;
        } else if (id == 2131362922) {
            this.L--;
        }
        oz6 oz6 = this.w;
        if (oz6 != null) {
            this.K = zd5.a(this.L, oz6.d());
        }
        d();
        c(view.getId());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0099, code lost:
        if (r1 == 0) goto L_0x009e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setData(java.util.Map<java.lang.Long, java.lang.Integer> r8) {
        /*
            r7 = this;
            java.lang.String r0 = "data"
            com.fossil.ee7.b(r8, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "setData dataSize="
            r1.append(r2)
            int r2 = r8.size()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "RecyclerViewHeartRateCalendar"
            r0.d(r2, r1)
            java.util.Collection r0 = r8.values()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x0032:
            boolean r2 = r0.hasNext()
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x004f
            java.lang.Object r2 = r0.next()
            r5 = r2
            java.lang.Number r5 = (java.lang.Number) r5
            int r5 = r5.intValue()
            if (r5 <= 0) goto L_0x0048
            goto L_0x0049
        L_0x0048:
            r3 = 0
        L_0x0049:
            if (r3 == 0) goto L_0x0032
            r1.add(r2)
            goto L_0x0032
        L_0x004f:
            java.lang.Comparable r0 = com.fossil.ea7.i(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x005c
            int r0 = r0.intValue()
            goto L_0x005d
        L_0x005c:
            r0 = 0
        L_0x005d:
            java.util.Collection r1 = r8.values()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r1 = r1.iterator()
        L_0x006a:
            boolean r5 = r1.hasNext()
            if (r5 == 0) goto L_0x0086
            java.lang.Object r5 = r1.next()
            r6 = r5
            java.lang.Number r6 = (java.lang.Number) r6
            int r6 = r6.intValue()
            if (r6 <= 0) goto L_0x007f
            r6 = 1
            goto L_0x0080
        L_0x007f:
            r6 = 0
        L_0x0080:
            if (r6 == 0) goto L_0x006a
            r2.add(r5)
            goto L_0x006a
        L_0x0086:
            java.lang.Comparable r1 = com.fossil.ea7.h(r2)
            java.lang.Integer r1 = (java.lang.Integer) r1
            r2 = 100
            if (r1 == 0) goto L_0x0095
            int r1 = r1.intValue()
            goto L_0x0097
        L_0x0095:
            r1 = 100
        L_0x0097:
            if (r0 != r1) goto L_0x009c
            if (r1 != 0) goto L_0x009d
            goto L_0x009e
        L_0x009c:
            r4 = r0
        L_0x009d:
            r2 = r1
        L_0x009e:
            com.fossil.oz6 r0 = r7.w
            if (r0 == 0) goto L_0x00ac
            java.util.Calendar r1 = r7.K
            java.lang.String r3 = "chosenCalendar"
            com.fossil.ee7.a(r1, r3)
            r0.a(r8, r4, r2, r1)
        L_0x00ac:
            com.fossil.oz6 r8 = r7.w
            if (r8 == 0) goto L_0x00b3
            r8.notifyDataSetChanged()
        L_0x00b3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.setData(java.util.Map):void");
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(boolean z2) {
        View view = this.z;
        if (view != null) {
            view.setEnabled(z2);
        }
        View view2 = this.A;
        if (view2 != null) {
            view2.setEnabled(z2);
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        ee7.b(calendar, GoalPhase.COLUMN_END_DATE);
        this.K = zd5.a(this.L, calendar);
        oz6 oz6 = this.w;
        if (oz6 != null) {
            oz6.a(calendar);
            oz6 oz62 = this.w;
            if (oz62 != null) {
                oz62.notifyDataSetChanged();
                oz6 oz63 = this.w;
                if (oz63 != null) {
                    a(oz63.p(), calendar);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(oz6 oz6) {
        this.w = oz6;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(b bVar) {
        ee7.b(bVar, "listener");
        oz6 oz6 = this.w;
        if (oz6 != null) {
            oz6.a(bVar);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(c cVar) {
        ee7.b(cVar, "listener");
        this.x = cVar;
    }

    @DexIgnore
    public final void setTintColor(int i) {
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i, int i2, zd7 zd7) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        ee7.b(calendar, "currentCalendar");
        ee7.b(calendar2, "startCalendar");
        ee7.b(calendar3, "endCalendar");
        oz6 oz6 = this.w;
        if (oz6 != null) {
            oz6.c(calendar2);
            oz6.a(calendar3);
            oz6.b(calendar);
            oz6.notifyDataSetChanged();
            a(calendar2, calendar3);
        }
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.K.get(2);
            int i2 = this.K.get(1);
            View view = this.A;
            if (view != null) {
                int i3 = 8;
                view.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
                View view2 = this.z;
                if (view2 != null) {
                    if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                        i3 = 0;
                    }
                    view2.setVisibility(i3);
                    TextView textView = this.B;
                    if (textView != null) {
                        we7 we7 = we7.a;
                        Calendar calendar3 = this.K;
                        ee7.a((Object) calendar3, "chosenCalendar");
                        String format = String.format("%s %s", Arrays.copyOf(new Object[]{a(calendar3), Integer.valueOf(i2)}, 2));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886803);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a2;
            case 1:
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886802);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return a3;
            case 2:
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886806);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return a4;
            case 3:
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886799);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return a5;
            case 4:
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886807);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return a6;
            case 5:
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886805);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return a7;
            case 6:
                String a8 = ig5.a(PortfolioApp.g0.c(), 2131886804);
                ee7.a((Object) a8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return a8;
            case 7:
                String a9 = ig5.a(PortfolioApp.g0.c(), 2131886800);
                ee7.a((Object) a9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return a9;
            case 8:
                String a10 = ig5.a(PortfolioApp.g0.c(), 2131886810);
                ee7.a((Object) a10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return a10;
            case 9:
                String a11 = ig5.a(PortfolioApp.g0.c(), 2131886809);
                ee7.a((Object) a11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return a11;
            case 10:
                String a12 = ig5.a(PortfolioApp.g0.c(), 2131886808);
                ee7.a((Object) a12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return a12;
            case 11:
                String a13 = ig5.a(PortfolioApp.g0.c(), 2131886801);
                ee7.a((Object) a13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return a13;
            default:
                String a14 = ig5.a(PortfolioApp.g0.c(), 2131886803);
                ee7.a((Object) a14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a14;
        }
    }
}
