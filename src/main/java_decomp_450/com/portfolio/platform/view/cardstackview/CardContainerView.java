package com.portfolio.platform.view.cardstackview;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bz6;
import com.fossil.q9;
import com.fossil.xy6;
import com.fossil.yx6;
import com.fossil.zy6;
import java.util.List;

public class CardContainerView extends FrameLayout {
    public xy6 a;
    public float b;
    public float c;
    public float d;
    public float e;
    public boolean f;
    public boolean g;
    public ViewGroup h;
    public ViewGroup i;
    public View j;
    public View p;
    public View q;
    public View r;
    public c s;
    public GestureDetector.SimpleOnGestureListener t;
    public GestureDetector u;

    public class a extends GestureDetector.SimpleOnGestureListener {
        public a() {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            c cVar = CardContainerView.this.s;
            if (cVar == null) {
                return true;
            }
            cVar.a();
            return true;
        }
    }

    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /*
        static {
            /*
                com.fossil.zy6[] r0 = com.fossil.zy6.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.portfolio.platform.view.cardstackview.CardContainerView.b.a = r0
                com.fossil.zy6 r1 = com.fossil.zy6.TopLeft     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.portfolio.platform.view.cardstackview.CardContainerView.b.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.zy6 r1 = com.fossil.zy6.TopRight     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.portfolio.platform.view.cardstackview.CardContainerView.b.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.zy6 r1 = com.fossil.zy6.BottomLeft     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.portfolio.platform.view.cardstackview.CardContainerView.b.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.zy6 r1 = com.fossil.zy6.BottomRight     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.cardstackview.CardContainerView.b.<clinit>():void");
        }
        */
    }

    public interface c {
        void a();

        void a(float f, float f2);

        void a(Point point, bz6 bz6);

        void b();
    }

    public CardContainerView(Context context) {
        this(context, null);
    }

    public final void a(MotionEvent motionEvent) {
        this.d = motionEvent.getRawX();
        this.e = motionEvent.getRawY();
    }

    public final void b(MotionEvent motionEvent) {
        this.f = true;
        d(motionEvent);
        h();
        g();
        c cVar = this.s;
        if (cVar != null) {
            cVar.a(getPercentX(), getPercentY());
        }
    }

    public final void c(MotionEvent motionEvent) {
        float f2;
        if (this.f) {
            this.f = false;
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            Point c2 = yx6.c(this.d, this.e, rawX, rawY);
            zy6 a2 = yx6.a(this.d, this.e, rawX, rawY);
            double b2 = yx6.b(this.d, this.e, rawX, rawY);
            bz6 bz6 = null;
            int i2 = b.a[a2.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (Math.cos(Math.toRadians(360.0d - Math.toDegrees(b2))) < 0.5d) {
                                bz6 = bz6.Bottom;
                            } else {
                                bz6 = bz6.Right;
                            }
                        }
                    } else if (Math.cos(Math.toRadians(Math.toDegrees(b2) + 180.0d)) < -0.5d) {
                        bz6 = bz6.Left;
                    } else {
                        bz6 = bz6.Bottom;
                    }
                } else if (Math.cos(Math.toRadians(Math.toDegrees(b2))) < 0.5d) {
                    bz6 = bz6.Top;
                } else {
                    bz6 = bz6.Right;
                }
            } else if (Math.cos(Math.toRadians(180.0d - Math.toDegrees(b2))) < -0.5d) {
                bz6 = bz6.Left;
            } else {
                bz6 = bz6.Top;
            }
            if (bz6 == bz6.Left || bz6 == bz6.Right) {
                f2 = getPercentX();
            } else {
                f2 = getPercentY();
            }
            float abs = Math.abs(f2);
            xy6 xy6 = this.a;
            if (abs <= xy6.b) {
                a();
                c cVar = this.s;
                if (cVar != null) {
                    cVar.b();
                }
            } else if (xy6.l.contains(bz6)) {
                c cVar2 = this.s;
                if (cVar2 != null) {
                    cVar2.a(c2, bz6);
                }
            } else {
                a();
                c cVar3 = this.s;
                if (cVar3 != null) {
                    cVar3.b();
                }
            }
        }
        this.d = motionEvent.getRawX();
        this.e = motionEvent.getRawY();
    }

    public final void d(MotionEvent motionEvent) {
        setTranslationX((this.b + motionEvent.getRawX()) - this.d);
        setTranslationY((this.c + motionEvent.getRawY()) - this.e);
    }

    public void e() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.q;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.r;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.p;
        if (view4 != null) {
            view4.setAlpha(1.0f);
        }
    }

    public void f() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.q;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.r;
        if (view3 != null) {
            view3.setAlpha(1.0f);
        }
        View view4 = this.p;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    public final void g() {
        float percentX = getPercentX();
        float percentY = getPercentY();
        List<bz6> list = this.a.l;
        if (list == bz6.HORIZONTAL) {
            a(percentX);
        } else if (list == bz6.VERTICAL) {
            b(percentY);
        } else if (list == bz6.FREEDOM_NO_BOTTOM) {
            if (Math.abs(percentX) >= Math.abs(percentY) || percentY >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                a(percentX);
                return;
            }
            f();
            setOverlayAlpha(Math.abs(percentY));
        } else if (list == bz6.FREEDOM) {
            if (Math.abs(percentX) > Math.abs(percentY)) {
                a(percentX);
            } else {
                b(percentY);
            }
        } else if (Math.abs(percentX) > Math.abs(percentY)) {
            if (percentX < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                d();
            } else {
                e();
            }
            setOverlayAlpha(Math.abs(percentX));
        } else {
            if (percentY < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f();
            } else {
                c();
            }
            setOverlayAlpha(Math.abs(percentY));
        }
    }

    public ViewGroup getContentContainer() {
        return this.h;
    }

    public ViewGroup getOverlayContainer() {
        return this.i;
    }

    public float getPercentX() {
        float translationX = ((getTranslationX() - this.b) * 2.0f) / ((float) getWidth());
        if (translationX > 1.0f) {
            translationX = 1.0f;
        }
        if (translationX < -1.0f) {
            return -1.0f;
        }
        return translationX;
    }

    public float getPercentY() {
        float translationY = ((getTranslationY() - this.c) * 2.0f) / ((float) getHeight());
        if (translationY > 1.0f) {
            translationY = 1.0f;
        }
        if (translationY < -1.0f) {
            return -1.0f;
        }
        return translationY;
    }

    public float getViewOriginX() {
        return this.b;
    }

    public float getViewOriginY() {
        return this.c;
    }

    public final void h() {
        setRotation(getPercentX() * 20.0f);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        FrameLayout.inflate(getContext(), 2131558448, this);
        this.h = (ViewGroup) findViewById(2131361983);
        this.i = (ViewGroup) findViewById(2131361984);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.u.onTouchEvent(motionEvent);
        if (this.a.g && this.g) {
            int a2 = q9.a(motionEvent);
            if (a2 == 0) {
                a(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(true);
            } else if (a2 == 1) {
                c(motionEvent);
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            } else if (a2 == 2) {
                b(motionEvent);
            } else if (a2 == 3) {
                getParent().getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
        return true;
    }

    public void setCardStackOption(xy6 xy6) {
        this.a = xy6;
    }

    public void setContainerEventListener(c cVar) {
        this.s = cVar;
        this.b = getTranslationX();
        this.c = getTranslationY();
    }

    public void setDraggable(boolean z) {
        this.g = z;
    }

    public void setOverlayAlpha(AnimatorSet animatorSet) {
        if (animatorSet != null) {
            animatorSet.start();
        }
    }

    public CardContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public void setOverlayAlpha(float f2) {
        this.i.setAlpha(f2);
    }

    public CardContainerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.d = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = false;
        this.g = true;
        this.h = null;
        this.i = null;
        this.j = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = new a();
        this.u = new GestureDetector(getContext(), this.t);
    }

    public final void a(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            d();
        } else {
            e();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    public void d() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(1.0f);
        }
        View view2 = this.p;
        if (view2 != null) {
            view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.q;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.r;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    public final void a() {
        animate().translationX(this.b).translationY(this.c).setDuration(300).setInterpolator(new OvershootInterpolator(1.0f)).setListener(null).start();
    }

    public final void b(float f2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f();
        } else {
            c();
        }
        setOverlayAlpha(Math.abs(f2));
    }

    public void b() {
        this.h.setAlpha(1.0f);
        this.i.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    public void a(int i2, int i3, int i4, int i5) {
        View view = this.j;
        if (view != null) {
            this.i.removeView(view);
        }
        if (i2 != 0) {
            View inflate = LayoutInflater.from(getContext()).inflate(i2, this.i, false);
            this.j = inflate;
            this.i.addView(inflate);
            this.j.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.p;
        if (view2 != null) {
            this.i.removeView(view2);
        }
        if (i3 != 0) {
            View inflate2 = LayoutInflater.from(getContext()).inflate(i3, this.i, false);
            this.p = inflate2;
            this.i.addView(inflate2);
            this.p.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view3 = this.q;
        if (view3 != null) {
            this.i.removeView(view3);
        }
        if (i4 != 0) {
            View inflate3 = LayoutInflater.from(getContext()).inflate(i4, this.i, false);
            this.q = inflate3;
            this.i.addView(inflate3);
            this.q.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.r;
        if (view4 != null) {
            this.i.removeView(view4);
        }
        if (i5 != 0) {
            View inflate4 = LayoutInflater.from(getContext()).inflate(i5, this.i, false);
            this.r = inflate4;
            this.i.addView(inflate4);
            this.r.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    public void c() {
        View view = this.j;
        if (view != null) {
            view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view2 = this.q;
        if (view2 != null) {
            view2.setAlpha(1.0f);
        }
        View view3 = this.r;
        if (view3 != null) {
            view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        View view4 = this.p;
        if (view4 != null) {
            view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }
}
