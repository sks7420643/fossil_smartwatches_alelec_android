package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.az6;
import com.fossil.bz6;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.x87;
import com.fossil.xy6;
import com.fossil.yx6;
import com.fossil.yy6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.cardstackview.CardContainerView;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CardStackView extends FrameLayout {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ xy6 a;
    @DexIgnore
    public /* final */ yy6 b;
    @DexIgnore
    public BaseAdapter c;
    @DexIgnore
    public /* final */ LinkedList<CardContainerView> d;
    @DexIgnore
    public a e;
    @DexIgnore
    public /* final */ d f;
    @DexIgnore
    public /* final */ c g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(float f, float f2);

        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(bz6 bz6);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CardContainerView.c {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void a(float f, float f2) {
            this.a.a(f, f2);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void b() {
            this.a.c();
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a();
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void a(Point point, bz6 bz6) {
            ee7.b(point, "point");
            ee7.b(bz6, "direction");
            this.a.b(point, bz6);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void a() {
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a(this.a.getState$app_fossilRelease().a);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends DataSetObserver {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void onChanged() {
            if (this.a.getAdapter$app_fossilRelease() != null) {
                boolean z = false;
                if (this.a.getState$app_fossilRelease().d) {
                    this.a.getState$app_fossilRelease().d = false;
                } else {
                    int i = this.a.getState$app_fossilRelease().c;
                    BaseAdapter adapter$app_fossilRelease = this.a.getAdapter$app_fossilRelease();
                    if (adapter$app_fossilRelease != null) {
                        if (i == adapter$app_fossilRelease.getCount()) {
                            z = true;
                        }
                        z = !z;
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                this.a.a(z);
                yy6 state$app_fossilRelease = this.a.getState$app_fossilRelease();
                BaseAdapter adapter$app_fossilRelease2 = this.a.getAdapter$app_fossilRelease();
                if (adapter$app_fossilRelease2 != null) {
                    state$app_fossilRelease.c = adapter$app_fossilRelease2.getCount();
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;
        @DexIgnore
        public /* final */ /* synthetic */ Point b;
        @DexIgnore
        public /* final */ /* synthetic */ bz6 c;

        @DexIgnore
        public e(CardStackView cardStackView, Point point, bz6 bz6) {
            this.a = cardStackView;
            this.b = point;
            this.c = bz6;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ee7.b(animator, "animator");
            this.a.a(this.b, this.c);
        }
    }

    /*
    static {
        new b(null);
        String simpleName = CardStackView.class.getSimpleName();
        ee7.a((Object) simpleName, "CardStackView::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        ee7.b(context, "context");
        this.a = new xy6();
        this.b = new yy6();
        this.d = new LinkedList<>();
        this.f = new d(this);
        this.g = new c(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CardStackView);
        setVisibleCount(obtainStyledAttributes.getInt(11, this.a.a));
        setSwipeThreshold(obtainStyledAttributes.getFloat(8, this.a.b));
        setTranslationDiff(obtainStyledAttributes.getFloat(10, this.a.c));
        setScaleDiff(obtainStyledAttributes.getFloat(4, this.a.d));
        setStackFrom(az6.values()[obtainStyledAttributes.getInt(5, this.a.e.ordinal())]);
        setElevationEnabled(obtainStyledAttributes.getBoolean(1, this.a.f));
        setSwipeEnabled(obtainStyledAttributes.getBoolean(7, this.a.g));
        List<bz6> from = bz6.from(obtainStyledAttributes.getInt(6, 0));
        ee7.a((Object) from, "SwipeDirection.from(arra\u2026kView_swipeDirection, 0))");
        setSwipeDirection(from);
        setLeftOverlay(obtainStyledAttributes.getResourceId(2, 0));
        setRightOverlay(obtainStyledAttributes.getResourceId(3, 0));
        setBottomOverlay(obtainStyledAttributes.getResourceId(0, 0));
        setTopOverlay(obtainStyledAttributes.getResourceId(9, 0));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final CardContainerView getBottomView() {
        CardContainerView last = this.d.getLast();
        ee7.a((Object) last, "containers.last");
        return last;
    }

    @DexIgnore
    private final CardContainerView getTopView() {
        CardContainerView first = this.d.getFirst();
        ee7.a((Object) first, "containers.first");
        return first;
    }

    @DexIgnore
    private final void setBottomOverlay(int i) {
        this.a.j = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setElevationEnabled(boolean z) {
        this.a.f = z;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setLeftOverlay(int i) {
        this.a.h = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setRightOverlay(int i) {
        this.a.i = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setScaleDiff(float f2) {
        this.a.d = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setStackFrom(az6 az6) {
        this.a.e = az6;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.bz6> */
    /* JADX WARN: Multi-variable type inference failed */
    private final void setSwipeDirection(List<? extends bz6> list) {
        this.a.l = list;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeEnabled(boolean z) {
        this.a.g = z;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeThreshold(float f2) {
        this.a.b = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTopOverlay(int i) {
        this.a.k = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTranslationDiff(float f2) {
        this.a.c = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setVisibleCount(int i) {
        this.a.a = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        b(z);
        e();
        c();
        d();
    }

    @DexIgnore
    public final void b(boolean z) {
        if (z) {
            this.b.a();
        }
    }

    @DexIgnore
    public final void c() {
        a();
        a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void d() {
        if (this.c != null) {
            int i = this.a.a;
            int i2 = 0;
            while (i2 < i) {
                CardContainerView cardContainerView = this.d.get(i2);
                ee7.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                int i3 = this.b.a + i2;
                BaseAdapter baseAdapter = this.c;
                if (baseAdapter != null) {
                    if (i3 < baseAdapter.getCount()) {
                        ViewGroup contentContainer = cardContainerView2.getContentContainer();
                        BaseAdapter baseAdapter2 = this.c;
                        if (baseAdapter2 != null) {
                            View view = baseAdapter2.getView(i3, contentContainer.getChildAt(0), contentContainer);
                            ee7.a((Object) contentContainer, "parent");
                            if (contentContainer.getChildCount() == 0) {
                                contentContainer.addView(view);
                            }
                            cardContainerView2.setVisibility(0);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        cardContainerView2.setVisibility(8);
                    }
                    i2++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            BaseAdapter baseAdapter3 = this.c;
            if (baseAdapter3 == null) {
                ee7.a();
                throw null;
            } else if (!baseAdapter3.isEmpty()) {
                getTopView().setDraggable(true);
            }
        }
    }

    @DexIgnore
    public final void e() {
        removeAllViews();
        this.d.clear();
        int i = this.a.a;
        int i2 = 0;
        while (i2 < i) {
            View inflate = LayoutInflater.from(getContext()).inflate(2131558447, (ViewGroup) this, false);
            if (inflate != null) {
                CardContainerView cardContainerView = (CardContainerView) inflate;
                cardContainerView.setDraggable(false);
                cardContainerView.setCardStackOption(this.a);
                xy6 xy6 = this.a;
                cardContainerView.a(xy6.h, xy6.i, xy6.j, xy6.k);
                this.d.add(0, cardContainerView);
                addView(cardContainerView);
                i2++;
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.cardstackview.CardContainerView");
            }
        }
        this.d.getFirst().setContainerEventListener(this.g);
        this.b.e = true;
    }

    @DexIgnore
    public final void f() {
        int i = (this.b.a + this.a.a) - 1;
        BaseAdapter baseAdapter = this.c;
        if (baseAdapter == null) {
            return;
        }
        if (baseAdapter != null) {
            boolean z = false;
            if (i < baseAdapter.getCount()) {
                CardContainerView bottomView = getBottomView();
                bottomView.setDraggable(false);
                ViewGroup contentContainer = bottomView.getContentContainer();
                BaseAdapter baseAdapter2 = this.c;
                if (baseAdapter2 != null) {
                    View view = baseAdapter2.getView(i, contentContainer.getChildAt(0), contentContainer);
                    ee7.a((Object) contentContainer, "parent");
                    if (contentContainer.getChildCount() == 0) {
                        contentContainer.addView(view);
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                CardContainerView bottomView2 = getBottomView();
                bottomView2.setDraggable(false);
                bottomView2.setVisibility(8);
            }
            int i2 = this.b.a;
            BaseAdapter baseAdapter3 = this.c;
            if (baseAdapter3 != null) {
                if (i2 < baseAdapter3.getCount()) {
                    z = true;
                }
                if (z) {
                    getTopView().setDraggable(true);
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void g() {
        a(getTopView());
        LinkedList<CardContainerView> linkedList = this.d;
        linkedList.addLast(linkedList.removeFirst());
    }

    @DexIgnore
    public final BaseAdapter getAdapter$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final a getCardEventListener$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    public final yy6 getState$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (this.b.e && i == 0) {
            c();
        }
    }

    @DexIgnore
    public final void setAdapter(BaseAdapter baseAdapter) {
        ee7.b(baseAdapter, "adapter");
        BaseAdapter baseAdapter2 = this.c;
        if (baseAdapter2 != null) {
            try {
                baseAdapter2.unregisterDataSetObserver(this.f);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = h;
                local.d(str, "Exception when unregisterDataSetObserver e=" + e2);
            }
        }
        this.c = baseAdapter;
        if (baseAdapter != null) {
            baseAdapter.registerDataSetObserver(this.f);
            this.b.c = baseAdapter.getCount();
            a(true);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void setAdapter$app_fossilRelease(BaseAdapter baseAdapter) {
        this.c = baseAdapter;
    }

    @DexIgnore
    public final void setCardEventListener$app_fossilRelease(a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public final void b() {
        this.d.getFirst().setContainerEventListener(null);
        this.d.getFirst().setDraggable(false);
        if (this.d.size() > 1) {
            this.d.get(1).setContainerEventListener(this.g);
            this.d.get(1).setDraggable(true);
        }
    }

    @DexIgnore
    public final void a() {
        int i = this.a.a;
        for (int i2 = 0; i2 < i; i2++) {
            CardContainerView cardContainerView = this.d.get(i2);
            ee7.a((Object) cardContainerView, "containers[i]");
            CardContainerView cardContainerView2 = cardContainerView;
            cardContainerView2.b();
            cardContainerView2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setScaleX(1.0f);
            cardContainerView2.setScaleY(1.0f);
            cardContainerView2.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void b(Point point, bz6 bz6) {
        ee7.b(point, "point");
        ee7.b(bz6, "direction");
        b();
        a(point, new e(this, point, bz6));
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        a aVar = this.e;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(f2, f3);
            } else {
                ee7.a();
                throw null;
            }
        }
        xy6 xy6 = this.a;
        if (xy6.f) {
            int i = xy6.a;
            for (int i2 = 1; i2 < i; i2++) {
                CardContainerView cardContainerView = this.d.get(i2);
                ee7.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                float f4 = (float) i2;
                float f5 = this.a.d;
                float f6 = 1.0f - (f4 * f5);
                float f7 = (float) (i2 - 1);
                float abs = f6 + (((1.0f - (f5 * f7)) - f6) * Math.abs(f2));
                cardContainerView2.setScaleX(abs);
                cardContainerView2.setScaleY(abs);
                float a2 = f4 * yx6.a(this.a.c);
                if (this.a.e == az6.Top) {
                    a2 *= -1.0f;
                }
                float a3 = f7 * yx6.a(this.a.c);
                if (this.a.e == az6.Top) {
                    a3 *= -1.0f;
                }
                cardContainerView2.setTranslationY(a2 - (Math.abs(f2) * (a2 - a3)));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context) {
        this(context, null);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Point point, Animator.AnimatorListener animatorListener) {
        getTopView().animate().translationX((float) point.x).translationY(-((float) point.y)).setDuration(400).setListener(animatorListener).start();
    }

    @DexIgnore
    public final void a(CardContainerView cardContainerView) {
        CardStackView cardStackView = (CardStackView) cardContainerView.getParent();
        if (cardStackView != null) {
            cardStackView.removeView(cardContainerView);
            cardStackView.addView(cardContainerView, 0);
        }
    }

    @DexIgnore
    public final void a(Point point, bz6 bz6) {
        ee7.b(point, "point");
        ee7.b(bz6, "direction");
        g();
        this.b.b = point;
        c();
        this.b.a++;
        a aVar = this.e;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(bz6);
            } else {
                ee7.a();
                throw null;
            }
        }
        f();
        this.d.getLast().setContainerEventListener(null);
        this.d.getFirst().setContainerEventListener(this.g);
    }
}
