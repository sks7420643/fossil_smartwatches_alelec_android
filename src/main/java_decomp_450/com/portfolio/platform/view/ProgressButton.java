package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;
import com.fossil.da;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.py6;
import com.fossil.x87;
import com.fossil.zd7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProgressButton extends FlexibleButton {
    @DexIgnore
    public long A;
    @DexIgnore
    public Drawable[] B;
    @DexIgnore
    public py6 C;
    @DexIgnore
    public ViewGroup D;
    @DexIgnore
    public FrameLayout E;
    @DexIgnore
    public String F; // = "";
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public String H; // = "";
    @DexIgnore
    public String I; // = "";
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x; // = true;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context) {
        super(context);
        ee7.b(context, "context");
        a(context, null);
    }

    @DexIgnore
    public final void a() {
        CharSequence charSequence;
        py6 py6 = this.C;
        if (py6 != null) {
            if (!this.w) {
                if (py6 != null) {
                    charSequence = py6.a(isSelected());
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (py6 != null) {
                charSequence = py6.a();
            } else {
                ee7.a();
                throw null;
            }
            setText(charSequence);
        }
    }

    @DexIgnore
    public final void b() {
        py6 py6 = this.C;
        if (py6 != null) {
            setText(py6.a(isSelected()));
            Drawable[] drawableArr = this.B;
            if (drawableArr != null) {
                if (drawableArr != null) {
                    Drawable drawable = drawableArr[0];
                    if (drawableArr != null) {
                        Drawable drawable2 = drawableArr[1];
                        if (drawableArr != null) {
                            Drawable drawable3 = drawableArr[2];
                            if (drawableArr != null) {
                                setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawableArr[3]);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            if (this.v) {
                setClickable(true);
            }
            this.w = false;
            this.z = 0;
            ViewGroup viewGroup = this.D;
            if (viewGroup != null && this.x) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.E);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c() {
        py6 py6 = this.C;
        if (py6 != null) {
            setText(py6.a());
            this.B = (Drawable[]) Arrays.copyOf(getCompoundDrawables(), 4);
            setCompoundDrawables(null, null, null, null);
            if (this.v) {
                setClickable(false);
            }
            this.w = true;
            this.z = 0;
            if (this.D != null && this.x) {
                FrameLayout frameLayout = this.E;
                if (frameLayout != null) {
                    ViewParent parent = frameLayout.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.E);
                    }
                    ViewGroup viewGroup = this.D;
                    if (viewGroup != null) {
                        viewGroup.addView(this.E);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.onDraw(canvas);
        if (this.y != null && this.w) {
            canvas.save();
            int width = getWidth();
            Drawable drawable = this.y;
            if (drawable != null) {
                int minimumWidth = (width - drawable.getMinimumWidth()) / 2;
                int height = getHeight();
                Drawable drawable2 = this.y;
                if (drawable2 != null) {
                    canvas.translate((float) minimumWidth, (float) ((height - drawable2.getMinimumHeight()) / 2));
                    long drawingTime = getDrawingTime();
                    if (drawingTime - this.A > 100) {
                        this.A = drawingTime;
                        int i = this.z + 1;
                        this.z = i;
                        if (((long) i) >= 12) {
                            this.z = 0;
                        }
                    }
                    int i2 = (int) (((float) (this.z * 10000)) / ((float) 12));
                    Drawable drawable3 = this.y;
                    if (drawable3 != null) {
                        drawable3.setLevel(i2);
                        Drawable drawable4 = this.y;
                        if (drawable4 != null) {
                            drawable4.draw(canvas);
                            canvas.restore();
                            da.K(this);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void setSelected(boolean z2) {
        super.setSelected(z2);
        a();
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.ProgressButton);
        if (obtainStyledAttributes.hasValue(5)) {
            this.y = obtainStyledAttributes.getDrawable(5);
        }
        py6 py6 = new py6();
        String str = "";
        py6.a(str);
        py6.b(getText());
        py6.c(getText());
        if (obtainStyledAttributes.hasValue(7)) {
            py6.b(obtainStyledAttributes.getString(7));
        }
        if (obtainStyledAttributes.hasValue(10)) {
            py6.c(obtainStyledAttributes.getString(10));
        }
        if (obtainStyledAttributes.hasValue(6)) {
            py6.a(obtainStyledAttributes.getString(6));
        }
        if (obtainStyledAttributes.hasValue(4)) {
            this.w = obtainStyledAttributes.getBoolean(4, false);
        }
        if (obtainStyledAttributes.hasValue(0)) {
            this.v = obtainStyledAttributes.getBoolean(0, true);
        }
        if (obtainStyledAttributes.hasValue(3)) {
            this.x = obtainStyledAttributes.getBoolean(3, true);
        }
        if (obtainStyledAttributes.hasValue(8)) {
            String string = obtainStyledAttributes.getString(8);
            if (string == null) {
                string = str;
            }
            this.F = string;
        }
        if (obtainStyledAttributes.hasValue(9)) {
            String string2 = obtainStyledAttributes.getString(9);
            if (string2 == null) {
                string2 = str;
            }
            this.G = string2;
        }
        if (obtainStyledAttributes.hasValue(1)) {
            String string3 = obtainStyledAttributes.getString(1);
            if (string3 == null) {
                string3 = str;
            }
            this.H = string3;
        }
        if (obtainStyledAttributes.hasValue(2)) {
            String string4 = obtainStyledAttributes.getString(2);
            if (string4 != null) {
                str = string4;
            }
            this.I = str;
        }
        this.C = py6;
        obtainStyledAttributes.recycle();
        if (context != null) {
            try {
                Window window = ((AppCompatActivity) context).getWindow();
                ee7.a((Object) window, "(context as AppCompatActivity).window");
                this.D = (ViewGroup) window.getDecorView().findViewById(16908290);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            this.E = new FrameLayout(getContext());
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            FrameLayout frameLayout = this.E;
            if (frameLayout != null) {
                frameLayout.setBackgroundColor(0);
                FrameLayout frameLayout2 = this.E;
                if (frameLayout2 != null) {
                    frameLayout2.setClickable(true);
                    FrameLayout frameLayout3 = this.E;
                    if (frameLayout3 != null) {
                        frameLayout3.setLayoutParams(layoutParams);
                        Drawable drawable = this.y;
                        if (drawable != null) {
                            if (drawable == null) {
                                ee7.a();
                                throw null;
                            } else if (drawable != null) {
                                int intrinsicWidth = drawable.getIntrinsicWidth();
                                Drawable drawable2 = this.y;
                                if (drawable2 != null) {
                                    drawable.setBounds(0, 0, intrinsicWidth, drawable2.getIntrinsicHeight());
                                    if (this.w) {
                                        c();
                                    } else {
                                        b();
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        if (TextUtils.isEmpty(this.F)) {
                            this.F = "primaryText";
                        }
                        if (TextUtils.isEmpty(this.G)) {
                            this.G = "nonBrandTextStyle2";
                        }
                        a(this.F, this.G, this.H, this.I);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        throw new x87("null cannot be cast to non-null type androidx.appcompat.app.AppCompatActivity");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(context, attributeSet);
    }
}
