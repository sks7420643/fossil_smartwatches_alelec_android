package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.pl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AutoResizeTextView extends FlexibleTextView {
    @DexIgnore
    public float A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public a u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(TextView textView, float f, float f2);
    }

    @DexIgnore
    public AutoResizeTextView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public final void a(int i, int i2) {
        CharSequence text = getText();
        if (text != null && text.length() != 0 && i2 > 0 && i > 0 && this.w != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (getTransformationMethod() != null) {
                text = getTransformationMethod().getTransformation(text, this);
            }
            TextPaint paint = getPaint();
            float textSize = paint.getTextSize();
            float f = this.x;
            float min = f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? Math.min(this.w, f) : this.w;
            int a2 = a(text, paint, i, min);
            float f2 = min;
            while (a2 > i2) {
                float f3 = this.y;
                if (f2 <= f3) {
                    break;
                }
                f2 = Math.max(f2 - 2.0f, f3);
                a2 = a(text, paint, i, f2);
            }
            if (this.B && f2 == this.y && a2 > i2) {
                StaticLayout staticLayout = new StaticLayout(text, new TextPaint(paint), i, Layout.Alignment.ALIGN_NORMAL, this.z, this.A, false);
                if (staticLayout.getLineCount() > 0) {
                    int lineForVertical = staticLayout.getLineForVertical(i2) - 1;
                    if (lineForVertical < 0) {
                        setText("");
                    } else {
                        int lineStart = staticLayout.getLineStart(lineForVertical);
                        int lineEnd = staticLayout.getLineEnd(lineForVertical);
                        float lineWidth = staticLayout.getLineWidth(lineForVertical);
                        float measureText = paint.measureText("...");
                        while (((float) i) < lineWidth + measureText) {
                            lineEnd--;
                            lineWidth = paint.measureText(text.subSequence(lineStart, lineEnd + 1).toString());
                        }
                        setText(((Object) text.subSequence(0, lineEnd)) + "...");
                    }
                }
            }
            setTextSize(0, f2);
            setLineSpacing(this.A, this.z);
            a aVar = this.u;
            if (aVar != null) {
                aVar.a(this, textSize, f2);
            }
            this.v = false;
        }
    }

    @DexIgnore
    public final void f() {
        float f = this.w;
        if (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            super.setTextSize(0, f);
            this.x = this.w;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatTextView
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        if (z2 || this.v) {
            a(((i3 - i) - getCompoundPaddingLeft()) - getCompoundPaddingRight(), ((i4 - i2) - getCompoundPaddingBottom()) - getCompoundPaddingTop());
        }
        super.onLayout(z2, i, i2, i3, i4);
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        if (i != i3 || i2 != i4) {
            this.v = true;
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatTextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.v = true;
        f();
    }

    @DexIgnore
    public void setLineSpacing(float f, float f2) {
        super.setLineSpacing(f, f2);
        this.z = f2;
        this.A = f;
    }

    @DexIgnore
    public void setTextSize(float f) {
        super.setTextSize(f);
        this.w = getTextSize();
    }

    @DexIgnore
    public AutoResizeTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public AutoResizeTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.v = false;
        this.x = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.z = 1.0f;
        this.A = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.B = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.AutoResizeTextView);
        this.y = obtainStyledAttributes.getDimension(0, 20.0f);
        obtainStyledAttributes.recycle();
        this.w = getTextSize();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatTextView
    public void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        this.w = getTextSize();
    }

    @DexIgnore
    public final int a(CharSequence charSequence, TextPaint textPaint, int i, float f) {
        TextPaint textPaint2 = new TextPaint(textPaint);
        textPaint2.setTextSize(f);
        return new StaticLayout(charSequence, textPaint2, i, Layout.Alignment.ALIGN_NORMAL, this.z, this.A, true).getHeight();
    }
}
