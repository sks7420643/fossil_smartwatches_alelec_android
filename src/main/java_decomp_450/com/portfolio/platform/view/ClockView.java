package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.k9;
import com.fossil.oe5;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.yx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ClockView extends View implements GestureDetector.OnGestureListener {
    @DexIgnore
    public static /* final */ String a0; // = ClockView.class.getSimpleName();
    @DexIgnore
    public static /* final */ int[] b0; // = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ Rect F; // = new Rect();
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public boolean O; // = true;
    @DexIgnore
    public boolean P; // = false;
    @DexIgnore
    public boolean Q; // = false;
    @DexIgnore
    public boolean R; // = false;
    @DexIgnore
    public boolean S; // = true;
    @DexIgnore
    public boolean T; // = false;
    @DexIgnore
    public float U;
    @DexIgnore
    public b V;
    @DexIgnore
    public a W;
    @DexIgnore
    public String a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public Paint p;
    @DexIgnore
    public Paint q;
    @DexIgnore
    public Paint r;
    @DexIgnore
    public Bitmap s;
    @DexIgnore
    public k9 t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(float f);

        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore
    public ClockView(Context context) {
        super(context);
        c();
    }

    @DexIgnore
    private void setScale(boolean z2) {
        FLogger.INSTANCE.getLocal().d(a0, "setScale() called with: scale = [" + z2 + "]");
        if (this.E != z2) {
            if (z2) {
                float f2 = this.e;
                float f3 = this.B;
                this.e = f2 / f3;
                float f4 = this.u / f3;
                this.u = f4;
                this.A /= f3;
                this.x = f4 / 2.0f;
                this.C = 1.0f;
            } else {
                float f5 = this.e;
                float f6 = this.B;
                this.e = f5 * f6;
                this.u *= f6;
                this.A *= f6;
                this.x *= f6;
                this.C = f6;
            }
            this.E = z2;
            invalidate();
            a aVar = this.W;
            if (aVar != null) {
                aVar.a(this.u);
                this.W.a(this.J == this.K);
            }
        }
    }

    @DexIgnore
    public final void a() {
        this.p.setStyle(Paint.Style.FILL);
    }

    @DexIgnore
    public final boolean a(float f2, float f3, float f4) {
        return f3 <= f2 && f2 < f4;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.I;
        float f2 = (float) i2;
        this.u = f2;
        float f3 = (float) i2;
        this.v = f3;
        this.w = f3 / 2.0f;
        this.x = f2 / 2.0f;
        this.y = (float) ((int) yx6.a(15.0f));
        float min = (Math.min(this.u, this.v) / 2.0f) - this.y;
        this.z = min;
        float a2 = (float) ((int) (min - yx6.a(15.0f)));
        this.A = a2;
        float f4 = a2 - (this.e * 3.0f);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), 2131231259);
        float height = f4 / ((float) decodeResource.getHeight());
        int width = (int) (((float) decodeResource.getWidth()) * height);
        int height2 = (int) (((float) decodeResource.getHeight()) * height);
        if (width <= 0) {
            width = decodeResource.getWidth();
        }
        if (height2 <= 0) {
            height2 = decodeResource.getHeight();
        }
        this.s = Bitmap.createScaledBitmap(decodeResource, width, height2, false);
        this.U = (this.B - 1.0f) / 20.0f;
    }

    @DexIgnore
    public final void c() {
        this.c = this.b;
        if (getContext().getResources().getConfiguration().getLayoutDirection() == 1) {
            this.b = ((float) oe5.b().a()) - this.b;
            this.T = true;
        }
        this.B = this.j / this.i;
        this.E = true;
        setScale(false);
        this.p = new Paint();
        if (!TextUtils.isEmpty(this.a)) {
            this.p.setTypeface(Typeface.createFromAsset(getResources().getAssets(), this.a));
            this.p.setAntiAlias(true);
        }
        this.p.setTextSize(this.e);
        Paint paint = new Paint();
        this.q = paint;
        paint.setAntiAlias(true);
        this.q.setColor(this.h);
        this.q.setStyle(Paint.Style.FILL);
        Paint paint2 = new Paint();
        this.r = paint2;
        paint2.setAntiAlias(true);
        this.r.setFilterBitmap(true);
        this.r.setDither(true);
        this.t = new k9(getContext(), this);
    }

    @DexIgnore
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.D) {
            b();
            this.D = true;
        }
        if (this.R) {
            b(canvas);
            return;
        }
        a();
        b(canvas);
        a(canvas);
    }

    @DexIgnore
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onLongPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int a2 = a(i2);
        int a3 = a(i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "onMeasure() called with: measureWidth = [" + a2 + "], measureHeight = [" + a3 + "]");
        int min = Math.min(a2, a3);
        setMeasuredDimension(a2, min);
        if (this.O && a2 > a3) {
            this.I = min;
            this.H = (a2 - a3) / 2;
            this.O = false;
        }
    }

    @DexIgnore
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    @DexIgnore
    public void onShowPress(MotionEvent motionEvent) {
    }

    @DexIgnore
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int a2 = a(a(motionEvent.getX(), motionEvent.getY()));
        this.G = a2;
        b bVar = this.V;
        if (bVar != null) {
            bVar.a(a2);
        }
        invalidate();
        return true;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && this.t.a(motionEvent);
    }

    @DexIgnore
    public void setClockOnTouchListener(b bVar) {
        this.V = bVar;
    }

    @DexIgnore
    public void setCurrentHour(int i2) {
        this.G = i2;
        invalidate();
    }

    @DexIgnore
    public void setOnAnimationListener(a aVar) {
        this.W = aVar;
    }

    @DexIgnore
    public void setShowAnimation(boolean z2) {
        this.P = false;
    }

    @DexIgnore
    public void setShowHand(boolean z2) {
        this.S = z2;
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        int i2 = this.G;
        if (i2 != -1 && this.S) {
            Matrix matrix = new Matrix();
            float f2 = this.C;
            matrix.postScale(f2, f2);
            matrix.postTranslate((((float) (-this.s.getWidth())) * this.C) / 2.0f, (((float) (-this.s.getHeight())) * this.C) / 2.0f);
            matrix.postRotate((float) Math.toDegrees(((double) i2) * 0.5235987755982988d), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (((float) this.s.getHeight()) * this.C) / 2.0f);
            matrix.postTranslate(this.w + ((float) this.H), this.x - ((((float) this.s.getHeight()) * this.C) / 2.0f));
            canvas.drawBitmap(this.s, matrix, this.r);
            matrix.reset();
        }
    }

    @DexIgnore
    public ClockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.ClockView);
        this.a = obtainStyledAttributes.getString(3);
        this.i = obtainStyledAttributes.getDimension(7, yx6.a(202.0f));
        this.j = obtainStyledAttributes.getDimension(6, yx6.a(314.0f));
        this.b = obtainStyledAttributes.getDimension(1, yx6.a(50.0f));
        this.d = obtainStyledAttributes.getDimension(0, yx6.a(50.0f));
        this.e = obtainStyledAttributes.getDimension(5, (float) yx6.b(13.0f));
        this.f = obtainStyledAttributes.getColor(2, v6.a(context, 2131099703));
        this.g = obtainStyledAttributes.getColor(4, v6.a(context, 2131100361));
        this.h = obtainStyledAttributes.getColor(8, v6.a(context, 2131099829));
        obtainStyledAttributes.recycle();
        c();
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int abs = Math.abs(i3);
        this.J = abs;
        this.K = i2;
        this.R = abs > 0;
        if (!this.P) {
            float f2 = this.d;
            float f3 = this.e;
            float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (f2 - f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f4 = f2 - f3;
            }
            this.L = f4;
            this.P = true;
        }
        invalidate();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        float f2;
        float f3;
        double d2 = 0.5235987755982988d;
        int i2 = 0;
        float f4 = 2.0f;
        if (this.R) {
            int i3 = this.J;
            float f5 = ((float) i3) / ((float) this.K);
            float f6 = this.L * f5;
            float f7 = i3 == 0 ? this.e : this.e + f6;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "drawNumeral() called with: deltaOffset = [" + f5 + "], deltaFontScale = [" + this.L + "], fontScale = [" + f6 + "], finalFontSize = [" + f7 + "], deltaXScale = [" + this.M + "], deltaYScale = [" + this.N + "]");
            float f8 = this.e;
            if (f7 <= f8) {
                f7 = f8;
            }
            this.p.setTextSize(f7);
            this.p.getTextBounds("22", 0, "22".length(), this.F);
            double d3 = ((double) (this.G - 3)) * 0.5235987755982988d;
            if (!this.T) {
                f3 = ((float) (((int) ((((double) (this.v / 2.0f)) + (Math.cos(d3) * ((double) this.A))) - ((double) (this.F.width() / 2)))) + this.H)) - (this.M * f5);
                f2 = ((float) ((int) ((((double) (this.u / 2.0f)) + (Math.sin(d3) * ((double) this.A))) + ((double) (this.F.height() / 2))))) - (this.N * f5);
                if (f3 - f7 <= this.b - yx6.a(10.0f)) {
                    f3 = (this.b - yx6.a(10.0f)) + f7;
                }
                float f9 = this.c;
                if (f2 - f7 <= f9) {
                    f2 = f7 + f9;
                }
            } else {
                float cos = ((float) (((int) ((((double) (this.v / 2.0f)) + (Math.cos(d3) * ((double) this.A))) - ((double) (this.F.width() / 2)))) + this.H)) + (this.M * f5);
                f2 = ((float) ((int) ((((double) (this.u / 2.0f)) + (Math.sin(d3) * ((double) this.A))) + ((double) (this.F.height() / 2))))) - (this.N * f5);
                if (cos + f7 >= this.b - yx6.a(10.0f)) {
                    cos = (this.b - yx6.a(10.0f)) - f7;
                }
                float f10 = this.c;
                if (f2 - f7 <= f10) {
                    f2 = f10 + f7;
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.d(str2, "drawNumeral() called with: x = [" + f3 + "], y = [" + f2 + "]");
            canvas.drawCircle(((float) (this.F.width() / 2)) + f3, f2 - ((float) (this.F.height() / 2)), f7, this.q);
            this.p.setColor(this.g);
            String valueOf = String.valueOf(this.G);
            canvas.drawText(valueOf, (float) ((int) (f3 + ((((float) this.F.width()) - this.p.measureText(valueOf)) / 2.0f))), f2, this.p);
            return;
        }
        int[] iArr = b0;
        int length = iArr.length;
        int i4 = 0;
        while (i4 < length) {
            int i5 = iArr[i4];
            this.p.setTextSize(this.e);
            this.p.getTextBounds("22", i2, "22".length(), this.F);
            double d4 = ((double) (i5 - 3)) * d2;
            int cos2 = ((int) ((((double) (this.v / f4)) + (Math.cos(d4) * ((double) this.A))) - ((double) (this.F.width() / 2)))) + this.H;
            int sin = (int) (((double) (this.u / f4)) + (Math.sin(d4) * ((double) this.A)) + ((double) (this.F.height() / 2)));
            if (i5 == this.G) {
                canvas.drawCircle((float) ((this.F.width() / 2) + cos2), (float) (sin - (this.F.height() / 2)), this.e, this.q);
                this.p.setColor(this.g);
                if (!this.Q) {
                    if (!this.T) {
                        this.M = (float) cos2;
                    } else {
                        this.M = ((float) oe5.b().a()) - this.M;
                    }
                    this.N = (float) sin;
                    this.Q = true;
                }
            } else {
                this.p.setColor(this.f);
            }
            canvas.drawText(String.valueOf(i5), (float) ((int) (((float) cos2) + ((((float) this.F.width()) - this.p.measureText(String.valueOf(i5))) / 2.0f))), (float) sin, this.p);
            i4++;
            d2 = 0.5235987755982988d;
            i2 = 0;
            f4 = 2.0f;
        }
    }

    @DexIgnore
    public final int a(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            return 200;
        }
        return size;
    }

    @DexIgnore
    public final float a(float f2, float f3) {
        return ((float) Math.toDegrees(Math.atan2((double) (f3 - ((float) (getHeight() / 2))), (double) (f2 - ((float) (getWidth() / 2)))))) + 90.0f;
    }

    @DexIgnore
    public final int a(float f2) {
        if (a(f2, -15.0f, 15.0f)) {
            return 12;
        }
        if (a(f2, 15.0f, 45.0f)) {
            return 1;
        }
        if (a(f2, 45.0f, 75.0f)) {
            return 2;
        }
        if (a(f2, 75.0f, 105.0f)) {
            return 3;
        }
        if (a(f2, 105.0f, 135.0f)) {
            return 4;
        }
        if (a(f2, 135.0f, 165.0f)) {
            return 5;
        }
        if (a(f2, 165.0f, 195.0f)) {
            return 6;
        }
        if (a(f2, 195.0f, 225.0f)) {
            return 7;
        }
        if (a(f2, 225.0f, 255.0f)) {
            return 8;
        }
        if (a(f2, 255.0f, 270.0f) || a(f2, -90.0f, -70.0f)) {
            return 9;
        }
        if (a(f2, -70.0f, -40.0f)) {
            return 10;
        }
        return a(f2, -40.0f, -15.0f) ? 11 : 0;
    }
}
