package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.pl4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingChart extends View {
    @DexIgnore
    public /* final */ ArrayList<b> a; // = new ArrayList<>();
    @DexIgnore
    public float b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public float e;
    @DexIgnore
    public Paint f; // = new Paint(1);
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public GestureDetector i;
    @DexIgnore
    public View.OnClickListener j;
    @DexIgnore
    public RectF p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            ee7.b(motionEvent, "e");
            return RingChart.this.getMListener$app_fossilRelease() != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            ee7.b(motionEvent, "e");
            if (RingChart.this.getMListener$app_fossilRelease() != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float measuredWidth = (float) (RingChart.this.getMeasuredWidth() / 2);
                float f = x - measuredWidth;
                float f2 = y - measuredWidth;
                if ((f * f) + (f2 * f2) < (RingChart.this.getMThickness$app_fossilRelease() + measuredWidth) * (measuredWidth + RingChart.this.getMThickness$app_fossilRelease())) {
                    View.OnClickListener mListener$app_fossilRelease = RingChart.this.getMListener$app_fossilRelease();
                    if (mListener$app_fossilRelease != null) {
                        mListener$app_fossilRelease.onClick(RingChart.this);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {
        @DexIgnore
        public float a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final void b(float f) {
            this.c = f;
        }

        @DexIgnore
        public final void c(float f) {
            this.a = f;
        }

        @DexIgnore
        public final float d() {
            return this.a;
        }

        @DexIgnore
        public final void a(int i) {
            this.b = i;
        }

        @DexIgnore
        public final float b() {
            return this.d;
        }

        @DexIgnore
        public final float c() {
            return this.c;
        }

        @DexIgnore
        public final void a(float f) {
            this.d = f;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context) {
        super(context);
        ee7.b(context, "context");
        a();
    }

    @DexIgnore
    public final void a(float f2, int i2) {
        b bVar = new b();
        bVar.a(i2);
        bVar.c(f2);
        this.b += f2;
        this.a.add(bVar);
    }

    @DexIgnore
    public final void b() {
        float max = Math.max(this.b, (float) this.c);
        Iterator<b> it = this.a.iterator();
        float f2 = -90.0f;
        while (it.hasNext()) {
            b next = it.next();
            next.b(f2);
            if (max != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f2 += (next.d() / max) * 360.0f;
            }
            next.a(f2);
            if (next.b() > ((float) 270)) {
                next.a(270.0f);
            }
            f2 = next.b();
        }
        invalidate();
    }

    @DexIgnore
    public final View.OnClickListener getMListener$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    public final float getMThickness$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        ee7.b(canvas, "canvas");
        super.onDraw(canvas);
        RectF rectF = this.p;
        if (rectF != null) {
            float centerX = rectF.centerX();
            RectF rectF2 = this.p;
            if (rectF2 != null) {
                float centerY = rectF2.centerY();
                RectF rectF3 = this.p;
                if (rectF3 != null) {
                    float f3 = (float) 2;
                    float width = rectF3.width() / f3;
                    Paint paint = this.h;
                    if (paint != null) {
                        canvas.drawCircle(centerX, centerY, width, paint);
                        Iterator<b> it = this.a.iterator();
                        while (it.hasNext()) {
                            b next = it.next();
                            this.f.setColor(next.a());
                            if (next.b() > next.c()) {
                                RectF rectF4 = this.p;
                                if (rectF4 != null) {
                                    canvas.drawArc(rectF4, next.c(), next.b() - next.c(), true, this.f);
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                        }
                        float f4 = this.b;
                        if (f4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f4 < ((float) this.c)) {
                            this.f.setColor(this.d);
                            if (this.a.isEmpty() || this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                f2 = -90.0f;
                            } else {
                                ArrayList<b> arrayList = this.a;
                                f2 = arrayList.get(arrayList.size() - 1).b();
                            }
                            if (270.0f > f2) {
                                RectF rectF5 = this.p;
                                if (rectF5 != null) {
                                    canvas.drawArc(rectF5, f2, 270.0f - f2, true, this.f);
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                        }
                        RectF rectF6 = this.p;
                        if (rectF6 != null) {
                            float centerX2 = rectF6.centerX();
                            RectF rectF7 = this.p;
                            if (rectF7 != null) {
                                float centerY2 = rectF7.centerY();
                                RectF rectF8 = this.p;
                                if (rectF8 != null) {
                                    float width2 = (rectF8.width() / f3) - this.e;
                                    Paint paint2 = this.g;
                                    if (paint2 != null) {
                                        canvas.drawCircle(centerX2, centerY2, width2, paint2);
                                    } else {
                                        ee7.d("mClearPaint");
                                        throw null;
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mBackgroundPaint");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        RectF rectF = this.p;
        if (rectF == null) {
            this.p = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i3);
        } else if (rectF != null) {
            rectF.right = (float) i2;
            if (rectF != null) {
                rectF.bottom = (float) i3;
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
        b();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        ee7.b(motionEvent, Constants.EVENT);
        GestureDetector gestureDetector = this.i;
        if (gestureDetector != null) {
            return gestureDetector.onTouchEvent(motionEvent);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void setGoal(int i2) {
        this.c = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(View.OnClickListener onClickListener) {
        this.j = onClickListener;
    }

    @DexIgnore
    public final void setMThickness$app_fossilRelease(float f2) {
        this.e = f2;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, pl4.RingChart, 0, 0);
        try {
            this.c = obtainStyledAttributes.getInteger(1, 0);
            this.d = obtainStyledAttributes.getColor(0, 0);
            Resources resources = context.getResources();
            ee7.a((Object) resources, "context.resources");
            this.e = obtainStyledAttributes.getDimension(2, resources.getDisplayMetrics().density * ((float) 8));
            obtainStyledAttributes.recycle();
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a() {
        setLayerType(1, null);
        this.f.setStyle(Paint.Style.FILL);
        Paint paint = new Paint(this.f);
        this.g = paint;
        if (paint != null) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            this.i = new GestureDetector(getContext(), new a());
            Paint paint2 = new Paint(this.f);
            this.h = paint2;
            if (paint2 != null) {
                paint2.setAlpha(50);
                if (isInEditMode()) {
                    Random random = new Random();
                    float f2 = (float) 100;
                    float nextFloat = random.nextFloat() * f2;
                    float f3 = f2 - nextFloat;
                    float nextFloat2 = random.nextFloat() * f3;
                    a(nextFloat, -65536);
                    a(nextFloat2, -16711936);
                    a(random.nextFloat() * (f3 - nextFloat2), -16776961);
                    if (this.c == 0) {
                        this.c = 100;
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBackgroundPaint");
            throw null;
        }
        ee7.d("mClearPaint");
        throw null;
    }
}
