package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gb5;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.kz6;
import com.fossil.nh7;
import com.fossil.pl4;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public gb5 B;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<LinkedList<Integer>, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ SleepMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, int i2, SleepMonthDetailsChart sleepMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = sleepMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(LinkedList<Integer> linkedList) {
            invoke(linkedList);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            ee7.b(linkedList, "centerXList");
            this.this$0.a(this.$canvas$inlined, linkedList);
            SleepMonthDetailsChart sleepMonthDetailsChart = this.this$0;
            sleepMonthDetailsChart.a(this.$canvas$inlined, linkedList, this.$chartWidth, this.$chartHeight, sleepMonthDetailsChart.y * 2, this.$chartHeight - 4);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = SleepMonthDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "SleepMonthDetailsChart::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = context.getResources().getDimensionPixelSize(2131165384);
        this.x = context.getResources().getDimensionPixelSize(2131165372);
        this.y = context.getResources().getDimensionPixelSize(2131165419);
        this.z = context.getResources().getDimensionPixelSize(2131165372);
        this.A = context.getResources().getDimensionPixelSize(2131165406);
        this.B = gb5.TOTAL_SLEEP;
        this.C = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.SleepMonthDetailsChart, 0, 0));
        }
        int a2 = v6.a(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(4, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(3, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(1, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(2, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.i = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.p = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296261) : 2131296261;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = kz6.b[this.B.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G;
            local.d(str, "Max of awake: " + this.F);
            i2 = this.F;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = G;
            local2.d(str2, "Max of light: " + this.E);
            i2 = this.E;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = G;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = G;
            local4.d(str4, "Max of restful: " + this.D);
            i2 = this.D;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                int sleepGoal = next.getSleepGoal();
                do {
                    T next2 = it.next();
                    int sleepGoal2 = next2.getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        next = next2;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                int totalSleepMinutes = next.getTotalSleepMinutes();
                do {
                    T next2 = it.next();
                    int totalSleepMinutes2 = next2.getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        next = next2;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = kz6.a[this.B.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            Rect rect = new Rect();
            this.s.getTextBounds("1", 0, nh7.c("1") > 0 ? nh7.c((CharSequence) "1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.y)) - ((float) this.A));
            int width = getWidth() - getStartBitmap().getWidth();
            int i2 = this.y;
            int i3 = this.z;
            int i4 = (width - (i2 * 2)) - i3;
            a(canvas, i4, height, i3, i2 * 2, new b(i4, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.w;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.q.setColor(this.d);
        float f2 = (float) 4;
        this.q.setStrokeWidth(f2);
        this.s.setColor(this.i);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setTextSize((float) this.j);
        this.s.setTypeface(c7.a(getContext(), this.p));
        this.r.setColor(this.e);
        this.r.setStyle(Paint.Style.STROKE);
        this.r.setStrokeWidth(f2);
        this.r.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.t.setColor(this.f);
        this.t.setStrokeWidth((float) this.x);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.g);
        this.u.setStrokeWidth((float) this.x);
        this.u.setStyle(Paint.Style.FILL);
        this.v.setColor(this.h);
        this.v.setStrokeWidth((float) this.x);
        this.v.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, gd7<? super LinkedList<Integer>, i97> gd7) {
        float f2;
        if (!this.C.isEmpty()) {
            int size = i2 / this.C.size();
            int i6 = this.x;
            int i7 = size < i6 ? size : i6;
            int i8 = i7 / 2;
            LinkedList linkedList = new LinkedList();
            float mChartMax = (float) getMChartMax();
            Iterator<SleepDayData> it = this.C.iterator();
            int i9 = i4;
            while (it.hasNext()) {
                SleepDayData next = it.next();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = G;
                local.d(str, "Actual sleep: " + next.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int i10 = i9 + (i8 / 2);
                int totalSleepMinutes = next.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    f2 = mChartMax;
                    int dayAwake = next.getDayAwake();
                    a(canvas, this.t, i10, i3, (int) ((((float) dayAwake) / f2) * ((float) i3)), i8, i5, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    f2 = mChartMax;
                    int dayLight = next.getDayLight();
                    a(canvas, this.u, i10, i3, (int) ((((float) dayLight) / f2) * ((float) i3)), i8, i5, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    ee7.a((Object) next, "sleepDayData");
                    a(canvas, i10, i3, (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i3)), i8, i5, next);
                    f2 = mChartMax;
                } else {
                    int dayRestful = next.getDayRestful();
                    f2 = mChartMax;
                    a(canvas, this.v, i10, i3, (int) ((((float) dayRestful) / mChartMax) * ((float) i3)), i8, i5, dayRestful, totalSleepMinutes);
                }
                i9 += i7;
                linkedList.add(Integer.valueOf(i10));
                mChartMax = f2;
            }
            gd7.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = i2 - (i5 / 2);
        if (i9 < 0) {
            i9 = 0;
        }
        RectF rectF = new RectF((float) i9, (float) ((i3 - ((int) ((((float) i7) / ((float) i8)) * ((float) i4)))) + i6), (float) (i5 + i9), (float) i3);
        tw6.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, int i6, SleepDayData sleepDayData) {
        int i7;
        int i8;
        int i9;
        int i10;
        Paint paint;
        int i11 = i2 - (i5 / 2);
        if (i11 < 0) {
            i11 = 0;
        }
        int i12 = i11 + i5;
        int size = (i4 - i6) - (this.A * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i13 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G;
            local.d(str, "session duration: " + component1);
            i13 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i14 = i3;
        int i15 = 0;
        while (i15 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i15);
            ee7.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i16 = (int) ((f2 / ((float) i13)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i17 = 0;
            while (i17 < size3) {
                int i18 = sleepStates.get(i17).state;
                if (i17 < size3 - 1) {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = ((int) sleepStates.get(i17 + 1).index) - ((int) sleepStates.get(i17).index);
                } else {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = durationInMinutes - ((int) sleepStates.get(i17).index);
                }
                int i19 = (int) ((((float) i10) / f2) * ((float) i16));
                if (i19 < 1) {
                    i19 = 1;
                }
                int i20 = i14 - i19;
                RectF rectF = new RectF((float) i11, (float) i20, (float) i12, (float) i14);
                if (i18 == 1) {
                    paint = this.u;
                } else if (i18 != 2) {
                    paint = this.t;
                } else {
                    paint = this.v;
                }
                canvas.drawRect(rectF, paint);
                i17++;
                size2 = i7;
                i14 = i20;
                i13 = i9;
                i11 = i11;
                size = i8;
            }
            i14 -= this.A;
            i15++;
            size2 = size2;
            i11 = i11;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        if (!list.isEmpty()) {
            int size = list.size();
            int i2 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                int i3 = 0;
                while (i2 < length) {
                    try {
                        i3 = numArr[i2].intValue();
                        a(canvas, String.valueOf(i3 + 1), list.get(i3).intValue());
                        i2++;
                    } catch (Exception e2) {
                        e = e2;
                        i2 = i3;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = G;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e3) {
                e = e3;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = G;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.s.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.A), this.s);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.q);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.q);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        List<Integer> list2 = list;
        if ((!this.C.isEmpty()) && this.C.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            float height = (float) getStartBitmap().getHeight();
            int size = list.size();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = G;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.C.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.C.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.C.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.C.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.y), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas.drawPath(path, this.r);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas.drawPath(path, this.r);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas.drawPath(path, this.r);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.y), min);
                        canvas.drawPath(path, this.r);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas.drawBitmap(getStartBitmap(), (float) (i2 + this.y), height - ((float) (getStartBitmap().getHeight() / 2)), this.s);
        }
    }
}
