package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ae7;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gb5;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.lz6;
import com.fossil.nh7;
import com.fossil.pl4;
import com.fossil.qe7;
import com.fossil.se7;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String I;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public gb5 G;
    @DexIgnore
    public /* final */ String[] H;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements kd7<Integer, List<? extends Integer>, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ qe7 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qe7 qe7, se7 se7) {
            super(2);
            this.$bottomTextHeight = qe7;
            this.$charactersCenterXList = se7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public /* bridge */ /* synthetic */ i97 invoke(Integer num, List<? extends Integer> list) {
            invoke(num.intValue(), (List<Integer>) list);
            return i97.a;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<java.lang.Integer> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void invoke(int i, List<Integer> list) {
            ee7.b(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = SleepWeekDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "SleepWeekDetailsChart::class.java.simpleName");
        I = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = context.getResources().getDimensionPixelSize(2131165384);
        this.x = context.getResources().getDimensionPixelSize(2131165384);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165372);
        this.A = context.getResources().getDimensionPixelSize(2131165419);
        this.B = context.getResources().getDimensionPixelSize(2131165406);
        this.C = new ArrayList<>();
        this.G = gb5.TOTAL_SLEEP;
        String[] stringArray = context.getResources().getStringArray(2130903040);
        ee7.a((Object) stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.H = stringArray;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.SleepWeekDetailsChart, 0, 0));
        }
        int a2 = v6.a(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(1, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(2, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.i = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.p = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296261) : 2131296261;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = lz6.a[this.G.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = I;
            local.d(str, "Max of awake: " + this.F);
            i2 = this.F;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = I;
            local2.d(str2, "Max of light: " + this.E);
            i2 = this.E;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = I;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = I;
            local4.d(str4, "Max of restful: " + this.D);
            i2 = this.D;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                int sleepGoal = next.getSleepGoal();
                do {
                    T next2 = it.next();
                    int sleepGoal2 = next2.getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        next = next2;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.C.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                int totalSleepMinutes = next.getTotalSleepMinutes();
                do {
                    T next2 = it.next();
                    int totalSleepMinutes2 = next2.getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        next = next2;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = lz6.b[this.G.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3) {
        if (!this.C.isEmpty()) {
            float mChartMax = (float) getMChartMax();
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                SleepDayData sleepDayData = this.C.get(i4);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = I;
                local.d(str, "Actual sleep: " + sleepDayData.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int intValue = list.get(i4).intValue();
                int totalSleepMinutes = sleepDayData.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int dayAwake = sleepDayData.getDayAwake();
                    a(canvas, this.t, intValue, i2, (int) ((((float) dayAwake) / mChartMax) * ((float) i2)), i3, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    int dayLight = sleepDayData.getDayLight();
                    a(canvas, this.u, intValue, i2, (int) ((((float) dayLight) / mChartMax) * ((float) i2)), i3, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    int intValue2 = list.get(i4).intValue();
                    int i5 = (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i2));
                    SleepDayData sleepDayData2 = this.C.get(i4);
                    ee7.a((Object) sleepDayData2, "mSleepDayDataList[i]");
                    a(canvas, intValue2, i2, i5, i3, sleepDayData2);
                } else {
                    int dayRestful = sleepDayData.getDayRestful();
                    a(canvas, this.v, intValue, i2, (int) ((((float) dayRestful) / mChartMax) * ((float) i2)), i3, dayRestful, totalSleepMinutes);
                }
            }
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.A * 2);
            qe7 qe7 = new qe7();
            qe7.element = 0;
            se7 se7 = new se7();
            se7.element = null;
            a(canvas, width, new b(qe7, se7));
            int height = (getHeight() - qe7.element) - this.y;
            T t2 = se7.element;
            if (t2 != null) {
                a(canvas, t2, height, this.A * 2);
                a(canvas, t2, width, height, this.A * 2, height);
            }
            a(canvas, 0, height + 4);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.w;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.q.setColor(this.d);
        float f2 = (float) 4;
        this.q.setStrokeWidth(f2);
        this.s.setColor(this.i);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setTextSize((float) this.j);
        this.s.setTypeface(c7.a(getContext(), this.p));
        this.r.setColor(this.e);
        this.r.setStyle(Paint.Style.STROKE);
        this.r.setStrokeWidth(f2);
        this.r.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.t.setColor(this.f);
        this.t.setStrokeWidth((float) this.z);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.g);
        this.u.setStrokeWidth((float) this.z);
        this.u.setStyle(Paint.Style.FILL);
        this.v.setColor(this.h);
        this.v.setStrokeWidth((float) this.z);
        this.v.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i2 - (this.z / 2);
        if (i8 < 0) {
            i8 = 0;
        }
        RectF rectF = new RectF((float) i8, (float) ((i3 - ((int) ((((float) i6) / ((float) i7)) * ((float) i4)))) + i5), (float) (this.z + i8), (float) i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = I;
        local.d(str, "duration: " + i6 + ", totalMinutes: " + i7 + ", rect top: " + rectF.top + ", rect bot: " + rectF.bottom);
        tw6.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, SleepDayData sleepDayData) {
        int i6;
        int i7;
        int i8;
        int i9;
        Paint paint;
        int i10 = i2 - (this.z / 2);
        if (i10 < 0) {
            i10 = 0;
        }
        int i11 = this.z + i10;
        int size = (i4 - i5) - (this.B * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i12 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = I;
            local.d(str, "session duration: " + component1);
            i12 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i13 = i3;
        int i14 = 0;
        while (i14 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i14);
            ee7.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i15 = (int) ((f2 / ((float) i12)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i16 = 0;
            while (i16 < size3) {
                int i17 = sleepStates.get(i16).state;
                if (i16 < size3 - 1) {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = ((int) sleepStates.get(i16 + 1).index) - ((int) sleepStates.get(i16).index);
                } else {
                    i6 = size;
                    i8 = size2;
                    i7 = i12;
                    i9 = durationInMinutes - ((int) sleepStates.get(i16).index);
                }
                int i18 = (int) ((((float) i9) / f2) * ((float) i15));
                if (i18 < 1) {
                    i18 = 1;
                }
                int i19 = i13 - i18;
                RectF rectF = new RectF((float) i10, (float) i19, (float) i11, (float) i13);
                if (i17 == 1) {
                    paint = this.u;
                } else if (i17 != 2) {
                    paint = this.t;
                } else {
                    paint = this.v;
                }
                canvas.drawRect(rectF, paint);
                i16++;
                i13 = i19;
                size2 = i8;
                i12 = i7;
                i10 = i10;
                size = i6;
            }
            i13 -= this.B;
            i14++;
            size2 = size2;
            i10 = i10;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, kd7<? super Integer, ? super List<Integer>, i97> kd7) {
        int length = this.H.length;
        Rect rect = new Rect();
        String str = this.H[0];
        ae7 ae7 = ae7.a;
        Paint paint = this.s;
        ee7.a((Object) str, "sundayCharacter");
        paint.getTextBounds(str, 0, nh7.c(str) > 0 ? nh7.c((CharSequence) str) : 1, rect);
        float measureText = this.s.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.x * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float f3 = (float) 2;
        float height2 = ((float) getHeight()) - (height / f3);
        float f4 = (float) this.x;
        LinkedList linkedList = new LinkedList();
        for (String str2 : this.H) {
            canvas.drawText(str2, f4, height2, this.s);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f4)));
            f4 += measureText + f2;
        }
        kd7.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.q);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.q);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        List<Integer> list2 = list;
        if ((!this.C.isEmpty()) && this.C.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            float height = (float) getStartBitmap().getHeight();
            int size = list.size();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = I;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.C.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.C.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.C.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.C.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.A), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas.drawPath(path, this.r);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas.drawPath(path, this.r);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas.drawPath(path, this.r);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.A), min);
                        canvas.drawPath(path, this.r);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas.drawBitmap(getStartBitmap(), (float) (i2 + this.A), height - ((float) (getStartBitmap().getHeight() / 2)), this.s);
        }
    }
}
