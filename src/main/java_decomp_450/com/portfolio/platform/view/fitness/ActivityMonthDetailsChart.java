package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.nh7;
import com.fossil.pl4;
import com.fossil.r87;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public /* final */ ArrayList<r87<Float, Float>> A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<LinkedList<Integer>, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, int i2, ActivityMonthDetailsChart activityMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = activityMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(LinkedList<Integer> linkedList) {
            invoke(linkedList);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            ee7.b(linkedList, "barCenterXList");
            this.this$0.a(this.$canvas$inlined, linkedList);
            ActivityMonthDetailsChart activityMonthDetailsChart = this.this$0;
            Canvas canvas = this.$canvas$inlined;
            int i = this.$chartWidth;
            int i2 = this.$chartHeight;
            activityMonthDetailsChart.a(canvas, linkedList, i, i2, i2 - 4);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ActivityMonthDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivityMonthDetailsChart::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = context.getResources().getDimensionPixelSize(2131165384);
        this.v = context.getResources().getDimensionPixelSize(2131165372);
        this.w = context.getResources().getDimensionPixelSize(2131165419);
        this.x = context.getResources().getDimensionPixelSize(2131165406);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = 4;
        this.A = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.ActivityMonthDetailsChart, 0, 0));
        }
        int a2 = v6.a(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.i = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296261) : 2131296261;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxValues());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                float floatValue = ((Number) next.getSecond()).floatValue();
                do {
                    T next2 = it.next();
                    float floatValue2 = ((Number) next2.getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        next = next2;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 == null || (f2 = (Float) t3.getSecond()) == null) {
            return this.B;
        }
        return f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxValues() {
        T t2;
        Float f2;
        Iterator<T> it = this.A.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (!it.hasNext()) {
                t2 = next;
            } else {
                float floatValue = ((Number) next.getFirst()).floatValue();
                do {
                    T next2 = it.next();
                    float floatValue2 = ((Number) next2.getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        next = next2;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 == null || (f2 = (Float) t3.getFirst()) == null) {
            return this.C;
        }
        return f2.floatValue();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            Rect rect = new Rect();
            this.r.getTextBounds("1", 0, nh7.c("1") > 0 ? nh7.c((CharSequence) "1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.w)) - ((float) this.x));
            int i2 = this.y;
            int width = ((getWidth() - getStartBitmap().getWidth()) - (this.w * 2)) - i2;
            a(canvas, width, height, i2, new b(width, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.u;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.d);
        float f2 = (float) 4;
        this.p.setStrokeWidth(f2);
        this.r.setColor(this.h);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.j);
        this.r.setTypeface(c7.a(getContext(), this.i));
        this.q.setColor(this.e);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f2);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.s.setColor(this.f);
        this.s.setStrokeWidth((float) this.v);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.g);
        this.t.setStrokeWidth((float) this.v);
        this.t.setStyle(Paint.Style.FILL);
        this.z = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, gd7<? super LinkedList<Integer>, i97> gd7) {
        if (!this.A.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = i2 / this.A.size();
            int i5 = this.v;
            if (size < i5) {
                i5 = i2 / this.A.size();
            }
            int i6 = i5 / 2;
            int i7 = i6 / 3;
            LinkedList linkedList = new LinkedList();
            Iterator<r87<Float, Float>> it = this.A.iterator();
            while (it.hasNext()) {
                float f2 = (float) i3;
                RectF rectF = new RectF((float) i4, Math.max(f2 - ((it.next().component1().floatValue() / mChartMax) * f2), (float) this.z), (float) (i4 + i6), f2);
                linkedList.add(Integer.valueOf((int) (rectF.left + (rectF.width() / ((float) 2)))));
                i4 += i5;
                tw6.c(canvas, rectF, this.s, (float) i7);
            }
            gd7.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        if ((!this.A.isEmpty()) && this.A.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                float height = (float) getStartBitmap().getHeight();
                int size = list.size();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = D;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(this.A.get(i6).getSecond().floatValue());
                    sb.append(", current goal: ");
                    sb.append(this.A.get(i5).getSecond().floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (this.A.get(i5).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.z);
                    float intValue2 = (float) list.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (this.A.get(i6).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.z);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.w), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas.drawPath(path, this.q);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas.drawPath(path, this.q);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas.drawPath(path, this.q);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.w), max);
                            canvas.drawPath(path, this.q);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas.drawBitmap(getStartBitmap(), (float) (i2 + this.w), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        if (!list.isEmpty()) {
            int size = list.size();
            int i2 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                int i3 = 0;
                while (i2 < length) {
                    try {
                        i3 = numArr[i2].intValue();
                        a(canvas, String.valueOf(i3 + 1), list.get(i3).intValue());
                        i2++;
                    } catch (Exception e2) {
                        e = e2;
                        i2 = i3;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = D;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e3) {
                e = e3;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = D;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.r.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.x), this.r);
    }
}
