package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gb5;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.iz6;
import com.fossil.nh7;
import com.fossil.pe7;
import com.fossil.pl4;
import com.fossil.qe7;
import com.fossil.re5;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.v87;
import com.fossil.we7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String k0;
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ Paint B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public /* final */ Paint I;
    @DexIgnore
    public /* final */ int J;
    @DexIgnore
    public /* final */ String K;
    @DexIgnore
    public /* final */ String L;
    @DexIgnore
    public /* final */ int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public /* final */ int O;
    @DexIgnore
    public /* final */ int P;
    @DexIgnore
    public /* final */ int Q;
    @DexIgnore
    public /* final */ Typeface R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public float W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h;
    @DexIgnore
    public float h0;
    @DexIgnore
    public List<v87<Integer, Float, v87<Float, Float, Float>>> i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public int j0;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public gb5 y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<RectF, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ pe7 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ pe7 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ qe7 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $leftIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, qe7 qe7, pe7 pe7, pe7 pe72) {
            super(1);
            this.$leftIndex = i;
            this.$goalPosition = qe7;
            this.$goalDashLineBottom = pe7;
            this.$goalDashLineX = pe72;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(RectF rectF) {
            invoke(rectF);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            ee7.b(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$leftIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<RectF, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ pe7 $goalDashLineBottom;
        @DexIgnore
        public /* final */ /* synthetic */ pe7 $goalDashLineX;
        @DexIgnore
        public /* final */ /* synthetic */ qe7 $goalPosition;
        @DexIgnore
        public /* final */ /* synthetic */ int $rightIndex;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, qe7 qe7, pe7 pe7, pe7 pe72) {
            super(1);
            this.$rightIndex = i;
            this.$goalPosition = qe7;
            this.$goalDashLineBottom = pe7;
            this.$goalDashLineX = pe72;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(RectF rectF) {
            invoke(rectF);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(RectF rectF) {
            ee7.b(rectF, "drawnRect");
            if (rectF.height() > ((float) 0) && this.$rightIndex == this.$goalPosition.element) {
                this.$goalDashLineBottom.element = rectF.top;
                this.$goalDashLineX.element = (rectF.right + rectF.left) / ((float) 2);
            }
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ActivityDayDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "ActivityDayDetailsChart::class.java.simpleName");
        k0 = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.e = 0.5f;
        this.y = gb5.ACTIVE_TIME;
        this.z = new Paint(1);
        this.A = new Paint(1);
        this.B = new Paint(1);
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        this.I = new Paint(1);
        this.J = context.getResources().getDimensionPixelSize(2131165419);
        we7 we7 = we7.a;
        String string = context.getString(2131887228);
        ee7.a((Object) string, "context.getString(R.string.am_hour)");
        String format = String.format(string, Arrays.copyOf(new Object[]{12}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        this.K = format;
        we7 we72 = we7.a;
        String string2 = context.getString(2131887473);
        ee7.a((Object) string2, "context.getString(R.string.pm_hour)");
        String format2 = String.format(string2, Arrays.copyOf(new Object[]{12}, 1));
        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
        this.L = format2;
        this.M = v6.a(context, 2131099849);
        this.N = 4;
        this.O = context.getResources().getDimensionPixelSize(2131165419);
        this.P = context.getResources().getDimensionPixelSize(2131165425);
        this.Q = context.getResources().getDimensionPixelSize(2131165384);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.ActivityDayDetailsChart, 0, 0));
        }
        this.R = c7.a(context, 2131296258);
        int a2 = v6.a(context, 2131099831);
        int a3 = v6.a(context, 2131099849);
        int a4 = v6.a(context, (int) R.color.steps);
        int a5 = v6.a(context, 2131099845);
        TypedArray mTypedArray = getMTypedArray();
        this.r = mTypedArray != null ? mTypedArray.getColor(4, a3) : a3;
        TypedArray mTypedArray2 = getMTypedArray();
        this.s = mTypedArray2 != null ? mTypedArray2.getColor(5, a4) : a4;
        TypedArray mTypedArray3 = getMTypedArray();
        this.t = mTypedArray3 != null ? mTypedArray3.getColor(3, a5) : a5;
        TypedArray mTypedArray4 = getMTypedArray();
        this.u = mTypedArray4 != null ? mTypedArray4.getColor(1, a4) : a4;
        TypedArray mTypedArray5 = getMTypedArray();
        this.v = mTypedArray5 != null ? mTypedArray5.getColor(0, a4) : a4;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getColor(7, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.p = mTypedArray7 != null ? mTypedArray7.getColor(6, a3) : a3;
        TypedArray mTypedArray8 = getMTypedArray();
        this.q = mTypedArray8 != null ? mTypedArray8.getColor(2, a2) : a2;
        TypedArray mTypedArray9 = getMTypedArray();
        this.w = mTypedArray9 != null ? mTypedArray9.getColor(8, a2) : a2;
        TypedArray mTypedArray10 = getMTypedArray();
        this.x = mTypedArray10 != null ? mTypedArray10.getDimensionPixelSize(9, 40) : 40;
        TypedArray mTypedArray11 = getMTypedArray();
        if (mTypedArray11 != null) {
            mTypedArray11.recycle();
        }
    }

    @DexIgnore
    private final float getGoalForEachBar() {
        return this.h / ((float) 16);
    }

    @DexIgnore
    private final String getMHighLevelText() {
        int i2 = iz6.a[this.y.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887410 : 2131887394 : 2131887408;
        we7 we7 = we7.a;
        String string = getContext().getString(i3);
        ee7.a((Object) string, "context.getString(rawText)");
        String format = String.format(string, Arrays.copyOf(new Object[]{re5.a(this.g, 3)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final String getMLowLevelText() {
        int i2 = iz6.b[this.y.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? 2131887410 : 2131887394 : 2131887408;
        we7 we7 = we7.a;
        String string = getContext().getString(i3);
        ee7.a((Object) string, "context.getString(rawText)");
        String format = String.format(string, Arrays.copyOf(new Object[]{re5.a(this.f, 3)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    private final float getMMax() {
        float f2;
        T t2;
        Float f3;
        List<v87<Integer, Float, v87<Float, Float, Float>>> list = this.i;
        if (list != null) {
            Iterator<T> it = list.iterator();
            if (!it.hasNext()) {
                t2 = null;
            } else {
                T next = it.next();
                if (!it.hasNext()) {
                    t2 = next;
                } else {
                    float floatValue = ((Number) next.getSecond()).floatValue();
                    do {
                        T next2 = it.next();
                        float floatValue2 = ((Number) next2.getSecond()).floatValue();
                        if (Float.compare(floatValue, floatValue2) < 0) {
                            next = next2;
                            floatValue = floatValue2;
                        }
                    } while (it.hasNext());
                }
                t2 = next;
            }
            T t3 = t2;
            if (!(t3 == null || (f3 = (Float) t3.getSecond()) == null)) {
                f2 = f3.floatValue();
                return Math.max(Math.max(getGoalForEachBar(), f2), this.g);
            }
        }
        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        return Math.max(Math.max(getGoalForEachBar(), f2), this.g);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, float f2, float f3, float f4, float f5, v87<Integer, Float, v87<Float, Float, Float>> v87, gb5 gb5, boolean z2, gd7<? super RectF, i97> gd7) {
        Paint paint;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k0;
        local.d(str, "Maximum: " + f5 + ", value: " + v87);
        float f6 = f2 / ((float) 2);
        float f7 = (float) 0;
        if (v87.getSecond().floatValue() <= f7) {
            gd7.invoke(new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            return;
        }
        float floatValue = v87.getSecond().floatValue() / f5;
        if (floatValue > 1.0f) {
            floatValue = 1.0f;
        }
        float f8 = (float) this.N;
        float f9 = (float) i2;
        float max = Math.max(f9 * floatValue, f6);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = k0;
        local2.d(str2, "Percentage: " + floatValue + ", chart height: " + i2 + ", real bar height: " + max);
        float max2 = Math.max((f9 - max) + f8, f8 + ((float) (z2 ? getStartBitmap().getHeight() * 2 : 0)));
        float round = (float) Math.round(f3);
        float round2 = (float) Math.round(f4);
        RectF rectF = new RectF(round, max2, round2, f9);
        v87<Float, Float, Float> third = v87.getThird();
        if (third == null) {
            if (iz6.e[gb5.ordinal()] != 1) {
                paint = this.H;
            } else {
                paint = this.I;
            }
            tw6.c(canvas, rectF, paint, f6);
        } else {
            tw6.c(canvas, rectF, this.E, f6);
            float floatValue2 = (third.getThird().floatValue() / v87.getSecond().floatValue()) * max;
            float floatValue3 = ((third.getThird().floatValue() + third.getSecond().floatValue()) / v87.getSecond().floatValue()) * max;
            if (floatValue3 > f7) {
                tw6.c(canvas, new RectF(round, f9 - floatValue3, round2, f9), this.F, f6);
            }
            if (floatValue2 > f7) {
                tw6.c(canvas, new RectF(round, f9 - floatValue2, round2, f9), this.G, f6);
            }
        }
        gd7.invoke(rectF);
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        Paint paint = this.B;
        String str = this.K;
        this.S = paint.measureText(str, 0, nh7.c((CharSequence) str));
        this.T = ((float) getHeight()) - ((float) this.J);
        this.V = ((float) (getWidth() / 2)) - (this.S / ((float) 2));
        this.U = (float) this.J;
        this.W = (((float) getWidth()) - this.S) - ((float) (this.J * 3));
        Rect rect = new Rect();
        Paint paint2 = this.B;
        String str2 = this.K;
        paint2.getTextBounds(str2, 0, nh7.c((CharSequence) str2), rect);
        this.N = rect.height();
        this.a0 = (getHeight() - rect.height()) - (this.J * 2);
        Rect rect2 = new Rect();
        Rect rect3 = new Rect();
        int i2 = 1;
        this.B.getTextBounds(getMHighLevelText(), 0, nh7.c(getMHighLevelText()) > 0 ? nh7.c((CharSequence) getMHighLevelText()) : 1, rect2);
        Paint paint3 = this.B;
        String mLowLevelText = getMLowLevelText();
        if (nh7.c((CharSequence) getMLowLevelText()) > 0) {
            i2 = nh7.c((CharSequence) getMLowLevelText());
        }
        paint3.getTextBounds(mLowLevelText, 0, i2, rect3);
        this.b0 = (((float) getWidth()) - Math.max(this.B.measureText(getMHighLevelText()), this.B.measureText(getMLowLevelText()))) - ((float) (this.J * 3));
        this.j0 = 0;
        int i3 = this.a0;
        this.i0 = i3 - 4;
        this.f0 = Math.max(((float) i3) * this.d, (float) this.N);
        float max = Math.max(((float) this.a0) * this.e, (float) this.N);
        this.c0 = max;
        if (max < this.f0 + ((float) rect2.height())) {
            this.f0 = this.c0 - ((float) rect2.height());
        }
        float f2 = this.b0;
        int i4 = this.J;
        this.d0 = ((float) i4) + f2;
        this.g0 = f2 + ((float) i4);
        this.e0 = this.c0 + ((float) (rect3.height() / 2));
        this.h0 = this.f0 + ((float) (rect2.height() / 2));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Canvas canvas2;
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawText(this.K, this.U, this.T, this.B);
            canvas.drawText(this.L, this.V, this.T, this.B);
            canvas.drawText(this.K, this.W, this.T, this.B);
            List<v87<Integer, Float, v87<Float, Float, Float>>> list = this.i;
            if (list != null) {
                float f2 = this.b0 / ((float) 2);
                int i2 = this.O;
                float f3 = (float) (i2 / 2);
                float f4 = f2 - f3;
                float f5 = (f2 + ((float) i2)) - f3;
                qe7 qe7 = new qe7();
                qe7.element = -1;
                float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                int size = list.size();
                int i3 = 0;
                while (true) {
                    if (i3 >= size) {
                        break;
                    }
                    f6 += list.get(i3).getSecond().floatValue();
                    if (f6 >= this.h) {
                        FLogger.INSTANCE.getLocal().d(k0, "Goal reach at " + i3);
                        qe7.element = i3;
                        break;
                    }
                    i3++;
                }
                pe7 pe7 = new pe7();
                pe7.element = -1.0f;
                pe7 pe72 = new pe7();
                pe72.element = -1.0f;
                float mMax = getMMax();
                FLogger.INSTANCE.getLocal().d(k0, "Data max: " + getMMax() + ", goal for each bar: " + getGoalForEachBar() + ", chart max: " + mMax);
                int i4 = this.P;
                float f7 = f4;
                int i5 = 11;
                while (true) {
                    boolean z2 = true;
                    if (i5 < 0) {
                        break;
                    }
                    v87<Integer, Float, v87<Float, Float, Float>> v87 = list.get(i5);
                    int i6 = this.a0;
                    float f8 = (float) i4;
                    float f9 = f7 - f8;
                    gb5 gb5 = this.y;
                    if (i5 != qe7.element) {
                        z2 = false;
                    }
                    a(canvas, i6, f8, f9, f7, mMax, v87, gb5, z2, new b(i5, qe7, pe72, pe7));
                    f7 -= (float) (i4 + this.O);
                    i5--;
                    qe7 = qe7;
                    pe72 = pe72;
                    i4 = i4;
                    pe7 = pe7;
                    mMax = mMax;
                }
                pe7 pe73 = pe7;
                qe7 qe72 = qe7;
                int i7 = 12;
                for (int i8 = 24; i7 < i8; i8 = 24) {
                    float f10 = (float) i4;
                    a(canvas, this.a0, f10, f5, f5 + f10, mMax, list.get(i7), this.y, i7 == qe72.element, new c(i7, qe72, pe72, pe73));
                    f5 += (float) (i4 + this.O);
                    i7++;
                    pe73 = pe73;
                    qe72 = qe72;
                }
                float f11 = (float) 0;
                if (pe72.element < f11 || pe73.element < f11) {
                    canvas2 = canvas;
                } else {
                    int height = getStartBitmap().getHeight();
                    int width = getStartBitmap().getWidth();
                    float f12 = (float) (height + 10);
                    if (pe72.element >= f12) {
                        canvas2 = canvas;
                        canvas2.drawBitmap(getStartBitmap(), pe73.element - ((float) (width / 2)), (float) 10, this.C);
                        Path path = new Path();
                        path.moveTo(pe73.element, f12);
                        path.lineTo(pe73.element, pe72.element);
                        canvas2.drawPath(path, this.A);
                    } else {
                        canvas2 = canvas;
                        canvas2.drawBitmap(getStartBitmap(), pe73.element - ((float) (width / 2)), (float) 10, this.C);
                    }
                }
            } else {
                canvas2 = canvas;
            }
            canvas2.drawRect(new Rect(0, this.i0, getWidth(), this.a0), this.z);
            canvas2.drawRect(new Rect(0, this.j0, getWidth(), 4), this.z);
            FLogger.INSTANCE.getLocal().d(k0, "low level x: " + this.d0 + " y: " + this.e0 + ", high level x: " + this.g0 + " y: " + this.h0);
            float f13 = this.e0;
            if (f13 > ((float) this.j0) && f13 < ((float) this.i0)) {
                canvas2.drawText(getMLowLevelText(), this.d0, this.e0, this.B);
            }
            float f14 = this.h0;
            if (f14 > ((float) this.j0) && f14 < ((float) this.i0)) {
                canvas2.drawText(getMHighLevelText(), this.g0, this.h0, this.B);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 2131231081;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.Q;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.z.setColor(this.j);
        float f2 = (float) 4;
        this.z.setStrokeWidth(f2);
        this.D.setColor(this.q);
        this.D.setStrokeWidth((float) this.P);
        a(this.E, this.r, (float) this.P, Paint.Style.FILL);
        a(this.F, this.s, (float) this.P, Paint.Style.FILL);
        a(this.G, this.t, (float) this.P, Paint.Style.FILL);
        a(this.H, this.u, (float) this.P, Paint.Style.FILL);
        a(this.I, this.v, (float) this.P, Paint.Style.FILL);
        this.A.setColor(this.p);
        this.A.setStyle(Paint.Style.STROKE);
        this.A.setStrokeWidth(f2);
        this.A.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.B.setColor(this.w);
        this.B.setStyle(Paint.Style.FILL);
        this.B.setTextSize((float) this.x);
        this.B.setTypeface(this.R);
        this.C.setStyle(Paint.Style.FILL);
        this.C.setColorFilter(new PorterDuffColorFilter(this.M, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Paint paint, int i2, float f2, Paint.Style style) {
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(style);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }
}
