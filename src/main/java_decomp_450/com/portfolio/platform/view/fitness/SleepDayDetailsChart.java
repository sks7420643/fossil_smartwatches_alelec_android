package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.nh7;
import com.fossil.pl4;
import com.fossil.tw6;
import com.fossil.v6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public /* final */ ArrayList<SleepSessionData> C;
    @DexIgnore
    public b D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        All,
        Awake,
        Light,
        Restful
    }

    /*
    static {
        new a(null);
        String simpleName = SleepDayDetailsChart.class.getSimpleName();
        ee7.a((Object) simpleName, "SleepDayDetailsChart::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.j = v6.a(context, 2131100341);
        this.p = context.getResources().getDimensionPixelSize(2131165718);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = new Paint(1);
        this.y = context.getResources().getDimensionPixelSize(2131165406);
        this.z = context.getResources().getDimensionPixelSize(2131165419);
        this.A = context.getResources().getDimensionPixelSize(2131165372);
        this.B = context.getResources().getDimensionPixelSize(2131165384);
        this.C = new ArrayList<>();
        this.D = b.All;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, pl4.SleepDayDetailsChart, 0, 0));
        }
        int a2 = v6.a(context, 2131099831);
        int i3 = 2131296258;
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(4, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(2, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(5, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.i = mTypedArray6 != null ? mTypedArray6.getColor(1, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.q = mTypedArray7 != null ? mTypedArray7.getResourceId(6, 2131296258) : i3;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 2), this.x);
        canvas.drawRect(new Rect(0, i3 - 2, getWidth(), i3), this.x);
    }

    @DexIgnore
    public final void b(Canvas canvas, int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        local.d(str, "positionX: " + i2 + ", positionY: " + i3);
        canvas.drawBitmap(getStartBitmap(), (float) (i2 - (getStartBitmap().getWidth() / 2)), (float) this.z, this.u);
        Path path = new Path();
        float f2 = (float) i2;
        path.moveTo(f2, (float) (this.z + getStartBitmap().getHeight()));
        path.lineTo(f2, (float) i3);
        canvas.drawPath(path, this.w);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        boolean z2;
        int i5;
        int i6;
        Canvas canvas2;
        RectF rectF;
        SleepDayDetailsChart sleepDayDetailsChart;
        int i7;
        SleepDayDetailsChart sleepDayDetailsChart2 = this;
        Canvas canvas3 = canvas;
        super.draw(canvas);
        if (canvas3 != null) {
            canvas3.drawColor(-1);
            int i8 = Integer.MAX_VALUE;
            Iterator<SleepSessionData> it = sleepDayDetailsChart2.C.iterator();
            int i9 = 0;
            int i10 = 0;
            while (it.hasNext()) {
                SleepSessionData next = it.next();
                int component1 = next.component1();
                String component2 = next.component2();
                String component3 = next.component3();
                i10 += component1;
                Rect rect = new Rect();
                Rect rect2 = new Rect();
                sleepDayDetailsChart2.v.getTextBounds(component2, 0, nh7.c((CharSequence) component2), rect);
                sleepDayDetailsChart2.v.getTextBounds(component3, 0, nh7.c((CharSequence) component3), rect2);
                i8 = Math.min(rect2.height(), Math.min(rect.height(), i8));
                i9++;
            }
            int width = getWidth() - (sleepDayDetailsChart2.z * ((i9 > 0 ? i9 - 1 : 0) + 2));
            int height = (getHeight() - i8) - (sleepDayDetailsChart2.z * 2);
            if (height < 0) {
                height = getHeight() - (sleepDayDetailsChart2.z * 2);
            }
            int i11 = sleepDayDetailsChart2.z;
            int size = sleepDayDetailsChart2.C.size();
            int i12 = 0;
            int i13 = 0;
            boolean z3 = false;
            while (i12 < size) {
                SleepSessionData sleepSessionData = sleepDayDetailsChart2.C.get(i12);
                ee7.a((Object) sleepSessionData, "mListSleepSession[sleepDataId]");
                SleepSessionData sleepSessionData2 = sleepSessionData;
                float measureText = sleepDayDetailsChart2.v.measureText(sleepSessionData2.getEndTimeString());
                int i14 = i8 / 2;
                canvas3.drawText(sleepSessionData2.getStartTimeString(), (float) i11, (float) (getHeight() - i14), sleepDayDetailsChart2.v);
                int durationInMinutes = sleepSessionData2.getDurationInMinutes();
                float f2 = (float) durationInMinutes;
                float f3 = f2 / ((float) i10);
                float f4 = ((float) width) * f3;
                FLogger.INSTANCE.getLocal().d(F, "sessionPercentage: " + f3 + ", realWidth: " + width + ", sessionWidth: " + f4);
                List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
                int size2 = sleepStates.size();
                int i15 = i11;
                int i16 = i13;
                int i17 = 0;
                while (i17 < size2) {
                    int i18 = sleepStates.get(i17).state;
                    if (i17 < sleepStates.size() - 1) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        z2 = z3;
                        String str = F;
                        i4 = i16;
                        StringBuilder sb = new StringBuilder();
                        sb.append("From ");
                        sb.append(sleepStates.get(i17).index);
                        sb.append(" to ");
                        int i19 = i17 + 1;
                        i3 = i2;
                        sb.append(sleepStates.get(i19).index);
                        sb.append(": stateType ");
                        sb.append(i18);
                        local.d(str, sb.toString());
                        i5 = ((int) sleepStates.get(i19).index) - ((int) sleepStates.get(i17).index);
                    } else {
                        i3 = i2;
                        i4 = i16;
                        z2 = z3;
                        FLogger.INSTANCE.getLocal().d(F, "From " + sleepStates.get(i17).index + " to " + durationInMinutes + ": stateType " + i18);
                        i5 = durationInMinutes - ((int) sleepStates.get(i17).index);
                    }
                    int i20 = (int) ((((float) i5) / f2) * f4);
                    if (i20 < 1) {
                        i20 = 1;
                    }
                    if (i18 == 0) {
                        sleepDayDetailsChart = this;
                        canvas2 = canvas;
                        i2 = i3;
                        i6 = durationInMinutes;
                        float f5 = (float) i2;
                        rectF = new RectF((float) i15, (float) ((int) (0.8f * f5)), (float) (i15 + i20), f5);
                        Paint paint = sleepDayDetailsChart.r;
                        b bVar = sleepDayDetailsChart.D;
                        if (bVar == b.All) {
                            if (i17 == 0) {
                                if (sleepStates.get(i17 + 1).state != 0) {
                                    tw6.a(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                                } else {
                                    tw6.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                                }
                            } else if (i17 != sleepStates.size() - 1) {
                                int i21 = i17 - 1;
                                if (sleepStates.get(i21).state != 0 && sleepStates.get(i17 + 1).state != 0) {
                                    canvas2.drawRect(rectF, paint);
                                } else if (sleepStates.get(i21).state != 0) {
                                    tw6.b(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                                } else if (sleepStates.get(i17 + 1).state != 0) {
                                    tw6.a(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                                } else {
                                    tw6.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                                }
                            } else if (sleepStates.get(i17 - 1).state != 0) {
                                tw6.b(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                            } else {
                                tw6.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                            }
                        } else if (bVar == b.Awake) {
                            tw6.c(canvas2, rectF, paint, (float) sleepDayDetailsChart.y);
                        }
                        i7 = i4 + i5;
                        if (i7 >= sleepDayDetailsChart.E || z2) {
                            z3 = z2;
                        } else {
                            sleepDayDetailsChart.b(canvas2, (int) (rectF.left + (rectF.width() / ((float) 2))), i2);
                            z3 = true;
                        }
                        i15 += i20;
                        i17++;
                        sleepDayDetailsChart2 = sleepDayDetailsChart;
                        i12 = i12;
                        width = width;
                        size2 = size2;
                        measureText = measureText;
                        sleepSessionData2 = sleepSessionData2;
                        durationInMinutes = i6;
                        i16 = i7;
                        canvas3 = canvas2;
                    } else if (i18 != 1) {
                        i2 = i3;
                        float f6 = (float) i2;
                        rectF = new RectF((float) i15, (float) ((int) (0.3f * f6)), (float) (i15 + i20), f6);
                        sleepDayDetailsChart = this;
                        b bVar2 = sleepDayDetailsChart.D;
                        if (bVar2 == b.All || bVar2 == b.Restful) {
                            canvas2 = canvas;
                            tw6.c(canvas2, rectF, sleepDayDetailsChart.t, (float) sleepDayDetailsChart.y);
                        } else {
                            canvas2 = canvas;
                        }
                        i6 = durationInMinutes;
                    } else {
                        sleepDayDetailsChart = this;
                        canvas2 = canvas;
                        i2 = i3;
                        Paint paint2 = sleepDayDetailsChart.s;
                        float f7 = (float) i2;
                        i6 = durationInMinutes;
                        RectF rectF2 = new RectF((float) i15, (float) ((int) (0.55f * f7)), (float) (i15 + i20), f7);
                        b bVar3 = sleepDayDetailsChart.D;
                        if (bVar3 == b.All) {
                            if (i17 == 0) {
                                if (sleepStates.get(i17 + 1).state != 0) {
                                    tw6.a(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                                } else {
                                    tw6.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                                }
                            } else if (i17 != sleepStates.size() - 1) {
                                int i22 = i17 - 1;
                                if (sleepStates.get(i22).state != 0 && sleepStates.get(i17 + 1).state != 0) {
                                    canvas2.drawRect(rectF2, paint2);
                                } else if (sleepStates.get(i22).state != 0) {
                                    tw6.b(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                                } else if (sleepStates.get(i17 + 1).state != 0) {
                                    tw6.a(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                                } else {
                                    tw6.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                                }
                            } else if (sleepStates.get(i17 - 1).state != 0) {
                                tw6.b(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                            } else {
                                tw6.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                            }
                        } else if (bVar3 == b.Light) {
                            tw6.c(canvas2, rectF2, paint2, (float) sleepDayDetailsChart.y);
                        }
                        rectF = rectF2;
                    }
                    i7 = i4 + i5;
                    if (i7 >= sleepDayDetailsChart.E) {
                    }
                    z3 = z2;
                    i15 += i20;
                    i17++;
                    sleepDayDetailsChart2 = sleepDayDetailsChart;
                    i12 = i12;
                    width = width;
                    size2 = size2;
                    measureText = measureText;
                    sleepSessionData2 = sleepSessionData2;
                    durationInMinutes = i6;
                    i16 = i7;
                    canvas3 = canvas2;
                }
                canvas3.drawText(sleepSessionData2.getEndTimeString(), ((float) i15) - measureText, (float) (getHeight() - i14), sleepDayDetailsChart2.v);
                i11 = sleepDayDetailsChart2.z + i15;
                i12++;
                sleepDayDetailsChart2 = sleepDayDetailsChart2;
                canvas3 = canvas3;
                i8 = i8;
                i10 = i10;
                size = size;
                width = width;
                i13 = i16;
            }
            sleepDayDetailsChart2.a(canvas3, 0, i2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 2131231081;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.B;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.x.setColor(this.d);
        float f2 = (float) 2;
        this.x.setStrokeWidth(f2);
        this.r.setColor(this.g);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setStrokeWidth((float) this.A);
        this.s.setColor(this.f);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setStrokeWidth((float) this.A);
        this.t.setColor(this.e);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setStrokeWidth((float) this.A);
        this.v.setColor(this.h);
        this.v.setStyle(Paint.Style.FILL);
        this.v.setTextSize((float) this.p);
        this.v.setTypeface(c7.a(getContext(), this.q));
        this.w.setColor(this.i);
        this.w.setStyle(Paint.Style.STROKE);
        this.w.setStrokeWidth(f2);
        this.w.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.u.setStyle(Paint.Style.FILL);
        this.u.setColorFilter(new PorterDuffColorFilter(this.j, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }
}
