package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public int g;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public Typeface x;
    @DexIgnore
    public RectF y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        ee7.b(context, "context");
        String str = "";
        this.h = str;
        this.j = 5.0f;
        this.p = 8.0f;
        this.q = 8.0f;
        this.r = 1.0f;
        this.s = 8;
        this.t = -1;
        this.u = 10;
        this.w = true;
        this.y = new RectF();
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, pl4.ActivityHorizontalBar, 0, 0)) == null)) {
            this.g = obtainStyledAttributes.getColor(0, 0);
            this.i = obtainStyledAttributes.getColor(2, 0);
            this.j = (float) obtainStyledAttributes.getDimensionPixelSize(4, 5);
            this.p = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.q = (float) obtainStyledAttributes.getDimensionPixelSize(3, 8);
            this.r = obtainStyledAttributes.getFloat(6, 1.0f);
            this.t = obtainStyledAttributes.getResourceId(7, -1);
            this.s = obtainStyledAttributes.getDimensionPixelSize(8, 8);
            String string = obtainStyledAttributes.getString(9);
            this.h = string != null ? string : str;
            this.v = obtainStyledAttributes.getColor(10, 0);
            this.u = obtainStyledAttributes.getDimensionPixelSize(11, 10);
            try {
                this.x = c7.a(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        c();
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void c() {
        this.H.setAntiAlias(true);
        this.H.setStyle(Paint.Style.STROKE);
        this.D.setColor(this.i);
        this.D.setAntiAlias(true);
        this.D.setStyle(Paint.Style.FILL);
        this.F.setAlpha((int) (this.r * ((float) 255)));
        this.F.setColorFilter(new PorterDuffColorFilter(this.i, PorterDuff.Mode.SRC_IN));
        this.F.setAntiAlias(true);
        this.G.setColor(-1);
        this.G.setAntiAlias(true);
        this.G.setStyle(Paint.Style.FILL);
        this.E.setColor(this.v);
        this.E.setAntiAlias(true);
        this.E.setStyle(Paint.Style.FILL);
        this.E.setTextSize((float) this.u);
        Typeface typeface = this.x;
        if (typeface != null) {
            this.E.setTypeface(typeface);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return this.t;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.s;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityHorizontalBar", "onDraw - mValue=" + this.e + ", mGoal=" + this.f + ", mMax=" + this.d);
        this.z = ((float) getWidth()) - this.q;
        if (canvas != null) {
            canvas.drawColor(this.g);
            RectF rectF = this.y;
            float f2 = this.d;
            float f3 = (float) 0;
            float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f5 = f2 > f3 ? (this.z * this.e) / f2 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f6 = this.p;
            float f7 = (float) 2;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5 + (f6 * f7), f6 * ((float) 3));
            float f8 = this.e;
            if (f8 > f3) {
                if (f8 >= this.f) {
                    this.D.setAlpha((int) 15.299999999999999d);
                    RectF rectF2 = this.y;
                    float f9 = (float) 4;
                    float f10 = this.j;
                    canvas.drawRoundRect(rectF2, f9 * f10, f9 * f10, this.D);
                    this.D.setAlpha((int) 40.800000000000004d);
                    RectF a2 = a(this.y, this.p / f7);
                    float f11 = this.j;
                    canvas.drawRoundRect(a2, f11 * 2.0f, f11 * 2.0f, this.D);
                }
                this.D.setAlpha(255);
                RectF a3 = a(this.y, this.p);
                float f12 = this.j;
                canvas.drawRoundRect(a3, f12, f12, this.D);
            }
            float height = ((float) getHeight()) / 2.0f;
            float f13 = this.d;
            if (f13 > f3) {
                f4 = (this.z * this.f) / f13;
            }
            float f14 = this.p;
            float f15 = f4 + f14;
            this.A = f15;
            if (this.e <= this.f) {
                canvas.drawBitmap(getStartBitmap(), this.A - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.F);
            } else {
                canvas.drawCircle(f15 - (((float) this.s) / 2.0f), height, f14 / 4.0f, this.G);
            }
            if (this.w) {
                canvas.drawText(this.h, ((float) getWidth()) - this.B, height + (this.C / f7), this.E);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.p * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }
}
