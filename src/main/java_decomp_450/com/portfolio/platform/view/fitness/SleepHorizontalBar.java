package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.ee7;
import com.fossil.jz6;
import com.fossil.p87;
import com.fossil.pl4;
import com.fossil.r87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public int A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public Typeface C;
    @DexIgnore
    public RectF D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public int J;
    @DexIgnore
    public Paint K;
    @DexIgnore
    public /* final */ Paint L;
    @DexIgnore
    public /* final */ Paint M;
    @DexIgnore
    public /* final */ Paint N;
    @DexIgnore
    public /* final */ Paint O;
    @DexIgnore
    public /* final */ Paint P;
    @DexIgnore
    public ArrayList<LinkedList<r87<b, Integer>>> d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        AWAKE,
        SLEEP,
        DEEP
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        ee7.b(context, "context");
        this.d = new ArrayList<>();
        String str = "";
        this.i = str;
        this.s = 5.0f;
        this.t = 8.0f;
        this.u = 8.0f;
        this.v = 8.0f;
        this.w = 1.0f;
        this.x = 8;
        this.y = -1;
        this.z = 10;
        this.B = true;
        this.D = new RectF();
        this.I = 8.0f;
        this.L = new Paint(1);
        this.M = new Paint(1);
        this.N = new Paint(1);
        this.O = new Paint(1);
        this.P = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, pl4.SleepHorizontalBar, 0, 0)) == null)) {
            this.h = obtainStyledAttributes.getColor(0, 0);
            this.j = obtainStyledAttributes.getColor(3, 0);
            this.p = obtainStyledAttributes.getColor(2, 0);
            this.q = obtainStyledAttributes.getColor(7, 0);
            this.r = obtainStyledAttributes.getColor(4, 0);
            this.s = (float) obtainStyledAttributes.getDimensionPixelSize(6, 5);
            this.u = (float) obtainStyledAttributes.getDimensionPixelSize(8, 8);
            this.t = (float) obtainStyledAttributes.getDimensionPixelSize(9, 8);
            this.v = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.w = obtainStyledAttributes.getFloat(10, 1.0f);
            this.y = obtainStyledAttributes.getResourceId(11, -1);
            this.x = obtainStyledAttributes.getDimensionPixelSize(12, 8);
            String string = obtainStyledAttributes.getString(13);
            this.i = string != null ? string : str;
            this.A = obtainStyledAttributes.getColor(14, 0);
            this.z = obtainStyledAttributes.getDimensionPixelSize(15, 10);
            try {
                this.C = c7.a(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SleepHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        c();
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void c() {
        this.P.setAntiAlias(true);
        this.P.setStyle(Paint.Style.STROKE);
        this.L.setColor(this.j);
        this.L.setAntiAlias(true);
        this.L.setStyle(Paint.Style.FILL);
        this.K = new Paint(this.L);
        this.N.setAlpha((int) (this.w * ((float) 255)));
        this.N.setColorFilter(new PorterDuffColorFilter(this.j, PorterDuff.Mode.SRC_IN));
        this.N.setAntiAlias(true);
        this.O.setColor(-1);
        this.O.setAntiAlias(true);
        this.O.setStyle(Paint.Style.FILL);
        this.M.setColor(this.A);
        this.M.setAntiAlias(true);
        this.M.setStyle(Paint.Style.FILL);
        this.M.setTextSize((float) this.z);
        Typeface typeface = this.C;
        if (typeface != null) {
            this.M.setTypeface(typeface);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return this.y;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.x;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepHorizontalBar", "onDraw - mTotalValue=" + this.f + ", mGoal=" + this.g);
        this.E = ((float) getWidth()) - this.v;
        if (canvas != null) {
            canvas.drawColor(this.h);
            RectF rectF = this.D;
            float f2 = this.e;
            float f3 = (float) 0;
            float f4 = f2 > f3 ? (this.E * this.f) / f2 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f5 = this.t;
            float f6 = (float) 2;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4 + (f5 * f6), f5 * ((float) 3));
            float f7 = this.f;
            if (f7 > f3) {
                if (f7 >= this.g) {
                    Paint paint = this.K;
                    if (paint != null) {
                        paint.setAlpha((int) 15.299999999999999d);
                        RectF rectF2 = this.D;
                        float f8 = (float) 4;
                        float f9 = this.s;
                        float f10 = f8 * f9;
                        float f11 = f8 * f9;
                        Paint paint2 = this.K;
                        if (paint2 != null) {
                            canvas.drawRoundRect(rectF2, f10, f11, paint2);
                            Paint paint3 = this.K;
                            if (paint3 != null) {
                                paint3.setAlpha((int) 40.800000000000004d);
                                RectF a2 = a(this.D, this.t / f6);
                                float f12 = this.s;
                                float f13 = f12 * 2.0f;
                                float f14 = f12 * 2.0f;
                                Paint paint4 = this.K;
                                if (paint4 != null) {
                                    canvas.drawRoundRect(a2, f13, f14, paint4);
                                } else {
                                    ee7.d("mPaintProgressReach");
                                    throw null;
                                }
                            } else {
                                ee7.d("mPaintProgressReach");
                                throw null;
                            }
                        } else {
                            ee7.d("mPaintProgressReach");
                            throw null;
                        }
                    } else {
                        ee7.d("mPaintProgressReach");
                        throw null;
                    }
                }
                if (this.J != 0) {
                    RectF a3 = a(this.D, this.t);
                    float f15 = a3.right;
                    float f16 = a3.left;
                    float f17 = (f15 - f16) - this.I;
                    float f18 = a3.bottom - a3.top;
                    int i3 = this.J;
                    int i4 = 0;
                    while (i4 < i3) {
                        LinkedList<r87<b, Integer>> linkedList = this.d.get(i4);
                        ee7.a((Object) linkedList, "mValues[k]");
                        LinkedList<r87<b, Integer>> linkedList2 = linkedList;
                        Iterator<T> it = linkedList2.iterator();
                        int i5 = 0;
                        while (it.hasNext()) {
                            i5 += ((Number) it.next().getSecond()).intValue();
                        }
                        float f19 = (((float) i5) * f17) / this.f;
                        int i6 = 1;
                        if (f19 < ((float) 1)) {
                            f19 = this.t;
                        } else {
                            float f20 = this.E;
                            if (f19 > f20) {
                                f19 = f20;
                            }
                        }
                        r87<Bitmap, Canvas> a4 = a((int) f19, (int) f18);
                        Bitmap first = a4.getFirst();
                        Canvas second = a4.getSecond();
                        int size = linkedList2.size();
                        int i7 = 0;
                        float f21 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        while (i7 < size) {
                            int i8 = jz6.a[linkedList2.get(i7).getFirst().ordinal()];
                            if (i8 == i6) {
                                i2 = this.p;
                            } else if (i8 == 2) {
                                i2 = this.q;
                            } else if (i8 == 3) {
                                i2 = this.r;
                            } else {
                                throw new p87();
                            }
                            float floatValue = (linkedList2.get(i7).getSecond().floatValue() * f17) / this.f;
                            this.L.setColor(i2);
                            float f22 = f21 + floatValue;
                            second.drawRect(f21, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f22, f18, this.L);
                            i7++;
                            size = size;
                            f21 = f22;
                            i4 = i4;
                            i6 = 1;
                        }
                        Bitmap a5 = a(first, this.s);
                        canvas.drawBitmap(a5, f16, a3.top, this.L);
                        first.recycle();
                        a5.recycle();
                        f16 += this.u + f21;
                        i4++;
                    }
                }
            }
            float height = ((float) getHeight()) / 2.0f;
            float f23 = this.e;
            float f24 = f23 > f3 ? (this.E * this.g) / f23 : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f25 = this.t;
            float f26 = f24 + f25;
            this.F = f26;
            if (this.f <= this.g) {
                canvas.drawBitmap(getStartBitmap(), this.F - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.N);
            } else {
                canvas.drawCircle(f26 - (((float) this.x) / 2.0f), height, f25 / 4.0f, this.O);
            }
            if (this.B) {
                canvas.drawText(this.i, (((float) getWidth()) - this.G) - f6, height + (this.H / f6), this.M);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.t * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    @Keep
    public final void setMax(float f2) {
        this.e = f2;
        invalidate();
    }

    @DexIgnore
    public final r87<Bitmap, Canvas> a(int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        return new r87<>(createBitmap, new Canvas(createBitmap));
    }

    @DexIgnore
    public final Bitmap a(Bitmap bitmap, float f2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        Paint paint = new Paint(1);
        canvas.drawRoundRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bitmap.getWidth(), (float) bitmap.getHeight(), f2, f2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        canvas.drawBitmap(bitmap, rect, rect, paint);
        ee7.a((Object) createBitmap, "output");
        return createBitmap;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context) {
        this(context, null, 0);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        ee7.b(context, "context");
    }
}
