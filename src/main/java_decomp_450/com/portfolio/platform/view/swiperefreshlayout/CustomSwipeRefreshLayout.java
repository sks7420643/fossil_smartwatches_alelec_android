package com.portfolio.platform.view.swiperefreshlayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import com.facebook.internal.NativeProtocol;
import com.fossil.ee7;
import com.fossil.vz6;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomSwipeRefreshLayout extends ViewGroup {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public /* final */ Runnable A;
    @DexIgnore
    public /* final */ k B;
    @DexIgnore
    public /* final */ Runnable C;
    @DexIgnore
    public /* final */ Runnable D;
    @DexIgnore
    public /* final */ g E;
    @DexIgnore
    public DecelerateInterpolator a;
    @DexIgnore
    public /* final */ f b;
    @DexIgnore
    public e c;
    @DexIgnore
    public e d;
    @DexIgnore
    public View e;
    @DexIgnore
    public View f;
    @DexIgnore
    public MotionEvent g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public d x;
    @DexIgnore
    public /* final */ j y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animation.AnimationListener {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
            ee7.b(animation, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            ee7.b(animation, "animation");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(e eVar, e eVar2);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(boolean z);

        @DexIgnore
        void b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public int a;
        @DexIgnore
        public float b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }
        }

        /*
        static {
            new a(null);
        }
        */

        @DexIgnore
        public e(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public String toString() {
            return "[refreshState = " + this.a + ", percent = " + this.b + ", top = " + this.c + ", trigger = " + this.d + "]";
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            this.a = i;
            this.c = i2;
            this.d = i3;
            this.b = ((float) i2) / ((float) i3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends Animation {
        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            ee7.b(transformation, "t");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends Animation {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            ee7.b(transformation, "t");
            int mTargetOriginalTop$app_fossilRelease = this.a.getMTargetOriginalTop$app_fossilRelease();
            if (this.a.getMFrom$app_fossilRelease() != this.a.getMTargetOriginalTop$app_fossilRelease()) {
                mTargetOriginalTop$app_fossilRelease = this.a.getMFrom$app_fossilRelease() + ((int) (((float) (this.a.getMTargetOriginalTop$app_fossilRelease() - this.a.getMFrom$app_fossilRelease())) * f));
            }
            View mTarget$app_fossilRelease = this.a.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                int top = mTargetOriginalTop$app_fossilRelease - mTarget$app_fossilRelease.getTop();
                View mTarget$app_fossilRelease2 = this.a.getMTarget$app_fossilRelease();
                if (mTarget$app_fossilRelease2 != null) {
                    int top2 = mTarget$app_fossilRelease2.getTop();
                    if (top + top2 < 0) {
                        top = 0 - top2;
                    }
                    this.a.a(top, true);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        public h(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.a.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.a(mTarget$app_fossilRelease.getTop(), this.a.y);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        public i(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.a.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.a(mTarget$app_fossilRelease.getTop(), this.a.y);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super();
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            ee7.b(animation, "animation");
            this.b.setMInReturningAnimation$app_fossilRelease(false);
            this.b.setStartSwipe$app_fossilRelease(true);
            if (this.b.getLastState$app_fossilRelease().a() == 2) {
                d mListener$app_fossilRelease = this.b.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.a(true);
                    return;
                }
                return;
            }
            d mListener$app_fossilRelease2 = this.b.getMListener$app_fossilRelease();
            if (mListener$app_fossilRelease2 != null) {
                mListener$app_fossilRelease2.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super();
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            ee7.b(animation, "animation");
            this.b.getMReturnToStartPosition$app_fossilRelease().run();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout a;

        @DexIgnore
        public l(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.a = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a;
            customSwipeRefreshLayout.a(customSwipeRefreshLayout.B);
        }
    }

    /*
    static {
        new b(null);
        String simpleName = CustomSwipeRefreshLayout.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomSwipeRefreshLayout::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.b = new f();
        this.c = new e(0);
        this.d = new e(-1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        ee7.a((Object) viewConfiguration, "ViewConfiguration.get(context)");
        this.h = viewConfiguration.getScaledTouchSlop();
        this.j = -1;
        this.s = true;
        this.t = true;
        this.y = new j(this);
        this.A = new i(this);
        this.B = new k(this);
        this.C = new l(this);
        this.D = new h(this);
        this.E = new g(this);
        setWillNotDraw(false);
        this.a = new DecelerateInterpolator(2.0f);
        new AccelerateInterpolator(1.5f);
        d();
    }

    @DexIgnore
    private final View getContentView() {
        String str;
        View view;
        if (getChildAt(0) == this.e) {
            view = getChildAt(1);
            str = "getChildAt(1)";
        } else {
            view = getChildAt(0);
            str = "getChildAt(0)";
        }
        ee7.a((Object) view, str);
        return view;
    }

    @DexIgnore
    private final void setRefreshState(int i2) {
        this.c.a(i2, this.r, this.j);
        View view = this.e;
        if (view != null) {
            ((c) view).a(this.c, this.d);
            this.d.a(i2, this.r, this.j);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.CustomSwipeRefreshHeadLayout");
    }

    @DexIgnore
    private final void setRefreshing(boolean z2) {
        if (this.u != z2) {
            a();
            this.u = z2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "isRefreshing - mRefreshing: " + this.u);
            if (this.u) {
                this.C.run();
                return;
            }
            setRefreshState(3);
            removeCallbacks(this.A);
            removeCallbacks(this.D);
            this.C.run();
        }
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ee7.b(view, "child");
        ee7.b(layoutParams, NativeProtocol.WEB_DIALOG_PARAMS);
        if (getChildCount() <= 1 || isInEditMode()) {
            super.addView(view, i2, layoutParams);
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one child content view");
    }

    @DexIgnore
    public final void c() {
        setRefreshing(false);
    }

    @DexIgnore
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ee7.b(layoutParams, "p");
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    @DexIgnore
    public final void d() {
        Context context = getContext();
        ee7.a((Object) context, "context");
        vz6 vz6 = new vz6(context);
        this.e = vz6;
        addView(vz6, new ViewGroup.MarginLayoutParams(-1, -2));
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        ee7.b(motionEvent, Constants.EVENT);
        boolean dispatchTouchEvent = super.dispatchTouchEvent(motionEvent);
        if (motionEvent.getAction() == 0) {
            return true;
        }
        return dispatchTouchEvent;
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d(F, "startRefresh");
        setRefreshing(true);
        setRefreshState(2);
        d dVar = this.x;
        if (dVar == null) {
            return;
        }
        if (dVar != null) {
            dVar.a();
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void f() {
        removeCallbacks(this.D);
        postDelayed(this.D, 100);
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(F, "updateRefreshingUI");
        setRefreshState(2);
        setRefreshing(true);
    }

    @DexIgnore
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -1);
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ee7.b(layoutParams, "p");
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    @DexIgnore
    public final boolean getByPass() {
        return this.v;
    }

    @DexIgnore
    public final boolean getDisableSwipe() {
        return this.w;
    }

    @DexIgnore
    public final View getHeadView() {
        return this.e;
    }

    @DexIgnore
    public final e getLastState$app_fossilRelease() {
        return this.d;
    }

    @DexIgnore
    public final int getMFrom$app_fossilRelease() {
        return this.q;
    }

    @DexIgnore
    public final boolean getMInReturningAnimation$app_fossilRelease() {
        return this.z;
    }

    @DexIgnore
    public final d getMListener$app_fossilRelease() {
        return this.x;
    }

    @DexIgnore
    public final Runnable getMReturnToStartPosition$app_fossilRelease() {
        return this.A;
    }

    @DexIgnore
    public final View getMTarget$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final int getMTargetOriginalTop$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks(this.D);
        removeCallbacks(this.A);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.A);
        removeCallbacks(this.D);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            java.lang.String r0 = "ev"
            com.fossil.ee7.b(r6, r0)
            boolean r0 = r5.w
            r1 = 0
            if (r0 == 0) goto L_0x000b
            return r1
        L_0x000b:
            r5.a()
            float r0 = r6.getY()
            boolean r2 = r5.isEnabled()
            if (r2 != 0) goto L_0x0019
            return r1
        L_0x0019:
            int r2 = r5.r
            if (r2 != 0) goto L_0x001f
            r5.z = r1
        L_0x001f:
            int r2 = r6.getAction()
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x004b
            if (r2 == r3) goto L_0x002d
            r3 = 2
            if (r2 == r3) goto L_0x002d
            goto L_0x005b
        L_0x002d:
            android.view.MotionEvent r2 = r5.g
            if (r2 == 0) goto L_0x005b
            if (r2 == 0) goto L_0x0047
            float r2 = r2.getY()
            float r2 = r0 - r2
            float r2 = java.lang.Math.abs(r2)
            int r3 = r5.h
            float r3 = (float) r3
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x005b
            r5.p = r0
            return r1
        L_0x0047:
            com.fossil.ee7.a()
            throw r4
        L_0x004b:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r6)
            r5.g = r0
            if (r0 == 0) goto L_0x0088
            float r0 = r0.getY()
            r5.p = r0
            r5.t = r3
        L_0x005b:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r6)
            boolean r2 = r5.z
            if (r2 != 0) goto L_0x007b
            android.view.View r2 = r5.f
            if (r2 == 0) goto L_0x0077
            java.lang.String r3 = "event"
            com.fossil.ee7.a(r0, r3)
            boolean r0 = r5.b(r2, r0)
            if (r0 != 0) goto L_0x007b
            boolean r1 = r5.onTouchEvent(r6)
            goto L_0x0081
        L_0x0077:
            com.fossil.ee7.a()
            throw r4
        L_0x007b:
            float r0 = r6.getY()
            r5.p = r0
        L_0x0081:
            if (r1 != 0) goto L_0x0087
            boolean r1 = super.onInterceptTouchEvent(r6)
        L_0x0087:
            return r1
        L_0x0088:
            com.fossil.ee7.a()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (getChildCount() != 0) {
            View view = this.e;
            if (view != null) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    int paddingLeft = getPaddingLeft() + marginLayoutParams.leftMargin;
                    int paddingTop = getPaddingTop() + marginLayoutParams.topMargin;
                    View view2 = this.e;
                    if (view2 != null) {
                        int measuredWidth = view2.getMeasuredWidth() + paddingLeft;
                        View view3 = this.e;
                        if (view3 != null) {
                            int measuredHeight = view3.getMeasuredHeight() + paddingTop;
                            View view4 = this.e;
                            if (view4 != null) {
                                view4.layout(paddingLeft, paddingTop, measuredWidth, measuredHeight);
                            }
                            View contentView = getContentView();
                            ViewGroup.LayoutParams layoutParams2 = contentView.getLayoutParams();
                            if (layoutParams2 != null) {
                                ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) layoutParams2;
                                int paddingLeft2 = getPaddingLeft() + marginLayoutParams2.leftMargin;
                                int paddingTop2 = this.r + getPaddingTop() + marginLayoutParams2.topMargin;
                                contentView.layout(paddingLeft2, paddingTop2, contentView.getMeasuredWidth() + paddingLeft2, contentView.getMeasuredHeight() + paddingTop2);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (getChildCount() <= 2 || isInEditMode()) {
            measureChildWithMargins(this.e, i2, 0, i3, 0);
            View contentView = getContentView();
            if (getChildCount() > 0) {
                ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    contentView.measure(View.MeasureSpec.makeMeasureSpec((((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom()) - marginLayoutParams.topMargin) - marginLayoutParams.bottomMargin, 1073741824));
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host one child content view.");
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2;
        ee7.b(motionEvent, Constants.EVENT);
        boolean z2 = false;
        if (!isEnabled() || this.w) {
            return false;
        }
        int action = motionEvent.getAction();
        View view = this.f;
        if (view != null) {
            int top = view.getTop();
            this.r = top - this.i;
            if (action != 1) {
                if (action != 2) {
                    if (action != 3 || (motionEvent2 = this.g) == null) {
                        return false;
                    }
                    if (motionEvent2 != null) {
                        motionEvent2.recycle();
                        this.g = null;
                        return false;
                    }
                    ee7.a();
                    throw null;
                } else if (this.g == null || this.z) {
                    return false;
                } else {
                    float y2 = motionEvent.getY();
                    MotionEvent motionEvent3 = this.g;
                    if (motionEvent3 != null) {
                        float y3 = y2 - motionEvent3.getY();
                        boolean z3 = y2 - this.p > ((float) 0);
                        if (this.t) {
                            int i2 = this.h;
                            if (y3 > ((float) i2) || y3 < ((float) (-i2))) {
                                this.t = false;
                            }
                        }
                        if (z3 || top >= this.j || top >= this.i + 1) {
                            if (this.s) {
                                this.s = false;
                                d dVar = this.x;
                                if (dVar != null) {
                                    dVar.b();
                                }
                            }
                            int i3 = (int) ((y2 - this.p) * 0.3f);
                            if (top < this.i || b()) {
                                z2 = true;
                            }
                            a(i3, z2);
                            this.p = motionEvent.getY();
                        } else {
                            this.p = motionEvent.getY();
                            return false;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            } else if (this.u && this.d.a() == 2) {
                removeCallbacks(this.D);
                this.C.run();
                return false;
            } else if (this.r < this.j || this.v) {
                f();
            } else {
                e();
            }
            return true;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void requestDisallowInterceptTouchEvent(boolean z2) {
    }

    @DexIgnore
    public final void setByPass(boolean z2) {
        this.v = z2;
    }

    @DexIgnore
    public final void setDisableSwipe(boolean z2) {
        this.w = z2;
    }

    @DexIgnore
    public final void setLastState$app_fossilRelease(e eVar) {
        ee7.b(eVar, "<set-?>");
        this.d = eVar;
    }

    @DexIgnore
    public final void setMFrom$app_fossilRelease(int i2) {
        this.q = i2;
    }

    @DexIgnore
    public final void setMInReturningAnimation$app_fossilRelease(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(d dVar) {
        this.x = dVar;
    }

    @DexIgnore
    public final void setMTarget$app_fossilRelease(View view) {
        this.f = view;
    }

    @DexIgnore
    public final void setMTargetOriginalTop$app_fossilRelease(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void setOnRefreshListener(d dVar) {
        ee7.b(dVar, "listener");
        this.x = dVar;
    }

    @DexIgnore
    public final void setStartSwipe$app_fossilRelease(boolean z2) {
        this.s = z2;
    }

    @DexIgnore
    public final boolean b() {
        return this.u;
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        ee7.b(attributeSet, "attrs");
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    @DexIgnore
    @SuppressLint({"ObsoleteSdkInt"})
    public final boolean b(View view, MotionEvent motionEvent) {
        motionEvent.offsetLocation((float) (view.getScrollX() - view.getLeft()), (float) (view.getScrollY() - view.getTop()));
        return view.canScrollVertically(-1) || a(view, motionEvent);
    }

    @DexIgnore
    public final void a(Animation.AnimationListener animationListener) {
        this.b.reset();
        this.b.setDuration(50);
        this.b.setAnimationListener(animationListener);
        View view = this.f;
        if (view != null) {
            view.startAnimation(this.b);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void b(int i2, boolean z2) {
        int i3;
        View view = this.f;
        if (view == null) {
            i3 = 0;
        } else if (view != null) {
            i3 = view.getTop();
        } else {
            ee7.a();
            throw null;
        }
        int i4 = this.i;
        if (i2 < i4) {
            i2 = i4;
        }
        a(i2 - i3, z2);
    }

    @DexIgnore
    public final void a(int i2, Animation.AnimationListener animationListener) {
        this.q = i2;
        this.E.reset();
        this.E.setDuration(500);
        this.E.setAnimationListener(animationListener);
        this.E.setInterpolator(this.a);
        View view = this.f;
        if (view != null) {
            view.startAnimation(this.E);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(View view, MotionEvent motionEvent) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                Rect rect = new Rect();
                childAt.getHitRect(rect);
                if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    ee7.a((Object) childAt, "child");
                    return b(childAt, motionEvent);
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2, int i3, zd7 zd7) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (z2) {
            setRefreshState(this.c.a());
            return;
        }
        View view = this.f;
        if (view == null) {
            ee7.a();
            throw null;
        } else if (view.getTop() > this.j) {
            setRefreshState(1);
        } else {
            setRefreshState(0);
        }
    }

    @DexIgnore
    public final void a() {
        if (this.f == null) {
            if (getChildCount() <= 2 || isInEditMode()) {
                View contentView = getContentView();
                this.f = contentView;
                if (contentView != null) {
                    this.i = contentView.getTop();
                    View view = this.f;
                    if (view != null) {
                        view.getHeight();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one direct child");
            }
        }
        if (this.j <= 0) {
            View view2 = this.e;
            this.j = view2 != null ? view2.getHeight() : SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS;
        }
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 != 0) {
            View headView = getHeadView();
            if (headView != null) {
                int i3 = this.r;
                if (i3 + i2 < 0) {
                    b(this.i, z2);
                } else if (i3 <= headView.getHeight() || i2 < 0) {
                    View view = this.f;
                    if (view != null) {
                        view.offsetTopAndBottom(i2);
                        this.r += i2;
                        invalidate();
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            a(z2);
        }
    }
}
