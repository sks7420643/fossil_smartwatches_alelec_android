package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.gb5;
import com.fossil.hy6;
import com.fossil.w97;
import com.fossil.x87;
import com.fossil.xe5;
import com.fossil.zd7;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomEditGoalView extends ConstraintLayout {
    @DexIgnore
    public static /* final */ ArrayList<String> B; // = w97.a((Object[]) new String[]{"primaryText", "goalNumber", "goalNumberDescription"});
    @DexIgnore
    public static /* final */ ArrayList<String> C; // = w97.a((Object[]) new String[]{"nonBrandDisableCalendarDay", "goalNumber", "goalNumberDescription"});
    @DexIgnore
    public gb5 A;
    @DexIgnore
    public AppCompatImageView v;
    @DexIgnore
    public AutoResizeTextView w;
    @DexIgnore
    public FlexibleTextView x;
    @DexIgnore
    public String y; // = "";
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public CustomEditGoalView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(2131558832, (ViewGroup) this, true) : view;
        if (view != null) {
            View findViewById = view.findViewById(2131361853);
            ee7.a((Object) findViewById, "view.findViewById(R.id.aciv_icon)");
            this.v = (AppCompatImageView) findViewById;
            View findViewById2 = view.findViewById(2131363350);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_value)");
            this.w = (AutoResizeTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362416);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.ftv_goal_type)");
            this.x = (FlexibleTextView) findViewById3;
            b("custom_edit_goal_disable");
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4, String str5) {
        String b = eh5.l.a().b(str3);
        Typeface c = eh5.l.a().c(str5);
        Typeface c2 = eh5.l.a().c(str4);
        String b2 = eh5.l.a().b(str2);
        if (b != null) {
            this.x.setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            this.x.setTypeface(c);
        }
        if (c2 != null) {
            this.w.setTypeface(c2);
        }
        if (b2 != null) {
            this.w.setTextColor(Color.parseColor(b2));
        }
        String b3 = eh5.l.a().b(str);
        if (b3 != null) {
            this.v.setBackground(c(Color.parseColor(b3)));
        }
    }

    @DexIgnore
    public final void b(String str) {
        int hashCode = str.hashCode();
        if (hashCode != -836633469) {
            if (hashCode == 1806838920 && str.equals("custom_edit_goal_enable")) {
                String str2 = this.y;
                String str3 = B.get(0);
                ee7.a((Object) str3, "STATE_ENABLED_STYLES[0]");
                String str4 = str3;
                String str5 = B.get(1);
                ee7.a((Object) str5, "STATE_ENABLED_STYLES[1]");
                String str6 = str5;
                String str7 = B.get(2);
                ee7.a((Object) str7, "STATE_ENABLED_STYLES[2]");
                a(str2, str2, str4, str6, str7);
            }
        } else if (str.equals("custom_edit_goal_disable")) {
            String str8 = C.get(0);
            ee7.a((Object) str8, "STATE_DISABLED_STYLES[0]");
            String str9 = str8;
            String str10 = C.get(1);
            ee7.a((Object) str10, "STATE_DISABLED_STYLES[1]");
            String str11 = str10;
            String str12 = C.get(2);
            ee7.a((Object) str12, "STATE_DISABLED_STYLES[2]");
            a("nonBrandSurface", "nonBrandDisableCalendarDay", str9, str11, str12);
        }
    }

    @DexIgnore
    public final Drawable c(int i) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(i);
        return gradientDrawable;
    }

    @DexIgnore
    public final gb5 getMGoalType() {
        return this.A;
    }

    @DexIgnore
    public final int getMValue() {
        return this.z;
    }

    @DexIgnore
    public final int getValue() {
        return this.z;
    }

    @DexIgnore
    public final void setMGoalType(gb5 gb5) {
        this.A = gb5;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.z = i;
    }

    @DexIgnore
    public void setSelected(boolean z2) {
        if (z2) {
            b("custom_edit_goal_enable");
        } else {
            b("custom_edit_goal_disable");
        }
        super.setSelected(z2);
    }

    @DexIgnore
    public final void setType(gb5 gb5) {
        ee7.b(gb5, "type");
        this.A = gb5;
        int i = hy6.a[gb5.ordinal()];
        if (i == 1) {
            this.v.setImageDrawable(getContext().getDrawable(2131231061));
            this.y = "dianaActiveCaloriesTab";
            this.x.setText(getContext().getString(2131886584));
        } else if (i == 2) {
            this.v.setImageDrawable(getContext().getDrawable(2131231066));
            this.y = "dianaActiveMinutesTab";
            this.x.setText(getContext().getString(2131886592));
        } else if (i == 3) {
            this.v.setImageDrawable(getContext().getDrawable(2131231065));
            this.y = "hybridStepsTab";
            this.x.setText(getContext().getString(2131887070));
        } else if (i == 4) {
            this.v.setImageDrawable(getContext().getDrawable(2131231174));
            this.y = "dianaSleepTab";
            this.x.setText(getContext().getString(2131886716));
        } else if (i == 5) {
            this.v.setImageDrawable(getContext().getDrawable(2131231080));
            this.y = "hybridGoalTrackingTab";
            this.x.setText(getContext().getString(2131886443));
        }
    }

    @DexIgnore
    public final void setValue(int i) {
        this.z = i;
        gb5 gb5 = this.A;
        if (gb5 != null) {
            int i2 = hy6.b[gb5.ordinal()];
            if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
                this.w.setText(xe5.b.c(this.z));
            } else if (i2 == 5) {
                this.w.setText(xe5.b.d(this.z));
            }
        }
    }
}
