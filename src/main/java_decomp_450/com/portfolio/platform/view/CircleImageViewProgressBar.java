package com.portfolio.platform.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.pl4;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CircleImageViewProgressBar extends AppCompatImageView {
    @DexIgnore
    public static /* final */ ImageView.ScaleType K; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public static /* final */ Bitmap.Config L; // = Bitmap.Config.ARGB_8888;
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public ValueAnimator C;
    @DexIgnore
    public ColorFilter D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public /* final */ Matrix f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public /* final */ Paint h;
    @DexIgnore
    public /* final */ Paint i;
    @DexIgnore
    public /* final */ Paint j;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public Bitmap v;
    @DexIgnore
    public BitmapShader w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            CircleImageViewProgressBar.this.B = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            CircleImageViewProgressBar.this.invalidate();
        }
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context) {
        super(context);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.q = -16777216;
        this.r = 0;
        this.s = 10;
        this.t = 0;
        this.u = -16776961;
        this.J = true;
        f();
    }

    @DexIgnore
    private int getBorderWidth() {
        return this.r;
    }

    @DexIgnore
    private int getProgressBorderWidth() {
        return this.s;
    }

    @DexIgnore
    public final Bitmap a(Drawable drawable) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(2, 2, L);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), L);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final void c() {
        this.g.setColorFilter(this.D);
    }

    @DexIgnore
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        int i2 = this.s;
        int i3 = this.r;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft + ((float) i3), (((float) i2) * 1.5f) + paddingTop + ((float) i3), ((paddingLeft + f2) - (((float) i2) * 1.5f)) - ((float) i3), ((paddingTop + f2) - (((float) i2) * 1.5f)) - ((float) i3));
    }

    @DexIgnore
    public final RectF e() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        int i2 = this.s;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft, (((float) i2) * 1.5f) + paddingTop, (paddingLeft + f2) - (((float) i2) * 1.5f), (paddingTop + f2) - (((float) i2) * 1.5f));
    }

    @DexIgnore
    public final void f() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, this.B);
        this.C = ofFloat;
        ofFloat.setDuration(800L);
        this.C.addUpdateListener(new a());
        super.setScaleType(K);
        this.E = true;
        if (this.F) {
            h();
            this.F = false;
        }
    }

    @DexIgnore
    public final void g() {
        if (this.I) {
            this.v = null;
        } else {
            this.v = a(getDrawable());
        }
        h();
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.D;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.t;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return K;
    }

    @DexIgnore
    public final void h() {
        int i2;
        if (!this.E) {
            this.F = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            if (this.v == null) {
                invalidate();
                return;
            }
            Bitmap bitmap = this.v;
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            this.w = new BitmapShader(bitmap, tileMode, tileMode);
            this.g.setAntiAlias(true);
            this.g.setShader(this.w);
            this.h.setStyle(Paint.Style.STROKE);
            this.h.setAntiAlias(true);
            this.h.setColor(this.q);
            this.h.setStrokeWidth((float) this.r);
            this.h.setStrokeCap(Paint.Cap.ROUND);
            this.i.setStyle(Paint.Style.STROKE);
            this.i.setAntiAlias(true);
            this.i.setColor(this.u);
            this.i.setStrokeWidth((float) this.s);
            this.i.setStrokeCap(Paint.Cap.ROUND);
            this.j.setStyle(Paint.Style.FILL);
            this.j.setAntiAlias(true);
            this.j.setColor(this.t);
            this.y = this.v.getHeight();
            this.x = this.v.getWidth();
            this.d.set(d());
            this.e.set(e());
            this.c.set(this.e);
            if (!this.G && (i2 = this.s) > 0) {
                this.c.inset((float) (i2 / 2), (float) (i2 / 2));
            }
            this.A = Math.min(this.c.height() / 2.0f, this.c.width() / 2.0f);
            if (this.z > 1.0f) {
                this.z = 1.0f;
            }
            this.A *= this.z;
            c();
            i();
            invalidate();
        }
    }

    @DexIgnore
    public final void i() {
        float f2;
        float f3;
        this.f.set(null);
        float height = ((float) this.x) * this.c.height();
        float width = this.c.width() * ((float) this.y);
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (height > width) {
            f3 = this.c.height() / ((float) this.y);
            f4 = (this.c.width() - (((float) this.x) * f3)) * 0.5f;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f3 = this.c.width() / ((float) this.x);
            f2 = (this.c.height() - (((float) this.y) * f3)) * 0.5f;
        }
        this.f.setScale(f3, f3);
        Matrix matrix = this.f;
        RectF rectF = this.c;
        matrix.postTranslate(((float) ((int) (f4 + 0.5f))) + rectF.left, ((float) ((int) (f2 + 0.5f))) + rectF.top);
        BitmapShader bitmapShader = this.w;
        if (bitmapShader != null) {
            bitmapShader.setLocalMatrix(this.f);
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (this.I) {
            super.onDraw(canvas);
        } else if (this.v != null) {
            canvas.save();
            canvas.rotate(this.p - 90.0f, this.c.centerX(), this.c.centerY());
            int i2 = this.r;
            if (i2 > 0 && i2 < 10) {
                this.h.setColor(this.q);
                canvas.drawArc(this.d, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 360.0f, false, this.h);
            }
            this.h.setColor(this.q);
            this.i.setColor(this.u);
            float f2 = (this.B / 100.0f) * 360.0f;
            RectF rectF = this.e;
            if (this.H) {
                f2 = -f2;
            }
            canvas.drawArc(rectF, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, false, this.i);
            canvas.restore();
            canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.A, this.g);
            if (this.t != 0) {
                canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.A, this.j);
            }
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z2) {
        if (z2) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        if (i2 != this.q) {
            this.q = i2;
            this.h.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderProgressColor(int i2) {
        if (i2 != this.u) {
            this.u = i2;
            this.i.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.r) {
            this.r = i2;
            h();
        }
    }

    @DexIgnore
    @Override // android.widget.ImageView
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.D)) {
            this.D = colorFilter;
            c();
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.t) {
            this.t = i2;
            this.j.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        g();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        g();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageResource(int i2) {
        super.setImageResource(i2);
        g();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.AppCompatImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        g();
    }

    @DexIgnore
    public void setInnrCircleDiammeter(float f2) {
        this.z = f2;
    }

    @DexIgnore
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    public void setProgressBorderWidth(int i2) {
        if (i2 != this.s) {
            this.s = i2;
            h();
        }
    }

    @DexIgnore
    public void setProgressValue(float f2) {
        if (this.J) {
            if (this.C.isRunning()) {
                this.C.cancel();
            }
            this.C.setFloatValues(this.B, f2);
            this.C.start();
            return;
        }
        this.B = f2;
        invalidate();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != K) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.q = -16777216;
        this.r = 0;
        this.s = 10;
        this.t = 0;
        this.u = -16776961;
        this.J = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CircleImageViewProgressBar, i2, 0);
        this.s = obtainStyledAttributes.getDimensionPixelSize(5, 0);
        this.r = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.q = obtainStyledAttributes.getColor(3, -16777216);
        this.G = obtainStyledAttributes.getBoolean(4, false);
        this.H = obtainStyledAttributes.getBoolean(1, false);
        this.t = obtainStyledAttributes.getColor(7, 0);
        this.z = obtainStyledAttributes.getFloat(0, 0.805f);
        this.u = obtainStyledAttributes.getColor(6, -16776961);
        this.p = obtainStyledAttributes.getFloat(8, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
        f();
    }
}
