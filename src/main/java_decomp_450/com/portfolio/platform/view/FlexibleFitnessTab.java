package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.p7;
import com.fossil.pl4;
import com.fossil.t0;
import com.fossil.x87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleFitnessTab extends ConstraintLayout {
    @DexIgnore
    public int A;
    @DexIgnore
    public String B; // = "";
    @DexIgnore
    public View v;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public FlexibleTextView x;
    @DexIgnore
    public FlexibleTextView y;
    @DexIgnore
    public String z; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    @Override // androidx.constraintlayout.widget.ConstraintLayout
    private final void a(AttributeSet attributeSet) {
        Context context = getContext();
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558836, (ViewGroup) this, true) : null;
        if (inflate != null) {
            this.v = inflate.findViewById(2131362096);
            this.w = (ImageView) inflate.findViewById(2131362725);
            this.x = (FlexibleTextView) inflate.findViewById(2131362508);
            this.y = (FlexibleTextView) inflate.findViewById(2131362507);
            if (attributeSet != null) {
                Context context2 = getContext();
                TypedArray obtainStyledAttributes = context2 != null ? context2.obtainStyledAttributes(attributeSet, pl4.FlexibleFitnessTab) : null;
                if (obtainStyledAttributes != null) {
                    String string = obtainStyledAttributes.getString(0);
                    if (string == null) {
                        string = "hybridInactiveTab";
                    }
                    ee7.a((Object) string, "styledAttrs.getString(R.\u2026d) ?: \"hybridInactiveTab\"");
                    String b = eh5.l.a().b(string);
                    if (b == null) {
                        b = "#ffffff";
                    }
                    this.z = b;
                    this.A = obtainStyledAttributes.getResourceId(1, 2131231065);
                    String string2 = obtainStyledAttributes.getString(2);
                    if (string2 == null) {
                        string2 = "";
                    }
                    this.B = string2;
                    ImageView imageView = this.w;
                    if (imageView != null) {
                        imageView.setImageResource(this.A);
                        FlexibleTextView flexibleTextView = this.y;
                        if (flexibleTextView != null) {
                            flexibleTextView.setText(this.B);
                            obtainStyledAttributes.recycle();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            View view = this.v;
            if (view != null) {
                view.setBackgroundResource(2131231173);
                d();
                return;
            }
            ee7.a();
            throw null;
        }
        throw new x87("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setTabBackgroundColor(int i) {
        Context context = getContext();
        if (context != null) {
            Drawable c = t0.c(context, 2131231173);
            if (c != null) {
                p7.b(p7.i(c), i);
                View view = this.v;
                if (view != null) {
                    Drawable background = view.getBackground();
                    if (background instanceof ShapeDrawable) {
                        Paint paint = ((ShapeDrawable) background).getPaint();
                        ee7.a((Object) paint, "drawable.paint");
                        paint.setColor(i);
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(i);
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(i);
                    } else if (background instanceof StateListDrawable) {
                        background.setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    } else if (background instanceof BitmapDrawable) {
                        background.mutate().setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "value");
        FlexibleTextView flexibleTextView = this.y;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "value");
        FlexibleTextView flexibleTextView = this.x;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public final void d(int i) {
        setTabBackgroundColor(i);
    }

    @DexIgnore
    public final void c(int i) {
        ImageView imageView = this.w;
        if (imageView != null) {
            imageView.setColorFilter(i);
        }
        FlexibleTextView flexibleTextView = this.y;
        if (flexibleTextView != null) {
            flexibleTextView.setTextColor(i);
        }
        FlexibleTextView flexibleTextView2 = this.x;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setTextColor(i);
        }
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.z)) {
            setTabBackgroundColor(Color.parseColor(this.z));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
