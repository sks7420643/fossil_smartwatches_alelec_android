package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.we5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLEditText extends FlexibleEditText {
    @DexIgnore
    public RTLEditText(Context context) {
        super(context);
        a(context);
    }

    @DexIgnore
    public final void a(Context context) {
        we5.a(this, context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }
}
