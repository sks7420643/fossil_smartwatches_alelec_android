package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.fossil.x87;
import com.google.android.material.tabs.TabLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTabLayout extends TabLayout {
    @DexIgnore
    public String U; // = "";
    @DexIgnore
    public String V; // = "";
    @DexIgnore
    public String W; // = "";
    @DexIgnore
    public String a0; // = "";
    @DexIgnore
    public String b0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.TabLayout);
            String string = obtainStyledAttributes.getString(25);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.U = string;
            String string2 = obtainStyledAttributes.getString(26);
            if (string2 == null) {
                string2 = str;
            }
            this.V = string2;
            String string3 = obtainStyledAttributes.getString(27);
            if (string3 == null) {
                string3 = str;
            }
            this.W = string3;
            String string4 = obtainStyledAttributes.getString(28);
            if (string4 == null) {
                string4 = str;
            }
            this.a0 = string4;
            String string5 = obtainStyledAttributes.getString(29);
            if (string5 != null) {
                str = string5;
            }
            this.b0 = str;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.U)) {
            this.U = "nonBrandSurface";
        }
        if (TextUtils.isEmpty(this.V)) {
            this.V = "primaryColor";
        }
        if (TextUtils.isEmpty(this.W)) {
            this.W = "primaryText";
        }
        if (TextUtils.isEmpty(this.a0)) {
            this.a0 = "secondaryText";
        }
        if (TextUtils.isEmpty(this.b0)) {
            this.b0 = "descriptionText1";
        }
        i();
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void i() {
        if (!TextUtils.isEmpty(this.U) && !TextUtils.isEmpty(this.V) && !TextUtils.isEmpty(this.W) && !TextUtils.isEmpty(this.a0)) {
            String b = eh5.l.a().b(this.U);
            String b2 = eh5.l.a().b(this.V);
            String b3 = eh5.l.a().b(this.W);
            String b4 = eh5.l.a().b(this.a0);
            if (!TextUtils.isEmpty(b2)) {
                setSelectedTabIndicatorColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b4) && !TextUtils.isEmpty(b3)) {
                a(Color.parseColor(b4), Color.parseColor(b3));
            }
            if (!TextUtils.isEmpty(b)) {
                setBackgroundColor(Color.parseColor(b));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    @Override // com.google.android.material.tabs.TabLayout
    public void a(TabLayout.g gVar) {
        Typeface c;
        ee7.b(gVar, "tab");
        super.a(gVar);
        if (!TextUtils.isEmpty(this.b0) && (c = eh5.l.a().c(this.b0)) != null) {
            View childAt = getChildAt(0);
            if (childAt != null) {
                View childAt2 = ((ViewGroup) childAt).getChildAt(gVar.c());
                if (childAt2 != null) {
                    ViewGroup viewGroup = (ViewGroup) childAt2;
                    int childCount = viewGroup.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View childAt3 = viewGroup.getChildAt(i);
                        ee7.a((Object) childAt3, "tabView.getChildAt(i)");
                        if (childAt3 instanceof TextView) {
                            ((TextView) childAt3).setTypeface(c, 0);
                        }
                    }
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
            }
            throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }
}
