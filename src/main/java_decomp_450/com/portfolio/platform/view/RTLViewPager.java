package com.portfolio.platform.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.n4;
import com.fossil.pl;
import com.fossil.w8;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLViewPager extends ViewPager {
    @DexIgnore
    public /* final */ Map<ViewPager.i, d> p0; // = new n4(1);
    @DexIgnore
    public DataSetObserver q0;
    @DexIgnore
    public boolean r0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pl {
        @DexIgnore
        public /* final */ pl b;

        @DexIgnore
        public a(pl plVar) {
            this.b = plVar;
        }

        @DexIgnore
        @Override // com.fossil.pl
        public int a() {
            return this.b.a();
        }

        @DexIgnore
        @Override // com.fossil.pl
        public float b(int i) {
            return this.b.b(i);
        }

        @DexIgnore
        public pl c() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.pl
        public boolean a(View view, Object obj) {
            return this.b.a(view, obj);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void b(ViewGroup viewGroup, int i, Object obj) {
            this.b.b(viewGroup, i, obj);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void c(DataSetObserver dataSetObserver) {
            this.b.c(dataSetObserver);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public CharSequence a(int i) {
            return this.b.a(i);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public Parcelable b() {
            return this.b.b();
        }

        @DexIgnore
        @Override // com.fossil.pl
        public int a(Object obj) {
            return this.b.a(obj);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void b(ViewGroup viewGroup) {
            this.b.b(viewGroup);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public Object a(ViewGroup viewGroup, int i) {
            return this.b.a(viewGroup, i);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void a(ViewGroup viewGroup, int i, Object obj) {
            this.b.a(viewGroup, i, obj);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void a(DataSetObserver dataSetObserver) {
            this.b.a(dataSetObserver);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void a(Parcelable parcelable, ClassLoader classLoader) {
            this.b.a(parcelable, classLoader);
        }

        @DexIgnore
        @Override // com.fossil.pl
        public void a(ViewGroup viewGroup) {
            this.b.a(viewGroup);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends DataSetObserver {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onChanged() {
            super.onChanged();
            this.a.d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends a {
        @DexIgnore
        public int c;

        @DexIgnore
        public c(pl plVar) {
            super(plVar);
            this.c = plVar.a();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public CharSequence a(int i) {
            return super.a(c(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public float b(int i) {
            return super.b(c(i));
        }

        @DexIgnore
        public final int c(int i) {
            return (a() - i) - 1;
        }

        @DexIgnore
        public void d() {
            int a = a();
            int i = this.c;
            if (a != i) {
                RTLViewPager.this.setCurrentItemWithoutNotification(Math.max(0, i - 1));
                this.c = a;
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public int a(Object obj) {
            int a = super.a(obj);
            return a < 0 ? a : c(a);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public void b(ViewGroup viewGroup, int i, Object obj) {
            super.b(viewGroup, (this.c - i) - 1, obj);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public Object a(ViewGroup viewGroup, int i) {
            return super.a(viewGroup, c(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.pl
        public void a(ViewGroup viewGroup, int i, Object obj) {
            super.a(viewGroup, c(i), obj);
        }
    }

    @DexIgnore
    public RTLViewPager(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(pl plVar) {
        if ((plVar instanceof c) && this.q0 == null) {
            c cVar = (c) plVar;
            b bVar = new b(cVar);
            this.q0 = bVar;
            plVar.a((DataSetObserver) bVar);
            cVar.d();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void b(float f) {
        if (!n()) {
            f = -f;
        }
        super.b(f);
    }

    @DexIgnore
    public final int g(int i) {
        if (i < 0 || !n()) {
            return i;
        }
        if (getAdapter() == null) {
            return 0;
        }
        return (getAdapter().a() - i) - 1;
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public pl getAdapter() {
        pl adapter = super.getAdapter();
        return adapter instanceof c ? ((c) adapter).c() : adapter;
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public int getCurrentItem() {
        return g(super.getCurrentItem());
    }

    @DexIgnore
    public final boolean n() {
        return w8.b(getContext().getResources().getConfiguration().locale) == 1;
    }

    @DexIgnore
    public final void o() {
        DataSetObserver dataSetObserver;
        pl adapter = super.getAdapter();
        if ((adapter instanceof c) && (dataSetObserver = this.q0) != null) {
            adapter.c(dataSetObserver);
            this.q0 = null;
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(super.getAdapter());
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onDetachedFromWindow() {
        o();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onRestoreInstanceState(Parcelable parcelable) {
        e eVar = (e) parcelable;
        super.onRestoreInstanceState(eVar.a);
        if (eVar.c != n()) {
            a(eVar.b, false);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public Parcelable onSaveInstanceState() {
        return new e(super.onSaveInstanceState(), getCurrentItem(), n());
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void setAdapter(pl plVar) {
        o();
        boolean z = plVar != null && n();
        if (z) {
            c cVar = new c(plVar);
            a(cVar);
            plVar = cVar;
        }
        super.setAdapter(plVar);
        if (z) {
            setCurrentItemWithoutNotification(0);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void setCurrentItem(int i) {
        super.setCurrentItem(g(i));
    }

    @DexIgnore
    public void setCurrentItemWithoutNotification(int i) {
        this.r0 = true;
        a(i, false);
        this.r0 = false;
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void b(ViewPager.i iVar) {
        if (n()) {
            iVar = this.p0.remove(iVar);
        }
        super.b(iVar);
    }

    @DexIgnore
    public RTLViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ViewPager.i {
        @DexIgnore
        public /* final */ ViewPager.i a;
        @DexIgnore
        public int b; // = -1;

        @DexIgnore
        public d(ViewPager.i iVar) {
            this.a = iVar;
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void a(int i, float f, int i2) {
            if (!RTLViewPager.this.r0) {
                int i3 = (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i3 == 0 && i2 == 0) {
                    this.b = c(i);
                } else {
                    this.b = c(i + 1);
                }
                ViewPager.i iVar = this.a;
                int i4 = this.b;
                if (i3 > 0) {
                    f = 1.0f - f;
                }
                iVar.a(i4, f, i2);
            }
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void b(int i) {
            if (!RTLViewPager.this.r0) {
                this.a.b(c(i));
            }
        }

        @DexIgnore
        public final int c(int i) {
            pl adapter = RTLViewPager.this.getAdapter();
            return adapter == null ? i : (adapter.a() - i) - 1;
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void a(int i) {
            if (!RTLViewPager.this.r0) {
                this.a.a(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.ClassLoaderCreator<e> CREATOR; // = new a();
        @DexIgnore
        public /* final */ Parcelable a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.ClassLoaderCreator<e> {
            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public e[] newArray(int i) {
                return new e[i];
            }

            @DexIgnore
            @Override // android.os.Parcelable.ClassLoaderCreator
            public e createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new e(parcel, classLoader);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public e createFromParcel(Parcel parcel) {
                return new e(parcel, null);
            }
        }

        @DexIgnore
        public e(Parcelable parcelable, int i, boolean z) {
            this.a = parcelable;
            this.b = i;
            this.c = z;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.a, i);
            parcel.writeInt(this.b);
            parcel.writeByte(this.c ? (byte) 1 : 0);
        }

        @DexIgnore
        public e(Parcel parcel, ClassLoader classLoader) {
            this.a = parcel.readParcelable(classLoader == null ? e.class.getClassLoader() : classLoader);
            this.b = parcel.readInt();
            this.c = parcel.readByte() != 0;
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void a(int i, boolean z) {
        super.a(g(i), z);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void a(ViewPager.i iVar) {
        if (n()) {
            d dVar = new d(iVar);
            this.p0.put(iVar, dVar);
            iVar = dVar;
        }
        super.a(iVar);
    }
}
