package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.fz6;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.r87;
import com.fossil.v6;
import com.fossil.v87;
import com.fossil.w97;
import com.fossil.x97;
import com.fossil.yx6;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TodayHeartRateChart extends BaseChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public String B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public short N;
    @DexIgnore
    public short O;
    @DexIgnore
    public Paint P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public List<fz6> T;
    @DexIgnore
    public float U;
    @DexIgnore
    public String V;
    @DexIgnore
    public List<v87<Integer, r87<Integer, Float>, String>> W; // = new ArrayList();
    @DexIgnore
    public int a0;
    @DexIgnore
    public String v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public String y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TodayHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        Typeface c2;
        String b;
        String b2;
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        String str = "";
        this.y = str;
        this.z = str;
        this.B = str;
        this.V = str;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.TodayHeartRateChart);
        String string = obtainStyledAttributes.getString(1);
        this.V = string == null ? "#ffffff" : string;
        String string2 = obtainStyledAttributes.getString(0);
        this.v = string2 == null ? str : string2;
        this.w = obtainStyledAttributes.getColor(5, v6.a(context, (int) R.color.steps));
        this.x = obtainStyledAttributes.getColor(4, v6.a(context, 2131099834));
        String string3 = obtainStyledAttributes.getString(6);
        this.y = string3 == null ? str : string3;
        String string4 = obtainStyledAttributes.getString(3);
        this.z = string4 == null ? str : string4;
        this.A = obtainStyledAttributes.getDimension(2, yx6.c(13.0f));
        String string5 = obtainStyledAttributes.getString(8);
        this.B = string5 != null ? string5 : str;
        this.C = obtainStyledAttributes.getDimension(7, yx6.c(13.0f));
        obtainStyledAttributes.recycle();
        this.P = new Paint();
        if (!TextUtils.isEmpty(this.v) && (b2 = eh5.l.a().b(this.v)) != null) {
            this.P.setColor(Color.parseColor(b2));
        }
        this.P.setStrokeWidth(2.0f);
        this.P.setStyle(Paint.Style.STROKE);
        this.Q = new Paint(1);
        if (!TextUtils.isEmpty(this.y) && (b = eh5.l.a().b(this.y)) != null) {
            this.Q.setColor(Color.parseColor(b));
        }
        this.Q.setStyle(Paint.Style.FILL);
        this.Q.setTextSize(this.A);
        if (!TextUtils.isEmpty(this.z) && (c2 = eh5.l.a().c(this.z)) != null) {
            this.Q.setTypeface(c2);
        }
        Paint paint = new Paint(1);
        this.R = paint;
        paint.setColor(this.Q.getColor());
        this.R.setStyle(Paint.Style.FILL);
        this.R.setTextSize(this.C);
        if (!TextUtils.isEmpty(this.B) && (c = eh5.l.a().c(this.B)) != null) {
            this.R.setTypeface(c);
        }
        Paint paint2 = new Paint(1);
        this.S = paint2;
        paint2.setStrokeWidth(yx6.a(2.0f));
        this.S.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.Q.getTextBounds("222", 0, 3, rect);
        this.K = (float) rect.width();
        this.L = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887176);
        this.R.getTextBounds(a2, 0, a2.length(), rect2);
        this.M = (float) rect2.height();
        this.T = new ArrayList();
        d();
        a();
        e();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, "maxHeartRate");
        ee7.b(str2, "lowestHeartRate");
        String b = eh5.l.a().b(str);
        String b2 = eh5.l.a().b(str2);
        if (b != null) {
            this.x = Color.parseColor(b);
        }
        if (b2 != null) {
            this.w = Color.parseColor(b2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.b(canvas);
        this.D = ((float) canvas.getHeight()) - (this.M * ((float) 2));
        this.E = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.F = (float) canvas.getWidth();
        this.G = this.C;
        float a2 = this.E + yx6.a(5.0f);
        this.H = a2;
        this.J = this.D - (this.C * ((float) 6));
        float f = this.F - (this.K * 2.0f);
        this.I = f;
        float f2 = f - a2;
        int i = this.a0;
        if (i == 0) {
            i = DateTimeConstants.MINUTES_PER_DAY;
        }
        this.U = f2 / ((float) i);
        f(canvas);
        e();
    }

    @DexIgnore
    public final void d() {
        Object obj;
        List<fz6> list = this.T;
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (next.d() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        Iterator it2 = arrayList.iterator();
        Object obj2 = null;
        if (!it2.hasNext()) {
            obj = null;
        } else {
            obj = it2.next();
            if (it2.hasNext()) {
                int d = ((fz6) obj).d();
                do {
                    Object next2 = it2.next();
                    int d2 = ((fz6) next2).d();
                    if (d > d2) {
                        obj = next2;
                        d = d2;
                    }
                } while (it2.hasNext());
            }
        }
        fz6 fz6 = (fz6) obj;
        this.N = fz6 != null ? (short) fz6.d() : 0;
        List<fz6> list2 = this.T;
        ArrayList arrayList2 = new ArrayList();
        for (T t : list2) {
            if (t.b() > 0) {
                arrayList2.add(t);
            }
        }
        Iterator it3 = arrayList2.iterator();
        if (it3.hasNext()) {
            obj2 = it3.next();
            if (it3.hasNext()) {
                int b = ((fz6) obj2).b();
                do {
                    Object next3 = it3.next();
                    int b2 = ((fz6) next3).b();
                    if (b < b2) {
                        obj2 = next3;
                        b = b2;
                    }
                } while (it3.hasNext());
            }
        }
        fz6 fz62 = (fz6) obj2;
        short b3 = fz62 != null ? (short) fz62.b() : 0;
        this.O = b3;
        if (b3 == ((short) 0)) {
            this.O = 100;
        }
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.E, this.J, (float) canvas.getWidth(), this.J, this.P);
        canvas.drawLine(this.E, this.G, (float) canvas.getWidth(), this.G, this.P);
        float f = (float) 2;
        canvas.drawText(String.valueOf((int) this.N), this.I + (((((float) canvas.getWidth()) - this.I) - this.Q.measureText(String.valueOf((int) this.N))) / f) + yx6.a(2.0f), this.J + (this.L * 1.5f), this.Q);
        canvas.drawText(String.valueOf((int) this.O), this.I + (((((float) canvas.getWidth()) - this.I) - this.Q.measureText(String.valueOf((int) this.O))) / f) + yx6.a(2.0f), this.G + (this.L * 1.5f), this.Q);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        ArrayList<Path> arrayList = new ArrayList();
        while (true) {
            Path path = null;
            do {
                boolean z2 = true;
                i = 0;
                for (T t : this.T) {
                    float c = this.H + (((float) t.c()) * this.U);
                    float f = this.G;
                    short s = this.O;
                    float d = f + (((this.J - f) / ((float) (s - this.N))) * ((float) (s - t.d())));
                    if (d <= this.J) {
                        if (z2) {
                            path = new Path();
                            path.moveTo(c, d);
                            z2 = false;
                        } else if (path != null) {
                            path.lineTo(c, d);
                        } else {
                            ee7.a();
                            throw null;
                        }
                        i++;
                    }
                }
                if (path != null) {
                    arrayList.add(path);
                }
                Paint paint = this.S;
                float f2 = this.H;
                paint.setShader(new LinearGradient(f2, this.G, f2, this.J, this.x, this.w, Shader.TileMode.MIRROR));
                for (Path path2 : arrayList) {
                    canvas.drawPath(path2, this.S);
                }
                return;
            } while (path == null);
            if (i > 1) {
                arrayList.add(path);
            }
        }
    }

    @DexIgnore
    public final int getDayInMinuteWithTimeZone() {
        return this.a0;
    }

    @DexIgnore
    public final List<v87<Integer, r87<Integer, Float>, String>> getListTimeZoneChange() {
        return this.W;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.V;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setDayInMinuteWithTimeZone(int i) {
        this.a0 = i;
    }

    @DexIgnore
    public final void setListTimeZoneChange(List<v87<Integer, r87<Integer, Float>, String>> list) {
        ee7.b(list, "value");
        this.W = list;
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        ee7.b(str, "<set-?>");
        this.V = str;
    }

    @DexIgnore
    public void a(String str, int i) {
        ee7.b(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " valueColor=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1222874814) {
            if (hashCode == -685682508 && str.equals("lowestHeartRate")) {
                this.w = i;
            }
        } else if (str.equals("maxHeartRate")) {
            this.x = i;
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void e() {
        if (!TextUtils.isEmpty(this.V)) {
            String b = eh5.l.a().b(this.V);
            if (!TextUtils.isEmpty(b)) {
                setBackgroundColor(Color.parseColor(b));
            }
        }
    }

    @DexIgnore
    public final void a(List<fz6> list) {
        ee7.b(list, "heartRatePointList");
        this.T.clear();
        this.T.addAll(list);
        d();
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f) {
        int i = 0;
        for (T t : list) {
            int i2 = i + 1;
            if (i >= 0) {
                canvas.drawText(t, list2.get(i).floatValue(), f, this.R);
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void d(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.d(canvas);
        if (this.W.isEmpty()) {
            List<String> d = w97.d(ig5.a(PortfolioApp.g0.c(), 2131886667), ig5.a(PortfolioApp.g0.c(), 2131886669), ig5.a(PortfolioApp.g0.c(), 2131886668), ig5.a(PortfolioApp.g0.c(), 2131886670), ig5.a(PortfolioApp.g0.c(), 2131886667));
            ArrayList arrayList = new ArrayList();
            int size = d.size();
            float f = (this.I - this.H) / ((float) (size - 1));
            for (int i = 0; i < size; i++) {
                arrayList.add(Float.valueOf(this.H + (((float) i) * f)));
            }
            a(canvas, d, arrayList, this.D + (((((float) canvas.getHeight()) - this.D) + this.M) / ((float) 2)));
            return;
        }
        List<v87<Integer, r87<Integer, Float>, String>> list = this.W;
        ArrayList arrayList2 = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList2.add((String) it.next().getThird());
        }
        List<String> d2 = ea7.d((Collection) arrayList2);
        List<v87<Integer, r87<Integer, Float>, String>> list2 = this.W;
        ArrayList arrayList3 = new ArrayList(x97.a(list2, 10));
        Iterator<T> it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList3.add(Integer.valueOf(((Number) it2.next().getFirst()).intValue()));
        }
        List<Number> d3 = ea7.d((Collection) arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (Number number : d3) {
            arrayList4.add(Float.valueOf(this.H + (((float) number.intValue()) * this.U)));
        }
        a(canvas, d2, arrayList4, this.D + (((((float) canvas.getHeight()) - this.D) + this.M) / ((float) 2)));
    }
}
