package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;
import com.fossil.cz6;
import com.fossil.dz6;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.w97;
import com.fossil.x97;
import com.fossil.yx6;
import com.fossil.zd7;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WLHeartRateChart extends View {
    @DexIgnore
    public short A;
    @DexIgnore
    public short B;
    @DexIgnore
    public short C;
    @DexIgnore
    public Paint D;
    @DexIgnore
    public Paint E;
    @DexIgnore
    public Paint F;
    @DexIgnore
    public Paint G;
    @DexIgnore
    public Paint H;
    @DexIgnore
    public Paint I;
    @DexIgnore
    public Paint J;
    @DexIgnore
    public List<RectF> K;
    @DexIgnore
    public b L; // = b.AVERAGE;
    @DexIgnore
    public List<dz6> M;
    @DexIgnore
    public int N; // = c.NONE.ordinal();
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Integer e;
    @DexIgnore
    public float f;
    @DexIgnore
    public Integer g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public short z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        AVERAGE,
        RESTING,
        PEAK
    }

    @DexIgnore
    public enum c {
        GENERAL,
        DAY,
        WEEK,
        MONTH,
        NONE
    }

    /*
    static {
        new a(null);
        ee7.a((Object) WLHeartRateChart.class.getSimpleName(), "WLHeartRateChart::class.java.simpleName");
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WLHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.WLHeartRateChart);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.getColor(2, -3355444);
            this.a = obtainStyledAttributes.getColor(0, -3355444);
            this.b = obtainStyledAttributes.getColor(1, -3355444);
            this.c = obtainStyledAttributes.getColor(10, -3355444);
            this.d = obtainStyledAttributes.getColor(6, -3355444);
            this.e = Integer.valueOf(obtainStyledAttributes.getResourceId(4, -1));
            this.f = obtainStyledAttributes.getDimension(3, yx6.c(13.0f));
            this.g = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
            this.h = obtainStyledAttributes.getDimension(7, yx6.c(13.0f));
            this.i = obtainStyledAttributes.getDimension(5, yx6.a(4.0f));
            this.N = obtainStyledAttributes.getInt(9, c.NONE.ordinal());
            Paint paint = new Paint();
            this.D = paint;
            paint.setColor(this.a);
            this.D.setStrokeWidth(2.0f);
            this.D.setStyle(Paint.Style.STROKE);
            Paint paint2 = new Paint();
            this.E = paint2;
            paint2.setColor(this.a);
            this.E.setStyle(Paint.Style.STROKE);
            this.E.setPathEffect(new DashPathEffect(new float[]{6.0f, 6.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            Paint paint3 = new Paint(1);
            this.F = paint3;
            paint3.setColor(this.d);
            this.F.setStyle(Paint.Style.FILL);
            this.F.setTextSize(this.f);
            Integer num = this.e;
            if (num == null || num.intValue() != -1) {
                Paint paint4 = this.F;
                Integer num2 = this.e;
                if (num2 != null) {
                    paint4.setTypeface(c7.a(context, num2.intValue()));
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Paint paint5 = new Paint(1);
            this.G = paint5;
            paint5.setColor(this.d);
            this.G.setStyle(Paint.Style.FILL);
            this.G.setTextSize(this.h);
            Integer num3 = this.g;
            if (num3 == null || num3.intValue() != -1) {
                Paint paint6 = this.G;
                Integer num4 = this.g;
                if (num4 != null) {
                    paint6.setTypeface(c7.a(context, num4.intValue()));
                } else {
                    ee7.a();
                    throw null;
                }
            }
            Paint paint7 = new Paint(1);
            this.H = paint7;
            paint7.setColor(this.b);
            this.H.setStyle(Paint.Style.FILL);
            Paint paint8 = new Paint(1);
            this.I = paint8;
            paint8.setColor(-1);
            this.I.setStyle(Paint.Style.FILL);
            Paint paint9 = new Paint(1);
            this.J = paint9;
            paint9.setColor(this.c);
            this.J.setStyle(Paint.Style.STROKE);
            Rect rect = new Rect();
            this.F.getTextBounds("222", 0, 3, rect);
            this.w = (float) rect.width();
            this.x = (float) rect.height();
            Rect rect2 = new Rect();
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131887176);
            this.G.getTextBounds(a2, 0, a2.length(), rect2);
            this.y = (float) rect2.height();
            this.M = new ArrayList();
            this.K = new ArrayList();
            obtainStyledAttributes.recycle();
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        e(canvas);
        float size = (this.u - this.t) / ((float) ((this.M.size() + this.M.size()) - 1));
        a(canvas, size, ((this.u - this.t) - (((float) this.M.size()) * size)) / ((float) (this.M.size() - 1)));
        a(canvas, w97.d(ig5.a(PortfolioApp.g0.c(), 2131887176), ig5.a(PortfolioApp.g0.c(), 2131887177), ig5.a(PortfolioApp.g0.c(), 2131887176)), w97.d(Float.valueOf(this.K.get(0).left), Float.valueOf(this.K.get(11).left), Float.valueOf(this.K.get(23).left)), this.p + (((((float) getMeasuredHeight()) - this.p) + this.y) / ((float) 2)));
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        this.v = this.p;
        f(canvas);
        float size = (this.u - this.t) / ((float) ((this.M.size() + this.M.size()) - 1));
        a(canvas, size, ((this.u - this.t) - (((float) this.M.size()) * size)) / ((float) (this.M.size() - 1)));
        a(canvas, w97.d(ig5.a(PortfolioApp.g0.c(), 2131887176), ig5.a(PortfolioApp.g0.c(), 2131887177), ig5.a(PortfolioApp.g0.c(), 2131887176)), w97.d(Float.valueOf(this.K.get(0).left), Float.valueOf(this.K.get(11).left), Float.valueOf(this.K.get(23).left)), this.p + (((((float) getMeasuredHeight()) - this.p) + this.y) / ((float) 2)));
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        e(canvas);
        float size = (this.u - this.t) / ((float) ((this.M.size() + this.M.size()) - 1));
        a(canvas, size, ((this.u - this.t) - (((float) this.M.size()) * size)) / ((float) (this.M.size() - 1)));
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        float measuredHeight = this.p + (((((float) getMeasuredHeight()) - this.p) + this.y) / ((float) 2));
        int size2 = this.M.size() / 4;
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.M.size()) {
            arrayList.add(String.valueOf(i2 + 1));
            arrayList2.add(Float.valueOf(this.K.get(i2).left));
            int i4 = i3 + 1;
            int i5 = i3 + (size2 * i4);
            i3 = i4;
            i2 = i5;
        }
        arrayList.add(String.valueOf(this.K.size()));
        List<RectF> list = this.K;
        arrayList2.add(Float.valueOf(list.get(w97.a((List) list)).left));
        a(canvas, arrayList, arrayList2, measuredHeight);
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        e(canvas);
        float f2 = this.u;
        float f3 = this.t;
        float f4 = (f2 - f3) / ((float) 48);
        a(canvas, f4, ((f2 - f3) - (((float) 7) * f4)) / ((float) 6));
        List<String> d2 = w97.d(ig5.a(PortfolioApp.g0.c(), 2131886719), ig5.a(PortfolioApp.g0.c(), 2131886718), ig5.a(PortfolioApp.g0.c(), 2131886721), ig5.a(PortfolioApp.g0.c(), 2131886723), ig5.a(PortfolioApp.g0.c(), 2131886722), ig5.a(PortfolioApp.g0.c(), 2131886717), ig5.a(PortfolioApp.g0.c(), 2131886720));
        List<RectF> list = this.K;
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(Float.valueOf(((RectF) it.next()).left));
        }
        a(canvas, d2, ea7.d((Collection) arrayList), this.p + (((((float) getMeasuredHeight()) - this.p) + this.y) / ((float) 2)));
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.q, this.p, (float) getMeasuredWidth(), this.p, this.D);
        canvas.drawLine(this.q, this.j, (float) getMeasuredWidth(), this.j, this.D);
        float f2 = (float) 2;
        float measuredWidth = this.r + (((((float) getMeasuredWidth()) - this.r) - this.F.measureText(String.valueOf((int) this.B))) / f2);
        float f3 = this.v;
        float f4 = (float) 1;
        float f5 = (float) 4;
        float f6 = f3 - (((f3 - this.j) * f4) / f5);
        canvas.drawText(String.valueOf((int) this.B), measuredWidth, (this.x / f2) + f6, this.F);
        float measuredWidth2 = this.r + (((((float) getMeasuredWidth()) - this.r) - this.F.measureText(String.valueOf((int) this.C))) / f2);
        float f7 = this.s;
        float f8 = f7 + (((this.v - f7) * f4) / f5);
        canvas.drawText(String.valueOf((int) this.C), measuredWidth2, (this.x / f2) + f8, this.F);
        Path path = new Path();
        path.moveTo(this.q, f6);
        path.lineTo(this.r, f6);
        canvas.drawPath(path, this.E);
        path.moveTo(this.q, f8);
        path.lineTo(this.r, f8);
        canvas.drawPath(path, this.E);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2 = (float) 2;
        float measuredWidth = this.r + (((((float) getMeasuredWidth()) - this.r) - this.F.measureText(String.valueOf((int) this.A))) / f2);
        float f3 = this.s;
        canvas.drawText(String.valueOf((int) this.A), measuredWidth, (this.x / f2) + f3, this.F);
        float measuredWidth2 = this.r + (((((float) getMeasuredWidth()) - this.r) - this.F.measureText(String.valueOf((int) this.z))) / f2);
        float f4 = this.v;
        canvas.drawText(String.valueOf((int) this.z), measuredWidth2, (this.x / f2) + f4, this.F);
        Path path = new Path();
        path.moveTo(this.q, f3);
        path.lineTo(this.r, f3);
        canvas.drawPath(path, this.E);
        path.moveTo(this.q, f4);
        path.lineTo(this.r, f4);
        canvas.drawPath(path, this.E);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.p = ((float) getMeasuredHeight()) - (this.y * ((float) 2));
        this.q = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.r = ((float) getMeasuredWidth()) - (this.w * 1.5f);
        this.s = this.h;
        this.t = this.q + yx6.a(5.0f);
        this.v = this.p - this.h;
        this.u = this.r - yx6.a(5.0f);
        int i2 = this.N;
        if (i2 == c.GENERAL.ordinal()) {
            b(canvas);
        } else if (i2 == c.DAY.ordinal()) {
            if (!this.M.isEmpty()) {
                a(canvas);
            }
        } else if (i2 == c.WEEK.ordinal()) {
            if (!this.M.isEmpty()) {
                d(canvas);
            }
        } else if (i2 == c.MONTH.ordinal() && (!this.M.isEmpty())) {
            c(canvas);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3) {
        this.K.clear();
        int i2 = 0;
        int i3 = 0;
        for (T t2 : this.M) {
            int i4 = i3 + 1;
            if (i3 >= 0) {
                T t3 = t2;
                short a2 = t3.a();
                short b2 = t3.b();
                float f4 = this.s;
                float f5 = this.v;
                short s2 = this.A;
                short s3 = this.z;
                float f6 = (((f5 - f4) / ((float) (s2 - s3))) * ((float) (s2 - b2))) + f4;
                float f7 = this.t + ((f2 + f3) * ((float) i3));
                RectF rectF = new RectF(f7, f6, f7 + f2, (((float) (b2 - a2)) * ((f5 - f4) / ((float) (s2 - s3)))) + f6);
                float f8 = this.i;
                canvas.drawRoundRect(rectF, f8, f8, this.H);
                if (a2 == -1 && b2 == -1) {
                    rectF.top = 1.0f;
                    rectF.bottom = -1.0f;
                }
                this.K.add(rectF);
                i3 = i4;
            } else {
                w97.c();
                throw null;
            }
        }
        if (cz6.a[this.L.ordinal()] == 2 && (!this.K.isEmpty())) {
            float f9 = f2 / ((float) 2);
            this.J.setStrokeWidth(f9 / 1.5f);
            for (T t4 : this.K) {
                int i5 = i2 + 1;
                if (i2 >= 0) {
                    T t5 = t4;
                    if (i2 < w97.a((List) this.K)) {
                        RectF rectF2 = this.K.get(i5);
                        float f10 = ((RectF) t5).top;
                        if (f10 <= ((RectF) t5).bottom) {
                            float f11 = rectF2.top;
                            if (f11 <= rectF2.bottom) {
                                canvas.drawLine(((RectF) t5).left + f9, f10 + f9, rectF2.left + f9, f11 + f9, this.J);
                            }
                        }
                    }
                    float f12 = ((RectF) t5).top;
                    if (f12 <= ((RectF) t5).bottom) {
                        canvas.drawCircle(((RectF) t5).left + f9, f12 + f9, f9, this.I);
                        canvas.drawCircle(((RectF) t5).left + f9, ((RectF) t5).top + f9, f9, this.J);
                    }
                    i2 = i5;
                } else {
                    w97.c();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f2) {
        int i2 = 0;
        for (T t2 : list) {
            int i3 = i2 + 1;
            if (i2 >= 0) {
                canvas.drawText(t2, list2.get(i2).floatValue(), f2, this.G);
                i2 = i3;
            } else {
                w97.c();
                throw null;
            }
        }
    }
}
