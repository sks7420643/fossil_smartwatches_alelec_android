package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.e7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.ig5;
import com.fossil.og7;
import com.fossil.pl4;
import com.fossil.w97;
import com.fossil.x97;
import com.fossil.yx6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeekHeartRateChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public String B; // = "";
    @DexIgnore
    public float C;
    @DexIgnore
    public String D; // = "";
    @DexIgnore
    public float E;
    @DexIgnore
    public String F; // = "";
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public String H; // = "";
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public short S;
    @DexIgnore
    public short T;
    @DexIgnore
    public Paint U;
    @DexIgnore
    public Paint V;
    @DexIgnore
    public Paint W;
    @DexIgnore
    public Paint a0;
    @DexIgnore
    public Paint b0;
    @DexIgnore
    public Paint c0;
    @DexIgnore
    public int d0;
    @DexIgnore
    public List<Integer> e0;
    @DexIgnore
    public List<PointF> f0;
    @DexIgnore
    public int g0; // = 1;
    @DexIgnore
    public List<String> h0; // = new ArrayList();
    @DexIgnore
    public String v; // = "";
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public String x; // = "";
    @DexIgnore
    public float y;
    @DexIgnore
    public String z; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<Integer, Boolean> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
            return Boolean.valueOf(invoke(num.intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<Integer, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
            return Boolean.valueOf(invoke(num.intValue()));
        }

        @DexIgnore
        public final boolean invoke(int i) {
            return i > 0;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeekHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        String b2;
        Typeface c2;
        Typeface c3;
        String b3;
        String b4;
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.WeekHeartRateChart);
        String string = obtainStyledAttributes.getString(2);
        this.v = string == null ? "dianaHeartRateTab" : string;
        String string2 = obtainStyledAttributes.getString(4);
        this.w = string2 == null ? "nonBrandDisableCalendarDay" : string2;
        String string3 = obtainStyledAttributes.getString(5);
        this.x = string3 == null ? "calendarDate" : string3;
        this.y = obtainStyledAttributes.getDimension(3, yx6.c(13.0f));
        String string4 = obtainStyledAttributes.getString(8);
        this.z = string4 == null ? "onDianaHeartRateTab" : string4;
        String string5 = obtainStyledAttributes.getString(9);
        String str = "calendarWeekday";
        this.A = string5 == null ? str : string5;
        String string6 = obtainStyledAttributes.getString(12);
        this.B = string6 != null ? string6 : str;
        this.C = obtainStyledAttributes.getDimension(10, yx6.c(13.0f));
        String string7 = obtainStyledAttributes.getString(11);
        this.D = string7 == null ? "secondaryText" : string7;
        this.E = obtainStyledAttributes.getDimension(7, yx6.a(13.0f));
        String string8 = obtainStyledAttributes.getString(0);
        this.F = string8 == null ? "averageRestingHeartRate" : string8;
        String string9 = obtainStyledAttributes.getString(1);
        this.G = string9 == null ? "aboveAverageRestingHeartRate" : string9;
        String string10 = obtainStyledAttributes.getString(6);
        this.H = string10 == null ? "maxHeartRate" : string10;
        obtainStyledAttributes.recycle();
        this.U = new Paint();
        if (!TextUtils.isEmpty(this.w) && (b4 = eh5.l.a().b(this.v)) != null) {
            this.U.setColor(Color.parseColor(b4));
        }
        this.U.setStrokeWidth(2.0f);
        this.U.setStyle(Paint.Style.STROKE);
        this.V = new Paint(1);
        if (!TextUtils.isEmpty(this.w) && (b3 = eh5.l.a().b(this.w)) != null) {
            this.V.setColor(Color.parseColor(b3));
        }
        this.V.setStyle(Paint.Style.FILL);
        this.V.setTextSize(this.y);
        if (!TextUtils.isEmpty(this.x) && (c3 = eh5.l.a().c(this.x)) != null) {
            this.V.setTypeface(c3);
        }
        Paint paint = new Paint(1);
        this.W = paint;
        paint.setColor(Color.parseColor(eh5.l.a().b(this.z)));
        this.W.setTextSize(this.E);
        this.W.setStyle(Paint.Style.FILL);
        if (!TextUtils.isEmpty(this.A) && (c2 = eh5.l.a().c(this.A)) != null) {
            this.W.setTypeface(c2);
        }
        this.a0 = new Paint(1);
        if (!TextUtils.isEmpty(this.D) && (b2 = eh5.l.a().b(this.D)) != null) {
            this.a0.setColor(Color.parseColor(b2));
        }
        this.a0.setStyle(Paint.Style.FILL);
        this.a0.setTextSize(this.C);
        if (!TextUtils.isEmpty(this.B) && (c = eh5.l.a().c(this.B)) != null) {
            this.a0.setTypeface(c);
        }
        Paint paint2 = new Paint(1);
        this.b0 = paint2;
        paint2.setColor(Color.parseColor(eh5.l.a().b(this.F)));
        this.b0.setStyle(Paint.Style.FILL);
        Paint paint3 = new Paint(1);
        this.c0 = paint3;
        paint3.setColor(e7.c(Color.parseColor(eh5.l.a().b(this.H)), 20));
        this.c0.setStyle(Paint.Style.STROKE);
        this.c0.setStrokeWidth(yx6.a(2.0f));
        Rect rect = new Rect();
        this.V.getTextBounds("222", 0, 3, rect);
        this.P = (float) rect.width();
        this.Q = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887176);
        this.a0.getTextBounds(a2, 0, a2.length(), rect2);
        this.R = (float) rect2.height();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(0);
        }
        this.e0 = arrayList;
        this.f0 = new ArrayList();
        d();
        a();
    }

    @DexIgnore
    public final void a(List<Integer> list) {
        ee7.b(list, "heartRatePointList");
        this.e0.clear();
        this.f0.clear();
        List<Integer> list2 = this.e0;
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (next != null) {
                i = next.intValue();
            }
            arrayList.add(Integer.valueOf(i));
        }
        list2.addAll(arrayList);
        d();
        int size = this.e0.size();
        if (size < 7) {
            int i2 = 7 - size;
            ArrayList arrayList2 = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList2.add(0);
            }
            this.e0.addAll(arrayList2);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void b(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.b(canvas);
        this.I = ((float) canvas.getHeight()) - (this.R * ((float) 2));
        this.J = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.K = ((float) canvas.getWidth()) - (this.P / ((float) 3));
        float f = (float) 6;
        this.L = this.C + this.E + (this.R * f);
        float a2 = this.J + yx6.a(10.0f);
        float f2 = this.E;
        this.M = a2 + f2;
        this.O = (this.I - f2) - (this.C * f);
        this.N = (this.K - (this.P * 1.5f)) - f2;
        f(canvas);
    }

    @DexIgnore
    public final void d() {
        int i;
        short s;
        Integer num = (Integer) og7.e(og7.a(ea7.b((Iterable) this.e0), a.INSTANCE));
        int i2 = 0;
        this.S = num != null ? (short) num.intValue() : 0;
        Integer num2 = (Integer) og7.d(og7.a(ea7.b((Iterable) this.e0), b.INSTANCE));
        short intValue = num2 != null ? (short) num2.intValue() : 0;
        this.T = intValue;
        if (intValue == ((short) 0)) {
            this.T = 100;
        }
        Iterator<T> it = this.e0.iterator();
        int i3 = 0;
        while (it.hasNext()) {
            int intValue2 = it.next().intValue();
            if (intValue2 > 0) {
                i3 += intValue2;
                i2++;
            }
        }
        if (i2 > 0) {
            i = i3 / i2;
        } else {
            i = (this.T - this.S) / 2;
        }
        this.d0 = i;
        if (Math.abs(this.T - i) >= Math.abs(this.S - this.d0)) {
            s = this.T;
        } else {
            s = this.S;
        }
        int abs = Math.abs((int) s);
        int i4 = this.d0;
        this.g0 = abs != i4 ? Math.abs(s - i4) * 2 : 1;
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        float f;
        short s = this.T;
        int i = this.d0;
        if (s != ((short) i)) {
            float f2 = this.L;
            f = f2 + (((this.O - f2) / ((float) this.g0)) * ((float) (s - i)));
        } else {
            float f3 = this.L;
            f = f3 + ((this.O - f3) / 2.0f);
        }
        canvas.drawLine(this.J, f, (float) canvas.getWidth(), f, this.U);
        canvas.drawText(String.valueOf(this.d0), this.K - this.V.measureText(String.valueOf(this.d0)), f + (this.Q * 1.5f), this.V);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        a(canvas, (this.N - this.M) / ((float) 6));
    }

    @DexIgnore
    public final List<String> getListWeekDays() {
        return this.h0;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setMWidth(i);
        setMHeight(i2);
        setMLeftPadding(getPaddingLeft());
        setMTopPadding(getPaddingTop());
        setMRightPadding(getPaddingRight());
        setMBottomPadding(getPaddingBottom());
        getMGraph().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMBackground().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
        getMGraphOverlay().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), i2 - getMBottomPadding());
    }

    @DexIgnore
    public final void setListWeekDays(List<String> list) {
        ee7.b(list, "value");
        this.h0.clear();
        this.h0.addAll(list);
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void a(Canvas canvas, float f) {
        String str;
        eh5 eh5;
        Rect rect = new Rect();
        int i = 0;
        for (T t : this.e0) {
            int i2 = i + 1;
            if (i >= 0) {
                int intValue = t.intValue();
                float f2 = this.M + (((float) i) * f);
                float f3 = this.L;
                float f4 = this.O;
                float f5 = (f4 - f3) / ((float) this.g0);
                short s = this.T;
                float f6 = f3 + (f5 * ((float) (s - intValue)));
                if (this.S == s) {
                    if (intValue > 0) {
                        f6 = f3 + ((f4 - f3) / 2.0f);
                        a(canvas, f2, f6, intValue, Color.parseColor(eh5.l.a().b(this.F)), rect);
                    }
                } else if (intValue > 0) {
                    if (intValue > this.d0) {
                        eh5 = eh5.l.a();
                        str = this.G;
                    } else {
                        eh5 = eh5.l.a();
                        str = this.F;
                    }
                    a(canvas, f2, f6, intValue, Color.parseColor(eh5.b(str)), rect);
                }
                this.f0.add(new PointF(f2, f6));
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void d(Canvas canvas) {
        ee7.b(canvas, "canvas");
        super.d(canvas);
        if (this.h0.isEmpty()) {
            this.h0.addAll(w97.d(ig5.a(PortfolioApp.g0.c(), 2131886719), ig5.a(PortfolioApp.g0.c(), 2131886718), ig5.a(PortfolioApp.g0.c(), 2131886721), ig5.a(PortfolioApp.g0.c(), 2131886723), ig5.a(PortfolioApp.g0.c(), 2131886722), ig5.a(PortfolioApp.g0.c(), 2131886717), ig5.a(PortfolioApp.g0.c(), 2131886720)));
        }
        a(canvas, this.h0, (this.N - this.M) / ((float) 6), this.I + (((((float) canvas.getHeight()) - this.I) + this.R) / ((float) 2)));
    }

    @DexIgnore
    public final void a(Canvas canvas, float f, float f2, int i, int i2, Rect rect) {
        this.b0.setColor(i2);
        canvas.drawCircle(f, f2, this.E, this.b0);
        canvas.drawCircle(f, f2, this.E + yx6.a(3.0f), this.c0);
        String valueOf = String.valueOf(i);
        this.a0.getTextBounds(valueOf, 0, valueOf.length(), rect);
        canvas.drawText(valueOf, f - (this.W.measureText(valueOf) / ((float) 2)), f2 + ((float) (rect.height() / 2)), this.W);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, float f, float f2) {
        int i = 0;
        for (T t : list) {
            int i2 = i + 1;
            if (i >= 0) {
                T t2 = t;
                canvas.drawText(t2, (this.M + (((float) i) * f)) - (this.V.measureText(t2) / ((float) 2)), f2, this.a0);
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
    }
}
