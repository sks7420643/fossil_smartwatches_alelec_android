package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.qy6;
import com.fossil.ux6;
import com.fossil.v6;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleEditText extends AppCompatEditText {
    @DexIgnore
    public /* final */ String d; // = "FlexibleEditText";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p; // = FlexibleTextView.t.a();
    @DexIgnore
    public String q; // = "";
    @DexIgnore
    public String r; // = "primaryColor";
    @DexIgnore
    public /* final */ LayerDrawable s;
    @DexIgnore
    public /* final */ GradientDrawable t;
    @DexIgnore
    public boolean u;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context) {
        super(context);
        ee7.b(context, "context");
        LayerDrawable layerDrawable = (LayerDrawable) v6.c(getContext(), 2131230851);
        this.s = layerDrawable;
        this.t = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363060) : null);
        this.u = true;
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        this.i = 0;
        Editable text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleEditText);
                this.i = obtainStyledAttributes.getInt(7, 0);
                this.j = obtainStyledAttributes.getInt(3, 0);
                this.p = obtainStyledAttributes.getColor(8, FlexibleTextView.t.a());
                String string = obtainStyledAttributes.getString(5);
                String str = "";
                if (string == null) {
                    string = str;
                }
                this.e = string;
                String string2 = obtainStyledAttributes.getString(6);
                if (string2 == null) {
                    string2 = str;
                }
                this.f = string2;
                String string3 = obtainStyledAttributes.getString(4);
                if (string3 == null) {
                    string3 = str;
                }
                this.g = string3;
                String string4 = obtainStyledAttributes.getString(0);
                if (string4 == null) {
                    string4 = str;
                }
                this.q = string4;
                this.u = obtainStyledAttributes.getBoolean(1, true);
                String string5 = obtainStyledAttributes.getString(2);
                if (string5 != null) {
                    str = string5;
                }
                this.h = str;
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                if (resourceId != -1) {
                    text = a(resourceId);
                }
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                if (resourceId2 != -1) {
                    hint = a(resourceId2);
                }
                obtainStyledAttributes2.recycle();
            }
            if (!TextUtils.isEmpty(text)) {
                setText(text);
            }
            if (!TextUtils.isEmpty(hint)) {
                ee7.a((Object) hint, "hint");
                setHint(a(hint, this.j));
            }
            if (this.p != FlexibleTextView.t.a()) {
                qy6.a(this, this.p);
            }
            if (TextUtils.isEmpty(this.e)) {
                this.e = "primaryText";
            }
            if (TextUtils.isEmpty(this.f)) {
                this.f = "nonBrandTextStyle2";
            }
            a(this.e, this.f, this.g, this.h, this.q);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final LayerDrawable getShape() {
        return this.s;
    }

    @DexIgnore
    public boolean onKeyPreIme(int i2, KeyEvent keyEvent) {
        ee7.b(keyEvent, Constants.EVENT);
        if (keyEvent.getKeyCode() != 4) {
            return false;
        }
        clearFocus();
        return false;
    }

    @DexIgnore
    public void onSelectionChanged(int i2, int i3) {
        Editable text;
        if (this.u || (text = getText()) == null || (i2 == text.length() && i3 == text.length())) {
            super.onSelectionChanged(i2, i3);
        } else {
            setSelection(text.length(), text.length());
        }
    }

    @DexIgnore
    @Override // android.widget.TextView, android.widget.EditText
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        ee7.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        LayerDrawable layerDrawable = (LayerDrawable) v6.c(getContext(), 2131230851);
        this.s = layerDrawable;
        this.t = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363060) : null);
        this.u = true;
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        LayerDrawable layerDrawable = (LayerDrawable) v6.c(getContext(), 2131230851);
        this.s = layerDrawable;
        this.t = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363060) : null);
        this.u = true;
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4, String str5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = this.d;
        local.d(str6, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String b = eh5.l.a().b(str);
        Typeface c = eh5.l.a().c(str2);
        String b2 = eh5.l.a().b(str3);
        String b3 = eh5.l.a().b(str4);
        String b4 = eh5.l.a().b(str5);
        if (b != null) {
            setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b2 != null) {
            setBackgroundColor(Color.parseColor(b2));
        }
        if (b3 != null) {
            setHintTextColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            GradientDrawable gradientDrawable = this.t;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(b4));
            }
            setBackground(this.s);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        String str;
        String b;
        if (z) {
            str = this.r;
        } else {
            str = this.q;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.d;
        local.d(str2, "switchBorderColor " + str);
        if (!TextUtils.isEmpty(str) && (b = eh5.l.a().b(str)) != null) {
            GradientDrawable gradientDrawable = this.t;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(b));
            }
            setBackground(this.s);
        }
    }

    @DexIgnore
    public final void a() {
        Object systemService = PortfolioApp.g0.c().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).showSoftInput(this, 0);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.i);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return ux6.a(charSequence);
        }
        if (i2 == 2) {
            return ux6.b(charSequence);
        }
        if (i2 == 3) {
            return ux6.d(charSequence);
        }
        if (i2 == 4) {
            return ux6.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return ux6.c(charSequence);
    }

    @DexIgnore
    public final String a(int i2) {
        String a = ig5.a(PortfolioApp.g0.c(), i2);
        ee7.a((Object) a, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a;
    }
}
