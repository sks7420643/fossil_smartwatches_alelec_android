package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class EditTextCustom extends FlexibleEditText {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public a C;
    @DexIgnore
    public Drawable v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public b z;

    @DexIgnore
    public interface a {

        @DexIgnore
        /* renamed from: com.portfolio.platform.view.EditTextCustom$a$a  reason: collision with other inner class name */
        public enum EnumC0309a {
            TOP,
            BOTTOM,
            LEFT,
            RIGHT
        }

        @DexIgnore
        void a(EnumC0309a aVar);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public EditTextCustom(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // java.lang.Object
    public void finalize() throws Throwable {
        this.v = null;
        this.y = null;
        this.w = null;
        this.x = null;
        super.finalize();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.FlexibleEditText
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            b bVar = this.z;
            if (bVar != null) {
                bVar.a();
            }
            clearFocus();
        }
        return true;
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        a aVar;
        a aVar2;
        if (motionEvent.getAction() == 0) {
            this.A = (int) motionEvent.getX();
            this.B = (int) motionEvent.getY();
            Drawable drawable = this.y;
            if (drawable == null || !drawable.getBounds().contains(this.A, this.B)) {
                Drawable drawable2 = this.x;
                if (drawable2 == null || !drawable2.getBounds().contains(this.A, this.B)) {
                    Drawable drawable3 = this.w;
                    if (drawable3 != null) {
                        Rect bounds = drawable3.getBounds();
                        int i = (int) (((double) (getResources().getDisplayMetrics().density * 13.0f)) + 0.5d);
                        int i2 = this.A;
                        int i3 = this.B;
                        if (!bounds.contains(i2, i3)) {
                            i2 = this.A;
                            int i4 = i2 - i;
                            int i5 = this.B - i;
                            if (i4 > 0) {
                                i2 = i4;
                            }
                            i3 = i5 <= 0 ? this.B : i5;
                        }
                        if (bounds.contains(i2, i3) && (aVar2 = this.C) != null) {
                            aVar2.a(a.EnumC0309a.LEFT);
                            motionEvent.setAction(3);
                            return false;
                        }
                    }
                    Drawable drawable4 = this.v;
                    if (drawable4 != null) {
                        Rect bounds2 = drawable4.getBounds();
                        int i6 = this.B - 13;
                        int width = getWidth() - (this.A + 13);
                        if (width <= 0) {
                            width += 13;
                        }
                        if (i6 <= 0) {
                            i6 = this.B;
                        }
                        if (!bounds2.contains(width, i6) || (aVar = this.C) == null) {
                            return super.onTouchEvent(motionEvent);
                        }
                        aVar.a(a.EnumC0309a.RIGHT);
                        motionEvent.setAction(3);
                        return false;
                    }
                } else {
                    this.C.a(a.EnumC0309a.TOP);
                    return super.onTouchEvent(motionEvent);
                }
            } else {
                this.C.a(a.EnumC0309a.BOTTOM);
                return super.onTouchEvent(motionEvent);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setBackPressListener(b bVar) {
        this.z = bVar;
    }

    @DexIgnore
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            this.w = drawable;
        }
        if (drawable3 != null) {
            this.v = drawable3;
        }
        if (drawable2 != null) {
            this.x = drawable2;
        }
        if (drawable4 != null) {
            this.y = drawable4;
        }
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }

    @DexIgnore
    public void setDrawableClickListener(a aVar) {
        this.C = aVar;
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Fossil_Scout-Regular_10.otf"));
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
