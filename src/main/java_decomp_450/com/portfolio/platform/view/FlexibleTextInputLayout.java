package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputLayout extends TextInputLayout {
    @DexIgnore
    public String A0; // = "";
    @DexIgnore
    public String B0; // = "";
    @DexIgnore
    public String x0; // = "";
    @DexIgnore
    public String y0; // = "";
    @DexIgnore
    public String z0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleTextInputLayout);
            String string = obtainStyledAttributes.getString(3);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.x0 = string;
            String string2 = obtainStyledAttributes.getString(4);
            if (string2 == null) {
                string2 = str;
            }
            this.y0 = string2;
            String string3 = obtainStyledAttributes.getString(2);
            if (string3 == null) {
                string3 = str;
            }
            this.z0 = string3;
            String string4 = obtainStyledAttributes.getString(0);
            if (string4 == null) {
                string4 = str;
            }
            this.A0 = string4;
            String string5 = obtainStyledAttributes.getString(1);
            if (string5 != null) {
                str = string5;
            }
            this.B0 = str;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.x0)) {
            this.x0 = "primaryText";
        }
        if (TextUtils.isEmpty(this.y0)) {
            this.y0 = "nonBrandTextStyle2";
        }
        a(this.x0, this.y0, this.B0);
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        String b = eh5.l.a().b(str);
        Typeface c = eh5.l.a().c(str2);
        String b2 = eh5.l.a().b(this.z0);
        String b3 = eh5.l.a().b(this.A0);
        String b4 = eh5.l.a().b(str3);
        if (b != null) {
            setDefaultHintTextColor(ColorStateList.valueOf(Color.parseColor(b)));
        }
        if (b2 != null) {
            setPasswordVisibilityToggleTintList(ColorStateList.valueOf(Color.parseColor(b2)));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b3 != null) {
            setBoxStrokeColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            setErrorTextColor(ColorStateList.valueOf(Color.parseColor(b4)));
        }
    }
}
