package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.pl4;
import com.google.android.material.textfield.TextInputEditText;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputEditText extends TextInputEditText {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context) {
        super(context);
        ee7.b(context, "context");
        a(null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleTextInputEditText);
            String string = obtainStyledAttributes.getString(0);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.d = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 != null) {
                str = string2;
            }
            this.e = str;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.d)) {
            this.d = "primaryText";
        }
        if (TextUtils.isEmpty(this.e)) {
            this.e = "nonBrandTextStyle2";
        }
        a(this.d, this.e);
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        String b = eh5.l.a().b(str);
        Typeface c = eh5.l.a().c(str2);
        if (b != null) {
            setTextColor(Color.parseColor(b));
            setHintTextColor(Color.parseColor(b));
            setLinkTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
    }
}
