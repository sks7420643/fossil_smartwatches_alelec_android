package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.p7;
import com.fossil.pl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleSwitchCompat extends SwitchCompat {
    @DexIgnore
    public String U; // = "";
    @DexIgnore
    public String V; // = "";
    @DexIgnore
    public String W; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleSwitchCompat);
            String string = obtainStyledAttributes.getString(2);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.U = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = str;
            }
            this.V = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 != null) {
                str = string3;
            }
            this.W = str;
            obtainStyledAttributes.recycle();
        }
        d();
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.U) && !TextUtils.isEmpty(this.V) && !TextUtils.isEmpty(this.W)) {
            String b = eh5.l.a().b(this.U);
            String b2 = eh5.l.a().b(this.V);
            if (!TextUtils.isEmpty(b) && !TextUtils.isEmpty(b2)) {
                p7.a(getTrackDrawable(), new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(b2), Color.parseColor(b)}));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
