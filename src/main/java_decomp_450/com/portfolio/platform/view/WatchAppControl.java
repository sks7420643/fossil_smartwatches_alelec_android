package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.hd5;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.w87;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppControl extends ConstraintLayout {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public String C;
    @DexIgnore
    public String D;
    @DexIgnore
    public int E;
    @DexIgnore
    public String F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public /* final */ ColorDrawable L; // = new ColorDrawable(v6.a(getContext(), (int) R.color.transparent));
    @DexIgnore
    public ImageView v;
    @DexIgnore
    public TextView w;
    @DexIgnore
    public TextView x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public Drawable z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public WatchAppControl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int parseColor = Color.parseColor("#FFFF00");
        this.y = parseColor;
        this.E = parseColor;
        this.G = parseColor;
        this.H = parseColor;
        this.I = parseColor;
        TypedArray typedArray = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558837, (ViewGroup) this, true) : null;
        if (inflate != null) {
            View findViewById = inflate.findViewById(2131362696);
            ee7.a((Object) findViewById, "view.findViewById(R.id.iv_icon)");
            this.v = (ImageView) findViewById;
            View findViewById2 = inflate.findViewById(2131363264);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_end)");
            this.w = (TextView) findViewById2;
            View findViewById3 = inflate.findViewById(2131363224);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.tv_bottom)");
            this.x = (TextView) findViewById3;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, pl4.WatchAppControl) : typedArray;
            if (typedArray != null) {
                this.z = typedArray.getDrawable(8);
                this.A = typedArray.getDrawable(9);
                this.B = typedArray.getDrawable(10);
                this.D = typedArray.getString(5);
                this.E = typedArray.getColor(6, this.y);
                typedArray.getDimension(7, -1.0f);
                this.F = typedArray.getString(0);
                this.G = typedArray.getColor(1, this.y);
                typedArray.getDimension(2, -1.0f);
                this.H = typedArray.getColor(3, this.y);
                this.I = typedArray.getColor(4, this.y);
                this.J = typedArray.getBoolean(12, false);
                boolean z2 = typedArray.getBoolean(11, false);
                this.K = z2;
                setRemoveMode(z2);
                typedArray.recycle();
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setIconSrc(Drawable drawable) {
        this.B = drawable;
        if (drawable != null) {
            this.v.setImageDrawable(drawable);
            this.v.setImageTintList(ColorStateList.valueOf(this.I));
        }
    }

    @DexIgnore
    private final void setRemoveMode(boolean z2) {
        this.K = z2;
        if (z2) {
            this.v.setVisibility(8);
            this.w.setVisibility(8);
            this.x.setVisibility(8);
            return;
        }
        d();
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.C)) {
            w87.a(hd5.a(this.v).a(this.C).f(), this.v);
        } else {
            Drawable drawable = this.B;
            if (drawable != null) {
                this.v.setImageDrawable(drawable);
                this.v.setImageTintList(ColorStateList.valueOf(this.I));
            }
        }
        if (!TextUtils.isEmpty(this.D)) {
            this.w.setText(this.D);
            this.w.setVisibility(0);
        } else {
            this.w.setVisibility(8);
        }
        if (!TextUtils.isEmpty(this.F)) {
            this.x.setText(this.F);
            this.x.setVisibility(0);
        } else {
            this.x.setVisibility(8);
        }
        if (this.J) {
            ImageView imageView = this.v;
            Drawable drawable2 = this.A;
            if (drawable2 == null) {
                drawable2 = this.z;
            }
            if (drawable2 == null) {
                drawable2 = this.L;
            }
            imageView.setBackground(drawable2);
            int i = this.I;
            int i2 = this.y;
            if (i != i2) {
                this.w.setTextColor(i);
            } else {
                int i3 = this.H;
                if (i3 != i2) {
                    this.w.setTextColor(i3);
                } else {
                    int i4 = this.E;
                    if (i4 != i2) {
                        this.w.setTextColor(i4);
                    }
                }
            }
            int i5 = this.I;
            int i6 = this.y;
            if (i5 != i6) {
                this.x.setTextColor(i5);
                return;
            }
            int i7 = this.H;
            if (i7 != i6) {
                this.x.setTextColor(i7);
                return;
            }
            int i8 = this.G;
            if (i8 != i6) {
                this.x.setTextColor(i8);
                return;
            }
            return;
        }
        ImageView imageView2 = this.v;
        Drawable drawable3 = this.z;
        if (drawable3 == null) {
            drawable3 = this.L;
        }
        imageView2.setBackground(drawable3);
        int i9 = this.H;
        int i10 = this.y;
        if (i9 != i10) {
            this.w.setTextColor(i9);
        } else {
            int i11 = this.E;
            if (i11 != i10) {
                this.w.setTextColor(i11);
            }
        }
        int i12 = this.H;
        int i13 = this.y;
        if (i12 != i13) {
            this.x.setTextColor(i12);
            return;
        }
        int i14 = this.G;
        if (i14 != i13) {
            this.x.setTextColor(i14);
        }
    }

    @DexIgnore
    public final void setDragMode(boolean z2) {
        if (z2) {
            setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else {
            setAlpha(1.0f);
        }
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.J = z2;
        setRemoveMode(this.K);
    }
}
