package com.portfolio.platform.view.ruler;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.pl4;
import com.fossil.we7;
import com.fossil.x87;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RulerView extends View {
    @DexIgnore
    public int a;
    @DexIgnore
    public Paint b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public int d; // = 14;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 100;
    @DexIgnore
    public float g; // = 0.6f;
    @DexIgnore
    public float h; // = 0.4f;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public int q;
    @DexIgnore
    public Typeface r;
    @DexIgnore
    public RulerValuePicker.a s;
    @DexIgnore
    public int t; // = -1;
    @DexIgnore
    public int u; // = 36;
    @DexIgnore
    public float v; // = 4.0f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            Context context = getContext();
            ee7.a((Object) context, "context");
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, pl4.RulerView, 0, 0);
            try {
                if (obtainStyledAttributes.hasValue(6)) {
                    this.p = obtainStyledAttributes.getColor(6, -1);
                }
                if (obtainStyledAttributes.hasValue(7)) {
                    this.u = obtainStyledAttributes.getDimensionPixelSize(7, 24);
                }
                if (obtainStyledAttributes.hasValue(0)) {
                    this.t = obtainStyledAttributes.getColor(0, -1);
                }
                if (obtainStyledAttributes.hasValue(2)) {
                    this.v = (float) obtainStyledAttributes.getDimensionPixelSize(2, 4);
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.d = obtainStyledAttributes.getDimensionPixelSize(1, 4);
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.g = obtainStyledAttributes.getFraction(3, 1, 1, 0.6f);
                }
                if (obtainStyledAttributes.hasValue(8)) {
                    this.h = obtainStyledAttributes.getFraction(8, 1, 1, 0.4f);
                }
                a(this.g, this.h);
                if (obtainStyledAttributes.hasValue(5)) {
                    this.e = obtainStyledAttributes.getInteger(5, 0);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.f = obtainStyledAttributes.getInteger(4, 100);
                }
                a(this.e, this.f);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        a();
    }

    @DexIgnore
    public final void b(float f2, float f3) {
        int i2 = this.a;
        this.i = (int) (((float) i2) * f2);
        this.j = (int) (((float) i2) * f3);
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2) {
        String a2 = a(this.e + i2);
        float f2 = (float) (this.d * i2);
        Paint paint = this.c;
        if (paint != null) {
            float textSize = paint.getTextSize();
            Paint paint2 = this.c;
            if (paint2 != null) {
                canvas.drawText(a2, f2, textSize, paint2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final int getIndicatorColor() {
        return this.t;
    }

    @DexIgnore
    public final int getIndicatorIntervalWidth() {
        return this.d;
    }

    @DexIgnore
    public final float getIndicatorWidth() {
        return this.v;
    }

    @DexIgnore
    public final float getLongIndicatorHeightRatio() {
        return this.g;
    }

    @DexIgnore
    public final int getMaxValue() {
        return this.f;
    }

    @DexIgnore
    public final int getMinValue() {
        return this.e;
    }

    @DexIgnore
    public final float getShortIndicatorHeightRatio() {
        return this.h;
    }

    @DexIgnore
    public final int getTextColor() {
        return this.t;
    }

    @DexIgnore
    public final float getTextSize() {
        return (float) this.u;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        ee7.b(canvas, "canvas");
        int i2 = this.f - this.e;
        for (int i3 = 1; i3 < i2; i3++) {
            RulerValuePicker.a aVar = this.s;
            if (aVar != null) {
                if (aVar == null) {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.data.ProfileFormatter");
                } else if (((ProfileFormatter) aVar).getMUnit() == 3) {
                    if (i3 % 12 == 0) {
                        a(canvas, i3);
                        c(canvas, i3);
                    } else {
                        b(canvas, i3);
                    }
                }
            }
            if (i3 % 5 == 0) {
                a(canvas, i3);
                if (i3 % 10 == 0) {
                    c(canvas, i3);
                }
            } else {
                b(canvas, i3);
            }
        }
        b(canvas, 0);
        b(canvas, getWidth());
        float f2 = (float) this.a;
        float measuredWidth = (float) getMeasuredWidth();
        float f3 = (float) this.a;
        Paint paint = this.b;
        if (paint != null) {
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, measuredWidth, f3, paint);
            super.onDraw(canvas);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        this.a = View.MeasureSpec.getSize(i3);
        int i4 = ((this.f - this.e) - 1) * this.d;
        b(this.g, this.h);
        setMeasuredDimension(i4, this.a);
    }

    @DexIgnore
    public final void setFormatter(RulerValuePicker.a aVar) {
        ee7.b(aVar, "formatter");
        if (!ee7.a(aVar, this.s)) {
            this.s = aVar;
            invalidate();
            requestLayout();
        }
    }

    @DexIgnore
    public final void setIndicatorColor(int i2) {
        this.t = i2;
        a();
    }

    @DexIgnore
    public final void setIndicatorIntervalDistance(int i2) {
        if (i2 > 0) {
            this.d = i2;
            invalidate();
            return;
        }
        throw new IllegalArgumentException("Interval cannot be negative or zero.");
    }

    @DexIgnore
    public final void setIndicatorWidth(int i2) {
        this.v = (float) i2;
        a();
    }

    @DexIgnore
    public final void setTextColor(int i2) {
        this.p = i2;
        a();
    }

    @DexIgnore
    public final void setTextSize(int i2) {
        this.u = i2;
        a();
    }

    @DexIgnore
    public final void setTextStyle(int i2) {
        this.q = i2;
        a();
    }

    @DexIgnore
    public final void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            this.r = typeface;
            a();
        }
    }

    @DexIgnore
    public final void b(Canvas canvas, int i2) {
        int i3 = this.d;
        float f2 = (float) (i3 * i2);
        int i4 = this.a;
        float f3 = (float) i4;
        float f4 = (float) (i3 * i2);
        float f5 = (float) (i4 - this.j);
        Paint paint = this.b;
        if (paint != null) {
            canvas.drawLine(f2, f3, f4, f5, paint);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final String b(int i2) {
        we7 we7 = we7.a;
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "Locale.getDefault()");
        String format = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    public final void a() {
        Paint paint = new Paint(1);
        this.b = paint;
        if (paint != null) {
            paint.setColor(this.t);
            Paint paint2 = this.b;
            if (paint2 != null) {
                paint2.setStrokeWidth(this.v);
                Paint paint3 = this.b;
                if (paint3 != null) {
                    paint3.setStyle(Paint.Style.STROKE);
                    Paint paint4 = new Paint(1);
                    this.c = paint4;
                    if (paint4 != null) {
                        paint4.setColor(this.p);
                        Paint paint5 = this.c;
                        if (paint5 != null) {
                            paint5.setTextSize((float) this.u);
                            Typeface typeface = this.r;
                            if (typeface == null) {
                                Paint paint6 = this.c;
                                if (paint6 == null) {
                                    ee7.a();
                                    throw null;
                                } else if (paint6 != null) {
                                    paint6.setTypeface(Typeface.create(paint6.getTypeface(), this.q));
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                Paint paint7 = this.c;
                                if (paint7 != null) {
                                    paint7.setTypeface(Typeface.create(typeface, this.q));
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                            Paint paint8 = this.c;
                            if (paint8 != null) {
                                paint8.setTextAlign(Paint.Align.CENTER);
                                invalidate();
                                requestLayout();
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2) {
        int i3 = this.d;
        float f2 = (float) (i3 * i2);
        int i4 = this.a;
        float f3 = (float) i4;
        float f4 = (float) (i3 * i2);
        float f5 = (float) (i4 - this.i);
        Paint paint = this.b;
        if (paint != null) {
            canvas.drawLine(f2, f3, f4, f5, paint);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, int i3) {
        this.e = i2;
        this.f = i3;
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        float f4 = (float) 0;
        if (f3 >= f4) {
            float f5 = (float) 1;
            if (f3 <= f5) {
                if (f2 < f4 || f2 > f5) {
                    throw new IllegalArgumentException("Long indicator height must be between 0 to 1.");
                } else if (f3 <= f2) {
                    this.g = f2;
                    this.h = f3;
                    b(f2, f3);
                    invalidate();
                    return;
                } else {
                    throw new IllegalArgumentException("Long indicator height cannot be less than sort indicator height.");
                }
            }
        }
        throw new IllegalArgumentException("Sort indicator height must be between 0 to 1.");
    }

    @DexIgnore
    public final String a(int i2) {
        RulerValuePicker.a aVar = this.s;
        if (aVar == null) {
            return b(i2);
        }
        if (aVar != null) {
            return aVar.format(i2);
        }
        ee7.a();
        throw null;
    }
}
