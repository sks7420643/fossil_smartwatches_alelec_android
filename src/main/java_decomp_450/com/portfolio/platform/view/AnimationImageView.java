package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.ee7;
import com.fossil.we7;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AnimationImageView extends AppCompatImageView {
    @DexIgnore
    public /* final */ ArrayList<String> c; // = new ArrayList<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Bitmap j;
    @DexIgnore
    public Bitmap p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public Runnable s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AnimationImageView a;

        @DexIgnore
        public a(AnimationImageView animationImageView) {
            this.a = animationImageView;
        }

        @DexIgnore
        public final void run() {
            AnimationImageView animationImageView = this.a;
            animationImageView.setImageBitmap(animationImageView.p);
            AnimationImageView animationImageView2 = this.a;
            animationImageView2.a(animationImageView2.j);
            AnimationImageView animationImageView3 = this.a;
            animationImageView3.j = animationImageView3.p;
            if (!(!this.a.c.isEmpty())) {
                return;
            }
            if (this.a.i == this.a.f) {
                AnimationImageView animationImageView4 = this.a;
                Object obj = animationImageView4.c.get(0);
                ee7.a(obj, "mListImagesPathFromAssets[0]");
                animationImageView4.p = animationImageView4.a((String) obj);
                this.a.a(0);
                return;
            }
            AnimationImageView animationImageView5 = this.a;
            Object obj2 = animationImageView5.c.get(this.a.i + 1);
            ee7.a(obj2, "mListImagesPathFromAssets[mFrameNo + 1]");
            animationImageView5.p = animationImageView5.a((String) obj2);
            AnimationImageView animationImageView6 = this.a;
            animationImageView6.a(animationImageView6.i + 1);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context) {
        super(context);
        ee7.b(context, "context");
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        Resources resources = context2.getResources();
        ee7.a((Object) resources, "context.resources");
        this.q = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        ee7.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        ee7.a((Object) resources2, "context.resources");
        this.r = resources2.getDisplayMetrics().widthPixels;
        this.s = new a(this);
    }

    @DexIgnore
    public final void d() {
        removeCallbacks(this.s);
        this.e = false;
    }

    @DexIgnore
    public final void c() {
        if (this.e) {
            this.i = 0;
            return;
        }
        this.e = true;
        removeCallbacks(this.s);
        a(1);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, int i4, int i5, int i6) {
        ee7.b(str, "imagesPathFromAssets");
        this.c.clear();
        if (i2 <= i3) {
            while (true) {
                ArrayList<String> arrayList = this.c;
                we7 we7 = we7.a;
                String format = String.format(str, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                arrayList.add(format);
                if (i2 == i3) {
                    break;
                }
                i2++;
            }
        }
        if (this.c.size() > 1) {
            this.d = i4;
            this.g = i5;
            this.h = i6;
            this.f = this.c.size() - 1;
            String str2 = this.c.get(0);
            ee7.a((Object) str2, "mListImagesPathFromAssets[0]");
            this.j = a(str2);
            String str3 = this.c.get(1);
            ee7.a((Object) str3, "mListImagesPathFromAssets[1]");
            this.p = a(str3);
            setImageBitmap(this.j);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        Resources resources = context2.getResources();
        ee7.a((Object) resources, "context.resources");
        this.q = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        ee7.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        ee7.a((Object) resources2, "context.resources");
        this.r = resources2.getDisplayMetrics().widthPixels;
        this.s = new a(this);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        Context context2 = getContext();
        ee7.a((Object) context2, "context");
        Resources resources = context2.getResources();
        ee7.a((Object) resources, "context.resources");
        this.q = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        ee7.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        ee7.a((Object) resources2, "context.resources");
        this.r = resources2.getDisplayMetrics().widthPixels;
        this.s = new a(this);
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public final void a(int i2) {
        int i3;
        if (this.e) {
            int i4 = this.d;
            if ((i2 == this.f && (i3 = this.h) > 0) || (i2 == 1 && (i3 = this.g) > 0)) {
                i4 = i3;
            }
            this.i = i2;
            try {
                postDelayed(this.s, (long) i4);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        r5.close();
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Bitmap a(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            android.content.Context r1 = r4.getContext()     // Catch:{ IOException -> 0x0029, all -> 0x0024 }
            java.lang.String r2 = "context"
            com.fossil.ee7.a(r1, r2)     // Catch:{ IOException -> 0x0029, all -> 0x0024 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x0029, all -> 0x0024 }
            java.io.InputStream r5 = r1.open(r5)     // Catch:{ IOException -> 0x0029, all -> 0x0024 }
            int r1 = r4.r     // Catch:{ IOException -> 0x0022 }
            int r2 = r4.q     // Catch:{ IOException -> 0x0022 }
            int r2 = r2 / 2
            android.graphics.Bitmap r0 = com.fossil.je5.a(r5, r1, r2)     // Catch:{ IOException -> 0x0022 }
            if (r5 == 0) goto L_0x0031
        L_0x001e:
            r5.close()
            goto L_0x0031
        L_0x0022:
            r1 = move-exception
            goto L_0x002b
        L_0x0024:
            r5 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0033
        L_0x0029:
            r1 = move-exception
            r5 = r0
        L_0x002b:
            r1.printStackTrace()     // Catch:{ all -> 0x0032 }
            if (r5 == 0) goto L_0x0031
            goto L_0x001e
        L_0x0031:
            return r0
        L_0x0032:
            r0 = move-exception
        L_0x0033:
            if (r5 == 0) goto L_0x0038
            r5.close()
        L_0x0038:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.AnimationImageView.a(java.lang.String):android.graphics.Bitmap");
    }
}
