package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.ig5;
import com.fossil.pl4;
import com.fossil.qy6;
import com.fossil.ux6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    @DexIgnore
    public /* final */ String a; // = "FlexibleAutoCompleteTextView";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = FlexibleTextView.t.a();
    @DexIgnore
    public String g; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context) {
        super(context);
        ee7.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        CharSequence text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, pl4.FlexibleAutoCompleteTextView);
                obtainStyledAttributes.getInt(5, 0);
                this.e = obtainStyledAttributes.getInt(1, 0);
                this.f = obtainStyledAttributes.getColor(6, FlexibleTextView.t.a());
                String string = obtainStyledAttributes.getString(2);
                String str = "";
                if (string == null) {
                    string = str;
                }
                this.b = string;
                String string2 = obtainStyledAttributes.getString(3);
                if (string2 == null) {
                    string2 = str;
                }
                this.c = string2;
                String string3 = obtainStyledAttributes.getString(0);
                if (string3 == null) {
                    string3 = str;
                }
                this.d = string3;
                String string4 = obtainStyledAttributes.getString(4);
                if (string4 != null) {
                    str = string4;
                }
                this.g = str;
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                if (resourceId != -1) {
                    text = a(resourceId);
                }
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                if (resourceId2 != -1) {
                    hint = a(resourceId2);
                }
                obtainStyledAttributes2.recycle();
            }
            if (!TextUtils.isEmpty(text)) {
                setText(text);
            }
            if (!TextUtils.isEmpty(hint)) {
                ee7.a((Object) hint, "hint");
                setHint(a(hint, this.e));
            }
            if (this.f != FlexibleTextView.t.a()) {
                qy6.a(this, this.f);
            }
            if (TextUtils.isEmpty(this.b)) {
                this.b = "primaryText";
            }
            if (TextUtils.isEmpty(this.c)) {
                this.c = "nonBrandTextStyle2";
            }
            a(this.b, this.c, this.d, this.g);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ee7.b(context, "context");
        ee7.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local.d(str5, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String b2 = eh5.l.a().b(str);
        Typeface c2 = eh5.l.a().c(str2);
        String b3 = eh5.l.a().b(str3);
        String b4 = eh5.l.a().b(str4);
        if (b2 != null) {
            setTextColor(Color.parseColor(b2));
        }
        if (c2 != null) {
            setTypeface(c2);
        }
        if (b3 != null) {
            setBackgroundColor(Color.parseColor(b3));
        }
        if (b4 != null) {
            setHintTextColor(Color.parseColor(b4));
        }
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i) {
        if (i == 1) {
            return ux6.a(charSequence);
        }
        if (i == 2) {
            return ux6.b(charSequence);
        }
        if (i == 3) {
            return ux6.d(charSequence);
        }
        if (i == 4) {
            return ux6.e(charSequence);
        }
        if (i != 5) {
            return charSequence;
        }
        return ux6.c(charSequence);
    }

    @DexIgnore
    public final String a(int i) {
        String a2 = ig5.a(PortfolioApp.g0.c(), i);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a2;
    }
}
