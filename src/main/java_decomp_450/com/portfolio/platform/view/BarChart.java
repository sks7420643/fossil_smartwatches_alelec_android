package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.aw;
import com.fossil.pl4;
import com.fossil.v6;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BarChart extends View {
    @DexIgnore
    public Paint A;
    @DexIgnore
    public Paint B;
    @DexIgnore
    public /* final */ Path C; // = new Path();
    @DexIgnore
    public Bitmap D;
    @DexIgnore
    public Canvas E;
    @DexIgnore
    public /* final */ List<b> a; // = new ArrayList();
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public /* final */ Rect f; // = new Rect();
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float i;
    @DexIgnore
    public String j;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public Drawable u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public GestureDetector x;
    @DexIgnore
    public View.OnClickListener y;
    @DexIgnore
    public Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return BarChart.this.y != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (BarChart.this.y != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float x2 = BarChart.this.getX();
                float y2 = BarChart.this.getY();
                float measuredWidth = ((float) (BarChart.this.getMeasuredWidth() / 2)) - x2;
                float f = (x - x2) - measuredWidth;
                float f2 = (y - y2) - measuredWidth;
                float f3 = (f * f) + (f2 * f2);
                BarChart barChart = BarChart.this;
                float f4 = barChart.c;
                if (f3 < (measuredWidth + f4) * (measuredWidth + f4)) {
                    barChart.y.onClick(barChart);
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public float b;
        @DexIgnore
        public List<Pair<Float, Integer>> c;
    }

    @DexIgnore
    public BarChart(Context context) {
        super(context);
        a();
    }

    @DexIgnore
    public void a(String str, float f2, List<Pair<Float, Integer>> list) {
        b bVar = new b();
        bVar.a = str;
        bVar.b = f2;
        bVar.c = new ArrayList(list);
        this.a.add(bVar);
    }

    @DexIgnore
    public void b() {
        for (b bVar : this.a) {
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (Pair<Float, Integer> pair : bVar.c) {
                f2 = Math.max(f2, ((Float) pair.first).floatValue());
            }
            float max = Math.max(this.b, f2);
            this.b = max;
            this.b = Math.max(max, bVar.b);
        }
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        float f3;
        super.onDraw(canvas);
        Bitmap bitmap = this.D;
        if (bitmap != null) {
            bitmap.eraseColor(0);
            this.C.reset();
            float f4 = this.g;
            float measuredHeight = ((float) getMeasuredHeight()) - f4;
            float ascent = (((f4 - this.A.ascent()) - this.A.descent()) / 2.0f) + measuredHeight;
            float measuredWidth = ((((float) getMeasuredWidth()) - this.d) - this.e) / ((float) this.a.size());
            float f5 = measuredHeight - this.p;
            float f6 = f5 - (this.v / 2.0f);
            float f7 = this.d + (measuredWidth / 2.0f);
            Iterator<b> it = this.a.iterator();
            float f8 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f9 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            while (it.hasNext()) {
                b next = it.next();
                float f10 = f5;
                for (Pair<Float, Integer> pair : next.c) {
                    this.z.setColor(((Integer) pair.second).intValue());
                    float floatValue = f10 - ((((Float) pair.first).floatValue() * f6) / this.b);
                    this.E.drawLine(f7, f10, f7, floatValue, this.z);
                    next = next;
                    f10 = floatValue;
                    f8 = f8;
                }
                Paint paint = this.A;
                String str = next.a;
                paint.getTextBounds(str, 0, str.length(), this.f);
                this.E.drawText(next.a, f7 - ((float) (this.f.width() / 2)), ascent, this.A);
                float f11 = next.b;
                float f12 = f5 - ((f6 * f11) / this.b);
                if (f12 == f9 || f11 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f2 = f8;
                    f12 = f9;
                } else {
                    f2 = f8;
                    this.C.moveTo(f2, f12);
                }
                float f13 = (this.a.indexOf(next) == 0 ? this.d : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + measuredWidth;
                if (this.a.indexOf(next) == this.a.size() - 1) {
                    f3 = this.e - (this.u != null ? this.v + (this.w * 2.0f) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                f8 = f2 + f13 + f3;
                if (f12 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && next.b > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    this.C.lineTo(f8, f12);
                }
                f7 += measuredWidth;
                f9 = f12;
            }
            this.E.drawPath(this.C, this.B);
            Drawable drawable = this.u;
            if (drawable != null && f9 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                float f14 = f8 + this.w;
                float f15 = this.v;
                drawable.setBounds((int) f14, (int) (f9 - (f15 / 2.0f)), (int) (f14 + f15), (int) (f9 + (f15 / 2.0f)));
                this.u.draw(this.E);
            }
            canvas.drawBitmap(this.D, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Bitmap bitmap = this.D;
        if (bitmap == null || i2 > bitmap.getWidth() || i3 > this.D.getHeight()) {
            this.D = aw.a(getContext()).c().a(i2, i3, Bitmap.Config.ARGB_8888);
        } else {
            this.D.setWidth(i2);
            this.D.setHeight(i3);
        }
        Canvas canvas = this.E;
        if (canvas == null) {
            this.E = new Canvas(this.D);
        } else {
            canvas.setBitmap(this.D);
        }
        b();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.x.onTouchEvent(motionEvent);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public BarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, pl4.BarChart, 0, 0);
        try {
            this.c = obtainStyledAttributes.getDimension(14, getResources().getDimension(2131165412));
            this.d = obtainStyledAttributes.getDimension(8, getResources().getDimension(2131165436));
            this.e = obtainStyledAttributes.getDimension(0, getResources().getDimension(2131165391));
            this.g = obtainStyledAttributes.getDimension(11, getResources().getDimension(2131165399));
            this.h = obtainStyledAttributes.getColor(9, v6.a(context, 2131099827));
            this.i = obtainStyledAttributes.getDimension(13, getResources().getDimension(2131165724));
            this.j = obtainStyledAttributes.getString(10);
            this.p = obtainStyledAttributes.getDimension(12, getResources().getDimension(2131165436));
            this.q = obtainStyledAttributes.getColor(1, v6.a(context, 2131099827));
            this.r = obtainStyledAttributes.getDimension(4, getResources().getDimension(2131165371));
            this.s = obtainStyledAttributes.getDimension(3, getResources().getDimension(2131165406));
            this.t = obtainStyledAttributes.getDimension(2, getResources().getDimension(2131165412));
            this.u = obtainStyledAttributes.getDrawable(5);
            this.v = obtainStyledAttributes.getDimension(7, getResources().getDimension(2131165397));
            this.w = obtainStyledAttributes.getDimension(6, getResources().getDimension(2131165425));
            obtainStyledAttributes.recycle();
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a() {
        Paint paint = new Paint(1);
        this.z = paint;
        paint.setStrokeWidth(this.c);
        Paint paint2 = new Paint(1);
        this.A = paint2;
        if (this.j != null) {
            paint2.setTypeface(Typeface.createFromAsset(getContext().getAssets(), this.j));
        }
        this.A.setTextSize(this.i);
        this.A.setColor(this.h);
        Paint paint3 = new Paint(1);
        this.B = paint3;
        paint3.setColor(this.q);
        this.B.setStrokeWidth(this.r);
        this.B.setStyle(Paint.Style.STROKE);
        this.B.setPathEffect(new DashPathEffect(new float[]{this.s, this.t}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.x = new GestureDetector(getContext(), new a());
        if (isInEditMode()) {
            Context context = getContext();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new Pair<>(Float.valueOf(123.0f), Integer.valueOf(v6.a(context, 2131099850))));
            arrayList.add(new Pair<>(Float.valueOf(234.0f), Integer.valueOf(v6.a(context, 2131099829))));
            arrayList.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(v6.a(context, 2131099847))));
            a(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, 500.0f, arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new Pair<>(Float.valueOf(234.0f), Integer.valueOf(v6.a(context, 2131099850))));
            arrayList2.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(v6.a(context, 2131099829))));
            arrayList2.add(new Pair<>(Float.valueOf(456.0f), Integer.valueOf(v6.a(context, 2131099847))));
            a("M", 1000.0f, arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(v6.a(context, 2131099850))));
            arrayList3.add(new Pair<>(Float.valueOf(456.0f), Integer.valueOf(v6.a(context, 2131099829))));
            arrayList3.add(new Pair<>(Float.valueOf(678.0f), Integer.valueOf(v6.a(context, 2131099847))));
            a("T", 1500.0f, arrayList3);
            this.b = 1479.0f;
            this.u = v6.c(context, 2131230888);
        }
    }
}
