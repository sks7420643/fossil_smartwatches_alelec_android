package com.portfolio.platform.view.indicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.mz6;
import com.fossil.pl;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.we5;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomPageIndicator extends View implements mz6 {
    @DexIgnore
    public HashMap<Integer, Integer> A;
    @DexIgnore
    public List<a> B;
    @DexIgnore
    public Rect C;
    @DexIgnore
    public float a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public RecyclerView f;
    @DexIgnore
    public ViewPager g;
    @DexIgnore
    public ViewPager.i h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public int v;
    @DexIgnore
    public float w;
    @DexIgnore
    public int x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.CustomPageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.CustomPageIndicator$a$a  reason: collision with other inner class name */
        public static final class C0312a {
            @DexIgnore
            public C0312a() {
            }

            @DexIgnore
            public /* synthetic */ C0312a(zd7 zd7) {
                this();
            }
        }

        /*
        static {
            new C0312a(null);
        }
        */

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a(null);
        @DexIgnore
        public int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public b createFromParcel(Parcel parcel) {
                ee7.b(parcel, "parcel");
                return new b(parcel);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            ee7.b(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            ee7.b(parcel, "dest");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            ee7.b(parcel, "in");
            this.a = parcel.readInt();
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context) {
        super(context);
        ee7.b(context, "context");
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.w = -1.0f;
        this.x = -1;
        this.A = new HashMap<>();
        this.B = new ArrayList();
        this.C = new Rect();
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void b(int i2) {
        int i3;
        if (this.s || this.p == 0) {
            if (we5.a(getContext())) {
                ViewPager viewPager = this.g;
                pl adapter = viewPager != null ? viewPager.getAdapter() : null;
                if (adapter != null) {
                    ee7.a((Object) adapter, "mViewPager?.adapter!!");
                    i3 = (adapter.a() - i2) - 1;
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                i3 = i2;
            }
            this.i = i3;
            this.j = i2;
            invalidate();
        }
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        for (a aVar : this.B) {
            if (aVar.b() == i2) {
                return aVar.a();
            }
        }
        return 0;
    }

    @DexIgnore
    public final int d(int i2) {
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.g) == null) {
            return size;
        }
        if (viewPager != null) {
            pl adapter = viewPager.getAdapter();
            if (adapter != null) {
                ee7.a((Object) adapter, "mViewPager!!.adapter!!");
                int a2 = adapter.a();
                float f2 = this.a;
                int paddingLeft = (int) (((float) getPaddingLeft()) + ((float) getPaddingRight()) + (((float) a2) * 2.0f * f2) + (((float) (a2 - 1)) * f2) + 1.0f);
                if (mode == Integer.MIN_VALUE) {
                    return Math.min(paddingLeft, size);
                }
                return paddingLeft;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final int e(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((((float) 2) * this.a) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public final int getMCurrentPage$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public final ViewPager.i getMListener$app_fossilRelease() {
        return this.h;
    }

    @DexIgnore
    public final RecyclerView getMRecyclerView$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r19) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            java.lang.String r2 = "canvas"
            com.fossil.ee7.b(r1, r2)
            super.onDraw(r19)
            androidx.viewpager.widget.ViewPager r2 = r0.g
            r3 = 0
            if (r2 == 0) goto L_0x003b
            if (r2 == 0) goto L_0x0037
            com.fossil.pl r2 = r2.getAdapter()
            if (r2 == 0) goto L_0x003b
            androidx.viewpager.widget.ViewPager r2 = r0.g
            if (r2 == 0) goto L_0x0033
            com.fossil.pl r2 = r2.getAdapter()
            if (r2 == 0) goto L_0x002f
            java.lang.String r4 = "mViewPager!!.adapter!!"
            com.fossil.ee7.a(r2, r4)
            int r2 = r2.a()
            r0.z = r2
            goto L_0x0069
        L_0x002f:
            com.fossil.ee7.a()
            throw r3
        L_0x0033:
            com.fossil.ee7.a()
            throw r3
        L_0x0037:
            com.fossil.ee7.a()
            throw r3
        L_0x003b:
            androidx.recyclerview.widget.RecyclerView r2 = r0.f
            if (r2 == 0) goto L_0x0069
            if (r2 == 0) goto L_0x0065
            androidx.recyclerview.widget.RecyclerView$g r2 = r2.getAdapter()
            if (r2 == 0) goto L_0x0069
            androidx.recyclerview.widget.RecyclerView r2 = r0.f
            if (r2 == 0) goto L_0x0061
            androidx.recyclerview.widget.RecyclerView$g r2 = r2.getAdapter()
            if (r2 == 0) goto L_0x005d
            java.lang.String r4 = "mRecyclerView!!.adapter!!"
            com.fossil.ee7.a(r2, r4)
            int r2 = r2.getItemCount()
            r0.z = r2
            goto L_0x0069
        L_0x005d:
            com.fossil.ee7.a()
            throw r3
        L_0x0061:
            com.fossil.ee7.a()
            throw r3
        L_0x0065:
            com.fossil.ee7.a()
            throw r3
        L_0x0069:
            int r2 = r0.z
            if (r2 != 0) goto L_0x006e
            return
        L_0x006e:
            int r4 = r0.i
            if (r4 < r2) goto L_0x0078
            int r2 = r2 + -1
            r0.setCurrentItem(r2)
            return
        L_0x0078:
            int r2 = r0.q
            r4 = 2
            if (r2 != 0) goto L_0x0091
            int r2 = r18.getWidth()
            int r5 = r18.getPaddingLeft()
            int r6 = r18.getPaddingRight()
            int r7 = r18.getHeight()
            float r7 = (float) r7
            float r8 = r0.a
            goto L_0x00a4
        L_0x0091:
            int r2 = r18.getHeight()
            int r5 = r18.getPaddingTop()
            int r6 = r18.getPaddingBottom()
            int r7 = r18.getWidth()
            float r7 = (float) r7
            float r8 = r0.a
        L_0x00a4:
            float r9 = (float) r4
            float r8 = r8 * r9
            float r7 = r7 - r8
            float r7 = r7 / r9
            int r7 = (int) r7
            float r8 = r0.a
            float r9 = (float) r4
            float r9 = r9 * r8
            float r10 = r0.t
            float r9 = r9 + r10
            float r7 = (float) r7
            float r7 = r7 + r8
            float r10 = (float) r5
            float r10 = r10 + r8
            boolean r8 = r0.r
            r11 = 1073741824(0x40000000, float:2.0)
            if (r8 == 0) goto L_0x00c8
            int r2 = r2 - r5
            int r2 = r2 - r6
            float r2 = (float) r2
            float r2 = r2 / r11
            int r5 = r0.z
            float r5 = (float) r5
            float r5 = r5 * r9
            float r5 = r5 / r11
            float r2 = r2 - r5
            float r10 = r10 + r2
        L_0x00c8:
            float r2 = r0.a
            android.graphics.Paint r5 = r0.c
            float r5 = r5.getStrokeWidth()
            r6 = 0
            float r8 = (float) r6
            int r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x00de
            android.graphics.Paint r5 = r0.c
            float r5 = r5.getStrokeWidth()
            float r5 = r5 / r11
            float r2 = r2 - r5
        L_0x00de:
            r0.a(r1, r10, r9)
            int r5 = r0.z
            r8 = 0
        L_0x00e4:
            if (r8 >= r5) goto L_0x01ac
            float r11 = (float) r8
            float r11 = r11 * r9
            float r11 = r11 + r10
            int r12 = r0.q
            if (r12 != 0) goto L_0x00f0
            r12 = r7
            goto L_0x00f2
        L_0x00f0:
            r12 = r11
            r11 = r7
        L_0x00f2:
            boolean r13 = r0.s
            if (r13 == 0) goto L_0x00f9
            int r13 = r0.j
            goto L_0x00fb
        L_0x00f9:
            int r13 = r0.i
        L_0x00fb:
            androidx.recyclerview.widget.RecyclerView r14 = r0.f
            if (r14 == 0) goto L_0x0118
            if (r14 == 0) goto L_0x0114
            androidx.recyclerview.widget.RecyclerView$g r14 = r14.getAdapter()
            if (r14 == 0) goto L_0x0110
            int r14 = r14.getItemViewType(r8)
            int r14 = r0.c(r14)
            goto L_0x012c
        L_0x0110:
            com.fossil.ee7.a()
            throw r3
        L_0x0114:
            com.fossil.ee7.a()
            throw r3
        L_0x0118:
            java.util.HashMap<java.lang.Integer, java.lang.Integer> r14 = r0.A
            java.lang.Integer r15 = java.lang.Integer.valueOf(r8)
            java.lang.Object r14 = r14.get(r15)
            java.lang.Integer r14 = (java.lang.Integer) r14
            if (r14 == 0) goto L_0x012b
            int r14 = r14.intValue()
            goto L_0x012c
        L_0x012b:
            r14 = 0
        L_0x012c:
            if (r14 == 0) goto L_0x017d
            android.content.Context r15 = r18.getContext()
            android.graphics.drawable.Drawable r14 = com.fossil.v6.c(r15, r14)
            android.graphics.Rect r15 = r0.C
            float r3 = r0.a
            float r6 = r11 - r3
            int r6 = (int) r6
            float r4 = r12 - r3
            int r4 = (int) r4
            float r11 = r11 - r3
            int r11 = (int) r11
            r17 = r5
            int r5 = (int) r3
            r16 = 2
            int r5 = r5 * 2
            int r11 = r11 + r5
            float r12 = r12 - r3
            int r5 = (int) r12
            int r3 = (int) r3
            int r3 = r3 * 2
            int r5 = r5 + r3
            r15.set(r6, r4, r11, r5)
            if (r14 == 0) goto L_0x015a
            android.graphics.Rect r3 = r0.C
            r14.setBounds(r3)
        L_0x015a:
            if (r8 == r13) goto L_0x016a
            if (r14 == 0) goto L_0x0177
            android.graphics.Paint r3 = r0.b
            int r3 = r3.getColor()
            android.graphics.PorterDuff$Mode r4 = android.graphics.PorterDuff.Mode.SRC_IN
            r14.setColorFilter(r3, r4)
            goto L_0x0177
        L_0x016a:
            if (r14 == 0) goto L_0x0177
            android.graphics.Paint r3 = r0.d
            int r3 = r3.getColor()
            android.graphics.PorterDuff$Mode r4 = android.graphics.PorterDuff.Mode.SRC_IN
            r14.setColorFilter(r3, r4)
        L_0x0177:
            if (r14 == 0) goto L_0x01a3
            r14.draw(r1)
            goto L_0x01a3
        L_0x017d:
            r17 = r5
            r16 = 2
            if (r8 != r13) goto L_0x018b
            float r3 = r0.a
            android.graphics.Paint r4 = r0.d
            r1.drawCircle(r11, r12, r3, r4)
            goto L_0x0198
        L_0x018b:
            android.graphics.Paint r3 = r0.b
            int r3 = r3.getAlpha()
            if (r3 <= 0) goto L_0x0198
            android.graphics.Paint r3 = r0.b
            r1.drawCircle(r11, r12, r2, r3)
        L_0x0198:
            float r3 = r0.a
            int r4 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r4 == 0) goto L_0x01a3
            android.graphics.Paint r4 = r0.c
            r1.drawCircle(r11, r12, r3, r4)
        L_0x01a3:
            int r8 = r8 + 1
            r5 = r17
            r3 = 0
            r4 = 2
            r6 = 0
            goto L_0x00e4
        L_0x01ac:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.indicator.CustomPageIndicator.onDraw(android.graphics.Canvas):void");
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.q == 0) {
            setMeasuredDimension(d(i2), e(i3));
        } else {
            setMeasuredDimension(e(i2), d(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        ee7.b(parcelable, "state");
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        this.i = bVar.a();
        this.j = bVar.a();
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.a(this.i);
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0082, code lost:
        if (r11.a() != false) goto L_0x0089;
     */
    @DexIgnore
    @android.annotation.SuppressLint({"ClickableViewAccessibility"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            java.lang.String r0 = "ev"
            com.fossil.ee7.b(r11, r0)
            boolean r0 = super.onTouchEvent(r11)
            r1 = 1
            if (r0 == 0) goto L_0x000d
            return r1
        L_0x000d:
            androidx.viewpager.widget.ViewPager r0 = r10.g
            r2 = 0
            if (r0 == 0) goto L_0x016d
            r3 = 0
            if (r0 == 0) goto L_0x0169
            com.fossil.pl r0 = r0.getAdapter()
            if (r0 == 0) goto L_0x016d
            androidx.viewpager.widget.ViewPager r0 = r10.g
            if (r0 == 0) goto L_0x0165
            com.fossil.pl r0 = r0.getAdapter()
            if (r0 == 0) goto L_0x0161
            java.lang.String r4 = "mViewPager!!.adapter!!"
            com.fossil.ee7.a(r0, r4)
            int r0 = r0.a()
            if (r0 != 0) goto L_0x0032
            goto L_0x016d
        L_0x0032:
            int r0 = r11.getAction()
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r0 != 0) goto L_0x0048
            int r0 = r11.getPointerId(r2)
            r10.x = r0
            float r11 = r11.getX()
            r10.w = r11
            goto L_0x0160
        L_0x0048:
            r5 = 2
            if (r0 != r5) goto L_0x009a
            int r0 = r10.x
            int r0 = r11.findPointerIndex(r0)
            float r11 = r11.getX(r0)
            float r0 = r10.w
            float r0 = r11 - r0
            boolean r2 = r10.y
            if (r2 != 0) goto L_0x006a
            float r2 = java.lang.Math.abs(r0)
            int r4 = r10.v
            float r4 = (float) r4
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x006a
            r10.y = r1
        L_0x006a:
            boolean r2 = r10.y
            if (r2 == 0) goto L_0x0160
            r10.w = r11
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x0096
            boolean r11 = r11.g()
            if (r11 != 0) goto L_0x0089
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x0085
            boolean r11 = r11.a()
            if (r11 == 0) goto L_0x0160
            goto L_0x0089
        L_0x0085:
            com.fossil.ee7.a()
            throw r3
        L_0x0089:
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x0092
            r11.b(r0)
            goto L_0x0160
        L_0x0092:
            com.fossil.ee7.a()
            throw r3
        L_0x0096:
            com.fossil.ee7.a()
            throw r3
        L_0x009a:
            r5 = 3
            if (r0 != r5) goto L_0x009e
            goto L_0x00a0
        L_0x009e:
            if (r0 != r1) goto L_0x0128
        L_0x00a0:
            boolean r6 = r10.y
            if (r6 != 0) goto L_0x0109
            androidx.viewpager.widget.ViewPager r6 = r10.g
            if (r6 == 0) goto L_0x0105
            com.fossil.pl r6 = r6.getAdapter()
            if (r6 == 0) goto L_0x0101
            com.fossil.ee7.a(r6, r4)
            int r4 = r6.a()
            int r6 = r10.getWidth()
            float r6 = (float) r6
            r7 = 1073741824(0x40000000, float:2.0)
            float r7 = r6 / r7
            r8 = 1086324736(0x40c00000, float:6.0)
            float r6 = r6 / r8
            int r8 = r10.i
            if (r8 <= 0) goto L_0x00e1
            float r8 = r11.getX()
            float r9 = r7 - r6
            int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
            if (r8 >= 0) goto L_0x00e1
            if (r0 == r5) goto L_0x00e0
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x00dc
            int r0 = r10.i
            int r0 = r0 - r1
            r11.setCurrentItem(r0)
            goto L_0x00e0
        L_0x00dc:
            com.fossil.ee7.a()
            throw r3
        L_0x00e0:
            return r1
        L_0x00e1:
            int r8 = r10.i
            int r4 = r4 - r1
            if (r8 >= r4) goto L_0x0109
            float r11 = r11.getX()
            float r7 = r7 + r6
            int r11 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r11 <= 0) goto L_0x0109
            if (r0 == r5) goto L_0x0100
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x00fc
            int r0 = r10.i
            int r0 = r0 + r1
            r11.setCurrentItem(r0)
            goto L_0x0100
        L_0x00fc:
            com.fossil.ee7.a()
            throw r3
        L_0x0100:
            return r1
        L_0x0101:
            com.fossil.ee7.a()
            throw r3
        L_0x0105:
            com.fossil.ee7.a()
            throw r3
        L_0x0109:
            r10.y = r2
            r11 = -1
            r10.x = r11
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x0124
            boolean r11 = r11.g()
            if (r11 == 0) goto L_0x0160
            androidx.viewpager.widget.ViewPager r11 = r10.g
            if (r11 == 0) goto L_0x0120
            r11.d()
            goto L_0x0160
        L_0x0120:
            com.fossil.ee7.a()
            throw r3
        L_0x0124:
            com.fossil.ee7.a()
            throw r3
        L_0x0128:
            r3 = 5
            if (r0 != r3) goto L_0x013c
            int r0 = r11.getActionIndex()
            float r2 = r11.getX(r0)
            r10.w = r2
            int r11 = r11.getPointerId(r0)
            r10.x = r11
            goto L_0x0160
        L_0x013c:
            r3 = 6
            if (r0 != r3) goto L_0x0160
            int r0 = r11.getActionIndex()
            int r3 = r11.getPointerId(r0)
            int r4 = r10.x
            if (r3 != r4) goto L_0x0154
            if (r0 != 0) goto L_0x014e
            r2 = 1
        L_0x014e:
            int r0 = r11.getPointerId(r2)
            r10.x = r0
        L_0x0154:
            int r0 = r10.x
            int r0 = r11.findPointerIndex(r0)
            float r11 = r11.getX(r0)
            r10.w = r11
        L_0x0160:
            return r1
        L_0x0161:
            com.fossil.ee7.a()
            throw r3
        L_0x0165:
            com.fossil.ee7.a()
            throw r3
        L_0x0169:
            com.fossil.ee7.a()
            throw r3
        L_0x016d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.indicator.CustomPageIndicator.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.g == null && this.f == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.g;
        if (viewPager == null) {
            if (we5.a(getContext())) {
                RecyclerView recyclerView = this.f;
                if (recyclerView != null) {
                    RecyclerView.g adapter = recyclerView.getAdapter();
                    if (adapter != null) {
                        ee7.a((Object) adapter, "mRecyclerView!!.adapter!!");
                        i2 = (adapter.getItemCount() - i2) - 1;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.i = i2;
        } else if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (we5.a(getContext())) {
                ViewPager viewPager2 = this.g;
                if (viewPager2 != null) {
                    pl adapter2 = viewPager2.getAdapter();
                    if (adapter2 != null) {
                        ee7.a((Object) adapter2, "mViewPager!!.adapter!!");
                        i2 = (adapter2.a() - i2) - 1;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.i = i2;
        } else {
            ee7.a();
            throw null;
        }
        invalidate();
    }

    @DexIgnore
    public final void setCurrentPage(int i2) {
        if (i2 < this.z) {
            this.i = i2;
            invalidate();
        }
    }

    @DexIgnore
    public final void setMCurrentPage$app_fossilRelease(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(ViewPager.i iVar) {
        this.h = iVar;
    }

    @DexIgnore
    public final void setMRecyclerView$app_fossilRelease(RecyclerView recyclerView) {
        this.f = recyclerView;
    }

    @DexIgnore
    public final void setNumberOfPage(int i2) {
        if (i2 > 0) {
            this.z = i2;
            invalidate();
        }
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!ee7.a(this.g, viewPager)) {
            ViewPager viewPager2 = this.g;
            if (viewPager2 != null) {
                viewPager2.b((ViewPager.i) this);
            }
            if ((viewPager != null ? viewPager.getAdapter() : null) != null) {
                this.g = viewPager;
                if (viewPager != null) {
                    viewPager.a((ViewPager.i) this);
                }
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2) {
        this.p = i2;
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        this.i = i2;
        invalidate();
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public final void a(HashMap<Integer, Integer> hashMap) {
        ee7.b(hashMap, Constants.MAP);
        this.A.clear();
        this.A = hashMap;
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3) {
        if (this.u) {
            float f4 = f2 - f3;
            float f5 = f2 + (((float) (this.z - 1)) * f3) + f3;
            float height = (float) getHeight();
            float f6 = height - LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f7 = (float) 2;
            canvas.drawRoundRect(f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5, height, Math.abs(f6) / f7, Math.abs(f6) / f7, this.e);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969879);
        ee7.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ee7.b(context, "context");
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.w = -1.0f;
        this.x = -1;
        this.A = new HashMap<>();
        this.B = new ArrayList();
        this.C = new Rect();
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = v6.a(context, 2131099842);
            int a3 = v6.a(context, 2131099829);
            int a4 = v6.a(context, (int) R.color.transparent);
            float dimension = resources.getDimension(2131165425);
            float dimension2 = resources.getDimension(2131165439);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, pl4.LinePageIndicator, i2, 0);
            this.r = obtainStyledAttributes.getBoolean(4, true);
            this.q = obtainStyledAttributes.getInt(0, 0);
            this.b.setStyle(Paint.Style.FILL);
            this.b.setColor(obtainStyledAttributes.getColor(5, a2));
            this.c.setStyle(Paint.Style.STROKE);
            this.c.setColor(obtainStyledAttributes.getColor(9, a4));
            this.c.setStrokeWidth(obtainStyledAttributes.getDimension(10, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.d.setStyle(Paint.Style.FILL);
            this.d.setColor(obtainStyledAttributes.getColor(3, a3));
            String b2 = eh5.l.a().b("primaryColor");
            if (!TextUtils.isEmpty(b2)) {
                this.d.setColor(Color.parseColor(b2));
            }
            this.a = obtainStyledAttributes.getDimension(6, dimension);
            this.s = obtainStyledAttributes.getBoolean(8, false);
            this.t = obtainStyledAttributes2.getDimension(1, dimension2);
            this.u = obtainStyledAttributes.getBoolean(7, false);
            this.e.setColor(obtainStyledAttributes.getColor(2, 0));
            this.e.setStyle(Paint.Style.FILL);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            ee7.a((Object) viewConfiguration, "configuration");
            this.v = viewConfiguration.getScaledPagingTouchSlop();
        }
    }
}
