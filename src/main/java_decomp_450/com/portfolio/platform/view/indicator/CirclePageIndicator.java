package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.mz6;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.we5;
import com.misfit.frameworks.buttonservice.R;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CirclePageIndicator extends View implements mz6 {
    @DexIgnore
    public float a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public RecyclerView e;
    @DexIgnore
    public ViewPager f;
    @DexIgnore
    public ViewPager.i g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public int u;
    @DexIgnore
    public float v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0311a();
        @DexIgnore
        public int a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.CirclePageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.CirclePageIndicator$a$a  reason: collision with other inner class name */
        public static class C0311a implements Parcelable.Creator<a> {
            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }

        @DexIgnore
        public a(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
        }
    }

    @DexIgnore
    public CirclePageIndicator(Context context) {
        this(context, null);
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void b(int i2) {
        if (this.s || this.p == 0) {
            this.h = we5.a(getContext()) ? (this.f.getAdapter().a() - i2) - 1 : i2;
            this.i = i2;
            invalidate();
        }
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.f) == null) {
            return size;
        }
        int a2 = viewPager.getAdapter().a();
        float f2 = this.a;
        int paddingLeft = (int) (((float) (getPaddingLeft() + getPaddingRight())) + (((float) (a2 * 2)) * f2) + (((float) (a2 - 1)) * f2) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
    }

    @DexIgnore
    public final int d(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((this.a * 2.0f) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        float f2;
        float f3;
        super.onDraw(canvas);
        ViewPager viewPager = this.f;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.e;
            i2 = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.e.getAdapter().getItemCount();
        } else {
            i2 = this.f.getAdapter().a();
        }
        if (i2 != 0) {
            if (this.h >= i2) {
                setCurrentItem(i2 - 1);
                return;
            }
            if (this.q == 0) {
                i6 = getWidth();
                i5 = getPaddingLeft();
                i4 = getPaddingRight();
                i3 = getPaddingTop();
            } else {
                i6 = getHeight();
                i5 = getPaddingTop();
                i4 = getPaddingBottom();
                i3 = getPaddingLeft();
            }
            float f4 = this.a;
            float f5 = (f4 * 2.0f) + this.t;
            float f6 = ((float) i3) + f4;
            float f7 = ((float) i5) + f4;
            if (this.r) {
                f7 += (((float) ((i6 - i5) - i4)) / 2.0f) - ((((float) i2) * f5) / 2.0f);
            }
            float f8 = this.a;
            if (this.c.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f8 -= this.c.getStrokeWidth() / 2.0f;
            }
            for (int i7 = 0; i7 < i2; i7++) {
                float f9 = (((float) i7) * f5) + f7;
                if (this.q == 0) {
                    f3 = f6;
                } else {
                    f3 = f9;
                    f9 = f6;
                }
                if (this.b.getAlpha() > 0) {
                    canvas.drawCircle(f9, f3, f8, this.b);
                }
                float f10 = this.a;
                if (f8 != f10) {
                    canvas.drawCircle(f9, f3, f10, this.c);
                }
            }
            float f11 = ((float) (this.s ? this.i : this.h)) * f5;
            if (!this.s) {
                f11 += this.j * f5;
            }
            if (this.q == 0) {
                float f12 = f7 + f11;
                f2 = f6;
                f6 = f12;
            } else {
                f2 = f7 + f11;
            }
            canvas.drawCircle(f6, f2, this.a, this.d);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.q == 0) {
            setMeasuredDimension(c(i2), d(i3));
        } else {
            setMeasuredDimension(d(i2), c(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        int i2 = aVar.a;
        this.h = i2;
        this.i = i2;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.a = this.h;
        return aVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.f;
        int i2 = 0;
        if (viewPager == null || viewPager.getAdapter() == null || this.f.getAdapter().a() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.w));
                    float f2 = x2 - this.v;
                    if (!this.x && Math.abs(f2) > ((float) this.u)) {
                        this.x = true;
                    }
                    if (this.x) {
                        this.v = x2;
                        if (this.f.g() || this.f.a()) {
                            this.f.b(f2);
                        }
                    }
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.v = motionEvent.getX(actionIndex);
                        this.w = motionEvent.getPointerId(actionIndex);
                    } else if (action == 6) {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.w) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.w = motionEvent.getPointerId(i2);
                        }
                        this.v = motionEvent.getX(motionEvent.findPointerIndex(this.w));
                    }
                }
            }
            if (!this.x) {
                int a2 = this.f.getAdapter().a();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.h > 0 && motionEvent.getX() < f3 - f4) {
                    if (action != 3) {
                        this.f.setCurrentItem(this.h - 1);
                    }
                    return true;
                } else if (this.h < a2 - 1 && motionEvent.getX() > f3 + f4) {
                    if (action != 3) {
                        this.f.setCurrentItem(this.h + 1);
                    }
                    return true;
                }
            }
            this.x = false;
            this.w = -1;
            if (this.f.g()) {
                this.f.d();
            }
        } else {
            this.w = motionEvent.getPointerId(0);
            this.v = motionEvent.getX();
        }
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.f == null && this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.f;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (we5.a(getContext())) {
                i2 = (this.f.getAdapter().a() - i2) - 1;
            }
            this.h = i2;
        } else {
            if (we5.a(getContext())) {
                i2 = (this.e.getAdapter().getItemCount() - i2) - 1;
            }
            this.h = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.g = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.f, viewPager)) {
            ViewPager viewPager2 = this.f;
            if (viewPager2 != null) {
                viewPager2.b((ViewPager.i) this);
            }
            if (viewPager.getAdapter() != null) {
                this.f = viewPager;
                viewPager.a((ViewPager.i) this);
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969879);
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.v = -1.0f;
        this.w = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = v6.a(context, 2131099842);
            int a3 = v6.a(context, 2131099829);
            int a4 = v6.a(context, (int) R.color.transparent);
            float dimension = resources.getDimension(2131165425);
            float dimension2 = resources.getDimension(2131165439);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, pl4.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, pl4.LinePageIndicator, i2, 0);
            this.r = obtainStyledAttributes.getBoolean(4, true);
            this.q = obtainStyledAttributes.getInt(0, 0);
            this.b.setStyle(Paint.Style.FILL);
            this.b.setColor(obtainStyledAttributes.getColor(5, a2));
            this.c.setStyle(Paint.Style.STROKE);
            this.c.setColor(obtainStyledAttributes.getColor(9, a4));
            this.c.setStrokeWidth(obtainStyledAttributes.getDimension(10, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.d.setStyle(Paint.Style.FILL);
            this.d.setColor(obtainStyledAttributes.getColor(3, a3));
            this.a = obtainStyledAttributes.getDimension(6, dimension);
            this.s = obtainStyledAttributes.getBoolean(8, false);
            this.t = obtainStyledAttributes2.getDimension(1, dimension2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            this.u = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2) {
        this.p = i2;
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        this.h = i2;
        this.j = f2;
        invalidate();
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }
}
