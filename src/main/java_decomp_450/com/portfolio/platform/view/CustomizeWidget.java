package com.portfolio.platform.view;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.ee7;
import com.fossil.eh5;
import com.fossil.hd5;
import com.fossil.j9;
import com.fossil.jy6;
import com.fossil.pl4;
import com.fossil.v6;
import com.fossil.x87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeWidget extends ConstraintLayout {
    @DexIgnore
    public ProgressBar A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public Integer C;
    @DexIgnore
    public Integer D;
    @DexIgnore
    public Integer E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public Drawable I;
    @DexIgnore
    public Drawable J;
    @DexIgnore
    public Drawable K;
    @DexIgnore
    public String L;
    @DexIgnore
    public String M;
    @DexIgnore
    public String N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public float Q; // = -1.0f;
    @DexIgnore
    public float R; // = -1.0f;
    @DexIgnore
    public String S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public float W;
    @DexIgnore
    public String a0;
    @DexIgnore
    public int b0;
    @DexIgnore
    public String c0;
    @DexIgnore
    public int d0;
    @DexIgnore
    public String e0;
    @DexIgnore
    public int f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h0;
    @DexIgnore
    public boolean i0;
    @DexIgnore
    public boolean j0;
    @DexIgnore
    public Drawable k0;
    @DexIgnore
    public /* final */ ColorDrawable l0;
    @DexIgnore
    public boolean m0;
    @DexIgnore
    public View v;
    @DexIgnore
    public View w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public TextView y;
    @DexIgnore
    public TextView z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CustomizeWidget customizeWidget);
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements j9.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;
        @DexIgnore
        public /* final */ /* synthetic */ float c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Intent e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends View.DragShadowBuilder {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ j9 b;
            @DexIgnore
            public /* final */ /* synthetic */ View c;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, j9 j9Var, View view, View view2) {
                super(view2);
                this.a = dVar;
                this.b = j9Var;
                this.c = view;
            }

            @DexIgnore
            public void onDrawShadow(Canvas canvas) {
                ee7.b(canvas, "canvas");
                float f = this.a.c;
                canvas.scale(f, f);
                View view = getView();
                if (view != null) {
                    view.draw(canvas);
                }
                this.a.a.setDragMode(true);
            }

            @DexIgnore
            public void onProvideShadowMetrics(Point point, Point point2) {
                ee7.b(point, "shadowSize");
                ee7.b(point2, "shadowTouchPoint");
                super.onProvideShadowMetrics(point, point2);
                View view = getView();
                ee7.a((Object) view, "view");
                int width = (int) (((float) view.getWidth()) * this.a.c);
                View view2 = getView();
                ee7.a((Object) view2, "view");
                int height = (int) (((float) view2.getHeight()) * this.a.c);
                point.set(width, height);
                point2.set(width / 2, height / 2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WidgetControl", "onDragStart - shadowTouchPoint=" + point2);
                j9 j9Var = this.b;
                if (j9Var != null) {
                    j9Var.a(point2);
                }
            }
        }

        @DexIgnore
        public d(CustomizeWidget customizeWidget, b bVar, float f, String str, Intent intent) {
            this.a = customizeWidget;
            this.b = bVar;
            this.c = f;
            this.d = str;
            this.e = intent;
        }

        @DexIgnore
        @Override // com.fossil.j9.c
        public final boolean a(View view, j9 j9Var) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onDragStart - v=");
            sb.append(view != null ? Integer.valueOf(view.getId()) : null);
            sb.append(", helper=");
            sb.append(j9Var);
            local.d("WidgetControl", sb.toString());
            b bVar = this.b;
            if (bVar != null) {
                bVar.a(this.a);
            }
            a aVar = new a(this, j9Var, view, view);
            ClipData clipData = new ClipData(new ClipDescription(this.d, new String[]{"text/plain"}), new ClipData.Item(this.e));
            FLogger.INSTANCE.getLocal().d("WidgetControl", "onDragStart - created ClipDescription");
            if (Build.VERSION.SDK_INT >= 24) {
                return this.a.startDragAndDrop(clipData, aVar, null, 257);
            }
            return this.a.startDrag(clipData, aVar, null, 257);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public CustomizeWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String b2;
        String b3;
        int parseColor = Color.parseColor("#FFFFFF");
        this.B = parseColor;
        this.F = parseColor;
        this.G = parseColor;
        this.H = parseColor;
        this.O = parseColor;
        this.P = parseColor;
        this.T = parseColor;
        this.U = parseColor;
        this.V = -1.0f;
        this.W = -1.0f;
        this.b0 = parseColor;
        String str = "";
        this.c0 = str;
        this.d0 = parseColor;
        this.e0 = str;
        this.f0 = parseColor;
        this.g0 = -1.0f;
        this.h0 = -1.0f;
        this.l0 = new ColorDrawable(v6.a(getContext(), (int) R.color.transparent));
        TypedArray typedArray = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558838, (ViewGroup) this, true) : null;
        if (inflate != null) {
            View findViewById = findViewById(2131362961);
            ee7.a((Object) findViewById, "findViewById(R.id.root)");
            this.v = findViewById;
            findViewById.setElevation(getElevation());
            View findViewById2 = findViewById(2131362553);
            ee7.a((Object) findViewById2, "findViewById(R.id.holder)");
            this.w = findViewById2;
            View findViewById3 = inflate.findViewById(2131362729);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.iv_top)");
            this.x = (ImageView) findViewById3;
            View findViewById4 = inflate.findViewById(2131363345);
            ee7.a((Object) findViewById4, "view.findViewById(R.id.tv_top)");
            this.y = (TextView) findViewById4;
            View findViewById5 = inflate.findViewById(2131363224);
            ee7.a((Object) findViewById5, "view.findViewById(R.id.tv_bottom)");
            this.z = (TextView) findViewById5;
            View findViewById6 = inflate.findViewById(2131362888);
            ee7.a((Object) findViewById6, "view.findViewById(R.id.pb_progress)");
            this.A = (ProgressBar) findViewById6;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, pl4.WidgetControl) : typedArray;
            if (typedArray != null) {
                this.C = Integer.valueOf(typedArray.getResourceId(0, 2131230872));
                this.D = Integer.valueOf(typedArray.getResourceId(2, 2131230874));
                typedArray.getResourceId(2, 2131230874);
                this.E = this.C;
                this.I = typedArray.getDrawable(22);
                this.a0 = typedArray.getString(7);
                this.J = typedArray.getDrawable(21);
                this.K = typedArray.getDrawable(23);
                this.N = typedArray.getString(16);
                this.O = typedArray.getColor(17, this.B);
                this.P = typedArray.getColor(18, this.B);
                this.Q = typedArray.getDimension(19, -1.0f);
                this.R = typedArray.getDimension(20, -1.0f);
                this.S = typedArray.getString(4);
                this.T = typedArray.getColor(5, this.B);
                this.U = typedArray.getColor(6, this.B);
                this.V = typedArray.getDimension(8, -1.0f);
                this.W = typedArray.getDimension(9, -1.0f);
                String string = typedArray.getString(10);
                this.c0 = string == null ? str : string;
                String string2 = typedArray.getString(11);
                this.e0 = string2 != null ? string2 : str;
                this.V = typedArray.getDimension(12, -1.0f);
                this.h0 = typedArray.getDimension(13, -1.0f);
                this.i0 = typedArray.getBoolean(15, false);
                boolean z2 = typedArray.getBoolean(14, false);
                this.j0 = z2;
                setRemoveMode(z2);
                typedArray.recycle();
            }
            if (!TextUtils.isEmpty(this.c0) && (b3 = eh5.l.a().b(this.c0)) != null) {
                int parseColor2 = Color.parseColor(b3);
                this.f0 = parseColor2;
                this.b0 = parseColor2;
            }
            if (!TextUtils.isEmpty(this.e0) && (b2 = eh5.l.a().b(this.e0)) != null) {
                this.d0 = Color.parseColor(b2);
            }
            if (!this.i0) {
                this.x.setImageTintList(ColorStateList.valueOf(this.b0));
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setTopIconSrc(Drawable drawable) {
        this.J = drawable;
        if (drawable != null) {
            this.J = drawable;
            this.x.setImageDrawable(drawable);
            if (!this.i0) {
                this.x.setImageDrawable(this.J);
                this.x.setImageTintList(ColorStateList.valueOf(this.b0));
                this.f0 = this.b0;
            }
        }
    }

    @DexIgnore
    public final CustomizeWidget a(c cVar) {
        return this;
    }

    @DexIgnore
    public final void a(Integer num, Integer num2, Integer num3, Integer num4) {
        if (num3 != null) {
            num3.intValue();
            this.b0 = num3.intValue();
            this.O = num3.intValue();
            this.T = num3.intValue();
        }
        if (num != null) {
            num.intValue();
            this.d0 = num.intValue();
            this.P = num.intValue();
            this.U = num.intValue();
        }
        if (num2 != null) {
            num2.intValue();
            this.G = num2.intValue();
        }
        if (num4 != null) {
            num4.intValue();
            this.F = num4.intValue();
        }
        f();
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "complicationId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(v6.c(getContext(), 2131231129));
                    return;
                }
                break;
            case -331239923:
                if (str.equals(Constants.BATTERY)) {
                    setTopIconSrc(v6.c(getContext(), 2131231125));
                    return;
                }
                break;
            case -168965370:
                if (str.equals("calories")) {
                    setTopIconSrc(v6.c(getContext(), 2131231127));
                    return;
                }
                break;
            case -85386984:
                if (str.equals("active-minutes")) {
                    setTopIconSrc(v6.c(getContext(), 2131231140));
                    return;
                }
                break;
            case -48173007:
                if (str.equals("chance-of-rain")) {
                    setTopIconSrc(v6.c(getContext(), 2131231134));
                    return;
                }
                break;
            case 3076014:
                if (str.equals("date")) {
                    setTopIconSrc(v6.c(getContext(), 2131231126));
                    return;
                }
                break;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(v6.c(getContext(), 2131231128));
                    return;
                }
                break;
            case 109761319:
                if (str.equals("steps")) {
                    setTopIconSrc(v6.c(getContext(), 2131231135));
                    return;
                }
                break;
            case 134170930:
                if (str.equals("second-timezone")) {
                    setTopIconSrc(v6.c(getContext(), 2131231137));
                    return;
                }
                break;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(v6.c(getContext(), 2131231138));
                    return;
                }
                break;
            case 1884273159:
                if (str.equals("heart-rate")) {
                    setTopIconSrc(v6.c(getContext(), 2131231130));
                    return;
                }
                break;
        }
        setTopIconSrc(v6.c(getContext(), 2131231128));
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "microAppId");
        switch (jy6.a[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(str).ordinal()]) {
            case 1:
                setTopIconSrc(v6.c(getContext(), 2131231100));
                return;
            case 2:
                setTopIconSrc(v6.c(getContext(), 2131231167));
                return;
            case 3:
                setTopIconSrc(v6.c(getContext(), 2131231159));
                return;
            case 4:
                setTopIconSrc(v6.c(getContext(), 2131231160));
                return;
            case 5:
                setTopIconSrc(v6.c(getContext(), 2131231165));
                return;
            case 6:
                setTopIconSrc(v6.c(getContext(), 2131231163));
                return;
            case 7:
                setTopIconSrc(v6.c(getContext(), 2131231162));
                return;
            case 8:
                setTopIconSrc(v6.c(getContext(), 2131231080));
                return;
            case 9:
                setTopIconSrc(v6.c(getContext(), 2131231166));
                return;
            case 10:
                setTopIconSrc(v6.c(getContext(), 2131231170));
                return;
            case 11:
                setTopIconSrc(v6.c(getContext(), 2131231171));
                return;
            case 12:
                setTopIconSrc(v6.c(getContext(), 2131231158));
                return;
            case 13:
                setTopIconSrc(v6.c(getContext(), 2131231172));
                return;
            case 14:
                setTopIconSrc(v6.c(getContext(), 2131231168));
                return;
            case 15:
                setTopIconSrc(v6.c(getContext(), 2131231169));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void d(String str) {
        ee7.b(str, "watchAppId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(v6.c(getContext(), 2131231039));
                    return;
                }
                return;
            case -740386388:
                if (str.equals("diagnostics")) {
                    setTopIconSrc(v6.c(getContext(), 2131231163));
                    return;
                }
                return;
            case -420342747:
                if (str.equals("wellness")) {
                    setTopIconSrc(v6.c(getContext(), 2131231139));
                    return;
                }
                return;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(null);
                    return;
                }
                return;
            case 104263205:
                if (str.equals(Constants.MUSIC)) {
                    setTopIconSrc(v6.c(getContext(), 2131231131));
                    return;
                }
                return;
            case 110364485:
                if (str.equals("timer")) {
                    setTopIconSrc(v6.c(getContext(), 2131231191));
                    return;
                }
                return;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(v6.c(getContext(), 2131231138));
                    return;
                }
                return;
            case 1374620322:
                if (str.equals("notification-panel")) {
                    setTopIconSrc(v6.c(getContext(), 2131231165));
                    return;
                }
                return;
            case 1525170845:
                if (str.equals("workout")) {
                    setTopIconSrc(v6.c(getContext(), 2131231140));
                    return;
                }
                return;
            case 1860261700:
                if (str.equals("stop-watch")) {
                    setTopIconSrc(v6.c(getContext(), 2131231169));
                    return;
                }
                return;
            case 1904923164:
                if (str.equals("buddy-challenge")) {
                    setTopIconSrc(v6.c(getContext(), 2131231161));
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void e() {
        if (this.m0) {
            this.m0 = false;
            this.A.setProgress(0);
        }
    }

    @DexIgnore
    public final void f() {
        if (!TextUtils.isEmpty(this.N)) {
            this.y.setText(this.N);
        } else {
            this.y.setVisibility(4);
        }
        if (this.j0) {
            this.x.setVisibility(0);
            ImageView imageView = this.x;
            Drawable drawable = this.I;
            if (drawable == null) {
                drawable = this.l0;
            }
            imageView.setImageDrawable(drawable);
            this.y.setVisibility(4);
            if (!TextUtils.isEmpty(this.a0)) {
                this.z.setText(this.a0);
                this.z.setVisibility(0);
            } else {
                this.z.setVisibility(8);
            }
        } else if (this.i0) {
            String str = this.M;
            if (str == null) {
                str = this.L;
            }
            if (!TextUtils.isEmpty(str)) {
                ee7.a((Object) hd5.a(this.x).a(str).f().a(this.x), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable2 = this.K;
                if (drawable2 == null) {
                    drawable2 = this.J;
                }
                if (drawable2 != null) {
                    this.x.setImageDrawable(drawable2);
                    this.x.setVisibility(0);
                    this.y.setVisibility(4);
                } else {
                    this.x.setImageDrawable(this.l0);
                    this.x.setVisibility(4);
                    this.y.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.S)) {
                this.z.setText(this.S);
                this.z.setVisibility(0);
            } else {
                this.z.setVisibility(8);
            }
        } else {
            if (!TextUtils.isEmpty(this.L)) {
                ee7.a((Object) hd5.a(this.x).a(this.L).f().a(this.x), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable3 = this.J;
                if (drawable3 != null) {
                    this.x.setImageDrawable(drawable3);
                    this.x.setVisibility(0);
                    this.y.setVisibility(4);
                } else {
                    this.x.setImageDrawable(this.l0);
                    this.x.setVisibility(4);
                    this.y.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.S)) {
                this.z.setText(this.S);
                this.z.setVisibility(0);
            } else {
                this.z.setVisibility(8);
            }
        }
        this.x.setVisibility(0);
        if (this.i0) {
            this.E = this.D;
            this.H = this.G;
            this.v.getBackground().clearColorFilter();
            if (this.G != this.B) {
                this.v.getBackground().mutate().setColorFilter(this.G, PorterDuff.Mode.SRC_IN);
            } else {
                Drawable drawable4 = this.k0;
                if (drawable4 != null) {
                    this.v.setBackground(drawable4);
                } else {
                    View view = this.v;
                    Context context = getContext();
                    Integer num = this.E;
                    if (num != null) {
                        view.setBackground(v6.c(context, num.intValue()));
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            this.x.setImageTintList(ColorStateList.valueOf(this.d0));
            this.f0 = this.d0;
            this.y.setTextColor(this.P);
            this.z.setTextColor(this.U);
            float f = this.R;
            if (f != -1.0f) {
                this.y.setTextSize(f);
            } else {
                float f2 = this.Q;
                if (f2 != -1.0f) {
                    this.y.setTextSize(f2);
                } else {
                    float f3 = this.h0;
                    if (f3 != -1.0f) {
                        this.y.setTextSize(f3);
                    } else {
                        float f4 = this.g0;
                        if (f4 != -1.0f) {
                            this.y.setTextSize(f4);
                        }
                    }
                }
            }
            float f5 = this.W;
            if (f5 != -1.0f) {
                this.z.setTextSize(f5);
                return;
            }
            float f6 = this.V;
            if (f6 != -1.0f) {
                this.z.setTextSize(f6);
                return;
            }
            float f7 = this.h0;
            if (f7 != -1.0f) {
                this.z.setTextSize(f7);
                return;
            }
            float f8 = this.g0;
            if (f8 != -1.0f) {
                this.z.setTextSize(f8);
                return;
            }
            return;
        }
        this.E = this.C;
        this.H = this.F;
        this.v.getBackground().clearColorFilter();
        if (this.F != this.B) {
            this.v.getBackground().mutate().setColorFilter(this.F, PorterDuff.Mode.SRC_IN);
        } else {
            Drawable drawable5 = this.k0;
            if (drawable5 != null) {
                this.v.setBackground(drawable5);
            } else {
                View view2 = this.v;
                Context context2 = getContext();
                Integer num2 = this.E;
                if (num2 != null) {
                    view2.setBackground(v6.c(context2, num2.intValue()));
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        this.x.setImageTintList(ColorStateList.valueOf(this.b0));
        this.f0 = this.b0;
        this.y.setTextColor(this.O);
        this.z.setTextColor(this.T);
        float f9 = this.Q;
        if (f9 != -1.0f) {
            this.y.setTextSize(f9);
        } else {
            float f10 = this.g0;
            if (f10 != -1.0f) {
                this.y.setTextSize(f10);
            }
        }
        float f11 = this.V;
        if (f11 != -1.0f) {
            this.z.setTextSize(f11);
            return;
        }
        float f12 = this.g0;
        if (f12 != -1.0f) {
            this.z.setTextSize(f12);
        }
    }

    @DexIgnore
    public final void g() {
        int i = this.B;
        this.F = i;
        this.G = i;
        this.d0 = i;
        this.P = i;
        this.U = i;
        this.O = i;
        this.T = i;
        f();
    }

    @DexIgnore
    public final int getBackgroundDrawableColor() {
        int i = this.H;
        if (i != this.B) {
            return i;
        }
        Integer num = this.E;
        if (num != null && num.intValue() == 2131230872) {
            return 2131099840;
        }
        return (num != null && num.intValue() == 2131230871) ? 2131099972 : 2131100361;
    }

    @DexIgnore
    public final int getBottomTextColor() {
        return this.z.getCurrentTextColor();
    }

    @DexIgnore
    public final int getIconTintColor() {
        return this.f0;
    }

    @DexIgnore
    public final void h() {
        f();
    }

    @DexIgnore
    public final void setBackgroundDrawableCus(Drawable drawable) {
        ee7.b(drawable, ResourceManager.DRAWABLE);
        this.k0 = drawable;
        f();
    }

    @DexIgnore
    public final void setBackgroundRes(Integer num) {
        this.k0 = null;
        this.C = num;
        f();
    }

    @DexIgnore
    public final void setBottomContent(String str) {
        this.S = str;
        if (this.j0) {
            if (!TextUtils.isEmpty(str)) {
                this.z.setText(str);
            }
        } else if (!TextUtils.isEmpty(str)) {
            this.z.setVisibility(0);
            this.z.setText(str);
        } else {
            this.z.setVisibility(8);
        }
    }

    @DexIgnore
    public final void setDefaultColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.b0 = num.intValue();
            f();
        }
    }

    @DexIgnore
    public final void setDragMode(boolean z2) {
        if (z2) {
            this.v.setVisibility(4);
            this.w.setVisibility(0);
            setAlpha(1.0f);
            return;
        }
        this.v.setVisibility(0);
        this.w.setVisibility(4);
        setAlpha(1.0f);
    }

    @DexIgnore
    public final void setRemoveMode(boolean z2) {
        this.j0 = z2;
        f();
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.i0 = z2;
        f();
    }

    @DexIgnore
    public static /* synthetic */ j9 a(CustomizeWidget customizeWidget, String str, Intent intent, View.OnDragListener onDragListener, b bVar, int i, Object obj) {
        if ((i & 4) != 0) {
            onDragListener = null;
        }
        if ((i & 8) != 0) {
            bVar = null;
        }
        return customizeWidget.a(str, intent, onDragListener, bVar);
    }

    @DexIgnore
    public final j9 a(String str, Intent intent, View.OnDragListener onDragListener, b bVar) {
        ee7.b(str, "label");
        ee7.b(intent, "intentItem");
        setTag(getTag());
        j9 j9Var = new j9(this, new d(this, bVar, 1.0f, str, intent));
        j9Var.a();
        if (onDragListener != null) {
            setOnDragListener(onDragListener);
        }
        return j9Var;
    }

    @DexIgnore
    public final void c(int i) {
        setTopIconSrc(v6.c(getContext(), i));
    }

    @DexIgnore
    public final void d() {
        this.m0 = true;
        this.A.setProgress(100);
    }
}
