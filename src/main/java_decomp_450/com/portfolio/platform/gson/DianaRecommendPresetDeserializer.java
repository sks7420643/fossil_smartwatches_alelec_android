package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.wc5;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendPresetDeserializer implements fe4<DianaRecommendPreset> {
    @DexIgnore
    @Override // com.fossil.fe4
    public DianaRecommendPreset deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        boolean z;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        Iterator<JsonElement> it;
        String str13;
        String str14;
        String str15;
        String str16;
        String str17;
        String str18;
        Iterator<JsonElement> it2;
        String str19;
        String str20;
        String str21;
        if (jsonElement != null) {
            ie4 d = jsonElement.d();
            String str22 = "name";
            JsonElement a = d.a(str22);
            ee7.a((Object) a, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a.f();
            String str23 = "id";
            JsonElement a2 = d.a(str23);
            ee7.a((Object) a2, "jsonObject.get(\"id\")");
            String f2 = a2.f();
            JsonElement a3 = d.a("isDefault");
            ee7.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_IS_DEFAULT)");
            boolean a4 = a3.a();
            String str24 = "updatedAt";
            JsonElement a5 = d.a(str24);
            ee7.a((Object) a5, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a5.f();
            String str25 = "createdAt";
            JsonElement a6 = d.a(str25);
            ee7.a((Object) a6, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a6.f();
            String str26 = "watchFaceId";
            JsonElement a7 = d.a(str26);
            ee7.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f5 = a7.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (ee4 != null) {
                Iterator<JsonElement> it3 = d.b("buttons").iterator();
                while (true) {
                    z = a4;
                    str = str24;
                    str9 = f3;
                    str8 = str25;
                    str7 = f4;
                    str6 = str26;
                    str10 = "itemJsonObject.get(Constants.JSON_KEY_APP_ID)";
                    str5 = f5;
                    str11 = "item";
                    str4 = str22;
                    str3 = f;
                    str2 = str23;
                    str12 = "";
                    if (!it3.hasNext()) {
                        break;
                    }
                    JsonElement next = it3.next();
                    ee7.a((Object) next, str11);
                    ie4 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a8 = d2.a("buttonPosition");
                        it2 = it3;
                        ee7.a((Object) a8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str19 = a8.f();
                    } else {
                        it2 = it3;
                        str19 = str12;
                    }
                    if (d2.d("appId")) {
                        JsonElement a9 = d2.a("appId");
                        ee7.a((Object) a9, str10);
                        str20 = a9.f();
                    } else {
                        str20 = str12;
                    }
                    if (d2.d("localUpdatedAt")) {
                        JsonElement a10 = d2.a("localUpdatedAt");
                        ee7.a((Object) a10, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str21 = a10.f();
                    } else {
                        str21 = str12;
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a11 = d2.a(Constants.USER_SETTING);
                            ee7.a((Object) a11, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str12 = wc5.a(a11.d());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    ee7.a((Object) str19, "position");
                    ee7.a((Object) str20, "appId");
                    ee7.a((Object) str21, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str19, str20, str21, str12));
                    a4 = z;
                    str24 = str;
                    it3 = it2;
                    f3 = str9;
                    str25 = str8;
                    f4 = str7;
                    str26 = str6;
                    f5 = str5;
                    str22 = str4;
                    f = str3;
                    str23 = str2;
                }
                Iterator<JsonElement> it4 = d.b("complications").iterator();
                while (it4.hasNext()) {
                    JsonElement next2 = it4.next();
                    ee7.a((Object) next2, str11);
                    ie4 d3 = next2.d();
                    if (d3.d("complicationPosition")) {
                        JsonElement a12 = d3.a("complicationPosition");
                        it = it4;
                        ee7.a((Object) a12, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str13 = a12.f();
                    } else {
                        it = it4;
                        str13 = str12;
                    }
                    if (d3.d("appId")) {
                        JsonElement a13 = d3.a("appId");
                        ee7.a((Object) a13, str10);
                        str14 = a13.f();
                    } else {
                        str14 = str12;
                    }
                    if (d3.d("localUpdatedAt")) {
                        str16 = str10;
                        JsonElement a14 = d3.a("localUpdatedAt");
                        ee7.a((Object) a14, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str17 = a14.f();
                        str15 = str11;
                    } else {
                        str16 = str10;
                        Calendar instance = Calendar.getInstance();
                        str15 = str11;
                        ee7.a((Object) instance, "Calendar.getInstance()");
                        str17 = zd5.y(instance.getTime());
                    }
                    if (d3.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a15 = d3.a(Constants.USER_SETTING);
                            ee7.a((Object) a15, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str18 = wc5.a(a15.d());
                        } catch (Exception unused2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        ee7.a((Object) str13, "position");
                        ee7.a((Object) str14, "appId");
                        ee7.a((Object) str17, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str13, str14, str17, str18));
                        it4 = it;
                        str10 = str16;
                        str11 = str15;
                    }
                    str18 = str12;
                    ee7.a((Object) str13, "position");
                    ee7.a((Object) str14, "appId");
                    ee7.a((Object) str17, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str13, str14, str17, str18));
                    it4 = it;
                    str10 = str16;
                    str11 = str15;
                }
            } else {
                str4 = str22;
                str2 = str23;
                str = str24;
                str8 = str25;
                str6 = str26;
                str3 = f;
                z = a4;
                str5 = f5;
                str7 = f4;
                str9 = f3;
            }
            ee7.a((Object) f2, str2);
            ee7.a((Object) str3, str4);
            ee7.a((Object) str5, str6);
            ee7.a((Object) str7, str8);
            ee7.a((Object) str9, str);
            return new DianaRecommendPreset("", f2, str3, z, arrayList2, arrayList, str5, str7, str9);
        }
        ee7.a();
        throw null;
    }
}
