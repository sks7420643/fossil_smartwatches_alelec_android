package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.wc5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDeserializer implements fe4<HybridPreset> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.fe4
    public HybridPreset deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        String str2;
        String str3;
        String str4;
        Iterator<JsonElement> it;
        String str5;
        String str6;
        String str7;
        if (jsonElement != null) {
            ie4 d = jsonElement.d();
            JsonElement a2 = d.a("name");
            ee7.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a2.f();
            JsonElement a3 = d.a("id");
            ee7.a((Object) a3, "jsonObject.get(\"id\")");
            String f2 = a3.f();
            String str8 = "";
            if (d.d("serialNumber")) {
                JsonElement a4 = d.a("serialNumber");
                ee7.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = a4.f();
            } else {
                str = str8;
            }
            JsonElement a5 = d.a("isActive");
            ee7.a((Object) a5, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a6 = a5.a();
            JsonElement a7 = d.a("updatedAt");
            ee7.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a7.f();
            JsonElement a8 = d.a("createdAt");
            ee7.a((Object) a8, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a8.f();
            ArrayList arrayList = new ArrayList();
            if (ee4 != null) {
                Iterator<JsonElement> it2 = d.b("buttons").iterator();
                while (it2.hasNext()) {
                    JsonElement next = it2.next();
                    ee7.a((Object) next, "item");
                    ie4 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a9 = d2.a("buttonPosition");
                        ee7.a((Object) a9, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = a9.f();
                    } else {
                        str2 = str8;
                    }
                    if (d2.d("appId")) {
                        JsonElement a10 = d2.a("appId");
                        ee7.a((Object) a10, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str3 = a10.f();
                    } else {
                        str3 = str8;
                    }
                    if (d2.d("localUpdatedAt")) {
                        it = it2;
                        JsonElement a11 = d2.a("localUpdatedAt");
                        str4 = str8;
                        ee7.a((Object) a11, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str5 = a11.f();
                    } else {
                        it = it2;
                        str4 = str8;
                        Calendar instance = Calendar.getInstance();
                        ee7.a((Object) instance, "Calendar.getInstance()");
                        str5 = zd5.y(instance.getTime());
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a12 = d2.a(Constants.USER_SETTING);
                            ee7.a((Object) a12, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str7 = wc5.a(a12.d());
                            str6 = f3;
                        } catch (Exception unused) {
                            str6 = f3;
                            FLogger.INSTANCE.getLocal().d("HybridPresetDeserializer", "Exception when parse json string");
                        }
                    } else {
                        str6 = f3;
                        str7 = str4;
                    }
                    ee7.a((Object) str2, "position");
                    ee7.a((Object) str3, "appId");
                    ee7.a((Object) str5, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str3, str5, str7));
                    it2 = it;
                    str8 = str4;
                    f3 = str6;
                }
            }
            ee7.a((Object) f2, "id");
            ee7.a((Object) str, "serialNumber");
            HybridPreset hybridPreset = new HybridPreset(f2, f, str, arrayList, a6);
            hybridPreset.setCreatedAt(f4);
            hybridPreset.setUpdatedAt(f3);
            return hybridPreset;
        }
        ee7.a();
        throw null;
    }
}
