package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.wc5;
import com.fossil.zd5;
import com.fossil.zd7;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDeserializer implements fe4<DianaPreset> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.fe4
    public DianaPreset deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        boolean z;
        String str10;
        String str11;
        String str12;
        Iterator<JsonElement> it;
        String str13;
        String str14;
        String str15;
        String str16;
        String str17;
        String str18;
        Iterator<JsonElement> it2;
        String str19;
        String str20;
        String str21;
        if (jsonElement != null) {
            ie4 d = jsonElement.d();
            String str22 = "id";
            JsonElement a2 = d.a(str22);
            ee7.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_ID)");
            String f = a2.f();
            String str23 = "name";
            JsonElement a3 = d.a(str23);
            ee7.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f2 = a3.f();
            JsonElement a4 = d.a("updatedAt");
            ee7.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a4.f();
            JsonElement a5 = d.a("createdAt");
            ee7.a((Object) a5, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a5.f();
            JsonElement a6 = d.a("isActive");
            ee7.a((Object) a6, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a7 = a6.a();
            String str24 = "serialNumber";
            JsonElement a8 = d.a(str24);
            ee7.a((Object) a8, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
            String f5 = a8.f();
            String str25 = "watchFaceId";
            JsonElement a9 = d.a(str25);
            ee7.a((Object) a9, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f6 = a9.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (ee4 != null) {
                Iterator<JsonElement> it3 = d.b("buttons").iterator();
                while (true) {
                    str2 = f3;
                    str = f4;
                    z = a7;
                    str9 = str25;
                    str8 = f6;
                    str7 = str23;
                    str10 = "itemJsonObject.get(Constants.JSON_KEY_APP_ID)";
                    str6 = f2;
                    str11 = "item";
                    str5 = str24;
                    str4 = f5;
                    str3 = str22;
                    str12 = "";
                    if (!it3.hasNext()) {
                        break;
                    }
                    JsonElement next = it3.next();
                    ee7.a((Object) next, str11);
                    ie4 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a10 = d2.a("buttonPosition");
                        it2 = it3;
                        ee7.a((Object) a10, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str19 = a10.f();
                    } else {
                        it2 = it3;
                        str19 = str12;
                    }
                    if (d2.d("appId")) {
                        JsonElement a11 = d2.a("appId");
                        ee7.a((Object) a11, str10);
                        str20 = a11.f();
                    } else {
                        str20 = str12;
                    }
                    if (d2.d("localUpdatedAt")) {
                        JsonElement a12 = d2.a("localUpdatedAt");
                        ee7.a((Object) a12, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str21 = a12.f();
                    } else {
                        str21 = str12;
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a13 = d2.a(Constants.USER_SETTING);
                            ee7.a((Object) a13, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str12 = wc5.a(a13.d());
                        } catch (Exception unused) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    ee7.a((Object) str19, "position");
                    ee7.a((Object) str20, "appId");
                    ee7.a((Object) str21, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str19, str20, str21, str12));
                    f3 = str2;
                    f4 = str;
                    it3 = it2;
                    a7 = z;
                    str25 = str9;
                    f6 = str8;
                    str23 = str7;
                    f2 = str6;
                    str24 = str5;
                    f5 = str4;
                    str22 = str3;
                }
                Iterator<JsonElement> it4 = d.b("complications").iterator();
                while (it4.hasNext()) {
                    JsonElement next2 = it4.next();
                    ee7.a((Object) next2, str11);
                    ie4 d3 = next2.d();
                    if (d3.d("complicationPosition")) {
                        JsonElement a14 = d3.a("complicationPosition");
                        it = it4;
                        ee7.a((Object) a14, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str13 = a14.f();
                    } else {
                        it = it4;
                        str13 = str12;
                    }
                    if (d3.d("appId")) {
                        JsonElement a15 = d3.a("appId");
                        ee7.a((Object) a15, str10);
                        str14 = a15.f();
                    } else {
                        str14 = str12;
                    }
                    if (d3.d("localUpdatedAt")) {
                        str16 = str10;
                        JsonElement a16 = d3.a("localUpdatedAt");
                        ee7.a((Object) a16, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str17 = a16.f();
                        str15 = str11;
                    } else {
                        str16 = str10;
                        Calendar instance = Calendar.getInstance();
                        str15 = str11;
                        ee7.a((Object) instance, "Calendar.getInstance()");
                        str17 = zd5.y(instance.getTime());
                    }
                    if (d3.d(Constants.USER_SETTING)) {
                        try {
                            JsonElement a17 = d3.a(Constants.USER_SETTING);
                            ee7.a((Object) a17, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str18 = wc5.a(a17.d());
                        } catch (Exception unused2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        ee7.a((Object) str13, "position");
                        ee7.a((Object) str14, "appId");
                        ee7.a((Object) str17, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str13, str14, str17, str18));
                        it4 = it;
                        str10 = str16;
                        str11 = str15;
                    }
                    str18 = str12;
                    ee7.a((Object) str13, "position");
                    ee7.a((Object) str14, "appId");
                    ee7.a((Object) str17, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str13, str14, str17, str18));
                    it4 = it;
                    str10 = str16;
                    str11 = str15;
                }
            } else {
                str3 = str22;
                str7 = str23;
                str2 = f3;
                str5 = str24;
                str4 = f5;
                str6 = f2;
                z = a7;
                str9 = str25;
                str8 = f6;
                str = f4;
            }
            ee7.a((Object) f, str3);
            ee7.a((Object) str4, str5);
            ee7.a((Object) str6, str7);
            ee7.a((Object) str8, str9);
            DianaPreset dianaPreset = new DianaPreset(f, str4, str6, z, arrayList2, arrayList, str8);
            dianaPreset.setCreatedAt(str);
            dianaPreset.setUpdatedAt(str2);
            return dianaPreset;
        }
        ee7.a();
        throw null;
    }
}
