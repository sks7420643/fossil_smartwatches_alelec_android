package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.je4;
import com.fossil.le4;
import com.fossil.me4;
import com.fossil.ne4;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DateTimeSerializer implements fe4<DateTime>, ne4<DateTime> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(DateTime dateTime, Type type, me4 me4) {
        String str;
        if (dateTime == null || (str = zd5.b(dateTime)) == null) {
            str = "";
        }
        return new le4(str);
    }

    @DexIgnore
    @Override // com.fossil.fe4
    public DateTime deserialize(JsonElement jsonElement, Type type, ee4 ee4) throws je4 {
        ee7.b(jsonElement, "je");
        String f = jsonElement.f();
        ee7.a((Object) f, "je.asString");
        if (f.length() == 0) {
            return null;
        }
        return zd5.c(jsonElement.f());
    }
}
