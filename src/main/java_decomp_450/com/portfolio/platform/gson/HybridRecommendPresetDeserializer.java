package com.portfolio.platform.gson;

import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.wc5;
import com.fossil.zd5;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridRecommendPresetDeserializer implements fe4<HybridRecommendPreset> {
    @DexIgnore
    @Override // com.fossil.fe4
    public HybridRecommendPreset deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        String str2;
        String str3;
        Iterator<JsonElement> it;
        String str4;
        String str5;
        boolean z;
        String str6;
        String str7;
        if (jsonElement != null) {
            ie4 d = jsonElement.d();
            JsonElement a = d.a("name");
            ee7.a((Object) a, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = a.f();
            JsonElement a2 = d.a("id");
            ee7.a((Object) a2, "jsonObject.get(\"id\")");
            String f2 = a2.f();
            String str8 = "";
            if (d.d("serialNumber")) {
                JsonElement a3 = d.a("serialNumber");
                ee7.a((Object) a3, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = a3.f();
            } else {
                str = str8;
            }
            JsonElement a4 = d.a("isDefault");
            ee7.a((Object) a4, "jsonObject.get(Constants.JSON_KEY_IS_DEFAULT)");
            boolean a5 = a4.a();
            String str9 = "updatedAt";
            JsonElement a6 = d.a(str9);
            ee7.a((Object) a6, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = a6.f();
            JsonElement a7 = d.a("createdAt");
            ee7.a((Object) a7, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = a7.f();
            ArrayList arrayList = new ArrayList();
            if (ee4 != null) {
                Iterator<JsonElement> it2 = d.b("buttons").iterator();
                while (it2.hasNext()) {
                    JsonElement next = it2.next();
                    ee7.a((Object) next, "item");
                    ie4 d2 = next.d();
                    if (d2.d("buttonPosition")) {
                        JsonElement a8 = d2.a("buttonPosition");
                        ee7.a((Object) a8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = a8.f();
                    } else {
                        str2 = str8;
                    }
                    if (d2.d("appId")) {
                        it = it2;
                        JsonElement a9 = d2.a("appId");
                        str3 = str8;
                        ee7.a((Object) a9, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str4 = a9.f();
                    } else {
                        it = it2;
                        str3 = str8;
                        str4 = str3;
                    }
                    if (d2.d("localUpdatedAt")) {
                        z = a5;
                        JsonElement a10 = d2.a("localUpdatedAt");
                        str5 = f;
                        ee7.a((Object) a10, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str6 = a10.f();
                    } else {
                        str5 = f;
                        z = a5;
                        Calendar instance = Calendar.getInstance();
                        ee7.a((Object) instance, "Calendar.getInstance()");
                        str6 = zd5.y(instance.getTime());
                    }
                    if (d2.d(Constants.USER_SETTING)) {
                        JsonElement a11 = d2.a(Constants.USER_SETTING);
                        ee7.a((Object) a11, "settingJsonObject");
                        if (!a11.h()) {
                            str7 = wc5.a(a11.d());
                            ee7.a((Object) str2, "position");
                            ee7.a((Object) str4, "appId");
                            ee7.a((Object) str6, "localUpdatedAt");
                            arrayList.add(new HybridPresetAppSetting(str2, str4, str6, str7));
                            it2 = it;
                            str8 = str3;
                            a5 = z;
                            f = str5;
                            str9 = str9;
                        }
                    }
                    str7 = str3;
                    ee7.a((Object) str2, "position");
                    ee7.a((Object) str4, "appId");
                    ee7.a((Object) str6, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str4, str6, str7));
                    it2 = it;
                    str8 = str3;
                    a5 = z;
                    f = str5;
                    str9 = str9;
                }
            }
            ee7.a((Object) f2, "id");
            ee7.a((Object) str, "serialNumber");
            ee7.a((Object) f4, "createdAt");
            ee7.a((Object) f3, str9);
            return new HybridRecommendPreset(f2, f, str, arrayList, a5, f4, f3);
        }
        ee7.a();
        throw null;
    }
}
