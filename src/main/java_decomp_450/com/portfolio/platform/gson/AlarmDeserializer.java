package com.portfolio.platform.gson;

import com.fossil.de4;
import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.pd5;
import com.fossil.w97;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDeserializer implements fe4<Alarm> {
    @DexIgnore
    @Override // com.fossil.fe4
    public Alarm deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        String str;
        String str2;
        String str3;
        int[] iArr;
        String f;
        String f2;
        String f3;
        String f4;
        ie4 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        JsonElement a = d.a("id");
        String f5 = a != null ? a.f() : null;
        if (f5 == null) {
            return null;
        }
        JsonElement a2 = d.a("title");
        String str4 = (a2 == null || (f4 = a2.f()) == null) ? "" : f4;
        JsonElement a3 = d.a("message");
        if (a3 == null || (f3 = a3.f()) == null) {
            str = "";
        } else {
            str = f3;
        }
        JsonElement a4 = d.a(MFSleepGoal.COLUMN_MINUTE);
        int i = 0;
        int b = a4 != null ? a4.b() : 0;
        JsonElement a5 = d.a(AppFilter.COLUMN_HOUR);
        int b2 = a5 != null ? a5.b() : 0;
        JsonElement a6 = d.a("isActive");
        boolean a7 = a6 != null ? a6.a() : false;
        JsonElement a8 = d.a("isRepeated");
        boolean a9 = a8 != null ? a8.a() : false;
        JsonElement a10 = d.a("createdAt");
        if (a10 == null || (f2 = a10.f()) == null) {
            str2 = "";
        } else {
            str2 = f2;
        }
        JsonElement a11 = d.a("updatedAt");
        if (a11 == null || (f = a11.f()) == null) {
            str3 = "";
        } else {
            str3 = f;
        }
        de4 b3 = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
        if (b3 != null) {
            int[] iArr2 = new int[b3.size()];
            for (Object obj : b3) {
                int i2 = i + 1;
                if (i >= 0) {
                    JsonElement jsonElement2 = (JsonElement) obj;
                    if (jsonElement2 != null) {
                        pd5.a aVar = pd5.d;
                        String f6 = jsonElement2.f();
                        ee7.a((Object) f6, "it.asString");
                        iArr2[i] = aVar.a(f6);
                    }
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            }
            iArr = iArr2;
        } else {
            iArr = null;
        }
        return new Alarm(f5, f5, str4, str, b2, b, iArr, a7, a9, str2, str3, 0);
    }
}
