package com.portfolio.platform.gson;

import com.fossil.ao4;
import com.fossil.ee4;
import com.fossil.ee7;
import com.fossil.fe4;
import com.fossil.ie4;
import com.fossil.io4;
import com.fossil.pn4;
import com.fossil.zd5;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDeserializer implements fe4<ao4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<pn4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<io4> {
    }

    @DexIgnore
    @Override // com.fossil.fe4
    public ao4 deserialize(JsonElement jsonElement, Type type, ee4 ee4) {
        Date date;
        pn4 pn4;
        io4 io4;
        boolean z;
        int i;
        int i2;
        boolean z2;
        if (jsonElement == null) {
            return null;
        }
        ie4 d = jsonElement.d();
        JsonElement a2 = d.a("id");
        ee7.a((Object) a2, "jsonObject.get(Constants.JSON_KEY_ID)");
        String f = a2.f();
        JsonElement a3 = d.a("createdAt");
        String f2 = a3 != null ? a3.f() : null;
        Date date2 = new Date();
        if (f2 != null) {
            Date d2 = zd5.d(f2);
            ee7.a((Object) d2, "DateHelper.parseJodaTime(dateStr)");
            date = d2;
        } else {
            date = date2;
        }
        JsonElement a4 = d.a("message");
        ee7.a((Object) a4, "jsonObject.get(BCConst.NOTIFICATION_MESSAGE_KEY)");
        ie4 d3 = a4.d();
        JsonElement a5 = d3.a("titleLocKey");
        ee7.a((Object) a5, "message.get(BCConst.NOTI\u2026ON_TITLE_LOC_MESSAGE_KEY)");
        String f3 = a5.f();
        ee7.a((Object) f3, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement a6 = d3.a("bodyLocKey");
        ee7.a((Object) a6, "message.get(BCConst.NOTI\u2026ION_BODY_LOC_MESSAGE_KEY)");
        String f4 = a6.f();
        ee7.a((Object) f4, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement a7 = d3.a("payload");
        ee7.a((Object) a7, "message.get(BCConst.NOTIFICATION_PAYLOAD_KEY)");
        ie4 d4 = a7.d();
        JsonElement a8 = d4.a("challenge");
        String f5 = a8 != null ? a8.f() : null;
        try {
            pn4 = (pn4) new Gson().a(f5, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String simpleName = NotificationDeserializer.class.getSimpleName();
            ee7.a((Object) simpleName, "this::class.java.simpleName");
            local.e(simpleName, "exception when parse challengeData: " + f5);
            e.printStackTrace();
            pn4 = null;
        }
        JsonElement a9 = d4.a("profile");
        String f6 = a9 != null ? a9.f() : null;
        try {
            io4 = (io4) new Gson().a(f6, new b().getType());
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String simpleName2 = NotificationDeserializer.class.getSimpleName();
            ee7.a((Object) simpleName2, "this::class.java.simpleName");
            local2.e(simpleName2, "exception when parse profileData: " + f6);
            e2.printStackTrace();
            io4 = null;
        }
        JsonElement a10 = d4.a("confirm");
        String f7 = a10 != null ? a10.f() : null;
        if (f7 != null) {
            try {
                z2 = Boolean.parseBoolean(f7);
            } catch (Exception unused) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String simpleName3 = NotificationDeserializer.class.getSimpleName();
                ee7.a((Object) simpleName3, "this::class.java.simpleName");
                local3.e(simpleName3, "exception when parse confirmChallenge: " + f7);
                z2 = false;
            }
            z = z2;
        } else {
            z = false;
        }
        JsonElement a11 = d4.a("rank");
        String f8 = a11 != null ? a11.f() : null;
        if (f8 != null) {
            try {
                i2 = Integer.parseInt(f8);
            } catch (Exception unused2) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String simpleName4 = NotificationDeserializer.class.getSimpleName();
                ee7.a((Object) simpleName4, "this::class.java.simpleName");
                local4.e(simpleName4, "exception when parse rank: " + f8);
                i2 = 0;
            }
            i = i2;
        } else {
            i = 0;
        }
        JsonElement a12 = d3.a("reachGoalAt");
        String f9 = a12 != null ? a12.f() : null;
        Date d5 = f9 != null ? zd5.d(f9) : null;
        ee7.a((Object) f, "id");
        return new ao4(f, f3, f4, date, pn4, io4, z, i, d5);
    }
}
