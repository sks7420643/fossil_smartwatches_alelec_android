package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import com.fossil.ae;
import com.fossil.bl4;
import com.fossil.rd;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AutoClearedValue$Anon1 implements rd {
    @DexIgnore
    public /* final */ /* synthetic */ bl4 a;

    @DexIgnore
    @ae(Lifecycle.a.ON_DESTROY)
    public final void onDestroy() {
        this.a.a(null);
        throw null;
    }
}
