package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import com.fossil.h62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk3 implements ServiceConnection, h62.a, h62.b {
    @DexIgnore
    public volatile boolean a;
    @DexIgnore
    public volatile gg3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 c;

    @DexIgnore
    public zk3(ek3 ek3) {
        this.c = ek3;
    }

    @DexIgnore
    public final void a(Intent intent) {
        this.c.g();
        Context f = this.c.f();
        e92 a2 = e92.a();
        synchronized (this) {
            if (this.a) {
                this.c.e().B().a("Connection attempt already in progress");
                return;
            }
            this.c.e().B().a("Using local app measurement service");
            this.a = true;
            a2.a(f, intent, this.c.c, 129);
        }
    }

    @DexIgnore
    public final void b() {
        this.c.g();
        Context f = this.c.f();
        synchronized (this) {
            if (this.a) {
                this.c.e().B().a("Connection attempt already in progress");
            } else if (this.b == null || (!this.b.f() && !this.b.c())) {
                this.b = new gg3(f, Looper.getMainLooper(), this, this);
                this.c.e().B().a("Connecting to remote service");
                this.a = true;
                this.b.r();
            } else {
                this.c.e().B().a("Already awaiting connection attempt");
            }
        }
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        bg3 bg3;
        a72.a("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.a = false;
                this.c.e().t().a("Service connected with null binder");
                return;
            }
            bg3 bg32 = null;
            try {
                String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                    if (iBinder != null) {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                        if (queryLocalInterface instanceof bg3) {
                            bg3 = (bg3) queryLocalInterface;
                        } else {
                            bg3 = new dg3(iBinder);
                        }
                        bg32 = bg3;
                    }
                    this.c.e().B().a("Bound to IMeasurementService interface");
                } else {
                    this.c.e().t().a("Got binder with a wrong descriptor", interfaceDescriptor);
                }
            } catch (RemoteException unused) {
                this.c.e().t().a("Service connect failed to get IMeasurementService");
            }
            if (bg32 == null) {
                this.a = false;
                try {
                    e92.a().a(this.c.f(), this.c.c);
                } catch (IllegalArgumentException unused2) {
                }
            } else {
                this.c.c().a(new yk3(this, bg32));
            }
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        a72.a("MeasurementServiceConnection.onServiceDisconnected");
        this.c.e().A().a("Service disconnected");
        this.c.c().a(new bl3(this, componentName));
    }

    @DexIgnore
    public final void a() {
        if (this.b != null && (this.b.c() || this.b.f())) {
            this.b.a();
        }
        this.b = null;
    }

    @DexIgnore
    @Override // com.fossil.h62.a
    public final void a(int i) {
        a72.a("MeasurementServiceConnection.onConnectionSuspended");
        this.c.e().A().a("Service connection suspended");
        this.c.c().a(new dl3(this));
    }

    @DexIgnore
    @Override // com.fossil.h62.a
    public final void b(Bundle bundle) {
        a72.a("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.c().a(new al3(this, (bg3) this.b.A()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.b = null;
                this.a = false;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.h62.b
    public final void a(i02 i02) {
        a72.a("MeasurementServiceConnection.onConnectionFailed");
        jg3 q = ((ii3) this.c).a.q();
        if (q != null) {
            q.w().a("Service connection failed", i02);
        }
        synchronized (this) {
            this.a = false;
            this.b = null;
        }
        this.c.c().a(new cl3(this));
    }
}
