package com.fossil;

import android.content.Intent;
import com.fossil.fl4;
import com.fossil.nj5;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm5 extends fl4<b, d, c> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ e e; // = new e();
    @DexIgnore
    public /* final */ NotificationsRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ List<bt5> a;
        @DexIgnore
        public /* final */ List<at5> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(List<bt5> list, List<at5> list2, int i) {
            ee7.b(list, "contactWrapperList");
            ee7.b(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
            this.c = i;
        }

        @DexIgnore
        public final List<at5> a() {
            return this.b;
        }

        @DexIgnore
        public final List<bt5> b() {
            return this.a;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetNotificationFiltersUserCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS && cm5.this.d()) {
                boolean z = false;
                cm5.this.a(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    cm5.this.a(new d());
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                cm5.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public cm5(NotificationsRepository notificationsRepository, DeviceRepository deviceRepository) {
        ee7.b(notificationsRepository, "mNotificationsRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        this.f = notificationsRepository;
        this.g = deviceRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final List<AppNotificationFilter> b(List<bt5> list, short s, boolean z) {
        Contact contact;
        ArrayList arrayList = new ArrayList();
        for (bt5 bt5 : list) {
            short s2 = -1;
            if (bt5.isAdded() && (contact = bt5.getContact()) != null) {
                if (contact.isUseCall()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                    }
                    aa0 aa0 = new aa0(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                    appNotificationFilter.setSender(contact.getDisplayName());
                    appNotificationFilter.setHandMovingConfig(aa0);
                    appNotificationFilter.setVibePattern(ca0.CALL);
                    arrayList.add(appNotificationFilter);
                }
                if (contact.isUseSms()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                    }
                    aa0 aa02 = new aa0(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                    appNotificationFilter2.setSender(contact.getDisplayName());
                    appNotificationFilter2.setHandMovingConfig(aa02);
                    appNotificationFilter2.setVibePattern(ca0.TEXT);
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetNotificationFiltersUserCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        nj5.d.a(this.e, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        try {
            FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "running UseCase");
            this.d = true;
            Integer num = null;
            List<bt5> b2 = bVar != null ? bVar.b() : null;
            List<at5> a2 = bVar != null ? bVar.a() : null;
            if (bVar != null) {
                num = pb7.a(bVar.c());
            }
            if (b2 == null || a2 == null || num == null) {
                a(new c(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG, -1, new ArrayList()));
            } else {
                String c2 = PortfolioApp.g0.c().c();
                boolean a3 = nx6.b.a(this.g.getSkuModelBySerialPrefix(be5.o.b(c2)), c2);
                short c3 = (short) ze5.c(num.intValue());
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(a(this.f.getAllNotificationsByHour(c2, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), b2, a2, c3, a3));
                arrayList.addAll(b(b2, c3, a3));
                arrayList.addAll(a(a2, c3, a3));
                AppNotificationFilterSettings appNotificationFilterSettings = new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
                PortfolioApp.g0.c().a(appNotificationFilterSettings, c2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetNotificationFiltersUserCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
            }
            return new Object();
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SetNotificationFiltersUserCase", "Error inside SetNotificationFiltersUserCase.connectDevice - e=" + e2);
            return new c(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0201 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0084 A[EDGE_INSN: B:110:0x0084->B:27:0x0084 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x018e A[EDGE_INSN: B:113:0x018e->B:73:0x018e ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter> a(android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>> r21, java.util.List<com.fossil.bt5> r22, java.util.List<com.fossil.at5> r23, short r24, boolean r25) {
        /*
            r20 = this;
            r0 = r21
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            if (r0 == 0) goto L_0x020f
            int r2 = r21.size()
            r5 = 0
            r6 = -1
        L_0x000f:
            if (r5 >= r2) goto L_0x020f
            java.lang.Object r7 = r0.valueAt(r5)
            java.util.List r7 = (java.util.List) r7
            java.util.Iterator r7 = r7.iterator()
        L_0x001b:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x0207
            java.lang.Object r8 = r7.next()
            com.fossil.wearables.fsl.shared.BaseFeatureModel r8 = (com.fossil.wearables.fsl.shared.BaseFeatureModel) r8
            boolean r9 = r8.isEnabled()
            if (r9 == 0) goto L_0x0139
            boolean r9 = r8 instanceof com.fossil.wearables.fsl.contact.ContactGroup
            java.lang.String r10 = ""
            r11 = 10000(0x2710, float:1.4013E-41)
            if (r9 == 0) goto L_0x013d
            com.fossil.wearables.fsl.contact.ContactGroup r8 = (com.fossil.wearables.fsl.contact.ContactGroup) r8
            java.util.List r9 = r8.getContacts()
            java.util.Iterator r9 = r9.iterator()
            r14 = 0
            r15 = -1
        L_0x0041:
            boolean r16 = r9.hasNext()
            if (r16 == 0) goto L_0x0139
            java.lang.Object r16 = r9.next()
            r3 = r16
            com.fossil.wearables.fsl.contact.Contact r3 = (com.fossil.wearables.fsl.contact.Contact) r3
            java.util.Iterator r16 = r22.iterator()
        L_0x0053:
            boolean r17 = r16.hasNext()
            java.lang.String r4 = "existedContact"
            if (r17 == 0) goto L_0x0082
            java.lang.Object r17 = r16.next()
            r18 = r17
            com.fossil.bt5 r18 = (com.fossil.bt5) r18
            com.fossil.wearables.fsl.contact.Contact r19 = r18.getContact()
            if (r19 == 0) goto L_0x007e
            com.fossil.wearables.fsl.contact.Contact r18 = r18.getContact()
            if (r18 == 0) goto L_0x007e
            int r13 = r18.getContactId()
            com.fossil.ee7.a(r3, r4)
            int r12 = r3.getContactId()
            if (r13 != r12) goto L_0x007e
            r12 = 1
            goto L_0x007f
        L_0x007e:
            r12 = 0
        L_0x007f:
            if (r12 == 0) goto L_0x0053
            goto L_0x0084
        L_0x0082:
            r17 = 0
        L_0x0084:
            com.fossil.bt5 r17 = (com.fossil.bt5) r17
            if (r17 == 0) goto L_0x0091
            boolean r12 = r17.isAdded()
            if (r12 == 0) goto L_0x0091
            r15 = r24
            goto L_0x009c
        L_0x0091:
            if (r17 != 0) goto L_0x009d
            int r12 = r8.getHour()
            int r12 = com.fossil.ze5.c(r12)
            short r15 = (short) r12
        L_0x009c:
            r14 = 1
        L_0x009d:
            if (r14 == 0) goto L_0x012f
            com.fossil.ee7.a(r3, r4)
            boolean r4 = r3.isUseCall()
            if (r4 == 0) goto L_0x00e4
            if (r25 == 0) goto L_0x00b1
            com.portfolio.platform.data.model.setting.SpecialSkuSetting$AngleSubeye r4 = com.portfolio.platform.data.model.setting.SpecialSkuSetting.AngleSubeye.MOVEMBER
            int r4 = r4.getAngleForCall()
            short r6 = (short) r4
        L_0x00b1:
            com.fossil.aa0 r4 = new com.fossil.aa0
            r4.<init>(r15, r15, r6, r11)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r12 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r12 = r12.getPHONE_INCOMING_CALL()
            com.misfit.frameworks.buttonservice.model.notification.FNotification r13 = new com.misfit.frameworks.buttonservice.model.notification.FNotification
            java.lang.String r11 = r12.getAppName()
            java.lang.String r0 = r12.getPackageName()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r12 = r12.getNotificationType()
            r13.<init>(r11, r0, r10, r12)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r0 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter
            r0.<init>(r13)
            java.lang.String r11 = r3.getDisplayName()
            r0.setSender(r11)
            r0.setHandMovingConfig(r4)
            com.fossil.ca0 r4 = com.fossil.ca0.CALL
            r0.setVibePattern(r4)
            r1.add(r0)
        L_0x00e4:
            boolean r0 = r3.isUseSms()
            if (r0 == 0) goto L_0x012f
            if (r25 == 0) goto L_0x00f3
            com.portfolio.platform.data.model.setting.SpecialSkuSetting$AngleSubeye r0 = com.portfolio.platform.data.model.setting.SpecialSkuSetting.AngleSubeye.MOVEMBER
            int r0 = r0.getAngleForSms()
            short r6 = (short) r0
        L_0x00f3:
            com.fossil.aa0 r0 = new com.fossil.aa0
            r4 = 10000(0x2710, float:1.4013E-41)
            r0.<init>(r15, r15, r6, r4)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r4 = r4.getMESSAGES()
            com.misfit.frameworks.buttonservice.model.notification.FNotification r11 = new com.misfit.frameworks.buttonservice.model.notification.FNotification
            java.lang.String r12 = r4.getAppName()
            java.lang.String r13 = r4.getPackageName()
            r17 = r2
            java.lang.String r2 = r4.getIconFwPath()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r4 = r4.getNotificationType()
            r11.<init>(r12, r13, r2, r4)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter
            r2.<init>(r11)
            java.lang.String r3 = r3.getDisplayName()
            r2.setSender(r3)
            r2.setHandMovingConfig(r0)
            com.fossil.ca0 r0 = com.fossil.ca0.TEXT
            r2.setVibePattern(r0)
            r1.add(r2)
            goto L_0x0131
        L_0x012f:
            r17 = r2
        L_0x0131:
            r0 = r21
            r2 = r17
            r11 = 10000(0x2710, float:1.4013E-41)
            goto L_0x0041
        L_0x0139:
            r17 = r2
            goto L_0x0201
        L_0x013d:
            r17 = r2
            boolean r0 = r8 instanceof com.fossil.wearables.fsl.appfilter.AppFilter
            if (r0 == 0) goto L_0x0201
            com.fossil.wearables.fsl.appfilter.AppFilter r8 = (com.fossil.wearables.fsl.appfilter.AppFilter) r8
            java.lang.String r0 = r8.getType()
            if (r0 == 0) goto L_0x0154
            boolean r0 = com.fossil.mh7.a(r0)
            if (r0 == 0) goto L_0x0152
            goto L_0x0154
        L_0x0152:
            r0 = 0
            goto L_0x0155
        L_0x0154:
            r0 = 1
        L_0x0155:
            if (r0 != 0) goto L_0x0201
            java.util.Iterator r0 = r23.iterator()
        L_0x015b:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x018d
            java.lang.Object r2 = r0.next()
            r3 = r2
            com.fossil.at5 r3 = (com.fossil.at5) r3
            com.portfolio.platform.data.model.InstalledApp r4 = r3.getInstalledApp()
            if (r4 == 0) goto L_0x0189
            com.portfolio.platform.data.model.InstalledApp r3 = r3.getInstalledApp()
            if (r3 == 0) goto L_0x0184
            java.lang.String r3 = r3.getIdentifier()
            java.lang.String r4 = r8.getType()
            boolean r3 = com.fossil.ee7.a(r3, r4)
            if (r3 == 0) goto L_0x0189
            r3 = 1
            goto L_0x018a
        L_0x0184:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0189:
            r3 = 0
        L_0x018a:
            if (r3 == 0) goto L_0x015b
            goto L_0x018e
        L_0x018d:
            r2 = 0
        L_0x018e:
            com.fossil.at5 r2 = (com.fossil.at5) r2
            if (r2 == 0) goto L_0x01af
            com.portfolio.platform.data.model.InstalledApp r0 = r2.getInstalledApp()
            if (r0 == 0) goto L_0x01aa
            java.lang.Boolean r0 = r0.isSelected()
            java.lang.String r3 = "app.installedApp!!.isSelected"
            com.fossil.ee7.a(r0, r3)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x01af
            r0 = r24
            goto L_0x01ba
        L_0x01aa:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x01af:
            if (r2 != 0) goto L_0x01bc
            int r0 = r8.getHour()
            int r0 = com.fossil.ze5.c(r0)
            short r0 = (short) r0
        L_0x01ba:
            r13 = 1
            goto L_0x01be
        L_0x01bc:
            r0 = -1
            r13 = 0
        L_0x01be:
            if (r13 == 0) goto L_0x0201
            if (r25 == 0) goto L_0x01c9
            com.portfolio.platform.data.model.setting.SpecialSkuSetting$AngleSubeye r2 = com.portfolio.platform.data.model.setting.SpecialSkuSetting.AngleSubeye.MOVEMBER
            int r2 = r2.getAngleForApp()
            short r6 = (short) r2
        L_0x01c9:
            java.lang.String r2 = r8.getName()
            if (r2 != 0) goto L_0x01d1
            r2 = r10
            goto L_0x01d5
        L_0x01d1:
            java.lang.String r2 = r8.getName()
        L_0x01d5:
            com.fossil.aa0 r3 = new com.fossil.aa0
            r4 = 10000(0x2710, float:1.4013E-41)
            r3.<init>(r0, r0, r6, r4)
            com.misfit.frameworks.buttonservice.model.notification.FNotification r0 = new com.misfit.frameworks.buttonservice.model.notification.FNotification
            java.lang.String r4 = "appName"
            com.fossil.ee7.a(r2, r4)
            java.lang.String r4 = r8.getType()
            java.lang.String r8 = "item.type"
            com.fossil.ee7.a(r4, r8)
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r8 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.NOTIFICATION
            r0.<init>(r2, r4, r10, r8)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r2 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter
            r2.<init>(r0)
            r2.setHandMovingConfig(r3)
            com.fossil.ca0 r0 = com.fossil.ca0.DEFAULT_OTHER_APPS
            r2.setVibePattern(r0)
            r1.add(r2)
        L_0x0201:
            r0 = r21
            r2 = r17
            goto L_0x001b
        L_0x0207:
            r17 = r2
            int r5 = r5 + 1
            r0 = r21
            goto L_0x000f
        L_0x020f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cm5.a(android.util.SparseArray, java.util.List, java.util.List, short, boolean):java.util.List");
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(List<at5> list, short s, boolean z) {
        short s2;
        ArrayList arrayList = new ArrayList();
        if (!z) {
            s2 = -1;
        } else {
            s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        }
        for (at5 at5 : list) {
            InstalledApp installedApp = at5.getInstalledApp();
            if (installedApp != null) {
                Boolean isSelected = installedApp.isSelected();
                ee7.a((Object) isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    String title = installedApp.getTitle() == null ? "" : installedApp.getTitle();
                    aa0 aa0 = new aa0(s, s, s2, 10000);
                    ee7.a((Object) title, "appName");
                    String identifier = installedApp.getIdentifier();
                    ee7.a((Object) identifier, "it.identifier");
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(title, identifier, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                    appNotificationFilter.setHandMovingConfig(aa0);
                    appNotificationFilter.setVibePattern(ca0.DEFAULT_OTHER_APPS);
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        return arrayList;
    }
}
