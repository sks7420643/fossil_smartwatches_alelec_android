package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b37 extends u27 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public b37(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.u27
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxapi_getmessage_req_lang");
        this.d = bundle.getString("_wxapi_getmessage_req_country");
    }

    @DexIgnore
    @Override // com.fossil.u27
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.u27
    public int b() {
        return 3;
    }

    @DexIgnore
    @Override // com.fossil.u27
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putString("_wxapi_getmessage_req_lang", this.c);
        bundle.putString("_wxapi_getmessage_req_country", this.d);
    }
}
