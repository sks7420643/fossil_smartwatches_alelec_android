package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fo extends eo<qn> {
    @DexIgnore
    public static /* final */ String j; // = im.a("NetworkStateTracker");
    @DexIgnore
    public /* final */ ConnectivityManager g; // = ((ConnectivityManager) ((eo) this).b.getSystemService("connectivity"));
    @DexIgnore
    public b h;
    @DexIgnore
    public a i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                im.a().a(fo.j, "Network broadcast received", new Throwable[0]);
                fo foVar = fo.this;
                foVar.a(foVar.d());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            im.a().a(fo.j, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            fo foVar = fo.this;
            foVar.a(foVar.d());
        }

        @DexIgnore
        public void onLost(Network network) {
            im.a().a(fo.j, "Network connection lost", new Throwable[0]);
            fo foVar = fo.this;
            foVar.a(foVar.d());
        }
    }

    @DexIgnore
    public fo(Context context, vp vpVar) {
        super(context, vpVar);
        if (f()) {
            this.h = new b();
        } else {
            this.i = new a();
        }
    }

    @DexIgnore
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    @Override // com.fossil.eo
    public void b() {
        if (f()) {
            try {
                im.a().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                im.a().b(j, "Received exception while registering network callback", e);
            }
        } else {
            im.a().a(j, "Registering broadcast receiver", new Throwable[0]);
            ((eo) this).b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    @Override // com.fossil.eo
    public void c() {
        if (f()) {
            try {
                im.a().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                im.a().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            im.a().a(j, "Unregistering broadcast receiver", new Throwable[0]);
            ((eo) this).b.unregisterReceiver(this.i);
        }
    }

    @DexIgnore
    public qn d() {
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean e = e();
        boolean a2 = z7.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new qn(z2, e, a2, z);
    }

    @DexIgnore
    public final boolean e() {
        NetworkCapabilities networkCapabilities;
        if (Build.VERSION.SDK_INT >= 23 && (networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork())) != null && networkCapabilities.hasCapability(16)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.eo
    public qn a() {
        return d();
    }
}
