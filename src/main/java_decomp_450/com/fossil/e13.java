package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e13 implements f13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true);

    @DexIgnore
    @Override // com.fossil.f13
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
