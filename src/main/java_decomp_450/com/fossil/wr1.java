package com.fossil;

import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr1 extends k60 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ ci1 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public String j;
    @DexIgnore
    public m60 k;
    @DexIgnore
    public sn0 l;
    @DexIgnore
    public JSONObject m;

    @DexIgnore
    public /* synthetic */ wr1(String str, ci1 ci1, String str2, String str3, String str4, boolean z, String str5, m60 m60, sn0 sn0, JSONObject jSONObject, int i2) {
        String str6;
        m60 m602;
        String str7;
        String str8 = (i2 & 64) != 0 ? "" : str5;
        if ((i2 & 128) != 0) {
            str6 = str8;
            str7 = str2;
            m602 = new m60("", str2, "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
        } else {
            str7 = str2;
            str6 = str8;
            m602 = m60;
        }
        sn0 sn02 = (i2 & 256) != 0 ? new sn0("", "", "", 0, "", null, 32) : sn0;
        JSONObject jSONObject2 = (i2 & 512) != 0 ? new JSONObject() : jSONObject;
        String str9 = str7 + str4 + str3 + str + yz0.a(ci1);
        this.a = System.currentTimeMillis();
        this.d = str;
        this.e = ci1;
        this.f = str7;
        this.g = str3;
        this.h = str4;
        this.i = z;
        this.j = str6;
        this.k = m602;
        this.l = sn02;
        this.m = jSONObject2;
        this.c = yh0.a("UUID.randomUUID().toString()");
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("timestamp", yz0.a(this.a)).put("line_number", this.b);
        ee7.a((Object) put, "JSONObject().put(LogEntr\u2026.LINE_NUMBER, lineNumber)");
        put.put("phase_uuid", this.h).put("phase_name", this.g).put("entry_uuid", this.c).put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, this.d).put("type", yz0.a(this.e)).put("is_success", this.i).put("value", this.m).put("user_id", this.l.c).put(Constants.SERIAL_NUMBER, this.k.getSerialNumber()).put("model_number", this.k.getModelNumber()).put(Constants.FIRMWARE_VERSION, this.k.getFirmwareVersion()).put("phone_model", this.l.b).put("os", this.l.f).put(Constants.OS_VERSION, this.l.a).put(FetchedAppGateKeepersManager.APPLICATION_SDK_VERSION, this.l.e).put("session_uuid", this.j).put(LegacySecondTimezoneSetting.COLUMN_TIMEZONE_OFFSET, this.l.d);
        return put;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.f;
    }
}
