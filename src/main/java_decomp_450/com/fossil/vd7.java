package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vd7 implements sf7, Serializable {
    @DexIgnore
    public static /* final */ Object NO_RECEIVER; // = a.a;
    @DexIgnore
    public /* final */ Object receiver;
    @DexIgnore
    public transient sf7 reflected;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Serializable {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        private Object readResolve() throws ObjectStreamException {
            return a;
        }
    }

    @DexIgnore
    public vd7() {
        this(NO_RECEIVER);
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public Object call(Object... objArr) {
        return getReflected().call(objArr);
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public Object callBy(Map map) {
        return getReflected().callBy(map);
    }

    @DexIgnore
    public sf7 compute() {
        sf7 sf7 = this.reflected;
        if (sf7 != null) {
            return sf7;
        }
        sf7 computeReflected = computeReflected();
        this.reflected = computeReflected;
        return computeReflected;
    }

    @DexIgnore
    public abstract sf7 computeReflected();

    @DexIgnore
    @Override // com.fossil.rf7
    public List<Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    @DexIgnore
    public Object getBoundReceiver() {
        return this.receiver;
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public String getName() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public uf7 getOwner() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public List<Object> getParameters() {
        return getReflected().getParameters();
    }

    @DexIgnore
    public sf7 getReflected() {
        sf7 compute = compute();
        if (compute != this) {
            return compute;
        }
        throw new uc7();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public cg7 getReturnType() {
        return getReflected().getReturnType();
    }

    @DexIgnore
    public String getSignature() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public List<Object> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public dg7 getVisibility() {
        return getReflected().getVisibility();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @DexIgnore
    @Override // com.fossil.sf7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public vd7(Object obj) {
        this.receiver = obj;
    }
}
