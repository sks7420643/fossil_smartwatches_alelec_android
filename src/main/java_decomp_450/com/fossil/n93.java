package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<n93> CREATOR; // = new ea3();
    @DexIgnore
    public /* final */ List<LatLng> a;
    @DexIgnore
    public /* final */ List<List<LatLng>> b;
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public List<l93> p;

    @DexIgnore
    public n93() {
        this.c = 10.0f;
        this.d = -16777216;
        this.e = 0;
        this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.g = true;
        this.h = false;
        this.i = false;
        this.j = 0;
        this.p = null;
        this.a = new ArrayList();
        this.b = new ArrayList();
    }

    @DexIgnore
    public final boolean A() {
        return this.i;
    }

    @DexIgnore
    public final boolean B() {
        return this.h;
    }

    @DexIgnore
    public final boolean C() {
        return this.g;
    }

    @DexIgnore
    public final n93 a(float f2) {
        this.c = f2;
        return this;
    }

    @DexIgnore
    public final n93 b(int i2) {
        this.d = i2;
        return this;
    }

    @DexIgnore
    public final n93 c(Iterable<LatLng> iterable) {
        for (LatLng latLng : iterable) {
            this.a.add(latLng);
        }
        return this;
    }

    @DexIgnore
    public final int e() {
        return this.e;
    }

    @DexIgnore
    public final List<LatLng> g() {
        return this.a;
    }

    @DexIgnore
    public final int v() {
        return this.d;
    }

    @DexIgnore
    public final int w() {
        return this.j;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.c(parcel, 2, g(), false);
        k72.a(parcel, 3, (List) this.b, false);
        k72.a(parcel, 4, y());
        k72.a(parcel, 5, v());
        k72.a(parcel, 6, e());
        k72.a(parcel, 7, z());
        k72.a(parcel, 8, C());
        k72.a(parcel, 9, B());
        k72.a(parcel, 10, A());
        k72.a(parcel, 11, w());
        k72.c(parcel, 12, x(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final List<l93> x() {
        return this.p;
    }

    @DexIgnore
    public final float y() {
        return this.c;
    }

    @DexIgnore
    public final float z() {
        return this.f;
    }

    @DexIgnore
    public final n93 a(int i2) {
        this.e = i2;
        return this;
    }

    @DexIgnore
    public final n93 b(float f2) {
        this.f = f2;
        return this;
    }

    @DexIgnore
    public final n93 a(boolean z) {
        this.i = z;
        return this;
    }

    @DexIgnore
    public final n93 b(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final n93 c(boolean z) {
        this.g = z;
        return this;
    }

    @DexIgnore
    public n93(List<LatLng> list, List list2, float f2, int i2, int i3, float f3, boolean z, boolean z2, boolean z3, int i4, List<l93> list3) {
        this.c = 10.0f;
        this.d = -16777216;
        this.e = 0;
        this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.g = true;
        this.h = false;
        this.i = false;
        this.j = 0;
        this.p = null;
        this.a = list;
        this.b = list2;
        this.c = f2;
        this.d = i2;
        this.e = i3;
        this.f = f3;
        this.g = z;
        this.h = z2;
        this.i = z3;
        this.j = i4;
        this.p = list3;
    }
}
