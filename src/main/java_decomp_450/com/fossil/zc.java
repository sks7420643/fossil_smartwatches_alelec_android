package com.fossil;

import com.fossil.ik7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc<T> {
    @DexIgnore
    public ik7 a;
    @DexIgnore
    public ik7 b;
    @DexIgnore
    public /* final */ dd<T> c;
    @DexIgnore
    public /* final */ kd7<vd<T>, fb7<? super i97>, Object> d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ yi7 f;
    @DexIgnore
    public /* final */ vc7<i97> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "androidx.lifecycle.BlockRunner$cancel$1", f = "CoroutineLiveData.kt", l = {187}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zc this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(zc zcVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zcVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                long e = this.this$0.e;
                this.L$0 = yi7;
                this.label = 1;
                if (kj7.a(e, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.c.c()) {
                ik7 d = this.this$0.a;
                if (d != null) {
                    ik7.a.a(d, null, 1, null);
                }
                this.this$0.a = null;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "androidx.lifecycle.BlockRunner$maybeRun$1", f = "CoroutineLiveData.kt", l = {176}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zc this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zc zcVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zcVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                wd wdVar = new wd(this.this$0.c, yi7.a());
                kd7 a2 = this.this$0.d;
                this.L$0 = yi7;
                this.L$1 = wdVar;
                this.label = 1;
                if (a2.invoke(wdVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                wd wdVar2 = (wd) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.invoke();
            return i97.a;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.kd7<? super com.fossil.vd<T>, ? super com.fossil.fb7<? super com.fossil.i97>, ? extends java.lang.Object> */
    /* JADX WARN: Multi-variable type inference failed */
    public zc(dd<T> ddVar, kd7<? super vd<T>, ? super fb7<? super i97>, ? extends Object> kd7, long j, yi7 yi7, vc7<i97> vc7) {
        ee7.b(ddVar, "liveData");
        ee7.b(kd7, "block");
        ee7.b(yi7, "scope");
        ee7.b(vc7, "onDone");
        this.c = ddVar;
        this.d = kd7;
        this.e = j;
        this.f = yi7;
        this.g = vc7;
    }

    @DexIgnore
    public final void b() {
        ik7 ik7 = this.b;
        if (ik7 != null) {
            ik7.a.a(ik7, null, 1, null);
        }
        this.b = null;
        if (this.a == null) {
            this.a = xh7.b(this.f, null, null, new b(this, null), 3, null);
        }
    }

    @DexIgnore
    public final void a() {
        if (this.b == null) {
            this.b = xh7.b(this.f, qj7.c().g(), null, new a(this, null), 2, null);
            return;
        }
        throw new IllegalStateException("Cancel call cannot happen without a maybeRun".toString());
    }
}
