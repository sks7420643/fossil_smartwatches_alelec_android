package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr7 implements sr7 {
    @DexIgnore
    public byte a;
    @DexIgnore
    public /* final */ mr7 b;
    @DexIgnore
    public /* final */ Inflater c;
    @DexIgnore
    public /* final */ gr7 d;
    @DexIgnore
    public /* final */ CRC32 e; // = new CRC32();

    @DexIgnore
    public fr7(sr7 sr7) {
        ee7.b(sr7, "source");
        this.b = new mr7(sr7);
        Inflater inflater = new Inflater(true);
        this.c = inflater;
        this.d = new gr7(this.b, inflater);
    }

    @DexIgnore
    public final void a() throws IOException {
        this.b.h(10);
        byte e2 = this.b.a.e(3);
        boolean z = true;
        boolean z2 = ((e2 >> 1) & 1) == 1;
        if (z2) {
            a(this.b.a, 0, 10);
        }
        a("ID1ID2", 8075, this.b.readShort());
        this.b.skip(8);
        if (((e2 >> 2) & 1) == 1) {
            this.b.h(2);
            if (z2) {
                a(this.b.a, 0, 2);
            }
            long r = (long) this.b.a.r();
            this.b.h(r);
            if (z2) {
                a(this.b.a, 0, r);
            }
            this.b.skip(r);
        }
        if (((e2 >> 3) & 1) == 1) {
            long a2 = this.b.a((byte) 0);
            if (a2 != -1) {
                if (z2) {
                    a(this.b.a, 0, a2 + 1);
                }
                this.b.skip(a2 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((e2 >> 4) & 1) != 1) {
            z = false;
        }
        if (z) {
            long a3 = this.b.a((byte) 0);
            if (a3 != -1) {
                if (z2) {
                    a(this.b.a, 0, a3 + 1);
                }
                this.b.skip(a3 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z2) {
            a("FHCRC", this.b.b(), (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) throws IOException {
        ee7.b(yq7, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (i == 0) {
            return 0;
        } else {
            if (this.a == 0) {
                a();
                this.a = 1;
            }
            if (this.a == 1) {
                long x = yq7.x();
                long b2 = this.d.b(yq7, j);
                if (b2 != -1) {
                    a(yq7, x, b2);
                    return b2;
                }
                this.a = 2;
            }
            if (this.a == 2) {
                b();
                this.a = 3;
                if (!this.b.h()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
    public void close() throws IOException {
        this.d.close();
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.b.d();
    }

    @DexIgnore
    public final void b() throws IOException {
        a("CRC", this.b.a(), (int) this.e.getValue());
        a("ISIZE", this.b.a(), (int) this.c.getBytesWritten());
    }

    @DexIgnore
    public final void a(yq7 yq7, long j, long j2) {
        nr7 nr7 = yq7.a;
        if (nr7 != null) {
            do {
                int i = nr7.c;
                int i2 = nr7.b;
                if (j >= ((long) (i - i2))) {
                    j -= (long) (i - i2);
                    nr7 = nr7.f;
                } else {
                    while (j2 > 0) {
                        int i3 = (int) (((long) nr7.b) + j);
                        int min = (int) Math.min((long) (nr7.c - i3), j2);
                        this.e.update(nr7.a, i3, min);
                        j2 -= (long) min;
                        nr7 = nr7.f;
                        if (nr7 != null) {
                            j = 0;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    return;
                }
            } while (nr7 != null);
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str, int i, int i2) {
        if (i2 != i) {
            String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}, 3));
            ee7.a((Object) format, "java.lang.String.format(this, *args)");
            throw new IOException(format);
        }
    }
}
