package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t35 extends s35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L;
    @DexIgnore
    public long J;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        L = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        L.put(2131362517, 2);
        L.put(2131362765, 3);
        L.put(2131362337, 4);
        L.put(2131363390, 5);
        L.put(2131362766, 6);
        L.put(2131362339, 7);
        L.put(2131363391, 8);
        L.put(2131362784, 9);
        L.put(2131362325, 10);
        L.put(2131362736, 11);
        L.put(2131363108, 12);
        L.put(2131362477, 13);
        L.put(2131363392, 14);
        L.put(2131362407, 15);
        L.put(2131362747, 16);
        L.put(2131361960, 17);
        L.put(2131362986, 18);
    }
    */

    @DexIgnore
    public t35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 19, K, L));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.J != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.J = 1;
        }
        g();
    }

    @DexIgnore
    public t35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[17], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (RTLImageView) objArr[11], (ConstraintLayout) objArr[16], (LinearLayout) objArr[3], (LinearLayout) objArr[6], (LinearLayout) objArr[9], (ConstraintLayout) objArr[0], (RecyclerView) objArr[18], (FlexibleSwitchCompat) objArr[12], (View) objArr[5], (View) objArr[8], (View) objArr[14]);
        this.J = -1;
        ((s35) this).D.setTag(null);
        a(view);
        f();
    }
}
