package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx1 implements Factory<rx1> {
    @DexIgnore
    public /* final */ Provider<by1> a;
    @DexIgnore
    public /* final */ Provider<by1> b;
    @DexIgnore
    public /* final */ Provider<tw1> c;
    @DexIgnore
    public /* final */ Provider<xx1> d;

    @DexIgnore
    public sx1(Provider<by1> provider, Provider<by1> provider2, Provider<tw1> provider3, Provider<xx1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static sx1 a(Provider<by1> provider, Provider<by1> provider2, Provider<tw1> provider3, Provider<xx1> provider4) {
        return new sx1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rx1 get() {
        return new rx1(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
