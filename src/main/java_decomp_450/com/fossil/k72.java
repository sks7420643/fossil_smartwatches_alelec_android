package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k72 {
    @DexIgnore
    public static int a(Parcel parcel) {
        return b(parcel, 20293);
    }

    @DexIgnore
    public static void b(Parcel parcel, int i, int i2) {
        if (i2 >= 65535) {
            parcel.writeInt(i | -65536);
            parcel.writeInt(i2);
            return;
        }
        parcel.writeInt(i | (i2 << 16));
    }

    @DexIgnore
    public static void c(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i - 4);
        parcel.writeInt(dataPosition - i);
        parcel.setDataPosition(dataPosition);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i) {
        c(parcel, i);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, boolean z) {
        b(parcel, i, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    @DexIgnore
    public static int b(Parcel parcel, int i) {
        parcel.writeInt(i | -65536);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Boolean bool, boolean z) {
        if (bool != null) {
            b(parcel, i, 4);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static <T extends Parcelable> void c(Parcel parcel, int i, List<T> list, boolean z) {
        if (list != null) {
            int b = b(parcel, i);
            int size = list.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                T t = list.get(i2);
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    a(parcel, t, 0);
                }
            }
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void b(Parcel parcel, int i, List<String> list, boolean z) {
        if (list != null) {
            int b = b(parcel, i);
            parcel.writeStringList(list);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, byte b) {
        b(parcel, i, 4);
        parcel.writeInt(b);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, int i2) {
        b(parcel, i, 4);
        parcel.writeInt(i2);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Integer num, boolean z) {
        if (num != null) {
            b(parcel, i, 4);
            parcel.writeInt(num.intValue());
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, long j) {
        b(parcel, i, 8);
        parcel.writeLong(j);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Long l, boolean z) {
        if (l != null) {
            b(parcel, i, 8);
            parcel.writeLong(l.longValue());
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, float f) {
        b(parcel, i, 4);
        parcel.writeFloat(f);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Float f, boolean z) {
        if (f != null) {
            b(parcel, i, 4);
            parcel.writeFloat(f.floatValue());
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, double d) {
        b(parcel, i, 8);
        parcel.writeDouble(d);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Double d, boolean z) {
        if (d != null) {
            b(parcel, i, 8);
            parcel.writeDouble(d.doubleValue());
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, String str, boolean z) {
        if (str != null) {
            int b = b(parcel, i);
            parcel.writeString(str);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, IBinder iBinder, boolean z) {
        if (iBinder != null) {
            int b = b(parcel, i);
            parcel.writeStrongBinder(iBinder);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Parcelable parcelable, int i2, boolean z) {
        if (parcelable != null) {
            int b = b(parcel, i);
            parcelable.writeToParcel(parcel, i2);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, Bundle bundle, boolean z) {
        if (bundle != null) {
            int b = b(parcel, i);
            parcel.writeBundle(bundle);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, byte[] bArr, boolean z) {
        if (bArr != null) {
            int b = b(parcel, i);
            parcel.writeByteArray(bArr);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, int[] iArr, boolean z) {
        if (iArr != null) {
            int b = b(parcel, i);
            parcel.writeIntArray(iArr);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, float[] fArr, boolean z) {
        if (fArr != null) {
            int b = b(parcel, i);
            parcel.writeFloatArray(fArr);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, String[] strArr, boolean z) {
        if (strArr != null) {
            int b = b(parcel, i);
            parcel.writeStringArray(strArr);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static <T extends Parcelable> void a(Parcel parcel, int i, T[] tArr, int i2, boolean z) {
        if (tArr != null) {
            int b = b(parcel, i);
            int length = tArr.length;
            parcel.writeInt(length);
            for (T t : tArr) {
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    a(parcel, t, i2);
                }
            }
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }

    @DexIgnore
    public static <T extends Parcelable> void a(Parcel parcel, T t, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        t.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }

    @DexIgnore
    public static void a(Parcel parcel, int i, List list, boolean z) {
        if (list != null) {
            int b = b(parcel, i);
            parcel.writeList(list);
            c(parcel, b);
        } else if (z) {
            b(parcel, i, 0);
        }
    }
}
