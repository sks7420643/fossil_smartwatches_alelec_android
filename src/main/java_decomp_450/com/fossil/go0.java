package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum go0 {
    FIRE_TIME((byte) 0),
    c((byte) 1),
    MESSAGE((byte) 2);
    
    @DexIgnore
    public static /* final */ km0 f; // = new km0(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public go0(byte b) {
        this.a = b;
    }
}
