package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qt5 extends io5<pt5> {
    @DexIgnore
    void B(String str);

    @DexIgnore
    void F0();

    @DexIgnore
    List<ContactGroup> H0();

    @DexIgnore
    boolean P0();

    @DexIgnore
    int T();

    @DexIgnore
    void U0();

    @DexIgnore
    void a();

    @DexIgnore
    void a(ContactGroup contactGroup);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void close();

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void e0();

    @DexIgnore
    int h0();

    @DexIgnore
    void m(String str);

    @DexIgnore
    void r(List<ContactGroup> list);

    @DexIgnore
    void u(boolean z);

    @DexIgnore
    void w0();
}
