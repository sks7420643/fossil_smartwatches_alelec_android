package com.fossil;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at6 extends go5 implements zs6 {
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public ys6 f;
    @DexIgnore
    public qw6<y55> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final at6 a() {
            return new at6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ at6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ y55 a;

            @DexIgnore
            public a(y55 y55) {
                this.a = y55;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.a.L;
                ee7.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.a.M;
                ee7.a((Object) flexibleButton, "it.tvLogin");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, at6 at6) {
            this.a = view;
            this.b = at6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                y55 y55 = (y55) at6.a(this.b).a();
                if (y55 != null) {
                    try {
                        FlexibleTextView flexibleTextView = y55.L;
                        ee7.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = y55.M;
                        ee7.a((Object) flexibleButton, "it.tvLogin");
                        flexibleButton.setVisibility(8);
                        i97 i97 = i97.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e);
                        i97 i972 = i97.a;
                    }
                }
            } else {
                y55 y552 = (y55) at6.a(this.b).a();
                if (y552 != null) {
                    try {
                        ee7.a((Object) y552, "it");
                        y552.d().postDelayed(new a(y552), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e2);
                        i97 i973 = i97.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public c(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            at6.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public d(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            at6.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public e(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.D;
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y55 a;
        @DexIgnore
        public /* final */ /* synthetic */ at6 b;

        @DexIgnore
        public f(y55 y55, at6 at6) {
            this.a = y55;
            this.b = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.a.w;
            ee7.a((Object) flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.a.v;
            ee7.a((Object) flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.b.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public g(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            at6.b(this.a).a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public h(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            at6.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public i(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            at6.b(this.a).b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public j(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.a.f1();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public k(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public l(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            at6.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public m(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            at6.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ at6 a;

        @DexIgnore
        public n(at6 at6) {
            this.a = at6;
        }

        @DexIgnore
        public final void onClick(View view) {
            at6.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ y55 a;

        @DexIgnore
        public o(y55 y55) {
            this.a = y55;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.a.J;
                ee7.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.a.K;
                ee7.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
                flexibleTextView2.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView3 = this.a.J;
            ee7.a((Object) flexibleTextView3, "binding.tvErrorCheckCharacter");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = this.a.K;
            ee7.a((Object) flexibleTextView4, "binding.tvErrorCheckCombine");
            flexibleTextView4.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ qw6 a(at6 at6) {
        qw6<y55> qw6 = at6.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ys6 b(at6 at6) {
        ys6 ys6 = at6.f;
        if (ys6 != null) {
            return ys6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void C0() {
        if (isActive()) {
            qw6<y55> qw6 = this.g;
            if (qw6 != null) {
                y55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    ee7.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.s;
                    ee7.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.s;
                    ee7.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(false);
                    a2.s.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void S(String str) {
        ee7.b(str, "errorMessage");
        qw6<y55> qw6 = this.g;
        if (qw6 != null) {
            y55 a2 = qw6.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.y;
                ee7.a((Object) rTLImageView, "it.ivCheckedEmail");
                if (rTLImageView.getVisibility() == 0) {
                    RTLImageView rTLImageView2 = a2.y;
                    ee7.a((Object) rTLImageView2, "it.ivCheckedEmail");
                    rTLImageView2.setVisibility(8);
                }
                FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void c0() {
        if (isActive()) {
            qw6<y55> qw6 = this.g;
            if (qw6 != null) {
                y55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    ee7.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.s;
                    ee7.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.s;
                    ee7.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(true);
                    a2.s.a("flexible_button_primary");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "SignUpFragment";
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void e() {
        qw6<y55> qw6 = this.g;
        if (qw6 != null) {
            y55 a2 = qw6.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.F, "progress", 0, 10);
                ee7.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(500L);
                ofInt.start();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void f() {
        a();
    }

    @DexIgnore
    public final void f1() {
        qw6<y55> qw6 = this.g;
        if (qw6 != null) {
            y55 a2 = qw6.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                ee7.a((Object) flexibleButton, "this.btContinue");
                if (flexibleButton.isEnabled()) {
                    ys6 ys6 = this.f;
                    if (ys6 != null) {
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                        ee7.a((Object) flexibleTextInputEditText, "etEmail");
                        String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                        ee7.a((Object) flexibleTextInputEditText2, "etPassword");
                        ys6.a(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                        return;
                    }
                    ee7.d("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void g() {
        String string = getString(2131886948);
        ee7.a((Object) string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        W(string);
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void i() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.z;
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            HomeActivity.a.a(aVar, requireActivity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            ys6 ys6 = this.f;
            if (ys6 != null) {
                ee7.a((Object) signUpSocialAuth, "authCode");
                ys6.a(signUpSocialAuth);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        y55 y55 = (y55) qb.a(layoutInflater, 2131558621, viewGroup, false, a1());
        ee7.a((Object) y55, "bindingLocal");
        View d2 = y55.d();
        ee7.a((Object) d2, "bindingLocal.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        qw6<y55> qw6 = new qw6<>(this, y55);
        this.g = qw6;
        if (qw6 != null) {
            y55 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ys6 ys6 = this.f;
        if (ys6 != null) {
            ys6.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ys6 ys6 = this.f;
        if (ys6 != null) {
            ys6.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<y55> qw6 = this.g;
        if (qw6 != null) {
            y55 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new f(a2, this));
                a2.t.addTextChangedListener(new g(this));
                a2.t.setOnFocusChangeListener(new h(this));
                a2.u.addTextChangedListener(new i(this));
                a2.u.setOnFocusChangeListener(new o(a2));
                a2.u.setOnEditorActionListener(new j(this));
                a2.z.setOnClickListener(new k(this));
                a2.A.setOnClickListener(new l(this));
                a2.B.setOnClickListener(new m(this));
                a2.x.setOnClickListener(new n(this));
                a2.C.setOnClickListener(new c(this));
                a2.D.setOnClickListener(new d(this));
                a2.M.setOnClickListener(new e(this));
            }
            V("sign_up_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void q() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = rw6.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.C;
            ee7.a((Object) activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void y(boolean z) {
        if (isActive()) {
            qw6<y55> qw6 = this.g;
            if (qw6 != null) {
                y55 a2 = qw6.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.D;
                    ee7.a((Object) constraintLayout, "it.ivWeibo");
                    constraintLayout.setVisibility(0);
                    ConstraintLayout constraintLayout2 = a2.C;
                    ee7.a((Object) constraintLayout2, "it.ivWechat");
                    constraintLayout2.setVisibility(0);
                    ImageView imageView = a2.B;
                    ee7.a((Object) imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.A;
                    ee7.a((Object) imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.D;
                ee7.a((Object) constraintLayout3, "it.ivWeibo");
                constraintLayout3.setVisibility(8);
                ConstraintLayout constraintLayout4 = a2.C;
                ee7.a((Object) constraintLayout4, "it.ivWechat");
                constraintLayout4.setVisibility(8);
                ImageView imageView3 = a2.B;
                ee7.a((Object) imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.A;
                ee7.a((Object) imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void b(int i2, String str) {
        ee7.b(str, "errorMessage");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(ys6 ys6) {
        ee7.b(ys6, "presenter");
        this.f = ys6;
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void a(boolean z, boolean z2, String str) {
        RTLImageView rTLImageView;
        ee7.b(str, "errorMessage");
        if (isActive()) {
            qw6<y55> qw6 = this.g;
            if (qw6 != null) {
                y55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                    ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                }
                qw6<y55> qw62 = this.g;
                if (qw62 != null) {
                    y55 a3 = qw62.a();
                    if (a3 != null && (rTLImageView = a3.y) != null) {
                        ee7.a((Object) rTLImageView, "it");
                        rTLImageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void b(boolean z, boolean z2) {
        if (isActive()) {
            qw6<y55> qw6 = this.g;
            if (qw6 != null) {
                y55 a2 = qw6.a();
                if (a2 != null) {
                    if (z) {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231110), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231110), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void e(int i2, String str) {
        ee7.b(str, "errorMessage");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void b(SignUpEmailAuth signUpEmailAuth) {
        FragmentActivity activity;
        ee7.b(signUpEmailAuth, "emailAuth");
        if (isActive() && (activity = getActivity()) != null) {
            EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }

    @DexIgnore
    @Override // com.fossil.zs6
    public void b(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity, signUpSocialAuth);
            activity.finish();
        }
    }
}
