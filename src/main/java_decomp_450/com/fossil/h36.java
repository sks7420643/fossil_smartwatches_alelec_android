package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.imagefilters.FilterType;
import com.fossil.j36;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h36 extends go5 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public j36 g;
    @DexIgnore
    public qw6<c55> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final h36 a(ArrayList<fz5> arrayList, FilterType filterType) {
            ee7.b(arrayList, "complications");
            ee7.b(filterType, "filterType");
            h36 h36 = new h36();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            bundle.putSerializable("FILTER_TYPE_ARG", filterType);
            h36.setArguments(bundle);
            return h36;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<j36.b> {
        @DexIgnore
        public /* final */ /* synthetic */ h36 a;

        @DexIgnore
        public b(h36 h36) {
            this.a = h36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(j36.b bVar) {
            ImageView imageView;
            c55 a2 = this.a.g1();
            if (a2 != null && (imageView = a2.t) != null) {
                imageView.setImageDrawable(bVar.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<j36.c> {
        @DexIgnore
        public /* final */ /* synthetic */ h36 a;

        @DexIgnore
        public c(h36 h36) {
            this.a = h36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(j36.c cVar) {
            int i;
            this.a.a();
            if (cVar.c()) {
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Success");
                try {
                    i = Color.parseColor(cVar.a());
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("PreviewFragment", e.getMessage());
                    e.printStackTrace();
                    i = Color.parseColor("#FFFFFF");
                }
                this.a.a(cVar.b(), Integer.valueOf(i));
                return;
            }
            FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive loadComplicationSettingResult Fail");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<j36.d> {
        @DexIgnore
        public /* final */ /* synthetic */ h36 a;

        @DexIgnore
        public d(h36 h36) {
            this.a = h36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(j36.d dVar) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                this.a.a();
                if (dVar.b()) {
                    FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Success");
                    Intent intent = new Intent();
                    intent.putExtra("WATCH_FACE_ID", dVar.a());
                    activity.setResult(-1, intent);
                    activity.finish();
                    return;
                }
                FLogger.INSTANCE.getLocal().d("PreviewFragment", "Receive saveThemeResult Fail");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ h36 a;

        @DexIgnore
        public e(h36 h36) {
            this.a = h36;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ h36 a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public f(h36 h36, int i) {
            this.a = h36;
            this.b = i;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b();
            h36.b(this.a).c(this.b);
        }
    }

    @DexIgnore
    public static final /* synthetic */ j36 b(h36 h36) {
        j36 j36 = h36.g;
        if (j36 != null) {
            return j36;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) {
            return true;
        }
        fragmentManager.E();
        return true;
    }

    @DexIgnore
    public final void f1() {
        j36 j36 = this.g;
        if (j36 != null) {
            j36.c().a(getViewLifecycleOwner(), new b(this));
            j36 j362 = this.g;
            if (j362 != null) {
                j362.d().a(getViewLifecycleOwner(), new c(this));
                j36 j363 = this.g;
                if (j363 != null) {
                    j363.e().a(getViewLifecycleOwner(), new d(this));
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final c55 g1() {
        qw6<c55> qw6 = this.h;
        if (qw6 != null) {
            return qw6.a();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().s().a(this);
        FragmentActivity requireActivity = requireActivity();
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(requireActivity, rj4).a(j36.class);
            ee7.a((Object) a2, "ViewModelProviders.of(re\u2026iewViewModel::class.java)");
            this.g = (j36) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    @Override // androidx.fragment.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View onCreateView(android.view.LayoutInflater r3, android.view.ViewGroup r4, android.os.Bundle r5) {
        /*
            r2 = this;
            java.lang.String r0 = "inflater"
            com.fossil.ee7.b(r3, r0)
            super.onCreateView(r3, r4, r5)
            android.content.Context r3 = r2.getContext()
            android.view.LayoutInflater r3 = android.view.LayoutInflater.from(r3)
            com.fossil.pb r4 = r2.a1()
            r5 = 0
            r0 = 2131558607(0x7f0d00cf, float:1.8742535E38)
            r1 = 1
            androidx.databinding.ViewDataBinding r3 = com.fossil.qb.a(r3, r0, r5, r1, r4)
            com.fossil.c55 r3 = (com.fossil.c55) r3
            com.fossil.qw6 r4 = new com.fossil.qw6
            r4.<init>(r2, r3)
            r2.h = r4
            com.fossil.c55 r4 = r2.g1()
            if (r4 == 0) goto L_0x0038
            android.widget.TextView r4 = r4.v
            if (r4 == 0) goto L_0x0038
            com.fossil.h36$e r0 = new com.fossil.h36$e
            r0.<init>(r2)
            com.fossil.bf5.a(r4, r0)
        L_0x0038:
            r2.f1()
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r0 = 2131231251(0x7f080213, float:1.8078578E38)
            android.graphics.drawable.Drawable r4 = com.fossil.v6.c(r4, r0)
            if (r4 == 0) goto L_0x006a
            r4.getIntrinsicHeight()
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            android.graphics.drawable.Drawable r4 = com.fossil.v6.c(r4, r0)
            if (r4 == 0) goto L_0x0062
            int r4 = r4.getMinimumWidth()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            goto L_0x0063
        L_0x0062:
            r4 = r5
        L_0x0063:
            if (r4 == 0) goto L_0x006a
            int r4 = r4.intValue()
            goto L_0x007b
        L_0x006a:
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            android.content.res.Resources r4 = r4.getResources()
            r0 = 2131165418(0x7f0700ea, float:1.7945053E38)
            int r4 = r4.getDimensionPixelSize(r0)
        L_0x007b:
            com.fossil.c55 r0 = r2.g1()
            if (r0 == 0) goto L_0x008d
            android.widget.TextView r0 = r0.u
            if (r0 == 0) goto L_0x008d
            com.fossil.h36$f r1 = new com.fossil.h36$f
            r1.<init>(r2, r4)
            com.fossil.bf5.a(r0, r1)
        L_0x008d:
            com.fossil.j36 r0 = r2.g
            if (r0 == 0) goto L_0x00ac
            r0.a(r4)
            android.os.Bundle r4 = r2.getArguments()
            if (r4 == 0) goto L_0x00a2
            java.lang.String r5 = "it"
            com.fossil.ee7.a(r4, r5)
            r2.b(r4)
        L_0x00a2:
            java.lang.String r4 = "binding"
            com.fossil.ee7.a(r3, r4)
            android.view.View r3 = r3.d()
            return r3
        L_0x00ac:
            java.lang.String r3 = "mViewModel"
            com.fossil.ee7.d(r3)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.h36.onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle):android.view.View");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        j36 j36 = this.g;
        if (j36 != null) {
            bundle.putSerializable("FILTER_TYPE_ARG", j36.b());
            j36 j362 = this.g;
            if (j362 != null) {
                bundle.putParcelableArrayList("COMPLICATIONS_ARG", j362.a());
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        Bundle arguments;
        ArrayList<fz5> parcelableArrayList;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "initialize()");
        Serializable serializable = null;
        if (!(!bundle.containsKey("COMPLICATIONS_ARG") || (arguments = getArguments()) == null || (parcelableArrayList = arguments.getParcelableArrayList("COMPLICATIONS_ARG")) == null)) {
            j36 j36 = this.g;
            if (j36 != null) {
                j36.a(parcelableArrayList);
                int dimensionPixelSize = PortfolioApp.g0.c().getResources().getDimensionPixelSize(2131165420);
                b();
                j36 j362 = this.g;
                if (j362 != null) {
                    j362.b(dimensionPixelSize);
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
        if (bundle.containsKey("FILTER_TYPE_ARG")) {
            j36 j363 = this.g;
            if (j363 != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    serializable = arguments2.getSerializable("FILTER_TYPE_ARG");
                }
                if (serializable != null) {
                    j363.a((FilterType) serializable);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.fossil.imagefilters.FilterType");
            }
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Drawable drawable, Integer num) {
        String c2;
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "showComplications()");
        c55 g1 = g1();
        if (g1 != null) {
            j36 j36 = this.g;
            if (j36 != null) {
                ArrayList<fz5> a2 = j36.a();
                if (a2 != null) {
                    for (T t : a2) {
                        if ((!ee7.a((Object) t.a(), (Object) "empty")) && (c2 = t.c()) != null) {
                            switch (c2.hashCode()) {
                                case -1383228885:
                                    if (c2.equals("bottom")) {
                                        CustomizeWidget customizeWidget = g1.x;
                                        ee7.a((Object) customizeWidget, "it.wcBottom");
                                        a(customizeWidget, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 115029:
                                    if (c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        CustomizeWidget customizeWidget2 = g1.A;
                                        ee7.a((Object) customizeWidget2, "it.wcTop");
                                        a(customizeWidget2, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 3317767:
                                    if (c2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                        CustomizeWidget customizeWidget3 = g1.z;
                                        ee7.a((Object) customizeWidget3, "it.wcStart");
                                        a(customizeWidget3, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 108511772:
                                    if (c2.equals("right")) {
                                        CustomizeWidget customizeWidget4 = g1.y;
                                        ee7.a((Object) customizeWidget4, "it.wcEnd");
                                        a(customizeWidget4, t.a(), t.d(), num, drawable);
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
                    }
                    return;
                }
                return;
            }
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str, String str2, Integer num, Drawable drawable) {
        FLogger.INSTANCE.getLocal().d("PreviewFragment", "updateComplicationButton()");
        customizeWidget.setVisibility(0);
        customizeWidget.b(str);
        if (str2 != null) {
            customizeWidget.setBottomContent(str2);
        }
        if (num != null) {
            customizeWidget.setDefaultColorRes(Integer.valueOf(num.intValue()));
        }
        if (drawable != null) {
            customizeWidget.setBackgroundDrawableCus(drawable);
        }
        customizeWidget.h();
    }
}
