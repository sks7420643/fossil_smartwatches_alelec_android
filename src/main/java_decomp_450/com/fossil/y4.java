package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c5;
import com.fossil.h5;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y4 {
    @DexIgnore
    public static int p; // = 1000;
    @DexIgnore
    public static z4 q;
    @DexIgnore
    public int a;
    @DexIgnore
    public HashMap<String, c5> b;
    @DexIgnore
    public a c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public v4[] f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean[] h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public /* final */ w4 l;
    @DexIgnore
    public c5[] m;
    @DexIgnore
    public int n;
    @DexIgnore
    public /* final */ a o;

    @DexIgnore
    public interface a {
        @DexIgnore
        c5 a(y4 y4Var, boolean[] zArr);

        @DexIgnore
        void a(c5 c5Var);

        @DexIgnore
        void a(a aVar);

        @DexIgnore
        void clear();

        @DexIgnore
        c5 getKey();
    }

    @DexIgnore
    public y4() {
        this.a = 0;
        this.b = null;
        this.d = 32;
        this.e = 32;
        this.f = null;
        this.g = false;
        this.h = new boolean[32];
        this.i = 1;
        this.j = 0;
        this.k = 32;
        this.m = new c5[p];
        this.n = 0;
        this.f = new v4[32];
        h();
        w4 w4Var = new w4();
        this.l = w4Var;
        this.c = new x4(w4Var);
        this.o = new v4(this.l);
    }

    @DexIgnore
    public static z4 j() {
        return q;
    }

    @DexIgnore
    public c5 a(Object obj) {
        c5 c5Var = null;
        if (obj == null) {
            return null;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        if (obj instanceof h5) {
            h5 h5Var = (h5) obj;
            c5Var = h5Var.e();
            if (c5Var == null) {
                h5Var.a(this.l);
                c5Var = h5Var.e();
            }
            int i2 = c5Var.b;
            if (i2 == -1 || i2 > this.a || this.l.c[i2] == null) {
                if (c5Var.b != -1) {
                    c5Var.a();
                }
                int i3 = this.a + 1;
                this.a = i3;
                this.i++;
                c5Var.b = i3;
                c5Var.g = c5.a.UNRESTRICTED;
                this.l.c[i3] = c5Var;
            }
        }
        return c5Var;
    }

    @DexIgnore
    public c5 b() {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.n++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        c5 a2 = a(c5.a.SLACK, (String) null);
        int i2 = this.a + 1;
        this.a = i2;
        this.i++;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public v4 c() {
        v4 a2 = this.l.a.a();
        if (a2 == null) {
            a2 = new v4(this.l);
        } else {
            a2.d();
        }
        c5.b();
        return a2;
    }

    @DexIgnore
    public c5 d() {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.m++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        c5 a2 = a(c5.a.SLACK, (String) null);
        int i2 = this.a + 1;
        this.a = i2;
        this.i++;
        a2.b = i2;
        this.l.c[i2] = a2;
        return a2;
    }

    @DexIgnore
    public w4 e() {
        return this.l;
    }

    @DexIgnore
    public final void f() {
        int i2 = this.d * 2;
        this.d = i2;
        this.f = (v4[]) Arrays.copyOf(this.f, i2);
        w4 w4Var = this.l;
        w4Var.c = (c5[]) Arrays.copyOf(w4Var.c, this.d);
        int i3 = this.d;
        this.h = new boolean[i3];
        this.e = i3;
        this.k = i3;
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.d++;
            z4Var.o = Math.max(z4Var.o, (long) i3);
            z4 z4Var2 = q;
            z4Var2.A = z4Var2.o;
        }
    }

    @DexIgnore
    public void g() throws Exception {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.e++;
        }
        if (this.g) {
            z4 z4Var2 = q;
            if (z4Var2 != null) {
                z4Var2.q++;
            }
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 >= this.j) {
                    z = true;
                    break;
                } else if (!this.f[i2].e) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                b(this.c);
                return;
            }
            z4 z4Var3 = q;
            if (z4Var3 != null) {
                z4Var3.p++;
            }
            a();
            return;
        }
        b(this.c);
    }

    @DexIgnore
    public final void h() {
        int i2 = 0;
        while (true) {
            v4[] v4VarArr = this.f;
            if (i2 < v4VarArr.length) {
                v4 v4Var = v4VarArr[i2];
                if (v4Var != null) {
                    this.l.a.a(v4Var);
                }
                this.f[i2] = null;
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public void i() {
        w4 w4Var;
        int i2 = 0;
        while (true) {
            w4Var = this.l;
            c5[] c5VarArr = w4Var.c;
            if (i2 >= c5VarArr.length) {
                break;
            }
            c5 c5Var = c5VarArr[i2];
            if (c5Var != null) {
                c5Var.a();
            }
            i2++;
        }
        w4Var.b.a(this.m, this.n);
        this.n = 0;
        Arrays.fill(this.l.c, (Object) null);
        HashMap<String, c5> hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.a = 0;
        this.c.clear();
        this.i = 1;
        for (int i3 = 0; i3 < this.j; i3++) {
            this.f[i3].c = false;
        }
        h();
        this.j = 0;
    }

    @DexIgnore
    public final void c(v4 v4Var) {
        v4[] v4VarArr = this.f;
        int i2 = this.j;
        if (v4VarArr[i2] != null) {
            this.l.a.a(v4VarArr[i2]);
        }
        v4[] v4VarArr2 = this.f;
        int i3 = this.j;
        v4VarArr2[i3] = v4Var;
        c5 c5Var = v4Var.a;
        c5Var.c = i3;
        this.j = i3 + 1;
        c5Var.c(v4Var);
    }

    @DexIgnore
    public final void b(v4 v4Var) {
        v4Var.a(this, 0);
    }

    @DexIgnore
    public final void d(v4 v4Var) {
        if (this.j > 0) {
            v4Var.d.a(v4Var, this.f);
            if (v4Var.d.a == 0) {
                v4Var.e = true;
            }
        }
    }

    @DexIgnore
    public int b(Object obj) {
        c5 e2 = ((h5) obj).e();
        if (e2 != null) {
            return (int) (e2.e + 0.5f);
        }
        return 0;
    }

    @DexIgnore
    public void c(c5 c5Var, c5 c5Var2, int i2, int i3) {
        v4 c2 = c();
        c5 d2 = d();
        d2.d = 0;
        c2.b(c5Var, c5Var2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public void b(a aVar) throws Exception {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.s++;
            z4Var.t = Math.max(z4Var.t, (long) this.i);
            z4 z4Var2 = q;
            z4Var2.u = Math.max(z4Var2.u, (long) this.j);
        }
        d((v4) aVar);
        a(aVar);
        a(aVar, false);
        a();
    }

    @DexIgnore
    public void a(v4 v4Var, int i2, int i3) {
        v4Var.a(a(i3, (String) null), i2);
    }

    @DexIgnore
    public c5 a(int i2, String str) {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.l++;
        }
        if (this.i + 1 >= this.e) {
            f();
        }
        c5 a2 = a(c5.a.ERROR, str);
        int i3 = this.a + 1;
        this.a = i3;
        this.i++;
        a2.b = i3;
        a2.d = i2;
        this.l.c[i3] = a2;
        this.c.a(a2);
        return a2;
    }

    @DexIgnore
    public void b(c5 c5Var, c5 c5Var2, int i2, int i3) {
        v4 c2 = c();
        c5 d2 = d();
        d2.d = 0;
        c2.a(c5Var, c5Var2, d2, i2);
        if (i3 != 6) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), i3);
        }
        a(c2);
    }

    @DexIgnore
    public final c5 a(c5.a aVar, String str) {
        c5 a2 = this.l.b.a();
        if (a2 == null) {
            a2 = new c5(aVar, str);
            a2.a(aVar, str);
        } else {
            a2.a();
            a2.a(aVar, str);
        }
        int i2 = this.n;
        int i3 = p;
        if (i2 >= i3) {
            int i4 = i3 * 2;
            p = i4;
            this.m = (c5[]) Arrays.copyOf(this.m, i4);
        }
        c5[] c5VarArr = this.m;
        int i5 = this.n;
        this.n = i5 + 1;
        c5VarArr[i5] = a2;
        return a2;
    }

    @DexIgnore
    public void b(c5 c5Var, c5 c5Var2, boolean z) {
        v4 c2 = c();
        c5 d2 = d();
        d2.d = 0;
        c2.b(c5Var, c5Var2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(v4 v4Var) {
        c5 c2;
        if (v4Var != null) {
            z4 z4Var = q;
            if (z4Var != null) {
                z4Var.f++;
                if (v4Var.e) {
                    z4Var.g++;
                }
            }
            boolean z = true;
            if (this.j + 1 >= this.k || this.i + 1 >= this.e) {
                f();
            }
            boolean z2 = false;
            if (!v4Var.e) {
                d(v4Var);
                if (!v4Var.c()) {
                    v4Var.a();
                    if (v4Var.a(this)) {
                        c5 b2 = b();
                        v4Var.a = b2;
                        c(v4Var);
                        this.o.a(v4Var);
                        a(this.o, true);
                        if (b2.c == -1) {
                            if (v4Var.a == b2 && (c2 = v4Var.c(b2)) != null) {
                                z4 z4Var2 = q;
                                if (z4Var2 != null) {
                                    z4Var2.j++;
                                }
                                v4Var.d(c2);
                            }
                            if (!v4Var.e) {
                                v4Var.a.c(v4Var);
                            }
                            this.j--;
                        }
                    } else {
                        z = false;
                    }
                    if (v4Var.b()) {
                        z2 = z;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!z2) {
                c(v4Var);
            }
        }
    }

    @DexIgnore
    public final int a(a aVar, boolean z) {
        z4 z4Var = q;
        if (z4Var != null) {
            z4Var.h++;
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.h[i2] = false;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            z4 z4Var2 = q;
            if (z4Var2 != null) {
                z4Var2.i++;
            }
            i3++;
            if (i3 >= this.i * 2) {
                return i3;
            }
            if (aVar.getKey() != null) {
                this.h[aVar.getKey().b] = true;
            }
            c5 a2 = aVar.a(this, this.h);
            if (a2 != null) {
                boolean[] zArr = this.h;
                int i4 = a2.b;
                if (zArr[i4]) {
                    return i3;
                }
                zArr[i4] = true;
            }
            if (a2 != null) {
                float f2 = Float.MAX_VALUE;
                int i5 = -1;
                for (int i6 = 0; i6 < this.j; i6++) {
                    v4 v4Var = this.f[i6];
                    if (v4Var.a.g != c5.a.UNRESTRICTED && !v4Var.e && v4Var.b(a2)) {
                        float b2 = v4Var.d.b(a2);
                        if (b2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            float f3 = (-v4Var.b) / b2;
                            if (f3 < f2) {
                                i5 = i6;
                                f2 = f3;
                            }
                        }
                    }
                }
                if (i5 > -1) {
                    v4 v4Var2 = this.f[i5];
                    v4Var2.a.c = -1;
                    z4 z4Var3 = q;
                    if (z4Var3 != null) {
                        z4Var3.j++;
                    }
                    v4Var2.d(a2);
                    c5 c5Var = v4Var2.a;
                    c5Var.c = i5;
                    c5Var.c(v4Var2);
                }
            }
            z2 = true;
        }
        return i3;
    }

    @DexIgnore
    public final int a(a aVar) throws Exception {
        float f2;
        boolean z;
        int i2 = 0;
        while (true) {
            int i3 = this.j;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (i2 >= i3) {
                z = false;
                break;
            }
            v4[] v4VarArr = this.f;
            if (v4VarArr[i2].a.g != c5.a.UNRESTRICTED && v4VarArr[i2].b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
                break;
            }
            i2++;
        }
        if (!z) {
            return 0;
        }
        boolean z2 = false;
        int i4 = 0;
        while (!z2) {
            z4 z4Var = q;
            if (z4Var != null) {
                z4Var.k++;
            }
            i4++;
            float f3 = Float.MAX_VALUE;
            int i5 = 0;
            int i6 = -1;
            int i7 = -1;
            int i8 = 0;
            while (i5 < this.j) {
                v4 v4Var = this.f[i5];
                if (v4Var.a.g != c5.a.UNRESTRICTED && !v4Var.e && v4Var.b < f2) {
                    int i9 = 1;
                    while (i9 < this.i) {
                        c5 c5Var = this.l.c[i9];
                        float b2 = v4Var.d.b(c5Var);
                        if (b2 > f2) {
                            for (int i10 = 0; i10 < 7; i10++) {
                                float f4 = c5Var.f[i10] / b2;
                                if ((f4 < f3 && i10 == i8) || i10 > i8) {
                                    i7 = i9;
                                    i8 = i10;
                                    f3 = f4;
                                    i6 = i5;
                                }
                            }
                        }
                        i9++;
                        f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                }
                i5++;
                f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            if (i6 != -1) {
                v4 v4Var2 = this.f[i6];
                v4Var2.a.c = -1;
                z4 z4Var2 = q;
                if (z4Var2 != null) {
                    z4Var2.j++;
                }
                v4Var2.d(this.l.c[i7]);
                c5 c5Var2 = v4Var2.a;
                c5Var2.c = i6;
                c5Var2.c(v4Var2);
            } else {
                z2 = true;
            }
            if (i4 > this.i / 2) {
                z2 = true;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return i4;
    }

    @DexIgnore
    public final void a() {
        for (int i2 = 0; i2 < this.j; i2++) {
            v4 v4Var = this.f[i2];
            v4Var.a.e = v4Var.b;
        }
    }

    @DexIgnore
    public void a(c5 c5Var, c5 c5Var2, boolean z) {
        v4 c2 = c();
        c5 d2 = d();
        d2.d = 0;
        c2.a(c5Var, c5Var2, d2, 0);
        if (z) {
            a(c2, (int) (c2.d.b(d2) * -1.0f), 1);
        }
        a(c2);
    }

    @DexIgnore
    public void a(c5 c5Var, c5 c5Var2, int i2, float f2, c5 c5Var3, c5 c5Var4, int i3, int i4) {
        v4 c2 = c();
        c2.a(c5Var, c5Var2, i2, f2, c5Var3, c5Var4, i3);
        if (i4 != 6) {
            c2.a(this, i4);
        }
        a(c2);
    }

    @DexIgnore
    public void a(c5 c5Var, c5 c5Var2, c5 c5Var3, c5 c5Var4, float f2, int i2) {
        v4 c2 = c();
        c2.a(c5Var, c5Var2, c5Var3, c5Var4, f2);
        if (i2 != 6) {
            c2.a(this, i2);
        }
        a(c2);
    }

    @DexIgnore
    public v4 a(c5 c5Var, c5 c5Var2, int i2, int i3) {
        v4 c2 = c();
        c2.a(c5Var, c5Var2, i2);
        if (i3 != 6) {
            c2.a(this, i3);
        }
        a(c2);
        return c2;
    }

    @DexIgnore
    public void a(c5 c5Var, int i2) {
        int i3 = c5Var.c;
        if (i3 != -1) {
            v4 v4Var = this.f[i3];
            if (v4Var.e) {
                v4Var.b = (float) i2;
            } else if (v4Var.d.a == 0) {
                v4Var.e = true;
                v4Var.b = (float) i2;
            } else {
                v4 c2 = c();
                c2.c(c5Var, i2);
                a(c2);
            }
        } else {
            v4 c3 = c();
            c3.b(c5Var, i2);
            a(c3);
        }
    }

    @DexIgnore
    public static v4 a(y4 y4Var, c5 c5Var, c5 c5Var2, c5 c5Var3, float f2, boolean z) {
        v4 c2 = y4Var.c();
        if (z) {
            y4Var.b(c2);
        }
        c2.a(c5Var, c5Var2, c5Var3, f2);
        return c2;
    }

    @DexIgnore
    public void a(i5 i5Var, i5 i5Var2, float f2, int i2) {
        c5 a2 = a(i5Var.a(h5.d.LEFT));
        c5 a3 = a(i5Var.a(h5.d.TOP));
        c5 a4 = a(i5Var.a(h5.d.RIGHT));
        c5 a5 = a(i5Var.a(h5.d.BOTTOM));
        c5 a6 = a(i5Var2.a(h5.d.LEFT));
        c5 a7 = a(i5Var2.a(h5.d.TOP));
        c5 a8 = a(i5Var2.a(h5.d.RIGHT));
        c5 a9 = a(i5Var2.a(h5.d.BOTTOM));
        v4 c2 = c();
        double d2 = (double) f2;
        double d3 = (double) i2;
        c2.b(a3, a5, a7, a9, (float) (Math.sin(d2) * d3));
        a(c2);
        v4 c3 = c();
        c3.b(a2, a4, a6, a8, (float) (Math.cos(d2) * d3));
        a(c3);
    }
}
