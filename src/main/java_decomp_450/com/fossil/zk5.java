package com.fossil;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk5 {
    @DexIgnore
    public static /* final */ zk5 a; // = new zk5();

    @DexIgnore
    public final Transition a() {
        Fade fade = new Fade();
        fade.excludeTarget(16908336, true);
        fade.excludeTarget(16908335, true);
        return fade;
    }

    @DexIgnore
    public final void a(View view) {
        ee7.b(view, "view");
        view.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) view.getHeight(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        view.startAnimation(translateAnimation);
    }

    @DexIgnore
    public final TransitionSet a(long j) {
        TransitionSet transitionSet = new TransitionSet();
        Slide slide = new Slide(80);
        slide.setInterpolator(AnimationUtils.loadInterpolator(PortfolioApp.g0.c(), 17563662));
        slide.setDuration(j);
        slide.excludeTarget(16908336, true);
        slide.excludeTarget(16908335, true);
        transitionSet.addTransition(slide);
        return transitionSet;
    }

    @DexIgnore
    public final Transition a(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "mApp");
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.setOrdering(0);
        transitionSet.setDuration(500L);
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        changeImageTransform.addTarget(portfolioApp.getString(2131887553));
        changeImageTransform.addTarget(portfolioApp.getString(2131887554));
        transitionSet.addTransition(changeImageTransform);
        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.addTarget(portfolioApp.getString(2131887553));
        changeBounds.addTarget(portfolioApp.getString(2131887554));
        changeBounds.addTarget(portfolioApp.getString(2131887559));
        changeBounds.addTarget(portfolioApp.getString(2131887552));
        changeBounds.addTarget(portfolioApp.getString(2131887549));
        changeBounds.addTarget(portfolioApp.getString(2131887551));
        changeBounds.addTarget(portfolioApp.getString(2131887550));
        changeBounds.addTarget(portfolioApp.getString(2131887563));
        changeBounds.addTarget(portfolioApp.getString(2131887562));
        changeBounds.addTarget(portfolioApp.getString(2131887561));
        changeBounds.addTarget(portfolioApp.getString(2131887557));
        changeBounds.addTarget(portfolioApp.getString(2131887556));
        changeBounds.addTarget(portfolioApp.getString(2131887558));
        changeBounds.addTarget(portfolioApp.getString(2131887560));
        transitionSet.addTransition(changeBounds);
        ChangeTransform changeTransform = new ChangeTransform();
        changeTransform.addTarget(portfolioApp.getString(2131887553));
        changeTransform.addTarget(portfolioApp.getString(2131887554));
        changeTransform.addTarget(portfolioApp.getString(2131887559));
        changeTransform.addTarget(portfolioApp.getString(2131887552));
        changeTransform.addTarget(portfolioApp.getString(2131887549));
        changeTransform.addTarget(portfolioApp.getString(2131887551));
        changeTransform.addTarget(portfolioApp.getString(2131887550));
        changeTransform.addTarget(portfolioApp.getString(2131887563));
        changeTransform.addTarget(portfolioApp.getString(2131887562));
        changeTransform.addTarget(portfolioApp.getString(2131887561));
        changeTransform.addTarget(portfolioApp.getString(2131887557));
        changeTransform.addTarget(portfolioApp.getString(2131887558));
        changeTransform.addTarget(portfolioApp.getString(2131887556));
        changeTransform.addTarget(portfolioApp.getString(2131887560));
        transitionSet.addTransition(changeTransform);
        Fade fade = new Fade();
        fade.addTarget(portfolioApp.getString(2131887560));
        fade.addTarget(portfolioApp.getString(2131887555));
        transitionSet.addTransition(fade);
        bl5 bl5 = new bl5();
        bl5.addTarget(portfolioApp.getString(2131887552));
        bl5.addTarget(portfolioApp.getString(2131887552));
        bl5.addTarget(portfolioApp.getString(2131887549));
        bl5.addTarget(portfolioApp.getString(2131887551));
        bl5.addTarget(portfolioApp.getString(2131887550));
        bl5.addTarget(portfolioApp.getString(2131887563));
        bl5.addTarget(portfolioApp.getString(2131887562));
        bl5.addTarget(portfolioApp.getString(2131887561));
        transitionSet.addTransition(bl5);
        al5 al5 = new al5();
        al5.addTarget(portfolioApp.getString(2131887559));
        transitionSet.addTransition(al5);
        return transitionSet;
    }
}
