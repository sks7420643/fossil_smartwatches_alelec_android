package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.fossil.n63;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa3 extends d83 {
    @DexIgnore
    public /* final */ /* synthetic */ n63.n a;

    @DexIgnore
    public wa3(n63 n63, n63.n nVar) {
        this.a = nVar;
    }

    @DexIgnore
    @Override // com.fossil.c83
    public final void c(ab2 ab2) throws RemoteException {
        this.a.onSnapshotReady((Bitmap) cb2.g(ab2));
    }

    @DexIgnore
    @Override // com.fossil.c83
    public final void onSnapshotReady(Bitmap bitmap) throws RemoteException {
        this.a.onSnapshotReady(bitmap);
    }
}
