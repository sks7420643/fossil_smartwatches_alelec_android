package com.fossil;

import com.fossil.ou1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu1 extends ou1 {
    @DexIgnore
    public /* final */ pu1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ct1<?> c;
    @DexIgnore
    public /* final */ et1<?, byte[]> d;
    @DexIgnore
    public /* final */ bt1 e;

    @DexIgnore
    @Override // com.fossil.ou1
    public bt1 a() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ou1
    public ct1<?> b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ou1
    public et1<?, byte[]> d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ou1
    public pu1 e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ou1)) {
            return false;
        }
        ou1 ou1 = (ou1) obj;
        if (!this.a.equals(ou1.e()) || !this.b.equals(ou1.f()) || !this.c.equals(ou1.b()) || !this.d.equals(ou1.d()) || !this.e.equals(ou1.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ou1
    public String f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SendRequest{transportContext=" + this.a + ", transportName=" + this.b + ", event=" + this.c + ", transformer=" + this.d + ", encoding=" + this.e + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ou1.a {
        @DexIgnore
        public pu1 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public ct1<?> c;
        @DexIgnore
        public et1<?, byte[]> d;
        @DexIgnore
        public bt1 e;

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1.a a(pu1 pu1) {
            if (pu1 != null) {
                this.a = pu1;
                return this;
            }
            throw new NullPointerException("Null transportContext");
        }

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1.a a(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1.a a(ct1<?> ct1) {
            if (ct1 != null) {
                this.c = ct1;
                return this;
            }
            throw new NullPointerException("Null event");
        }

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1.a a(et1<?, byte[]> et1) {
            if (et1 != null) {
                this.d = et1;
                return this;
            }
            throw new NullPointerException("Null transformer");
        }

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1.a a(bt1 bt1) {
            if (bt1 != null) {
                this.e = bt1;
                return this;
            }
            throw new NullPointerException("Null encoding");
        }

        @DexIgnore
        @Override // com.fossil.ou1.a
        public ou1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " transportContext";
            }
            if (this.b == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " event";
            }
            if (this.d == null) {
                str = str + " transformer";
            }
            if (this.e == null) {
                str = str + " encoding";
            }
            if (str.isEmpty()) {
                return new eu1(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public eu1(pu1 pu1, String str, ct1<?> ct1, et1<?, byte[]> et1, bt1 bt1) {
        this.a = pu1;
        this.b = str;
        this.c = ct1;
        this.d = et1;
        this.e = bt1;
    }
}
