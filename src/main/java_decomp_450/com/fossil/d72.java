package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d72 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<d72> CREATOR; // = new l82();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    @Deprecated
    public /* final */ Scope[] d;

    @DexIgnore
    public d72(int i, int i2, int i3, Scope[] scopeArr) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = scopeArr;
    }

    @DexIgnore
    public int e() {
        return this.b;
    }

    @DexIgnore
    public int g() {
        return this.c;
    }

    @DexIgnore
    @Deprecated
    public Scope[] v() {
        return this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, e());
        k72.a(parcel, 3, g());
        k72.a(parcel, 4, (Parcelable[]) v(), i, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public d72(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, null);
    }
}
