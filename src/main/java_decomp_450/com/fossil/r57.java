package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r57 {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean a; // = (!r57.class.desiredAssertionStatus());

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        t57 t57 = new t57(i3, new byte[((i2 * 3) / 4)]);
        if (t57.a(bArr, i, i2, true)) {
            int i4 = ((s57) t57).b;
            byte[] bArr2 = ((s57) t57).a;
            if (i4 == bArr2.length) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
        throw new IllegalArgumentException("bad base-64");
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        u57 u57 = new u57(i3, null);
        int i4 = (i2 / 3) * 4;
        int i5 = 2;
        if (!u57.f) {
            int i6 = i2 % 3;
            if (i6 == 1) {
                i4 += 2;
            } else if (i6 == 2) {
                i4 += 3;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (u57.g && i2 > 0) {
            int i7 = ((i2 - 1) / 57) + 1;
            if (!u57.h) {
                i5 = 1;
            }
            i4 += i7 * i5;
        }
        ((s57) u57).a = new byte[i4];
        u57.a(bArr, i, i2, true);
        if (a || ((s57) u57).b == i4) {
            return ((s57) u57).a;
        }
        throw new AssertionError();
    }
}
