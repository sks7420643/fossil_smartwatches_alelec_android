package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uw3<K, V> implements Map.Entry<K, V> {
    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!gw3.a(getKey(), entry.getKey()) || !gw3.a(getValue(), entry.getValue())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract K getKey();

    @DexIgnore
    @Override // java.util.Map.Entry
    public abstract V getValue();

    @DexIgnore
    public int hashCode() {
        int i;
        K key = getKey();
        V value = getValue();
        int i2 = 0;
        if (key == null) {
            i = 0;
        } else {
            i = key.hashCode();
        }
        if (value != null) {
            i2 = value.hashCode();
        }
        return i ^ i2;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public V setValue(V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return ((Object) getKey()) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) getValue());
    }
}
