package com.fossil;

import android.os.DeadObjectException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm2 implements fm2<nl2> {
    @DexIgnore
    public /* final */ /* synthetic */ km2 a;

    @DexIgnore
    public lm2(km2 km2) {
        this.a = km2;
    }

    @DexIgnore
    @Override // com.fossil.fm2
    public final void a() {
        this.a.s();
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.fm2
    public final /* synthetic */ nl2 getService() throws DeadObjectException {
        return (nl2) this.a.A();
    }
}
