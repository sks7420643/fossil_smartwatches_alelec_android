package com.fossil;

import com.fossil.n63;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bb3 extends n73 {
    @DexIgnore
    public /* final */ /* synthetic */ n63.i a;

    @DexIgnore
    public bb3(n63 n63, n63.i iVar) {
        this.a = iVar;
    }

    @DexIgnore
    @Override // com.fossil.m73
    public final void onMapLongClick(LatLng latLng) {
        this.a.onMapLongClick(latLng);
    }
}
