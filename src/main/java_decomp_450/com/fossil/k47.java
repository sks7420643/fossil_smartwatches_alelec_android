package com.fossil;

import android.content.Context;
import android.content.IntentFilter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k47 {
    @DexIgnore
    public static k47 i;
    @DexIgnore
    public List<String> a; // = null;
    @DexIgnore
    public volatile int b; // = 2;
    @DexIgnore
    public volatile String c; // = "";
    @DexIgnore
    public volatile HttpHost d; // = null;
    @DexIgnore
    public p57 e; // = null;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public Context g; // = null;
    @DexIgnore
    public k57 h; // = null;

    @DexIgnore
    public k47(Context context) {
        this.g = context.getApplicationContext();
        this.e = new p57();
        h67.a(context);
        this.h = v57.b();
        l();
        i();
        g();
    }

    @DexIgnore
    public static k47 a(Context context) {
        if (i == null) {
            synchronized (k47.class) {
                if (i == null) {
                    i = new k47(context);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public HttpHost a() {
        return this.d;
    }

    @DexIgnore
    public void a(String str) {
        if (w37.q()) {
            this.h.e("updateIpList " + str);
        }
        try {
            if (v57.c(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() > 0) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String string = jSONObject.getString(keys.next());
                        if (v57.c(string)) {
                            String[] split = string.split(";");
                            for (String str2 : split) {
                                if (v57.c(str2)) {
                                    String[] split2 = str2.split(":");
                                    if (split2.length > 1) {
                                        String str3 = split2[0];
                                        if (b(str3) && !this.a.contains(str3)) {
                                            if (w37.q()) {
                                                this.h.e("add new ip:" + str3);
                                            }
                                            this.a.add(str3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
        }
        this.f = new Random().nextInt(this.a.size());
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public final boolean b(String str) {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(str).matches();
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public void d() {
        this.f = (this.f + 1) % this.a.size();
    }

    @DexIgnore
    public boolean e() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean f() {
        return this.b != 0;
    }

    @DexIgnore
    public void g() {
        if (a67.f(this.g)) {
            if (w37.v) {
                k();
            }
            this.c = v57.o(this.g);
            if (w37.q()) {
                k57 k57 = this.h;
                k57.e("NETWORK name:" + this.c);
            }
            if (v57.c(this.c)) {
                this.b = "WIFI".equalsIgnoreCase(this.c) ? 1 : 2;
                this.d = v57.d(this.g);
            }
            if (z37.a()) {
                z37.d(this.g);
                return;
            }
            return;
        }
        if (w37.q()) {
            this.h.e("NETWORK TYPE: network is close.");
        }
        l();
    }

    @DexIgnore
    public void h() {
        this.g.getApplicationContext().registerReceiver(new d57(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void i() {
        ArrayList arrayList = new ArrayList(10);
        this.a = arrayList;
        arrayList.add("117.135.169.101");
        this.a.add("140.207.54.125");
        this.a.add("180.153.8.53");
        this.a.add("120.198.203.175");
        this.a.add("14.17.43.18");
        this.a.add("163.177.71.186");
        this.a.add("111.30.131.31");
        this.a.add("123.126.121.167");
        this.a.add("123.151.152.111");
        this.a.add("113.142.45.79");
        this.a.add("123.138.162.90");
        this.a.add("103.7.30.94");
    }

    @DexIgnore
    public final String j() {
        try {
            return !b("pingma.qq.com") ? InetAddress.getByName("pingma.qq.com").getHostAddress() : "";
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
            return "";
        }
    }

    @DexIgnore
    public final void k() {
        String j = j();
        if (w37.q()) {
            k57 k57 = this.h;
            k57.e("remoteIp ip is " + j);
        }
        if (v57.c(j)) {
            if (!this.a.contains(j)) {
                String str = this.a.get(this.f);
                if (w37.q()) {
                    k57 k572 = this.h;
                    k572.g(j + " not in ip list, change to:" + str);
                }
                j = str;
            }
            w37.c("http://" + j + ":80/mstat/report");
        }
    }

    @DexIgnore
    public final void l() {
        this.b = 0;
        this.d = null;
        this.c = null;
    }
}
