package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul3 {
    @DexIgnore
    public /* final */ n92 a;
    @DexIgnore
    public long b;

    @DexIgnore
    public ul3(n92 n92) {
        a72.a(n92);
        this.a = n92;
    }

    @DexIgnore
    public final void a() {
        this.b = this.a.c();
    }

    @DexIgnore
    public final void b() {
        this.b = 0;
    }

    @DexIgnore
    public final boolean a(long j) {
        if (this.b != 0 && this.a.c() - this.b < 3600000) {
            return false;
        }
        return true;
    }
}
