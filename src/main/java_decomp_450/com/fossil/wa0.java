package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa0 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ j90 a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ i90 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wa0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                j90 valueOf = j90.valueOf(readString);
                float readFloat = parcel.readFloat();
                float readFloat2 = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    return new wa0(valueOf, readFloat, readFloat2, i90.valueOf(readString2));
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wa0[] newArray(int i) {
            return new wa0[i];
        }
    }

    @DexIgnore
    public wa0(j90 j90, float f, float f2, i90 i90) {
        this.a = j90;
        this.b = yz0.a(f, 2);
        this.c = yz0.a(f2, 2);
        this.d = i90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.Z1, yz0.a(this.a)), r51.X1, Float.valueOf(this.b)), r51.Y1, Float.valueOf(this.c)), r51.s, yz0.a(this.d));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put("day", this.a.a()).put("high", Float.valueOf(this.b)).put("low", Float.valueOf(this.c)).put("cond_id", this.d.a());
        ee7.a((Object) put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(wa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            wa0 wa0 = (wa0) obj;
            return this.a == wa0.a && this.b == wa0.b && this.c == wa0.c && this.d == wa0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherDayForecast");
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.b;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.c;
    }

    @DexIgnore
    public final i90 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public final j90 getWeekday() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
