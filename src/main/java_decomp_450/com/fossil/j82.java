package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j82 implements Parcelable.Creator<b72> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b72 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Account account = null;
        GoogleSignInAccount googleSignInAccount = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                account = (Account) j72.a(parcel, a, Account.CREATOR);
            } else if (a2 == 3) {
                i2 = j72.q(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                googleSignInAccount = (GoogleSignInAccount) j72.a(parcel, a, GoogleSignInAccount.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new b72(i, account, i2, googleSignInAccount);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b72[] newArray(int i) {
        return new b72[i];
    }
}
