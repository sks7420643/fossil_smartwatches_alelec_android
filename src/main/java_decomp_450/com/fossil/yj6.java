package com.fossil;

import com.fossil.fl4;
import com.fossil.on5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj6 extends tj6 {
    @DexIgnore
    public /* final */ String e; // = "ProfileOptInPresenter";
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ uj6 g;
    @DexIgnore
    public /* final */ on5 h;
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ yj6 a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public a(yj6 yj6, boolean z) {
            this.a = yj6;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.i().N0();
            qd5.f.c().a(this.b);
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.i().K(!this.b);
            this.a.i().N0();
            uj6 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ yj6 a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public b(yj6 yj6, boolean z) {
            this.a = yj6;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.i().N0();
            qd5.f.c().a(this.b);
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.i().N(!this.b);
            this.a.i().N0();
            uj6 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.h();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(yj6 yj6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = yj6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yj6 yj6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                yj6 yj62 = this.this$0;
                ti7 a3 = yj62.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = yj62;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                yj6 = yj62;
            } else if (i == 1) {
                yj6 = (yj6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            yj6.f = (MFUser) obj;
            MFUser b = this.this$0.f;
            if (b != null) {
                this.this$0.i().K(b.getDiagnosticEnabled());
                this.this$0.i().N(b.getEmailOptIn());
            }
            return i97.a;
        }
    }

    @DexIgnore
    public yj6(uj6 uj6, on5 on5, UserRepository userRepository) {
        ee7.b(uj6, "mView");
        ee7.b(on5, "mUpdateUser");
        ee7.b(userRepository, "mUserRepository");
        this.g = uj6;
        this.h = on5;
        this.i = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter starts: Get user information");
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter stop");
    }

    @DexIgnore
    public final UserRepository h() {
        return this.i;
    }

    @DexIgnore
    public final uj6 i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // com.fossil.tj6
    public void b(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setSubcribeEmailData() called with: checked = " + z);
            mFUser.setEmailOptIn(z);
            this.g.H();
            if (this.h.a(new on5.b(mFUser), new b(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    @Override // com.fossil.tj6
    public void a(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setAnonymouslySendUsageData() called with: checked = " + z);
            mFUser.setDiagnosticEnabled(z);
            this.g.H();
            if (this.h.a(new on5.b(mFUser), new a(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }
}
