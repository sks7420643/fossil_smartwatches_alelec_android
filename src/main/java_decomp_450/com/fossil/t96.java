package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t96 implements Factory<s96> {
    @DexIgnore
    public static s96 a(q96 q96, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        return new s96(q96, summariesRepository, fitnessDataRepository, userRepository, pj4);
    }
}
