package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z42 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ i02 b;

    @DexIgnore
    public z42(i02 i02, int i) {
        a72.a(i02);
        this.b = i02;
        this.a = i;
    }

    @DexIgnore
    public final i02 a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }
}
