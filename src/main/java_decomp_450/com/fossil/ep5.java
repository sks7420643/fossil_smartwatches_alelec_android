package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep5 extends rf<GoalTrackingData, a> {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ ep5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ep5$a$a")
        /* renamed from: com.fossil.ep5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0060a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0060a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2;
                if (this.a.d.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1) {
                    a aVar = this.a;
                    GoalTrackingData a3 = ep5.a(aVar.d, aVar.getAdapterPosition());
                    if (a3 != null && (a2 = this.a.d.d) != null) {
                        ee7.a((Object) a3, "it1");
                        a2.a(a3);
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ep5 ep5, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = ep5;
            this.a = (FlexibleTextView) view.findViewById(2131362514);
            this.b = (FlexibleTextView) view.findViewById(2131362464);
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362375);
            this.c = flexibleTextView;
            flexibleTextView.setOnClickListener(new View$OnClickListenerC0060a(this));
        }

        @DexIgnore
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            ee7.b(goalTrackingData, "item");
            if (this.d.c == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + re5.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = re5.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                ee7.a((Object) str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            FlexibleTextView flexibleTextView = this.a;
            ee7.a((Object) flexibleTextView, "mTvTime");
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131887517);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{zd5.b(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView.setText(format);
            FlexibleTextView flexibleTextView2 = this.b;
            ee7.a((Object) flexibleTextView2, "mTvNoTime");
            flexibleTextView2.setVisibility(8);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ep5(b bVar, kl4 kl4) {
        super(kl4);
        ee7.b(kl4, "goalTrackingDataDiff");
        this.d = bVar;
        TimeZone timeZone = TimeZone.getDefault();
        ee7.a((Object) timeZone, "TimeZone.getDefault()");
        this.c = zd5.a(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData a(ep5 ep5, int i) {
        return (GoalTrackingData) ep5.getItem(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558706, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) getItem(i);
        if (goalTrackingData != null) {
            aVar.a(goalTrackingData);
        }
    }
}
