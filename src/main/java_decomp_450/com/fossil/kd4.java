package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd4 {
    @DexIgnore
    public static WeakReference<kd4> d;
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public id4 b;
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public kd4(SharedPreferences sharedPreferences, Executor executor) {
        this.c = executor;
        this.a = sharedPreferences;
    }

    @DexIgnore
    public static synchronized kd4 a(Context context, Executor executor) {
        kd4 kd4;
        synchronized (kd4.class) {
            kd4 = null;
            if (d != null) {
                kd4 = d.get();
            }
            if (kd4 == null) {
                kd4 = new kd4(context.getSharedPreferences("com.google.android.gms.appid", 0), executor);
                kd4.b();
                d = new WeakReference<>(kd4);
            }
        }
        return kd4;
    }

    @DexIgnore
    public final synchronized void b() {
        this.b = id4.a(this.a, "topic_operation_queue", ",", this.c);
    }

    @DexIgnore
    public final synchronized jd4 a() {
        return jd4.a(this.b.c());
    }

    @DexIgnore
    public final synchronized boolean a(jd4 jd4) {
        return this.b.a(jd4.c());
    }
}
