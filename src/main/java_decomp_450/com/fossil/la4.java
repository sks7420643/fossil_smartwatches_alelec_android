package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la4 implements ka4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public la4(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // com.fossil.ka4
    public final String a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ka4
    public final String getId() {
        return this.a;
    }
}
