package com.fossil;

import com.fossil.mc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc4 extends mc4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ mc4.b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends mc4.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public mc4.b c;

        @DexIgnore
        @Override // com.fossil.mc4.a
        public mc4.a a(String str) {
            this.a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.mc4.a
        public mc4.a a(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.mc4.a
        public mc4.a a(mc4.b bVar) {
            this.c = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.mc4.a
        public mc4 a() {
            String str = "";
            if (this.b == null) {
                str = str + " tokenExpirationTimestamp";
            }
            if (str.isEmpty()) {
                return new jc4(this.a, this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.mc4
    public mc4.b a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.mc4
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.mc4
    public long c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof mc4)) {
            return false;
        }
        mc4 mc4 = (mc4) obj;
        String str = this.a;
        if (str != null ? str.equals(mc4.b()) : mc4.b() == null) {
            if (this.b == mc4.c()) {
                mc4.b bVar = this.c;
                if (bVar == null) {
                    if (mc4.a() == null) {
                        return true;
                    }
                } else if (bVar.equals(mc4.a())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = str == null ? 0 : str.hashCode();
        long j = this.b;
        int i2 = (((hashCode ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003;
        mc4.b bVar = this.c;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return i2 ^ i;
    }

    @DexIgnore
    public String toString() {
        return "TokenResult{token=" + this.a + ", tokenExpirationTimestamp=" + this.b + ", responseCode=" + this.c + "}";
    }

    @DexIgnore
    public jc4(String str, long j, mc4.b bVar) {
        this.a = str;
        this.b = j;
        this.c = bVar;
    }
}
