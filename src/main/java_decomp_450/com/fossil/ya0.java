package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ya0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ g90 c;
    @DexIgnore
    public /* final */ va0 d;
    @DexIgnore
    public /* final */ xa0[] e;
    @DexIgnore
    public /* final */ wa0[] f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ya0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ya0 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    g90 valueOf = g90.valueOf(readString2);
                    Parcelable readParcelable = parcel.readParcelable(va0.class.getClassLoader());
                    if (readParcelable != null) {
                        va0 va0 = (va0) readParcelable;
                        Object[] createTypedArray = parcel.createTypedArray(xa0.CREATOR);
                        if (createTypedArray != null) {
                            ee7.a((Object) createTypedArray, "parcel.createTypedArray(\u2026erHourForecast.CREATOR)!!");
                            xa0[] xa0Arr = (xa0[]) createTypedArray;
                            Object[] createTypedArray2 = parcel.createTypedArray(wa0.CREATOR);
                            if (createTypedArray2 != null) {
                                ee7.a((Object) createTypedArray2, "parcel.createTypedArray(\u2026herDayForecast.CREATOR)!!");
                                return new ya0(readLong, readString, valueOf, va0, xa0Arr, (wa0[]) createTypedArray2);
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ya0[] newArray(int i) {
            return new ya0[i];
        }
    }

    @DexIgnore
    public ya0(long j, String str, g90 g90, va0 va0, xa0[] xa0Arr, wa0[] wa0Arr) throws IllegalArgumentException {
        this.a = j;
        this.b = str;
        this.c = g90;
        this.d = va0;
        this.e = xa0Arr;
        this.f = wa0Arr;
        if (xa0Arr.length < 3) {
            throw new IllegalArgumentException("hourForecast must have at least 3 elements.");
        } else if (wa0Arr.length < 3) {
            throw new IllegalArgumentException("dayForecast must have at least 3 elements.");
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.K2, Long.valueOf(this.a)), r51.J, this.b), r51.t, yz0.a(this.c)), r51.a2, this.d.a()), r51.b2, yz0.a(this.e)), r51.c2, yz0.a(this.f));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            jSONArray.put(this.e[i].b());
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = 0; i2 < 3; i2++) {
            jSONArray2.put(this.f[i2].b());
        }
        JSONObject put = new JSONObject().put("alive", this.a).put("city", this.b).put(Constants.PROFILE_KEY_UNIT, yz0.a(this.c));
        ee7.a((Object) put, "JSONObject().put(UIScrip\u2026ratureUnit.lowerCaseName)");
        JSONObject put2 = yz0.a(put, this.d.b()).put("forecast_day", jSONArray).put("forecast_week", jSONArray2);
        ee7.a((Object) put2, "JSONObject().put(UIScrip\u2026ST_WEEK, dayForecastJSON)");
        return put2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ya0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ya0 ya0 = (ya0) obj;
            return this.a == ya0.a && this.c == ya0.c && !(ee7.a(this.b, ya0.b) ^ true) && !(ee7.a(this.d, ya0.d) ^ true) && Arrays.equals(this.e, ya0.e) && Arrays.equals(this.f, ya0.f);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherInfo");
    }

    @DexIgnore
    public final va0 getCurrentWeatherInfo() {
        return this.d;
    }

    @DexIgnore
    public final wa0[] getDayForecast() {
        return this.f;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public final xa0[] getHourForecast() {
        return this.e;
    }

    @DexIgnore
    public final String getLocation() {
        return this.b;
    }

    @DexIgnore
    public final g90 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.b.hashCode();
        return ((((this.d.hashCode() + ((hashCode2 + ((hashCode + (Long.valueOf(this.a).hashCode() * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.e)) * 31) + Arrays.hashCode(this.f);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.e, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.f, i);
        }
    }
}
