package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b86 extends y76 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public List<ActivitySummary> i; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<qx6<List<ActivitySummary>>> j;
    @DexIgnore
    public TreeMap<Long, Float> k;
    @DexIgnore
    public /* final */ z76 l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ SummariesRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$loadData$1", f = "ActiveTimeOverviewMonthPresenter.kt", l = {102}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ b86 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$loadData$1$currentUser$1", f = "ActiveTimeOverviewMonthPresenter.kt", l = {102}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository i2 = this.this$0.this$0.m;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = i2.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(b86 b86, fb7 fb7) {
            super(2, fb7);
            this.this$0 = b86;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.h = zd5.d(mFUser.getCreatedAt());
                z76 j = this.this$0.l;
                Date f = this.this$0.g;
                if (f != null) {
                    Date c = this.this$0.h;
                    if (c == null) {
                        c = new Date();
                    }
                    j.a(f, c);
                    this.this$0.f.a(this.this$0.g);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ b86 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$mActivitySummaries$1$1", f = "ActiveTimeOverviewMonthPresenter.kt", l = {48, 48}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                Date date;
                Date date2;
                vd vdVar2;
                Date date3;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    Date c = this.this$0.a.h;
                    if (c == null) {
                        c = new Date();
                    }
                    long time = c.getTime();
                    Date date4 = this.$it;
                    ee7.a((Object) date4, "it");
                    if (!zd5.a(time, date4.getTime())) {
                        Calendar r = zd5.r(this.$it);
                        ee7.a((Object) r, "DateHelper.getStartOfMonth(it)");
                        c = r.getTime();
                        ee7.a((Object) c, "DateHelper.getStartOfMonth(it).time");
                    }
                    date = c;
                    Boolean v = zd5.v(this.$it);
                    ee7.a((Object) v, "DateHelper.isThisMonth(it)");
                    if (v.booleanValue()) {
                        date3 = new Date();
                    } else {
                        Calendar m = zd5.m(this.$it);
                        ee7.a((Object) m, "DateHelper.getEndOfMonth(it)");
                        date3 = m.getTime();
                    }
                    ee7.a((Object) date3, GoalPhase.COLUMN_END_DATE);
                    boolean z = date3.getTime() >= date.getTime();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ActiveTimeOverviewMonthPresenter", "mActivitySummaries onDateChanged " + this.$it + " startDate " + date + " endDate " + date3 + " isValid " + z);
                    if (z) {
                        SummariesRepository h = this.this$0.a.n;
                        this.L$0 = vdVar2;
                        this.L$1 = date;
                        this.L$2 = date3;
                        this.L$3 = vdVar2;
                        this.label = 1;
                        Object summaries = h.getSummaries(date, date3, true, this);
                        if (summaries == a) {
                            return a;
                        }
                        vdVar = vdVar2;
                        date2 = date3;
                        obj = summaries;
                    }
                    return i97.a;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$3;
                    date2 = (Date) this.L$2;
                    date = (Date) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    Date date5 = (Date) this.L$2;
                    Date date6 = (Date) this.L$1;
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.L$1 = date;
                this.L$2 = date2;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(b86 b86) {
            this.a = b86;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySummary>>> apply(Date date) {
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ b86 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1", f = "ActiveTimeOverviewMonthPresenter.kt", l = {73}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.b86$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter$start$1$1$1", f = "ActiveTimeOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.b86$d$a$a  reason: collision with other inner class name */
            public static final class C0015a extends zb7 implements kd7<yi7, fb7<? super TreeMap<Long, Float>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0015a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0015a aVar = new C0015a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super TreeMap<Long, Float>> fb7) {
                    return ((C0015a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        b86 b86 = this.this$0.this$0.a;
                        Object a = b86.f.a();
                        if (a != null) {
                            ee7.a(a, "mDateLiveData.value!!");
                            return b86.a((Date) a, this.this$0.$data);
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                b86 b86;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    b86 b862 = this.this$0.a;
                    ti7 b = b862.c();
                    C0015a aVar = new C0015a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = b862;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                    b86 = b862;
                } else if (i == 1) {
                    b86 = (b86) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                b86.k = (TreeMap) obj;
                z76 j = this.this$0.a.l;
                TreeMap<Long, Float> d = this.this$0.a.k;
                if (d == null) {
                    d = new TreeMap<>();
                }
                j.a(d);
                return i97.a;
            }
        }

        @DexIgnore
        public d(b86 b86) {
            this.a = b86;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<ActivitySummary>> qx6) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(qx6 != null ? qx6.d() : null);
            sb.append(" -- data.size=");
            sb.append((qx6 == null || (list = (List) qx6.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("ActiveTimeOverviewMonthPresenter", sb.toString());
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                List list2 = qx6 != null ? (List) qx6.c() : null;
                if (list2 != null && (!ee7.a(this.a.i, list2))) {
                    this.a.i = list2;
                    ik7 unused = xh7.b(this.a.e(), null, null, new a(this, list2, null), 3, null);
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public b86(z76 z76, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        ee7.b(z76, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(portfolioApp, "mApp");
        this.l = z76;
        this.m = userRepository;
        this.n = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        LiveData<qx6<List<ActivitySummary>>> b2 = ge.b(this.f, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026         }\n       }\n    }");
        this.j = b2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        i();
        LiveData<qx6<List<ActivitySummary>>> liveData = this.j;
        z76 z76 = this.l;
        if (z76 != null) {
            liveData.a((a86) z76, new d(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthPresenter", "stop");
        try {
            LiveData<qx6<List<ActivitySummary>>> liveData = this.j;
            z76 z76 = this.l;
            if (z76 != null) {
                liveData.a((a86) z76);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.y76
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        Date date = this.g;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.g = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewMonthPresenter", "loadData - mDate=" + this.g);
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("ActiveTimeOverviewMonthPresenter", "loadData - mDate=" + this.g);
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    @Override // com.fossil.y76
    public void a(Date date) {
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthPresenter", "setDate date=" + date);
        if (this.f.a() == null || !zd5.d(this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActiveTimeOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary activitySummary : list) {
                instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (activitySummary.getActiveTimeGoal() > 0) {
                    ee7.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) activitySummary.getActiveTime()) / ((float) activitySummary.getActiveTimeGoal())));
                } else {
                    ee7.a((Object) instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }
}
