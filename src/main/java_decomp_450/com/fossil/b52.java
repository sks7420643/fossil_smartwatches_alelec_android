package com.fossil;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b52 extends z32 {
    @DexIgnore
    public /* final */ /* synthetic */ Dialog a;
    @DexIgnore
    public /* final */ /* synthetic */ c52 b;

    @DexIgnore
    public b52(c52 c52, Dialog dialog) {
        this.b = c52;
        this.a = dialog;
    }

    @DexIgnore
    @Override // com.fossil.z32
    public final void a() {
        this.b.b.g();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
