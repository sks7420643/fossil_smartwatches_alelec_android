package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa0 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ i90 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public xa0 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            float readFloat = parcel.readFloat();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                return new xa0(readInt, readFloat, i90.valueOf(readString));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public xa0[] newArray(int i) {
            return new xa0[i];
        }
    }

    @DexIgnore
    public xa0(int i, float f, i90 i90) throws IllegalArgumentException {
        this.a = i;
        this.b = yz0.a(f, 2);
        this.c = i90;
        int i2 = this.a;
        if (!(i2 >= 0 && 23 >= i2)) {
            throw new IllegalArgumentException(yh0.a(yh0.b("hourIn24Format("), this.a, ") is out of range ", "[0, 23]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.z, Integer.valueOf(this.a)), r51.R1, Float.valueOf(this.b)), r51.s, yz0.a(this.c));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put(AppFilter.COLUMN_HOUR, this.a).put("temp", Float.valueOf(this.b)).put("cond_id", this.c.a());
        ee7.a((Object) put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(xa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            xa0 xa0 = (xa0) obj;
            return this.a == xa0.a && this.b == xa0.b && this.c == xa0.c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherHourForecast");
    }

    @DexIgnore
    public final int getHourIn24Format() {
        return this.a;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.b;
    }

    @DexIgnore
    public final i90 getWeatherCondition() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        return this.c.hashCode() + ((hashCode + (this.a * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
