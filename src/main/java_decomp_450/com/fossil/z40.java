package com.fossil;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.f50;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z40<Z> extends d50<ImageView, Z> implements f50.a {
    @DexIgnore
    public Animatable g;

    @DexIgnore
    public z40(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    @Override // com.fossil.c50, com.fossil.v40
    public void a(Drawable drawable) {
        super.a(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    @Override // com.fossil.c50, com.fossil.d50, com.fossil.v40
    public void b(Drawable drawable) {
        super.b(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    @Override // com.fossil.c50, com.fossil.d50, com.fossil.v40
    public void c(Drawable drawable) {
        super.c(drawable);
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.stop();
        }
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public abstract void c(Z z);

    @DexIgnore
    public void d(Drawable drawable) {
        ((ImageView) ((d50) this).a).setImageDrawable(drawable);
    }

    @DexIgnore
    @Override // com.fossil.q30, com.fossil.v40
    public void onStart() {
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.start();
        }
    }

    @DexIgnore
    @Override // com.fossil.q30, com.fossil.v40
    public void onStop() {
        Animatable animatable = this.g;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    public final void d(Z z) {
        c((Object) z);
        b((Object) z);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void a(Z z, f50<? super Z> f50) {
        if (f50 == null || !f50.a(z, this)) {
            d((Object) z);
        } else {
            b((Object) z);
        }
    }

    @DexIgnore
    public final void b(Z z) {
        if (z instanceof Animatable) {
            Z z2 = z;
            this.g = z2;
            z2.start();
            return;
        }
        this.g = null;
    }
}
