package com.fossil;

import android.util.Base64;
import android.util.JsonReader;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.plugins.FilesDumperPlugin;
import com.fossil.v54;
import com.fossil.wearables.fsl.countdown.CountDown;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Explore;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e64 {
    @DexIgnore
    public static /* final */ r84 a;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        T a(JsonReader jsonReader) throws IOException;
    }

    /*
    static {
        d94 d94 = new d94();
        d94.a(a54.a);
        d94.a(true);
        a = d94.a();
    }
    */

    @DexIgnore
    public static v54.d.a h(JsonReader jsonReader) throws IOException {
        v54.d.a.AbstractC0205a f = v54.d.a.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1618432855:
                    if (nextName.equals("identifier")) {
                        c = 0;
                        break;
                    }
                    break;
                case 351608024:
                    if (nextName.equals("version")) {
                        c = 1;
                        break;
                    }
                    break;
                case 719853845:
                    if (nextName.equals("installationUuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1975623094:
                    if (nextName.equals("displayVersion")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                f.b(jsonReader.nextString());
            } else if (c == 1) {
                f.d(jsonReader.nextString());
            } else if (c == 2) {
                f.a(jsonReader.nextString());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                f.c(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return f.a();
    }

    @DexIgnore
    public static v54.b i(JsonReader jsonReader) throws IOException {
        v54.b.a c = v54.b.c();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c2 = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != 106079) {
                if (hashCode == 111972721 && nextName.equals("value")) {
                    c2 = 1;
                }
            } else if (nextName.equals("key")) {
                c2 = 0;
            }
            if (c2 == 0) {
                c.a(jsonReader.nextString());
            } else if (c2 != 1) {
                jsonReader.skipValue();
            } else {
                c.b(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return c.a();
    }

    @DexIgnore
    public static v54.d.c j(JsonReader jsonReader) throws IOException {
        v54.d.c.a j = v54.d.c.j();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1981332476:
                    if (nextName.equals("simulator")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1969347631:
                    if (nextName.equals("manufacturer")) {
                        c = 7;
                        break;
                    }
                    break;
                case 112670:
                    if (nextName.equals("ram")) {
                        c = 3;
                        break;
                    }
                    break;
                case 3002454:
                    if (nextName.equals("arch")) {
                        c = 0;
                        break;
                    }
                    break;
                case 81784169:
                    if (nextName.equals("diskSpace")) {
                        c = 4;
                        break;
                    }
                    break;
                case 94848180:
                    if (nextName.equals("cores")) {
                        c = 2;
                        break;
                    }
                    break;
                case 104069929:
                    if (nextName.equals(DeviceRequestsHelper.DEVICE_INFO_MODEL)) {
                        c = 1;
                        break;
                    }
                    break;
                case 109757585:
                    if (nextName.equals("state")) {
                        c = 6;
                        break;
                    }
                    break;
                case 2078953423:
                    if (nextName.equals("modelClass")) {
                        c = '\b';
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    j.a(jsonReader.nextInt());
                    break;
                case 1:
                    j.b(jsonReader.nextString());
                    break;
                case 2:
                    j.b(jsonReader.nextInt());
                    break;
                case 3:
                    j.b(jsonReader.nextLong());
                    break;
                case 4:
                    j.a(jsonReader.nextLong());
                    break;
                case 5:
                    j.a(jsonReader.nextBoolean());
                    break;
                case 6:
                    j.c(jsonReader.nextInt());
                    break;
                case 7:
                    j.a(jsonReader.nextString());
                    break;
                case '\b':
                    j.c(jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return j.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d k(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.b g = v54.d.AbstractC0206d.g();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1335157162:
                    if (nextName.equals("device")) {
                        c = 3;
                        break;
                    }
                    break;
                case 96801:
                    if (nextName.equals("app")) {
                        c = 2;
                        break;
                    }
                    break;
                case 107332:
                    if (nextName.equals("log")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 1;
                        break;
                    }
                    break;
                case 55126294:
                    if (nextName.equals("timestamp")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                g.a(jsonReader.nextLong());
            } else if (c == 1) {
                g.a(jsonReader.nextString());
            } else if (c == 2) {
                g.a(l(jsonReader));
            } else if (c == 3) {
                g.a(n(jsonReader));
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                g.a(r(jsonReader));
            }
        }
        jsonReader.endObject();
        return g.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a l(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.AbstractC0207a f = v54.d.AbstractC0206d.a.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1332194002:
                    if (nextName.equals(Explore.COLUMN_BACKGROUND)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1090974952:
                    if (nextName.equals("execution")) {
                        c = 2;
                        break;
                    }
                    break;
                case 555169704:
                    if (nextName.equals("customAttributes")) {
                        c = 3;
                        break;
                    }
                    break;
                case 928737948:
                    if (nextName.equals("uiOrientation")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                f.a(Boolean.valueOf(jsonReader.nextBoolean()));
            } else if (c == 1) {
                f.a(jsonReader.nextInt());
            } else if (c == 2) {
                f.a(o(jsonReader));
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                f.a(a(jsonReader, z54.a()));
            }
        }
        jsonReader.endObject();
        return f.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b.AbstractC0208a m(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a f = v54.d.AbstractC0206d.a.b.AbstractC0208a.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case 3373707:
                    if (nextName.equals("name")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3530753:
                    if (nextName.equals("size")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3601339:
                    if (nextName.equals("uuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1153765347:
                    if (nextName.equals("baseAddress")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                f.a(jsonReader.nextString());
            } else if (c == 1) {
                f.a(jsonReader.nextLong());
            } else if (c == 2) {
                f.b(jsonReader.nextLong());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                f.a(Base64.decode(jsonReader.nextString(), 2));
            }
        }
        jsonReader.endObject();
        return f.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.c n(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.c.a g = v54.d.AbstractC0206d.c.g();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1708606089:
                    if (nextName.equals(LegacyDeviceModel.COLUMN_BATTERY_LEVEL)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1455558134:
                    if (nextName.equals("batteryVelocity")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1439500848:
                    if (nextName.equals("orientation")) {
                        c = 4;
                        break;
                    }
                    break;
                case 279795450:
                    if (nextName.equals("diskUsed")) {
                        c = 2;
                        break;
                    }
                    break;
                case 976541947:
                    if (nextName.equals("ramUsed")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1516795582:
                    if (nextName.equals("proximityOn")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                g.a(Double.valueOf(jsonReader.nextDouble()));
            } else if (c == 1) {
                g.a(jsonReader.nextInt());
            } else if (c == 2) {
                g.a(jsonReader.nextLong());
            } else if (c == 3) {
                g.a(jsonReader.nextBoolean());
            } else if (c == 4) {
                g.b(jsonReader.nextInt());
            } else if (c != 5) {
                jsonReader.skipValue();
            } else {
                g.b(jsonReader.nextLong());
            }
        }
        jsonReader.endObject();
        return g.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b o(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.AbstractC0210b e = v54.d.AbstractC0206d.a.b.e();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1337936983:
                    if (nextName.equals("threads")) {
                        c = 0;
                        break;
                    }
                    break;
                case -902467928:
                    if (nextName.equals("signal")) {
                        c = 2;
                        break;
                    }
                    break;
                case 937615455:
                    if (nextName.equals("binaries")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1481625679:
                    if (nextName.equals("exception")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                e.b(a(jsonReader, a64.a()));
            } else if (c == 1) {
                e.a(p(jsonReader));
            } else if (c == 2) {
                e.a(s(jsonReader));
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                e.a(a(jsonReader, b64.a()));
            }
        }
        jsonReader.endObject();
        return e.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b.c p(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.c.AbstractC0211a f = v54.d.AbstractC0206d.a.b.c.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1266514778:
                    if (nextName.equals("frames")) {
                        c = 1;
                        break;
                    }
                    break;
                case -934964668:
                    if (nextName.equals("reason")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 3;
                        break;
                    }
                    break;
                case 91997906:
                    if (nextName.equals("causedBy")) {
                        c = 0;
                        break;
                    }
                    break;
                case 581754413:
                    if (nextName.equals("overflowCount")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                f.a(p(jsonReader));
            } else if (c == 1) {
                f.a(a(jsonReader, c64.a()));
            } else if (c == 2) {
                f.a(jsonReader.nextInt());
            } else if (c == 3) {
                f.b(jsonReader.nextString());
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                f.a(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return f.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b.e.AbstractC0215b q(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a f = v54.d.AbstractC0206d.a.b.e.AbstractC0215b.f();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1019779949:
                    if (nextName.equals(Constants.JSON_KEY_OFFSET)) {
                        c = 2;
                        break;
                    }
                    break;
                case -887523944:
                    if (nextName.equals("symbol")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3571:
                    if (nextName.equals("pc")) {
                        c = 3;
                        break;
                    }
                    break;
                case 3143036:
                    if (nextName.equals("file")) {
                        c = 1;
                        break;
                    }
                    break;
                case 2125650548:
                    if (nextName.equals("importance")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                f.a(jsonReader.nextInt());
            } else if (c == 1) {
                f.a(jsonReader.nextString());
            } else if (c == 2) {
                f.a(jsonReader.nextLong());
            } else if (c == 3) {
                f.b(jsonReader.nextLong());
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                f.b(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return f.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.AbstractC0217d r(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.AbstractC0217d.a b = v54.d.AbstractC0206d.AbstractC0217d.b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            if (nextName.hashCode() == 951530617 && nextName.equals("content")) {
                c = 0;
            }
            if (c != 0) {
                jsonReader.skipValue();
            } else {
                b.a(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return b.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b.AbstractC0212d s(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a d = v54.d.AbstractC0206d.a.b.AbstractC0212d.d();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -1147692044) {
                if (hashCode != 3059181) {
                    if (hashCode == 3373707 && nextName.equals("name")) {
                        c = 0;
                    }
                } else if (nextName.equals("code")) {
                    c = 1;
                }
            } else if (nextName.equals("address")) {
                c = 2;
            }
            if (c == 0) {
                d.b(jsonReader.nextString());
            } else if (c == 1) {
                d.a(jsonReader.nextString());
            } else if (c != 2) {
                jsonReader.skipValue();
            } else {
                d.a(jsonReader.nextLong());
            }
        }
        jsonReader.endObject();
        return d.a();
    }

    @DexIgnore
    public static v54.d.AbstractC0206d.a.b.e t(JsonReader jsonReader) throws IOException {
        v54.d.AbstractC0206d.a.b.e.AbstractC0214a d = v54.d.AbstractC0206d.a.b.e.d();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -1266514778) {
                if (hashCode != 3373707) {
                    if (hashCode == 2125650548 && nextName.equals("importance")) {
                        c = 0;
                    }
                } else if (nextName.equals("name")) {
                    c = 1;
                }
            } else if (nextName.equals("frames")) {
                c = 2;
            }
            if (c == 0) {
                d.a(jsonReader.nextInt());
            } else if (c == 1) {
                d.a(jsonReader.nextString());
            } else if (c != 2) {
                jsonReader.skipValue();
            } else {
                d.a(a(jsonReader, d64.a()));
            }
        }
        jsonReader.endObject();
        return d.a();
    }

    @DexIgnore
    public static v54.c.b u(JsonReader jsonReader) throws IOException {
        v54.c.b.a c = v54.c.b.c();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c2 = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -734768633) {
                if (hashCode == -567321830 && nextName.equals("contents")) {
                    c2 = 1;
                }
            } else if (nextName.equals("filename")) {
                c2 = 0;
            }
            if (c2 == 0) {
                c.a(jsonReader.nextString());
            } else if (c2 != 1) {
                jsonReader.skipValue();
            } else {
                c.a(Base64.decode(jsonReader.nextString(), 2));
            }
        }
        jsonReader.endObject();
        return c.a();
    }

    @DexIgnore
    public static v54.c v(JsonReader jsonReader) throws IOException {
        v54.c.a c = v54.c.c();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c2 = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != 97434231) {
                if (hashCode == 106008351 && nextName.equals("orgId")) {
                    c2 = 1;
                }
            } else if (nextName.equals(FilesDumperPlugin.NAME)) {
                c2 = 0;
            }
            if (c2 == 0) {
                c.a(a(jsonReader, y54.a()));
            } else if (c2 != 1) {
                jsonReader.skipValue();
            } else {
                c.a(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return c.a();
    }

    @DexIgnore
    public static v54.d.e w(JsonReader jsonReader) throws IOException {
        v54.d.e.a e = v54.d.e.e();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -911706486:
                    if (nextName.equals("buildVersion")) {
                        c = 2;
                        break;
                    }
                    break;
                case -293026577:
                    if (nextName.equals("jailbroken")) {
                        c = 3;
                        break;
                    }
                    break;
                case 351608024:
                    if (nextName.equals("version")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1874684019:
                    if (nextName.equals("platform")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                e.a(jsonReader.nextInt());
            } else if (c == 1) {
                e.b(jsonReader.nextString());
            } else if (c == 2) {
                e.a(jsonReader.nextString());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                e.a(jsonReader.nextBoolean());
            }
        }
        jsonReader.endObject();
        return e.a();
    }

    @DexIgnore
    public static v54 x(JsonReader jsonReader) throws IOException {
        v54.a l = v54.l();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -2118372775:
                    if (nextName.equals("ndkPayload")) {
                        c = 7;
                        break;
                    }
                    break;
                case -1962630338:
                    if (nextName.equals("sdkVersion")) {
                        c = 0;
                        break;
                    }
                    break;
                case -911706486:
                    if (nextName.equals("buildVersion")) {
                        c = 4;
                        break;
                    }
                    break;
                case 344431858:
                    if (nextName.equals("gmpAppId")) {
                        c = 1;
                        break;
                    }
                    break;
                case 719853845:
                    if (nextName.equals("installationUuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1874684019:
                    if (nextName.equals("platform")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1975623094:
                    if (nextName.equals("displayVersion")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1984987798:
                    if (nextName.equals(Constants.SESSION)) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    l.e(jsonReader.nextString());
                    break;
                case 1:
                    l.c(jsonReader.nextString());
                    break;
                case 2:
                    l.a(jsonReader.nextInt());
                    break;
                case 3:
                    l.d(jsonReader.nextString());
                    break;
                case 4:
                    l.a(jsonReader.nextString());
                    break;
                case 5:
                    l.b(jsonReader.nextString());
                    break;
                case 6:
                    l.a(y(jsonReader));
                    break;
                case 7:
                    l.a(v(jsonReader));
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return l.a();
    }

    @DexIgnore
    public static v54.d y(JsonReader jsonReader) throws IOException {
        v54.d.b n = v54.d.n();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -2128794476:
                    if (nextName.equals("startedAt")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1618432855:
                    if (nextName.equals("identifier")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1606742899:
                    if (nextName.equals(CountDown.COLUMN_ENDED_AT)) {
                        c = 3;
                        break;
                    }
                    break;
                case -1335157162:
                    if (nextName.equals("device")) {
                        c = '\b';
                        break;
                    }
                    break;
                case -1291329255:
                    if (nextName.equals("events")) {
                        c = '\t';
                        break;
                    }
                    break;
                case 3556:
                    if (nextName.equals("os")) {
                        c = 7;
                        break;
                    }
                    break;
                case 96801:
                    if (nextName.equals("app")) {
                        c = 6;
                        break;
                    }
                    break;
                case 3599307:
                    if (nextName.equals("user")) {
                        c = 5;
                        break;
                    }
                    break;
                case 286956243:
                    if (nextName.equals("generator")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1025385094:
                    if (nextName.equals("crashed")) {
                        c = 4;
                        break;
                    }
                    break;
                case 2047016109:
                    if (nextName.equals("generatorType")) {
                        c = '\n';
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    n.a(jsonReader.nextString());
                    break;
                case 1:
                    n.a(Base64.decode(jsonReader.nextString(), 2));
                    break;
                case 2:
                    n.a(jsonReader.nextLong());
                    break;
                case 3:
                    n.a(Long.valueOf(jsonReader.nextLong()));
                    break;
                case 4:
                    n.a(jsonReader.nextBoolean());
                    break;
                case 5:
                    n.a(z(jsonReader));
                    break;
                case 6:
                    n.a(h(jsonReader));
                    break;
                case 7:
                    n.a(w(jsonReader));
                    break;
                case '\b':
                    n.a(j(jsonReader));
                    break;
                case '\t':
                    n.a(a(jsonReader, x54.a()));
                    break;
                case '\n':
                    n.a(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return n.a();
    }

    @DexIgnore
    public static v54.d.f z(JsonReader jsonReader) throws IOException {
        v54.d.f.a b = v54.d.f.b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            if (nextName.hashCode() == -1618432855 && nextName.equals("identifier")) {
                c = 0;
            }
            if (c != 0) {
                jsonReader.skipValue();
            } else {
                b.a(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return b.a();
    }

    @DexIgnore
    public String a(v54 v54) {
        return a.a(v54);
    }

    @DexIgnore
    public v54 b(String str) throws IOException {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            try {
                v54 x = x(jsonReader);
                jsonReader.close();
                return x;
            } catch (Throwable unused) {
            }
            throw th;
        } catch (IllegalStateException e) {
            throw new IOException(e);
        }
    }

    @DexIgnore
    public String a(v54.d.AbstractC0206d dVar) {
        return a.a(dVar);
    }

    @DexIgnore
    public v54.d.AbstractC0206d a(String str) throws IOException {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            try {
                v54.d.AbstractC0206d k = k(jsonReader);
                jsonReader.close();
                return k;
            } catch (Throwable unused) {
            }
            throw th;
        } catch (IllegalStateException e) {
            throw new IOException(e);
        }
    }

    @DexIgnore
    public static <T> w54<T> a(JsonReader jsonReader, a<T> aVar) throws IOException {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            arrayList.add(aVar.a(jsonReader));
        }
        jsonReader.endArray();
        return w54.a(arrayList);
    }
}
