package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr extends or {
    @DexIgnore
    public /* final */ ar7 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ br c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vr(ar7 ar7, String str, br brVar) {
        super(null);
        ee7.b(ar7, "source");
        ee7.b(brVar, "dataSource");
        this.a = ar7;
        this.b = str;
        this.c = brVar;
    }

    @DexIgnore
    public final br a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final ar7 c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof vr)) {
            return false;
        }
        vr vrVar = (vr) obj;
        return ee7.a(this.a, vrVar.a) && ee7.a(this.b, vrVar.b) && ee7.a(this.c, vrVar.c);
    }

    @DexIgnore
    public int hashCode() {
        ar7 ar7 = this.a;
        int i = 0;
        int hashCode = (ar7 != null ? ar7.hashCode() : 0) * 31;
        String str = this.b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        br brVar = this.c;
        if (brVar != null) {
            i = brVar.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "SourceResult(source=" + this.a + ", mimeType=" + this.b + ", dataSource=" + this.c + ")";
    }
}
