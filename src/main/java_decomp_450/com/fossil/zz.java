package com.fossil;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.m00;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zz<Data> implements m00<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;
    @DexIgnore
    public /* final */ a<Data> b;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        ix<Data> a(AssetManager assetManager, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements n00<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, ParcelFileDescriptor> a(q00 q00) {
            return new zz(this.a, this);
        }

        @DexIgnore
        @Override // com.fossil.zz.a
        public ix<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new mx(assetManager, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements n00<Uri, InputStream>, a<InputStream> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, InputStream> a(q00 q00) {
            return new zz(this.a, this);
        }

        @DexIgnore
        @Override // com.fossil.zz.a
        public ix<InputStream> a(AssetManager assetManager, String str) {
            return new sx(assetManager, str);
        }
    }

    @DexIgnore
    public zz(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    @DexIgnore
    public m00.a<Data> a(Uri uri, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(uri), this.b.a(this.a, uri.toString().substring(c)));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
