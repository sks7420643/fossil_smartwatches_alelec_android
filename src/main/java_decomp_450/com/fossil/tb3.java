package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb3 extends i72 implements Iterable<String> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tb3> CREATOR; // = new vb3();
    @DexIgnore
    public /* final */ Bundle a;

    @DexIgnore
    public tb3(Bundle bundle) {
        this.a = bundle;
    }

    @DexIgnore
    public final Object b(String str) {
        return this.a.get(str);
    }

    @DexIgnore
    public final Long c(String str) {
        return Long.valueOf(this.a.getLong(str));
    }

    @DexIgnore
    public final Double d(String str) {
        return Double.valueOf(this.a.getDouble(str));
    }

    @DexIgnore
    public final String e(String str) {
        return this.a.getString(str);
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public final Iterator<String> iterator() {
        return new sb3(this);
    }

    @DexIgnore
    public final String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, zzb(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final int zza() {
        return this.a.size();
    }

    @DexIgnore
    public final Bundle zzb() {
        return new Bundle(this.a);
    }
}
