package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import androidx.fragment.app.Fragment;
import com.fossil.d8;
import com.fossil.oc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bc {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements d8.a {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;

        @DexIgnore
        public a(Fragment fragment) {
            this.a = fragment;
        }

        @DexIgnore
        @Override // com.fossil.d8.a
        public void onCancel() {
            if (this.a.getAnimatingAway() != null) {
                View animatingAway = this.a.getAnimatingAway();
                this.a.setAnimatingAway(null);
                animatingAway.clearAnimation();
            }
            this.a.setAnimator(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ oc.g c;
        @DexIgnore
        public /* final */ /* synthetic */ d8 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                if (b.this.b.getAnimatingAway() != null) {
                    b.this.b.setAnimatingAway(null);
                    b bVar = b.this;
                    bVar.c.a(bVar.b, bVar.d);
                }
            }
        }

        @DexIgnore
        public b(ViewGroup viewGroup, Fragment fragment, oc.g gVar, d8 d8Var) {
            this.a = viewGroup;
            this.b = fragment;
            this.c = gVar;
            this.d = d8Var;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.a.post(new a());
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ oc.g d;
        @DexIgnore
        public /* final */ /* synthetic */ d8 e;

        @DexIgnore
        public c(ViewGroup viewGroup, View view, Fragment fragment, oc.g gVar, d8 d8Var) {
            this.a = viewGroup;
            this.b = view;
            this.c = fragment;
            this.d = gVar;
            this.e = d8Var;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.endViewTransition(this.b);
            Animator animator2 = this.c.getAnimator();
            this.c.setAnimator(null);
            if (animator2 != null && this.a.indexOfChild(this.b) < 0) {
                this.d.a(this.c, this.e);
            }
        }
    }

    @DexIgnore
    public static d a(Context context, cc ccVar, Fragment fragment, boolean z) {
        int a2;
        int nextTransition = fragment.getNextTransition();
        int nextAnim = fragment.getNextAnim();
        boolean z2 = false;
        fragment.setNextAnim(0);
        View a3 = ccVar.a(fragment.mContainerId);
        if (!(a3 == null || a3.getTag(wb.visible_removing_fragment_view_tag) == null)) {
            a3.setTag(wb.visible_removing_fragment_view_tag, null);
        }
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null && viewGroup.getLayoutTransition() != null) {
            return null;
        }
        Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, z, nextAnim);
        if (onCreateAnimation != null) {
            return new d(onCreateAnimation);
        }
        Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, z, nextAnim);
        if (onCreateAnimator != null) {
            return new d(onCreateAnimator);
        }
        if (nextAnim != 0) {
            boolean equals = "anim".equals(context.getResources().getResourceTypeName(nextAnim));
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(context, nextAnim);
                    if (loadAnimation != null) {
                        return new d(loadAnimation);
                    }
                    z2 = true;
                } catch (Resources.NotFoundException e2) {
                    throw e2;
                } catch (RuntimeException unused) {
                }
            }
            if (!z2) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(context, nextAnim);
                    if (loadAnimator != null) {
                        return new d(loadAnimator);
                    }
                } catch (RuntimeException e3) {
                    if (!equals) {
                        Animation loadAnimation2 = AnimationUtils.loadAnimation(context, nextAnim);
                        if (loadAnimation2 != null) {
                            return new d(loadAnimation2);
                        }
                    } else {
                        throw e3;
                    }
                }
            }
        }
        if (nextTransition != 0 && (a2 = a(nextTransition, z)) >= 0) {
            return new d(AnimationUtils.loadAnimation(context, a2));
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ Animation a;
        @DexIgnore
        public /* final */ Animator b;

        @DexIgnore
        public d(Animation animation) {
            this.a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        @DexIgnore
        public d(Animator animator) {
            this.a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends AnimationSet implements Runnable {
        @DexIgnore
        public /* final */ ViewGroup a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e; // = true;

        @DexIgnore
        public e(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.a = viewGroup;
            this.b = view;
            addAnimation(animation);
            this.a.post(this);
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation) {
            this.e = true;
            if (this.c) {
                return !this.d;
            }
            if (!super.getTransformation(j, transformation)) {
                this.c = true;
                aa.a(this.a, this);
            }
            return true;
        }

        @DexIgnore
        public void run() {
            if (this.c || !this.e) {
                this.a.endViewTransition(this.b);
                this.d = true;
                return;
            }
            this.e = false;
            this.a.post(this);
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation, float f) {
            this.e = true;
            if (this.c) {
                return !this.d;
            }
            if (!super.getTransformation(j, transformation, f)) {
                this.c = true;
                aa.a(this.a, this);
            }
            return true;
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, d dVar, oc.g gVar) {
        View view = fragment.mView;
        ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        d8 d8Var = new d8();
        d8Var.a(new a(fragment));
        gVar.b(fragment, d8Var);
        if (dVar.a != null) {
            e eVar = new e(dVar.a, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            eVar.setAnimationListener(new b(viewGroup, fragment, gVar, d8Var));
            fragment.mView.startAnimation(eVar);
            return;
        }
        Animator animator = dVar.b;
        fragment.setAnimator(animator);
        animator.addListener(new c(viewGroup, view, fragment, gVar, d8Var));
        animator.setTarget(fragment.mView);
        animator.start();
    }

    @DexIgnore
    public static int a(int i, boolean z) {
        if (i == 4097) {
            return z ? vb.fragment_open_enter : vb.fragment_open_exit;
        }
        if (i == 4099) {
            return z ? vb.fragment_fade_enter : vb.fragment_fade_exit;
        }
        if (i != 8194) {
            return -1;
        }
        return z ? vb.fragment_close_enter : vb.fragment_close_exit;
    }
}
