package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.go7;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn7 {
    @DexIgnore
    public /* final */ go7 a;
    @DexIgnore
    public /* final */ bo7 b;
    @DexIgnore
    public /* final */ SocketFactory c;
    @DexIgnore
    public /* final */ on7 d;
    @DexIgnore
    public /* final */ List<jo7> e;
    @DexIgnore
    public /* final */ List<wn7> f;
    @DexIgnore
    public /* final */ ProxySelector g;
    @DexIgnore
    public /* final */ Proxy h;
    @DexIgnore
    public /* final */ SSLSocketFactory i;
    @DexIgnore
    public /* final */ HostnameVerifier j;
    @DexIgnore
    public /* final */ sn7 k;

    @DexIgnore
    public nn7(String str, int i2, bo7 bo7, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, sn7 sn7, on7 on7, Proxy proxy, List<jo7> list, List<wn7> list2, ProxySelector proxySelector) {
        go7.a aVar = new go7.a();
        aVar.f(sSLSocketFactory != null ? Utility.URL_SCHEME : "http");
        aVar.b(str);
        aVar.a(i2);
        this.a = aVar.a();
        if (bo7 != null) {
            this.b = bo7;
            if (socketFactory != null) {
                this.c = socketFactory;
                if (on7 != null) {
                    this.d = on7;
                    if (list != null) {
                        this.e = ro7.a(list);
                        if (list2 != null) {
                            this.f = ro7.a(list2);
                            if (proxySelector != null) {
                                this.g = proxySelector;
                                this.h = proxy;
                                this.i = sSLSocketFactory;
                                this.j = hostnameVerifier;
                                this.k = sn7;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    @DexIgnore
    public sn7 a() {
        return this.k;
    }

    @DexIgnore
    public List<wn7> b() {
        return this.f;
    }

    @DexIgnore
    public bo7 c() {
        return this.b;
    }

    @DexIgnore
    public HostnameVerifier d() {
        return this.j;
    }

    @DexIgnore
    public List<jo7> e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof nn7) {
            nn7 nn7 = (nn7) obj;
            return this.a.equals(nn7.a) && a(nn7);
        }
    }

    @DexIgnore
    public Proxy f() {
        return this.h;
    }

    @DexIgnore
    public on7 g() {
        return this.d;
    }

    @DexIgnore
    public ProxySelector h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31;
        Proxy proxy = this.h;
        int i2 = 0;
        int hashCode2 = (hashCode + (proxy != null ? proxy.hashCode() : 0)) * 31;
        SSLSocketFactory sSLSocketFactory = this.i;
        int hashCode3 = (hashCode2 + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.j;
        int hashCode4 = (hashCode3 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        sn7 sn7 = this.k;
        if (sn7 != null) {
            i2 = sn7.hashCode();
        }
        return hashCode4 + i2;
    }

    @DexIgnore
    public SocketFactory i() {
        return this.c;
    }

    @DexIgnore
    public SSLSocketFactory j() {
        return this.i;
    }

    @DexIgnore
    public go7 k() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.a.g());
        sb.append(":");
        sb.append(this.a.k());
        if (this.h != null) {
            sb.append(", proxy=");
            sb.append(this.h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.g);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public boolean a(nn7 nn7) {
        return this.b.equals(nn7.b) && this.d.equals(nn7.d) && this.e.equals(nn7.e) && this.f.equals(nn7.f) && this.g.equals(nn7.g) && ro7.a(this.h, nn7.h) && ro7.a(this.i, nn7.i) && ro7.a(this.j, nn7.j) && ro7.a(this.k, nn7.k) && k().k() == nn7.k().k();
    }
}
