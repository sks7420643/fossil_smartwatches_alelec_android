package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kp {
    @DexIgnore
    public /* final */ WorkDatabase a;

    @DexIgnore
    public kp(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    @DexIgnore
    public boolean a() {
        Long a2 = this.a.b().a("reschedule_needed");
        return a2 != null && a2.longValue() == 1;
    }

    @DexIgnore
    public void a(boolean z) {
        this.a.b().a(new no("reschedule_needed", z));
    }

    @DexIgnore
    public static void a(Context context, wi wiVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (sharedPreferences.contains("reschedule_needed") || sharedPreferences.contains("last_cancel_all_time_ms")) {
            long j = 0;
            long j2 = sharedPreferences.getLong("last_cancel_all_time_ms", 0);
            if (sharedPreferences.getBoolean("reschedule_needed", false)) {
                j = 1;
            }
            wiVar.beginTransaction();
            try {
                wiVar.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"last_cancel_all_time_ms", Long.valueOf(j2)});
                wiVar.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", Long.valueOf(j)});
                sharedPreferences.edit().clear().apply();
                wiVar.setTransactionSuccessful();
            } finally {
                wiVar.endTransaction();
            }
        }
    }
}
