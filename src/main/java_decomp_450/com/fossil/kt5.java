package com.fossil;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt5 implements Factory<jt5> {
    @DexIgnore
    public static jt5 a(ft5 ft5, rl4 rl4, nw5 nw5, vu5 vu5, mt5 mt5, ch5 ch5, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new jt5(ft5, rl4, nw5, vu5, mt5, ch5, notificationSettingsDatabase);
    }
}
