package com.fossil;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx5 extends go5 implements kx5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public jx5 f;
    @DexIgnore
    public qw6<y35> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lx5.i;
        }

        @DexIgnore
        public final lx5 b() {
            return new lx5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lx5 a;

        @DexIgnore
        public b(lx5 lx5) {
            this.a = lx5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationSummaryDialView.b {
        @DexIgnore
        public /* final */ /* synthetic */ lx5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(lx5 lx5) {
            this.a = lx5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NotificationSummaryDialView.b
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.z.a(this.a, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ y35 a;
        @DexIgnore
        public /* final */ /* synthetic */ se7 b;

        @DexIgnore
        public d(y35 y35, se7 se7) {
            this.a = y35;
            this.b = se7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.a.v.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.a.v;
            ee7.a((Object) notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener(this.b.element);
        }
    }

    /*
    static {
        String simpleName = lx5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        y35 y35 = (y35) qb.a(layoutInflater, 2131558591, viewGroup, false, a1());
        y35.u.setOnClickListener(new b(this));
        y35.v.setOnItemClickListener(new c(this));
        se7 se7 = new se7();
        se7.element = null;
        se7.element = (T) new d(y35, se7);
        NotificationSummaryDialView notificationSummaryDialView = y35.v;
        ee7.a((Object) notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener(se7.element);
        this.g = new qw6<>(this, y35);
        ee7.a((Object) y35, "binding");
        return y35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        jx5 jx5 = this.f;
        if (jx5 != null) {
            jx5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        jx5 jx5 = this.f;
        if (jx5 != null) {
            jx5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(jx5 jx5) {
        ee7.b(jx5, "presenter");
        this.f = jx5;
    }

    @DexIgnore
    @Override // com.fossil.kx5
    public void a(SparseArray<List<BaseFeatureModel>> sparseArray) {
        NotificationSummaryDialView notificationSummaryDialView;
        ee7.b(sparseArray, "data");
        qw6<y35> qw6 = this.g;
        if (qw6 != null) {
            y35 a2 = qw6.a();
            if (a2 != null && (notificationSummaryDialView = a2.v) != null) {
                notificationSummaryDialView.setDataAsync(sparseArray);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
