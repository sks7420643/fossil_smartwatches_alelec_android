package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aw5 extends dy6 implements zv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<q55> p;
    @DexIgnore
    public yv5 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return aw5.s;
        }

        @DexIgnore
        public final aw5 b() {
            return new aw5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ aw5 a;
        @DexIgnore
        public /* final */ /* synthetic */ q55 b;

        @DexIgnore
        public b(aw5 aw5, q55 q55) {
            this.a = aw5;
            this.b = q55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            yv5 a2 = aw5.a(this.a);
            NumberPicker numberPicker2 = this.b.u;
            ee7.a((Object) numberPicker2, "binding.numberPicker");
            a2.a(numberPicker2.getValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ aw5 a;

        @DexIgnore
        public c(aw5 aw5) {
            this.a = aw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            aw5.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ q55 a;
        @DexIgnore
        public /* final */ /* synthetic */ se7 b;

        @DexIgnore
        public d(q55 q55, se7 se7) {
            this.a = q55;
            this.b = se7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.q;
            ee7.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.e(3);
                        q55 q55 = this.a;
                        ee7.a((Object) q55, "it");
                        View d = q55.d();
                        ee7.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener(this.b.element);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = aw5.class.getSimpleName();
        ee7.a((Object) simpleName, "RemindTimeFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yv5 a(aw5 aw5) {
        yv5 yv5 = aw5.q;
        if (yv5 != null) {
            return yv5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zv5
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        q55 q55 = (q55) qb.a(layoutInflater, 2131558614, viewGroup, false, this.j);
        String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            q55.q.setBackgroundColor(Color.parseColor(b2));
        }
        q55.r.setOnClickListener(new c(this));
        ee7.a((Object) q55, "binding");
        a(q55);
        this.p = new qw6<>(this, q55);
        return q55.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        yv5 yv5 = this.q;
        if (yv5 != null) {
            yv5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yv5 yv5 = this.q;
        if (yv5 != null) {
            yv5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<q55> qw6 = this.p;
        if (qw6 != null) {
            q55 a2 = qw6.a();
            if (a2 != null) {
                se7 se7 = new se7();
                se7.element = null;
                se7.element = (T) new d(a2, se7);
                ee7.a((Object) a2, "it");
                View d2 = a2.d();
                ee7.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener(se7.element);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(yv5 yv5) {
        ee7.b(yv5, "presenter");
        this.q = yv5;
    }

    @DexIgnore
    public final void a(q55 q55) {
        ArrayList arrayList = new ArrayList();
        jf7 a2 = qf7.a(new lf7(20, 120), 20);
        int first = a2.getFirst();
        int last = a2.getLast();
        int a3 = a2.a();
        if (a3 < 0 ? first >= last : first <= last) {
            while (true) {
                String d2 = ze5.d(first);
                ee7.a((Object) d2, "timeString");
                arrayList.add(d2);
                if (first == last) {
                    break;
                }
                first += a3;
            }
        }
        NumberPicker numberPicker = q55.u;
        ee7.a((Object) numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = q55.u;
        ee7.a((Object) numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = q55.u;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            q55.u.setOnValueChangedListener(new b(this, q55));
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.zv5
    public void a(int i) {
        NumberPicker numberPicker;
        qw6<q55> qw6 = this.p;
        if (qw6 != null) {
            q55 a2 = qw6.a();
            if (a2 != null && (numberPicker = a2.u) != null) {
                ee7.a((Object) numberPicker, "numberPicker");
                numberPicker.setValue(i / 20);
                yv5 yv5 = this.q;
                if (yv5 != null) {
                    yv5.a(numberPicker.getValue());
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }
}
