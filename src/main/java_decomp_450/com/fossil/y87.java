package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y87 implements Comparable<y87> {
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public /* synthetic */ y87(byte b) {
        this.a = b;
    }

    @DexIgnore
    public static boolean a(byte b, Object obj) {
        return (obj instanceof y87) && b == ((y87) obj).a();
    }

    @DexIgnore
    public static final /* synthetic */ y87 b(byte b) {
        return new y87(b);
    }

    @DexIgnore
    public static byte c(byte b) {
        return b;
    }

    @DexIgnore
    public static int d(byte b) {
        return b;
    }

    @DexIgnore
    public static String e(byte b) {
        return String.valueOf((int) (b & 255));
    }

    @DexIgnore
    public final /* synthetic */ byte a() {
        return this.a;
    }

    @DexIgnore
    public final int a(byte b) {
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(y87 y87) {
        a(y87.a());
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        byte b = this.a;
        d(b);
        return b;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }
}
