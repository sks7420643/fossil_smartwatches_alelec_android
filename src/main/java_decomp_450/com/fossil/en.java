package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import com.fossil.qm;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class en implements Runnable {
    @DexIgnore
    public static /* final */ String y; // = im.a("WorkerWrapper");
    @DexIgnore
    public Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public List<ym> c;
    @DexIgnore
    public WorkerParameters.a d;
    @DexIgnore
    public zo e;
    @DexIgnore
    public ListenableWorker f;
    @DexIgnore
    public ListenableWorker.a g; // = ListenableWorker.a.a();
    @DexIgnore
    public zl h;
    @DexIgnore
    public vp i;
    @DexIgnore
    public io j;
    @DexIgnore
    public WorkDatabase p;
    @DexIgnore
    public ap q;
    @DexIgnore
    public lo r;
    @DexIgnore
    public dp s;
    @DexIgnore
    public List<String> t;
    @DexIgnore
    public String u;
    @DexIgnore
    public up<Boolean> v; // = up.e();
    @DexIgnore
    public i14<ListenableWorker.a> w; // = null;
    @DexIgnore
    public volatile boolean x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ up a;

        @DexIgnore
        public a(up upVar) {
            this.a = upVar;
        }

        @DexIgnore
        public void run() {
            try {
                im.a().a(en.y, String.format("Starting work for %s", en.this.e.c), new Throwable[0]);
                en.this.w = en.this.f.k();
                this.a.a((i14) en.this.w);
            } catch (Throwable th) {
                this.a.a(th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ up a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public b(up upVar, String str) {
            this.a = upVar;
            this.b = str;
        }

        @DexIgnore
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                ListenableWorker.a aVar = (ListenableWorker.a) this.a.get();
                if (aVar == null) {
                    im.a().b(en.y, String.format("%s returned a null result. Treating it as a failure.", en.this.e.c), new Throwable[0]);
                } else {
                    im.a().a(en.y, String.format("%s returned a %s result.", en.this.e.c, aVar), new Throwable[0]);
                    en.this.g = aVar;
                }
            } catch (CancellationException e) {
                im.a().c(en.y, String.format("%s was cancelled", this.b), e);
            } catch (InterruptedException | ExecutionException e2) {
                im.a().b(en.y, String.format("%s failed because it threw an exception/error", this.b), e2);
            } catch (Throwable th) {
                en.this.c();
                throw th;
            }
            en.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public Context a;
        @DexIgnore
        public ListenableWorker b;
        @DexIgnore
        public io c;
        @DexIgnore
        public vp d;
        @DexIgnore
        public zl e;
        @DexIgnore
        public WorkDatabase f;
        @DexIgnore
        public String g;
        @DexIgnore
        public List<ym> h;
        @DexIgnore
        public WorkerParameters.a i; // = new WorkerParameters.a();

        @DexIgnore
        public c(Context context, zl zlVar, vp vpVar, io ioVar, WorkDatabase workDatabase, String str) {
            this.a = context.getApplicationContext();
            this.d = vpVar;
            this.c = ioVar;
            this.e = zlVar;
            this.f = workDatabase;
            this.g = str;
        }

        @DexIgnore
        public c a(List<ym> list) {
            this.h = list;
            return this;
        }

        @DexIgnore
        public c a(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.i = aVar;
            }
            return this;
        }

        @DexIgnore
        public en a() {
            return new en(this);
        }
    }

    @DexIgnore
    public en(c cVar) {
        this.a = cVar.a;
        this.i = cVar.d;
        this.j = cVar.c;
        this.b = cVar.g;
        this.c = cVar.h;
        this.d = cVar.i;
        this.f = cVar.b;
        this.h = cVar.e;
        WorkDatabase workDatabase = cVar.f;
        this.p = workDatabase;
        this.q = workDatabase.f();
        this.r = this.p.a();
        this.s = this.p.g();
    }

    @DexIgnore
    public i14<Boolean> a() {
        return this.v;
    }

    @DexIgnore
    public void b() {
        boolean z;
        this.x = true;
        j();
        i14<ListenableWorker.a> i14 = this.w;
        if (i14 != null) {
            z = i14.isDone();
            this.w.cancel(true);
        } else {
            z = false;
        }
        ListenableWorker listenableWorker = this.f;
        if (listenableWorker == null || z) {
            im.a().a(y, String.format("WorkSpec %s is already done. Not interrupting.", this.e), new Throwable[0]);
            return;
        }
        listenableWorker.l();
    }

    @DexIgnore
    public void c() {
        if (!j()) {
            this.p.beginTransaction();
            try {
                qm.a d2 = this.q.d(this.b);
                this.p.e().a(this.b);
                if (d2 == null) {
                    a(false);
                } else if (d2 == qm.a.RUNNING) {
                    a(this.g);
                } else if (!d2.isFinished()) {
                    d();
                }
                this.p.setTransactionSuccessful();
            } finally {
                this.p.endTransaction();
            }
        }
        List<ym> list = this.c;
        if (list != null) {
            for (ym ymVar : list) {
                ymVar.a(this.b);
            }
            zm.a(this.h, this.p, this.c);
        }
    }

    @DexIgnore
    public final void d() {
        this.p.beginTransaction();
        try {
            this.q.a(qm.a.ENQUEUED, this.b);
            this.q.b(this.b, System.currentTimeMillis());
            this.q.a(this.b, -1);
            this.p.setTransactionSuccessful();
        } finally {
            this.p.endTransaction();
            a(true);
        }
    }

    @DexIgnore
    public final void e() {
        this.p.beginTransaction();
        try {
            this.q.b(this.b, System.currentTimeMillis());
            this.q.a(qm.a.ENQUEUED, this.b);
            this.q.f(this.b);
            this.q.a(this.b, -1);
            this.p.setTransactionSuccessful();
        } finally {
            this.p.endTransaction();
            a(false);
        }
    }

    @DexIgnore
    public final void f() {
        qm.a d2 = this.q.d(this.b);
        if (d2 == qm.a.RUNNING) {
            im.a().a(y, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", this.b), new Throwable[0]);
            a(true);
            return;
        }
        im.a().a(y, String.format("Status for %s is %s; not doing any work", this.b, d2), new Throwable[0]);
        a(false);
    }

    @DexIgnore
    public final void g() {
        cm a2;
        if (!j()) {
            this.p.beginTransaction();
            try {
                zo e2 = this.q.e(this.b);
                this.e = e2;
                if (e2 == null) {
                    im.a().b(y, String.format("Didn't find WorkSpec for id %s", this.b), new Throwable[0]);
                    a(false);
                } else if (e2.b != qm.a.ENQUEUED) {
                    f();
                    this.p.setTransactionSuccessful();
                    im.a().a(y, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.e.c), new Throwable[0]);
                    this.p.endTransaction();
                } else {
                    if (e2.d() || this.e.c()) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!(this.e.n == 0) && currentTimeMillis < this.e.a()) {
                            im.a().a(y, String.format("Delaying execution for %s because it is being executed before schedule.", this.e.c), new Throwable[0]);
                            a(true);
                            this.p.endTransaction();
                            return;
                        }
                    }
                    this.p.setTransactionSuccessful();
                    this.p.endTransaction();
                    if (this.e.d()) {
                        a2 = this.e.e;
                    } else {
                        gm b2 = this.h.c().b(this.e.d);
                        if (b2 == null) {
                            im.a().b(y, String.format("Could not create Input Merger %s", this.e.d), new Throwable[0]);
                            h();
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.e.e);
                        arrayList.addAll(this.q.g(this.b));
                        a2 = b2.a(arrayList);
                    }
                    WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.b), a2, this.t, this.d, this.e.k, this.h.b(), this.i, this.h.j(), new qp(this.p, this.i), new pp(this.p, this.j, this.i));
                    if (this.f == null) {
                        this.f = this.h.j().b(this.a, this.e.c, workerParameters);
                    }
                    ListenableWorker listenableWorker = this.f;
                    if (listenableWorker == null) {
                        im.a().b(y, String.format("Could not create Worker %s", this.e.c), new Throwable[0]);
                        h();
                    } else if (listenableWorker.h()) {
                        im.a().b(y, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", this.e.c), new Throwable[0]);
                        h();
                    } else {
                        this.f.j();
                        if (!k()) {
                            f();
                        } else if (!j()) {
                            up e3 = up.e();
                            this.i.a().execute(new a(e3));
                            e3.a(new b(e3, this.u), this.i.b());
                        }
                    }
                }
            } finally {
                this.p.endTransaction();
            }
        }
    }

    @DexIgnore
    public void h() {
        this.p.beginTransaction();
        try {
            a(this.b);
            this.q.a(this.b, ((ListenableWorker.a.C0005a) this.g).d());
            this.p.setTransactionSuccessful();
        } finally {
            this.p.endTransaction();
            a(false);
        }
    }

    @DexIgnore
    public final void i() {
        this.p.beginTransaction();
        try {
            this.q.a(qm.a.SUCCEEDED, this.b);
            this.q.a(this.b, ((ListenableWorker.a.c) this.g).d());
            long currentTimeMillis = System.currentTimeMillis();
            for (String str : this.r.a(this.b)) {
                if (this.q.d(str) == qm.a.BLOCKED && this.r.b(str)) {
                    im.a().c(y, String.format("Setting status to enqueued for %s", str), new Throwable[0]);
                    this.q.a(qm.a.ENQUEUED, str);
                    this.q.b(str, currentTimeMillis);
                }
            }
            this.p.setTransactionSuccessful();
        } finally {
            this.p.endTransaction();
            a(false);
        }
    }

    @DexIgnore
    public final boolean j() {
        if (!this.x) {
            return false;
        }
        im.a().a(y, String.format("Work interrupted for %s", this.u), new Throwable[0]);
        qm.a d2 = this.q.d(this.b);
        if (d2 == null) {
            a(false);
        } else {
            a(!d2.isFinished());
        }
        return true;
    }

    @DexIgnore
    public final boolean k() {
        this.p.beginTransaction();
        try {
            boolean z = true;
            if (this.q.d(this.b) == qm.a.ENQUEUED) {
                this.q.a(qm.a.RUNNING, this.b);
                this.q.h(this.b);
            } else {
                z = false;
            }
            this.p.setTransactionSuccessful();
            return z;
        } finally {
            this.p.endTransaction();
        }
    }

    @DexIgnore
    public void run() {
        List<String> a2 = this.s.a(this.b);
        this.t = a2;
        this.u = a(a2);
        g();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e A[Catch:{ all -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027 A[Catch:{ all -> 0x005b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(boolean r5) {
        /*
            r4 = this;
            androidx.work.impl.WorkDatabase r0 = r4.p
            r0.beginTransaction()
            androidx.work.impl.WorkDatabase r0 = r4.p     // Catch:{ all -> 0x005b }
            com.fossil.ap r0 = r0.f()     // Catch:{ all -> 0x005b }
            java.util.List r0 = r0.d()     // Catch:{ all -> 0x005b }
            r1 = 0
            if (r0 == 0) goto L_0x001b
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0019
            goto L_0x001b
        L_0x0019:
            r0 = 0
            goto L_0x001c
        L_0x001b:
            r0 = 1
        L_0x001c:
            if (r0 == 0) goto L_0x0025
            android.content.Context r0 = r4.a     // Catch:{ all -> 0x005b }
            java.lang.Class<androidx.work.impl.background.systemalarm.RescheduleReceiver> r2 = androidx.work.impl.background.systemalarm.RescheduleReceiver.class
            com.fossil.jp.a(r0, r2, r1)     // Catch:{ all -> 0x005b }
        L_0x0025:
            if (r5 == 0) goto L_0x0030
            com.fossil.ap r0 = r4.q     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r4.b     // Catch:{ all -> 0x005b }
            r2 = -1
            r0.a(r1, r2)     // Catch:{ all -> 0x005b }
        L_0x0030:
            com.fossil.zo r0 = r4.e     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            androidx.work.ListenableWorker r0 = r4.f     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            androidx.work.ListenableWorker r0 = r4.f     // Catch:{ all -> 0x005b }
            boolean r0 = r0.g()     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            com.fossil.io r0 = r4.j     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r4.b     // Catch:{ all -> 0x005b }
            r0.a(r1)     // Catch:{ all -> 0x005b }
        L_0x0047:
            androidx.work.impl.WorkDatabase r0 = r4.p     // Catch:{ all -> 0x005b }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x005b }
            androidx.work.impl.WorkDatabase r0 = r4.p
            r0.endTransaction()
            com.fossil.up<java.lang.Boolean> r0 = r4.v
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0.b(r5)
            return
        L_0x005b:
            r5 = move-exception
            androidx.work.impl.WorkDatabase r0 = r4.p
            r0.endTransaction()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.en.a(boolean):void");
    }

    @DexIgnore
    public final void a(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            im.a().c(y, String.format("Worker result SUCCESS for %s", this.u), new Throwable[0]);
            if (this.e.d()) {
                e();
            } else {
                i();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            im.a().c(y, String.format("Worker result RETRY for %s", this.u), new Throwable[0]);
            d();
        } else {
            im.a().c(y, String.format("Worker result FAILURE for %s", this.u), new Throwable[0]);
            if (this.e.d()) {
                e();
            } else {
                h();
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.q.d(str2) != qm.a.CANCELLED) {
                this.q.a(qm.a.FAILED, str2);
            }
            linkedList.addAll(this.r.a(str2));
        }
    }

    @DexIgnore
    public final String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.b);
        sb.append(", tags={ ");
        boolean z = true;
        for (String str : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }
}
