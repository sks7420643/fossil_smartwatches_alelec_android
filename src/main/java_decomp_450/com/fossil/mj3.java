package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public mj3(ti3 ti3, long j) {
        this.b = ti3;
        this.a = j;
    }

    @DexIgnore
    public final void run() {
        this.b.k().p.a(this.a);
        this.b.e().A().a("Minimum session duration set", Long.valueOf(this.a));
    }
}
