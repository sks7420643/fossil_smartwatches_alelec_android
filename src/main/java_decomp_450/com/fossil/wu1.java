package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu1 implements Factory<uu1> {
    @DexIgnore
    public /* final */ Provider<by1> a;
    @DexIgnore
    public /* final */ Provider<by1> b;
    @DexIgnore
    public /* final */ Provider<sv1> c;
    @DexIgnore
    public /* final */ Provider<jw1> d;
    @DexIgnore
    public /* final */ Provider<nw1> e;

    @DexIgnore
    public wu1(Provider<by1> provider, Provider<by1> provider2, Provider<sv1> provider3, Provider<jw1> provider4, Provider<nw1> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static wu1 a(Provider<by1> provider, Provider<by1> provider2, Provider<sv1> provider3, Provider<jw1> provider4, Provider<nw1> provider5) {
        return new wu1(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public uu1 get() {
        return new uu1(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
