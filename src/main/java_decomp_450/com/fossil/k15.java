package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ RTLImageView G;
    @DexIgnore
    public /* final */ RTLImageView H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ View J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ FlexibleProgressBar L;
    @DexIgnore
    public /* final */ ConstraintLayout M;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ FlexibleTextView P;
    @DexIgnore
    public /* final */ View Q;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ CoordinatorLayout x;
    @DexIgnore
    public /* final */ OverviewDayGoalChart y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public k15(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, CoordinatorLayout coordinatorLayout, OverviewDayGoalChart overviewDayGoalChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, ConstraintLayout constraintLayout7, View view2, View view3, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout8, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, View view4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = constraintLayout4;
        this.v = constraintLayout5;
        this.w = constraintLayout6;
        this.x = coordinatorLayout;
        this.y = overviewDayGoalChart;
        this.z = flexibleTextView;
        this.A = flexibleTextView2;
        this.B = flexibleTextView3;
        this.C = flexibleTextView4;
        this.D = flexibleTextView5;
        this.E = flexibleTextView6;
        this.F = rTLImageView;
        this.G = rTLImageView2;
        this.H = rTLImageView3;
        this.I = constraintLayout7;
        this.J = view2;
        this.K = view3;
        this.L = flexibleProgressBar;
        this.M = constraintLayout8;
        this.N = recyclerViewEmptySupport;
        this.O = flexibleTextView7;
        this.P = flexibleTextView8;
        this.Q = view4;
    }
}
