package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.DatabaseHelper;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji5 extends BaseDbProvider implements ii5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ji5(Context context, String str) {
        super(context, str);
        ee7.b(context, "context");
        ee7.b(str, "dbPath");
    }

    @DexIgnore
    @Override // com.fossil.ii5
    public boolean a() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str, "TAG");
            local.d(str, "Inside .clearAllServerSettings");
            e().deleteBuilder().delete();
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str2, "TAG");
            local2.e(str2, "Error Inside e = " + e);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.ii5
    public boolean addOrUpdateServerSetting(ServerSetting serverSetting) {
        if (serverSetting == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str, "TAG");
            local.d(str, "serverSetting is null");
            return false;
        }
        try {
            Dao.CreateOrUpdateStatus createOrUpdate = e().createOrUpdate(serverSetting);
            ee7.a((Object) createOrUpdate, "status");
            if (createOrUpdate.isCreated() || createOrUpdate.isUpdated()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str2, "TAG");
            local2.e(str2, "addOrUpdateServerSetting + e = " + e);
            return false;
        }
    }

    @DexIgnore
    public final Dao<ServerSetting, String> e() {
        Dao<ServerSetting, String> dao = ((BaseDbProvider) this).databaseHelper.getDao(ServerSetting.class);
        ee7.a((Object) dao, "databaseHelper.getDao(ServerSetting::class.java)");
        return dao;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{ServerSetting.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        DatabaseHelper databaseHelper = ((BaseDbProvider) this).databaseHelper;
        ee7.a((Object) databaseHelper, "databaseHelper");
        String dbPath = databaseHelper.getDbPath();
        ee7.a((Object) dbPath, "databaseHelper.dbPath");
        return dbPath;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.ii5
    public ServerSetting getServerSettingByKey(String str) {
        if (str == null) {
            return null;
        }
        try {
            return e().queryBuilder().where().eq("key", str).queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str2, "TAG");
            local.e(str2, "getServerSettingByKey + e = " + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ii5
    public void a(List<ServerSetting> list) {
        Boolean valueOf = list != null ? Boolean.valueOf(list.isEmpty()) : null;
        if (valueOf == null) {
            ee7.a();
            throw null;
        } else if (valueOf.booleanValue()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ((BaseDbProvider) this).TAG;
            ee7.a((Object) str, "TAG");
            local.d(str, "serverSettingList is null or empty");
        } else {
            try {
                for (ServerSetting serverSetting : list) {
                    e().createOrUpdate(serverSetting);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ((BaseDbProvider) this).TAG;
                ee7.a((Object) str2, "TAG");
                local2.e(str2, "addOrUpdateServerSettingList + e = " + e);
            }
        }
    }
}
