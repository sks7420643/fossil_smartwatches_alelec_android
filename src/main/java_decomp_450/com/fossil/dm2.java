package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dm2> CREATOR; // = new em2();
    @DexIgnore
    public int a;
    @DexIgnore
    public bm2 b;
    @DexIgnore
    public i63 c;
    @DexIgnore
    public PendingIntent d;
    @DexIgnore
    public f63 e;
    @DexIgnore
    public kl2 f;

    @DexIgnore
    public dm2(int i, bm2 bm2, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        this.a = i;
        this.b = bm2;
        kl2 kl2 = null;
        this.c = iBinder == null ? null : j63.a(iBinder);
        this.d = pendingIntent;
        this.e = iBinder2 == null ? null : g63.a(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            kl2 = queryLocalInterface instanceof kl2 ? (kl2) queryLocalInterface : new ml2(iBinder3);
        }
        this.f = kl2;
    }

    @DexIgnore
    public static dm2 a(f63 f63, kl2 kl2) {
        return new dm2(2, null, null, null, f63.asBinder(), kl2 != null ? kl2.asBinder() : null);
    }

    @DexIgnore
    public static dm2 a(i63 i63, kl2 kl2) {
        return new dm2(2, null, i63.asBinder(), null, null, kl2 != null ? kl2.asBinder() : null);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, (Parcelable) this.b, i, false);
        i63 i63 = this.c;
        IBinder iBinder = null;
        k72.a(parcel, 3, i63 == null ? null : i63.asBinder(), false);
        k72.a(parcel, 4, (Parcelable) this.d, i, false);
        f63 f63 = this.e;
        k72.a(parcel, 5, f63 == null ? null : f63.asBinder(), false);
        kl2 kl2 = this.f;
        if (kl2 != null) {
            iBinder = kl2.asBinder();
        }
        k72.a(parcel, 6, iBinder, false);
        k72.a(parcel, a2);
    }
}
