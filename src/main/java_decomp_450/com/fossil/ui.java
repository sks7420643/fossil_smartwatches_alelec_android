package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.savedstate.SavedStateRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ui extends LifecycleOwner {
    @DexIgnore
    SavedStateRegistry getSavedStateRegistry();
}
