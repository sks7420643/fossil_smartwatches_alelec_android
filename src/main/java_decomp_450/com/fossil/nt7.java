package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nt7 implements Comparable<nt7> {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public d c;

    @DexIgnore
    public interface c {
        @DexIgnore
        int compareTo(c cVar);

        @DexIgnore
        int getType();

        @DexIgnore
        boolean isNull();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ArrayList<c> implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int compareTo(c cVar) {
            int i;
            if (cVar != null) {
                int type = cVar.getType();
                if (type == 0) {
                    return -1;
                }
                if (type == 1) {
                    return 1;
                }
                if (type == 2) {
                    Iterator it = iterator();
                    Iterator it2 = ((d) cVar).iterator();
                    do {
                        if (!it.hasNext() && !it2.hasNext()) {
                            return 0;
                        }
                        c cVar2 = it.hasNext() ? (c) it.next() : null;
                        c cVar3 = it2.hasNext() ? (c) it2.next() : null;
                        if (cVar2 != null) {
                            i = cVar2.compareTo(cVar3);
                            continue;
                        } else if (cVar3 == null) {
                            i = 0;
                            continue;
                        } else {
                            i = cVar3.compareTo(cVar2) * -1;
                            continue;
                        }
                    } while (i == 0);
                    return i;
                }
                throw new RuntimeException("invalid item: " + cVar.getClass());
            } else if (size() == 0) {
                return 0;
            } else {
                return ((c) get(0)).compareTo(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int getType() {
            return 2;
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public boolean isNull() {
            return size() == 0;
        }

        @DexIgnore
        public void normalize() {
            for (int size = size() - 1; size >= 0; size--) {
                c cVar = (c) get(size);
                if (cVar.isNull()) {
                    remove(size);
                } else if (!(cVar instanceof d)) {
                    return;
                }
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator it = iterator();
            while (it.hasNext()) {
                c cVar = (c) it.next();
                if (sb.length() > 0) {
                    sb.append(cVar instanceof d ? '-' : '.');
                }
                sb.append(cVar);
            }
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements c {
        @DexIgnore
        public static /* final */ String[] b;
        @DexIgnore
        public static /* final */ List<String> c;
        @DexIgnore
        public static /* final */ Properties d;
        @DexIgnore
        public static /* final */ String e; // = String.valueOf(c.indexOf(""));
        @DexIgnore
        public String a;

        /*
        static {
            String[] strArr = {"alpha", "beta", "milestone", "rc", "snapshot", "", "sp"};
            b = strArr;
            c = Arrays.asList(strArr);
            Properties properties = new Properties();
            d = properties;
            properties.put("ga", "");
            d.put("final", "");
            d.put("cr", "rc");
        }
        */

        @DexIgnore
        public e(String str, boolean z) {
            if (z && str.length() == 1) {
                char charAt = str.charAt(0);
                if (charAt == 'a') {
                    str = "alpha";
                } else if (charAt == 'b') {
                    str = "beta";
                } else if (charAt == 'm') {
                    str = "milestone";
                }
            }
            this.a = d.getProperty(str, str);
        }

        @DexIgnore
        public static String a(String str) {
            int indexOf = c.indexOf(str);
            if (indexOf != -1) {
                return String.valueOf(indexOf);
            }
            return c.size() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + str;
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int compareTo(c cVar) {
            if (cVar == null) {
                return a(this.a).compareTo(e);
            }
            int type = cVar.getType();
            if (type == 0) {
                return -1;
            }
            if (type == 1) {
                return a(this.a).compareTo(a(((e) cVar).a));
            }
            if (type == 2) {
                return -1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int getType() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public boolean isNull() {
            return a(this.a).compareTo(e) == 0;
        }

        @DexIgnore
        public String toString() {
            return this.a;
        }
    }

    @DexIgnore
    public nt7(String str) {
        a(str);
    }

    @DexIgnore
    public final void a(String str) {
        this.a = str;
        this.c = new d();
        String lowerCase = str.toLowerCase(Locale.ENGLISH);
        d dVar = this.c;
        Stack stack = new Stack();
        stack.push(dVar);
        int i = 0;
        boolean z = false;
        for (int i2 = 0; i2 < lowerCase.length(); i2++) {
            char charAt = lowerCase.charAt(i2);
            if (charAt == '.') {
                if (i2 == i) {
                    dVar.add(b.c);
                } else {
                    dVar.add(a(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
            } else if (charAt == '-') {
                if (i2 == i) {
                    dVar.add(b.c);
                } else {
                    dVar.add(a(z, lowerCase.substring(i, i2)));
                }
                i = i2 + 1;
                d dVar2 = new d();
                dVar.add(dVar2);
                stack.push(dVar2);
                dVar = dVar2;
            } else if (Character.isDigit(charAt)) {
                if (!z && i2 > i) {
                    dVar.add(new e(lowerCase.substring(i, i2), true));
                    d dVar3 = new d();
                    dVar.add(dVar3);
                    stack.push(dVar3);
                    dVar = dVar3;
                    i = i2;
                }
                z = true;
            } else {
                if (z && i2 > i) {
                    dVar.add(a(true, lowerCase.substring(i, i2)));
                    d dVar4 = new d();
                    dVar.add(dVar4);
                    stack.push(dVar4);
                    dVar = dVar4;
                    i = i2;
                }
                z = false;
            }
        }
        if (lowerCase.length() > i) {
            dVar.add(a(z, lowerCase.substring(i)));
        }
        while (!stack.isEmpty()) {
            ((d) stack.pop()).normalize();
        }
        this.b = this.c.toString();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof nt7) && this.b.equals(((nt7) obj).b);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        public static /* final */ BigInteger b; // = new BigInteger("0");
        @DexIgnore
        public static /* final */ b c; // = new b();
        @DexIgnore
        public /* final */ BigInteger a;

        @DexIgnore
        public b() {
            this.a = b;
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int compareTo(c cVar) {
            if (cVar == null) {
                return !b.equals(this.a);
            }
            int type = cVar.getType();
            if (type == 0) {
                return this.a.compareTo(((b) cVar).a);
            }
            if (type == 1 || type == 2) {
                return 1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public int getType() {
            return 0;
        }

        @DexIgnore
        @Override // com.fossil.nt7.c
        public boolean isNull() {
            return b.equals(this.a);
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public b(String str) {
            this.a = new BigInteger(str);
        }
    }

    @DexIgnore
    public static c a(boolean z, String str) {
        return z ? new b(str) : new e(str, false);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(nt7 nt7) {
        return this.c.compareTo(nt7.c);
    }
}
