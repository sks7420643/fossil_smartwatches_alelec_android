package com.fossil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr0 extends wi1<dg0[], byte[]> {
    @DexIgnore
    public static /* final */ y71<dg0[]>[] b; // = {new ok0(), new lm0()};
    @DexIgnore
    public static /* final */ uk1<byte[]>[] c; // = {new ho0(pb1.ASSET), new dq0(pb1.ASSET)};
    @DexIgnore
    public static /* final */ zr0 d; // = new zr0();

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<dg0[]>[] a() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<byte[]>[] b() {
        return c;
    }

    @DexIgnore
    public final byte[] a(dg0[] dg0Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ArrayList arrayList = new ArrayList();
        for (dg0 dg0 : dg0Arr) {
            byte[] b2 = dg0.b();
            if (b2 != null) {
                arrayList.add(b2);
            }
        }
        for (byte[] bArr : ea7.c((Iterable) arrayList)) {
            byteArrayOutputStream.write(bArr);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final byte[] a(gg0[] gg0Arr, short s, r60 r60) {
        if (gg0Arr.length == 0) {
            return new byte[0];
        }
        return a(s, r60, gg0Arr);
    }
}
