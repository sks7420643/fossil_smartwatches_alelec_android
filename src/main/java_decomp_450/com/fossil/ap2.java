package com.fossil;

import com.fossil.bw2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap2 extends bw2<ap2, a> implements lx2 {
    @DexIgnore
    public static /* final */ ap2 zzl;
    @DexIgnore
    public static volatile wx2<ap2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public jw2<bp2> zzf; // = bw2.o();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public cp2 zzh;
    @DexIgnore
    public boolean zzi;
    @DexIgnore
    public boolean zzj;
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<ap2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(ap2.zzl);
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((ap2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final int o() {
            return ((ap2) ((bw2.a) this).b).s();
        }

        @DexIgnore
        public final String zza() {
            return ((ap2) ((bw2.a) this).b).q();
        }

        @DexIgnore
        public /* synthetic */ a(yo2 yo2) {
            this();
        }

        @DexIgnore
        public final bp2 a(int i) {
            return ((ap2) ((bw2.a) this).b).b(i);
        }

        @DexIgnore
        public final a a(int i, bp2 bp2) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((ap2) ((bw2.a) this).b).a(i, bp2);
            return this;
        }
    }

    /*
    static {
        ap2 ap2 = new ap2();
        zzl = ap2;
        bw2.a(ap2.class, ap2);
    }
    */

    @DexIgnore
    public static a z() {
        return (a) zzl.e();
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final bp2 b(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final List<bp2> r() {
        return this.zzf;
    }

    @DexIgnore
    public final int s() {
        return this.zzf.size();
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final cp2 u() {
        cp2 cp2 = this.zzh;
        return cp2 == null ? cp2.y() : cp2;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean w() {
        return this.zzj;
    }

    @DexIgnore
    public final boolean x() {
        return (this.zzc & 64) != 0;
    }

    @DexIgnore
    public final boolean y() {
        return this.zzk;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void a(int i, bp2 bp2) {
        bp2.getClass();
        jw2<bp2> jw2 = this.zzf;
        if (!jw2.zza()) {
            this.zzf = bw2.a(jw2);
        }
        this.zzf.set(i, bp2);
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new ap2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001\u1004\u0000\u0002\u1008\u0001\u0003\u001b\u0004\u1007\u0002\u0005\u1009\u0003\u0006\u1007\u0004\u0007\u1007\u0005\b\u1007\u0006", new Object[]{"zzc", "zzd", "zze", "zzf", bp2.class, "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                wx2<ap2> wx2 = zzm;
                if (wx2 == null) {
                    synchronized (ap2.class) {
                        wx2 = zzm;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzl);
                            zzm = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
