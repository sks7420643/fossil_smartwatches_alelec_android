package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zf4 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public a countryCodeSource_; // = a.FROM_NUMBER_WITH_PLUS_SIGN;
    @DexIgnore
    public int countryCode_; // = 0;
    @DexIgnore
    public String extension_; // = "";
    @DexIgnore
    public boolean hasCountryCode;
    @DexIgnore
    public boolean hasCountryCodeSource;
    @DexIgnore
    public boolean hasExtension;
    @DexIgnore
    public boolean hasItalianLeadingZero;
    @DexIgnore
    public boolean hasNationalNumber;
    @DexIgnore
    public boolean hasNumberOfLeadingZeros;
    @DexIgnore
    public boolean hasPreferredDomesticCarrierCode;
    @DexIgnore
    public boolean hasRawInput;
    @DexIgnore
    public boolean italianLeadingZero_; // = false;
    @DexIgnore
    public long nationalNumber_; // = 0;
    @DexIgnore
    public int numberOfLeadingZeros_; // = 1;
    @DexIgnore
    public String preferredDomesticCarrierCode_; // = "";
    @DexIgnore
    public String rawInput_; // = "";

    @DexIgnore
    public enum a {
        FROM_NUMBER_WITH_PLUS_SIGN,
        FROM_NUMBER_WITH_IDD,
        FROM_NUMBER_WITHOUT_PLUS_SIGN,
        FROM_DEFAULT_COUNTRY
    }

    @DexIgnore
    public final zf4 clear() {
        clearCountryCode();
        clearNationalNumber();
        clearExtension();
        clearItalianLeadingZero();
        clearNumberOfLeadingZeros();
        clearRawInput();
        clearCountryCodeSource();
        clearPreferredDomesticCarrierCode();
        return this;
    }

    @DexIgnore
    public zf4 clearCountryCode() {
        this.hasCountryCode = false;
        this.countryCode_ = 0;
        return this;
    }

    @DexIgnore
    public zf4 clearCountryCodeSource() {
        this.hasCountryCodeSource = false;
        this.countryCodeSource_ = a.FROM_NUMBER_WITH_PLUS_SIGN;
        return this;
    }

    @DexIgnore
    public zf4 clearExtension() {
        this.hasExtension = false;
        this.extension_ = "";
        return this;
    }

    @DexIgnore
    public zf4 clearItalianLeadingZero() {
        this.hasItalianLeadingZero = false;
        this.italianLeadingZero_ = false;
        return this;
    }

    @DexIgnore
    public zf4 clearNationalNumber() {
        this.hasNationalNumber = false;
        this.nationalNumber_ = 0;
        return this;
    }

    @DexIgnore
    public zf4 clearNumberOfLeadingZeros() {
        this.hasNumberOfLeadingZeros = false;
        this.numberOfLeadingZeros_ = 1;
        return this;
    }

    @DexIgnore
    public zf4 clearPreferredDomesticCarrierCode() {
        this.hasPreferredDomesticCarrierCode = false;
        this.preferredDomesticCarrierCode_ = "";
        return this;
    }

    @DexIgnore
    public zf4 clearRawInput() {
        this.hasRawInput = false;
        this.rawInput_ = "";
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof zf4) && exactlySameAs((zf4) obj);
    }

    @DexIgnore
    public boolean exactlySameAs(zf4 zf4) {
        if (zf4 == null) {
            return false;
        }
        if (this == zf4) {
            return true;
        }
        return this.countryCode_ == zf4.countryCode_ && this.nationalNumber_ == zf4.nationalNumber_ && this.extension_.equals(zf4.extension_) && this.italianLeadingZero_ == zf4.italianLeadingZero_ && this.numberOfLeadingZeros_ == zf4.numberOfLeadingZeros_ && this.rawInput_.equals(zf4.rawInput_) && this.countryCodeSource_ == zf4.countryCodeSource_ && this.preferredDomesticCarrierCode_.equals(zf4.preferredDomesticCarrierCode_) && hasPreferredDomesticCarrierCode() == zf4.hasPreferredDomesticCarrierCode();
    }

    @DexIgnore
    public int getCountryCode() {
        return this.countryCode_;
    }

    @DexIgnore
    public a getCountryCodeSource() {
        return this.countryCodeSource_;
    }

    @DexIgnore
    public String getExtension() {
        return this.extension_;
    }

    @DexIgnore
    public long getNationalNumber() {
        return this.nationalNumber_;
    }

    @DexIgnore
    public int getNumberOfLeadingZeros() {
        return this.numberOfLeadingZeros_;
    }

    @DexIgnore
    public String getPreferredDomesticCarrierCode() {
        return this.preferredDomesticCarrierCode_;
    }

    @DexIgnore
    public String getRawInput() {
        return this.rawInput_;
    }

    @DexIgnore
    public boolean hasCountryCode() {
        return this.hasCountryCode;
    }

    @DexIgnore
    public boolean hasCountryCodeSource() {
        return this.hasCountryCodeSource;
    }

    @DexIgnore
    public boolean hasExtension() {
        return this.hasExtension;
    }

    @DexIgnore
    public boolean hasItalianLeadingZero() {
        return this.hasItalianLeadingZero;
    }

    @DexIgnore
    public boolean hasNationalNumber() {
        return this.hasNationalNumber;
    }

    @DexIgnore
    public boolean hasNumberOfLeadingZeros() {
        return this.hasNumberOfLeadingZeros;
    }

    @DexIgnore
    public boolean hasPreferredDomesticCarrierCode() {
        return this.hasPreferredDomesticCarrierCode;
    }

    @DexIgnore
    public boolean hasRawInput() {
        return this.hasRawInput;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1231;
        int countryCode = (((((((((((((((2173 + getCountryCode()) * 53) + Long.valueOf(getNationalNumber()).hashCode()) * 53) + getExtension().hashCode()) * 53) + (isItalianLeadingZero() ? 1231 : 1237)) * 53) + getNumberOfLeadingZeros()) * 53) + getRawInput().hashCode()) * 53) + getCountryCodeSource().hashCode()) * 53) + getPreferredDomesticCarrierCode().hashCode()) * 53;
        if (!hasPreferredDomesticCarrierCode()) {
            i = 1237;
        }
        return countryCode + i;
    }

    @DexIgnore
    public boolean isItalianLeadingZero() {
        return this.italianLeadingZero_;
    }

    @DexIgnore
    public zf4 mergeFrom(zf4 zf4) {
        if (zf4.hasCountryCode()) {
            setCountryCode(zf4.getCountryCode());
        }
        if (zf4.hasNationalNumber()) {
            setNationalNumber(zf4.getNationalNumber());
        }
        if (zf4.hasExtension()) {
            setExtension(zf4.getExtension());
        }
        if (zf4.hasItalianLeadingZero()) {
            setItalianLeadingZero(zf4.isItalianLeadingZero());
        }
        if (zf4.hasNumberOfLeadingZeros()) {
            setNumberOfLeadingZeros(zf4.getNumberOfLeadingZeros());
        }
        if (zf4.hasRawInput()) {
            setRawInput(zf4.getRawInput());
        }
        if (zf4.hasCountryCodeSource()) {
            setCountryCodeSource(zf4.getCountryCodeSource());
        }
        if (zf4.hasPreferredDomesticCarrierCode()) {
            setPreferredDomesticCarrierCode(zf4.getPreferredDomesticCarrierCode());
        }
        return this;
    }

    @DexIgnore
    public zf4 setCountryCode(int i) {
        this.hasCountryCode = true;
        this.countryCode_ = i;
        return this;
    }

    @DexIgnore
    public zf4 setCountryCodeSource(a aVar) {
        if (aVar != null) {
            this.hasCountryCodeSource = true;
            this.countryCodeSource_ = aVar;
            return this;
        }
        throw null;
    }

    @DexIgnore
    public zf4 setExtension(String str) {
        if (str != null) {
            this.hasExtension = true;
            this.extension_ = str;
            return this;
        }
        throw null;
    }

    @DexIgnore
    public zf4 setItalianLeadingZero(boolean z) {
        this.hasItalianLeadingZero = true;
        this.italianLeadingZero_ = z;
        return this;
    }

    @DexIgnore
    public zf4 setNationalNumber(long j) {
        this.hasNationalNumber = true;
        this.nationalNumber_ = j;
        return this;
    }

    @DexIgnore
    public zf4 setNumberOfLeadingZeros(int i) {
        this.hasNumberOfLeadingZeros = true;
        this.numberOfLeadingZeros_ = i;
        return this;
    }

    @DexIgnore
    public zf4 setPreferredDomesticCarrierCode(String str) {
        if (str != null) {
            this.hasPreferredDomesticCarrierCode = true;
            this.preferredDomesticCarrierCode_ = str;
            return this;
        }
        throw null;
    }

    @DexIgnore
    public zf4 setRawInput(String str) {
        if (str != null) {
            this.hasRawInput = true;
            this.rawInput_ = str;
            return this;
        }
        throw null;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Country Code: ");
        sb.append(this.countryCode_);
        sb.append(" National Number: ");
        sb.append(this.nationalNumber_);
        if (hasItalianLeadingZero() && isItalianLeadingZero()) {
            sb.append(" Leading Zero(s): true");
        }
        if (hasNumberOfLeadingZeros()) {
            sb.append(" Number of leading zeros: ");
            sb.append(this.numberOfLeadingZeros_);
        }
        if (hasExtension()) {
            sb.append(" Extension: ");
            sb.append(this.extension_);
        }
        if (hasCountryCodeSource()) {
            sb.append(" Country Code Source: ");
            sb.append(this.countryCodeSource_);
        }
        if (hasPreferredDomesticCarrierCode()) {
            sb.append(" Preferred Domestic Carrier Code: ");
            sb.append(this.preferredDomesticCarrierCode_);
        }
        return sb.toString();
    }
}
