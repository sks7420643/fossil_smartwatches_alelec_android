package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public xg0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (ee7.a((Object) readString, (Object) t70.class.getCanonicalName())) {
                return t70.CREATOR.createFromParcel(parcel);
            }
            if (ee7.a((Object) readString, (Object) oa0.class.getCanonicalName())) {
                return oa0.CREATOR.createFromParcel(parcel);
            }
            if (ee7.a((Object) readString, (Object) h70.class.getCanonicalName())) {
                return h70.CREATOR.createFromParcel(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public xg0[] newArray(int i) {
            return new xg0[i];
        }
    }

    @DexIgnore
    public xg0() {
        b21.x.d();
    }

    @DexIgnore
    public final void a(r60 r60) {
    }

    @DexIgnore
    public abstract JSONObject b();

    @DexIgnore
    public final JSONObject c() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", b()));
            ee7.a((Object) put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            wl0.h.a(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }

    @DexIgnore
    public xg0(Parcel parcel) {
        b21.x.d();
        parcel.readString();
    }
}
