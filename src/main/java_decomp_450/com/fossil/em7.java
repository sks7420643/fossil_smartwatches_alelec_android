package com.fossil;

import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em7 {
    @DexIgnore
    public static /* final */ boolean a; // = mm7.a("kotlinx.coroutines.fast.service.loader", true);
    @DexIgnore
    public static /* final */ tk7 b;

    /*
    static {
        em7 em7 = new em7();
        b = em7.a();
    }
    */

    @DexIgnore
    public final tk7 a() {
        List<MainDispatcherFactory> list;
        Object obj;
        tk7 a2;
        try {
            if (a) {
                list = xl7.a.a();
            } else {
                list = og7.f(mg7.a(ServiceLoader.load(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader()).iterator()));
            }
            Iterator it = list.iterator();
            if (!it.hasNext()) {
                obj = null;
            } else {
                obj = it.next();
                if (it.hasNext()) {
                    int b2 = ((MainDispatcherFactory) obj).b();
                    do {
                        Object next = it.next();
                        int b3 = ((MainDispatcherFactory) next).b();
                        if (b2 < b3) {
                            obj = next;
                            b2 = b3;
                        }
                    } while (it.hasNext());
                }
            }
            MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory) obj;
            if (mainDispatcherFactory == null || (a2 = fm7.a(mainDispatcherFactory, list)) == null) {
                return fm7.a(null, null, 3, null);
            }
            return a2;
        } catch (Throwable th) {
            return fm7.a(th, null, 2, null);
        }
    }
}
