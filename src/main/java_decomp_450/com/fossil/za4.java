package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class za4 implements fo3 {
    @DexIgnore
    public /* final */ db4 a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public za4(db4 db4, Bundle bundle) {
        this.a = db4;
        this.b = bundle;
    }

    @DexIgnore
    @Override // com.fossil.fo3
    public final Object then(no3 no3) {
        return this.a.a(this.b, no3);
    }
}
