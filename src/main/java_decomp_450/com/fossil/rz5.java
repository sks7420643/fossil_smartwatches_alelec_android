package com.fossil;

import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rz5 extends io5<qz5> {
    @DexIgnore
    void E(String str);

    @DexIgnore
    void O(String str);

    @DexIgnore
    void a(dz5 dz5, DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void a(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    void a(String str, String str2, String str3, boolean z);

    @DexIgnore
    void c();

    @DexIgnore
    void f(String str);

    @DexIgnore
    void f(boolean z);

    @DexIgnore
    void g(boolean z);

    @DexIgnore
    void h(int i);

    @DexIgnore
    void k();

    @DexIgnore
    void m();

    @DexIgnore
    void o();

    @DexIgnore
    void p();
}
