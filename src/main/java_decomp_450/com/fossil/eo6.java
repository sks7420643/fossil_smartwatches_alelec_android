package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo6 {
    @DexIgnore
    public static /* final */ n87 a; // = o87.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ b b; // = new b(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements vc7<eo6> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final eo6 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final eo6 a() {
            n87 a = eo6.a;
            b bVar = eo6.b;
            return (eo6) a.getValue();
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ eo6 a; // = new eo6();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final eo6 a() {
            return a;
        }
    }

    /*
    static {
        ee7.a((Object) do6.INSTANCE.getClass().getSimpleName(), "InAppNotificationUtils::\u2026lass.javaClass.simpleName");
    }
    */
}
