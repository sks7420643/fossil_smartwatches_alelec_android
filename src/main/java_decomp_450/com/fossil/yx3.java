package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.lang.Enum;
import java.util.Collection;
import java.util.EnumSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx3<E extends Enum<E>> extends iy3<E> {
    @DexIgnore
    public /* final */ transient EnumSet<E> b;
    @DexIgnore
    @LazyInit
    public transient int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E extends Enum<E>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumSet<E> delegate;

        @DexIgnore
        public b(EnumSet<E> enumSet) {
            this.delegate = enumSet;
        }

        @DexIgnore
        public Object readResolve() {
            return new yx3(this.delegate.clone());
        }
    }

    @DexIgnore
    public static iy3 asImmutable(EnumSet enumSet) {
        int size = enumSet.size();
        if (size == 0) {
            return iy3.of();
        }
        if (size != 1) {
            return new yx3(enumSet);
        }
        return iy3.of(py3.b(enumSet));
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        return this.b.contains(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof yx3) {
            collection = ((yx3) collection).b;
        }
        return this.b.containsAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof yx3) {
            obj = ((yx3) obj).b;
        }
        return this.b.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public int hashCode() {
        int i = this.c;
        if (i != 0) {
            return i;
        }
        int hashCode = this.b.hashCode();
        this.c = hashCode;
        return hashCode;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.b.isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.iy3
    public Object writeReplace() {
        return new b(this.b);
    }

    @DexIgnore
    public yx3(EnumSet<E> enumSet) {
        this.b = enumSet;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
    public j04<E> iterator() {
        return qy3.e(this.b.iterator());
    }
}
