package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq4 {
    @DexIgnore
    public /* final */ oy6.b a;
    @DexIgnore
    public /* final */ st4 b; // = st4.d.a();
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ String a; // = eh5.l.a().b("nonBrandSurface");
        @DexIgnore
        public /* final */ String b; // = eh5.l.a().b("nonBrandPlaceholderBackground");
        @DexIgnore
        public /* final */ aa5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(aa5 aa5) {
            super(aa5.d());
            ee7.b(aa5, "binding");
            this.c = aa5;
        }

        @DexIgnore
        public final void a(jn4 jn4, long j, int i, oy6.b bVar, st4 st4) {
            String str;
            Date f;
            int i2;
            String valueOf;
            String valueOf2;
            ee7.b(jn4, "player");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            aa5 aa5 = this.c;
            FlexibleTextView flexibleTextView = aa5.t;
            ee7.a((Object) flexibleTextView, "tvLastSync");
            xe5 xe5 = xe5.b;
            Date f2 = jn4.f();
            flexibleTextView.setText(xe5.a(f2 != null ? f2.getTime() : 0));
            Integer p = jn4.p();
            int intValue = p != null ? p.intValue() : 0;
            Integer m = jn4.m();
            int intValue2 = intValue - (m != null ? m.intValue() : 0);
            we7 we7 = we7.a;
            FlexibleTextView flexibleTextView2 = aa5.v;
            ee7.a((Object) flexibleTextView2, "tvSteps");
            String a2 = ig5.a(flexibleTextView2.getContext(), 2131886306);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026rd_FullView_Label__Steps)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            String b2 = eh5.l.a().b("primaryText");
            if (b2 == null) {
                b2 = "#242424";
            }
            int parseColor = Color.parseColor(b2);
            String b3 = eh5.l.a().b("nonBrandNonReachGoal");
            if (b3 == null) {
                b3 = "#a7a7a7";
            }
            int parseColor2 = Color.parseColor(b3);
            if (ee7.a((Object) jn4.s(), (Object) true)) {
                FlexibleTextView flexibleTextView3 = aa5.s;
                ee7.a((Object) flexibleTextView3, "tvFullName");
                View d = aa5.d();
                ee7.a((Object) d, "root");
                flexibleTextView3.setText(ig5.a(d.getContext(), 2131887275));
                ImageView imageView = aa5.r;
                ee7.a((Object) imageView, "ivAvatar");
                rt4.a(imageView, jn4.g(), "", bVar, st4);
                FlexibleTextView flexibleTextView4 = aa5.u;
                ee7.a((Object) flexibleTextView4, "tvRank");
                Integer h = jn4.h();
                flexibleTextView4.setText((h == null || (valueOf2 = String.valueOf(h.intValue())) == null) ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : valueOf2);
                aa5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                aa5.s.setTextColor(parseColor);
                FlexibleTextView flexibleTextView5 = aa5.v;
                ee7.a((Object) flexibleTextView5, "tvSteps");
                flexibleTextView5.setText(xe5.a(xe5.b, String.valueOf(intValue2), format, 0, 4, null));
                str = "tvLastSync";
            } else {
                str = "tvLastSync";
                if (ee7.a((Object) jn4.d(), (Object) PortfolioApp.g0.c().w())) {
                    FlexibleTextView flexibleTextView6 = aa5.s;
                    ee7.a((Object) flexibleTextView6, "tvFullName");
                    View d2 = aa5.d();
                    ee7.a((Object) d2, "root");
                    flexibleTextView6.setText(ig5.a(d2.getContext(), 2131886246));
                    if (this.b != null) {
                        aa5.d().setBackgroundColor(Color.parseColor(this.b));
                    }
                    i2 = parseColor;
                } else {
                    FlexibleTextView flexibleTextView7 = aa5.s;
                    ee7.a((Object) flexibleTextView7, "tvFullName");
                    i2 = parseColor;
                    flexibleTextView7.setText(fu4.a.b(jn4.c(), jn4.e(), jn4.i()));
                    if (this.a != null) {
                        aa5.d().setBackgroundColor(Color.parseColor(this.a));
                    }
                }
                ImageView imageView2 = aa5.r;
                ee7.a((Object) imageView2, "ivAvatar");
                rt4.a(imageView2, jn4.g(), jn4.e(), bVar, st4);
                if (ee7.a((Object) "left_after_start", (Object) jn4.j())) {
                    FlexibleTextView flexibleTextView8 = aa5.u;
                    ee7.a((Object) flexibleTextView8, "tvRank");
                    flexibleTextView8.setText("");
                    aa5.u.setCompoundDrawablesWithIntrinsicBounds(2131231022, 0, 0, 0);
                    aa5.s.setTextColor(parseColor2);
                    FlexibleTextView flexibleTextView9 = aa5.v;
                    ee7.a((Object) flexibleTextView9, "tvSteps");
                    flexibleTextView9.setText(xe5.b.a(String.valueOf(intValue2), format, 0));
                } else {
                    FlexibleTextView flexibleTextView10 = aa5.u;
                    ee7.a((Object) flexibleTextView10, "tvRank");
                    Integer h2 = jn4.h();
                    flexibleTextView10.setText((h2 == null || (valueOf = String.valueOf(h2.intValue())) == null) ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : valueOf);
                    aa5.u.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    aa5.s.setTextColor(i2);
                    FlexibleTextView flexibleTextView11 = aa5.v;
                    ee7.a((Object) flexibleTextView11, "tvSteps");
                    flexibleTextView11.setText(xe5.a(xe5.b, String.valueOf(intValue2), format, 0, 4, null));
                }
            }
            String j2 = jn4.j();
            if (j2 != null && j2.hashCode() == -1402931637 && j2.equals("completed") && i != -1 && (f = jn4.f()) != null) {
                long time = (f.getTime() - j) / ((long) 1000);
                FlexibleTextView flexibleTextView12 = aa5.t;
                ee7.a((Object) flexibleTextView12, str);
                flexibleTextView12.setText(xe5.b.h((int) time));
            }
        }
    }

    @DexIgnore
    public jq4(int i, long j, int i2) {
        this.c = j;
        this.d = i2;
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.a = b2;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        return list.get(i) instanceof jn4;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        aa5 a2 = aa5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemPlayerListBinding.in\u2026(inflater, parent, false)");
        return new a(a2);
    }

    @DexIgnore
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof jn4)) {
            obj = null;
        }
        jn4 jn4 = (jn4) obj;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        if (jn4 != null && aVar != null) {
            aVar.a(jn4, this.c, this.d, this.a, this.b);
        }
    }
}
