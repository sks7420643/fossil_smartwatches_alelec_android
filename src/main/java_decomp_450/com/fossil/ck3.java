package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wj3 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ zj3 c;

    @DexIgnore
    public ck3(zj3 zj3, wj3 wj3, long j) {
        this.c = zj3;
        this.a = wj3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        this.c.a(this.a, false, this.b);
        zj3 zj3 = this.c;
        zj3.e = null;
        zj3.q().a((wj3) null);
    }
}
