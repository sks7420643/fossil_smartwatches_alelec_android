package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o97<E> extends l97<E> implements List<E>, ye7 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(int i, int i2) {
            if (i < 0 || i >= i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public final void b(int i, int i2) {
            if (i < 0 || i > i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            if (i < 0 || i2 > i3) {
                throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2 + ", size: " + i3);
            } else if (i > i2) {
                throw new IllegalArgumentException("fromIndex: " + i + " > toIndex: " + i2);
            }
        }

        @DexIgnore
        public final int a(Collection<?> collection) {
            ee7.b(collection, "c");
            Iterator<?> it = collection.iterator();
            int i = 1;
            while (it.hasNext()) {
                Object next = it.next();
                i = (i * 31) + (next != null ? next.hashCode() : 0);
            }
            return i;
        }

        @DexIgnore
        public final boolean a(Collection<?> collection, Collection<?> collection2) {
            ee7.b(collection, "c");
            ee7.b(collection2, FacebookRequestErrorClassification.KEY_OTHER);
            if (collection.size() != collection2.size()) {
                return false;
            }
            Iterator<?> it = collection2.iterator();
            Iterator<?> it2 = collection.iterator();
            while (it2.hasNext()) {
                if (!ee7.a(it2.next(), it.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E>, ye7 {
        @DexIgnore
        public int a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < o97.this.size();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (hasNext()) {
                o97 o97 = o97.this;
                int i = this.a;
                this.a = i + 1;
                return (E) o97.get(i);
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends o97<E>.b implements ListIterator<E>, ye7 {
        @DexIgnore
        public c(int i) {
            super();
            o97.a.b(i, o97.this.size());
            a(i);
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public boolean hasPrevious() {
            return a() > 0;
        }

        @DexIgnore
        public int nextIndex() {
            return a();
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public E previous() {
            if (hasPrevious()) {
                o97 o97 = o97.this;
                a(a() - 1);
                return (E) o97.get(a());
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return a() - 1;
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<E> extends o97<E> implements RandomAccess {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ o97<E> c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.o97<? extends E> */
        /* JADX WARN: Multi-variable type inference failed */
        public d(o97<? extends E> o97, int i, int i2) {
            ee7.b(o97, "list");
            this.c = o97;
            this.d = i;
            o97.a.a(i, i2, o97.size());
            this.b = i2 - this.d;
        }

        @DexIgnore
        @Override // com.fossil.l97
        public int a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.o97, java.util.List
        public E get(int i) {
            o97.a.a(i, this.b);
            return this.c.get(this.d + i);
        }
    }

    @DexIgnore
    @Override // java.util.List
    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        return a.a(this, (Collection) obj);
    }

    @DexIgnore
    @Override // java.util.List
    public abstract E get(int i);

    @DexIgnore
    public int hashCode() {
        return a.a(this);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int i = 0;
        for (E e : this) {
            if (ee7.a((Object) e, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        ListIterator<E> listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (ee7.a((Object) listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator() {
        return new c(0);
    }

    @DexIgnore
    @Override // java.util.List
    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public List<E> subList(int i, int i2) {
        return new d(this, i, i2);
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator(int i) {
        return new c(i);
    }
}
