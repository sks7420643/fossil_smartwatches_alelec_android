package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u63 {
    @DexIgnore
    public /* final */ c73 a;

    @DexIgnore
    public u63(c73 c73) {
        this.a = c73;
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.setCompassEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(boolean z) {
        try {
            this.a.setMapToolbarEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.setMyLocationButtonEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        try {
            this.a.setRotateGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        try {
            this.a.setScrollGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        try {
            this.a.setTiltGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void g(boolean z) {
        try {
            this.a.setZoomControlsEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void h(boolean z) {
        try {
            this.a.setZoomGesturesEnabled(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean a() {
        try {
            return this.a.o();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean b() {
        try {
            return this.a.t();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean c() {
        try {
            return this.a.p();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.a.i();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean e() {
        try {
            return this.a.B();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean f() {
        try {
            return this.a.k();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean g() {
        try {
            return this.a.D();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean h() {
        try {
            return this.a.l();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
