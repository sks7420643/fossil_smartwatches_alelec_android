package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q30 {
    @DexIgnore
    void onDestroy();

    @DexIgnore
    void onStart();

    @DexIgnore
    void onStop();
}
