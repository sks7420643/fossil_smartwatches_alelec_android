package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz1 extends zy1 {
    @DexIgnore
    public /* final */ /* synthetic */ hz1 a;

    @DexIgnore
    public iz1(hz1 hz1) {
        this.a = hz1;
    }

    @DexIgnore
    @Override // com.fossil.zy1, com.fossil.nz1
    public final void b(Status status) throws RemoteException {
        this.a.a((i12) status);
    }
}
