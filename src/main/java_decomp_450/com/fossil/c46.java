package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c46 implements Factory<b46> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public c46(Provider<ch5> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static c46 a(Provider<ch5> provider, Provider<UserRepository> provider2) {
        return new c46(provider, provider2);
    }

    @DexIgnore
    public static b46 a(ch5 ch5, UserRepository userRepository) {
        return new b46(ch5, userRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public b46 get() {
        return a(this.a.get(), this.b.get());
    }
}
