package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g10<DataType> implements cx<DataType, BitmapDrawable> {
    @DexIgnore
    public /* final */ cx<DataType, Bitmap> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public g10(Resources resources, cx<DataType, Bitmap> cxVar) {
        u50.a(resources);
        this.b = resources;
        u50.a(cxVar);
        this.a = cxVar;
    }

    @DexIgnore
    @Override // com.fossil.cx
    public boolean a(DataType datatype, ax axVar) throws IOException {
        return this.a.a(datatype, axVar);
    }

    @DexIgnore
    @Override // com.fossil.cx
    public uy<BitmapDrawable> a(DataType datatype, int i, int i2, ax axVar) throws IOException {
        return a20.a(this.b, this.a.a(datatype, i, i2, axVar));
    }
}
