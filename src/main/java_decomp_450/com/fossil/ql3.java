package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ql3 implements Runnable {
    @DexIgnore
    public /* final */ nl3 a;

    @DexIgnore
    public ql3(nl3 nl3) {
        this.a = nl3;
    }

    @DexIgnore
    public final void run() {
        nl3 nl3 = this.a;
        ol3 ol3 = nl3.c;
        long j = nl3.a;
        long j2 = nl3.b;
        ol3.b.g();
        ol3.b.e().A().a("Application going to the background");
        boolean z = true;
        if (ol3.b.l().a(wb3.D0)) {
            ol3.b.k().w.a(true);
        }
        Bundle bundle = new Bundle();
        if (!ol3.b.l().r().booleanValue()) {
            ol3.b.e.b(j2);
            if (ol3.b.l().a(wb3.s0)) {
                bundle.putLong("_et", ol3.b.a(j2));
                zj3.a(ol3.b.r().a(true), bundle, true);
            } else {
                z = false;
            }
            ol3.b.a(false, z, j2);
        }
        ol3.b.o().a("auto", "_ab", j, bundle);
    }
}
