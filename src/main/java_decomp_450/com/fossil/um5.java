package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um5 extends fl4<b, d, c> {
    @DexIgnore
    public LocationSource d;
    @DexIgnore
    public PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public d(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation", f = "GetUserLocation.kt", l = {16}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ um5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(um5 um5, fb7 fb7) {
            super(fb7);
            this.this$0 = um5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public um5(LocationSource locationSource, PortfolioApp portfolioApp) {
        ee7.b(locationSource, "mLocationSource");
        ee7.b(portfolioApp, "mPortfolioApp");
        this.d = locationSource;
        this.e = portfolioApp;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetUserLocation";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.um5.b r11, com.fossil.fb7<java.lang.Object> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.um5.e
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.um5$e r0 = (com.fossil.um5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.um5$e r0 = new com.fossil.um5$e
            r0.<init>(r10, r12)
        L_0x0018:
            r7 = r0
            java.lang.Object r12 = r7.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r7.label
            r2 = 1
            if (r1 == 0) goto L_0x003a
            if (r1 != r2) goto L_0x0032
            java.lang.Object r11 = r7.L$1
            com.fossil.um5$b r11 = (com.fossil.um5.b) r11
            java.lang.Object r11 = r7.L$0
            com.fossil.um5 r11 = (com.fossil.um5) r11
            com.fossil.t87.a(r12)
            goto L_0x0063
        L_0x0032:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003a:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r1 = "GetUserLocation"
            java.lang.String r3 = "running UseCase"
            r12.d(r1, r3)
            com.portfolio.platform.data.LocationSource r1 = r10.d
            com.portfolio.platform.PortfolioApp r12 = r10.e
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r8 = 28
            r9 = 0
            r7.L$0 = r10
            r7.L$1 = r11
            r7.label = r2
            r2 = r12
            java.lang.Object r12 = com.portfolio.platform.data.LocationSource.getLocation$default(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            if (r12 != r0) goto L_0x0063
            return r0
        L_0x0063:
            com.portfolio.platform.data.LocationSource$Result r12 = (com.portfolio.platform.data.LocationSource.Result) r12
            com.portfolio.platform.data.LocationSource$ErrorState r11 = r12.getErrorState()
            com.portfolio.platform.data.LocationSource$ErrorState r0 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            if (r11 != r0) goto L_0x0083
            android.location.Location r11 = r12.getLocation()
            if (r11 == 0) goto L_0x0081
            com.fossil.um5$d r12 = new com.fossil.um5$d
            double r0 = r11.getLatitude()
            double r2 = r11.getLongitude()
            r12.<init>(r0, r2)
            goto L_0x0088
        L_0x0081:
            r12 = 0
            goto L_0x0088
        L_0x0083:
            com.fossil.um5$c r12 = new com.fossil.um5$c
            r12.<init>()
        L_0x0088:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.um5.a(com.fossil.um5$b, com.fossil.fb7):java.lang.Object");
    }
}
