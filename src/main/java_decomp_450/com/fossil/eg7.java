package com.fossil;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg7<T> implements hg7<T> {
    @DexIgnore
    public /* final */ AtomicReference<hg7<T>> a;

    @DexIgnore
    public eg7(hg7<? extends T> hg7) {
        ee7.b(hg7, "sequence");
        this.a = new AtomicReference<>(hg7);
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<T> iterator() {
        hg7<T> andSet = this.a.getAndSet(null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
