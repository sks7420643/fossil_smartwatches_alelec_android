package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.oy6;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sw6 {
    @DexIgnore
    public static /* final */ String a; // = "sw6";
    @DexIgnore
    public static /* final */ Bitmap.Config b; // = Bitmap.Config.ARGB_8888;

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, int i) {
        Bitmap a2 = a(bitmap, i);
        Bitmap a3 = a(bitmap2, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (a2 != null) {
            canvas.drawBitmap(a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a2.recycle();
        }
        if (a3 != null) {
            canvas.drawBitmap(a3, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a3.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap b(Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            bitmap2 = Bitmap.createBitmap(bitmap, (width / 2) - (height / 2), 0, height, height);
        } else {
            bitmap2 = Bitmap.createBitmap(bitmap, 0, (height / 2) - (width / 2), width, width);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap2.getWidth() + 4, bitmap2.getHeight() + 4, bitmap2.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(bitmap2, 2.0f, 2.0f, (Paint) null);
        bitmap2.recycle();
        return Bitmap.createScaledBitmap(createBitmap, i, i, false);
    }

    @DexIgnore
    public static Bitmap c(String str) {
        String[] split = str.split(" ");
        String str2 = split[0];
        String str3 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str3)) {
            str3 = str3.substring(0, 1);
        }
        int color = PortfolioApp.c0.getResources().getColor(2131099827);
        int color2 = PortfolioApp.c0.getResources().getColor(2131100361);
        oy6.c c = oy6.a().c();
        c.a(200);
        c.c(200);
        c.b(a(30));
        c.d(color2);
        oy6.d a2 = c.a();
        return a(a2.a(str2 + str3, color));
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, int i) {
        Bitmap a2 = a(bitmap, i);
        Bitmap b2 = b(bitmap2, i);
        Bitmap b3 = b(bitmap3, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (a2 != null) {
            canvas.drawBitmap(a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a2.recycle();
        }
        if (b2 != null) {
            canvas.drawBitmap(b2, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b2.recycle();
        }
        if (b3 != null) {
            float f = (float) i;
            canvas.drawBitmap(b3, f, f, (Paint) null);
            b3.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap b(String str) {
        int i;
        int i2;
        String b2 = eh5.l.a().b("nonBrandPlaceholderText");
        if (!TextUtils.isEmpty(b2)) {
            i = Color.parseColor(b2);
        } else {
            i = PortfolioApp.c0.getResources().getColor(2131099827);
        }
        Typeface c = eh5.l.a().c("nonBrandTextStyle");
        String b3 = eh5.l.a().b("nonBrandSurface");
        if (!TextUtils.isEmpty(b3)) {
            i2 = Color.parseColor(b3);
        } else {
            i2 = PortfolioApp.c0.getResources().getColor(2131100361);
        }
        oy6.c c2 = oy6.a().c();
        c2.a(50);
        c2.c(50);
        c2.b(25);
        c2.d(i2);
        c2.a(c);
        return a(c2.a().a(str, i));
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4, int i) {
        Bitmap b2 = b(bitmap, i);
        Bitmap b3 = b(bitmap2, i);
        Bitmap b4 = b(bitmap3, i);
        Bitmap b5 = b(bitmap4, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (b2 != null) {
            canvas.drawBitmap(b2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b2.recycle();
        }
        if (b3 != null) {
            canvas.drawBitmap(b3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i, (Paint) null);
            b3.recycle();
        }
        if (b4 != null) {
            canvas.drawBitmap(b4, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b4.recycle();
        }
        if (b5 != null) {
            float f = (float) i;
            canvas.drawBitmap(b5, f, f, (Paint) null);
            b5.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i2 = width * 2;
        if (height > i2) {
            bitmap2 = Bitmap.createBitmap(bitmap, 0, (height / 2) - width, width, i2);
        } else {
            bitmap2 = Bitmap.createBitmap(bitmap, (width / 2) - (height / 4), 0, height / 2, height);
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap2, i, i * 2, false);
        bitmap2.recycle();
        Bitmap createBitmap = Bitmap.createBitmap(createScaledBitmap.getWidth() + 4, createScaledBitmap.getHeight() + 4, createScaledBitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(createScaledBitmap, 2.0f, 2.0f, (Paint) null);
        createScaledBitmap.recycle();
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap a(Drawable drawable) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        try {
            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(2, 2, b);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), b);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static Bitmap a(Contact contact) {
        int i;
        int i2;
        String[] split = contact.getDisplayName().split(" ");
        String str = split[0];
        String str2 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str)) {
            str = str.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        String b2 = eh5.l.a().b("primaryText");
        if (!TextUtils.isEmpty(b2)) {
            i = Color.parseColor(b2);
        } else {
            i = PortfolioApp.c0.getResources().getColor(2131099971);
        }
        Typeface c = eh5.l.a().c("nonBrandTextStyle");
        String b3 = eh5.l.a().b("nonBrandSurface");
        if (!TextUtils.isEmpty(b3)) {
            i2 = Color.parseColor(b3);
        } else {
            i2 = PortfolioApp.c0.getResources().getColor(2131100361);
        }
        oy6.c c2 = oy6.a().c();
        c2.a(200);
        c2.c(200);
        c2.b(a(25));
        c2.d(i2);
        c2.a(c);
        oy6.d a2 = c2.a();
        return a(a2.a(str + str2, i));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c A[SYNTHETIC, Splitter:B:30:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.Long r7) {
        /*
            com.fossil.px6$a r0 = com.fossil.px6.a
            com.portfolio.platform.PortfolioApp r1 = com.portfolio.platform.PortfolioApp.c0
            java.lang.String r2 = "android.permission.READ_CONTACTS"
            java.lang.String r3 = "android.permission.READ_PHONE_STATE"
            java.lang.String r4 = "android.permission.READ_CALL_LOG"
            java.lang.String r5 = "android.permission.READ_SMS"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3, r4, r5}
            boolean r0 = r0.a(r1, r2)
            r1 = 0
            if (r0 != 0) goto L_0x0025
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = com.fossil.sw6.a
            java.lang.String r2 = "getContactPhotoById does not enough permissions"
            r7.d(r0, r2)
            return r1
        L_0x0025:
            com.portfolio.platform.PortfolioApp r0 = com.portfolio.platform.PortfolioApp.c0     // Catch:{ Exception -> 0x0059 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0059 }
            android.net.Uri r2 = android.provider.ContactsContract.Contacts.CONTENT_URI     // Catch:{ Exception -> 0x0059 }
            long r3 = r7.longValue()     // Catch:{ Exception -> 0x0059 }
            android.net.Uri r7 = android.content.ContentUris.withAppendedId(r2, r3)     // Catch:{ Exception -> 0x0059 }
            r2 = 1
            java.io.InputStream r7 = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(r0, r7, r2)     // Catch:{ Exception -> 0x0059 }
            if (r7 == 0) goto L_0x004c
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r7)     // Catch:{ Exception -> 0x0047, all -> 0x0044 }
            r7.close()     // Catch:{ Exception -> 0x0047, all -> 0x0044 }
            goto L_0x004c
        L_0x0044:
            r0 = move-exception
            r1 = r7
            goto L_0x006a
        L_0x0047:
            r0 = move-exception
            r6 = r1
            r1 = r7
            r7 = r6
            goto L_0x005b
        L_0x004c:
            if (r7 == 0) goto L_0x0069
            r7.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x0069
        L_0x0052:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x0069
        L_0x0057:
            r0 = move-exception
            goto L_0x006a
        L_0x0059:
            r0 = move-exception
            r7 = r1
        L_0x005b:
            r0.printStackTrace()     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x0064 }
            goto L_0x0068
        L_0x0064:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0068:
            r1 = r7
        L_0x0069:
            return r1
        L_0x006a:
            if (r1 == 0) goto L_0x0074
            r1.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x0074
        L_0x0070:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0074:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sw6.a(java.lang.Long):android.graphics.Bitmap");
    }

    @DexIgnore
    public static int a(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, PortfolioApp.c0.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public static String a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        try {
            String str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 2), "UTF-8");
            try {
                byteArrayOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                byteArrayOutputStream.close();
                return "";
            } catch (Exception e3) {
                e3.printStackTrace();
                return "";
            }
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap a(String str) {
        try {
            byte[] decode = Base64.decode(str, 0);
            return BitmapFactory.decodeByteArray(decode, 0, decode.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static Bitmap a(Resources resources, Drawable drawable) {
        Bitmap bitmap;
        int i;
        if (drawable instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        } else {
            Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            bitmap = createBitmap;
        }
        int dimensionPixelSize = resources.getDimensionPixelSize(2131165417);
        if (bitmap.getHeight() <= dimensionPixelSize && bitmap.getWidth() <= dimensionPixelSize) {
            return bitmap;
        }
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > width) {
            i = dimensionPixelSize;
            dimensionPixelSize = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
        } else {
            i = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
        }
        return Bitmap.createScaledBitmap(bitmap, dimensionPixelSize, i, false);
    }
}
