package com.fossil;

import android.content.Context;
import com.fossil.c24;
import com.fossil.l94;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k94 implements l94 {
    @DexIgnore
    public m94 a;

    @DexIgnore
    public k94(Context context) {
        this.a = m94.a(context);
    }

    @DexIgnore
    @Override // com.fossil.l94
    public l94.a a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean a2 = this.a.a(str, currentTimeMillis);
        boolean a3 = this.a.a(currentTimeMillis);
        if (a2 && a3) {
            return l94.a.COMBINED;
        }
        if (a3) {
            return l94.a.GLOBAL;
        }
        if (a2) {
            return l94.a.SDK;
        }
        return l94.a.NONE;
    }

    @DexIgnore
    public static c24<l94> a() {
        c24.b a2 = c24.a(l94.class);
        a2.a(m24.b(Context.class));
        a2.a(j94.a());
        return a2.b();
    }

    @DexIgnore
    public static /* synthetic */ l94 a(d24 d24) {
        return new k94((Context) d24.get(Context.class));
    }
}
