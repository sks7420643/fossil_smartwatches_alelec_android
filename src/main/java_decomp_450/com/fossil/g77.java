package com.fossil;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g77 implements ErrorResponse {
    @DexIgnore
    public Throwable a;
    @DexIgnore
    public fv7 b;

    @DexIgnore
    public g77(Throwable th) {
        this.a = th;
    }

    @DexIgnore
    public static g77 a(Throwable th) {
        return new g77(th);
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public String b() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        fv7 fv7 = this.b;
        if (fv7 != null) {
            if (k77.b(fv7.e())) {
                sb.append(this.b.e());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public boolean c() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }

    @DexIgnore
    public static g77 a(fv7 fv7) {
        return new g77(fv7);
    }

    @DexIgnore
    public g77(fv7 fv7) {
        this.b = fv7;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public int a() {
        fv7 fv7 = this.b;
        if (fv7 != null) {
            return fv7.b();
        }
        return -1;
    }
}
