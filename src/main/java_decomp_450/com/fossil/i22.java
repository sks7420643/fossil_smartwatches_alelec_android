package com.fossil;

import com.fossil.c12;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i22 implements c12.a {
    @DexIgnore
    public /* final */ /* synthetic */ BasePendingResult a;
    @DexIgnore
    public /* final */ /* synthetic */ j22 b;

    @DexIgnore
    public i22(j22 j22, BasePendingResult basePendingResult) {
        this.b = j22;
        this.a = basePendingResult;
    }

    @DexIgnore
    @Override // com.fossil.c12.a
    public final void a(Status status) {
        this.b.a.remove(this.a);
    }
}
