package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm2 implements Parcelable.Creator<mm2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ mm2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        w53 w53 = mm2.e;
        List<i62> list = mm2.d;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                w53 = (w53) j72.a(parcel, a, w53.CREATOR);
            } else if (a2 == 2) {
                list = j72.c(parcel, a, i62.CREATOR);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                str = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new mm2(w53, list, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ mm2[] newArray(int i) {
        return new mm2[i];
    }
}
