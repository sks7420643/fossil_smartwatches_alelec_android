package com.fossil;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn7 {
    @DexIgnore
    public static /* final */ sn7 c; // = new a().a();
    @DexIgnore
    public /* final */ Set<b> a;
    @DexIgnore
    public /* final */ qq7 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<b> a; // = new ArrayList();

        @DexIgnore
        public sn7 a() {
            return new sn7(new LinkedHashSet(this.a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ br7 d;

        @DexIgnore
        public boolean a(String str) {
            if (!this.a.startsWith("*.")) {
                return str.equals(this.b);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 == this.b.length()) {
                String str2 = this.b;
                if (str.regionMatches(false, indexOf + 1, str2, 0, str2.length())) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.a.equals(bVar.a) && this.c.equals(bVar.c) && this.d.equals(bVar.d);
            }
        }

        @DexIgnore
        public int hashCode() {
            return ((((527 + this.a.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.c + this.d.base64();
        }
    }

    @DexIgnore
    public sn7(Set<b> set, qq7 qq7) {
        this.a = set;
        this.b = qq7;
    }

    @DexIgnore
    public static br7 b(X509Certificate x509Certificate) {
        return br7.of(x509Certificate.getPublicKey().getEncoded()).sha256();
    }

    @DexIgnore
    public void a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<b> a2 = a(str);
        if (!a2.isEmpty()) {
            qq7 qq7 = this.b;
            if (qq7 != null) {
                list = qq7.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a2.size();
                br7 br7 = null;
                br7 br72 = null;
                for (int i2 = 0; i2 < size2; i2++) {
                    b bVar = a2.get(i2);
                    if (bVar.c.equals("sha256/")) {
                        if (br7 == null) {
                            br7 = b(x509Certificate);
                        }
                        if (bVar.d.equals(br7)) {
                            return;
                        }
                    } else if (bVar.c.equals("sha1/")) {
                        if (br72 == null) {
                            br72 = a(x509Certificate);
                        }
                        if (bVar.d.equals(br72)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + bVar.c);
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                sb.append("\n    ");
                sb.append(a((Certificate) x509Certificate2));
                sb.append(": ");
                sb.append(x509Certificate2.getSubjectDN().getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            int size4 = a2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                sb.append("\n    ");
                sb.append(a2.get(i4));
            }
            throw new SSLPeerUnverifiedException(sb.toString());
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof sn7) {
            sn7 sn7 = (sn7) obj;
            if (!ro7.a(this.b, sn7.b) || !this.a.equals(sn7.a)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        qq7 qq7 = this.b;
        return ((qq7 != null ? qq7.hashCode() : 0) * 31) + this.a.hashCode();
    }

    @DexIgnore
    public List<b> a(String str) {
        List<b> emptyList = Collections.emptyList();
        for (b bVar : this.a) {
            if (bVar.a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(bVar);
            }
        }
        return emptyList;
    }

    @DexIgnore
    public sn7 a(qq7 qq7) {
        return ro7.a(this.b, qq7) ? this : new sn7(this.a, qq7);
    }

    @DexIgnore
    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + b((X509Certificate) certificate).base64();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    @DexIgnore
    public static br7 a(X509Certificate x509Certificate) {
        return br7.of(x509Certificate.getPublicKey().getEncoded()).sha1();
    }
}
