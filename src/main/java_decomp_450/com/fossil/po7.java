package com.fossil;

import com.fossil.fo7;
import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class po7 {
    @DexIgnore
    public static po7 a;

    @DexIgnore
    public abstract int a(Response.a aVar);

    @DexIgnore
    public abstract bp7 a(vn7 vn7, nn7 nn7, fp7 fp7, no7 no7);

    @DexIgnore
    public abstract cp7 a(vn7 vn7);

    @DexIgnore
    public abstract IOException a(qn7 qn7, IOException iOException);

    @DexIgnore
    public abstract Socket a(vn7 vn7, nn7 nn7, fp7 fp7);

    @DexIgnore
    public abstract void a(fo7.a aVar, String str);

    @DexIgnore
    public abstract void a(fo7.a aVar, String str, String str2);

    @DexIgnore
    public abstract void a(wn7 wn7, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract boolean a(nn7 nn7, nn7 nn72);

    @DexIgnore
    public abstract boolean a(vn7 vn7, bp7 bp7);

    @DexIgnore
    public abstract void b(vn7 vn7, bp7 bp7);
}
