package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gb extends eb {
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public LayoutInflater p;

    @DexIgnore
    @Deprecated
    public gb(Context context, int i2, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.j = i2;
        this.i = i2;
        this.p = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @DexIgnore
    @Override // com.fossil.eb
    public View a(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.p.inflate(this.j, viewGroup, false);
    }

    @DexIgnore
    @Override // com.fossil.eb
    public View b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.p.inflate(this.i, viewGroup, false);
    }
}
