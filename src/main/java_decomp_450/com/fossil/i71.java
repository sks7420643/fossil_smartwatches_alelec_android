package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i71 implements Parcelable.Creator<e91> {
    @DexIgnore
    public /* synthetic */ i71(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public e91 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            di0 valueOf = di0.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                ee7.a((Object) readString2, "parcel.readString()!!");
                ei1 valueOf2 = ei1.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    ee7.a((Object) readString3, "parcel.readString()!!");
                    ak0 valueOf3 = ak0.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        ee7.a((Object) readString4, "parcel.readString()!!");
                        return new e91(valueOf, valueOf2, valueOf3, xl0.valueOf(readString4), (short) parcel.readInt());
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public e91[] newArray(int i) {
        return new e91[i];
    }
}
