package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hf5 {
    @DexIgnore
    public /* final */ Map<String, String> a; // = new HashMap();
    @DexIgnore
    public /* final */ qd5 b;

    @DexIgnore
    public hf5(qd5 qd5) {
        ee7.b(qd5, "mAnalyticsInstance");
        this.b = qd5;
    }

    @DexIgnore
    public final hf5 a(String str, String str2) {
        ee7.b(str, "propertyName");
        ee7.b(str2, "propertyValue");
        this.a.put(str, str2);
        return this;
    }

    @DexIgnore
    public final void a() {
        this.b.a(this.a);
    }
}
