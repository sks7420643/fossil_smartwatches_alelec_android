package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be5 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static float[] e;
    @DexIgnore
    public static /* final */ String[] f; // = {"HW.0.0", "HL.0.0", "HM.0.0", "DN.0.0", "DN.1.0"};
    @DexIgnore
    public static be5 g;
    @DexIgnore
    public static /* final */ MFDeviceFamily[] h; // = {MFDeviceFamily.DEVICE_FAMILY_SAM, MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM, MFDeviceFamily.DEVICE_FAMILY_SAM_MINI, MFDeviceFamily.DEVICE_FAMILY_RMM};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] i; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] j; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] k; // = {FossilDeviceSerialPatternUtil.DEVICE.RMM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] l; // = {FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] m; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] n; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.DIANA, FossilDeviceSerialPatternUtil.DEVICE.IVY};
    @DexIgnore
    public static /* final */ a o; // = new a(null);
    @DexIgnore
    public ch5 a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public List<String> c; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            if (i < 25) {
                return 2131231004;
            }
            if (i < 50) {
                return 2131231006;
            }
            return i < 75 ? 2131231008 : 2131231002;
        }

        @DexIgnore
        public final void a(float[] fArr) {
            be5.e = fArr;
        }

        @DexIgnore
        public final String[] b() {
            return be5.f;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] c() {
            return be5.l;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] d() {
            return be5.m;
        }

        @DexIgnore
        public final synchronized be5 e() {
            be5 f;
            if (be5.o.f() == null) {
                be5.o.a(new be5());
            }
            f = be5.o.f();
            if (f == null) {
                ee7.a();
                throw null;
            }
            return f;
        }

        @DexIgnore
        public final be5 f() {
            return be5.g;
        }

        @DexIgnore
        public final float[] g() {
            return be5.e;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] h() {
            return be5.n;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] i() {
            return be5.i;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] j() {
            return be5.k;
        }

        @DexIgnore
        public final void k() {
            a(new float[71]);
            int i = 0;
            while (i <= 20) {
                float[] g = g();
                if (g != null) {
                    g[i] = 0.005f * ((float) i) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            while (i <= 35) {
                float[] g2 = g();
                if (g2 != null) {
                    g2[i] = ((((float) (i - 20)) * 0.06f) + 0.1f) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            while (i <= 50) {
                float[] g3 = g();
                if (g3 != null) {
                    g3[i] = ((((float) (i - 35)) * 0.06666667f) + 1.0f) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            while (i <= 60) {
                float[] g4 = g();
                if (g4 != null) {
                    g4[i] = ((((float) (i - 50)) * 0.1f) + 2.0f) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            while (i <= 63) {
                float[] g5 = g();
                if (g5 != null) {
                    g5[i] = ((((float) (i - 60)) * 0.33333334f) + 3.0f) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
            while (i <= 70) {
                float[] g6 = g();
                if (g6 != null) {
                    g6[i] = ((((float) (i - 60)) * 0.14285715f) + 4.0f) * 3.28f;
                    i++;
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public final boolean l() {
            return Build.VERSION.SDK_INT >= 29;
        }

        @DexIgnore
        public final boolean m() {
            return Build.VERSION.SDK_INT >= 26;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(be5 be5) {
            be5.g = be5;
        }

        @DexIgnore
        public final float b(int i) {
            if (g() == null) {
                k();
            }
            int i2 = (-i) - 30;
            if (i2 >= 0) {
                float[] g = g();
                if (g == null) {
                    ee7.a();
                    throw null;
                } else if (i2 < g.length) {
                    float[] g2 = g();
                    if (g2 != null) {
                        return g2[i2];
                    }
                    ee7.a();
                    throw null;
                }
            }
            if (i2 < 0) {
                return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            return 16.4f;
        }

        @DexIgnore
        public final List<String> c(String str) {
            ee7.b(str, "serial");
            ArrayList arrayList = new ArrayList();
            FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            if (deviceBySerial != null) {
                int i = ae5.b[deviceBySerial.ordinal()];
                if (i == 1) {
                    arrayList.add("HW.0.0");
                } else if (i == 2) {
                    arrayList.add("HM.0.0");
                } else if (i == 3) {
                    arrayList.add("HL.0.0");
                } else if (i == 4) {
                    arrayList.add("DN.0.0");
                    arrayList.add("DN.1.0");
                } else if (i == 5) {
                    arrayList.add("IV.0.0");
                }
            }
            return arrayList;
        }

        @DexIgnore
        public final boolean d(String str) {
            ee7.b(str, "serial");
            return a(str, i());
        }

        @DexIgnore
        public final boolean f(String str) {
            ee7.b(str, "serial");
            return a(str, c());
        }

        @DexIgnore
        public final boolean g(String str) {
            ee7.b(str, "serial");
            return a(str, d());
        }

        @DexIgnore
        public final boolean h(String str) {
            ee7.b(str, "serial");
            return a(str, j());
        }

        @DexIgnore
        public final MFDeviceFamily[] a() {
            return be5.h;
        }

        @DexIgnore
        public final MFDeviceFamily a(String str) {
            ee7.b(str, "serial");
            MFDeviceFamily deviceFamily = DeviceIdentityUtils.getDeviceFamily(str);
            ee7.a((Object) deviceFamily, "DeviceIdentityUtils.getDeviceFamily(serial)");
            return deviceFamily;
        }

        @DexIgnore
        public final boolean e(String str) {
            ee7.b(str, "serial");
            return a(str, h());
        }

        @DexIgnore
        public final boolean a(String str, FossilDeviceSerialPatternUtil.DEVICE[] deviceArr) {
            ee7.b(str, "serial");
            ee7.b(deviceArr, "supportedDevices");
            FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            for (FossilDeviceSerialPatternUtil.DEVICE device : deviceArr) {
                if (device == deviceBySerial) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final boolean a(FossilDeviceSerialPatternUtil.DEVICE device) {
            return device == FossilDeviceSerialPatternUtil.DEVICE.DIANA || device == FossilDeviceSerialPatternUtil.DEVICE.IVY;
        }

        @DexIgnore
        public final int b(String str, b bVar) {
            ee7.b(str, "serial");
            ee7.b(bVar, AnalyticsEvents.PARAMETER_LIKE_VIEW_STYLE);
            return a(str, bVar);
        }

        @DexIgnore
        public final int a(String str, b bVar) {
            if (!e(str)) {
                return 2131231316;
            }
            switch (ae5.a[bVar.ordinal()]) {
                case 1:
                    return 2131231318;
                case 2:
                    return 2131231317;
                case 3:
                case 4:
                    return 2131231316;
                case 5:
                    return 2131230969;
                case 6:
                    return 2131230970;
                case 7:
                    return 2131230971;
                case 8:
                    return 2131230951;
                case 9:
                    return 2131230952;
                default:
                    throw new p87();
            }
        }

        @DexIgnore
        public final String b(String str) {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            if (str == null) {
                ee7.a();
                throw null;
            } else if (str.length() < 5) {
                return "";
            } else {
                if (FossilDeviceSerialPatternUtil.isQMotion(str)) {
                    String substring = str.substring(0, 5);
                    ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    return substring;
                }
                String substring2 = str.substring(0, 6);
                ee7.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                return substring2;
            }
        }
    }

    @DexIgnore
    public enum b {
        SMALL(0),
        NORMAL(1),
        LARGE(2),
        HYBRID_WATCH_HOUR(3),
        HYBRID_WATCH_MINUTE(4),
        HYBRID_WATCH_SUBEYE(5),
        DIANA_WATCH_HOUR(6),
        DIANA_WATCH_MINUTE(7),
        WATCH_COMPLETED(8);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public b(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    /*
    static {
        String simpleName = be5.class.getSimpleName();
        ee7.a((Object) simpleName, "DeviceHelper::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public be5() {
        PortfolioApp.g0.c().f().a(this);
        a();
    }

    @DexIgnore
    public final boolean b(String str) {
        ee7.b(str, "serial");
        return o.a(str, j);
    }

    @DexIgnore
    public final void a() {
        this.c.clear();
        if (!mh7.b("release", "release", true)) {
            ch5 ch5 = this.a;
            if (ch5 == null) {
                ee7.d("sharedPreferencesManager");
                throw null;
            } else if (ch5.Z() || ee7.a((Object) PortfolioApp.g0.c().h(), (Object) eb5.PORTFOLIO.getName())) {
                this.c.add(DeviceIdentityUtils.RAY_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX);
                List<String> list = this.c;
                String[] strArr = DeviceIdentityUtils.Q_MOTION_PREFIX;
                List asList = Arrays.asList((String[]) Arrays.copyOf(strArr, strArr.length));
                ee7.a((Object) asList, "Arrays.asList(*DeviceIde\u2026ityUtils.Q_MOTION_PREFIX)");
                list.addAll(asList);
                this.c.add(DeviceIdentityUtils.RMM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FAKE_SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SLIM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_DIANA_SERIAL_NUMBER_PREFIX);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        StringBuilder sb = new StringBuilder();
        sb.append("debug=");
        sb.append(PortfolioApp.g0.e());
        sb.append(", BUILD_TYPE=");
        sb.append("release");
        sb.append(", filterList=");
        Object[] array = this.c.toArray(new String[0]);
        if (array != null) {
            sb.append(Arrays.toString(array));
            local.d(str, sb.toString());
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0047, code lost:
        if (r0.Z() == false) goto L_0x0050;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r10, java.util.List<com.portfolio.platform.data.model.SKUModel> r11) {
        /*
            r9 = this;
            java.lang.String r0 = "serial"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = "allSkuModel"
            com.fossil.ee7.b(r11, r0)
            com.fossil.be5$a r0 = com.fossil.be5.o
            boolean r0 = r0.e(r10)
            r1 = 0
            if (r0 != 0) goto L_0x0035
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r0 = com.fossil.be5.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "device "
            r2.append(r3)
            r2.append(r10)
            java.lang.String r10 = " is not supported"
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r11.d(r0, r10)
            return r1
        L_0x0035:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            boolean r0 = r0.e()
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0050
            com.fossil.ch5 r0 = r9.a
            if (r0 == 0) goto L_0x004a
            boolean r0 = r0.Z()
            if (r0 != 0) goto L_0x0066
            goto L_0x0050
        L_0x004a:
            java.lang.String r10 = "sharedPreferencesManager"
            com.fossil.ee7.d(r10)
            throw r2
        L_0x0050:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.h()
            com.fossil.eb5 r4 = com.fossil.eb5.PORTFOLIO
            java.lang.String r4 = r4.getName()
            boolean r0 = com.fossil.ee7.a(r0, r4)
            if (r0 == 0) goto L_0x0067
        L_0x0066:
            return r3
        L_0x0067:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r4 = com.fossil.be5.d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "prefix "
            r5.append(r6)
            com.fossil.be5$a r6 = com.fossil.be5.o
            java.lang.String r6 = r6.b(r10)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r0.d(r4, r5)
            java.util.Iterator r11 = r11.iterator()
        L_0x008d:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x00cf
            java.lang.Object r0 = r11.next()
            r4 = r0
            com.portfolio.platform.data.model.SKUModel r4 = (com.portfolio.platform.data.model.SKUModel) r4
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.be5.d
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "compare sku "
            r7.append(r8)
            r7.append(r4)
            java.lang.String r8 = " to "
            r7.append(r8)
            r7.append(r10)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            com.fossil.be5$a r5 = com.fossil.be5.o
            java.lang.String r5 = r5.b(r10)
            java.lang.String r4 = r4.getSerialNumberPrefix()
            boolean r4 = com.fossil.ee7.a(r5, r4)
            if (r4 == 0) goto L_0x008d
            r2 = r0
        L_0x00cf:
            com.portfolio.platform.data.model.SKUModel r2 = (com.portfolio.platform.data.model.SKUModel) r2
            if (r2 == 0) goto L_0x00d4
            r1 = 1
        L_0x00d4:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.be5.a(java.lang.String, java.util.List):boolean");
    }

    @DexIgnore
    public final MisfitDeviceProfile a(String str) {
        ee7.b(str, "serial");
        IButtonConnectivity b2 = PortfolioApp.g0.b();
        MisfitDeviceProfile misfitDeviceProfile = null;
        if (b2 == null) {
            return null;
        }
        try {
            MisfitDeviceProfile deviceProfile = b2.getDeviceProfile(str);
            if (deviceProfile == null) {
                try {
                    DeviceRepository deviceRepository = this.b;
                    if (deviceRepository != null) {
                        Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
                        if (deviceBySerial != null) {
                            String macAddress = deviceBySerial.getMacAddress();
                            if (macAddress != null) {
                                String productDisplayName = deviceBySerial.getProductDisplayName();
                                if (productDisplayName != null) {
                                    String deviceId = deviceBySerial.getDeviceId();
                                    String sku = deviceBySerial.getSku();
                                    if (sku != null) {
                                        String firmwareRevision = deviceBySerial.getFirmwareRevision();
                                        if (firmwareRevision != null) {
                                            return new MisfitDeviceProfile(macAddress, productDisplayName, deviceId, sku, firmwareRevision, deviceBySerial.getBatteryLevel(), "", 0, 0, (short) deviceBySerial.getMajor(), (short) deviceBySerial.getMinor(), "", null);
                                        }
                                        ee7.a();
                                        throw null;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                                ee7.a();
                                throw null;
                            }
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.d("mDeviceRepository");
                        throw null;
                    }
                } catch (Exception e2) {
                    e = e2;
                    misfitDeviceProfile = deviceProfile;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = d;
                    local.e(str2, "getDeviceProfileFromSerial exception=" + e);
                    return misfitDeviceProfile;
                }
            }
            return deviceProfile;
        } catch (Exception e3) {
            e = e3;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str22 = d;
            local2.e(str22, "getDeviceProfileFromSerial exception=" + e);
            return misfitDeviceProfile;
        }
    }
}
