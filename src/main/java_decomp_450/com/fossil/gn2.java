package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn2 extends tm2 implements en2 {
    @DexIgnore
    public gn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final int a() throws RemoteException {
        Parcel a = a(17, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void b(String str) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        b(7, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void f() throws RemoteException {
        b(11, zza());
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final String getId() throws RemoteException {
        Parcel a = a(2, zza());
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final LatLng getPosition() throws RemoteException {
        Parcel a = a(4, zza());
        LatLng latLng = (LatLng) xm2.a(a, LatLng.CREATOR);
        a.recycle();
        return latLng;
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void r() throws RemoteException {
        b(12, zza());
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void remove() throws RemoteException {
        b(1, zza());
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setAlpha(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(25, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setAnchor(float f, float f2) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        zza.writeFloat(f2);
        b(19, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setDraggable(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(9, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setFlat(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(20, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setInfoWindowAnchor(float f, float f2) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        zza.writeFloat(f2);
        b(24, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setPosition(LatLng latLng) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLng);
        b(3, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setRotation(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(22, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setTitle(String str) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        b(5, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(14, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void setZIndex(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(27, zza);
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final boolean v() throws RemoteException {
        Parcel a = a(13, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final boolean b(en2 en2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, en2);
        Parcel a = a(16, zza);
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.en2
    public final void b(ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        b(18, zza);
    }
}
