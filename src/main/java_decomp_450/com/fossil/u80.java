package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final u80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new u80(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", ", "require: 2"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public u80 createFromParcel(Parcel parcel) {
            return new u80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public u80[] newArray(int i) {
            return new u80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public u80 m77createFromParcel(Parcel parcel) {
            return new u80(parcel, null);
        }
    }

    @DexIgnore
    public u80(short s) {
        super(o80.SECOND_TIMEZONE_OFFSET);
        this.b = s;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        short s = this.b;
        if (!(s == 1024 || (-720 <= s && 840 >= s))) {
            StringBuilder b2 = yh0.b("secondTimezoneOffsetInMinute (");
            b2.append((int) this.b);
            b2.append(") ");
            b2.append(" must be equal to 1024 ");
            b2.append(" or in range ");
            b2.append("[-720, ");
            b2.append("840].");
            throw new IllegalArgumentException(b2.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(u80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((u80) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig");
    }

    @DexIgnore
    public final short getSecondTimezoneOffsetInMinute() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return (super.hashCode() * 31) + this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.b));
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.u2, Short.valueOf(this.b));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ u80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = (short) parcel.readInt();
        e();
    }
}
