package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb7 {
    @DexIgnore
    public static final <T> void a(gd7<? super fb7<? super T>, ? extends Object> gd7, fb7<? super T> fb7) {
        ee7.b(gd7, "$this$startCoroutine");
        ee7.b(fb7, "completion");
        fb7 a = mb7.a(mb7.a(gd7, fb7));
        i97 i97 = i97.a;
        s87.a aVar = s87.Companion;
        a.resumeWith(s87.m60constructorimpl(i97));
    }

    @DexIgnore
    public static final <R, T> void a(kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7, R r, fb7<? super T> fb7) {
        ee7.b(kd7, "$this$startCoroutine");
        ee7.b(fb7, "completion");
        fb7 a = mb7.a(mb7.a(kd7, r, fb7));
        i97 i97 = i97.a;
        s87.a aVar = s87.Companion;
        a.resumeWith(s87.m60constructorimpl(i97));
    }
}
