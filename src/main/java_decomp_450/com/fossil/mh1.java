package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh1 extends zk0 {
    @DexIgnore
    public h41 C;
    @DexIgnore
    public /* final */ ArrayList<ul0> D; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ x91 E;

    @DexIgnore
    public mh1(ri1 ri1, en0 en0, x91 x91, String str) {
        super(ri1, en0, wm0.j, str, false, 16);
        this.E = x91;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        h41 h41 = this.C;
        return h41 != null ? h41 : new h41(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new z81(((zk0) this).w), new ia1(this), new cc1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.k4, this.E.a());
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.l4;
        h41 h41 = this.C;
        return yz0.a(k, r51, h41 != null ? h41.a() : null);
    }

    @DexIgnore
    public final h41 m() {
        return this.C;
    }
}
