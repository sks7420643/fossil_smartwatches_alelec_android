package com.fossil;

import com.fossil.fitness.WorkoutState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class w91 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[pb1.values().length];
        a = iArr;
        iArr[pb1.HARDWARE_LOG.ordinal()] = 1;
        a[pb1.ALARM.ordinal()] = 2;
        a[pb1.ACTIVITY_FILE.ordinal()] = 3;
        a[pb1.NOTIFICATION_FILTER.ordinal()] = 4;
        a[pb1.DEVICE_CONFIG.ordinal()] = 5;
        a[pb1.DATA_COLLECTION_FILE.ordinal()] = 6;
        a[pb1.OTA.ordinal()] = 7;
        a[pb1.FONT.ordinal()] = 8;
        a[pb1.MUSIC_CONTROL.ordinal()] = 9;
        a[pb1.UI_SCRIPT.ordinal()] = 10;
        a[pb1.ASSET.ordinal()] = 11;
        a[pb1.NOTIFICATION.ordinal()] = 12;
        a[pb1.DEVICE_INFO.ordinal()] = 13;
        a[pb1.ALL_FILE.ordinal()] = 14;
        a[pb1.MICRO_APP.ordinal()] = 15;
        a[pb1.DEPRECATED_UI_PACKAGE_FILE.ordinal()] = 16;
        a[pb1.WATCH_PARAMETERS_FILE.ordinal()] = 17;
        a[pb1.UI_PACKAGE_FILE.ordinal()] = 18;
        a[pb1.LUTS_FILE.ordinal()] = 19;
        a[pb1.RATE_FILE.ordinal()] = 20;
        a[pb1.REPLY_MESSAGES_FILE.ordinal()] = 21;
        a[pb1.UI_ENCRYPTED_FILE.ordinal()] = 22;
        int[] iArr2 = new int[qg0.values().length];
        b = iArr2;
        iArr2[qg0.ETA.ordinal()] = 1;
        b[qg0.TRAVEL.ordinal()] = 2;
        int[] iArr3 = new int[pg0.values().length];
        c = iArr3;
        iArr3[pg0.COMMUTE_TIME.ordinal()] = 1;
        c[pg0.RING_PHONE.ordinal()] = 2;
        int[] iArr4 = new int[ru0.values().length];
        d = iArr4;
        iArr4[ru0.JSON_FILE_EVENT.ordinal()] = 1;
        d[ru0.HEARTBEAT_EVENT.ordinal()] = 2;
        d[ru0.SERVICE_CHANGE_EVENT.ordinal()] = 3;
        d[ru0.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 4;
        d[ru0.APP_NOTIFICATION_EVENT.ordinal()] = 5;
        d[ru0.MUSIC_EVENT.ordinal()] = 6;
        d[ru0.BACKGROUND_SYNC_EVENT.ordinal()] = 7;
        d[ru0.MICRO_APP_EVENT.ordinal()] = 8;
        d[ru0.TIME_SYNC_EVENT.ordinal()] = 9;
        d[ru0.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        d[ru0.BATTERY_EVENT.ordinal()] = 11;
        d[ru0.ENCRYPTED_DATA.ordinal()] = 12;
        int[] iArr5 = new int[tf0.values().length];
        e = iArr5;
        iArr5[tf0.START.ordinal()] = 1;
        e[tf0.STOP.ordinal()] = 2;
        int[] iArr6 = new int[ky0.values().length];
        f = iArr6;
        iArr6[ky0.GET_CHALLENGE_INFO.ordinal()] = 1;
        f[ky0.LIST_CHALLENGES.ordinal()] = 2;
        f[ky0.SYNC_DATA.ordinal()] = 3;
        int[] iArr7 = new int[WorkoutState.values().length];
        g = iArr7;
        iArr7[WorkoutState.START.ordinal()] = 1;
        g[WorkoutState.RESUME.ordinal()] = 2;
        g[WorkoutState.PAUSE.ordinal()] = 3;
        g[WorkoutState.END.ordinal()] = 4;
        int[] iArr8 = new int[v11.values().length];
        h = iArr8;
        iArr8[v11.ROUTE_IMAGE.ordinal()] = 1;
        h[v11.DISTANCE.ordinal()] = 2;
    }
    */
}
