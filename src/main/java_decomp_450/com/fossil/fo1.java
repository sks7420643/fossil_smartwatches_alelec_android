package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fo1 {
    CBC_PKCS5_PADDING("AES/CBC/PKCS5PADDING"),
    CBC_NO_PADDING("AES/CBC/NoPadding"),
    CTR_NO_PADDING("AES/CTR/NoPadding"),
    GCM_NO_PADDING("AES/GCM/NoPadding");
    
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public fo1(String str) {
        this.a = str;
    }
}
