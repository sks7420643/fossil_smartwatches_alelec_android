package com.fossil;

import com.fossil.bw2;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nv2 {
    @DexIgnore
    public static volatile nv2 b;
    @DexIgnore
    public static volatile nv2 c;
    @DexIgnore
    public static /* final */ nv2 d; // = new nv2(true);
    @DexIgnore
    public /* final */ Map<a, bw2.d<?, ?>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    @DexIgnore
    public nv2() {
        this.a = new HashMap();
    }

    @DexIgnore
    public static nv2 a() {
        nv2 nv2 = b;
        if (nv2 == null) {
            synchronized (nv2.class) {
                nv2 = b;
                if (nv2 == null) {
                    nv2 = d;
                    b = nv2;
                }
            }
        }
        return nv2;
    }

    @DexIgnore
    public static nv2 b() {
        nv2 nv2 = c;
        if (nv2 != null) {
            return nv2;
        }
        synchronized (nv2.class) {
            nv2 nv22 = c;
            if (nv22 != null) {
                return nv22;
            }
            nv2 a2 = aw2.a(nv2.class);
            c = a2;
            return a2;
        }
    }

    @DexIgnore
    public nv2(boolean z) {
        this.a = Collections.emptyMap();
    }

    @DexIgnore
    public final <ContainingType extends jx2> bw2.d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return (bw2.d<ContainingType, ?>) this.a.get(new a(containingtype, i));
    }
}
