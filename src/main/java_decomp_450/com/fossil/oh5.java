package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh5 implements Factory<nh5> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> c;

    @DexIgnore
    public oh5(Provider<DianaPresetRepository> provider, Provider<ch5> provider2, Provider<WatchFaceRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static oh5 a(Provider<DianaPresetRepository> provider, Provider<ch5> provider2, Provider<WatchFaceRepository> provider3) {
        return new oh5(provider, provider2, provider3);
    }

    @DexIgnore
    public static nh5 a(DianaPresetRepository dianaPresetRepository, ch5 ch5, WatchFaceRepository watchFaceRepository) {
        return new nh5(dianaPresetRepository, ch5, watchFaceRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nh5 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
