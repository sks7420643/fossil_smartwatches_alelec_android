package com.fossil;

import android.os.IBinder;
import com.fossil.ma4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class pa4 implements Runnable {
    @DexIgnore
    public /* final */ ma4.b a;
    @DexIgnore
    public /* final */ IBinder b;

    @DexIgnore
    public pa4(ma4.b bVar, IBinder iBinder) {
        this.a = bVar;
        this.b = iBinder;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
