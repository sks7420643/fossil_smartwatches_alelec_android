package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk4 implements Factory<ih5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public gk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static gk4 a(wj4 wj4) {
        return new gk4(wj4);
    }

    @DexIgnore
    public static ih5 b(wj4 wj4) {
        ih5 f = wj4.f();
        c87.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ih5 get() {
        return b(this.a);
    }
}
