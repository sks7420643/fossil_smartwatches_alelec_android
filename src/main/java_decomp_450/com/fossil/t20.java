package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.fossil.el;
import com.fossil.x20;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t20 extends Drawable implements x20.b, Animatable, el {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Rect j;
    @DexIgnore
    public List<el.a> p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ x20 a;

        @DexIgnore
        public a(x20 x20) {
            this.a = x20;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new t20(this);
        }
    }

    @DexIgnore
    public t20(Context context, nw nwVar, ex<Bitmap> exVar, int i2, int i3, Bitmap bitmap) {
        this(new a(new x20(aw.a(context), nwVar, i2, i3, exVar, bitmap)));
    }

    @DexIgnore
    public void a(ex<Bitmap> exVar, Bitmap bitmap) {
        this.a.a.a(exVar, bitmap);
    }

    @DexIgnore
    public final Drawable.Callback b() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    @DexIgnore
    public ByteBuffer c() {
        return this.a.a.b();
    }

    @DexIgnore
    public final Rect d() {
        if (this.j == null) {
            this.j = new Rect();
        }
        return this.j;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!this.d) {
            if (this.h) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), d());
                this.h = false;
            }
            canvas.drawBitmap(this.a.a.c(), (Rect) null, d(), h());
        }
    }

    @DexIgnore
    public Bitmap e() {
        return this.a.a.e();
    }

    @DexIgnore
    public int f() {
        return this.a.a.f();
    }

    @DexIgnore
    public int g() {
        return this.a.a.d();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.a.a.g();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.a.a.i();
    }

    @DexIgnore
    public int getOpacity() {
        return -2;
    }

    @DexIgnore
    public final Paint h() {
        if (this.i == null) {
            this.i = new Paint(2);
        }
        return this.i;
    }

    @DexIgnore
    public int i() {
        return this.a.a.h();
    }

    @DexIgnore
    public boolean isRunning() {
        return this.b;
    }

    @DexIgnore
    public final void j() {
        List<el.a> list = this.p;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.p.get(i2).a(this);
            }
        }
    }

    @DexIgnore
    public void k() {
        this.d = true;
        this.a.a.a();
    }

    @DexIgnore
    public final void l() {
        this.f = 0;
    }

    @DexIgnore
    public final void m() {
        u50.a(!this.d, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.a.a.f() == 1) {
            invalidateSelf();
        } else if (!this.b) {
            this.b = true;
            this.a.a.a(this);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void n() {
        this.b = false;
        this.a.a.b(this);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.h = true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        h().setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        h().setColorFilter(colorFilter);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        u50.a(!this.d, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.e = z;
        if (!z) {
            n();
        } else if (this.c) {
            m();
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        this.c = true;
        l();
        if (this.e) {
            m();
        }
    }

    @DexIgnore
    public void stop() {
        this.c = false;
        n();
    }

    @DexIgnore
    @Override // com.fossil.x20.b
    public void a() {
        if (b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (g() == f() - 1) {
            this.f++;
        }
        int i2 = this.g;
        if (i2 != -1 && this.f >= i2) {
            j();
            stop();
        }
    }

    @DexIgnore
    public t20(a aVar) {
        this.e = true;
        this.g = -1;
        u50.a(aVar);
        this.a = aVar;
    }
}
