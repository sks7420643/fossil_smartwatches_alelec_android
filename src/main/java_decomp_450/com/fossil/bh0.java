package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bh0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ch0 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bh0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bh0 createFromParcel(Parcel parcel) {
            ch0 ch0 = ch0.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = tp0.a[ch0.ordinal()];
            if (i == 1) {
                return lt0.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return ah0.CREATOR.createFromParcel(parcel);
            }
            throw new p87();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bh0[] newArray(int i) {
            return new bh0[i];
        }
    }

    @DexIgnore
    public bh0(ch0 ch0) {
        this.a = ch0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((bh0) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
    }
}
