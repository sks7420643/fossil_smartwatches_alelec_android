package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s3<K, V> implements Iterable<Map.Entry<K, V>> {
    @DexIgnore
    public c<K, V> a;
    @DexIgnore
    public c<K, V> b;
    @DexIgnore
    public WeakHashMap<f<K, V>, Boolean> c; // = new WeakHashMap<>();
    @DexIgnore
    public int d; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends e<K, V> {
        @DexIgnore
        public a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        @Override // com.fossil.s3.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.d;
        }

        @DexIgnore
        @Override // com.fossil.s3.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends e<K, V> {
        @DexIgnore
        public b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        @Override // com.fossil.s3.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.c;
        }

        @DexIgnore
        @Override // com.fossil.s3.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public /* final */ V b;
        @DexIgnore
        public c<K, V> c;
        @DexIgnore
        public c<K, V> d;

        @DexIgnore
        public c(K k, V v) {
            this.a = k;
            this.b = v;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!this.a.equals(cVar.a) || !this.b.equals(cVar.b)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            return this.a;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        @DexIgnore
        public String toString() {
            return ((Object) this.a) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> a;
        @DexIgnore
        public boolean b; // = true;

        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.s3.f
        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.a;
            if (cVar == cVar2) {
                c<K, V> cVar3 = cVar2.d;
                this.a = cVar3;
                this.b = cVar3 == null;
            }
        }

        @DexIgnore
        public boolean hasNext() {
            if (!this.b) {
                c<K, V> cVar = this.a;
                if (cVar == null || cVar.c == null) {
                    return false;
                }
                return true;
            } else if (s3.this.a != null) {
                return true;
            } else {
                return false;
            }
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            if (this.b) {
                this.b = false;
                this.a = s3.this.a;
            } else {
                c<K, V> cVar = this.a;
                this.a = cVar != null ? cVar.c : null;
            }
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> a;
        @DexIgnore
        public c<K, V> b;

        @DexIgnore
        public e(c<K, V> cVar, c<K, V> cVar2) {
            this.a = cVar2;
            this.b = cVar;
        }

        @DexIgnore
        @Override // com.fossil.s3.f
        public void a(c<K, V> cVar) {
            if (this.a == cVar && cVar == this.b) {
                this.b = null;
                this.a = null;
            }
            c<K, V> cVar2 = this.a;
            if (cVar2 == cVar) {
                this.a = b(cVar2);
            }
            if (this.b == cVar) {
                this.b = a();
            }
        }

        @DexIgnore
        public abstract c<K, V> b(c<K, V> cVar);

        @DexIgnore
        public abstract c<K, V> c(c<K, V> cVar);

        @DexIgnore
        public boolean hasNext() {
            return this.b != null;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            c<K, V> cVar = this.b;
            this.b = a();
            return cVar;
        }

        @DexIgnore
        public final c<K, V> a() {
            c<K, V> cVar = this.b;
            c<K, V> cVar2 = this.a;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return c(cVar);
        }
    }

    @DexIgnore
    public interface f<K, V> {
        @DexIgnore
        void a(c<K, V> cVar);
    }

    @DexIgnore
    public c<K, V> a(K k) {
        c<K, V> cVar = this.a;
        while (cVar != null && !cVar.a.equals(k)) {
            cVar = cVar.c;
        }
        return cVar;
    }

    @DexIgnore
    public V b(K k, V v) {
        c<K, V> a2 = a(k);
        if (a2 != null) {
            return a2.b;
        }
        a(k, v);
        return null;
    }

    @DexIgnore
    public Map.Entry<K, V> c() {
        return this.b;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> descendingIterator() {
        b bVar = new b(this.b, this.a);
        this.c.put(bVar, false);
        return bVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof s3)) {
            return false;
        }
        s3 s3Var = (s3) obj;
        if (size() != s3Var.size()) {
            return false;
        }
        Iterator<Map.Entry<K, V>> it = iterator();
        Iterator<Map.Entry<K, V>> it2 = s3Var.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry<K, V> next = it.next();
            Map.Entry<K, V> next2 = it2.next();
            if ((next == null && next2 != null) || (next != null && !next.equals(next2))) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Iterator<Map.Entry<K, V>> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += it.next().hashCode();
        }
        return i;
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.a, this.b);
        this.c.put(aVar, false);
        return aVar;
    }

    @DexIgnore
    public V remove(K k) {
        c<K, V> a2 = a(k);
        if (a2 == null) {
            return null;
        }
        this.d--;
        if (!this.c.isEmpty()) {
            for (f<K, V> fVar : this.c.keySet()) {
                fVar.a(a2);
            }
        }
        c<K, V> cVar = a2.d;
        if (cVar != null) {
            cVar.c = a2.c;
        } else {
            this.a = a2.c;
        }
        c<K, V> cVar2 = a2.c;
        if (cVar2 != null) {
            cVar2.d = a2.d;
        } else {
            this.b = a2.d;
        }
        a2.c = null;
        a2.d = null;
        return a2.b;
    }

    @DexIgnore
    public int size() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Map.Entry<K, V>> it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public c<K, V> a(K k, V v) {
        c<K, V> cVar = new c<>(k, v);
        this.d++;
        c<K, V> cVar2 = this.b;
        if (cVar2 == null) {
            this.a = cVar;
            this.b = cVar;
            return cVar;
        }
        cVar2.c = cVar;
        cVar.d = cVar2;
        this.b = cVar;
        return cVar;
    }

    @DexIgnore
    public s3<K, V>.d b() {
        s3<K, V>.d dVar = new d();
        this.c.put(dVar, false);
        return dVar;
    }

    @DexIgnore
    public Map.Entry<K, V> a() {
        return this.a;
    }
}
