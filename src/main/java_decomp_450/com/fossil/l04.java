package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l04 extends jz3<Object> implements Serializable {
    @DexIgnore
    public static /* final */ l04 INSTANCE; // = new l04();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return obj.toString().compareTo(obj2.toString());
    }

    @DexIgnore
    public String toString() {
        return "Ordering.usingToString()";
    }
}
