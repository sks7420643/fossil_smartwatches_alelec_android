package com.fossil;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.imagefilters.FilterType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e36 extends RecyclerView.g<c> {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ ArrayList<b> b;
    @DexIgnore
    public d c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ FilterType b;

        @DexIgnore
        public b(Bitmap bitmap, FilterType filterType) {
            ee7.b(bitmap, "image");
            ee7.b(filterType, "type");
            this.a = bitmap;
            this.b = filterType;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.a;
        }

        @DexIgnore
        public final FilterType b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b);
        }

        @DexIgnore
        public int hashCode() {
            Bitmap bitmap = this.a;
            int i = 0;
            int hashCode = (bitmap != null ? bitmap.hashCode() : 0) * 31;
            FilterType filterType = this.b;
            if (filterType != null) {
                i = filterType.hashCode();
            }
            return hashCode + i;
        }

        @DexIgnore
        public String toString() {
            return "ImageFilter(image=" + this.a + ", type=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ e36 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                d c;
                if (!(this.a.d.getItemCount() <= this.a.getAdapterPosition() || this.a.getAdapterPosition() == -1 || (c = this.a.d.c()) == null)) {
                    Object obj = this.a.d.b.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mFilterList[adapterPosition]");
                    c.a((b) obj);
                }
                c cVar = this.a;
                cVar.d.a(cVar.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(e36 e36, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = e36;
            View findViewById = view.findViewById(2131362641);
            ee7.a((Object) findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363304);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363381);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(b bVar, int i) {
            String str;
            ee7.b(bVar, "imageFilter");
            this.a.setImageBitmap(bVar.a());
            switch (f36.a[bVar.b().ordinal()]) {
                case 1:
                    View view = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view, "itemView");
                    str = ig5.a(view.getContext(), 2131886521);
                    break;
                case 2:
                    View view2 = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view2, "itemView");
                    str = ig5.a(view2.getContext(), 2131886517);
                    break;
                case 3:
                    View view3 = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view3, "itemView");
                    str = ig5.a(view3.getContext(), 2131886519);
                    break;
                case 4:
                    View view4 = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view4, "itemView");
                    str = ig5.a(view4.getContext(), 2131886518);
                    break;
                case 5:
                    View view5 = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view5, "itemView");
                    str = ig5.a(view5.getContext(), 2131886522);
                    break;
                case 6:
                    View view6 = ((RecyclerView.ViewHolder) this).itemView;
                    ee7.a((Object) view6, "itemView");
                    str = ig5.a(view6.getContext(), 2131886520);
                    break;
                default:
                    str = "";
                    break;
            }
            this.b.setText(str);
            if (i == this.d.a) {
                this.c.setVisibility(0);
            } else {
                this.c.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(b bVar);
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ e36(ArrayList arrayList, d dVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : dVar);
    }

    @DexIgnore
    public final d c() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void a(d dVar) {
        this.c = dVar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558681, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026na_filter, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public e36(ArrayList<b> arrayList, d dVar) {
        ee7.b(arrayList, "mFilterList");
        this.b = arrayList;
        this.c = dVar;
    }

    @DexIgnore
    public final void a(List<b> list) {
        ee7.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ImageFilterAdapter", "setData size = " + list.size());
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        if (getItemCount() > i && i != -1) {
            b bVar = this.b.get(i);
            ee7.a((Object) bVar, "mFilterList[position]");
            cVar.a(bVar, i);
        }
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e("ImageFilterAdapter", e.getMessage());
            e.printStackTrace();
        }
    }
}
