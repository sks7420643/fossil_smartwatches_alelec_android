package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o93 {
    @DexIgnore
    public /* final */ kn2 a;

    @DexIgnore
    public o93(kn2 kn2) {
        a72.a(kn2);
        this.a = kn2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof o93)) {
            return false;
        }
        try {
            return this.a.b(((o93) obj).a);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(float f) {
        try {
            this.a.setWidth(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(List<LatLng> list) {
        try {
            this.a.setPoints(list);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(int i) {
        try {
            this.a.setColor(i);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(b93 b93) {
        a72.a(b93, "startCap must not be null");
        try {
            this.a.setStartCap(b93);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(b93 b93) {
        a72.a(b93, "endCap must not be null");
        try {
            this.a.setEndCap(b93);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(int i) {
        try {
            this.a.setJointType(i);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(List<l93> list) {
        try {
            this.a.setPattern(list);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.a(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(boolean z) {
        try {
            this.a.setGeodesic(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
