package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y61 {
    @DexIgnore
    public /* final */ /* synthetic */ v81 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public y61(v81 v81) {
        this.a = v81;
    }

    @DexIgnore
    public void a(ti1 ti1) {
        if (ti1 instanceof rk1) {
            rk1 rk1 = (rk1) ti1;
            if (this.a.a(rk1) > 0) {
                v81 v81 = this.a;
                v81.a(v81.a(rk1));
                v81 v812 = this.a;
                v812.a(v812.p);
                v81 v813 = this.a;
                v813.g.add(new zq0(0, rk1.a, rk1.b, yz0.a(new JSONObject(), r51.g1, Long.valueOf(this.a.e())), 1));
                return;
            }
        }
        this.a.a(ti1);
    }
}
