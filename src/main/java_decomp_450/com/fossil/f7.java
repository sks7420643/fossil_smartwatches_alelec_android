package com.fossil;

import android.graphics.Insets;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f7 {
    @DexIgnore
    public static /* final */ f7 e; // = new f7(0, 0, 0, 0);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public f7(int i, int i2, int i3, int i4) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    @DexIgnore
    public static f7 a(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            return e;
        }
        return new f7(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f7.class != obj.getClass()) {
            return false;
        }
        f7 f7Var = (f7) obj;
        return this.d == f7Var.d && this.a == f7Var.a && this.c == f7Var.c && this.b == f7Var.b;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public String toString() {
        return "Insets{left=" + this.a + ", top=" + this.b + ", right=" + this.c + ", bottom=" + this.d + '}';
    }

    @DexIgnore
    public Insets a() {
        return Insets.of(this.a, this.b, this.c, this.d);
    }
}
