package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class el3 implements Runnable {
    @DexIgnore
    public /* final */ fl3 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ jg3 c;
    @DexIgnore
    public /* final */ Intent d;

    @DexIgnore
    public el3(fl3 fl3, int i, jg3 jg3, Intent intent) {
        this.a = fl3;
        this.b = i;
        this.c = jg3;
        this.d = intent;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c, this.d);
    }
}
