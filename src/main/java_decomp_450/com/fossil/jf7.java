package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jf7 implements Iterable<Integer>, ye7 {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final jf7 a(int i, int i2, int i3) {
            return new jf7(i, i2, i3);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public jf7(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.a = i;
            this.b = cc7.b(i, i2, i3);
            this.c = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof jf7) {
            if (!isEmpty() || !((jf7) obj).isEmpty()) {
                jf7 jf7 = (jf7) obj;
                if (!(this.a == jf7.a && this.b == jf7.b && this.c == jf7.c)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int getFirst() {
        return this.a;
    }

    @DexIgnore
    public final int getLast() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.a * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public boolean isEmpty() {
        if (this.c > 0) {
            if (this.a > this.b) {
                return true;
            }
        } else if (this.a < this.b) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public String toString() {
        int i;
        StringBuilder sb;
        if (this.c > 0) {
            sb = new StringBuilder();
            sb.append(this.a);
            sb.append("..");
            sb.append(this.b);
            sb.append(" step ");
            i = this.c;
        } else {
            sb = new StringBuilder();
            sb.append(this.a);
            sb.append(" downTo ");
            sb.append(this.b);
            sb.append(" step ");
            i = -this.c;
        }
        sb.append(i);
        return sb.toString();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.ja7' to match base method */
    @Override // java.lang.Iterable
    public Iterator<Integer> iterator() {
        return new kf7(this.a, this.b, this.c);
    }
}
