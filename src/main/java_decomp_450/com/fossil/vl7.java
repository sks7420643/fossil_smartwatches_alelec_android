package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl7 implements yi7 {
    @DexIgnore
    public /* final */ ib7 a;

    @DexIgnore
    public vl7(ib7 ib7) {
        this.a = ib7;
    }

    @DexIgnore
    @Override // com.fossil.yi7
    public ib7 a() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineScope(coroutineContext=" + a() + ')';
    }
}
