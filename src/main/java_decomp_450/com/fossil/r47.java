package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r47 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (w37.s() && z37.r != null) {
            if (w37.p()) {
                x47.b(z37.r).a((e47) new d47(z37.r, z37.a(z37.r, false, (a47) null), 2, th, thread, null), (g67) null, false, true);
                z37.m.b("MTA has caught the following uncaught exception:");
                z37.m.b(th);
            }
            z37.f(z37.r);
            if (z37.n != null) {
                z37.m.a("Call the original uncaught exception handler.");
                if (!(z37.n instanceof r47)) {
                    z37.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
