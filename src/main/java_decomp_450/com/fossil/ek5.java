package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.os.Build;
import android.view.KeyEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponseFactory;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek5 {
    @DexIgnore
    public MediaSessionManager a;
    @DexIgnore
    public hk5 b;
    @DexIgnore
    public lk5<hk5> c; // = new lk5<>();
    @DexIgnore
    public /* final */ n87 d; // = o87.a(d.INSTANCE);
    @DexIgnore
    public MediaSessionManager.OnActiveSessionsChangedListener e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent", f = "MusicControlComponent.kt", l = {169}, m = "forcePushEventAndMetadataChanged")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ek5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ek5 ek5, fb7 fb7) {
            super(fb7);
            this.this$0 = ek5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements vc7<AudioManager> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final AudioManager invoke() {
            Object systemService = PortfolioApp.g0.c().getSystemService("audio");
            if (!(systemService instanceof AudioManager)) {
                systemService = null;
            }
            return (AudioManager) systemService;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends fe7 implements gd7<hk5, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ MediaController $mediaController;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(MediaController mediaController) {
            super(1);
            this.$mediaController = mediaController;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(hk5 hk5) {
            return Boolean.valueOf(invoke(hk5));
        }

        @DexIgnore
        public final boolean invoke(hk5 hk5) {
            ee7.b(hk5, "musicController");
            return ee7.a((Object) hk5.c(), (Object) this.$mediaController.getPackageName());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final AudioManager c() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus e() {
        AudioManager c2 = c();
        if (c2 != null) {
            c2.adjustStreamVolume(3, -1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus f() {
        AudioManager c2 = c();
        if (c2 != null) {
            c2.adjustStreamVolume(3, 1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final hk5 b() {
        return this.b;
    }

    @DexIgnore
    public final void d() {
        MediaSessionManager mediaSessionManager;
        this.f = false;
        MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener = this.e;
        if (onActiveSessionsChangedListener != null && (mediaSessionManager = this.a) != null) {
            mediaSessionManager.removeOnActiveSessionsChangedListener(onActiveSessionsChangedListener);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends jk5 {
        @DexIgnore
        public /* final */ /* synthetic */ MediaController h;
        @DexIgnore
        public /* final */ /* synthetic */ ek5 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(MediaController mediaController, MediaController mediaController2, String str, ek5 ek5) {
            super(mediaController2, str);
            this.h = mediaController;
            this.i = ek5;
        }

        @DexIgnore
        @Override // com.fossil.jk5
        public void a(ik5 ik5, ik5 ik52) {
            ee7.b(ik5, "oldMetadata");
            ee7.b(ik52, "newMetadata");
            super.a(ik5, ik52);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", "onMetadataChanged of controller " + this.h.getPackageName() + " old " + ik5 + " new " + ik52);
            this.i.a(b());
        }

        @DexIgnore
        @Override // com.fossil.jk5
        public void a(int i2, int i3, hk5 hk5) {
            ee7.b(hk5, "controller");
            super.a(i2, i3, hk5);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", "onPlaybackStateChanged of controller " + this.h.getPackageName() + " state " + i3);
            if (this.i.b() == null || i3 == 3) {
                this.i.a(hk5);
            }
            this.i.a(i3);
        }

        @DexIgnore
        @Override // com.fossil.jk5
        public void a(hk5 hk5) {
            ee7.b(hk5, "controller");
            super.a(hk5);
            this.i.c.b(hk5);
            if (ee7.a(hk5, this.i.b())) {
                this.i.a((hk5) null);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.ek5.c
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.ek5$c r0 = (com.fossil.ek5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ek5$c r0 = new com.fossil.ek5$c
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r3) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.ek5 r0 = (com.fossil.ek5) r0
            com.fossil.t87.a(r7)
            goto L_0x00de
        L_0x002e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0036:
            com.fossil.t87.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "forcePushEventAndMetadataChanged activeController "
            r2.append(r4)
            com.fossil.hk5 r4 = r6.b
            r2.append(r4)
            java.lang.String r4 = " initialize "
            r2.append(r4)
            boolean r4 = r6.f
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = "MusicControlComponent"
            r7.d(r4, r2)
            r6.g = r3
            com.fossil.hk5 r7 = r6.b
            if (r7 == 0) goto L_0x00d3
            r0 = 0
            if (r7 == 0) goto L_0x00cf
            int r7 = r7.d()
            com.fossil.hk5 r1 = r6.b
            if (r1 == 0) goto L_0x00cb
            com.fossil.ik5 r1 = r1.b()
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = ".forcePushEventAndMetadataChanged active controller="
            r3.append(r5)
            com.fossil.hk5 r5 = r6.b
            if (r5 == 0) goto L_0x00c7
            java.lang.String r5 = r5.c()
            r3.append(r5)
            java.lang.String r5 = ", state="
            r3.append(r5)
            r3.append(r7)
            java.lang.String r7 = ", metadata="
            r3.append(r7)
            r3.append(r1)
            java.lang.String r7 = r3.toString()
            r2.d(r4, r7)
            com.fossil.hk5 r7 = r6.b
            if (r7 == 0) goto L_0x00c3
            com.fossil.ik5 r7 = r7.b()
            r6.a(r7)
            com.fossil.hk5 r7 = r6.b
            if (r7 == 0) goto L_0x00bf
            int r7 = r7.d()
            r6.a(r7)
            goto L_0x00de
        L_0x00bf:
            com.fossil.ee7.a()
            throw r0
        L_0x00c3:
            com.fossil.ee7.a()
            throw r0
        L_0x00c7:
            com.fossil.ee7.a()
            throw r0
        L_0x00cb:
            com.fossil.ee7.a()
            throw r0
        L_0x00cf:
            com.fossil.ee7.a()
            throw r0
        L_0x00d3:
            r0.L$0 = r6
            r0.label = r3
            java.lang.Object r7 = r6.a(r0)
            if (r7 != r1) goto L_0x00de
            return r1
        L_0x00de:
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ek5.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableMusicControlViaNotification$2", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ek5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MediaSessionManager.OnActiveSessionsChangedListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // android.media.session.MediaSessionManager.OnActiveSessionsChangedListener
            public final void onActiveSessionsChanged(List<MediaController> list) {
                FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".enableMusicControlViaNotification Process controllers when active sessions changed");
                this.a.this$0.a(list);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ek5 ek5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ek5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                boolean a2 = xg5.a(xg5.b, (Context) PortfolioApp.g0.c(), xg5.a.SET_WATCH_APP_MUSIC, false, false, true, (Integer) null, 32, (Object) null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", ".enableMusicControlViaNotification isPermissionGranted " + a2 + " isRegister " + this.this$0.f);
                if (this.this$0.f || !a2) {
                    return i97.a;
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    ComponentName componentName = new ComponentName(PortfolioApp.g0.c().getApplicationContext(), FossilNotificationListenerService.class);
                    ek5 ek5 = this.this$0;
                    Object systemService = PortfolioApp.g0.c().getSystemService("media_session");
                    if (systemService != null) {
                        ek5.a = (MediaSessionManager) systemService;
                        MediaSessionManager c = this.this$0.a;
                        if (c != null) {
                            List<MediaController> activeSessions = c.getActiveSessions(componentName);
                            ee7.a((Object) activeSessions, "mediaSessionManager!!.getActiveSessions(component)");
                            FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".enableMusicControlViaNotification Process current active controllers first");
                            this.this$0.a(activeSessions);
                            if (this.this$0.e == null) {
                                FLogger.INSTANCE.getLocal().d("MusicControlComponent", "init activeSessionChangedListener");
                                this.this$0.e = new a(this);
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("add activeSessionChangedListener to mediaSessionManager,current active sessions ");
                            MediaSessionManager c2 = this.this$0.a;
                            if (c2 != null) {
                                sb.append(c2.getActiveSessions(componentName).size());
                                local2.d("MusicControlComponent", sb.toString());
                                MediaSessionManager c3 = this.this$0.a;
                                if (c3 != null) {
                                    MediaSessionManager.OnActiveSessionsChangedListener b = this.this$0.e;
                                    if (b != null) {
                                        c3.addOnActiveSessionsChangedListener(b, componentName);
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type android.media.session.MediaSessionManager");
                    }
                } else {
                    C0058b bVar = new C0058b(this, "All Apps");
                    if (this.this$0.c.a(bVar)) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("MusicControlComponent", ".OldNotificationMusicController is added to list controller, packageName=" + bVar.c());
                    }
                }
                this.this$0.f = true;
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ek5$b$b")
        /* renamed from: com.fossil.ek5$b$b  reason: collision with other inner class name */
        public static final class C0058b extends kk5 {
            @DexIgnore
            public /* final */ /* synthetic */ b h;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0058b(b bVar, String str) {
                super(str);
                this.h = bVar;
            }

            @DexIgnore
            @Override // com.fossil.kk5
            public void a(int i, int i2, hk5 hk5) {
                ee7.b(hk5, "controller");
                super.a(i, i2, hk5);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", ".OldNotificationMusicController onPlaybackStateChanged oldState " + i + " newState " + i2);
                if (i2 == 3) {
                    this.h.this$0.a(hk5);
                } else if (this.h.this$0.b() == null) {
                    this.h.this$0.a(hk5);
                }
                this.h.this$0.a(i2);
            }

            @DexIgnore
            @Override // com.fossil.kk5
            public void a(ik5 ik5, ik5 ik52) {
                ee7.b(ik5, "oldMetadata");
                ee7.b(ik52, "newMetadata");
                super.a(ik5, ik52);
                FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".OldNotificationMusicController onMetadataChanged");
                this.h.this$0.a(b());
            }
        }
    }

    @DexIgnore
    public final void a(hk5 hk5) {
        boolean z = !ee7.a(this.b, hk5);
        this.b = hk5;
        if (z && hk5 != null) {
            FLogger.INSTANCE.getLocal().d("MusicControlComponent", "musicController changed notifyMusicTrackInfo");
            a(hk5.b());
        }
    }

    @DexIgnore
    public final Object a(fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.c(), new b(this, null), fb7);
        return a2 == nb7.a() ? a2 : i97.a;
    }

    @DexIgnore
    public final void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "Music watch app state change, isActive " + z);
        this.g = z;
    }

    @DexIgnore
    public final void a(List<MediaController> list) {
        if (list != null && !list.isEmpty()) {
            for (T t : list) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", "Process controller " + t.getPackageName());
                if (!(this.c.a(new f(t)) != null)) {
                    String packageName = t.getPackageName();
                    ee7.a((Object) packageName, "mediaController.packageName");
                    e eVar = new e(t, t, a(packageName), this);
                    if (this.b == null) {
                        a(eVar);
                    }
                    boolean a2 = this.c.a(eVar);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("MusicControlComponent", ".processActiveMediaControllers() Add controller " + eVar.a() + " to controller map, currentActiveController " + this.b + ", isSuccess " + a2);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("MusicControlComponent", ".processActiveMediaControllers() Controller " + t.getPackageName() + " already added");
                }
            }
        }
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus b(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "sendKeyEvent activeMusicController " + this.b);
        hk5 hk5 = this.b;
        if (hk5 != null) {
            boolean a2 = hk5.a(new KeyEvent(0, i));
            boolean a3 = hk5.a(new KeyEvent(1, i));
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MusicControlComponent", "sendKeyEVent actionDown isSuccess " + a2 + " actionUp isSuccess " + a3);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = (!a2 || !a3) ? NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER : NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.NO_MUSIC_PLAYER;
    }

    @DexIgnore
    public final String a(String str) {
        ApplicationInfo applicationInfo;
        String obj;
        PackageManager packageManager = PortfolioApp.g0.c().getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(str, 0);
        } catch (Exception unused) {
            applicationInfo = null;
        }
        return (applicationInfo == null || (obj = packageManager.getApplicationLabel(applicationInfo).toString()) == null) ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN : obj;
    }

    @DexIgnore
    public final void a(NotifyMusicEventResponse.MusicMediaAction musicMediaAction) {
        NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus;
        ee7.b(musicMediaAction, "action");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "doMusicAction isMusicActive " + this.g);
        switch (fk5.a[musicMediaAction.ordinal()]) {
            case 1:
                musicMediaStatus = b(126);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 2:
                musicMediaStatus = b(127);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 3:
                musicMediaStatus = b(85);
                break;
            case 4:
                musicMediaStatus = b(87);
                break;
            case 5:
                musicMediaStatus = b(88);
                break;
            case 6:
                musicMediaStatus = f();
                break;
            case 7:
                musicMediaStatus = e();
                break;
            default:
                musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
                break;
        }
        if (this.g) {
            PortfolioApp.g0.c().a(MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, musicMediaStatus), PortfolioApp.g0.c().c());
        }
    }

    @DexIgnore
    public final int a() {
        AudioManager c2 = c();
        if (c2 != null) {
            return c2.getStreamVolume(3);
        }
        return 100;
    }

    @DexIgnore
    public final synchronized void a(int i) {
        NotifyMusicEventResponse.MusicMediaAction musicMediaAction;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", ".notifyMusicAction(), state=" + i + " isMusicActive " + this.g);
        if (this.g) {
            if (i == 2) {
                musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PAUSE;
            } else if (i != 3) {
                musicMediaAction = null;
            } else {
                musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PLAY;
            }
            if (musicMediaAction != null) {
                PortfolioApp.g0.c().a(MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus.SUCCESS), PortfolioApp.g0.c().c());
            }
        }
    }

    @DexIgnore
    public final synchronized void a(ik5 ik5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", ".notifyMusicTrackInfo(), metadata=" + ik5 + " isMusicActive " + this.g);
        if (this.g) {
            String c2 = ik5.c();
            hk5 hk5 = this.b;
            if (!(!ee7.a((Object) c2, (Object) (hk5 != null ? hk5.c() : null)))) {
                PortfolioApp.g0.c().a(MusicResponseFactory.INSTANCE.createMusicTrackInfoResponse("", (byte) a(), ik5.d(), ik5.b(), ik5.a()), PortfolioApp.g0.c().c());
            }
        }
    }
}
