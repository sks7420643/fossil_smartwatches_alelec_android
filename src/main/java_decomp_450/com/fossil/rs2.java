package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs2<E> extends wr2<E> {
    @DexIgnore
    public /* final */ os2<E> c;

    @DexIgnore
    public rs2(os2<E> os2, int i) {
        super(os2.size(), i);
        this.c = os2;
    }

    @DexIgnore
    @Override // com.fossil.wr2
    public final E a(int i) {
        return this.c.get(i);
    }
}
