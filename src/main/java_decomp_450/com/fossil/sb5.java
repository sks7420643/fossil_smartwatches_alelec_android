package com.fossil;

import com.fossil.fitness.CadenceUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sb5 {
    UNKNOWN("unknown"),
    SPM("spm"),
    RPM("rpm");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final sb5 a(Integer num) {
            int ordinal = CadenceUnit.SPM.ordinal();
            if (num != null && num.intValue() == ordinal) {
                return sb5.SPM;
            }
            int ordinal2 = CadenceUnit.RPM.ordinal();
            if (num != null && num.intValue() == ordinal2) {
                return sb5.RPM;
            }
            return sb5.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public sb5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
