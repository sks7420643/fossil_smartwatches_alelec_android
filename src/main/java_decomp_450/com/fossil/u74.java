package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u74 {
    @DexIgnore
    public /* final */ d44 a;

    @DexIgnore
    public u74(d44 d44) {
        this.a = d44;
    }

    @DexIgnore
    public c84 a(JSONObject jSONObject) throws JSONException {
        return a(jSONObject.getInt("settings_version")).a(this.a, jSONObject);
    }

    @DexIgnore
    public static v74 a(int i) {
        if (i != 3) {
            return new q74();
        }
        return new w74();
    }
}
