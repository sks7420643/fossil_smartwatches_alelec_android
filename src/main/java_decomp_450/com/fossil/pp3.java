package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ lp3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Callable b;

    @DexIgnore
    public pp3(lp3 lp3, Callable callable) {
        this.a = lp3;
        this.b = callable;
    }

    @DexIgnore
    public final void run() {
        try {
            this.a.a(this.b.call());
        } catch (Exception e) {
            this.a.a(e);
        }
    }
}
