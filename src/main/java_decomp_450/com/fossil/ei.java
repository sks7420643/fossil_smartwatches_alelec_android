package com.fossil;

import android.database.Cursor;
import com.fossil.xi;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ei extends xi.a {
    @DexIgnore
    public th b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(wi wiVar);

        @DexIgnore
        public abstract void dropAllTables(wi wiVar);

        @DexIgnore
        public abstract void onCreate(wi wiVar);

        @DexIgnore
        public abstract void onOpen(wi wiVar);

        @DexIgnore
        public abstract void onPostMigrate(wi wiVar);

        @DexIgnore
        public abstract void onPreMigrate(wi wiVar);

        @DexIgnore
        public abstract b onValidateSchema(wi wiVar);

        @DexIgnore
        @Deprecated
        public void validateMigration(wi wiVar) {
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(boolean z, String str) {
            this.a = z;
            this.b = str;
        }
    }

    @DexIgnore
    public ei(th thVar, a aVar, String str, String str2) {
        super(aVar.version);
        this.b = thVar;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean h(wi wiVar) {
        Cursor query = wiVar.query("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            boolean z = false;
            if (query.moveToFirst() && query.getInt(0) == 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static boolean i(wi wiVar) {
        Cursor query = wiVar.query("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (query.moveToFirst() && query.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.xi.a
    public void a(wi wiVar) {
        super.a(wiVar);
    }

    @DexIgnore
    @Override // com.fossil.xi.a
    public void b(wi wiVar, int i, int i2) {
        boolean z;
        List<li> a2;
        th thVar = this.b;
        if (thVar == null || (a2 = thVar.d.a(i, i2)) == null) {
            z = false;
        } else {
            this.c.onPreMigrate(wiVar);
            for (li liVar : a2) {
                liVar.migrate(wiVar);
            }
            b onValidateSchema = this.c.onValidateSchema(wiVar);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(wiVar);
                g(wiVar);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + onValidateSchema.b);
            }
        }
        if (!z) {
            th thVar2 = this.b;
            if (thVar2 == null || thVar2.a(i, i2)) {
                throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.c.dropAllTables(wiVar);
            this.c.createAllTables(wiVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.xi.a
    public void c(wi wiVar) {
        boolean h = h(wiVar);
        this.c.createAllTables(wiVar);
        if (!h) {
            b onValidateSchema = this.c.onValidateSchema(wiVar);
            if (!onValidateSchema.a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
            }
        }
        g(wiVar);
        this.c.onCreate(wiVar);
    }

    @DexIgnore
    @Override // com.fossil.xi.a
    public void d(wi wiVar) {
        super.d(wiVar);
        e(wiVar);
        this.c.onOpen(wiVar);
        this.b = null;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void e(wi wiVar) {
        if (i(wiVar)) {
            String str = null;
            Cursor query = wiVar.query(new vi("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (query.moveToFirst()) {
                    str = query.getString(0);
                }
                query.close();
                if (!this.d.equals(str) && !this.e.equals(str)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        } else {
            b onValidateSchema = this.c.onValidateSchema(wiVar);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(wiVar);
                g(wiVar);
                return;
            }
            throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
        }
    }

    @DexIgnore
    public final void f(wi wiVar) {
        wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void g(wi wiVar) {
        f(wiVar);
        wiVar.execSQL(di.a(this.d));
    }

    @DexIgnore
    @Override // com.fossil.xi.a
    public void a(wi wiVar, int i, int i2) {
        b(wiVar, i, i2);
    }
}
