package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vs7 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 4029025366392702726L;

    @DexIgnore
    public vs7() {
    }

    @DexIgnore
    public vs7(String str) {
        super(str);
    }

    @DexIgnore
    public vs7(Throwable th) {
        super(th);
    }

    @DexIgnore
    public vs7(String str, Throwable th) {
        super(str, th);
    }
}
