package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp4 {
    @DexIgnore
    public /* final */ go4 a;

    @DexIgnore
    public dp4(go4 go4) {
        ee7.b(go4, "dao");
        this.a = go4;
    }

    @DexIgnore
    public final long a(fo4 fo4) {
        ee7.b(fo4, "profile");
        return this.a.a(fo4);
    }

    @DexIgnore
    public final fo4 b() {
        return this.a.c();
    }

    @DexIgnore
    public final LiveData<fo4> c() {
        return this.a.b();
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }
}
