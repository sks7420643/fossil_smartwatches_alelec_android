package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l54 extends v54.d.AbstractC0206d.a.b {
    @DexIgnore
    public /* final */ w54<v54.d.AbstractC0206d.a.b.e> a;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.a.b.c b;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.a.b.AbstractC0212d c;
    @DexIgnore
    public /* final */ w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> d;

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b
    public w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b
    public v54.d.AbstractC0206d.a.b.c b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b
    public v54.d.AbstractC0206d.a.b.AbstractC0212d c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b
    public w54<v54.d.AbstractC0206d.a.b.e> d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b bVar = (v54.d.AbstractC0206d.a.b) obj;
        if (!this.a.equals(bVar.d()) || !this.b.equals(bVar.b()) || !this.c.equals(bVar.c()) || !this.d.equals(bVar.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Execution{threads=" + this.a + ", exception=" + this.b + ", signal=" + this.c + ", binaries=" + this.d + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.AbstractC0210b {
        @DexIgnore
        public w54<v54.d.AbstractC0206d.a.b.e> a;
        @DexIgnore
        public v54.d.AbstractC0206d.a.b.c b;
        @DexIgnore
        public v54.d.AbstractC0206d.a.b.AbstractC0212d c;
        @DexIgnore
        public w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> d;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0210b
        public v54.d.AbstractC0206d.a.b.AbstractC0210b a(v54.d.AbstractC0206d.a.b.c cVar) {
            if (cVar != null) {
                this.b = cVar;
                return this;
            }
            throw new NullPointerException("Null exception");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0210b
        public v54.d.AbstractC0206d.a.b.AbstractC0210b b(w54<v54.d.AbstractC0206d.a.b.e> w54) {
            if (w54 != null) {
                this.a = w54;
                return this;
            }
            throw new NullPointerException("Null threads");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0210b
        public v54.d.AbstractC0206d.a.b.AbstractC0210b a(v54.d.AbstractC0206d.a.b.AbstractC0212d dVar) {
            if (dVar != null) {
                this.c = dVar;
                return this;
            }
            throw new NullPointerException("Null signal");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0210b
        public v54.d.AbstractC0206d.a.b.AbstractC0210b a(w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> w54) {
            if (w54 != null) {
                this.d = w54;
                return this;
            }
            throw new NullPointerException("Null binaries");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0210b
        public v54.d.AbstractC0206d.a.b a() {
            String str = "";
            if (this.a == null) {
                str = str + " threads";
            }
            if (this.b == null) {
                str = str + " exception";
            }
            if (this.c == null) {
                str = str + " signal";
            }
            if (this.d == null) {
                str = str + " binaries";
            }
            if (str.isEmpty()) {
                return new l54(this.a, this.b, this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public l54(w54<v54.d.AbstractC0206d.a.b.e> w54, v54.d.AbstractC0206d.a.b.c cVar, v54.d.AbstractC0206d.a.b.AbstractC0212d dVar, w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> w542) {
        this.a = w54;
        this.b = cVar;
        this.c = dVar;
        this.d = w542;
    }
}
