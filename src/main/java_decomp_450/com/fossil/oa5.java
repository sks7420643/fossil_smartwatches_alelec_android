package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oa5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public oa5(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, ConstraintLayout constraintLayout, ImageView imageView, View view2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = constraintLayout;
        this.s = imageView;
        this.t = view2;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
    }

    @DexIgnore
    public static oa5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static oa5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (oa5) ViewDataBinding.a(layoutInflater, 2131558714, viewGroup, z, obj);
    }
}
