package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq4 extends he {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Pattern j; // = Pattern.compile("^[a-z0-9.]+$");
    @DexIgnore
    public MutableLiveData<c> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ fp4 f;
    @DexIgnore
    public /* final */ to4 g;
    @DexIgnore
    public /* final */ ch5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;

        @DexIgnore
        public b(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && ee7.a(this.b, bVar.b);
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a * 31;
            String str = this.b;
            return i + (str != null ? str.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "Error(errorCode=" + this.a + ", errorMessage=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.a == cVar.a && this.b == cVar.b;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            boolean z = this.a;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = i2 * 31;
            boolean z2 = this.b;
            if (z2 == 0) {
                i = z2;
            }
            return i4 + i;
        }

        @DexIgnore
        public String toString() {
            return "IdValidation(isCombineMet=" + this.a + ", isLengthMet=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1", f = "BCCreateSocialProfileViewModel.kt", l = {57, 65}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $socialId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ aq4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$1", f = "BCCreateSocialProfileViewModel.kt", l = {66, 71}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Integer code;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    to4 a2 = this.this$0.this$0.g;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = a2.a(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    ko4 ko4 = (ko4) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ko4 ko42 = (ko4) obj;
                if (!(ko42.a() == null || (code = ko42.a().getCode()) == null || code.intValue() != 0)) {
                    FLogger.INSTANCE.getLocal().e(aq4.i, "reset device Id");
                    this.this$0.this$0.h.x(true);
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.L$1 = ko42;
                    this.label = 2;
                    if (c.b(this) == a) {
                        return a;
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$result$1", f = "BCCreateSocialProfileViewModel.kt", l = {58}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<fo4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<fo4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    fp4 c = this.this$0.this$0.f;
                    String str = this.this$0.$socialId;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.a(str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(aq4 aq4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = aq4;
            this.$socialId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$socialId, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                this.this$0.c.a(pb7.a(true));
                ti7 b2 = qj7.b();
                b bVar = new b(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b2, bVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                ko4 ko4 = (ko4) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko42 = (ko4) obj;
            this.this$0.c.a(pb7.a(false));
            if (ko42.c() != null) {
                this.this$0.e.a(pb7.a(true));
                ti7 b3 = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = ko42;
                this.label = 2;
                if (vh7.a(b3, aVar, this) == a2) {
                    return a2;
                }
            } else if (ko42.a() != null) {
                aq4 aq4 = this.this$0;
                Integer code = ko42.a().getCode();
                ee7.a((Object) code, "result.error.code");
                aq4.a(code.intValue(), ko42.a().getMessage());
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String name = aq4.class.getName();
        ee7.a((Object) name, "BCCreateSocialProfileViewModel::class.java.name");
        i = name;
    }
    */

    @DexIgnore
    public aq4(fp4 fp4, to4 to4, ch5 ch5) {
        ee7.b(fp4, "socialProfileRepository");
        ee7.b(to4, "fcmRepository");
        ee7.b(ch5, "sharePrefs");
        this.f = fp4;
        this.g = to4;
        this.h = ch5;
    }

    @DexIgnore
    public final LiveData<b> b() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<c> e() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Boolean> a() {
        return this.d;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "socialId");
        boolean z = true;
        if (str.length() > 0) {
            int length = str.length();
            boolean z2 = 6 <= length && 24 >= length;
            boolean matches = j.matcher(str).matches();
            a(matches, z2);
            MutableLiveData<Boolean> mutableLiveData = this.d;
            if (!matches || !z2) {
                z = false;
            }
            mutableLiveData.a(Boolean.valueOf(z));
            return;
        }
        a(this, false, false, 3, null);
        this.d.a((Boolean) false);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "socialId");
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, str, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ void a(aq4 aq4, boolean z, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        aq4.a(z, z2);
    }

    @DexIgnore
    public final void a(boolean z, boolean z2) {
        this.a.a(new c(z, z2));
    }

    @DexIgnore
    public final void a(int i2, String str) {
        this.b.a(new b(i2, str));
    }
}
