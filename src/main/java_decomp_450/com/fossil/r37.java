package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.l27;
import com.fossil.m27;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r37 implements j37 {
    @DexIgnore
    public static a e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public Handler b;
        @DexIgnore
        public Context c;
        @DexIgnore
        public Runnable d;
        @DexIgnore
        public Runnable e;

        @DexIgnore
        public a(Context context) {
            this.a = false;
            this.b = new Handler(Looper.getMainLooper());
            this.d = new p37(this);
            this.e = new q37(this);
            this.c = context;
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityPaused");
            this.b.removeCallbacks(this.e);
            this.b.postDelayed(this.d, 800);
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityResumed");
            this.b.removeCallbacks(this.d);
            this.b.postDelayed(this.e, 800);
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public r37(Context context, String str, boolean z) {
        p27.d("MicroMsg.SDK.WXApiImplV10", "<init>, appId = " + str + ", checkSignature = " + z);
        this.a = context;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final void a(Context context, String str) {
        String str2 = "AWXOP" + str;
        w37.b(context, str2);
        w37.a(true);
        w37.a(x37.PERIOD);
        w37.c(60);
        w37.c(context, "Wechat_Sdk");
        try {
            y37.a(context, str2, "2.0.3");
        } catch (u37 e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.j37
    public final boolean a() {
        if (!this.d) {
            try {
                PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo("com.tencent.mm", 64);
                if (packageInfo == null) {
                    return false;
                }
                return n37.a(this.a, packageInfo.signatures, this.c);
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        } else {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
    }

    @DexIgnore
    public final boolean a(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/createChatroom"), null, null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_create_chatroom_group_id"), bundle.getString("_wxapi_create_chatroom_chatroom_name"), bundle.getString("_wxapi_create_chatroom_chatroom_nickname"), bundle.getString("_wxapi_create_chatroom_ext_msg")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.fossil.j37
    public final boolean a(Intent intent, k37 k37) {
        try {
            if (!n37.a(intent, "com.tencent.mm.openapi.token")) {
                p27.c("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, intent not from weixin msg");
                return false;
            } else if (!this.d) {
                String stringExtra = intent.getStringExtra("_mmessage_content");
                int intExtra = intent.getIntExtra("_mmessage_sdkVersion", 0);
                String stringExtra2 = intent.getStringExtra("_mmessage_appPackage");
                if (stringExtra2 != null) {
                    if (stringExtra2.length() != 0) {
                        if (!a(intent.getByteArrayExtra("_mmessage_checksum"), n27.a(stringExtra, intExtra, stringExtra2))) {
                            p27.a("MicroMsg.SDK.WXApiImplV10", "checksum fail");
                            return false;
                        }
                        int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                        if (intExtra2 == 9) {
                            k37.a(new w27(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 12) {
                            k37.a(new a37(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 14) {
                            k37.a(new y27(intent.getExtras()));
                            return true;
                        } else if (intExtra2 != 15) {
                            switch (intExtra2) {
                                case 1:
                                    k37.a(new e37(intent.getExtras()));
                                    return true;
                                case 2:
                                    k37.a(new f37(intent.getExtras()));
                                    return true;
                                case 3:
                                    k37.a(new b37(intent.getExtras()));
                                    return true;
                                case 4:
                                    k37.a(new g37(intent.getExtras()));
                                    return true;
                                case 5:
                                    k37.a(new i37(intent.getExtras()));
                                    return true;
                                case 6:
                                    k37.a(new c37(intent.getExtras()));
                                    return true;
                                default:
                                    p27.a("MicroMsg.SDK.WXApiImplV10", "unknown cmd = " + intExtra2);
                                    break;
                            }
                            return false;
                        } else {
                            k37.a(new z27(intent.getExtras()));
                            return true;
                        }
                    }
                }
                p27.a("MicroMsg.SDK.WXApiImplV10", "invalid argument");
                return false;
            } else {
                throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
            }
        } catch (Exception e2) {
            p27.a("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, ex = %s", e2.getMessage());
        }
    }

    @DexIgnore
    @Override // com.fossil.j37
    public final boolean a(u27 u27) {
        String str;
        if (!this.d) {
            if (!n37.a(this.a, "com.tencent.mm", this.c)) {
                str = "sendReq failed for wechat app signature check failed";
            } else if (!u27.a()) {
                str = "sendReq checkArgs fail";
            } else {
                p27.d("MicroMsg.SDK.WXApiImplV10", "sendReq, req type = " + u27.b());
                Bundle bundle = new Bundle();
                u27.b(bundle);
                if (u27.b() == 5) {
                    return j(this.a, bundle);
                }
                if (u27.b() == 7) {
                    return d(this.a, bundle);
                }
                if (u27.b() == 8) {
                    return f(this.a, bundle);
                }
                if (u27.b() == 10) {
                    return e(this.a, bundle);
                }
                if (u27.b() == 9) {
                    return c(this.a, bundle);
                }
                if (u27.b() == 11) {
                    return h(this.a, bundle);
                }
                if (u27.b() == 12) {
                    return i(this.a, bundle);
                }
                if (u27.b() == 13) {
                    return g(this.a, bundle);
                }
                if (u27.b() == 14) {
                    return a(this.a, bundle);
                }
                if (u27.b() == 15) {
                    return b(this.a, bundle);
                }
                l27.a aVar = new l27.a();
                aVar.e = bundle;
                aVar.c = "weixin://sendreq?appid=" + this.b;
                aVar.a = "com.tencent.mm";
                aVar.b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
                return l27.a(this.a, aVar);
            }
            p27.a("MicroMsg.SDK.WXApiImplV10", str);
            return false;
        }
        throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
    }

    @DexIgnore
    @Override // com.fossil.j37
    public final boolean a(String str) {
        Application application;
        if (this.d) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!n37.a(this.a, "com.tencent.mm", this.c)) {
            p27.a("MicroMsg.SDK.WXApiImplV10", "register app failed for wechat app signature check failed");
            return false;
        } else {
            if (e == null && Build.VERSION.SDK_INT >= 14) {
                Context context = this.a;
                if (context instanceof Activity) {
                    a(context, str);
                    e = new a(this.a);
                    application = ((Activity) this.a).getApplication();
                } else if (context instanceof Service) {
                    a(context, str);
                    e = new a(this.a);
                    application = ((Service) this.a).getApplication();
                } else {
                    p27.b("MicroMsg.SDK.WXApiImplV10", "context is not instanceof Activity or Service, disable WXStat");
                }
                application.registerActivityLifecycleCallbacks(e);
            }
            p27.d("MicroMsg.SDK.WXApiImplV10", "registerApp, appId = " + str);
            if (str != null) {
                this.b = str;
            }
            p27.d("MicroMsg.SDK.WXApiImplV10", "register app " + this.a.getPackageName());
            m27.a aVar = new m27.a();
            aVar.a = "com.tencent.mm";
            aVar.b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
            aVar.c = "weixin://registerapp?appid=" + this.b;
            return m27.a(this.a, aVar);
        }
    }

    @DexIgnore
    public final boolean a(byte[] bArr, byte[] bArr2) {
        String str;
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            str = "checkSumConsistent fail, invalid arguments";
        } else if (bArr.length != bArr2.length) {
            str = "checkSumConsistent fail, length is different";
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
        p27.a("MicroMsg.SDK.WXApiImplV10", str);
        return false;
    }

    @DexIgnore
    public final boolean b(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/joinChatroom"), null, null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_join_chatroom_group_id"), bundle.getString("_wxapi_join_chatroom_chatroom_nickname"), bundle.getString("_wxapi_join_chatroom_ext_msg")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean c(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), null, null, new String[]{this.b, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean d(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene"));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg"), sb.toString(), sb2.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean e(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizTempSession");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_show_type"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_session_from"), sb.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean f(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene"));
        Cursor query = contentResolver.query(parse, null, null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg"), sb.toString()}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean g(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusiLuckyMoney"), null, null, new String[]{this.b, bundle.getString("_wxapi_open_busi_lucky_money_timeStamp"), bundle.getString("_wxapi_open_busi_lucky_money_nonceStr"), bundle.getString("_wxapi_open_busi_lucky_money_signType"), bundle.getString("_wxapi_open_busi_lucky_money_signature"), bundle.getString("_wxapi_open_busi_lucky_money_package")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean h(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openRankList"), null, null, new String[0], null);
        if (query == null) {
            return true;
        }
        query.close();
        return true;
    }

    @DexIgnore
    public final boolean i(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openWebview"), null, null, new String[]{this.b, bundle.getString("_wxapi_jump_to_webview_url"), bundle.getString("_wxapi_basereq_transaction")}, null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean j(Context context, Bundle bundle) {
        if (f == null) {
            f = new l37(context).getString("_wxapp_pay_entry_classname_", null);
            p27.d("MicroMsg.SDK.WXApiImplV10", "pay, set wxappPayEntryClassname = " + f);
            if (f == null) {
                p27.a("MicroMsg.SDK.WXApiImplV10", "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        l27.a aVar = new l27.a();
        aVar.e = bundle;
        aVar.a = "com.tencent.mm";
        aVar.b = f;
        return l27.a(context, aVar);
    }
}
