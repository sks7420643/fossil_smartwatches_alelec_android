package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr3 {
    @DexIgnore
    public static /* final */ int BOTTOM_END; // = 2131361795;
    @DexIgnore
    public static /* final */ int BOTTOM_START; // = 2131361796;
    @DexIgnore
    public static /* final */ int TOP_END; // = 2131361810;
    @DexIgnore
    public static /* final */ int TOP_START; // = 2131361811;
    @DexIgnore
    public static /* final */ int accessibility_action_clickable_span; // = 2131361817;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_0; // = 2131361818;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_1; // = 2131361819;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_10; // = 2131361820;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_11; // = 2131361821;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_12; // = 2131361822;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_13; // = 2131361823;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_14; // = 2131361824;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_15; // = 2131361825;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_16; // = 2131361826;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_17; // = 2131361827;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_18; // = 2131361828;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_19; // = 2131361829;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_2; // = 2131361830;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_20; // = 2131361831;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_21; // = 2131361832;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_22; // = 2131361833;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_23; // = 2131361834;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_24; // = 2131361835;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_25; // = 2131361836;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_26; // = 2131361837;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_27; // = 2131361838;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_28; // = 2131361839;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_29; // = 2131361840;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_3; // = 2131361841;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_30; // = 2131361842;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_31; // = 2131361843;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_4; // = 2131361844;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_5; // = 2131361845;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_6; // = 2131361846;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_7; // = 2131361847;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_8; // = 2131361848;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_9; // = 2131361849;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int add; // = 2131361881;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361884;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int auto; // = 2131361901;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361915;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361977;
    @DexIgnore
    public static /* final */ int cancel_button; // = 2131361982;
    @DexIgnore
    public static /* final */ int center; // = 2131362009;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131362015;
    @DexIgnore
    public static /* final */ int checked; // = 2131362016;
    @DexIgnore
    public static /* final */ int chip; // = 2131362017;
    @DexIgnore
    public static /* final */ int chip_group; // = 2131362018;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362019;
    @DexIgnore
    public static /* final */ int clear_text; // = 2131362122;
    @DexIgnore
    public static /* final */ int confirm_button; // = 2131362140;
    @DexIgnore
    public static /* final */ int container; // = 2131362148;
    @DexIgnore
    public static /* final */ int content; // = 2131362149;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362150;
    @DexIgnore
    public static /* final */ int coordinator; // = 2131362151;
    @DexIgnore
    public static /* final */ int custom; // = 2131362161;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362162;
    @DexIgnore
    public static /* final */ int cut; // = 2131362165;
    @DexIgnore
    public static /* final */ int date_picker_actions; // = 2131362178;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362189;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362190;
    @DexIgnore
    public static /* final */ int design_bottom_sheet; // = 2131362191;
    @DexIgnore
    public static /* final */ int design_menu_item_action_area; // = 2131362192;
    @DexIgnore
    public static /* final */ int design_menu_item_action_area_stub; // = 2131362193;
    @DexIgnore
    public static /* final */ int design_menu_item_text; // = 2131362194;
    @DexIgnore
    public static /* final */ int design_navigation_view; // = 2131362195;
    @DexIgnore
    public static /* final */ int dialog_button; // = 2131362196;
    @DexIgnore
    public static /* final */ int dropdown_menu; // = 2131362206;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362208;
    @DexIgnore
    public static /* final */ int end; // = 2131362215;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362239;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362240;
    @DexIgnore
    public static /* final */ int fade; // = 2131362242;
    @DexIgnore
    public static /* final */ int fill; // = 2131362287;
    @DexIgnore
    public static /* final */ int filled; // = 2131362290;
    @DexIgnore
    public static /* final */ int filter_chip; // = 2131362291;
    @DexIgnore
    public static /* final */ int fixed; // = 2131362294;
    @DexIgnore
    public static /* final */ int forever; // = 2131362303;
    @DexIgnore
    public static /* final */ int ghost_view; // = 2131362536;
    @DexIgnore
    public static /* final */ int ghost_view_holder; // = 2131362537;
    @DexIgnore
    public static /* final */ int group_divider; // = 2131362541;
    @DexIgnore
    public static /* final */ int home; // = 2131362554;
    @DexIgnore
    public static /* final */ int icon; // = 2131362579;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362580;
    @DexIgnore
    public static /* final */ int image; // = 2131362587;
    @DexIgnore
    public static /* final */ int info; // = 2131362603;
    @DexIgnore
    public static /* final */ int italic; // = 2131362619;
    @DexIgnore
    public static /* final */ int item_touch_helper_previous_elevation; // = 2131362621;
    @DexIgnore
    public static /* final */ int labeled; // = 2131362744;
    @DexIgnore
    public static /* final */ int largeLabel; // = 2131362746;
    @DexIgnore
    public static /* final */ int left; // = 2131362749;
    @DexIgnore
    public static /* final */ int line1; // = 2131362752;
    @DexIgnore
    public static /* final */ int line3; // = 2131362754;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362761;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362762;
    @DexIgnore
    public static /* final */ int masked; // = 2131362803;
    @DexIgnore
    public static /* final */ int message; // = 2131362805;
    @DexIgnore
    public static /* final */ int mini; // = 2131362808;
    @DexIgnore
    public static /* final */ int month_grid; // = 2131362811;
    @DexIgnore
    public static /* final */ int month_navigation_bar; // = 2131362812;
    @DexIgnore
    public static /* final */ int month_navigation_fragment_toggle; // = 2131362813;
    @DexIgnore
    public static /* final */ int month_navigation_next; // = 2131362814;
    @DexIgnore
    public static /* final */ int month_navigation_previous; // = 2131362815;
    @DexIgnore
    public static /* final */ int month_title; // = 2131362816;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_selector_frame; // = 2131362817;
    @DexIgnore
    public static /* final */ int mtrl_calendar_days_of_week; // = 2131362818;
    @DexIgnore
    public static /* final */ int mtrl_calendar_frame; // = 2131362819;
    @DexIgnore
    public static /* final */ int mtrl_calendar_main_pane; // = 2131362820;
    @DexIgnore
    public static /* final */ int mtrl_calendar_months; // = 2131362821;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selection_frame; // = 2131362822;
    @DexIgnore
    public static /* final */ int mtrl_calendar_text_input_frame; // = 2131362823;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_selector_frame; // = 2131362824;
    @DexIgnore
    public static /* final */ int mtrl_card_checked_layer_id; // = 2131362825;
    @DexIgnore
    public static /* final */ int mtrl_child_content_container; // = 2131362826;
    @DexIgnore
    public static /* final */ int mtrl_internal_children_alpha_tag; // = 2131362827;
    @DexIgnore
    public static /* final */ int mtrl_picker_fullscreen; // = 2131362828;
    @DexIgnore
    public static /* final */ int mtrl_picker_header; // = 2131362829;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_selection_text; // = 2131362830;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_title_and_selection; // = 2131362831;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_toggle; // = 2131362832;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date; // = 2131362833;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_range_end; // = 2131362834;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_range_start; // = 2131362835;
    @DexIgnore
    public static /* final */ int mtrl_picker_title_text; // = 2131362836;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362837;
    @DexIgnore
    public static /* final */ int navigation_header_container; // = 2131362838;
    @DexIgnore
    public static /* final */ int none; // = 2131362843;
    @DexIgnore
    public static /* final */ int normal; // = 2131362844;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362845;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362846;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362847;
    @DexIgnore
    public static /* final */ int off; // = 2131362865;
    @DexIgnore
    public static /* final */ int on; // = 2131362866;
    @DexIgnore
    public static /* final */ int outline; // = 2131362873;
    @DexIgnore
    public static /* final */ int parallax; // = 2131362878;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362880;
    @DexIgnore
    public static /* final */ int parent_matrix; // = 2131362881;
    @DexIgnore
    public static /* final */ int password_toggle; // = 2131362882;
    @DexIgnore
    public static /* final */ int pin; // = 2131362894;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362930;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362931;
    @DexIgnore
    public static /* final */ int radio; // = 2131362934;
    @DexIgnore
    public static /* final */ int right; // = 2131362948;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362949;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362950;
    @DexIgnore
    public static /* final */ int rounded; // = 2131362963;
    @DexIgnore
    public static /* final */ int save_non_transition_alpha; // = 2131363016;
    @DexIgnore
    public static /* final */ int save_overlay_view; // = 2131363017;
    @DexIgnore
    public static /* final */ int scale; // = 2131363023;
    @DexIgnore
    public static /* final */ int screen; // = 2131363025;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131363027;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131363028;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131363029;
    @DexIgnore
    public static /* final */ int scrollable; // = 2131363032;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131363033;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131363034;
    @DexIgnore
    public static /* final */ int search_button; // = 2131363035;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131363036;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131363037;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131363038;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131363039;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131363040;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131363041;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131363043;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131363047;
    @DexIgnore
    public static /* final */ int selected; // = 2131363048;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131363062;
    @DexIgnore
    public static /* final */ int slide; // = 2131363068;
    @DexIgnore
    public static /* final */ int smallLabel; // = 2131363070;
    @DexIgnore
    public static /* final */ int snackbar_action; // = 2131363071;
    @DexIgnore
    public static /* final */ int snackbar_text; // = 2131363072;
    @DexIgnore
    public static /* final */ int spacer; // = 2131363079;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131363081;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131363085;
    @DexIgnore
    public static /* final */ int src_in; // = 2131363086;
    @DexIgnore
    public static /* final */ int src_over; // = 2131363087;
    @DexIgnore
    public static /* final */ int start; // = 2131363091;
    @DexIgnore
    public static /* final */ int stretch; // = 2131363094;
    @DexIgnore
    public static /* final */ int submenuarrow; // = 2131363096;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131363097;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131363117;
    @DexIgnore
    public static /* final */ int tag_accessibility_actions; // = 2131363119;
    @DexIgnore
    public static /* final */ int tag_accessibility_clickable_spans; // = 2131363120;
    @DexIgnore
    public static /* final */ int tag_accessibility_heading; // = 2131363121;
    @DexIgnore
    public static /* final */ int tag_accessibility_pane_title; // = 2131363122;
    @DexIgnore
    public static /* final */ int tag_screen_reader_focusable; // = 2131363123;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363124;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363125;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363126;
    @DexIgnore
    public static /* final */ int test_checkbox_android_button_tint; // = 2131363130;
    @DexIgnore
    public static /* final */ int test_checkbox_app_button_tint; // = 2131363131;
    @DexIgnore
    public static /* final */ int text; // = 2131363132;
    @DexIgnore
    public static /* final */ int text2; // = 2131363133;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363135;
    @DexIgnore
    public static /* final */ int textSpacerNoTitle; // = 2131363136;
    @DexIgnore
    public static /* final */ int text_input_end_icon; // = 2131363139;
    @DexIgnore
    public static /* final */ int text_input_start_icon; // = 2131363140;
    @DexIgnore
    public static /* final */ int textinput_counter; // = 2131363141;
    @DexIgnore
    public static /* final */ int textinput_error; // = 2131363142;
    @DexIgnore
    public static /* final */ int textinput_helper_text; // = 2131363143;
    @DexIgnore
    public static /* final */ int time; // = 2131363154;
    @DexIgnore
    public static /* final */ int title; // = 2131363156;
    @DexIgnore
    public static /* final */ int titleDividerNoCustom; // = 2131363157;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363159;
    @DexIgnore
    public static /* final */ int top; // = 2131363165;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363166;
    @DexIgnore
    public static /* final */ int touch_outside; // = 2131363168;
    @DexIgnore
    public static /* final */ int transition_current_scene; // = 2131363169;
    @DexIgnore
    public static /* final */ int transition_layout_save; // = 2131363170;
    @DexIgnore
    public static /* final */ int transition_position; // = 2131363171;
    @DexIgnore
    public static /* final */ int transition_scene_layoutid_cache; // = 2131363172;
    @DexIgnore
    public static /* final */ int transition_transform; // = 2131363173;
    @DexIgnore
    public static /* final */ int unchecked; // = 2131363364;
    @DexIgnore
    public static /* final */ int uniform; // = 2131363366;
    @DexIgnore
    public static /* final */ int unlabeled; // = 2131363368;
    @DexIgnore
    public static /* final */ int up; // = 2131363369;
    @DexIgnore
    public static /* final */ int view_offset_helper; // = 2131363421;
    @DexIgnore
    public static /* final */ int visible; // = 2131363447;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363473;
}
