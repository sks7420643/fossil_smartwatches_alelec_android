package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l65 extends k65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public long t;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        v = sparseIntArray;
        sparseIntArray.put(2131362701, 1);
        v.put(2131362642, 2);
    }
    */

    @DexIgnore
    public l65(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 3, u, v));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public l65(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ImageView) objArr[2], (ImageView) objArr[1]);
        this.t = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.s = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
