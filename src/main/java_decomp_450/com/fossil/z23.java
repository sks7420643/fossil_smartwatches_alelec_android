package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z23 implements w23 {
    @DexIgnore
    public static /* final */ tq2<Long> a;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        dr2.a("measurement.id.max_bundles_per_iteration", 0L);
        a = dr2.a("measurement.max_bundles_per_iteration", 2L);
    }
    */

    @DexIgnore
    @Override // com.fossil.w23
    public final long zza() {
        return a.b().longValue();
    }
}
