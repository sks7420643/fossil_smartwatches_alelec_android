package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e21 implements Parcelable.Creator<y31> {
    @DexIgnore
    public /* synthetic */ e21(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public y31 createFromParcel(Parcel parcel) {
        return new y31(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public y31[] newArray(int i) {
        return new y31[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public y31 m12createFromParcel(Parcel parcel) {
        return new y31(parcel, null);
    }
}
