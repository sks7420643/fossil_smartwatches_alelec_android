package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j44 implements k44 {
    @DexIgnore
    public static /* final */ Pattern f; // = Pattern.compile("[^\\p{Alnum}]");
    @DexIgnore
    public static /* final */ String g; // = Pattern.quote("/");
    @DexIgnore
    public /* final */ l44 a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ nb4 d;
    @DexIgnore
    public String e;

    @DexIgnore
    public j44(Context context, String str, nb4 nb4) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str != null) {
            this.b = context;
            this.c = str;
            this.d = nb4;
            this.a = new l44();
        } else {
            throw new IllegalArgumentException("appIdentifier must not be null");
        }
    }

    @DexIgnore
    public static String b(String str) {
        if (str == null) {
            return null;
        }
        return f.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    @DexIgnore
    @Override // com.fossil.k44
    public synchronized String a() {
        if (this.e != null) {
            return this.e;
        }
        SharedPreferences h = t34.h(this.b);
        String id = this.d.getId();
        String string = h.getString("firebase.installation.id", null);
        if (string == null) {
            SharedPreferences d2 = t34.d(this.b);
            String string2 = d2.getString("crashlytics.installation.id", null);
            z24 a2 = z24.a();
            a2.a("No cached FID; legacy id is " + string2);
            if (string2 == null) {
                this.e = a(id, h);
            } else {
                this.e = string2;
                a(string2, id, h, d2);
            }
            return this.e;
        }
        if (string.equals(id)) {
            this.e = h.getString("crashlytics.installation.id", null);
            z24 a3 = z24.a();
            a3.a("Found matching FID, using Crashlytics IID: " + this.e);
            if (this.e == null) {
                this.e = a(id, h);
            }
        } else {
            this.e = a(id, h);
        }
        return this.e;
    }

    @DexIgnore
    public String c() {
        return this.a.a(this.b);
    }

    @DexIgnore
    public String d() {
        return String.format(Locale.US, "%s/%s", a(Build.MANUFACTURER), a(Build.MODEL));
    }

    @DexIgnore
    public String e() {
        return a(Build.VERSION.INCREMENTAL);
    }

    @DexIgnore
    public String f() {
        return a(Build.VERSION.RELEASE);
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public final synchronized void a(String str, String str2, SharedPreferences sharedPreferences, SharedPreferences sharedPreferences2) {
        z24 a2 = z24.a();
        a2.a("Migrating legacy Crashlytics IID: " + str);
        sharedPreferences.edit().putString("crashlytics.installation.id", str).putString("firebase.installation.id", str2).apply();
        sharedPreferences2.edit().remove("crashlytics.installation.id").remove("crashlytics.advertising.id").apply();
    }

    @DexIgnore
    public final synchronized String a(String str, SharedPreferences sharedPreferences) {
        String b2;
        b2 = b(UUID.randomUUID().toString());
        z24 a2 = z24.a();
        a2.a("Created new Crashlytics IID: " + b2);
        sharedPreferences.edit().putString("crashlytics.installation.id", b2).putString("firebase.installation.id", str).apply();
        return b2;
    }

    @DexIgnore
    public final String a(String str) {
        return str.replaceAll(g, "");
    }
}
