package com.fossil;

import com.fossil.zf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bg7<T, R> extends zf7<R>, gd7<T, R> {

    @DexIgnore
    public interface a<T, R> extends zf7.a<R>, gd7<T, R> {
    }

    @DexIgnore
    R get(T t);

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    a<T, R> getGetter();
}
