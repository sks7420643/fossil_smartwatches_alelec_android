package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x40<T> implements c50<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public n40 c;

    @DexIgnore
    public x40() {
        this(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void a(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public final void a(b50 b50) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public final void a(n40 n40) {
        this.c = n40;
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void b(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public final void b(b50 b50) {
        b50.a(this.a, this.b);
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStop() {
    }

    @DexIgnore
    public x40(int i, int i2) {
        if (v50.b(i, i2)) {
            this.a = i;
            this.b = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public final n40 a() {
        return this.c;
    }
}
