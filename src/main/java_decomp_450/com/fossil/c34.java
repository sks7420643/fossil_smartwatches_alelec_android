package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c34 {
    @DexIgnore
    public /* final */ m64 a; // = new m64();
    @DexIgnore
    public /* final */ l14 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public PackageManager d;
    @DexIgnore
    public String e;
    @DexIgnore
    public PackageInfo f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public j44 l;
    @DexIgnore
    public e44 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mo3<y74, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ s74 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;

        @DexIgnore
        public a(String str, s74 s74, Executor executor) {
            this.a = str;
            this.b = s74;
            this.c = executor;
        }

        @DexIgnore
        /* renamed from: a */
        public no3<Void> then(y74 y74) throws Exception {
            try {
                c34.this.a(y74, this.a, this.b, this.c, true);
                return null;
            } catch (Exception e) {
                z24.a().b("Error performing auto configuration.", e);
                throw e;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements mo3<Void, y74> {
        @DexIgnore
        public /* final */ /* synthetic */ s74 a;

        @DexIgnore
        public b(c34 c34, s74 s74) {
            this.a = s74;
        }

        @DexIgnore
        /* renamed from: a */
        public no3<y74> then(Void r1) throws Exception {
            return this.a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements fo3<Void, Object> {
        @DexIgnore
        public c(c34 c34) {
        }

        @DexIgnore
        @Override // com.fossil.fo3
        public Object then(no3<Void> no3) throws Exception {
            if (no3.e()) {
                return null;
            }
            z24.a().b("Error fetching settings.", no3.a());
            return null;
        }
    }

    @DexIgnore
    public c34(l14 l14, Context context, j44 j44, e44 e44) {
        this.b = l14;
        this.c = context;
        this.l = j44;
        this.m = e44;
    }

    @DexIgnore
    public static String e() {
        return y34.e();
    }

    @DexIgnore
    public final boolean b(y74 y74, String str, boolean z) {
        return new i84(c(), y74.b, this.a, e()).a(a(y74.e, str), z);
    }

    @DexIgnore
    public String c() {
        return t34.b(this.c, "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public boolean d() {
        String str;
        try {
            this.i = this.l.c();
            this.d = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            this.e = packageName;
            PackageInfo packageInfo = this.d.getPackageInfo(packageName, 0);
            this.f = packageInfo;
            this.g = Integer.toString(packageInfo.versionCode);
            if (this.f.versionName == null) {
                str = "0.0";
            } else {
                str = this.f.versionName;
            }
            this.h = str;
            this.j = this.d.getApplicationLabel(this.c.getApplicationInfo()).toString();
            this.k = Integer.toString(this.c.getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            z24.a().b("Failed init", e2);
            return false;
        }
    }

    @DexIgnore
    public Context a() {
        return this.c;
    }

    @DexIgnore
    public void a(Executor executor, s74 s74) {
        this.m.c().a(executor, new b(this, s74)).a(executor, new a(this.b.d().b(), s74, executor));
    }

    @DexIgnore
    public final j44 b() {
        return this.l;
    }

    @DexIgnore
    public s74 a(Context context, l14 l14, Executor executor) {
        s74 a2 = s74.a(context, l14.d().b(), this.l, this.a, this.g, this.h, c(), this.m);
        a2.a(executor).a(executor, new c(this));
        return a2;
    }

    @DexIgnore
    public final void a(y74 y74, String str, s74 s74, Executor executor, boolean z) {
        if ("new".equals(y74.a)) {
            if (a(y74, str, z)) {
                s74.a(r74.SKIP_CACHE_LOOKUP, executor);
            } else {
                z24.a().b("Failed to create app with Crashlytics service.", null);
            }
        } else if ("configured".equals(y74.a)) {
            s74.a(r74.SKIP_CACHE_LOOKUP, executor);
        } else if (y74.f) {
            z24.a().a("Server says an update is required - forcing a full App update.");
            b(y74, str, z);
        }
    }

    @DexIgnore
    public final boolean a(y74 y74, String str, boolean z) {
        return new f84(c(), y74.b, this.a, e()).a(a(y74.e, str), z);
    }

    @DexIgnore
    public final x74 a(String str, String str2) {
        return new x74(str, str2, b().b(), this.h, this.g, t34.a(t34.e(a()), str2, this.h, this.g), this.j, g44.determineFrom(this.i).getId(), this.k, "0");
    }
}
