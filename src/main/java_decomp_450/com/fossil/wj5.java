package com.fossil;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wj5 extends bk5 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public LocationSource d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wj5.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = wj5.class.getSimpleName();
        ee7.a((Object) simpleName, "LocationSupportedService::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public final void a(LocationSource.ErrorState errorState) {
        String str;
        ee7.b(errorState, "errorState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = e;
        local.d(str2, "sendNotificationWeather, errorState=" + errorState);
        Intent intent = new Intent();
        if (errorState == LocationSource.ErrorState.LOCATION_PERMISSION_OFF) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.g0.c().getPackageName(), null));
            str = ig5.a(getApplicationContext(), 2131886874);
            ee7.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else if (errorState == LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = ig5.a(getApplicationContext(), 2131887255);
            ee7.a((Object) str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (errorState == LocationSource.ErrorState.LOCATION_SERVICE_OFF) {
            intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
            str = ig5.a(getApplicationContext(), 2131886874);
            ee7.a((Object) str, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        } else {
            str = "";
        }
        PendingIntent activity = PendingIntent.getActivity(this, 0, intent, 134217728);
        qe5 qe5 = qe5.c;
        String string = PortfolioApp.g0.c().getString(2131887265);
        ee7.a((Object) string, "PortfolioApp.instance.ge\u2026ring(R.string.brand_name)");
        ee7.a((Object) activity, "pendingIntent");
        qe5.b(this, 8, string, str, activity, null);
    }

    @DexIgnore
    public final LocationSource e() {
        LocationSource locationSource = this.d;
        if (locationSource != null) {
            return locationSource;
        }
        ee7.d("mLocationSource");
        throw null;
    }
}
