package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g33 implements h33 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Double> b;
    @DexIgnore
    public static /* final */ tq2<Long> c;
    @DexIgnore
    public static /* final */ tq2<Long> d;
    @DexIgnore
    public static /* final */ tq2<String> e;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.test.boolean_flag", false);
        b = dr2.a("measurement.test.double_flag", -3.0d);
        c = dr2.a("measurement.test.int_flag", -2L);
        d = dr2.a("measurement.test.long_flag", -1L);
        e = dr2.a("measurement.test.string_flag", "---");
    }
    */

    @DexIgnore
    @Override // com.fossil.h33
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.h33
    public final double zzb() {
        return b.b().doubleValue();
    }

    @DexIgnore
    @Override // com.fossil.h33
    public final long zzc() {
        return c.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.h33
    public final long zzd() {
        return d.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.h33
    public final String zze() {
        return e.b();
    }
}
