package com.fossil;

import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p17 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public SQLiteDatabase e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements DatabaseErrorHandler {
        @DexIgnore
        public a(p17 p17) {
        }

        @DexIgnore
        public void onCorruption(SQLiteDatabase sQLiteDatabase) {
        }
    }

    @DexIgnore
    public p17(String str, int i, boolean z, int i2) {
        this.b = str;
        this.a = z;
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    public void a() {
        this.e.close();
    }

    @DexIgnore
    public SQLiteDatabase b() {
        return this.e;
    }

    @DexIgnore
    public String c() {
        return "[" + d() + "] ";
    }

    @DexIgnore
    public String d() {
        Thread currentThread = Thread.currentThread();
        return "" + this.c + "," + currentThread.getName() + "(" + currentThread.getId() + ")";
    }

    @DexIgnore
    public SQLiteDatabase e() {
        return this.e;
    }

    @DexIgnore
    public void f() {
        this.e = SQLiteDatabase.openDatabase(this.b, null, net.sqlcipher.database.SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @DexIgnore
    public void g() {
        this.e = SQLiteDatabase.openDatabase(this.b, null, 1, new a(this));
    }

    @DexIgnore
    public static void a(String str) {
        SQLiteDatabase.deleteDatabase(new File(str));
    }
}
