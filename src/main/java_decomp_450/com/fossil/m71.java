package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m71 extends fe7 implements gd7<g70, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m71(ArrayList arrayList) {
        super(1);
        this.a = arrayList;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public Boolean invoke(g70 g70) {
        boolean z;
        Object obj;
        re0 b;
        boolean z2;
        g70 g702 = g70;
        Iterator it = this.a.iterator();
        while (true) {
            z = false;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            s70 s70 = (s70) obj;
            if (s70.getPositionConfig().getDistanceFromCenter() == g702.getPositionConfig().getDistanceFromCenter() && s70.getPositionConfig().getAngle() == g702.getPositionConfig().getAngle()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        s70 s702 = (s70) obj;
        if (!(s702 == null || (b = s702.b()) == null)) {
            z = b.b();
        }
        return Boolean.valueOf(z);
    }
}
