package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ zk3 a;

    @DexIgnore
    public dl3(zk3 zk3) {
        this.a = zk3;
    }

    @DexIgnore
    public final void run() {
        ek3 ek3 = this.a.c;
        Context f = this.a.c.f();
        this.a.c.b();
        ek3.a(new ComponentName(f, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
