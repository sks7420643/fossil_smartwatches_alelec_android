package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h70 extends xg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ArrayList<g70> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<h70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final boolean a(g70 g70, g70 g702) {
            return ee7.a(g70.getFileName(), g702.getFileName()) && !Arrays.equals(g70.getFileData(), g702.getFileData());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public h70 createFromParcel(Parcel parcel) {
            return new h70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public h70[] newArray(int i) {
            return new h70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public h70 m22createFromParcel(Parcel parcel) {
            return new h70(parcel, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<g70, Boolean> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public Boolean invoke(g70 g70) {
            return Boolean.valueOf(g70.e());
        }
    }

    @DexIgnore
    public h70(g70 g70, g70 g702, g70 g703, g70 g704, g70 g705) throws IllegalArgumentException {
        g70.a(new me0(0, 0, 0));
        g702.a(new me0(0, 62, 1));
        g703.a(new me0(90, 62, 1));
        g704.a(new me0(180, 62, 1));
        g705.a(new me0(270, 62, 1));
        this.a.addAll(w97.a((Object[]) new g70[]{g70, g702, g703, g704, g705}));
        e();
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            r51 r51 = r51.F;
            Object[] array = this.a.toArray(new g70[0]);
            if (array != null) {
                yz0.a(jSONObject, r51, yz0.a((k60[]) array));
                return jSONObject;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        } catch (JSONException e) {
            wl0.h.a(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public JSONObject b() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.a.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().f());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.backgrounds", jSONArray);
        ee7.a((Object) put, "JSONObject().put(UIScrip\u2026oundsAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final ArrayList<g70> d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int size = this.a.size() - 1;
        for (int i = 0; i < size; i++) {
            int size2 = this.a.size();
            int i2 = i;
            while (i2 < size2) {
                a aVar = CREATOR;
                g70 g70 = this.a.get(i);
                ee7.a((Object) g70, "backgroundImageList[i]");
                g70 g702 = this.a.get(i2);
                ee7.a((Object) g702, "backgroundImageList[j]");
                if (!aVar.a(g70, g702)) {
                    i2++;
                } else {
                    StringBuilder b2 = yh0.b("Background images ");
                    b2.append(this.a.get(i).getFileName());
                    b2.append(" and  ");
                    b2.append(this.a.get(i2).getFileName());
                    b2.append(' ');
                    b2.append("have same names but different data.");
                    throw new IllegalArgumentException(b2.toString());
                }
            }
        }
        ba7.a((List) this.a, (gd7) b.a);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(h70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.a, ((h70) obj).a) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImageConfig");
    }

    @DexIgnore
    public final g70 getBottomBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            boolean z = true;
            if (t2.getPositionConfig().getAngle() != 180 || t2.getPositionConfig().getDistanceFromCenter() != 62 || t2.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final g70 getLeftBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            boolean z = true;
            if (t2.getPositionConfig().getAngle() != 270 || t2.getPositionConfig().getDistanceFromCenter() != 62 || t2.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final g70 getMainBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 0 && t2.getPositionConfig().getZIndex() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final g70 getRightBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            boolean z = true;
            if (t2.getPositionConfig().getAngle() != 90 || t2.getPositionConfig().getDistanceFromCenter() != 62 || t2.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final g70 getTopBackground() {
        T t;
        Iterator<T> it = this.a.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            T t2 = t;
            boolean z = true;
            if (t2.getPositionConfig().getAngle() != 0 || t2.getPositionConfig().getDistanceFromCenter() != 62 || t2.getPositionConfig().getZIndex() != 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.xg0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(this.a);
        }
    }

    @DexIgnore
    public /* synthetic */ h70(Parcel parcel, zd7 zd7) {
        super(parcel);
        ArrayList createTypedArrayList = parcel.createTypedArrayList(g70.CREATOR);
        if (createTypedArrayList != null) {
            this.a.addAll(createTypedArrayList);
        }
    }
}
