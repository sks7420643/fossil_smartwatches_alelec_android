package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k10 implements uy<Bitmap>, qy {
    @DexIgnore
    public /* final */ Bitmap a;
    @DexIgnore
    public /* final */ dz b;

    @DexIgnore
    public k10(Bitmap bitmap, dz dzVar) {
        u50.a(bitmap, "Bitmap must not be null");
        this.a = bitmap;
        u50.a(dzVar, "BitmapPool must not be null");
        this.b = dzVar;
    }

    @DexIgnore
    public static k10 a(Bitmap bitmap, dz dzVar) {
        if (bitmap == null) {
            return null;
        }
        return new k10(bitmap, dzVar);
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
        this.b.a(this.a);
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return v50.a(this.a);
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<Bitmap> d() {
        return Bitmap.class;
    }

    @DexIgnore
    @Override // com.fossil.qy
    public void a() {
        this.a.prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Bitmap get() {
        return this.a;
    }
}
